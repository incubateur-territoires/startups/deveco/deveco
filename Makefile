build:
	docker compose down --remove-orphans && docker compose build db back front e2e

ci_build:
	docker compose -f docker-compose.yaml -f docker-compose.ci.yaml build db back front e2e

test: build
	docker compose run --name deveco_e2e_runner e2e

ci_test: ci_build
	docker compose run e2e

migrate:
	docker compose up --build --force-recreate migrate

migrate_fixtures:
	docker compose up --build --force-recreate migrate_fixtures
