ALTER TABLE "etablissement_creation" ADD COLUMN "commune_id" varchar(5);--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation" ADD CONSTRAINT "etablissement_creation_commune_id_commune_id_fk" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
