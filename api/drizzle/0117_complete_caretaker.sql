CREATE TABLE IF NOT EXISTS "equipe__territoire_industrie" (
	"equipe_id" integer NOT NULL,
	"territoire_industrie_id" varchar(7) NOT NULL,
	CONSTRAINT "pk_equipe__territoire_industrie" PRIMARY KEY("equipe_id","territoire_industrie_id")
);
--> statement-breakpoint
DROP MATERIALIZED VIEW "public"."equipe__commune_vue";--> statement-breakpoint
ALTER TABLE "commune" ADD COLUMN "territoire_industrie_id" varchar(7);--> statement-breakpoint
ALTER TABLE "equipe_type" ALTER COLUMN "id" TYPE varchar(20);--> statement-breakpoint
ALTER TABLE "equipe_demande_autorisation" ADD COLUMN "territoire_industrie_id" varchar(7);
DO $$ BEGIN
 ALTER TABLE "equipe__territoire_industrie" ADD CONSTRAINT "fk_equipe__territoire_industrie__equipe" FOREIGN KEY ("equipe_id") REFERENCES "public"."equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__territoire_industrie" ADD CONSTRAINT "fk_equipe__territoire_industrie__territoire_industrie" FOREIGN KEY ("territoire_industrie_id") REFERENCES "public"."territoire_industrie"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "fk_commune__territoireIndustrie" FOREIGN KEY ("territoire_industrie_id") REFERENCES "public"."territoire_industrie"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
CREATE MATERIALIZED VIEW "public"."equipe__commune_vue" AS (
	select
		"equipe"."id" as "eqcomvue_equipe_id",
		"commune"."id" as "eqcomvue_commune_id"
	from "equipe"
	left join "equipe__commune" on "equipe__commune"."equipe_id" = "equipe"."id"
	left join "equipe__metropole" on "equipe__metropole"."equipe_id" = "equipe"."id"
	left join "equipe__epci" on "equipe__epci"."equipe_id" = "equipe"."id"
	left join "equipe__petr" on "equipe__petr"."equipe_id" = "equipe"."id"
	left join "equipe__territoire_industrie" on "equipe__territoire_industrie"."equipe_id" = "equipe"."id"
	left join "equipe__departement" on "equipe__departement"."equipe_id" = "equipe"."id"
	left join "equipe__region" on "equipe__region"."equipe_id" = "equipe"."id"
	left join "commune" on (
		"commune"."id" = "equipe__commune"."commune_id"
		or "commune"."commune_parent_id" = "equipe__commune"."commune_id"
		or "commune"."metropole_id" = "equipe__metropole"."metropole_id"
		or "commune"."epci_id" = "equipe__epci"."epci_id"
		or "commune"."petr_id" = "equipe__petr"."petr_id"
		or "commune"."territoire_industrie_id" = "equipe__territoire_industrie"."territoire_industrie_id"
		or "commune"."departement_id" = "equipe__departement"."departement_id"
		or "commune"."region_id" = "equipe__region"."region_id"
	)
);
CREATE UNIQUE INDEX ON equipe__commune_vue(eqcomvue_equipe_id, eqcomvue_commune_id);
--> statement-breakpoint
INSERT INTO territoire_type
VALUES ('territoire_industrie', 'territoire d''industrie');
--> statement-breakpoint
INSERT INTO equipe_type
VALUES ('territoire_industrie', 'territoire d''industrie');
--> statement-breakpoint
\copy source.anct__commune_territoire_d_industrie FROM '_sources/anct_commune-territoire-industrie_2023.csv' CSV HEADER;
--> statement-breakpoint
DELETE FROM source.anct__commune_territoire_d_industrie
WHERE insee_com NOT IN (SELECT id FROM commune);
--> statement-breakpoint
INSERT INTO territoire_industrie (id, nom)
SELECT DISTINCT id_ti, lib_ti
FROM source.anct__commune_territoire_d_industrie;
--> statement-breakpoint
INSERT INTO commune__territoire_industrie (commune_id, territoire_industrie_id)
SELECT insee_com, id_ti
FROM source.anct__commune_territoire_d_industrie;
--> statement-breakpoint
UPDATE commune
SET territoire_industrie_id = commune_ti.territoire_industrie_id
FROM commune__territoire_industrie commune_ti
WHERE commune.id = commune_ti.commune_id;
