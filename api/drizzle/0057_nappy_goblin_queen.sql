CREATE TABLE IF NOT EXISTS
  "equipe_local_contribution_vacance_motif_type" (
    "id" varchar NOT NULL,
    "nom" varchar NOT NULL,
    CONSTRAINT "pk_equipe_local_contribution_vacance_motif_type" PRIMARY KEY ("id")
  );

--> statement-breakpoint
ALTER TABLE "equipe__local"
RENAME COLUMN "contribution_fond_euros" TO "contribution_fonds_euros";

--> statement-breakpoint
ALTER TABLE "equipe__local_contribution"
RENAME COLUMN "fond_euros" TO "fonds_euros";

--> statement-breakpoint
ALTER TABLE "equipe__local_contribution"
DROP CONSTRAINT "fk_equipe__local_contribution__vacance_motif_type";

--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local_contribution" ADD CONSTRAINT "fk_equipe__local_contribution__vacance_motif_type" FOREIGN KEY ("equipe_local_contribution_vacance_motif_type_id") REFERENCES "equipe_local_contribution_vacance_motif_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

INSERT INTO
  equipe_local_contribution_bail_type (id, nom)
VALUES
  ('commercial', 'commercial'),
  ('derogatoire', 'dérogatoire'),
  ('precaire', 'précaire');

--> statement-breakpoint
INSERT INTO
  equipe_local_contribution_vacance_type (id, nom)
VALUES
  ('conjoncturelle', 'conjoncturelle'),
  ('classique', 'classique'),
  ('autre', 'autre');

--> statement-breakpoint
INSERT INTO
  equipe_local_contribution_vacance_motif_type (id, nom)
VALUES
  ('choix', 'choix'),
  ('commercialisation', 'commercialisation'),
  ('peril', 'péril'),
  ('travaux', 'travaux');

--> statement-breakpoint
