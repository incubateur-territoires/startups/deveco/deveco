CREATE TABLE IF NOT EXISTS "source"."etalab__etablissement_geoloc" (
	"siret" varchar(14) NOT NULL,
	"longitude" double precision,
	"latitude" double precision,
	"geo_score" double precision,
	"geo_adresse" varchar,
	"etranger_commune" varchar,
	"etranger_pays" varchar,
	"id" serial NOT NULL,
	"cedex_code" varchar(9),
	"cedex_nom" varchar,
	"code_postal" varchar(5),
	"distribution_speciale" varchar,
	"geolocalisation" "geometry(Point,4326)",
	"numero" varchar,
	"voie_nom" varchar,
	"commune_id" varchar(5),
	CONSTRAINT "pk_etalab__etablissement_geoloc" PRIMARY KEY("siret"),
	CONSTRAINT "uk_etalab__etablissement_geoloc__etablissement" UNIQUE("siret")
);
--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_etalab__etablissement_geoloc__etablissement" ON "source"."etalab__etablissement_geoloc" ("siret");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_etalab__etablissement_geoloc__commune" ON "source"."etalab__etablissement_geoloc" ("commune_id");--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "source"."etalab__etablissement_geoloc" ADD CONSTRAINT "fk_etalab__etablissement_geoloc__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

\copy source.etalab__etablissement_geoloc(siret,longitude,latitude,geo_score,geo_adresse) from '_sources/etalab__etablissement_geoloc.csv' csv header;--> statement-breakpoint

