DROP TABLE "etablissement_subvention";--> statement-breakpoint
UPDATE "source"."dgcl__subvention" SET "montant" = REPLACE("montant", ',', '.');
ALTER TABLE "source"."dgcl__subvention" ALTER COLUMN "montant" SET DATA TYPE double precision USING montant::double precision;--> statement-breakpoint
ALTER TABLE "source"."dgcl__subvention" ALTER COLUMN "convention_date" SET DATA TYPE date USING convention_date::date;;--> statement-breakpoint
ALTER TABLE "source"."dgcl__subvention" ALTER COLUMN "versement_date" SET DATA TYPE date USING versement_date::date;
