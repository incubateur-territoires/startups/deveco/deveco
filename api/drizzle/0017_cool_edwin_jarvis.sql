CREATE TABLE IF NOT EXISTS "recherche_sauvegardee" (
	"id" serial NOT NULL,
	"token" uuid NOT NULL,
	"creation_date" date DEFAULT now() NOT NULL,
	"equipe_id" integer NOT NULL,
	"deveco_id" integer NOT NULL,
	"params" jsonb,
	"props" jsonb
);
