
CREATE schema IF NOT EXISTS "old";

CREATE TABLE old.equipe_local_contribution_adresse AS
TABLE equipe_local_contribution_adresse;

CREATE TABLE old.equipe_local_contribution AS
TABLE equipe_local_contribution;

CREATE TABLE old.equipe_local__etiquette AS
TABLE equipe_local__etiquette;

CREATE TABLE old.equipe_local__contact AS
TABLE equipe_local__contact;

CREATE TABLE old.action_equipe_local AS
TABLE action_equipe_local;

CREATE TABLE old.action_equipe_local_contact AS
TABLE action_equipe_local_contact;

CREATE TABLE old.equipe_local_contribution__equipe_local_contribution_type AS
TABLE equipe_local_contribution__equipe_local_contribution_type;

CREATE TABLE old.proprietaire_etablissement AS
TABLE proprietaire_etablissement;

CREATE TABLE old.action_proprietaire_etablissement AS
TABLE action_proprietaire_etablissement;

TRUNCATE TABLE equipe_local_contribution_adresse, equipe_local_contribution, equipe_local__etiquette, equipe_local__contact, action_equipe_local, action_equipe_local_contact, equipe_local_contribution__equipe_local_contribution_type, proprietaire_etablissement, action_proprietaire_etablissement;

CREATE TABLE IF NOT EXISTS "equipe__local" (
	"equipe_id" integer NOT NULL,
	"local_id" varchar(12) NOT NULL,
	"invariant" varchar(10) NOT NULL,
	"local_categorie_type_id" varchar,
	"local_nature_type_id" varchar NOT NULL,
	"commune_id" varchar(5),
	"code_postal" varchar(5),
	"numero" varchar,
	"voie_nom" varchar,
	"geolocalisation" geometry(Point,4326),
	"consultation_at" date,
	"contribution_nom" varchar,
	"equipe_local_contribution_statut_type_id" varchar,
	"contribution_actif" boolean DEFAULT true,
	CONSTRAINT "pk_equipe__local" PRIMARY KEY("equipe_id","local_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "local" (
	"id" varchar(12) NOT NULL,
	"invariant" varchar(10) NOT NULL,
	"local_nature_type_id" varchar NOT NULL,
	"local_categorie_type_id" varchar,
	"commune_id" varchar(5),
	"section" varchar(2),
	"parcelle" varchar(4),
	"niveau" varchar(2),
	"surface_totale" integer,
	"surface_vente" integer,
	"surface_reserve" integer,
	"surface_exterieure_non_couverte" integer,
	"surface_stationnement_couvert" integer,
	"surface_stationnement_non_couvert" integer,
	CONSTRAINT "pk_local" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "local_adresse" (
	"local_id" varchar NOT NULL
) INHERITS ("adresse");

CREATE TABLE IF NOT EXISTS "local_categorie_type" (
	"id" varchar NOT NULL,
	"nom" varchar NOT NULL,
	CONSTRAINT "pk_local_categorie_type" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "local_nature_type" (
	"id" varchar NOT NULL,
	"nom" varchar NOT NULL,
	CONSTRAINT "pk_local_nature_type" PRIMARY KEY("id")
);

--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "local__proprietaire_etablissement" (
	"local_id" varchar(12) NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	CONSTRAINT "pk_local__proprietaire_etablissement" PRIMARY KEY("local_id","etablissement_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "local__proprietaire_personne" (
	"local_id" varchar(12) NOT NULL,
	"nom" varchar NOT NULL,
	"prenom" varchar,
	CONSTRAINT "pk_local__proprietaire_personne" PRIMARY KEY("local_id","nom")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_cerema_local" (
	"invar" varchar(10),
	"dnvoiri" varchar(4),
	"dindic" varchar(1),
	"dvoilib" varchar(30),
	"idcom" varchar(5),
	"ccosec" varchar(2),
	"dnupla" varchar(4),
	"cconlc" varchar(2),
	"typeact" varchar(4),
	"dniv" varchar(2),
	"sprincp" integer,
	"ssecp" integer,
	"ssecncp" integer,
	"sparkp" integer,
	"sparkncp" integer,
	"idprocpte" varchar(11),
	"geomloc" geometry(Point,2154)
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_cerema_local_categorie" (
	"id" varchar(4) PRIMARY KEY NOT NULL,
	"nom" varchar
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_cerema_local_nature" (
	"id" varchar(2) PRIMARY KEY NOT NULL,
	"nom" varchar
);
--> statement-breakpoint
DROP TABLE "equipe_local_contribution_adresse";--> statement-breakpoint
DROP TABLE "action_proprietaire_etablissement";--> statement-breakpoint
DROP TABLE "proprietaire_etablissement";--> statement-breakpoint
ALTER TABLE "equipe_local__contact" DROP CONSTRAINT "fk_equipe_local__contact__equipe_local_contribution";
--> statement-breakpoint
ALTER TABLE "equipe_local_contribution__equipe_local_contribution_type" DROP CONSTRAINT "fk_elc__elct__elct";
--> statement-breakpoint
ALTER TABLE "equipe_local__etiquette" DROP CONSTRAINT "fk_equipe_local__etiquette__equipe_local_contribution";
--> statement-breakpoint
ALTER TABLE "action_equipe_local" DROP CONSTRAINT "fk_action_equipe_local__equipe_local_contribution";
--> statement-breakpoint
ALTER TABLE "action_equipe_local_contact" DROP CONSTRAINT "fk_action_equipe_local_contact__equipe_local_contribution";
--> statement-breakpoint
DROP INDEX IF EXISTS "idx_action_equipe_local";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_action_equipe_local_contact";--> statement-breakpoint
ALTER TABLE "equipe_local_contribution" ALTER COLUMN "local_id" SET DATA TYPE varchar(12);--> statement-breakpoint
ALTER TABLE "equipe_local_contribution" ALTER COLUMN "local_id" SET NOT NULL;--> statement-breakpoint
ALTER TABLE "equipe_local_contribution_type" ALTER COLUMN "id" SET DATA TYPE varchar;--> statement-breakpoint
ALTER TABLE "equipe_local__contact" ADD COLUMN "local_id" varchar NOT NULL;--> statement-breakpoint
ALTER TABLE "equipe_local__contact" ADD COLUMN "equipe_id" integer NOT NULL;--> statement-breakpoint
ALTER TABLE "equipe_local__contact" ADD COLUMN "fonction" varchar;--> statement-breakpoint
ALTER TABLE "equipe_local_contribution__equipe_local_contribution_type" ADD COLUMN "equipe_id" integer NOT NULL;--> statement-breakpoint
ALTER TABLE "equipe_local_contribution__equipe_local_contribution_type" ADD COLUMN "local_id" varchar(12) NOT NULL;--> statement-breakpoint
ALTER TABLE "equipe_local__etiquette" ADD COLUMN "equipe_id" integer NOT NULL;--> statement-breakpoint
ALTER TABLE "equipe_local__etiquette" ADD COLUMN "local_id" varchar(12) NOT NULL;--> statement-breakpoint
ALTER TABLE "action_equipe_local" ADD COLUMN "local_id" varchar(12) NOT NULL;--> statement-breakpoint
ALTER TABLE "action_equipe_local" ADD COLUMN "equipe_id" integer NOT NULL;--> statement-breakpoint
ALTER TABLE "action_equipe_local_contact" ADD COLUMN "equipe_id" integer NOT NULL;--> statement-breakpoint
ALTER TABLE "action_equipe_local_contact" ADD COLUMN "local_id" varchar(12) NOT NULL;--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_equipe_local" ON "action_equipe_local" ("evenement_id","local_id","equipe_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_equipe_local_contact" ON "action_equipe_local_contact" ("evenement_id","equipe_id","local_id","personne_id","action_type_id");--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local_contribution" ADD CONSTRAINT "fk_equipe_local_contribution__equipe_local" FOREIGN KEY ("equipe_id","local_id") REFERENCES "equipe__local"("equipe_id","local_id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local__contact" ADD CONSTRAINT "fk_equipe_local__contact__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local__contact" ADD CONSTRAINT "fk_equipe_local__contact__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local__contact" ADD CONSTRAINT "fk_equipe_local__contact__equipe_local" FOREIGN KEY ("equipe_id","local_id") REFERENCES "equipe__local"("equipe_id","local_id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local_contribution__equipe_local_contribution_type" ADD CONSTRAINT "fk_elc__elct__el" FOREIGN KEY ("equipe_id","local_id") REFERENCES "equipe__local"("equipe_id","local_id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local_contribution__equipe_local_contribution_type" ADD CONSTRAINT "fk_elc__elct__elct" FOREIGN KEY ("equipe_local_contribution_type_id") REFERENCES "equipe_local_contribution_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local__etiquette" ADD CONSTRAINT "fk_equipe_local__etiquette__equipe_local" FOREIGN KEY ("equipe_id","local_id") REFERENCES "equipe__local"("equipe_id","local_id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local__etiquette" ADD CONSTRAINT "fk_equipe_local__etiquette__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local__etiquette" ADD CONSTRAINT "fk_equipe_local__etiquette__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local" ADD CONSTRAINT "fk_action_equipe_local__equipe_local" FOREIGN KEY ("equipe_id","local_id") REFERENCES "equipe__local"("equipe_id","local_id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local_contact" ADD CONSTRAINT "fk_action_equipe_local_contact__equipe_local" FOREIGN KEY ("equipe_id","local_id") REFERENCES "equipe__local"("equipe_id","local_id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local_contact" ADD CONSTRAINT "fk_action_equipe_local_contact__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local_contact" ADD CONSTRAINT "fk_action_equipe_local_contact__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
ALTER TABLE "action_equipe_local" DROP COLUMN IF EXISTS "equipe_local_contribution_id";--> statement-breakpoint
ALTER TABLE "action_equipe_local_contact" DROP COLUMN IF EXISTS "equipe_local_contribution_id";--> statement-breakpoint
ALTER TABLE "equipe_local__contact" DROP COLUMN IF EXISTS "equipe_local_contribution_id";--> statement-breakpoint
ALTER TABLE "equipe_local_contribution__equipe_local_contribution_type" DROP COLUMN IF EXISTS "equipe_local_contribution_id";--> statement-breakpoint
ALTER TABLE "equipe_local__etiquette" DROP COLUMN IF EXISTS "equipe_local_contribution_id";--> statement-breakpoint
ALTER TABLE "equipe_local_contribution" DROP COLUMN IF EXISTS "id";--> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "equipe__local" ADD CONSTRAINT "fk_equipe__local__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local" ADD CONSTRAINT "fk_equipe__local__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local" ADD CONSTRAINT "local_commune_id_commune_id_fk" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local" ADD CONSTRAINT "fk_local__categorie_type" FOREIGN KEY ("local_categorie_type_id") REFERENCES "local_categorie_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local" ADD CONSTRAINT "fk_local__nature_type" FOREIGN KEY ("local_nature_type_id") REFERENCES "local_nature_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local_adresse" ADD CONSTRAINT "local_adresse_commune_id_commune_id_fk" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local__proprietaire_etablissement" ADD CONSTRAINT "fk_local__proprietaire_etablissement__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local__proprietaire_etablissement" ADD CONSTRAINT "fk_local__proprietaire_etablissement__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local__proprietaire_personne" ADD CONSTRAINT "fk_local__proprietaire_personne__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
ALTER TABLE "equipe_local_contribution" ADD CONSTRAINT pk_equipe_local_contribution PRIMARY KEY(equipe_id,local_id);--> statement-breakpoint
ALTER TABLE "equipe_local__contact" ADD CONSTRAINT pk_equipe_local__contact PRIMARY KEY(local_id,equipe_id,personne_id);--> statement-breakpoint
--> statement-breakpoint
ALTER TABLE "equipe_local_contribution__equipe_local_contribution_type" ADD CONSTRAINT pk_equipe_local_contribution__equipe_local_contribution_type PRIMARY KEY(equipe_local_contribution_type_id,equipe_id,local_id);--> statement-breakpoint
ALTER TABLE "equipe_local__etiquette" ADD CONSTRAINT pk_equipe_local__etiquette PRIMARY KEY(local_id,equipe_id,etiquette_id);--> statement-breakpoint

\copy source.source_cerema_local_nature(id, nom) FROM '_sources/cerema_local_nature_2022.csv' delimiter ',' csv header;--> statement-breakpoint

INSERT INTO local_nature_type (id, nom) SELECT id, nom FROM source.source_cerema_local_nature;--> statement-breakpoint

\copy source.source_cerema_local_categorie(id, nom) FROM '_sources/cerema_local_categorie_2022.csv' delimiter ',' csv header;--> statement-breakpoint

INSERT INTO local_categorie_type (id, nom) SELECT id, nom FROM source.source_cerema_local_categorie;--> statement-breakpoint

TRUNCATE source.source_cerema_local;--> statement-breakpoint
TRUNCATE local CASCADE;--> statement-breakpoint
TRUNCATE equipe__local CASCADE;--> statement-breakpoint

\copy source.source_cerema_local(invar, dnvoiri, dindic, dvoilib, idcom, geomloc, ccosec, dnupla, cconlc, typeact, dniv, sprincp, ssecp, ssecncp, sparkp, sparkncp, idprocpte) FROM '_sources/cerema_local_fixtures_2022.csv' delimiter ',' csv header;--> statement-breakpoint
--
INSERT INTO local (
	id,
	invariant,
	local_nature_type_id,
	local_categorie_type_id,
	commune_id,
	section,
	parcelle,
	niveau,
	surface_vente,
	surface_reserve,
	surface_exterieure_non_couverte,
	surface_stationnement_couvert,
	surface_stationnement_non_couvert
)
SELECT
	LEFT(idcom, 2) || invar,
	invar,
	cconlc,
	typeact,
	idcom,
	ccosec,
	dnupla,
	dniv,
	sprincp,
	ssecp,
	ssecncp,
	sparkp,
	sparkncp
FROM source.source_cerema_local;
--
INSERT INTO local_adresse (
	local_id,
		commune_id,
	voie_nom,
	numero,
	geolocalisation
)
SELECT
	LEFT(idcom, 2) || invar,
	idcom,
	replace(dvoilib, '  ', ' '),
	COALESCE(regexp_replace(dnvoiri, '^0*', '', 'g'), '') || COALESCE(dindic, ''),
	ST_SETSRID(geomloc, 4326)
FROM source.source_cerema_local;
--
INSERT INTO equipe__local (
	equipe_id,
	local_id,
	invariant,
	commune_id,
	code_postal,
	numero,
	voie_nom,
	geolocalisation,
	contribution_nom,
	equipe_local_contribution_statut_type_id,
	contribution_actif,
	local_categorie_type_id,
	local_nature_type_id
)
SELECT
	equipe__commune_vue.equipe_id,
	local.id,
	local.invariant,
	local.commune_id,
	adresse.code_postal,
	adresse.numero,
	adresse.voie_nom,
	adresse.geolocalisation,
	contribution.nom,
	contribution.equipe_local_contribution_statut_type_id,
	contribution.actif,
	local.local_categorie_type_id,
	local.local_nature_type_id
FROM local
JOIN local_adresse adresse ON adresse.local_id = local.id
JOIN equipe__commune_vue ON local.commune_id = equipe__commune_vue.commune_id
LEFT JOIN equipe_local_contribution contribution ON contribution.local_id = local.id AND contribution.equipe_id = equipe__commune_vue.equipe_id;
