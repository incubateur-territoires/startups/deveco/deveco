CREATE TABLE IF NOT EXISTS "equipe__local__demande" (
	"equipe_id" integer NOT NULL,
	"local_id" varchar(14) NOT NULL,
	"demande_id" integer NOT NULL,
	CONSTRAINT "pk_equipe__local__demande" PRIMARY KEY("equipe_id","local_id","demande_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__local__echange" (
	"equipe_id" integer NOT NULL,
	"local_id" varchar(14) NOT NULL,
	"echange_id" integer NOT NULL,
	CONSTRAINT "pk_equipe__local__echange" PRIMARY KEY("equipe_id","local_id","echange_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__local__rappel" (
	"equipe_id" integer NOT NULL,
	"local_id" varchar(14) NOT NULL,
	"rappel_id" integer NOT NULL,
	CONSTRAINT "pk_equipe__local__rappel" PRIMARY KEY("equipe_id","local_id","rappel_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "action_zonage" (
	"evenement_id" integer NOT NULL,
	"created_at" timestamp with time zone DEFAULT now() NOT NULL,
	"action_type_id" varchar NOT NULL,
	"diff" jsonb,
	"zonage_id" integer NOT NULL
);
--> statement-breakpoint
ALTER TABLE "equipe__etablissement_contribution" ADD COLUMN "creation_date" timestamp with time zone DEFAULT now() NOT NULL;--> statement-breakpoint
ALTER TABLE "equipe__etablissement_contribution" ADD COLUMN "modification_date" timestamp with time zone;--> statement-breakpoint
ALTER TABLE "equipe__etablissement_contribution" ADD COLUMN "modification_compte_id" integer;--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" ADD COLUMN "creation_date" timestamp with time zone DEFAULT now() NOT NULL;--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" ADD COLUMN "modification_date" timestamp with time zone;--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" ADD COLUMN "modification_compte_id" integer;--> statement-breakpoint
ALTER TABLE "demande" ADD COLUMN "nom" varchar;--> statement-breakpoint
ALTER TABLE "demande" ADD COLUMN "description" text;--> statement-breakpoint
ALTER TABLE "demande" ADD COLUMN "cloture_date" date;--> statement-breakpoint
ALTER TABLE "demande" ADD COLUMN "creation_date" timestamp with time zone DEFAULT now() NOT NULL;--> statement-breakpoint
ALTER TABLE "demande" ADD COLUMN "modification_date" timestamp with time zone;--> statement-breakpoint
ALTER TABLE "demande" ADD COLUMN "modification_compte_id" integer;--> statement-breakpoint
ALTER TABLE "demande" ADD COLUMN "suppression_date" date;--> statement-breakpoint
ALTER TABLE "echange" ADD COLUMN "creation_date" timestamp with time zone DEFAULT now() NOT NULL;--> statement-breakpoint
ALTER TABLE "echange" ADD COLUMN "modification_date" timestamp with time zone;--> statement-breakpoint
ALTER TABLE "echange" ADD COLUMN "modification_compte_id" integer;--> statement-breakpoint
ALTER TABLE "echange" ADD COLUMN "suppression_date" date;--> statement-breakpoint
ALTER TABLE "rappel" ADD COLUMN "cloture_date" date;--> statement-breakpoint
ALTER TABLE "rappel" ADD COLUMN "creation_date" timestamp with time zone DEFAULT now() NOT NULL;--> statement-breakpoint
ALTER TABLE "rappel" ADD COLUMN "modification_date" timestamp with time zone;--> statement-breakpoint
ALTER TABLE "rappel" ADD COLUMN "modification_compte_id" integer;--> statement-breakpoint
ALTER TABLE "rappel" ADD COLUMN "suppression_date" date;--> statement-breakpoint
ALTER TABLE "etablissement_creation" ADD COLUMN "creation_date" timestamp with time zone DEFAULT now() NOT NULL;--> statement-breakpoint
ALTER TABLE "etablissement_creation" ADD COLUMN "modification_date" timestamp with time zone;--> statement-breakpoint
ALTER TABLE "etablissement_creation" ADD COLUMN "modification_compte_id" integer;--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_zonage" ON "action_zonage" ("evenement_id","zonage_id","action_type_id");--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement_contribution" ADD CONSTRAINT "fk_equipe__etablissement_contribution__modification_compte" FOREIGN KEY ("modification_compte_id") REFERENCES "compte"("id") ON DELETE set null ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local_contribution" ADD CONSTRAINT "fk_equipe__local_contribution__modification_compte" FOREIGN KEY ("modification_compte_id") REFERENCES "compte"("id") ON DELETE set null ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "demande" ADD CONSTRAINT "fk_demande__modification_compte" FOREIGN KEY ("modification_compte_id") REFERENCES "compte"("id") ON DELETE set null ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "echange" ADD CONSTRAINT "fk_echange__modification_compte" FOREIGN KEY ("modification_compte_id") REFERENCES "compte"("id") ON DELETE set null ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "rappel" ADD CONSTRAINT "fk_rappel__modification_compte" FOREIGN KEY ("modification_compte_id") REFERENCES "compte"("id") ON DELETE set null ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation" ADD CONSTRAINT "fk_etablissementCreation__modification_compte" FOREIGN KEY ("modification_compte_id") REFERENCES "compte"("id") ON DELETE set null ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local__demande" ADD CONSTRAINT "fk_equipe__local__demande__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local__demande" ADD CONSTRAINT "fk_equipe__local__demande__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local__demande" ADD CONSTRAINT "fk_equipe__local__demande__demande" FOREIGN KEY ("demande_id") REFERENCES "demande"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local__echange" ADD CONSTRAINT "fk_equipe__local__echange__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local__echange" ADD CONSTRAINT "fk_equipe__local__echange__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local__echange" ADD CONSTRAINT "fk_equipe__local__echange__echange" FOREIGN KEY ("echange_id") REFERENCES "echange"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local__rappel" ADD CONSTRAINT "fk_equipe__local__rappel__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local__rappel" ADD CONSTRAINT "fk_equipe__local__rappel__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local__rappel" ADD CONSTRAINT "fk_equipe__local__rappel__rappel" FOREIGN KEY ("rappel_id") REFERENCES "rappel"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_zonage" ADD CONSTRAINT "fk_action_zonage__zonage" FOREIGN KEY ("zonage_id") REFERENCES "zonage"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_zonage" ADD CONSTRAINT "fk_action_zonage__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_zonage" ADD CONSTRAINT "fk_action_zonage__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
