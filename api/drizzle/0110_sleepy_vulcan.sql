CREATE TABLE IF NOT EXISTS "etablissement_creation__createur_diplome_niveau_type" (
	"id" varchar NOT NULL,
	"nom" varchar NOT NULL,
	CONSTRAINT "pk_etablissement_creation__createur_diplome_niveau_type" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_creation__commune" (
	"equipe_id" integer NOT NULL,
	"etablissement_creation_id" integer NOT NULL,
	"commune_id" varchar(5) NOT NULL,
	CONSTRAINT "pk_etablissement_creation__commune" PRIMARY KEY("equipe_id","etablissement_creation_id","commune_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_creation_contact_origine" (
	"id" varchar NOT NULL,
	"nom" varchar NOT NULL,
	CONSTRAINT "pk_etablissement_creation_contact_origine" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_creation_projet_type" (
	"id" varchar NOT NULL,
	"nom" varchar NOT NULL,
	CONSTRAINT "pk_etablissement_creation_projet_type" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_creation_ressource" (
	"id" serial NOT NULL,
	"equipe_id" integer NOT NULL,
	"etablissement_creation_id" integer NOT NULL,
	"nom" varchar,
	"lien" varchar,
	"ressource_type_id" varchar,
	CONSTRAINT "pk_etablissement_creation_ressource" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_creation_secteur_activite" (
	"id" varchar NOT NULL,
	"nom" varchar NOT NULL,
	CONSTRAINT "pk_etablissement_creation_secteur_activite" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_creation_source_financement" (
	"id" varchar NOT NULL,
	"nom" varchar NOT NULL,
	CONSTRAINT "pk_etablissement_creation_source_financement" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_creation__createur_profession_situation_type" (
	"id" varchar NOT NULL,
	"nom" varchar NOT NULL,
	CONSTRAINT "pk_etablissement_creation__createur_profession_situation_type" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "action_etablissement_creation_createur" (
	"evenement_id" integer NOT NULL,
	"created_at" timestamp with time zone DEFAULT now() NOT NULL,
	"action_type_id" varchar NOT NULL,
	"diff" jsonb,
	"equipe_id" integer NOT NULL,
	"etablissement_creation_id" integer NOT NULL,
	"createur_id" integer NOT NULL
);
--> statement-breakpoint
ALTER TABLE "etablissement_creation" DROP CONSTRAINT "fk_etablissement_creation__commune";
--> statement-breakpoint
ALTER TABLE "etablissement_creation" DROP CONSTRAINT "fk_etabCrea__modification_compte";
--> statement-breakpoint
ALTER TABLE "etablissement_creation" ADD COLUMN "projet_type_id" varchar;--> statement-breakpoint
ALTER TABLE "etablissement_creation" ADD COLUMN "contact_origine_id" varchar;--> statement-breakpoint
ALTER TABLE "etablissement_creation" ADD COLUMN "contact_date" date;--> statement-breakpoint
ALTER TABLE "etablissement_creation" ADD COLUMN "secteur_activite_id" varchar;--> statement-breakpoint
ALTER TABLE "etablissement_creation" ADD COLUMN "source_financement_id" varchar;--> statement-breakpoint
ALTER TABLE "etablissement_creation" ADD COLUMN "categorie_juridique_envisagee" varchar;--> statement-breakpoint
ALTER TABLE "etablissement_creation" ADD COLUMN "contrat_accompagnement" boolean DEFAULT false;--> statement-breakpoint
ALTER TABLE "etablissement_creation" ADD COLUMN "recherche_local" boolean DEFAULT false;--> statement-breakpoint
ALTER TABLE "etablissement_creation__createur" ADD COLUMN "diplome_niveau_type_id" varchar;--> statement-breakpoint
ALTER TABLE "etablissement_creation__createur" ADD COLUMN "profession_situation_type_id" varchar;--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_etablissement_creation_ressource" ON "etablissement_creation_ressource" ("equipe_id","etablissement_creation_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_etablissement_creation_createur" ON "action_etablissement_creation_createur" ("evenement_id","etablissement_creation_id","action_type_id");--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation" ADD CONSTRAINT "fk_etablissement_creation__modification_compte" FOREIGN KEY ("modification_compte_id") REFERENCES "compte"("id") ON DELETE set null ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation" ADD CONSTRAINT "fk_etablissement_creation__e_c_projet_type" FOREIGN KEY ("projet_type_id") REFERENCES "etablissement_creation_projet_type"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation" ADD CONSTRAINT "fk_etablissement_creation__e_c_contact_origine" FOREIGN KEY ("contact_origine_id") REFERENCES "etablissement_creation_contact_origine"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation" ADD CONSTRAINT "fk_etablissement_creation__e_c_secteur_activite" FOREIGN KEY ("secteur_activite_id") REFERENCES "etablissement_creation_secteur_activite"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation" ADD CONSTRAINT "fk_etablissement_creation__e_c_source_financement" FOREIGN KEY ("source_financement_id") REFERENCES "etablissement_creation_source_financement"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation__createur" ADD CONSTRAINT "fk_etablissement_creation__createur__diplome_niveau" FOREIGN KEY ("diplome_niveau_type_id") REFERENCES "etablissement_creation__createur_diplome_niveau_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation__createur" ADD CONSTRAINT "fk_etablissement_creation__createur__profession_situation" FOREIGN KEY ("profession_situation_type_id") REFERENCES "etablissement_creation__createur_profession_situation_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
ALTER TABLE "etablissement_creation" DROP COLUMN IF EXISTS "commune_id";--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation__commune" ADD CONSTRAINT "fk_etablissement_creation__commune__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation__commune" ADD CONSTRAINT "fk_etablissement_creation__commune__etablissement_creation" FOREIGN KEY ("etablissement_creation_id") REFERENCES "etablissement_creation"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation__commune" ADD CONSTRAINT "fk_etablissement_creation__commune__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation_ressource" ADD CONSTRAINT "fk_etablissement_creation_ressource__ressource_type" FOREIGN KEY ("ressource_type_id") REFERENCES "ressource_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etablissement_creation_createur" ADD CONSTRAINT "fk_action_etablissement_creation_createur__equipe" FOREIGN KEY ("etablissement_creation_id") REFERENCES "etablissement_creation"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etablissement_creation_createur" ADD CONSTRAINT "fk_action_etablissement_creation_createur__etab_crea" FOREIGN KEY ("etablissement_creation_id") REFERENCES "etablissement_creation"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etablissement_creation_createur" ADD CONSTRAINT "fk_action_etablissement_creation_createur__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etablissement_creation_createur" ADD CONSTRAINT "fk_action_etablissement_creation_createur__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
