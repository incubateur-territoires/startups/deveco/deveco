CREATE TABLE IF NOT EXISTS "equipe_local_contribution_bail_type" (
	"id" varchar NOT NULL,
	"nom" varchar NOT NULL,
	CONSTRAINT "pk_equipe_local_contribution_bail_type" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe_local_contribution_vacance_type" (
	"id" varchar NOT NULL,
	"nom" varchar NOT NULL,
	CONSTRAINT "pk_equipe_local_contribution_vacance_type" PRIMARY KEY("id")
);
--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" RENAME COLUMN "loyer" TO "loyer_euros";--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" DROP CONSTRAINT "fk_equipe__local_contribution__statut_type";
--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" ALTER COLUMN "loyer_euros" SET DATA TYPE integer USING loyer_euros::integer;--> statement-breakpoint
ALTER TABLE "equipe__local" ADD COLUMN "contribution_occupe" boolean;--> statement-breakpoint
ALTER TABLE "equipe__local" ADD COLUMN "contribution_remembre" boolean DEFAULT false;--> statement-breakpoint
ALTER TABLE "equipe__local" ADD COLUMN "contribution_loyer_euros" integer;--> statement-breakpoint
ALTER TABLE "equipe__local" ADD COLUMN "contribution_fond_euros" integer;--> statement-breakpoint
ALTER TABLE "equipe__local" ADD COLUMN "contribution_vente_euros" integer;--> statement-breakpoint
ALTER TABLE "equipe__local" ADD COLUMN "contribution_bail_type_id" varchar;--> statement-breakpoint
ALTER TABLE "equipe__local" ADD COLUMN "contribution_vacance_type_id" varchar;--> statement-breakpoint
ALTER TABLE "equipe__local" ADD COLUMN "contribution_vacance_motif_type_id" varchar;--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" ADD COLUMN "occupe" boolean;--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" ADD COLUMN "remembre" boolean DEFAULT false;--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" ADD COLUMN "fond_euros" integer;--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" ADD COLUMN "vente_euros" integer;--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" ADD COLUMN "equipe_local_contribution_bail_type_id" varchar;--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" ADD COLUMN "equipe_local_contribution_vacance_type_id" varchar;--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" ADD COLUMN "equipe_local_contribution_vacance_motif_type_id" varchar;--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local_contribution" ADD CONSTRAINT "fk_equipe__local_contribution__bail_type" FOREIGN KEY ("equipe_local_contribution_bail_type_id") REFERENCES "equipe_local_contribution_bail_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local_contribution" ADD CONSTRAINT "fk_equipe__local_contribution__vacance_type" FOREIGN KEY ("equipe_local_contribution_vacance_type_id") REFERENCES "equipe_local_contribution_vacance_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local_contribution" ADD CONSTRAINT "fk_equipe__local_contribution__vacance_motif_type" FOREIGN KEY ("equipe_local_contribution_vacance_type_id") REFERENCES "equipe_local_contribution_vacance_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
ALTER TABLE "equipe__local" DROP COLUMN IF EXISTS "equipe_local_contribution_statut_type_id";--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" DROP COLUMN IF EXISTS "equipe_local_contribution_statut_type_id";

DROP TABLE "equipe_local_contribution_statut_type";--> statement-breakpoint
