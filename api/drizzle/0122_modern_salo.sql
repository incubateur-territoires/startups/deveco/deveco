DROP TABLE IF EXISTS "commune_etablissement_stats";--> statement-breakpoint

DROP MATERIALIZED VIEW "public"."etablissement_geolocalisation_vue";--> statement-breakpoint
DROP VIEW IF EXISTS "public"."etablissement_adresse_vue";

ALTER TABLE "source"."etalab__etablissement_geoloc" RENAME TO "etalab__etablissement_adresse";--> statement-breakpoint
ALTER TABLE "source"."etalab__etablissement_adresse" RENAME COLUMN "geo_score" TO "score";--> statement-breakpoint
ALTER TABLE "source"."etalab__etablissement_adresse" DROP CONSTRAINT IF EXISTS "fk_etalab__etablissement_geoloc__commune";--> statement-breakpoint
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "source"."etalab__etablissement_adresse" ADD CONSTRAINT "fk_etalab__etablissement_adresse__commune" FOREIGN KEY ("commune_id") REFERENCES "public"."commune"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_etalab__etablissement_adresse__commune" ON "source"."etalab__etablissement_adresse" USING btree ("commune_id");--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "zonage" ADD CONSTRAINT "fk_zonage__etiquette" FOREIGN KEY ("etiquette_id") REFERENCES "public"."etiquette"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
ALTER TABLE "source"."etalab__etablissement_adresse" DROP COLUMN IF EXISTS "longitude";--> statement-breakpoint
ALTER TABLE "source"."etalab__etablissement_adresse" DROP COLUMN IF EXISTS "latitude";--> statement-breakpoint
ALTER TABLE "source"."etalab__etablissement_adresse" DROP COLUMN IF EXISTS "geo_adresse";--> statement-breakpoint
ALTER TABLE "source"."etalab__etablissement_adresse" DROP COLUMN IF EXISTS "cedex_code";--> statement-breakpoint
ALTER TABLE "source"."etalab__etablissement_adresse" DROP COLUMN IF EXISTS "cedex_nom";--> statement-breakpoint
ALTER TABLE "source"."etalab__etablissement_adresse" DROP COLUMN IF EXISTS "distribution_speciale";--> statement-breakpoint
ALTER TABLE "source"."etalab__etablissement_adresse" DROP COLUMN IF EXISTS "etranger_commune";--> statement-breakpoint
ALTER TABLE "source"."etalab__etablissement_adresse" DROP COLUMN IF EXISTS "etranger_pays";--> statement-breakpoint

CREATE MATERIALIZED VIEW "public"."etablissement_geolocalisation_vue" AS (
	SELECT
		"insee_sirene_api__etablissement"."siret" AS etabgeovue_siret,
		COALESCE(
			ST_SetSRID(ST_MakePoint("insee_sirene__etablissement_geoloc"."x_longitude", "insee_sirene__etablissement_geoloc"."y_latitude"), 4326),
			"etalab__etablissement_adresse"."geolocalisation"
		) AS etabgeovue_geolocalisation,
		"insee_sirene_api__etablissement"."mise_a_jour_at" AS etabgeovue_mise_a_jour_at
	FROM "source"."insee_sirene_api__etablissement"
	LEFT JOIN "source"."insee_sirene__etablissement_geoloc" ON "insee_sirene__etablissement_geoloc"."siret" = "insee_sirene_api__etablissement"."siret"
	LEFT JOIN "source"."etalab__etablissement_adresse" ON "etalab__etablissement_adresse"."siret" = "insee_sirene_api__etablissement"."siret"
	WHERE "insee_sirene__etablissement_geoloc"."siret" IS NOT NULL
		OR "etalab__etablissement_adresse"."siret" IS NOT NULL
);--> statement-breakpoint
CREATE UNIQUE INDEX idx_etablissement_geolocalisation_vue_etablissement ON etablissement_geolocalisation_vue (etabgeovue_siret);--> statement-breakpoint
CREATE INDEX idx_etablissement_geolocalisation_vue_geolocalisation ON etablissement_geolocalisation_vue USING gist (etabgeovue_geolocalisation);--> statement-breakpoint

CREATE OR REPLACE VIEW etablissement_adresse_vue AS (
	SELECT
		E.siret,
		E.code_commune_etablissement AS commune_id,
		E.mise_a_jour_at,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.score > 0.5
			THEN EEG.numero
			ELSE E.numero_voie_etablissement || COALESCE(E.indice_repetition_etablissement, '')
		END AS numero,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.score > 0.5
			THEN EEG.voie_nom
			ELSE COALESCE(E.type_voie_etablissement || ' ', '') || E.libelle_voie_etablissement
		END AS voie_nom,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.score > 0.5
			THEN EEG.code_postal
			ELSE E.code_postal_etablissement
		END AS code_postal,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.score > 0.5
			THEN null
			ELSE E.code_cedex_etablissement
		END AS cedex_code,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.score > 0.5
			THEN null
			ELSE E.libelle_cedex_etablissement
		END AS cedex_nom,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.score > 0.5
			THEN null
			ELSE E.distribution_speciale_etablissement
		END AS distribution_speciale,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.score > 0.5
			THEN null
			ELSE E.libelle_commune_etranger_etablissement
		END AS etranger_commune,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.score > 0.5
			THEN null
			ELSE E.libelle_pays_etranger_etablissement
		END AS etranger_pays,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.score > 0.5
			THEN EEG.geolocalisation
			ELSE
				CASE WHEN ESG.siret IS NOT NULL
					THEN ST_SetSrid(ST_MakePoint(ESG.x_longitude, ESG.y_latitude), 4326)
					ELSE NULL
				END
		END AS geolocalisation
	FROM source.insee_sirene_api__etablissement E
	LEFT JOIN source.insee_sirene__etablissement_geoloc ESG USING (siret)
	LEFT JOIN source.etalab__etablissement_adresse EEG USING (siret)
);--> statement-breakpoint


TRUNCATE "source"."insee_sirene__etablissement_geoloc";--> statement-breakpoint
\copy source.insee_sirene__etablissement_geoloc(siret,x,y,qualite_xy,epsg,plg_qp15,plg_iris,plg_zus,plg_qva,plg_code_commune,distance_precision,qualite_qp15,qualite_iris,qualite_zus,qualite_qva,y_latitude,x_longitude,plg_qp24,qualite_qp24) from './_sources/insee_sirene__etablissement_geoloc.csv' CSV HEADER;--> statement-breakpoint
TRUNCATE "source"."etalab__etablissement_adresse";--> statement-breakpoint
\copy source.etalab__etablissement_adresse(siret,commune_id,code_postal,numero,voie_nom,geolocalisation,score,mise_a_jour_at) from './_sources/etalab__etablissement_adresse.csv' CSV HEADER;--> statement-breakpoint

