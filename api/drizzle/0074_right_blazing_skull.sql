CREATE TABLE IF NOT EXISTS "commune__qpv_2024" (
	"commune_id" varchar(5) NOT NULL,
	"qpv_2024_id" varchar(8) NOT NULL,
	CONSTRAINT "pk_commune__qpv_2024" PRIMARY KEY("commune_id","qpv_2024_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "qpv_2024" (
	"id" varchar(8) NOT NULL,
	"geometrie" geometry(MultiPolygon,4326) NOT NULL,
	"nom" varchar NOT NULL,
	CONSTRAINT "pk_qpv_2024" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement__qpv_2015" (
	"etablissement_id" varchar(14) NOT NULL,
	"qpv_2015_id" varchar(8) NOT NULL,
	CONSTRAINT "uk_etablissement__qpv_2015" UNIQUE("etablissement_id","qpv_2015_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement__qpv_2024" (
	"etablissement_id" varchar(14) NOT NULL,
	"qpv_2024_id" varchar(8) NOT NULL,
	CONSTRAINT "uk_etablissement__qpv_2024" UNIQUE("etablissement_id","qpv_2024_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."anct__commune__qpv_2015" (
	"commune_id" varchar(5) NOT NULL,
	"qpv_2015_id" varchar(8),
	CONSTRAINT "pk_anct__commune__qpv_2015" PRIMARY KEY("commune_id","qpv_2015_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."anct__commune__qpv_2024" (
	"commune_id" varchar(5) NOT NULL,
	"qpv_2024_id" varchar(8),
	CONSTRAINT "pk_anct__commune__qpv_2024" PRIMARY KEY("commune_id","qpv_2024_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."anct__qpv_2024" (
	"gid" varchar NOT NULL,
	"fid" serial NOT NULL,
	"code_qp" varchar(8),
	"lib_qp" varchar,
	"commune" varchar,
	"geom" geometry(MultiPolygon, 4326),
	CONSTRAINT "pk_anct__qpv_2024" PRIMARY KEY("gid")
);
--> statement-breakpoint
ALTER TABLE "commune__qpv" RENAME TO "commune__qpv_2015";--> statement-breakpoint
ALTER TABLE "qpv" RENAME TO "qpv_2015";--> statement-breakpoint
ALTER TABLE "source"."anct__qpv" RENAME TO "anct__qpv_2015";--> statement-breakpoint
ALTER TABLE "commune__qpv_2015" RENAME COLUMN "qpv_id" TO "qpv_2015_id";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP CONSTRAINT "fk_equipe__etablissement__qpv";
--> statement-breakpoint
ALTER TABLE "commune__qpv_2015" DROP CONSTRAINT "fk_commune__qpv__commune";
--> statement-breakpoint
ALTER TABLE "commune__qpv_2015" DROP CONSTRAINT "fk_commune__qpv__qpv";
--> statement-breakpoint
DROP INDEX IF EXISTS "idx_equipe__etablissement__qpv";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_qpv__geometrie__gist";--> statement-breakpoint
ALTER TABLE "commune__qpv_2015" DROP CONSTRAINT "pk_commune__qpv";--> statement-breakpoint
ALTER TABLE "qpv_2015" DROP CONSTRAINT "pk_qpv";--> statement-breakpoint
ALTER TABLE "source"."anct__qpv_2015" DROP CONSTRAINT "pk_anct__qpv";--> statement-breakpoint
ALTER TABLE "source"."anct__qpv_2015" ALTER COLUMN "gid" SET DATA TYPE varchar;--> statement-breakpoint
ALTER TABLE "commune__qpv_2015" ADD CONSTRAINT "pk_commune__qpv_2015" PRIMARY KEY("commune_id","qpv_2015_id");--> statement-breakpoint
ALTER TABLE "qpv_2015" ADD CONSTRAINT "pk_qpv_2015" PRIMARY KEY("id");--> statement-breakpoint
ALTER TABLE "source"."anct__qpv_2015" ADD CONSTRAINT "pk_anct__qpv_2015" PRIMARY KEY("gid");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_qpv_2024__geometrie__gist" ON "qpv_2024" using gist ("geometrie");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_qpv_2015__geometrie__gist" ON "qpv_2015" using gist ("geometrie");--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune__qpv_2015" ADD CONSTRAINT "fk_commune__qpv_2015__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune__qpv_2015" ADD CONSTRAINT "fk_commune__qpv_2015__qpv_2015" FOREIGN KEY ("qpv_2015_id") REFERENCES "qpv_2015"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "qpv_id";--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune__qpv_2024" ADD CONSTRAINT "fk_commune__qpv_2024__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune__qpv_2024" ADD CONSTRAINT "fk_commune__qpv_2024__qpv_2024" FOREIGN KEY ("qpv_2024_id") REFERENCES "qpv_2024"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement__qpv_2015" ADD CONSTRAINT "fk_etablissement__qpv_2015__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement__qpv_2015" ADD CONSTRAINT "fk_qpv__qpv_2015__qpv" FOREIGN KEY ("qpv_2015_id") REFERENCES "qpv_2015"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement__qpv_2024" ADD CONSTRAINT "fk_etablissement__qpv_2024__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement__qpv_2024" ADD CONSTRAINT "fk_qpv__qpv_2024__qpv" FOREIGN KEY ("qpv_2024_id") REFERENCES "qpv_2024"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint

\copy source.anct__qpv_2015 from '_sources/anct_qpv_2015.csv' delimiter ',' csv header;
--> statement-breakpoint

\copy source.anct__qpv_2024 from '_sources/anct_qpv_2024.csv' delimiter ',' csv header;
--> statement-breakpoint

SELECT UpdateGeometrySRID('source', 'anct__qpv_2024', 'geom', 2154);
ALTER TABLE source.anct__qpv_2024 ALTER COLUMN geom TYPE geometry(MultiPolygon, 4326) USING ST_Transform(ST_SetSRID(geom,2154),4326);
--> statement-breakpoint

\copy source.anct__commune__qpv_2015 from '_sources/anct_commune__qpv_2015.csv' delimiter ',' csv header;
--> statement-breakpoint

\copy source.anct__commune__qpv_2024 from '_sources/anct_commune__qpv_2024.csv' delimiter ',' csv header;
--> statement-breakpoint


INSERT INTO qpv_2015 (id, nom, geometrie)
SELECT code_qp, nom_qp, geom
FROM source.anct__qpv_2015
ON CONFLICT (id) DO NOTHING;
--> statement-breakpoint

INSERT INTO qpv_2024 (id, nom, geometrie)
SELECT code_qp, nom_qp, geom
FROM source.anct__qpv_2015
WHERE code_qp like 'QP97%';
--> statement-breakpoint
INSERT INTO qpv_2024 (id, nom, geometrie)
SELECT code_qp, lib_qp, geom
FROM source.anct__qpv_2024;
--> statement-breakpoint

INSERT INTO commune__qpv_2015 (commune_id, qpv_2015_id)
SELECT commune_id, qpv_2015_id
FROM source.anct__commune__qpv_2015
ON CONFLICT DO NOTHING;
--> statement-breakpoint

INSERT INTO commune__qpv_2024 (commune_id, qpv_2024_id)
SELECT commune_id, qpv_2015_id
FROM source.anct__commune__qpv_2015
WHERE qpv_2015_id like 'QP97%';
--> statement-breakpoint
INSERT INTO commune__qpv_2024 (commune_id, qpv_2024_id)
SELECT commune_id, qpv_2024_id
FROM source.anct__commune__qpv_2024;
--> statement-breakpoint

INSERT INTO etablissement__qpv_2015 (etablissement_id, qpv_2015_id)
SELECT a.etablissement_id, q.id as qpv_2015_id
FROM etablissement_adresse_1 a
JOIN commune__qpv_2015 cq using (commune_id)
JOIN qpv_2015 q on cq.qpv_2015_id = q.id
LEFT JOIN etablissement_adresse_1_etalab ae using (etablissement_id)
WHERE
    (
        ae.geolocalisation IS NOT NULL
        OR a.geolocalisation IS NOT NULL
    )
    AND ST_Covers(
        q.geometrie,
        CASE WHEN ae.score > 0.5
            THEN ae.geolocalisation
            ELSE a.geolocalisation
        END
    )
;
--> statement-breakpoint

INSERT INTO etablissement__qpv_2024 (etablissement_id, qpv_2024_id)
SELECT a.etablissement_id, q.id as qpv_2015_id
FROM etablissement_adresse_1 a
JOIN commune__qpv_2015 cq using (commune_id)
JOIN qpv_2015 q on cq.qpv_2015_id = q.id
LEFT JOIN etablissement_adresse_1_etalab ae using (etablissement_id)
WHERE
    (
        ae.geolocalisation IS NOT NULL
        OR a.geolocalisation IS NOT NULL
    )
    AND ST_Covers(
        q.geometrie,
        CASE WHEN ae.score > 0.5
            THEN ae.geolocalisation
            ELSE a.geolocalisation
        END
    )
	AND q.id like 'QP97%'
;
--> statement-breakpoint
INSERT INTO etablissement__qpv_2024 (etablissement_id, qpv_2024_id)
SELECT a.etablissement_id, q.id as qpv_2024_id
FROM etablissement_adresse_1 a
JOIN commune__qpv_2024 cq using (commune_id)
JOIN qpv_2024 q on cq.qpv_2024_id = q.id
LEFT JOIN etablissement_adresse_1_etalab ae using (etablissement_id)
WHERE
    (
        ae.geolocalisation IS NOT NULL
        OR a.geolocalisation IS NOT NULL
    )
    AND ST_Covers(
        q.geometrie,
        CASE WHEN ae.score > 0.5
            THEN ae.geolocalisation
            ELSE a.geolocalisation
        END
    )
ON CONFLICT DO NOTHING
;
--> statement-breakpoint
