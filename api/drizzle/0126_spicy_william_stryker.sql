ALTER TABLE "equipe__etablissement__qpv_2015" DROP CONSTRAINT "fk_equipe__qpv2015__qpv_2015__qpv2015";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement__qpv_2024" DROP CONSTRAINT "fk_equipe__qpv2024__qpv_2024__qpv2024";
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__qpv_2015" ADD CONSTRAINT "fk_equipe__etablissement__qpv_2015__qpv2015" FOREIGN KEY ("id") REFERENCES "public"."qpv_2015"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__qpv_2024" ADD CONSTRAINT "fk_equipe__etablissement__qpv_2024__qpv2024" FOREIGN KEY ("id") REFERENCES "public"."qpv_2024"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
