CREATE VIEW public.entreprise_periode_fermeture_vue AS (
  SELECT
    entreprise_id,
    debut_date AS fermeture_date
  FROM
    (
      SELECT
        DISTINCT ON (siren)
        siren as entreprise_id,
        date_debut as debut_date,
        etat_administratif_unite_legale = 'A' as statut
      FROM
        source.insee_sirene_api__unite_legale_periode
      WHERE
        changement_etat_administratif_unite_legale
      ORDER BY
        siren,
        date_debut DESC
    ) t
  WHERE
    (NOT statut)
);
