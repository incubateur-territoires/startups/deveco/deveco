CREATE TABLE IF NOT EXISTS "source"."insee_sirene_api__unite_legale_doublon" (
	"siren" varchar(9) NOT NULL,
	"siren_doublon" varchar(9) NOT NULL,
	"date_dernier_traitement_doublon" timestamp with time zone,
	"mise_a_jour_at" timestamp with time zone,
	CONSTRAINT "pk_insee_sirene_api__unite_legale_doublon" PRIMARY KEY("siren","siren_doublon")
);
--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_insee_sirene_api__unite_legale_doublon__mise_a_jour_at" ON "source"."insee_sirene_api__unite_legale_doublon" ("mise_a_jour_at");