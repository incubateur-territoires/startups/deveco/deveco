CREATE TABLE IF NOT EXISTS "recherche_enregistree" (
	"id" serial NOT NULL,
	"nom" varchar NOT NULL,
	"url" varchar NOT NULL,
	"deveco_id" integer NOT NULL,
	"created_at" timestamp with time zone DEFAULT now() NOT NULL,
	CONSTRAINT "pk_recherche_enregistree" PRIMARY KEY("id")
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "recherche_enregistree" ADD CONSTRAINT "fk_recherche_enregistree__deveco" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
