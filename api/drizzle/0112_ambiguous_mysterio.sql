ALTER TABLE "equipe" ADD COLUMN "created_at" timestamp with time zone DEFAULT now() NOT NULL;--> statement-breakpoint

UPDATE equipe
SET created_at = equipe_import.created_at
FROM equipe_import
WHERE equipe.id = equipe_import.equipe_id;
--> statement-breakpoint

DROP TABLE "equipe_import";--> statement-breakpoint