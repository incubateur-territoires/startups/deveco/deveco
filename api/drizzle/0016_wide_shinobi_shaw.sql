CREATE TABLE IF NOT EXISTS "source"."source_bodacc_procedure_collective" (
	"siren" varchar(9),
	"famille" varchar,
	"nature" varchar,
	"date" date
);
--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_source_bodacc_procedure_collective__entreprise" ON "source"."source_bodacc_procedure_collective" ("siren");