ALTER TABLE "demande" ALTER COLUMN "suppression_date" SET DATA TYPE timestamp with time zone;--> statement-breakpoint
ALTER TABLE "echange" ALTER COLUMN "suppression_date" SET DATA TYPE timestamp with time zone;--> statement-breakpoint
ALTER TABLE "rappel" ALTER COLUMN "suppression_date" SET DATA TYPE timestamp with time zone;--> statement-breakpoint
ALTER TABLE "contact" ADD COLUMN "creation_date" timestamp with time zone DEFAULT now() NOT NULL;--> statement-breakpoint
ALTER TABLE "contact" ADD COLUMN "modification_date" timestamp with time zone;--> statement-breakpoint
ALTER TABLE "contact" ADD COLUMN "modification_compte_id" integer;--> statement-breakpoint
ALTER TABLE "contact" ADD COLUMN "suppression_date" timestamp with time zone;--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "contact" ADD CONSTRAINT "fk_contact__modification_compte" FOREIGN KEY ("modification_compte_id") REFERENCES "compte"("id") ON DELETE set null ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
