ALTER TABLE "action_contact_personne" RENAME TO "action_contact";--> statement-breakpoint

-- problématique en prod
-- ALTER TABLE "etablissement" ADD CONSTRAINT "fk_etablissement__entreprise" FOREIGN KEY (entreprise_id) REFERENCES entreprise (id);

ALTER TABLE "etablissement" ADD CONSTRAINT "fk_etablissement__effectif_type" FOREIGN KEY (effectif_type_id) REFERENCES effectif_type (id);--> statement-breakpoint

ALTER TABLE "echange__demande" RENAME CONSTRAINT "echange__demande_pkey" TO "pk_echange__demande";--> statement-breakpoint
ALTER TABLE "equipe__commune" RENAME CONSTRAINT "equipe__commune_pkey" TO "pk_equipe__commune";--> statement-breakpoint
ALTER TABLE "equipe__departement" RENAME CONSTRAINT "equipe__departement_pkey" TO "pk_equipe__departement";--> statement-breakpoint
ALTER TABLE "equipe__epci" RENAME CONSTRAINT "equipe__epci_pkey" TO "pk_equipe__epci";--> statement-breakpoint
ALTER TABLE "equipe__metropole" RENAME CONSTRAINT "equipe__metropole_pkey" TO "pk_equipe__metropole";--> statement-breakpoint
ALTER TABLE "equipe__petr" RENAME CONSTRAINT "equipe__petr_pkey" TO "pk_equipe__petr";--> statement-breakpoint
ALTER TABLE "equipe__region" RENAME CONSTRAINT "equipe__region_pkey" TO "pk_equipe__region";--> statement-breakpoint
ALTER TABLE "etablissement_creation__demande" RENAME CONSTRAINT "etablissement_creation__demande_pkey" TO "pk_etablissement_creation__demande" ;--> statement-breakpoint
ALTER TABLE "etablissement_creation__echange" RENAME CONSTRAINT "etablissement_creation__echange_pkey" TO "pk_etablissement_creation__echange";--> statement-breakpoint
ALTER TABLE "etablissement_creation__etiquette" RENAME CONSTRAINT "etablissement_creation__etiquette_pkey" TO "pk_etablissement_creation__etiquette";--> statement-breakpoint
ALTER TABLE "etablissement_creation__rappel" RENAME CONSTRAINT "etablissement_creation__rappel_pkey" TO "pk_etablissement_creation__rappel";--> statement-breakpoint
ALTER TABLE "etablissement_creation" RENAME CONSTRAINT "etablissement_creation_pkey" TO "pk_etablissement_creation";--> statement-breakpoint
ALTER TABLE "etablissement_creation_transformation" RENAME CONSTRAINT "etablissement_creation_transformation_pkey" TO "pk_etablissement_creation_transformation";--> statement-breakpoint

ALTER TABLE "categorie_juridique_type" RENAME CONSTRAINT "categorie_juridique_type_pkey" TO "pk_categorie_juridique_type";--> statement-breakpoint
ALTER TABLE "effectif_type" RENAME CONSTRAINT "effectif_type_pkey" TO "pk_effectif_type";--> statement-breakpoint
ALTER TABLE "entreprise" RENAME CONSTRAINT "entreprise_pkey" TO "pk_entreprise";--> statement-breakpoint
ALTER TABLE "entreprise_categorie_type" RENAME CONSTRAINT "categorie_entreprise_type_pkey" TO "pk_entreprise_categorie_type";--> statement-breakpoint
ALTER TABLE "entreprise_ess" RENAME CONSTRAINT "entreprise_ess_pkey" TO "pk_entreprise_ess";--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_entreprise_ratio_financier__entreprise" ON "entreprise_ratio_financier"("entreprise_id");
ALTER TABLE "entreprise_type" RENAME CONSTRAINT "entreprise_type_pkey" TO "pk_entreprise_type";--> statement-breakpoint
ALTER TABLE "etablissement" RENAME CONSTRAINT "etablissement_pkey" TO "pk_etablissement";--> statement-breakpoint
ALTER TABLE "mandataire_personne_morale" RENAME CONSTRAINT "mandataire_personne_morale_pkey" TO "pk_mandataire_personne_morale";--> statement-breakpoint
ALTER TABLE "mandataire_personne_physique" RENAME CONSTRAINT "mandataire_personne_physique_pkey" TO "pk_mandataire_personne_physique";--> statement-breakpoint
ALTER TABLE "naf_revision_type" RENAME CONSTRAINT "naf_revision_type_pkey" TO "pk_naf_revision_type";--> statement-breakpoint
ALTER TABLE "naf_type" RENAME CONSTRAINT "naf_type_pkey" TO "pk_naf_type";--> statement-breakpoint
ALTER TABLE "personne_morale" RENAME CONSTRAINT "personne_morale_pkey" TO "pk_personne_morale";--> statement-breakpoint
ALTER TABLE "personne_physique" RENAME CONSTRAINT "personne_physique_pkey" TO "pk_personne_physique";--> statement-breakpoint
ALTER TABLE "sexe_type" RENAME CONSTRAINT "sexe_type_pkey" TO "pk_sexe_type";--> statement-breakpoint

ALTER TABLE "etablissement_creation__demande" ADD CONSTRAINT "fk_etablissement_creation__demande__equipe" FOREIGN KEY(equipe_id) REFERENCES equipe(id); --> statement-breakpoint
ALTER TABLE "etablissement_creation__echange" ADD CONSTRAINT "fk_etablissement_creation__echange__equipe" FOREIGN KEY(equipe_id) REFERENCES equipe(id); --> statement-breakpoint
ALTER TABLE "etablissement_creation__etiquette" ADD CONSTRAINT "fk_etablissement_creation__etiquette__equipe" FOREIGN KEY(equipe_id) REFERENCES equipe(id); --> statement-breakpoint
ALTER TABLE "etablissement_creation__rappel" ADD CONSTRAINT "fk_etablissement_creation__rappel__equipe" FOREIGN KEY(equipe_id) REFERENCES equipe(id); --> statement-breakpoint

ALTER TABLE "entreprise" RENAME CONSTRAINT "fk_entreprise__categorie_entreprise_type" TO "fk_entreprise__entreprise_categorie_type"; --> statement-breakpoint

ALTER TABLE "action_contact" RENAME CONSTRAINT "fk_action_contact_evenement" TO "fk_action_contact__evenement"; --> statement-breakpoint
ALTER TABLE "entreprise_exercice" RENAME CONSTRAINT "entreprise_exercice_entreprise_id_fkey" TO "fk_entreprise_exercice__entreprise"; --> statement-breakpoint
create unique index if not exists uk_equipe__commune_vue on equipe__commune_vue (equipe_id , commune_id);--> statement-breakpoint
ALTER INDEX "idx_entreprise_periode__courante_fin_date" RENAME TO "idx_entreprise_periode__fin_date_is_null";--> statement-breakpoint
ALTER INDEX "idx_entreprise_periode__fin_date_partial" RENAME TO "idx_entreprise_periode__entreprise_fin_date_is_null";--> statement-breakpoint
ALTER INDEX "idx_etablissement_periode__courante_fin_date" RENAME TO "idx_etablissement_periode__fin_date_is_null";--> statement-breakpoint
