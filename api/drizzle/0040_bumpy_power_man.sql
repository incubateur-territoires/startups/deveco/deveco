ALTER TABLE "equipe_local__contact" RENAME TO "equipe__local__contact";--> statement-breakpoint
ALTER TABLE "equipe_local_contribution" RENAME TO "equipe__local_contribution";--> statement-breakpoint
ALTER TABLE "equipe_local_contribution__equipe_local_contribution_type" RENAME TO "equipe__local_contribution__equipe_local_contribution_type";--> statement-breakpoint
ALTER TABLE "equipe_local__etiquette" RENAME TO "equipe__local__etiquette";--> statement-breakpoint

ALTER TABLE "equipe" DROP CONSTRAINT "fk_equipe__territoire_categorie";--> statement-breakpoint
ALTER TABLE "equipe__local_contribution__equipe_local_contribution_type" DROP CONSTRAINT "fk_local__local_type__local_type";--> statement-breakpoint
ALTER TABLE "equipe__local__etiquette" DROP CONSTRAINT "fk_local__etiquette__etiquette";--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" DROP CONSTRAINT "fk_local__equipe";--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" DROP CONSTRAINT "fk_local__local_statut_type";--> statement-breakpoint

ALTER TABLE "equipe" DROP COLUMN "territoire_categorie_id";--> statement-breakpoint
DROP TABLE territoire_categorie;--> statement-breakpoint


ALTER TABLE "equipe__local__contact" RENAME CONSTRAINT "fk_equipe_local__contact__local" TO "fk_equipe__local__contact__local";--> statement-breakpoint
ALTER TABLE "equipe__local__contact" RENAME CONSTRAINT "fk_equipe_local__contact__equipe" TO "fk_equipe__local__contact__equipe";--> statement-breakpoint
ALTER TABLE "equipe__local__contact" RENAME CONSTRAINT "fk_equipe_local__contact__equipe_local" TO "fk_equipe__local__contact__equipe_local";--> statement-breakpoint
ALTER TABLE "equipe__local__contact" RENAME CONSTRAINT "fk_equipe_local__contact__contact" TO "fk_equipe__local__contact__contact";--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" RENAME CONSTRAINT "fk_equipe_local_contribution__equipe_local" TO "fk_equipe__local_contribution__equipe_local";--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" RENAME CONSTRAINT "fk_equipe_local_contribution__statut_type" TO "fk_equipe__local_contribution__statut_type";--> statement-breakpoint
ALTER TABLE "equipe__local__etiquette" RENAME CONSTRAINT "fk_equipe_local__etiquette__equipe_local" TO "fk_equipe__local__etiquette__equipe_local";--> statement-breakpoint
ALTER TABLE "equipe__local__etiquette" RENAME CONSTRAINT "fk_equipe_local__etiquette__local" TO "fk_equipe__local__etiquette__local";--> statement-breakpoint
ALTER TABLE "equipe__local__etiquette" RENAME CONSTRAINT "fk_equipe_local__etiquette__equipe" TO "fk_equipe__local__etiquette__equipe";--> statement-breakpoint
ALTER TABLE "equipe__local__etiquette" RENAME CONSTRAINT "fk_equipe_local__etiquette__etiquette" TO "fk_equipe__local__etiquette__etiquette";--> statement-breakpoint

ALTER TABLE "equipe_local_contribution_statut_type" RENAME CONSTRAINT "local_statut_type_pkey" TO "pk_equipe_local_contribution_statut_type";--> statement-breakpoint
ALTER TABLE "equipe_local_contribution_type" RENAME CONSTRAINT "local_type_pkey" TO "pk_equipe_local_contribution_type";--> statement-breakpoint
ALTER TABLE "demande" RENAME CONSTRAINT "demande_pkey" TO "pk_demande";--> statement-breakpoint
ALTER TABLE "demande_type" RENAME CONSTRAINT "demande_type_pkey" TO "pk_demande_type";--> statement-breakpoint
ALTER TABLE "deveco" RENAME CONSTRAINT "deveco_pkey" TO "pk_deveco";--> statement-breakpoint
ALTER TABLE "echange" RENAME CONSTRAINT "echange_pkey" TO "pk_echange";--> statement-breakpoint
ALTER TABLE "echange_type" RENAME CONSTRAINT "echange_type_pkey" TO "pk_echange_type";--> statement-breakpoint
ALTER TABLE "equipe" RENAME CONSTRAINT "equipe_pkey" TO "pk_equipe";--> statement-breakpoint
ALTER TABLE "equipe_type" RENAME CONSTRAINT "equipe_type_pkey" TO "pk_equipe_type";--> statement-breakpoint
ALTER TABLE "etiquette" RENAME CONSTRAINT "etiquette_pkey" TO "pk_etiquette";--> statement-breakpoint
ALTER TABLE "etiquette_type" RENAME CONSTRAINT "etiquette_type_pkey" TO "pk_etiquette_type";--> statement-breakpoint
ALTER TABLE "geo_token" RENAME CONSTRAINT "PK_a22102ccd6036b34c92c675e7f9" TO "pk_geo_token";--> statement-breakpoint
ALTER TABLE "geo_token_request" RENAME CONSTRAINT "PK_d5fe856933f6def106e8af74b5b" TO "pk_geo_token_request";--> statement-breakpoint
ALTER TABLE "rappel" RENAME CONSTRAINT "rappel_pkey" TO "pk_rappel";--> statement-breakpoint
ALTER TABLE "recherche" RENAME CONSTRAINT "PK_d01dd084a9e155f976ca2e55d47" TO "pk_recherche";--> statement-breakpoint
ALTER TABLE "territoire_type" RENAME CONSTRAINT "territoire_type_pkey" TO "pk_territoire_type";--> statement-breakpoint
ALTER TABLE "zonage" RENAME CONSTRAINT "zonage_pkey" TO "pk_zonage";--> statement-breakpoint
ALTER TABLE "equipe__local__contact" RENAME CONSTRAINT "pk_equipe_local__contact" TO "pk_equipe__local__contact";--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" RENAME CONSTRAINT "pk_equipe_local_contribution" TO "pk_equipe__local_contribution";--> statement-breakpoint
ALTER TABLE "equipe__local_contribution__equipe_local_contribution_type" RENAME CONSTRAINT "pk_equipe_local_contribution__equipe_local_contribution_type" TO "pk_equipe__local_contribution__equipe_local_contribution_type";--> statement-breakpoint
ALTER TABLE "equipe__local__etiquette" RENAME CONSTRAINT "pk_equipe_local__etiquette" TO "pk_equipe__local__etiquette";--> statement-breakpoint

ALTER TABLE "geo_token_request" RENAME CONSTRAINT "FK_aeef7771b7cd26f7030c3c83bef" TO "fk_geo_token_request__geo_token";--> statement-breakpoint
ALTER TABLE "rappel" RENAME CONSTRAINT "rappel_affecte_id_fkey" TO "fk_rappel__affecte_compte";--> statement-breakpoint
ALTER TABLE "equipe_import" RENAME CONSTRAINT "uk_equipe_import_equipe" TO "uk_equipe_import__equipe";
