CREATE TABLE "equipe__etablissement__brouillon" (
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	"brouillon_id" integer NOT NULL,
	CONSTRAINT "pk_equipe__etablissement__brouillon" PRIMARY KEY("equipe_id","etablissement_id","brouillon_id")
);
--> statement-breakpoint
CREATE TABLE "equipe__local__brouillon" (
	"equipe_id" integer NOT NULL,
	"local_id" varchar(14) NOT NULL,
	"brouillon_id" integer NOT NULL,
	CONSTRAINT "pk_equipe__local__brouillon" PRIMARY KEY("equipe_id","local_id","brouillon_id")
);
--> statement-breakpoint
CREATE TABLE "etablissement_creation__brouillon" (
	"equipe_id" integer NOT NULL,
	"etablissement_creation_id" integer NOT NULL,
	"brouillon_id" integer NOT NULL,
	CONSTRAINT "pk_etablissement_creation__brouillon" PRIMARY KEY("etablissement_creation_id","brouillon_id")
);
--> statement-breakpoint
CREATE TABLE "action_brouillon" (
	"evenement_id" integer NOT NULL,
	"created_at" timestamp with time zone DEFAULT now() NOT NULL,
	"action_type_id" varchar NOT NULL,
	"diff" jsonb,
	"brouillon_id" integer NOT NULL
);
--> statement-breakpoint
CREATE TABLE "brouillon" (
	"id" serial NOT NULL,
	"date" date NOT NULL,
	"description" text,
	"equipe_id" integer NOT NULL,
	"nom" varchar NOT NULL,
	"creation_date" timestamp with time zone DEFAULT now() NOT NULL,
	"creation_compte_id" integer NOT NULL,
	"modification_date" timestamp with time zone,
	"modification_compte_id" integer,
	"suppression_date" timestamp with time zone,
	CONSTRAINT "pk_brouillon" PRIMARY KEY("id")
);
--> statement-breakpoint
ALTER TABLE "equipe__etablissement__brouillon" ADD CONSTRAINT "fk_equipe__etablissement__brouillon__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;--> statement-breakpoint
ALTER TABLE "equipe__etablissement__brouillon" ADD CONSTRAINT "fk_equipe__etablissement__brouillon__equipe" FOREIGN KEY ("equipe_id") REFERENCES "public"."equipe"("id") ON DELETE cascade ON UPDATE cascade;--> statement-breakpoint
ALTER TABLE "equipe__etablissement__brouillon" ADD CONSTRAINT "fk_equipe__etablissement__brouillon__brouillon" FOREIGN KEY ("brouillon_id") REFERENCES "public"."brouillon"("id") ON DELETE cascade ON UPDATE cascade;--> statement-breakpoint
ALTER TABLE "equipe__local__brouillon" ADD CONSTRAINT "fk_equipe__local__brouillon__local" FOREIGN KEY ("local_id") REFERENCES "public"."local"("id") ON DELETE cascade ON UPDATE cascade;--> statement-breakpoint
ALTER TABLE "equipe__local__brouillon" ADD CONSTRAINT "fk_equipe__local__brouillon__equipe" FOREIGN KEY ("equipe_id") REFERENCES "public"."equipe"("id") ON DELETE cascade ON UPDATE cascade;--> statement-breakpoint
ALTER TABLE "equipe__local__brouillon" ADD CONSTRAINT "fk_equipe__local__brouillon__brouillon" FOREIGN KEY ("brouillon_id") REFERENCES "public"."brouillon"("id") ON DELETE cascade ON UPDATE cascade;--> statement-breakpoint
ALTER TABLE "etablissement_creation__brouillon" ADD CONSTRAINT "fk_etablissement_creation__brouillon__equipe" FOREIGN KEY ("equipe_id") REFERENCES "public"."equipe"("id") ON DELETE cascade ON UPDATE cascade;--> statement-breakpoint
ALTER TABLE "etablissement_creation__brouillon" ADD CONSTRAINT "fk_etablissement_creation__brouillon__etablissement_creation" FOREIGN KEY ("etablissement_creation_id") REFERENCES "public"."etablissement_creation"("id") ON DELETE cascade ON UPDATE cascade;--> statement-breakpoint
ALTER TABLE "etablissement_creation__brouillon" ADD CONSTRAINT "fk_etablissement_creation__brouillon__brouillon" FOREIGN KEY ("brouillon_id") REFERENCES "public"."brouillon"("id") ON DELETE cascade ON UPDATE cascade;--> statement-breakpoint
ALTER TABLE "action_brouillon" ADD CONSTRAINT "fk_action_brouillon__brouillon" FOREIGN KEY ("brouillon_id") REFERENCES "public"."brouillon"("id") ON DELETE cascade ON UPDATE cascade;--> statement-breakpoint
ALTER TABLE "action_brouillon" ADD CONSTRAINT "fk_action_brouillon__evenement" FOREIGN KEY ("evenement_id") REFERENCES "public"."evenement"("id") ON DELETE cascade ON UPDATE cascade;--> statement-breakpoint
ALTER TABLE "action_brouillon" ADD CONSTRAINT "fk_action_brouillon__action_type" FOREIGN KEY ("action_type_id") REFERENCES "public"."action_type"("id") ON DELETE restrict ON UPDATE cascade;--> statement-breakpoint
ALTER TABLE "brouillon" ADD CONSTRAINT "fk_brouillon__equipe" FOREIGN KEY ("equipe_id") REFERENCES "public"."equipe"("id") ON DELETE cascade ON UPDATE cascade;--> statement-breakpoint
ALTER TABLE "brouillon" ADD CONSTRAINT "fk_brouillon__modification_compte" FOREIGN KEY ("modification_compte_id") REFERENCES "public"."compte"("id") ON DELETE set null ON UPDATE cascade;--> statement-breakpoint
CREATE INDEX "idx_action_brouillon" ON "action_brouillon" USING btree ("evenement_id","brouillon_id","action_type_id");