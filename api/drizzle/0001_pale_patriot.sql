CREATE SCHEMA "source";
--> statement-breakpoint
DO $$ BEGIN
 CREATE TYPE "source_sirene_etablissement_caractere_employeur_enum" AS ENUM('N', 'O');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 CREATE TYPE "source_sirene_etablissement_etat_administratif_enum" AS ENUM('F', 'A');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 CREATE TYPE "source_sirene_etablissement_statut_diffusion_enum" AS ENUM('P', 'N', 'O');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 CREATE TYPE "source_sirene_unite_legale_categorie_entreprise_enum" AS ENUM('GE', 'ETI', 'PME');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 CREATE TYPE "source_sirene_unite_legale_eco_soc_sol_enum" AS ENUM('N', 'O');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 CREATE TYPE "source_sirene_unite_legale_etat_administratif_enum" AS ENUM('C', 'A');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 CREATE TYPE "source_sirene_unite_legale_sexe_enum" AS ENUM('[ND]', 'M', 'F');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 CREATE TYPE "source_sirene_unite_legale_societe_mission_enum" AS ENUM('N', 'O');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 CREATE TYPE "source_sirene_unite_legale_statut_diffusion_enum" AS ENUM('P', 'N', 'O');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "compte" (
	"id" serial PRIMARY KEY NOT NULL,
	"nom" varchar,
	"prenom" varchar,
	"email" varchar NOT NULL,
	"compte_type_id" varchar NOT NULL,
	"cle" varchar,
	"actif" boolean
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "mot_de_passe" (
	"id" serial PRIMARY KEY NOT NULL,
	"hash" varchar NOT NULL,
	"updated_at" timestamp with time zone DEFAULT now() NOT NULL,
	"created_at" timestamp with time zone DEFAULT now() NOT NULL,
	"compte_id" integer NOT NULL,
	CONSTRAINT "mot_de_passe_compte_id_unique" UNIQUE("compte_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "nouveautes" (
	"id" serial PRIMARY KEY NOT NULL,
	"amount" integer NOT NULL,
	"created_at" timestamp with time zone DEFAULT now() NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "tache" (
	"id" serial PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL,
	"debut_at" timestamp with time zone DEFAULT now() NOT NULL,
	"en_cours_at" timestamp with time zone,
	"fin_at" timestamp with time zone,
	"manuel" boolean DEFAULT false,
	"rapport" jsonb,
	CONSTRAINT "uk_tache" UNIQUE("id","nom")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "personne" (
	"id" serial PRIMARY KEY NOT NULL,
	"created_at" timestamp with time zone DEFAULT now() NOT NULL,
	"email" varchar,
	"equipe_id" integer NOT NULL,
	"naissance_date" date,
	"nom" varchar NOT NULL,
	"prenom" varchar NOT NULL,
	"telephone" varchar
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "personne_adresse" (
	"id" serial PRIMARY KEY NOT NULL,
	"cedex_code" varchar(9),
	"cedex_nom" varchar,
	"code_postal" varchar(5),
	"commune_id" varchar(5) NOT NULL,
	"distribution_speciale" varchar,
	"geolocalisation" "geometry(Point, 4326)",
	"numero" varchar,
	"voie_nom" varchar,
	"personne_id" integer
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "qpv" (
	"id" varchar(8) NOT NULL,
	"geometrie" "geometry(MultiPolygon)" NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "contact" (
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	"personne_id" integer NOT NULL,
	"fonction" varchar,
	"contact_source_type_id" varchar(10) NOT NULL,
	CONSTRAINT contact_equipe_id_etablissement_id_personne_id PRIMARY KEY("equipe_id","etablissement_id","personne_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__etablissement" (
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	"actif" boolean NOT NULL,
	"categorie_entreprise_type_id" varchar(3),
	"categorie_juridique_2_type_id" varchar(2) NOT NULL,
	"categorie_juridique_3_type_id" varchar(4) NOT NULL,
	"code_postal" varchar(5),
	"commune_id" varchar(5),
	"contact_noms" varchar,
	"creation_date" date,
	"economie_sociale_solidaire" boolean NOT NULL,
	"effectif_type_id" varchar(2),
	"employeur" boolean NOT NULL,
	"entreprise_id" varchar(9) NOT NULL,
	"entreprise_type_id" varchar(1) NOT NULL,
	"etablissement_noms" varchar,
	"exogene" boolean NOT NULL,
	"fermeture_date" date,
	"geolocalisation" "geometry(Point, 4326)",
	"naf_type_id" varchar(6),
	"qpv_id" varchar(8),
	"numero" varchar,
	"voie_nom" varchar,
	"zrr_type_id" varchar(1),
	"consultation_at" date,
	CONSTRAINT equipe__etablissement_equipe_id_etablissement_id PRIMARY KEY("equipe_id","etablissement_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__etablissement__demande" (
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	"demande_id" integer NOT NULL,
	CONSTRAINT equipe__etablissement__demande_equipe_id_etablissement_id_demande_id PRIMARY KEY("equipe_id","etablissement_id","demande_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__etablissement_description" (
	"description" text NOT NULL,
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	CONSTRAINT equipe__etablissement_description_equipe_id_etablissement_id PRIMARY KEY("equipe_id","etablissement_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__etablissement__echange" (
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	"echange_id" integer NOT NULL,
	CONSTRAINT equipe__etablissement__echange_equipe_id_etablissement_id_echange_id PRIMARY KEY("equipe_id","etablissement_id","echange_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__etablissement__etiquette" (
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	"etiquette_id" integer NOT NULL,
	CONSTRAINT equipe__etablissement__etiquette_equipe_id_etablissement_id_etiquette_id PRIMARY KEY("equipe_id","etablissement_id","etiquette_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__etablissement_exogene" (
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	"ajout_at" timestamp with time zone DEFAULT now() NOT NULL,
	CONSTRAINT equipe__etablissement_exogene_equipe_id_etablissement_id PRIMARY KEY("equipe_id","etablissement_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__etablissement_portefeuille" (
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	CONSTRAINT equipe__etablissement_portefeuille_equipe_id_etablissement_id PRIMARY KEY("equipe_id","etablissement_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__etablissement__rappel" (
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	"rappel_id" integer NOT NULL,
	CONSTRAINT equipe__etablissement__rappel_equipe_id_etablissement_id_rappel_id PRIMARY KEY("equipe_id","etablissement_id","rappel_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_favori" (
	"deveco_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	CONSTRAINT etablissement_favori_deveco_id_etablissement_id PRIMARY KEY("deveco_id","etablissement_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "demande" (
	"id" serial PRIMARY KEY NOT NULL,
	"cloture_motif" varchar,
	"date" date NOT NULL,
	"equipe_id" integer NOT NULL,
	"demande_type_id" "string" NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "demande_type" (
	"id" "string" PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "deveco" (
	"id" serial PRIMARY KEY NOT NULL,
	"bienvenue_email" boolean DEFAULT false NOT NULL,
	"compte_id" integer NOT NULL,
	"created_at" timestamp with time zone DEFAULT now() NOT NULL,
	"equipe_id" integer NOT NULL,
	"updated_at" timestamp with time zone DEFAULT now() NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "echange" (
	"id" serial PRIMARY KEY NOT NULL,
	"date" date NOT NULL,
	"description" text,
	"deveco_id" integer NOT NULL,
	"equipe_id" integer NOT NULL,
	"nom" varchar,
	"echange_type_id" varchar(10) NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "echange__demande" (
	"echange_id" integer NOT NULL,
	"demande_id" integer NOT NULL,
	CONSTRAINT echange__demande_echange_id_demande_id PRIMARY KEY("echange_id","demande_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "echange_type" (
	"id" varchar(10) PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe" (
	"id" serial PRIMARY KEY NOT NULL,
	"groupement" boolean DEFAULT false NOT NULL,
	"nom" varchar NOT NULL,
	"territoire_type_id" "string" NOT NULL,
	"equipe_type_id" "string"
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__commune" (
	"equipe_id" integer NOT NULL,
	"commune_id" varchar(5) NOT NULL,
	CONSTRAINT equipe__commune_equipe_id_commune_id PRIMARY KEY("equipe_id","commune_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__departement" (
	"equipe_id" integer NOT NULL,
	"departement_id" varchar(3) NOT NULL,
	CONSTRAINT equipe__departement_equipe_id_departement_id PRIMARY KEY("equipe_id","departement_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__epci" (
	"equipe_id" integer NOT NULL,
	"epci_id" varchar(9) NOT NULL,
	CONSTRAINT equipe__epci_equipe_id_epci_id PRIMARY KEY("equipe_id","epci_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe_import" (
	"id" serial PRIMARY KEY NOT NULL,
	"created_at" timestamp with time zone DEFAULT now() NOT NULL,
	"done" boolean DEFAULT false NOT NULL,
	"equipe_id" integer NOT NULL,
	"ess" boolean DEFAULT false NOT NULL,
	"geolocation" boolean DEFAULT false NOT NULL,
	"qpv" boolean DEFAULT false NOT NULL,
	"started" boolean DEFAULT false NOT NULL,
	"stats" boolean DEFAULT false NOT NULL,
	"ter" boolean DEFAULT false NOT NULL,
	"updated_at" timestamp with time zone DEFAULT now() NOT NULL,
	CONSTRAINT "uk_equipe_import_equipe" UNIQUE("equipe_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__metropole" (
	"equipe_id" integer NOT NULL,
	"metropole_id" varchar(5) NOT NULL,
	CONSTRAINT equipe__metropole_equipe_id_metropole_id PRIMARY KEY("equipe_id","metropole_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__petr" (
	"equipe_id" integer NOT NULL,
	"petr_id" varchar(5) NOT NULL,
	CONSTRAINT equipe__petr_equipe_id_petr_id PRIMARY KEY("equipe_id","petr_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__region" (
	"equipe_id" integer NOT NULL,
	"region_id" varchar(2) NOT NULL,
	CONSTRAINT equipe__region_equipe_id_region_id PRIMARY KEY("equipe_id","region_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe_type" (
	"id" "string" PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etiquette" (
	"id" serial PRIMARY KEY NOT NULL,
	"equipe_id" integer NOT NULL,
	"nom" varchar NOT NULL,
	"etiquette_type_id" varchar(16) NOT NULL,
	CONSTRAINT "uk_etiquette" UNIQUE("etiquette_type_id","nom","equipe_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etiquette_type" (
	"id" varchar(20) PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "geo_token" (
	"id" serial PRIMARY KEY NOT NULL,
	"active" boolean DEFAULT true NOT NULL,
	"deveco_id" integer NOT NULL,
	"token" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "geo_token_request" (
	"id" serial PRIMARY KEY NOT NULL,
	"created_at" timestamp with time zone DEFAULT now() NOT NULL,
	"geo_token_id" integer,
	"url" varchar NOT NULL,
	"user_agent" varchar
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "rappel" (
	"id" serial PRIMARY KEY NOT NULL,
	"date" date NOT NULL,
	"description" text,
	"equipe_id" integer NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "recherche" (
	"id" serial PRIMARY KEY NOT NULL,
	"created_at" timestamp with time zone DEFAULT now() NOT NULL,
	"deveco_id" integer NOT NULL,
	"entite" varchar NOT NULL,
	"format" varchar,
	"requete" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "territoire_type" (
	"id" "string" PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "zonage" (
	"id" serial PRIMARY KEY NOT NULL,
	"equipe_id" integer NOT NULL,
	"geometrie" "geometry(MultiPolygon,4326)",
	"nom" varchar(254) NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "createur" (
	"equipe_id" integer NOT NULL,
	"etablissement_creation_id" integer NOT NULL,
	"personne_id" integer NOT NULL,
	"fonction" varchar(16),
	CONSTRAINT createur_etablissement_creation_id_personne_id PRIMARY KEY("etablissement_creation_id","personne_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_creation" (
	"id" serial PRIMARY KEY NOT NULL,
	"enseigne" varchar,
	"description" text,
	"commentaire" text,
	"equipe_id" integer NOT NULL,
	"entreprise_id" varchar(9)
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_creation__demande" (
	"equipe_id" integer NOT NULL,
	"etablissement_creation_id" integer NOT NULL,
	"demande_id" integer NOT NULL,
	CONSTRAINT etablissement_creation__demande_etablissement_creation_id_demande_id PRIMARY KEY("etablissement_creation_id","demande_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_creation__echange" (
	"equipe_id" integer NOT NULL,
	"etablissement_creation_id" integer NOT NULL,
	"echange_id" integer NOT NULL,
	CONSTRAINT etablissement_creation__echange_etablissement_creation_id_echange_id PRIMARY KEY("etablissement_creation_id","echange_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_creation__etiquette" (
	"equipe_id" integer NOT NULL,
	"etablissement_creation_id" integer NOT NULL,
	"etiquette_id" integer NOT NULL,
	CONSTRAINT etablissement_creation__etiquette_etablissement_creation_id_etiquette_id PRIMARY KEY("etablissement_creation_id","etiquette_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_creation__rappel" (
	"equipe_id" integer NOT NULL,
	"etablissement_creation_id" integer NOT NULL,
	"rappel_id" integer NOT NULL,
	CONSTRAINT etablissement_creation__rappel_etablissement_creation_id_rappel_id PRIMARY KEY("etablissement_creation_id","rappel_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_creation_transformation" (
	"id" serial PRIMARY KEY NOT NULL,
	"date" date NOT NULL,
	"equipe_id" integer NOT NULL,
	"etablissement_creation_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "categorie_entreprise_type" (
	"id" varchar(3) PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "categorie_juridique_type" (
	"id" varchar(4) PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL,
	"parent_categorie_juridique_type_id" varchar(4),
	"niveau" integer NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "effectif_type" (
	"id" varchar(2) PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "entreprise" (
	"id" varchar(9) PRIMARY KEY NOT NULL,
	"nom" varchar,
	"creation_date" date,
	"effectif_annee" integer,
	"effectif_type_id" varchar(2),
	"entreprise_type_id" varchar(1) NOT NULL,
	"diffusible" boolean DEFAULT true NOT NULL,
	"categorie_entreprise_type_id" varchar(3),
	"categorie_entreprise_annee" integer,
	"unite_purgee" boolean DEFAULT false
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "entreprise_periode" (
	"debut_date" date,
	"fin_date" date,
	"entreprise_id" varchar(9) NOT NULL,
	"siege_etablissement_id" varchar(14) NOT NULL,
	"nom" varchar,
	"nom_usage" varchar,
	"denomination" varchar,
	"denomination_usuelle_1" varchar,
	"denomination_usuelle_2" varchar,
	"denomination_usuelle_3" varchar,
	"naf_revision_type_id" varchar(10),
	"naf_type_id" varchar(6),
	"categorie_juridique_type_id" varchar(4) NOT NULL,
	"actif" boolean DEFAULT false NOT NULL,
	"employeur" boolean DEFAULT false NOT NULL,
	"economie_sociale_solidaire" boolean DEFAULT false NOT NULL,
	"societe_mission" boolean DEFAULT false,
	"siege_etablissement_id_change" boolean DEFAULT false NOT NULL,
	"nom_change" boolean DEFAULT false NOT NULL,
	"nom_usage_change" boolean DEFAULT false NOT NULL,
	"denomination_change" boolean DEFAULT false NOT NULL,
	"denomination_usuelle_change" boolean DEFAULT false NOT NULL,
	"naf_type_change" boolean DEFAULT false NOT NULL,
	"categorie_juridique_change" boolean DEFAULT false NOT NULL,
	"actif_change" boolean DEFAULT false NOT NULL,
	"employeur_change" boolean DEFAULT false NOT NULL,
	"economie_sociale_solidaire_change" boolean DEFAULT false NOT NULL,
	"societe_mission_change" boolean DEFAULT false NOT NULL,
	CONSTRAINT "uk_entreprise_periode" UNIQUE("entreprise_id","debut_date")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "entreprise_type" (
	"id" varchar(1) PRIMARY KEY NOT NULL,
	"nom" varchar(10) NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement" (
	"id" varchar(14) PRIMARY KEY NOT NULL,
	"nic" varchar(5),
	"nom" varchar,
	"creation_date" date,
	"sirene_dernier_traitement_date" date,
	"effectif_type_id" varchar(2),
	"effectif_annee" integer,
	"entreprise_id" varchar(9) NOT NULL,
	"commune_id" varchar(5) NOT NULL,
	"diffusible" boolean DEFAULT true NOT NULL,
	"siege" boolean
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_adresse_1" (
	"id" serial PRIMARY KEY NOT NULL,
	"cedex_code" varchar(9),
	"cedex_nom" varchar,
	"code_postal" varchar(5),
	"commune_id" varchar(5) NOT NULL,
	"distribution_speciale" varchar,
	"geolocalisation" "geometry(Point, 4326)",
	"numero" varchar,
	"voie_nom" varchar,
	"etablissement_id" varchar
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_effectif_emm" (
	"etablissement_id" varchar(14) NOT NULL,
	"annee" integer NOT NULL,
	"mois" integer NOT NULL,
	"valeur" double precision NOT NULL,
	CONSTRAINT etablissement_effectif_emm_etablissement_id_annee_mois PRIMARY KEY("etablissement_id","annee","mois")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_exercice" (
	"cloture_date" date NOT NULL,
	"etablissement_id" varchar(14),
	"chiffre_affaires" bigint NOT NULL,
	CONSTRAINT "uk_etablissement_exercice" UNIQUE("cloture_date","etablissement_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_periode" (
	"etablissement_id" varchar(14),
	"debut_date" date,
	"fin_date" date,
	"enseigne_1" varchar,
	"enseigne_2" varchar,
	"enseigne_3" varchar,
	"naf_revision_type_id" varchar(10),
	"naf_type_id" varchar(6),
	"actif" boolean,
	"denomination_usuelle" varchar,
	"employeur" boolean DEFAULT false NOT NULL,
	"actif_change" boolean DEFAULT false NOT NULL,
	"enseigne_change" boolean DEFAULT false NOT NULL,
	"naf_type_change" boolean DEFAULT false NOT NULL,
	"employeur_change" boolean DEFAULT false NOT NULL,
	CONSTRAINT "uk_etablissement_periode" UNIQUE("etablissement_id","debut_date")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "mandataire_personne_morale" (
	"id" serial PRIMARY KEY NOT NULL,
	"entreprise_id" varchar(9) NOT NULL,
	"mandataire_entreprise_id" varchar(9),
	"fonction" varchar NOT NULL,
	"greffe_code" varchar,
	"greffe_libelle" varchar
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "mandataire_personne_physique" (
	"id" serial PRIMARY KEY NOT NULL,
	"entreprise_id" varchar(9) NOT NULL,
	"nom" varchar NOT NULL,
	"prenom" varchar,
	"fonction" varchar NOT NULL,
	"naissance_date" date,
	"naissance_lieu" varchar,
	"naissance_pays" varchar,
	"naissance_pays_code" varchar,
	"nationalite" varchar,
	"nationalite_code" varchar
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "naf_revision_type" (
	"id" varchar(10) PRIMARY KEY NOT NULL,
	"nom" varchar(30) NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "naf_type" (
	"naf_revision_type_id" varchar(10) NOT NULL,
	"id" varchar(10) NOT NULL,
	"nom" varchar NOT NULL,
	"niveau" integer NOT NULL,
	"parent_naf_type_id" varchar(10),
	CONSTRAINT naf_type_naf_revision_type_id_id PRIMARY KEY("naf_revision_type_id","id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "personne_morale" (
	"entreprise_id" varchar(9) PRIMARY KEY NOT NULL,
	"sigle" varchar,
	"association_id" varchar(10)
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "personne_physique" (
	"entreprise_id" varchar(9) PRIMARY KEY NOT NULL,
	"sexe_type_id" varchar(1),
	"prenom_1" varchar,
	"prenom_2" varchar,
	"prenom_3" varchar,
	"prenom_4" varchar,
	"prenom_usuel" varchar,
	"pseudonyme" varchar
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "sexe_type" (
	"id" varchar(1) PRIMARY KEY NOT NULL,
	"nom" varchar(10) NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement" (
	"id" serial PRIMARY KEY NOT NULL,
	"evenement_type_id" varchar NOT NULL,
	"date" timestamp with time zone DEFAULT now() NOT NULL,
	"diff" jsonb,
	"compte_id" integer,
	"api_type_id" varchar(16)
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__compte" (
	"evenement_id" integer NOT NULL,
	"compte_id" integer NOT NULL,
	CONSTRAINT evenement__compte_evenement_id_compte_id PRIMARY KEY("evenement_id","compte_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__contact" (
	"evenement_id" integer NOT NULL,
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	"personne_id" integer NOT NULL,
	CONSTRAINT evenement__contact_evenement_id_equipe_id_etablissement_id_personne_id PRIMARY KEY("evenement_id","equipe_id","etablissement_id","personne_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__demande" (
	"evenement_id" integer NOT NULL,
	"demande_id" integer NOT NULL,
	CONSTRAINT evenement__demande_evenement_id_demande_id PRIMARY KEY("evenement_id","demande_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__echange" (
	"evenement_id" integer NOT NULL,
	"echange_id" integer NOT NULL,
	CONSTRAINT evenement__echange_evenement_id_echange_id PRIMARY KEY("evenement_id","echange_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__entreprise" (
	"evenement_id" integer NOT NULL,
	"entreprise_id" varchar(14) NOT NULL,
	CONSTRAINT evenement__entreprise_evenement_id_entreprise_id PRIMARY KEY("evenement_id","entreprise_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__equipe_etablissement" (
	"evenement_id" integer NOT NULL,
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	CONSTRAINT evenement__equipe_etablissement_evenement_id_equipe_id_etablissement_id PRIMARY KEY("evenement_id","equipe_id","etablissement_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__etablissement" (
	"evenement_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	CONSTRAINT evenement__etablissement_evenement_id_etablissement_id PRIMARY KEY("evenement_id","etablissement_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__etablissement_creation" (
	"evenement_id" integer NOT NULL,
	"etablissement_creation_id" integer NOT NULL,
	CONSTRAINT evenement__etablissement_creation_evenement_id_etablissement_creation_id PRIMARY KEY("evenement_id","etablissement_creation_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__etiquette" (
	"evenement_id" integer NOT NULL,
	"etiquette_id" integer NOT NULL,
	CONSTRAINT evenement__etiquette_evenement_id_etiquette_id PRIMARY KEY("evenement_id","etiquette_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__etiquette__etablissement" (
	"evenement_id" integer NOT NULL,
	"etiquette_id" integer NOT NULL,
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	CONSTRAINT evenement__etiquette__etablissement_evenement_id_etiquette_id_etablissement_id PRIMARY KEY("evenement_id","etiquette_id","etablissement_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__etiquette__etablissement_creation" (
	"evenement_id" integer NOT NULL,
	"etiquette_id" integer NOT NULL,
	"etablissement_creation_id" integer NOT NULL,
	CONSTRAINT evenement__etiquette__etablissement_creation_evenement_id_etiquette_id_etablissement_creation_id PRIMARY KEY("evenement_id","etiquette_id","etablissement_creation_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__local" (
	"evenement_id" integer NOT NULL,
	"local_id" integer NOT NULL,
	CONSTRAINT evenement__local_evenement_id_local_id PRIMARY KEY("evenement_id","local_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__personne" (
	"evenement_id" integer NOT NULL,
	"personne_id" integer NOT NULL,
	CONSTRAINT evenement__personne_evenement_id_personne_id PRIMARY KEY("evenement_id","personne_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__proprietaire_etablissement" (
	"evenement_id" integer NOT NULL,
	"local_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	CONSTRAINT evenement__proprietaire_etablissement_evenement_id_local_id_etablissement_id PRIMARY KEY("evenement_id","local_id","etablissement_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__proprietaire_personne" (
	"evenement_id" integer NOT NULL,
	"local_id" integer NOT NULL,
	"personne_id" integer NOT NULL,
	CONSTRAINT evenement__proprietaire_personne_evenement_id_local_id_personne_id PRIMARY KEY("evenement_id","local_id","personne_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement__rappel" (
	"evenement_id" integer NOT NULL,
	"rappel_id" integer NOT NULL,
	CONSTRAINT evenement__rappel_evenement_id_rappel_id PRIMARY KEY("evenement_id","rappel_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "evenement_type" (
	"id" varchar PRIMARY KEY NOT NULL,
	"nom" varchar
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "local" (
	"id" serial PRIMARY KEY NOT NULL,
	"nom" varchar,
	"surface" varchar,
	"loyer" varchar,
	"description" text,
	"local_statut_type_id" "string",
	"equipe_id" integer NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "local_adresse" (
	"id" serial PRIMARY KEY NOT NULL,
	"cedex_code" varchar(9),
	"cedex_nom" varchar,
	"code_postal" varchar(5),
	"commune_id" varchar(5) NOT NULL,
	"distribution_speciale" varchar,
	"geolocalisation" "geometry(Point, 4326)",
	"numero" varchar,
	"voie_nom" varchar,
	"local_id" integer
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "local__etiquette" (
	"local_id" integer NOT NULL,
	"etiquette_id" integer NOT NULL,
	CONSTRAINT local__etiquette_local_id_etiquette_id PRIMARY KEY("local_id","etiquette_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "local__local_type" (
	"local_id" integer NOT NULL,
	"local_type_id" varchar(20) NOT NULL,
	CONSTRAINT local__local_type_local_id_local_type_id PRIMARY KEY("local_id","local_type_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "local_statut_type" (
	"id" "string" PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "local_type" (
	"id" varchar(20) PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "proprietaire_etablissement" (
	"local_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	CONSTRAINT proprietaire_etablissement_local_id_etablissement_id PRIMARY KEY("local_id","etablissement_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "proprietaire_personne" (
	"local_id" integer NOT NULL,
	"personne_id" integer NOT NULL,
	CONSTRAINT proprietaire_personne_local_id_personne_id PRIMARY KEY("local_id","personne_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_api_entreprise_entreprise_mandataire_social" (
	"entreprise_id" varchar(9) NOT NULL,
	"payload" jsonb NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_api_entreprise_etablissement_exercice" (
	"siret" varchar(14) NOT NULL,
	"chiffre_affaires" varchar,
	"date_fin_exercice" date NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_api_sirene_etablissement" (
	"siret" varchar(14) NOT NULL,
	"siren" varchar(9) NOT NULL,
	"activite_principale_etablissement" varchar(6),
	"activite_principale_registre_metiers_etablissement" varchar(6),
	"annee_effectifs_etablissement" varchar(9),
	"caractere_employeur_etablissement" "source_sirene_etablissement_caractere_employeur_enum",
	"code_cedex2_etablissement" varchar(9),
	"code_cedex_etablissement" varchar(9),
	"code_commune2_etablissement" varchar(5),
	"code_commune_etablissement" varchar(5) NOT NULL,
	"code_pays_etranger2_etablissement" varchar(5),
	"code_pays_etranger_etablissement" varchar(5),
	"code_postal2_etablissement" varchar(5),
	"code_postal_etablissement" varchar(5),
	"complement_adresse2_etablissement" varchar,
	"complement_adresse_etablissement" varchar,
	"date_creation_etablissement" date,
	"date_debut" date,
	"date_dernier_traitement_etablissement" timestamp with time zone,
	"denomination_usuelle_etablissement" varchar,
	"distribution_speciale2_etablissement" varchar,
	"distribution_speciale_etablissement" varchar,
	"enseigne1_etablissement" varchar,
	"enseigne2_etablissement" varchar,
	"enseigne3_etablissement" varchar,
	"etablissement_siege" boolean NOT NULL,
	"etat_administratif_etablissement" "source_sirene_etablissement_etat_administratif_enum" NOT NULL,
	"indice_repetition2_etablissement" varchar(5),
	"indice_repetition_etablissement" varchar(10),
	"libelle_cedex2_etablissement" varchar,
	"libelle_cedex_etablissement" varchar,
	"libelle_commune2_etablissement" varchar,
	"libelle_commune_etablissement" varchar,
	"libelle_commune_etranger2_etablissement" varchar,
	"libelle_commune_etranger_etablissement" varchar,
	"libelle_pays_etranger2_etablissement" varchar,
	"libelle_pays_etranger_etablissement" varchar,
	"libelle_voie2_etablissement" varchar,
	"libelle_voie_etablissement" varchar,
	"nic" varchar(5) NOT NULL,
	"nombre_periodes_etablissement" integer,
	"nomenclature_activite_principale_etablissement" varchar(8),
	"numero_voie2_etablissement" varchar,
	"numero_voie_etablissement" varchar,
	"statut_diffusion_etablissement" "source_sirene_etablissement_statut_diffusion_enum" NOT NULL,
	"tranche_effectifs_etablissement" varchar(2),
	"type_voie2_etablissement" varchar(4),
	"type_voie_etablissement" varchar(4),
	"mise_a_jour_at" timestamp with time zone
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_api_sirene_etablissement_periode" (
	"siret" varchar(14) NOT NULL,
	"activite_principale_etablissement" varchar(6),
	"caractere_employeur_etablissement" "source_sirene_etablissement_caractere_employeur_enum",
	"date_debut" date,
	"date_fin" date,
	"denomination_usuelle_etablissement" varchar,
	"enseigne1_etablissement" varchar,
	"enseigne2_etablissement" varchar,
	"enseigne3_etablissement" varchar,
	"etat_administratif_etablissement" "source_sirene_etablissement_etat_administratif_enum" NOT NULL,
	"nomenclature_activite_principale_etablissement" varchar(8),
	"changement_activite_principale_etablissement" boolean,
	"changement_caractere_employeur_etablissement" boolean,
	"changement_enseigne_etablissement" boolean,
	"changement_etat_administratif_etablissement" boolean,
	"mise_a_jour_at" timestamp with time zone,
	CONSTRAINT "uk_source_api_sirene_etablissement_periode" UNIQUE("siret","date_debut")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_api_sirene_unite_legale" (
	"siren" varchar(9) NOT NULL,
	"activite_principale_unite_legale" varchar(6),
	"annee_categorie_entreprise" integer,
	"annee_effectifs_unite_legale" integer,
	"caractere_employeur_unite_legale" varchar(6),
	"categorie_entreprise" "source_sirene_unite_legale_categorie_entreprise_enum",
	"categorie_juridique_unite_legale" varchar(4),
	"date_creation_unite_legale" date,
	"date_dernier_traitement_unite_legale" timestamp with time zone,
	"denomination_unite_legale" varchar(120),
	"denomination_usuelle1_unite_legale" varchar,
	"denomination_usuelle2_unite_legale" varchar,
	"denomination_usuelle3_unite_legale" varchar,
	"economie_sociale_solidaire_unite_legale" "source_sirene_unite_legale_eco_soc_sol_enum",
	"etat_administratif_unite_legale" "source_sirene_unite_legale_etat_administratif_enum",
	"identifiant_association_unite_legale" varchar(10),
	"nic_siege_unite_legale" varchar(5),
	"nombre_periodes_unite_legale" integer,
	"nomenclature_activite_principale_unite_legale" varchar(8),
	"nom_unite_legale" varchar,
	"nom_usage_unite_legale" varchar,
	"prenom1_unite_legale" varchar,
	"prenom2_unite_legale" varchar,
	"prenom3_unite_legale" varchar,
	"prenom4_unite_legale" varchar,
	"prenom_usuel_unite_legale" varchar(20),
	"pseudonyme_unite_legale" varchar,
	"sexe_unite_legale" "source_sirene_unite_legale_sexe_enum",
	"sigle_unite_legale" varchar,
	"societe_mission_unite_legale" "source_sirene_unite_legale_societe_mission_enum",
	"statut_diffusion_unite_legale" "source_sirene_unite_legale_statut_diffusion_enum" NOT NULL,
	"tranche_effectifs_unite_legale" varchar(2),
	"unite_purgee_unite_legale" boolean,
	"mise_a_jour_at" timestamp with time zone
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_api_sirene_unite_legale_periode" (
	"siren" varchar NOT NULL,
	"activite_principale_unite_legale" varchar(6),
	"caractere_employeur_unite_legale" varchar(6),
	"categorie_juridique_unite_legale" varchar(4),
	"date_debut" date,
	"date_fin" date,
	"denomination_unite_legale" varchar(120),
	"denomination_usuelle_1_unite_legale" varchar,
	"denomination_usuelle_2_unite_legale" varchar,
	"denomination_usuelle_3_unite_legale" varchar,
	"economie_sociale_solidaire_unite_legale" "source_sirene_unite_legale_eco_soc_sol_enum",
	"etat_administratif_unite_legale" "source_sirene_unite_legale_etat_administratif_enum",
	"nic_siege_unite_legale" varchar(5),
	"nom_unite_legale" varchar,
	"nom_usage_unite_legale" varchar,
	"nomenclature_activite_principale_unite_legale" varchar(10),
	"societe_mission_unite_legale" "source_sirene_unite_legale_societe_mission_enum",
	"changement_activite_principale_unite_legale" boolean,
	"changement_caractere_employeur_unite_legale" boolean,
	"changement_categorie_juridique_unite_legale" boolean,
	"changement_denomination_unite_legale" boolean,
	"changement_denomination_usuelle_unite_legale" boolean,
	"changement_economie_sociale_solidaire_unite_legale" boolean,
	"changement_societe_mission_unite_legale" boolean,
	"changement_etat_administratif_unite_legale" boolean,
	"changement_nic_siege_unite_legale" boolean,
	"changement_nom_unite_legale" boolean,
	"changement_nom_usage_unite_legale" boolean,
	"mise_a_jour_at" timestamp with time zone,
	CONSTRAINT "uk_source_api_sirene_unite_legale_periode" UNIQUE("siren","date_debut")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_datagouv_commune_ept" (
	"insee_com" varchar(5) NOT NULL,
	"insee_dep" varchar(2) NOT NULL,
	"insee_ept" varchar(3) NOT NULL,
	"insee_reg" varchar(2) NOT NULL,
	"lib_com" varchar NOT NULL,
	"lib_ept" varchar NOT NULL,
	"siren" varchar(9) NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_datagouv_qpv" (
	"code_qp" varchar(8),
	"commune_qp" varchar,
	"geom" "geometry(MultiPolygon,4326)",
	"gid" serial NOT NULL,
	"nom_qp" varchar
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_datagouv_zrr" (
	"insee_com" varchar(5) NOT NULL,
	"zrr" varchar(1) NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_ess_france" (
	"entreprise_id" varchar(9) PRIMARY KEY NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_geoloc_entreprise" (
	"distance_precision" varchar,
	"epsg" varchar,
	"plg_code_commune" varchar,
	"plg_iris" varchar,
	"plg_qp" varchar,
	"plg_qva" varchar,
	"plg_zfu" varchar,
	"plg_zus" varchar,
	"qualite_iris" varchar,
	"qualite_qp" varchar,
	"qualite_qva" varchar,
	"qualite_xy" varchar,
	"qualite_zfu" varchar,
	"qualite_zus" varchar,
	"siret" varchar NOT NULL,
	"x" double precision,
	"x_longitude" double precision,
	"y" double precision,
	"y_latitude" double precision
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_insee_commune" (
	"arr" varchar(4),
	"can" varchar(5),
	"com" varchar(5),
	"comparent" varchar(5),
	"ctcd" varchar(4),
	"dep" varchar(3),
	"libelle" varchar,
	"ncc" varchar,
	"nccenr" varchar,
	"reg" varchar(2),
	"tncc" varchar(1),
	"typecom" varchar(4)
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_insee_commune_historique" (
	"com" varchar(5),
	"date_debut" date,
	"date_fin" date,
	"libelle" varchar,
	"ncc" varchar,
	"nccenr" varchar,
	"tncc" varchar(1)
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_insee_commune_mouvement" (
	"com_ap" varchar(5),
	"com_av" varchar(5),
	"date_eff" date,
	"indic_oe" varchar,
	"indic_oe2" varchar,
	"libelle_ap" varchar,
	"libelle_av" varchar,
	"mod" varchar,
	"ncc_ap" varchar,
	"ncc_av" varchar,
	"nccenr_ap" varchar,
	"nccenr_av" varchar,
	"tncc_ap" varchar(1),
	"tncc_av" varchar(1),
	"typecom_ap" varchar(4),
	"typecom_av" varchar(4)
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_insee_departement" (
	"cheflieu" varchar(5) NOT NULL,
	"dep" varchar(3) NOT NULL,
	"libelle" varchar NOT NULL,
	"ncc" varchar NOT NULL,
	"nccenr" varchar NOT NULL,
	"reg" varchar(2) NOT NULL,
	"tncc" varchar(1) NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_insee_epci" (
	"epci" varchar(9) NOT NULL,
	"libepci" varchar NOT NULL,
	"nature_epci" varchar NOT NULL,
	"nb_com" integer NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_insee_epci_commune" (
	"codgeo" varchar(5) PRIMARY KEY NOT NULL,
	"dep" varchar(3) NOT NULL,
	"epci" varchar(9) NOT NULL,
	"libepci" varchar NOT NULL,
	"libgeo" varchar NOT NULL,
	"reg" varchar(2) NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_insee_region" (
	"cheflieu" varchar(5) NOT NULL,
	"libelle" varchar NOT NULL,
	"ncc" varchar NOT NULL,
	"nccenr" varchar NOT NULL,
	"reg" varchar(2) PRIMARY KEY NOT NULL,
	"tncc" varchar(1) NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_laposte_code_insee_code_postal" (
	"code_insee" varchar(5),
	"code_postal" varchar(5),
	"libelle_acheminement" varchar,
	"ligne_5" varchar,
	"nom" varchar
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_odt_epci_petr" (
	"codgeo" varchar(9) PRIMARY KEY NOT NULL,
	"libgeo" varchar NOT NULL,
	"pays" varchar(4),
	"pays_libgeo" varchar
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_rcd_etablissement_effectif_emm" (
	"siret" varchar(14) NOT NULL,
	"mois" date NOT NULL,
	"effectif" varchar
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_sirene_lien_succession" (
	"id" serial PRIMARY KEY NOT NULL,
	"continuite_economique" boolean NOT NULL,
	"date_succession" timestamp with time zone NOT NULL,
	"date_traitement" timestamp with time zone NOT NULL,
	"siret_predecesseur" varchar(14) NOT NULL,
	"siret_successeur" varchar(14) NOT NULL,
	"transfert_siege" boolean NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_sirene_lien_succession_temp" (
	"id" serial PRIMARY KEY NOT NULL,
	"continuite_economique" boolean NOT NULL,
	"date_succession" timestamp with time zone NOT NULL,
	"date_traitement" timestamp with time zone NOT NULL,
	"siret_predecesseur" varchar(14) NOT NULL,
	"siret_successeur" varchar(14) NOT NULL,
	"transfert_siege" boolean NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "commune" (
	"id" varchar(5) PRIMARY KEY NOT NULL,
	"nom" varchar(100) NOT NULL,
	"commune_type_id" varchar(4) NOT NULL,
	"commune_parent_id" varchar(5),
	"metropole_id" varchar(5),
	"epci_id" varchar(9) NOT NULL,
	"petr_id" varchar(4),
	"departement_id" varchar(3) NOT NULL,
	"region_id" varchar(2) NOT NULL,
	"zrr_type_id" varchar(1) DEFAULT 'N' NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "commune_type" (
	"id" varchar PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "departement" (
	"id" varchar(3) PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL,
	"region_id" varchar(2) NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "epci" (
	"id" varchar(9) PRIMARY KEY NOT NULL,
	"epci_type_id" varchar(2) NOT NULL,
	"nom" varchar NOT NULL,
	"petr_id" varchar(4)
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "epci_type" (
	"id" varchar(2) PRIMARY KEY NOT NULL,
	"nom" varchar(50) NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "metropole" (
	"id" varchar(5) PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "petr" (
	"id" varchar(4) PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "region" (
	"id" varchar(2) PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "zrr_type" (
	"id" varchar(1) PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "IDX_zonage__geometrie" ON "zonage" ("geometrie");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "IDX_etablissement_effectif_emm__etablissement" ON "etablissement_effectif_emm" ("etablissement_id");--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "mot_de_passe" ADD CONSTRAINT "mot_de_passe_compte_id_compte_id_fk" FOREIGN KEY ("compte_id") REFERENCES "compte"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "personne" ADD CONSTRAINT "personne_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "personne_adresse" ADD CONSTRAINT "personne_adresse_commune_id_commune_id_fk" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "contact" ADD CONSTRAINT "contact_personne_id_personne_id_fk" FOREIGN KEY ("personne_id") REFERENCES "personne"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__demande" ADD CONSTRAINT "equipe__etablissement__demande_demande_id_demande_id_fk" FOREIGN KEY ("demande_id") REFERENCES "demande"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__echange" ADD CONSTRAINT "equipe__etablissement__echange_echange_id_echange_id_fk" FOREIGN KEY ("echange_id") REFERENCES "echange"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__etiquette" ADD CONSTRAINT "equipe__etablissement__etiquette_etiquette_id_etiquette_id_fk" FOREIGN KEY ("etiquette_id") REFERENCES "etiquette"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__rappel" ADD CONSTRAINT "equipe__etablissement__rappel_rappel_id_rappel_id_fk" FOREIGN KEY ("rappel_id") REFERENCES "rappel"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "demande" ADD CONSTRAINT "demande_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "demande" ADD CONSTRAINT "demande_demande_type_id_demande_type_id_fk" FOREIGN KEY ("demande_type_id") REFERENCES "demande_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "deveco" ADD CONSTRAINT "deveco_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "echange" ADD CONSTRAINT "echange_deveco_id_deveco_id_fk" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "echange" ADD CONSTRAINT "echange_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "echange" ADD CONSTRAINT "echange_echange_type_id_echange_type_id_fk" FOREIGN KEY ("echange_type_id") REFERENCES "echange_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "echange__demande" ADD CONSTRAINT "echange__demande_echange_id_echange_id_fk" FOREIGN KEY ("echange_id") REFERENCES "echange"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "echange__demande" ADD CONSTRAINT "echange__demande_demande_id_demande_id_fk" FOREIGN KEY ("demande_id") REFERENCES "demande"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe" ADD CONSTRAINT "equipe_territoire_type_id_territoire_type_id_fk" FOREIGN KEY ("territoire_type_id") REFERENCES "territoire_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe" ADD CONSTRAINT "equipe_equipe_type_id_equipe_type_id_fk" FOREIGN KEY ("equipe_type_id") REFERENCES "equipe_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__commune" ADD CONSTRAINT "equipe__commune_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__commune" ADD CONSTRAINT "equipe__commune_commune_id_commune_id_fk" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__departement" ADD CONSTRAINT "equipe__departement_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__departement" ADD CONSTRAINT "equipe__departement_departement_id_departement_id_fk" FOREIGN KEY ("departement_id") REFERENCES "departement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__epci" ADD CONSTRAINT "equipe__epci_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__epci" ADD CONSTRAINT "equipe__epci_epci_id_epci_id_fk" FOREIGN KEY ("epci_id") REFERENCES "epci"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_import" ADD CONSTRAINT "equipe_import_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__metropole" ADD CONSTRAINT "equipe__metropole_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__metropole" ADD CONSTRAINT "equipe__metropole_metropole_id_metropole_id_fk" FOREIGN KEY ("metropole_id") REFERENCES "metropole"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__petr" ADD CONSTRAINT "equipe__petr_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__petr" ADD CONSTRAINT "equipe__petr_petr_id_petr_id_fk" FOREIGN KEY ("petr_id") REFERENCES "petr"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__region" ADD CONSTRAINT "equipe__region_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__region" ADD CONSTRAINT "equipe__region_region_id_region_id_fk" FOREIGN KEY ("region_id") REFERENCES "region"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etiquette" ADD CONSTRAINT "etiquette_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etiquette" ADD CONSTRAINT "etiquette_etiquette_type_id_etiquette_type_id_fk" FOREIGN KEY ("etiquette_type_id") REFERENCES "etiquette_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "geo_token" ADD CONSTRAINT "geo_token_deveco_id_deveco_id_fk" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "geo_token_request" ADD CONSTRAINT "geo_token_request_geo_token_id_geo_token_id_fk" FOREIGN KEY ("geo_token_id") REFERENCES "geo_token"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "rappel" ADD CONSTRAINT "rappel_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "recherche" ADD CONSTRAINT "recherche_deveco_id_deveco_id_fk" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "zonage" ADD CONSTRAINT "zonage_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "createur" ADD CONSTRAINT "createur_etablissement_creation_id_etablissement_creation_id_fk" FOREIGN KEY ("etablissement_creation_id") REFERENCES "etablissement_creation"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "createur" ADD CONSTRAINT "createur_personne_id_personne_id_fk" FOREIGN KEY ("personne_id") REFERENCES "personne"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation" ADD CONSTRAINT "etablissement_creation_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation" ADD CONSTRAINT "etablissement_creation_entreprise_id_entreprise_id_fk" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation__demande" ADD CONSTRAINT "etablissement_creation__demande_etablissement_creation_id_etablissement_creation_id_fk" FOREIGN KEY ("etablissement_creation_id") REFERENCES "etablissement_creation"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation__demande" ADD CONSTRAINT "etablissement_creation__demande_demande_id_demande_id_fk" FOREIGN KEY ("demande_id") REFERENCES "demande"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation__echange" ADD CONSTRAINT "etablissement_creation__echange_etablissement_creation_id_etablissement_creation_id_fk" FOREIGN KEY ("etablissement_creation_id") REFERENCES "etablissement_creation"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation__echange" ADD CONSTRAINT "etablissement_creation__echange_echange_id_echange_id_fk" FOREIGN KEY ("echange_id") REFERENCES "echange"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation__etiquette" ADD CONSTRAINT "etablissement_creation__etiquette_etablissement_creation_id_etablissement_creation_id_fk" FOREIGN KEY ("etablissement_creation_id") REFERENCES "etablissement_creation"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation__etiquette" ADD CONSTRAINT "etablissement_creation__etiquette_etiquette_id_etiquette_id_fk" FOREIGN KEY ("etiquette_id") REFERENCES "etiquette"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation__rappel" ADD CONSTRAINT "etablissement_creation__rappel_etablissement_creation_id_etablissement_creation_id_fk" FOREIGN KEY ("etablissement_creation_id") REFERENCES "etablissement_creation"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation__rappel" ADD CONSTRAINT "etablissement_creation__rappel_rappel_id_rappel_id_fk" FOREIGN KEY ("rappel_id") REFERENCES "rappel"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation_transformation" ADD CONSTRAINT "etablissement_creation_transformation_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation_transformation" ADD CONSTRAINT "etablissement_creation_transformation_etablissement_creation_id_etablissement_creation_id_fk" FOREIGN KEY ("etablissement_creation_id") REFERENCES "etablissement_creation"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation_transformation" ADD CONSTRAINT "etablissement_creation_transformation_etablissement_id_etablissement_id_fk" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "categorie_juridique_type" ADD CONSTRAINT "categorie_juridique_type_parent_categorie_juridique_type_id_categorie_juridique_type_id_fk" FOREIGN KEY ("parent_categorie_juridique_type_id") REFERENCES "categorie_juridique_type"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "entreprise" ADD CONSTRAINT "entreprise_entreprise_type_id_entreprise_type_id_fk" FOREIGN KEY ("entreprise_type_id") REFERENCES "entreprise_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "entreprise" ADD CONSTRAINT "entreprise_categorie_entreprise_type_id_categorie_entreprise_type_id_fk" FOREIGN KEY ("categorie_entreprise_type_id") REFERENCES "categorie_entreprise_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "entreprise_periode" ADD CONSTRAINT "entreprise_periode_categorie_juridique_type_id_categorie_juridique_type_id_fk" FOREIGN KEY ("categorie_juridique_type_id") REFERENCES "categorie_juridique_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement" ADD CONSTRAINT "etablissement_effectif_type_id_effectif_type_id_fk" FOREIGN KEY ("effectif_type_id") REFERENCES "effectif_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement" ADD CONSTRAINT "etablissement_entreprise_id_entreprise_id_fk" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement" ADD CONSTRAINT "etablissement_commune_id_commune_id_fk" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_adresse_1" ADD CONSTRAINT "etablissement_adresse_1_commune_id_commune_id_fk" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "mandataire_personne_morale" ADD CONSTRAINT "mandataire_personne_morale_entreprise_id_entreprise_id_fk" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "mandataire_personne_morale" ADD CONSTRAINT "mandataire_personne_morale_mandataire_entreprise_id_entreprise_id_fk" FOREIGN KEY ("mandataire_entreprise_id") REFERENCES "entreprise"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "mandataire_personne_physique" ADD CONSTRAINT "mandataire_personne_physique_entreprise_id_entreprise_id_fk" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "naf_type" ADD CONSTRAINT "naf_type_naf_revision_type_id_naf_revision_type_id_fk" FOREIGN KEY ("naf_revision_type_id") REFERENCES "naf_revision_type"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "naf_type" ADD CONSTRAINT "naf_type_parent_naf_type_id_naf_revision_type_id_naf_type_naf_revision_type_id_id_fk" FOREIGN KEY ("parent_naf_type_id","naf_revision_type_id") REFERENCES "naf_type"("naf_revision_type_id","id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "personne_morale" ADD CONSTRAINT "personne_morale_entreprise_id_entreprise_id_fk" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "personne_physique" ADD CONSTRAINT "personne_physique_entreprise_id_entreprise_id_fk" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "personne_physique" ADD CONSTRAINT "personne_physique_sexe_type_id_sexe_type_id_fk" FOREIGN KEY ("sexe_type_id") REFERENCES "sexe_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement" ADD CONSTRAINT "evenement_evenement_type_id_evenement_type_id_fk" FOREIGN KEY ("evenement_type_id") REFERENCES "evenement_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement" ADD CONSTRAINT "evenement_compte_id_compte_id_fk" FOREIGN KEY ("compte_id") REFERENCES "compte"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__compte" ADD CONSTRAINT "evenement__compte_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__compte" ADD CONSTRAINT "evenement__compte_compte_id_compte_id_fk" FOREIGN KEY ("compte_id") REFERENCES "compte"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__contact" ADD CONSTRAINT "evenement__contact_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__contact" ADD CONSTRAINT "evenement__contact_equipe_id_etablissement_id_personne_id_contact_equipe_id_etablissement_id_personne_id_fk" FOREIGN KEY ("equipe_id","etablissement_id","personne_id") REFERENCES "contact"("equipe_id","etablissement_id","personne_id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__demande" ADD CONSTRAINT "evenement__demande_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__demande" ADD CONSTRAINT "evenement__demande_demande_id_demande_id_fk" FOREIGN KEY ("demande_id") REFERENCES "demande"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__echange" ADD CONSTRAINT "evenement__echange_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__echange" ADD CONSTRAINT "evenement__echange_echange_id_echange_id_fk" FOREIGN KEY ("echange_id") REFERENCES "echange"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__entreprise" ADD CONSTRAINT "evenement__entreprise_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__entreprise" ADD CONSTRAINT "evenement__entreprise_entreprise_id_entreprise_id_fk" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__equipe_etablissement" ADD CONSTRAINT "evenement__equipe_etablissement_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__equipe_etablissement" ADD CONSTRAINT "evenement__equipe_etablissement_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__equipe_etablissement" ADD CONSTRAINT "evenement__equipe_etablissement_etablissement_id_etablissement_id_fk" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__etablissement" ADD CONSTRAINT "evenement__etablissement_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__etablissement" ADD CONSTRAINT "evenement__etablissement_etablissement_id_etablissement_id_fk" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__etablissement_creation" ADD CONSTRAINT "evenement__etablissement_creation_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__etablissement_creation" ADD CONSTRAINT "evenement__etablissement_creation_etablissement_creation_id_etablissement_creation_id_fk" FOREIGN KEY ("etablissement_creation_id") REFERENCES "etablissement_creation"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__etiquette" ADD CONSTRAINT "evenement__etiquette_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__etiquette" ADD CONSTRAINT "evenement__etiquette_etiquette_id_etiquette_id_fk" FOREIGN KEY ("etiquette_id") REFERENCES "etiquette"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__etiquette__etablissement" ADD CONSTRAINT "evenement__etiquette__etablissement_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__etiquette__etablissement" ADD CONSTRAINT "evenement__etiquette__etablissement_etiquette_id_etiquette_id_fk" FOREIGN KEY ("etiquette_id") REFERENCES "etiquette"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__etiquette__etablissement" ADD CONSTRAINT "evenement__etiquette__etablissement_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__etiquette__etablissement" ADD CONSTRAINT "evenement__etiquette__etablissement_etablissement_id_etablissement_id_fk" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__etiquette__etablissement_creation" ADD CONSTRAINT "evenement__etiquette__etablissement_creation_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__etiquette__etablissement_creation" ADD CONSTRAINT "evenement__etiquette__etablissement_creation_etiquette_id_etiquette_id_fk" FOREIGN KEY ("etiquette_id") REFERENCES "etiquette"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__etiquette__etablissement_creation" ADD CONSTRAINT "evenement__etiquette__etablissement_creation_etablissement_creation_id_etablissement_creation_id_fk" FOREIGN KEY ("etablissement_creation_id") REFERENCES "etablissement_creation"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__local" ADD CONSTRAINT "evenement__local_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__local" ADD CONSTRAINT "evenement__local_local_id_local_id_fk" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__personne" ADD CONSTRAINT "evenement__personne_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__personne" ADD CONSTRAINT "evenement__personne_personne_id_personne_id_fk" FOREIGN KEY ("personne_id") REFERENCES "personne"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__proprietaire_etablissement" ADD CONSTRAINT "evenement__proprietaire_etablissement_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__proprietaire_etablissement" ADD CONSTRAINT "evenement__proprietaire_etablissement_local_id_local_id_fk" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__proprietaire_etablissement" ADD CONSTRAINT "evenement__proprietaire_etablissement_etablissement_id_etablissement_id_fk" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__proprietaire_personne" ADD CONSTRAINT "evenement__proprietaire_personne_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__proprietaire_personne" ADD CONSTRAINT "evenement__proprietaire_personne_local_id_proprietaire_personne_local_id_fk" FOREIGN KEY ("local_id") REFERENCES "proprietaire_personne"("local_id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__proprietaire_personne" ADD CONSTRAINT "evenement__proprietaire_personne_personne_id_proprietaire_personne_personne_id_fk" FOREIGN KEY ("personne_id") REFERENCES "proprietaire_personne"("personne_id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__rappel" ADD CONSTRAINT "evenement__rappel_evenement_id_evenement_id_fk" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "evenement__rappel" ADD CONSTRAINT "evenement__rappel_rappel_id_rappel_id_fk" FOREIGN KEY ("rappel_id") REFERENCES "rappel"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local" ADD CONSTRAINT "local_local_statut_type_id_local_statut_type_id_fk" FOREIGN KEY ("local_statut_type_id") REFERENCES "local_statut_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local" ADD CONSTRAINT "local_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local_adresse" ADD CONSTRAINT "local_adresse_commune_id_commune_id_fk" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local__etiquette" ADD CONSTRAINT "local__etiquette_local_id_local_id_fk" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local__etiquette" ADD CONSTRAINT "local__etiquette_etiquette_id_etiquette_id_fk" FOREIGN KEY ("etiquette_id") REFERENCES "etiquette"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local__local_type" ADD CONSTRAINT "local__local_type_local_id_local_id_fk" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local__local_type" ADD CONSTRAINT "local__local_type_local_type_id_local_type_id_fk" FOREIGN KEY ("local_type_id") REFERENCES "local_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "proprietaire_etablissement" ADD CONSTRAINT "proprietaire_etablissement_local_id_local_id_fk" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "proprietaire_etablissement" ADD CONSTRAINT "proprietaire_etablissement_etablissement_id_etablissement_id_fk" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "proprietaire_personne" ADD CONSTRAINT "proprietaire_personne_local_id_local_id_fk" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "proprietaire_personne" ADD CONSTRAINT "proprietaire_personne_personne_id_personne_id_fk" FOREIGN KEY ("personne_id") REFERENCES "personne"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "commune_commune_type_id_commune_type_id_fk" FOREIGN KEY ("commune_type_id") REFERENCES "commune_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "commune_metropole_id_metropole_id_fk" FOREIGN KEY ("metropole_id") REFERENCES "metropole"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "commune_epci_id_epci_id_fk" FOREIGN KEY ("epci_id") REFERENCES "epci"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "commune_petr_id_petr_id_fk" FOREIGN KEY ("petr_id") REFERENCES "petr"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "commune_departement_id_departement_id_fk" FOREIGN KEY ("departement_id") REFERENCES "departement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "commune_region_id_region_id_fk" FOREIGN KEY ("region_id") REFERENCES "region"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "commune_zrr_type_id_zrr_type_id_fk" FOREIGN KEY ("zrr_type_id") REFERENCES "zrr_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "departement" ADD CONSTRAINT "departement_region_id_region_id_fk" FOREIGN KEY ("region_id") REFERENCES "region"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "epci" ADD CONSTRAINT "epci_epci_type_id_epci_type_id_fk" FOREIGN KEY ("epci_type_id") REFERENCES "epci_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "epci" ADD CONSTRAINT "epci_petr_id_petr_id_fk" FOREIGN KEY ("petr_id") REFERENCES "petr"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
