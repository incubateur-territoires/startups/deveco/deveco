CREATE TABLE IF NOT EXISTS "source"."source_afr" (
	"commune_id" varchar(5) PRIMARY KEY NOT NULL,
	"classement" varchar DEFAULT true NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "commune_afr" (
	"commune_id" varchar(5) PRIMARY KEY NOT NULL,
	"integral" boolean DEFAULT true NOT NULL
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune_afr" ADD CONSTRAINT "fk_commune_afr__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

ALTER TABLE source.source_afr ADD COLUMN lib_com varchar;

\copy source.source_afr(commune_id, lib_com, classement) from '_sources/anct_commune-afr_2022.csv' delimiter ',' csv header;

ALTER TABLE source.source_afr DROP COLUMN lib_com;

INSERT INTO commune_afr
SELECT
	commune_id,
	classement = 'intégralement'
FROM source.source_afr;
--> statement-breakpoint
