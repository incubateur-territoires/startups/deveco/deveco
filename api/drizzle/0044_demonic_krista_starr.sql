CREATE TABLE IF NOT EXISTS "entreprise_procedure_collective" (
	"entreprise_id" varchar(9) NOT NULL,
	"famille" varchar,
	"nature" varchar,
	"date" date,
	CONSTRAINT "uk_entreprise_procedure_collective" UNIQUE("entreprise_id","famille","nature","date")
);
--> statement-breakpoint
--  DO $$ BEGIN
--   ALTER TABLE "entreprise_procedure_collective" ADD CONSTRAINT "fk_entreprise_procedure_collective__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE no action ON UPDATE no action;
--  EXCEPTION
--   WHEN duplicate_object THEN null;
--  END $$;
--> statement-breakpoint
\copy source.source_bodacc_procedure_collective from '_sources/bodacc_procedure-collective_2024-03-05.csv' delimiter ',' csv header;
--> statement-breakpoint
INSERT into entreprise_procedure_collective (entreprise_id, famille, nature, date)
SELECT
   siren AS etablissement_id,
   famille,
   nature,
   date
FROM
   source.source_bodacc_procedure_collective
ON CONFLICT (entreprise_id, famille, nature, date)
DO NOTHING;
--> statement-breakpoint
