CREATE TABLE IF NOT EXISTS "etablissement_adresse_1_etalab" (
	"etablissement_id" varchar(14) NOT NULL,
	"score" double precision NOT NULL
) INHERITS ("adresse");

ALTER TABLE "etablissement_adresse_1_etalab" ADD CONSTRAINT "fk_etablissement_adresse_1_etalab__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;

ALTER TABLE "etablissement_adresse_1_etalab" ADD CONSTRAINT "fk_etablissement_adresse_1_etalab__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;

ALTER TABLE "etablissement_adresse_1_etalab" ADD CONSTRAINT "uk_etablissement_adresse_1_etalab__etablissement_id" UNIQUE ("etablissement_id");

CREATE INDEX IF NOT EXISTS "idx_etablissement_adresse_1__etablissement_etalab" ON "etablissement_adresse_1_etalab" ("etablissement_id");

ALTER TABLE "local_adresse" ADD CONSTRAINT "fk_local_adresse__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;

ALTER TABLE "personne_adresse" ADD CONSTRAINT "fk_personne_adresse__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;

ALTER TABLE "etablissement_adresse_1" ADD CONSTRAINT "fk_etablissement_adresse_1__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;

--> statement-breakpoint
ALTER TABLE "personne_adresse" ALTER COLUMN "personne_id" SET NOT NULL;--> statement-breakpoint
ALTER TABLE "etablissement_adresse_1" ALTER COLUMN "etablissement_id" SET NOT NULL;--> statement-breakpoint
ALTER TABLE "local_adresse" ALTER COLUMN "local_id" SET NOT NULL;--> statement-breakpoint

ALTER TABLE "source"."source_api_sirene_etablissement" ALTER COLUMN "etat_administratif_etablissement" DROP NOT NULL;--> statement-breakpoint
ALTER TABLE "source"."source_api_sirene_etablissement_periode" ALTER COLUMN "etat_administratif_etablissement" DROP NOT NULL;--> statement-breakpoint
