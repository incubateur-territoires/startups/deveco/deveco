CREATE MATERIALIZED VIEW etablissement_effectif_emm_dernier_vue AS
SELECT DISTINCT ON ("etablissement_id")
	"etablissement_effectif_emm"."etablissement_id",
	"valeur"
FROM "etablissement_effectif_emm"
ORDER BY "etablissement_effectif_emm"."etablissement_id",
  "etablissement_effectif_emm"."annee" DESC,
  "etablissement_effectif_emm"."mois" DESC;

CREATE INDEX idx_etablissement_effectif_emm_dernier_vue ON etablissement_effectif_emm_dernier_vue(etablissement_id);

--> statement-breakpoint
CREATE TABLE IF NOT EXISTS entreprise_exercice (
    entreprise_id varchar(9) NOT NULL REFERENCES entreprise(id),
    cloture_date date NOT NULL,
	chiffre_affaires varchar,

	PRIMARY KEY (entreprise_id, cloture_date)
);

INSERT INTO entreprise_exercice (
	entreprise_id,
	cloture_date,
	chiffre_affaires
)
SELECT DISTINCT
	LEFT(etablissement_id, 9) as entreprise,
	cloture_date,
	chiffre_affaires
FROM etablissement_exercice;

DROP TABLE IF EXISTS "etablissement_exercice";--> statement-breakpoint
