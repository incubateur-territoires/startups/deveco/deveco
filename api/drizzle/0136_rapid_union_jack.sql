CREATE TABLE "source"."api_entreprise__entreprise_participation" (
	"source" varchar(9) NOT NULL,
	"cible" varchar(9) NOT NULL,
	"lien" varchar NOT NULL,
	"mise_a_jour_at" timestamp with time zone DEFAULT now(),
	CONSTRAINT "pk_api_entreprise__entreprise_participation" PRIMARY KEY("source","cible")
);
--> statement-breakpoint
CREATE INDEX "idx_api_entreprise__entreprise_participation__source" ON "source"."api_entreprise__entreprise_participation" USING btree ("source");--> statement-breakpoint
CREATE INDEX "idx_api_entreprise__entreprise_participation__cible" ON "source"."api_entreprise__entreprise_participation" USING btree ("cible");