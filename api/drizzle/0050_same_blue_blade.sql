ALTER INDEX "idx_equipe__etablissement__categorie_juridique_2" RENAME TO "idx_equipe__etablissement__categorie_juridique_2_type";--> statement-breakpoint
ALTER INDEX "idx_equipe__etablissement__categorie_juridique_3" RENAME TO "idx_equipe__etablissement__categorie_juridique_3_type";

ALTER INDEX idx_equipe__etablissement__contact_noms RENAME TO idx_equipe__etablissement__contact_noms__gin;

ALTER INDEX idx_equipe__etablissement__etablissement_noms RENAME TO idx_equipe__etablissement__etablissement_noms__gin;

DO $$
	DECLARE
		e RECORD;
	BEGIN
		FOR e IN
			SELECT id FROM equipe
		LOOP
			EXECUTE 'ALTER TABLE equipe__etablissement_' || e.id || ' RENAME CONSTRAINT equipe__etablissement_' || e.id || '_pkey TO pk_equipe__etablissement_' || e.id || ';
			ALTER INDEX equipe__etablissement_' || e.id || '_categorie_juridique_2_type_id_idx RENAME TO idx_equipe__etablissement_' || e.id || '__categorie_juridique_2_type;
			ALTER INDEX equipe__etablissement_' || e.id || '_categorie_juridique_3_type_id_idx RENAME TO idx_equipe__etablissement_' || e.id || '__categorie_juridique_3_type;
			ALTER INDEX equipe__etablissement_' || e.id || '_commune_id_idx RENAME TO idx_equipe__etablissement_' || e.id || '__commune;
			ALTER INDEX equipe__etablissement_' || e.id || '_contact_noms_idx RENAME TO idx_equipe__etablissement_' || e.id || '__contact_noms__gin;
			ALTER INDEX equipe__etablissement_' || e.id || '_creation_date_idx RENAME TO idx_equipe__etablissement_' || e.id || '__creation_date;
			ALTER INDEX equipe__etablissement_' || e.id || '_entreprise_id_idx RENAME TO idx_equipe__etablissement_' || e.id || '__entreprise;
			ALTER INDEX equipe__etablissement_' || e.id || '_equipe_id_idx RENAME TO idx_equipe__etablissement_' || e.id || '__equipe;
			ALTER INDEX equipe__etablissement_' || e.id || '_etablissement_id_idx RENAME TO idx_equipe__etablissement_' || e.id || '__etablissement;
			ALTER INDEX equipe__etablissement_' || e.id || '_etablissement_id_idx1 RENAME TO idx_equipe__etablissement_' || e.id || '__etablissement__gin;
			ALTER INDEX equipe__etablissement_' || e.id || '_etablissement_noms_idx RENAME TO idx_equipe__etablissement_' || e.id || '__etablissement_noms__gin;
			ALTER INDEX equipe__etablissement_' || e.id || '_fermeture_date_idx RENAME TO idx_equipe__etablissement_' || e.id || '__fermeture_date;
			ALTER INDEX equipe__etablissement_' || e.id || '_naf_type_id_idx RENAME TO idx_equipe__etablissement_' || e.id || '__naf;
			ALTER INDEX equipe__etablissement_' || e.id || '_qpv_id_idx RENAME TO idx_equipe__etablissement_' || e.id || '__qpv;';
			COMMIT;
		END LOOP;
	END;
$$ LANGUAGE plpgsql;
