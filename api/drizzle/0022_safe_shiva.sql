CREATE TABLE IF NOT EXISTS "source"."source_anct_commune_territoire_industrie" (
	"insee_com" varchar(5) PRIMARY KEY NOT NULL,
	"lib_com" varchar,
	"id_ti" varchar(7),
	"lib_ti" varchar
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "commune__territoire_industrie" (
	"commune_id" varchar(5) NOT NULL,
	"territoire_industrie_id" varchar(7) NOT NULL,
	CONSTRAINT commune__territoire_industrie_commune_id_territoire_industrie_id PRIMARY KEY("commune_id","territoire_industrie_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "territoire_industrie" (
	"id" varchar(7) PRIMARY KEY NOT NULL,
	"nom" varchar NOT NULL
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune__territoire_industrie" ADD CONSTRAINT "fk_commune__territoire_industrie__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune__territoire_industrie" ADD CONSTRAINT "fk_commune__territoire_industrie__territoire" FOREIGN KEY ("territoire_industrie_id") REFERENCES "territoire_industrie"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
