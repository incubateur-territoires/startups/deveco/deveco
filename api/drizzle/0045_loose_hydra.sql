ALTER TABLE "commune__qpv" DROP CONSTRAINT "fk_commune__qpv__commune"; --> statement-breakpoint
ALTER TABLE "contact_adresse" DROP CONSTRAINT "fk_personne_adresse__commune"; --> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP CONSTRAINT "fk_equipe__etablissement__entreprise_type"; --> statement-breakpoint
ALTER TABLE "equipe__local_contribution" DROP CONSTRAINT "fk_equipe__local_contribution__statut_type"; --> statement-breakpoint
ALTER TABLE "equipe__local_contribution__equipe_local_contribution_type" DROP CONSTRAINT "fk_elc__elct__elct"; --> statement-breakpoint
ALTER TABLE "demande" DROP CONSTRAINT "fk_demande__demande_type"; --> statement-breakpoint
ALTER TABLE "echange" DROP CONSTRAINT "fk_echange__echange_type"; --> statement-breakpoint
ALTER TABLE "equipe" DROP CONSTRAINT "fk_equipe__territoire_type"; --> statement-breakpoint
ALTER TABLE "equipe__commune" DROP CONSTRAINT "fk_equipe__commune__commune"; --> statement-breakpoint
ALTER TABLE "equipe__departement" DROP CONSTRAINT "fk_equipe__departement__departement"; --> statement-breakpoint
ALTER TABLE "equipe__epci" DROP CONSTRAINT "fk_equipe__epci__epci"; --> statement-breakpoint
ALTER TABLE "equipe__metropole" DROP CONSTRAINT "fk_equipe__metropole__metropole"; --> statement-breakpoint
ALTER TABLE "equipe__petr" DROP CONSTRAINT "fk_equipe__petr__petr"; --> statement-breakpoint
ALTER TABLE "equipe__region" DROP CONSTRAINT "fk_equipe__region__region"; --> statement-breakpoint
ALTER TABLE "etiquette" DROP CONSTRAINT "fk_etiquette__etiquette_type"; --> statement-breakpoint
ALTER TABLE "rappel" DROP CONSTRAINT "fk_rappel__affecte_compte"; --> statement-breakpoint
ALTER TABLE "etablissement_creation" DROP CONSTRAINT "fk_etablissement_creation__commune"; --> statement-breakpoint
ALTER TABLE "etablissement_creation_transformation" DROP CONSTRAINT "fk_etablissement_creation_transfo__etablissement_creation"; --> statement-breakpoint
ALTER TABLE "categorie_juridique_type" DROP CONSTRAINT "fk_categorie_juridique_type__parent_categorie_juridique_type"; --> statement-breakpoint
ALTER TABLE "entreprise" DROP CONSTRAINT "fk_entreprise__entreprise_type"; --> statement-breakpoint
ALTER TABLE "entreprise_exercice" DROP CONSTRAINT "fk_entreprise_exercice__entreprise"; --> statement-breakpoint
ALTER TABLE "entreprise_periode" DROP CONSTRAINT "fk_entreprise_periode__entreprise"; --> statement-breakpoint
ALTER TABLE "etablissement" DROP CONSTRAINT "fk_etablissement__effectif_type"; --> statement-breakpoint
ALTER TABLE "etablissement_adresse_1" DROP CONSTRAINT "fk_etablissement_adresse_1__commune"; --> statement-breakpoint
ALTER TABLE "etablissement_adresse_1_etalab" DROP CONSTRAINT "fk_etablissement_adresse_1_etalab__commune"; --> statement-breakpoint
ALTER TABLE "etablissement_periode" DROP CONSTRAINT "fk_etablissement_periode__etablissement"; --> statement-breakpoint
ALTER TABLE "mandataire_personne_morale" DROP CONSTRAINT "fk_mandataire_personne_morale__entreprise"; --> statement-breakpoint
ALTER TABLE "mandataire_personne_physique" DROP CONSTRAINT "fk_mandataire_personne_physique__entreprise"; --> statement-breakpoint
ALTER TABLE "naf_type" DROP CONSTRAINT "fk_naf_type__parent_naf_type"; --> statement-breakpoint
ALTER TABLE "personne_morale" DROP CONSTRAINT "fk_personne_morale__entreprise"; --> statement-breakpoint
ALTER TABLE "personne_physique" DROP CONSTRAINT "fk_personne_physique__entreprise"; --> statement-breakpoint
ALTER TABLE "action_compte" DROP CONSTRAINT "fk_action_compte__compte"; --> statement-breakpoint
ALTER TABLE "action_contact" DROP CONSTRAINT "fk_action_contact__action_type"; --> statement-breakpoint
ALTER TABLE "action_demande" DROP CONSTRAINT "fk_action_demande__action_type"; --> statement-breakpoint
ALTER TABLE "action_echange" DROP CONSTRAINT "fk_action_echange__action_type"; --> statement-breakpoint
ALTER TABLE "action_entreprise" DROP CONSTRAINT "fk_action_entreprise__action_type"; --> statement-breakpoint
ALTER TABLE "action_equipe" DROP CONSTRAINT "fk_action_equipe__action_type"; --> statement-breakpoint
ALTER TABLE "action_equipe_etablissement" DROP CONSTRAINT "fk_action_equipe_etablissement__action_type"; --> statement-breakpoint
ALTER TABLE "action_equipe_etablissement_contact" DROP CONSTRAINT "fk_action_equipe_etablissement_contact__action_type"; --> statement-breakpoint
ALTER TABLE "action_equipe_local" DROP CONSTRAINT "fk_action_equipe_local__action_type"; --> statement-breakpoint
ALTER TABLE "action_equipe_local_contact" DROP CONSTRAINT "fk_action_equipe_local_contact__action_type"; --> statement-breakpoint
ALTER TABLE "action_etablissement" DROP CONSTRAINT "fk_action_etablissement__action_type"; --> statement-breakpoint
ALTER TABLE "action_etablissement_creation" DROP CONSTRAINT "fk_action_etablissement_creation__action_type"; --> statement-breakpoint
ALTER TABLE "action_etiquette" DROP CONSTRAINT "fk_action_etiquette__action_type"; --> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement" DROP CONSTRAINT "fk_action_etiquette_etablissement__action_type"; --> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement_creation" DROP CONSTRAINT "fk_action_etiquette_etablissement_creation__action_type"; --> statement-breakpoint
ALTER TABLE "action_rappel" DROP CONSTRAINT "fk_action_rappel__action_type"; --> statement-breakpoint
ALTER TABLE "evenement" DROP CONSTRAINT "fk_evenement_compte"; --> statement-breakpoint
ALTER TABLE "local" DROP CONSTRAINT "local_commune_id_commune_id_fk"; --> statement-breakpoint
ALTER TABLE "local" DROP CONSTRAINT "fk_local__categorie_type"; --> statement-breakpoint
ALTER TABLE "local" DROP CONSTRAINT "fk_local__nature_type"; --> statement-breakpoint
ALTER TABLE "local_adresse" DROP CONSTRAINT "fk_local_adresse_adresse__commune"; --> statement-breakpoint
ALTER TABLE "local__proprietaire_personne_morale" DROP CONSTRAINT "fk_local__proprietaire_personne_morale__local"; --> statement-breakpoint
ALTER TABLE "local__proprietaire_personne_physique" DROP CONSTRAINT "fk_local__proprietaire_personne_physique__ppp"; --> statement-breakpoint
ALTER TABLE "commune" DROP CONSTRAINT "fk_commune__commune_type"; --> statement-breakpoint
ALTER TABLE "commune" DROP CONSTRAINT "fk_commune__metropole"; --> statement-breakpoint
ALTER TABLE "commune" DROP CONSTRAINT "fk_commune__epci"; --> statement-breakpoint
ALTER TABLE "commune" DROP CONSTRAINT "fk_commune__petr"; --> statement-breakpoint
ALTER TABLE "commune" DROP CONSTRAINT "fk_commune__departement"; --> statement-breakpoint
ALTER TABLE "commune" DROP CONSTRAINT "fk_commune__region"; --> statement-breakpoint
ALTER TABLE "commune" DROP CONSTRAINT "fk_commune__zrr_type"; --> statement-breakpoint
ALTER TABLE "commune__territoire_industrie" DROP CONSTRAINT "fk_commune__territoire_industrie__territoire"; --> statement-breakpoint
ALTER TABLE "departement" DROP CONSTRAINT "fk_departement__region"; --> statement-breakpoint
ALTER TABLE "epci" DROP CONSTRAINT "fk_epci__epci_type"; --> statement-breakpoint
ALTER TABLE "epci" DROP CONSTRAINT "fk_epci__petr"; --> statement-breakpoint
ALTER TABLE "commune__territoire_industrie" RENAME CONSTRAINT "commune__territoire_industrie_commune_id_territoire_industrie_i" TO "pk_commune__territoire_industrie";--> statement-breakpoint
ALTER TABLE "commune" RENAME CONSTRAINT "commune_pkey" TO "pk_commune";--> statement-breakpoint
ALTER TABLE "commune_acv" RENAME CONSTRAINT "commune_acv_pkey" TO "pk_commune_acv";--> statement-breakpoint
ALTER TABLE "commune_afr" RENAME CONSTRAINT "commune_afr_pkey" TO "pk_commune_afr";--> statement-breakpoint
ALTER TABLE "commune_pvd" RENAME CONSTRAINT "commune_pvd_pkey" TO "pk_commune_pvd";--> statement-breakpoint
ALTER TABLE "commune_type" RENAME CONSTRAINT "commune_type_pkey" TO "pk_commune_type";--> statement-breakpoint
ALTER TABLE "commune_va" RENAME CONSTRAINT "commune_va_pkey" TO "pk_commune_va";--> statement-breakpoint
ALTER TABLE "departement" RENAME CONSTRAINT "departement_pkey" TO "pk_departement";--> statement-breakpoint
ALTER TABLE "epci" RENAME CONSTRAINT "epci_pkey" TO "pk_epci";--> statement-breakpoint
ALTER TABLE "epci_type" RENAME CONSTRAINT "epci_type_pkey" TO "pk_epci_type";--> statement-breakpoint
ALTER TABLE "metropole" RENAME CONSTRAINT "metropole_pkey" TO "pk_metropole";--> statement-breakpoint
ALTER TABLE "petr" RENAME CONSTRAINT "petr_pkey" TO "pk_petr";--> statement-breakpoint
ALTER TABLE "region" RENAME CONSTRAINT "region_pkey" TO "pk_region";--> statement-breakpoint
ALTER TABLE "territoire_industrie" RENAME CONSTRAINT "territoire_industrie_pkey" TO "pk_territoire_industrie";--> statement-breakpoint
ALTER TABLE "zrr_type" RENAME CONSTRAINT "zrr_type_pkey" TO "pk_zrr_type";--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune__qpv" ADD CONSTRAINT "fk_commune__qpv__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "contact_adresse" ADD CONSTRAINT "fk_contact_adresse__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "equipe__local_contribution" ADD CONSTRAINT "fk_equipe__local_contribution__statut_type" FOREIGN KEY ("equipe_local_contribution_statut_type_id") REFERENCES "equipe_local_contribution_statut_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "equipe__local_contribution__equipe_local_contribution_type" ADD CONSTRAINT "fk_elc__elct__elct" FOREIGN KEY ("equipe_local_contribution_type_id") REFERENCES "equipe_local_contribution_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "demande" ADD CONSTRAINT "fk_demande__demande_type" FOREIGN KEY ("demande_type_id") REFERENCES "demande_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "echange" ADD CONSTRAINT "fk_echange__echange_type" FOREIGN KEY ("echange_type_id") REFERENCES "echange_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "equipe" ADD CONSTRAINT "fk_equipe__territoire_type" FOREIGN KEY ("territoire_type_id") REFERENCES "territoire_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "equipe__commune" ADD CONSTRAINT "fk_equipe__commune__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "equipe__departement" ADD CONSTRAINT "fk_equipe__departement__departement" FOREIGN KEY ("departement_id") REFERENCES "departement"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "equipe__epci" ADD CONSTRAINT "fk_equipe__epci__epci" FOREIGN KEY ("epci_id") REFERENCES "epci"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "equipe__metropole" ADD CONSTRAINT "fk_equipe__metropole__metropole" FOREIGN KEY ("metropole_id") REFERENCES "metropole"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "equipe__petr" ADD CONSTRAINT "fk_equipe__petr__petr" FOREIGN KEY ("petr_id") REFERENCES "petr"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "equipe__region" ADD CONSTRAINT "fk_equipe__region__region" FOREIGN KEY ("region_id") REFERENCES "region"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "etiquette" ADD CONSTRAINT "fk_etiquette__etiquette_type" FOREIGN KEY ("etiquette_type_id") REFERENCES "etiquette_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "rappel" ADD CONSTRAINT "fk_rappel__affecte_compte" FOREIGN KEY ("affecte_compte_id") REFERENCES "compte"("id") ON DELETE set null ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "etablissement_creation" ADD CONSTRAINT "fk_etablissement_creation__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "etablissement_creation_transformation" ADD CONSTRAINT "fk_etablissement_creation_transfo__etablissement_creation" FOREIGN KEY ("etablissement_creation_id") REFERENCES "etablissement_creation"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "categorie_juridique_type" ADD CONSTRAINT "fk_categorie_juridique_type__parent_categorie_juridique_type" FOREIGN KEY ("parent_categorie_juridique_type_id") REFERENCES "categorie_juridique_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "entreprise" ADD CONSTRAINT "fk_entreprise__entreprise_type" FOREIGN KEY ("entreprise_type_id") REFERENCES "entreprise_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "entreprise_exercice" ADD CONSTRAINT "fk_entreprise_exercice__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "entreprise_periode" ADD CONSTRAINT "fk_entreprise_periode__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "etablissement" ADD CONSTRAINT "fk_etablissement__effectif_type" FOREIGN KEY ("effectif_type_id") REFERENCES "effectif_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "etablissement_adresse_1" ADD CONSTRAINT "fk_etablissement_adresse_1__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "etablissement_adresse_1_etalab" ADD CONSTRAINT "fk_etablissement_adresse_1_etalab__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "etablissement_periode" ADD CONSTRAINT "fk_etablissement_periode__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "mandataire_personne_morale" ADD CONSTRAINT "fk_mandataire_personne_morale__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "mandataire_personne_physique" ADD CONSTRAINT "fk_mandataire_personne_physique__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "naf_type" ADD CONSTRAINT "fk_naf_type__parent_naf_type" FOREIGN KEY ("parent_naf_type_id","naf_revision_type_id") REFERENCES "naf_type"("id", "naf_revision_type_id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "personne_morale" ADD CONSTRAINT "fk_personne_morale__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "personne_physique" ADD CONSTRAINT "fk_personne_physique__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_compte" ADD CONSTRAINT "fk_action_compte__compte" FOREIGN KEY ("compte_id") REFERENCES "compte"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_contact" ADD CONSTRAINT "fk_action_contact__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_demande" ADD CONSTRAINT "fk_action_demande__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_echange" ADD CONSTRAINT "fk_action_echange__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_entreprise" ADD CONSTRAINT "fk_action_entreprise__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_equipe" ADD CONSTRAINT "fk_action_equipe__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_equipe_etablissement" ADD CONSTRAINT "fk_action_equipe_etablissement__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_equipe_etablissement_contact" ADD CONSTRAINT "fk_action_equipe_etablissement_contact__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_equipe_local" ADD CONSTRAINT "fk_action_equipe_local__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_equipe_local_contact" ADD CONSTRAINT "fk_action_equipe_local_contact__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_etablissement" ADD CONSTRAINT "fk_action_etablissement__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_etablissement_creation" ADD CONSTRAINT "fk_action_etablissement_creation__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_etiquette" ADD CONSTRAINT "fk_action_etiquette__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_etiquette__etablissement" ADD CONSTRAINT "fk_action_etiquette_etablissement__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_etiquette__etablissement_creation" ADD CONSTRAINT "fk_action_etiquette_etablissement_creation__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "action_rappel" ADD CONSTRAINT "fk_action_rappel__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "evenement" ADD CONSTRAINT "fk_evenement_compte" FOREIGN KEY ("compte_id") REFERENCES "compte"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "local" ADD CONSTRAINT "fk_local__local_categorie_type" FOREIGN KEY ("local_categorie_type_id") REFERENCES "local_categorie_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "local" ADD CONSTRAINT "fk_local__local_nature_type" FOREIGN KEY ("local_nature_type_id") REFERENCES "local_nature_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "local" ADD CONSTRAINT "fk_local__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "local_adresse" ADD CONSTRAINT "fk_local_adresse__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "local__proprietaire_personne_morale" ADD CONSTRAINT "fk_local__proprietaire_personne_morale__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "local__proprietaire_personne_physique" ADD CONSTRAINT "fk_local__proprietaire_personne_physique__ppp" FOREIGN KEY ("proprietaire_personne_physique_id") REFERENCES "proprietaire_personne_physique"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "fk_commune__commune_type" FOREIGN KEY ("commune_type_id") REFERENCES "commune_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "fk_commune__commune_parent" FOREIGN KEY ("commune_parent_id") REFERENCES "commune"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "fk_commune__metropole" FOREIGN KEY ("metropole_id") REFERENCES "metropole"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "fk_commune__epci" FOREIGN KEY ("epci_id") REFERENCES "epci"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "fk_commune__petr" FOREIGN KEY ("petr_id") REFERENCES "petr"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "fk_commune__departement" FOREIGN KEY ("departement_id") REFERENCES "departement"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "fk_commune__region" FOREIGN KEY ("region_id") REFERENCES "region"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "commune" ADD CONSTRAINT "fk_commune__zrr_type" FOREIGN KEY ("zrr_type_id") REFERENCES "zrr_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "commune__territoire_industrie" ADD CONSTRAINT "fk_commune__territoire_industrie__territoire_industrie" FOREIGN KEY ("territoire_industrie_id") REFERENCES "territoire_industrie"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "departement" ADD CONSTRAINT "fk_departement__region" FOREIGN KEY ("region_id") REFERENCES "region"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "epci" ADD CONSTRAINT "fk_epci__epci_type" FOREIGN KEY ("epci_type_id") REFERENCES "epci_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "epci" ADD CONSTRAINT "fk_epci__petr" FOREIGN KEY ("petr_id") REFERENCES "petr"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;--> statement-breakpoint

-- ajouté à la main

ALTER TABLE "adresse" DROP CONSTRAINT "fk_adresse__commune";--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "adresse" ADD CONSTRAINT "fk_adresse__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;--> statement-breakpoint

ALTER TABLE "equipe" DROP CONSTRAINT "fk_equipe__equipe_type";--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe" ADD CONSTRAINT "fk_equipe__equipe_type" FOREIGN KEY ("equipe_type_id") REFERENCES "equipe_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

ALTER TABLE "echange" DROP CONSTRAINT "fk_echange__deveco"; --> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "echange" ADD CONSTRAINT "fk_echange__deveco" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

ALTER TABLE "etablissement_creation" DROP CONSTRAINT "fk_etablissement_creation__entreprise"; --> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation" ADD CONSTRAINT "fk_etablissement_creation__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE set null ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

ALTER TABLE "naf_type" DROP CONSTRAINT "fk_naf_type__naf_revision_type"; --> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "naf_type" ADD CONSTRAINT "fk_naf_type__naf_revision_type" FOREIGN KEY ("naf_revision_type_id") REFERENCES "naf_revision_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

ALTER TABLE "personne_physique" DROP CONSTRAINT "fk_personne_physique__sexe_type"; --> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "personne_physique" ADD CONSTRAINT "fk_personne_physique__sexe_type" FOREIGN KEY ("sexe_type_id") REFERENCES "sexe_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

ALTER TABLE "entreprise" DROP CONSTRAINT "fk_entreprise__entreprise_categorie_type"; --> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "entreprise" ADD CONSTRAINT "fk_entreprise__entreprise_categorie_type" FOREIGN KEY ("entreprise_categorie_type_id") REFERENCES "entreprise_categorie_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

ALTER TABLE "entreprise_periode" DROP CONSTRAINT "fk_entreprise_periode__categorie_juridique_type"; --> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "entreprise_periode" ADD CONSTRAINT "fk_entreprise_periode__categorie_juridique_type" FOREIGN KEY ("categorie_juridique_type_id") REFERENCES "categorie_juridique_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

ALTER TABLE "action_compte" DROP CONSTRAINT "fk_action_compte__action_type"; --> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_compte" ADD CONSTRAINT "fk_action_compte__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

ALTER TABLE "local__proprietaire_personne_morale" DROP CONSTRAINT "fk_local__proprietaire_personne_morale__proprietaire_personne_m"; --> statement-breakpoint

--

ALTER TABLE "equipe__etablissement" DROP CONSTRAINT "fk_equipe__etablissement__entreprise_categorie_type";--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement" ADD CONSTRAINT "fk_equipe__etablissement__entreprise_categorie_type" FOREIGN KEY ("entreprise_categorie_type_id") REFERENCES "entreprise_categorie_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

ALTER TABLE "equipe__etablissement" DROP CONSTRAINT "fk_equipe__etablissement__qpv";--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement" ADD CONSTRAINT "fk_equipe__etablissement__qpv" FOREIGN KEY ("qpv_id") REFERENCES "qpv"("id") ON DELETE set null ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

ALTER TABLE "equipe__etablissement" DROP CONSTRAINT "fk_equipe__etablissement__commune";--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement" ADD CONSTRAINT "fk_equipe__etablissement__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

ALTER TABLE "equipe__etablissement" DROP CONSTRAINT "fk_equipe__etablissement__effectif_type";--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement" ADD CONSTRAINT "fk_equipe__etablissement__effectif_type" FOREIGN KEY ("effectif_type_id") REFERENCES "effectif_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "equipe__etablissement" ADD CONSTRAINT "fk_equipe__etablissement__entreprise_type" FOREIGN KEY ("entreprise_type_id") REFERENCES "entreprise_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$; --> statement-breakpoint
