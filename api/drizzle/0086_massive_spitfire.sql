CREATE TABLE IF NOT EXISTS "local_mutation" (
	"id" integer NOT NULL,
	"local_mutation_nature_type_id" integer NOT NULL,
	"date" date NOT NULL,
	"valeur" double precision,
	CONSTRAINT "pk_local_mutation" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "local__local_mutation" (
	"local_id" varchar(12) NOT NULL,
	"local_mutation_id" integer NOT NULL,
	CONSTRAINT "pk_local__local_mutation" PRIMARY KEY("local_id","local_mutation_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "local_mutation_nature_type" (
	"id" integer NOT NULL,
	"nom" varchar NOT NULL,
	CONSTRAINT "pk_local_mutation_nature_type" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."cerema__local_mutation" (
	"idmutation" integer,
	"idnatmut" integer,
	"datemut" date,
	"valeurfonc" double precision,
	CONSTRAINT "pk_cerema__local_mutation" PRIMARY KEY("idmutation")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."cerema__local__local_mutation" (
	"idloc" varchar(12),
	"idmutation" integer,
	CONSTRAINT "pk_cerema__local__local_mutation" PRIMARY KEY("idloc","idmutation")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."cerema__local_mutation_nature_type" (
	"id" integer NOT NULL,
	"nom" varchar,
	CONSTRAINT "pk_cerema__local_mutation_nature_type" PRIMARY KEY("id")
);
--> statement-breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_morale" ADD CONSTRAINT "pk_cerema__local_poprietaire_morale" PRIMARY KEY("idprocpte","dnulp");--> statement-breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_physique" ADD CONSTRAINT "pk_cerema__local_poprietaire_physique" PRIMARY KEY("idprocpte","dnulp");--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local_mutation" ADD CONSTRAINT "fk_local_mutation_nature_type" FOREIGN KEY ("local_mutation_nature_type_id") REFERENCES "local_mutation_nature_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local__local_mutation" ADD CONSTRAINT "fk_local__local_mutation__mutation" FOREIGN KEY ("local_mutation_id") REFERENCES "local_mutation"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

--> statement-breakpoint
\copy source.cerema__local_mutation_nature_type (id, nom) FROM '_sources/cerema_local_mutation_nature_2023.csv' DELIMITER ',' CSV HEADER;
--> statement-breakpoint
\copy source.cerema__local_mutation (idmutation, idnatmut, datemut, valeurfonc) FROM '_sources/cerema_local_mutation_fixtures_2023.csv' DELIMITER ',' CSV HEADER;
--> statement-breakpoint
\copy source.cerema__local__local_mutation (idmutation, idloc) FROM '_sources/cerema_local__local_mutation_fixtures_2023.csv' DELIMITER ',' CSV HEADER;
--> statement-breakpoint
INSERT INTO local_mutation_nature_type (id, nom)
SELECT id, nom
FROM source.cerema__local_mutation_nature_type;
--> statement-breakpoint
INSERT INTO local_mutation (
	id,
	local_mutation_nature_type_id,
	date,
	valeur
)
SELECT
	idmutation,
	idnatmut,
	datemut,
	valeurfonc
FROM source.cerema__local_mutation;
--> statement-breakpoint
INSERT INTO local__local_mutation (
	local_id,
	local_mutation_id
)
SELECT
	idloc,
	idmutation
FROM source.cerema__local__local_mutation;
