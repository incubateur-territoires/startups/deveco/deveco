CREATE TABLE IF NOT EXISTS "refresh_token" (
	"id" serial NOT NULL,
	"compte_id" integer NOT NULL,
	"token" uuid NOT NULL,
	"created_at" timestamp DEFAULT now() NOT NULL,
	"creator_user_agent" varchar NOT NULL,
	"last_accessed_at" timestamp,
	"last_accessed_user_agent" varchar,
	"jwt_expired" varchar,
	"jwt_refreshed" varchar,
	CONSTRAINT "pk_refresh_token" PRIMARY KEY("id")
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "refresh_token" ADD CONSTRAINT "fk_refresh_token__compte" FOREIGN KEY ("compte_id") REFERENCES "compte"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
