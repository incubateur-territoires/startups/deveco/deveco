CREATE TABLE IF NOT EXISTS
  "equipe__etablissement_ressource" (
    "id" serial NOT NULL,
    "equipe_id" integer NOT NULL,
    "etablissement_id" varchar(14) NOT NULL,
    "nom" varchar,
    "lien" varchar,
    "ressource_type_id" varchar,
    CONSTRAINT "pk_equipe__etablissement_ressource" PRIMARY KEY ("id")
  );

--> statement-breakpoint
CREATE TABLE IF NOT EXISTS
  "ressource_type" (
    "id" varchar NOT NULL,
    "nom" varchar NOT NULL,
    CONSTRAINT "pk_ressource_type" PRIMARY KEY ("id")
  );

INSERT INTO
  "ressource_type"
VALUES
  ('website', 'Site web'),
  ('article', 'Article de presse'),
  ('social', 'Réseaux sociaux'),
  ('url', 'URL'),
  ('autre', 'Autre');

--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement_ressource" ADD CONSTRAINT "fk_equipe__etablissement_ressource__ressource_type" FOREIGN KEY ("ressource_type_id") REFERENCES "ressource_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
