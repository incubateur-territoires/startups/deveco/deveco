ALTER TABLE "local_adresse" ADD CONSTRAINT "uk_local_adresse" UNIQUE("local_id");-->statement breakpoint
ALTER TABLE "source"."cerema__local" ALTER COLUMN "invar" SET NOT NULL;-->statement breakpoint
ALTER TABLE "source"."cerema__local" ALTER COLUMN "idcom" SET NOT NULL;-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_morale" ALTER COLUMN "idprocpte" SET NOT NULL;-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_physique" ALTER COLUMN "idprocpte" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "source"."cerema__local" ADD COLUMN "annee" integer;-->statement breakpoint
UPDATE "source"."cerema__local" SET "annee" = 2022;-->statement breakpoint
ALTER TABLE "source"."cerema__local" ALTER COLUMN "annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_morale" ADD COLUMN "annee" integer;-->statement breakpoint
UPDATE "source"."cerema__local_proprietaire_morale" SET "annee" = 2022;-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_morale" ALTER COLUMN "annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_physique" ADD COLUMN "annee" integer;-->statement breakpoint
UPDATE "source"."cerema__local_proprietaire_physique" SET "annee" = 2022;-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_physique" ALTER COLUMN "annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "source"."cerema__local" ADD CONSTRAINT "pk_cerema__local" PRIMARY KEY("invar","idcom","annee");-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_morale" DROP CONSTRAINT "pk_cerema__local_poprietaire_morale";-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_morale" ADD CONSTRAINT "pk_cerema__local_proprietaire_morale" PRIMARY KEY("idprocpte","dnulp","annee");-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_physique" DROP CONSTRAINT "pk_cerema__local_poprietaire_physique";-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_physique" ADD CONSTRAINT "pk_cerema__local_proprietaire_physique" PRIMARY KEY("idprocpte","dnulp","annee");-->statement breakpoint
-->statement breakpoint
ALTER TABLE "local" ADD COLUMN "debut_annee" varchar;-->statement breakpoint
UPDATE "local" SET "debut_annee" = 2022;-->statement breakpoint
ALTER TABLE "local" ALTER COLUMN "debut_annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "local" ADD COLUMN "annee" integer;-->statement breakpoint
UPDATE "local" SET "annee" = 2022;-->statement breakpoint
ALTER TABLE "local" ALTER COLUMN "annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "local" ADD COLUMN "actif" boolean;-->statement breakpoint
UPDATE "local" SET "actif" = false;-->statement breakpoint
ALTER TABLE "local" ALTER COLUMN "actif" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "proprietaire_personne_morale" ADD COLUMN "debut_annee" integer;-->statement breakpoint
UPDATE "proprietaire_personne_morale" SET "debut_annee" = 2022;-->statement breakpoint
ALTER TABLE "proprietaire_personne_morale" ALTER COLUMN "debut_annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "proprietaire_personne_morale" ADD COLUMN "annee" integer;-->statement breakpoint
UPDATE "proprietaire_personne_morale" SET "annee" = 2022;-->statement breakpoint
ALTER TABLE "proprietaire_personne_morale" ALTER COLUMN "annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "proprietaire_personne_physique" ADD COLUMN "debut_annee" integer;-->statement breakpoint
UPDATE "proprietaire_personne_physique" SET "debut_annee" = 2022;-->statement breakpoint
ALTER TABLE "proprietaire_personne_physique" ALTER COLUMN "debut_annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "proprietaire_personne_physique" ADD COLUMN "annee" integer;-->statement breakpoint
UPDATE "proprietaire_personne_physique" SET "annee" = 2022;-->statement breakpoint
ALTER TABLE "proprietaire_personne_physique" ALTER COLUMN "annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
-->statement breakpoint
\copy source.cerema__local(invar, dnvoiri, dindic, dvoilib, idcom, geomloc, ccosec, dnupla, cconlc, typeact, dniv, sprincp, ssecp, ssecncp, sparkp, sparkncp, idprocpte, annee) FROM '_sources/cerema_local_fixtures_2023.csv' DELIMITER ',' CSV HEADER;
-->statement breakpoint
\copy source.cerema__local_proprietaire_morale(idprocpte, dnulp, dsiren, ddenom, annee) FROM '_sources/cerema_local_proprietaire_morale_2023.csv' DELIMITER ',' CSV HEADER;
-->statement breakpoint
\copy source.cerema__local_proprietaire_physique(idprocpte, dnulp, dnomus, dprnus, dnomlp, jdatnss, dldnss, annee) FROM '_sources/cerema_local_proprietaire_physique_2023.csv' DELIMITER ',' CSV HEADER;
-->statement breakpoint
INSERT INTO local (
	id,
	invariant,
	local_nature_type_id,
	local_categorie_type_id,
	commune_id,
	section,
	parcelle,
	niveau,
	surface_vente,
	surface_reserve,
	surface_exterieure_non_couverte,
	surface_stationnement_couvert,
	surface_stationnement_non_couvert,
	debut_annee,
	annee,
	actif
)
SELECT
	LEFT(idcom, 2) || invar,
	invar,
	cconlc,
	typeact,
	idcom,
	ccosec,
	dnupla,
	dniv,
	sprincp,
	ssecp,
	ssecncp,
	sparkp,
	sparkncp,
	annee as debut_annee,
	annee,
	true as actif
FROM source.cerema__local
WHERE annee = 2023
ON CONFLICT (id) DO UPDATE SET
	local_nature_type_id = excluded.local_nature_type_id,
	local_categorie_type_id = excluded.local_categorie_type_id,
	commune_id = excluded.commune_id,
	section = excluded.section,
	parcelle = excluded.parcelle,
	niveau = excluded.niveau,
	surface_vente = excluded.surface_vente,
	surface_reserve = excluded.surface_reserve,
	surface_exterieure_non_couverte = excluded.surface_exterieure_non_couverte,
	surface_stationnement_couvert = excluded.surface_stationnement_couvert,
	surface_stationnement_non_couvert = excluded.surface_stationnement_non_couvert,
	-- pas de mise à jour de debut_annee, car on veut garder l'ancienne
	annee = excluded.annee,
	actif = true;
-->statement breakpoint
INSERT INTO local_adresse (
	local_id,
	commune_id,
	voie_nom,
	numero,
	geolocalisation
)
SELECT
	LEFT(idcom, 2) || invar,
	idcom,
	replace(dvoilib, '  ', ' '),
	COALESCE(regexp_replace(dnvoiri, '^0*', '', 'g'), '') || COALESCE(dindic, ''),
	ST_Transform(ST_SetSRID(geomloc::geometry,2154),4326)
FROM source.cerema__local
WHERE annee = 2023
ON CONFLICT (local_id) DO UPDATE SET
	commune_id = excluded.commune_id,
	voie_nom = excluded.voie_nom,
	numero = excluded.numero,
	geolocalisation = COALESCE(excluded.geolocalisation, local_adresse.geolocalisation);
-->statement breakpoint
INSERT INTO proprietaire_personne_physique (
	id,
	proprietaire_compte_id,
	nom,
	prenom,
	naissance_nom,
	naissance_date,
	naissance_lieu,
	debut_annee,
	annee
)
SELECT
	idprocpte || dnulp,
	idprocpte,
	dnomus,
	dprnus,
	dnomlp,
	TO_DATE(jdatnss, 'DD/MM/YYYY'),
	dldnss,
	annee as debut_annee,
	annee
FROM source.cerema__local_proprietaire_physique
WHERE annee = 2023
ON CONFLICT (id) DO UPDATE SET
	proprietaire_compte_id = excluded.proprietaire_compte_id,
	nom = excluded.nom,
	prenom = excluded.prenom,
	naissance_nom = excluded.naissance_nom,
	naissance_date = excluded.naissance_date,
	naissance_lieu = excluded.naissance_lieu,
	annee = excluded.annee;
-->statement breakpoint
INSERT INTO proprietaire_personne_morale (
	id,
	proprietaire_compte_id,
	entreprise_id,
	nom,
	debut_annee,
	annee
)
SELECT
	idprocpte || dnulp,
	idprocpte,
	dsiren,
	ddenom,
	annee as debut_annee,
	annee
FROM source.cerema__local_proprietaire_morale
WHERE annee = 2023
ON CONFLICT (id) DO UPDATE SET
	proprietaire_compte_id = excluded.proprietaire_compte_id,
	entreprise_id = excluded.entreprise_id,
	nom = excluded.nom,
	annee = excluded.annee;
-->statement breakpoint
DELETE FROM local__proprietaire_personne_physique
-- on supprime les liens des locaux qui existent en 2023
WHERE local_id IN (
	SELECT id
	FROM local
	WHERE annee = 2023
);
-->statement breakpoint
INSERT INTO local__proprietaire_personne_physique (
	local_id,
	proprietaire_personne_physique_id
)
SELECT DISTINCT
	LEFT(l.idcom, 2) || l.invar,
	idprocpte || dnulp
FROM source.cerema__local_proprietaire_physique pp
JOIN source.cerema__local l USING(idprocpte)
WHERE pp.annee = 2023 AND l.annee = 2023 ON
CONFLICT (local_id, proprietaire_personne_physique_id) DO NOTHING;
-->statement breakpoint
DELETE FROM local__proprietaire_personne_morale
-- on supprime les liens des locaux qui existent en 2023
WHERE local_id IN (
	SELECT id
	FROM local
	WHERE annee = 2023
);
-->statement breakpoint
INSERT INTO local__proprietaire_personne_morale (
	local_id,
	proprietaire_personne_morale_id
)
SELECT
	DISTINCT LEFT(l.idcom, 2) || l.invar,
	pm.idprocpte || pm.dnulp
FROM source.cerema__local_proprietaire_morale pm
JOIN source.cerema__local l USING(idprocpte)
WHERE pm.annee = 2023 AND l.annee = 2023 ON
CONFLICT (local_id, proprietaire_personne_morale_id) DO NOTHING;
