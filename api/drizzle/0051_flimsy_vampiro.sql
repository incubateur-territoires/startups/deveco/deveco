CREATE TABLE IF NOT EXISTS "ayant_droit" (
	"id" serial NOT NULL,
	"nom" varchar,
	"prenom" varchar,
	"email" varchar NOT NULL,
	"fonction" varchar NOT NULL,
	CONSTRAINT "pk_ayant_droit" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "compte_demande" (
	"id" serial NOT NULL,
	"equipe_demande_id" integer NOT NULL,
	"nom" varchar NOT NULL,
	"prenom" varchar NOT NULL,
	"fonction" varchar NOT NULL,
	"email" varchar NOT NULL,
	CONSTRAINT "pk_compte_demande" PRIMARY KEY("id"),
	CONSTRAINT "uk_compte_demande__equipe_demande" UNIQUE("equipe_demande_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe_demande" (
	"id" serial NOT NULL,
	"nom" varchar NOT NULL,
	"equipe_id" integer,
	"compte_id" integer NOT NULL,
	"created_at" timestamp with time zone DEFAULT now() NOT NULL,
	"transformed_at" timestamp with time zone,
	CONSTRAINT "pk_equipe_demande" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe_demande_autorisation" (
	"id" serial NOT NULL,
	"token" uuid NOT NULL,
	"equipe_demande_id" integer NOT NULL,
	"ayant_droit_id" integer NOT NULL,
	"commune_id" varchar(5),
	"epci_id" varchar(9),
	"petr_id" varchar(4),
	"departement_id" varchar(3),
	"region_id" varchar(2),
	"created_at" timestamp with time zone DEFAULT now() NOT NULL,
	"accepted_at" timestamp with time zone,
	CONSTRAINT "pk_equipe_demande_autorisation" PRIMARY KEY("id"),
	CONSTRAINT "uk_equipe_demande_autorisation__ayant_droit" UNIQUE("ayant_droit_id")
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "compte_demande" ADD CONSTRAINT "fk_compte_demande__equipe_demande" FOREIGN KEY ("equipe_demande_id") REFERENCES "equipe_demande"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_demande" ADD CONSTRAINT "fk_equipe_demande__compte" FOREIGN KEY ("compte_id") REFERENCES "compte"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_demande" ADD CONSTRAINT "fk_equipe_demande__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_demande_autorisation" ADD CONSTRAINT "fk_equipe_demande_autorisation__equipe_demande" FOREIGN KEY ("equipe_demande_id") REFERENCES "equipe_demande"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_demande_autorisation" ADD CONSTRAINT "fk_equipe_demande_autorisation__ayant_droit" FOREIGN KEY ("ayant_droit_id") REFERENCES "ayant_droit"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_demande_autorisation" ADD CONSTRAINT "fk_equipe_demande_autorisation__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_demande_autorisation" ADD CONSTRAINT "fk_equipe_demande_autorisation__epci" FOREIGN KEY ("epci_id") REFERENCES "epci"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_demande_autorisation" ADD CONSTRAINT "fk_equipe_demande_autorisation__petr" FOREIGN KEY ("petr_id") REFERENCES "petr"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_demande_autorisation" ADD CONSTRAINT "fk_equipe_demande_autorisation__departement" FOREIGN KEY ("departement_id") REFERENCES "departement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_demande_autorisation" ADD CONSTRAINT "fk_equipe_demande_autorisation__region" FOREIGN KEY ("region_id") REFERENCES "region"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
