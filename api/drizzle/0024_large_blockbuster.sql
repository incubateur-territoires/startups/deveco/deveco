ALTER TABLE "evenement__compte" RENAME TO "action_compte";--> statement-breakpoint
ALTER TABLE "evenement__contact" RENAME TO "action_contact";--> statement-breakpoint
ALTER TABLE "evenement__demande" RENAME TO "action_demande";--> statement-breakpoint
ALTER TABLE "evenement__echange" RENAME TO "action_echange";--> statement-breakpoint
ALTER TABLE "evenement__entreprise" RENAME TO "action_entreprise";--> statement-breakpoint
ALTER TABLE "evenement__equipe_etablissement" RENAME TO "action_equipe_etablissement";--> statement-breakpoint
ALTER TABLE "evenement__etablissement" RENAME TO "action_etablissement";--> statement-breakpoint
ALTER TABLE "evenement__etablissement_creation" RENAME TO "action_etablissement_creation";--> statement-breakpoint
ALTER TABLE "evenement__etiquette" RENAME TO "action_etiquette";--> statement-breakpoint
ALTER TABLE "evenement__etiquette__etablissement" RENAME TO "action_etiquette__etablissement";--> statement-breakpoint
ALTER TABLE "evenement__etiquette__etablissement_creation" RENAME TO "action_etiquette__etablissement_creation";--> statement-breakpoint
ALTER TABLE "evenement__local" RENAME TO "action_local";--> statement-breakpoint
ALTER TABLE "evenement__personne" RENAME TO "action_personne";--> statement-breakpoint
ALTER TABLE "evenement__proprietaire_etablissement" RENAME TO "action_proprietaire_etablissement";--> statement-breakpoint
ALTER TABLE "evenement__proprietaire_personne" RENAME TO "action_proprietaire_personne";--> statement-breakpoint
ALTER TABLE "evenement__rappel" RENAME TO "action_rappel";--> statement-breakpoint
ALTER TABLE "evenement_type" RENAME TO "action_type";--> statement-breakpoint
ALTER TABLE "rappel" RENAME COLUMN "affecte_id" TO "affecte_compte_id";--> statement-breakpoint
ALTER TABLE "evenement" RENAME COLUMN "date" TO "created_at";--> statement-breakpoint

ALTER TABLE "evenement" RENAME CONSTRAINT "evenement_pkey" TO "pk_evenement";--> statement-breakpoint
ALTER TABLE "action_type" RENAME CONSTRAINT "evenement_type_pkey" TO "pk_action_type";--> statement-breakpoint

ALTER TABLE "action_compte" DROP CONSTRAINT "evenement__compte_pkey";--> statement-breakpoint
ALTER TABLE "action_entreprise" DROP CONSTRAINT "evenement__entreprise_pkey";--> statement-breakpoint
ALTER TABLE "action_equipe_etablissement" DROP CONSTRAINT "evenement__equipe_etablissement_pkey";--> statement-breakpoint
ALTER TABLE "action_contact" DROP CONSTRAINT "evenement__contact_pkey";--> statement-breakpoint
ALTER TABLE "action_demande" DROP CONSTRAINT "evenement__demande_pkey";--> statement-breakpoint
ALTER TABLE "action_echange" DROP CONSTRAINT "evenement__echange_pkey";--> statement-breakpoint
ALTER TABLE "action_etablissement" DROP CONSTRAINT "evenement__etablissement_pkey";--> statement-breakpoint
ALTER TABLE "action_etablissement_creation" DROP CONSTRAINT "evenement__etablissement_creation_pkey";--> statement-breakpoint
ALTER TABLE "action_etiquette" DROP CONSTRAINT "evenement__etiquette_pkey";--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement" DROP CONSTRAINT "evenement__etiquette__etablissement_pkey";--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement_creation" DROP CONSTRAINT "evenement__etiquette__etablissement_creation_pkey";--> statement-breakpoint
ALTER TABLE "action_local" DROP CONSTRAINT "evenement__local_pkey";--> statement-breakpoint
ALTER TABLE "action_personne" DROP CONSTRAINT "evenement__personne_pkey";--> statement-breakpoint
ALTER TABLE "action_proprietaire_personne" DROP CONSTRAINT "evenement__proprietaire_personne_pkey";--> statement-breakpoint
ALTER TABLE "action_rappel" DROP CONSTRAINT "evenement__rappel_pkey";--> statement-breakpoint
ALTER TABLE "action_proprietaire_etablissement" DROP CONSTRAINT "evenement__proprietaire_etablissement_pkey";--> statement-breakpoint

ALTER TABLE "evenement" DROP CONSTRAINT "evenement_evenement_type_id_evenement_type_id_fk";
--> statement-breakpoint
ALTER TABLE "evenement" DROP CONSTRAINT "evenement_compte_id_compte_id_fk";
--> statement-breakpoint
ALTER TABLE "evenement" DROP CONSTRAINT "fk_evenement__compte";
--> statement-breakpoint
ALTER TABLE "evenement" DROP CONSTRAINT "fk_evenement__evenement_type";
--> statement-breakpoint

ALTER TABLE "action_compte" DROP CONSTRAINT "evenement__compte_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_compte" DROP CONSTRAINT "evenement__compte_compte_id_compte_id_fk";
--> statement-breakpoint
ALTER TABLE "action_contact" DROP CONSTRAINT "evenement__contact_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_contact" DROP CONSTRAINT "evenement__contact_equipe_id_etablissement_id_personne_id_contact_equipe_id_etablissement_id_personne_id_fk";
--> statement-breakpoint
ALTER TABLE "action_demande" DROP CONSTRAINT "evenement__demande_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_demande" DROP CONSTRAINT "evenement__demande_demande_id_demande_id_fk";
--> statement-breakpoint
ALTER TABLE "action_echange" DROP CONSTRAINT "evenement__echange_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_echange" DROP CONSTRAINT "evenement__echange_echange_id_echange_id_fk";
--> statement-breakpoint
ALTER TABLE "action_entreprise" DROP CONSTRAINT "evenement__entreprise_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_entreprise" DROP CONSTRAINT "evenement__entreprise_entreprise_id_entreprise_id_fk";
--> statement-breakpoint
ALTER TABLE "action_equipe_etablissement" DROP CONSTRAINT "evenement__equipe_etablissement_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_equipe_etablissement" DROP CONSTRAINT "evenement__equipe_etablissement_equipe_id_equipe_id_fk";
--> statement-breakpoint
ALTER TABLE "action_equipe_etablissement" DROP CONSTRAINT "evenement__equipe_etablissement_etablissement_id_etablissement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_etablissement" DROP CONSTRAINT "evenement__etablissement_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_etablissement" DROP CONSTRAINT "evenement__etablissement_etablissement_id_etablissement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_etablissement_creation" DROP CONSTRAINT "evenement__etablissement_creation_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_etablissement_creation" DROP CONSTRAINT "evenement__etablissement_creation_etablissement_creation_id_etablissement_creation_id_fk";
--> statement-breakpoint
ALTER TABLE "action_etiquette" DROP CONSTRAINT "evenement__etiquette_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_etiquette" DROP CONSTRAINT "evenement__etiquette_etiquette_id_etiquette_id_fk";
--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement" DROP CONSTRAINT "evenement__etiquette__etablissement_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement" DROP CONSTRAINT "evenement__etiquette__etablissement_etiquette_id_etiquette_id_fk";
--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement" DROP CONSTRAINT "evenement__etiquette__etablissement_equipe_id_equipe_id_fk";
--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement" DROP CONSTRAINT "evenement__etiquette__etablissement_etablissement_id_etablissement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement_creation" DROP CONSTRAINT "evenement__etiquette__etablissement_creation_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement_creation" DROP CONSTRAINT "evenement__etiquette__etablissement_creation_etiquette_id_etiquette_id_fk";
--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement_creation" DROP CONSTRAINT "evenement__etiquette__etablissement_creation_etablissement_creation_id_etablissement_creation_id_fk";
--> statement-breakpoint
ALTER TABLE "action_local" DROP CONSTRAINT "evenement__local_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_local" DROP CONSTRAINT "evenement__local_local_id_local_id_fk";
--> statement-breakpoint
ALTER TABLE "action_personne" DROP CONSTRAINT "evenement__personne_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_personne" DROP CONSTRAINT "evenement__personne_personne_id_personne_id_fk";
--> statement-breakpoint
ALTER TABLE "action_proprietaire_etablissement" DROP CONSTRAINT "evenement__proprietaire_etablissement_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_proprietaire_etablissement" DROP CONSTRAINT "evenement__proprietaire_etablissement_local_id_local_id_fk";
--> statement-breakpoint
ALTER TABLE "action_proprietaire_etablissement" DROP CONSTRAINT "evenement__proprietaire_etablissement_etablissement_id_etablissement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_proprietaire_personne" DROP CONSTRAINT "evenement__proprietaire_personne_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_rappel" DROP CONSTRAINT "evenement__rappel_evenement_id_evenement_id_fk";
--> statement-breakpoint
ALTER TABLE "action_rappel" DROP CONSTRAINT "evenement__rappel_rappel_id_rappel_id_fk";
--> statement-breakpoint

ALTER TABLE "evenement" ADD COLUMN "log" varchar;--> statement-breakpoint
ALTER TABLE "evenement" ADD COLUMN "contexte" jsonb;--> statement-breakpoint
ALTER TABLE "action_compte" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_compte" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_compte" ADD COLUMN "diff" jsonb;--> statement-breakpoint
ALTER TABLE "action_contact" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_contact" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_contact" ADD COLUMN "diff" jsonb;--> statement-breakpoint
ALTER TABLE "action_demande" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_demande" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_demande" ADD COLUMN "diff" jsonb;--> statement-breakpoint
ALTER TABLE "action_echange" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_echange" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_echange" ADD COLUMN "diff" jsonb;--> statement-breakpoint
ALTER TABLE "action_entreprise" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_entreprise" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_entreprise" ADD COLUMN "diff" jsonb;--> statement-breakpoint
ALTER TABLE "action_equipe_etablissement" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_equipe_etablissement" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_equipe_etablissement" ADD COLUMN "diff" jsonb;--> statement-breakpoint
ALTER TABLE "action_etablissement" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_etablissement" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_etablissement" ADD COLUMN "diff" jsonb;--> statement-breakpoint
ALTER TABLE "action_etablissement_creation" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_etablissement_creation" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_etablissement_creation" ADD COLUMN "diff" jsonb;--> statement-breakpoint
ALTER TABLE "action_etiquette" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_etiquette" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_etiquette" ADD COLUMN "diff" jsonb;--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement" ADD COLUMN "diff" jsonb;--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement_creation" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement_creation" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement_creation" ADD COLUMN "diff" jsonb;--> statement-breakpoint
ALTER TABLE "action_local" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_local" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_local" ADD COLUMN "diff" jsonb;--> statement-breakpoint
ALTER TABLE "action_personne" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_personne" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_personne" ADD COLUMN "diff" jsonb;--> statement-breakpoint
ALTER TABLE "action_proprietaire_etablissement" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_proprietaire_etablissement" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_proprietaire_etablissement" ADD COLUMN "diff" jsonb;--> statement-breakpoint
ALTER TABLE "action_proprietaire_personne" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_proprietaire_personne" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_proprietaire_personne" ADD COLUMN "diff" jsonb;--> statement-breakpoint
ALTER TABLE "action_rappel" ADD COLUMN "created_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "action_rappel" ADD COLUMN "action_type_id" varchar;--> statement-breakpoint
ALTER TABLE "action_rappel" ADD COLUMN "diff" jsonb;--> statement-breakpoint

UPDATE "action_compte"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_compte.evenement_id;

UPDATE "action_contact"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_contact.evenement_id;

UPDATE "action_demande"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_demande.evenement_id;

UPDATE "action_echange"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_echange.evenement_id;

UPDATE "action_entreprise"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_entreprise.evenement_id;

UPDATE "action_equipe_etablissement"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_equipe_etablissement.evenement_id;

UPDATE "action_etablissement"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_etablissement.evenement_id;

UPDATE "action_etablissement_creation"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_etablissement_creation.evenement_id;

UPDATE "action_etiquette"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_etiquette.evenement_id;

UPDATE "action_etiquette__etablissement"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_etiquette__etablissement.evenement_id;

UPDATE "action_etiquette__etablissement_creation"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_etiquette__etablissement_creation.evenement_id;

UPDATE "action_local"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_local.evenement_id;

UPDATE "action_personne"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_personne.evenement_id;

UPDATE "action_proprietaire_etablissement"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_proprietaire_etablissement.evenement_id;

UPDATE "action_proprietaire_personne"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_proprietaire_personne.evenement_id;

UPDATE "action_rappel"
SET diff = evenement.diff,
  created_at = evenement.created_at,
  action_type_id = evenement.evenement_type_id
FROM evenement
WHERE evenement.id = action_rappel.evenement_id;



ALTER TABLE "action_compte"
ALTER COLUMN "action_type_id"
SET NOT NULL;
--> statement-breakpoint
ALTER TABLE "action_contact"
ALTER COLUMN "action_type_id"
SET NOT NULL;
--> statement-breakpoint
ALTER TABLE "action_demande"
ALTER COLUMN "action_type_id"
SET NOT NULL;
--> statement-breakpoint
ALTER TABLE "action_echange"
ALTER COLUMN "action_type_id"
SET NOT NULL;
--> statement-breakpoint
ALTER TABLE "action_entreprise"
ALTER COLUMN "action_type_id"
SET NOT NULL;
--> statement-breakpoint
ALTER TABLE "action_equipe_etablissement"
ALTER COLUMN "action_type_id"
SET NOT NULL;
--> statement-breakpoint
ALTER TABLE "action_etablissement"
ALTER COLUMN "action_type_id"
SET NOT NULL;
--> statement-breakpoint
ALTER TABLE "action_etablissement_creation"
ALTER COLUMN "action_type_id"
SET NOT NULL;
--> statement-breakpoint
ALTER TABLE "action_etiquette"
ALTER COLUMN "action_type_id"
SET NOT NULL;
--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement"
ALTER COLUMN "action_type_id"
SET NOT NULL;
--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement_creation"
ALTER COLUMN "action_type_id"
SET NOT NULL;
--> statement-breakpoint
ALTER TABLE "action_local"
ALTER COLUMN "action_type_id"
SET NOT NULL;
--> statement-breakpoint
ALTER TABLE "action_personne"
ALTER COLUMN "action_type_id"
SET NOT NULL;
--> statement-breakpoint
ALTER TABLE "action_proprietaire_etablissement"
ALTER COLUMN "action_type_id"
SET NOT NULL;
--> statement-breakpoint
ALTER TABLE "action_proprietaire_personne"
ALTER COLUMN "action_type_id"
SET NOT NULL;
--> statement-breakpoint
ALTER TABLE "action_rappel"
ALTER COLUMN "action_type_id"
SET NOT NULL;


CREATE INDEX IF NOT EXISTS "idx_action_compte" ON "action_compte" ("evenement_id","compte_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_contact" ON "action_contact" ("evenement_id","equipe_id","etablissement_id","personne_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_demande" ON "action_demande" ("evenement_id","demande_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_echange" ON "action_echange" ("evenement_id","echange_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_entreprise" ON "action_entreprise" ("evenement_id","entreprise_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_equipe_etablissement" ON "action_equipe_etablissement" ("evenement_id","equipe_id","etablissement_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_etablissement" ON "action_etablissement" ("evenement_id","etablissement_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_etablissement_creation" ON "action_etablissement_creation" ("evenement_id","etablissement_creation_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_etiquette" ON "action_etiquette" ("evenement_id","etiquette_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_etiquette_etablissement" ON "action_etiquette__etablissement" ("evenement_id","etiquette_id","etablissement_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_etiquette_etablissement_creation" ON "action_etiquette__etablissement_creation" ("evenement_id","etiquette_id","etablissement_creation_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_local" ON "action_local" ("evenement_id","local_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_personne" ON "action_personne" ("evenement_id","personne_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_proprietaire_etablissement" ON "action_proprietaire_etablissement" ("evenement_id","local_id","etablissement_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_proprietaire_personne" ON "action_proprietaire_personne" ("evenement_id","local_id","personne_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_rappel" ON "action_rappel" ("evenement_id","rappel_id","action_type_id");--> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "evenement" ADD CONSTRAINT "fk_evenement_compte" FOREIGN KEY ("compte_id") REFERENCES "compte"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_compte" ADD CONSTRAINT "fk_action_compte__compte" FOREIGN KEY ("compte_id") REFERENCES "compte"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_compte" ADD CONSTRAINT "fk_action_compte__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_compte" ADD CONSTRAINT "fk_action_compte__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_contact" ADD CONSTRAINT "fk_action_contact__contact" FOREIGN KEY ("equipe_id","etablissement_id","personne_id") REFERENCES "contact"("equipe_id","etablissement_id","personne_id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_contact" ADD CONSTRAINT "fk_action_contact__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_contact" ADD CONSTRAINT "fk_action_contact__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_demande" ADD CONSTRAINT "fk_action_demande__demande" FOREIGN KEY ("demande_id") REFERENCES "demande"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_demande" ADD CONSTRAINT "fk_action_demande__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_demande" ADD CONSTRAINT "fk_action_demande__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_echange" ADD CONSTRAINT "fk_action_echange__echange" FOREIGN KEY ("echange_id") REFERENCES "echange"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_echange" ADD CONSTRAINT "fk_action_Echange__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_echange" ADD CONSTRAINT "fk_action_Echange__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_entreprise" ADD CONSTRAINT "fk_action_entreprise__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_entreprise" ADD CONSTRAINT "fk_action_entreprise__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_entreprise" ADD CONSTRAINT "fk_action_entreprise__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_etablissement" ADD CONSTRAINT "fk_action_equipe_etablissement__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_etablissement" ADD CONSTRAINT "fk_action_equipe_etablissement__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_etablissement" ADD CONSTRAINT "fk_action_equipe_etablissement__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_etablissement" ADD CONSTRAINT "fk_action_equipe_etablissement__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etablissement" ADD CONSTRAINT "fk_action_etablissement__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etablissement" ADD CONSTRAINT "fk_action_etablissement__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etablissement" ADD CONSTRAINT "fk_action_etablissement__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etablissement_creation" ADD CONSTRAINT "fk_action_etablissement_creation__etablissement_creation" FOREIGN KEY ("etablissement_creation_id") REFERENCES "etablissement_creation"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etablissement_creation" ADD CONSTRAINT "fk_action_etablissement_creation__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etablissement_creation" ADD CONSTRAINT "fk_action_etablissement_creation__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette" ADD CONSTRAINT "fk_action_etiquette__etiquette" FOREIGN KEY ("etiquette_id") REFERENCES "etiquette"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette" ADD CONSTRAINT "fk_action_etiquette__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette" ADD CONSTRAINT "fk_action_etiquette__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette__etablissement" ADD CONSTRAINT "fk_action_etiquette_etablissement__etiquette" FOREIGN KEY ("etiquette_id") REFERENCES "etiquette"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette__etablissement" ADD CONSTRAINT "fk_action_etiquette_etablissement__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette__etablissement" ADD CONSTRAINT "fk_action_etiquette_etablissement__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette__etablissement" ADD CONSTRAINT "fk_action_etiquette_etablissement__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette__etablissement" ADD CONSTRAINT "fk_action_etiquette_etablissement__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette__etablissement_creation" ADD CONSTRAINT "fk_action_etiquette_etablissement_creation__ec" FOREIGN KEY ("etablissement_creation_id") REFERENCES "etablissement_creation"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette__etablissement_creation" ADD CONSTRAINT "fk_action_etiquette_etablissement_creation__etiquette" FOREIGN KEY ("etiquette_id") REFERENCES "etiquette"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette__etablissement_creation" ADD CONSTRAINT "fk_action_etiquette_etablissement_creation__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette__etablissement_creation" ADD CONSTRAINT "fk_action_etiquette_etablissement_creation__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_local" ADD CONSTRAINT "fk_action_local__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_local" ADD CONSTRAINT "fk_action_local__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_local" ADD CONSTRAINT "fk_action_local__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_personne" ADD CONSTRAINT "fk_action_personne_evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_personne" ADD CONSTRAINT "fk_action_personne__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_proprietaire_etablissement" ADD CONSTRAINT "fk_action_proprietaire_etablissement_local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_proprietaire_etablissement" ADD CONSTRAINT "fk_action_proprietaire_etablissement_etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_proprietaire_etablissement" ADD CONSTRAINT "fk_action_proprietaire_etablissement__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_proprietaire_etablissement" ADD CONSTRAINT "fk_proprietaire_etablissement__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_proprietaire_personne" ADD CONSTRAINT "fk_action_proprietaire_personne_local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_proprietaire_personne" ADD CONSTRAINT "fk_action_proprietaire_personne_personne" FOREIGN KEY ("personne_id") REFERENCES "personne"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_proprietaire_personne" ADD CONSTRAINT "fk_action_proprietaire_personne__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_proprietaire_personne" ADD CONSTRAINT "fk_proprietaire_personne__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_rappel" ADD CONSTRAINT "fk_action_rappel__rappel" FOREIGN KEY ("rappel_id") REFERENCES "rappel"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_rappel" ADD CONSTRAINT "fk_action_rappel__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_rappel" ADD CONSTRAINT "fk_action_rappel__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
