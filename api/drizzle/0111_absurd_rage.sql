DROP TABLE "equipe__etablissement_exogene";--> statement-breakpoint
DROP TABLE "etablissement_adresse_1_etalab";--> statement-breakpoint
ALTER TABLE "source"."etalab__etablissement_geoloc" DROP CONSTRAINT "uk_etalab__etablissement_geoloc__etablissement";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_etalab__etablissement_geoloc__etablissement";--> statement-breakpoint

ALTER TABLE "source"."insee_sirene__etablissement_geoloc" ALTER COLUMN "epsg" SET NOT NULL;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene__etablissement_geoloc" ALTER COLUMN "plg_code_commune" SET NOT NULL;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene__etablissement_geoloc" ALTER COLUMN "qualite_xy" SET NOT NULL;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene__etablissement_geoloc" ALTER COLUMN "x" SET NOT NULL;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene__etablissement_geoloc" ALTER COLUMN "x_longitude" SET NOT NULL;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene__etablissement_geoloc" ALTER COLUMN "y" SET NOT NULL;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene__etablissement_geoloc" ALTER COLUMN "y_latitude" SET NOT NULL;--> statement-breakpoint

ALTER TABLE "source"."etalab__etablissement_geoloc" ALTER COLUMN "geo_score" SET NOT NULL;--> statement-breakpoint
ALTER TABLE "source"."etalab__etablissement_geoloc" DROP COLUMN IF EXISTS "id";--> statement-breakpoint
ALTER TABLE "source"."etalab__etablissement_geoloc" ADD COLUMN "mise_a_jour_at" timestamp with time zone DEFAULT now();--> statement-breakpoint

ALTER TABLE "source"."insee_sirene_api__etablissement" DROP COLUMN IF EXISTS "longitude";--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" DROP COLUMN IF EXISTS "latitude";--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" DROP COLUMN IF EXISTS "geo_score";--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" DROP COLUMN IF EXISTS "geo_type";--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" DROP COLUMN IF EXISTS "geo_adresse";--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" DROP COLUMN IF EXISTS "geo_id";--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" DROP COLUMN IF EXISTS "geo_ligne";--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" DROP COLUMN IF EXISTS "geo_l4";--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" DROP COLUMN IF EXISTS "geo_l5";--> statement-breakpoint

DROP VIEW IF EXISTS etablissement_adresse_vue;
CREATE OR REPLACE VIEW etablissement_adresse_vue AS (
	SELECT
		E.siret,
		E.code_commune_etablissement AS commune_id,
		E.mise_a_jour_at,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.geo_score > 0.5
			THEN EEG.numero
			ELSE E.numero_voie_etablissement || COALESCE(E.indice_repetition_etablissement, '')
		END AS numero,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.geo_score > 0.5
			THEN EEG.voie_nom
			ELSE COALESCE(E.type_voie_etablissement || ' ', '') || E.libelle_voie_etablissement
		END AS voie_nom,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.geo_score > 0.5
			THEN EEG.code_postal
			ELSE E.code_postal_etablissement
		END AS code_postal,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.geo_score > 0.5
			THEN EEG.cedex_code
			ELSE E.code_cedex_etablissement
		END AS cedex_code,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.geo_score > 0.5
			THEN EEG.cedex_nom
			ELSE E.libelle_cedex_etablissement
		END AS cedex_nom,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.geo_score > 0.5
			THEN EEG.distribution_speciale
			ELSE E.distribution_speciale_etablissement
		END AS distribution_speciale,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.geo_score > 0.5
			THEN EEG.etranger_commune
			ELSE E.libelle_commune_etranger_etablissement
		END AS etranger_commune,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.geo_score > 0.5
			THEN EEG.etranger_pays
			ELSE E.libelle_pays_etranger_etablissement
		END AS etranger_pays,
		CASE WHEN ESG.siret IS NULL AND EEG.siret IS NOT NULL AND EEG.geo_score > 0.5
			THEN EEG.geolocalisation
			ELSE
				CASE WHEN ESG.siret IS NOT NULL
					THEN ST_SetSrid(ST_MakePoint(ESG.x_longitude, ESG.y_latitude), 4326)
					ELSE NULL
				END
		END AS geolocalisation
	FROM source.insee_sirene_api__etablissement E
	LEFT JOIN source.insee_sirene__etablissement_geoloc ESG USING (siret)
	LEFT JOIN source.etalab__etablissement_geoloc EEG USING (siret)
);--> statement-breakpoint

DROP MATERIALIZED VIEW IF EXISTS etablissement_geolocalisation_vue;
CREATE MATERIALIZED VIEW etablissement_geolocalisation_vue AS (
		SELECT
				siret AS etabgeovue_siret,
				COALESCE(
						st_setsrid(st_makepoint(x_longitude, y_latitude), 4326),
						geolocalisation
				) AS etabgeovue_geolocalisation
		FROM
				source.insee_sirene_api__etablissement
				LEFT JOIN source.insee_sirene__etablissement_geoloc ESG USING (siret)
				LEFT JOIN source.etalab__etablissement_geoloc EEG USING (siret)
		WHERE
				ESG.siret IS NOT NULL
				OR EEG.siret IS NOT NULL
);--> statement-breakpoint
CREATE UNIQUE INDEX idx_etablissement_geolocalisation_vue_siret ON etablissement_geolocalisation_vue (etabgeovue_siret);--> statement-breakpoint
CREATE INDEX idx_etablissement_geolocalisation_vue_geolocalisation ON etablissement_geolocalisation_vue USING gist(etabgeovue_geolocalisation);--> statement-breakpoint

DROP MATERIALIZED VIEW IF EXISTS equipe__commune_vue;
CREATE MATERIALIZED VIEW equipe__commune_vue AS (
SELECT e.id AS eqcomvue_equipe_id,
	c.id AS eqcomvue_commune_id
	FROM equipe e
	LEFT JOIN equipe__commune ec ON ec.equipe_id = e.id
	LEFT JOIN equipe__metropole em ON em.equipe_id = e.id
	LEFT JOIN equipe__epci ee ON ee.equipe_id = e.id
	LEFT JOIN equipe__petr ep ON ep.equipe_id = e.id
	LEFT JOIN equipe__departement ed ON ed.equipe_id = e.id
	LEFT JOIN equipe__region er ON er.equipe_id = e.id
	LEFT JOIN commune c
		ON c.id::text = ec.commune_id::text
		OR c.metropole_id::text = em.metropole_id::text
		OR c.epci_id::text = ee.epci_id::text
		OR c.petr_id::text = ep.petr_id::text
		OR c.departement_id::text = ed.departement_id::text
		OR c.region_id::text = er.region_id::text
);--> statement-breakpoint
CREATE UNIQUE INDEX idx_equipe__commune_vue_unique ON equipe__commune_vue (eqcomvue_equipe_id, eqcomvue_commune_id);--> statement-breakpoint
CREATE INDEX idx_equipe__commune_vue_commune ON equipe__commune_vue (eqcomvue_commune_id);--> statement-breakpoint
CREATE INDEX idx_equipe__commune_vue_equipe ON equipe__commune_vue (eqcomvue_equipe_id);--> statement-breakpoint
CLUSTER equipe__commune_vue USING idx_equipe__commune_vue_equipe; --> statement-breakpoint