ALTER TYPE "source"."source_sirene_caractere_employeur_enum" RENAME TO "insee_sirene__etablissement__caractere_employeur_enum";--> statement-breakpoint
ALTER TYPE "source"."source_sirene_etablissement_etat_administratif_enum" RENAME TO "insee_sirene__etablissement__etat_administratif_enum";--> statement-breakpoint
ALTER TYPE "source"."source_sirene_etablissement_statut_diffusion_enum" RENAME TO "insee_sirene__etablissement__statut_diffusion_enum";--> statement-breakpoint
ALTER TYPE "source"."source_sirene_unite_legale_categorie_entreprise_enum" RENAME TO "insee_sirene__unite_legale__categorie_entreprise_enum";--> statement-breakpoint
ALTER TYPE "source"."source_sirene_unite_legale_economie_sociale_solidaire_enum" RENAME TO "insee_sirene__unite_legale__eco_soc_sol_enum";--> statement-breakpoint
ALTER TYPE "source"."source_sirene_unite_legale_etat_administratif_enum" RENAME TO "insee_sirene__unite_legale__etat_administratif_enum";--> statement-breakpoint
ALTER TYPE "source"."source_sirene_unite_legale_sexe_enum" RENAME TO "insee_sirene__unite_legale__sexe_enum";--> statement-breakpoint
ALTER TYPE "source"."source_sirene_unite_legale_societe_mission_enum" RENAME TO "insee_sirene__unite_legale__societe_mission_enum";--> statement-breakpoint
ALTER TYPE "source"."source_sirene_unite_legale_statut_diffusion_enum" RENAME TO "insee_sirene__unite_legale__statut_diffusion_enum";--> statement-breakpoint

ALTER TABLE "source"."source_insee_qpv_commune" RENAME TO "insee__qpv_commune";--> statement-breakpoint
ALTER TABLE "source"."source_insee_commune_comer" RENAME TO "insee__commune_comer";--> statement-breakpoint
ALTER TABLE "source"."source_acv" RENAME TO "anct__commune_action_coeur_de_ville";--> statement-breakpoint
ALTER TABLE "source"."source_afr" RENAME TO "anct__commune_aide_finalite_regionale";--> statement-breakpoint
ALTER TABLE "source"."source_pvd" RENAME TO "anct__commune_petites_villes_de_demain";--> statement-breakpoint
ALTER TABLE "source"."source_anct_commune_territoire_industrie" RENAME TO "anct__commune_territoire_d_industrie";--> statement-breakpoint
ALTER TABLE "source"."source_va" RENAME TO "anct__commune_village_d_avenir";--> statement-breakpoint
ALTER TABLE "source"."source_datagouv_zrr" RENAME TO "anct__commune_zone_revitalisation_rurale";--> statement-breakpoint
ALTER TABLE "source"."source_datagouv_qpv" RENAME TO "anct__qpv";--> statement-breakpoint
ALTER TABLE "source"."source_api_entreprise_etablissement_exercice" RENAME TO "api_entreprise__etablissement_exercice";--> statement-breakpoint
ALTER TABLE "source"."source_api_entreprise_entreprise_mandataire_social" RENAME TO "api_entreprise__entreprise_mandataire_social";--> statement-breakpoint
ALTER TABLE "source"."source_bodacc_procedure_collective" RENAME TO "bodacc__procedure_collective";--> statement-breakpoint
ALTER TABLE "source"."source_cerema_local" RENAME TO "cerema__local";--> statement-breakpoint
ALTER TABLE "source"."source_cerema_local_categorie" RENAME TO "cerema__local_categorie";--> statement-breakpoint
ALTER TABLE "source"."source_cerema_local_nature" RENAME TO "cerema__local_nature";--> statement-breakpoint
ALTER TABLE "source"."source_cerema_local_proprietaire_moral" RENAME TO "cerema__local_proprietaire_moral";--> statement-breakpoint
ALTER TABLE "source"."source_cerema_local_proprietaire_physique" RENAME TO "cerema__local_proprietaire_physique";--> statement-breakpoint
ALTER TABLE "source"."source_datagouv_commune_contour" RENAME TO "datagouv__commune_contour";--> statement-breakpoint
ALTER TABLE "source"."source_datagouv_commune_ept" RENAME TO "datagouv__commune_ept";--> statement-breakpoint
ALTER TABLE "source"."source_dgcl_subventions" RENAME TO "dgcl__subventions";--> statement-breakpoint
ALTER TABLE "source"."source_ess_france" RENAME TO "ess_france__entreprise_ess";--> statement-breakpoint
ALTER TABLE "source"."source_inpi_ratio_financier" RENAME TO "inpi__ratio_financier";--> statement-breakpoint
ALTER TABLE "source"."source_insee_commune" RENAME TO "insee__commune";--> statement-breakpoint
ALTER TABLE "source"."source_insee_commune_historique" RENAME TO "insee__commune_historique";--> statement-breakpoint
ALTER TABLE "source"."source_insee_commune_mouvement" RENAME TO "insee__commune_mouvement";--> statement-breakpoint
ALTER TABLE "source"."source_insee_departement" RENAME TO "insee__departement";--> statement-breakpoint
ALTER TABLE "source"."source_insee_epci" RENAME TO "insee__epci";--> statement-breakpoint
ALTER TABLE "source"."source_insee_epci_commune" RENAME TO "insee__epci_commune";--> statement-breakpoint
ALTER TABLE "source"."source_insee_region" RENAME TO "insee__region";--> statement-breakpoint
ALTER TABLE "source"."source_api_sirene_etablissement" RENAME TO "insee_sirene_api__etablissement";--> statement-breakpoint
ALTER TABLE "source"."source_api_sirene_etablissement_periode" RENAME TO "insee_sirene_api__etablissement_periode";--> statement-breakpoint
ALTER TABLE "source"."source_api_sirene_lien_succession" RENAME TO "insee_sirene_api__lien_succession";--> statement-breakpoint
ALTER TABLE "source"."source_api_sirene_unite_legale" RENAME TO "insee_sirene_api__unite_legale";--> statement-breakpoint
ALTER TABLE "source"."source_api_sirene_unite_legale_periode" RENAME TO "insee_sirene_api__unite_legale_periode";--> statement-breakpoint
ALTER TABLE "source"."source_geoloc_entreprise" RENAME TO "insee_sirene__entreprise_geoloc";--> statement-breakpoint
ALTER TABLE "source"."source_laposte_code_insee_code_postal" RENAME TO "laposte__code_insee_code_postal";--> statement-breakpoint
ALTER TABLE "source"."source_odt_epci_petr" RENAME TO "observatoire_des_territoires__epci_petr";--> statement-breakpoint
ALTER TABLE "source"."source_rcd_etablissement_effectif_emm_13_mois" RENAME TO "rcd__etablissement_effectif_emm_13_mois";--> statement-breakpoint
ALTER TABLE "source"."source_rcd_etablissement_effectif_emm_historique" RENAME TO "rcd__etablissement_effectif_emm_historique";--> statement-breakpoint

ALTER TABLE "source"."anct__commune_action_coeur_de_ville" RENAME CONSTRAINT "pk_source_acv__commune" TO "pk_anct__commune_action_coeur_de_ville";--> statement-breakpoint
ALTER TABLE "source"."anct__commune_aide_finalite_regionale" RENAME CONSTRAINT "source_afr_pkey" TO "pk_anct__commune_aide_finalite_regionale";--> statement-breakpoint
ALTER TABLE "source"."anct__commune_petites_villes_de_demain" ADD CONSTRAINT "pk_anct__commune_petites_villes_de_demain" PRIMARY KEY (commune_id);--> statement-breakpoint
ALTER TABLE "source"."anct__commune_territoire_d_industrie" RENAME CONSTRAINT "source_anct_commune_territoire_industrie_pkey" TO "pk_anct__commune_territoire_d_industrie";--> statement-breakpoint
ALTER TABLE "source"."anct__commune_village_d_avenir" RENAME CONSTRAINT "source_va_pkey" TO "pk_anct__commune_village_d_avenir";--> statement-breakpoint
ALTER TABLE "source"."anct__commune_zone_revitalisation_rurale" RENAME CONSTRAINT "source_datagouv_zrr_pkey" TO "pk_anct__zone_revitalisation_rurale";--> statement-breakpoint
ALTER TABLE "source"."anct__qpv" RENAME CONSTRAINT "source_datagouv_qpv_pkey" TO "pk_anct__qpv";--> statement-breakpoint
ALTER TABLE "source"."api_entreprise__etablissement_exercice" RENAME CONSTRAINT "source_api_entreprise_etablissement_exercice_pkey" TO "pk_api_entreprise__etablissement_exercice";--> statement-breakpoint
ALTER TABLE "source"."api_entreprise__entreprise_mandataire_social" RENAME CONSTRAINT "source_api_entreprise_entreprise_mandataire_social_pkey" TO "pk_api_entreprise__entreprise_mandataire_social";--> statement-breakpoint
ALTER TABLE "source"."cerema__local_categorie" RENAME CONSTRAINT "source_cerema_local_categorie_pkey" TO "pk_cerema__local_categorie";--> statement-breakpoint
ALTER TABLE "source"."cerema__local_nature" RENAME CONSTRAINT "source_cerema_local_nature_pkey" TO "pk_cerema__local_nature";--> statement-breakpoint
ALTER TABLE "source"."datagouv__commune_contour" RENAME CONSTRAINT "source_datagouv_commune_contour_pk" TO "pk_datagouv__commune_contour";--> statement-breakpoint
ALTER TABLE "source"."datagouv__commune_ept" RENAME CONSTRAINT "source_datagouv_commune_ept_pkey" TO "pk_datagouv__commune_ept";--> statement-breakpoint
ALTER TABLE "source"."ess_france__entreprise_ess" RENAME CONSTRAINT "pk_source_ess_france__entreprise" TO "pk_ess_france__entreprise_ess";--> statement-breakpoint
ALTER TABLE "source"."insee__departement" RENAME CONSTRAINT "source_insee_departement_pkey" TO "pk_insee__departement";--> statement-breakpoint
ALTER TABLE "source"."insee__epci" RENAME CONSTRAINT "source_insee_epci_pkey" TO "pk_insee__epci";--> statement-breakpoint
ALTER TABLE "source"."insee__epci_commune" RENAME CONSTRAINT "source_insee_epci_commune_pkey" TO "pk_insee__epci_commune";--> statement-breakpoint
ALTER TABLE "source"."insee__region" RENAME CONSTRAINT "source_insee_region_pkey" TO "pk_insee__region";--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" RENAME CONSTRAINT "PK_4e7fe9960ac8949d7e28724a255" TO "pk_insee_sirene_api__etablissement";--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__unite_legale" RENAME CONSTRAINT "PK_7eef7f1f8853920ba288f62bd9e" TO "pk_insee_sirene_api__unite_legale";--> statement-breakpoint
ALTER TABLE "source"."insee_sirene__entreprise_geoloc" RENAME CONSTRAINT "PK_94e552e5dbe3d06f725ca06961d" TO "pk_insee_sirene__entreprise_geoloc";--> statement-breakpoint
ALTER TABLE "source"."observatoire_des_territoires__epci_petr" RENAME CONSTRAINT "source_odt_epci_petr_pkey" TO "pk_observatoire_des_territoires__epci_petr";--> statement-breakpoint

ALTER TABLE "source"."insee_sirene_api__etablissement_periode" RENAME CONSTRAINT "uk_source_api_sirene_etablissement_periode" TO "uk_insee_sirene_api__etablissement_periode";--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__lien_succession" RENAME CONSTRAINT "uk_predecesseur_successeur_date_lien" TO "uk_insee_sirene_api__lien_succession";--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__unite_legale_periode" RENAME CONSTRAINT "uk_source_api_sirene_unite_legale_periode" TO "uk_insee_sirene_api__unite_legale_periode";--> statement-breakpoint

ALTER INDEX "source"."idx_source_bodacc_procedure_collective__entreprise" RENAME TO "idx_bodacc__procedure_collective__entreprise";--> statement-breakpoint
ALTER INDEX "source"."source_datagouv_commune_contour_wkb_geometry_geom_idx" RENAME TO "idx_datagouv__commune_contour__wkb_geometry";--> statement-breakpoint
ALTER INDEX "source"."idx_source_inpi_ratios_financiers__entreprise" RENAME TO "idx_inpi__ratio_financier";--> statement-breakpoint
ALTER INDEX "source"."idx_source_insee_commune_comer__commune" RENAME TO "idx_insee__commune_comer__com_comer";--> statement-breakpoint
ALTER INDEX "source"."IDX_39bde3c6f4efb0b5619c66bb21" RENAME TO "idx_insee_sirene_api__etablissement__siren";--> statement-breakpoint
ALTER INDEX "source"."idx_sirene_etablissement__date_dernier_traitement" RENAME TO "idx_insee_sirene_api__etablissement__date_dte";--> statement-breakpoint
ALTER INDEX "source"."idx_source_api_sirene_etablissement" RENAME TO "idx_insee_sirene_api__etablissement__mise_a_jour_at";--> statement-breakpoint
ALTER INDEX "source"."idx_source_api_sirene_etablissement_periode" RENAME TO "idx_insee_sirene_api__etablissement_periode__mise_a_jour_at";--> statement-breakpoint
ALTER INDEX "source"."IDX_08f86ed1791de4e0863e1b8ccf" RENAME TO "idx_insee_sirene__api_lien_succession__predecesseur";--> statement-breakpoint
ALTER INDEX "source"."IDX_70329533adf03ec170bb587323" RENAME TO "idx_insee_sirene__api_lien_succession__successeur";--> statement-breakpoint
ALTER INDEX "source"."idx_source_api_sirene_unite_legale" RENAME TO "idx_insee_sirene_api__unite_legale__mise_a_jour_at";--> statement-breakpoint

