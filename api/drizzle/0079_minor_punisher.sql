CREATE TABLE IF NOT EXISTS "beneficiaire_effectif" (
	"id" serial NOT NULL,
	"entreprise_id" varchar(9) NOT NULL,
	"nom" varchar,
	"nom_usage" varchar,
	"prenoms" varchar,
	"naissance_date" date,
	"nationalite" varchar,
	"pays_residence" varchar,
	"total_parts" double precision,
	CONSTRAINT "pk_beneficiaire_effectif" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."api_entreprise__entreprise_beneficiaire_effectif" (
	"entreprise_id" varchar(9) NOT NULL,
	"payload" jsonb NOT NULL,
	CONSTRAINT "pk_api_entreprise__entreprise_beneficiaire_effectif" PRIMARY KEY("entreprise_id")
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "beneficiaire_effectif" ADD CONSTRAINT "fk_beneficiaire_effectif__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
