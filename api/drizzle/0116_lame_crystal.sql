CREATE TABLE IF NOT EXISTS "source"."etalab__commune_contour" (
	"ogc_fid" serial NOT NULL,
	"wkb_geometry" geometry(MultiPolygon,4326),
	"code" varchar,
	"nom" varchar,
	"departement" varchar,
	"region" varchar,
	"epci" varchar,
	"plm" boolean,
	"commune" varchar,
	CONSTRAINT "pk_etalab__commune_contour" PRIMARY KEY("ogc_fid")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."etalab__departement_contour" (
	"ogc_fid" serial NOT NULL,
	"wkb_geometry" geometry(MultiPolygon,4326),
	"code" varchar,
	"nom" varchar,
	"region" varchar,
	CONSTRAINT "pk_etalab__departement_contour" PRIMARY KEY("ogc_fid")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."etalab__epci_contour" (
	"ogc_fid" serial NOT NULL,
	"wkb_geometry" geometry(MultiPolygon,4326),
	"code" varchar,
	"nom" varchar,
	CONSTRAINT "pk_etalab__epci_contour" PRIMARY KEY("ogc_fid")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."etalab__mairie_point" (
	"ogc_fid" serial NOT NULL,
	"wkb_geometry" geometry(MultiPolygon,4326),
	"code" varchar,
	"nom" varchar,
	"type" varchar,
	CONSTRAINT "pk_etalab__mairie_point" PRIMARY KEY("ogc_fid")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."etalab__region_contour" (
	"ogc_fid" serial NOT NULL,
	"wkb_geometry" geometry(MultiPolygon,4326),
	"code" varchar,
	"nom" varchar,
	CONSTRAINT "pk_etalab__region_contour" PRIMARY KEY("ogc_fid")
);
--> statement-breakpoint
DROP TABLE "source"."datagouv__commune_contour";--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_etalab__commune_contour__wkb_geometry" ON "source"."etalab__commune_contour" USING gist ("wkb_geometry");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_etalab__departement_contour__wkb_geometry" ON "source"."etalab__departement_contour" USING gist ("wkb_geometry");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_etalab__epci_contour__wkb_geometry" ON "source"."etalab__epci_contour" USING gist ("wkb_geometry");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_etalab__mairie_point__wkb_geometry" ON "source"."etalab__mairie_point" USING gist ("wkb_geometry");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_etalab__region_contour__wkb_geometry" ON "source"."etalab__region_contour" USING gist ("wkb_geometry");
