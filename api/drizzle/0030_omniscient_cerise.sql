CREATE TABLE IF NOT EXISTS
  "action_equipe" (
    "evenement_id" integer NOT NULL,
    "created_at" timestamp with time zone DEFAULT now() NOT NULL,
    "action_type_id" varchar NOT NULL,
    "diff" jsonb,
    "equipe_id" integer NOT NULL
  );

--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_equipe" ON "action_equipe" ("evenement_id", "equipe_id", "action_type_id");

--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe" ADD CONSTRAINT "fk_action_equipe__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe" ADD CONSTRAINT "fk_action_equipe__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe" ADD CONSTRAINT "fk_action_equipe__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
