ALTER TABLE "zonage" ALTER COLUMN "geometrie" SET NOT NULL;--> statement-breakpoint
ALTER TABLE "zonage" ADD COLUMN "description" varchar;--> statement-breakpoint
ALTER TABLE "zonage" ADD COLUMN "etiquette_id" integer;--> statement-breakpoint
UPDATE zonage
SET etiquette_id = etiquette.id
FROM etiquette
WHERE zonage.equipe_id = etiquette.equipe_id
	AND zonage.nom = etiquette.nom
	AND etiquette.etiquette_type_id = 'localisation';--> statement-breakpoint
ALTER TABLE "zonage" ALTER COLUMN "etiquette_id" SET NOT NULL;--> statement-breakpoint
ALTER TABLE "zonage" DROP COLUMN IF EXISTS "nom";--> statement-breakpoint