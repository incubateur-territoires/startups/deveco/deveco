CREATE TABLE IF NOT EXISTS "deveco__local_favori" (
	"deveco_id" integer NOT NULL,
	"local_id" varchar(14) NOT NULL,
	CONSTRAINT "pk_deveco__local_favori" PRIMARY KEY("deveco_id","local_id")
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "deveco__local_favori" ADD CONSTRAINT "fk_deveco__local_favori__deveco" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "deveco__local_favori" ADD CONSTRAINT "fk_deveco__local_favori__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
