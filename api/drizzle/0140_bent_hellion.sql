DROP MATERIALIZED VIEW "public"."etablissement_geolocalisation_vue" CASCADE;--> statement-breakpoint
CREATE VIEW "public"."etablissement_geolocalisation_vue" AS (
	SELECT
			"source"."insee_sirene_api__etablissement"."siret" AS siret,
			COALESCE(
				"equipe__etablissement_contribution"."geolocalisation",
				ST_SetSRID(ST_MakePoint("source"."insee_sirene__etablissement_geoloc"."x_longitude", "source"."insee_sirene__etablissement_geoloc"."y_latitude"), 4326),
				"source"."etalab__etablissement_adresse"."geolocalisation"
			) AS geolocalisation,
			"source"."insee_sirene_api__etablissement"."mise_a_jour_at" AS mise_a_jour_at
		FROM "source"."insee_sirene_api__etablissement"
		LEFT JOIN "source"."insee_sirene__etablissement_geoloc" ON "source"."insee_sirene__etablissement_geoloc"."siret" = "source"."insee_sirene_api__etablissement"."siret" AND "source"."insee_sirene__etablissement_geoloc"."siret" IS NOT NULL
		LEFT JOIN "source"."etalab__etablissement_adresse" ON "source"."etalab__etablissement_adresse"."siret" = "source"."insee_sirene_api__etablissement"."siret" AND "source"."etalab__etablissement_adresse"."siret" IS NOT NULL
		LEFT JOIN "equipe__etablissement_contribution" ON "equipe__etablissement_contribution"."etablissement_id" = "source"."insee_sirene_api__etablissement"."siret" AND "equipe__etablissement_contribution"."geolocalisation" IS NOT NULL
		WHERE "source"."insee_sirene__etablissement_geoloc"."siret" IS NOT NULL
			OR "equipe__etablissement_contribution"."geolocalisation" IS NOT NULL
			OR "source"."etalab__etablissement_adresse"."siret" IS NOT NULL
);
CREATE VIEW public.etablissement__qpv_2024_vue AS (
  SELECT
    siret as etablissement_id,
    qpv_2024.id as qpv_2024_id
  FROM etablissement_geolocalisation_vue
  JOIN qpv_2024 ON ST_Covers(geometrie, geolocalisation));
--> statement-breakpoint
CREATE VIEW public.etablissement__qpv_2015_vue AS (
  SELECT
    siret as etablissement_id,
    qpv_2015.id as qpv_2015_id
  FROM etablissement_geolocalisation_vue
  JOIN qpv_2015 ON ST_Covers(geometrie, geolocalisation));
--> statement-breakpoint
