CREATE TABLE IF NOT EXISTS "source"."source_datagouv_commune_contour" (
	"ogc_fid" serial NOT NULL,
	"wkb_geometry" "geometry(Geometry,4326)",
	"code" varchar,
	"nom" varchar,
	"departement" varchar,
	"region" varchar,
	"epci" varchar,
	"plm" boolean,
	"commune" varchar,
	CONSTRAINT "pk_source_datagouv_commune_contour" PRIMARY KEY("ogc_fid")
);
--> statement-breakpoint

ALTER TABLE "qpv" ALTER COLUMN "geometrie" SET DATA TYPE geometry(MultiPolygon,4326);--> statement-breakpoint
ALTER TABLE "commune__qpv" ADD CONSTRAINT "pk_commune__qpv" PRIMARY KEY("commune_id","qpv_id");--> statement-breakpoint

ALTER TABLE "source"."source_api_sirene_lien_succession" RENAME CONSTRAINT "uniq_predecesseur_successeur_date_lien" TO "uk_predecesseur_successeur_date_lien";--> statement-breakpoint
ALTER TABLE "action_echange" RENAME CONSTRAINT "fk_action_Echange__evenement" TO "fk_action_echange__evenement";
--> statement-breakpoint