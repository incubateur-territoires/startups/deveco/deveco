--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "etablissement_subvention" (
	"etablissement_id" varchar(14) NOT NULL,
	"type" varchar NOT NULL,
	"objet" varchar NOT NULL,
	"convention_date" date NOT NULL,
	"montant" double precision NOT NULL
);
--> statement-breakpoint
ALTER TABLE "source"."dgcl__subventions" RENAME TO "dgcl__subvention";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_dgcl__subventions__siret";--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_etablissement_subvention__etablissement" ON "etablissement_subvention" ("etablissement_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_dgcl__subvention__siret" ON "source"."dgcl__subvention" ("siret");
--> statement-breakpoint
INSERT INTO etablissement_subvention (
	etablissement_id,
	type,
	objet,
	convention_date,
	montant
)
SELECT
	siret as etablissement_id,
	type,
	objet,
	convention_date::date,
	REPLACE(montant, ',', '.')::float
FROM source.dgcl__subvention;
--> statement-breakpoint
