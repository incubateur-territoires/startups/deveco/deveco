CREATE TABLE "source"."egapro__entreprise" (
	"siren" varchar(9) NOT NULL,
	"annee" integer NOT NULL,
	"note_ecart_remuneration" integer,
	"note_ecart_taux__augmentation_hors_promotion" integer,
	"note_ecart_taux_promotion" integer,
	"note_ecart_taux_augmentation" integer,
	"note_retour_conge_maternite" integer,
	"note_hautes_remunerations" integer,
	"note_index" integer,
	"cadres_pourcentage_femmes" double precision,
	"cadres_pourcentage_hommes" double precision,
	"membres_pourcentage_femmes" double precision,
	"membres_pourcentage_hommes" double precision,
	"mise_a_jour_at" timestamp with time zone DEFAULT now() NOT NULL,
	CONSTRAINT "pk_egapro__entreprise" PRIMARY KEY("siren","annee")
);
--> statement-breakpoint
DROP TABLE "sexe_type" CASCADE;