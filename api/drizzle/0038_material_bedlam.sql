ALTER TABLE "etablissement_effectif_ema" RENAME TO "etablissement_effectif_moyen_annuel";--> statement-breakpoint
ALTER TABLE "etablissement_effectif_emm" RENAME TO "etablissement_effectif_moyen_mensuel";--> statement-breakpoint
ALTER INDEX "IDX_etablissement_effectif_ema__etablissement" RENAME TO "idx_etablissement_effectif_moyen_annuel__etablissement";--> statement-breakpoint
ALTER INDEX "idx_etablissement_effectif_emm__etablissement" RENAME TO "idx_etablissement_effectif_moyen_mensuel__etablissement";--> statement-breakpoint
ALTER TABLE "etablissement_effectif_moyen_mensuel" RENAME CONSTRAINT "pk_etablissement_effectif_emm" TO "pk_etablissement_effectif_moyen_mensuel";--> statement-breakpoint

ALTER TABLE "etablissement_effectif_moyen_annuel" ADD CONSTRAINT "pk_etablissement_effectif_moyen_annuel" PRIMARY KEY("etablissement_id","annee","mois");--> statement-breakpoint
