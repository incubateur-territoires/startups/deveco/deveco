CREATE TABLE IF NOT EXISTS "source"."rcd__etablissement_effectif_emm_13_mois_annuel" (
	"siret" varchar(14) NOT NULL,
	"date" date NOT NULL,
	"effectif" double precision,
	CONSTRAINT "pk_rcd__etablissement_effectif_emm_13_mois_annuel" PRIMARY KEY("siret","date")
);
--> statement-breakpoint
DROP TABLE "etablissement_effectif_moyen_annuel";--> statement-breakpoint
DROP TABLE "etablissement_effectif_moyen_mensuel";--> statement-breakpoint
ALTER TABLE "source"."rcd__etablissement_effectif_emm_13_mois" ALTER COLUMN "effectif" SET DATA TYPE double precision USING effectif::double precision;--> statement-breakpoint
ALTER TABLE "source"."rcd__etablissement_effectif_emm_13_mois" ADD CONSTRAINT "pk_rcd__etablissement_effectif_emm_13_mois" PRIMARY KEY("siret","date");--> statement-breakpoint
ALTER TABLE "source"."rcd__etablissement_effectif_emm_historique" RENAME COLUMN "mois" TO "date";--> statement-breakpoint
ALTER TABLE "source"."rcd__etablissement_effectif_emm_historique" ALTER COLUMN "effectif" SET DATA TYPE double precision USING effectif::double precision;--> statement-breakpoint
ALTER TABLE "source"."rcd__etablissement_effectif_emm_historique" ADD CONSTRAINT "pk_rcd__etablissement_effectif_emm_historique" PRIMARY KEY("siret","date");--> statement-breakpoint
\copy source.rcd__etablissement_effectif_emm_13_mois from './_sources/rcd__etablissement_effectif.csv' csv header;--> statement-breakpoint
