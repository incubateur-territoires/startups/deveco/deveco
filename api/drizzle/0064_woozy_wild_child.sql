CREATE TABLE IF NOT EXISTS "equipe__local__occupant" (
	"local_id" varchar(12) NOT NULL,
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	CONSTRAINT "pk_equipe__local__occupant" PRIMARY KEY("local_id","equipe_id","etablissement_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "action_equipe_local_occupant" (
	"evenement_id" integer NOT NULL,
	"created_at" timestamp with time zone DEFAULT now() NOT NULL,
	"action_type_id" varchar NOT NULL,
	"diff" jsonb,
	"equipe_id" integer NOT NULL,
	"local_id" varchar(12) NOT NULL,
	"occupant_id" varchar(14) NOT NULL
);
--> statement-breakpoint
ALTER TABLE "equipe__local" ADD COLUMN "contact_noms" varchar;--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_equipe_local_occupant" ON "action_equipe_local_occupant" ("evenement_id","equipe_id","local_id","occupant_id","action_type_id");--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local__occupant" ADD CONSTRAINT "fk_equipe__local__occupant__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local__occupant" ADD CONSTRAINT "fk_equipe__local__occupant__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local__occupant" ADD CONSTRAINT "fk_equipe__local__occupant__equipe_local" FOREIGN KEY ("equipe_id","local_id") REFERENCES "equipe__local"("equipe_id","local_id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local__occupant" ADD CONSTRAINT "fk_equipe__local__occupant__occupant" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local_occupant" ADD CONSTRAINT "fk_action_equipe_local_occupant__equipe_local" FOREIGN KEY ("equipe_id","local_id") REFERENCES "equipe__local"("equipe_id","local_id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local_occupant" ADD CONSTRAINT "fk_action_equipe_local_occupant__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local_occupant" ADD CONSTRAINT "fk_action_equipe_local_occupant__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local_occupant" ADD CONSTRAINT "fk_action_equipe_local_occupant__occupant" FOREIGN KEY ("occupant_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local_occupant" ADD CONSTRAINT "fk_action_equipe_local_occupant__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local_occupant" ADD CONSTRAINT "fk_action_equipe_local_occupant__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
