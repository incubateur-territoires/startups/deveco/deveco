ALTER TABLE "personne" RENAME TO "contact";--> statement-breakpoint
ALTER TABLE "personne_adresse" RENAME TO "contact_adresse";--> statement-breakpoint
ALTER TABLE "createur" RENAME TO "etablissement_creation__createur";--> statement-breakpoint
ALTER TABLE "action_personne" RENAME TO "action_contact_personne";--> statement-breakpoint
ALTER TABLE "action_contact" RENAME TO "action_equipe_etablissement_contact";--> statement-breakpoint
ALTER TABLE "equipe_etablissement__contact" RENAME COLUMN "personne_id" TO "contact_id";--> statement-breakpoint
ALTER TABLE "equipe_local__contact" RENAME COLUMN "personne_id" TO "contact_id";--> statement-breakpoint
ALTER TABLE "action_equipe_local_contact" RENAME COLUMN "personne_id" TO "contact_id";--> statement-breakpoint
ALTER TABLE "contact_adresse" RENAME COLUMN "personne_id" TO "contact_id";--> statement-breakpoint
ALTER TABLE "etablissement_creation__createur" RENAME COLUMN "personne_id" TO "contact_id";--> statement-breakpoint
ALTER TABLE "action_contact_personne" RENAME COLUMN "personne_id" TO "contact_id";--> statement-breakpoint
ALTER TABLE "action_equipe_etablissement_contact" RENAME COLUMN "personne_id" TO "contact_id";--> statement-breakpoint
ALTER TABLE "equipe_etablissement__contact" DROP CONSTRAINT "equipe_etablissement__contact_personne_id_personne_id_fk";
--> statement-breakpoint
ALTER TABLE "equipe_local__contact" DROP CONSTRAINT "fk_equipe_local__contact__personne";
--> statement-breakpoint
ALTER TABLE "action_equipe_local_contact" DROP CONSTRAINT "fk_action_equipe_local_contact__personne";
--> statement-breakpoint
ALTER TABLE "action_contact_personne" DROP CONSTRAINT "fk_action_personne_evenement";
--> statement-breakpoint
ALTER TABLE "action_contact_personne" DROP CONSTRAINT "fk_action_personne__action_type";
--> statement-breakpoint
ALTER TABLE "action_equipe_etablissement_contact" DROP CONSTRAINT "fk_action_contact__contact";
--> statement-breakpoint
ALTER TABLE "action_equipe_etablissement_contact" DROP CONSTRAINT "fk_action_contact__evenement";
--> statement-breakpoint
ALTER TABLE "action_equipe_etablissement_contact" DROP CONSTRAINT "fk_action_contact__action_type";
--> statement-breakpoint
DROP INDEX IF EXISTS "idx_action_personne";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_action_contact";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_action_equipe_local_contact";--> statement-breakpoint
ALTER TABLE "etablissement_creation__createur" RENAME CONSTRAINT "createur_pkey" to  "pk_etablissement_creation__createur";--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_contact" ON "action_contact_personne" ("evenement_id","contact_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_equipe_etablissement_contact" ON "action_equipe_etablissement_contact" ("evenement_id","equipe_id","etablissement_id","contact_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_equipe_local_contact" ON "action_equipe_local_contact" ("evenement_id","equipe_id","local_id","contact_id","action_type_id");--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_etablissement__contact" ADD CONSTRAINT "equipe_etablissement__contact_contact_id_contact_id_fk" FOREIGN KEY ("contact_id") REFERENCES "contact"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local__contact" ADD CONSTRAINT "fk_equipe_local__contact__contact" FOREIGN KEY ("contact_id") REFERENCES "contact"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local_contact" ADD CONSTRAINT "fk_action_equipe_local_contact__contact" FOREIGN KEY ("contact_id") REFERENCES "contact"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "contact" ADD CONSTRAINT "contact_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "contact_adresse" ADD CONSTRAINT "contact_adresse_commune_id_commune_id_fk" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation__createur" ADD CONSTRAINT "fk_etablissement_creation__createur__contact" FOREIGN KEY ("contact_id") REFERENCES "contact"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation__createur" ADD CONSTRAINT "fk_etablissement_creation__createur__etablissement_creation" FOREIGN KEY ("etablissement_creation_id") REFERENCES "etablissement_creation"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_contact_personne" ADD CONSTRAINT "fk_action_contact_evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_contact_personne" ADD CONSTRAINT "fk_action_contact__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_etablissement_contact" ADD CONSTRAINT "fk_action_equipe_etablissement_contact__contact" FOREIGN KEY ("equipe_id","etablissement_id","contact_id") REFERENCES "equipe_etablissement__contact"("equipe_id","etablissement_id","contact_id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_etablissement_contact" ADD CONSTRAINT "fk_action_equipe_etablissement_contact__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_etablissement_contact" ADD CONSTRAINT "fk_action_equipe_etablissement_contact__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
ALTER TABLE "local" DROP COLUMN IF EXISTS "surface_totale";--> statement-breakpoint
