CREATE TABLE IF NOT EXISTS "notifications" (
	"id" serial NOT NULL,
	"deveco_id" integer NOT NULL,
	"connexion" boolean DEFAULT true NOT NULL,
	"rappels" boolean DEFAULT true NOT NULL,
	"activite" boolean DEFAULT true NOT NULL,
	CONSTRAINT "pk_notifications" PRIMARY KEY("id"),
	CONSTRAINT "uk_notifications_deveco" UNIQUE("deveco_id")
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "notifications" ADD CONSTRAINT "fk_notifications__deveco" FOREIGN KEY ("deveco_id") REFERENCES "deveco"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
