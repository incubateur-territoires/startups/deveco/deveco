DO $$ BEGIN
 ALTER TABLE "etablissement" ADD CONSTRAINT "fk_etablissement__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
