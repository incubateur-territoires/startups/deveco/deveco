ALTER TABLE "etablissement_favori" RENAME TO "deveco__etablissement_favori";--> statement-breakpoint
ALTER TABLE "equipe_etablissement__contact" RENAME TO "equipe__etablissement__contact";--> statement-breakpoint
ALTER TABLE "categorie_entreprise_type" RENAME TO "entreprise_categorie_type";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" RENAME COLUMN "categorie_entreprise_type_id" TO "entreprise_categorie_type_id";--> statement-breakpoint
ALTER TABLE "entreprise" RENAME COLUMN "categorie_entreprise_type_id" TO "entreprise_categorie_type_id";--> statement-breakpoint
ALTER TABLE "entreprise" RENAME COLUMN "categorie_entreprise_annee" TO "entreprise_categorie_annee";--> statement-breakpoint
ALTER TABLE "contact" DROP CONSTRAINT "contact_equipe_id_equipe_id_fk";
--> statement-breakpoint
ALTER TABLE "contact_adresse" DROP CONSTRAINT "contact_adresse_commune_id_commune_id_fk";
--> statement-breakpoint
ALTER TABLE "action_echange" RENAME CONSTRAINT "fk_action_Echange__action_type" TO "fk_action_echange__action_type";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement__contact" DROP CONSTRAINT "equipe_etablissement__contact_equipe_id_equipe_id_fk";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement__contact" DROP CONSTRAINT "equipe_etablissement__contact_contact_id_contact_id_fk";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement" RENAME CONSTRAINT "equipe__etablissement_pkey" TO "pk_equipe__etablissement";--> statement-breakpoint
ALTER TABLE "etablissement_adresse_1" ALTER COLUMN "commune_id" DROP NOT NULL;--> statement-breakpoint
ALTER TABLE "etablissement_adresse_1_etalab" ALTER COLUMN "commune_id" DROP NOT NULL;--> statement-breakpoint
ALTER TABLE "compte" RENAME CONSTRAINT "compte_pkey" TO "pk_compte";--> statement-breakpoint

ALTER TABLE "nouveautes" RENAME CONSTRAINT "PK_31d64ed71eccdac9ee6846bde38" TO "pk_nouveautes";--> statement-breakpoint
ALTER TABLE "tache" RENAME CONSTRAINT "tache_pkey" TO "pk_tache";--> statement-breakpoint
ALTER TABLE "contact" RENAME CONSTRAINT "personne_pkey" TO "pk_contact";--> statement-breakpoint
ALTER TABLE "contact" RENAME CONSTRAINT "fk_personne__equipe" TO "fk_contact__equipe";--> statement-breakpoint
ALTER TABLE "contact_adresse" ADD CONSTRAINT "pk_contact_adresse" PRIMARY KEY("id");--> statement-breakpoint
ALTER TABLE "equipe__etablissement_contribution" RENAME CONSTRAINT "equipe__etablissement_description_pkey" TO "pk_equipe__etablissement_contribution";--> statement-breakpoint
ALTER TABLE "equipe__etablissement__demande" RENAME CONSTRAINT "equipe__etablissement__demande_pkey" TO "pk_equipe__etablissement__demande";--> statement-breakpoint
ALTER TABLE "equipe__etablissement__echange" RENAME CONSTRAINT "equipe__etablissement__echange_pkey" TO "pk_equipe__etablissement__echange";--> statement-breakpoint
ALTER TABLE "equipe__etablissement__etiquette" RENAME CONSTRAINT "pk_equipe_etablissement_etiquette" TO "pk_equipe__etablissement__etiquette" ;--> statement-breakpoint
ALTER TABLE "equipe__etablissement_exogene" RENAME CONSTRAINT "equipe__etablissement_exogene_pkey" TO "pk_equipe__etablissement_exogene";--> statement-breakpoint
ALTER TABLE "equipe__etablissement__rappel" RENAME CONSTRAINT "equipe__etablissement__rappel_pkey" TO "pk_equipe__etablissement__rappel";--> statement-breakpoint
ALTER TABLE "local_adresse" ADD CONSTRAINT "pk_local_adresse" PRIMARY KEY("id");--> statement-breakpoint
ALTER TABLE "deveco__etablissement_favori" RENAME CONSTRAINT "etablissement_favori_pkey" TO "pk_deveco__etablissement_favori";--> statement-breakpoint
ALTER TABLE "equipe__etablissement__contact" RENAME CONSTRAINT "pk_equipe_etablissement__contact" TO "pk_equipe__etablissement__contact";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_compte__prenom";--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_compte__email" ON "compte" ("email");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_compte__identifiant" ON "compte" ("identifiant");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_qpv__geometrie" ON "qpv" ("geometrie");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_equipe__etablissement_exogene__equipe" ON "equipe__etablissement_exogene" ("equipe_id");--> statement-breakpoint
ALTER INDEX "idx_contact_personne" RENAME TO "idx_equipe__etablissement__contact__contact";--> statement-breakpoint
ALTER INDEX "idx_contact_etablissement" RENAME TO "idx_equipe__etablissement__contact__etablissement";--> statement-breakpoint
ALTER INDEX "idx_contact_equipe" RENAME TO "idx_equipe__etablissement__contact__equipe";--> statement-breakpoint
ALTER TABLE "mot_de_passe" RENAME CONSTRAINT "fk_password__compte" TO "fk_mot_de_passe__compte";
--> statement-breakpoint
ALTER TABLE "mot_de_passe" RENAME CONSTRAINT "uk_password__compte" TO "uk_mot_de_passe";--> statement-breakpoint
ALTER TABLE "mot_de_passe" RENAME CONSTRAINT "PK_cbeb55948781be9257f44febfa0" TO "pk_mot_de_passe";--> statement-breakpoint
ALTER TABLE "adresse" RENAME CONSTRAINT "adresse_pkey" TO "pk_adresse";--> statement-breakpoint

ALTER TABLE "contact_adresse" RENAME CONSTRAINT "uk_personne_adresse__personne_id" TO "uk_contact_adresse__contact";--> statement-breakpoint
ALTER TABLE "contact_adresse" RENAME CONSTRAINT "fk_personne_adresse__personne" TO "fk_contact_adresse__contact";--> statement-breakpoint
 ALTER TABLE "commune__qpv" RENAME CONSTRAINT "fk_commune__qpv_commune" TO "fk_commune__qpv__commune";
--> statement-breakpoint
 ALTER TABLE "commune__qpv" RENAME CONSTRAINT "fk_commune__qpv_qpv" TO "fk_commune__qpv__qpv";
--> statement-breakpoint


 ALTER TABLE "equipe__etablissement" RENAME CONSTRAINT "fk_equipe__etablissement__categorie_entreprise_type" TO "fk_equipe__etablissement__entreprise_categorie_type";


 ALTER TABLE "local_adresse" RENAME CONSTRAINT "local_adresse_commune_id_commune_id_fk" TO "fk_local_adresse_adresse__commune";
--> statement-breakpoint
 ALTER TABLE "deveco__etablissement_favori" RENAME CONSTRAINT "fk_etablissement_favori__deveco" TO "fk_deveco__etablissement_favori__deveco";
--> statement-breakpoint
 ALTER TABLE "deveco__etablissement_favori" RENAME CONSTRAINT "fk_etablissement_favori__etablissement" TO "fk_deveco__etablissement_favori__etablissement";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement__contact" RENAME CONSTRAINT "fk_contact__equipe" TO "fk_equipe__etablissement__contact__equipe";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement__contact" RENAME CONSTRAINT "fk_contact__etablissement" TO "fk_equipe__etablissement__contact__etablissement";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement__contact" RENAME CONSTRAINT "fk_contact__personne" TO "fk_equipe__etablissement__contact__contact";
--> statement-breakpoint
--> statement-breakpoint

DO $$ BEGIN
 ALTER TABLE "equipe__etablissement_exogene" ADD CONSTRAINT "fk_equipe__etablissement_exogene__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement_exogene" ADD CONSTRAINT "fk_equipe__etablissement_exogene__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement_portefeuille" ADD CONSTRAINT "fk_equipe__etablissement_portefeuille__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement_portefeuille" ADD CONSTRAINT "fk_equipe__etablissement_portefeuille__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_adresse_1_etalab" ADD CONSTRAINT "fk_etablissement_adresse_1_etalab__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint

ALTER TABLE "etablissement_adresse_1" ADD CONSTRAINT "pk_etablissement_adresse_1" PRIMARY KEY("id");--> statement-breakpoint
ALTER TABLE "etablissement_adresse_1_etalab" ADD CONSTRAINT "pk_etablissement_adresse_1_etalab_adresse" PRIMARY KEY("id");--> statement-breakpoint
