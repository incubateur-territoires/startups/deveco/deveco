ALTER TABLE "source"."api_entreprise__etablissement_exercice" DROP CONSTRAINT pk_api_entreprise__etablissement_exercice; --> statement-breakpoint

ALTER TABLE "source"."api_entreprise__etablissement_exercice" ADD CONSTRAINT pk_api_entreprise__etablissement_exercice PRIMARY KEY(siret,date_fin_exercice); --> statement-breakpoint

ALTER TABLE "source"."api_entreprise__entreprise_beneficiaire_effectif" ADD CONSTRAINT pk_api_entreprise__entreprise_beneficiaire_effectif PRIMARY KEY(entreprise_id); --> statement-breakpoint

ALTER TABLE "source"."api_entreprise__entreprise_mandataire_social" ADD CONSTRAINT pk_api_entreprise__entreprise_mandataire_social PRIMARY KEY(entreprise_id); --> statement-breakpoint
