ALTER TABLE "source"."insee_sirene__entreprise_geoloc" RENAME TO "insee_sirene__etablissement_geoloc";--> statement-breakpoint
ALTER TABLE "entreprise_exercice" DROP CONSTRAINT "uk_entreprise_exercice";--> statement-breakpoint
ALTER TABLE "etablissement_lien" DROP CONSTRAINT "fk_etablissement_lien__successeur";
--> statement-breakpoint
ALTER TABLE "etablissement_lien" DROP CONSTRAINT "fk_etablissement_lien__predecesseur";
--> statement-breakpoint
ALTER TABLE "source"."insee_sirene__etablissement_geoloc" DROP CONSTRAINT "pk_insee_sirene__entreprise_geoloc";--> statement-breakpoint
ALTER TABLE "source"."insee_sirene__etablissement_geoloc" ADD CONSTRAINT "pk_insee_sirene__etablissement_geoloc" PRIMARY KEY("siret");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_entreprise_exercice__entreprise" ON "entreprise_exercice" ("entreprise_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_entreprise_procedure_collective__entreprise" ON "entreprise_procedure_collective" ("entreprise_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_etablissement_lien__predecesseur" ON "etablissement_lien" ("predecesseur_etablissement_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_etablissement_lien__successeur" ON "etablissement_lien" ("successeur_etablissement_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_local__proprietaire_personne_morale__local" ON "local__proprietaire_personne_morale" ("local_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_local__proprietaire_personne_morale__proprio" ON "local__proprietaire_personne_morale" ("proprietaire_personne_morale_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_local__proprietaire_personne_physique__local" ON "local__proprietaire_personne_physique" ("local_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_local__proprietaire_personne_physique__proprio" ON "local__proprietaire_personne_physique" ("proprietaire_personne_physique_id");--> statement-breakpoint
ALTER TABLE "entreprise_exercice" DROP CONSTRAINT pk_entreprise_exercice;
--> statement-breakpoint
ALTER TABLE "entreprise_exercice" ADD CONSTRAINT pk_entreprise_exercice PRIMARY KEY(entreprise_id,cloture_date);
