CREATE TABLE "source"."api_entreprise__entreprise_exercice" (
	"siren" varchar(9) NOT NULL,
	"payload" jsonb NOT NULL,
	"mise_a_jour_at" timestamp with time zone DEFAULT now(),
	CONSTRAINT "pk_api_entreprise__entreprise_exercice" PRIMARY KEY("siren")
);
--> statement-breakpoint
CREATE TABLE "source"."api_entreprise__entreprise_liasse_fiscale" (
	"siren" varchar(9) NOT NULL,
	"payload" jsonb NOT NULL,
	"mise_a_jour_at" timestamp with time zone DEFAULT now(),
	"reseau_fait" boolean DEFAULT false,
	CONSTRAINT "pk_api_entreprise__entreprise_liasse_fiscale" PRIMARY KEY("siren")
);
--> statement-breakpoint
ALTER TABLE "source"."api_entreprise__etablissement_exercice" DISABLE ROW LEVEL SECURITY;
--> statement-breakpoint
ALTER TABLE "beneficiaire_effectif" DISABLE ROW LEVEL SECURITY;
--> statement-breakpoint
ALTER TABLE "entreprise_exercice" DISABLE ROW LEVEL SECURITY;
--> statement-breakpoint
ALTER TABLE "mandataire_personne_morale" DISABLE ROW LEVEL SECURITY;
--> statement-breakpoint
ALTER TABLE "mandataire_personne_physique" DISABLE ROW LEVEL SECURITY;
--> statement-breakpoint
DROP MATERIALIZED VIEW "public"."etablissement_geolocalisation_vue";
--> statement-breakpoint
INSERT INTO "source"."api_entreprise__entreprise_exercice"
SELECT DISTINCT LEFT(siret, 9) AS siren,
	'[]'::jsonb AS payload,
	NULL::timestamptz AS mise_a_jour_at
FROM "source"."api_entreprise__etablissement_exercice";
--> statement-breakpoint
DROP TABLE "source"."api_entreprise__etablissement_exercice" CASCADE;
--> statement-breakpoint
DROP TABLE "beneficiaire_effectif" CASCADE;
--> statement-breakpoint
DROP TABLE "entreprise_exercice" CASCADE;
--> statement-breakpoint
DROP TABLE "mandataire_personne_morale" CASCADE;
--> statement-breakpoint
DROP TABLE "mandataire_personne_physique" CASCADE;
--> statement-breakpoint
ALTER TABLE "source"."api_entreprise__entreprise_beneficiaire_effectif"
	RENAME COLUMN "entreprise_id" TO "siren";
--> statement-breakpoint
ALTER TABLE "source"."api_entreprise__entreprise_mandataire_social"
	RENAME COLUMN "entreprise_id" TO "siren";
--> statement-breakpoint
ALTER TABLE "source"."api_entreprise__entreprise_beneficiaire_effectif"
ADD COLUMN "mise_a_jour_at" timestamp with time zone DEFAULT now();
--> statement-breakpoint
ALTER TABLE "source"."api_entreprise__entreprise_mandataire_social"
ADD COLUMN "mise_a_jour_at" timestamp with time zone DEFAULT now();
--> statement-breakpoint
ALTER TABLE "source"."api_entreprise__entreprise_beneficiaire_effectif" DROP CONSTRAINT "pk_api_entreprise__entreprise_beneficiaire_effectif";
--> statement-breakpoint
ALTER TABLE "source"."api_entreprise__entreprise_beneficiaire_effectif"
ADD CONSTRAINT "pk_api_entreprise__entreprise_beneficiaire_effectif" PRIMARY KEY("siren");
--> statement-breakpoint
ALTER TABLE "source"."api_entreprise__entreprise_mandataire_social" DROP CONSTRAINT "pk_api_entreprise__entreprise_mandataire_social";
--> statement-breakpoint
ALTER TABLE "source"."api_entreprise__entreprise_mandataire_social"
ADD CONSTRAINT "pk_api_entreprise__entreprise_mandataire_social" PRIMARY KEY("siren");
--> statement-breakpoint
ALTER TABLE "equipe"
ADD CONSTRAINT "uk_equipe_nom" UNIQUE("equipe_type_id", "nom");
--> statement-breakpoint
CREATE MATERIALIZED VIEW "public"."etablissement_geolocalisation_vue" AS (
	SELECT "source"."insee_sirene_api__etablissement"."siret" AS etabgeovue_siret,
		COALESCE(
			ST_SetSRID(
				ST_MakePoint(
					"source"."insee_sirene__etablissement_geoloc"."x_longitude",
					"source"."insee_sirene__etablissement_geoloc"."y_latitude"
				),
				4326
			),
			"source"."etalab__etablissement_adresse"."geolocalisation"
		) AS etabgeovue_geolocalisation,
		"source"."insee_sirene_api__etablissement"."mise_a_jour_at" AS etabgeovue_mise_a_jour_at
	FROM "source"."insee_sirene_api__etablissement"
		LEFT JOIN "source"."insee_sirene__etablissement_geoloc" ON "source"."insee_sirene__etablissement_geoloc"."siret" = "source"."insee_sirene_api__etablissement"."siret"
		LEFT JOIN "source"."etalab__etablissement_adresse" ON "source"."etalab__etablissement_adresse"."siret" = "source"."insee_sirene_api__etablissement"."siret"
	WHERE "source"."insee_sirene__etablissement_geoloc"."siret" IS NOT NULL
		OR "source"."etalab__etablissement_adresse"."siret" IS NOT NULL
);
--> statement-breakpoint
CREATE UNIQUE INDEX idx_etablissement_geolocalisation_vue_etablissement ON etablissement_geolocalisation_vue (etabgeovue_siret);
--> statement-breakpoint
CREATE INDEX idx_etablissement_geolocalisation_vue_etablissement ON etablissement_geolocalisation_vue USING gist (etabgeovue_geolocalisation);