CREATE TABLE IF NOT EXISTS "action_etiquette__local" (
	"evenement_id" integer NOT NULL,
	"created_at" timestamp with time zone DEFAULT now() NOT NULL,
	"action_type_id" varchar NOT NULL,
	"diff" jsonb,
	"etiquette_id" integer NOT NULL,
	"equipe_id" integer NOT NULL,
	"local_id" varchar(12) NOT NULL
);
--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_etiquette_local" ON "action_etiquette__local" ("evenement_id","etiquette_id","local_id","action_type_id");--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette__local" ADD CONSTRAINT "fk_action_etiquette_local__etiquette" FOREIGN KEY ("etiquette_id") REFERENCES "etiquette"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette__local" ADD CONSTRAINT "fk_action_etiquette_local__equipe" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette__local" ADD CONSTRAINT "fk_action_etiquette_local__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette__local" ADD CONSTRAINT "fk_action_etiquette_local__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette__local" ADD CONSTRAINT "fk_action_etiquette_local__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE restrict ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
