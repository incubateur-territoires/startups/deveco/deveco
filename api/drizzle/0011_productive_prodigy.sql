CREATE TABLE IF NOT EXISTS "entreprise_ess" (
	"entreprise_id" varchar(9) PRIMARY KEY NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "entreprise_ratio_financier" (
	"entreprise_id" varchar(9),
	"exercice_cloture_date" date NOT NULL,
	"chiffre_d_affaires" double precision,
	"marge_brute" double precision,
	"ebe" double precision,
	"ebit" double precision,
	"resultat_net" double precision
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "commune_acv" (
	"commune_id" varchar(5) PRIMARY KEY NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "commune_pvd" (
	"commune_id" varchar(5) PRIMARY KEY NOT NULL
);
--> statement-breakpoint
ALTER TABLE "source"."source_inpi_ratios_financiers" RENAME TO "source_inpi_ratio_financier";--> statement-breakpoint
ALTER TABLE "entreprise_exercice" RENAME COLUMN "chiffre_affaires" TO "chiffre_d_affaires";--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "entreprise_ess" ADD CONSTRAINT "fk_entreprise_ess__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "entreprise_ratio_financier" ADD CONSTRAINT "fk_entreprise_ratio_financier__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "entreprise"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune_acv" ADD CONSTRAINT "fk_commune_acv__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "commune_pvd" ADD CONSTRAINT "fk_commune_pvd__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

INSERT INTO entreprise_ess (
	"entreprise_id"
)
SELECT DISTINCT
	"entreprise_id"
FROM source.source_ess_france;

INSERT INTO entreprise_ratio_financier (
	"entreprise_id",
	"exercice_cloture_date",
	"chiffre_d_affaires",
	"marge_brute",
	"ebe",
	"ebit",
	"resultat_net"
)
SELECT
	"siren",
	"date_cloture_exercice",
	"chiffre_d_affaires",
	"marge_brute",
	"ebe",
	"ebit",
	"resultat_net"
FROM source.source_inpi_ratio_financier;

INSERT INTO commune_acv (
	"commune_id"
)
SELECT
	"commune_id"
FROM source.source_acv;

INSERT INTO commune_pvd (
	"commune_id"
)
SELECT
	"commune_id"
FROM source.source_pvd;
