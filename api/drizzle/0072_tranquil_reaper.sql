CREATE TABLE IF NOT EXISTS "equipe__local_ressource" (
	"id" serial NOT NULL,
	"equipe_id" integer NOT NULL,
	"local_id" varchar(14) NOT NULL,
	"nom" varchar,
	"lien" varchar,
	"ressource_type_id" varchar,
	CONSTRAINT "pk_equipe__local_ressource" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_equipe_local_ressource" ON "equipe__local_ressource" ("equipe_id","local_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_equipe_etablissement_ressource" ON "equipe__etablissement_ressource" ("equipe_id","etablissement_id");--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local_ressource" ADD CONSTRAINT "fk_equipe__local_ressource__ressource_type" FOREIGN KEY ("ressource_type_id") REFERENCES "ressource_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
