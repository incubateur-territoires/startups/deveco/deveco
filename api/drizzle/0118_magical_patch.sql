DROP MATERIALIZED VIEW "public"."equipe__commune_vue";--> statement-breakpoint
CREATE MATERIALIZED VIEW "public"."equipe__commune_vue" AS (
	select distinct
		"equipe"."id" as "eqcomvue_equipe_id",
		"commune"."id" as "eqcomvue_commune_id"
	from "equipe"
	left join "equipe__commune" on "equipe__commune"."equipe_id" = "equipe"."id"
	left join "equipe__metropole" on "equipe__metropole"."equipe_id" = "equipe"."id"
	left join "equipe__epci" on "equipe__epci"."equipe_id" = "equipe"."id"
	left join "equipe__petr" on "equipe__petr"."equipe_id" = "equipe"."id"
	left join "equipe__territoire_industrie" on "equipe__territoire_industrie"."equipe_id" = "equipe"."id"
	left join "equipe__departement" on "equipe__departement"."equipe_id" = "equipe"."id"
	left join "equipe__region" on "equipe__region"."equipe_id" = "equipe"."id"
	left join "commune" on (
		"commune"."id" = "equipe__commune"."commune_id"
		or "commune"."commune_parent_id" = "equipe__commune"."commune_id"
		or "commune"."metropole_id" = "equipe__metropole"."metropole_id"
		or "commune"."epci_id" = "equipe__epci"."epci_id"
		or "commune"."petr_id" = "equipe__petr"."petr_id"
		or "commune"."territoire_industrie_id" = "equipe__territoire_industrie"."territoire_industrie_id"
		or "commune"."departement_id" = "equipe__departement"."departement_id"
		or "commune"."region_id" = "equipe__region"."region_id"
	)
);
CREATE UNIQUE INDEX ON equipe__commune_vue(eqcomvue_equipe_id, eqcomvue_commune_id);
CREATE INDEX idx_equipe__commune_vue_commune ON equipe__commune_vue (eqcomvue_commune_id);
CREATE INDEX idx_equipe__commune_vue_equipe ON equipe__commune_vue (eqcomvue_equipe_id);
CLUSTER equipe__commune_vue USING idx_equipe__commune_vue_equipe;
