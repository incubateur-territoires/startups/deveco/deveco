CREATE TABLE IF NOT EXISTS "erreur_front" (
	"id" serial NOT NULL,
	"compte_id" integer NOT NULL,
	"message" varchar NOT NULL,
	"date" timestamp DEFAULT now() NOT NULL,
	CONSTRAINT "pk_erreur_front" PRIMARY KEY("id")
);
