-- pour éviter des erreurs
-- ERROR:  column "numero" of relation "bodacc__procedure_collective" contains null values
-- on supprime tout le contenu des table existantes

-- truncate source.bodacc__procedure_collective;
-- truncate entreprise_procedure_collective;


CREATE TABLE IF NOT EXISTS "source"."bodacc__procedure_collective_import" (
	"annee" integer NOT NULL,
	"numero" integer NOT NULL,
	"mise_a_jour_at" timestamp with time zone DEFAULT now(),
	CONSTRAINT "uk_bodacc__procedure_collective_import" UNIQUE("annee","numero")
);
--> statement-breakpoint
ALTER TABLE "entreprise_procedure_collective" DROP CONSTRAINT "uk_entreprise_procedure_collective";--> statement-breakpoint
ALTER TABLE "source"."bodacc__procedure_collective" ALTER COLUMN "siren" SET NOT NULL;--> statement-breakpoint
ALTER TABLE "entreprise_procedure_collective" ADD COLUMN "numero" integer NOT NULL;--> statement-breakpoint
ALTER TABLE "source"."bodacc__procedure_collective" ADD COLUMN "numero" integer NOT NULL;--> statement-breakpoint
ALTER TABLE "source"."bodacc__procedure_collective" ADD COLUMN "mise_a_jour_at" timestamp with time zone DEFAULT now();--> statement-breakpoint
ALTER TABLE "entreprise_procedure_collective" ADD CONSTRAINT "uk_entreprise_procedure_collective" UNIQUE("entreprise_id","famille","nature","date","numero");
