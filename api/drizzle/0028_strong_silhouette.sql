ALTER TABLE "contact" RENAME TO "equipe_etablissement__contact";--> statement-breakpoint
ALTER TABLE "proprietaire_personne" RENAME TO "equipe_local__contact";--> statement-breakpoint
ALTER TABLE "local" RENAME TO "equipe_local_contribution";--> statement-breakpoint
ALTER TABLE "local_adresse" RENAME TO "equipe_local_contribution_adresse";--> statement-breakpoint
ALTER TABLE "local__local_type" RENAME TO "equipe_local_contribution__equipe_local_contribution_type";--> statement-breakpoint
ALTER TABLE "local_statut_type" RENAME TO "equipe_local_contribution_statut_type";--> statement-breakpoint
ALTER TABLE "local_type" RENAME TO "equipe_local_contribution_type";--> statement-breakpoint
ALTER TABLE "local__etiquette" RENAME TO "equipe_local__etiquette";--> statement-breakpoint
ALTER TABLE "action_local" RENAME TO "action_equipe_local";--> statement-breakpoint
ALTER TABLE "action_proprietaire_personne" RENAME TO "action_equipe_local_contact";--> statement-breakpoint
ALTER TABLE "action_proprietaire_etablissement" RENAME COLUMN "local_id" TO "equipe_local_contribution_id";--> statement-breakpoint
ALTER TABLE "proprietaire_etablissement" RENAME COLUMN "local_id" TO "equipe_local_contribution_id";--> statement-breakpoint
ALTER TABLE "equipe_local__contact" RENAME COLUMN "local_id" TO "equipe_local_contribution_id";--> statement-breakpoint
ALTER TABLE "equipe_local_contribution" RENAME COLUMN "local_statut_type_id" TO "equipe_local_contribution_statut_type_id";--> statement-breakpoint
ALTER TABLE "equipe_local_contribution_adresse" RENAME COLUMN "local_id" TO "equipe_local_contribution_id";--> statement-breakpoint
ALTER TABLE "equipe_local_contribution__equipe_local_contribution_type" RENAME COLUMN "local_id" TO "equipe_local_contribution_id";--> statement-breakpoint
ALTER TABLE "equipe_local_contribution__equipe_local_contribution_type" RENAME COLUMN "local_type_id" TO "equipe_local_contribution_type_id";--> statement-breakpoint
ALTER TABLE "equipe_local__etiquette" RENAME COLUMN "local_id" TO "equipe_local_contribution_id";--> statement-breakpoint
ALTER TABLE "action_equipe_local" RENAME COLUMN "local_id" TO "equipe_local_contribution_id";--> statement-breakpoint
ALTER TABLE "action_equipe_local_contact" RENAME COLUMN "local_id" TO "equipe_local_contribution_id";--> statement-breakpoint
ALTER TABLE "action_contact" DROP CONSTRAINT "fk_action_contact__contact";
--> statement-breakpoint
ALTER TABLE "action_proprietaire_etablissement" DROP CONSTRAINT "fk_action_proprietaire_etablissement_local";
--> statement-breakpoint
ALTER TABLE "proprietaire_etablissement" DROP CONSTRAINT "proprietaire_etablissement_local_id_local_id_fk";
--> statement-breakpoint
ALTER TABLE "proprietaire_etablissement" DROP CONSTRAINT "proprietaire_etablissement_etablissement_id_etablissement_id_fk";
--> statement-breakpoint
ALTER TABLE "equipe_etablissement__contact" DROP CONSTRAINT "contact_personne_id_personne_id_fk";
--> statement-breakpoint
ALTER TABLE "equipe_local__contact" DROP CONSTRAINT "proprietaire_personne_local_id_local_id_fk";
--> statement-breakpoint
ALTER TABLE "equipe_local__contact" DROP CONSTRAINT "proprietaire_personne_personne_id_personne_id_fk";
--> statement-breakpoint
ALTER TABLE "equipe_local_contribution" DROP CONSTRAINT "local_local_statut_type_id_local_statut_type_id_fk";
--> statement-breakpoint
ALTER TABLE "equipe_local_contribution" DROP CONSTRAINT "local_equipe_id_equipe_id_fk";
--> statement-breakpoint
ALTER TABLE "equipe_local_contribution_adresse" DROP CONSTRAINT "local_adresse_commune_id_commune_id_fk";
--> statement-breakpoint
ALTER TABLE "equipe_local_contribution__equipe_local_contribution_type" DROP CONSTRAINT "local__local_type_local_id_local_id_fk";
--> statement-breakpoint
ALTER TABLE "equipe_local_contribution__equipe_local_contribution_type" DROP CONSTRAINT "local__local_type_local_type_id_local_type_id_fk";
--> statement-breakpoint
ALTER TABLE "equipe_local__etiquette" DROP CONSTRAINT "local__etiquette_local_id_local_id_fk";
--> statement-breakpoint
ALTER TABLE "equipe_local__etiquette" DROP CONSTRAINT "local__etiquette_etiquette_id_etiquette_id_fk";
--> statement-breakpoint
ALTER TABLE "action_equipe_local" DROP CONSTRAINT "fk_action_local__local";
--> statement-breakpoint
ALTER TABLE "action_equipe_local_contact" DROP CONSTRAINT "fk_action_proprietaire_personne_local";
--> statement-breakpoint
ALTER TABLE "action_equipe_local_contact" DROP CONSTRAINT "fk_proprietaire_personne__action_type";
--> statement-breakpoint
ALTER TABLE "action_equipe_local_contact" DROP CONSTRAINT "fk_action_proprietaire_personne__evenement";
--> statement-breakpoint
ALTER TABLE "action_equipe_local_contact" DROP CONSTRAINT "fk_action_proprietaire_personne_personne";

--> statement-breakpoint
DROP INDEX IF EXISTS "idx_action_local";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_action_proprietaire_personne";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_action_proprietaire_etablissement";--> statement-breakpoint

ALTER TABLE "equipe_local_contribution__equipe_local_contribution_type" ALTER COLUMN "equipe_local_contribution_type_id" SET DATA TYPE varchar;--> statement-breakpoint
ALTER TABLE "equipe__etablissement__etiquette" RENAME CONSTRAINT "equipe__etablissement__etiquette_pkey" TO "pk_equipe_etablissement_etiquette";--> statement-breakpoint
-- ALTER TABLE "proprietaire_etablissement" ADD CONSTRAINT "proprietaire_etablissement_equipe_local_contribution_id_etablissement_id_pk" PRIMARY KEY("equipe_local_contribution_id","etablissement_id");--> statement-breakpoint
ALTER TABLE "equipe_etablissement__contact" RENAME CONSTRAINT "contact_pkey" TO "pk_equipe_etablissement__contact";--> statement-breakpoint
ALTER TABLE "equipe_local__contact" RENAME CONSTRAINT "proprietaire_personne_pkey" TO "pk_equipe_local__contact";--> statement-breakpoint
ALTER TABLE "equipe_local_contribution" RENAME CONSTRAINT "local_pkey" TO "pk_equipe_local_contribution";--> statement-breakpoint
ALTER TABLE "equipe_local_contribution__equipe_local_contribution_type" RENAME CONSTRAINT "local__local_type_pkey" TO "pk_equipe_local_contribution__equipe_local_contribution_type";--> statement-breakpoint
ALTER TABLE "equipe_local__etiquette" RENAME CONSTRAINT "local__etiquette_pkey" TO "pk_equipe_local__etiquette";--> statement-breakpoint
ALTER TABLE "equipe_local_contribution" ADD COLUMN "local_id" integer;--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_equipe_local" ON "action_equipe_local" ("evenement_id","equipe_local_contribution_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_equipe_local_contact" ON "action_equipe_local_contact" ("evenement_id","equipe_local_contribution_id","personne_id","action_type_id");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "idx_action_proprietaire_etablissement" ON "action_proprietaire_etablissement" ("evenement_id","equipe_local_contribution_id","etablissement_id","action_type_id");--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_contact" ADD CONSTRAINT "fk_action_contact__contact" FOREIGN KEY ("equipe_id","etablissement_id","personne_id") REFERENCES "equipe_etablissement__contact"("equipe_id","etablissement_id","personne_id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_proprietaire_etablissement" ADD CONSTRAINT "fk_action_proprietaire_etablissement__equipe_local_contribution" FOREIGN KEY ("equipe_local_contribution_id") REFERENCES "equipe_local_contribution"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "proprietaire_etablissement" ADD CONSTRAINT "fk_proprietaire_etablissement__equipe_local_contribution" FOREIGN KEY ("equipe_local_contribution_id") REFERENCES "equipe_local_contribution"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "proprietaire_etablissement" ADD CONSTRAINT "fk_proprietaire_etablissement__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_etablissement__contact" ADD CONSTRAINT "equipe_etablissement__contact_equipe_id_equipe_id_fk" FOREIGN KEY ("equipe_id") REFERENCES "equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_etablissement__contact" ADD CONSTRAINT "equipe_etablissement__contact_personne_id_personne_id_fk" FOREIGN KEY ("personne_id") REFERENCES "personne"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local__contact" ADD CONSTRAINT "fk_equipe_local__contact__equipe_local_contribution" FOREIGN KEY ("equipe_local_contribution_id") REFERENCES "equipe_local_contribution"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local__contact" ADD CONSTRAINT "fk_equipe_local__contact__personne" FOREIGN KEY ("personne_id") REFERENCES "personne"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local_contribution" ADD CONSTRAINT "fk_equipe_local_contribution__statut_type" FOREIGN KEY ("equipe_local_contribution_statut_type_id") REFERENCES "equipe_local_contribution_statut_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local_contribution_adresse" ADD CONSTRAINT "equipe_local_contribution_adresse_commune_id_commune_id_fk" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local_contribution_adresse" ADD CONSTRAINT "fk_equipe_local_contribution_adresse__equipe_local_contribution" FOREIGN KEY ("equipe_local_contribution_id") REFERENCES "equipe_local_contribution"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local_contribution__equipe_local_contribution_type" ADD CONSTRAINT "fk_elc__elct__elct" FOREIGN KEY ("equipe_local_contribution_type_id") REFERENCES "equipe_local_contribution_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local__etiquette" ADD CONSTRAINT "fk_equipe_local__etiquette__equipe_local_contribution" FOREIGN KEY ("equipe_local_contribution_id") REFERENCES "equipe_local_contribution"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe_local__etiquette" ADD CONSTRAINT "fk_equipe_local__etiquette__etiquette" FOREIGN KEY ("etiquette_id") REFERENCES "etiquette"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local" ADD CONSTRAINT "fk_action_equipe_local__equipe_local_contribution" FOREIGN KEY ("equipe_local_contribution_id") REFERENCES "equipe_local_contribution"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local" ADD CONSTRAINT "fk_action_equipe_local__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local" ADD CONSTRAINT "fk_action_equipe_local__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local_contact" ADD CONSTRAINT "fk_action_equipe_local_contact__equipe_local_contribution" FOREIGN KEY ("equipe_local_contribution_id") REFERENCES "equipe_local_contribution"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local_contact" ADD CONSTRAINT "fk_action_equipe_local_contact__personne" FOREIGN KEY ("personne_id") REFERENCES "personne"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local_contact" ADD CONSTRAINT "fk_action_equipe_local_contact__evenement" FOREIGN KEY ("evenement_id") REFERENCES "evenement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local_contact" ADD CONSTRAINT "fk_action_equipe_local_contact__action_type" FOREIGN KEY ("action_type_id") REFERENCES "action_type"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
