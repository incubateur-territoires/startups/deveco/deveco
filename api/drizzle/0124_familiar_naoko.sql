CREATE TABLE IF NOT EXISTS "equipe__etablissement__qpv_2015" (
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	"id" varchar(8) NOT NULL,
	CONSTRAINT "pk_equipe__etablissement__qpv_2015" PRIMARY KEY("equipe_id","etablissement_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "equipe__etablissement__qpv_2024" (
	"equipe_id" integer NOT NULL,
	"etablissement_id" varchar(14) NOT NULL,
	"id" varchar(8) NOT NULL,
	CONSTRAINT "pk_equipe__etablissement__qpv_2024" PRIMARY KEY("equipe_id","etablissement_id")
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__qpv_2015" ADD CONSTRAINT "fk_equipe__etablissement__qpv_2015__equipe" FOREIGN KEY ("equipe_id") REFERENCES "public"."equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__qpv_2015" ADD CONSTRAINT "fk_equipe__etablissement__qpv_2015__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__qpv_2015" ADD CONSTRAINT "fk_equipe__qpv2015__qpv_2015__qpv2015" FOREIGN KEY ("id") REFERENCES "public"."qpv_2015"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__qpv_2024" ADD CONSTRAINT "fk_equipe__etablissement__qpv_2024__equipe" FOREIGN KEY ("equipe_id") REFERENCES "public"."equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__qpv_2024" ADD CONSTRAINT "fk_equipe__etablissement__qpv_2024__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__qpv_2024" ADD CONSTRAINT "fk_equipe__qpv2024__qpv_2024__qpv2024" FOREIGN KEY ("id") REFERENCES "public"."qpv_2024"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement_contribution" ADD CONSTRAINT "fk_equipe__etablissement_contribution__equipe" FOREIGN KEY ("equipe_id") REFERENCES "public"."equipe"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement_contribution" ADD CONSTRAINT "fk_equipe__etablissement_contribution__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
