ALTER TABLE "equipe__etablissement" DROP CONSTRAINT "fk_equipe__etablissement__entreprise";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP CONSTRAINT "fk_equipe__etablissement__entreprise_type";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP CONSTRAINT "fk_equipe__etablissement__entreprise_categorie_type";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP CONSTRAINT "fk_equipe__etablissement__commune";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP CONSTRAINT "fk_equipe__etablissement__effectif_type";
--> statement-breakpoint
DROP INDEX IF EXISTS "idx_equipe__etablissement__categorie_juridique_2_type";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_equipe__etablissement__categorie_juridique_3_type";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_equipe__etablissement__commune";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_equipe__etablissement__contact_noms__gin";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_equipe__etablissement__creation_date";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_equipe__etablissement__etablissement_noms__gin";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_equipe__etablissement__fermeture_date";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_equipe__etablissement__naf";--> statement-breakpoint
DROP INDEX IF EXISTS "idx_equipe__etablissement__entreprise";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "actif";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "entreprise_categorie_type_id";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "categorie_juridique_2_type_id";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "categorie_juridique_3_type_id";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "code_postal";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "commune_id";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "contact_noms";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "creation_date";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "economie_sociale_solidaire";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "effectif_type_id";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "employeur";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "entreprise_id";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "entreprise_type_id";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "etablissement_noms";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "exogene";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "fermeture_date";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "geolocalisation";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "naf_type_id";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "numero";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "voie_nom";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "consultation_at";--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP COLUMN IF EXISTS "siege";