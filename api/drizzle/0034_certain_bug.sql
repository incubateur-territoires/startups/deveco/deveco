CREATE TABLE IF NOT EXISTS "local__proprietaire_personne_morale" (
	"proprietaire_personne_morale_id" varchar(13) NOT NULL,
	"local_id" varchar(12) NOT NULL,
	CONSTRAINT "pk_local__proprietaire_personne_morale" PRIMARY KEY("proprietaire_personne_morale_id","local_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "local__proprietaire_personne_physique" (
	"local_id" varchar(12) NOT NULL,
	"proprietaire_personne_physique_id" varchar(13) NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "proprietaire_personne_morale" (
	"id" varchar(13) NOT NULL,
	"proprietaire_compte_id" varchar(11) NOT NULL,
	"entreprise_id" varchar(9),
	"nom" varchar,
	CONSTRAINT "pk_proprietaire_personne_morale" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "proprietaire_personne_physique" (
	"id" varchar(13) NOT NULL,
	"proprietaire_compte_id" varchar(11) NOT NULL,
	"nom" varchar NOT NULL,
	"prenom" varchar,
	"naissance_date" date,
	"naissance_nom" varchar,
	"naissance_lieu" varchar,
	"prenoms" varchar,
	CONSTRAINT "pk_proprietaire_personne_physique" PRIMARY KEY("id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_cerema_local_proprietaire_moral" (
	"idprocpte" varchar(11),
	"dnulp" varchar(2),
	"dsiren" varchar(9),
	"ddenom" varchar
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "source"."source_cerema_local_proprietaire_physique" (
	"idprocpte" varchar(11),
	"dnulp" varchar(2),
	"dnomus" varchar,
	"dprnus" varchar,
	"dnomlp" varchar,
	"jdatnss" varchar,
	"dldnss" varchar
);
--> statement-breakpoint
DROP TABLE "local__proprietaire_etablissement";--> statement-breakpoint
DROP TABLE "local__proprietaire_personne";--> statement-breakpoint
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local__proprietaire_personne_morale" ADD CONSTRAINT "fk_local__proprietaire_personne_morale__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local__proprietaire_personne_morale" ADD CONSTRAINT "fk_local__proprietaire_personne_morale__ppm" FOREIGN KEY ("proprietaire_personne_morale_id") REFERENCES "proprietaire_personne_morale"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local__proprietaire_personne_physique" ADD CONSTRAINT "fk_local__proprietaire_personne_physique__ppp" FOREIGN KEY ("proprietaire_personne_physique_id") REFERENCES "proprietaire_personne_physique"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "local__proprietaire_personne_physique" ADD CONSTRAINT "fk_local__proprietaire_personne_physique__local" FOREIGN KEY ("local_id") REFERENCES "local"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;--> statement-breakpoint


TRUNCATE source.source_cerema_local_proprietaire_moral;--> statement-breakpoint
TRUNCATE source.source_cerema_local_proprietaire_physique;--> statement-breakpoint
TRUNCATE proprietaire_personne_physique CASCADE;--> statement-breakpoint
TRUNCATE proprietaire_personne_morale CASCADE;--> statement-breakpoint

TRUNCATE local__proprietaire_personne_physique;--> statement-breakpoint
TRUNCATE local__proprietaire_personne_morale; --> statement-breakpoint


\copy source.source_cerema_local_proprietaire_moral(idprocpte, dnulp, dsiren, ddenom) FROM '_sources/cerema_local_proprietaire_morale_2022.csv' delimiter ',' csv header;--> statement-breakpoint

\copy source.source_cerema_local_proprietaire_physique(idprocpte, dnulp, dnomus, dprnus, dnomlp, jdatnss, dldnss) FROM '_sources/cerema_local_proprietaire_physique_2022.csv' delimiter ',' csv header;--> statement-breakpoint

INSERT INTO proprietaire_personne_physique (
	id,
	proprietaire_compte_id,
	nom,
	prenom,
	naissance_nom,
	naissance_date,
	naissance_lieu
)
SELECT
	idprocpte || dnulp,
	idprocpte,
	dnomus,
	dprnus,
	dnomlp,
	TO_DATE(jdatnss, 'DD/MM/YYYY'),
	dldnss
FROM source.source_cerema_local_proprietaire_physique;--> statement-breakpoint

INSERT INTO proprietaire_personne_morale (
	id,
	proprietaire_compte_id,
	entreprise_id,
	nom
)
SELECT
	idprocpte || dnulp,
	idprocpte,
	dsiren,
	ddenom
FROM source.source_cerema_local_proprietaire_moral;--> statement-breakpoint

INSERT INTO local__proprietaire_personne_physique (
	local_id,
	proprietaire_personne_physique_id
)
SELECT
	DISTINCT
	LEFT(l.idcom, 2) || l.invar,
	idprocpte || dnulp
FROM source.source_cerema_local_proprietaire_physique pp
JOIN source.source_cerema_local l USING(idprocpte);--> statement-breakpoint

INSERT INTO local__proprietaire_personne_morale (
	local_id,
	proprietaire_personne_morale_id
)
SELECT
	DISTINCT
	LEFT(l.idcom, 2) || l.invar,
	idprocpte || dnulp
FROM source.source_cerema_local_proprietaire_moral pp
JOIN source.source_cerema_local l USING(idprocpte);--> statement-breakpoint
