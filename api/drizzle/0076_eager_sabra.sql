ALTER TABLE "entreprise_ratio_financier" ADD CONSTRAINT "pk_entreprise_ratio_financier" PRIMARY KEY("entreprise_id","exercice_cloture_date");--> statement-breakpoint
ALTER TABLE "source"."inpi__ratio_financier" ADD CONSTRAINT "pk_inpi__ratio_financier" PRIMARY KEY("siren","date_cloture_exercice","type_bilan");--> statement-breakpoint
ALTER TABLE "source"."inpi__ratio_financier" ADD COLUMN "confidentiality" varchar;--> statement-breakpoint
ALTER TABLE "source"."inpi__ratio_financier" ADD COLUMN "total_general" double precision;

DROP MATERIALIZED VIEW IF EXISTS "entreprise_chiffre_d_affaires_3_dernier_vue";
