DROP MATERIALIZED VIEW IF EXISTS "public"."equipe__commune_vue";--> statement-breakpoint
CREATE OR REPLACE VIEW "public"."equipe__commune_vue" AS (
	select distinct
		"equipe"."id" as "eqcomvue_equipe_id",
		"commune"."id" as "eqcomvue_commune_id"
	from "equipe"
	left join "equipe__commune" on "equipe__commune"."equipe_id" = "equipe"."id"
	left join "equipe__metropole" on "equipe__metropole"."equipe_id" = "equipe"."id"
	left join "equipe__epci" on "equipe__epci"."equipe_id" = "equipe"."id"
	left join "equipe__petr" on "equipe__petr"."equipe_id" = "equipe"."id"
	left join "equipe__territoire_industrie" on "equipe__territoire_industrie"."equipe_id" = "equipe"."id"
	left join "equipe__departement" on "equipe__departement"."equipe_id" = "equipe"."id"
	left join "equipe__region" on "equipe__region"."equipe_id" = "equipe"."id"
	left join "commune" on (
		"commune"."id" = "equipe__commune"."commune_id"
		or "commune"."commune_parent_id" = "equipe__commune"."commune_id"
		or "commune"."metropole_id" = "equipe__metropole"."metropole_id"
		or "commune"."epci_id" = "equipe__epci"."epci_id"
		or "commune"."petr_id" = "equipe__petr"."petr_id"
		or "commune"."territoire_industrie_id" = "equipe__territoire_industrie"."territoire_industrie_id"
		or "commune"."departement_id" = "equipe__departement"."departement_id"
		or "commune"."region_id" = "equipe__region"."region_id"
	)
);--> statement-breakpoint

DROP MATERIALIZED VIEW IF EXISTS "public"."etablissement_geolocalisation_vue";--> statement-breakpoint
CREATE MATERIALIZED VIEW "public"."etablissement_geolocalisation_vue" AS (
	select
		"insee_sirene_api__etablissement"."siret" as "etabgeovue_siret",
		COALESCE(
			ST_SetSRID(ST_MakePoint("insee_sirene__etablissement_geoloc"."x_longitude", "insee_sirene__etablissement_geoloc"."y_latitude"), 4326),
	 		"etalab__etablissement_geoloc"."geolocalisation"
		) as "etabgeovue_geolocalisation",
		"insee_sirene_api__etablissement"."mise_a_jour_at" as "etabgeovue_mise_a_jour_at"
	from "source"."insee_sirene_api__etablissement"
	left join "source"."insee_sirene__etablissement_geoloc" on "insee_sirene__etablissement_geoloc"."siret" = "insee_sirene_api__etablissement"."siret"
	left join "source"."etalab__etablissement_geoloc" on "etalab__etablissement_geoloc"."siret" = "insee_sirene_api__etablissement"."siret"
	where (
		"insee_sirene__etablissement_geoloc"."x_longitude" is not null
		or "etalab__etablissement_geoloc"."siret" is not null
	)
);--> statement-breakpoint
CREATE UNIQUE INDEX idx_etablissement_geolocalisation_vue_etablissement ON etablissement_geolocalisation_vue (etabgeovue_siret);--> statement-breakpoint
CREATE INDEX idx_etablissement_geolocalisation_vue_geolocalisation ON etablissement_geolocalisation_vue USING gist (etabgeovue_geolocalisation);

ALTER TABLE "equipe__local__contact" DROP CONSTRAINT "fk_equipe__local__contact__equipe_local";
--> statement-breakpoint
ALTER TABLE "equipe__local_contribution" DROP CONSTRAINT "fk_equipe__local_contribution__equipe_local";
--> statement-breakpoint
ALTER TABLE "equipe__local__etiquette" DROP CONSTRAINT "fk_equipe__local__etiquette__equipe_local";
--> statement-breakpoint
ALTER TABLE "equipe__local__occupant" DROP CONSTRAINT "fk_equipe__local__occupant__equipe_local";
--> statement-breakpoint
ALTER TABLE "action_equipe_local" DROP CONSTRAINT "fk_action_equipe_local__equipe_local";
--> statement-breakpoint
ALTER TABLE "action_equipe_local_contact" DROP CONSTRAINT "fk_action_equipe_local_contact__equipe_local";
--> statement-breakpoint
ALTER TABLE "action_equipe_local_occupant" DROP CONSTRAINT "fk_action_equipe_local_occupant__equipe_local";
DROP TABLE "equipe__local";--> statement-breakpoint