DROP MATERIALIZED VIEW "public"."etablissement_geolocalisation_vue";--> statement-breakpoint
DROP TABLE "equipe__etablissement__qpv_2015" CASCADE;--> statement-breakpoint
DROP TABLE "equipe__etablissement__qpv_2024" CASCADE;--> statement-breakpoint
DROP TABLE "etablissement__qpv_2015" CASCADE;--> statement-breakpoint
DROP TABLE "etablissement__qpv_2024" CASCADE;--> statement-breakpoint
CREATE MATERIALIZED VIEW "public"."etablissement_geolocalisation_vue" AS (SELECT
			"source"."insee_sirene_api__etablissement"."siret" AS etabgeovue_siret,
			COALESCE(
				"equipe__etablissement_contribution"."geolocalisation",
				ST_SetSRID(ST_MakePoint("source"."insee_sirene__etablissement_geoloc"."x_longitude", "source"."insee_sirene__etablissement_geoloc"."y_latitude"), 4326),
				"source"."etalab__etablissement_adresse"."geolocalisation"
			) AS etabgeovue_geolocalisation,
			"source"."insee_sirene_api__etablissement"."mise_a_jour_at" AS etabgeovue_mise_a_jour_at
		FROM "source"."insee_sirene_api__etablissement"
		LEFT JOIN "source"."insee_sirene__etablissement_geoloc" ON "source"."insee_sirene__etablissement_geoloc"."siret" = "source"."insee_sirene_api__etablissement"."siret"
		LEFT JOIN "source"."etalab__etablissement_adresse" ON "source"."etalab__etablissement_adresse"."siret" = "source"."insee_sirene_api__etablissement"."siret"
		LEFT JOIN "equipe__etablissement_contribution" ON "equipe__etablissement_contribution"."etablissement_id" = "source"."insee_sirene_api__etablissement"."siret"
		WHERE "source"."insee_sirene__etablissement_geoloc"."siret" IS NOT NULL OR "source"."etalab__etablissement_adresse"."siret" IS NOT NULL);
CREATE VIEW public.etablissement__qpv_2024_vue AS (
  SELECT
    etabgeovue_siret as etablissement_id,
    qpv_2024.id as qpv_2024_id
  FROM etablissement_geolocalisation_vue
  JOIN qpv_2024 ON ST_Covers(geometrie, etabgeovue_geolocalisation));
--> statement-breakpoint
CREATE VIEW public.etablissement__qpv_2015_vue AS (
  SELECT
    etabgeovue_siret as etablissement_id,
    qpv_2015.id as qpv_2015_id
  FROM etablissement_geolocalisation_vue
  JOIN qpv_2015 ON ST_Covers(geometrie, etabgeovue_geolocalisation));
--> statement-breakpoint
