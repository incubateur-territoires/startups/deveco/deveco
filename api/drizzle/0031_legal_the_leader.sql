ALTER TABLE "compte" ADD COLUMN "identifiant" varchar;

UPDATE "compte" SET "identifiant" = "email";

ALTER TABLE "compte" ALTER COLUMN "identifiant" SET NOT NULL;
