ALTER TABLE "stats" RENAME TO "commune_etablissement_stats";

ALTER TABLE "commune_etablissement_stats" RENAME CONSTRAINT "stats_pkey" TO "pk_commune_etablissement_stats"; --> statement-breakpoint
ALTER TABLE "etablissement_lien" RENAME CONSTRAINT "uniq_etablissement_lien" TO "uk_etablissement_lien";--> statement-breakpoint

ALTER INDEX "idx_qpv__geometrie" RENAME TO "idx_qpv__geometrie__gist";--> statement-breakpoint
ALTER TABLE "entreprise_exercice" RENAME CONSTRAINT "entreprise_exercice_pkey" TO "pk_entreprise_exercice";--> statement-breakpoint

ALTER INDEX "idx_personne__email" RENAME TO "idx_contact__email";--> statement-breakpoint
ALTER INDEX "idx_personne__equipe" RENAME TO "idx_contact__equipe";--> statement-breakpoint
ALTER INDEX "idx_personne__nom" RENAME TO "idx_contact__nom";--> statement-breakpoint
ALTER INDEX "idx_personne__prenom" RENAME TO "idx_contact__prenom";--> statement-breakpoint
ALTER INDEX "idx_personne__telephone" RENAME TO "idx_contact__telephone";--> statement-breakpoint

ALTER INDEX "idx_equipe__etablissement_entreprise" RENAME TO "idx_equipe__etablissement__entreprise";--> statement-breakpoint
ALTER INDEX "idx_equipe__etablissement_equipe" RENAME TO "idx_equipe__etablissement__equipe";--> statement-breakpoint
ALTER INDEX "idx_equipe__etablissement_etablissement" RENAME TO "idx_equipe__etablissement__etablissement";--> statement-breakpoint
ALTER INDEX "idx_equipe__etablissement_etablissement_gin" RENAME TO "idx_equipe__etablissement__etablissement__gin";--> statement-breakpoint

ALTER INDEX "idx_etablissement_adresse_1__etablissement_etalab" RENAME TO "idx_etablissement_adresse_1_etalab__etablissement";--> statement-breakpoint

ALTER TABLE "etablissement_adresse_1" RENAME CONSTRAINT "uk_etablissement_adresse_1__etablissement_id" TO "uk_etablissement_adresse_1__etablissement";--> statement-breakpoint
ALTER TABLE "etablissement_adresse_1_etalab" RENAME CONSTRAINT "uk_etablissement_adresse_1_etalab__etablissement_id" TO "uk_etablissement_adresse_1_etalab__etablissement";--> statement-breakpoint

alter table action_equipe_local drop constraint fk_action_local__action_type;--> statement-breakpoint
alter table action_equipe_local drop constraint fk_action_local__evenement;--> statement-breakpoint

drop index "IDX_commune__departement";--> statement-breakpoint
drop index "IDX_commune__epci";--> statement-breakpoint
drop index "IDX_commune__metropole";--> statement-breakpoint
drop index "IDX_commune__petr";--> statement-breakpoint
drop index "IDX_commune__region";--> statement-breakpoint
drop table if exists commune_temp;--> statement-breakpoint
drop table if exists source_acv;--> statement-breakpoint
drop table if exists source_ess_france;--> statement-breakpoint
drop table if exists source_pvd;--> statement-breakpoint
drop table if exists source_sirene_lien_succession;--> statement-breakpoint


alter table commune_etablissement_stats drop constraint uk_stats__commune_date;--> statement-breakpoint
