CREATE TABLE source.source_datagouv_commune_contour(
    ogc_fid serial,
    wkb_geometry geometry(Geometry, 4326),
    code character varying,
    nom character varying,
    departement character varying,
    region character varying,
    epci character varying,
    plm boolean,
    commune character varying,
    CONSTRAINT "pk_source_datagouv_commune_contour" PRIMARY KEY ("ogc_fid")
);

--> statement-breakpoint
UPDATE
    source.source_datagouv_commune_contour
SET
    wkb_geometry = ST_MAKEVALID(wkb_geometry);

--> statement-breakpoint
UPDATE
    qpv
SET
    geometrie = ST_COLLECTIONEXTRACT(ST_MAKEVALID(geometrie), 3);
--> statement-breakpoint

CREATE TABLE source.source_insee_qpv_commune (
    qp varchar,
    lib_qp varchar,
    list_com_2023 varchar,
    list_lib_com_2023 varchar,
    list_comarr_2023 varchar,
    list_lib_comarr_2023 varchar,
    zone_epci_2023_1 varchar,
    zone_epci_2023_2 varchar,
    list_zone_epci_2023 varchar,
    list_lib_zone_epci_2023 varchar,
    reg_1 varchar,
    dep_1 varchar,
    dep_2 varchar,
    uu2020_1 varchar,
    fusion_com_2223 varchar,
    liste_com_modif_2223 varchar,
    fusion_epci_2223 varchar,
    liste_epci_modif_2223 varchar
);

\copy source.source_insee_qpv_commune FROM '_sources/insee_qpv_communes_2023.csv' csv header DELIMITER ';';

CREATE TABLE commune__qpv (
    commune_id varchar(5),
    qpv_id varchar(8),
    CONSTRAINT "pk_commune__qpv" PRIMARY KEY ("commune_id", "qpv_id"),
    CONSTRAINT "fk_commune__qpv_commune" FOREIGN KEY ("commune_id") REFERENCES commune("id"),
    CONSTRAINT "fk_commune__qpv_qpv" FOREIGN KEY ("qpv_id") REFERENCES qpv("id")
);
--> statement-breakpoint

INSERT INTO commune__qpv (qpv_id, commune_id)
SELECT qp, unnest(regexp_split_to_array(list_comarr_2023, E'(?=([0-9AB]{5})+$)'))
FROM source.source_insee_qpv_commune;

--> statement-breakpoint
ALTER TABLE "qpv" RENAME CONSTRAINT "qpv_pkey" TO "pk_qpv";
