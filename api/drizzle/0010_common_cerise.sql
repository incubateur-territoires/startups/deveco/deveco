CREATE TABLE IF NOT EXISTS "etablissement_lien" (
	"continuite_economique" boolean NOT NULL,
	"date" timestamp with time zone NOT NULL,
	"traitement_date" timestamp with time zone NOT NULL,
	"predecesseur_etablissement_id" varchar(14) NOT NULL,
	"successeur_etablissement_id" varchar(14) NOT NULL,
	"siege_transfert" boolean NOT NULL,
	CONSTRAINT "uniq_etablissement_lien" UNIQUE("predecesseur_etablissement_id","successeur_etablissement_id","date")
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_lien" ADD CONSTRAINT "fk_etablissement_lien__successeur" FOREIGN KEY ("successeur_etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_lien" ADD CONSTRAINT "fk_etablissement_lien__predecesseur" FOREIGN KEY ("predecesseur_etablissement_id") REFERENCES "etablissement"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
