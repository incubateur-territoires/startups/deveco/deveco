CREATE TABLE IF NOT EXISTS "source"."source_va" (
	"commune_id" varchar(5) PRIMARY KEY NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "commune_va" (
	"commune_id" varchar(5) PRIMARY KEY NOT NULL
);
--> statement-breakpoint

ALTER TABLE source.source_va ADD COLUMN lib_com varchar, ADD COLUMN id_va varchar, ADD COLUMN date_signature varchar;

\copy source.source_va(commune_id, lib_com, id_va, date_signature) from '_sources/anct_commune-va_2023.csv' delimiter ',' csv header;

ALTER TABLE source.source_va DROP COLUMN lib_com, DROP COLUMN id_va, DROP COLUMN date_signature;

DO $$ BEGIN
 ALTER TABLE "commune_va" ADD CONSTRAINT "fk_commune_va__commune" FOREIGN KEY ("commune_id") REFERENCES "commune"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

INSERT INTO commune_va SELECT * FROM source.source_va;
--> statement-breakpoint
