DROP VIEW IF EXISTS etablissement_periode_fermeture_vue;
CREATE VIEW public.etablissement_periode_fermeture_vue AS (
  SELECT
    etablissement_id,
    debut_date AS fermeture_date
  FROM
    (
      SELECT
        DISTINCT ON (siret)
        siret as etablissement_id,
        date_debut as debut_date,
        changement_etat_administratif_etablissement as statut
      FROM
        source.insee_sirene_api__etablissement_periode
      WHERE
        changement_etat_administratif_etablissement
      ORDER BY
        siret,
        date_debut DESC
    ) t
  WHERE
    (NOT statut)
);

ALTER TABLE "source"."insee_sirene_api__etablissement" ADD COLUMN "dernier_numero_voie_etablissement" varchar(9);--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" ADD COLUMN "indice_repetition_dernier_numero_voie_etablissement" varchar(4);--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" ADD COLUMN "identifiant_adresse_etablissement" varchar(15);--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" ADD COLUMN "coordonnee_lambert_abscisse_etablissement" varchar(18);--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" ADD COLUMN "coordonnee_lambert_ordonnee_etablissement" varchar(18);--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" ADD COLUMN "longitude" double precision;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" ADD COLUMN "latitude" double precision;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" ADD COLUMN "geo_score" double precision;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" ADD COLUMN "geo_type" varchar;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" ADD COLUMN "geo_adresse" varchar;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" ADD COLUMN "geo_id" varchar;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" ADD COLUMN "geo_ligne" varchar;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" ADD COLUMN "geo_l4" varchar;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement" ADD COLUMN "geo_l5" varchar;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__etablissement_periode" ADD COLUMN "changement_denomination_usuelle_etablissement" boolean;--> statement-breakpoint
ALTER TABLE "source"."insee_sirene_api__unite_legale" ADD COLUMN "date_debut" date;--> statement-breakpoint
\copy "source"."insee_sirene_api__unite_legale"(siren,statut_diffusion_unite_legale,unite_purgee_unite_legale,date_creation_unite_legale,sigle_unite_legale,sexe_unite_legale,prenom1_unite_legale,prenom2_unite_legale,prenom3_unite_legale,prenom4_unite_legale,prenom_usuel_unite_legale,pseudonyme_unite_legale,identifiant_association_unite_legale,tranche_effectifs_unite_legale,annee_effectifs_unite_legale,date_dernier_traitement_unite_legale,nombre_periodes_unite_legale,categorie_entreprise,annee_categorie_entreprise,date_debut,etat_administratif_unite_legale,nom_unite_legale,nom_usage_unite_legale,denomination_unite_legale,denomination_usuelle1_unite_legale,denomination_usuelle2_unite_legale,denomination_usuelle3_unite_legale,categorie_juridique_unite_legale,activite_principale_unite_legale,nomenclature_activite_principale_unite_legale,nic_siege_unite_legale,economie_sociale_solidaire_unite_legale,caractere_employeur_unite_legale,societe_mission_unite_legale,mise_a_jour_at) FROM './_sources/insee_sirene_api__unite_legale.csv' CSV HEADER;--> statement-breakpoint
\copy "source"."insee_sirene_api__unite_legale_periode"(date_debut,date_fin,siren,nic_siege_unite_legale,nom_unite_legale,nom_usage_unite_legale,denomination_unite_legale,denomination_usuelle_1_unite_legale,denomination_usuelle_2_unite_legale,denomination_usuelle_3_unite_legale,nomenclature_activite_principale_unite_legale,activite_principale_unite_legale,categorie_juridique_unite_legale,etat_administratif_unite_legale,caractere_employeur_unite_legale,economie_sociale_solidaire_unite_legale,societe_mission_unite_legale,changement_activite_principale_unite_legale,changement_caractere_employeur_unite_legale,changement_categorie_juridique_unite_legale,changement_denomination_unite_legale,changement_denomination_usuelle_unite_legale,changement_economie_sociale_solidaire_unite_legale,changement_societe_mission_unite_legale,changement_etat_administratif_unite_legale,changement_nic_siege_unite_legale,changement_nom_unite_legale,changement_nom_usage_unite_legale,mise_a_jour_at) FROM './_sources/insee_sirene_api__unite_legale_periode.csv' CSV HEADER;--> statement-breakpoint
\copy "source"."insee_sirene_api__etablissement"(siren,nic,siret,statut_diffusion_etablissement,date_creation_etablissement,tranche_effectifs_etablissement,annee_effectifs_etablissement,activite_principale_registre_metiers_etablissement,date_dernier_traitement_etablissement,etablissement_siege,nombre_periodes_etablissement,type_voie_etablissement,libelle_voie_etablissement,code_postal_etablissement,libelle_commune_etablissement,libelle_commune_etranger_etablissement,code_commune_etablissement,code_cedex_etablissement,libelle_cedex_etablissement,code_pays_etranger_etablissement,libelle_pays_etranger_etablissement,complement_adresse2_etablissement,numero_voie2_etablissement,indice_repetition2_etablissement,type_voie2_etablissement,libelle_voie2_etablissement,code_postal2_etablissement,libelle_commune2_etablissement,libelle_commune_etranger2_etablissement,distribution_speciale2_etablissement,code_commune2_etablissement,code_cedex2_etablissement,libelle_cedex2_etablissement,code_pays_etranger2_etablissement,libelle_pays_etranger2_etablissement,date_debut,etat_administratif_etablissement,enseigne2_etablissement,enseigne3_etablissement,activite_principale_etablissement,nomenclature_activite_principale_etablissement,caractere_employeur_etablissement,longitude,latitude,geo_score,geo_type,geo_adresse,geo_id,geo_ligne,geo_l4,geo_l5,complement_adresse_etablissement,indice_repetition_etablissement,distribution_speciale_etablissement,denomination_usuelle_etablissement,enseigne1_etablissement,numero_voie_etablissement,mise_a_jour_at,dernier_numero_voie_etablissement,indice_repetition_dernier_numero_voie_etablissement,identifiant_adresse_etablissement,coordonnee_lambert_abscisse_etablissement,coordonnee_lambert_ordonnee_etablissement) FROM './_sources/insee_sirene_api__etablissement.csv' CSV HEADER;--> statement-breakpoint
\copy "source"."insee_sirene_api__etablissement_periode"(siret,activite_principale_etablissement,caractere_employeur_etablissement,date_debut,date_fin,denomination_usuelle_etablissement,enseigne1_etablissement,enseigne2_etablissement,enseigne3_etablissement,etat_administratif_etablissement,nomenclature_activite_principale_etablissement,changement_activite_principale_etablissement,changement_caractere_employeur_etablissement,changement_enseigne_etablissement,changement_etat_administratif_etablissement,mise_a_jour_at,changement_denomination_usuelle_etablissement) FROM './_sources/insee_sirene_api__etablissement_periode.csv' CSV HEADER;--> statement-breakpoint
\copy "source"."insee_sirene_api__lien_succession"(siret_etablissement_predecesseur,siret_etablissement_successeur,date_lien_succession,date_dernier_traitement_lien_succession,transfert_siege,continuite_economique,mise_a_jour_at) FROM './_sources/insee_sirene_api__lien_succession.csv' CSV HEADER;--> statement-breakpoint
