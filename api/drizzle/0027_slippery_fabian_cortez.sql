CREATE TABLE IF NOT EXISTS "source"."source_dgcl_subventions" (
	"siret" varchar(14) NOT NULL,
	"type" varchar,
	"attributaire" varchar NOT NULL,
	"objet" varchar NOT NULL,
	"nature" varchar NOT NULL,
	"montant" varchar NOT NULL,
	"convention_date" varchar NOT NULL,
	"versement_date" varchar NOT NULL
);

ALTER TABLE source.source_dgcl_subventions ADD COLUMN nom_attr varchar, ADD COLUMN ref varchar, ADD COLUMN nom_benef varchar, ADD COLUMN conditions varchar, ADD COLUMN rae varchar, ADD COLUMN notif varchar, ADD COLUMN pourcentage varchar;

\copy source.source_dgcl_subventions(nom_attr, attributaire, convention_date, ref, siret, nom_benef, objet, montant, nature, conditions, versement_date, rae, notif, pourcentage) from '_sources/anct_subventions-p147_2020.csv' delimiter ';' csv header;

ALTER TABLE source.source_dgcl_subventions ADD COLUMN code_dep varchar, ADD COLUMN libelle_dep varchar;

\copy source.source_dgcl_subventions(nom_attr, code_dep, libelle_dep, attributaire, convention_date, ref, siret, nom_benef, objet, montant, nature, conditions, versement_date, rae, notif, pourcentage) from '_sources/anct_subventions-p147_2021.csv' delimiter ';' csv header;

\copy source.source_dgcl_subventions(nom_attr, code_dep, libelle_dep, attributaire, convention_date, ref, siret, nom_benef, objet, montant, nature, conditions, versement_date, rae, notif, pourcentage) from '_sources/anct_subventions-p147_2022.csv' delimiter ';' csv header;

\copy source.source_dgcl_subventions(nom_attr, code_dep, libelle_dep, attributaire, convention_date, ref, siret, nom_benef, objet, montant, nature, conditions, versement_date, rae, notif, pourcentage) from '_sources/anct_subventions-p147_2023.csv' delimiter ';' csv header;

ALTER TABLE source.source_dgcl_subventions DROP COLUMN nom_attr, DROP COLUMN ref, DROP COLUMN nom_benef, DROP COLUMN conditions, DROP COLUMN rae, DROP COLUMN notif, DROP COLUMN pourcentage, DROP COLUMN code_dep, DROP COLUMN libelle_dep;

UPDATE source.source_dgcl_subventions SET type = 'P147';
