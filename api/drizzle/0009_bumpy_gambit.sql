DROP TABLE "source"."source_sirene_lien_succession_temp";--> statement-breakpoint
ALTER TABLE "source"."source_sirene_lien_succession" RENAME TO "source_api_sirene_lien_succession";--> statement-breakpoint
ALTER TABLE "source"."source_api_sirene_lien_succession" RENAME COLUMN "date_succession" TO "date_lien_succession";--> statement-breakpoint
ALTER TABLE "source"."source_api_sirene_lien_succession" RENAME COLUMN "date_traitement" TO "date_dernier_traitement_lien_succession";--> statement-breakpoint
ALTER TABLE "source"."source_api_sirene_lien_succession" RENAME COLUMN "siret_predecesseur" TO "siret_etablissement_predecesseur";--> statement-breakpoint
ALTER TABLE "source"."source_api_sirene_lien_succession" RENAME COLUMN "siret_successeur" TO "siret_etablissement_successeur";--> statement-breakpoint
ALTER TABLE "source"."source_api_sirene_lien_succession" ADD COLUMN "mise_a_jour_at" timestamp with time zone;--> statement-breakpoint
ALTER TABLE "source"."source_api_sirene_lien_succession" DROP COLUMN IF EXISTS "id";--> statement-breakpoint
ALTER TABLE "source"."source_api_sirene_lien_succession" ADD CONSTRAINT "uniq_predecesseur_successeur_date_lien" UNIQUE("date_dernier_traitement_lien_succession","siret_etablissement_predecesseur","siret_etablissement_successeur","date_lien_succession");