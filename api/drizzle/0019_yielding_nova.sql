CREATE TABLE IF NOT EXISTS "etablissement_effectif_ema" (
	"etablissement_id" varchar(14) NOT NULL,
	"annee" integer NOT NULL,
	"mois" integer NOT NULL,
	"valeur" double precision NOT NULL,
	CONSTRAINT etablissement_effectif_ema_etablissement_id_annee_mois PRIMARY KEY("etablissement_id","annee","mois")
);
--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "IDX_etablissement_effectif_ema__etablissement" ON "etablissement_effectif_ema" ("etablissement_id");--> statement-breakpoint

DROP MATERIALIZED VIEW "etablissement_effectif_emm_dernier_vue";