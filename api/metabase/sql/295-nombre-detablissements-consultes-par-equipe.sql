SELECT "Equipe"."nom" AS "Equipe__nom",
  count(
    distinct "public"."action_equipe_etablissement"."etablissement_id"
  ) AS "count"
FROM "public"."action_equipe_etablissement"
  INNER JOIN "public"."equipe" AS "Equipe" ON "public"."action_equipe_etablissement"."equipe_id" = "Equipe"."id"
WHERE "public"."action_equipe_etablissement"."action_type_id" = 'affichage'
GROUP BY "Equipe"."nom"
ORDER BY "Equipe"."nom" ASC