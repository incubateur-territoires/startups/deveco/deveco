SELECT SUM("source"."Nombre d'exports geojson") AS "sum",
  MIN("source"."Nombre d'exports geojson") AS "min",
  MAX("source"."Nombre d'exports geojson") AS "max",
  AVG("source"."Nombre d'exports geojson") AS "avg",
  PERCENTILE_CONT(0.5) within group (
    order by "source"."Nombre d'exports geojson"
  ) AS "median"
FROM (
    SELECT "public"."equipe"."id" AS "id",
      SUM(
        CASE
          WHEN ("Recherche"."id" IS NOT NULL)
          AND ("Recherche"."format" = 'geojson') THEN 1
          ELSE 0.0
        END
      ) AS "Nombre d'exports geojson"
    FROM "public"."equipe"
      LEFT JOIN "public"."deveco" AS "Deveco" ON "public"."equipe"."id" = "Deveco"."equipe_id"
      LEFT JOIN "public"."equipe_type" AS "Equipe Type" ON "public"."equipe"."equipe_type_id" = "Equipe Type"."id"
      LEFT JOIN "public"."recherche" AS "Recherche" ON "Deveco"."id" = "Recherche"."deveco_id"
    GROUP BY "public"."equipe"."id"
    ORDER BY "public"."equipe"."id" ASC
  ) AS "source"