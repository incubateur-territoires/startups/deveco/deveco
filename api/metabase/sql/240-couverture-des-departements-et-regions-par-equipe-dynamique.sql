select equipe.id,
	equipe.nom,
	STRING_AGG(distinct c.departement_id::varchar, ', ') as departements,
	STRING_AGG(distinct c.region_id, ', ') as regions
from equipe
	join equipe__commune_vue ec on ec.eqcomvue_equipe_id = equipe.id
	join commune c on c.id = ec.eqcomvue_commune_id
	join departement on departement.id = c.departement_id
	join region on region.id = c.region_id
where { { equipes } }
	AND { { departementCodes } }
	AND { { departementNoms } }
	AND { { regionCodes } }
	AND { { regionNoms } }
	AND equipe.id NOT IN (265, 846, 858, 869)
group by equipe.id;