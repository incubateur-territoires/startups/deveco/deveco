SELECT DATE_TRUNC('week', "public"."evenement"."created_at") AS "created_at",
    count(distinct "public"."evenement"."compte_id") AS "count"
FROM "public"."evenement"
WHERE "public"."evenement"."evenement_type_id" = 'authentification'
GROUP BY DATE_TRUNC('week', "public"."evenement"."created_at")
ORDER BY DATE_TRUNC('week', "public"."evenement"."created_at") ASC