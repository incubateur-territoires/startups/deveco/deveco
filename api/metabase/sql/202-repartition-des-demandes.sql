SELECT "Demande Type"."nom" AS "Demande Type__nom",
  COUNT(*) AS "count",
  SUM(
    CASE
      WHEN "public"."demande"."cloture_motif" IS NULL THEN 1
      ELSE 0.0
    END
  ) AS "Ouvertes",
  SUM(
    CASE
      WHEN "public"."demande"."cloture_motif" IS NOT NULL THEN 1
      ELSE 0.0
    END
  ) AS "Fermées"
FROM "public"."demande"
  LEFT JOIN "public"."demande_type" AS "Demande Type" ON "public"."demande"."demande_type_id" = "Demande Type"."id"
GROUP BY "Demande Type"."nom"
ORDER BY "Demande Type"."nom" ASC