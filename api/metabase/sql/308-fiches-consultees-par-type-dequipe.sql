-- Nb, min, max, moyenne, médiane de fiches en portefeuille (échange, demande, rappel, contact (hors étiquette)) par rapport aux fiches uniques consultées
WITH consultations AS (
    SELECT equipe.id as equipe_id,
        count(equipe.id) as consultations
    FROM (
            SELECT DISTINCT ON (
                    action_equipe_etablissement.equipe_id,
                    action_equipe_etablissement.etablissement_id
                ) *
            FROM action_equipe_etablissement
            WHERE action_equipe_etablissement.action_type_id = 'affichage'
            ORDER BY action_equipe_etablissement.equipe_id,
                action_equipe_etablissement.etablissement_id,
                action_equipe_etablissement.created_at DESC
        ) t
        JOIN equipe ON equipe.id = equipe_id
    GROUP BY equipe.id
)
SELECT equipe_type.nom as "Type de l'équipe",
    SUM(consultations.consultations) as "Somme",
    MIN(consultations.consultations) as "Min",
    MAX(consultations.consultations) as "Max",
    AVG(consultations.consultations) as "Moyenne",
    PERCENTILE_CONT(0.5) within group (
        order by consultations.consultations
    ) as "Médiane"
FROM equipe
    JOIN equipe_type ON equipe.equipe_type_id = equipe_type.id
    LEFT JOIN consultations ON consultations.equipe_id = equipe.id
WHERE { { equipe_type_id } }
GROUP BY equipe_type.id;