SELECT metabase_equipe_user_type_daily_snapshot.equipe_id AS "ID Organisation",
    "public"."equipe"."nom" AS "Organisation",
    MAX(metabase_deveco_info.deveco_count) AS "# Agents",
    MAX(
        CASE
            WHEN NOT is_signin_y
            AND is_signin THEN date
            ELSE NULL
        END
    ) AS "Date d''inscription",
    MAX(
        CASE
            WHEN user_type_y_v2 = 'never_user_v2'
            AND user_type_v2 = 'test_v2' THEN '1 action ou +'
            ELSE '0 action'
        END
    ) AS "Activation"
FROM metabase_equipe_user_type_daily_snapshot
    LEFT JOIN metabase_deveco_info ON metabase_equipe_user_type_daily_snapshot.equipe_id = metabase_deveco_info."equipe_id"
    LEFT JOIN "public"."equipe" ON metabase_equipe_user_type_daily_snapshot.equipe_id = "public"."equipe"."id"
GROUP BY metabase_equipe_user_type_daily_snapshot.equipe_id,
    "public"."equipe"."nom"
HAVING (
        MAX(
            CASE
                WHEN NOT is_signin_y
                AND is_signin THEN date
                ELSE NULL
            END
        ) >= date_trunc('week', (now() + (INTERVAL '-4 week')))
        AND MAX(
            CASE
                WHEN NOT is_signin_y
                AND is_signin THEN date
                ELSE NULL
            END
        ) < date_trunc('week', now())
    )
ORDER BY "Date d''inscription" DESC
