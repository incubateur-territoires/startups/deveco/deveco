SELECT --date,
    CONCAT(
        TO_CHAR(date -6, 'DD/MM'),
        '-',
        TO_CHAR(date, 'DD/MM')
    ) AS "semaine",
    1 -(
        SUM(
            CASE
                WHEN (user_type_29_v2 = 'actif_v2')
                AND user_type_v2 = 'dem_v2' THEN 1
                ELSE 0
            END
        ) * 1.0 / nullif(
            SUM(
                CASE
                    WHEN user_type_29_v2 = 'actif_v2' THEN 1
                    ELSE 0
                END
            ),
            0
        )
    ) AS "Tx de retention"
FROM metabase_equipe_user_type_daily_snapshot
WHERE extract(
        dow
        from date
    ) = 0
GROUP BY metabase_equipe_user_type_daily_snapshot.date
ORDER BY metabase_equipe_user_type_daily_snapshot.date ASC