SELECT --date,
    CONCAT(
        TO_CHAR(date -6, 'DD/MM'),
        '-',
        TO_CHAR(date, 'DD/MM')
    ) AS "semaine",
    SUM(
        CASE
            WHEN is_signin THEN 1
            ELSE 0
        END
    ) AS "# inscrits",
    SUM(
        CASE
            WHEN user_type_v2 = 'actif_v2' THEN 1
            ELSE 0
        END
    ) + SUM(
        CASE
            WHEN user_type_v2 = 'test_v2' THEN 1
            ELSE 0
        END
    ) AS "# actifs"
FROM metabase_equipe_user_type_daily_snapshot
WHERE extract(
        dow
        from date
    ) = 0
GROUP BY date
ORDER BY date ASC