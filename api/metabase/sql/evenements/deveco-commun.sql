CREATE TABLE metabase_s1_evenement AS (
	SELECT "public"."evenement"."created_at" AS "date",
		"public"."evenement"."id" AS "id",
		"public"."evenement"."evenement_type_id" AS "type_id",
		"public"."equipe"."id" AS "equipe_id",
		"public"."deveco"."id" AS "deveco_id",
		"public"."compte"."id" AS "compte_id"
	FROM "public"."evenement"
		LEFT JOIN "public"."compte" ON "public"."evenement"."compte_id" = "public"."compte"."id"
		LEFT JOIN "public"."deveco" ON "public"."compte"."id" = "public"."deveco"."compte_id"
		LEFT JOIN "public"."equipe" ON "public"."equipe"."id" = "public"."deveco"."equipe_id"
	WHERE "public"."compte"."compte_type_id" = 'deveco'
);

CREATE TABLE metabase_deveco_stat1_daily_snapshot AS (
	SELECT equipe.deveco_id,
		d.date::date,
		count(distinct metabase_s1_evenement.id) AS activity_count
	FROM (
			SELECT DISTINCT "public"."deveco"."id" AS "deveco_id"
			FROM "public"."deveco"
				LEFT JOIN "public"."equipe" ON "public"."equipe"."id" = "public"."deveco"."equipe_id"
			WHERE {{ terr_id }}
				AND {{ terr_name }}
		) equipe
		CROSS JOIN (
			SELECT generate_series('2022-06-01', CURRENT_DATE, '1 day'::interval) AS date
		) d
		LEFT JOIN metabase_s1_evenement ON equipe.deveco_id = metabase_s1_evenement.deveco_id
		AND d.date::date = metabase_s1_evenement.date::date
	GROUP BY equipe.deveco_id,
		d.date
	ORDER BY equipe.deveco_id,
		d.date
);

CREATE TABLE metabase_deveco_stat2_daily_snapshot AS (
	SELECT metabase_deveco_stat1_daily_snapshot.deveco_id,
		metabase_deveco_stat1_daily_snapshot.date,
		SUM(metabase_deveco_stat1_daily_snapshot.activity_count) OVER (
			PARTITION BY metabase_deveco_stat1_daily_snapshot.deveco_id
			ORDER BY metabase_deveco_stat1_daily_snapshot.date
		) as log_count_ever,
		SUM(metabase_deveco_stat1_daily_snapshot.activity_count) OVER (
			PARTITION BY metabase_deveco_stat1_daily_snapshot.deveco_id
			ORDER BY metabase_deveco_stat1_daily_snapshot.date ROWS BETWEEN 29 PRECEDING AND 0 PRECEDING
		) as log_count_30d,
		-- on 30 last days including d day. if not user type change the day after
		SUM(metabase_deveco_stat1_daily_snapshot.activity_count) OVER (
			PARTITION BY metabase_deveco_stat1_daily_snapshot.deveco_id
			ORDER BY metabase_deveco_stat1_daily_snapshot.date ROWS BETWEEN 6 PRECEDING AND 0 PRECEDING
		) as log_count_7d,
		CASE
			WHEN SUM(metabase_deveco_stat1_daily_snapshot.activity_count) OVER (
				PARTITION BY metabase_deveco_stat1_daily_snapshot.deveco_id
				ORDER BY metabase_deveco_stat1_daily_snapshot.date ROWS BETWEEN 6 PRECEDING AND 0 PRECEDING
			) > 0 THEN 1
			ELSE 0
		END as log_count_1stw,
		CASE
			WHEN SUM(metabase_deveco_stat1_daily_snapshot.activity_count) OVER (
				PARTITION BY metabase_deveco_stat1_daily_snapshot.deveco_id
				ORDER BY metabase_deveco_stat1_daily_snapshot.date ROWS BETWEEN 13 PRECEDING AND 7 PRECEDING
			) > 0 THEN 1
			ELSE 0
		END as log_count_2ndw,
		CASE
			WHEN SUM(metabase_deveco_stat1_daily_snapshot.activity_count) OVER (
				PARTITION BY metabase_deveco_stat1_daily_snapshot.deveco_id
				ORDER BY metabase_deveco_stat1_daily_snapshot.date ROWS BETWEEN 20 PRECEDING AND 14 PRECEDING
			) > 0 THEN 1
			ELSE 0
		END as log_count_3rdw,
		CASE
			WHEN SUM(metabase_deveco_stat1_daily_snapshot.activity_count) OVER (
				PARTITION BY metabase_deveco_stat1_daily_snapshot.deveco_id
				ORDER BY metabase_deveco_stat1_daily_snapshot.date ROWS BETWEEN 27 PRECEDING AND 21 PRECEDING
			) > 0 THEN 1
			ELSE 0
		END as log_count_4thw
	FROM metabase_deveco_stat1_daily_snapshot
	GROUP BY deveco_id,
		date,
		activity_count
	ORDER BY deveco_id,
		date
);

CREATE TABLE metabase_deveco_user_type_daily_snapshot AS (
	SELECT deveco_id,
		date,
		log_count_ever,
		log_count_30d,
		CASE
			WHEN date < "public"."deveco"."created_at" THEN FALSE
			ELSE TRUE
		END AS is_signin,
		CASE
			WHEN LAG(date) OVER ( PARTITION BY deveco_id ORDER BY date ) < "public"."deveco"."created_at" THEN FALSE
			ELSE TRUE
		END AS is_signin_y,
		CASE
			WHEN log_count_ever >= 20 THEN TRUE
			ELSE FALSE
		END AS is_user_v1,
		CASE
			WHEN LAG(log_count_ever) OVER ( BY deveco_id ORDER BY date ) >= 20 THEN TRUE
			ELSE FALSE
		END AS is_user_y_v1,
		CASE
			WHEN log_count_ever >= 1 THEN TRUE
			ELSE FALSE
		END AS is_user_v2,
		CASE
			WHEN LAG(log_count_ever) OVER ( PARTITION BY deveco_id ORDER BY date ) >= 1 THEN TRUE
			ELSE FALSE
		END AS is_user_y_v2,
		CASE
			WHEN log_count_30d < 25
			AND log_count_30d > 0 THEN 'test_v1'
			WHEN log_count_30d >= 25
			AND log_count_1stw + log_count_2ndw + log_count_3rdw + log_count_4thw >= 3 THEN 'actif_v1'
			WHEN log_count_30d = 0
			AND log_count_ever = 0 THEN 'never_user_v1'
			WHEN log_count_30d = 0 THEN 'dem_v1'
			ELSE 'no_type'
		END AS user_type_v1,
		CASE
			WHEN LAG(log_count_30d) OVER ( PARTITION BY deveco_id ORDER BY date ) < 25
			AND LAG(log_count_30d) OVER ( PARTITION BY deveco_id ORDER BY date ) > 0 THEN 'test_v1'
			WHEN LAG(log_count_30d) OVER ( PARTITION BY deveco_id ORDER BY date ) >= 25
			AND LAG(log_count_1stw) OVER ( PARTITION BY deveco_id ORDER BY date )
				+ LAG(log_count_2ndw) OVER ( PARTITION BY deveco_id ORDER BY date )
				+ LAG(log_count_3rdw) OVER ( PARTITION BY deveco_id ORDER BY date )
				+ LAG(log_count_4thw) OVER ( PARTITION BY deveco_id ORDER BY date ) >= 3 THEN 'actif_v1'
			WHEN LAG(log_count_30d) OVER ( PARTITION BY deveco_id ORDER BY date ) = 0
			AND LAG(log_count_ever) OVER ( PARTITION BY deveco_id ORDER BY date ) = 0 THEN 'never_user_v1'
			WHEN LAG(log_count_30d) OVER ( PARTITION BY deveco_id ORDER BY date ) = 0 THEN 'dem_v1'
			ELSE 'no_type'
		END AS user_type_y_v1,
		CASE
			WHEN log_count_30d < 25
			AND log_count_30d > 0 THEN 'test_v2'
			WHEN log_count_30d >= 25 THEN 'actif_v2'
			WHEN log_count_30d = 0
			AND log_count_ever = 0 THEN 'never_user_v2'
			WHEN log_count_30d = 0 THEN 'dem_v2'
			ELSE 'no_type'
		END AS user_type_v2,
		CASE
			WHEN LAG(log_count_30d) OVER ( PARTITION BY deveco_id ORDER BY date ) < 25
			AND LAG(log_count_30d) OVER ( PARTITION BY deveco_id ORDER BY date ) > 0 THEN 'test_v2'
			WHEN LAG(log_count_30d) OVER ( PARTITION BY deveco_id ORDER BY date ) >= 25 THEN 'actif_v2'
			WHEN LAG(log_count_30d) OVER ( PARTITION BY deveco_id ORDER BY date ) = 0
			AND LAG(log_count_ever) OVER ( PARTITION BY deveco_id ORDER BY date ) = 0 THEN 'never_user_v2'
			WHEN LAG(log_count_30d) OVER ( PARTITION BY deveco_id ORDER BY date ) = 0 THEN 'dem_v2'
			ELSE 'no_type'
		END AS user_type_y_v2
	FROM metabase_deveco_stat2_daily_snapshot
		LEFT JOIN "public"."deveco" ON "public"."deveco"."id" = metabase_deveco_stat2_daily_snapshot.deveco_id
);
