-- 1) Aggregate daily events per équipe into a staging table
CREATE MATERIALIZED VIEW IF NOT EXISTS metabase_equipe_daily_events AS
SELECT
    date_trunc('day', e.created_at)::date AS date,
    d.equipe_id,
    COUNT(DISTINCT e.id) AS daily_count
FROM public.evenement e
JOIN public.compte c      ON e.compte_id = c.id
JOIN public.deveco d      ON c.id      = d.compte_id
JOIN public.equipe eq    ON eq.id     = d.equipe_id
WHERE c.compte_type_id = 'deveco'
  AND c.actif
  AND e.evenement_type_id NOT IN (
        'equipe_etablissement_etiquette_description_import',
        'equipe_etablissement_contact_import',
        'equipe_etablissement_echange_import'
      )
GROUP BY 1, 2;  -- (date, equipe_id)

CREATE INDEX IF NOT EXISTS idx_equipe_daily_events 
  ON metabase_equipe_daily_events(equipe_id, date);

-- 2) Generate the daily counts table, ensuring every (équipe, date) is present
CREATE MATERIALIZED VIEW IF NOT EXISTS metabase_equipe_daily_counts AS
WITH all_dates AS (
    SELECT generate_series('2022-06-01'::date, CURRENT_DATE, interval '1 day')::date AS date
),
all_equipes AS (
    SELECT DISTINCT d.equipe_id
    FROM public.deveco d
    JOIN public.compte c ON c.id = d.compte_id
    WHERE c.actif
)
SELECT
    ad.date,
    ae.equipe_id,
    COALESCE(e.daily_count, 0) AS daily_count
FROM all_dates ad
CROSS JOIN all_equipes ae
LEFT JOIN metabase_equipe_daily_events e
       ON e.date = ad.date 
      AND e.equipe_id = ae.equipe_id
ORDER BY ae.equipe_id, ad.date;

CREATE INDEX IF NOT EXISTS idx_equipe_daily_counts
  ON metabase_equipe_daily_counts(equipe_id, date);

-- Earliest date a 'deveco' was created for this équipe
-- (If multiple 'deveco' rows exist per équipe, we consider the earliest as "signup_date")
CREATE MATERIALIZED VIEW earliest_signup AS
SELECT
    equipe_id,
    MIN(created_at)::date AS signup_date
FROM public.deveco
GROUP BY equipe_id;

CREATE MATERIALIZED VIEW metabase_deveco_info AS
SELECT
    d.equipe_id,
    COUNT(d.id) AS deveco_count
FROM public.deveco d
GROUP BY d.equipe_id;

CREATE MATERIALIZED VIEW IF NOT EXISTS metabase_equipe_user_type_daily_snapshot AS
WITH main AS (
    SELECT
        m.equipe_id,
        m.date,
        SUM(m.daily_count) OVER (
            PARTITION BY m.equipe_id
            ORDER BY m.date
            ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
        ) AS log_count_ever,

        -- Rolling 30d sum (including the current day)
        SUM(m.daily_count) OVER (
            PARTITION BY m.equipe_id
            ORDER BY m.date
            ROWS BETWEEN 29 PRECEDING AND CURRENT ROW
        ) AS log_count_30d,

        -- Similarly for 7-day, etc. if needed:
        SUM(m.daily_count) OVER (
            PARTITION BY m.equipe_id
            ORDER BY m.date
            ROWS BETWEEN 6 PRECEDING AND CURRENT ROW
        ) AS log_count_7d
    FROM metabase_equipe_daily_counts m
),
joined AS (
    SELECT
        main.*,
        CASE 
          WHEN main.date < e.signup_date THEN FALSE
          ELSE TRUE
        END AS is_signin
    FROM main
    LEFT JOIN earliest_signup e ON e.equipe_id = main.equipe_id
)
SELECT
    equipe_id,
    date,
    log_count_ever,
    log_count_30d,
    log_count_7d,
    is_signin,
    -- Similarly store "previous day" 30d for transitions:
    LAG(log_count_30d) OVER (PARTITION BY equipe_id ORDER BY date) AS log_count_30d_y,

    -- Example: user_type_v2 logic
    CASE
      WHEN log_count_30d = 0 AND log_count_ever = 0 THEN 'never_user_v2'
      WHEN log_count_30d = 0 THEN 'dem_v2'
      WHEN log_count_30d < 25 THEN 'test_v2'
      ELSE 'actif_v2'
    END AS user_type_v2,

    -- Example: "yesterday" user_type_v2 for transitions (or store it if you want)
    CASE
      WHEN LAG(log_count_30d) OVER (PARTITION BY equipe_id ORDER BY date) = 0 
           AND LAG(log_count_ever) OVER (PARTITION BY equipe_id ORDER BY date) = 0 THEN 'never_user_v2'
      WHEN LAG(log_count_30d) OVER (PARTITION BY equipe_id ORDER BY date) = 0 THEN 'dem_v2'
      WHEN LAG(log_count_30d) OVER (PARTITION BY equipe_id ORDER BY date) < 25 
           AND LAG(log_count_30d) OVER (PARTITION BY equipe_id ORDER BY date) > 0 THEN 'test_v2'
      WHEN LAG(log_count_30d) OVER (PARTITION BY equipe_id ORDER BY date) >= 25 THEN 'actif_v2'
      ELSE 'no_type'
    END AS user_type_y_v2,

    CASE
    -- “Yesterday’s is_signin”:
    WHEN LAG(is_signin) OVER (
            PARTITION BY equipe_id 
            ORDER BY date
        )
    THEN TRUE
    ELSE FALSE
    END AS is_signin_y,

    CASE
        WHEN LAG(log_count_30d, 29) OVER (PARTITION BY equipe_id ORDER BY date) < 25
            AND LAG(log_count_30d, 29) OVER (PARTITION BY equipe_id ORDER BY date) > 0
        THEN 'test_v2'
        WHEN LAG(log_count_30d, 29) OVER (PARTITION BY equipe_id ORDER BY date) >= 25
        THEN 'actif_v2'
        WHEN LAG(log_count_30d, 29) OVER (PARTITION BY equipe_id ORDER BY date) = 0
            AND LAG(log_count_ever, 29) OVER (PARTITION BY equipe_id ORDER BY date) = 0
        THEN 'never_user_v2'
        WHEN LAG(log_count_30d, 29) OVER (PARTITION BY equipe_id ORDER BY date) = 0
        THEN 'dem_v2'
        ELSE 'no_type'
    END AS user_type_29_v2

FROM joined
ORDER BY equipe_id, date;


REFRESH MATERIALIZED VIEW metabase_equipe_daily_events;
REFRESH MATERIALIZED VIEW metabase_equipe_daily_counts;
REFRESH MATERIALIZED VIEW earliest_signup;
REFRESH MATERIALIZED VIEW metabase_deveco_info;
REFRESH MATERIALIZED VIEW metabase_equipe_user_type_daily_snapshot;