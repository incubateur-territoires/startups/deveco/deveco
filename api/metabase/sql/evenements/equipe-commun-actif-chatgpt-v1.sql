DROP TABLE IF EXISTS metabase_s1_evenement;
CREATE TABLE metabase_s1_evenement AS
SELECT
    e.created_at::date AS date,
    e.id AS id,
    e.evenement_type_id AS type_id,
    d.equipe_id AS equipe_id,
    d.id AS deveco_id,
    c.id AS compte_id
FROM public.evenement e
JOIN public.compte c ON e.compte_id = c.id
JOIN public.deveco d ON c.id = d.compte_id
WHERE c.compte_type_id = 'deveco'
  AND c.actif
  AND e.evenement_type_id NOT IN (
      'equipe_etablissement_etiquette_description_import',
      'equipe_etablissement_contact_import',
      'equipe_etablissement_echange_import'
  );

CREATE INDEX idx_metabase_s1_evenement_date ON metabase_s1_evenement(date);
CREATE INDEX idx_metabase_s1_evenement_equipe ON metabase_s1_evenement(equipe_id);

DROP TABLE IF EXISTS metabase_daily_calendar;
CREATE TABLE metabase_daily_calendar AS
SELECT
    generate_series('2022-06-01'::date, CURRENT_DATE, '1 day') AS date;


CREATE TABLE metabase_equipe_stat1_daily_snapshot AS (
	SELECT equipe.equipe_id,
		d.date::date,
		count(distinct metabase_s1_evenement.id) AS activity_count
	FROM (
		SELECT DISTINCT "public"."deveco"."equipe_id" AS "equipe_id",
			"public"."deveco"."id" AS "deveco_id"
		FROM "public"."deveco"
		JOIN "public"."compte" ON "public"."compte"."id" = "public"."deveco"."compte_id"
		WHERE "public"."compte"."actif"
		AND d.date::date = metabase_s1_evenement.date::date
	) equipe
	LEFT JOIN metabase_s1_evenement ON equipe.equipe_id = metabase_s1_evenement.equipe_id
	CROSS JOIN (
		SELECT generate_series('2022-06-01', CURRENT_DATE, '1 day'::interval) AS date
	) d
	GROUP BY equipe.equipe_id,
		d.date::date
	ORDER BY equipe.equipe_id,
		d.date::date
);

DROP TABLE IF EXISTS metabase_equipe_stat1_daily_snapshot;
CREATE TABLE metabase_equipe_stat1_daily_snapshot AS
SELECT
    d.equipe_id,
    c.date,
    COALESCE(COUNT(DISTINCT e.id), 0) AS activity_count
FROM public.deveco d
CROSS JOIN metabase_daily_calendar c
LEFT JOIN metabase_s1_evenement e
    ON d.equipe_id = e.equipe_id
    AND c.date = e.date
GROUP BY d.equipe_id, c.date;

CREATE INDEX idx_metabase_equipe_stat1_daily_snapshot ON metabase_equipe_stat1_daily_snapshot(equipe_id, date);

DROP TABLE IF EXISTS metabase_equipe_stat2_daily_snapshot;
CREATE TABLE metabase_equipe_stat2_daily_snapshot AS
SELECT
    equipe_id,
    date,
    SUM(activity_count) OVER (PARTITION BY equipe_id ORDER BY date) AS log_count_ever,
    SUM(activity_count) OVER (PARTITION BY equipe_id ORDER BY date ROWS BETWEEN 29 PRECEDING AND CURRENT ROW) AS log_count_30d,
    SUM(activity_count) OVER (PARTITION BY equipe_id ORDER BY date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS log_count_7d,
    CASE WHEN SUM(activity_count) OVER (PARTITION BY equipe_id ORDER BY date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) > 0 THEN 1 ELSE 0 END AS log_count_1stw,
    CASE WHEN SUM(activity_count) OVER (PARTITION BY equipe_id ORDER BY date ROWS BETWEEN 13 PRECEDING AND 7 PRECEDING) > 0 THEN 1 ELSE 0 END AS log_count_2ndw,
    CASE WHEN SUM(activity_count) OVER (PARTITION BY equipe_id ORDER BY date ROWS BETWEEN 20 PRECEDING AND 14 PRECEDING) > 0 THEN 1 ELSE 0 END AS log_count_3rdw,
    CASE WHEN SUM(activity_count) OVER (PARTITION BY equipe_id ORDER BY date ROWS BETWEEN 27 PRECEDING AND 21 PRECEDING) > 0 THEN 1 ELSE 0 END AS log_count_4thw
FROM metabase_equipe_stat1_daily_snapshot;

DROP TABLE IF EXISTS metabase_equipe_user_type_daily_snapshot;
CREATE TABLE metabase_equipe_user_type_daily_snapshot AS
SELECT
    s.equipe_id,
    s.date,
    s.log_count_30d,
    s.log_count_ever,
    CASE WHEN s.date >= d.created_at THEN TRUE ELSE FALSE END AS is_signin,
    CASE WHEN LAG(s.date) OVER (PARTITION BY s.equipe_id ORDER BY s.date) >= d.created_at THEN TRUE ELSE FALSE END AS is_signin_y,
    CASE WHEN s.log_count_ever >= 20 THEN TRUE ELSE FALSE END AS is_user_v1,
    CASE WHEN LAG(s.log_count_ever) OVER (PARTITION BY s.equipe_id ORDER BY s.date) >= 20 THEN TRUE ELSE FALSE END AS is_user_y_v1,
    CASE WHEN s.log_count_ever >= 1 THEN TRUE ELSE FALSE END AS is_user_v2,
    CASE WHEN LAG(s.log_count_ever) OVER (PARTITION BY s.equipe_id ORDER BY s.date) >= 1 THEN TRUE ELSE FALSE END AS is_user_y_v2,
    CASE
        WHEN s.log_count_30d < 25 AND s.log_count_30d > 0 THEN 'test_v2'
        WHEN s.log_count_30d >= 25 THEN 'actif_v2'
        WHEN s.log_count_30d = 0 AND s.log_count_ever = 0 THEN 'never_user_v2'
        WHEN s.log_count_30d = 0 THEN 'dem_v2'
        ELSE 'no_type'
    END AS user_type_v2
FROM metabase_equipe_stat2_daily_snapshot s
JOIN (
    SELECT equipe_id, MIN(created_at) AS created_at
    FROM public.deveco
    GROUP BY equipe_id
) d ON d.equipe_id = s.equipe_id;

DROP TABLE IF EXISTS metabase_deveco_info;
CREATE TABLE metabase_deveco_info AS
SELECT
    d.equipe_id,
    COUNT(d.id) AS deveco_count
FROM public.deveco d
GROUP BY d.equipe_id;
