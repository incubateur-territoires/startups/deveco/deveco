SELECT --date,
    CONCAT(
        TO_CHAR(date -6, 'DD/MM'),
        '-',
        TO_CHAR(date, 'DD/MM')
    ) AS "semaine",
    --SUM(CASE WHEN is_user_v2 THEN 1 ELSE 0 END) AS user_v2_count,
    SUM(
        CASE
            WHEN user_type_v2 = 'test_v2' THEN 1
            ELSE 0
        END
    ) + SUM(
        CASE
            WHEN user_type_v2 = 'actif_v2' THEN 1
            ELSE 0
        END
    ) AS "# testeuses & actives",
    SUM(
        CASE
            WHEN user_type_v2 = 'test_v2' THEN 1
            ELSE 0
        END
    ) AS "# testeuses",
    SUM(
        CASE
            WHEN user_type_v2 = 'actif_v2' THEN 1
            ELSE 0
        END
    ) AS "# actives",
    SUM(
        CASE
            WHEN user_type_v2 = 'dem_v2' THEN 1
            ELSE 0
        END
    ) AS "# demiss."
FROM metabase_equipe_user_type_daily_snapshot
WHERE extract(
        dow
        from date
    ) = 0
GROUP BY metabase_equipe_user_type_daily_snapshot.date
ORDER BY metabase_equipe_user_type_daily_snapshot.date ASC