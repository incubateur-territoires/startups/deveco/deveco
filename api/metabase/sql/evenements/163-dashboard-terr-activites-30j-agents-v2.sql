SELECT date,
    deveco_id,
    --CONCAT(TO_CHAR(date-6, 'DD/MM'),'-',TO_CHAR(date, 'DD/MM')) AS "semaine",
    MAX(log_count_30d) AS "# actions cumulées - 30j" --SUM(CASE WHEN is_signin THEN 1 ELSE 0 END) AS "# inscrits",
    --SUM(CASE WHEN user_type_v2 = 'actif_v2' THEN 1 ELSE 0 END) + SUM(CASE WHEN user_type_v2 = 'test_v2' THEN 1 ELSE 0 END) AS "# actifs"
FROM deveco_user_type_daily_snapshot
WHERE date >= (
        SELECT MIN("public"."deveco".created_at)
        FROM "public"."deveco"
            LEFT JOIN "public"."equipe" ON "public"."equipe"."id" = "public"."deveco"."equipe_id"
        WHERE { { terr_id } }
            AND { { terr_name } }
    )
GROUP BY date,
    deveco_id
ORDER BY date ASC