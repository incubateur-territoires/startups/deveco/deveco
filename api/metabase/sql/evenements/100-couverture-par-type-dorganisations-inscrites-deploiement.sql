WITH global_stat AS (
    SELECT 3312 AS "Nombre total", 'Commune' AS "Type d'organisation"
    UNION ALL
    SELECT 992 AS "Nombre total", 'ComCom' AS "Type d'organisation"
    UNION ALL
    SELECT 221 AS "Nombre total", 'ComAgglo' AS "Type d'organisation"
    UNION ALL
    SELECT 14 AS "Nombre total", 'ComUrb' AS "Type d'organisation"
    UNION ALL
    SELECT 22 AS "Nombre total", 'Métropole' AS "Type d'organisation"
    UNION ALL
    SELECT 125 AS "Nombre total", 'Pays (PETR)' AS "Type d'organisation"
    UNION ALL
    SELECT 11 AS "Nombre total", 'EPT' AS "Type d'organisation"
    UNION ALL
    SELECT 18 AS "Nombre total", 'Region' AS "Type d'organisation"
    UNION ALL
    SELECT 101 AS "Nombre total", 'Conseil Départemental' AS "Type d'organisation"
),
equipe_type AS (
SELECT 
    "public"."equipe"."id" AS "equipe_id",
    CASE
        WHEN "public"."equipe"."equipe_type_id" = 'CA' THEN 'ComAgglo'
        WHEN "public"."equipe"."equipe_type_id" = 'CC' THEN 'ComCom'
        WHEN "public"."equipe"."equipe_type_id" = 'COM' OR "public"."equipe"."equipe_type_id" = 'commune' THEN 'Commune'
        WHEN "public"."equipe"."equipe_type_id" = 'CU' THEN 'ComUrb'
        WHEN "public"."equipe"."equipe_type_id" = 'departement' THEN 'Conseil Départemental'
        WHEN "public"."equipe"."equipe_type_id" = 'EPT' THEN 'EPT'
        WHEN "public"."equipe"."equipe_type_id" = 'ME' THEN 'Métropole'
        WHEN "public"."equipe"."equipe_type_id" = 'metropole' THEN 'Métropole'
        WHEN "public"."equipe"."equipe_type_id" = 'petr' THEN 'Pays (PETR)'
        WHEN "public"."equipe"."equipe_type_id" = 'region' THEN 'Region'
        WHEN "public"."equipe"."equipe_type_id" = 'dreets' OR "public"."equipe"."equipe_type_id" = 'prefecture' THEN 'Préfécture - DREETS - DDETS'

    ELSE
      'Autre'
    END
    AS "equipe_type",
    COUNT( DISTINCT CASE
        WHEN "public"."equipe"."equipe_type_id" = 'CA' THEN epci.id
        WHEN "public"."equipe"."equipe_type_id" = 'CC' THEN epci.id
        WHEN "public"."equipe"."equipe_type_id" = 'COM' OR "public"."equipe"."equipe_type_id" = 'commune' THEN commune.id
        WHEN "public"."equipe"."equipe_type_id" = 'CU' THEN epci.id
        WHEN "public"."equipe"."equipe_type_id" = 'departement' THEN departement.id
        WHEN "public"."equipe"."equipe_type_id" = 'EPT' THEN epci.id
        WHEN "public"."equipe"."equipe_type_id" = 'ME' THEN metropole.id
        WHEN "public"."equipe"."equipe_type_id" = 'metropole' THEN metropole.id
        WHEN "public"."equipe"."equipe_type_id" = 'petr' THEN petr.id
        WHEN "public"."equipe"."equipe_type_id" = 'region' THEN region.id
    ELSE
      NULL
    END)
    AS "count_commune"
FROM "public"."equipe"
LEFT JOIN "public"."equipe__epci" ON "public"."equipe"."id" = "public"."equipe__epci"."equipe_id"
LEFT JOIN "public"."equipe__commune" ON "public"."equipe"."id" = "public"."equipe__commune"."equipe_id"
LEFT JOIN "public"."equipe__petr" ON "public"."equipe"."id" = "public"."equipe__petr"."equipe_id"
LEFT JOIN "public"."equipe__metropole" ON "public"."equipe"."id" = "public"."equipe__metropole"."equipe_id"
LEFT JOIN "public"."equipe__departement" ON "public"."equipe"."id" = "public"."equipe__departement"."equipe_id"
LEFT JOIN "public"."equipe__region" ON "public"."equipe"."id" = "public"."equipe__region"."equipe_id"
LEFT JOIN "public"."commune" epci ON epci."epci_id" = "public"."equipe__epci"."epci_id"
LEFT JOIN "public"."commune" commune ON commune."id" = "public"."equipe__commune"."commune_id"
LEFT JOIN "public"."commune" petr ON petr."petr_id" = "public"."equipe__petr"."petr_id"
LEFT JOIN "public"."commune" metropole ON metropole."metropole_id" = "public"."equipe__metropole"."metropole_id"
LEFT JOIN "public"."commune" departement ON departement."departement_id" = "public"."equipe__departement"."departement_id"
LEFT JOIN "public"."commune" region ON region."region_id" = "public"."equipe__region"."region_id"
GROUP BY "public"."equipe"."id", "equipe_type")
SELECT
    equipe_type AS "Type d'organisation",
    SUM(CASE WHEN is_signin THEN 1 ELSE 0 END) AS "# org. inscrites",
    SUM(CASE WHEN is_signin THEN 1 ELSE 0 END)*1.0/MAX(global_stat."Nombre total") AS "% sur total org.",
    SUM(equipe_type.count_commune) AS "# communes couvertes"
FROM metabase_equipe_user_type_daily_snapshot
JOIN equipe_type ON equipe_type.equipe_id = metabase_equipe_user_type_daily_snapshot.equipe_id
LEFT JOIN global_stat ON equipe_type.equipe_type = global_stat."Type d'organisation"
WHERE date = CURRENT_DATE - 1
GROUP BY date, equipe_type
ORDER BY "# org. inscrites" DESC