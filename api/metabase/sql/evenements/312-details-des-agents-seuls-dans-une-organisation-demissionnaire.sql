SELECT "source"."ID Equipe" AS "ID Equipe",
  "source"."Equipe" AS "Equipe",
  "source"."Date d'inscription" AS "Date d'inscription",
  "source"."Total actions sur les 30 derniers jours" AS "Total actions sur les 30 derniers jours",
  "source"."Total actions depuis inscription" AS "Total actions depuis inscription",
  "Compte"."nom" AS "Compte__nom",
  "Compte"."prenom" AS "Compte__prenom",
  "Compte"."email" AS "Compte__email"
FROM (
      152-organisation-liste-globale
  ) AS "source"
  INNER JOIN "public"."deveco" AS "Deveco" ON "source"."ID Equipe" = "Deveco"."equipe_id"
  INNER JOIN "public"."compte" AS "Compte" ON "Deveco"."compte_id" = "Compte"."id"
WHERE ("source"."# Agents" = 1)
  AND ("source"."Statut d'activité" = 'dem_v2')
LIMIT 1048575