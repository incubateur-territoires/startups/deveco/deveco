SELECT metabase_equipe_user_type_daily_snapshot.equipe_id AS "ID Organisation",
    "public"."equipe"."nom" AS "Organisation",
    MAX(
        CASE
            WHEN user_type_y_v2 != 'dem_v2'
            AND user_type_v2 = 'dem_v2' THEN date
            ELSE NULL
        END
    ) AS "Date de dernière démission",
    MAX(
        CASE
            WHEN user_type_y_v2 != 'dem_v2'
            AND user_type_v2 = 'dem_v2' THEN user_type_y_v2
            ELSE NULL
        END
    ) AS "Statut avant démission"
FROM metabase_equipe_user_type_daily_snapshot
    LEFT JOIN "public"."equipe" ON "public"."equipe"."id" = metabase_equipe_user_type_daily_snapshot.equipe_id
GROUP BY metabase_equipe_user_type_daily_snapshot.equipe_id,
    "public"."equipe"."nom"
HAVING (
        MAX(
            CASE
                WHEN user_type_y_v2 != 'dem_v2'
                AND user_type_v2 = 'dem_v2' THEN date
                ELSE NULL
            END
        ) >= date_trunc('week', (now() + (INTERVAL '-4 week')))
        AND MAX(
            CASE
                WHEN user_type_y_v2 != 'dem_v2'
                AND user_type_v2 = 'dem_v2' THEN date
                ELSE NULL
            END
        ) < date_trunc('week', now())
    )
ORDER BY "Date de dernière démission" DESC