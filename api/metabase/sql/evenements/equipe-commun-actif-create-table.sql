CREATE TABLE metabase_s1_evenement AS (
	SELECT "public"."evenement"."created_at" AS "date",
		"public"."evenement"."id" AS "id",
		"public"."evenement"."evenement_type_id" AS "type_id",
		"public"."equipe"."id" AS "equipe_id",
		"public"."deveco"."id" AS "deveco_id",
		"public"."compte"."id" AS "compte_id"
	FROM "public"."evenement"
	LEFT JOIN "public"."compte" ON "public"."evenement"."compte_id" = "public"."compte"."id"
	LEFT JOIN "public"."deveco" ON "public"."compte"."id" = "public"."deveco"."compte_id"
	LEFT JOIN "public"."equipe" ON "public"."equipe"."id" = "public"."deveco"."equipe_id"
	WHERE "public"."compte"."compte_type_id" = 'deveco'
		AND "public"."compte"."actif"
		AND evenement_type_id not in (
			'equipe_etablissement_etiquette_description_import',
			'equipe_etablissement_contact_import',
			'equipe_etablissement_echange_import'
		)
);

CREATE TABLE metabase_equipe_stat1_daily_snapshot AS (
	SELECT equipe.equipe_id,
		d.date::date,
		count(distinct metabase_s1_evenement.id) AS activity_count
	FROM (
		SELECT DISTINCT "public"."deveco"."equipe_id" AS "equipe_id",
			"public"."deveco"."id" AS "deveco_id"
		FROM "public"."deveco"
		JOIN "public"."compte" ON "public"."compte"."id" = "public"."deveco"."compte_id"
		WHERE "public"."compte"."actif"
		AND d.date::date = metabase_s1_evenement.date::date
	) equipe
	LEFT JOIN metabase_s1_evenement ON equipe.equipe_id = metabase_s1_evenement.equipe_id
	CROSS JOIN (
		SELECT generate_series('2022-06-01', CURRENT_DATE, '1 day'::interval) AS date
	) d
	GROUP BY equipe.equipe_id,
		d.date::date
	ORDER BY equipe.equipe_id,
		d.date::date
);

CREATE TABLE metabase_equipe_stat2_daily_snapshot AS (
	SELECT equipe_id,
		metabase_equipe_stat1_daily_snapshot.date,
		SUM(metabase_equipe_stat1_daily_snapshot.activity_count) OVER (
			PARTITION BY metabase_equipe_stat1_daily_snapshot.equipe_id
			ORDER BY metabase_equipe_stat1_daily_snapshot.date
		) as log_count_ever,
		SUM(metabase_equipe_stat1_daily_snapshot.activity_count) OVER (
			PARTITION BY metabase_equipe_stat1_daily_snapshot.equipe_id
			ORDER BY metabase_equipe_stat1_daily_snapshot.date ROWS BETWEEN 29 PRECEDING AND 0 PRECEDING
		) as log_count_30d,
		-- on 30 last days including d day. if not user type change the day after
		SUM(metabase_equipe_stat1_daily_snapshot.activity_count) OVER (
			PARTITION BY metabase_equipe_stat1_daily_snapshot.equipe_id
			ORDER BY metabase_equipe_stat1_daily_snapshot.date ROWS BETWEEN 6 PRECEDING AND 0 PRECEDING
		) as log_count_7d,
		CASE
			WHEN SUM(metabase_equipe_stat1_daily_snapshot.activity_count) OVER (
				PARTITION BY metabase_equipe_stat1_daily_snapshot.equipe_id
				ORDER BY metabase_equipe_stat1_daily_snapshot.date ROWS BETWEEN 6 PRECEDING AND 0 PRECEDING
			) > 0 THEN 1
			ELSE 0
		END as log_count_1stw,
		CASE
			WHEN SUM(metabase_equipe_stat1_daily_snapshot.activity_count) OVER (
				PARTITION BY metabase_equipe_stat1_daily_snapshot.equipe_id
				ORDER BY metabase_equipe_stat1_daily_snapshot.date ROWS BETWEEN 13 PRECEDING AND 7 PRECEDING
			) > 0 THEN 1
			ELSE 0
		END as log_count_2ndw,
		CASE
			WHEN SUM(metabase_equipe_stat1_daily_snapshot.activity_count) OVER (
				PARTITION BY metabase_equipe_stat1_daily_snapshot.equipe_id
				ORDER BY metabase_equipe_stat1_daily_snapshot.date ROWS BETWEEN 20 PRECEDING AND 14 PRECEDING
			) > 0 THEN 1
			ELSE 0
		END as log_count_3rdw,
		CASE
			WHEN SUM(metabase_equipe_stat1_daily_snapshot.activity_count) OVER (
				PARTITION BY metabase_equipe_stat1_daily_snapshot.equipe_id
				ORDER BY metabase_equipe_stat1_daily_snapshot.date ROWS BETWEEN 27 PRECEDING AND 21 PRECEDING
			) > 0 THEN 1
			ELSE 0
		END as log_count_4thw
	FROM metabase_equipe_stat1_daily_snapshot
	GROUP BY equipe_id,
		date,
		activity_count
	ORDER BY equipe_id,
		date
);

CREATE TABLE metabase_equipe_user_type_daily_snapshot AS (
	SELECT metabase_equipe_stat2_daily_snapshot.equipe_id,
		date,
		log_count_30d,
		log_count_ever,
		CASE
			WHEN date < equ.created_at THEN FALSE
			ELSE TRUE
		END AS is_signin,
		CASE
			WHEN LAG(date) OVER (
				PARTITION BY equipe_id
				ORDER BY date
			) < equ.created_at THEN FALSE
			ELSE TRUE
		END AS is_signin_y,
		CASE
			WHEN log_count_ever >= 20 THEN TRUE
			ELSE FALSE
		END AS is_user_v1,
		CASE
			WHEN LAG(log_count_ever) OVER ( PARTITION BY equipe_id ORDER BY date ) >= 20 THEN TRUE
			ELSE FALSE
		END AS is_user_y_v1,
		CASE
			WHEN log_count_ever >= 1 THEN TRUE
			ELSE FALSE
		END AS is_user_v2,
		CASE
			WHEN LAG(log_count_ever) OVER ( PARTITION BY equipe_id ORDER BY date ) >= 1 THEN TRUE
			ELSE FALSE
		END AS is_user_y_v2,
		CASE
			WHEN log_count_30d < 25
			AND log_count_30d > 0 THEN 'test_v1'
			WHEN log_count_30d >= 25
			AND log_count_1stw + log_count_2ndw + log_count_3rdw + log_count_4thw >= 3 THEN 'actif_v1'
			WHEN log_count_30d = 0
			AND log_count_ever = 0 THEN 'never_user_v1'
			WHEN log_count_30d = 0 THEN 'dem_v1'
			ELSE 'no_type'
		END AS user_type_v1,
		CASE
			WHEN LAG(log_count_30d) OVER ( PARTITION BY equipe_id ORDER BY date ) < 25
			AND LAG(log_count_30d) OVER ( PARTITION BY equipe_id ORDER BY date ) > 0 THEN 'test_v1'
			WHEN LAG(log_count_30d) OVER ( PARTITION BY equipe_id ORDER BY date ) >= 25
			AND LAG(log_count_1stw) OVER ( PARTITION BY equipe_id ORDER BY date )
				+ LAG(log_count_2ndw) OVER ( PARTITION BY equipe_id ORDER BY date )
				+ LAG(log_count_3rdw) OVER ( PARTITION BY equipe_id ORDER BY date )
				+ LAG(log_count_4thw) OVER ( PARTITION BY equipe_id ORDER BY date ) >= 3 THEN 'actif_v1'
			WHEN LAG(log_count_30d) OVER ( PARTITION BY equipe_id ORDER BY date ) = 0
			AND LAG(log_count_ever) OVER ( PARTITION BY equipe_id ORDER BY date ) = 0 THEN 'never_user_v1'
			WHEN LAG(log_count_30d) OVER ( PARTITION BY equipe_id ORDER BY date ) = 0 THEN 'dem_v1'
			ELSE 'no_type'
		END AS user_type_y_v1,
		CASE
			WHEN log_count_30d < 25
			AND log_count_30d > 0 THEN 'test_v2'
			WHEN log_count_30d >= 25 THEN 'actif_v2'
			WHEN log_count_30d = 0
			AND log_count_ever = 0 THEN 'never_user_v2'
			WHEN log_count_30d = 0 THEN 'dem_v2'
			ELSE 'no_type'
		END AS user_type_v2,
		CASE
			WHEN LAG(log_count_30d) OVER ( PARTITION BY equipe_id ORDER BY date ) < 25
			AND LAG(log_count_30d) OVER ( PARTITION BY equipe_id ORDER BY date ) > 0 THEN 'test_v2'
			WHEN LAG(log_count_30d) OVER ( PARTITION BY equipe_id ORDER BY date ) >= 25 THEN 'actif_v2'
			WHEN LAG(log_count_30d) OVER ( PARTITION BY equipe_id ORDER BY date ) = 0
			AND LAG(log_count_ever) OVER ( PARTITION BY equipe_id ORDER BY date ) = 0 THEN 'never_user_v2'
			WHEN LAG(log_count_30d) OVER ( PARTITION BY equipe_id ORDER BY date ) = 0 THEN 'dem_v2'
			ELSE 'no_type'
		END AS user_type_y_v2,
		CASE
			WHEN LAG(log_count_30d, 29) OVER ( PARTITION BY metabase_equipe_stat2_daily_snapshot.equipe_id ORDER BY date ) < 25
			AND LAG(log_count_30d, 29) OVER ( PARTITION BY metabase_equipe_stat2_daily_snapshot.equipe_id ORDER BY date ) > 0 THEN 'test_v2'
			WHEN LAG(log_count_30d, 29) OVER ( PARTITION BY metabase_equipe_stat2_daily_snapshot.equipe_id ORDER BY date ) >= 25 THEN 'actif_v2'
			WHEN LAG(log_count_30d, 29) OVER ( PARTITION BY metabase_equipe_stat2_daily_snapshot.equipe_id ORDER BY date ) = 0
			AND LAG(log_count_ever, 29) OVER ( PARTITION BY metabase_equipe_stat2_daily_snapshot.equipe_id ORDER BY date ) = 0 THEN 'never_user_v2'
			WHEN LAG(log_count_30d, 29) OVER ( PARTITION BY metabase_equipe_stat2_daily_snapshot.equipe_id ORDER BY date ) = 0 THEN 'dem_v2'
			ELSE 'no_type'
		END AS user_type_29_v2
	FROM metabase_equipe_stat2_daily_snapshot
		LEFT JOIN (
			SELECT "public"."deveco"."equipe_id" AS "equipe_id_",
				MIN("public"."deveco"."created_at") AS created_at
			FROM "public"."deveco"
			GROUP BY "public"."deveco"."equipe_id"
		) equ ON equ.equipe_id_ = metabase_equipe_stat2_daily_snapshot.equipe_id
);

CREATE TABLE metabase_deveco_info AS (
	SELECT "public"."deveco"."equipe_id",
		COUNT(DISTINCT "public"."deveco"."id") AS deveco_count
	FROM "public"."deveco"
	GROUP BY "public"."deveco"."equipe_id"
);
