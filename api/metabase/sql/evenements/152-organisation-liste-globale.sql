SELECT metabase_equipe_user_type_daily_snapshot.equipe_id AS "ID Equipe",
    "public"."equipe"."nom" AS "Equipe",
    MAX(
        CASE
            WHEN date = CURRENT_DATE THEN user_type_v2
            ELSE NULL
        END
    ) AS "Statut d'activité",
    MAX(metabase_deveco_info.deveco_count) AS "# Agents",
    MAX(
        CASE
            WHEN NOT is_signin_y
            AND is_signin THEN date
            ELSE NULL
        END
    ) AS "Date d'inscription",
    SUM(
        CASE
            WHEN date = CURRENT_DATE THEN log_count_30d
            ELSE NULL
        END
    ) AS "Total actions sur les 30 derniers jours",
    SUM(
        CASE
            WHEN date = CURRENT_DATE THEN log_count_ever
            ELSE NULL
        END
    ) AS "Total actions depuis inscription"
FROM metabase_equipe_user_type_daily_snapshot
    LEFT JOIN "public"."equipe" ON metabase_equipe_user_type_daily_snapshot.equipe_id = "public"."equipe"."id"
    LEFT JOIN metabase_deveco_info ON metabase_equipe_user_type_daily_snapshot.equipe_id = metabase_deveco_info."equipe_id"
GROUP BY metabase_equipe_user_type_daily_snapshot.equipe_id,
    "public"."equipe"."nom"
ORDER BY SUM(
        CASE
            WHEN date = CURRENT_DATE THEN log_count_30d
            ELSE NULL
        END
    ) DESC