SELECT CONCAT(
        TO_CHAR(
            CAST(date_trunc('week', date) AS DATE) -6,
            'DD/MM'
        ),
        '-',
        TO_CHAR(CAST(date_trunc('week', date) AS DATE), 'DD/MM')
    ) AS "semaine",
    SUM(
        CASE
            WHEN is_signin
            AND NOT is_signin_y THEN 1
            ELSE 0
        END
    ) AS "# nv inscrites",
    SUM(
        CASE
            WHEN is_signin
            AND NOT is_signin_y
            AND log_count_ever > 0 THEN 1
            ELSE 0
        END
    ) * 1.0 / NULLIF(
        SUM(
            CASE
                WHEN is_signin
                AND NOT is_signin_y THEN 1
                ELSE 0
            END
        ),
        0
    ) AS "Tx inscrits -> utilisateurs"
FROM metabase_equipe_user_type_daily_snapshot
GROUP BY date_trunc('week', date)
ORDER BY date_trunc('week', date) ASC