SELECT deveco_id AS "ID deveco",
    "public"."compte"."email" AS "Email",
    MAX(
        CASE
            WHEN NOT is_signin_y
            AND is_signin THEN date
            ELSE NULL
        END
    ) AS "Date d'inscription",
    MIN(
        CASE
            WHEN NOT is_user_y_v2
            AND is_user_v2 THEN date
            ELSE NULL
        END
    ) AS "Date d'activation (première action)",
    MAX(log_count_ever) AS "# actions depuis inscription",
    MAX(
        CASE
            WHEN date = CURRENT_DATE - 1 THEN log_count_30d
            ELSE NULL
        END
    ) AS "# actions sur les 30 derniers jours",
    MAX(
        CASE
            WHEN date = CURRENT_DATE - 1 THEN user_type_v2
            ELSE NULL
        END
    ) AS "Statut actuel (hier)"
FROM deveco_user_type_daily_snapshot
    LEFT JOIN "public"."deveco" ON deveco_user_type_daily_snapshot.deveco_id = "public"."deveco"."id"
    LEFT JOIN "public"."compte" ON "public"."compte"."id" = "public"."deveco"."compte_id"
GROUP BY 1,
    2
ORDER BY 7 DESC