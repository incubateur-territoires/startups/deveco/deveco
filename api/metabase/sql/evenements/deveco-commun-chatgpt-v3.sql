-- Step A: Create a daily event counts table
-- This captures, for each deveco, how many events happened on each date
-- We'll call it "metabase_deveco_daily_events" here
CREATE TABLE IF NOT EXISTS metabase_deveco_daily_events AS
SELECT
    date_trunc('day', e.created_at)::date AS date,
    d.id AS deveco_id,
    COUNT(DISTINCT e.id) AS daily_count
FROM public.evenement e
JOIN public.compte  c  ON e.compte_id = c.id
JOIN public.deveco  d  ON c.id        = d.compte_id
-- Possibly also JOIN public.equipe eq ON eq.id = d.equipe_id if you need territory filters
WHERE c.compte_type_id = 'deveco'
  -- AND c.actif = true              -- if you only want “active” ones
  /* AND ... (apply your {{ terr_id }} / {{ terr_name }} filters here if needed) */
GROUP BY 1, 2   -- (date, deveco_id)
;

-- Add an index to speed up grouping / window functions
CREATE INDEX IF NOT EXISTS metabase_idx_deveco_daily_events
  ON metabase_deveco_daily_events(deveco_id, date);

CREATE TABLE IF NOT EXISTS metabase_deveco_daily_counts AS
WITH all_dates AS (
    SELECT generate_series('2022-06-01'::date, CURRENT_DATE, interval '1 day')::date AS date
),
all_deveco AS (
    SELECT DISTINCT d.id AS deveco_id
    FROM public.deveco d
    /* Add territory filters if needed:
       JOIN public.equipe eq ON eq.id = d.equipe_id
       WHERE {{ terr_id }} AND {{ terr_name }} 
    */
)
SELECT
    ad.date,
    adx.deveco_id,
    COALESCE(e.daily_count, 0) AS daily_count
FROM all_dates ad
CROSS JOIN all_deveco adx
LEFT JOIN metabase_deveco_daily_events e
       ON e.deveco_id = adx.deveco_id
      AND e.date      = ad.date
ORDER BY adx.deveco_id, ad.date
;

-- Add an index for faster rolling sums
CREATE INDEX IF NOT EXISTS idx_metabase_deveco_daily_counts
  ON metabase_deveco_daily_counts(deveco_id, date);

CREATE TABLE IF NOT EXISTS metabase_deveco_user_type_daily_snapshot AS
WITH main AS (
    SELECT
        m.deveco_id,
        m.date,
        m.daily_count,

        /* Rolling total of activity, from the earliest date to the current */
        SUM(m.daily_count) OVER (
            PARTITION BY m.deveco_id
            ORDER BY m.date
            ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
        ) AS log_count_ever,

        /* Rolling 30-day window (including the current day). 
           If you prefer "previous 30 full days" then shift 1 row. */
        SUM(m.daily_count) OVER (
            PARTITION BY m.deveco_id
            ORDER BY m.date
            ROWS BETWEEN 29 PRECEDING AND CURRENT ROW
        ) AS log_count_30d,

        /* Rolling 7-day window */
        SUM(m.daily_count) OVER (
            PARTITION BY m.deveco_id
            ORDER BY m.date
            ROWS BETWEEN 6 PRECEDING AND CURRENT ROW
        ) AS log_count_7d,

        /* Sums in discrete weeks for your '1stw', '2ndw', etc. */
        CASE 
          WHEN SUM(m.daily_count) OVER (
               PARTITION BY m.deveco_id
               ORDER BY m.date
               ROWS BETWEEN 6 PRECEDING AND CURRENT ROW
               ) > 0 
          THEN 1 ELSE 0
        END AS log_count_1stw,

        CASE 
          WHEN SUM(m.daily_count) OVER (
               PARTITION BY m.deveco_id
               ORDER BY m.date
               ROWS BETWEEN 13 PRECEDING AND 7 PRECEDING
               ) > 0 
          THEN 1 ELSE 0
        END AS log_count_2ndw,

        CASE 
          WHEN SUM(m.daily_count) OVER (
               PARTITION BY m.deveco_id
               ORDER BY m.date
               ROWS BETWEEN 20 PRECEDING AND 14 PRECEDING
               ) > 0 
          THEN 1 ELSE 0
        END AS log_count_3rdw,

        CASE 
          WHEN SUM(m.daily_count) OVER (
               PARTITION BY m.deveco_id
               ORDER BY m.date
               ROWS BETWEEN 27 PRECEDING AND 21 PRECEDING
               ) > 0 
          THEN 1 ELSE 0
        END AS log_count_4thw
    FROM metabase_deveco_daily_counts m
),
joined AS (
    /* join to get each deveco's "created_at" (signup date) for is_signin logic */
    SELECT
        main.*,
        d.created_at::date AS created_at
    FROM main
    LEFT JOIN public.deveco d ON d.id = main.deveco_id
)
SELECT
    j.deveco_id,
    j.date,
    j.log_count_ever,
    j.log_count_30d,
    j.log_count_7d,
    j.log_count_1stw,
    j.log_count_2ndw,
    j.log_count_3rdw,
    j.log_count_4thw,

    /* ========== is_signin / is_signin_y logic ========== */
    CASE WHEN j.date < j.created_at THEN FALSE ELSE TRUE END AS is_signin,
    CASE
      WHEN LAG(j.date) OVER (PARTITION BY j.deveco_id ORDER BY j.date) < j.created_at 
      THEN FALSE
      ELSE TRUE
    END AS is_signin_y,

    /* ========== user_type_v1 logic ========== */
    CASE
      WHEN j.log_count_30d < 25 AND j.log_count_30d > 0 THEN 'test_v1'
      WHEN j.log_count_30d >= 25
       AND (j.log_count_1stw + j.log_count_2ndw + j.log_count_3rdw + j.log_count_4thw) >= 3
      THEN 'actif_v1'
      WHEN j.log_count_30d = 0 AND j.log_count_ever = 0 THEN 'never_user_v1'
      WHEN j.log_count_30d = 0 THEN 'dem_v1'
      ELSE 'no_type'
    END AS user_type_v1,

    CASE
      WHEN LAG(j.log_count_30d) OVER (PARTITION BY j.deveco_id ORDER BY j.date) < 25
           AND LAG(j.log_count_30d) OVER (PARTITION BY j.deveco_id ORDER BY j.date) > 0
      THEN 'test_v1'
      WHEN LAG(j.log_count_30d) OVER (PARTITION BY j.deveco_id ORDER BY j.date) >= 25
       AND ( LAG(j.log_count_1stw) OVER (PARTITION BY j.deveco_id ORDER BY j.date)
           + LAG(j.log_count_2ndw) OVER (PARTITION BY j.deveco_id ORDER BY j.date)
           + LAG(j.log_count_3rdw) OVER (PARTITION BY j.deveco_id ORDER BY j.date)
           + LAG(j.log_count_4thw) OVER (PARTITION BY j.deveco_id ORDER BY j.date)
             ) >= 3
      THEN 'actif_v1'
      WHEN LAG(j.log_count_30d) OVER (PARTITION BY j.deveco_id ORDER BY j.date) = 0
           AND LAG(j.log_count_ever) OVER (PARTITION BY j.deveco_id ORDER BY j.date) = 0
      THEN 'never_user_v1'
      WHEN LAG(j.log_count_30d) OVER (PARTITION BY j.deveco_id ORDER BY j.date) = 0
      THEN 'dem_v1'
      ELSE 'no_type'
    END AS user_type_y_v1,

    /* ========== user_type_v2 logic ========== */
    CASE
      WHEN j.log_count_30d < 25 AND j.log_count_30d > 0 THEN 'test_v2'
      WHEN j.log_count_30d >= 25 THEN 'actif_v2'
      WHEN j.log_count_30d = 0 AND j.log_count_ever = 0 THEN 'never_user_v2'
      WHEN j.log_count_30d = 0 THEN 'dem_v2'
      ELSE 'no_type'
    END AS user_type_v2,

    CASE
      WHEN LAG(j.log_count_30d) OVER (PARTITION BY j.deveco_id ORDER BY j.date) < 25
           AND LAG(j.log_count_30d) OVER (PARTITION BY j.deveco_id ORDER BY j.date) > 0
      THEN 'test_v2'
      WHEN LAG(j.log_count_30d) OVER (PARTITION BY j.deveco_id ORDER BY j.date) >= 25
      THEN 'actif_v2'
      WHEN LAG(j.log_count_30d) OVER (PARTITION BY j.deveco_id ORDER BY j.date) = 0
           AND LAG(j.log_count_ever) OVER (PARTITION BY j.deveco_id ORDER BY j.date) = 0
      THEN 'never_user_v2'
      WHEN LAG(j.log_count_30d) OVER (PARTITION BY j.deveco_id ORDER BY j.date) = 0
      THEN 'dem_v2'
      ELSE 'no_type'
    END AS user_type_y_v2,

    /* ========== user_type_29_v2 if needed for retention queries ========== */
    CASE
      WHEN LAG(j.log_count_30d, 29) OVER (PARTITION BY j.deveco_id ORDER BY j.date) < 25
           AND LAG(j.log_count_30d, 29) OVER (PARTITION BY j.deveco_id ORDER BY j.date) > 0
      THEN 'test_v2'
      WHEN LAG(j.log_count_30d, 29) OVER (PARTITION BY j.deveco_id ORDER BY j.date) >= 25
      THEN 'actif_v2'
      WHEN LAG(j.log_count_30d, 29) OVER (PARTITION BY j.deveco_id ORDER BY j.date) = 0
           AND LAG(j.log_count_ever, 29) OVER (PARTITION BY j.deveco_id ORDER BY j.date) = 0
      THEN 'never_user_v2'
      WHEN LAG(j.log_count_30d, 29) OVER (PARTITION BY j.deveco_id ORDER BY j.date) = 0
      THEN 'dem_v2'
      ELSE 'no_type'
    END AS user_type_29_v2

FROM joined j
ORDER BY j.deveco_id, j.date;

CREATE INDEX IF NOT EXISTS idx_deveco_snapshot
  ON metabase_deveco_user_type_daily_snapshot (deveco_id, date);
