SELECT
    equipe_type,
    SUM(CASE WHEN is_signin THEN 1 ELSE 0 END) AS "# inscrites",
    SUM(CASE WHEN user_type_v2 = 'actif_v2' THEN 1 ELSE 0 END) + SUM(CASE WHEN user_type_v2 = 'test_v2' THEN 1 ELSE 0 END) AS "# actives"
FROM metabase_equipe_user_type_daily_snapshot
JOIN equipe ON metabase_equipe_user_type_daily_snapshot.equipe_id = equipe.id
JOIN equipe_type ON equipe_type.id = equipe.equipe_type_id
WHERE date = CURRENT_DATE - 1
GROUP BY date, equipe_type
ORDER BY "# inscrites" DESC