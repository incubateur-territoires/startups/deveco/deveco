-- Step 1: Create the main table with all required fields
DROP TABLE IF EXISTS metabase_equipe_daily_snapshot;
CREATE TABLE metabase_equipe_daily_snapshot AS
WITH event_filtered AS (
    SELECT 
        e.created_at::date AS date,
        e.id AS evenement_id,
        e.evenement_type_id AS type_id,
        eq.id AS equipe_id
    FROM public.evenement e
    JOIN public.compte c ON e.compte_id = c.id
    JOIN public.deveco d ON c.id = d.compte_id
    JOIN public.equipe eq ON eq.id = d.equipe_id
    WHERE c.compte_type_id = 'deveco'
      AND c.actif
      AND e.evenement_type_id NOT IN (
          'equipe_etablissement_etiquette_description_import',
          'equipe_etablissement_contact_import',
          'equipe_etablissement_echange_import'
      )
),
daily_activity AS (
    SELECT 
        e.equipe_id,
        d.date::date,
        COUNT(DISTINCT e.evenement_id) AS activity_count
    FROM (
        SELECT DISTINCT deveco.equipe_id
        FROM public.deveco
        JOIN public.compte ON public.compte.id = public.deveco.compte_id
        WHERE public.compte.actif
    ) equipe
    CROSS JOIN generate_series('2022-06-01', CURRENT_DATE, '1 day'::interval) d(date)
    LEFT JOIN event_filtered e ON equipe.equipe_id = e.equipe_id AND e.date = d.date
    GROUP BY equipe.equipe_id, d.date
)
SELECT
    da.equipe_id,
    da.date,
    da.activity_count,
    SUM(da.activity_count) OVER (
        PARTITION BY da.equipe_id ORDER BY da.date
    ) AS log_count_ever,
    SUM(da.activity_count) OVER (
        PARTITION BY da.equipe_id ORDER BY da.date ROWS BETWEEN 29 PRECEDING AND CURRENT ROW
    ) AS log_count_30d,
    SUM(da.activity_count) OVER (
        PARTITION BY da.equipe_id ORDER BY da.date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW
    ) AS log_count_7d,
    CASE WHEN SUM(da.activity_count) OVER (
        PARTITION BY da.equipe_id ORDER BY da.date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW
    ) > 0 THEN 1 ELSE 0 END AS log_count_1stw,
    CASE WHEN SUM(da.activity_count) OVER (
        PARTITION BY da.equipe_id ORDER BY da.date ROWS BETWEEN 13 PRECEDING AND 7 PRECEDING
    ) > 0 THEN 1 ELSE 0 END AS log_count_2ndw,
    CASE WHEN SUM(da.activity_count) OVER (
        PARTITION BY da.equipe_id ORDER BY da.date ROWS BETWEEN 20 PRECEDING AND 14 PRECEDING
    ) > 0 THEN 1 ELSE 0 END AS log_count_3rdw,
    CASE WHEN SUM(da.activity_count) OVER (
        PARTITION BY da.equipe_id ORDER BY da.date ROWS BETWEEN 27 PRECEDING AND 21 PRECEDING
    ) > 0 THEN 1 ELSE 0 END AS log_count_4thw
FROM daily_activity da
ORDER BY da.equipe_id, da.date;

-- Step 2: Create indexes to optimize query performance
CREATE INDEX idx_metabase_equipe_daily ON metabase_equipe_daily_snapshot (equipe_id, date);
CREATE INDEX idx_metabase_equipe_user_type ON metabase_equipe_daily_snapshot (log_count_30d, log_count_ever);

-- Step 3: Create user type classification
DROP TABLE IF EXISTS metabase_equipe_user_type_daily_snapshot;
CREATE TABLE metabase_equipe_user_type_daily_snapshot AS
SELECT
    es.equipe_id,
    es.date,
    es.log_count_30d,
    es.log_count_ever,
    CASE WHEN date < eq.created_at THEN FALSE ELSE TRUE END AS is_signin,
    CASE WHEN LAG(es.date) OVER (PARTITION BY es.equipe_id ORDER BY es.date) < eq.created_at THEN FALSE ELSE TRUE END AS is_signin_y,
    CASE WHEN es.log_count_ever >= 20 THEN TRUE ELSE FALSE END AS is_user_v1,
    CASE WHEN LAG(es.log_count_ever) OVER (PARTITION BY es.equipe_id ORDER BY es.date) >= 20 THEN TRUE ELSE FALSE END AS is_user_y_v1,
    CASE WHEN es.log_count_ever >= 1 THEN TRUE ELSE FALSE END AS is_user_v2,
    CASE WHEN LAG(es.log_count_ever) OVER (PARTITION BY es.equipe_id ORDER BY es.date) >= 1 THEN TRUE ELSE FALSE END AS is_user_y_v2,
    CASE 
        WHEN es.log_count_30d < 25 AND es.log_count_30d > 0 THEN 'test_v2'
        WHEN es.log_count_30d >= 25 THEN 'actif_v2'
        WHEN es.log_count_30d = 0 AND es.log_count_ever = 0 THEN 'never_user_v2'
        WHEN es.log_count_30d = 0 THEN 'dem_v2'
        ELSE 'no_type'
    END AS user_type_v2,
    CASE 
        WHEN LAG(es.log_count_30d, 29) OVER (PARTITION BY es.equipe_id ORDER BY es.date) < 25
        AND LAG(es.log_count_30d, 29) OVER (PARTITION BY es.equipe_id ORDER BY es.date) > 0 THEN 'test_v2'
        WHEN LAG(es.log_count_30d, 29) OVER (PARTITION BY es.equipe_id ORDER BY es.date) >= 25 THEN 'actif_v2'
        WHEN LAG(es.log_count_30d, 29) OVER (PARTITION BY es.equipe_id ORDER BY es.date) = 0
        AND LAG(es.log_count_ever, 29) OVER (PARTITION BY es.equipe_id ORDER BY es.date) = 0 THEN 'never_user_v2'
        WHEN LAG(es.log_count_30d, 29) OVER (PARTITION BY es.equipe_id ORDER BY es.date) = 0 THEN 'dem_v2'
        ELSE 'no_type'
    END AS user_type_29_v2
FROM metabase_equipe_daily_snapshot es
LEFT JOIN (
    SELECT d.equipe_id, MIN(d.created_at) AS created_at FROM public.deveco d GROUP BY d.equipe_id
) eq ON eq.equipe_id = es.equipe_id;

-- Step 4: Indexes for fast queries
CREATE INDEX idx_metabase_equipe_user_type ON metabase_equipe_user_type_daily_snapshot (equipe_id, date, user_type_v2);
