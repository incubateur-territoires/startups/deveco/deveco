SELECT equipe_type.nom as "Type d'équipe",
    filtre as "Filtre",
    count(*) as "Nombre"
FROM (
        SELECT "public"."equipe"."equipe_type_id" AS "equipe_type_id",
            replace(
                unnest(
                    regexp_matches(
                        unnest(string_to_array("Recherche"."requete", '&')),
                        '([^=]+)='
                    )
                ),
                '%5B%5D',
                ''
            ) AS "filtre"
        FROM "public"."equipe"
            LEFT JOIN "public"."deveco" AS "Deveco" ON "public"."equipe"."id" = "Deveco"."equipe_id"
            LEFT JOIN "public"."recherche" AS "Recherche" ON "Deveco"."id" = "Recherche"."deveco_id"
        WHERE "Recherche"."requete" IS NOT NULL
            AND "Recherche"."requete" <> ''
            AND "Recherche"."requete" NOT IN ('xlsx', 'geojson')
            AND (
                "Recherche"."entite" = 'etablissements'
                OR "Recherche"."entite" = 'etablissement'
            )
    ) data
    LEFT JOIN "public"."equipe_type" ON "public"."equipe_type"."id" = "equipe_type_id"
WHERE filtre NOT IN ('o', 'd')
    AND { { equipeType } } [[AND filtre = {{filtreType}}]]
GROUP by
    equipe_type.nom,
    filtre