SELECT "source"."Equipe Type__nom" AS "Equipe Type__nom",
  SUM("source"."Nombre de visualisations Umap") AS "sum",
  MIN("source"."Nombre de visualisations Umap") AS "min",
  MAX("source"."Nombre de visualisations Umap") AS "max",
  AVG("source"."Nombre de visualisations Umap") AS "avg",
  PERCENTILE_CONT(0.5) within group (
    order by "source"."Nombre de visualisations Umap"
  ) AS "median"
FROM (
    SELECT "public"."equipe"."id" AS "id",
      "Equipe Type"."nom" AS "Equipe Type__nom",
      SUM(
        CASE
          WHEN "Recherche Sauvegardee"."id" IS NOT NULL THEN 1
          ELSE 0.0
        END
      ) AS "Nombre de visualisations Umap"
    FROM "public"."equipe"
      LEFT JOIN "public"."equipe_type" AS "Equipe Type" ON "public"."equipe"."equipe_type_id" = "Equipe Type"."id"
      LEFT JOIN "public"."recherche_sauvegardee" AS "Recherche Sauvegardee" ON "public"."equipe"."id" = "Recherche Sauvegardee"."equipe_id"
    GROUP BY "public"."equipe"."id",
      "Equipe Type"."nom"
    ORDER BY "public"."equipe"."id" ASC,
      "Equipe Type"."nom" ASC
  ) AS "source"
GROUP BY "source"."Equipe Type__nom"
ORDER BY "source"."Equipe Type__nom" ASC