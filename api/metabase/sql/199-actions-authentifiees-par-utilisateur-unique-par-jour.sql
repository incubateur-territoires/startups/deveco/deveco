SELECT CAST("public"."evenement"."created_at" AS date) AS "created_at",
  count(distinct "public"."evenement"."compte_id") AS "count"
FROM "public"."evenement"
WHERE "public"."evenement"."evenement_type_id" = 'authentification'
GROUP BY CAST("public"."evenement"."created_at" AS date)
ORDER BY CAST("public"."evenement"."created_at" AS date) ASC