SELECT SUM("source"."Nombre d'étiquettes filières") AS "sum",
  MIN("source"."Nombre d'étiquettes filières") AS "min",
  MAX("source"."Nombre d'étiquettes filières") AS "max",
  AVG("source"."Nombre d'étiquettes filières") AS "avg",
  PERCENTILE_CONT(0.5) within group (
    order by "source"."Nombre d'étiquettes filières"
  ) AS "median"
FROM (
    SELECT "public"."equipe"."id" AS "id",
      SUM(
        CASE
          WHEN "Etiquette"."id" IS NOT NULL THEN 1
          ELSE 0.0
        END
      ) AS "Nombre d'étiquettes filières"
    FROM "public"."equipe"
      LEFT JOIN "public"."etiquette" AS "Etiquette" ON "public"."equipe"."id" = "Etiquette"."equipe_id"
    WHERE "Etiquette"."etiquette_type_id" = 'activite'
    GROUP BY "public"."equipe"."id"
    ORDER BY "public"."equipe"."id" ASC
  ) AS "source"