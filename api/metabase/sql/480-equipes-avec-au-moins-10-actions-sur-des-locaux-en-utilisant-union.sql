SELECT count(*) as "Nombre d'équipes ayant au moins 10 actions"
FROM (
        SELECT equipe_id,
            SUM(actions)
        FROM (
                SELECT equipe_id,
                    count(distinct local_id) as actions -- est-ce qu'on fait le distinct ou pas ?
                FROM action_equipe_local
                WHERE action_type_id = 'affichage'
                GROUP BY equipe_id
                UNION ALL
                SELECT equipe_id,
                    count(*) as actions -- est-ce qu'on compte plusieurs types de contributions différents comme une seule action ou plusieurs ? (e.g. signalé vacant ET ajouter un nom)
                FROM equipe__local_contribution
                GROUP BY equipe_id
                UNION ALL
                SELECT equipe_id,
                    count(distinct local_id) as actions -- est-ce qu'on compte une action par local ou une action par (local, étiquette) ?
                FROM equipe__local__etiquette
                WHERE not auto
                GROUP BY equipe_id
            ) "actions"
        GROUP BY equipe_id
        HAVING SUM(actions) >= 10
    ) "somme";