SELECT "source"."Equipe Type__nom" AS "Equipe Type__nom",
  SUM("source"."Nombre de rappels") AS "sum",
  MIN("source"."Nombre de rappels") AS "min",
  MAX("source"."Nombre de rappels") AS "max",
  AVG("source"."Nombre de rappels") AS "avg",
  PERCENTILE_CONT(0.5) within group (
    order by "source"."Nombre de rappels"
  ) AS "median"
FROM (
    SELECT "public"."equipe"."id" AS "id",
      "Equipe Type"."nom" AS "Equipe Type__nom",
      SUM(
        CASE
          WHEN "Rappel"."id" IS NOT NULL THEN 1
          ELSE 0.0
        END
      ) AS "Nombre de rappels"
    FROM "public"."equipe"
      LEFT JOIN "public"."rappel" AS "Rappel" ON "public"."equipe"."id" = "Rappel"."equipe_id"
      LEFT JOIN "public"."equipe_type" AS "Equipe Type" ON "public"."equipe"."equipe_type_id" = "Equipe Type"."id"
    GROUP BY "public"."equipe"."id",
      "Equipe Type"."nom"
    ORDER BY "public"."equipe"."id" ASC,
      "Equipe Type"."nom" ASC
  ) AS "source"
GROUP BY "source"."Equipe Type__nom"
ORDER BY "source"."Equipe Type__nom" ASC