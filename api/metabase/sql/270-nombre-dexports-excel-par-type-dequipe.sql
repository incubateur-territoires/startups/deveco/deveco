SELECT "source"."Equipe Type__nom" AS "Equipe Type__nom",
  SUM("source"."Nombre d'exports excel") AS "sum",
  MIN("source"."Nombre d'exports excel") AS "min",
  MAX("source"."Nombre d'exports excel") AS "max",
  AVG("source"."Nombre d'exports excel") AS "avg",
  PERCENTILE_CONT(0.5) within group (
    order by "source"."Nombre d'exports excel"
  ) AS "median"
FROM (
    SELECT "public"."equipe"."id" AS "id",
      "Equipe Type"."nom" AS "Equipe Type__nom",
      SUM(
        CASE
          WHEN ("Recherche"."id" IS NOT NULL)
          AND ("Recherche"."format" = 'xlsx') THEN 1
          ELSE 0.0
        END
      ) AS "Nombre d'exports excel"
    FROM "public"."equipe"
      LEFT JOIN "public"."deveco" AS "Deveco" ON "public"."equipe"."id" = "Deveco"."equipe_id"
      LEFT JOIN "public"."equipe_type" AS "Equipe Type" ON "public"."equipe"."equipe_type_id" = "Equipe Type"."id"
      LEFT JOIN "public"."recherche" AS "Recherche" ON "Deveco"."id" = "Recherche"."deveco_id"
    GROUP BY "public"."equipe"."id",
      "Equipe Type"."nom"
    ORDER BY "public"."equipe"."id" ASC,
      "Equipe Type"."nom" ASC
  ) AS "source"
GROUP BY "source"."Equipe Type__nom"
ORDER BY "source"."Equipe Type__nom" ASC