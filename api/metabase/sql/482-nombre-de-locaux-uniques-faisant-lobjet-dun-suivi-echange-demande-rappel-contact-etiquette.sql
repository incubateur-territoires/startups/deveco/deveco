SELECT count(distinct local_id) as "Nombre de locaux uniques faisant l'objet d'un suivi"
FROM (
        -- on compte les locaux uniques quelle que soit l'équipe ; il faut vérifier si on ne veut pas compter plutôt les couples (local, équipe) uniques
        SELECT local_id
        FROM equipe__local__echange
        UNION ALL
        SELECT local_id
        FROM equipe__local__demande
        UNION ALL
        SELECT local_id
        FROM equipe__local__rappel
        UNION ALL
        SELECT local_id
        FROM equipe__local__etiquette
        WHERE not auto
        UNION ALL
        SELECT local_id
        FROM equipe__local__contact
    ) "somme";