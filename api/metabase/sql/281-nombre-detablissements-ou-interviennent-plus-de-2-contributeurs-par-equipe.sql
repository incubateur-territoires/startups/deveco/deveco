SELECT SUM("source"."Nombre d'établissements") AS "sum",
  MIN("source"."Nombre d'établissements") AS "min",
  MAX("source"."Nombre d'établissements") AS "max",
  AVG("source"."Nombre d'établissements") AS "avg",
  PERCENTILE_CONT(0.5) within group (
    order by "source"."Nombre d'établissements"
  ) AS "median"
FROM (
    SELECT "source"."id" AS "id",
      SUM(
        CASE
          WHEN "source"."count" > 2 THEN 1
          ELSE 0.0
        END
      ) AS "Nombre d'établissements"
    FROM (
        SELECT "public"."equipe"."id" AS "id",
          "Action Equipe Etablissement Contact"."etablissement_id" AS "Action Equipe Etablissement Contact__etablissement_id",
          count(distinct "Evenement"."compte_id") AS "count"
        FROM "public"."equipe"
          LEFT JOIN "public"."equipe_type" AS "Equipe Type" ON "public"."equipe"."equipe_type_id" = "Equipe Type"."id"
          LEFT JOIN "public"."action_equipe_etablissement_contact" AS "Action Equipe Etablissement Contact" ON "public"."equipe"."id" = "Action Equipe Etablissement Contact"."equipe_id"
          LEFT JOIN "public"."evenement" AS "Evenement" ON "Action Equipe Etablissement Contact"."evenement_id" = "Evenement"."id"
        WHERE (
            "Action Equipe Etablissement Contact"."action_type_id" <> 'affichage'
          )
          OR (
            "Action Equipe Etablissement Contact"."action_type_id" IS NULL
          )
        GROUP BY "public"."equipe"."id",
          "Action Equipe Etablissement Contact"."etablissement_id"
        ORDER BY "public"."equipe"."id" ASC,
          "Action Equipe Etablissement Contact"."etablissement_id" ASC
      ) AS "source"
    GROUP BY "source"."id"
    ORDER BY "source"."id" ASC
  ) AS "source"