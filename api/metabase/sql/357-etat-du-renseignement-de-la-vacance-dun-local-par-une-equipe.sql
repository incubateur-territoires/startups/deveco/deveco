SELECT "Equipe Local Contribution"."equipe_id" AS "Equipe Local Contribution__equipe_id",
  COUNT(*) AS "count",
  SUM(
    CASE
      WHEN "Equipe Local Contribution"."equipe_id" IS NULL THEN 1
      ELSE 0.0
    END
  ) AS "Jamais modifié",
  SUM(
    CASE
      WHEN ("Equipe Local Contribution"."equipe_id" IS NULL)
      AND (
        "Equipe Local Contribution"."equipe_id" IS NOT NULL
      ) THEN 1
      ELSE 0.0
    END
  ) AS "Indéterminé",
  SUM(
    CASE
      WHEN "Equipe Local Contribution"."occupe" = TRUE THEN 1
      ELSE 0.0
    END
  ) AS "Occupé",
  SUM(
    CASE
      WHEN "Equipe Local Contribution"."occupe" = FALSE THEN 1
      ELSE 0.0
    END
  ) AS "Vacant"
FROM "public"."local"
  LEFT JOIN "public"."equipe__local_contribution" AS "Equipe Local Contribution" ON "public"."local"."id" = "Equipe Local Contribution"."local_id"
GROUP BY "Equipe Local Contribution"."equipe_id"
ORDER BY "Equipe Local Contribution"."equipe_id" ASC