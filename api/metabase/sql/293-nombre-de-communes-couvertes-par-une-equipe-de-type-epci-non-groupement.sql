SELECT "public"."commune"."id" AS "id",
  "Laposte Code Insee Code Postal"."code_postal" AS "Laposte Code Insee Code Postal__code_postal"
FROM "public"."commune"
  INNER JOIN "public"."epci" AS "Epci" ON "public"."commune"."epci_id" = "Epci"."id"
  INNER JOIN "public"."equipe__epci" AS "Equipe Epci" ON "Epci"."id" = "Equipe Epci"."epci_id"
  INNER JOIN "public"."equipe" AS "Equipe" ON "Equipe Epci"."equipe_id" = "Equipe"."id"
  LEFT JOIN "source"."laposte__code_insee_code_postal" AS "Laposte Code Insee Code Postal" ON "public"."commune"."id" = "Laposte Code Insee Code Postal"."code_insee"
WHERE "Equipe"."groupement" = FALSE
LIMIT 1048575