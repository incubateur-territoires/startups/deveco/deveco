SELECT "public"."epci"."id" AS "id",
  COUNT(*) AS "count"
FROM "public"."epci"
  INNER JOIN "public"."equipe__epci" AS "Equipe Epci" ON "public"."epci"."id" = "Equipe Epci"."epci_id"
  INNER JOIN "public"."equipe" AS "Equipe" ON "Equipe Epci"."equipe_id" = "Equipe"."id"
WHERE "Equipe"."groupement" = FALSE
GROUP BY "public"."epci"."id"
ORDER BY "public"."epci"."id" ASC