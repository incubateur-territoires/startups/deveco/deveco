-- Nb, min, max, moyenne, médiane de fiches en portefeuille (échange, demande, rappel, contact (hors étiquette)) par rapport aux fiches uniques consultées
WITH consultations AS (
    SELECT equipe.id,
        count(equipe.id) as consultations
    FROM (
            SELECT DISTINCT ON (
                    action_equipe_etablissement.equipe_id,
                    action_equipe_etablissement.etablissement_id
                ) *
            FROM action_equipe_etablissement
            WHERE action_equipe_etablissement.action_type_id = 'affichage'
            ORDER BY action_equipe_etablissement.equipe_id,
                action_equipe_etablissement.etablissement_id,
                action_equipe_etablissement.created_at DESC
        ) t
        JOIN equipe ON equipe.id = equipe_id
    GROUP BY equipe.id
),
portefeuille AS (
    SELECT equipe.id as id,
        count(*) as portefeuille
    FROM (
            SELECT DISTINCT ON (t.equipe_id, t.etablissement_id) t.equipe_id,
                t.etablissement_id
            FROM (
                    SELECT equipe_id,
                        etablissement_id
                    FROM equipe__etablissement__echange
                    UNION
                    SELECT equipe_id,
                        etablissement_id
                    FROM equipe__etablissement__demande
                    UNION
                    SELECT equipe_id,
                        etablissement_id
                    FROM equipe__etablissement__rappel
                    UNION
                    SELECT equipe_id,
                        etablissement_id
                    FROM equipe__etablissement__contact
                ) t
        ) v
        JOIN equipe ON equipe.id = v.equipe_id
    GROUP BY equipe.id
)
SELECT equipe.id as "#",
    equipe.nom as "Nom de l'équipe",
    COALESCE(portefeuille.portefeuille, 0) as "Nombre d'établissements dans le portefeuille",
    COALESCE(consultations.consultations, 0) as "Nombre d'établissements consultés",
    COALESCE(
        to_char(
            100.0 * portefeuille.portefeuille / consultations.consultations,
            '990D99%'
        ),
        '-'
    ) as "Ratio"
FROM equipe
    LEFT JOIN consultations ON consultations.id = equipe.id
    LEFT JOIN portefeuille ON portefeuille.id = equipe.id
WHERE { { equipe_nom } }
    AND { { equipe_type } };