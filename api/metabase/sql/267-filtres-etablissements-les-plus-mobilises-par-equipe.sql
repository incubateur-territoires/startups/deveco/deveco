SELECT filtre,
    count(*) as nb
FROM (
        SELECT "public"."equipe"."id" AS "id",
            replace(
                unnest(
                    regexp_matches(
                        unnest(string_to_array("Recherche"."requete", '&')),
                        '([^=]+)='
                    )
                ),
                '%5B%5D',
                ''
            ) AS "filtre"
        FROM "public"."equipe"
            LEFT JOIN "public"."deveco" AS "Deveco" ON "public"."equipe"."id" = "Deveco"."equipe_id"
            LEFT JOIN "public"."recherche" AS "Recherche" ON "Deveco"."id" = "Recherche"."deveco_id"
        WHERE "Recherche"."requete" IS NOT NULL
            AND "Recherche"."format" is NULL
            AND "Recherche"."requete" <> ''
            AND (
                "Recherche"."entite" = 'etablissements'
                OR "Recherche"."entite" = 'etablissement'
            )
    ) data
WHERE filtre NOT IN ('o', 'd')
GROUP BY "filtre"
order by 2 desc