WITH portefeuille AS (
    SELECT equipe.id as id,
        count(*) as portefeuille,
        mois
    FROM (
            -- on récupère, pour chaque (equipeId, etablissementId) la date de premier suivi, ce qui va nous permettre de compter, chaque mois, combien d'établissements ont été suivis par toutes les équipes
            SELECT DISTINCT ON (t.equipe_id, t.etablissement_id) t.equipe_id,
                t.etablissement_id,
                mois
            FROM (
                    SELECT eee.equipe_id,
                        eee.etablissement_id,
                        to_char(date_trunc('month', ae.created_at), 'yyyy/mm') as "mois"
                    FROM equipe__etablissement__echange eee
                        JOIN action_echange ae on ae.echange_id = eee.echange_id
                    UNION
                    SELECT eed.equipe_id,
                        eed.etablissement_id,
                        to_char(date_trunc('month', ad.created_at), 'yyyy/mm') as "mois"
                    FROM equipe__etablissement__demande eed
                        JOIN action_demande ad on ad.demande_id = eed.demande_id
                    UNION
                    SELECT equipe_id,
                        etablissement_id,
                        to_char(date_trunc('month', ar.created_at), 'yyyy/mm') as "mois"
                    FROM equipe__etablissement__rappel eer
                        JOIN action_rappel ar on ar.rappel_id = eer.rappel_id
                ) t
            ORDER BY t.equipe_id,
                t.etablissement_id,
                mois ASC
        ) v
        JOIN equipe ON equipe.id = v.equipe_id
    GROUP BY equipe.id,
        mois
)
SELECT (mois || '/01')::date,
    COALESCE(
        SUM(total_mois) OVER (
            ORDER BY mois rows between unbounded preceding and 1 PRECEDING
        ),
        0
    ) as total_mois_précédents,
    total_mois as nouveau
FROM (
        SELECT SUM(portefeuille.portefeuille) total_mois,
            portefeuille.mois
        FROM equipe
            LEFT JOIN portefeuille ON portefeuille.id = equipe.id
        GROUP BY portefeuille.mois
        ORDER BY portefeuille.mois ASC
    ) t
WHERE t.mois is not null;