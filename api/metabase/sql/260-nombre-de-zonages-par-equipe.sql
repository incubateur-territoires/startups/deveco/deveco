SELECT SUM("source"."Nombre de zonages") AS "sum",
  MIN("source"."Nombre de zonages") AS "min",
  MAX("source"."Nombre de zonages") AS "max",
  AVG("source"."Nombre de zonages") AS "avg",
  PERCENTILE_CONT(0.5) within group (
    order by "source"."Nombre de zonages"
  ) AS "median"
FROM (
    SELECT "public"."equipe"."id" AS "id",
      SUM(
        CASE
          WHEN "Zonage"."id" IS NOT NULL THEN 1
          ELSE 0.0
        END
      ) AS "Nombre de zonages"
    FROM "public"."equipe"
      LEFT JOIN "public"."zonage" AS "Zonage" ON "public"."equipe"."id" = "Zonage"."equipe_id"
    GROUP BY "public"."equipe"."id"
    ORDER BY "public"."equipe"."id" ASC
  ) AS "source"