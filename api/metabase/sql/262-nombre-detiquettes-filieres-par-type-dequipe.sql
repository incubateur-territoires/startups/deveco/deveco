SELECT "source"."equipe_type__via__equipe_type_id__nom" AS "equipe_type__via__equipe_type_id__nom",
  SUM("source"."Nombre d'étiquettes filières") AS "sum",
  MIN("source"."Nombre d'étiquettes filières") AS "min",
  MAX("source"."Nombre d'étiquettes filières") AS "max",
  AVG("source"."Nombre d'étiquettes filières") AS "avg",
  PERCENTILE_CONT(0.5) within group (
    order by "source"."Nombre d'étiquettes filières"
  ) AS "median"
FROM (
    SELECT "public"."equipe"."id" AS "id",
      "equipe_type__via__equipe_type_id"."nom" AS "equipe_type__via__equipe_type_id__nom",
      SUM(
        CASE
          WHEN "Etiquette"."id" IS NOT NULL THEN 1
          ELSE 0.0
        END
      ) AS "Nombre d'étiquettes filières"
    FROM "public"."equipe"
      LEFT JOIN "public"."etiquette" AS "Etiquette" ON "public"."equipe"."id" = "Etiquette"."equipe_id"
      LEFT JOIN "public"."equipe_type" AS "equipe_type__via__equipe_type_id" ON "public"."equipe"."equipe_type_id" = "equipe_type__via__equipe_type_id"."id"
    WHERE "Etiquette"."etiquette_type_id" = 'activite'
    GROUP BY "public"."equipe"."id",
      "equipe_type__via__equipe_type_id"."nom"
    ORDER BY "public"."equipe"."id" ASC,
      "equipe_type__via__equipe_type_id"."nom" ASC
  ) AS "source"
GROUP BY "source"."equipe_type__via__equipe_type_id__nom"
ORDER BY "source"."equipe_type__via__equipe_type_id__nom" ASC