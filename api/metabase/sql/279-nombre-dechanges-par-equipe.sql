SELECT SUM("source"."Nombre d'échanges") AS "sum",
  MIN("source"."Nombre d'échanges") AS "min",
  MAX("source"."Nombre d'échanges") AS "max",
  AVG("source"."Nombre d'échanges") AS "avg",
  PERCENTILE_CONT(0.5) within group (
    order by "source"."Nombre d'échanges"
  ) AS "median"
FROM (
    SELECT "public"."equipe"."id" AS "id",
      SUM(
        CASE
          WHEN "Echange"."id" IS NOT NULL THEN 1
          ELSE 0.0
        END
      ) AS "Nombre d'échanges"
    FROM "public"."equipe"
      LEFT JOIN "public"."echange" AS "Echange" ON "public"."equipe"."id" = "Echange"."equipe_id"
      LEFT JOIN "public"."equipe_type" AS "Equipe Type" ON "public"."equipe"."equipe_type_id" = "Equipe Type"."id"
    GROUP BY "public"."equipe"."id"
    ORDER BY "public"."equipe"."id" ASC
  ) AS "source"