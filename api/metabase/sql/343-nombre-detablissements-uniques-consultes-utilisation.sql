WITH consultations AS (
    SELECT mois,
        count(*) as total_mois
    FROM (
            SELECT DISTINCT equipe_id,
                etablissement_id,
                date_trunc('month', created_at) as "mois"
            FROM action_equipe_etablissement
            WHERE action_type_id = 'affichage'
            ORDER BY equipe_id,
                etablissement_id,
                date_trunc('month', created_at)
        ) v
    GROUP BY mois
)
SELECT mois,
    SUM(total_mois) OVER (
        ORDER BY mois rows between unbounded preceding and 1 PRECEDING
    ) as "Total des mois précédents",
    total_mois as "Total du mois"
FROM consultations;