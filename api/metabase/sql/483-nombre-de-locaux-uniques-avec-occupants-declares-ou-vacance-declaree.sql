SELECT count(distinct local_id) as "Nombre de locaux uniques avec occupants déclarés ou vacance déclarée"
FROM (
        -- on compte les locaux uniques quelle que soit l'équipe ; il faut vérifier si on ne veut pas compter plutôt les couples (local, équipe) uniques
        SELECT local_id
        FROM equipe__local_contribution
        WHERE occupe
            or not occupe -- that is the question
    ) "somme";