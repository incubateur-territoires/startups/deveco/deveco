WITH consultations AS (
    SELECT equipe.id,
        count(equipe.id) as consultations
    FROM (
            SELECT DISTINCT ON (
                    action_equipe_etablissement.equipe_id,
                    action_equipe_etablissement.etablissement_id
                ) *
            FROM action_equipe_etablissement
            WHERE action_equipe_etablissement.action_type_id = 'affichage'
            ORDER BY action_equipe_etablissement.equipe_id,
                action_equipe_etablissement.etablissement_id,
                action_equipe_etablissement.created_at DESC
        ) t
        JOIN equipe ON equipe.id = equipe_id
    GROUP BY equipe.id
),
portefeuille AS (
    SELECT equipe.id as id,
        count(*) as portefeuille
    FROM (
            SELECT DISTINCT ON (t.equipe_id, t.etablissement_id) t.equipe_id,
                t.etablissement_id
            FROM (
                    SELECT equipe_id,
                        etablissement_id
                    FROM equipe__etablissement__echange
                    UNION
                    SELECT equipe_id,
                        etablissement_id
                    FROM equipe__etablissement__demande
                    UNION
                    SELECT equipe_id,
                        etablissement_id
                    FROM equipe__etablissement__rappel
                ) t
        ) v
        JOIN equipe ON equipe.id = v.equipe_id
    GROUP BY equipe.id
)
SELECT SUM(portefeuille) as "Somme",
    MIN(portefeuille) as "Min",
    MAX(portefeuille) as "Max",
    AVG(portefeuille) as "Moyenne",
    PERCENTILE_CONT(0.5) within group (
        order by portefeuille
    ) AS "Médiane"
FROM (
        SELECT equipe.id,
            equipe.nom,
            portefeuille.portefeuille,
            consultations.consultations,
            (
                100 * consultations.consultations / portefeuille.portefeuille
            ) as ratio
        FROM equipe
            LEFT JOIN consultations ON consultations.id = equipe.id
            LEFT JOIN portefeuille ON portefeuille.id = equipe.id
    ) t;