SELECT "source"."equipe_type__via__equipe_type_id__nom" AS "equipe_type__via__equipe_type_id__nom",
  SUM("source"."Nombre de zonages") AS "sum",
  MIN("source"."Nombre de zonages") AS "min",
  MAX("source"."Nombre de zonages") AS "max",
  AVG("source"."Nombre de zonages") AS "avg",
  PERCENTILE_CONT(0.5) within group (
    order by "source"."Nombre de zonages"
  ) AS "median"
FROM (
    SELECT "public"."equipe"."id" AS "id",
      "equipe_type__via__equipe_type_id"."nom" AS "equipe_type__via__equipe_type_id__nom",
      SUM(
        CASE
          WHEN "Zonage"."id" IS NOT NULL THEN 1
          ELSE 0.0
        END
      ) AS "Nombre de zonages"
    FROM "public"."equipe"
      LEFT JOIN "public"."zonage" AS "Zonage" ON "public"."equipe"."id" = "Zonage"."equipe_id"
      LEFT JOIN "public"."equipe_type" AS "equipe_type__via__equipe_type_id" ON "public"."equipe"."equipe_type_id" = "equipe_type__via__equipe_type_id"."id"
    GROUP BY "public"."equipe"."id",
      "equipe_type__via__equipe_type_id"."nom"
    ORDER BY "public"."equipe"."id" ASC,
      "equipe_type__via__equipe_type_id"."nom" ASC
  ) AS "source"
GROUP BY "source"."equipe_type__via__equipe_type_id__nom"
ORDER BY "source"."equipe_type__via__equipe_type_id__nom" ASC