SELECT SUM("source"."Nombre de recherches") AS "sum",
  MIN("source"."Nombre de recherches") AS "min",
  MAX("source"."Nombre de recherches") AS "max",
  AVG("source"."Nombre de recherches") AS "avg",
  PERCENTILE_CONT(0.5) within group (
    order by "source"."Nombre de recherches"
  ) AS "median"
FROM (
    SELECT "source"."id" AS "id",
      SUM(
        CASE
          WHEN (
            ("source"."suivi" IS NOT NULL)
            AND (
              ("source"."suivi" <> '')
              OR ("source"."suivi" IS NULL)
            )
          )
          OR (
            ("source"."filtre perso" IS NOT NULL)
            AND (
              ("source"."filtre perso" <> '')
              OR ("source"."filtre perso" IS NULL)
            )
          ) THEN 1
          ELSE 0.0
        END
      ) AS "Nombre de recherches"
    FROM (
        SELECT "public"."equipe"."id" AS "id",
          substring(
            "Recherche"."requete"
            FROM 'suivi|((activites|localisations|motsCles|demandes)%5B%5D)='
          ) AS "suivi",
          substring(
            "Recherche"."requete"
            FROM '(activites|localisations|motsCles|demandes)%5B%5D='
          ) AS "filtre perso",
          "Deveco"."id" AS "Deveco__id",
          "Deveco"."compte_id" AS "Deveco__compte_id",
          "Deveco"."equipe_id" AS "Deveco__equipe_id",
          "Deveco"."bienvenue_email" AS "Deveco__bienvenue_email",
          "Deveco"."created_at" AS "Deveco__created_at",
          "Deveco"."updated_at" AS "Deveco__updated_at",
          "Recherche"."id" AS "Recherche__id",
          "Recherche"."requete" AS "Recherche__requete",
          "Recherche"."entite" AS "Recherche__entite",
          "Recherche"."created_at" AS "Recherche__created_at",
          "Recherche"."deveco_id" AS "Recherche__deveco_id",
          "Recherche"."format" AS "Recherche__format"
        FROM "public"."equipe"
          LEFT JOIN "public"."deveco" AS "Deveco" ON "public"."equipe"."id" = "Deveco"."equipe_id"
          LEFT JOIN "public"."recherche" AS "Recherche" ON "Deveco"."id" = "Recherche"."deveco_id"
      ) AS "source"
    GROUP BY "source"."id"
    ORDER BY "Nombre de recherches" ASC,
      "source"."id" ASC
  ) AS "source"