SELECT date_trunc('month', "public"."action_contact"."created_at") AS "Mois",
    count("public"."action_contact"."contact_id") AS "# Contacts créés"
FROM "public"."action_contact"
WHERE "public"."action_contact"."action_type_id" = 'creation'
GROUP BY date_trunc('month', "public"."action_contact"."created_at")
ORDER BY date_trunc('month', "public"."action_contact"."created_at") ASC
    /*SELECT 
     date_trunc('month', "public"."event_log"."date") AS "Mois", 
     count(distinct "public"."event_log"."id") AS "# Contacts renseignés"
     FROM "public"."event_log"
     WHERE ("public"."event_log"."entity_type" = 'FICHE_PM' 
     AND "public"."event_log"."action" = 'CONTACT_QUALIFICATION' AND "public"."event_log"."date" <= CURRENT_DATE - 1)
     GROUP BY date_trunc('month', "public"."event_log"."date")
     ORDER BY date_trunc('month', "public"."event_log"."date") ASC */