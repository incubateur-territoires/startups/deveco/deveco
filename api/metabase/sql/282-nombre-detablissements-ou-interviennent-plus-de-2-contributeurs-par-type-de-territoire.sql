SELECT "source"."territoire_type__via__territoire_type_id__nom" AS "territoire_type__via__territoire_type_id__nom",
  SUM("source"."Nombre d'établissements") AS "sum",
  MIN("source"."Nombre d'établissements") AS "min",
  MAX("source"."Nombre d'établissements") AS "max",
  AVG("source"."Nombre d'établissements") AS "avg",
  PERCENTILE_CONT(0.5) within group (
    order by "source"."Nombre d'établissements"
  ) AS "median"
FROM (
    SELECT "source"."id" AS "id",
      "source"."territoire_type__via__territoire_type_id__nom" AS "territoire_type__via__territoire_type_id__nom",
      SUM(
        CASE
          WHEN "source"."count" > 2 THEN 1
          ELSE 0.0
        END
      ) AS "Nombre d'établissements"
    FROM (
        SELECT "public"."equipe"."id" AS "id",
          "Action Equipe Etablissement Contact"."etablissement_id" AS "Action Equipe Etablissement Contact__etablissement_id",
          "territoire_type__via__territoire_type_id"."nom" AS "territoire_type__via__territoire_type_id__nom",
          count(distinct "Evenement"."compte_id") AS "count"
        FROM "public"."equipe"
          LEFT JOIN "public"."equipe_type" AS "Equipe Type" ON "public"."equipe"."equipe_type_id" = "Equipe Type"."id"
          LEFT JOIN "public"."action_equipe_etablissement_contact" AS "Action Equipe Etablissement Contact" ON "public"."equipe"."id" = "Action Equipe Etablissement Contact"."equipe_id"
          LEFT JOIN "public"."evenement" AS "Evenement" ON "Action Equipe Etablissement Contact"."evenement_id" = "Evenement"."id"
          LEFT JOIN "public"."territoire_type" AS "territoire_type__via__territoire_type_id" ON "public"."equipe"."territoire_type_id" = "territoire_type__via__territoire_type_id"."id"
        WHERE (
            "Action Equipe Etablissement Contact"."action_type_id" <> 'affichage'
          )
          OR (
            "Action Equipe Etablissement Contact"."action_type_id" IS NULL
          )
        GROUP BY "public"."equipe"."id",
          "Action Equipe Etablissement Contact"."etablissement_id",
          "territoire_type__via__territoire_type_id"."nom"
        ORDER BY "public"."equipe"."id" ASC,
          "Action Equipe Etablissement Contact"."etablissement_id" ASC,
          "territoire_type__via__territoire_type_id"."nom" ASC
      ) AS "source"
    GROUP BY "source"."id",
      "source"."territoire_type__via__territoire_type_id__nom"
    ORDER BY "source"."id" ASC,
      "source"."territoire_type__via__territoire_type_id__nom" ASC
  ) AS "source"
GROUP BY "source"."territoire_type__via__territoire_type_id__nom"
ORDER BY "source"."territoire_type__via__territoire_type_id__nom" ASC