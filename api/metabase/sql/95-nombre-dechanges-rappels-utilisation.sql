WITH echange_rappel AS (
  SELECT date_trunc('month', "public"."action_rappel"."created_at") AS "Mois",
    CONCAT("public"."rappel"."id", 'rappel') AS "hit_id"
  FROM "public"."rappel"
    LEFT JOIN "public"."action_rappel" ON "public"."action_rappel"."rappel_id" = "public"."rappel"."id"
  WHERE "public"."action_rappel"."action_type_id" = 'creation'
  UNION ALL
  SELECT date_trunc('month', "public"."action_echange"."created_at") AS "Mois",
    CONCAT("public"."echange"."id", 'echange') AS "hit_id"
  FROM "public"."echange"
    LEFT JOIN "public"."action_echange" ON "public"."action_echange"."echange_id" = "public"."echange"."id"
  WHERE "public"."action_echange"."action_type_id" = 'creation'
)
SELECT "Mois",
  COUNT(DISTINCT "hit_id") AS "# Échanges & rappels"
FROM "echange_rappel"
GROUP BY "Mois"
ORDER BY "Mois" ASC
  /*WITH echange_rappel AS (SELECT 
   date_trunc('month', "public"."rappel"."created_at") AS "Mois", 
   CONCAT("public"."rappel"."id",'rappel') AS "hit_id"
   FROM "public"."rappel"
   UNION ALL 
   SELECT 
   date_trunc('month', "public"."echange"."created_at") AS "Mois", 
   CONCAT("public"."echange"."id",'echange') AS "hit_id"
   FROM "public"."echange")
   SELECT 
   "Mois",
   COUNT(DISTINCT "hit_id" )
   FROM "echange_rappel"
   GROUP BY "Mois"
   ORDER BY "Mois" ASC */