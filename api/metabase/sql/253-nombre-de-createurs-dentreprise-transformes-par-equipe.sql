SELECT SUM("source"."Nombre de créateurs transformés") AS "sum",
  MIN("source"."Nombre de créateurs transformés") AS "min",
  MAX("source"."Nombre de créateurs transformés") AS "max",
  AVG("source"."Nombre de créateurs transformés") AS "avg",
  PERCENTILE_CONT(0.5) within group (
    order by "source"."Nombre de créateurs transformés"
  ) AS "median"
FROM (
    SELECT "public"."equipe"."id" AS "id",
      SUM(
        CASE
          WHEN "Etablissement Creation Transformation"."id" IS NOT NULL THEN 1
          ELSE 0.0
        END
      ) AS "Nombre de créateurs transformés"
    FROM "public"."equipe"
      LEFT JOIN "public"."etablissement_creation" AS "Etablissement Creation" ON "public"."equipe"."id" = "Etablissement Creation"."equipe_id"
      LEFT JOIN "public"."etablissement_creation_transformation" AS "Etablissement Creation Transformation" ON "Etablissement Creation"."id" = "Etablissement Creation Transformation"."etablissement_creation_id"
    GROUP BY "public"."equipe"."id"
    ORDER BY "public"."equipe"."id" ASC
  ) AS "source"