SELECT CAST("created_at" AS date) AS "date",
  "type_id" AS "type",
  SUM(daily_count) OVER (
    PARTITION BY type_id
    ORDER BY created_at
  ) AS "cumul"
FROM (
    SELECT created_at,
      type_id,
      SUM(
        CASE
          WHEN equipe_id IS NOT NULL THEN 1
          ELSE 0
        END
      ) as daily_count
    FROM (
        WITH date_series AS (
          SELECT generate_series('2024-01-01', CURRENT_DATE, '1 day'::interval) AS date
        ),
        equipe_type as (
          select distinct territoire_type_id as type_id
          from equipe
        ),
        contact_proprietaire as (
          select a.equipe_id,
            a.local_id,
            equipe.territoire_type_id as type_id,
            CAST(a.created_at AS DATE) as date
          from action_equipe_local_contact a
            join equipe__local__contact c on a.contact_id = c.contact_id
            and a.equipe_id = c.equipe_id
            JOIN equipe on equipe.id = a.equipe_id
          where fonction = 'Propriétaire'
            and a.action_type_id = 'creation'
          order by a.equipe_id,
            a.local_id,
            a.created_at desc
        )
        SELECT d.date as created_at,
          e.type_id,
          pcp.equipe_id,
          pcp.local_id
        FROM date_series d
          CROSS JOIN equipe_type e
          LEFT JOIN contact_proprietaire pcp on d.date = pcp.date
          and pcp.type_id = e.type_id
      ) AS "source"
    GROUP BY "source"."created_at",
      "source"."type_id"
    ORDER BY "source"."created_at" ASC,
      "source"."type_id" ASC
  ) as "truc"