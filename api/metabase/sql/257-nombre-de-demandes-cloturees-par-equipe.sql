SELECT SUM("source"."Nombre de demandes") AS "sum",
  MIN("source"."Nombre de demandes") AS "min",
  MAX("source"."Nombre de demandes") AS "max",
  AVG("source"."Nombre de demandes") AS "avg",
  PERCENTILE_CONT(0.5) within group (
    order by "source"."Nombre de demandes"
  ) AS "median"
FROM (
    SELECT "public"."equipe"."id" AS "id",
      SUM(
        CASE
          WHEN "Demande"."id" IS NOT NULL THEN 1
          ELSE 0.0
        END
      ) AS "Nombre de demandes"
    FROM "public"."equipe"
      LEFT JOIN "public"."demande" AS "Demande" ON "public"."equipe"."id" = "Demande"."equipe_id"
    WHERE "Demande"."cloture_motif" IS NOT NULL
    GROUP BY "public"."equipe"."id"
    ORDER BY "public"."equipe"."id" ASC
  ) AS "source"