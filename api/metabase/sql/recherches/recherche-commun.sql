CREATE MATERIALIZED VIEW metabase_recherches_par_equipe_type AS (
    SELECT
        "public"."equipe"."equipe_type_id" AS "equipe_type_id",
        regexp_matches(
            unnest(string_to_array("Recherche"."requete", '&')),
            '([^=]+)=([^&]+)'
        ) AS "filtre"
    FROM "public"."equipe"
    LEFT JOIN "public"."deveco" AS "Deveco" ON "public"."equipe"."id" = "Deveco"."equipe_id"
    LEFT JOIN "public"."recherche" AS "Recherche" ON "Deveco"."id" = "Recherche"."deveco_id"
    WHERE
        "Recherche"."requete" IS NOT NULL
        AND "Recherche"."requete" <> ''
        AND "Recherche"."requete" NOT IN ('xlsx', 'geojson')
        AND (
            "Recherche"."entite" = 'etablissements'
            OR "Recherche"."entite" = 'etablissement'
        )
);

REFRESH MATERIALIZED VIEW metabase_recherches_par_equipe_type;