SELECT 
    replace(filtre [1], '%5B%5D', '') as "Filtre",
    filtre [2] as "Valeur",
    count(*) as "Nombre"
FROM metabase_recherches_par_equipe_type
[[WHERE filtre = {{filtreType}}]]
GROUP BY "Filtre", "Valeur"
ORDER BY "Nombre" DESC, "Filtre"