SELECT 
    filtre as "Filtre",
    valeur as "Valeur",
    naf_type.nom as "Code NAF",
    count as "Nombre"
FROM (
    SELECT
        replace(filtre [1], '%5B%5D', '') as filtre,
        filtre [2] as valeur,
        count(*) as count
    FROM metabase_recherches_par_equipe_type
    WHERE filtre [1] = 'codesNaf%5B%5D'
    GROUP BY filtre, valeur
    ORDER BY filtre, count DESC
) t
JOIN naf_type ON naf_type.id = valeur
ORDER BY count DESC