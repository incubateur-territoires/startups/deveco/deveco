SELECT d.date,
    INITCAP("demande_type"."nom") AS "Type",
    SUM(
        case
            when d.date >= "demande"."date" THEN 1
            ELSE 0
        END
    ) AS "Demandes cumulées"
FROM "public"."demande"
    CROSS JOIN (
        SELECT generate_series('2022-06-01', CURRENT_DATE, '1 day'::interval) AS date
    ) d
    JOIN demande_type ON demande_type.id = demande.demande_type_id
WHERE d.date::text BETWEEN { { date_debut } } AND { { date_fin } }
GROUP BY d.date,
    "demande_type"."id"
ORDER BY d.date ASC