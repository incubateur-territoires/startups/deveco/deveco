WITH consultations AS (
    SELECT equipe.id,
        count(equipe.id) as consultations
    FROM (
            SELECT DISTINCT ON (
                    action_equipe_local.equipe_id,
                    action_equipe_local.local_id
                ) *
            FROM action_equipe_local
            WHERE action_equipe_local.action_type_id = 'affichage'
            ORDER BY action_equipe_local.equipe_id,
                action_equipe_local.local_id,
                action_equipe_local.created_at DESC
        ) t
        JOIN equipe ON equipe.id = equipe_id
    GROUP BY equipe.id
)
SELECT SUM(consultations) as "Somme",
    MIN(consultations) as "Min",
    MAX(consultations) as "Max",
    AVG(consultations) as "Moyenne",
    PERCENTILE_CONT(0.5) within group (
        order by consultations
    ) AS "Médiane"
FROM (
        SELECT equipe.id,
            equipe.nom,
            consultations.consultations
        FROM equipe
            LEFT JOIN consultations ON consultations.id = equipe.id
    ) t;