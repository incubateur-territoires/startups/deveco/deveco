select region.id as "Code",
    region.nom "Nom",
    STRING_AGG(distinct e.nom, ', ') as "Équipes"
from region
    JOIN equipe__region er ON er.region_id = region.id
    join equipe e on e.id = er.equipe_id
WHERE { { regionNoms } }
    AND { { regionCodes } }
group by region.id;