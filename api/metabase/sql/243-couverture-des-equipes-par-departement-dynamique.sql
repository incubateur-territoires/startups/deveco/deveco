select d.id as "Code",
    d.nom "Nom",
    STRING_AGG(distinct e.nom, ', ') as "Équipes"
from departement d
    join equipe__departement ed on ed.departement_id = d.id
    join equipe e on e.id = ed.equipe_id
where { { departementNoms } }
    AND { { departementCodes } }
    AND { { regionNoms } }
    AND { { regionCodes } }
group by d.id;