CREATE TABLE daily_calendar AS
SELECT eq.equipe_id,
    g.date::date,
    COALESCE(dc.activity_count, 0) AS activity_count
FROM (
        SELECT DISTINCT equipe_id
        FROM public.deveco
    ) eq
    CROSS JOIN generate_series('2022-06-01'::date, CURRENT_DATE, '1 day') g(date)
    LEFT JOIN daily_activity_count dc ON dc.equipe_id = eq.equipe_id
    AND dc.date = g.date;