CREATE TABLE daily_user_types AS
SELECT ds.equipe_id,
    ds.date,
    ds.log_count_ever,
    ds.log_count_30d,
    ds.log_count_7d,
    ds.log_count_1stw,
    ds.log_count_2ndw,
    ds.log_count_3rdw,
    ds.log_count_4thw,
    CASE
        WHEN ds.log_count_30d < 25
        AND ds.log_count_30d > 0 THEN 'test_v2'
        WHEN ds.log_count_30d >= 25 THEN 'actif_v2'
        WHEN ds.log_count_30d = 0
        AND ds.log_count_ever = 0 THEN 'never_user_v2'
        WHEN ds.log_count_30d = 0 THEN 'dem_v2'
        ELSE 'no_type'
    END AS user_type_v2
FROM daily_snapshot ds;