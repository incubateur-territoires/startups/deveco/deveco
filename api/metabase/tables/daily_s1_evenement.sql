CREATE TABLE daily_s1_evenement AS
SELECT e.created_at::date AS date,
  e.id AS evenement_id,
  eq.id AS equipe_id,
  d.id AS deveco_id
FROM public.evenement e
  JOIN public.compte c ON e.compte_id = c.id
  JOIN public.deveco d ON c.id = d.compte_id
  JOIN public.equipe eq ON eq.id = d.equipe_id
WHERE c.compte_type_id = 'deveco'
  AND c.actif;