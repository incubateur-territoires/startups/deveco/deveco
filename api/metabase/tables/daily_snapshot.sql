CREATE TABLE daily_snapshot AS
SELECT equipe_id,
    date,
    SUM(activity_count) OVER (
        PARTITION BY equipe_id
        ORDER BY date ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
    ) AS log_count_ever,
    SUM(activity_count) OVER (
        PARTITION BY equipe_id
        ORDER BY date ROWS BETWEEN 29 PRECEDING AND CURRENT ROW
    ) AS log_count_30d,
    SUM(activity_count) OVER (
        PARTITION BY equipe_id
        ORDER BY date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW
    ) AS log_count_7d,
    CASE
        WHEN SUM(activity_count) OVER (
            PARTITION BY equipe_id
            ORDER BY date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW
        ) > 0 THEN 1
        ELSE 0
    END AS log_count_1stw,
    CASE
        WHEN SUM(activity_count) OVER (
            PARTITION BY equipe_id
            ORDER BY date ROWS BETWEEN 13 PRECEDING AND 7 PRECEDING
        ) > 0 THEN 1
        ELSE 0
    END AS log_count_2ndw,
    CASE
        WHEN SUM(activity_count) OVER (
            PARTITION BY equipe_id
            ORDER BY date ROWS BETWEEN 20 PRECEDING AND 14 PRECEDING
        ) > 0 THEN 1
        ELSE 0
    END AS log_count_3rdw,
    CASE
        WHEN SUM(activity_count) OVER (
            PARTITION BY equipe_id
            ORDER BY date ROWS BETWEEN 27 PRECEDING AND 21 PRECEDING
        ) > 0 THEN 1
        ELSE 0
    END AS log_count_4thw
FROM daily_calendar;