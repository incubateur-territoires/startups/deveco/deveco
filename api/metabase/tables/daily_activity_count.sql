CREATE TABLE daily_activity_count AS
SELECT s.equipe_id,
    s.date,
    COUNT(DISTINCT s.evenement_id) AS activity_count
FROM daily_s1_evenement s
GROUP BY s.equipe_id,
    s.date;