# Équipe local

```mermaid
erDiagram
  equipe__local {
    equipe_id id FK
    local_id id FK
  }

  equipe_local_contribution {
    id id
    equipe_id id FK
    local_id id FK

    nom string
    surface number
    loyer number
    description text

    equipe_local_contribution_statut_type id FK
  }

  equipe_local_contribution_type {
    id id
    nom string
  }

  equipe_local_contribution_statut_type {
    id id
    nom string
  }

  equipe_local__contact {
    equipe_id id FK
    local_id id FK

    contact_id id FK
    fonction string
  }

  equipe_local__etablissement_occupant {
    equipe_id id FK
    local_id id FK

    etablissement_id id FK
  }

  equipe_local_contribution_adresse {
    id id PK
    equipe_local_contribution_id id FK
    numero string
    voie_nom string
    distribution_speciale string
    code_postal string
    commune_id id FK
    cedex_code string
    cedex_nom string
    geolocalisation point
  }

  equipe ||--o{ contact : "connait"
  equipe |o--o{ echange : ""
  equipe |o--o{ demande : ""
  equipe |o--o{ rappel : ""

  echange }|--o{ demande : ""

  equipe ||--o{ equipe__local : ""
  equipe__local |o--o{ equipe_local__contact : ""
  equipe__local ||--o{ equipe_local_contribution : ""
  equipe__local }o--|| local : ""
  equipe |o--o{ equipe_local__contact : ""

  contact ||--o{ equipe_local__contact : ""
  equipe_local__contact }o--o{ echange : ""
  equipe_local__contact |o--|| local : ""


  echange }o--o| local : ""
  demande }o--o| local : ""
  rappel }o--o| local : ""


  equipe_local_contribution }o--|{ equipe_local_contribution_type : ""
  equipe_local_contribution }o--|| equipe_local_contribution_statut_type : ""
```

## Jointures avec le local

```mermaid
erDiagram

  equipe_local__echange {
    local_id id FK

    echange_id id FK
  }

  equipe_local__demande {
    local_id id FK

    demande_id id FK
  }

  equipe_local__rappel {
    local_id id FK

    rappel_id id FK
  }

  equipe_local_contribution__equipe_local_contribution_type {
    equipe_local_contribution_id id FK
    equipe_local_contribution_type_id id FK
  }
```
