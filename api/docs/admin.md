# Admin

```mermaid
erDiagram
  compte_type {
    id string "superadmin, deveco"
    nom nom
  }

  compte {
    id serial
    email string
    nom string
    prenom string
    cle string
    actif boolean
    compte_type_id string
  }

  mot_de_passe {
    id serial
    compte_id id FK
    hash string
    created_at date
    updated_at date
  }

  nouveautes {
    id serial
    amount integer
    created_at date
  }

  tache {
    id serial
    nom string
    debut_at date
    en_cours_at date
    fin_at date
    manuel boolean
    rapport jsonb
  }

  admin ||--o{ equipe : "crée"
  admin ||--o{ compte : "crée"

  compte ||--|| mot_de_passe : "possède"
  compte_type ||--o{ compte : "définit"
  compte ||--o| deveco : "est de type"

  equipe |o--|| deveco : "appartient à"
```
