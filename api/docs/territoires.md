# Territoires

```mermaid
erDiagram

    commune_type {
        nom string "Commune, Arrondissement"
    }

    zrr_type {
        nom string "Complet, Partiel, Non"
    }

    commune {
        code_postal string
        nom string

        commune_type_id id FK
        commune_parent_id id FK

        metropole_id id FK
        epci_id id FK
        petr_id id FK
        departement_id id FK
        region_id id FK

        zrr_type_id id FK
    }

    metropole {
        nom string
    }

    epci_type {
        nom string "CA, CC, CU, ME, ET"
    }

    epci {
        nom string
        petr_id id FK
        epci_type_id id FK
    }

    petr {
        nom string
    }

    departement {
        nom string
        region_id id
    }

    region {
        nom string
    }

    epci }o--|| epci_type : ""

    equipe }o--o{ commune : "couvre"

    metropole |o--|{ commune : "regroupe"
    commune ||--|{ commune : "chapeaute"
    epci ||--|{ commune : "regroupe"
    petr ||--|{ commune : "regroupe"
    petr ||--|{ epci : "regroupe"
    departement ||--|{ commune : "regroupe"
    region ||--|{ commune : "regroupe"
    region ||--|{ departement : "regroupe"

    commune }o--|| commune_type : ""
    commune }o--|| zrr_type : ""

```

## Jointures avec l'équipe

```mermaid
erDiagram
    equipe__commune {
        equipe_id id FK
        commune_id id FK
    }

    equipe__metropole {
        equipe_id id FK
        metropole_id id FK
    }

    equipe__epci {
        equipe_id id FK
        epci_id id FK
    }

    equipe__petr {
        equipe_id id FK
        petr_id id FK
    }

    equipe__departement {
        equipe_id id FK
        departement_id id FK
    }

    equipe__region {
        equipe_id id FK
        region_id id FK
    }
```
