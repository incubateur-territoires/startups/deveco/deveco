# Local (données publiques)

```mermaid
erDiagram
  local {
    id id

    section string
    parcelle number
    local_type_id string
    nature string
    categorie string
    niveau number
    surface_totale number
    surface_vente number
    surface_reserve number
    surface_exterieure_couverte number
  }

  local_type {
    id id
    nom string
  }

  local__proprietaire_personne {
    local_id id FK
    nom string
    prenom string
  }

  local__proprietaire_etablissement {
    local_id id FK

    etablissement_id id FK
  }

  local_adresse {
    id id PK
    equipe_local_contribution_id id FK
    numero string
    voie_nom string
    distribution_speciale string
    code_postal string
    commune_id id FK
    cedex_code string
    cedex_nom string
    geolocalisation point
  }

  local }o--|{ local_type : ""
  local ||--|| local_adresse : ""
  local ||--o{ local__proprietaire_etablissement : ""
  local ||--o{ local__proprietaire_personne : ""

  local__proprietaire_etablissement }o--|| etablissement : ""

```

## Jointures avec le local

```mermaid
erDiagram
  local__local_type {
    local_id id FK
    local_statut_id string FK
  }
```
