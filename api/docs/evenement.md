# Evènement

```mermaid
erDiagram
  evenement_type {
    id id
    nom string "création, modification, fermeture, suppression, affichage, export, connexion, authentification"
  }

  evenement {
    id serial
    evenement_type_id id FK
    diff json

    date timestamp
    deveco_id id FK
    api_type_id id FK "api_entreprise, sirene"
    admin_id id FK
  }

  evenement_type ||--o{ evenement : "qualifie"
```

## Évènements d'API

```mermaid
erDiagram
  evenement }o--o| entreprise : "modification"
  evenement }o--o| etablissement : "modification"
  evenement }o--o{ entreprise_periode : "creation"
  evenement }o--o{ etablissement_periode : "creation"


  entreprise ||--o{ entreprise_periode : ""
  etablissement ||--o{ etablissement_periode : ""

```

## Évènements de Deveco

```mermaid
erDiagram
  evenement_type ||--o{ evenement : ""

  evenement }o--o| echange : "création"
  evenement }o--o| echange : "modification"

  evenement |o--o| rappel : "création"
  evenement }o--o| rappel : "modification"
  evenement }o--o| rappel : "fermeture"

  evenement }o--o| demande : "création"
  evenement }o--o| demande : "fermeture"

  evenement }o--o| etiquette : "création"
  evenement }o--o| etiquette : "modification"
  evenement }o--o| etiquette : "suppression"

  evenement }o--o| personne : "création"
  evenement }o--o| personne : "modification"
  evenement }o--o| personne : "suppression"

  evenement }o--o| etablissement : "modification"
  evenement }o--o{ etablissement : "affichage"

  evenement }o--o{ entreprise : "affichage"

  evenement }o--o| etablissement_creation : "modification"
  evenement }o--o{ etablissement_creation : "affichage"
  evenement }o--o{ etablissement_creation : "suppression"

  evenement }o--o| local : "modification"
  evenement }o--o{ local : "affichage"
  evenement }o--o{ local : "suppression"

  evenement ||--|| recherche : "affichage"
  evenement ||--|| recherche : "export"
  evenement ||--|| geo_token_usage : "export"

  evenement }o--o| compte : "modification"
  evenement }o--o| compte : "authentification"
  evenement }o--o| compte : "connexion"

  etiquette }o--o{ etablissement : ""
  echange }o--o| etablissement : ""
  demande }o--o| etablissement : ""
  rappel }o--o| etablissement : ""

  etiquette }o--o{ local : ""
  echange }o--o| local : ""
  demande }o--o| local : ""
  rappel }o--o| local : ""

  etiquette }o--o{ etablissement_creation : ""
  echange }o--o| etablissement_creation : ""
  demande }o--o| etablissement_creation : ""
  rappel }o--o| etablissement_creation : ""

```

### Tables de jointure

```mermaid
erDiagram
  evenement__echange {
    evenement_id id FK
    echange_id id FK
  }

  evenement__rappel {
    evenement_id id FK
    rappel_id id FK
  }

  evenement__contact {
    evenement_id id FK
    contact_id id FK
  }

  evenement__personne {
    evenement_id id FK
    personne_id id FK
  }

  evenement__etiquette {
    evenement_id id FK
    etiquette_id id FK
  }

  evenement__etablissement {
    evenement_id id FK
    etablissement_id id FK
  }

  evenement__equipe_etablissement {
    evenement_id id FK
    equipe_id id FK
    etablissement_id id FK
  }

  evenement__entreprise {
    evenement_id id FK
    entreprise_id id FK
  }

  evenement__etablissement_periode {
    evenement_id id FK
    etablissement_id id FK
  }

  evenement__entreprise_periode {
    evenement_id id FK
    entreprise_id id FK
  }

  evenement__etablissement_creation {
    evenement_id id FK
    etablissement_creation_id id FK
  }

  evenement__local {
    evenement_id id FK
    local_id id FK
  }

  evenement__compte {
    evenement_id id FK
    local_id id FK
  }
```

## Évènements d'admin

```mermaid
erDiagram
  evenement }o--o| compte : "création"
  evenement }o--o| equipe : "création"
  evenement }o--o| deveco : "création"
```
