# Annuaire

```mermaid
erDiagram

  contact {
    id id
    nom string
    prenom string
    email string
    telephone string
    naissance_date string
    adresse_id id FK
    equipe_id id FK
  }

  %% https://doc.adresse.data.gouv.fr/mettre-a-jour-sa-base-adresse-locale/le-format-base-adresse-locale
  adresse {
    numero string
    voie_nom string
    distribution_speciale string
    code_postal string
    commune_id id FK
    cedex_code string
    cedex_nom string
    geolocalisation point
  }

  qpv {
    code string
    nom string
    geometrie geometry
  }

  contact_adresse {
    contact_id integer FK
    numero string
    voie_nom string
    distribution_speciale string
    code_postal string
    commune_id id FK
    cedex_code string
    cedex_nom string
    geolocalisation point
  }

  mandataire_contact_physique |o--o| contact : ""

  adresse }o--|| commune : "appartient à"

  contact ||--o{ contact__etablissement : ""
  contact__etablissement |o--|| etablissement : ""

  contact ||--o{ contact__etablissement_creation : ""
  contact__etablissement_creation |o--|| etablissement_creation : ""

  contact ||--o{ contact__local : ""
  contact__local |o--|| local : ""

  contact |o--o| adresse : "possède"

  etablissement |o--o{ etablissement_adresse : "possède"
  etablissement_creation |o--o| etablissement_creation_adresse : "possède"
  local |o--o| local_adresse : "possède"

  commune }o--o| qpv : "intersecte"
```
