# Équipe établissement

```mermaid
erDiagram
  %% etablissement__contact
  equipe_etablissement__contact {
    equipe_id id FK
    etablissement_id id FK

    contact_id id FK
    contact_source_type_id id FK

    fonction string
  }

  equipe_etablissement_contribution {
    equipe_id id FK
    etablissement_id id FK

    description text
    enseigne string
    cloture_date date
  }


  echange }|--o{ demande : ""

  equipe ||--o{ deveco : "regroupe"
  equipe ||--o{ contact : "connait"

  equipe |o--o{ etiquette : ""
  equipe |o--o{ echange : ""
  equipe |o--o{ demande : ""
  equipe |o--o{ rappel : ""

  equipe }o--o{ equipe__etablissement : "suit"
  equipe ||--o{ equipe_etablissement_contribution : ""
  equipe |o--o{ equipe_etablissement__contact : ""

  equipe_etablissement_contribution }o--|| equipe__etablissement : ""
  deveco }o--o{ equipe__etablissement : "ajoute en favoris"

  contact ||--o{ equipe_etablissement__contact : ""
  equipe_etablissement__contact }o--o{ echange : ""
  equipe_etablissement__contact |o--|| equipe__etablissement : ""

  etiquette }o--o{ equipe__etablissement : ""
  echange }o--o| equipe__etablissement : ""
  demande }o--o| equipe__etablissement : ""
  rappel }o--o| equipe__etablissement : ""

  equipe__etablissement }o--|| etablissement : ""

  etablissement ||--o{ local__etablissement_proprietaire : ""
  local__etablissement_proprietaire |o--|| local : ""

  equipe ||--|| etablissement_creation_transformation : ""
  etablissement_creation_transformation ||--|| etablissement_creation : ""
  equipe__etablissement ||--|| etablissement_creation_transformation : ""

  equipe ||--o{ zonage : "gère"
  zonage }o--o{ adresse : "contient"
```

## Jointures avec `equipeEtablissement`

```mermaid
erDiagram
  equipe__etablissement_exogene {
    equipe_id id FK
    etablissement_id id FK

    ajout_at timestamp
  }

  equipe_etablissement_contribution {
    equipe_id id FK
    etablissement_id id FK

    description text
  }

  equipe__etablissement__etiquette {
    equipe_id id FK
    etablissement_id id FK

    etiquette_id id FK
  }

  equipe__etablissement__echange {
    equipe_id id FK
    etablissement_id id FK

    echange_id id FK
  }

  equipe__etablissement__demande {
    equipe_id id FK
    etablissement_id id FK

    demande_id id FK
  }

  equipe__etablissement__rappel {
    equipe_id id FK
    etablissement_id id FK

    rappel_id id FK
  }

  deveco__etablissement_favori {
    deveco_id id FK
    etablissement_id id FK
  }

  equipe__etablissement }o--o{ equipe__etablissement : ""
  equipe }o--o{ equipe__etablissement : ""

  equipe__etablissement }o--o{ equipe__etablissement_exogene : ""
  equipe }o--o{ equipe__etablissement_exogene : ""
```
