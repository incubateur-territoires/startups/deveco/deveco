# Création d'établissement

```mermaid
erDiagram

  etablissement_creation {
    enseigne string
    description text

    equipe_id id FK
    entreprise_id id FK
  }

  etablissement_creation_transformation {
    etablissement_creation_id id FK
    etablissement_id id FK

    equipe_id id FK
    date date
  }

  etablissement_creation__contact {
    equipe_id id FK
    etablissement_creation_id id FK
    fonction string

    contact_id id FK

    createur boolean
  }

  etabissement_creation_adresse {
    etabissement_creation_id integer FK
    numero string
    voie_nom string
    distribution_speciale string
    code_postal string
    commune_id id FK
    cedex_code string
    cedex_nom string
    geolocalisation point
  }

  echange }|--o{ demande : ""

  equipe ||--o{ contact : "connaît"

  equipe |o--o{ etiquette : ""
  equipe |o--o{ echange : ""
  equipe |o--o{ demande : ""
  equipe |o--o{ rappel : ""

  equipe ||--o{ etablissement_creation : ""

  equipe ||--o{ etabissement_creation_contact : ""

  etablissement_creation ||--o| etabissement_creation_adresse : ""

  contact ||--o{ etabissement_creation_contact : ""
  etabissement_creation_contact }o--o{ echange : ""
  etabissement_creation_contact |o--|| etablissement_creation : ""

  etiquette }o--o{ etablissement_creation : ""
  echange }o--o| etablissement_creation : ""
  demande }o--o| etablissement_creation : ""
  rappel }o--o| etablissement_creation : ""

  etablissement_creation_transformation ||--|| etablissement : ""
  etablissement_creation ||--|| etablissement_creation_transformation : ""
  equipe ||--|| etablissement_creation_transformation : ""
```

## Jointures avec la création d'établissement

```mermaid
erDiagram
  etablissement_creation__etiquette {
    etablissement_creation_id id FK

    etiquette_id id FK
  }

  etablissement_creation__echange {
    etablissement_creation_id id FK

    echange_id id FK
  }

  etablissement_creation__demande {
    etablissement_creation_id id FK

    demande_id id FK
  }

  etablissement_creation__rappel {
    etablissement_creation_id id FK

    rappel_id id FK
  }
```
