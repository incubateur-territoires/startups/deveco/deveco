# Equipe

## Suivi de relation

```mermaid
erDiagram

  equipe_type {
      nom string "agence économique de développement, mairie, préfecture, etc."
  }

  territoire_type {
      nom string "Commune, Epci, Petr, Metropole, Departement, Region"
  }

  equipe {
    nom string
    equipe_type_id id FK
    territoire_type_id id FK
  }

  deveco {
    compte_id id FK
    equipe_id id FK
    bienvenue_email boolean
  }

  etiquette_type {
    nom string "localisation, activité réelle, etc."
  }

  etiquette {
    nom string
    equipe_id id FK
    etiquette_type_id id FK
  }

  echange_type {
    nom string "telephone, email, rencontre, courrier"
  }

  echange {
    nom string
    description text
    date date
    echange_type_id id FK
    equipe_id id FK
    deveco_id id FK
  }

  demande_type {
    nom string "foncier, immobilier, aide, fiscalité, accompagnement, autre, economique, rh"
  }

  demande {
    date date
    demande_type_id id FK
    equipe_id id FK
    cloture_motif string
  }

  rappel {
    nom string
    description text
    date date
    equipe_id id FK
    deveco_id id FK
  }

  recherche {
    deveco_id id FK
    evenement_id id FK
    requete string
    entite string "etablissement, etablissement_creation, local"
    export boolean
  }

  geo_token {
    deveco_id id
    token string
  }

  geo_token_usage {
    evenement_id id FK
    geo_token_id id
    url string
    user_agent string
  }

  zonage {
    equipe_id id FK
    nom string
    perimetre multipolygon
  }

  territoire_type ||--o{ equipe : "définit"
  equipe_type ||--o{ equipe : "définit"

  equipe ||--o{ deveco : "regroupe"
  equipe ||--o{ contact : "connaît"

  equipe |o--o{ etiquette : ""
  equipe |o--o{ echange : ""
  equipe |o--o{ demande : ""
  equipe |o--o{ rappel : ""
  equipe ||--o{ zonage : "gère"


  deveco ||--o{ recherche : ""
  deveco ||--o{ geo_token : ""
  deveco ||--o{ echange : ""

  geo_token ||--o{ geo_token_usage : ""

  contact }o--o{ echange : ""

  zonage ||--|| etiquette : ""

  etiquette }o--|| etiquette_type : ""
  demande }o--|| demande_type : "possède"
  echange }o--|| echange_type : "possède"
  echange }|--o{ demande : ""

  zonage }o--o{ adresse : "contient"
```

### Jointures

```mermaid
erDiagram
  contact__mandataire_personne_physique {
    mandataire_personne_physique_id id FK
    contact_id id FK
  }

  echange__demande {
    demande_id id FK
    echange_id id FK
  }

  echange__contact {
    contact_id id FK
    echange_id id FK
  }
```


### Remise à zéro des données équipe dans l'index ElasticSearch


curl -XPOST "$ELASTICSEARCH_PROTOCOL://$ELASTICSEARCH_USER:$ELASTICSEARCH_PASSWORD@$ELASTICSEARCH_URL:$ELASTICSEARCH_PORT/$ELASTICSEARCH_INDEX_ETAB/_update_by_query" -H 'Content-Type: application/json' -d' { "query": { "matchAll": {} }, "script" : { "source": "ctx._source.equipes = null", "lang": "painless" } }'
