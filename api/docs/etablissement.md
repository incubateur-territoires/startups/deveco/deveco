# Établissement (données publiques)

```mermaid
erDiagram

  entreprise {
    id id

    creation_date date

    effectif_annee string
    effectif_type_id id FK

    diffusible boolean

    categorie_entreprise_type_id id FK
    categorie_entreprise_annee int

    unite_purgee boolean
  }

  entreprise_periode {
    entreprise_id string FK

    debut_date date
    fin_date date

    siege_etablissement_id string FK

    nom string
    nom_usage string

    denomination string
    denomination_usuelle_1 string
    denomination_usuelle_2 string
    denomination_usuelle_3 string

    naf_type_id id FK
    naf_revision_type_id id FK
    categorie_juridique_type_id id FK

    actif boolean
    economie_sociale_solidaire boolean
    employeur boolean
    societe_mission boolean

    actif_change boolean
    categorie_juridique_type_change boolean
    denomination_change boolean
    denomination_usuelle_change boolean
    economie_sociale_solidaire_change boolean
    employeur_change boolean
    naf_type_change boolean
    siege_etablissement_change boolean
    nom_change boolean
    nom_usage_change boolean
    societe_mission_change boolean
  }

  personne_physique {
    entreprise_id string FK
    sexe_type_id id

    prenom_1 string
    prenom_2 string
    prenom_3 string
    prenom_4 string
    prenom_Usuel string
    pseudonyme string
  }

  personne_morale {
    entreprise_id string FK

    sigle string
    association_id id
  }

  etablissement {
    id id PK
    nic string

    creation_date date
    sirene_dernier_traitement_date date

    effectif_type_id id FK
    effectif_annee string

    entreprise_id id FK

    commune_id id FK
    diffusible boolean

    siege boolean
  }

  etablissement_periode {
    etablissement_id id FK

    debut_date date
    fin_date date

    enseigne_1 string
    enseigne_2 string
    enseigne_3 string
    denomination_usuelle string

    actif boolean
    employeur boolean

    naf_revision_type_id id FK
    naf_type_id id FK

    actif_change boolean
    enseigne_change boolean
    denomination_change boolean
    naf_type_change boolean
    employeur_change boolean
  }

  etabissement_adresse {
    etabissement_id integer FK
    numero string
    voie_nom string
    distribution_speciale string
    code_postal string
    commune_id id FK
    cedex_code string
    cedex_nom string
    geolocalisation point
  }

  exercice {
    etablissement_id id FK
    cloture_date date
    chiffre_affaire int
  }

  mandataire_personne_physique {
    entreprise_id id
    fonction string

    nom string
    prenom string
    naissance_date date

    naissance_lieu string
    naissance_pays string
    naissance_pays_code string

    nationalite string
    nationalite_code string
  }

  mandataire_personne_morale {
    entreprise_id id

    mandataire_entreprise_id id FK
    fonction string
    greffe_code string
    greffe_nom string
  }

  sexe_type {
    id id
    nom string
  }

  effectif_type {
    id id
    nom string
  }

  naf_revision_type {
    id id "NAFRev2 NAFRev1 NAF1993 NAP"
    nom string "NAF Révision 2 (2008)"
  }

  naf_type {
    id id
    naf_revision_type_id id FK
    nom string "description"
    niveau int
    parent_naf_type_id id FK "optionnel"
  }

  categorie_juridique_type {
    id id
    nom string
    niveau integer
    parent_categorie_juridique_type_id id FK
  }

  entreprise ||--o| personne_physique : "est"
  entreprise ||--o| personne_morale : "est"

  entreprise ||--o| mandataire_personne_physique : "possède"
  entreprise ||--o| mandataire_personne_morale : "possède"

  entreprise ||--o{ entreprise_periode : "possède"

  entreprise ||--|| etablissement : "a pour siège"
  entreprise |o--o{ etablissement : "possède"

  entreprise }o--|| effectif_type : ""

  personne_physique }o--|| sexe_type : ""

  entreprise_periode }o--|| naf_type : ""
  entreprise_periode }o--|| categorie_juridique_type : "possède"

  etablissement ||--o{ etablissement_periode : ""
  etablissement ||--o{ exercice : ""
  etablissement }o--|| commune : "est sur"

  etablissement }o--|| effectif_type : ""

  etablissement_periode }o--|| naf_type : ""

  naf_type ||--|{ naf_revision_type : ""
  categorie_juridique_type }o--|| categorie_juridique_type : "est enfant de"

  commune |o--o{ adresse : ""
  adresse }o--o| qpv : "intersecte"

  mandataire_personne_physique ||--o| personne : ""
```
