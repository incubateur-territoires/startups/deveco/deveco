# Sources

## Données statiques

Données externes initialisées lors de la création de l'application.

### Codes NAF

[Codes Naf sur Datagouv](https://www.data.gouv.fr/fr/datasets/r/7bb2184b-88cb-4c6c-a408-5a0081816dcd)

Il existe 4 révision de la syntaxe des codes NAF :

- `NAFrev2` depuis 2008
- `NAFrev1` depuis 2003
- `NAF1993` depuis 1993
- `NAP` depuis 1973

### Métropoles (Paris 75056, Lyon 69123, Marseille 13055)

Action manuelle anuelle avec fichier

logique métier : [issue #232](https://gitlab.com/incubateur-territoires/startups/deveco/deveco-app/-/issues/232)

1. sur le site de l'INSEE, télécharge le [fichier commune](https://www.insee.fr/fr/information/6800675) et on écrase `source.insee__commune`.

2. transforme les données de `source.insee__commune` vers `metropole`

### Départments

Action manuelle anuelle avec fichier

logique métier : [issue #500](https://gitlab.com/incubateur-territoires/startups/deveco/deveco-app/-/issues/500)

1. sur le site l'INSEE, télécharge le [fichier tsv departement](https://www.insee.fr/fr/information/6800675) et écrase `source.insee__departement`.

2. transforme les données de `source.insee__departement` vers `departement` :

### Régions

Action manuelle anuelle avec fichier

logique métier : [issue #500](https://gitlab.com/incubateur-territoires/startups/deveco/deveco-app/-/issues/500)

1. sur le site l'INSEE, télécharge le [fichier tsv region](https://www.insee.fr/fr/information/6800675) et écrase `source.insee__region`.

2. transforme les données de `source.insee__region` vers `region`

### Établissements publics territoriaux

Action manuelle unique avec fichier

logique métier : [issue #511](https://gitlab.com/incubateur-territoires/startups/deveco/deveco-app/-/issues/511)

1. télécharge le [fichier créé par Léa](https://gitlab.com/incubateur-territoires/startups/deveco/deveco-app/-/issues/511) et écrase `source.datagouv_commune_ept`.

2. transforme les données de `source.datagouv_commune_ept` vers `epci`.

### QPV (quartiers prioritaires de la ville)

Action manuelle unique avec fichier

logique métier : [issue #394](https://gitlab.com/incubateur-territoires/startups/deveco/deveco-app/-/issues/394)

1. télécharge le [fichier sur Datagouv](https://www.data.gouv.fr/fr/datasets/quartiers-prioritaires-de-la-politique-de-la-ville-qpv/) et crée une nouvelle table `source.anct__qpv_20XX`, remplit la table avec .

```bash
shp2pgsql -a -I QP_METROPOLEOUTREMER_WGS84_EPSG4326.shp > qpv.sql
```

2. transforme les données de `source.anct__qpv_20XX` vers une nouvelle table `qpv_20XX`.

3. télécharge le [fichier CSV](https://www.data.gouv.fr/fr/datasets/quartiers-prioritaires-de-la-politique-de-la-ville-qpv/) et remplace le code commune des PLM par les codes des arrondissements, pour extraire les liens commune - qpv, crée la table `commune__qpv_20XX` et remplit avec les données

4. crée une table `etablissement__qpv_20XX` et met à jour avec la requête :

```sql
CREATE TABLE temp_qpv_etab AS (
    SELECT distinct etabgeovue_siret,
        q.id AS qpv_20XX_id
    FROM qpv_20XX
    JOIN etablissement_geolocalisation_vue ON ST_Covers (q.geometrie, etabgeovue_geolocalisation)
);
INSERT INTO etablissement__qpv_20XX (etablissement_id, qpv_20XX_id)
SELECT etabgeovue_siret,
    qpv_20XX_id
FROM temp_qpv_etab
ON CONFLICT (etablissement_id, qpv_20XX_id)
DO NOTHING;
```

### Catégories juridiques

Action manuelle unique depuis un fichier

- niveau I : 9 positions définissant les grandes familles de formes juridiques
- niveau II : 37 positions définissant des catégories juridiques par rapport aux critères juridiques fondamentaux du droit
- niveau III : 260 positions permettant de préciser la catégorie juridique en tenant compte de la spécificité des dispositions juridiques concernant les unités concernées

1. télécharge le [fichier](https://www.insee.fr/fr/information/2028129) sur le site de l'Insee
2. transforme le fichier en 3 fichier csv avec les 3 niveaux de catégories juridiques
3. transforme les csv en tables `categorie_juridique_type`.

## Données dynamiques

Données externes avec une mise à jour périodique.

On n'enregistre pas le diff lors de la première mise à jour des données.

### Communes

Action manuelle anuelle avec fichier

logique métier : [issue #462](https://gitlab.com/incubateur-territoires/startups/deveco/deveco-app/-/issues/462)

1. sur le site de l'INSEE, télécharge le [fichier commune](https://www.insee.fr/fr/information/6800675) et écrase `source.insee__commune`.

2. transforme les données de `source.insee__commune` vers `tmp_commune` :

3. compare `tmp_commune` et `commune`

	- compare les données
	 - si une commune a disparu
	 - si une commune apparaît
	 - si une commune fusionne
	- met à jour `equipe_territoire`
	- rafraîchit `equipe_commune`
	- rafraîchit `equipe_etablissement`
	- etc.
	- supprime `tmp_commune`

```mermaid
erDiagram

  "source.insee__commune" {
    typecom string
    com string
    dep string
    reg string
    ctcd string
    arr string
    tncc string
    ncc string
    nccenr string
    lib_com string
    can string
    com_parent string
  }
```

### Codes postaux

Action manuelle anuelle avec fichier

1. sur le site de l'INSEE, télécharge le [fichier code postaux](https://www.data.gouv.fr/fr/datasets/base-officielle-des-codes-postaux/), on enlève le ## de la première ligne et écrase `source.laposte_code_insee__code_postal`.

### EPCI (établissements publics de coopération intercommunale)

Action manuelle anuelle avec fichier

logique métier : [issue #462](https://gitlab.com/incubateur-territoires/startups/deveco/deveco-app/-/issues/462)

1. sur le site de l'INSEE, télécharge le [fichier EPCI](https://www.insee.fr/fr/information/2510634) et [fichier de liaison epcis - communes](https://www.insee.fr/fr/information/2510634) et écrase `source.insee__epci` et `source.insee__epci_commune`.

2. transforme les données de `source.insee__epci` vers `tmp_epci` :

3. compare `tmp_epci` et `epci`

	- compare les données
	 - si une epci a disparu,
	 - si une epci apparaît, ajoute dans `epci`
	 - si une epci fusionne, regouper les données dans `epci`
	- met à jour `equipe_territoire`
	- rafraîchit `equipe_commune`
	- rafraîchit `equipe_etablissement`
	- etc.
	- supprime `tmp_epci`

### PETR (pôle d'équilibre territorial et rural)

Action manuelle anuelle avec fichier

logique métier : [issue #478](https://gitlab.com/incubateur-territoires/startups/deveco/deveco-app/-/issues/478)

1. sur le site de l'Observatoire des territoires, télécharge le [fichier PETR - EPCI (.xlsx)](https://www.observatoire-des-territoires.gouv.fr/outils/cartographie-interactive/api/v1/functions/GC_API_download.php?type=stat&nivgeo=epci2021&dataset=_zon_&indic=pays), on supprime les 4 premières lignes inutilisées et le transforme en .csv puis on écrase `source.odt_epci_petr`.

2. transforme les données de `source.odt_epci_petr` vers `tmp_epci_petr` :

3. compare `tmp_epci_petr` et `petr`

	- compare les données
	 - si un petr a disparu,
	 - si un petr apparaît, ajoute dans `petr`
	 - si un petr fusionne, regouper les données dans `petr`
	- met à jour `equipe_territoire`
	- rafraîchit `equipe_commune`
	- rafraîchit `equipe_etablissement`
	- etc.
	- supprime `tmp_epci_petr`

### ZRR (Zone de Revitalisation Rurale)

Action manuelle mensuelle avec fichier

1. télécharge le [fichier ZRR sur Data gouv](https://www.data.gouv.fr/fr/datasets/zones-de-revitalisation-rurale-zrr/#/resources) de zrr et écrase `source.datagouv_zrr`.

2. parcourt `source.datagouv_zrr` et met à jour `commune`.

```mermaid
erDiagram

	"source.datagouv_zrr" {
		insee_com string
		zrr string "C, P, N"
	}
```

### SIRENE

cron périodique sur API

```mermaid
erDiagram

	"source.insee_sirene_api__unite_legale" {
		siren string PK
		statut_diffusion_unite_legale enum
		unite_purgee_unite_legale boolean
		date_creation_unite_legale date
		sigle_unite_legale string
		sexe_unite_legale enum
		prenom1_unite_legale string
		prenom2_unite_legale string
			prenom3_unite_legale string
		prenom4_unite_legale string
		prenom_usuel_unite_legale string
		pseudonyme_unite_legale string
		identifiant_association_unite_legale string
		tranche_effectifs_unite_legale string
		annee_effectifs_unite_legale number
		date_dernier_traitement_unite_legale date
		nombre_periodes_unite_legale number
		categorie_entreprise enum
		annee_categorie_entreprise number
		date_debut date
		etat_administratif_unite_legale enum

		nom_unite_legale string
		nom_usage_unite_legale string
		denomination_unite_legale string
		denomination_usuelle1_unite_legale string
		denomination_usuelle2_unite_legale string
		denomination_usuelle3_unite_legale string

		categorie_juridique_unite_legale number
		activite_principale_unite_legale string
		nomenclature_activite_principale_unite_legale string
		nic_siege_unite_legale string
		eco_soc_sol_unite_legale string
		societe_mission_unite_legale string
		caractere_employeur_unite_legale string

		mise_a_jour_date timestamp
	}

	"source.insee_sirene_api__etablissement" {
		siren string
		nic string
		siret string
		statut_diffusion_etablissement enum
		date_etablissement_creation date
		date_debut date
		date_dernier_traitement_etablissement timestamp
		tranche_effectifs_etablissement string
		annee_effectifs_etablissement string
		etablissement_siege boolean
		activite_principale_registre_metiers_etablissement string
		etat_administratif_etablissement enum
		activite_principale_etablissement string
		nomenclature_activite_principale_etablissement string
		nombre_periodes_etablissement int
		caractere_employeur_etablissement enum

		enseigne1_etablissement string
		enseigne2_etablissement string
		enseigne3_etablissement string
		denomination_usuelle_etablissement string

		complement_adresse_etablissement string
		numero_voie_etablissement string
		indice_repetition_etablissement string
		type_voie_etablissement string
		libelle_voie_etablissement string
		distribution_speciale_etablissement string
		code_postal_etablissement string
		code_commune_etablissement string
		libelle_commune_etablissement string
		libelle_commune_etranger_etablissement string
		code_cedex_etablissement string
		libelle_cedex_etablissement string
		code_pays_etranger_etablissement string
		libelle_pays_etranger_etablissement string

		mise_a_jour_date timestamp
	}

	"source.insee_api_sirene__unite_legale_periode" {
		siren string
		date_fin date "YYYY-MM-DD"
		date_debut date
		etat_administratif_unite_legale string
		nom_unite_legale string
		nom_usage_unite_legale string
		denomination_unite_legale string
		denomination_usuelle1_unite_legale string
		denomination_usuelle2_unite_legale string
		denomination_usuelle3_unite_legale string
		categorie_juridique_unite_legale string
		activite_principale_unite_legale string
		nomenclature_activite_principale_unite_legale string
		nic_siege_unite_legale string
		economie_sociale_solidaire_unite_legale string
		societe_mission_unite_legale string
		caractere_employeur_unite_legale string

		changement_etat_administratif_unite_legale string
		changement_nom_unite_legale string
		changement_nom_usage_unite_legale string
		changement_denomination_unite_legale string
		changement_denomination_usuelle_unite_legale string
		changement_categorie_juridique_unite_legale string
		changement_activite_principale_unite_legale string
		changement_nic_siege_unite_legale string
		changement_economie_sociale_solidaire_unite_legale string
		changement_societe_mission_unite_legale string
		changement_caractere_employeur_unite_legale string
	}

	"source.insee_api_sirene__etablissement_periode" {
		siren string
		nic string
		siret string

		date_fin date
		date_debut date

		enseigne1_etablissement string
		enseigne2_etablissement string
		enseigne3_etablissement string
		denomination_usuelle_etablissement string

		etat_administratif_etablissement string
		caractere_employeur_etablissement string
		activite_principale_etablissement string
		nomenclature_activite_principale_etablissement string

		changement_etat_administratif_etablissement string
		changement_enseigne_etablissement string
		changement_denomination_usuelle_etablissement string
		changement_activite_principale_etablissement string
		changement_caractere_employeur_etablissement string
	}
```

1. requête API SIRENE et met à jour les donnés dans les tables `source.insee_sirene_api__etablissement`, `source.insee_sirene_api__etablissement_periode`, `source.insee_sirene_api__unite_legale`, `source.insee_sirene_api__unite_legale_periode`.

2. met à jour l'index de recherche

Action manuelle mensuelle avec fichier

1. télécharge le [fichier StockUniteLegale_utf8.zip sur Data gouv](https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/) et dézippe le fichier csv

2. exécute le script d'import dans src/db/query/sql/source/sirene-unite-import.sql

3. télécharge le [fichier StockEtablissement_utf8.zip sur Data gouv](https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/) et dézippe le fichier csv

4. exécute le script d'import dans src/db/query/sql/source/sirene-etab-import.sql

5. télécharge le [fichier StockUniteLegaleHistorique_utf8.zip sur Data gouv](https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/) et dézippe le fichier csv

6. exécute le script d'import dans src/db/query/sql/source/sirene-unite-periode-import.sql

7. télécharge le [fichier StockEtablissementHistorique_utf8.zip sur Data gouv](https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/) et dézippe le fichier csv

8. exécute le script d'import dans src/db/query/sql/source/sirene-etab-periode-import.sql

### SIRENE liens de succession

Mise à jour quotidienne sur API

```mermaid
erDiagram
	"source.insee_sirene_api__lien_succession" {
		siret_etablissement_predecesseur string
		siret_etablissement_successeur string
		date_lien_succession timestamp
		date_dernier_traitement_lien_succession timestamp
		transfert_siege boolean
		continuite_economique boolean
		mise_a_jour_at timestamp
	}
```

Action manuelle mensuelle avec fichier

1. télécharge le [fichier stock des liens d'entreprise](https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/)

2. exécute le script d'import dans src/db/query/sql/source/sirene-lien-import.sql

### SIRENE entreprise en doublon

Pas de récupération depuis l'API

```mermaid
erDiagram

	"source.insee_sirene_api__unite_legale_doublon" {
		siren string
		siren_doublon string
		date_dernier_traitement_doublon timestamp
		mise_a_jour_at timestamp
	}
```

Action manuelle mensuelle avec fichier

1. télécharge le [fichier stock des doublons d'entreprise](https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/)

2. exécute le script d'import dans src/db/query/sql/source/sirene-doublon-import.sql

### Géolocalisation Sirene des établissements

Action manuelle mensuelle avec fichier

1. télécharge le fichier [SIRENE de géoloc sur Data Gouv](https://www.data.gouv.fr/fr/datasets/geolocalisation-des-etablissements-du-repertoire-sirene-pour-les-etudes-statistiques/)

2. exécute le script d'import dans src/db/query/sql/source/sirene-geoloc-import.sql

### Géolocalisation etalab des établissements

Le géocodage est fait tous les jours lors de la MAJ SIRENE via l'api Etalab, adresse par adresse.

```mermaid
erDiagram

	"source.etalab__etablissement_geoloc" {
		siret id
		longitude float
		latitude float
		geo_score float
		geo_type string
		geo_adresse string
		geo_id string
		geo_ligne string
		geo_l4 string
		geo_l5 string
		hash string

		banAdresse boolean
	}

```

Les données du fichier [étalab de géoloc](https://www.data.gouv.fr/fr/datasets/base-sirene-des-etablissements-siret-geolocalisee-avec-la-base-dadresse-nationale-ban/) ne sont pas assez précises et consistantes, (les établissements sont parfois localisés dans une ville à l'autre bout du pays).

Le fichier ne doit pas être utilisé comme tel, seulement à des fins d'amorçage de la base de données (et encore, préférer plutôt la géolocalisation INSEE).

### Base commune entreprise (effectifs)

Action manuelle mensuelle avec fichier

1. télécharge et dézippe le fichier d'effectifs depuis https://minio.data.social.gouv.fr/

2. exécute le script d'import dans src/db/query/sql/source/rcd-import.sql

3. met à jour l'index ElasticSearch :
	`curl https://deveco.incubateur.anct.gouv.fr/api/admin/index/JETON\?tache\=etablissement-index-effectif-update`

### API entreprise (mandataires sociaux, exercices)

Action manuelle sur API lors de la première consultation d'un établissement

```mermaid
erDiagram

	"source.api_entreprise_mandataire_social" {
		type personne_physique
		fonction string

		nom string
		prenom string
		date_naissance date
		date_naissance_timestamp timestamp
		lieu_naissance string
		pays_naissance string
		code_pays_naissance string
		nationalite string
		code_nationalite string

		numero_identification string
		raison_sociale string
		code_greffe string
		libelle_greffe string
	}

	"source.api_entreprise_exercice" {
		chiffre_d_affaires string
		date_fin_exercice int
	}
```

1. requête API Entreprise et met à jour les donnés dans les tables `source.api_entreprise_mandataire_social` et `source.api_entreprise_exercice`.

2. copie depuis `source.api_entreprise_mandataire_social` et `source.api_entreprise_exercice` vers :

	- `mandataire_social`
	- `etablissement_exercice`

3. enregistre le diff des données mise à jour dans `etablissement`.

### Céréma (locaux)

```mermaid
erDiagram

	"source.cerema_local" {
		invar string
		dnvoiri string
		dindic string
		dvoilib string
		idcom string
		ccosec string
		dnupla string
		cconlc string
		typeact string
		dniv string
		sprincp number
		ssecp number
		ssecncp number
		sparkp number
		sparkncp number
		idprocpte string
	}

	"source.cerema_local_nature" {
		id string
		nom string
	}

	"source.cerema_local_categorie" {
		id string
		nom string
	}
```

### Subventions DGCL

```mermaid
erDiagram

	"source.dgcl__subventions" {
		siret string
		type string
		attributaire string
		objet string
		nature string
		montant string
		convention_date string
		versement_date string
	}
```

1. récupération du fichier source sur [https://www.data.gouv.fr/fr/datasets/subventions-politique-de-la-ville/](https://www.data.gouv.fr/fr/datasets/subventions-politique-de-la-ville/)

2. conversion du fichier source à l'encodage UTF-8 si besoin

3. ajout des données dans la base, en créant des colonnes temporaires au besoin pour minimiser la modification du fichier source (vérifier manuellement la commande ci-dessous car tous les fichiers n'ont pas les mêmes colonnes) :

4. exécute le script d'import dans src/db/query/sql/source/dgcl-import.sql

### Index Egapro

```mermaid
erDiagram

	"source.egapro__index_egalite" {
		siren string
		annee number
		noteEcartRemuneration number
		noteEcartTauxAugmentationHorsPromotion number
		noteEcartTauxPromotion number
		noteEcartTauxAugmentation number
		noteRetourCongeMaternite number
		noteHautesRemunerations number
		noteIndex number
		cadresPourcentageFemmes float
		cadresPourcentageHommes float
		membresPourcentageFemmes float
		membresPourcentageHommesfloat
		miseAJourAt date
	}
```

1. récupération des fichiers source sur [https://egapro.travail.gouv.fr/index-egapro/recherche](https://egapro.travail.gouv.fr/index-egapro/recherche) et [https://egapro.travail.gouv.fr/representation-equilibree/recherche](https://egapro.travail.gouv.fr/representation-equilibree/recherche)

2. conversion du fichier source XLSX en CSV

3. ajout des données dans la base, en créant des colonnes temporaires au besoin pour minimiser la modification du fichier source (vérifier manuellement la commande ci-dessous car tous les fichiers n'ont pas les mêmes colonnes) :

4. exécute le script d'import dans src/db/query/sql/source/egapro-import.sql

### BODACC Procédures collectives

```mermaid
erDiagram

	"source.bodacc__procedure_collective" {
		siren string
		famille string
		nature string
		date date
	}
```

1. utilisation du projet de téléchargement et compilation des fichiers [https://gitlab.com/incubateur-territoires/startups/deveco/deveco-import-bodacc](https://gitlab.com/incubateur-territoires/startups/deveco/deveco-import-bodacc)

2. ajout des données dans la base à partir du fichier .csv généré

```sql
TRUNCATE source.bodacc__procedure_collective;

\copy source.bodacc__procedure_collective from 'bodacc-procedure-collective.csv' delimiter ',' csv header;

```

### INPI Ratios financiers

```mermaid
erDiagram

	"source.inpi__ratio_financier" {
		siren string
		date_cloture_exercice date
		chiffre_d_affaires double
		marge_brute double
		ebe double
		ebit double
		resultat_net double
		taux_d_endettement double
		ratio_de_liquidite double
		ratio_de_vetuste double
		autonomie_financiere double
		poids_bfr_exploitation_sur_ca double
		couverture_des_doubleerets double
		caf_sur_ca double
		capacite_de_remboursement double
		marge_ebe double
		resultat_courant_avant_impots_sur_ca double
		poids_bfr_exploitation_sur_ca_jours double
		rotation_des_stocks_jours double
		credit_clients_jours double
		credit_fournisseurs_jours double
		type_bilan string
		confidentiality string
		total_general double
	}
```

1. récupération du fichier source sur [https://www.data.gouv.fr/fr/datasets/ratios-financiers-bce-inpi/](https://www.data.gouv.fr/fr/datasets/ratios-financiers-bce-inpi)

2. copie du fichier dans la table `source.inpi__ratio_financier`

3. exécute le script d'import dans src/db/query/sql/source/inpi-ratio-import.sql

### ESS France

```mermaid
erDiagram

	"source.ess_france__entreprise_ess" {
		entreprise_id string
	}
```

1. récupération des fichiers XLSX source sur [https://www.ess-france.org/fr/la-liste-des-entreprises-de-less](https://www.ess-france.org/fr/la-liste-des-entreprises-de-less)

2. copie des fichiers à la racine de l'api et lancement du script de transformation en fichier CSV

```bash
./node_modules/.bin/ts-node src/script/ess-import.ts Liste1CvlNorHdfPdl.xlsx Liste2OccIdf.xlsx Liste3AuraBfcPacaCorse.xlsx Liste4BrGeNaMayReGuaMarGuy.xlsx
```

3. copie du fichier dans les sources

```sql
TRUNCATE source.ess_france__entreprise_ess;

\copy source.ess_france__entreprise_ess (entreprise_id)
	FROM './_full/_sources/ess-france_entreprise.csv' CSV HEADER DELIMITER ',';
```

4. mettre à jour l'index ElasticSearch :
   `curl https://deveco.incubateur.anct.gouv.fr/api/admin/sirene/JETON\?tache\=etablissement-index-ess-update`
