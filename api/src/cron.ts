import * as cron from 'node-cron';

import { serviceCron } from './service/cron';

export function cronStart() {
	if (process.env.NODE_ENV === 'test') {
		return;
	}

	if (process.env.CRON_MASTER !== 'ImTheCronMaster') {
		return;
	}

	console.logInfo('[Cron] starting cronjobs...');

	// compute Rappels reminders every day at 22:00 by default
	const rappelsReminderSchedule = process.env.RAPPELS_REMINDER_SCHEDULE
		? `${process.env.RAPPELS_REMINDER_SCHEDULE}`
		: '0 22 * * *';

	cron.schedule(rappelsReminderSchedule, () => void serviceCron.rappelRemindersSend());

	// send activity logs to each Territory
	// every two tuesdays (done in `equipeActiviteSend`)
	const equipeActviteSchedule = process.env.TERRITORY_ACTIVITY_SCHEDULE
		? `${process.env.TERRITORY_ACTIVITY_SCHEDULE}`
		: '30 6 * * 2';

	cron.schedule(equipeActviteSchedule, () => void serviceCron.equipeActiviteSend());

	// envoi d'un email de rappel aux devecos qui ne se sont pas connectés dans les 10 jours après la création de leur compte
	const devecoRappel10JoursSchedule = process.env.DEVECO_RAPPEL_10_JOURS_SCHEDULE
		? `${process.env.DEVECO_RAPPEL_10_JOURS_SCHEDULE}`
		: '0 2 * * *';

	cron.schedule(devecoRappel10JoursSchedule, () => void serviceCron.relance10JoursSend());

	// envoi d'un email de rappel aux devecos qui ne se sont pas connectés dans les 30 jours après la création de leur compte
	const devecoRappel30JoursSchedule = process.env.DEVECO_RAPPEL_30_JOURS_SCHEDULE
		? `${process.env.DEVECO_RAPPEL_30_JOURS_SCHEDULE}`
		: '0 2 * * *';

	cron.schedule(devecoRappel30JoursSchedule, () => void serviceCron.relance30JoursSend());

	// met à jour les données SIRENE et géocode les établissements sans géolocalisation
	const sireneDailySchedule = process.env.SIRENE_UPDATE_DAILY_SCHEDULE
		? `${process.env.SIRENE_UPDATE_DAILY_SCHEDULE}`
		: '5 0 * * *';

	cron.schedule(sireneDailySchedule, () => void serviceCron.sireneUpdate());

	const miseAJourContactsBrevoSchedule = process.env.CONTACTS_BREVO_UPDATE_DAILY_SCHEDULE
		? `${process.env.CONTACTS_BREVO_UPDATE_DAILY_SCHEDULE}`
		: '0 6 * * 0';

	cron.schedule(miseAJourContactsBrevoSchedule, () => void serviceCron.brevoContactUpdateMany());

	// met à jour les données BODACC de procédures collectives
	const bodaccDailySchedule = process.env.BODACC_UPDATE_DAILY_SCHEDULE
		? `${process.env.BODACC_UPDATE_DAILY_SCHEDULE}`
		: '1 0 * * *';

	cron.schedule(bodaccDailySchedule, () => void serviceCron.bodaccUpdate());

	// anonymisation des données personnelles
	const anonymisationDailySchedule = process.env.ANONYMISATION_DAILY_SCHEDULE
		? `${process.env.ANONYMISATION_DAILY_SCHEDULE}`
		: '1 0 * * *';

	cron.schedule(anonymisationDailySchedule, () => void serviceCron.anonymisation());

	if (process.env.MAIL_IMAP_PASSWORD) {
		// récupération des mails de création de brouillon
		const brouillonMailSchedule = process.env.BROUILLON_MAIL_SCHEDULE
			? `${process.env.BROUILLON_MAIL_SCHEDULE}`
			: '*/1 * * * *';

		cron.schedule(brouillonMailSchedule, () => void serviceCron.brouillonMailFetch());
	} else {
		console.logInfo('Pas de configuration IMAP');
	}

	if (process.env.API_ENTREPRISE_ARAIGNEE === 'oui') {
		// récupération du réseau des filiales / détentions depuis les liasses fiscales
		setTimeout(() => void serviceCron.liasseFiscaleReseau());
	}

	// rafraîchissement des tables Metabase
	const metabaseRefreshSchedule = process.env.METABASE_REFRESHSCHEDULE
		? `${process.env.METABASE_REFRESH_SCHEDULE}`
		: '0 3 * * *';

	cron.schedule(metabaseRefreshSchedule, () => void serviceCron.metabaseRefresh());
}
