import builder from 'elastic-builder';

import { Equipe, SituationGeographique } from '../../db/types';
import * as dbQueryEquipe from '../../db/query/equipe';

export function nestedQuery({ equipeId, query }: { equipeId: Equipe['id']; query: builder.Query }) {
	const equipeQuery = builder.termQuery('equipes.equipe_id', equipeId);

	return builder
		.nestedQuery()
		.path('equipes')
		.query(builder.boolQuery().must([equipeQuery, query]));
}

export function nestedQueryExists({
	equipeId,
	field,
}: {
	equipeId: Equipe['id'];
	field: Parameters<typeof builder.existsQuery>[0];
}) {
	return nestedQuery({
		equipeId,
		query: builder.existsQuery(field),
	});
}

export function nestedQueryBool({
	equipeId,
	must = [],
	mustNot = [],
	should = [],
}: {
	equipeId: Equipe['id'];
	must?: builder.Query[];
	mustNot?: builder.Query[];
	should?: builder.Query[];
}) {
	const query = builder.boolQuery();

	if (must && must.length) {
		query.must(must);
	}

	if (mustNot && mustNot.length) {
		query.mustNot(mustNot);
	}

	if (should && should.length) {
		query.should(should);
	}

	return nestedQuery({
		equipeId,
		query,
	});
}

export async function buildEquipeTerritoireQuery({
	equipeId,
	geo,
}: {
	equipeId: Equipe['id'];
	geo?: SituationGeographique | null;
}): Promise<{ must: builder.Query[]; mustNot: builder.Query[] }> {
	const must: builder.Query[] = [];
	const mustNot: builder.Query[] = [];

	const territoireIds: string[] = [];

	if (geo === 'france') {
		return {
			must,
			mustNot,
		};
	}

	const equipe = await dbQueryEquipe.equipeWithTerritoireGet({ equipeId });
	if (!equipe) {
		throw new Error("cette équipe n'existe pas");
	}

	if (equipe.territoireTypeId === 'france') {
		return {
			must,
			mustNot,
		};
	}

	switch (equipe.territoireTypeId) {
		case 'commune':
			territoireIds.push(
				...equipe.communes.flatMap((c) => [
					c.communeId,
					...c.commune.communeFilles.map((f) => f.id),
				])
			);
			break;
		case 'epci':
			territoireIds.push(...equipe.epcis.map((c) => c.epciId));
			break;
		case 'petr':
			territoireIds.push(...equipe.petrs.map((c) => c.petrId));
			break;
		case 'territoire_industrie':
			territoireIds.push(...equipe.territoireIndustries.map((c) => c.territoireIndustrieId));
			break;
		case 'departement':
			territoireIds.push(...equipe.departements.map((c) => c.departementId));
			break;
		case 'region':
			territoireIds.push(...equipe.regions.map((c) => c.regionId));
			break;
		case 'metropole':
			territoireIds.push(...equipe.metropoles.map((c) => c.metropoleId));
			break;
	}

	// territoire

	const endogene = builder.termsQuery(`territoire_${equipe.territoireTypeId}_id`, territoireIds);

	if (geo === 'hors') {
		mustNot.push(endogene);
	} else {
		must.push(endogene);
	}

	return {
		must,
		mustNot,
	};
}
