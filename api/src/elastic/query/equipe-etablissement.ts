import builder from 'elastic-builder';
import { ApiResponse } from '@opensearch-project/opensearch';
import { subYears, startOfYear, getYear } from 'date-fns';

import {
	replaceBanVoieToInseeVoie,
	replaceBanNumeroToInseeNumero,
} from '../../lib/normalisationAdresse';
import { codeChiffreDAffairesToChiffres } from '../../lib/exercices';
import { isDefined, regexpEscape, fromEntriesToCamelCaseKeys, unaccent } from '../../lib/utils';
import { isSiret, isSiren } from '../../lib/regexp';
import { mapChiffresChiffreDAffaires } from '../../lib/exercices';
import { codeToRangeDict } from '../../lib/etablissement-effectif';
import {
	DemandeTypeId,
	Deveco,
	Equipe,
	Etiquette,
	Zonage,
	EquipeEtablissementContribution,
	EquipeEtablissementEtiquette,
	ActionDemande,
	ActionEchange,
	ActionRappel,
	Entreprise,
	Etablissement,
	EntrepriseType,
} from '../../db/types';
import * as dbQueryEtablissement from '../../db/query/etablissement';
import * as dbQueryEquipeEtablissement from '../../db/query/equipe-etablissement';
import * as dbQueryEquipe from '../../db/query/equipe';
import elastic from '../../elastic';
import * as elasticQueryQpv from '../../elastic/query/qpv';
import { mappings as elasticQueryQpvMappings } from '../../elastic/query/qpv';
import * as elasticQueryZonage from '../../elastic/query/zonage';
import { mappings as elasticQueryZonageMappings } from '../../elastic/query/zonage';
import * as elasticQueryEtablissement from '../../elastic/query/etablissement';
import * as elasticQueryEquipe from '../../elastic/query/equipe';

import mapping from '../../elastic/mappings/etablissement_index.json';

const properties = fromEntriesToCamelCaseKeys(mapping.properties);

const propertiesEtablissementSubventions = fromEntriesToCamelCaseKeys(
	mapping.properties.etablissement_subventions.properties,
	properties.etablissementSubventions
);
const propertiesEtablissementSubventionsSet = fromEntriesToCamelCaseKeys(
	mapping.properties.etablissement_subventions.properties
);

const propertiesEquipes = fromEntriesToCamelCaseKeys(
	mapping.properties.equipes.properties,
	properties.equipes
);
const propertiesEquipesSet = fromEntriesToCamelCaseKeys(mapping.properties.equipes.properties);

const propertiesEquipesContribution = fromEntriesToCamelCaseKeys(
	mapping.properties.equipes.properties.contribution.properties,
	propertiesEquipes.contribution
);
const propertiesEquipesContributionSet = fromEntriesToCamelCaseKeys(
	mapping.properties.equipes.properties.contribution.properties
);

export const mappings = {
	properties,
	propertiesEtablissementSubventions,
	propertiesEquipes,
	propertiesEquipesContribution,
};

function motToQuery({
	mot,
	equipeId,
	fuzzy = false,
}: {
	mot: string;
	equipeId: Equipe['id'];
	fuzzy: boolean;
}) {
	if (!mot) {
		return null;
	}

	if (isSiret(mot)) {
		return builder.termQuery(properties.etablissementId, mot.replaceAll(/\s/g, ''));
	}

	if (isSiren(mot)) {
		return builder.termQuery(properties.entrepriseId, mot.replaceAll(/\s/g, ''));
	}

	const motRegexp = `.*${regexpEscape(unaccent(mot.toLowerCase()))}.*`;

	const conditions: builder.Query[] = [
		...(fuzzy
			? [builder.multiMatchQuery([properties.etablissementNoms, properties.entrepriseNoms], mot)]
			: [
					builder.regexpQuery(properties.etablissementNoms, motRegexp),
					builder.regexpQuery(properties.entrepriseNoms, motRegexp),
				]),
		// NOTE hack, si l'equipeId = 0, on ne cherche pas dans les champs perso
		...(equipeId
			? [
					elasticQueryEquipe.nestedQueryBool({
						equipeId,
						should: [
							builder.regexpQuery(propertiesEquipes.contactsNoms, motRegexp),
							builder.regexpQuery(propertiesEquipesContribution.enseigne, motRegexp),
						],
					}),
				]
			: []),
	];

	return builder.boolQuery().should(conditions);
}

async function buildQuery(
	params: dbQueryEquipeEtablissement.EtablissementsParams,
	{ fuzzy = false } = {}
) {
	const must: builder.Query[] = [];
	const mustNot: builder.Query[] = [];
	// utilisable seulement pour les documents qui ont un sous-document `equipes` de l'équipe courante
	// non pertinent pour les requêtes négatives
	const nested: builder.Query[] = [];
	const filter: builder.Query[] = [];

	const { equipeId } = params;

	const { must: mustEquipeTerritoire, mustNot: mustNotEquipeTerritoire } =
		await elasticQueryEquipe.buildEquipeTerritoireQuery({
			...params,
			equipeId,
		});
	must.push(...mustEquipeTerritoire);
	mustNot.push(...mustNotEquipeTerritoire);

	if (params.communes?.length) {
		must.push(builder.termsQuery(properties.territoireCommuneId, params.communes));
	}

	if (params.cp) {
		must.push(builder.termQuery(properties.adresseCodePostal, params.cp));
	}

	if (params.rue) {
		const rue = unaccent(params.rue.replaceAll('-', ' '));

		const rueNormalized = replaceBanVoieToInseeVoie(rue);

		if (rue !== rueNormalized) {
			must.push(
				builder
					.boolQuery()
					.should([
						builder.matchQuery(properties.adresseVoieNom, rue).operator('and'),
						builder.matchQuery(properties.adresseVoieNom, rueNormalized).operator('and'),
					])
			);
		} else {
			must.push(builder.matchQuery(properties.adresseVoieNom, rue).operator('and'));
		}
	}

	if (params.numero) {
		const numero = params.numero.replaceAll(' ', '');

		const numeroBanNormalized = replaceBanNumeroToInseeNumero(numero);

		if (numero !== numeroBanNormalized) {
			must.push(
				builder
					.boolQuery()
					.should([
						builder.matchQuery(properties.adresseNumero, numero),
						builder.matchQuery(properties.adresseNumero, numeroBanNormalized),
					])
			);
		} else {
			// TODO : faire matcher les numéros de voie sans indice de répétition
			// const numeroRegexp = `^${regexpEscape(numero.toLowerCase())}[^0-9]*$`;

			must.push(builder.matchQuery(properties.adresseNumero, numero));
		}
	}

	function radiusTileGet(precision: number, latitude = 0) {
		const earthCircumference = 40075000;
		const tileSize = earthCircumference / Math.pow(2, precision);
		const radius = (tileSize / 2) * Math.cos((latitude * Math.PI) / 180);

		return radius;
	}

	if (isDefined(params.longitude) && isDefined(params.latitude)) {
		const point = builder.geoPoint().lon(params.longitude).lat(params.latitude);

		const zoom = params.z;

		const precision =
			zoom &&
			(zoom <= 10 ? zoom + 1 : zoom <= 11 ? zoom + 2 : zoom <= 14 ? zoom + 3 : MAX_PRECISION);

		const rayon = precision ? radiusTileGet(precision, params.latitude) : (params.rayon ?? 1);

		must.push(builder.geoDistanceQuery(properties.adresseGeolocalisation, point).distance(rayon));
	}

	if (params.nwse.length) {
		const [lat1, lon1, lat2, lon2] = params.nwse;

		if (isDefined(lon1) && isDefined(lat1) && isDefined(lon2) && isDefined(lat2)) {
			const point1 = builder.geoPoint().lon(lon1).lat(lat1);
			const point2 = builder.geoPoint().lon(lon2).lat(lat2);

			must.push(
				builder
					.geoBoundingBoxQuery(properties.adresseGeolocalisation)
					.topLeft(point1)
					.bottomRight(point2)
			);
		}
	}

	if (params.zrrs?.length) {
		must.push(builder.termsQuery(properties.territoireZrrTypeId, params.zrrs));
	}

	if (isDefined(params.acv)) {
		must.push(builder.termQuery(properties.territoireAcv, params.acv));
	}

	if (isDefined(params.pvd)) {
		must.push(builder.termQuery(properties.territoirePvd, params.pvd));
	}

	if (isDefined(params.va)) {
		must.push(builder.termQuery(properties.territoireVa, params.va));
	}

	if (params.territoireIndustries?.length) {
		if (params.territoireIndustries.includes('tous')) {
			must.push(builder.existsQuery(properties.territoireTerritoireIndustrieId));
		} else {
			must.push(
				builder.termsQuery(properties.territoireTerritoireIndustrieId, params.territoireIndustries)
			);
		}
	}

	if (params.qpvs?.length) {
		if (params.qpvs.includes('tous')) {
			must.push(
				builder.boolQuery().should([
					builder.existsQuery(properties.adresseQpv2024Id),
					elasticQueryEquipe.nestedQueryExists({
						equipeId,
						field: propertiesEquipes.adresseQpv2024Id,
					}),
				])
			);
		} else {
			must.push(
				...params.qpvs.map((qpv) =>
					builder
						.geoShapeQuery(properties.adresseGeolocalisation)
						.indexedShape(
							builder
								.indexedShape()
								.index(elasticQueryQpv.index)
								.id(qpv)
								.path(elasticQueryQpvMappings.properties.qpvGeometrie)
						)
				)
			);
		}
	}

	if (params.afrs?.length) {
		const afrConditionsShould: builder.Query[] = [];

		if (params.afrs.includes('O')) {
			afrConditionsShould.push(builder.termQuery(properties.territoireAfr, true));
		}

		if (params.afrs.includes('P')) {
			afrConditionsShould.push(builder.termQuery(properties.territoireAfr, false));
		}

		if (params.afrs.includes('N')) {
			afrConditionsShould.push(
				builder.boolQuery().mustNot(builder.existsQuery(properties.territoireAfr))
			);
		}

		must.push(builder.boolQuery().should(afrConditionsShould));
	}

	if (params.epcis?.length) {
		must.push(builder.termsQuery(properties.territoireEpciId, params.epcis));
	}

	if (params.departements?.length) {
		must.push(builder.termsQuery(properties.territoireDepartementId, params.departements));
	}

	if (params.regions?.length) {
		must.push(builder.termsQuery(properties.territoireRegionId, params.regions));
	}

	if (params.etat?.length) {
		const should: builder.Query[] = [];

		const clotureCondition = elasticQueryEquipe.nestedQueryExists({
			equipeId,
			field: propertiesEquipesContribution.clotureDate,
		});

		if (params.etat.includes('A')) {
			const etatCondition = builder.termQuery(properties.etablissementActif, true);

			should.push(builder.boolQuery().must(etatCondition).mustNot(clotureCondition));
		}

		if (params.etat.includes('F')) {
			const etatCondition = builder.termQuery(properties.etablissementActif, false);

			should.push(builder.boolQuery().must(etatCondition).mustNot(clotureCondition));
		}

		if (params.etat.includes('S')) {
			should.push(clotureCondition);
		}

		must.push(builder.boolQuery().should(should));
	}

	if (params.entrepriseTypes?.length) {
		const entrepriseCategorieTypeConditionsShould: builder.Query[] = [];
		const entrepriseTypes: EntrepriseType[] = ['PME', 'ETI', 'GE'];

		entrepriseTypes.forEach((entrepriseType) => {
			if (params.entrepriseTypes.includes(entrepriseType)) {
				entrepriseCategorieTypeConditionsShould.push(
					builder.termQuery(properties.entrepriseCategorieTypeId, entrepriseType)
				);
			}
		});

		if (params.entrepriseTypes.includes('Inconnu')) {
			entrepriseCategorieTypeConditionsShould.push(
				builder.boolQuery().mustNot(builder.existsQuery(properties.entrepriseCategorieTypeId))
			);
		}

		must.push(builder.boolQuery().should(entrepriseCategorieTypeConditionsShould));
	}

	async function nafConditionBuild(nafTypes: string[]) {
		const codesNafAutres = nafTypes.filter((c) => c.length > 2);
		const codesNaf2 = nafTypes.filter((c) => c.length === 2);
		const codesNaf1 = nafTypes.filter((c) => c.length === 1);

		if (codesNaf1.length) {
			const nafTypes = await dbQueryEtablissement.nafTypeGetManyByParent({
				parentNafTypeIds: codesNaf1,
			});

			codesNaf2.push(...nafTypes.map(({ id }) => id));
		}

		const should: builder.Query[] = [];

		if (codesNaf2.length) {
			should.push(
				...codesNaf2.map((codeNaf) =>
					builder.prefixQuery(properties.etablissementNafTypeId, codeNaf)
				)
			);
		}

		if (codesNafAutres.length) {
			should.push(builder.termsQuery(properties.etablissementNafTypeId, codesNafAutres));
		}

		return builder.boolQuery().should(should);
	}

	if (params.codesNaf?.length) {
		const nafCondition = await nafConditionBuild(params.codesNaf);

		must.push(nafCondition);
	}

	if (params.codesNafSans?.length) {
		const nafCondition = await nafConditionBuild(params.codesNafSans);

		mustNot.push(nafCondition);
	}

	if (params.effectifInconnu || params.effectifMin || params.effectifMax) {
		const shoulds = [];
		if (params.effectifInconnu) {
			shoulds.push(
				builder.boolQuery().mustNot(builder.existsQuery(properties.etablissementEffectifEma))
			);
		}

		if (params.effectifMin || params.effectifMax) {
			const rangeQuery = builder.rangeQuery(properties.etablissementEffectifEma);
			if (params.effectifMin && !isNaN(Number(params.effectifMin))) {
				rangeQuery.gte(Number(params.effectifMin));
			}

			if (params.effectifMax && !isNaN(Number(params.effectifMax))) {
				rangeQuery.lte(Number(params.effectifMax));
			}

			shoulds.push(rangeQuery);
		}

		must.push(builder.boolQuery().should([...shoulds]));
	}

	if (params.effectifVarTypes?.length) {
		const should = [];

		if (params.effectifVarTypes.includes('hausse')) {
			const rangeQuery = builder.rangeQuery(properties.etablissementEffectifEmaVariation);
			if (params.effectifVarMin && !isNaN(Number(params.effectifVarMin))) {
				rangeQuery.gte(Number(params.effectifVarMin));
			} else {
				rangeQuery.gt(0);
			}

			if (params.effectifVarMax && !isNaN(Number(params.effectifVarMax))) {
				rangeQuery.lte(Number(params.effectifVarMax));
			}

			should.push(rangeQuery);
		}

		if (params.effectifVarTypes.includes('baisse')) {
			const rangeQuery = builder.rangeQuery(properties.etablissementEffectifEmaVariation);
			if (params.effectifVarMin && !isNaN(Number(params.effectifVarMin))) {
				rangeQuery.lte(-Number(params.effectifVarMin));
			} else {
				rangeQuery.lt(0);
			}

			if (params.effectifVarMax && !isNaN(Number(params.effectifVarMax))) {
				rangeQuery.gte(-Number(params.effectifVarMax));
			}

			should.push(rangeQuery);
		}

		if (should.length) {
			must.push(builder.boolQuery().should([...should]));
		}
	}

	function catJurConditionBuild(catJurs: string[]) {
		const [categoriesJuridiques2, categoriesJuridiques3] = catJurs.reduce(
			([categoriesJuridiques2, categoriesJuridiques3]: [string[], string[]], c) => {
				if (c.length === 2) {
					categoriesJuridiques2.push(c);
				} else {
					categoriesJuridiques3.push(c);
				}

				return [categoriesJuridiques2, categoriesJuridiques3];
			},
			[[], []]
		);

		if (categoriesJuridiques2.length) {
			return builder.termsQuery(
				properties.entrepriseCategorieJuridique2TypeId,
				categoriesJuridiques2
			);
		}

		if (categoriesJuridiques3.length) {
			return builder.termsQuery(
				properties.entrepriseCategorieJuridique3TypeId,
				categoriesJuridiques3
			);
		}

		return null;
	}

	const categoriesJuridiquesNonCourantes = [
		'00',
		'21',
		'22',
		'27',
		'65',
		'71',
		'72',
		'73',
		'74',
		'81',
		'83',
		'84',
		'85',
		'91',
	];

	// on ne peut pas utiliser le filtre "catégorie juridique courante"
	// en même temps que celui sur les "catégories juridiques"
	// c'est soit l'un soit l'autre
	if (params.categorieJuridiqueCourante === true) {
		mustNot.push(
			builder.termsQuery(
				properties.entrepriseCategorieJuridique2TypeId,
				categoriesJuridiquesNonCourantes
			)
		);
	} else {
		if (params.categoriesJuridiques?.length) {
			const catJurCondition = catJurConditionBuild(params.categoriesJuridiques);
			if (catJurCondition) {
				must.push(catJurCondition);
			}
		}

		if (params.categoriesJuridiquesSans?.length) {
			const catJurCondition = catJurConditionBuild(params.categoriesJuridiquesSans);
			if (catJurCondition) {
				mustNot.push(catJurCondition);
			}
		}
	}

	if (isDefined(params.ess)) {
		if (params.ess === true) {
			must.push(builder.termQuery(properties.entrepriseEconomieSocialeSolidaire, params.ess));
		} else {
			mustNot.push(builder.termQuery(properties.entrepriseEconomieSocialeSolidaire, true));
		}
	}

	if (isDefined(params.micro)) {
		if (params.micro === true) {
			must.push(builder.termQuery(properties.entrepriseMicro, params.micro));
		} else {
			mustNot.push(builder.termQuery(properties.entrepriseMicro, true));
		}
	}

	if (params.subventions?.length) {
		const subventionAggregationScript = `ArrayList x = params['_source']['${properties.etablissementSubventions}'];
if (x == null) {
	return 0;
}

double total = x.stream()
	.filter(subvention ->
		(params.anneeMin == null || subvention['${propertiesEtablissementSubventionsSet.conventionAnnee}'] >= params.anneeMin)
		&& (params.anneeMax == null || subvention['${propertiesEtablissementSubventionsSet.conventionAnnee}'] <= params.anneeMax)
	)
	.mapToDouble(subvention ->
		(subvention.containsKey('${propertiesEtablissementSubventionsSet.montantTotal}') ?
			subvention['${propertiesEtablissementSubventionsSet.montantTotal}'] :
			0
		)
	)
	.sum();

if (
	total > 0
	&& (params.montantMin == null || params.montantMin <= total)
	&& (params.montantMax == null || total <= params.montantMax)
) {
	return 2;
}

return 0;`;

		if (
			isDefined(params.subAnneeMin) ||
			isDefined(params.subAnneeMax) ||
			isDefined(params.subMontantMin) ||
			isDefined(params.subMontantMax)
		) {
			must.push(
				builder
					.functionScoreQuery()
					.minScore(1)
					.query(
						builder
							.nestedQuery()
							.path(properties.etablissementSubventions)
							.ignoreUnmapped(false)
							.scoreMode('min')
							.boost(1)
							.query(builder.matchAllQuery())
					)
					.functions([
						builder.scriptScoreFunction(
							builder.script('inline', subventionAggregationScript).lang('painless').params({
								anneeMin: params.subAnneeMin,
								anneeMax: params.subAnneeMax,
								montantMin: params.subMontantMin,
								montantMax: params.subMontantMax,
							})
						),
					])
					.scoreMode('multiply')
					.maxBoost(3.4028235e38)
					.boost(1)
			);
		}

		const shoulds = [];

		if (params.subventions.includes('oui')) {
			shoulds.push(builder.termQuery(properties.etablissementSubvention, true));
		}
		if (params.subventions.includes('non')) {
			shoulds.push(
				builder.boolQuery().mustNot(builder.existsQuery(properties.etablissementSubvention))
			);
		}

		if (shoulds.length) {
			must.push(builder.boolQuery().should(shoulds));
		}
	}

	if (isDefined(params.procedure)) {
		if (params.procedure) {
			must.push(builder.termQuery(properties.entrepriseProcedureCollective, params.procedure));

			if (params.procApres || params.procAvant) {
				const rangeQuery = builder.rangeQuery(properties.entrepriseProcedureCollectiveDates);

				if (params.procApres) {
					rangeQuery.gte(params.procApres.toISOString());
				}

				if (params.procAvant) {
					rangeQuery.lte(params.procAvant.toISOString());
				}

				must.push(rangeQuery);
			}
		} else {
			mustNot.push(builder.termQuery(properties.entrepriseProcedureCollective, true));
		}
	}

	if (params.filiales.length) {
		must.push(builder.termsQuery(properties.entrepriseParticipationFilialeDe, params.filiales));
	}

	if (params.detentions.length) {
		must.push(builder.termsQuery(properties.entrepriseParticipationDetient, params.detentions));
	}

	const chiffreDAffairesChiffresToCondition = (
		r: builder.Query[],
		[basse, haute]: [number | null, number | null]
	) => {
		const rangeQuery = builder.rangeQuery(properties.entrepriseChiffreDAffaires);

		if (basse === null && haute === null) {
			return r;
		}

		if (isDefined(basse)) {
			rangeQuery.gte(basse);
		}

		if (isDefined(haute)) {
			rangeQuery.lt(haute);
		}

		r.push(rangeQuery);

		return r;
	};

	if (params.cas?.length) {
		const conditions = params.cas
			.map(codeChiffreDAffairesToChiffres)
			.reduce(chiffreDAffairesChiffresToCondition, []);

		if (conditions.length) {
			must.push(builder.boolQuery().should(conditions));
		}

		if (params.cas?.includes('ND')) {
			mustNot.push(builder.existsQuery(properties.entrepriseChiffreDAffaires));
		}
	}

	if (params.caVarTypes?.length) {
		const should = [];

		if (params.caVarTypes.includes('hausse')) {
			const rangeQuery = builder.rangeQuery(properties.entrepriseChiffreDAffairesVariation);
			if (params.caVarMin && !isNaN(Number(params.caVarMin))) {
				rangeQuery.gte(Number(params.caVarMin));
			} else {
				rangeQuery.gte(0);
			}

			if (params.caVarMax && !isNaN(Number(params.caVarMax))) {
				rangeQuery.lte(Number(params.caVarMax));
			}

			should.push(rangeQuery);
		}

		if (params.caVarTypes.includes('baisse')) {
			const rangeQuery = builder.rangeQuery(properties.entrepriseChiffreDAffairesVariation);
			if (params.caVarMin && !isNaN(Number(params.caVarMin))) {
				rangeQuery.lte(-Number(params.caVarMin));
			} else {
				rangeQuery.lte(0);
			}

			if (params.caVarMax && !isNaN(Number(params.caVarMax))) {
				rangeQuery.gte(-Number(params.caVarMax));
			}

			should.push(rangeQuery);
		}

		if (should.length) {
			must.push(builder.boolQuery().should([...should]));
		}
	}

	if (isDefined(params.siege)) {
		must.push(builder.termQuery(properties.etablissementSiege, params.siege));
	}

	if (params.creeApres || params.creeAvant) {
		const rangeQuery = builder.rangeQuery(properties.etablissementCreationDate);

		if (params.creeApres) {
			rangeQuery.gte(params.creeApres.toISOString());
		}

		if (params.creeAvant) {
			rangeQuery.lte(params.creeAvant.toISOString());
		}

		must.push(rangeQuery);
	}

	if (params.fermeApres || params.fermeAvant) {
		const rangeQuery = builder.rangeQuery(properties.etablissementFermetureDate);

		if (params.fermeApres) {
			rangeQuery.gte(params.fermeApres.toISOString());
		}

		if (params.fermeAvant) {
			rangeQuery.lte(params.fermeAvant.toISOString());
		}

		must.push(rangeQuery);
	}

	if (params.recherche) {
		if (isSiret(params.recherche)) {
			must.push(
				builder.termQuery(properties.etablissementId, params.recherche.replaceAll(/\s/g, ''))
			);
		} else if (isSiren(params.recherche)) {
			must.push(builder.termQuery(properties.entrepriseId, params.recherche.replaceAll(/\s/g, '')));
		} else {
			must.push(
				...params.recherche.split(/[ -]/).flatMap(
					(recherche) =>
						motToQuery({
							equipeId,
							mot: recherche,
							fuzzy,
						}) ?? []
				)
			);
		}
	}

	if (params.geoloc?.length) {
		const should: builder.Query[] = [];

		const geolocPersoCondition = builder
			.nestedQuery()
			.path(properties.equipes)
			.query(builder.existsQuery(propertiesEquipesContribution.adresseGeolocalisation));

		if (params.geoloc.includes('O')) {
			const geolocCondition = builder.existsQuery(properties.adresseGeolocalisation);

			should.push(builder.boolQuery().must(geolocCondition).mustNot(geolocPersoCondition));
		}

		if (params.geoloc.includes('N')) {
			const geolocCondition = builder.existsQuery(properties.adresseGeolocalisation);

			should.push(builder.boolQuery().mustNot([geolocCondition, geolocPersoCondition]));
		}

		if (params.geoloc.includes('P')) {
			should.push(geolocPersoCondition);
		}

		must.push(builder.boolQuery().should(should));
	}

	if (params.egaproIndiceMin !== null || params.egaproIndiceMax !== null) {
		const rangeQuery = builder.rangeQuery(properties.entrepriseEgaproIndice);
		if (params.egaproIndiceMin && !isNaN(Number(params.egaproIndiceMin))) {
			rangeQuery.gte(Number(params.egaproIndiceMin));
		} else {
			rangeQuery.gt(0);
		}

		if (params.egaproIndiceMax && !isNaN(Number(params.egaproIndiceMax))) {
			rangeQuery.lte(Number(params.egaproIndiceMax));
		}

		must.push(rangeQuery);
	}

	if (params.egaproCadresMin !== null || params.egaproCadresMax !== null) {
		const rangeQuery = builder.rangeQuery(properties.entrepriseEgaproCadres);
		if (params.egaproCadresMin && !isNaN(Number(params.egaproCadresMin))) {
			rangeQuery.gte(Number(params.egaproCadresMin));
		} else {
			rangeQuery.gt(0);
		}

		if (params.egaproCadresMax && !isNaN(Number(params.egaproCadresMax))) {
			rangeQuery.lte(Number(params.egaproCadresMax));
		}

		must.push(rangeQuery);
	}

	if (params.egaproInstancesMin !== null || params.egaproInstancesMax !== null) {
		const rangeQuery = builder.rangeQuery(properties.entrepriseEgaproInstances);
		if (params.egaproInstancesMin && !isNaN(Number(params.egaproInstancesMin))) {
			rangeQuery.gte(Number(params.egaproInstancesMin));
		} else {
			rangeQuery.gt(0);
		}

		if (params.egaproInstancesMax && !isNaN(Number(params.egaproInstancesMax))) {
			rangeQuery.lte(Number(params.egaproInstancesMax));
		}

		must.push(rangeQuery);
	}

	// perso

	const activiteIds: Etiquette['id'][] = [];

	if (params.activites?.length) {
		const ids = (
			await dbQueryEquipe.etiquetteGetManyByTypeAndNoms({
				equipeId,
				etiquetteTypeId: 'activite',
				noms: params.activites,
			})
		).map((e) => e.id);

		activiteIds.push(...ids);
	}

	if (activiteIds.length) {
		if (params.activitesToutes) {
			const conditions = activiteIds.map((id) =>
				builder.termQuery(propertiesEquipes.etiquettes, id)
			);
			nested.push(builder.boolQuery().must(conditions));
		} else {
			nested.push(builder.termsQuery(propertiesEquipes.etiquettes, activiteIds));
		}
	}

	if (params.localisations?.length) {
		const etiquettes = await dbQueryEquipe.etiquetteGetManyByTypeAndNoms({
			equipeId,
			etiquetteTypeId: 'localisation',
			noms: params.localisations,
		});

		const conditions = etiquettes.map((etiquette) => {
			const conditionLocalisation = builder
				.nestedQuery()
				.path(properties.equipes)
				.query(builder.termQuery(propertiesEquipes.etiquettes, etiquette.id));

			if (etiquette.zonage?.id) {
				return builder
					.boolQuery()
					.should([
						conditionLocalisation,
						builder
							.geoShapeQuery(properties.adresseGeolocalisation)
							.indexedShape(
								builder
									.indexedShape()
									.index(elasticQueryZonage.index)
									.id(etiquette.zonage.id.toString())
									.path(elasticQueryZonageMappings.properties.zonageGeometrie)
							),
					]);
			}

			return conditionLocalisation;
		});

		if (conditions.length) {
			if (params.localisationsToutes) {
				must.push(builder.boolQuery().must(conditions));
			} else {
				must.push(builder.boolQuery().should(conditions));
			}
		}
	}

	const motCleIds: Etiquette['id'][] = [];

	if (params.motsCles?.length) {
		const ids = (
			await dbQueryEquipe.etiquetteGetManyByTypeAndNoms({
				equipeId,
				etiquetteTypeId: 'mot_cle',
				noms: params.motsCles,
			})
		).map((e) => e.id);

		motCleIds.push(...ids);
	}

	if (motCleIds.length) {
		if (params.motsClesTous) {
			const conditions = motCleIds.map((id) => builder.termQuery(propertiesEquipes.etiquettes, id));
			nested.push(builder.boolQuery().must(conditions));
		} else {
			nested.push(builder.termsQuery(propertiesEquipes.etiquettes, motCleIds));
		}
	}

	if (params.demandeTypeIds?.length) {
		nested.push(builder.termsQuery(propertiesEquipes.demandes, params.demandeTypeIds));
	}

	if (isDefined(params.favori) && params.devecoId !== null) {
		if (params.favori === true) {
			nested.push(builder.termQuery(propertiesEquipes.favoris, params.devecoId));
		} else {
			mustNot.push(
				elasticQueryEquipe.nestedQueryExists({
					equipeId,
					field: propertiesEquipes.favoris,
				})
			);
		}
	}

	if (isDefined(params.avecContact)) {
		if (params.avecContact === true) {
			nested.push(builder.termQuery(propertiesEquipes.contacts, true));
		} else {
			mustNot.push(
				elasticQueryEquipe.nestedQuery({
					equipeId,
					query: builder.termQuery(propertiesEquipes.contacts, true),
				})
			);
		}
	}

	if (isDefined(params.ancienCreateur)) {
		if (params.ancienCreateur === true) {
			nested.push(builder.termQuery(propertiesEquipes.ancienCreateur, true));
		} else {
			mustNot.push(
				elasticQueryEquipe.nestedQueryExists({
					equipeId,
					field: propertiesEquipes.ancienCreateur,
				})
			);
		}
	}

	if (params.accompagnes?.length) {
		const shoulds = [];

		if (params.accompagnes.includes('oui')) {
			let query;

			if (isDefined(params.accompagnesApres) || isDefined(params.accompagnesAvant)) {
				const rangeQuery = builder.rangeQuery(propertiesEquipes.suiviDates);

				if (isDefined(params.accompagnesApres)) {
					rangeQuery.gte(params.accompagnesApres.toISOString());
				}

				if (isDefined(params.accompagnesAvant)) {
					rangeQuery.lte(params.accompagnesAvant.toISOString());
				}
				query = rangeQuery;
			} else {
				query = builder.existsQuery(propertiesEquipes.suiviDates);
			}

			// on cherche les documents qui ont un sous-document `equipes` de l'équipe courante avec `suivi_dates`
			shoulds.push(
				elasticQueryEquipe.nestedQuery({
					equipeId,
					query,
				})
			);
		}

		if (params.accompagnes.includes('non')) {
			// on cherche les documents qui n'ont pas de sous-document `equipes` de l'équipe courante et `suivi_dates`
			shoulds.push(
				builder.boolQuery().mustNot(
					elasticQueryEquipe.nestedQueryExists({
						equipeId,
						field: propertiesEquipes.suiviDates,
					})
				)
			);
		}

		if (shoulds.length) {
			must.push(builder.boolQuery().should(shoulds));
		}
	}

	if (nested.length) {
		must.push(
			elasticQueryEquipe.nestedQueryBool({
				equipeId,
				must: nested,
			})
		);
	}

	const query = builder.boolQuery();

	if (must.length) {
		query.must(must);
	}

	if (mustNot.length) {
		query.mustNot(mustNot);
	}

	if (filter.length) {
		query.filter(filter);
	}

	return query;
}

function etablissementIdsFormat(resultsHits: ApiResponse): Etablissement['siret'][] {
	/* eslint-disable-next-line @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-return */
	return resultsHits.body.hits.hits.map((r: any) => r._source[properties.etablissementId]);
}

function toFixedNumber(val?: number | null): number {
	if (!isDefined(val)) {
		return 0;
	}

	return Number(val.toFixed(2));
}

const MAX_PRECISION = 23;

function aggregationGeoBuildMany({ precision }: { precision?: number | null }) {
	const precisionComputed = Math.min(precision ?? 2, MAX_PRECISION);

	const clusterAggregation = elastic.helpers.builder
		.geoTileGridAggregation('clusters', properties.adresseGeolocalisation)
		.precision(precisionComputed)
		.size(10000)
		.agg(
			elastic.helpers.builder.geoCentroidAggregation('centroid', properties.adresseGeolocalisation)
		);

	if (precisionComputed === MAX_PRECISION) {
		clusterAggregation.agg(
			elastic.helpers.builder.cardinalityAggregation(
				'geolocalisations',
				properties.adresseGeolocalisation
			)
		);
	}

	const debutYear = startOfYear(new Date());

	const anneeAggregations = Array.from({ length: 11 }, (_, index) => {
		const currentYear = getYear(subYears(debutYear, 10 - index));
		const nextYear = subYears(debutYear, 10 - index - 1);

		const agg = elastic.helpers.builder
			.filterAggregation(
				`${currentYear}`,
				builder
					.boolQuery()
					.should([
						builder.rangeQuery(properties.etablissementCreationDate).lt(nextYear.toISOString()),
						builder.boolQuery().mustNot(builder.existsQuery(properties.etablissementCreationDate)),
					])
			)
			.agg(clusterAggregation);

		if (index === 9) {
			agg.agg(
				elastic.helpers.builder.statsBucketAggregation('stats').bucketsPath('clusters._count')
			);
		}

		return agg;
	});

	return anneeAggregations;
}

function aggregationStatsBuildMany({ equipeId }: { equipeId?: Equipe['id'] }) {
	return [
		elastic.helpers.builder.filterAggregation('total', elastic.helpers.builder.matchAllQuery()),

		elastic.helpers.builder.cardinalityAggregation('entreprise', properties.entrepriseId),

		elastic.helpers.builder
			.filterAggregation(
				'effectif',
				elastic.helpers.builder.existsQuery(properties.etablissementEffectifEma)
			)
			.agg(elastic.helpers.builder.sumAggregation('total', properties.etablissementEffectifEma))
			.agg(
				elastic.helpers.builder
					.percentilesAggregation('median', properties.etablissementEffectifEma)
					.percents([50])
			)
			.agg(
				elastic.helpers.builder
					.rangeAggregation('effectifTranche', properties.etablissementEffectifEma)
					.ranges(
						Object.entries(codeToRangeDict)
							.filter((e) => e[0] !== 'NN')
							.map(([key, [from, to]]) => ({
								to,
								from,
								key,
							}))
					)
					.agg(
						elastic.helpers.builder.filterAggregation(
							'siege',
							elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
						)
					)
			)
			.agg(
				elastic.helpers.builder.filterAggregation(
					'commune',
					elastic.helpers.builder.termQuery(properties.territoireCommuneId, true)
				)
			)
			.agg(
				elastic.helpers.builder
					.termsAggregation('nafEffectif', properties.etablissementNafTypeId)
					.agg(
						elastic.helpers.builder.sumAggregation('effectif', properties.etablissementEffectifEma)
					)
					.agg(
						elastic.helpers.builder
							.filterAggregation(
								'siege',
								elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
							)
							.agg(
								elastic.helpers.builder.sumAggregation(
									'effectif',
									properties.etablissementEffectifEma
								)
							)
					)
					.order('effectif', 'desc')
			)
			.agg(
				elastic.helpers.builder
					.filterAggregation(
						'qpv',
						elastic.helpers.builder.existsQuery(properties.adresseQpv2024Id)
					)
					.agg(
						elastic.helpers.builder
							.termsAggregation('qpvEffectif', properties.adresseQpv2024Id)

							.agg(
								elastic.helpers.builder.sumAggregation(
									'effectif',
									properties.etablissementEffectifEma
								)
							)
							.agg(
								elastic.helpers.builder.filterAggregation(
									'siege',
									elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
								)
							)
							.order('effectif', 'desc')
					)
			)
			.agg(
				elastic.helpers.builder
					.termsAggregation('catjurEffectif', properties.entrepriseCategorieJuridique2TypeId)
					.agg(
						elastic.helpers.builder.sumAggregation('effectif', properties.etablissementEffectifEma)
					)
					.agg(
						elastic.helpers.builder.filterAggregation(
							'siege',
							elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
						)
					)
					.order('effectif', 'desc')
			),

		elastic.helpers.builder
			.filterAggregation(
				'siege',
				elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
			)
			.agg(
				elastic.helpers.builder
					.filterAggregation(
						'ca',
						elastic.helpers.builder.existsQuery(properties.entrepriseChiffreDAffaires)
					)
					.agg(
						elastic.helpers.builder.sumAggregation('total', properties.entrepriseChiffreDAffaires)
					)
					.agg(
						elastic.helpers.builder
							.percentilesAggregation('median', properties.entrepriseChiffreDAffaires)
							.percents([50])
					)
					.agg(
						elastic.helpers.builder
							.rangeAggregation('caTranche', properties.entrepriseChiffreDAffaires)
							.ranges(
								Object.entries(mapChiffresChiffreDAffaires)
									.filter((e) => e[0] !== 'ND')
									.map(([key, [from, to]]) => ({
										to,
										from,
										key,
									}))
							)
					)
					.agg(
						elastic.helpers.builder
							.termsAggregation('nafCa', properties.etablissementNafTypeId)
							.agg(
								elastic.helpers.builder.sumAggregation('ca', properties.entrepriseChiffreDAffaires)
							)
							.order('ca', 'desc')
					)
					.agg(
						elastic.helpers.builder
							.termsAggregation('catjurCa', properties.entrepriseCategorieJuridique2TypeId)
							.agg(
								elastic.helpers.builder.sumAggregation('ca', properties.entrepriseChiffreDAffaires)
							)
							.order('ca', 'desc')
					)
					.agg(
						elastic.helpers.builder
							.filterAggregation(
								'qpv',
								elastic.helpers.builder.existsQuery(properties.adresseQpv2024Id)
							)
							.agg(
								elastic.helpers.builder
									.termsAggregation('qpvCa', properties.adresseQpv2024Id)
									.agg(
										elastic.helpers.builder.sumAggregation(
											'ca',
											properties.entrepriseChiffreDAffaires
										)
									)
									.order('ca', 'desc')
							)
					)
			),

		elastic.helpers.builder
			.filterAggregation('qpv', elastic.helpers.builder.existsQuery(properties.adresseQpv2024Id))
			.agg(
				elastic.helpers.builder.filterAggregation(
					'siege',
					elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
				)
			)
			.agg(
				elastic.helpers.builder
					.termsAggregation('qpvListe', properties.adresseQpv2024Id)
					.agg(
						elastic.helpers.builder.filterAggregation(
							'siege',
							elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
						)
					)
			),

		elastic.helpers.builder
			.filterAggregation(
				'actif',
				elastic.helpers.builder
					.boolQuery()
					.should([elastic.helpers.builder.termQuery(properties.etablissementActif, true)])
					.mustNot(
						equipeId
							? [
									elasticQueryEquipe.nestedQueryExists({
										equipeId,
										field: propertiesEquipesContribution.clotureDate,
									}),
								]
							: []
					)
			)
			.agg(
				elastic.helpers.builder.filterAggregation(
					'siege',
					elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
				)
			),

		elastic.helpers.builder
			.filterAggregation(
				'ess',
				elastic.helpers.builder.termQuery(properties.entrepriseEconomieSocialeSolidaire, true)
			)
			.agg(
				elastic.helpers.builder.filterAggregation(
					'siege',
					elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
				)
			),

		elastic.helpers.builder
			.filterAggregation(
				'micro',
				elastic.helpers.builder.termQuery(properties.entrepriseMicro, true)
			)
			.agg(
				elastic.helpers.builder.filterAggregation(
					'siege',
					elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
				)
			),

		elastic.helpers.builder
			.filterAggregation(
				'subvention',
				elastic.helpers.builder.termQuery(properties.etablissementSubvention, true)
			)
			.agg(
				elastic.helpers.builder.filterAggregation(
					'siege',
					elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
				)
			)
			.agg(
				elastic.helpers.builder
					.nestedAggregation('nested', properties.etablissementSubventions)
					.agg(
						elastic.helpers.builder.sumAggregation(
							'total',
							propertiesEtablissementSubventions.montantTotal
						)
					)
					.agg(
						elastic.helpers.builder
							.termsAggregation('annee', propertiesEtablissementSubventions.conventionAnnee)
							.agg(
								elastic.helpers.builder.sumAggregation(
									'total',
									propertiesEtablissementSubventions.montantTotal
								)
							)
							.order('_key', 'asc')
					)
			),

		elastic.helpers.builder
			.termsAggregation('catjur', properties.entrepriseCategorieJuridique2TypeId)
			.agg(
				elastic.helpers.builder.filterAggregation(
					'siege',
					elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
				)
			),

		// stats de territoires
		elastic.helpers.builder
			.termsAggregation('communes', properties.territoireCommuneId)
			.agg(
				elastic.helpers.builder.filterAggregation(
					'siege',
					elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
				)
			),
		elastic.helpers.builder
			.termsAggregation('epcis', properties.territoireEpciId)
			.agg(
				elastic.helpers.builder.filterAggregation(
					'siege',
					elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
				)
			),
		elastic.helpers.builder
			.termsAggregation('departements', properties.territoireDepartementId)
			.agg(
				elastic.helpers.builder.filterAggregation(
					'siege',
					elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
				)
			),
		elastic.helpers.builder
			.termsAggregation('regions', properties.territoireRegionId)
			.agg(
				elastic.helpers.builder.filterAggregation(
					'siege',
					elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
				)
			),

		elastic.helpers.builder
			.termsAggregation('naf', properties.etablissementNafTypeId)
			.agg(
				elastic.helpers.builder.filterAggregation(
					'siege',
					elastic.helpers.builder.termQuery(properties.etablissementSiege, true)
				)
			),

		elastic.helpers.builder
			.filterAggregation(
				'creation',
				elastic.helpers.builder
					.rangeQuery(properties.etablissementCreationDate)
					.gte('now-10y/y')
					.lte('now')
			)
			.agg(
				elastic.helpers.builder
					.dateHistogramAggregation('creation', properties.etablissementCreationDate)
					//	.interval('year')
					// ElasticSearch
					.calendarInterval('1y')
			),

		elastic.helpers.builder
			.filterAggregation(
				'fermeture',
				elastic.helpers.builder
					.rangeQuery(properties.etablissementFermetureDate)
					.gte('now-10y/y')
					.lte('now')
			)
			.agg(
				elastic.helpers.builder
					.dateHistogramAggregation('fermeture', properties.etablissementFermetureDate)
					//	.interval('year')
					// ElasticSearch
					.calendarInterval('1y')
			),
	];
}

export async function eqEtabUnsortedGetMany(
	paramsBatch: dbQueryEquipeEtablissement.EtablissementsParams[],
	fields: (keyof typeof mapping.properties)[]
): Promise<
	{
		[properties.etablissementId]: number;
		[properties.etablissementNoms]: string[];
		[properties.etablissementSiege]: boolean;
		[properties.entrepriseNoms]: string[];
		[properties.adresseNumero]: string;
		[properties.adresseVoieNom]: string;
		[properties.territoireCommuneId]: number;
	}[][]
> {
	const queries = await Promise.all(
		paramsBatch.map(async (params) => ({
			query: await buildQuery(params, { fuzzy: true }),
			fields: fields ? fields : [properties.etablissementId],
			limit: params.elementsParPage ?? 20,
		}))
	);

	const response = await elastic.msearch(elasticQueryEtablissement.index, queries);

	/* eslint-disable-next-line @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-return */
	return response?.body.responses.map((r: any) => r.hits.hits.map((h: any) => h._source));
}

interface Bucket {
	key: string;
	doc_count: number;
	siege?: { doc_count: number; effectif?: { value: number } };
	effectif?: { value: number };
	ca?: { value: number };
	total?: { value: number };
	coordinates: [number, number];
}

function statBucketFormat(stats?: Bucket[]) {
	return (
		stats?.map((stat) => ({
			id: stat.key.toString(),
			count: stat.doc_count,
			value: stat.total?.value ?? stat.effectif?.value ?? stat.ca?.value ?? null,
			countSieges: stat.siege?.doc_count ?? null,
			valueSieges: stat.siege?.effectif?.value ?? null,
		})) ?? []
	);
}

function geoStatFormat(stats: any) {
	const { lon, lat } = stats.centroid.location;

	return {
		key: stats.key,
		count: stats.doc_count,
		// permet de dire que c'est un cluster de groupe
		geolocalisations: stats.geolocalisations?.value ?? 1000,
		coordinates: [lon, lat],
	};
}

function aggregationGeoFormatMany(aggregations: any) {
	const { stats, ...annee } = aggregations ?? {};

	const anneeCourante = getYear(new Date());

	return {
		stats: annee?.[anneeCourante].stats,

		annees:
			Object.entries(annee).map(([annee, { clusters }]: any) => {
				return {
					annee,
					/* eslint-disable-next-line @typescript-eslint/no-unsafe-call */
					clusters: clusters?.buckets.map(geoStatFormat),
				};
			}) ?? [],
	};
}

function aggregationStatFormatMany(aggregations: any) {
	return {
		// territoire
		communes: statBucketFormat(aggregations.communes?.buckets),
		epcis: statBucketFormat(aggregations.epcis?.buckets),
		metropoles: statBucketFormat(aggregations.metroples?.buckets),
		territoireIndustries: statBucketFormat(aggregations.territoireIndustries?.buckets),
		petrs: statBucketFormat(aggregations.petrs?.buckets),
		departements: statBucketFormat(aggregations.departements?.buckets),
		regions: statBucketFormat(aggregations.regions?.buckets),
		//
		qpvs: statBucketFormat(aggregations.qpv?.qpvListe?.buckets),
		qpvsEffectifs: statBucketFormat(aggregations.effectif?.qpv?.qpvEffectif?.buckets),
		qpvsCas: statBucketFormat(aggregations.siege?.ca?.qpv.qpvCa?.buckets),
		nafs: statBucketFormat(aggregations.naf?.buckets),
		nafsEffectifs: statBucketFormat(aggregations.effectif?.nafEffectif?.buckets),
		nafsCas: statBucketFormat(aggregations.siege?.ca?.nafCa?.buckets),
		catjurs: statBucketFormat(aggregations.catjur?.buckets),
		catjursEffectifs: statBucketFormat(aggregations.effectif?.catjurEffectif?.buckets),
		catjursCas: statBucketFormat(aggregations.siege?.ca?.catjurCa?.buckets),
		cas: statBucketFormat(aggregations.siege?.ca?.caTranche?.buckets),
		effectifs: statBucketFormat(aggregations.effectif?.effectifTranche?.buckets),
		creations: statBucketFormat(aggregations.creation?.creation?.buckets),
		fermetures: statBucketFormat(aggregations.fermeture?.fermeture?.buckets),
		subventionsAnnees: statBucketFormat(aggregations.subvention?.nested?.annee?.buckets),
		etablissements: {
			total: aggregations.total?.doc_count as number,
			entreprises: aggregations.entreprise?.value as number,
			sieges: aggregations.siege?.doc_count as number,
			actifs: aggregations.actif?.doc_count as number,
			actifsSieges: aggregations.actif?.siege?.doc_count as number,
			avecCasSieges: aggregations.siege?.ca?.doc_count as number,
			avecEffectifs: aggregations.effectif?.doc_count as number,
			enQpv: aggregations.qpv?.doc_count as number,
			enQpvAvecEffectifs: aggregations.effectif?.qpv?.doc_count as number,
			enQpvAvecCas: aggregations.siege?.ca?.qpv?.doc_count as number,
			enQpvSieges: aggregations.qpv?.siege?.doc_count as number,
			avecSubvention: aggregations.subvention?.doc_count,
			avecSubventionSieges: aggregations.subvention?.siege?.doc_count as number,
			ess: aggregations.ess?.doc_count as number,
			essSieges: aggregations.ess?.siege?.doc_count as number,
			micro: aggregations.micro?.doc_count as number,
			microSieges: aggregations.micro?.siege?.doc_count as number,
			caTotalSieges: aggregations.siege?.ca?.total?.value as number,
			caMedianSieges: toFixedNumber(aggregations.siege?.ca?.median?.values['50.0'] as number),
			effectifs: aggregations.effectif?.total?.value as number,
			effectifsMedian: toFixedNumber(aggregations.effectif?.median?.values['50.0'] as number),
			subventions: aggregations.subvention?.nested?.total?.value as number,
		},
	};
}

export async function eqEtabIdSortedGetMany(
	params: dbQueryEquipeEtablissement.EtablissementsParams,
	statsGet?: boolean
) {
	const query = await buildQuery(params);

	let sort;

	if (params.tri === 'creation') {
		sort = builder.sort(properties.etablissementCreationDate, 'desc');
	} else if (params.tri === 'fermeture') {
		sort = builder.sort(properties.etablissementFermetureDate, 'desc');
	} else {
		const equipeQuery = builder.termQuery(propertiesEquipes.equipeId, params.equipeId);

		sort = builder.sort(propertiesEquipes.consultationStr, 'desc').nested({
			path: properties.equipes,
			filter: equipeQuery,
		});
	}

	// const requestBodySearch = builder.requestBodySearch().query(query).sort(sort);
	// console.log(JSON.stringify(requestBodySearch, null, 2));

	const offset = params.elementsParPage ? ((params.page || 1) - 1) * params.elementsParPage : 0;

	const aggs = statsGet
		? aggregationStatsBuildMany({
				equipeId: params.equipeId,
			})
		: [];

	const [resultsHits, resultsCount] = await Promise.all([
		elastic.search(elasticQueryEtablissement.index, {
			fields: [properties.etablissementId],
			offset: statsGet ? undefined : offset,
			limit: statsGet ? 0 : (params.elementsParPage ?? 20),
			query,
			sort: statsGet ? undefined : sort,
			aggs,
		}),
		elastic.count(elasticQueryEtablissement.index, {
			query,
		}),
	]);

	if (!resultsHits || !resultsCount) {
		throw new Error('[eqEtabIdSortedGetMany] résultats vides');
	}

	const etablissementIds = etablissementIdsFormat(resultsHits);

	const total = resultsCount.body.count;

	const aggregations = resultsHits.body.aggregations;

	// console.log(JSON.stringify(aggregations?.clusters, null, 2));

	const stats = statsGet && aggregations ? aggregationStatFormatMany(aggregations) : null;

	return {
		etablissementIds,
		total,
		stats,
	};
}

export async function eqEtabGeoGetMany(params: dbQueryEquipeEtablissement.EtablissementsParams) {
	const query = await buildQuery(params);

	// const requestBodySearch = builder.requestBodySearch().query(query).sort(sort);
	// console.log(JSON.stringify(requestBodySearch, null, 2));

	const aggs = aggregationGeoBuildMany({
		precision: params.z,
	});

	const resultsHits = await elastic.search(elasticQueryEtablissement.index, {
		fields: [properties.etablissementId],
		limit: 0,
		query,
		aggs,
	});
	if (!resultsHits) {
		throw new Error('[eqEtabGeoGetMany] résultats vides');
	}

	const aggregations = resultsHits.body.aggregations;

	// console.log(JSON.stringify(aggregations, null, 2));

	const stats = aggregations ? aggregationGeoFormatMany(aggregations) : null;

	return {
		stats,
	};
}

export async function* eqEtabIdGetCursor(
	params: dbQueryEquipeEtablissement.EtablissementsParams
): AsyncGenerator<
	({ [properties.etablissementId]: Etablissement['siret'] } & Iterable<{
		[properties.etablissementId]: Etablissement['siret'];
	}>)[]
> {
	const query = await buildQuery(params);

	const stream = elastic.stream(elasticQueryEtablissement.index, {
		query,
		fields: [properties.etablissementId],
	});

	for await (const doc of stream) {
		yield doc;
	}
}

export async function eqEtabAggregate(
	params: dbQueryEquipeEtablissement.EtablissementsParams,
	aggs: builder.Aggregation[]
) {
	const query = await buildQuery(params);

	// const requestBodySearch = builder.requestBodySearch().query(query).sort(sort);
	// console.log(JSON.stringify(requestBodySearch, null, 2));

	const resultsAggs = await elastic.search(elasticQueryEtablissement.index, {
		fields: [properties.etablissementId],
		query,
		limit: 0,
		aggs,
	});

	if (!resultsAggs) {
		throw new Error('[eqEtabAggregate] résultats vides');
	}

	/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
	return resultsAggs.body.aggregations;
}

export function eqEtabContactsNomsUpdate({
	equipeId,
	etablissementId,
	contactsNoms,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	contactsNoms: string[];
}) {
	return elastic.update(elasticQueryEtablissement.index, {
		id: etablissementId,
		script: elastic.helpers.equipeFieldSet({
			equipeId,
			field: propertiesEquipesSet.contactsNoms,
			value: contactsNoms.map(unaccent),
		}),
	});
}

export function eqEtabAvecContactUpdate({
	equipeId,
	etablissementId,
	avecContact,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	avecContact: boolean;
}) {
	return elastic.update(elasticQueryEtablissement.index, {
		id: etablissementId,
		script: elastic.helpers.equipeFieldSet({
			equipeId,
			field: propertiesEquipesSet.contacts,
			value: avecContact,
		}),
	});
}

export function eqEtabAncienCreateurUpdate({
	equipeId,
	etablissementId,
	ancienCreateur,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	ancienCreateur: boolean | null;
}) {
	return elastic.update(elasticQueryEtablissement.index, {
		id: etablissementId,
		script: elastic.helpers.equipeFieldSet({
			equipeId,
			field: propertiesEquipesSet.ancienCreateur,
			value: ancienCreateur,
		}),
	});
}

export function eqEtabConsultationUpdate({
	equipeId,
	etablissementId,
	consultationAt,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	consultationAt: Date;
}) {
	return elastic.update(elasticQueryEtablissement.index, {
		id: etablissementId,
		script: elastic.helpers.equipeObjectMerge({
			equipeId,
			value: {
				[propertiesEquipesSet.consultationAt]: consultationAt,
				[propertiesEquipesSet.consultationStr]: consultationAt.toISOString(),
			},
		}),
	});
}

export function eqEtabEtiquetteUpdate({
	equipeId,
	etablissementId,
	etiquetteIds,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	etiquetteIds: Etiquette['id'][];
}) {
	return elastic.update(elasticQueryEtablissement.index, {
		id: etablissementId,
		script: elastic.helpers.equipeFieldSet({
			equipeId,
			field: propertiesEquipesSet.etiquettes,
			value: etiquetteIds,
		}),
	});
}

export async function eqEtabEtiquetteAddMany({
	equipeId,
	etablissementIds,
	etiquetteIds,
}: {
	equipeId: Equipe['id'];
	etablissementIds: Etablissement['siret'][];
	etiquetteIds: Etiquette['id'][];
}) {
	const results = await elastic.bulkUpdate(
		elasticQueryEtablissement.index,
		etablissementIds,
		(etablissementId: string) => [
			etablissementId,
			{
				script: elastic.helpers.equipeFieldAddMany({
					equipeId,
					field: propertiesEquipesSet.etiquettes,
					values: etiquetteIds,
				}),
			},
		]
	);

	elastic.helpers.updateErrorPrint(results);
}

export async function eqEtabEtiquetteAddManyByTuple({
	eqEtabEtiquettes,
}: {
	eqEtabEtiquettes: EquipeEtablissementEtiquette[];
}) {
	const results = await elastic.bulkUpdate(
		elasticQueryEtablissement.index,
		eqEtabEtiquettes,
		(eqEtabEtiquette: EquipeEtablissementEtiquette) => [
			eqEtabEtiquette.etablissementId,
			{
				script: elastic.helpers.equipeFieldAdd({
					equipeId: eqEtabEtiquette.equipeId,
					field: propertiesEquipesSet.etiquettes,
					value: eqEtabEtiquette.etiquetteId,
				}),
			},
		]
	);

	elastic.helpers.updateErrorPrint(results);
}

export async function eqEtabEtiquetteRemoveMany({
	equipeId,
	etablissementIds,
	etiquetteIds,
}: {
	equipeId: Equipe['id'];
	etablissementIds: Etablissement['siret'][];
	etiquetteIds: Etiquette['id'][];
}) {
	const results = await elastic.bulkUpdate(
		elasticQueryEtablissement.index,
		etablissementIds,
		(etablissementId: string) => [
			etablissementId,
			{
				script: elastic.helpers.equipeFieldRemoveMany({
					equipeId,
					field: propertiesEquipesSet.etiquettes,
					values: etiquetteIds,
				}),
			},
		]
	);

	elastic.helpers.updateErrorPrint(results);
}

export async function eqEtabEtiquetteRemoveManyByTuple({
	eqEtabEtiquettes,
}: {
	eqEtabEtiquettes: Pick<
		EquipeEtablissementEtiquette,
		'equipeId' | 'etiquetteId' | 'etablissementId'
	>[];
}) {
	const results = await elastic.bulkUpdate(
		elasticQueryEtablissement.index,
		eqEtabEtiquettes,
		(
			eqEtabEtiquette: Pick<
				EquipeEtablissementEtiquette,
				'equipeId' | 'etiquetteId' | 'etablissementId'
			>
		) => [
			eqEtabEtiquette.etablissementId,
			{
				script: elastic.helpers.equipeFieldRemove({
					equipeId: eqEtabEtiquette.equipeId,
					field: propertiesEquipesSet.etiquettes,
					value: eqEtabEtiquette.etiquetteId,
				}),
			},
		]
	);

	elastic.helpers.updateErrorPrint(results);
}

export async function equipeEtiquetteRemoveMany({
	equipeId,
	etiquetteIds,
}: {
	equipeId: Equipe['id'];
	etiquetteIds: Etiquette['id'][];
}) {
	const results = await elastic.updateByQuery(
		elasticQueryEtablissement.index,
		builder
			.nestedQuery()
			.path(properties.equipes)
			.query(builder.termQuery(propertiesEquipes.equipeId, equipeId)),
		{
			script: elastic.helpers.equipeFieldRemoveMany({
				equipeId,
				field: propertiesEquipesSet.etiquettes,
				values: etiquetteIds,
			}),
		}
	);

	elastic.helpers.updateErrorPrint(results);
}

export function eqEtabFavoriUpdate({
	equipeId,
	etablissementId,
	devecoId,
	favori,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	devecoId: Deveco['id'];
	favori: boolean;
}) {
	return elastic.update(elasticQueryEtablissement.index, {
		id: etablissementId,
		/* eslint-disable-next-line @typescript-eslint/unbound-method */
		script: (favori ? elastic.helpers.equipeFieldAdd : elastic.helpers.equipeFieldRemove)({
			equipeId,
			field: propertiesEquipesSet.favoris,
			value: devecoId,
		}),
	});
}

export function eqEtabDemandeAdd({
	equipeId,
	etablissementId,
	demandeTypeIds,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	demandeTypeIds: DemandeTypeId[];
}) {
	return elastic.update(elasticQueryEtablissement.index, {
		id: etablissementId,
		script: elastic.helpers.equipeFieldAddMany({
			equipeId,
			field: propertiesEquipesSet.demandes,
			values: demandeTypeIds,
		}),
	});
}

export function eqEtabDemandeRemove({
	equipeId,
	etablissementId,
	demandeTypeId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	demandeTypeId: DemandeTypeId;
}) {
	return elastic.update(elasticQueryEtablissement.index, {
		id: etablissementId,
		script: elastic.helpers.equipeFieldRemove({
			equipeId,
			field: propertiesEquipesSet.demandes,
			value: demandeTypeId,
		}),
	});
}

export function eqEtabSuiviDatesUpdate({
	equipeId,
	etablissementId,
	suiviDates,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	suiviDates: (
		| ActionDemande['createdAt']
		| ActionEchange['createdAt']
		| ActionRappel['createdAt']
	)[];
}) {
	return elastic.update(elasticQueryEtablissement.index, {
		id: etablissementId,
		script: elastic.helpers.equipeFieldSet({
			equipeId,
			field: propertiesEquipesSet.suiviDates,
			value: suiviDates,
		}),
	});
}

export function eqEtabContributionUpdate({
	equipeId,
	etablissementId,
	contribution: { clotureDate, enseigne, geolocalisation },
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	contribution: Partial<EquipeEtablissementContribution>;
}) {
	return elastic.update(elasticQueryEtablissement.index, {
		id: etablissementId,
		script: elastic.helpers.equipeFieldMerge({
			equipeId,
			field: propertiesEquipesSet.contribution,
			value: {
				[propertiesEquipesContributionSet.clotureDate]: clotureDate,
				[propertiesEquipesContributionSet.enseigne]: enseigne ? unaccent(enseigne) : enseigne,
				[propertiesEquipesContributionSet.adresseGeolocalisation]: geolocalisation,
			},
		}),
	});
}

export function eqEtabContributionClotureDateUpdate({
	equipeId,
	etablissementId,
	clotureDate,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	clotureDate: EquipeEtablissementContribution['clotureDate'];
}) {
	return elastic.update(elasticQueryEtablissement.index, {
		id: etablissementId,
		script: elastic.helpers.equipeFieldWithParentSet({
			equipeId,
			parent: propertiesEquipesSet.contribution,
			field: propertiesEquipesContributionSet.clotureDate,
			value: clotureDate,
		}),
	});
}

export function eqEtabContributionEnseigneUpdate({
	equipeId,
	etablissementId,
	enseigne,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	enseigne: EquipeEtablissementContribution['enseigne'];
}) {
	return elastic.update(elasticQueryEtablissement.index, {
		id: etablissementId,
		script: elastic.helpers.equipeFieldWithParentSet({
			equipeId,
			parent: propertiesEquipesSet.contribution,
			field: propertiesEquipesContributionSet.enseigne,
			value: enseigne,
		}),
	});
}

export async function eqEtabIdCount(params: dbQueryEquipeEtablissement.EtablissementsParams) {
	const query = await buildQuery(params);

	const resultsCount = await elastic.count(elasticQueryEtablissement.index, {
		query,
	});

	/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
	return resultsCount?.body.count ?? 0;
}

export async function eqEtabIdCountByZonageId({
	equipeId,
	zonageId,
}: {
	equipeId: Equipe['id'];
	zonageId: Zonage['id'];
}) {
	const must: builder.Query[] = [];
	const mustNot: builder.Query[] = [];

	const { must: mustEquipeTerritoire, mustNot: mustNotEquipeTerritoire } =
		await elasticQueryEquipe.buildEquipeTerritoireQuery({
			equipeId,
		});
	must.push(...mustEquipeTerritoire);
	mustNot.push(...mustNotEquipeTerritoire);

	must.push(
		builder
			.geoShapeQuery(properties.adresseGeolocalisation)
			.indexedShape(
				builder
					.indexedShape()
					.index(elasticQueryZonage.index)
					.id(zonageId.toString())
					.path(elasticQueryZonageMappings.properties.zonageGeometrie)
			)
	);

	const query = builder.boolQuery();

	if (must.length) {
		query.must(must);
	}

	if (mustNot.length) {
		query.mustNot(mustNot);
	}

	const resultsCount = await elastic.count(elasticQueryEtablissement.index, {
		query,
	});

	/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
	return resultsCount?.body.count ?? 0;
}

export async function eqEtabFrereIdGetMany({
	equipeId,
	etablissementId,
	entrepriseId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	entrepriseId: Entreprise['siren'];
}) {
	const must: builder.Query[] = [];

	const mustNot: builder.Query[] = [];

	const { must: mustEquipeTerritoire } = await elasticQueryEquipe.buildEquipeTerritoireQuery({
		equipeId,
	});

	must.push(...mustEquipeTerritoire);

	const query = builder.boolQuery();

	must.push(builder.termQuery(properties.entrepriseId, entrepriseId));

	mustNot.push(builder.termQuery(properties.etablissementId, etablissementId));

	if (must.length) {
		query.must(must);
	}

	if (mustNot.length) {
		query.mustNot(mustNot);
	}

	const resultsHits = await elastic.search(elasticQueryEtablissement.index, {
		fields: [properties.etablissementId],
		query,
		limit: 6,
	});

	if (!resultsHits) {
		throw new Error('[eqEtabFrereIdGetMany] résultats vides');
	}

	const etablissementIds = etablissementIdsFormat(resultsHits);

	return etablissementIds;
}

export async function eqEtabOuvertFermeGetMany(
	params: dbQueryEquipeEtablissement.EtablissementsParams
) {
	const query = await buildQuery(params);

	const resultsHits = await elastic.search(elasticQueryEtablissement.index, {
		fields: [properties.etablissementId],
		query,
	});

	if (!resultsHits) {
		throw new Error('[eqEtabOuvertFermeGetMany] résultats vides');
	}

	const etablissementIds = etablissementIdsFormat(resultsHits);

	return etablissementIds;
}
