import builder from 'elastic-builder';
import { ApiResponse } from '@opensearch-project/opensearch';

import { Readable } from 'stream';

import {
	Etablissement,
	Entreprise,
	ApiEntrepriseEntrepriseParticipation,
	Qpv2024,
	Qpv2015,
} from '../../db/types';
import { regexpEscape, fromEntriesToCamelCaseKeys, unaccent } from '../../lib/utils';
import { isSiren } from '../../lib/regexp';
import elastic, { client } from '../../elastic';
import ElasticsearchScrollStream from '../../elastic/stream';
import { Position } from '../../db/schema/custom-types/postgis';
import * as dbQueryEquipeEtablissement from '../../db/query/equipe-etablissement';

export const index = process.env.ELASTICSEARCH_INDEX_ETAB || 'etablissement_development';

import mapping from '../../elastic/mappings/etablissement_index.json';

export { mapping };

export const BATCH = 1000;

const properties = fromEntriesToCamelCaseKeys(mapping.properties);

export function etablissementQpv2024UpdateMany({
	etablissementQpv2024s,
}: {
	etablissementQpv2024s: { etablissementId: Etablissement['siret']; qpv2024Id: Qpv2024['id'] }[];
}) {
	return elastic.bulkUpdate(index, etablissementQpv2024s, (doc) => [
		doc.etablissementId,
		{
			doc: {
				[properties.adresseQpv2024Id]: doc.qpv2024Id,
			},
		},
	]);
}

export function etablissementQpv2024RemoveMany({
	etablissementIds,
}: {
	etablissementIds: Etablissement['siret'][];
}) {
	return elastic.bulkUpdate(index, etablissementIds, (doc) => [
		doc,
		{
			doc: {
				[properties.adresseQpv2024Id]: null,
			},
		},
	]);
}

export function etablissementQpvUpdate({
	etablissementId,
	qpv2024Id,
	qpv2015Id,
}: {
	etablissementId: Etablissement['siret'];
	qpv2024Id: Qpv2024['id'] | null;
	qpv2015Id: Qpv2015['id'] | null;
}) {
	return elastic.update(index, {
		id: etablissementId,
		doc: {
			[properties.adresseQpv2024Id]: qpv2024Id,
			[properties.adresseQpv2015Id]: qpv2015Id,
		},
	});
}

export async function* searchEtablissementIdStream({
	size = BATCH,
	query = builder.matchAllQuery(),
}: {
	size?: number;
	query?: builder.Query;
}) {
	try {
		const esStream = new ElasticsearchScrollStream(
			client,
			{
				index,
				// temps de cache du moteur de recherche dans ElasticSearch
				scroll: '10m',
				// nombre d'éléments pour chaque batch
				size,
				body: {
					_source: [properties.etablissementId],
					query,
				},
			},
			[],
			{ objectMode: true }
		) as Readable;

		// let total = 0;
		// let boucles = 0;

		const etablissementIds: string[] = [];

		for await (const doc of esStream) {
			etablissementIds.push(doc[properties.etablissementId]);

			if (etablissementIds.length >= size) {
				// total += etablissementIds.length;

				yield etablissementIds.splice(0, size);
			}

			// boucles += 1;
		}

		if (etablissementIds.length) {
			// total += etablissementIds.length;

			yield etablissementIds;
		}
	} catch (e: any) {
		console.logError('[searchEtablissementIdStream] erreur ElasticSearch :', e);
	}
}

export function etablissementGeolocalisationUpdateMany({
	etablissementGeolocalisations,
}: {
	etablissementGeolocalisations: {
		etablissementId: Etablissement['siret'];
		geolocalisation: Position | null;
	}[];
}) {
	return elastic.bulkUpdate(
		index,
		etablissementGeolocalisations,
		({ etablissementId, geolocalisation }) => [
			etablissementId,
			{
				doc: {
					[properties.adresseGeolocalisation]: geolocalisation,
				},
			},
		]
	);
}

type ParticipationIndexObject = {
	[properties.entrepriseParticipationFilialeDe]: string[];
	[properties.entrepriseParticipationDetient]: string[];
};

export async function etablissementParticipationUpdateMany({
	participations,
}: {
	participations: Pick<ApiEntrepriseEntrepriseParticipation, 'source' | 'cible'>[];
}) {
	const entrepriseParticipations = new Map<Entreprise['siren'], ParticipationIndexObject>();

	participations.reduce((entrepriseParticipations, { source, cible }) => {
		const entrepriseSourceParticipation = entrepriseParticipations.get(source) ?? {
			[properties.entrepriseParticipationFilialeDe]: [],
			[properties.entrepriseParticipationDetient]: [],
		};

		entrepriseSourceParticipation[properties.entrepriseParticipationDetient].push(cible);
		entrepriseParticipations.set(source, entrepriseSourceParticipation);

		const entrepriseCibleParticipation = entrepriseParticipations.get(cible) ?? {
			[properties.entrepriseParticipationFilialeDe]: [],
			[properties.entrepriseParticipationDetient]: [],
		};

		entrepriseCibleParticipation[properties.entrepriseParticipationFilialeDe].push(source);
		entrepriseParticipations.set(cible, entrepriseCibleParticipation);

		return entrepriseParticipations;
	}, entrepriseParticipations);

	for (const [entrepriseId, entrepriseParticipation] of entrepriseParticipations.entries()) {
		await elastic.updateByQuery(
			index,
			builder.termQuery(properties.entrepriseId, entrepriseId),
			{
				script: elastic.helpers.arrayMergeMany({
					fields: [
						properties.entrepriseParticipationFilialeDe,
						properties.entrepriseParticipationDetient,
					],
					value: entrepriseParticipation,
				}),
			},
			{
				refresh: true,
			}
		);
	}
}

function motToQuery({ mot, fuzzy = false }: { mot: string; fuzzy: boolean }) {
	if (!mot) {
		return null;
	}

	if (isSiren(mot)) {
		return builder.termQuery(properties.entrepriseId, mot.replaceAll(/\s/g, ''));
	}

	const motRegexp = `.*${regexpEscape(unaccent(mot.toLowerCase()))}.*`;

	const conditions: builder.Query[] = [
		...(fuzzy
			? [builder.multiMatchQuery([properties.etablissementNoms, properties.entrepriseNoms], mot)]
			: [
					builder.regexpQuery(properties.etablissementNoms, motRegexp),
					builder.regexpQuery(properties.entrepriseNoms, motRegexp),
				]),
		builder.prefixQuery(properties.entrepriseId, mot.replaceAll(/\s/g, '')),
	];

	return builder.boolQuery().should(conditions);
}

function buildQuery(params: dbQueryEquipeEtablissement.EntreprisesParams, { fuzzy = false } = {}) {
	const must: builder.Query[] = [];
	const mustNot: builder.Query[] = [];
	const filter: builder.Query[] = [];

	if (params.recherche) {
		if (isSiren(params.recherche)) {
			must.push(builder.termQuery(properties.entrepriseId, params.recherche.replaceAll(/\s/g, '')));
		} else {
			must.push(
				...params.recherche.split(/[ -]/).flatMap(
					(recherche) =>
						motToQuery({
							mot: recherche,
							fuzzy,
						}) ?? []
				)
			);
		}
	}

	const query = builder.boolQuery();

	if (must.length) {
		query.must(must);
	}

	if (mustNot.length) {
		query.mustNot(mustNot);
	}

	if (filter.length) {
		query.filter(filter);
	}

	return query;
}

function entrepriseIdsFormat(resultsHits: ApiResponse): {
	entrepriseIds: Entreprise['siren'][];
} {
	return {
		entrepriseIds:
			/* eslint-disable-next-line @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-return */
			resultsHits.body.aggregations?.entrepriseId?.buckets.map((stat: any) => stat.key) ?? [],
	};
}

export async function entrepriseIdSortedGetMany(
	params: dbQueryEquipeEtablissement.EntreprisesParams
) {
	const query = buildQuery(params);

	// const requestBodySearch = builder.requestBodySearch().query(query).sort(sort);
	// console.log(JSON.stringify(requestBodySearch, null, 2));

	const aggs = [elastic.helpers.builder.termsAggregation('entrepriseId', properties.entrepriseId)];

	const resultsHits = await elastic.search(index, {
		fields: [properties.entrepriseId],
		query,
		limit: 0,
		aggs,
	});

	if (!resultsHits) {
		throw new Error('[eqEtabIdSortedGetMany] résultats vides');
	}

	const { entrepriseIds } = entrepriseIdsFormat(resultsHits);

	// console.log(JSON.stringify(resultsHits.body.aggregations, null, 2));

	return {
		entrepriseIds,
	};
}
