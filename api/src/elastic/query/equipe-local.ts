import builder from 'elastic-builder';
import { ApiResponse } from '@opensearch-project/opensearch';

import { isInvariant } from '../../lib/regexp';
import { keysOf, isDefined, regexpEscape, fromEntriesToCamelCaseKeys } from '../../lib/utils';
import {
	ActionEchange,
	ActionDemande,
	ActionRappel,
	DemandeTypeId,
	Equipe,
	Local,
	EquipeLocalContribution,
	EquipeLocalEtiquette,
	LocalAdresse,
	Commune,
	Etiquette,
	Zonage,
	Deveco,
} from '../../db/types';
import elastic from '../../elastic';
import * as dbQueryEquipe from '../../db/query/equipe';
import * as dbQueryEquipeLocal from '../../db/query/equipe-local';
import * as elasticQueryZonage from '../../elastic/query/zonage';
import { mappings as elasticQueryZonageMappings } from '../../elastic/query/zonage';
import mapping from '../../elastic/mappings/local_index.json';
import * as elasticQueryLocal from '../../elastic/query/local';
import * as elasticQueryEquipe from '../../elastic/query/equipe';

const properties = fromEntriesToCamelCaseKeys(mapping.properties);
const propertiesEquipes = fromEntriesToCamelCaseKeys(
	mapping.properties.equipes.properties,
	properties.equipes
);
const propertiesEquipesSet = fromEntriesToCamelCaseKeys(mapping.properties.equipes.properties);
const propertiesEquipesContribution = fromEntriesToCamelCaseKeys(
	mapping.properties.equipes.properties.contribution.properties,
	propertiesEquipes.contribution
);
const propertiesEquipesContributionSet = fromEntriesToCamelCaseKeys(
	mapping.properties.equipes.properties.contribution.properties
);

async function buildQuery(params: dbQueryEquipeLocal.EquipeLocalParams) {
	const must: builder.Query[] = [];
	const mustNot: builder.Query[] = [];
	// utilisable seulement pour les documents qui ont un sous-document `equipes` de l'équipe courante
	// non pertinent pour les requêtes négatives
	const nested: builder.Query[] = [];
	const filter: builder.Query[] = [];

	const { equipeId } = params;

	const { must: mustEquipeTerritoire, mustNot: mustNotEquipeTerritoire } =
		await elasticQueryEquipe.buildEquipeTerritoireQuery(params);

	must.push(...mustEquipeTerritoire);
	mustNot.push(...mustNotEquipeTerritoire);

	const equipeQuery = builder.termQuery(propertiesEquipes.equipeId, equipeId);

	if (params.invariant && isInvariant(params.invariant)) {
		must.push(builder.termQuery(properties.localInvariant, params.invariant));
	}

	if (params.adresseCommune) {
		must.push(builder.termsQuery(properties.territoireCommuneId, params.adresseCommune));
	} else if (params.communes?.length) {
		must.push(builder.termsQuery(properties.territoireCommuneId, params.communes));
	}

	if (params.adresseVoie) {
		must.push(builder.matchQuery(properties.adresseVoieNom, params.adresseVoie).operator('and'));
	}

	if (params.adresseNumero) {
		must.push(builder.termQuery(properties.adresseNumero, params.adresseNumero));
	}

	if (params.zrrs?.length) {
		must.push(builder.termsQuery(properties.territoireZrrTypeId, params.zrrs));
	}

	if (isDefined(params.acv)) {
		must.push(builder.termQuery(properties.territoireAcv, params.acv));
	}

	if (isDefined(params.pvd)) {
		must.push(builder.termQuery(properties.territoirePvd, params.pvd));
	}

	if (isDefined(params.va)) {
		must.push(builder.termQuery(properties.territoireVa, params.va));
	}

	if (params.territoireIndustries?.length) {
		if (params.territoireIndustries.includes('tous')) {
			must.push(builder.existsQuery(properties.territoireTerritoireIndustrieId));
		} else {
			must.push(
				builder.termsQuery(properties.territoireTerritoireIndustrieId, params.territoireIndustries)
			);
		}
	}

	if (params.qpvs?.length) {
		if (params.qpvs.includes('tous')) {
			must.push(builder.existsQuery(properties.adresseQpv2024Id));
		} else {
			must.push(builder.termsQuery(properties.adresseQpv2024Id, params.qpvs));
		}
	}

	if (params.afrs?.length) {
		const afrConditionsShould: builder.Query[] = [];

		if (params.afrs.includes('O')) {
			afrConditionsShould.push(builder.termQuery(properties.territoireAfr, true));
		}

		if (params.afrs.includes('P')) {
			afrConditionsShould.push(builder.termQuery(properties.territoireAfr, false));
		}

		if (params.afrs.includes('N')) {
			afrConditionsShould.push(
				builder.boolQuery().mustNot(builder.existsQuery(properties.territoireAfr))
			);
		}

		must.push(builder.boolQuery().should(afrConditionsShould));
	}

	if (params.epcis?.length) {
		must.push(builder.termsQuery(properties.territoireEpciId, params.epcis));
	}

	if (params.departements?.length) {
		must.push(builder.termsQuery(properties.territoireDepartementId, params.departements));
	}

	if (params.regions?.length) {
		must.push(builder.termsQuery(properties.territoireRegionId, params.regions));
	}

	if (params.section) {
		must.push(builder.termQuery(properties.localSection, params.section));
	}

	if (params.parcelle) {
		must.push(builder.termQuery(properties.localParcelle, params.parcelle));
	}

	if (params.natures?.length) {
		must.push(builder.termsQuery(properties.localNatureTypeId, params.natures));
	}

	if (params.categories?.length) {
		must.push(builder.termsQuery(properties.localCategorieTypeId, params.categories));
	}

	const surfacesClefToField: (k: keyof dbQueryEquipeLocal.Surfaces) => string = (k) => {
		switch (k) {
			case 'vente':
				return properties.localSurfaceVente;
			case 'reserve':
				return properties.localSurfaceReserve;
			case 'exterieure':
				return properties.localSurfaceExterieureNonCouverte;
			case 'stationnementNonCouverte':
				return properties.localSurfaceStationnementNonCouvert;
			case 'stationnementCouverte':
				return properties.localSurfaceStationnementCouvert;
			case 'totale':
			default:
				return properties.localSurfaceTotale;
		}
	};

	const surfaceMinMaxToConditions = (
		field: string,
		{ min, max }: { min: string | null; max: string | null }
	) => {
		if (min === null && max === null) {
			return;
		}

		const rangeQuery = builder.rangeQuery(field);

		if (min !== null) {
			rangeQuery.gte(min);
		}
		if (max !== null) {
			rangeQuery.lt(max);
		}

		return rangeQuery;
	};

	if (params.surfaces) {
		const surfaceConditions = keysOf(params.surfaces).flatMap(
			(k: keyof dbQueryEquipeLocal.Surfaces) => {
				if (!params.surfaces || !params.surfaces[k]) {
					return [];
				}

				const field = surfacesClefToField(k);

				const query = surfaceMinMaxToConditions(field, params.surfaces[k]);

				return query ? [query] : [];
			}
		);

		must.push(...surfaceConditions);
	}

	if (params.etages?.length) {
		must.push(builder.termsQuery(properties.localNiveau, params.etages));
	}

	if (params.rechercheProprietaire) {
		const motRegexp = `.*${regexpEscape(params.rechercheProprietaire.toLowerCase())}.*`;

		// ça ressemble à un SIREN, on cherche dans le champ entreprise_ids
		if (params.rechercheProprietaire.match(/^(?:\s*\d\s*){1,9}$/)) {
			must.push(builder.regexpQuery(properties.proprietaireMoraleEntrepriseIds, motRegexp));
		} else {
			must.push(
				builder
					.boolQuery()
					.should([
						builder.regexpQuery(properties.proprietaireMoraleNoms, motRegexp),
						builder.regexpQuery(properties.proprietairePhysiqueNoms, motRegexp),
					])
			);
		}
	}

	if (params.geoloc?.length) {
		const should: builder.Query[] = [];

		const geolocPersoCondition = elasticQueryEquipe.nestedQueryExists({
			equipeId,
			field: propertiesEquipesContribution.adresseGeolocalisation,
		});

		if (params.geoloc.includes('O')) {
			const geolocCondition = builder.existsQuery(properties.adresseGeolocalisation);

			should.push(builder.boolQuery().must(geolocCondition).mustNot(geolocPersoCondition));
		}

		if (params.geoloc.includes('N')) {
			const geolocCondition = builder.existsQuery(properties.adresseGeolocalisation);

			should.push(builder.boolQuery().mustNot([geolocCondition, geolocPersoCondition]));
		}

		if (params.geoloc.includes('P')) {
			should.push(geolocPersoCondition);
		}

		must.push(builder.boolQuery().should(should));
	}

	function radiusTileGet(precision: number, latitude = 0) {
		const earthCircumference = 40075000;
		const tileSize = earthCircumference / Math.pow(2, precision);
		const radius = (tileSize / 2) * Math.cos((latitude * Math.PI) / 180);

		return radius;
	}

	if (isDefined(params.longitude) && isDefined(params.latitude)) {
		const point = builder.geoPoint().lon(params.longitude).lat(params.latitude);

		const zoom = params.z;

		const precision =
			zoom &&
			(zoom <= 10 ? zoom + 1 : zoom <= 11 ? zoom + 2 : zoom <= 14 ? zoom + 3 : MAX_PRECISION);

		const rayon = precision ? radiusTileGet(precision, params.latitude) : (params.rayon ?? 1);

		must.push(builder.geoDistanceQuery(properties.adresseGeolocalisation, point).distance(rayon));
	}

	if (params.nwse.length) {
		const [lat1, lon1, lat2, lon2] = params.nwse;

		if (isDefined(lon1) && isDefined(lat1) && isDefined(lon2) && isDefined(lat2)) {
			const point1 = builder.geoPoint().lon(lon1).lat(lat1);
			const point2 = builder.geoPoint().lon(lon2).lat(lat2);

			must.push(
				builder
					.geoBoundingBoxQuery(properties.adresseGeolocalisation)
					.topLeft(point1)
					.bottomRight(point2)
			);
		}
	}

	// perso

	if (params.nom?.length) {
		nested.push(builder.matchQuery(propertiesEquipesContribution.nom, regexpEscape(params.nom)));
	}

	if (params.contributionOccupe?.length) {
		const shoulds = [];

		const occupe = params.contributionOccupe.includes(true);
		const vacant = params.contributionOccupe.includes(false);
		const nonRenseigne = params.contributionOccupe.includes(null);

		if (occupe || vacant) {
			const terms = [];

			if (occupe) {
				terms.push(true);
			}
			if (vacant) {
				terms.push(false);
			}

			// si on ne cherche pas les non-renseignés, on peut directement faire une requête `nested`
			if (!nonRenseigne) {
				nested.push(builder.termsQuery(propertiesEquipesContribution.occupe, terms));
			} else {
				// sinon, on doit combiner les requêtes avec un `must` de haut niveau
				shoulds.push(
					builder
						.nestedQuery()
						.path(properties.equipes)
						.query(
							builder
								.boolQuery()
								.must([
									equipeQuery,
									builder.termsQuery(propertiesEquipesContribution.occupe, terms),
								])
						)
				);
			}
		}

		if (params.contributionOccupe.includes(null)) {
			shoulds.push(
				builder.boolQuery().mustNot(
					builder
						.nestedQuery()
						.path(properties.equipes)
						.query(
							builder
								.boolQuery()
								.must([equipeQuery, builder.existsQuery(propertiesEquipesContribution.occupe)])
						)
				)
			);
		}

		if (shoulds.length) {
			must.push(builder.boolQuery().should(shoulds));
		}
	}

	const localisationIds: Etiquette['id'][] = [];

	if (params.localisations?.length) {
		const ids = (
			await dbQueryEquipe.etiquetteGetManyByTypeAndNoms({
				equipeId,
				etiquetteTypeId: 'localisation',
				noms: params.localisations,
			})
		).map((e) => e.id);

		localisationIds.push(...ids);
	}

	if (localisationIds.length) {
		nested.push(builder.termsQuery(propertiesEquipes.etiquettes, localisationIds));
	}

	const motCleIds: Etiquette['id'][] = [];

	if (params.motsCles?.length) {
		const ids = (
			await dbQueryEquipe.etiquetteGetManyByTypeAndNoms({
				equipeId,
				etiquetteTypeId: 'mot_cle',
				noms: params.motsCles,
			})
		).map((e) => e.id);

		motCleIds.push(...ids);
	}

	if (motCleIds.length) {
		nested.push(builder.termsQuery(propertiesEquipes.etiquettes, motCleIds));
	}

	if (params.demandeTypeIds?.length) {
		nested.push(builder.termsQuery(propertiesEquipes.demandes, params.demandeTypeIds));
	}

	if (params.accompagnes?.length) {
		const shoulds = [];

		if (params.accompagnes.includes('oui')) {
			let query;

			if (isDefined(params.accompagnesApres) || isDefined(params.accompagnesAvant)) {
				const rangeQuery = builder.rangeQuery(propertiesEquipes.suiviDates);

				if (isDefined(params.accompagnesApres)) {
					rangeQuery.gte(params.accompagnesApres.toISOString());
				}

				if (isDefined(params.accompagnesAvant)) {
					rangeQuery.lte(params.accompagnesAvant.toISOString());
				}
				query = rangeQuery;
			} else {
				query = builder.existsQuery(propertiesEquipes.suiviDates);
			}

			// on cherche les documents qui ont un sous-document `equipes` de l'équipe courante avec `suivi_dates`
			shoulds.push(
				builder
					.nestedQuery()
					.path(properties.equipes)
					.query(builder.boolQuery().must([equipeQuery, query]))
			);
		}

		if (params.accompagnes.includes('non')) {
			// on cherche les documents qui n'ont pas de sous-document `equipes` de l'équipe courante et `suivi_dates`
			shoulds.push(
				builder.boolQuery().mustNot(
					builder
						.nestedQuery()
						.path(properties.equipes)
						.query(
							builder
								.boolQuery()
								.must([equipeQuery, builder.existsQuery(propertiesEquipes.suiviDates)])
						)
				)
			);
		}

		if (shoulds.length) {
			must.push(builder.boolQuery().should(shoulds));
		}
	}

	if (params.favoris?.length) {
		const shoulds = [];

		const query = builder.termQuery(propertiesEquipes.favoris, params.devecoId);
		if (params.favoris.includes('oui')) {
			// on cherche les documents qui ont un sous-document `equipes` de l'équipe courante avec un `favori`
			// pour le deveco demandé
			shoulds.push(
				builder
					.nestedQuery()
					.path(properties.equipes)
					.query(builder.boolQuery().must([equipeQuery, query]))
			);
		}

		if (params.favoris.includes('non')) {
			// on cherche les documents qui n'ont pas de sous-document `equipes` de l'équipe courante et `favori`
			// pour le deveco demandé
			shoulds.push(
				builder.boolQuery().mustNot(builder.nestedQuery().path(properties.equipes).query(query))
			);
		}

		if (shoulds.length) {
			must.push(builder.boolQuery().should(shoulds));
		}
	}

	if (nested.length) {
		must.push(
			builder
				.nestedQuery()
				.path(properties.equipes)
				.query(builder.boolQuery().must([equipeQuery, ...nested]))
		);
	}

	const query = builder.boolQuery();

	if (must.length) {
		query.must(must);
	}

	if (mustNot.length) {
		query.mustNot(mustNot);
	}

	if (filter.length) {
		query.filter(filter);
	}

	return query;
}

function localIdsFormat(resultsHits: ApiResponse): Local['id'][] {
	/* eslint-disable-next-line @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-return */
	return resultsHits.body.hits.hits.map((r: any) => r._source.local_id);
}

function localNaturesFormat(resultsHits: ApiResponse): Local['natureTypeId'][] {
	/* eslint-disable-next-line @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-return */
	return resultsHits.body.aggregations.natures.buckets.map((r: any) => r.key);
}

function localCategoriesFormat(resultsHits: ApiResponse): Local['categorieTypeId'][] {
	/* eslint-disable-next-line @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-return */
	return resultsHits.body.aggregations.categories.buckets.map((r: any) => r.key);
}

function localSectionsFormat(resultsHits: ApiResponse): Local['section'][] {
	/* eslint-disable-next-line @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-return */
	return resultsHits.body.aggregations.sections.buckets.map((r: any) => r.key);
}

function localParcellesFormat(resultsHits: ApiResponse): Local['parcelle'][] {
	/* eslint-disable-next-line @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-return */
	return resultsHits.body.aggregations.parcelles.buckets.map((r: any) => r.key);
}

function localVoieNomsFormat(resultsHits: ApiResponse): LocalAdresse['voieNom'][] {
	/* eslint-disable-next-line @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-return */
	return resultsHits.body.aggregations.voieNoms.buckets.map((r: any) => r.key);
}

function localNumerosFormat(resultsHits: ApiResponse): LocalAdresse['numero'][] {
	/* eslint-disable-next-line @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-return */
	return resultsHits.body.aggregations.numeros.buckets.map((r: any) => r.key);
}

function buildAggs() {
	return [];
}

function aggregationGeoFormatMany(aggregations: any) {
	return {
		/* eslint-disable-next-line @typescript-eslint/no-unsafe-call */
		clusters: aggregations.clusters?.buckets.map((stats: any) => {
			const { lon, lat } = stats.centroid.location;

			return {
				key: stats.key,
				count: stats.doc_count,
				// permet de dire que c'est un cluster de groupe
				geolocalisations: stats.geolocalisations?.value ?? 1000,
				coordinates: [lon, lat],
			};
		}),

		stats: aggregations.stats,
	};
}

const MAX_PRECISION = 23;

function aggregationGeoBuildMany({ precision }: { precision?: number | null }) {
	const precisionComputed = Math.min(precision ?? 2, MAX_PRECISION);

	const clusterAggregation = elastic.helpers.builder
		.geoTileGridAggregation('clusters', properties.adresseGeolocalisation)
		.precision(precisionComputed)
		.size(10000)
		.agg(
			elastic.helpers.builder.geoCentroidAggregation('centroid', properties.adresseGeolocalisation)
		);

	if (precisionComputed === MAX_PRECISION) {
		clusterAggregation.agg(
			elastic.helpers.builder.cardinalityAggregation(
				'geolocalisations',
				properties.adresseGeolocalisation
			)
		);
	}

	return [
		clusterAggregation,
		// .agg(elastic.helpers.builder.geoBoundsAggregation('box', properties.adresseGeolocalisation))
		elastic.helpers.builder.statsBucketAggregation('stats').bucketsPath('clusters._count'),
	];
}

export async function eqLocGeoGetMany(params: dbQueryEquipeLocal.EquipeLocalParams) {
	const query = await buildQuery(params);

	// const requestBodySearch = builder.requestBodySearch().query(query).sort(sort);
	// console.log(JSON.stringify(requestBodySearch, null, 2));

	const aggs = aggregationGeoBuildMany({
		precision: params.z,
	});

	const resultsHits = await elastic.search(elasticQueryLocal.index, {
		fields: ['local_id'],
		limit: 0,
		query,
		aggs,
	});
	if (!resultsHits) {
		throw new Error('[eqLocGeoGetMany] résultats vides');
	}

	const aggregations = resultsHits.body.aggregations;

	// console.log(JSON.stringify(aggregations, null, 2));

	const stats = aggregations ? aggregationGeoFormatMany(aggregations) : null;

	return {
		stats,
	};
}

export async function eqLocIdCount(params: dbQueryEquipeLocal.EquipeLocalParams) {
	const query = await buildQuery(params);

	const resultsCount = await elastic.count(elasticQueryLocal.index, {
		query,
	});

	/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
	return resultsCount?.body.count ?? 0;
}

export async function eqLocIdCountByZonageId({
	equipeId,
	zonageId,
}: {
	equipeId: Equipe['id'];
	zonageId: Zonage['id'];
}) {
	const must: builder.Query[] = [];
	const mustNot: builder.Query[] = [];

	const { must: mustEquipeTerritoire, mustNot: mustNotEquipeTerritoire } =
		await elasticQueryEquipe.buildEquipeTerritoireQuery({
			equipeId,
		});
	must.push(...mustEquipeTerritoire);
	mustNot.push(...mustNotEquipeTerritoire);

	must.push(
		builder
			.geoShapeQuery(properties.adresseGeolocalisation)
			.indexedShape(
				builder
					.indexedShape()
					.index(elasticQueryZonage.index)
					.id(zonageId.toString())
					.path(elasticQueryZonageMappings.properties.zonageGeometrie)
			)
	);

	const query = builder.boolQuery();

	if (must.length) {
		query.must(must);
	}

	if (mustNot.length) {
		query.mustNot(mustNot);
	}

	const resultsCount = await elastic.count(elasticQueryLocal.index, {
		query,
	});

	/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
	return resultsCount?.body.count ?? 0;
}

export async function* eqLocIdGetCursor(
	params: dbQueryEquipeLocal.EquipeLocalParams
): AsyncGenerator<({ local_id: Local['id'] } & Iterable<{ local_id: Local['id'] }>)[]> {
	const query = await buildQuery(params);

	const stream = elastic.stream(elasticQueryLocal.index, {
		query,
		fields: ['local_id'],
	});

	for await (const doc of stream) {
		yield doc;
	}
}

export async function eqLocIdSortedGetMany(
	params: dbQueryEquipeLocal.EquipeLocalParams,
	statsGet?: boolean
) {
	const query = await buildQuery(params);

	let sort;
	let sorts;

	if (params.tri === 'alphabetique') {
		sort = builder.sort(properties.adresseVoieNom, 'asc');
		sorts = [builder.sort(properties.adresseNumero, 'asc')];
	} else {
		const equipeQuery = builder.termQuery(propertiesEquipes.equipeId, params.equipeId);

		sort = builder.sort(propertiesEquipes.consultationStr, 'desc').nested({
			path: 'equipes',
			filter: equipeQuery,
		});
	}

	// const requestBodySearch = builder.requestBodySearch().query(query).sort(sort);
	// console.log(JSON.stringify(requestBodySearch, null, 2));

	const offset = params.elementsParPage ? ((params.page || 1) - 1) * params.elementsParPage : 0;

	const aggs = statsGet ? buildAggs() : [];

	const [resultsHits, resultsCount] = await Promise.all([
		elastic.search(elasticQueryLocal.index, {
			offset: statsGet ? undefined : offset,
			limit: statsGet ? 0 : (params.elementsParPage ?? 20),
			query,
			sort: statsGet ? undefined : sort,
			sorts: statsGet ? undefined : sorts,
			aggs,
			fields: ['local_id'],
		}),
		elastic.count(elasticQueryLocal.index, {
			query,
		}),
	]);

	if (!resultsHits || !resultsCount) {
		throw new Error('[eqLocIdSortedGetMany] résultats vides');
	}

	const localIds = localIdsFormat(resultsHits);

	const total = resultsCount.body.count;

	const stats = statsGet ? {} : null;

	return {
		localIds,
		total,
		stats,
	};
}

export async function eqLocNatureIdGetMany({ equipeId }: { equipeId: Equipe['id'] }) {
	const must: builder.Query[] = [];
	const mustNot: builder.Query[] = [];

	const { must: mustEquipeTerritoire, mustNot: mustNotEquipeTerritoire } =
		await elasticQueryEquipe.buildEquipeTerritoireQuery({ equipeId });

	must.push(...mustEquipeTerritoire);
	mustNot.push(...mustNotEquipeTerritoire);

	const query = builder.boolQuery();

	if (must.length) {
		query.must(must);
	}

	if (mustNot.length) {
		query.mustNot(mustNot);
	}

	const resultsAggs = await elastic.search(elasticQueryLocal.index, {
		limit: 0,
		fields: [],
		query,
		aggs: [
			elastic.helpers.builder
				.termsAggregation('natures', properties.localNatureTypeId)
				.order('_key', 'asc')
				.size(10000),
		],
	});

	if (!resultsAggs) {
		return [];
	}

	return localNaturesFormat(resultsAggs);
}

export async function eqLocCategorieIdGetMany({ equipeId }: { equipeId: Equipe['id'] }) {
	const must: builder.Query[] = [];
	const mustNot: builder.Query[] = [];
	const nested: builder.Query[] = [];

	const { must: mustEquipeTerritoire, mustNot: mustNotEquipeTerritoire } =
		await elasticQueryEquipe.buildEquipeTerritoireQuery({ equipeId });

	must.push(...mustEquipeTerritoire);
	mustNot.push(...mustNotEquipeTerritoire);

	if (nested.length) {
		const equipeQuery = builder.termQuery(propertiesEquipes.equipeId, equipeId);

		must.push(
			builder
				.nestedQuery()
				.path('equipes')
				.query(builder.boolQuery().must([equipeQuery, ...nested]))
		);
	}

	const query = builder.boolQuery();

	if (must.length) {
		query.must(must);
	}

	if (mustNot.length) {
		query.mustNot(mustNot);
	}

	const resultsAggs = await elastic.search(elasticQueryLocal.index, {
		limit: 0,
		fields: [],
		query,
		aggs: [
			elastic.helpers.builder
				.termsAggregation('categories', properties.localCategorieTypeId)
				.order('_key', 'asc')
				.size(10000),
		],
	});

	if (!resultsAggs) {
		return [];
	}

	return localCategoriesFormat(resultsAggs);
}

export async function eqLocCommuneSectionGetManyByCommune({
	equipeId,
	communeId,
}: {
	equipeId: Equipe['id'];
	communeId: Commune['id'];
}) {
	const must: builder.Query[] = [];
	const mustNot: builder.Query[] = [];

	const { must: mustEquipeTerritoire, mustNot: mustNotEquipeTerritoire } =
		await elasticQueryEquipe.buildEquipeTerritoireQuery({ equipeId });

	must.push(...mustEquipeTerritoire);
	mustNot.push(...mustNotEquipeTerritoire);

	must.push(builder.termQuery(properties.territoireCommuneId, communeId));

	const query = builder.boolQuery();

	if (must.length) {
		query.must(must);
	}

	if (mustNot.length) {
		query.mustNot(mustNot);
	}

	const resultsAggs = await elastic.search(elasticQueryLocal.index, {
		limit: 0,
		fields: [],
		query,
		aggs: [
			elastic.helpers.builder
				.termsAggregation('sections', properties.localSection)
				.order('_key', 'asc')
				.size(10000),
		],
	});

	if (!resultsAggs) {
		return [];
	}

	return localSectionsFormat(resultsAggs);
}

export async function eqLocCommuneSectionParcelleGetManyByCommuneAndSection({
	equipeId,
	communeId,
	section,
}: {
	equipeId: Equipe['id'];
	communeId: Commune['id'];
	section: string;
}) {
	const must: builder.Query[] = [];
	const mustNot: builder.Query[] = [];

	const { must: mustEquipeTerritoire, mustNot: mustNotEquipeTerritoire } =
		await elasticQueryEquipe.buildEquipeTerritoireQuery({ equipeId });

	must.push(...mustEquipeTerritoire);
	mustNot.push(...mustNotEquipeTerritoire);

	must.push(builder.termQuery(properties.territoireCommuneId, communeId));

	must.push(builder.termQuery(properties.localSection, section));

	const query = builder.boolQuery();

	if (must.length) {
		query.must(must);
	}

	const resultsAggs = await elastic.search(elasticQueryLocal.index, {
		limit: 0,
		fields: [],
		query,
		aggs: [
			elastic.helpers.builder
				.termsAggregation('parcelles', properties.localParcelle)
				.order('_key', 'asc')
				.size(10000),
		],
	});

	if (!resultsAggs) {
		return [];
	}

	return localParcellesFormat(resultsAggs);
}

export async function eqLocCommuneVoieNomGetManyByCommune({
	equipeId,
	communeId,
}: {
	equipeId: Equipe['id'];
	communeId: Commune['id'];
}) {
	const must: builder.Query[] = [];

	const mustNot: builder.Query[] = [];

	const { must: mustEquipeTerritoire, mustNot: mustNotEquipeTerritoire } =
		await elasticQueryEquipe.buildEquipeTerritoireQuery({
			equipeId,
		});

	must.push(...mustEquipeTerritoire);
	mustNot.push(...mustNotEquipeTerritoire);

	must.push(builder.termQuery(properties.territoireCommuneId, communeId));

	const query = builder.boolQuery();

	if (must.length) {
		query.must(must);
	}

	if (mustNot.length) {
		query.must(mustNot);
	}

	const resultsAggs = await elastic.search(elasticQueryLocal.index, {
		limit: 0,
		fields: [],
		query,
		aggs: [
			elastic.helpers.builder
				.termsAggregation('voieNoms', properties.adresseVoieNom)
				.order('_key', 'asc')
				.size(10000),
		],
	});

	if (!resultsAggs) {
		return [];
	}

	return localVoieNomsFormat(resultsAggs);
}

export async function eqLocCommuneVoieNomNumeroGetManyByCommuneAndVoieNom({
	equipeId,
	communeId,
	voieNom,
}: {
	equipeId: Equipe['id'];
	communeId: Commune['id'];
	voieNom: string;
}) {
	const must: builder.Query[] = [];

	const mustNot: builder.Query[] = [];

	const { must: mustEquipeTerritoire, mustNot: mustNotEquipeTerritoire } =
		await elasticQueryEquipe.buildEquipeTerritoireQuery({
			equipeId,
		});

	must.push(...mustEquipeTerritoire);
	mustNot.push(...mustNotEquipeTerritoire);

	must.push(builder.termQuery(properties.territoireCommuneId, communeId));

	must.push(builder.matchQuery(properties.adresseVoieNom, voieNom).operator('and'));

	const query = builder.boolQuery();

	if (must.length) {
		query.must(must);
	}

	if (mustNot.length) {
		query.must(mustNot);
	}

	const resultsAggs = await elastic.search(elasticQueryLocal.index, {
		limit: 0,
		fields: [],
		query,
		aggs: [
			elastic.helpers.builder
				.termsAggregation('numeros', properties.adresseNumero)
				.order('_key', 'asc')
				.size(10000),
		],
	});

	if (!resultsAggs) {
		return [];
	}

	return localNumerosFormat(resultsAggs);
}

export function eqLocConsultationUpdate({
	equipeId,
	localId,
	consultationAt,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	consultationAt: Date;
}) {
	return elastic.update(elasticQueryLocal.index, {
		id: localId,
		script: elastic.helpers.equipeObjectMerge({
			equipeId,
			value: {
				consultation_at: consultationAt,
				consultation_str: consultationAt.toISOString(),
			},
		}),
	});
}

export function eqLocContributionUpdate({
	equipeId,
	localId,
	contribution: { nom, occupe },
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	contribution: Partial<EquipeLocalContribution>;
}) {
	return elastic.update(elasticQueryLocal.index, {
		id: localId,
		script: elastic.helpers.equipeFieldMerge({
			equipeId,
			field: propertiesEquipesSet.contribution,
			value: {
				nom,
				occupe,
			},
		}),
	});
}

export function eqLocContributionOccupeUpdate({
	equipeId,
	localId,
	occupe,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	occupe: EquipeLocalContribution['occupe'];
}) {
	return elastic.update(elasticQueryLocal.index, {
		id: localId,
		script: elastic.helpers.equipeFieldWithParentSet({
			equipeId,
			parent: propertiesEquipesSet.contribution,
			field: propertiesEquipesContributionSet.occupe,
			value: occupe,
		}),
	});
}

export function eqLocSuiviDatesUpdate({
	equipeId,
	localId,
	suiviDates,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	suiviDates: (
		| ActionDemande['createdAt']
		| ActionEchange['createdAt']
		| ActionRappel['createdAt']
	)[];
}) {
	return elastic.update(elasticQueryLocal.index, {
		id: localId,
		script: elastic.helpers.equipeFieldSet({
			equipeId,
			field: propertiesEquipesSet.suiviDates,
			value: suiviDates,
		}),
	});
}

export function eqLocDemandeAdd({
	equipeId,
	localId,
	demandeTypeIds,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	demandeTypeIds: DemandeTypeId[];
}) {
	return elastic.update(elasticQueryLocal.index, {
		id: localId,
		script: elastic.helpers.equipeFieldAddMany({
			equipeId,
			field: propertiesEquipesSet.demandes,
			values: demandeTypeIds,
		}),
	});
}

export function eqLocDemandeRemove({
	equipeId,
	localId,
	demandeTypeId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	demandeTypeId: DemandeTypeId;
}) {
	return elastic.update(elasticQueryLocal.index, {
		id: localId,
		script: elastic.helpers.equipeFieldRemove({
			equipeId,
			field: propertiesEquipesSet.demandes,
			value: demandeTypeId,
		}),
	});
}

export function eqLocEtiquetteUpdate({
	equipeId,
	localId,
	etiquetteIds,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	etiquetteIds: Etiquette['id'][];
}) {
	return elastic.update(elasticQueryLocal.index, {
		id: localId,
		script: elastic.helpers.equipeFieldSet({
			equipeId,
			field: propertiesEquipesSet.etiquettes,
			value: etiquetteIds,
		}),
	});
}

export async function eqLocEtiquetteAddMany({
	equipeId,
	localIds,
	etiquetteIds,
}: {
	equipeId: Equipe['id'];
	localIds: Local['id'][];
	etiquetteIds: Etiquette['id'][];
}) {
	const results = await elastic.bulkUpdate(elasticQueryLocal.index, localIds, (localId: string) => [
		localId,
		{
			script: elastic.helpers.equipeFieldAddMany({
				equipeId,
				field: propertiesEquipesSet.etiquettes,
				values: etiquetteIds,
			}),
		},
	]);

	elastic.helpers.updateErrorPrint(results);
}

export async function eqLocEtiquetteAddManyByTuple({
	eqLocEtiquettes,
}: {
	eqLocEtiquettes: EquipeLocalEtiquette[];
}) {
	const results = await elastic.bulk(
		elasticQueryLocal.index,
		eqLocEtiquettes,
		(eqLocEtiquette: EquipeLocalEtiquette) => [
			{ update: { _id: eqLocEtiquette.localId } },
			{
				script: elastic.helpers.equipeFieldAdd({
					equipeId: eqLocEtiquette.equipeId,
					field: propertiesEquipesSet.etiquettes,
					value: eqLocEtiquette.etiquetteId,
				}),
			},
		]
	);

	elastic.helpers.updateErrorPrint(results);
}

export async function eqLocEtiquetteRemoveMany({
	equipeId,
	localIds,
	etiquetteIds,
}: {
	equipeId: Equipe['id'];
	localIds: Local['id'][];
	etiquetteIds: Etiquette['id'][];
}) {
	const results = await elastic.bulkUpdate(elasticQueryLocal.index, localIds, (localId: string) => [
		localId,
		{
			script: elastic.helpers.equipeFieldRemoveMany({
				equipeId,
				field: propertiesEquipesSet.etiquettes,
				values: etiquetteIds,
			}),
		},
	]);

	elastic.helpers.updateErrorPrint(results);
}

export async function eqLocEtiquetteRemoveManyByTuple({
	eqLocEtiquettes,
}: {
	eqLocEtiquettes: Pick<EquipeLocalEtiquette, 'equipeId' | 'etiquetteId' | 'localId'>[];
}) {
	const results = await elastic.bulk(
		elasticQueryLocal.index,
		eqLocEtiquettes,
		(eqLocEtiquette: Pick<EquipeLocalEtiquette, 'equipeId' | 'etiquetteId' | 'localId'>) => [
			{ update: { _id: eqLocEtiquette.localId } },
			{
				script: elastic.helpers.equipeFieldRemove({
					equipeId: eqLocEtiquette.equipeId,
					field: propertiesEquipesSet.etiquettes,
					value: eqLocEtiquette.etiquetteId,
				}),
			},
		]
	);

	elastic.helpers.updateErrorPrint(results);
}

export function eqLocFavoriUpdate({
	equipeId,
	localId,
	devecoId,
	favori,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	devecoId: Deveco['id'];
	favori: boolean;
}) {
	return elastic.update(elasticQueryLocal.index, {
		id: localId,
		/* eslint-disable-next-line @typescript-eslint/unbound-method */
		script: (favori ? elastic.helpers.equipeFieldAdd : elastic.helpers.equipeFieldRemove)({
			equipeId,
			field: propertiesEquipesSet.favoris,
			value: devecoId,
		}),
	});
}
