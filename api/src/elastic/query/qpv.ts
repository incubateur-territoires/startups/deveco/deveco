import { fromEntriesToCamelCaseKeys } from '../../lib/utils';

export const index = process.env.ELASTICSEARCH_INDEX_QPV || 'qpv_development';

import mapping from '../../elastic/mappings/qpv_index.json';

export { mapping };

export const BATCH = 1000;

const properties = fromEntriesToCamelCaseKeys(mapping.properties);

export const mappings = {
	properties,
};
