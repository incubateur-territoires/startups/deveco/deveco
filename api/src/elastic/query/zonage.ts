import { fromEntriesToCamelCaseKeys } from '../../lib/utils';
import { Equipe, Zonage, Etiquette } from '../../db/types';
import elastic from '../../elastic';

export const index = process.env.ELASTICSEARCH_INDEX_ZONAGE || 'zonage_development';

import mapping from '../../elastic/mappings/zonage_index.json';

export { mapping };

export const BATCH = 1000;

const properties = fromEntriesToCamelCaseKeys(mapping.properties);

export const mappings = {
	properties,
};

export function zonageAdd({
	equipeId,
	zonageId,
	zonageNom,
	etiquetteId,
	geometrie,
}: {
	equipeId: Equipe['id'];
	zonageId: Zonage['id'];
	zonageNom: Etiquette['nom'];
	etiquetteId: Zonage['etiquetteId'];
	geometrie: string;
}) {
	return elastic.index(index, zonageId.toString(), {
		[properties.zonageId]: zonageId,
		[properties.zonageNom]: zonageNom,
		[properties.zonageEquipeId]: equipeId,
		[properties.zonageEtiquetteId]: etiquetteId,
		[properties.zonageGeometrie]: geometrie,
	});
}

export function zonageUpdate({
	zonageId,
	geometrie,
}: {
	zonageId: Zonage['id'];
	geometrie: string;
}) {
	return elastic.index(index, zonageId.toString(), {
		[properties.zonageId]: zonageId,
		[properties.zonageGeometrie]: geometrie,
	});
}
