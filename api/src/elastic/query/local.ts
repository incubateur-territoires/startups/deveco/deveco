import { Local, Qpv2024, Qpv2015 } from '../../db/types';
import elastic from '../../elastic';
import { Position } from '../../db/schema/custom-types/postgis';

export const index = process.env.ELASTICSEARCH_INDEX_LOCAL || 'local_development';

export const BATCH = 10000;

import mapping from '../../elastic/mappings/etablissement_index.json';
import { fromEntriesToCamelCaseKeys } from '../../lib/utils';

export { mapping };

const properties = fromEntriesToCamelCaseKeys(mapping.properties);

export function localGeolocalisationUpdateMany({
	localGeolocalisations,
}: {
	localGeolocalisations: {
		localId: Local['id'];
		geolocalisation: Position | null;
	}[];
}) {
	return elastic.bulkUpdate(index, localGeolocalisations, ({ localId, geolocalisation }) => [
		localId,
		{
			doc: {
				adresse_geolocalisation: geolocalisation,
			},
		},
	]);
}

export function localQpvUpdate({
	localId,
	qpv2024Id,
	qpv2015Id,
}: {
	localId: Local['id'];
	qpv2024Id: Qpv2024['id'] | null;
	qpv2015Id: Qpv2015['id'] | null;
}) {
	return elastic.update(index, {
		id: localId,
		doc: {
			[properties.adresseQpv2024Id]: qpv2024Id,
			[properties.adresseQpv2015Id]: qpv2015Id,
		},
	});
}
