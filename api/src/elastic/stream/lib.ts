import { Client } from '@opensearch-project/opensearch';
import { SearchRequest } from '@opensearch-project/opensearch/api/types';

import { Readable, ReadableOptions } from 'stream';

type optionalField = '_id' | '_score' | '_type' | '_index' | '_parent' | '_routing' | 'inner_hits';

const MAX_URL_LENGTH = 2000;

export default class ElasticsearchScrollStream extends Readable {
	_client: Client;
	_options: SearchRequest;
	_extrafields: optionalField[];
	_reading: boolean;
	_counter: number;
	_total: number;
	_forceClose: boolean;

	constructor(
		client: Client,
		query_opts: SearchRequest,
		optional_fields: optionalField[],
		stream_opts: ReadableOptions
	) {
		super({
			...stream_opts,
			objectMode: true,
		});

		this._client = client;
		this._options = query_opts;
		this._extrafields = optional_fields;
		this._options.scroll = query_opts.scroll || '10m';
		this._reading = false;
		this._counter = 0;
		this._total = 0;
		this._forceClose = false;

		this.getMoreUntilDone = this.getMoreUntilDone.bind(this);
	}

	getMoreUntilDone(response: any) {
		const body: {
			hits: {
				hits: any[];
				total:
					| number
					| {
							value: number;
					  };
			};
			_scroll_id: string;
		} = response.body ? response.body : response;

		// Set the total matching documents
		// For Elasticsearch greater then 7.x hits.total is an object:
		//    {
		//       value: 20,
		//       relation: "eq"
		//    }
		this._total = typeof body.hits.total === 'object' ? body.hits.total.value : body.hits.total;

		body.hits.hits.forEach((hit: any) => {
			const ref_results = hit.fields ? hit.fields : hit._source || {};

			// populate extra fields
			this._extrafields.forEach((entry: any) => {
				ref_results[entry] = hit[entry];
			});

			this.push(ref_results);
			this._counter++;
		});

		if (this._total !== this._counter && !this._forceClose) {
			if (body._scroll_id.length > MAX_URL_LENGTH) {
				this._client
					.scroll({ body: { scroll: this._options.scroll, scroll_id: body._scroll_id } } as any)
					.then(this.getMoreUntilDone.bind(this))
					.catch((err) => this.emit('error', err));
			} else {
				this._client
					.scroll({ scroll: `${this._options.scroll}`, scroll_id: body._scroll_id }, undefined)
					.then(this.getMoreUntilDone.bind(this))
					.catch((err) => this.emit('error', err));
			}
		} else {
			// clearScroll for the current _scroll_id
			this._client
				.clearScroll({ scroll_id: [body._scroll_id] })
				.then(() => {
					// end correctly
					return setImmediate(() => {
						this._reading = false;
						this._counter = 0;
						this._forceClose = false;
						this.push(null);
					});
				})
				.catch((err) => this.emit('error', err));
		}
	}

	_read() {
		if (this._reading) {
			return false;
		}

		this._reading = true;
		this._client
			.search(this._options as any)
			.then(this.getMoreUntilDone.bind(this))
			.catch((err) => this.emit('error', err));
	}

	close() {
		return (this._forceClose = true);
	}
}
