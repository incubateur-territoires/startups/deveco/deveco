/**
 * Elasticsearch Scroll Stream
 *
 * Create a ReadableStream from an elasticsearch scroll query.
 */
import { Client } from '@opensearch-project/opensearch';

import { ReadableOptions } from 'stream';

import LibElasticsearchAdaptee from './lib';

const allowed_extrafields = [
	'_id',
	'_score',
	'_type',
	'_index',
	'_parent',
	'_routing',
	'inner_hits',
];

type optionalField = '_id' | '_score' | '_type' | '_index' | '_parent' | '_routing' | 'inner_hits';

export default class ElasticsearchScrollStream {
	constructor(
		client: Client,
		query_opts: any = {},
		optional_fields: optionalField[] = [],
		stream_opts: ReadableOptions = {}
	) {
		if (!client) {
			console.logError('ElasticsearchScrollStream: client is ', client);
			throw new Error('ElasticsearchScrollStream: no client');
		}
		if (Object.keys(query_opts).length === 0) {
			throw new Error('ElasticsearchScrollStream: missing parameters');
		}

		if (!Array.isArray(optional_fields)) {
			console.logError(
				'ElasticsearchScrollStream: optional_fields must be an array',
				optional_fields
			);
			throw new Error('ElasticsearchScrollStream: bad values for optional_fields');
		}

		optional_fields.forEach((entry) => {
			if (allowed_extrafields.indexOf(entry) === -1) {
				throw new Error(
					`ElasticsearchScrollStream: property '${entry}' not allowed in optional_fields`
				);
			}
		});

		return new LibElasticsearchAdaptee(client, query_opts, optional_fields, stream_opts);
	}
}
