import 'dotenv/config';
import { Client, ApiResponse } from '@opensearch-project/opensearch';
import builder from 'elastic-builder';

import util from 'util';
import { Readable } from 'stream';

import ElasticsearchScrollStream from './stream';

const node = process.env.ELASTICSEARCH_URL
	? process.env.ELASTICSEARCH_URL
	: `${process.env.ELASTICSEARCH_PROTOCOL}://${process.env.ELASTICSEARCH_USER}:${process.env.ELASTICSEARCH_PASSWORD}@${process.env.ELASTICSEARCH_HOST}:${process.env.ELASTICSEARCH_PORT}`;

export const client = new Client({
	node,
});

function ARRAY_MERGE_SOURCE_BUILD({ field }: { field: string }) {
	return `
if (params.${field} != null && params.${field}.size() > 0) {
	if (ctx._source.${field} == null) {
		ctx._source.${field} = new ArrayList();
	}

	ctx._source.${field}.addAll(params.${field});
}
`;
}

const FIELD_SET = `
ctx._source[params.field] = params.value;
`;

const OBJECT_MERGE = `
for (entry in params.value.entrySet()) {
	ctx._source[entry.key] = entry.value;
}
`;

const EQUIPE_INIT = `
if (ctx._source.equipes == null) {
  ctx._source.equipes = new ArrayList();
}

def target = ctx._source.equipes.find(
  equipe -> equipe !== null ? equipe.equipe_id == params.equipe_id : false
);

if (target == null) {
  target = [:];
  target.equipe_id = params.equipe_id;
  ctx._source.equipes.add(target);
}
`;

const EQUIPE_FIELD_SET = `
${EQUIPE_INIT}
target[params.field] = params.value;
`;

const EQUIPE_FIELD_SET_PARENT = `
${EQUIPE_INIT}
if (target[params.parent] == null) {
  target[params.parent] = [:];
}
target[params.parent][params.field] = params.value;
`;

const EQUIPE_FIELD_MERGE = `
${EQUIPE_INIT}
if (target[params.field] == null) {
  target[params.field] = params.value
} else {
  for (entry in params.value.entrySet()) {
    target[params.field][entry.key] = entry.value;
  }
}
`;

const EQUIPE_OBJECT_MERGE = `
${EQUIPE_INIT}
for (entry in params.value.entrySet()) {
	target[entry.key] = entry.value;
}
`;

const EQUIPE_FIELD_ARRAY_INIT = `
if (target[params.field] == null) {
  target[params.field] = new ArrayList();
}
`;

const EQUIPE_FIELD_ADD = `
${EQUIPE_INIT}
${EQUIPE_FIELD_ARRAY_INIT}
if (!target[params.field].contains(params.value)) {
  target[params.field].add(params.value);
}
`;

const EQUIPE_FIELD_REMOVE = `
${EQUIPE_INIT}
${EQUIPE_FIELD_ARRAY_INIT}
for (int i = target[params.field].length - 1; i >= 0; i--) {
  if (target[params.field][i] == params.value) {
    target[params.field].remove(i);
  }
}
`;

const EQUIPE_FIELD_ADD_MANY = `
${EQUIPE_INIT}
${EQUIPE_FIELD_ARRAY_INIT}
for (value in params.values) {
  def found = target[params.field].find(valueDoc -> valueDoc == value);
  if (found == null) {
    target[params.field].add(value);
  }
}
`;

const EQUIPE_FIELD_REMOVE_MANY = `
${EQUIPE_INIT}
${EQUIPE_FIELD_ARRAY_INIT}
for (value in params.values) {
	for (int i = target[params.field].length - 1; i >= 0; i--) {
		if (target[params.field][i] == value) {
			target[params.field].remove(i);
		}
	}
}
`;

interface idParams {
	id: string;
}

export interface scriptParams {
	script: {
		source: string;
		lang: 'painless';
		/* eslint-disable-next-line @typescript-eslint/no-redundant-type-constituents */
		params: Record<string, any | undefined>;
	};
}

export interface docParams {
	doc: unknown;
	doc_as_upsert?: boolean;
}

export type scriptParamsUpdate = idParams & scriptParams;
export type docParamsUpdate = idParams & docParams;

export type paramsUpdateBulk = scriptParams | docParams;
export type paramsUpdate = scriptParamsUpdate | docParamsUpdate;

const helpers = {
	builder,

	updateErrorPrint(results?: ApiResponse) {
		if (!results) {
			return;
		}

		let allErrorsFound = true;
		if (results.body?.errors) {
			for (const result of results.body.items) {
				if (!result) {
					continue;
				}

				const error = result.update?.error ?? result.index?.error;
				if (error) {
					console.logError(error);
				} else {
					allErrorsFound = false;
				}
			}
		}

		if (!allErrorsFound) {
			console.logError(`Au moins une erreur n'a pas été trouvée :`, results.body.errors);
		}
	},

	arrayMerge({ field, value }: { field: string; value: Record<string, unknown> }) {
		return {
			source: ARRAY_MERGE_SOURCE_BUILD({ field }),
			lang: 'painless',
			params: {
				field,
				value,
			},
		} as const;
	},

	arrayMergeMany({ fields, value }: { fields: string[]; value: Record<string, unknown> }) {
		return {
			source: fields.map((field) => ARRAY_MERGE_SOURCE_BUILD({ field })).join('\n'),
			lang: 'painless',
			params: value,
		} as const;
	},

	fieldSet({ field, value }: { field: string; value: unknown }) {
		return {
			source: FIELD_SET,
			lang: 'painless',
			params: {
				field,
				value,
			},
		} as const;
	},

	objectMerge({ value }: { value: Record<string, unknown> }) {
		return {
			source: OBJECT_MERGE,
			lang: 'painless',
			params: {
				value,
			},
		} as const;
	},

	equipeFieldSet({ equipeId, field, value }: { equipeId: number; field: string; value: unknown }) {
		return {
			source: EQUIPE_FIELD_SET,
			lang: 'painless',
			params: {
				equipe_id: equipeId,
				field,
				value,
			},
		} as const;
	},

	equipeObjectMerge({ equipeId, value }: { equipeId: number; value: Record<string, unknown> }) {
		return {
			source: EQUIPE_OBJECT_MERGE,
			lang: 'painless',
			params: {
				equipe_id: equipeId,
				value,
			},
		} as const;
	},

	equipeFieldWithParentSet({
		equipeId,
		parent,
		field,
		value,
	}: {
		equipeId: number;
		parent: string;
		field: string;
		value: unknown;
	}) {
		return {
			source: EQUIPE_FIELD_SET_PARENT,
			lang: 'painless',
			params: {
				equipe_id: equipeId,
				parent,
				field,
				value,
			},
		} as const;
	},

	equipeFieldMerge({
		equipeId,
		field,
		value,
	}: {
		equipeId: number;
		field: string;
		value: unknown;
	}) {
		return {
			source: EQUIPE_FIELD_MERGE,
			lang: 'painless',
			params: {
				equipe_id: equipeId,
				field,
				value,
			},
		} as const;
	},

	equipeFieldAdd({ equipeId, field, value }: { equipeId: number; field: string; value: unknown }) {
		return {
			source: EQUIPE_FIELD_ADD,
			lang: 'painless',
			params: {
				equipe_id: equipeId,
				field,
				value,
			},
		} as const;
	},

	equipeFieldAddMany({
		equipeId,
		field,
		values,
	}: {
		equipeId: number;
		field: string;
		values: unknown[];
	}) {
		return {
			source: EQUIPE_FIELD_ADD_MANY,
			lang: 'painless',
			params: {
				equipe_id: equipeId,
				field,
				values,
			},
		} as const;
	},

	equipeFieldRemove({
		equipeId,
		field,
		value,
	}: {
		equipeId: number;
		field: string;
		value: unknown;
	}) {
		return {
			source: EQUIPE_FIELD_REMOVE,
			lang: 'painless',
			params: {
				equipe_id: equipeId,
				field,
				value,
			},
		} as const;
	},

	equipeFieldRemoveMany({
		equipeId,
		field,
		values,
	}: {
		equipeId: number;
		field: string;
		values: unknown[];
	}) {
		return {
			source: EQUIPE_FIELD_REMOVE_MANY,
			lang: 'painless',
			params: {
				equipe_id: equipeId,
				field,
				values,
			},
		} as const;
	},
};

function errorPrint(prefix: string, e: any) {
	console.logError(
		`[${prefix}] Erreur ElasticSearch :`,
		e?.meta?.body?.error ?? e?.message ?? 'erreur inconnue',
		util.inspect(e, { depth: null })
	);

	const body = e?.meta?.meta?.request?.params?.body;
	if (body) {
		console.logError(`[${prefix}] paramètres :`, util.inspect(body));
	}
}

export default {
	client,

	async search(
		index: string,
		{
			fields,
			offset = 0,
			limit = 20,
			query = builder.matchAllQuery(),
			sort,
			sorts,
			aggs,
		}: {
			fields: string[];
			limit?: number;
			offset?: number;
			query?: builder.Query;
			sort?: builder.Sort;
			sorts?: builder.Sort[];
			aggs?: builder.Aggregation[];
		},
		options: { refresh?: boolean } = {}
	) {
		const body = builder.requestBodySearch().query(query);

		if (sort) {
			body.sort(sort);
		}

		if (sorts) {
			body.sorts(sorts);
		}

		if (aggs && aggs.length > 0) {
			body.aggs(aggs);
		}

		try {
			return await client.search({
				index,
				from: offset,
				size: limit,
				body: {
					_source: {
						includes: fields,
					},
					...body.toJSON(),
				},
				...options,
			});
		} catch (e: any) {
			errorPrint('search', e);
		}
	},

	async msearch(
		index: string,
		queries: {
			fields: string[];
			limit?: number;
			offset?: number;
			query: builder.Query;
		}[],
		options: { refresh?: boolean } = {}
	) {
		try {
			return await client.msearch({
				index,
				body: queries.flatMap(({ fields, limit, offset, query }) => [
					{ index },
					{
						from: offset,
						size: limit,
						_source: {
							includes: fields,
						},
						...builder.requestBodySearch().query(query).toJSON(),
					},
				]),
				...options,
			});
		} catch (e: any) {
			errorPrint('msearch', e);
		}
	},

	async count(
		index: string,
		{ query = builder.matchAllQuery() }: { query?: builder.Query },
		options: { refresh?: boolean } = {}
	) {
		try {
			return await client.count({
				index,
				body: {
					query: query.toJSON(),
				},
				...options,
			});
		} catch (e: any) {
			errorPrint('count', e);
		}
	},

	async updateByQuery(
		index: string,
		query: builder.Query,
		updateParams: scriptParams,
		options: { refresh?: boolean; wait_for_completion?: boolean } = {}
	) {
		try {
			return await client.updateByQuery({
				index,
				body: {
					...updateParams,
					query: query.toJSON(),
				},
				...options,
			});
		} catch (e: any) {
			errorPrint('updateByQuery', e);
		}
	},

	async index(index: string, id: string, body: object = {}, options: object = {}) {
		if (!id) {
			throw new Error(`[elastic.index] Paramètre obligatoire : id`);
		}

		if (Object.keys(body).length === 0) {
			throw new Error(`[elastic.index] Aucun champ à indexer pour l'id #${id}`);
		}

		try {
			return await client.index(
				{
					index,
					id,
					body,
					...options,
				}
				// { compression: 'gzip' }
			);
		} catch (e: any) {
			errorPrint('index', e);
		}
	},

	async update(index: string, updateParams: paramsUpdate, options: { refresh?: boolean } = {}) {
		const { id, ...body } = updateParams;

		if (!id) {
			throw new Error(`[elastic.update] Paramètre obligatoire : id`);
		}

		if (Object.keys(body).length === 0) {
			throw new Error(`[elastic.update] Aucun champ à mettre à jour pour l'id #${id}`);
		}

		try {
			return await client.update(
				{
					index,
					id,
					body,
					...options,
				}
				// { compression: 'gzip' }
			);
		} catch (e: any) {
			errorPrint('update', e);
		}
	},

	async bulkIndex<T>(
		index: string,
		items: T[],
		onDoc: (item: T) => [string, any],
		options: { refresh?: boolean } = {}
	) {
		if (items.length === 0) {
			return;
		}

		try {
			const operations = items.flatMap((item) => {
				const [_id, operation] = onDoc(item);

				/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
				return [{ index: { _id } }, operation];
			});

			return await client.bulk({
				index,
				body: operations,
				...options,
			});
		} catch (e: any) {
			errorPrint('bulkIndex', e);
		}
	},

	async bulkIndexStream(
		index: string,
		stream: any,
		options: { refresh?: boolean } = {},
		onDoc: (item: unknown) => string,
		onError: (doc: unknown) => void
	) {
		try {
			return await client.helpers.bulk({
				datasource: stream,
				onDocument(doc) {
					const _id = onDoc(doc);

					return [{ index: { _index: index, _id } }, doc];
				},
				onDrop(doc) {
					if (onError) {
						onError(doc);
					}
				},
				...options,
			});
		} catch (e: any) {
			errorPrint('bulkIndexStream', e);
		}
	},

	async bulkUpdate<T>(
		index: string,
		items: T[],
		onDoc: (item: T) => [string, paramsUpdateBulk],
		options: { refresh?: boolean } = {}
	) {
		if (items.length === 0) {
			return;
		}

		try {
			const operations = items.flatMap((item) => {
				const [_id, operation] = onDoc(item);

				return [{ update: { _id } }, operation];
			});

			const resp = await client.bulk({
				index,
				body: operations,
				...options,
			});

			helpers.updateErrorPrint(resp);

			return resp;
		} catch (e: any) {
			errorPrint('bulkUpdate', e);
		}
	},

	async bulkUpdateStream(
		index: string,
		stream: any,
		itemIdBuild: (item: unknown) => [string],
		options: { doc_as_upsert?: boolean; refresh?: boolean } = {}
	) {
		try {
			return await client.helpers.bulk({
				datasource: stream,
				onDocument(doc) {
					return [{ update: { _index: index, _id: itemIdBuild(doc) } }, { ...options }];
				},
				refreshOnCompletion: true,
			});
		} catch (e: any) {
			errorPrint('bulkIndexStream', e);
		}
	},

	async bulk<T>(
		index: string,
		items: T[],
		onDoc: (item: T) => any[],
		options: { refresh?: boolean } = {}
	) {
		if (items.length === 0) {
			return;
		}

		try {
			const operations = items.flatMap(onDoc);

			return await client.bulk({
				index,
				body: operations,
				...options,
			});
		} catch (e: any) {
			errorPrint('bulk', e);
		}
	},

	async *stream(
		index: string,
		{
			fields,
			query,
		}: {
			fields: ['etablissement_id' | 'local_id'];
			query: builder.Query;
		}
	) {
		const body = builder.requestBodySearch().query(query);

		try {
			const esStream = new ElasticsearchScrollStream(client, {
				index,
				// temps de cache du moteur de recherche dans ElasticSearch
				scroll: '10m',
				body: {
					_source: fields,
					...body.toJSON(),
				},
			}) as Readable;

			for await (const doc of esStream) {
				yield [doc];
			}
		} catch (e: any) {
			errorPrint('stream', e);
			console.logError('stream', body.toJSON());
		}
	},

	async reindex(
		index: string,
		{ isIndexInit, indexTemplate }: { isIndexInit: boolean; indexTemplate: string }
	) {
		// si l'index est initialisé pour la première fois
		// on copie de l'index de base vers l'index template
		// sinon, on écrase l'index template vers l'index de base
		const indexSource = !isIndexInit ? index : indexTemplate;
		const indexDest = !isIndexInit ? indexTemplate : index;

		console.logInfo(`[reindex] copie de l'index ${indexSource} vers ${indexDest}`);

		// const r =
		await client.reindex({
			refresh: true,
			body: {
				source: {
					index: indexSource,
				},
				dest: {
					index: indexDest,
				},
			},
		});

		// console.log(JSON.stringify(r, null, 2));
	},

	async alias(index: string, alias: string) {
		await client.indices.putAlias({
			index,
			name: alias,
		});
	},

	helpers,
};
