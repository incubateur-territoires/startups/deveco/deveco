import { CtxUser } from '../middleware/auth';
import * as dbQueryEtablissement from '../db/query/etablissement';
import * as dbQueryEtablissementCreation from '../db/query/etablissement-creation';
import * as dbQueryEquipe from '../db/query/equipe';
import * as dbQueryLocal from '../db/query/local';

export const controllerMeta = {
	async nafTypeGetMany(ctx: CtxUser) {
		const query = ctx.query;
		const recherche = query.recherche as string;

		const codesNaf = await dbQueryEtablissement.nafTypeGetManyByNomOrId(recherche, [1, 2, 5]);

		ctx.body = codesNaf.sort((a, b) => a.niveau - b.niveau || a.id.localeCompare(b.id));
	},

	async categorieJuridiqueGetMany(ctx: CtxUser) {
		const query = ctx.query;
		const recherche = query.recherche as string;

		let categoriesJuridiques;

		if (recherche) {
			categoriesJuridiques = await dbQueryEtablissement.categorieJuridiqueGetManyByNomOrId(
				recherche,
				[2, 3]
			);
		} else {
			categoriesJuridiques = await dbQueryEtablissement.categorieJuridiqueGetManyByNiveau({
				niveaux: [2, 3],
			});
		}

		// on filtre sur la taille pour afficher les catégories de niveau 2 avant celles de niveau 3
		// puis sur l'id par ordre croissant
		// on met les catégories indéfinies à la fin de la liste
		ctx.body = categoriesJuridiques.sort((a, b) => {
			const taille = a.id.length - b.id.length;
			if (taille !== 0) {
				return taille;
			}

			if ((a.nom === 'Indéfini' || b.nom === 'Indéfini') && a.nom !== b.nom) {
				return a.nom === 'Indéfini' ? 1 : -1;
			}

			return a.id.localeCompare(b.id);
		});
	},

	async equipeTypeGetMany(ctx: CtxUser) {
		const equipeTypes = await dbQueryEquipe.equipeTypeGetMany();

		ctx.body = equipeTypes;
	},

	async localCategorieGetMany(ctx: CtxUser) {
		const localCategories = await dbQueryLocal.localCategorieTypeGetMany();

		ctx.body = localCategories;
	},

	async localNatureGetMany(ctx: CtxUser) {
		const localNatures = await dbQueryLocal.localNatureTypeGetMany();

		ctx.body = localNatures;
	},

	async createursSourcesGet(ctx: CtxUser) {
		const typesProjet = await dbQueryEtablissementCreation.etabCreaTypeProjetGetMany();
		const contactOrigines = await dbQueryEtablissementCreation.etabCreaContactOrigineGetMany();
		const secteursActivite = await dbQueryEtablissementCreation.etabCreaSecteurActiviteGetMany();
		const sourcesFinancement =
			await dbQueryEtablissementCreation.etabCreaSourceFinancementGetMany();
		const niveauxDiplome = await dbQueryEtablissementCreation.etabCreaNiveauDiplomeGetMany();
		const situationsProfessionnelles =
			await dbQueryEtablissementCreation.etabCreaSituationProfessionnelleGetMany();
		const categoriesJuridiques = await dbQueryEtablissement.categorieJuridiqueGetManyByNiveau({
			niveaux: [2],
		});

		ctx.body = {
			typesProjet,
			contactOrigines,
			secteursActivite: secteursActivite.sort((a, b) => a.nom.localeCompare(b.nom)),
			sourcesFinancement: sourcesFinancement.sort((a, b) => a.nom.localeCompare(b.nom)),
			niveauxDiplome,
			situationsProfessionnelles: situationsProfessionnelles.sort((a, b) =>
				a.nom.localeCompare(b.nom)
			),
			categoriesJuridiques: categoriesJuridiques
				.map(({ id, nom }) => ({
					id,
					nom: `${id} - ${nom}`,
				}))
				.sort((a, b) => a.nom.localeCompare(b.nom)),
		};
	},
};
