import { StatusCodes } from 'http-status-codes';

import { ImportFileInput } from '../controller/types';
import { CtxUser } from '../middleware/auth';
import * as serviceImport from '../service/import';

export const controllerImport = {
	fromFile(ctx: CtxUser) {
		const admin = ctx.state.compte;
		const compteId = ctx.params.compteId;

		const {
			nom,
			taille,
			derniereModification,
			contenu: base64String,
		} = ctx.request.body as ImportFileInput;

		if (!base64String) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'fichier requis');
		}

		const excelFile = Buffer.from(base64String.split(',')[1], 'base64');

		try {
			console.logInfo(
				`Import du fichier nommé ${nom} de taille ${taille}, date de dernière modification : ${new Date(
					derniereModification
				).toISOString()}`
			);
		} catch (_e: any) {
			console.logError(`Problème de données front ?`, { nom, taille, derniereModification });
		}

		try {
			void serviceImport.importSheets({ excelFile, compteId, admin });

			ctx.status = StatusCodes.OK;
			ctx.body = {};
		} catch (e: any) {
			ctx.throw(StatusCodes.BAD_REQUEST, { error: e.message });
		}
	},
};
