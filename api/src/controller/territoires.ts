import { StatusCodes } from 'http-status-codes';

import { territoireTypeIdValidate } from '../db/types';
import { CtxUser } from '../middleware/auth';
import {
	communeGet,
	communeGetMany,
	epciGet,
	epciGetManyByPetr,
	epciGetMany,
} from '../db/query/territoire';
import { serviceTerritoire } from '../service/territoire';
import { dbQuoteEscape } from '../lib/utils';

export const controllerTerritoires = {
	async communeGetMany(ctx: CtxUser) {
		const communes = await communeGetMany({ communeIds: null });

		ctx.body = { communes };
	},

	async communeGet(ctx: CtxUser) {
		const communeId = ctx.params.id;
		const commune = await communeGet({ communeId });

		ctx.body = { commune };
	},

	async epciGet(ctx: CtxUser) {
		const epciId = ctx.params.id;
		const epci = await epciGet({ epciId });
		ctx.body = epci;
	},

	async epciGetMany(ctx: CtxUser) {
		const epcis = await epciGetMany({ epciIds: null });
		ctx.body = epcis;
	},

	async epciGetManyByPetr(ctx: CtxUser) {
		const petrId = ctx.params.id;

		const petr = await epciGetManyByPetr({ petrId });
		ctx.body = { petr };
	},

	async territoireGetManyByType(ctx: CtxUser) {
		const nomRaw = ctx.query.nom as string;
		const territoireTypeIdRaw = ctx.params.territoireType;

		const territoireTypeId = territoireTypeIdValidate(territoireTypeIdRaw);
		if (!territoireTypeId) {
			ctx.throw(
				StatusCodes.BAD_REQUEST,
				`le type de territoire ${territoireTypeIdRaw} est inconnu`
			);
		}

		const recherche = nomRaw ? dbQuoteEscape(nomRaw.trim()) : undefined;

		const territoires = await serviceTerritoire.territoireGetManyByType({
			territoireTypeId,
			recherche,
		});

		ctx.body = {
			territoires: (territoires ?? []).sort((a, b) => a.nom.localeCompare(b.nom, 'fr')),
		};
	},
};
