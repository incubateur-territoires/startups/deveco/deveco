import { StatusCodes } from 'http-status-codes';
import { createObjectCsvStringifier } from 'csv-writer';
import { v4 as uuidv4 } from 'uuid';
import { InferInsertModel, isNull } from 'drizzle-orm';
import { addDays, startOfDay } from 'date-fns';

import { ParsedUrlQuery } from 'node:querystring';

import tokenMiddleware from '../middleware/token-check';
import { CtxAdmin, CtxDeveco, CtxUnidentified } from '../middleware/auth';
import db from '../db';
import {
	TriDeveco,
	Direction,
	territoireTypeIdValidate,
	AyantDroit,
	EquipeDemande,
	CompteDemande,
} from '../db/types';
import {
	CompteActifUpdateInput,
	CompteAddInput,
	EquipeAddInput,
	EquipeDemandeInput,
	EquipeEtiquetteRemoveManyInput,
} from '../controller/types';
import {
	compteDemande,
	equipeDemandeAutorisation,
	equipeDemande,
	ayantDroit,
} from '../db/schema/admin';
import * as dbQueryAdmin from '../db/query/admin';
import {
	devecoAdd,
	equipeGetByNom,
	equipeGet,
	equipeWithTerritoireWithNomGetMany,
	geoTokenAdd,
	etiquetteWithCountGetManyByEquipe,
	etiquetteRemoveManyByEquipe,
	devecoWithRelationsGetMany,
	devecoCount,
	devecoWithCompteGet,
} from '../db/query/equipe';
import * as elasticQuery from '../elastic/query/equipe-etablissement';
import { emailIsValid } from '../lib/utils';
import { isSiren, isSiret } from '../lib/regexp';
import { emailSend, smtpCheck } from '../lib/email';
import {
	querystringToArray,
	querystringToBoolean,
	querystringToDate,
	querystringToNumber,
	querystringToString,
} from '../lib/querystring';
import { serviceAdmin } from '../service/admin';
import { serviceElastic } from '../service/elastic';
import { serviceBrevo } from '../service/brevo';
import { serviceApiSirene } from '../service/api-sirene';
import { serviceApiGeo } from '../service/api-geo';
import { serviceApiBodacc } from '../service/api-bodacc';
import { serviceApiEntreprise } from '../service/api-entreprise';
import { serviceEmailTemplatesAutorisation } from '../service/email-templates/autorisation';
import { controllerEtablissements } from '../controller/etablissements';
import { formatEquipe, utilisateurPourAdminFormat } from '../controller/_format/deveco';
import { etiquetteWithCountFormatMany } from '../controller/_format/equipe';

const elementsParPage = 20;

const devecoTris: TriDeveco[] = ['nom', 'prenom', 'email', 'identifiant', 'connexion', 'action'];

function devecoGetManyParamsBuild(query: ParsedUrlQuery) {
	const { recherche, equipeId, page, tri: triRaw, direction } = query;

	const tri = querystringToString(triRaw);
	const parsedTri = tri && devecoTris.find((t) => t === tri);

	return {
		recherche: querystringToString(recherche)?.trim(),
		equipeId: querystringToNumber(equipeId) ?? undefined,

		// pagination
		elementsParPage,
		page: querystringToNumber(page) || 1,
		tri: parsedTri ? parsedTri : undefined,
		direction: (direction === 'desc' ? 'desc' : 'asc') as Direction,
	};
}

async function adminUpdateSirene({
	tache,
	debut,
	fin,
	depuis,
	skipSirene,
	forceMiseAJourAt,
}: {
	tache: string;
	debut: number;
	fin: number;
	depuis: number;
	skipSirene?: boolean | null;
	forceMiseAJourAt?: Date | null;
}) {
	const miseAJourAt =
		forceMiseAJourAt ??
		(depuis && tache && serviceApiSirene.tachesMiseAJourAt.includes(tache)
			? startOfDay(addDays(new Date(), -depuis))
			: new Date());

	const frequence = 'manuel';

	await serviceApiSirene.updateByCursor({
		frequence,
		tache,
		debutJours: debut,
		finJours: fin,
		depuis,
		miseAJourAt,
		skipSirene,
	});

	if (tache === 'equipe-etablissement') {
		return;
	}

	await serviceApiGeo.geocodeUpdate({
		frequence,
		jours: depuis,
		skipIndexation: true,
	});

	await serviceElastic.indexUpdateByMiseAJourAt({
		tacheNom: `elasticsearch-update-manuel`,
		miseAJourAtDebut: miseAJourAt,
		resetIndexes: true,
	});
}

// TODO mettre dans un service
async function envoiEmailConfirmationAyantDroit({
	ayantDroit,
	compteDemande,
	equipeDemande,
	token,
}: {
	ayantDroit: AyantDroit;
	compteDemande: CompteDemande;
	equipeDemande: EquipeDemande;
	token: string;
}) {
	const appUrl = process.env.APP_URL || '';

	const to = ayantDroit.email;

	const cc = compteDemande.email;
	const requerant = {
		nom: compteDemande.nom,
		prenom: compteDemande.prenom,
		equipe: equipeDemande.nom,
	};

	const html = serviceEmailTemplatesAutorisation.confirmationAyantDroit({
		appUrl,
		requerant,
		ayantDroit: ayantDroit,
		cle: token,
	});

	const from = `Jean-Baptiste de Deveco<${process.env.SMTP_FROM}>`;

	const subject = `Demande d'autorisation d'accès à Deveco`;

	if (smtpCheck()) {
		try {
			await emailSend({
				from,
				to,
				cc,
				subject,
				html,
			});

			console.logInfo(`Confirmation d'autorisation de demande pour ${to} avec ${cc} en CC.`);
		} catch (error) {
			console.logError(error);
		}
	} else {
		console.logInfo(
			"Emailing désactivé, pas d'envoi de confirmation de demande d'autorisation d'équipe",
			{
				from,
				to,
				cc,
				subject,
				html,
			}
		);
	}
}

// TODO mettre dans un service
async function correctionEnvoiEmailConfirmationAyantDroit() {
	const equipeDemandeAutorisations = await db.query.equipeDemandeAutorisation.findMany({
		where: isNull(equipeDemandeAutorisation.acceptedAt),
		with: {
			ayantDroit: true,
			equipeDemande: {
				with: {
					compteDemande: true,
				},
			},
		},
	});

	for (const eda of equipeDemandeAutorisations) {
		if (!eda.equipeDemande.compteDemande) {
			continue;
		}

		await envoiEmailConfirmationAyantDroit({
			ayantDroit: eda.ayantDroit,
			compteDemande: eda.equipeDemande.compteDemande,
			equipeDemande: eda.equipeDemande,
			token: eda.token,
		});
	}
}

export const controllerAdmin = {
	apiSireneUpdate(ctx: CtxUnidentified) {
		const tache = ctx.query.tache as string;
		const skipSirene = querystringToBoolean(ctx.query.skipSirene);
		const forceMiseAJourAt = querystringToDate(ctx.query.forceMiseAJourAt);

		const debut = ctx.query.debut ? Number(ctx.query.debut) : 0;
		const fin = ctx.query.fin ? Number(ctx.query.fin) : 0;
		const depuis = ctx.query.depuis ? Number(ctx.query.depuis) : 0;

		if (Number.isNaN(debut)) {
			ctx.throw(StatusCodes.BAD_REQUEST, `debut, nombre incorrect : ${debut}`);
		}

		if (Number.isNaN(fin)) {
			ctx.throw(StatusCodes.BAD_REQUEST, `fin, nombre incorrect : ${fin}`);
		}

		if (fin > debut) {
			ctx.throw(
				StatusCodes.BAD_REQUEST,
				`le nombre de jours de début doit être supérieur au nombre de jours de fin`
			);
		}

		const frequence = 'manuel';

		if (tache === 'etablissement-update-by-query') {
			const q = querystringToString(ctx.query.q);

			if (!q) {
				ctx.throw(StatusCodes.BAD_REQUEST, `paramètre obligatoire : q`);
			}

			const miseAJourAt = forceMiseAJourAt ? startOfDay(new Date(forceMiseAJourAt)) : new Date();

			// on fire and forget pour les tâches admin
			serviceApiSirene
				.etablissementUpdateByQuery({
					q,
					frequence,
					miseAJourAt,
					skipSirene,
				})
				.catch((e) => {
					console.logError(`[api admin] Erreur`, e);
				});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'entreprise-by-sirens-fix') {
			const sirens = querystringToArray(ctx.query.sirens);

			const sirenErrors = sirens.filter((s) => !isSiren(s));

			if (sirenErrors.length) {
				ctx.throw(
					StatusCodes.BAD_REQUEST,
					`apiSireneUpdate. Certains numéro ne sont pas des sirens ${sirenErrors.join(', ')}`
				);
			}

			// met à jour des entreprises et leurs établissements
			if (sirens.length) {
				serviceApiSirene
					.updateBySirens({
						sirens,
						frequence,
					})
					.catch((e) => {
						console.logError(`[api admin] Erreur`, e);
					});

				ctx.status = StatusCodes.OK;

				return;
			}
		}

		if (tache === 'api-entreprise-exercice-fix') {
			serviceApiEntreprise.exerciceFix().catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});
			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'bodacc-update') {
			serviceApiBodacc.update().catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});
			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'envoi-email-confirmation-ayant-droit') {
			correctionEnvoiEmailConfirmationAyantDroit().catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});
			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache && !serviceApiSirene.taches.includes(tache)) {
			console.logError('Tâche inconnue : ', tache);

			ctx.throw(StatusCodes.BAD_REQUEST, 'tâche inconnue');
		}

		void adminUpdateSirene({
			tache,
			debut,
			fin,
			depuis,
			skipSirene,
			forceMiseAJourAt,
		});
		ctx.status = StatusCodes.OK;
	},

	geocodeUpdate(ctx: CtxUnidentified) {
		// const tache = ctx.query.tache as string;

		serviceApiGeo.geocodeFix({ frequence: 'manuel' }).catch((e) => {
			console.logError(`[api admin] Erreur`, e);
		});

		ctx.status = StatusCodes.OK;
	},

	indexUpdate(ctx: CtxUnidentified) {
		const tache = ctx.query.tache as string;

		const forceMiseAJourAt = querystringToDate(ctx.query.forceMiseAJourAt);

		if (tache === 'qpv-index-init') {
			serviceElastic.qpvIndexInit().catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'zonage-index-init') {
			serviceElastic.zonageIndexInit().catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-init') {
			const resetIndexes = ['oui', 'true', 'o'].includes(
				querystringToString(ctx.query.resetIndexes) || 'non'
			);
			serviceElastic.etablissementIndexInit({ resetIndexes }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'local-index-init') {
			const resetIndexes = ['oui', 'true', 'o'].includes(
				querystringToString(ctx.query.resetIndexes) || 'non'
			);
			serviceElastic.localIndexInit({ resetIndexes }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'local-index-qpv-2024-update') {
			serviceElastic.localIndexQpv2024Update({ tacheNom: tache }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'local-index-qpv-2015-update') {
			serviceElastic.localIndexQpv2015Update({ tacheNom: tache }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'local-index-mapping-update') {
			serviceElastic.localIndexUpdateMapping().catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'local-index-equipe-update') {
			serviceElastic.localIndexEquipeUpdate({ tacheNom: tache }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-mapping-update') {
			serviceElastic.etablissementIndexUpdateMapping().catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-fermeture-update') {
			serviceElastic.etablissementIndexFermetureUpdate({ tacheNom: tache }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-effectif-update') {
			serviceElastic.etablissementIndexEffectifUpdate({ tacheNom: tache }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-procedure-collective-update') {
			const miseAJourAt = forceMiseAJourAt ? startOfDay(new Date(forceMiseAJourAt)) : undefined;

			serviceElastic
				.etablissementIndexProcedureCollectiveUpdate({
					tacheNom: tache,
					miseAJourAt,
				})
				.catch((e) => {
					console.logError(`[api admin] Erreur`, e);
				});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-procedure-update') {
			serviceElastic.etablissementIndexProcedureCollectiveUpdate({ tacheNom: tache }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-egapro-update') {
			serviceElastic.etablissementIndexEgaproUpdate({ tacheNom: tache }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-participation-update') {
			serviceElastic.etablissementIndexParticipationUpdate({ tacheNom: tache }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-ca-update') {
			serviceElastic.etablissementIndexCaUpdate({ tacheNom: tache }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-subvention-update') {
			serviceElastic.etablissementIndexSubventionUpdate({ tacheNom: tache }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-qpv-2024-update') {
			serviceElastic.etablissementIndexQpv2024Update({ tacheNom: tache }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-qpv-2015-update') {
			serviceElastic.etablissementIndexQpv2015Update({ tacheNom: tache }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-ess-update') {
			serviceElastic.etablissementIndexEssUpdate({ tacheNom: tache }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-micro-update') {
			serviceElastic.etablissementIndexMicroUpdate({ tacheNom: tache }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-adresse-fix') {
			serviceElastic
				.etablissementIndexAdresseSansEtalabCorrectFix({ tacheNom: tache })
				.catch((e) => {
					console.logError(`[api admin] Erreur`, e);
				});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache === 'etablissement-index-equipe-update') {
			serviceElastic.etablissementIndexEquipeUpdate({ tacheNom: tache }).catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

			ctx.status = StatusCodes.OK;

			return;
		}

		if (tache) {
			console.logError('Tâche inconnue : ', tache);

			ctx.throw(StatusCodes.BAD_REQUEST, 'tâche inconnue');
		}

		const resetIndexes = querystringToBoolean(ctx.query.resetIndexes);
		const dateDebut = querystringToDate(ctx.query.dateDebut);
		const dateFin = querystringToDate(ctx.query.dateFin);

		if (resetIndexes && !dateDebut && !dateFin) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'paramètres obligatoires : dateDebut, dateFin');
		}

		if (dateFin && dateDebut && dateDebut > dateFin) {
			ctx.throw(StatusCodes.BAD_REQUEST, `la date de début doit être antérieure à la date de fin`);
		}

		serviceElastic
			.indexUpdateByMiseAJourAt({
				tacheNom: 'etablissement-index-update',
				miseAJourAtDebut: dateDebut,
				miseAJourAtFin: dateFin,
				resetIndexes,
			})
			.catch((e) => {
				console.logError(`[api admin] Erreur`, e);
			});

		ctx.status = StatusCodes.OK;
	},

	async nouveauteAdd(ctx: CtxUnidentified) {
		const amount = Number(ctx.params.amount);

		if (Number.isNaN(amount)) {
			ctx.throw(StatusCodes.BAD_REQUEST, `nombre incorrect : ${amount}`);
		}

		await dbQueryAdmin.nouveauteAdd(amount);

		ctx.status = StatusCodes.OK;
	},

	async devecoGetMany(ctx: CtxAdmin) {
		const params = devecoGetManyParamsBuild(ctx.query);

		const [devecos, [{ count: total }]] = await Promise.all([
			devecoWithRelationsGetMany({
				...params,
			}),
			devecoCount({ ...params }),
		]);

		ctx.body = {
			total: total,
			page: params.page,
			pages: Math.ceil(total / params.elementsParPage),
			elements: devecos.map(utilisateurPourAdminFormat),
		};
	},

	async devecoAsCsvGetMany(ctx: CtxAdmin) {
		const params = devecoGetManyParamsBuild(ctx.query);

		const devecos = await devecoWithRelationsGetMany({
			...params,
			elementsParPage: 0,
		});

		const csvDevecos = devecos.map(({ compte, equipe }) => ({
			email: compte.email,
			nom: compte.nom,
			prenom: compte.prenom,
			equipe: equipe.nom,
			connexion_date: compte.connexionAt,
			authentification_date: compte.authAt,
		}));

		const header: { id: keyof (typeof csvDevecos)[0]; title: string }[] = [
			{ id: 'email', title: 'Email' },
			{ id: 'nom', title: 'Nom' },
			{ id: 'prenom', title: 'Prénom' },
			{ id: 'equipe', title: 'Équipe' },
			{ id: 'connexion_date', title: 'Dernière connexion' },
			{ id: 'authentification_date', title: 'Dernière authentification' },
		];
		const csvStringifier = createObjectCsvStringifier({ header });

		ctx.type = 'text/csv';
		ctx.body = csvStringifier.getHeaderString() + csvStringifier.stringifyRecords(csvDevecos);
	},

	async equipeAdd(ctx: CtxAdmin) {
		const {
			nom: nomRaw,
			equipeTypeId,
			territoireTypeId: territoireTypeIdRaw,
			territoireIds,
			siret: fuzzy,
		} = ctx.request.body as EquipeAddInput;

		const compteId = ctx.state?.compte?.id;
		if (!compteId) {
			ctx.throw(StatusCodes.UNAUTHORIZED, `Compte non trouvé`);
		}

		const nom = nomRaw?.trim();
		if (!nom) {
			ctx.throw(StatusCodes.BAD_REQUEST, `nom requis : "${nomRaw}"`);
		}

		const territoireTypeId = territoireTypeIdValidate(territoireTypeIdRaw);
		if (!territoireTypeId) {
			ctx.throw(
				StatusCodes.BAD_REQUEST,
				`le type du territoire est incorrect : ${territoireTypeIdRaw}`
			);
		}

		if (await equipeGetByNom({ nom })) {
			ctx.throw(StatusCodes.BAD_REQUEST, `une équipe avec ce nom existe déjà`);
		}

		const siret = isSiret(fuzzy) ? fuzzy.replace(/ /g, '') : null;

		const equipe = await serviceAdmin.equipeAdd({
			compteId,
			nom,
			equipeTypeId: equipeTypeId ? equipeTypeId : 'autre',
			territoireTypeId,
			territoireIds,
			siret,
		});

		ctx.body = equipe;

		return;
	},

	async equipeGetMany(ctx: CtxAdmin) {
		const equipes = await equipeWithTerritoireWithNomGetMany();

		ctx.body = equipes.map(formatEquipe).sort((a, b) => a.nom.localeCompare(b.nom));
	},

	async equipeDemandeGetMany(ctx: CtxAdmin) {
		const equipes = await dbQueryAdmin.equipeDemandeGetMany();

		ctx.body = equipes;
	},

	// TODO mettre dans un service
	async equipeDemandeAdd(ctx: CtxAdmin) {
		const compteId = ctx.state.compte.id;

		const { nom, territoireType, compte, autorisations } = ctx.request.body as EquipeDemandeInput;

		const [newEquipeDemande] = await db
			.insert(equipeDemande)
			.values({
				nom,
				compteId,
			})
			.returning();

		const [newCompteDemande] = await db
			.insert(compteDemande)
			.values({
				nom: compte.nom,
				prenom: compte.prenom,
				email: compte.email,
				fonction: compte.fonction,
				equipeDemandeId: newEquipeDemande.id,
			})
			.returning();

		for (const autorisation of autorisations) {
			const [newAyantDroit] = await db
				.insert(ayantDroit)
				.values(autorisation.ayantDroit)
				.returning();

			const token = uuidv4();
			const autorisationValue: InferInsertModel<typeof equipeDemandeAutorisation> = {
				token,
				equipeDemandeId: newEquipeDemande.id,
				ayantDroitId: newAyantDroit.id,
			};

			if (territoireType !== 'france') {
				switch (territoireType) {
					case 'commune':
						autorisationValue.communeId = autorisation.territoireId;
						break;
					case 'epci':
						autorisationValue.epciId = autorisation.territoireId;
						break;
					case 'petr':
						autorisationValue.petrId = autorisation.territoireId;
						break;
					case 'territoire_industrie':
						autorisationValue.territoireIndustrieId = autorisation.territoireId;
						break;
					case 'departement':
						autorisationValue.departementId = autorisation.territoireId;
						break;
					case 'region':
						autorisationValue.regionId = autorisation.territoireId;
						break;
					case 'metropole':
						break;
				}
			}

			if (autorisationValue) {
				await db.insert(equipeDemandeAutorisation).values(autorisationValue);
			}

			void envoiEmailConfirmationAyantDroit({
				ayantDroit: newAyantDroit,
				compteDemande: newCompteDemande,
				equipeDemande: newEquipeDemande,
				token,
			});
		}

		const equipes = await dbQueryAdmin.equipeDemandeGetMany();

		ctx.body = equipes;
	},

	async equipeDemandeCloture(ctx: CtxAdmin) {
		const equipeDemandeId = ctx.params.equipeDemandeId;

		await dbQueryAdmin.equipeDemandeUpdate({
			equipeDemandeId,
			partialEquipeDemande: {
				transformedAt: new Date(),
			},
		});

		const equipes = await dbQueryAdmin.equipeDemandeGetMany();

		ctx.body = equipes;
	},

	async compteAdd(ctx: CtxAdmin, next: (ctx: CtxAdmin) => Promise<unknown>) {
		const {
			nom,
			prenom,
			identifiant: identifiantRaw,
			email: emailRaw,
			// compteTypeId,
			equipeId,
		} = ctx.request.body as CompteAddInput;

		const identifiant = identifiantRaw?.trim().toLocaleLowerCase();

		if (!identifiant) {
			ctx.body = { error: `L'identifiant est requis` };
			return;
		}

		const email = emailRaw?.trim().toLocaleLowerCase();

		if (!email) {
			ctx.body = { error: `L'email est requis` };
			return;
		}

		if (!emailIsValid(email)) {
			ctx.body = { error: `L'email ${email} est invalide` };
			return;
		}

		const compteWithSameIdentifiant = await dbQueryAdmin.compteGetByIdentifiant({ identifiant });

		if (compteWithSameIdentifiant) {
			ctx.body = { error: `Un compte avec l'identifiant ${identifiant} existe déjà` };
			return;
		}

		// TODO : pour l'instant, impossible de créer un superadmin par l'interface
		// if (compteTypeId === 'superadmin') {
		// 	const compte = await serviceAdmin.compteAdd({
		// 		compteId,
		// 		email,
		// 		nom,
		// 		prenom,
		// 		typeId: 'superadmin',
		// 	});
		//
		// 	ctx.body = { compte };
		// 	return;
		// }

		if (!equipeId) {
			ctx.body = { error: `Un id d'équipe est requis pour ajouter un compte deveco` };
			return;
		}

		const equipe = await equipeGet({ equipeId });

		if (!equipe) {
			ctx.body = { error: `Aucune équipe avec cet id : ${equipeId}` };
			return;
		}

		const compteAdminId = ctx.state.compte.id;

		const compteCree = await serviceAdmin.compteAdd({
			compteId: compteAdminId,
			identifiant,
			email,
			nom,
			prenom,
			typeId: 'deveco',
		});

		const [deveco] = await devecoAdd({
			deveco: {
				compteId: compteCree.id,
				equipeId,
			},
		});

		const motDePasse = uuidv4();
		await dbQueryAdmin.motDePasseUpsert({
			compteId: compteCree.id,
			motDePasse,
		});

		await geoTokenAdd({
			token: uuidv4(),
			devecoId: deveco.id,
		});

		serviceBrevo.contactAdd(compteCree, equipe).catch((e) => {
			console.logError(
				`Impossible de créer un utilisateur sur une liste Brevo: ${e?.message ?? 'Erreur inconnue'}`
			);
		});

		await serviceAdmin.emailBienvenueSend(compteCree, motDePasse);

		return next(ctx);
	},

	async compteMotDePasseAdd(ctx: CtxAdmin, next: (ctx: CtxAdmin) => Promise<unknown>) {
		const {
			motDePasse,
			repetition,
		}: {
			motDePasse?: string;
			repetition?: string;
		} = ctx.request.body ?? {};

		const compteId = ctx.params.id;

		if (!compteId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `CompteId obligatoire`);
		}

		if (!motDePasse) {
			ctx.throw(StatusCodes.BAD_REQUEST, `motDePasse obligatoire`);
		}

		if (!repetition) {
			ctx.throw(StatusCodes.BAD_REQUEST, `repetition obligatoire`);
		}

		if (motDePasse !== repetition) {
			ctx.throw(StatusCodes.BAD_REQUEST, `motDePasse et repetition sont dfférents`);
		}

		if (motDePasse.length < 10) {
			ctx.throw(StatusCodes.BAD_REQUEST, `au moins 10 caractères`);
		}

		const compte = await dbQueryAdmin.compteGet({ compteId });

		if (!compte) {
			ctx.throw(StatusCodes.NOT_FOUND, `aucun compte avec cet id : ${compteId}`);
		}

		await dbQueryAdmin.motDePasseUpsert({
			compteId,
			motDePasse,
		});

		return next(ctx);
	},

	async compteActifUpdate(ctx: CtxAdmin, next: (ctx: CtxAdmin) => Promise<unknown>) {
		const { compteId, actif } = (ctx.request.body as CompteActifUpdateInput) ?? {};

		if (!compteId || typeof actif === 'undefined') {
			ctx.throw(StatusCodes.BAD_REQUEST, `Propriété "compteId" obligatoire`);
		}

		if (typeof actif === 'undefined') {
			ctx.throw(StatusCodes.BAD_REQUEST, `Propriété "actif" obligatoire`);
		}

		const compte = await dbQueryAdmin.compteGet({ compteId });

		if (!compte) {
			ctx.throw(StatusCodes.NOT_FOUND, `Aucun compte trouvé pour l'id ${compteId}`);
		}

		await serviceAdmin.compteUpdate({
			compteId,
			partialCompte: {
				actif,
			},
		});

		return next(ctx);
	},

	async etiquetteGetManyByEquipe(ctx: CtxAdmin) {
		const equipeId = Number(ctx.params.equipeId);

		if (!equipeId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `Propriété "equipeId" obligatoire`);
		}

		const etiquetteWithCounts = await etiquetteWithCountGetManyByEquipe({ equipeId });

		ctx.body = {
			etiquettes: etiquetteWithCountFormatMany({ etiquetteWithCounts }),
		};
	},

	async equipeEtiquetteRemoveMany(ctx: CtxAdmin, next: (ctx: CtxAdmin) => Promise<unknown>) {
		const equipeId = Number(ctx.params.equipeId);
		const { etiquetteIds: etiquetteIdsRaw } = ctx.request.body as EquipeEtiquetteRemoveManyInput;

		if (!etiquetteIdsRaw?.length) {
			ctx.throw(StatusCodes.BAD_REQUEST, `ids d'étiquettes obligatoires`);
		}

		const etiquetteIds = etiquetteIdsRaw.map(Number).filter(Boolean);

		if (etiquetteIds.length !== etiquetteIdsRaw.length) {
			ctx.throw(StatusCodes.BAD_REQUEST, `tous les ids d'étiquette doivent être des entiers`);
		}

		await etiquetteRemoveManyByEquipe({ etiquetteIds, equipeId });

		// met à jour l'index de recherche en supprimant l'étiquette de tous les documents établissement
		await elasticQuery.equipeEtiquetteRemoveMany({
			equipeId,
			etiquetteIds,
		});

		return next(ctx);
	},

	async rechercheEnTantQue(ctx: CtxAdmin) {
		const devecoId = Number(ctx.params.devecoId);

		const deveco = await devecoWithCompteGet({ devecoId });

		if (!deveco || deveco.compte.typeId !== 'deveco') {
			ctx.throw(StatusCodes.NOT_FOUND, `pas de deveco avec cet id : ${devecoId}`);
		}

		delete ctx.query.elastic;

		ctx.state = {
			...ctx.state,
			typeId: 'deveco' as const,
			deveco,
			compte: {
				...deveco.compte,
				typeId: 'deveco' as const,
			},
			equipe: deveco.equipe,
		} as any;

		await controllerEtablissements.getMany(ctx as unknown as CtxDeveco);
	},

	brevoUpdate(ctx: CtxUnidentified) {
		const equipeId = ctx.params.equipeId ? Number(ctx.params.equipeId) : undefined;

		void serviceBrevo.contactUpdateMany({ equipeId });

		ctx.body = {};
	},
};

export const UpdateNouveautesMiddleware = tokenMiddleware(
	process.env.NOUVEAUTES_UPDATE_TOKEN,
	'mise à jour du nombre de nouveautés'
);
