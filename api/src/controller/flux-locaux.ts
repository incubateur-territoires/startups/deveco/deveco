import { StatusCodes } from 'http-status-codes';

import { decode } from 'querystring';

import { dataStream } from './_stream';

import { CtxBase, CtxDeveco } from '../middleware/auth';
import * as dbQueryEquipe from '../db/query/equipe';
import * as dbQueryEquipeLocal from '../db/query/equipe-local';
import * as elastic from '../elastic/query/equipe-local';
import { serviceExcel } from '../service/excel';
import { serviceGeojson } from '../service/geojson';
import { localGetManyParamsBuild } from '../controller/locaux';

export function localParamsInit() {
	return {
		numero: null,
		nom: null,
		invariant: null,
		etages: null,
		natures: null,
		categories: null,
		page: null,
		stmax: null,
		stmin: null,
		svmax: null,
		svmin: null,
		srmax: null,
		srmin: null,
		semax: null,
		semin: null,
		ssncmax: null,
		ssncmin: null,
		sscmax: null,
		sscmin: null,
		vacance: null,
		contributionRemembre: null,
		contributionLoyerEurosMin: null,
		contributionLoyerEurosMax: null,
		contributionFondEurosMin: null,
		contributionFondEurosMax: null,
		contributionVenteEurosMin: null,
		contributionVenteEurosMax: null,
		contributionBailTypeId: null,
		contributionVacanceTypeId: null,
		contributionVacanceMotifTypeId: null,
		demandes: null,
		communes: null,
		tri: null,
		direction: null,
		aCommune: null,
		aVoie: null,
		aNumero: null,
		aSection: null,
		aParcelle: null,
		localisations: null,
		motsCles: null,
		accompanes: null,
		accompanesApres: null,
		accompanesAvant: null,
		proprio: null,
	};
}

export const controllerFluxLocaux = {
	async geojsonWithRechercheSauvegardeeTokenStreamMany(ctx: CtxBase) {
		const token = ctx.params.token;

		const rechercheSauvegardee = await dbQueryEquipe.rechercheSauvegardeeGetByToken({ token });
		if (!rechercheSauvegardee) {
			ctx.throw(
				StatusCodes.NOT_FOUND,
				`Aucune recherche sauvegardée correspondant au token "${token}"`
			);
		}

		const { equipeId, devecoId, query } = rechercheSauvegardee;

		const parsedQuery = decode(query);

		const params = localGetManyParamsBuild({
			query: parsedQuery,
			equipeId,
			devecoId,
		});

		const { fileOptions, dataGenerator } = serviceGeojson.localStreamBuild({
			params,
			format: 'geojson',
		});

		void dataStream({
			ctx,
			dataGenerator,
			fileOptions,
			headersOnly: ctx.method === 'HEAD',
		});

		return;
	},

	geojsonStreamMany(
		ctx: CtxDeveco,
		params: dbQueryEquipeLocal.EquipeLocalParams,
		format: 'geojson' | 'ndgeojson'
	) {
		const { fileOptions, dataGenerator } = serviceGeojson.localStreamBuild({
			params,
			format,
		});

		void dataStream({
			ctx,
			dataGenerator,
			fileOptions,
			headersOnly: ctx.method === 'HEAD',
		});

		return;
	},

	async xlsxStreamMany(ctx: CtxDeveco, params: dbQueryEquipeLocal.EquipeLocalParams) {
		const timestamp = Date.now();
		const filename = `deveco-export-locaux-${timestamp}.xlsx`;

		ctx.set('Content-disposition', `attachment; filename=${filename}`);
		ctx.set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		ctx.status = StatusCodes.OK;

		const localIdCursor = elastic.eqLocIdGetCursor(params);

		try {
			await serviceExcel.eqLocStreamMany({
				equipeId: params.equipeId,
				params,
				localIdCursor,
				output: ctx.res,
			});
		} catch (e) {
			console.logError(e);
		}
	},
};
