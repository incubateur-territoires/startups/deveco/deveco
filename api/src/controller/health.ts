import { StatusCodes } from 'http-status-codes';
import Koa from 'koa';
import { v4 as uuidv4 } from 'uuid';

import * as dbQueryAdmin from '../db/query/admin';

const version = uuidv4();

export const controllerHealth = {
	async check(ctx: Koa.Context) {
		const [count] = await dbQueryAdmin.comptesCount();

		if (!count) {
			ctx.throw(StatusCodes.SERVICE_UNAVAILABLE);
		}

		ctx.status = StatusCodes.OK;
		ctx.body = {
			status: 'OK',
			version,
		};
	},
};
