import {
	New,
	Compte,
	Contact,
	EtablissementCreationCreateur,
	Commune,
	EtablissementCreation,
	Etiquette,
	TerritoireTypeId,
	EquipeEtablissementContribution,
	EquipeLocalContribution,
	EtiquetteTypeId,
	NafType,
	Entreprise,
	CompteSimple,
	Demande,
	Echange,
	Etablissement,
	ModificationParCompteADate,
	NotUndefined,
	QueryResult,
	Rappel,
	RessourceEtablissement,
	EtalabEtablissementAdresse,
	Equipe,
	EquipeLocalContributionBailTypeId,
	EquipeLocalContributionVacanceMotifTypeId,
	EquipeLocalContributionVacanceTypeId,
	Local,
	LocalAdresseWithCommune,
	LocalCategorieType,
	LocalMutationNatureType,
	LocalNatureType,
	RessourceLocal,
	ContactOrigine,
	ProjetType,
	RessourceEtablissementCreation,
	SecteurActivite,
	SourceFinancement,
} from '../db/types';
import { contact } from '../db/schema/contact';
import { Position } from '../db/schema/custom-types/postgis';
import * as dbQueryEtablissement from '../db/query/etablissement';
import * as dbQueryEquipeEtablissement from '../db/query/equipe-etablissement';

// TYPE D'ENTREE

// TODO dupliquer ce type front / back
// car le front n'envoie pas un New<typeof contact> (naissanceDate: string)
export type ContactAddInput = {
	fonction?: string;
} & (
	| {
			type: 'existant';
			contactId: Contact['id'];
	  }
	| {
			type: 'nouveau';
			contact: New<typeof contact>;
	  }
);

export interface ContactUpdateInput {
	fonction?: string;
	contact: Partial<Contact>;
}

export type EtablissementCreationForNomBuild = Pick<EtablissementCreation, 'id' | 'enseigne'> & {
	creas: {
		ctct: Pick<Contact, 'nom' | 'prenom'>;
	}[];
};

interface EtablissementLie {
	id: Etablissement['siret'];
	nomAffichage: string;
}

export interface EtablissementCreationSimple {
	id: EtablissementCreation['id'];
	nom: string;
	etiquettes: {
		activites: Etiquette[];
	};
	futureEnseigne: string;
	demandes: Demande[];
	createurs: ContactAvecFonctions[];
	creationAbandonnee: EtablissementCreation['creationAbandonnee'];
}

export type EtablissementCreationPageComplete = EtablissementCreationSimple & {
	description: string | null;
	commentaire: string | null;
	etiquettes: Etiquettes;
	echanges: (Omit<Echange, 'devecoId'> &
		({ modification: ModificationParCompteADate | null } | null))[];
	rappels: Rappel[];
	demandes: Demande[];
	zonagesPrioritaires: string[];
	etablissement: EtablissementLie | null;
	modification: ModificationParCompteADate | null;
	communes: Commune[];
	contactOrigine: ContactOrigine | null;
	contactDate: Date | null;
	projetType: ProjetType | null;
	secteurActivite: SecteurActivite | null;
	sourceFinancement: SourceFinancement | null;
	formeJuridique: string;
	contratAccompagnement: boolean | null;
	rechercheLocal: boolean | null;
	ressources: RessourceEtablissementCreation[];
};

export interface CreateurInfos {
	niveauDiplome?: EtablissementCreationCreateur['diplomeNiveauId'];
	situationProfessionnelle?: EtablissementCreationCreateur['professionSituationId'];
}

export type CreateurAddInput = ContactAddInput & CreateurInfos;

export type CreateurUpdateInput = ContactUpdateInput & CreateurInfos;

export interface EtablissementCreationInput {
	contactOrigine: string;
	contactDate: string;
	createurs: CreateurAddInput[];
	projet: {
		projetType: EtablissementCreation['projetTypeId'];
		futureEnseigne: EtablissementCreation['enseigne'];
		description: EtablissementCreation['description'];
		secteurActivite: EtablissementCreation['secteurActiviteId'];
		formeJuridique: EtablissementCreation['categorieJuridiqueEnvisagee'];
		sourceFinancement: EtablissementCreation['sourceFinancementId'];
		contratAccompagnement: EtablissementCreation['contratAccompagnement'];
		activites: Etiquette['nom'][];
		mots: Etiquette['nom'][];
		localisations: Etiquette['nom'][];
		communes: Commune['id'][];
		rechercheLocal: EtablissementCreation['rechercheLocal'];
		commentaires: EtablissementCreation['commentaire'];
	};
}

export interface LoginBody {
	identifiant: Compte['identifiant'];
	motDePasse?: string;
	redirectUrl?: string;
}

export interface DescriptionInput {
	description: EquipeEtablissementContribution['description'];
}

export interface EquipeEtablissementEtiquetteInput {
	activites: Etiquette['nom'][];
	localisations: Etiquette['nom'][];
	motsCles: Etiquette['nom'][];
}

export interface EquipeLocalContributionInput {
	contribution: {
		nom: EquipeLocalContribution['nom'];
		remembre: EquipeLocalContribution['remembre'];
		loyerEuros: EquipeLocalContribution['loyerEuros'];
		venteEuros: EquipeLocalContribution['venteEuros'];
		fondsEuros: EquipeLocalContribution['fondsEuros'];
		description: EquipeLocalContribution['description'];
		bailTypeId: EquipeLocalContribution['bailTypeId'];
		vacanceTypeId: EquipeLocalContribution['vacanceTypeId'];
		vacanceMotifTypeId: EquipeLocalContribution['vacanceMotifTypeId'];
		occupe: EquipeLocalContribution['occupe'];
	};
}

export interface EquipeLocalEtiquetteInput {
	localisations: Etiquette['nom'][];
	motsCles: Etiquette['nom'][];
}

export interface EquipeDemandeInput {
	nom: string;
	territoireType: TerritoireTypeId;
	compte: {
		nom: string;
		prenom: string;
		email: string;
		fonction: string;
	};
	autorisations: {
		territoireId: string;
		ayantDroit: {
			nom: string;
			prenom: string;
			email: string;
			fonction: string;
		};
	}[];
}

export interface EquipeAddInput {
	nom: string;
	equipeTypeId?: string;
	territoireTypeId: string;
	territoireIds: string[];
	siret: string;
}

export interface CompteActifUpdateInput {
	compteId: Compte['id'];
	actif: boolean;
}

export interface CompteAddInput {
	nom: string;
	prenom: string;
	identifiant: string;
	email: string;
	// compteTypeId: CompteTypeId;
	equipeId?: number;
}

export interface EquipeEtiquetteRemoveManyInput {
	etiquetteIds: number[] | null;
}

export interface InfosUpdateInput {
	nom: string;
	prenom: string;
}

export interface JwtRedirectInput {
	redirectUrl?: string;
}

export interface DemandeAddInput {
	demande: {
		nom: string;
		typeId: string;
		description: string;
		brouillonId?: string;
		brouillonSuppression?: boolean;
	};
}

export interface DemandeUpdateInput {
	demande: {
		description: string;
		typeId: string;
		nom: string;
	};
}

export interface DemandeClotureInput {
	demande: {
		clotureMotif: string;
	};
}

export interface EchangeAddInput {
	echange: {
		nom: string;
		date: string;
		typeId: string;
		description: string;
		demandeIds: string[];
		brouillonId?: string;
		brouillonSuppression?: boolean;
	};
}

export interface EchangeUpdateInput {
	echange: {
		typeId: string;
		date: string;
		nom: string;
		description: string;
		demandeIds: string[];
	};
}

export interface RappelAddInput {
	rappel: {
		nom: string;
		date: string;
		affecteId: string;
		description: string;
		brouillonId?: string;
		brouillonSuppression?: boolean;
	};
}

export interface RappelUpdateInput {
	rappel: {
		nom?: string;
		date?: string;
		clotureDate?: string;
		affecteId?: string;
		description: string;
	};
}

export interface RappelClotureInput {
	cloture?: boolean;
}

export interface BrouillonAddInput {
	brouillon: {
		nom: string;
		date: string;
		description: string;
	};
}

export interface BrouillonUpdateInput {
	brouillon: {
		nom?: string;
		date?: string;
		clotureDate?: string;
		affecteId?: string;
		description: string;
	};
}

export interface BrouillonTransformationInput {
	brouillon: {
		etablissementId?: Etablissement['siret'];
		etabCreaId?: EtablissementCreation['id'];
		localId?: Local['id'];
	};
}

export interface MotDePasseUpdateInput {
	motDePasse?: string;
}

export interface RechercheEnregistreeAddInput {
	nom: string;
	url: string;
}

export interface RechercheEnregistreeRemoveInput {
	rechercheEnregistreeId: number;
}

export interface NotificationsUpdateInput {
	notifications: {
		connexion: boolean;
		rappels: boolean;
		activite: boolean;
	};
}

export interface FavoriUpdateInput {
	favori?: boolean;
}

export interface PartageInput {
	compteId: number;
	commentaire: string;
}

export interface ZonageGeometrieBuildInput {
	geojsonString: string;
}

export interface ZonageNomBuildInput {
	nom: string;
}

export interface ZonageDescriptionBuildInput {
	description: string;
}

export interface EquipeEtiquetteAddInput {
	type: EtiquetteTypeId;
	nom: string;
}

export interface EquipeEtiquetteEditInput {
	type: EtiquetteTypeId;
	nom: string;
}

export interface EtabCreaEtiquetteAddManyInput {
	activites: string[];
	localisations: string[];
	motsCles: string[];
	selection: number[];
}

export interface GeolocalisationUpdateInput {
	geolocalisation: Position;
}

export interface OccupantAddInput {
	siret?: string;
}

export interface EtabCreationCreateurUpdateInput {
	fonction: EtablissementCreationCreateur['fonction'];
	niveauDiplome: EtablissementCreationCreateur['diplomeNiveauId'];
	situationProfessionnelle: EtablissementCreationCreateur['professionSituationId'];
	contact: Partial<Contact>;
}

export interface EtabCreationCreationAbandonneeUpdateInput {
	creationAbandonnee?: boolean;
}

export interface ImportFileInput {
	nom: string;
	taille: number;
	derniereModification: string;
	contenu?: string;
}

export interface LookupManyInput {
	contenu?: string;
}

export interface LogMessageInput {
	statut?: number;
	message?: string;
	compteId?: number;
}
export interface EnseigneUpdateInput {
	enseigne?: string;
}

export interface ClotureDateUpdateInput {
	cloture?: boolean;
}

// TYPE DE SORTIE

export interface LiasseFiscaleFormatted {
	estDetenue: boolean | null;
	sirensMere: string[] | null;
	aFiliales: boolean | null;
	filiales: {
		nom: string;
		siren: string | null;
		pourcentage: number | null;
	}[];
	detentions: {
		nom: string;
		siren: string | null;
		pourcentage: number | null;
		parts: number | null;
	}[];
}

export interface MandatairePersonnePhysiqueFormatted {
	fonction: string | null;
	contact: Pick<
		Contact,
		'id' | 'prenom' | 'nom' | 'email' | 'telephone' | 'telephone2' | 'naissanceDate'
	>;
}

export interface MandatairePersonneMoraleFormatted {
	fonction: string | null;
	nom: string;
	siren: Entreprise['siren'];
}

export interface BeneficiaireEffectifFormatted {
	nom?: string | null;
	nomUsage?: string | null;
	prenoms?: string | null;
	naissanceDate?: Date | null;
	totalParts?: number | null;
}

export interface EffectifRcdFormatted {
	valeur: number;
	annee: number;
	mois: number;
}

export interface AdresseFormatted {
	numero: string | null;
	voieNom: string | null;
	codePostal: string | null;
	cedex: string | null;
	cedexNom: string | null;
	distributionSpeciale: string | null;
	geolocalisation: Position | null;
	etrangerCommune: string | null;
	etrangerPays: string | null;
	commune: {
		nom: string;
	} | null;
}

export interface EtablissementExtra {
	categorieNaf: string | null;
	categorieNafId: string | null;
	emms: EffectifRcdFormatted[];
	ema: EffectifRcdFormatted | null;
	zonages: string[];
	acv: boolean;
	pvd: boolean;
	va: boolean;
	subventions: {
		montant: string;
		objet: string;
		annee: string;
		type: string;
	}[];
	sireneTraitementDate: Date | null;
	enseigne: string | null;
}

export interface EntrepriseData {
	entrepriseId: string;
	entrepriseNom: string;
	siegeAdresse?: AdresseFormatted;
	formeJuridique: string | null;
	formeJuridiqueId: string | null;
	categorieJuridique: string | null;
	categorieJuridiqueId: string | null;
	exercices:
		| {
				ca: string;
				annee: string;
				date: string;
		  }[]
		| null;
	inpi: {
		ca: string;
		marge: string;
		ebe: string;
		resultatNet: string;
		annee: string;
	}[];
	inpiCaVariation: number | null;
	ess: boolean;
	micro: boolean;
	mandatairesPersonnesPhysiques: MandatairePersonnePhysiqueFormatted[] | null;
	mandatairesPersonnesMorales: MandatairePersonneMoraleFormatted[] | null;
	beneficiairesEffectifs: BeneficiaireEffectifFormatted[] | null;
	liasseFiscale: LiasseFiscaleFormatted | null;
	creationDate: Date | null;
	fermetureDate: Date | null;
	effectif: string | null;
	active: boolean;
	procedure: boolean;
	procedures: {
		date: string;
		annee: string;
		famille: string;
		nature: string;
	}[];
	categorieNaf: string | null;
	categorieNafId: string | null;
	categorieEntreprise: string | null;
	activiteNaf: NafType | null;
	etablissementsActifs: { territoire: number; france: number } | null;
	effectifTotalTerritoire: number | null;
	egapro: EgaproData | null;
}

export interface EgaproData {
	annee: number;
	note: number | null;
	ecartRemunerations: number | null;
	ecartAugmentations: number | null;
	retourMaternite: number | null;
	hautesRemunerations: number | null;
	cadres: number | null;
	instances: number | null;
}

export type EntrepriseParticipation = [string, string][];

export interface EtablissementWithPortefeuilleInfos {
	etablissement: EtablissementSimple;
	portefeuilleInfos: PortefeuilleInfos;
}

export type EtablissementPageComplete = EtablissementWithPortefeuilleInfos & {
	etablissementExtra: EtablissementExtra;
	entreprise: EntrepriseData | null;
	equipeEtablissement: EquipeEtablissementData;
};

export interface EquipeEtablissementData {
	description: string;
	etiquettes: Etiquettes;
	contacts: ContactAvecFonctions[];
	echanges: Omit<Echange, 'devecoId'>[];
	rappels: (Rappel & { affecte: CompteSimple | null })[];
	demandes: (Demande & { nombreEchanges: number })[];
	etabCreaTransformes: EtabCreaTransformes[];
	modification: ModificationParCompteADate | null;
	freres: EtablissementWithPortefeuilleInfos[];
	predecesseurs: EtablissementWithPortefeuilleInfos[];
	successeurs: EtablissementWithPortefeuilleInfos[];
	ressources: RessourceEtablissement[];
	occupations: LocalSimple[];
}

export interface EtabCreaTransformes {
	etabCreaId: EtablissementCreation['id'];
	nom: string;
}

export type EtablissementForNomBuild = Etablissement & {
	ul: Entreprise;
};

export type EtablissementWithRelations = NotUndefined<
	QueryResult<typeof dbQueryEtablissement.etablissementWithRelationsGet>
>;

export type EtablissementWithEquipeEtablissementRelations = NotUndefined<
	QueryResult<typeof dbQueryEquipeEtablissement.etablissementWithEqEtabWithRelationsGetMany>[0]
>;

export interface EntrepriseApercu {
	id: string;
	nomAffichage: string;
	actif: boolean;
}

export interface EtablissementApercu {
	id: string;
	nomAffichage: string;
	adresse1: AdresseFormatted & {
		ban: boolean;
	};
	actif: boolean;
	clotureDateContribution: Date | null;
	siege: boolean;
}

export type EtablissementSimple = EtablissementApercu & {
	nom: string | null;
	nomEntreprise: string | null;
	enseigneContribution: string | null;
	geolocalisationContribution: Position | null;
	geolocalisationEquipe: string | null;
	procedure: boolean;
	diffusible: boolean;
	creationDate: Date | null;
	fermetureDate: Date | null;
	inpiCaVariation: number | null;
	emaVariation: number | null;
	france: boolean;
	activiteNaf: NafType | null;
};

export type EtablissementForAdresseBuild = Etablissement & {
	geoloc: {
		geolocalisation: Position | null;
	} | null;
	ban: Pick<EtalabEtablissementAdresse, 'geolocalisation'> | null;
	commune: Pick<Commune, 'nom' | 'zrrTypeId'> | null;
};
export interface PortefeuilleInfos {
	contacts: boolean;
	echanges: boolean;
	demandes: boolean;
	rappels: boolean;
	favori: boolean;
}
export type EtiquetteWithCount = Etiquette & {
	equipeId: Equipe['id'];
	count?: number;
};
export type ContactLien =
	| {
			type: 'etablissement';
			etablissementId: string;
			etablissementNom: string | null;
			fonction: string | null;
	  }
	| {
			type: 'createur';
			etablissementCreationId: number;
			etablissementCreationNom: string | null;
	  }
	| {
			type: 'local';
			localId: string;
			localNom: string | null;
			fonction: string | null;
	  };

export interface ContactAvecFonctions {
	id: number;
	nom: string;
	prenom: string;

	telephone: string | null;
	telephone2: string | null;
	email: string | null;
	liens: ContactLien[];
	demarchageAccepte: boolean | null;
}

export interface Etiquettes {
	activites: EtiquetteWithCount[];
	localisations: EtiquetteWithCount[];
	motsCles: EtiquetteWithCount[];
} // Adresse : 19 Rue Charles de Gaulle 69210 L'Arbresle
// Numéro d’invariant : 100961112 (invar)

export interface LocalSimple {
	id: Local['id'];
	invariant: Local['invariant'];
	parcelle: Local['parcelle'] | null;
	section: Local['section'] | null;
	niveau: Local['niveau'] | null;
	surfaceTotale: string | null;
	adresse: LocalAdresseWithCommune | null;
	nom: EquipeLocalContribution['nom'] | null;
	occupe: EquipeLocalContribution['occupe'] | null;
	categorieType: LocalCategorieType | null;
	actif: boolean;
	geolocalisationContribution: EquipeLocalContribution['geolocalisation'] | null;
	geolocalisationEquipe: string | null;
	portefeuilleInfos: PortefeuilleInfos;
}
// Catégorie du local : MAG1 (typeact)
// Nature : Maison exceptionnelle (cconlctxt)
// Numéro de parcelle : 150 (dnupla)
// Section : AL (ccosec)
// Niveau étage : 0 (dniv)
// Surface de vente (P1) : 15m2 (sprincp)
// Surface de reserve (P2) : 44m2 (ssecp)
// Surface extérieur non couverte (P3) : 0m2 (ssecncp)
// Surface des espaces de stationnement couverts (PK1) : 44m2 (sparkp)
// Surface des espaces de stationnement non couverts (PK2) : 0m2 (sparkncp)
export type LocalComplet = LocalSimple &
	Pick<
		Local,
		| 'surfaceVente'
		| 'surfaceReserve'
		| 'surfaceExterieureNonCouverte'
		| 'surfaceStationnementNonCouvert'
		| 'surfaceStationnementCouvert'
	> & {
		natureType: LocalNatureType;
		propPPhys: {
			nom: string | null;
			prenom: string | null;
			prenoms: string | null;
			naissanceNom: string | null;
			naissanceDate: string | null;
			naissanceLieu: string | null;
			proprietaireCompteId: string;
		}[];
		propPMors: {
			nom: string | null;
			entrepriseId: string | null;
			proprietaireCompteId: string;
			siegeEtablissementId?: string;
		}[];
		mutations: {
			id: number;
			date: Date;
			valeur: number | null;
			natureType: LocalMutationNatureType;
		}[];
		contacts: ContactAvecFonctions[];
		occupants: EtablissementApercu[];
		ressources: RessourceLocal[];
		modification: ModificationParCompteADate | null;
		echanges: Omit<Echange, 'devecoId'>[];
		rappels: (Rappel & { affecte: CompteSimple | null })[];
		demandes: (Demande & { nombreEchanges: number })[];
		etiquettes: Etiquettes;
	} & LocalContribution;
interface LocalContribution {
	loyer: string | null;
	bailType: EquipeLocalContributionBailTypeId | null;
	vacanceType: EquipeLocalContributionVacanceTypeId | null;
	vacanceMotif: EquipeLocalContributionVacanceMotifTypeId | null;
	remembre: boolean | null;
	fonds: string | null;
	vente: string | null;
	commentaires: string | null;
}
export interface EquipeLocalForNomBuild {
	id: Local['id'];
	contributions: { nom: string | null }[];
}

export type EquipeLocalForAdresseBuild = {
	adresse: LocalAdresseWithCommune;
};
