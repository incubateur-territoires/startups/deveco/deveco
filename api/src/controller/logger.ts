import { StatusCodes } from 'http-status-codes';

import db from '../db';
import { LogMessageInput } from '../controller/types';
import { erreurFront } from '../db/schema/admin';
import { CtxDefault } from '../middleware/auth';

export const controllerLogger = {
	async logMessage(ctx: CtxDefault) {
		const { statut = 0, message = '', compteId } = ctx.request.body as LogMessageInput;
		if (!compteId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `[Logger] compteId obligatoire`);
		}

		const reqId = ctx.state?.reqId ?? 'reqId inconnu';

		console.logInfo(reqId, '[Logger] Erreur remontée par le front :', {
			statut,
			message,
			compteId,
		});

		const [{ id: erreurId }] = await db
			.insert(erreurFront)
			.values({
				compteId,
				message,
			})
			.returning({ id: erreurFront.id });

		console.logError(reqId, '[Logger] Erreur remontée par le front :', { erreurId });

		ctx.status = StatusCodes.OK;
		ctx.body = {};
	},
};
