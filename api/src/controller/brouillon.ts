import { StatusCodes } from 'http-status-codes';

import { CtxDeveco } from '../middleware/auth';
import { Evenement } from '../db/types';
import {
	BrouillonAddInput,
	BrouillonUpdateInput,
	BrouillonTransformationInput,
} from '../controller/types';
import { querystringToDate, querystringToString } from '../lib/querystring';
import * as dbQueryEvenement from '../db/query/evenement';
import * as dbQueryBrouillon from '../db/query/brouillon';
import * as dbQueryEtablissement from '../db/query/etablissement';
import * as dbQueryEtablissementCreation from '../db/query/etablissement-creation';
import * as dbQueryEquipeLocal from '../db/query/equipe-local';
import { brouillonFormat } from '../controller/_format/equipe';
import { serviceBrouillon } from '../service/brouillon';

export const controllerBrouillon = {
	async brouillonGetMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const brouillons = await dbQueryBrouillon.brouillonWithRelationsGetMany({
			equipeId,
		});

		ctx.body = {
			brouillons: brouillons.map(brouillonFormat),
		};
	},

	async brouillonAdd(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		// TODO utiliser une fonction pour parser la requête
		const {
			brouillon: { nom, date, description },
		} = ctx.request.body as BrouillonAddInput;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		const partialBrouillon = {
			equipeId,
			nom,
			date: date ? new Date(date) : new Date(),
			description: querystringToString(description) ?? null,
			creationCompteId: compteId,
		};

		const brouillon = await serviceBrouillon.brouillonAdd({
			evenementId: evenement.id,
			partialBrouillon,
		});

		return {
			evenementId: evenement.id,
			equipeId,
			brouillon,
		};
	},

	async brouillonUpdate(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const brouillonId = ctx.params.brouillonId ? parseInt(ctx.params.brouillonId) : null;
		if (!brouillonId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `brouillonId obligatoire`);
		}

		const oldBrouillon = await dbQueryBrouillon.brouillonGet({
			equipeId,
			brouillonId,
		});
		if (!oldBrouillon) {
			ctx.throw(StatusCodes.NOT_FOUND, `aucun brouillon avec cet id : ${brouillonId}`);
		}

		const {
			brouillon: { nom, date, description },
		} = ctx.request.body as BrouillonUpdateInput;

		const partialBrouillon = {
			nom,
			date: querystringToDate(date) ?? undefined,
			description: querystringToString(description) ?? null,
			modificationCompteId: compteId,
			modificationDate: new Date(),
		};

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		const brouillon = await serviceBrouillon.brouillonUpdate({
			evenementId: evenement.id,
			oldBrouillon,
			partialBrouillon,
		});

		return {
			evenementId: evenement.id,
			equipeId,
			oldBrouillon,
			brouillon,
		};
	},

	async brouillonTransformation(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const brouillonId = ctx.params.brouillonId ? parseInt(ctx.params.brouillonId) : null;
		if (!brouillonId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `brouillonId obligatoire`);
		}

		const {
			brouillon: { etablissementId, etabCreaId, localId },
		} = ctx.request.body as BrouillonTransformationInput;

		if (!etablissementId || !etabCreaId || !localId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `au moins un id d'entité est nécessaire`);
		}

		const oldBrouillon = await dbQueryBrouillon.brouillonGet({
			equipeId,
			brouillonId,
		});
		if (!oldBrouillon) {
			ctx.throw(StatusCodes.NOT_FOUND, `aucun brouillon avec cet id : ${brouillonId}`);
		}

		if (etablissementId) {
			const etablissement = await dbQueryEtablissement.etablissementGet({ etablissementId });
			if (!etablissement) {
				ctx.throw(StatusCodes.NOT_FOUND, `aucun établissement avec ce siret : ${etablissementId}`);
			}
		} else if (etabCreaId) {
			const etabCrea = await dbQueryEtablissementCreation.etabCreaCheck({
				equipeId,
				etabCreaId,
			});
			if (!etabCrea) {
				ctx.throw(StatusCodes.BAD_REQUEST, `création d'établissement inexistante : ${etabCreaId}`);
			}
		} else if (localId) {
			const local = await dbQueryEquipeLocal.localWithEquipeCommuneGet({
				equipeId,
				localId,
			});
			if (!local) {
				ctx.throw(StatusCodes.BAD_REQUEST, `Local ${localId} introuvable`);
			}
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		return {
			evenementId: evenement.id,
			equipeId,
			oldBrouillon,
		};
	},

	async brouillonRemove(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const brouillonId = ctx.params.brouillonId ? parseInt(ctx.params.brouillonId) : null;
		if (!brouillonId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `brouillonId obligatoire`);
		}

		const oldBrouillon = await dbQueryBrouillon.brouillonGet({
			equipeId,
			brouillonId,
		});
		if (!oldBrouillon) {
			ctx.throw(StatusCodes.NOT_FOUND, `aucun brouillon avec cet id : ${brouillonId}`);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		await serviceBrouillon.brouillonRemove({
			evenementId: evenement.id,
			brouillonId,
		});

		return {
			evenementId: evenement.id,
			equipeId,
			brouillonId,
		};
	},
};
