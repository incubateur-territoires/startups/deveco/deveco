import { StatusCodes } from 'http-status-codes';

import { ParsedUrlQuery } from 'node:querystring';

import {
	CreateurAddInput,
	EtabCreaEtiquetteAddManyInput,
	EtabCreationCreateurUpdateInput,
	EtabCreationCreationAbandonneeUpdateInput,
} from './types';

import { CtxDeveco } from '../middleware/auth';
import { querystringToArray, querystringToNumber, querystringToString } from '../lib/querystring';
import {
	demandeTypeIdValidate,
	Direction,
	fileFormatValidate,
	suiviEtablissementCreationValidate,
	TriEtablissement,
	Commune,
	EtablissementCreation,
	Etiquette,
	Contact,
	New,
	RessourceEtablissementCreation,
} from '../db/types';
import * as dbQueryEtablissementCreation from '../db/query/etablissement-creation';
import * as dbQueryContact from '../db/query/contact';
import * as dbQueryEvenement from '../db/query/evenement';
import { serviceEtablissementCreation } from '../service/equipe-etablissement-creation';
import * as controllerEquipe from '../controller/equipe';
import { controllerEtablissementCreationsSuivi } from '../controller/etablissement-creations-suivi';
import { controllerFluxEtablissementCreations } from '../controller/flux-etablissement-creations';
import {
	etabCreaFormat,
	etabCreaNomBuild,
	etabCreaApercuFormat,
	etabCreaSimpleFormat,
} from '../controller/_format/etablissement-creation';
import { serviceContact } from '../service/contact';
import { etabCreaRessource } from '../db/schema/etablissement-creation';

interface EtablissementCreationInput {
	contactOrigine: string;
	contactDate: string;
	createurs: CreateurAddInput[];
	projet: {
		projetType: EtablissementCreation['projetTypeId'];
		futureEnseigne: EtablissementCreation['enseigne'];
		description: EtablissementCreation['description'];
		secteurActivite: EtablissementCreation['secteurActiviteId'];
		formeJuridique: EtablissementCreation['categorieJuridiqueEnvisagee'];
		sourceFinancement: EtablissementCreation['sourceFinancementId'];
		contratAccompagnement: EtablissementCreation['contratAccompagnement'];
		activites: Etiquette['nom'][];
		mots: Etiquette['nom'][];
		localisations: Etiquette['nom'][];
		communes: Commune['id'][];
		rechercheLocal: EtablissementCreation['rechercheLocal'];
		commentaires: EtablissementCreation['commentaire'];
	};
}

export const controllerEtablissementCreationsParam = {
	async etabCreaCheck(
		etablissementCreateurId: string,
		ctx: CtxDeveco,
		next: (ctx: CtxDeveco) => Promise<unknown>
	) {
		const equipeId = ctx.state.deveco.equipeId;
		const etabCreaId = parseInt(etablissementCreateurId);

		if (!etabCreaId) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'Identifiant vide');
		}

		const etabCrea = await dbQueryEtablissementCreation.etabCreaCheck({
			equipeId,
			etabCreaId,
		});
		if (!etabCrea) {
			ctx.throw(StatusCodes.BAD_REQUEST, `création d'établissement inexistante : ${etabCreaId}`);
		}

		return next(ctx);
	},

	async etabCreaTransformationCheck(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const equipeId = ctx.state.deveco.equipeId;
		const etabCreaId = parseInt(ctx.params.etabCreaId);

		const etabCrea = await dbQueryEtablissementCreation.etabCreaCheck({
			equipeId,
			etabCreaId,
		});

		if (!etabCrea) {
			ctx.throw(StatusCodes.BAD_REQUEST, `création d'établissement inexistante : ${etabCreaId}`);
		}

		if (etabCrea.transfo) {
			ctx.throw(
				StatusCodes.BAD_REQUEST,
				`la création d'établissement ${etabCreaId} a été transformée et ne peut plus être modifiée`
			);
		}

		return next(ctx);
	},
};

const elementsParPage = 20;

function getManyParamsBuild(query: ParsedUrlQuery) {
	const {
		recherche,

		suivi,

		activites,
		localisations,
		motsCles,

		demandes,

		numero,
		rue,
		cp,
		qpvs,

		page,
		format,
		tri,
		direction,
	} = query;

	const parsedTri = ['creation', 'consultation', 'alphabetique'].includes(
		querystringToString(tri) ?? ''
	)
		? querystringToString(tri)
		: 'consultation';

	return {
		recherche: querystringToString(recherche),

		suivi: suiviEtablissementCreationValidate(querystringToString(suivi) ?? ''),

		activites: querystringToArray(activites),
		localisations: querystringToArray(localisations),
		motsCles: querystringToArray(motsCles),

		demandeTypeIds: querystringToArray(demandes)
			.map(demandeTypeIdValidate)
			.flatMap((d) => (d ? [d] : [])),

		numero: querystringToString(numero),
		rue: querystringToString(rue),
		codePostal: querystringToString(cp),
		qpvs: querystringToArray(qpvs),

		// pagination
		format: fileFormatValidate(querystringToString(format) ?? ''),
		elementsParPage,
		page: querystringToNumber(page) || 1,
		direction: (direction ?? 'desc') as Direction,
		tri: parsedTri as TriEtablissement,
	};
}

export const controllerEtablissementCreations = {
	async getById(ctx: CtxDeveco) {
		const query = ctx.query;
		const equipeId = ctx.state.deveco.equipeId;

		const etabCreaId = querystringToNumber(query.recherche);
		if (!etabCreaId) {
			// NOTE: on retourne une 200 avec un body pour que le front puisse l'afficher
			ctx.body = {
				error: `Id vide`,
			};
			return;
		}

		const etabCrea = await dbQueryEtablissementCreation.etabCreaWithRelationsGet({
			equipeId,
			etabCreaId,
		});
		if (!etabCrea) {
			// NOTE: on retourne une 200 avec un body pour que le front puisse l'afficher
			ctx.body = {
				error: `Aucune création d'établissement avec cet id : ${etabCreaId}`,
			};
			return;
		}

		ctx.status = StatusCodes.OK;
		ctx.body = {
			etabCrea: etabCreaApercuFormat(etabCrea),
		};
	},

	async viewMany(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const params = getManyParamsBuild(ctx.query);
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		if (params.format) {
			if (params.format === 'xlsx') {
				return await controllerFluxEtablissementCreations.xlsxStreamMany(ctx, {
					...params,
					equipeId,
				});
			}

			ctx.body = { error: `Le format d'export ${params.format} est invalide` };
			return;
		}

		return next(ctx);
	},

	async getMany(ctx: CtxDeveco) {
		const params = getManyParamsBuild(ctx.query);
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const { etabCreaIds, total } = await dbQueryEtablissementCreation.etabCreaIdWithTotalGetMany({
			...params,
			equipeId,
		});

		// TODO intégrer la vérification dans la fonction
		// si on arrive à satisfaire TypeScript
		const etabCreas = etabCreaIds.length
			? await dbQueryEtablissementCreation.etabCreaWithSimpleRelationsGetMany({
					etabCreaIds,
					equipeId,
				})
			: [];

		// we need to sort to respect the original sirets order, because the In() does not
		etabCreas.sort((e1, e2) => etabCreaIds.indexOf(e1.id) - etabCreaIds.indexOf(e2.id));

		ctx.status = StatusCodes.OK;
		ctx.body = {
			total,
			elements: etabCreas.map(etabCreaSimpleFormat),
			elementsParPage: params.elementsParPage,
			page: params.page,
			pages: Math.ceil(total / params.elementsParPage),
			direction: params.direction,
			tri: params.tri,
		};
	},

	async add(ctx: CtxDeveco) {
		const {
			contactOrigine,
			contactDate,
			createurs,
			projet: {
				projetType,
				futureEnseigne,
				description,
				secteurActivite,
				formeJuridique,
				sourceFinancement,
				contratAccompagnement,
				activites,
				mots,
				localisations,
				communes,
				rechercheLocal,
				commentaires,
			},
		} = ctx.request.body as EtablissementCreationInput;

		if (!createurs?.length) {
			ctx.throw("Au moins un créateur requis pour ajouter une création d'établissement");
		}

		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const devecoId = deveco.id;
		const compteId = deveco.compteId;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'etablissement_creation',
		});

		const etabCreaNew = await serviceEtablissementCreation.add({
			equipeId,
			compteId,
			devecoId,
			evenementId: evenement.id,
			etabCrea: {
				contactOrigineId: contactOrigine || null,
				contactDate: contactDate ? new Date(contactDate) : null,
				projetTypeId: projetType || null,
				enseigne: futureEnseigne,
				description,
				secteurActiviteId: secteurActivite || null,
				categorieJuridiqueEnvisagee: formeJuridique || null,
				sourceFinancementId: sourceFinancement || null,
				contratAccompagnement,
				commentaire: commentaires,
				rechercheLocal,
			},
			createurs: createurs.map((createur) => {
				if (createur.type === 'nouveau') {
					const { id, ...contactSansId } = createur.contact;

					contactSansId.naissanceDate = contactSansId.naissanceDate
						? new Date(contactSansId.naissanceDate)
						: null;

					return {
						...createur,
						contact: contactSansId,
					};
				}

				return createur;
			}),
			communes,
			activites,
			localisations,
			motsCles: mots,
		});

		ctx.body = {
			etablissementCreation: {
				id: etabCreaNew.id,
			},
		};
	},

	async view(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const etabCreaId = ctx.params.etabCreaId;

		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'etablissement_creation_affichage',
		});

		await serviceEtablissementCreation.view({
			evenementId: evenement.id,
			etabCreaId,
		});

		return next(ctx);
	},

	async get(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const etabCreaId = ctx.params.etabCreaId;

		const { etabCrea, qpvs } = await serviceEtablissementCreation.get({
			equipeId,
			etabCreaId,
		});

		if (!etabCrea) {
			return;
		}

		ctx.status = StatusCodes.OK;
		ctx.body = {
			etablissementCreation: etabCreaFormat(etabCrea, qpvs),
		};
	},

	async remove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const compteId = ctx.state.deveco.compteId;
		const etabCreaId = ctx.params.etabCreaId;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'etablissement_creation_suppression',
		});

		await serviceEtablissementCreation.remove({
			etabCreaId,
			evenementId: evenement.id,
		});

		ctx.status = StatusCodes.OK;

		return next(ctx);
	},

	async update(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const {
			contactOrigine,
			contactDate,
			projet: {
				projetType,
				futureEnseigne,
				description,
				secteurActivite,
				formeJuridique,
				sourceFinancement,
				contratAccompagnement,
				activites,
				mots,
				localisations,
				communes,
				rechercheLocal,
				commentaires,
			},
		} = ctx.request.body as EtablissementCreationInput;

		const etabCreaId = ctx.params.etabCreaId;

		const compteId = ctx.state.deveco.compteId;
		const equipeId = ctx.state.deveco.equipeId;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'etablissement_creation_modification',
		});

		const etabCrea = {
			contactOrigineId: contactOrigine?.trim() || null,
			contactDate: contactDate ? new Date(contactDate) : null,
			projetTypeId: projetType?.trim() || null,
			enseigne: futureEnseigne,
			description,
			secteurActiviteId: secteurActivite?.trim() || null,
			categorieJuridiqueEnvisagee: formeJuridique?.trim() || null,
			sourceFinancementId: sourceFinancement?.trim() || null,
			contratAccompagnement,
			commentaire: commentaires,
			rechercheLocal,
			modificationCompteId: compteId,
			modificationDate: new Date(),
		};

		await serviceEtablissementCreation.update({
			equipeId,
			evenementId: evenement.id,
			etabCreaId,
			etabCrea,
			communes,
			activites,
			localisations,
			motsCles: mots,
		});

		return next(ctx);
	},

	async etiquetteAddMany(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const compteId = deveco.compteId;

		const { activites, localisations, motsCles, selection } = ctx.request
			.body as EtabCreaEtiquetteAddManyInput;

		const params = getManyParamsBuild(ctx.query);

		// on veut récupérer tous les créateurs d'établissements
		params.elementsParPage = 0;

		const allEtablissementCreationIds = selection.length
			? selection
			: (
					await dbQueryEtablissementCreation.etabCreaIdGetManyUnordered({
						...params,
						equipeId,
					})
				).map(({ id }) => id);

		if (!allEtablissementCreationIds.length) {
			return next(ctx);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'etablissement_creation_qualification_de_masse',
		});

		//  ajoute les étiquettes sur les créateurs d'établissements
		await serviceEtablissementCreation.etiquetteAddMany({
			equipeId,
			etabCreaIds: allEtablissementCreationIds,
			activites,
			localisations,
			motsCles,
			evenementId: evenement.id,
		});

		return next(ctx);
	},

	async transformationAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const compteId = deveco.compteId;

		const etabCreaId = parseInt(ctx.params.etabCreaId);
		const etablissementId = ctx.params.etablissementId;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'etablissement_creation_transformation',
		});

		await serviceEtablissementCreation.transformationAdd({
			etabCreaId,
			etablissementId,
			equipeId,
			evenementId: evenement.id,
		});

		return next(ctx);
	},

	async transformationRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const compteId = deveco.compteId;

		const etabCreaId = parseInt(ctx.params.etabCreaId);

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'etablissement_creation_transformation',
		});

		await serviceEtablissementCreation.transformationRemove({
			etabCreaId,
			equipeId,
			evenementId: evenement.id,
		});

		return next(ctx);
	},

	async partage(ctx: CtxDeveco) {
		async function partageEtablissementCreation() {
			const etabCreaId = ctx.params.etabCreaId;
			const deveco = ctx.state.deveco;
			const equipeId = deveco.equipeId;

			const etabCrea = await dbQueryEtablissementCreation.etabCreaWithCreateurGet({
				equipeId,
				etabCreaId,
			});

			if (!etabCrea) {
				ctx.throw(
					StatusCodes.NOT_FOUND,
					`Aucune création d'établissement trouvée avec l'identifiant ${etabCreaId}`
				);
			}

			const descriptif = etabCreaNomBuild(etabCrea);

			const redirectUrl = `/createurs/${etabCreaId}`;

			// TODO créer un événement ?

			return { descriptif, redirectUrl };
		}

		await controllerEquipe.partageGenerique(ctx, partageEtablissementCreation);
	},

	async createurAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const body = ctx.request.body as CreateurAddInput;
		const { fonction, niveauDiplome, situationProfessionnelle, type } = body;
		let createurId: Contact['id'];

		if (!fonction && fonction !== '') {
			ctx.throw(StatusCodes.BAD_REQUEST, `fonction requise`);
		}

		if (type === 'existant' && !body.contactId) {
			ctx.throw(
				StatusCodes.BAD_REQUEST,
				`contactId requis pour ajouter un contact existant comme créateur`
			);
		}

		if (type === 'nouveau' && !body.contact) {
			ctx.throw(
				StatusCodes.BAD_REQUEST,
				`contact requis pour ajouter un nouveau contact comme créateur`
			);
		}

		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const etabCreaId = ctx.params.etabCreaId;

		const etabCreaCreateur = await dbQueryEtablissementCreation.etabCreaWithCreateurGet({
			equipeId,
			etabCreaId,
		});

		if (!etabCreaCreateur) {
			ctx.throw(StatusCodes.NOT_FOUND, `Etablissment #${etabCreaId} introuvable`);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'etablissement_creation_createur',
		});

		const evenementId = evenement.id;

		if (type === 'nouveau') {
			const { id, ...createurSansId } = body.contact;
			const newContact = await serviceContact.contactAdd({
				evenementId,
				equipeId,
				contact: {
					...createurSansId,
					equipeId,
					naissanceDate: body.contact.naissanceDate ? new Date(body.contact.naissanceDate) : null,
				},
			});

			createurId = newContact.id;
		} else {
			createurId = body.contactId;
		}

		if (createurId) {
			await serviceEtablissementCreation.createurAdd({
				evenementId,
				equipeId,
				etabCreaId,
				contactId: createurId,
				fonction,
				niveauDiplome,
				situationProfessionnelle,
			});
		}

		return next(ctx);
	},

	async createurUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		// TODO utiliser une fonction pour parser la requête
		const body = ctx.request.body as EtabCreationCreateurUpdateInput;
		const { fonction, niveauDiplome, situationProfessionnelle, contact } = body;

		const etabCreaId = ctx.params.etabCreaId;
		const createurId = ctx.params.createurId;

		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const oldContact = await dbQueryContact.contactGet({ contactId: createurId });
		if (!oldContact || oldContact.equipeId !== equipeId) {
			ctx.throw(StatusCodes.NOT_FOUND, `createur ${createurId} inexistant`);
		}

		// si on doit mettre à jour la fonction,
		// vérifier l'existence du createur dans lequel on met à jour l'information
		if (fonction) {
			const createurs = await dbQueryEtablissementCreation.etabCreaCreateurGetMany({
				etabCreaId,
			});

			if (!createurs.length) {
				ctx.throw(
					StatusCodes.NOT_FOUND,
					`etabCreaContact inexistant: createurId ${createurId}, equipeId ${equipeId}, etabCreaId ${etabCreaId}`
				);
			}
		}

		const { id, ...createurSansId } = contact;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'etablissement_creation_createur',
		});

		await serviceEtablissementCreation.createurUpdate({
			evenementId: evenement.id,
			equipeId,
			etabCreaId,
			contactId: createurId,
			fonction,
			niveauDiplome: niveauDiplome || null,
			situationProfessionnelle: situationProfessionnelle || null,
			partialContact: {
				...createurSansId,
				naissanceDate: contact.naissanceDate ? new Date(contact.naissanceDate) : null,
			},
			oldContact,
		});

		return next(ctx);
	},

	async createurRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const compteId = deveco.compteId;
		const createurId = parseInt(ctx.params.createurId);

		if (!createurId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `id de créateur requis`);
		}

		const etabCreaId = parseInt(ctx.params.etabCreaId);

		const createurs = await dbQueryEtablissementCreation.etabCreaCreateurGetMany({
			etabCreaId,
		});

		const createur = createurs.find((createur) => createur.contactId === createurId);
		if (!createur) {
			ctx.throw(StatusCodes.NOT_FOUND, `createur ${createurId} inexistant`);
		}

		if (createurs.length === 1) {
			ctx.throw(
				StatusCodes.BAD_REQUEST,
				`etabCreaContact: createurId ${createurId} ne peut être supprimé car c'est le seul contact associé, equipeId ${equipeId}, etabCreaId ${etabCreaId}`
			);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'etablissement_creation_createur',
		});

		const deletedCreateur = await serviceEtablissementCreation.createurRemove({
			evenementId: evenement.id,
			equipeId,
			etabCreaId,
			contactId: createurId,
		});

		if (!deletedCreateur) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'aucun contact avec ces ids');
		}

		return next(ctx);
	},
	async ressourceAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const equipeId = ctx.state.deveco.equipeId;
		const etabCreaId = ctx.params.etabCreaId;

		const partialRessource = ctx.request.body as New<typeof etabCreaRessource>;

		await serviceEtablissementCreation.ressourceAdd({
			equipeId,
			etabCreaId,
			partialRessource,
		});

		return next(ctx);
	},

	async ressourceUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const ressourceId = ctx.params.ressourceId;
		const partialRessource = ctx.request.body as Partial<RessourceEtablissementCreation>;

		await serviceEtablissementCreation.ressourceUpdate({ ressourceId, partialRessource });

		return next(ctx);
	},

	async ressourceRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const ressourceId = ctx.params.ressourceId;

		await serviceEtablissementCreation.ressourceRemove({ ressourceId });

		return next(ctx);
	},

	async creationAbandonneeUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const etabCreaId = ctx.params.etabCreaId;

		const compteId = ctx.state.deveco.compteId;
		const equipeId = ctx.state.deveco.equipeId;

		const { creationAbandonnee } = ctx.request.body as EtabCreationCreationAbandonneeUpdateInput;

		if (typeof creationAbandonnee === 'undefined') {
			ctx.throw(StatusCodes.BAD_REQUEST, 'creationAbandonnee requis');
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'etablissement_creation_modification',
		});

		await serviceEtablissementCreation.creationAbandonneeUpdate({
			equipeId,
			evenementId: evenement.id,
			etabCreaId,
			creationAbandonnee,
		});

		return next(ctx);
	},

	...controllerEtablissementCreationsSuivi,
};
