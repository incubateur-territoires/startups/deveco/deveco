import { eq } from 'drizzle-orm';
import { StatusCodes } from 'http-status-codes';

import { CtxUser } from '../middleware/auth';
import db from '../db';
import { equipeDemande, equipeDemandeAutorisation } from '../db/schema/admin';
import { EquipeDemande, EquipeDemandeAutorisation, QueryResult } from '../db/types';
import { emailSend, smtpCheck } from '../lib/email';
import { serviceEmailTemplatesAutorisation } from '../service/email-templates/autorisation';

const appUrl = process.env.APP_URL || '';

const formatEquipeDemandeForAutorisation = (
	eda: QueryResult<typeof equipeDemandeAutorisationGetByToken>
) => {
	if (!eda) {
		return;
	}

	const { equipeDemande, equipeDemandeId, ayantDroitId, ...autorisation } = eda;

	return {
		...equipeDemande,
		autorisations: [autorisation],
	};
};

const equipeDemandeAutorisationGetByToken = ({
	token,
}: {
	token: EquipeDemandeAutorisation['token'];
}) => {
	return db.query.equipeDemandeAutorisation.findFirst({
		where: eq(equipeDemandeAutorisation.token, token),
		with: {
			commune: true,
			epci: true,
			petr: true,
			territoireIndustrie: true,
			departement: true,
			region: true,
			equipeDemande: {
				columns: {
					id: true,
					nom: true,
					equipeId: true,
					createdAt: true,
					transformedAt: true,
				},
				with: {
					admin: {
						columns: {
							nom: true,
							prenom: true,
							email: true,
						},
					},
					compteDemande: {
						columns: {
							nom: true,
							prenom: true,
							email: true,
							fonction: true,
						},
					},
				},
			},
			ayantDroit: {
				columns: {
					nom: true,
					prenom: true,
					email: true,
					fonction: true,
				},
			},
		},
	});
};

function equipeDemandeGet({ equipeDemandeId }: { equipeDemandeId: EquipeDemande['id'] }) {
	return db.query.equipeDemande.findFirst({
		where: eq(equipeDemande.id, equipeDemandeId),
		with: {
			admin: {
				columns: {
					nom: true,
					prenom: true,
					email: true,
				},
			},
			compteDemande: true,
			autorisations: {
				columns: {
					acceptedAt: true,
				},
			},
		},
	});
}

const equipeDemandeAutorisationAccept = ({
	token,
}: {
	token: EquipeDemandeAutorisation['token'];
}) => {
	return db
		.update(equipeDemandeAutorisation)
		.set({ acceptedAt: new Date() })
		.where(eq(equipeDemandeAutorisation.token, token))
		.returning({ equipeDemandeId: equipeDemandeAutorisation.equipeDemandeId });
};

export const controllerAutorisation = {
	async equipeDemandeGet(ctx: CtxUser) {
		const token = ctx.params.token;

		const equipeDemandeAutorisation = await equipeDemandeAutorisationGetByToken({ token });

		const formatted = formatEquipeDemandeForAutorisation(equipeDemandeAutorisation);
		if (!formatted) {
			ctx.throw(StatusCodes.NOT_FOUND, `Impossible de trouver une demande pour le jeton ${token}`);
		}

		ctx.body = formatted;
	},

	async equipeDemandeAccept(ctx: CtxUser, next: (ctx: CtxUser) => Promise<unknown>) {
		const token = ctx.params.token;

		const [autorisation] = await equipeDemandeAutorisationAccept({ token });
		if (!autorisation) {
			ctx.throw(
				StatusCodes.NOT_FOUND,
				`Impossible de trouver une demande d'autorisation d'équipe pour le jeton ${token}`
			);
		}

		const equipeDemandeId = autorisation.equipeDemandeId;
		const equipeDemande = await equipeDemandeGet({ equipeDemandeId });
		if (!equipeDemande) {
			ctx.throw(
				StatusCodes.NOT_FOUND,
				`Aucune demande d'autorisation d'équipe en cours avec cet id #${equipeDemandeId}`
			);
		}

		const autorisationsEnAttente = equipeDemande.autorisations.filter(
			(autorisation) => autorisation.acceptedAt === null
		);

		if (autorisationsEnAttente.length > 0) {
			return next(ctx);
		}

		// Toutes les autorisations ont été acceptées
		// on envoie un email à l'admin pour prévenir

		const { admin, compteDemande } = equipeDemande;
		const requerant = {
			equipe: equipeDemande.nom,
			nom: compteDemande.nom,
			prenom: compteDemande.prenom,
		};

		const html = serviceEmailTemplatesAutorisation.equipeDemandePrete({
			appUrl,
			requerant,
			admin,
		});

		if (!smtpCheck()) {
			console.logInfo(
				`[equipeDemandeAccept] Envoi d'un email à l'admin ${admin.email}, HTML:`,
				html
			);
			return next(ctx);
		}

		try {
			await emailSend({
				to: admin.email,
				subject: `Une demande de création d'équipe est prête`,
				html,
			});
		} catch (error) {
			console.logError(`[equipeDemandeAccept]`, error);
		}

		return next(ctx);
	},
};
