import { StatusCodes } from 'http-status-codes';
import { endOfDay, startOfDay, subDays } from 'date-fns';
import { check } from '@placemarkio/check-geojson';
import { MultiPolygon, Position } from 'geojson';

import { ParsedUrlQuery } from 'querystring';

import {
	EquipeEtiquetteAddInput,
	EquipeEtiquetteEditInput,
	PartageInput,
	ZonageDescriptionBuildInput,
	ZonageGeometrieBuildInput,
	ZonageNomBuildInput,
} from './types';

import { CtxDeveco } from '../middleware/auth';
import { querystringToDate, querystringToNumber, querystringToString } from '../lib/querystring';
import { devecoIdentiteFormat } from '../lib/utils';
import { emailSend, smtpCheck } from '../lib/email';
import { longitudeValidate, latitudeValidate } from '../lib/geo';
import {
	NotUndefined,
	QueryResult,
	Equipe,
	Etiquette,
	Compte,
	situationGeographiqueValidate,
} from '../db/types';
import * as dbQueryEquipe from '../db/query/equipe';
import * as dbQueryRappel from '../db/query/rappel';
import * as dbQueryDemande from '../db/query/demande';
import * as dbQueryEchange from '../db/query/echange';
import * as dbQueryBrouillon from '../db/query/brouillon';
import * as dbQueryEvenement from '../db/query/evenement';
import * as dbQueryAdmin from '../db/query/admin';
import * as elasticQuery from '../elastic/query/equipe-etablissement';
import { serviceEmailTemplatesCompte } from '../service/email-templates/compte';
import { serviceEquipe } from '../service/equipe';
import { equipeEtiquetteUpsertMany } from '../service/equipe';
import { serviceStats } from '../service/stats';
import {
	etiquetteWithCountFormatMany,
	rappelFormat,
	demandeFormat,
	echangeFormat,
	brouillonFormat,
} from '../controller/_format/equipe';
import {
	rappelNouveauOuvertClotureCount,
	demandeNouvelleOuverteClotureCount,
} from '../controller/_format/stats';
import { dataStream } from '../controller/_stream';
import { controllerBrouillon } from './brouillon';

const appUrl = process.env.APP_URL || '';

const zonageGeojsonFormat = ({
	etiquette: { nom },
	geojson,
}: {
	etiquette: { nom: string };
	geojson: string;
}) => ({
	type: 'FeatureCollection',
	features: [
		{
			type: 'Feature',
			properties: {
				name: nom,
			},
			geometry: JSON.parse(geojson),
		},
	],
});

export function geoParamsBuild({
	equipeId,
	query,
}: {
	equipeId: Equipe['id'];
	query: ParsedUrlQuery;
}): dbQueryEquipe.GeoParams {
	const { geo } = query;

	return {
		equipeId,

		// geo
		geo: situationGeographiqueValidate(querystringToString(geo) ?? ''),
	};
}

export async function partageGenerique(
	ctx: CtxDeveco,
	descriptifRedirectUrlBuild: () => Promise<{ descriptif: string; redirectUrl: string }>
) {
	if (!smtpCheck()) {
		ctx.throw(StatusCodes.BAD_REQUEST, `Envoi d'email désactivé`);
	}

	const expediteur = ctx.state.compte;
	const equipeId = ctx.state.deveco.equipeId;

	const { compteId, commentaire } = ctx.request.body as PartageInput;

	const compte = await dbQueryAdmin.compteGet({ compteId });
	if (!compte) {
		ctx.throw(StatusCodes.NOT_FOUND, `Aucun destinataire trouvé avec le compteId ${compteId}`);
	}

	const deveco = await dbQueryEquipe.devecoGetByCompte({ compteId });
	if (!deveco) {
		ctx.throw(StatusCodes.NOT_FOUND, `Aucun deveco trouvé avec le compteId ${compteId}`);
	}

	if (deveco.equipeId !== equipeId) {
		ctx.throw(
			StatusCodes.NOT_FOUND,
			`Le deveco trouvé avec le compteId ${compteId} n'est pas dans l'équipe du deveco demandeur (${equipeId})`
		);
	}

	const { descriptif, redirectUrl } = await descriptifRedirectUrlBuild();

	try {
		await emailSend({
			to: compte.email,
			subject: `Deveco - Votre collaborateur souhaite vous partager la fiche de ${descriptif}`,
			html: serviceEmailTemplatesCompte.partage({
				url: { appUrl, redirectUrl },
				compte,
				expediteur,
				descriptif,
				commentaire,
			}),
		});
	} catch (error) {
		console.logError(error);
	}

	ctx.status = StatusCodes.OK;
	ctx.body = {};
}

function geojsonPositionsInvalidFilter(positions: Position[]): Position[] {
	return positions.filter((position) => {
		if (typeof position === 'undefined') {
			return false;
		}

		if (position === null) {
			return false;
		}

		// Un point est défini par ses coordonnées x,y et optionnellement z
		if (position.length < 2 || position.length > 3) {
			return false;
		}

		const [long, lat] = position;

		if (longitudeValidate(long) !== long && latitudeValidate(lat) !== lat) {
			return false;
		}

		return true;
	});
}

function geojsonPolygonInvalidFilter(rings: Position[][]): Position[][] {
	return rings.filter((ring) => {
		const coordinates = geojsonPositionsInvalidFilter(ring);

		if (coordinates.length === 0) {
			return false;
		}

		if (coordinates.length < 3) {
			return false;
		}

		return true;
	});
}

function geojsonMultiPolygonInvalidFilter(polygons: Position[][][]): Position[][][] {
	return polygons.filter((polygon) => {
		const rings = geojsonPolygonInvalidFilter(polygon);

		if (rings.length === 0) {
			return false;
		}

		return true;
	});
}

function zonageGeometrieBuild(ctx: CtxDeveco) {
	const { geojsonString } = (ctx.request.body as ZonageGeometrieBuildInput) ?? {};
	if (!geojsonString) {
		return null;
	}

	let geometrie;

	try {
		const geojson = check(geojsonString);

		const geometries =
			geojson.type === 'Feature'
				? [geojson.geometry]
				: geojson.type === 'FeatureCollection'
					? geojson.features.map((geojson) => geojson.geometry)
					: [geojson];

		// transforme tous les GeoJSON en un seul MultiPolygon
		const multiPolygon: MultiPolygon['coordinates'] = geometries.flatMap((geometry) => {
			if (geometry.type === 'LineString') {
				// on construit un Polygon à partir du premier point de la LineString
				const coordinates = geojsonPositionsInvalidFilter(geometry.coordinates);

				if (coordinates.length === 0) {
					return [];
				}

				if (coordinates.length < 3) {
					return [];
				}

				return [[[...coordinates, coordinates[0]]]];
			}

			if (geometry.type === 'Polygon') {
				const rings = geojsonPolygonInvalidFilter(geometry.coordinates);

				if (rings.length === 0) {
					return [];
				}

				return [rings];
			}

			if (geometry.type === 'MultiPolygon') {
				const polygons = geojsonMultiPolygonInvalidFilter(geometry.coordinates);

				if (polygons.length === 0) {
					return [];
				}

				return geometry.coordinates;
			}

			return [];
		});

		const position = multiPolygon?.[0]?.[0]?.[0];
		if (!Array.isArray(position)) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'Le GeoJSON ne contient aucune forme géographique valide');
		}

		const str = multiPolygon
			.map((polygon) => {
				if (!Array.isArray(polygon[0])) {
					ctx.throw(StatusCodes.BAD_REQUEST, 'Polygone invalide');
				}

				const tout = polygon
					.map((part) => part.map(([lat, lon]) => `${lat} ${lon}`).join(','))
					.join('),(');

				return `(${tout})`;
			})
			.join('),(');

		geometrie = `MULTIPOLYGON((${str}))`;
	} catch (e: any) {
		ctx.throw(StatusCodes.BAD_REQUEST, `JSON invalide : ${e.message}`);
	}

	return geometrie;
}

function zonageNomBuild(ctx: CtxDeveco) {
	const { nom: nomRaw } = (ctx.request.body as ZonageNomBuildInput) ?? {};

	const nom = nomRaw?.trim();

	if (!nom) {
		ctx.throw(StatusCodes.BAD_REQUEST, `nom requis`);
	}

	return nom;
}

function zonageDescriptionBuild(ctx: CtxDeveco) {
	const { description: descriptionRaw } = (ctx.request.body as ZonageDescriptionBuildInput) ?? {};

	return descriptionRaw?.trim() ?? null;
}

function formatCompteForDeveco(compte: Pick<Compte, 'id' | 'prenom' | 'nom' | 'email' | 'actif'>) {
	return {
		id: compte.id,
		prenom: compte.prenom,
		nom: compte.nom,
		email: compte.email,
		actif: compte.actif,
	};
}

export const controllerEquipe = {
	async devecoGetMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;

		const comptes = await dbQueryEquipe.devecoWithCompteColumnsGetManyByEquipe({
			equipeId: deveco.equipeId,
		});

		ctx.body = {
			devecos: comptes.map(formatCompteForDeveco),
		};
	},

	async etablissementStatsGet(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const finRaw = querystringToDate(ctx.query.fin);
		const fin = finRaw ? endOfDay(finRaw) : new Date();
		const debut = querystringToDate(ctx.query.debut) ?? subDays(startOfDay(fin), 7);

		const {
			eqEtabRappels,
			eqEtabDemandes,
			eqEtabCounts,
			echangeWithRelations,
			evenementEquipeEtablissementAffichages,
			eqEtabWithContacts,
			evenementEtiquetteEtablissements,
		} = await serviceStats.eqEtabStatsGet({
			equipeId,
			debut,
			fin,
		});

		ctx.body = {
			etablissements: eqEtabCounts,
			rappels: rappelNouveauOuvertClotureCount({
				rappels: eqEtabRappels.map(({ rappel }) => rappel),
				debut,
				fin,
			}),
			echanges: echangeWithRelations.map((echange) => ({
				auteur: devecoIdentiteFormat(echange.deveco),
				type: echange.typeId,
			})),
			demandes: demandeNouvelleOuverteClotureCount({
				demandes: eqEtabDemandes.map(({ demande }) => demande),
				debut,
				fin,
			}),
			actions: {
				vues: evenementEquipeEtablissementAffichages.length,
				qualifications: eqEtabWithContacts.length,
				etiquetages: evenementEtiquetteEtablissements.length,
			},
		};
	},

	async etabCreaStatsGet(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const finRaw = querystringToDate(ctx.query.fin);
		const fin = finRaw ? endOfDay(finRaw) : new Date();
		const debut = querystringToDate(ctx.query.debut) ?? subDays(startOfDay(fin), 7);

		const {
			etabCreaRappels,
			etabCreaDemandes,
			etabCreaWithQpv,
			etabCreaTransformation,
			echangeWithRelations,
			actionEtablissementCreationAffichages,
			actionEtiquetteEtablissementCreations,
		} = await serviceStats.etabCreaStatsGet({
			equipeId,
			debut,
			fin,
		});

		ctx.body = {
			createurs: {
				nouveaux: etabCreaWithQpv.length,
				qpvs: etabCreaWithQpv.filter((e) => !!e.qpv).length,
				transformes: etabCreaTransformation.length,
			},
			rappels: rappelNouveauOuvertClotureCount({
				rappels: etabCreaRappels.map(({ rappel }) => rappel),
				debut,
				fin,
			}),
			echanges: echangeWithRelations.map((echange) => ({
				auteur: devecoIdentiteFormat(echange.deveco),
				type: echange.typeId,
			})),
			demandes: demandeNouvelleOuverteClotureCount({
				demandes: etabCreaDemandes.map(({ demande }) => demande),
				debut,
				fin,
			}),
			actions: {
				vues: actionEtablissementCreationAffichages[0]?.count,
				qualifications: 0,
				etiquetages: actionEtiquetteEtablissementCreations[0]?.count,
			},
		};
	},

	async localStatsGet(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const finRaw = querystringToDate(ctx.query.fin);
		const fin = finRaw ? endOfDay(finRaw) : new Date();
		const debut = querystringToDate(ctx.query.debut) ?? subDays(startOfDay(fin), 7);

		const {
			echangeWithRelations,
			localDemandes,
			localRappels,
			localContactsProprio,
			eqLocsWithAffichages,
			eqLocsOccupes,
			eqLocsVacants,
			actionEtiquetteLocal,
		} = await serviceStats.eqLocStatsGet({
			equipeId,
			debut,
			fin,
		});

		ctx.body = {
			locaux: {
				occupes: eqLocsOccupes ? Number(eqLocsOccupes) : 0,
				vacants: eqLocsVacants ? Number(eqLocsVacants) : 0,
				contacts: localContactsProprio.length,
			},
			rappels: rappelNouveauOuvertClotureCount({
				rappels: localRappels.map(({ rappel }) => rappel),
				debut,
				fin,
			}),
			echanges: echangeWithRelations.map((echange) => ({
				auteur: devecoIdentiteFormat(echange.deveco),
				type: echange.typeId,
			})),
			demandes: demandeNouvelleOuverteClotureCount({
				demandes: localDemandes.map(({ demande }) => demande),
				debut,
				fin,
			}),
			actions: {
				vues: eqLocsWithAffichages.length,
				qualifications: 0,
				etiquetages: actionEtiquetteLocal.length,
			},
		};
	},

	async etiquetteGetMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const etiquetteWithCounts = await dbQueryEquipe.etiquetteWithCountGetManyByEquipe({ equipeId });

		ctx.body = {
			etiquettes: etiquetteWithCountFormatMany({ etiquetteWithCounts }),
		};
	},

	async etiquetteGetManyByType(ctx: CtxDeveco) {
		const equipeId = ctx.state.deveco.equipeId;
		const etiquetteTypeId = ctx.params.etiquetteTypeId;
		const nom = ctx.query.recherche as string;

		const etiquettes = await dbQueryEquipe.etiquetteGetManyByTypeAndNom({
			equipeId,
			etiquetteTypeId,
			nom,
		});

		ctx.status = StatusCodes.OK;
		ctx.body = {
			elements: etiquettes,
		};
	},

	async etiquetteRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const compteId = deveco.compteId;

		const etiquetteIdRaw = ctx.params.etiquetteId;

		const etiquetteId = querystringToNumber(etiquetteIdRaw);
		if (!etiquetteId) {
			ctx.throw(StatusCodes.BAD_REQUEST, "Numéro d'étiquette invalide: ${etiquetteIdRaw}");
		}

		const etiquette = await dbQueryEquipe.etiquetteGet({ etiquetteId });
		if (!etiquette || etiquette.equipeId !== equipeId) {
			return next(ctx);
		}

		await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: `etiquette_suppression`,
		});

		await dbQueryEquipe.etiquetteRemoveManyByEquipe({
			etiquetteIds: [etiquetteId],
			equipeId,
		});

		// met à jour l'index de recherche en supprimant l'étiquette de tous les documents établissement
		await elasticQuery.equipeEtiquetteRemoveMany({
			equipeId,
			etiquetteIds: [etiquetteId],
		});

		// Note : si l'étiquette est liée à un zonage, le zonage sera supprimé par casade

		return next(ctx);
	},

	async rappelGetMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const rappels = await dbQueryRappel.rappelWithRelationsGetMany({
			equipeId,
		});

		ctx.body = {
			rappels: rappels.map(rappelFormat),
		};
	},

	async suiviGetMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const rappels = await dbQueryRappel.rappelWithRelationsGetMany({
			equipeId,
		});

		const demandes = await dbQueryDemande.demandeWithRelationsGetMany({
			equipeId,
			limit: 20,
		});

		const echanges = await dbQueryEchange.echangeWithRelationsGetMany({
			equipeId,
			limit: 20,
		});

		const brouillons = await dbQueryBrouillon.brouillonWithRelationsGetMany({
			equipeId,
		});

		ctx.body = {
			rappels: rappels.map(rappelFormat),
			demandes: demandes.map((demande) => {
				const { echanges } = demande;

				return {
					...demandeFormat(demande),
					nombreEchanges: echanges.length,
				};
			}),
			echanges: echanges.map(echangeFormat),
			brouillons: brouillons.map(brouillonFormat),
		};
	},

	async etiquetteAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const { type, nom: nomRaw } = ctx.request.body as EquipeEtiquetteAddInput;

		const nom = nomRaw.trim();

		const etiquette = await dbQueryEquipe.etiquetteGetByTypeAndNom({
			equipeId,
			etiquetteTypeId: type,
			nom,
		});
		if (etiquette) {
			ctx.throw(StatusCodes.BAD_REQUEST, `l'étiquette ${nom} de type ${type} existe déjà`);
		}

		const activites = type === 'activite' ? [nom] : [];
		const localisations = type === 'localisation' ? [nom] : [];
		const motsCles = type === 'mot_cle' ? [nom] : [];

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: `etiquette_creation`,
		});

		await equipeEtiquetteUpsertMany({
			evenementId: evenement.id,
			equipeId,
			activites,
			localisations,
			motsCles,
		});

		return next(ctx);
	},

	async etiquetteEdit(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const etiquetteId: Etiquette['id'] = ctx.params.etiquetteId;

		const etiquette = await dbQueryEquipe.etiquetteGet({ etiquetteId });

		if (!etiquette || etiquette.equipeId !== equipeId) {
			ctx.throw(StatusCodes.NOT_FOUND, `l'étiquette #${etiquetteId} n'existe pas`);
		}

		const { nom: nomRaw } = ctx.request.body as EquipeEtiquetteEditInput;

		const nom = nomRaw.trim();

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: `etiquette_modification`,
		});

		await serviceEquipe.etiquetteUpdate({
			evenementId: evenement.id,
			etiquetteId,
			nouveauNom: nom,
		});

		return next(ctx);
	},

	async zonageAdd(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const nom = zonageNomBuild(ctx);

		const geometrie = zonageGeometrieBuild(ctx);
		if (!geometrie) {
			ctx.throw(StatusCodes.BAD_REQUEST, `geojsonString requis`);
		}

		const description = zonageDescriptionBuild(ctx);

		const etiquette = await dbQueryEquipe.etiquetteGetByTypeAndNom({
			equipeId,
			etiquetteTypeId: 'localisation',
			nom,
		});
		if (etiquette) {
			ctx.throw(StatusCodes.BAD_REQUEST, `l'étiquette de localisation ${nom} existe déjà`);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'zonage_creation',
		});

		await serviceEquipe.zonageAdd({
			evenementId: evenement.id,
			equipeId,
			nom,
			description,
			geometrie,
		});

		ctx.body = {};
	},

	async zonageUpdate(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const compteId = deveco.compteId;

		const geometrie = zonageGeometrieBuild(ctx);

		const description = zonageDescriptionBuild(ctx);

		const zonageId = Number(ctx.params.zonageId);

		const zonage = await dbQueryEquipe.zonageWithEtiquetteGet({ zonageId });
		if (!zonage || zonage.equipeId !== equipeId) {
			ctx.throw(StatusCodes.NOT_FOUND, `le zonage #${zonageId} n'existe pas`);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'zonage_modification',
		});

		await serviceEquipe.zonageUpdate({
			evenementId: evenement.id,
			equipeId,
			zonageId,
			description,
			geometrie,
		});

		ctx.body = {};
	},

	async zonageFichierGet(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const zonageId = Number(ctx.params.zonageId);

		const zonage = await dbQueryEquipe.zonageWithEtiquetteGet({ zonageId });
		if (!zonage || zonage.equipeId !== equipeId) {
			ctx.throw(StatusCodes.NOT_FOUND, `le zonage #${zonageId} n'existe pas`);
		}

		function* gen(z: NotUndefined<QueryResult<typeof dbQueryEquipe.zonageWithEtiquetteGet>>) {
			yield JSON.stringify(zonageGeojsonFormat(z), null, 0);
		}

		void dataStream({
			ctx,
			dataGenerator: gen(zonage),
			fileOptions: {
				filename: `${zonage.etiquette.nom.toLocaleLowerCase().replace(/ /g, '-')}.geojson`,
				mimetype: 'application/geo+json',
			},
		});
	},

	async zonageGetMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const zonages = await dbQueryEquipe.zonageGetManyByEquipe({ equipeId });

		ctx.body = zonages.map(({ id, description, etiquette: { nom }, enCours }) => ({
			id,
			nom,
			description: description ?? '',
			enCours,
		}));
	},

	async zonageRemove(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const compteId = deveco.compteId;

		const zonageId = Number(ctx.params.zonageId);

		const zonage = await dbQueryEquipe.zonageWithEtiquetteGet({ zonageId });
		if (!zonage || zonage.equipeId !== equipeId) {
			ctx.throw(StatusCodes.NOT_FOUND, `le zonage #${zonageId} n'existe pas`);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'zonage_suppression',
		});

		await serviceEquipe.zonageRemove({
			evenementId: evenement.id,
			equipeId,
			etiquetteId: zonage.etiquetteId,
			zonageId,
		});

		ctx.body = {};
	},

	async brouillonAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		await controllerBrouillon.brouillonAdd(ctx, 'brouillon_creation');

		return next(ctx);
	},

	async brouillonUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		await controllerBrouillon.brouillonUpdate(ctx, 'brouillon_modification');

		return next(ctx);
	},

	async brouillonRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		await controllerBrouillon.brouillonRemove(ctx, 'brouillon_suppression');

		return next(ctx);
	},
};
