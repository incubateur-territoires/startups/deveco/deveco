import { StatusCodes } from 'http-status-codes';

import { decode } from 'querystring';

import { dataStream } from './_stream';
import { etablissementGetManyParamsBuild } from './etablissements';

import { CtxBase, CtxDeveco } from '../middleware/auth';
import { Equipe } from '../db/types';
import * as dbQueryEquipe from '../db/query/equipe';
import * as dbQueryEquipeEtablissement from '../db/query/equipe-etablissement';
import * as elasticQueryEquipeEtablissement from '../elastic/query/equipe-etablissement';
import { serviceExcel } from '../service/excel';
import { serviceGeojson } from '../service/geojson';

export function etablissementParamsInit({
	equipeId,
}: {
	equipeId: Equipe['id'];
}): dbQueryEquipeEtablissement.EtablissementsParams {
	return {
		// public
		recherche: null,
		etat: [],
		geoloc: [],
		categoriesJuridiques: [],
		categoriesJuridiquesSans: [],
		categorieJuridiqueCourante: null,
		codesNaf: [],
		codesNafSans: [],
		effectifInconnu: null,
		effectifMin: null,
		effectifMax: null,
		effectifVarTypes: [],
		effectifVarMin: null,
		effectifVarMax: null,

		ess: null,
		micro: null,
		siege: null,
		cas: [],
		caVarTypes: [],
		caVarMin: null,
		caVarMax: null,
		subventions: [],
		subAnneeMin: null,
		subAnneeMax: null,
		subMontantMin: null,
		subMontantMax: null,
		procedure: null,
		procApres: null,
		procAvant: null,
		filiales: [],
		detentions: [],
		creeApres: null,
		creeAvant: null,
		fermeApres: null,
		fermeAvant: null,
		entrepriseTypes: [],
		egaproIndiceMin: null,
		egaproIndiceMax: null,
		egaproCadresMin: null,
		egaproCadresMax: null,
		egaproInstancesMin: null,
		egaproInstancesMax: null,
		// adresse
		cp: null,
		numero: null,
		rue: null,
		// geoloc
		longitude: null,
		latitude: null,
		rayon: null,
		nwse: [],
		z: null,
		// territoire
		qpvs: [],
		zrrs: [],
		acv: null,
		pvd: null,
		va: null,
		afrs: [],
		territoireIndustries: [],
		communes: [],
		epcis: [],
		departements: [],
		regions: [],

		// perso
		equipeId,
		devecoId: null,
		demandeTypeIds: [],
		// etiquettes
		activites: [],
		activitesToutes: false,
		localisations: [],
		localisationsToutes: false,
		motsCles: [],
		motsClesTous: false,
		// geo
		geo: null,
		// suivi
		favori: null,
		accompagnes: [],
		accompagnesApres: null,
		accompagnesAvant: null,
		avecContact: null,
		ancienCreateur: null,

		// pagination
		elementsParPage: null,
		page: null,
		direction: null,
		tri: null,
	};
}

export const controllerFluxEtablissements = {
	async geojsonWithRechercheSauvegardeeTokenStreamMany(ctx: CtxBase) {
		const token = ctx.params.token;

		const rechercheSauvegardee = await dbQueryEquipe.rechercheSauvegardeeGetByToken({ token });
		if (!rechercheSauvegardee) {
			ctx.throw(
				StatusCodes.NOT_FOUND,
				`Aucune recherche sauvegardée correspondant au token "${token}"`
			);
		}

		const { query, equipeId, devecoId } = rechercheSauvegardee;

		const parsedQuery = decode(query);

		const params = etablissementGetManyParamsBuild({
			query: parsedQuery,
			equipeId,
			devecoId,
		});

		const { fileOptions, dataGenerator } = serviceGeojson.etablissementStreamBuild({
			params,
			format: 'geojson',
		});

		void dataStream({
			ctx,
			dataGenerator,
			fileOptions,
			headersOnly: ctx.method === 'HEAD',
		});

		return;
	},

	async geojsonWithTokenStreamMany(ctx: CtxBase) {
		if (!['GET', 'HEAD'].includes(ctx.method)) {
			ctx.throw(405);
		}

		const token = ctx.params.token;
		const url = ctx.url;
		const userAgent = ctx.header['user-agent'] ?? null;
		const [geoTokenRequest] = await dbQueryEquipe.geoTokenRequestAdd({ url, userAgent });

		if (!token) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'Le jeton ne peut pas être vide.');
		}

		const geoToken = await dbQueryEquipe.geoTokenGet(token);

		if (!geoToken) {
			ctx.throw(StatusCodes.BAD_REQUEST, "Le jeton n'a pas été trouvé.");
		}

		if (!geoToken.active) {
			ctx.throw(StatusCodes.BAD_REQUEST, "Le jeton n'est pas actif.");
		}

		await dbQueryEquipe.geoTokenRequestUpdate({
			geoTokenRequestId: geoTokenRequest.id,
			geoTokenId: geoToken.id,
		});

		const equipeId = geoToken.deveco.equipe.id;

		const params = etablissementParamsInit({
			equipeId,
		});

		// const props = {
		// 	dateCreation: true,
		// 	dateFermeture: true,
		// };

		const format = 'geojson';

		const { fileOptions, dataGenerator } = serviceGeojson.etablissementStreamBuild({
			params,
			// props,
			format,
		});

		void dataStream({
			ctx,
			dataGenerator,
			fileOptions,
			headersOnly: ctx.method === 'HEAD',
		});
	},

	geojsonStreamMany(
		ctx: CtxDeveco,
		params: dbQueryEquipeEtablissement.EtablissementsParams,
		format: 'geojson' | 'ndgeojson'
	) {
		const { fileOptions, dataGenerator } = serviceGeojson.etablissementStreamBuild({
			params,
			format,
		});

		void dataStream({
			ctx,
			dataGenerator,
			fileOptions,
			headersOnly: ctx.method === 'HEAD',
		});

		return;
	},

	async xlsxStreamMany(ctx: CtxDeveco, params: dbQueryEquipeEtablissement.EtablissementsParams) {
		const timestamp = Date.now();
		const filename = `deveco-export-etablissements-${timestamp}.xlsx`;

		ctx.set('Content-disposition', `attachment; filename=${filename}`);
		ctx.set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		ctx.status = StatusCodes.OK;

		const etablissementIdCursor = elasticQueryEquipeEtablissement.eqEtabIdGetCursor(params);

		try {
			await serviceExcel.eqEtabStreamMany({
				equipeId: params.equipeId,
				params,
				etablissementIdCursor,
				output: ctx.res,
			});
		} catch (e) {
			console.logError(e);
		}
	},
};
