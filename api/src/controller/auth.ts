import { compareSync } from 'bcrypt';
import { StatusCodes } from 'http-status-codes';
import { v4 as uuidv4 } from 'uuid';

import * as jwt from '../lib/jwt';
import { emailSend, smtpCheck } from '../lib/email';
import { serviceCookie } from '../service/cookie';
import { CtxDefault } from '../middleware/auth';
import {
	compteGetByCle,
	compteGetByIdentifiant,
	compteWithRelationsGet,
	motDePasseGet,
} from '../db/query/admin';
import { serviceEmailTemplatesCompte } from '../service/email-templates/compte';
import { intlFormatFr } from '../lib/dateUtils';
import { serviceAdmin } from '../service/admin';
import { JwtPayloadCustom, serviceAuth } from '../service/auth';
import { utilisateurFormat } from '../controller/_format/deveco';
import { NotUndefined, QueryResult } from '../db/types';
import { JwtRedirectInput, LoginBody } from '../controller/types';

function compteCheck({
	ctx,
	compte,
}: {
	ctx: CtxDefault;
	compte: NotUndefined<QueryResult<typeof compteWithRelationsGet>>;
}) {
	if (!compte.actif) {
		ctx.throw(StatusCodes.UNAUTHORIZED, `Le compte pour l'id ${compte.id} est inactif`);
	}

	if (compte.typeId === 'api') {
		ctx.throw(StatusCodes.UNAUTHORIZED, `Impossible de générer un JWT pour un compte api`);
	}

	return true;
}

async function motDePasseConnexion({
	ctx,
	compte,
	motDePasse,
	redirectUrl,
}: {
	ctx: CtxDefault;
	compte: NotUndefined<QueryResult<typeof compteWithRelationsGet>>;
	motDePasse: string;
	redirectUrl?: string;
}) {
	const currentPassword = await motDePasseGet(compte.id);
	if (!currentPassword) {
		ctx.status = StatusCodes.UNAUTHORIZED;

		return;
	}

	if (!compareSync(motDePasse, currentPassword.hash)) {
		ctx.status = StatusCodes.UNAUTHORIZED;

		return;
	}

	await serviceAdmin.compteConnexion({
		compteId: compte.id,
	});

	const now = new Date();
	const date = intlFormatFr(now);

	if (smtpCheck()) {
		void (async () => {
			// on envoie aux superadmins ou
			// aux devecos qui n'ont pas désactivé l'envoi de mail
			if (!compte.deveco || compte.deveco.notifications?.connexion !== false) {
				const html = serviceEmailTemplatesCompte.connexionDeveco({
					compte,
					date,
				});
				try {
					await emailSend({
						to: compte.email,
						subject: `Connexion à votre espace Deveco [${date} GMT]`,
						html: html,
					});
				} catch (error) {
					console.logError(error);
				}
			}
		})();
	}

	const { token, jwtPayload } = await serviceAuth.createJwt({
		userAgent: ctx.userAgent,
		compte,
	});

	serviceCookie.add({
		ctx,
		token,
	});

	ctx.status = StatusCodes.OK;
	ctx.body = { ...jwtPayload, redirectUrl };
}

async function jwtConnexion({
	ctx,
	compte,
	redirectUrl,
}: {
	ctx: CtxDefault;
	compte: NotUndefined<QueryResult<typeof compteWithRelationsGet>>;
	redirectUrl?: string;
}) {
	if (compte.typeId === 'superadmin' && smtpCheck()) {
		void (async () => {
			const now = new Date();
			const date = intlFormatFr(now);

			try {
				await emailSend({
					to: compte.email,
					subject: `Connexion à votre compte superadmin Deveco [${date} GMT]`,
					html: serviceEmailTemplatesCompte.connexionAdmin({
						compte,
						date,
					}),
				});
			} catch (error) {
				console.logError(error);
			}
		})();
	}

	const { token, jwtPayload } = await serviceAuth.createJwt({
		userAgent: ctx.userAgent,
		compte,
	});

	serviceCookie.add({
		ctx,
		token,
	});

	await serviceAdmin.compteConnexion({
		compteId: compte.id,
	});

	const user = utilisateurFormat(jwtPayload);

	ctx.status = StatusCodes.OK;
	ctx.body = {
		...user,
		redirectUrl,
	};
}

async function envoiMailConnexion({
	ctx,
	compte,
	redirectUrl,
}: {
	ctx: CtxDefault;
	compte: NotUndefined<QueryResult<typeof compteWithRelationsGet>>;
	redirectUrl?: string;
}) {
	const cle = uuidv4();

	await serviceAdmin.compteConnexionLien({
		compteId: compte.id,
		cle,
	});

	const appUrl = process.env.APP_URL || '';

	const now = new Date();
	const date = intlFormatFr(now);

	if (smtpCheck()) {
		void (async () => {
			try {
				await emailSend({
					to: compte.email,
					subject: `Accédez à votre espace Deveco [${date}]`,
					html: serviceEmailTemplatesCompte.login({
						url: { cle, appUrl, redirectUrl },
						compte,
					}),
				});
			} catch (error) {
				console.logError(error);
			}
		})();
	}

	const url = process.env.SANDBOX_LOGIN
		? `/auth/jwt/${cle}${redirectUrl ? `?url=${redirectUrl}` : ''}`
		: '';

	ctx.status = StatusCodes.OK;
	ctx.body = {
		email: compte.email,
		url,
	};
}

export async function getOrRefreshUserAndToken(ctx: CtxDefault) {
	const currentToken = ctx.cookies.get(serviceCookie.jwtName);

	const verifyResult = await jwt.verify(currentToken);
	if (verifyResult.result === 'invalid') {
		const errorMessage = `La vérification du jeton a échoué : ${verifyResult.reason}`;

		ctx.throw(StatusCodes.UNAUTHORIZED, errorMessage);
	}

	if (verifyResult.result === 'expired') {
		const errorMessage = `Le jeton a expiré à la date ${verifyResult.expiredAt}`;

		ctx.throw(StatusCodes.UNAUTHORIZED, errorMessage);
	}

	let jwtPayload: JwtPayloadCustom;
	let newToken: string;
	let refreshed: boolean;

	if (verifyResult.result === 'expiredWithRefresh') {
		try {
			const newJwt = await serviceAuth.getUserWithRefreshToken({
				refresh: verifyResult.refresh,
				token: verifyResult.token,
				userAgent: JSON.stringify(ctx.userAgent, undefined, 0),
			});

			jwtPayload = newJwt.jwtPayload;
			newToken = newJwt.token;
			refreshed = true;
		} catch (e: any) {
			const errorMessage = `Une erreur s'est produite lors de l'étape de rafraîchissement : ${
				e?.message ?? 'erreur inconnue'
			}`;

			ctx.throw(StatusCodes.UNAUTHORIZED, errorMessage);
		}
	} else {
		jwtPayload = verifyResult.decoded;
		newToken = verifyResult.token;
		refreshed = false;
	}

	if (verifyResult.result === 'oldVersion') {
		jwtPayload = verifyResult.decoded;
		newToken = verifyResult.token;
		refreshed = true;
	}

	return { newToken, jwtPayload, refreshed };
}

export const controllerAuth = {
	async jwt(ctx: CtxDefault) {
		const { redirectUrl } = ctx.request.body as JwtRedirectInput;
		const cle = ctx.params.cle;

		const compte = await compteGetByCle({ cle });
		if (!compte) {
			ctx.status = StatusCodes.NOT_FOUND;
			ctx.body = { error: `compte inexistant pour cette clé : ${cle}` };

			return;
		}

		await jwtConnexion({
			ctx,
			compte,
			redirectUrl,
		});
	},

	async status(ctx: CtxDefault) {
		ctx.set('Cache-Control', 'no-store, no-cache, must-revalidate');
		ctx.set('Expires', '0');

		const result = await getOrRefreshUserAndToken(ctx);

		const { newToken, jwtPayload, refreshed } = result;

		const user = utilisateurFormat(jwtPayload);

		ctx.status = StatusCodes.OK;
		ctx.body = {
			token: newToken,
			user,
		};

		if (refreshed) {
			serviceCookie.add({ ctx, token: newToken });
		}
	},

	async login(ctx: CtxDefault) {
		// TODO utiliser une fonction pour parser la requête
		const { identifiant: identifiantRaw, redirectUrl, motDePasse } = ctx.request.body as LoginBody;

		const identifiant = identifiantRaw?.trim().toLocaleLowerCase();
		if (!identifiant) {
			ctx.status = StatusCodes.BAD_REQUEST;
			ctx.body = { error: "l'identifiant est requis" };

			return;
		}

		const compte = await compteGetByIdentifiant({ identifiant });
		if (!compte) {
			ctx.status = StatusCodes.NOT_FOUND;
			ctx.body = { error: `aucun compte pour l'identifiant ${identifiant}` };

			return;
		}

		if (!compteCheck({ ctx, compte })) {
			return;
		}

		if (motDePasse) {
			await motDePasseConnexion({
				ctx,
				compte,
				motDePasse,
				redirectUrl,
			});

			return;
		}

		await envoiMailConnexion({
			ctx,
			compte,
			redirectUrl,
		});
	},

	logout(ctx: CtxDefault) {
		// TODO enregistrer une action ?
		serviceCookie.remove(ctx);

		ctx.status = StatusCodes.OK;
		ctx.body = {};
	},
};
