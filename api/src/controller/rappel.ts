import { StatusCodes } from 'http-status-codes';

import { CtxDeveco } from '../middleware/auth';
import { Evenement } from '../db/types';
import { RappelAddInput, RappelUpdateInput, RappelClotureInput } from '../controller/types';
import { querystringToNumber, querystringToDate, querystringToString } from '../lib/querystring';
import * as dbQueryEvenement from '../db/query/evenement';
import * as dbQueryEquipe from '../db/query/equipe';
import * as dbQueryRappel from '../db/query/rappel';
import * as dbQueryBrouillon from '../db/query/brouillon';
import { rappelFormat } from '../controller/_format/equipe';
import { serviceRappel } from '../service/rappel';
import { serviceBrouillon } from '../service/brouillon';

export const controllerRappel = {
	async rappelGetMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const rappels = await dbQueryRappel.rappelWithRelationsGetMany({
			equipeId,
		});

		ctx.body = {
			rappels: rappels.map(rappelFormat),
		};
	},

	async rappelAdd(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		// TODO utiliser une fonction pour parser la requête
		const {
			rappel: {
				nom,
				date,
				affecteId: affecteCompteId,
				description,
				brouillonId: brouillonIdRaw,
				brouillonSuppression,
			},
		} = ctx.request.body as RappelAddInput;

		if (!nom || !date) {
			ctx.throw(StatusCodes.BAD_REQUEST, `Un rappel doit avoir un titre et une date`);
		}

		const brouillonId = querystringToNumber(brouillonIdRaw);
		if (brouillonId) {
			const brouillon = await dbQueryBrouillon.brouillonGet({
				equipeId,
				brouillonId,
			});
			if (!brouillon) {
				ctx.throw(StatusCodes.BAD_REQUEST, `aucun brouillon avec cet id : ${brouillonId}`);
			}
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		const partialRappel = {
			equipeId,
			nom,
			date: new Date(date),
			affecteCompteId: querystringToNumber(affecteCompteId),
			description: querystringToString(description) ?? null,
		};

		const rappel = await serviceRappel.rappelAdd({
			evenementId: evenement.id,
			partialRappel,
			brouillonId,
		});

		if (brouillonId && brouillonSuppression) {
			await serviceBrouillon.brouillonRemove({
				evenementId: evenement.id,
				brouillonId,
			});
		}

		return {
			evenementId: evenement.id,
			equipeId,
			rappel,
		};
	},

	async rappelUpdate(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const rappelId = ctx.params.rappelId ? parseInt(ctx.params.rappelId) : null;
		if (!rappelId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `rappelId obligatoire`);
		}

		const oldRappel = await dbQueryRappel.rappelGet({
			equipeId,
			rappelId,
		});
		if (!oldRappel) {
			ctx.throw(StatusCodes.NOT_FOUND, `aucun rappel avec cet id : ${rappelId}`);
		}

		const {
			rappel: { nom, date, clotureDate, affecteId: affecteCompteId, description },
		} = ctx.request.body as RappelUpdateInput;

		const partialRappel = {
			nom,
			description: querystringToString(description) ?? null,
			date: querystringToDate(date) ?? undefined,
			clotureDate: querystringToDate(clotureDate),
			affecteCompteId: querystringToNumber(affecteCompteId),
			modificationCompteId: compteId,
			modificationDate: new Date(),
		};

		if (partialRappel.affecteCompteId !== null) {
			const affecteDeveco = await dbQueryEquipe.devecoGetByCompte({
				compteId: partialRappel.affecteCompteId,
			});
			if (!affecteDeveco || affecteDeveco.equipeId !== equipeId) {
				ctx.throw(StatusCodes.NOT_FOUND, `Le compte n'existe pas : ${affecteCompteId}`);
			}
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		const rappel = await serviceRappel.rappelUpdate({
			evenementId: evenement.id,
			oldRappel,
			partialRappel,
		});

		return {
			evenementId: evenement.id,
			equipeId,
			oldRappel,
			rappel,
		};
	},

	async rappelCloture(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const rappelId = ctx.params.rappelId ? parseInt(ctx.params.rappelId) : null;
		if (!rappelId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `rappelId obligatoire`);
		}

		const { cloture } = ctx.request.body as RappelClotureInput;
		if (typeof cloture === 'undefined') {
			ctx.throw(StatusCodes.BAD_REQUEST, `paramètre cloture obligatoire`);
		}

		const oldRappel = await dbQueryRappel.rappelGet({
			equipeId,
			rappelId,
		});
		if (!oldRappel || oldRappel.equipeId !== equipeId) {
			ctx.throw(StatusCodes.NOT_FOUND, `aucun rappel avec cet id : ${rappelId}`);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		const rappel = await serviceRappel.rappelCloture({
			evenementId: evenement.id,
			oldRappel,
		});

		return {
			evenementId: evenement.id,
			equipeId,
			rappel,
		};
	},

	async rappelReouverture(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const rappelId = ctx.params.rappelId ? parseInt(ctx.params.rappelId) : null;
		if (!rappelId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `rappelId obligatoire`);
		}

		const { cloture } = ctx.request.body as RappelClotureInput;
		if (typeof cloture === 'undefined') {
			ctx.throw(StatusCodes.BAD_REQUEST, `paramètre cloture obligatoire`);
		}

		const oldRappel = await dbQueryRappel.rappelGet({
			equipeId,
			rappelId,
		});
		if (!oldRappel || oldRappel.equipeId !== equipeId) {
			ctx.throw(StatusCodes.NOT_FOUND, `aucun rappel avec cet id : ${rappelId}`);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		const rappel = await serviceRappel.rappelReouverture({
			evenementId: evenement.id,
			oldRappel,
		});

		return {
			evenementId: evenement.id,
			equipeId,
			rappel,
		};
	},

	async rappelRemove(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const rappelId = ctx.params.rappelId ? parseInt(ctx.params.rappelId) : null;
		if (!rappelId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `rappelId obligatoire`);
		}

		const oldRappel = await dbQueryRappel.rappelGet({
			equipeId,
			rappelId,
		});
		if (!oldRappel) {
			ctx.throw(StatusCodes.NOT_FOUND, `aucun rappel avec cet id : ${rappelId}`);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		await serviceRappel.rappelRemove({
			evenementId: evenement.id,
			rappelId,
		});

		return {
			evenementId: evenement.id,
			equipeId,
			rappelId,
		};
	},
};
