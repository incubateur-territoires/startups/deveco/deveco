import { StatusCodes } from 'http-status-codes';

import { CtxDeveco } from '../middleware/auth';
import { serviceExcel } from '../service/excel';
import * as dbQueryEtablissementCreation from '../db/query/etablissement-creation';

export const controllerFluxEtablissementCreations = {
	async xlsxStreamMany(
		ctx: CtxDeveco,
		params: dbQueryEtablissementCreation.EtablissementCreationsParams
	) {
		const equipeId = ctx.state.deveco.equipeId;

		const timestamp = Date.now();
		const filename = `deveco-export-createurs-${timestamp}.xlsx`;

		ctx.set('Content-disposition', `attachment; filename=${filename}`);
		ctx.set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		ctx.status = StatusCodes.OK;

		const etabCreaSelection =
			params.selection?.map((id) => ({ id })) ??
			(await dbQueryEtablissementCreation.etabCreaIdGetManyUnordered(params));

		const etabCreaIds = etabCreaSelection.map(({ id }) => id);

		const etabCreas = etabCreaIds.length
			? await dbQueryEtablissementCreation.etabCreaWithRelationsGetManyById({
					etabCreaIds,
				})
			: [];

		const demandes = await dbQueryEtablissementCreation.etabCreaDemandeWithRelationGetMany({
			equipeId,
			etabCreaIds,
		});

		try {
			await serviceExcel.etabCreaStreamMany({
				etabCreas,
				demandes,
				params,
				output: ctx.res,
			});
		} catch (e) {
			console.logError(e);
		}
	},
};
