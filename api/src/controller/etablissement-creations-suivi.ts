import { CtxDeveco } from '../middleware/auth';
import { serviceEtablissementCreation } from '../service/equipe-etablissement-creation';
import { controllerEchange } from '../controller/echange';
import { controllerDemande } from '../controller/demande';
import { controllerRappel } from '../controller/rappel';

export const controllerEtablissementCreationsSuivi = {
	async echangeAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, echange } = await controllerEchange.echangeAdd(
			ctx,
			'etablissement_creation_echange'
		);

		const etabCreaId = parseInt(ctx.params.etabCreaId);

		await serviceEtablissementCreation.echangeAdd({
			evenementId,
			equipeId,
			etabCreaId,
			echange,
		});

		return next(ctx);
	},

	async echangeUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, echange } = await controllerEchange.echangeUpdate(
			ctx,
			'etablissement_creation_echange'
		);

		const etabCreaId = parseInt(ctx.params.etabCreaId);

		await serviceEtablissementCreation.echangeUpdate({
			evenementId,
			equipeId,
			etabCreaId,
			echange,
		});

		return next(ctx);
	},

	async echangeRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const result = await controllerEchange.echangeRemove(ctx, 'etablissement_creation_echange');

		if (!result) {
			return next(ctx);
		}

		const { evenementId, equipeId, echangeId } = result;

		const etabCreaId = parseInt(ctx.params.etabCreaId);

		await serviceEtablissementCreation.echangeRemove({
			evenementId,
			equipeId,
			etabCreaId,
			echangeId,
		});

		return next(ctx);
	},

	async demandeAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, demande } = await controllerDemande.demandeAdd(
			ctx,
			'etablissement_creation_demande'
		);

		const etabCreaId = parseInt(ctx.params.etabCreaId);

		await serviceEtablissementCreation.demandeAdd({
			evenementId,
			equipeId,
			etabCreaId,
			demande,
		});

		return next(ctx);
	},

	async demandeUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, demande } = await controllerDemande.demandeUpdate(
			ctx,
			'etablissement_creation_demande'
		);

		const etabCreaId = parseInt(ctx.params.etabCreaId);

		await serviceEtablissementCreation.demandeUpdate({
			evenementId,
			equipeId,
			etabCreaId,
			demande,
		});

		return next(ctx);
	},

	async demandeCloture(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, demande } = await controllerDemande.demandeCloture(
			ctx,
			'etablissement_creation_demande'
		);

		const etabCreaId = parseInt(ctx.params.etabCreaId);

		await serviceEtablissementCreation.demandeCloture({
			evenementId,
			equipeId,
			etabCreaId,
			demande,
		});

		return next(ctx);
	},

	async demandeReouverture(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, demande } = await controllerDemande.demandeReouverture(
			ctx,
			'etablissement_creation_demande'
		);

		const etabCreaId = parseInt(ctx.params.etabCreaId);

		await serviceEtablissementCreation.demandeReouverture({
			evenementId,
			equipeId,
			etabCreaId,
			demande,
		});

		return next(ctx);
	},

	async demandeRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const result = await controllerDemande.demandeRemove(ctx, 'etablissement_creation_demande');

		if (!result) {
			return next(ctx);
		}

		const { evenementId, equipeId, demandeId } = result;

		const etabCreaId = parseInt(ctx.params.etabCreaId);

		await serviceEtablissementCreation.demandeRemove({
			evenementId,
			equipeId,
			etabCreaId,
			demandeId,
		});

		return next(ctx);
	},

	async rappelAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, rappel } = await controllerRappel.rappelAdd(
			ctx,
			'etablissement_creation_rappel'
		);

		const etabCreaId = parseInt(ctx.params.etabCreaId);

		await serviceEtablissementCreation.rappelAdd({
			evenementId,
			equipeId,
			etabCreaId,
			rappel,
		});

		return next(ctx);
	},

	async rappelUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, rappel } = await controllerRappel.rappelUpdate(
			ctx,
			'etablissement_creation_rappel'
		);

		const etabCreaId = parseInt(ctx.params.etabCreaId);

		await serviceEtablissementCreation.rappelUpdate({
			evenementId,
			equipeId,
			etabCreaId,
			rappel,
		});

		return next(ctx);
	},

	async rappelCloture(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, rappel } = await controllerRappel.rappelCloture(
			ctx,
			'etablissement_creation_rappel'
		);

		const etabCreaId = parseInt(ctx.params.etabCreaId);

		await serviceEtablissementCreation.rappelCloture({
			equipeId,
			evenementId,
			etabCreaId,
			rappel,
		});

		return next(ctx);
	},

	async rappelReouverture(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, rappel } = await controllerRappel.rappelReouverture(
			ctx,
			'etablissement_creation_rappel'
		);

		const etabCreaId = parseInt(ctx.params.etabCreaId);

		await serviceEtablissementCreation.rappelReouverture({
			equipeId,
			evenementId,
			etabCreaId,
			rappel,
		});

		return next(ctx);
	},

	async rappelRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const result = await controllerRappel.rappelRemove(ctx, 'etablissement_creation_rappel');

		if (!result) {
			return next(ctx);
		}

		const { evenementId, equipeId, rappelId } = result;

		const etabCreaId = parseInt(ctx.params.etabCreaId);

		await serviceEtablissementCreation.rappelRemove({
			evenementId,
			equipeId,
			etabCreaId,
			rappelId,
		});

		return next(ctx);
	},
};
