import * as Koa from 'koa';
import { StatusCodes } from 'http-status-codes';
import { startOfDay, subDays } from 'date-fns';

import {
	InfosUpdateInput,
	MotDePasseUpdateInput,
	NotificationsUpdateInput,
	RechercheEnregistreeAddInput,
	RechercheEnregistreeRemoveInput,
} from './types';

import { serviceCookie } from '../service/cookie';
import { CtxUser, CtxDeveco } from '../middleware/auth';
import * as dbQueryEquipe from '../db/query/equipe';
import * as dbQueryAdmin from '../db/query/admin';
import { serviceStats } from '../service/stats';
import { serviceAdmin } from '../service/admin';
import { serviceAuth } from '../service/auth';
import { serviceEquipe } from '../service/equipe';

export const controllerCompte = {
	async statsGet(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;

		const devecoId = deveco.id;
		const equipeId = deveco.equipeId;

		const now = new Date();
		const date = subDays(startOfDay(now), 5);

		const { eqEtabs, echanges, demandes, rappels, rappelsAVenir, rappelsEnRetard } =
			await serviceStats.equipeStatsGet({
				equipeId,
				devecoId,
				debut: date,
				fin: now,
			});

		ctx.body = {
			etablissementsOuverts: eqEtabs.ouverts,
			etablissementsFermes: eqEtabs.fermes,
			etablissementsProcedure: eqEtabs.procedures,
			echanges,
			demandes,
			rappels,
			rappelsAVenir,
			rappelsEnRetard,
		};
	},

	async infosUpdate(ctx: CtxUser) {
		const { nom, prenom } = ctx.request.body as InfosUpdateInput;

		const { id } = await serviceAdmin.compteUpdate({
			compteId: ctx.state.compte.id,
			partialCompte: {
				nom,
				prenom,
			},
		});

		const compte = await dbQueryAdmin.compteWithRelationsGet({ compteId: id });

		if (!compte) {
			throw new Error(`Impossible de trouver le compte ${id}`);
		}

		const { token, jwtPayload } = await serviceAuth.createJwt({
			userAgent: ctx.userAgent,
			compte,
		});

		serviceCookie.add({
			ctx,
			token,
		});

		ctx.status = StatusCodes.OK;
		ctx.body = { ...jwtPayload };
	},

	async cguUpdate(ctx: CtxUser) {
		const nouveauCompte = await serviceAdmin.compteUpdate({
			compteId: ctx.state.compte.id,
			partialCompte: {
				cgu: true,
			},
		});

		const compte = await dbQueryAdmin.compteWithRelationsGet({ compteId: nouveauCompte.id });

		if (!compte) {
			throw new Error(`Impossible de trouver le compte ${nouveauCompte.id}`);
		}

		const { token, jwtPayload } = await serviceAuth.createJwt({
			userAgent: ctx.userAgent,
			compte,
		});

		serviceCookie.add({
			ctx,
			token,
		});

		ctx.status = StatusCodes.OK;
		ctx.body = { ...jwtPayload };
	},

	async motDePasseUpdate(ctx: CtxUser) {
		const compteId = ctx.state.compte.id;
		const { motDePasse } = (ctx.request.body as MotDePasseUpdateInput) ?? {};

		if (!motDePasse) {
			ctx.throw(StatusCodes.BAD_REQUEST, `Mot de passe requis`);
		}

		if (motDePasse.length < 10) {
			ctx.throw(StatusCodes.BAD_REQUEST, `Le mot de passe doit faire au moins 10 caractères`);
		}

		await serviceAdmin.compteMotDePasseUpdate({ compteId, motDePasse });

		const compte = await dbQueryAdmin.compteWithRelationsGet({ compteId });

		if (!compte) {
			ctx.throw(StatusCodes.NOT_FOUND, `Impossible de trouver le compte ${compteId}`);
		}

		const { token, jwtPayload } = await serviceAuth.createJwt({
			userAgent: ctx.userAgent,
			compte,
		});

		serviceCookie.add({
			ctx,
			token,
		});

		ctx.status = StatusCodes.OK;
		ctx.body = { ...jwtPayload };
	},

	async geoTokenGetMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;

		const tokens = await dbQueryEquipe.geoTokenGetManyByDeveco(deveco.id);

		ctx.body = tokens;
	},

	async rechercheEnregistreeGetMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const devecoId = deveco.id;

		const recherchesEnregistrees = await dbQueryEquipe.rechercheEnregistreeGetManyByDeveco({
			devecoId,
		});

		ctx.body = recherchesEnregistrees;
	},

	async rechercheEnregistreeAdd(ctx: CtxDeveco, next: (ctx: Koa.Context) => Promise<unknown>) {
		const deveco = ctx.state.deveco;
		const devecoId = deveco.id;

		const { nom, url } = ctx.request.body as RechercheEnregistreeAddInput;

		await dbQueryEquipe.rechercheEnregistreeAdd({ devecoId, nom, url });

		return next(ctx);
	},

	async rechercheEnregistreeRemove(ctx: CtxDeveco, next: (ctx: Koa.Context) => Promise<unknown>) {
		const deveco = ctx.state.deveco;
		const devecoId = deveco.id;

		const { rechercheEnregistreeId } = ctx.request.body as RechercheEnregistreeRemoveInput;

		await dbQueryEquipe.rechercheEnregistreeDelete({
			devecoId,
			rechercheEnregistreeId,
		});

		return next(ctx);
	},

	async notificationsGet(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const devecoId = deveco.id;

		const notifications = await serviceEquipe.notificationsGet({ devecoId });

		ctx.body = { notifications };
	},

	async notificationsUpdate(ctx: CtxDeveco, next: (ctx: Koa.Context) => Promise<unknown>) {
		const deveco = ctx.state.deveco;
		const devecoId = deveco.id;

		const { notifications } = ctx.request.body as NotificationsUpdateInput;

		await serviceEquipe.notificationsUpdate({ devecoId, notifications });

		return next(ctx);
	},
};
