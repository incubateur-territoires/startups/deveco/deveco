import { StatusCodes } from 'http-status-codes';

import { CtxDeveco } from '../middleware/auth';
import * as dbQueryContact from '../db/query/contact';
import { serviceExcel } from '../service/excel';

export const controllerFluxContact = {
	async xlsxStreamMany(ctx: CtxDeveco, params: dbQueryContact.ContactParams) {
		const timestamp = Date.now();
		const filename = `deveco-export-contacts-${timestamp}.xlsx`;

		ctx.set('Content-disposition', `attachment; filename=${filename}`);
		ctx.set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		ctx.status = StatusCodes.OK;

		// on veut récupérer tous les contacts
		params.elementsParPage = 0;

		const contactCursor = dbQueryContact.contactGetCursorByEquipe(params);

		try {
			await serviceExcel.contactStreamMany({
				input: contactCursor,
				output: ctx.res,
			});
		} catch (e) {
			console.logError(e);
		}
	},
};
