import { StatusCodes } from 'http-status-codes';

import { CtxDeveco } from '../middleware/auth';
import * as dbQueryEquipe from '../db/query/equipe';
import { serviceTerritoire } from '../service/territoire';
import * as elasticQueryEquipeEtablissement from '../elastic/query/equipe-etablissement';
import { etablissementGetManyParamsBuild } from '../controller/etablissements';
import { controllerEquipeMeta } from '../controller/equipe-meta';

export function geojsonFeaturesBuild({ clusters }: { clusters: any[] }) {
	if (!clusters) {
		return null;
	}

	return {
		type: 'FeatureCollection',
		features: clusters.map(({ key, coordinates, ...properties }) => {
			return {
				type: 'Feature',
				properties,
				geometry: {
					type: 'Point',
					coordinates,
				},
			};
		}),
	};
}

export const controllerEtablissementsGeo = {
	async getMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const devecoId = deveco.id;

		const params = etablissementGetManyParamsBuild({
			query: ctx.query,
			equipeId,
			devecoId,
		});

		const equipe = await dbQueryEquipe.equipeGet({ equipeId });
		if (!equipe) {
			ctx.throw(StatusCodes.BAD_REQUEST, `Equipe #${equipeId} introuvable`);
		}

		// force la géographie de la requête si l'équipe est de type national
		params.geo = equipe.territoireTypeId === 'france' ? 'france' : params.geo;

		const qpvs = params.qpvs.length ? await serviceTerritoire.qpvContourGetManyById(params) : [];

		const zonages = params.localisations.length
			? await serviceTerritoire.zonageContourGetManyById({
					equipeId: params.equipeId,
					etiquetteNoms: params.localisations,
					nwse: params.nwse,
				})
			: [];

		let total = 0;
		let totalCluster = 0;
		let zoom = 0;
		let precision = 0;
		let featuresCollection = null;
		let territoire = null;

		if (!params.nwse.length) {
			await controllerEquipeMeta.contourGet(ctx);
			const body = ctx.body as any;
			territoire = body.territoire;

			if (!territoire) {
				ctx.throw(StatusCodes.BAD_REQUEST, `Equipe ${equipeId} sans contour`);
			}
		} else {
			total = await elasticQueryEquipeEtablissement.eqEtabIdCount(params);

			zoom = params.z ?? 1;

			precision =
				total <= 10000
					? Infinity
					: zoom <= 10
						? zoom + 1
						: zoom <= 11
							? zoom + 2
							: zoom <= 14
								? zoom + 3
								: Infinity;

			const query = await elasticQueryEquipeEtablissement.eqEtabGeoGetMany({
				...params,
				z: precision,
			});

			totalCluster = query?.stats?.stats?.count ?? 0;

			if (totalCluster && zoom === Infinity) {
				total = totalCluster;
			}

			featuresCollection = query?.stats?.annees
				? Object.fromEntries(
						query?.stats?.annees.map(({ annee, clusters }: any) => [
							Number(annee),
							geojsonFeaturesBuild({ clusters })?.features ?? [],
						])
					)
				: null;
		}

		ctx.status = StatusCodes.OK;
		ctx.body = {
			zoom,
			precision,
			total,
			totalCluster,
			features: featuresCollection ? JSON.stringify(featuresCollection) : null,
			featuresParDate: null,
			territoire,
			qpvs: qpvs ?? [],
			zonages: zonages ?? [],
		};
	},
};
