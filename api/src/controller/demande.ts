import { StatusCodes } from 'http-status-codes';

import { DemandeAddInput, DemandeUpdateInput, DemandeClotureInput } from './types';

import { DemandeTypeId, Evenement } from '../db/types';
import { querystringToString, querystringToNumber } from '../lib/querystring';
import { CtxDeveco } from '../middleware/auth';
import * as dbQueryEvenement from '../db/query/evenement';
import * as dbQueryDemande from '../db/query/demande';
import * as dbQueryBrouillon from '../db/query/brouillon';
import { serviceDemande } from '../service/demande';
import { serviceBrouillon } from '../service/brouillon';

export const controllerDemande = {
	async demandeAdd(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const devecoId = deveco.id;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		// TODO utiliser une fonction pour parser la requête
		const {
			demande: { description, typeId, nom, brouillonId: brouillonIdRaw, brouillonSuppression },
		} = ctx.request.body as DemandeAddInput;

		const partialDemande = {
			nom,
			date: new Date(),
			typeId: typeId as DemandeTypeId,
			description,
			equipeId,
			devecoId,
		};

		const brouillonId = querystringToNumber(brouillonIdRaw);
		if (brouillonId) {
			const brouillon = await dbQueryBrouillon.brouillonGet({
				equipeId,
				brouillonId,
			});
			if (!brouillon) {
				ctx.throw(StatusCodes.BAD_REQUEST, `aucun brouillon avec cet id : ${brouillonId}`);
			}
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		const demande = await serviceDemande.demandeAdd({
			evenementId: evenement.id,
			partialDemande,
			brouillonId,
		});

		if (brouillonId && brouillonSuppression) {
			await serviceBrouillon.brouillonRemove({
				evenementId: evenement.id,
				brouillonId,
			});
		}

		return {
			evenementId: evenement.id,
			equipeId,
			demande,
		};
	},

	async demandeUpdate(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const demandeId = ctx.params.demandeId ? parseInt(ctx.params.demandeId) : null;
		if (!demandeId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `demandeId obligatoire`);
		}

		const oldDemande = await dbQueryDemande.demandeWithRelationsGet({ demandeId });
		if (!oldDemande || oldDemande.equipeId !== equipeId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `aucune demande avec cet id : ${demandeId}`);
		}

		const {
			demande: { description, typeId, nom },
		} = ctx.request.body as DemandeUpdateInput;

		const partialDemande = {
			description,
			typeId: typeId as DemandeTypeId,
			date: new Date(),
			nom,
			modificationCompteId: compteId,
			modificationDate: new Date(),
		};

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		const demande = await serviceDemande.demandeUpdate({
			evenementId: evenement.id,
			demandeId,
			partialDemande,
		});

		return {
			evenementId: evenement.id,
			equipeId,
			demandeId,
			demande,
		};
	},

	async demandeCloture(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const compteId = deveco.compteId;

		const demandeId = ctx.params.demandeId;
		if (!demandeId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `id de demande requis`);
		}

		const oldDemande = await dbQueryDemande.demandeWithRelationsGet({ demandeId });
		if (!oldDemande || oldDemande.equipeId !== equipeId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `aucune demande avec cet id ${demandeId}`);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		const {
			demande: { clotureMotif: clotureMotifRaw },
		} = ctx.request.body as DemandeClotureInput;

		const clotureMotif = querystringToString(clotureMotifRaw);

		const demande = await serviceDemande.demandeCloture({
			evenementId: evenement.id,
			demandeId,
			partialDemande: {
				clotureMotif,
			},
		});

		return {
			evenementId: evenement.id,
			equipeId,
			demandeId,
			demande,
		};
	},
	async demandeReouverture(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const compteId = deveco.compteId;

		const demandeId = ctx.params.demandeId;
		if (!demandeId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `id de demande requis`);
		}

		const oldDemande = await dbQueryDemande.demandeWithRelationsGet({ demandeId });
		if (!oldDemande || oldDemande.equipeId !== equipeId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `aucune demande avec cet id ${demandeId}`);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		const demande = await serviceDemande.demandeReouverture({
			evenementId: evenement.id,
			demandeId,
		});

		return {
			evenementId: evenement.id,
			equipeId,
			demandeId,
			demande,
		};
	},

	async demandeRemove(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const demandeId = ctx.params.demandeId ? parseInt(ctx.params.demandeId) : null;
		if (!demandeId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `demandeId obligatoire`);
		}

		const oldDemande = await dbQueryDemande.demandeWithRelationsGet({ demandeId });
		if (!oldDemande) {
			return;
		}

		if (oldDemande.equipeId !== equipeId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `aucune demande avec cet id ${demandeId}`);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		// TODO soft delete
		await serviceDemande.demandeRemove({
			evenementId: evenement.id,
			demandeId,
		});

		return {
			evenementId: evenement.id,
			equipeId,
			demandeId,
		};
	},
};
