import * as Koa from 'koa';
import { StatusCodes } from 'http-status-codes';
import { eq, sql } from 'drizzle-orm';
import { hashSync } from 'bcrypt';

import { ParsedUrlQuery } from 'querystring';

import { CtxUser } from '../middleware/auth';
import { compte, motDePasse } from '../db/schema/admin';
import { deveco, equipe, equipeEpci } from '../db/schema/equipe';
import { Compte, New } from '../db/types';
import db from '../db';
import dbTest from '../db/test';
import * as dbQueryEvenement from '../db/query/evenement';
import { serviceElastic } from '../service/elastic';

export const TestMiddleware = async (
	ctx: CtxUser,
	next: (ctx: Koa.Context) => Promise<unknown>
) => {
	if (process.env.NODE_ENV !== 'test') {
		ctx.throw(StatusCodes.UNAUTHORIZED);
	}

	return next(ctx);
};

const createDevecoAccount = async (
	payload: New<typeof compte> & { premiereFois?: boolean },
	query: ParsedUrlQuery
): Promise<Compte> => {
	const epciId = '200057990';

	const [equipeDie] = await db
		.select()
		.from(equipeEpci)
		.innerJoin(equipe, eq(equipe.id, equipeEpci.equipeId))
		.where(eq(equipeEpci.epciId, epciId));

	const { premiereFois, ...reste } = payload;

	const [nouveauCompte] = await db
		.insert(compte)
		.values({
			...reste,
			identifiant: reste.email,
			prenom: 'Prénom',
			nom: 'Nom',
			typeId: 'deveco',
			actif: true,
			cgu: true,
		})
		.returning();

	await db
		.insert(deveco)
		.values({
			equipeId: equipeDie.equipe.id,
			compteId: nouveauCompte.id,
		})
		.returning();

	if (!premiereFois) {
		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId: nouveauCompte.id,
			typeId: 'connexion',
		});

		await dbQueryEvenement.actionCompteAdd({
			evenementId: evenement.id,
			typeId: 'modification',
			compteId: nouveauCompte.id,
			diff: {
				message: 'ajoute la clé de connexion',
			},
		});
	}

	const password = query.password;
	if (password && typeof password === 'string') {
		const hash = hashSync(password, 10);
		await db.insert(motDePasse).values({
			compteId: nouveauCompte.id,
			hash,
		});
	}

	return nouveauCompte;
};

const createSuperAdminAccount = async (
	payload: New<typeof compte>,
	query: ParsedUrlQuery,
	premiereFois?: boolean
): Promise<Compte> => {
	const [nouveauCompte] = await db
		.insert(compte)
		.values({
			...payload,
			identifiant: payload.email,
			prenom: 'Prénom',
			nom: 'Nom',
			typeId: 'superadmin',
			actif: true,
			cgu: true,
		})
		.returning();

	if (!premiereFois) {
		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId: nouveauCompte.id,
			typeId: 'connexion',
		});

		await dbQueryEvenement.actionCompteAdd({
			evenementId: evenement.id,
			typeId: 'modification',
			compteId: nouveauCompte.id,
			diff: {
				message: 'ajoute la clé de connexion',
			},
		});
	}

	const password = query.password;
	if (password && typeof password === 'string') {
		const hash = hashSync(password, 10);
		await db.insert(motDePasse).values({
			compteId: nouveauCompte.id,
			hash,
		});
	}

	return nouveauCompte;
};

const upsertHelper = async (
	entityName: 'deveco' | 'superadmin',
	payload: Record<string, unknown>,
	query: ParsedUrlQuery
): Promise<any> => {
	if (entityName === 'superadmin') {
		return await createSuperAdminAccount(payload as New<typeof compte>, query);
	}

	if (payload.email && typeof payload.email === 'string') {
		const account = await db.query.compte.findFirst({
			where: eq(compte.email, payload.email),
		});

		if (account) {
			return await db
				.update(compte)
				.set(payload)
				.where(eq(compte.email, payload.email))
				.returning();
		}
	}
	return await createDevecoAccount(
		payload as New<typeof compte> & { premiereFois?: boolean },
		query
	);
};

const resetTestDb = async () => {
	await db.disconnect();
	await dbTest.execute(sql.raw(`DROP DATABASE ${process.env.POSTGRES_DB};`));
	await dbTest.execute(
		sql.raw(
			`CREATE DATABASE ${process.env.POSTGRES_DB} WITH TEMPLATE ${process.env.POSTGRES_DB_SEED};`
		)
	);
	db.reconnect();
};

const indiceInit = {
	etablissement: false,
	local: false,
	qpv: false,
	zonage: false,
};

export const controllerTest = {
	async upsert(ctx: CtxUser) {
		const entity = ctx.params.entity;
		const {
			body: payload,
			query,
		}: {
			body: Record<string, unknown>;
			query: ParsedUrlQuery;
		} = ctx.request as any;

		if (!['deveco', 'superadmin'].includes(entity)) {
			ctx.throw(`Unsupported entity name ${entity}`);
		}

		ctx.body = await upsertHelper(entity, payload, query);
	},

	async resetAndSeed(ctx: CtxUser) {
		await serviceElastic.etablissementIndexCopy({
			isIndexInit: indiceInit.etablissement,
		});

		if (!indiceInit.etablissement) {
			indiceInit.etablissement = true;
		}

		await serviceElastic.localIndexCopy({
			isIndexInit: indiceInit.local,
		});

		if (!indiceInit.local) {
			indiceInit.local = true;
		}

		await serviceElastic.qpvIndexCopy({
			isIndexInit: indiceInit.qpv,
		});

		if (!indiceInit.qpv) {
			indiceInit.qpv = true;
		}

		await serviceElastic.zonageIndexCopy({
			isIndexInit: indiceInit.zonage,
		});

		if (!indiceInit.zonage) {
			indiceInit.zonage = true;
		}

		await resetTestDb();

		return (ctx.status = StatusCodes.OK);
	},
};
