import { CtxDeveco } from '../middleware/auth';
import { serviceEquipeEtablissement } from '../service/equipe-etablissement';
import { controllerEchange } from '../controller/echange';
import { controllerDemande } from '../controller/demande';
import { controllerRappel } from '../controller/rappel';

export const controllerEtablissementsSuivi = {
	async echangeAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, echange } = await controllerEchange.echangeAdd(
			ctx,
			'equipe_etablissement_echange'
		);

		const etablissementId = ctx.params.etablissementId;

		await serviceEquipeEtablissement.echangeAdd({
			evenementId,
			equipeId,
			etablissementId,
			echange,
		});

		return next(ctx);
	},

	async echangeUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, echange } = await controllerEchange.echangeUpdate(
			ctx,
			'equipe_etablissement_echange'
		);

		const etablissementId = ctx.params.etablissementId;

		await serviceEquipeEtablissement.echangeUpdate({
			evenementId,
			equipeId,
			etablissementId,
			echange,
		});

		return next(ctx);
	},

	async echangeRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const result = await controllerEchange.echangeRemove(ctx, 'equipe_etablissement_echange');

		if (!result) {
			return next(ctx);
		}

		const { evenementId, equipeId, echangeId } = result;

		const etablissementId = ctx.params.etablissementId;

		await serviceEquipeEtablissement.echangeRemove({
			evenementId,
			equipeId,
			etablissementId,
			echangeId,
		});

		return next(ctx);
	},

	async demandeAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, demande } = await controllerDemande.demandeAdd(
			ctx,
			'equipe_etablissement_demande'
		);

		const etablissementId = ctx.params.etablissementId;

		await serviceEquipeEtablissement.demandeAdd({
			evenementId,
			equipeId,
			etablissementId,
			demande,
		});

		return next(ctx);
	},

	async demandeUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, demande } = await controllerDemande.demandeUpdate(
			ctx,
			'equipe_etablissement_demande'
		);

		const etablissementId = ctx.params.etablissementId;

		await serviceEquipeEtablissement.demandeUpdate({
			evenementId,
			equipeId,
			etablissementId,
			demande,
		});

		return next(ctx);
	},

	async demandeCloture(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, demande } = await controllerDemande.demandeCloture(
			ctx,
			'equipe_etablissement_demande'
		);

		const etablissementId = ctx.params.etablissementId;

		await serviceEquipeEtablissement.demandeCloture({
			evenementId,
			equipeId,
			etablissementId,
			demande,
		});

		return next(ctx);
	},

	async demandeReouverture(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, demande } = await controllerDemande.demandeReouverture(
			ctx,
			'equipe_etablissement_demande'
		);

		const etablissementId = ctx.params.etablissementId;

		await serviceEquipeEtablissement.demandeReouverture({
			evenementId,
			equipeId,
			etablissementId,
			demande,
		});

		return next(ctx);
	},

	async demandeRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const result = await controllerDemande.demandeRemove(ctx, 'equipe_etablissement_demande');

		if (!result) {
			return next(ctx);
		}

		const { evenementId, equipeId, demandeId } = result;

		const etablissementId = ctx.params.etablissementId;

		await serviceEquipeEtablissement.demandeRemove({
			evenementId,
			equipeId,
			etablissementId,
			demandeId,
		});

		return next(ctx);
	},

	async rappelAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, rappel } = await controllerRappel.rappelAdd(
			ctx,
			'equipe_etablissement_rappel'
		);

		const etablissementId = ctx.params.etablissementId;

		await serviceEquipeEtablissement.rappelAdd({
			evenementId,
			equipeId,
			etablissementId,
			rappel,
		});

		return next(ctx);
	},

	async rappelUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, rappel } = await controllerRappel.rappelUpdate(
			ctx,
			'equipe_etablissement_rappel'
		);

		const etablissementId = ctx.params.etablissementId;

		await serviceEquipeEtablissement.rappelUpdate({
			evenementId,
			equipeId,
			etablissementId,
			rappel,
		});

		return next(ctx);
	},

	async rappelCloture(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, rappel } = await controllerRappel.rappelCloture(
			ctx,
			'equipe_etablissement_rappel'
		);

		const etablissementId = ctx.params.etablissementId;

		await serviceEquipeEtablissement.rappelCloture({
			evenementId,
			equipeId,
			etablissementId,
			rappel,
		});

		return next(ctx);
	},

	async rappelReouverture(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, rappel } = await controllerRappel.rappelReouverture(
			ctx,
			'equipe_etablissement_rappel'
		);

		const etablissementId = ctx.params.etablissementId;

		await serviceEquipeEtablissement.rappelReouverture({
			evenementId,
			equipeId,
			etablissementId,
			rappel,
		});

		return next(ctx);
	},

	async rappelRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const result = await controllerRappel.rappelRemove(ctx, 'equipe_etablissement_rappel');

		if (!result) {
			return next(ctx);
		}

		const { evenementId, equipeId, rappelId } = result;

		const etablissementId = ctx.params.etablissementId;

		await serviceEquipeEtablissement.rappelRemove({
			evenementId,
			equipeId,
			etablissementId,
			rappelId,
		});

		return next(ctx);
	},
};
