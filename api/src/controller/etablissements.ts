import { StatusCodes } from 'http-status-codes';

import { deepEqual } from 'assert';
import { ParsedUrlQuery, encode } from 'querystring';

import {
	etabWithPersoFormat,
	etablissementApercuFormat,
	actionEquipeEtablissementFormatMany,
	etablissementNomBuild,
	etabAvecPortefeuilleInfosFormat,
	etablissementsFormatMany,
	entrepriseApercuFormat,
} from './_format/etablissements';

import { CtxDeveco } from '../middleware/auth';
import { isSiret } from '../lib/regexp';
import { longitudeValidate, latitudeValidate } from '../lib/geo';
import {
	querystringToNumber,
	querystringToBoolean,
	querystringToArray,
	querystringToString,
	querystringToDate,
} from '../lib/querystring';
import { isDefined } from '../lib/utils';
import { codeChiffreDAffairesToTrancheChiffreDAffaires } from '../lib/exercices';
import { effectifCodeToDescription } from '../lib/etablissement-effectif';
import {
	Direction,
	TriEtablissement,
	RessourceEtablissement,
	New,
	Etablissement,
	fileFormatValidate,
	etatEtablissementValidate,
	geolocValidate,
	subventionEtablissementRecueValidate,
	demandeTypeIdValidate,
	accompagnesValidate,
	Equipe,
	Deveco,
	Contact,
	entrepriseTypeValidate,
	varTypeValidate,
	NafType,
	CategorieJuridiqueType,
} from '../db/types';
import {
	ClotureDateUpdateInput,
	ContactAddInput,
	ContactUpdateInput,
	DescriptionInput,
	EnseigneUpdateInput,
	EquipeEtablissementEtiquetteInput,
	FavoriUpdateInput,
	GeolocalisationUpdateInput,
	LookupManyInput,
} from '../controller/types';
import { eqEtabRessource } from '../db/schema/equipe-etablissement';
import * as dbQueryEquipeEtablissement from '../db/query/equipe-etablissement';
import * as dbQueryEtablissement from '../db/query/etablissement';
import * as dbQueryEvenement from '../db/query/evenement';
import * as dbQueryContact from '../db/query/contact';
import * as dbQueryTerritoire from '../db/query/territoire';
import * as elasticQueryEquipeEtablissement from '../elastic/query/equipe-etablissement';
import { serviceEquipe } from '../service/equipe';
import * as serviceSiretisation from '../service/siretisation';
import { serviceContact } from '../service/contact';
import { serviceEquipeEtablissement } from '../service/equipe-etablissement';
import * as controllerEquipe from '../controller/equipe';
import {
	controllerFluxEtablissements,
	etablissementParamsInit,
} from '../controller/flux-etablissements';
import { controllerEtablissementsSuivi } from '../controller/etablissements-suivi';
import { controllerEtablissementsGeo } from '../controller/etablissements-geo';

const appUrl = process.env.APP_URL || '';

interface Stat {
	id: string;
	count: number | null;
	value: number | null;
	countSieges: number | null;
	valueSieges: number | null;
	coordinates?: [number, number];
}

type Stats = Record<
	| 'communes'
	| 'epcis'
	| 'petrs'
	| 'territoireIndustries'
	| 'metropoles'
	| 'departements'
	| 'regions'
	| 'qpvs'
	| 'qpvsEffectifs'
	| 'qpvsCas'
	| 'nafs'
	| 'nafsEffectifs'
	| 'nafsCas'
	| 'catjurs'
	| 'catjursEffectifs'
	| 'catjursCas'
	| 'cas'
	| 'effectifs'
	| 'creations'
	| 'fermetures'
	| 'subventionsAnnees',
	Stat[]
> & {
	etablissements: {
		total: number;
		entreprises: number;
		sieges: number;
		actifs: number;
		actifsSieges: number;
		avecCasSieges: number;
		avecEffectifs: number;
		enQpv: number;
		enQpvSieges: number;
		enQpvAvecEffectifs: number;
		avecSubvention: number;
		avecSubventionSieges: number;
		ess: number;
		essSieges: number;
		micro: number;
		microSieges: number;
		caTotalSieges: number;
		caMedianSieges: number;
		effectifs: number;
		effectifsMedian: number;
		subventions: number;
	};
};

const elementsParPageParDefaut = 20;

export function etablissementGetManyPublicParamsBuild({
	query,
}: {
	query: ParsedUrlQuery;
}): dbQueryEquipeEtablissement.PublicParams {
	const {
		// public
		recherche,
		etat,
		categoriesJuridiques,
		categoriesJuridiquesSans,
		categorieJuridiqueCourante,
		codesNaf,
		codesNafSans,
		effectifInconnu,
		effectifMin,
		effectifMax,
		effectifVarTypes,
		effectifVarMin,
		effectifVarMax,
		ess,
		micro,
		siege,
		types,
		cas,
		caVarTypes,
		caVarMin,
		caVarMax,
		procedure,
		procApres,
		procAvant,
		filiales,
		detentions,
		subventions,
		subAnneeMin,
		subAnneeMax,
		subMontantMin,
		subMontantMax,
		creeApres,
		creeAvant,
		fermeApres,
		fermeAvant,
		// adresse
		cp,
		numero,
		rue,
		// geoloc
		geoloc,
		longitude,
		latitude,
		rayon,
		nwse: nwseRaw,
		z,
		// territoire
		acv,
		pvd,
		va,
		afrs,
		qpvs,
		zrrs,
		territoireIndustries,
		communes,
		epcis,
		departements,
		regions,
		egaproIndiceMin,
		egaproIndiceMax,
		egaproCadresMin,
		egaproCadresMax,
		egaproInstancesMin,
		egaproInstancesMax,
	} = query;

	const nwseString = querystringToString(nwseRaw);

	const nwseArr = nwseString?.split(',').map(Number) ?? [];

	const lat1 = latitudeValidate(nwseArr[0]);
	const lon1 = longitudeValidate(nwseArr[1]);
	const lat2 = latitudeValidate(nwseArr[2]);
	const lon2 = longitudeValidate(nwseArr[3]);

	const nwse =
		isDefined(lat1) && isDefined(lon1) && isDefined(lat2) && isDefined(lon2)
			? [lat1, lon1, lat2, lon2]
			: [];

	return {
		recherche: querystringToString(recherche),
		siege: querystringToBoolean(siege),
		etat: querystringToArray(etat)
			.map(etatEtablissementValidate)
			.flatMap((d) => (d ? [d] : [])),
		categoriesJuridiques: querystringToArray(categoriesJuridiques),
		categoriesJuridiquesSans: querystringToArray(categoriesJuridiquesSans),
		categorieJuridiqueCourante: querystringToBoolean(categorieJuridiqueCourante),
		codesNaf: querystringToArray(codesNaf),
		codesNafSans: querystringToArray(codesNafSans),
		effectifInconnu: querystringToBoolean(effectifInconnu),
		effectifMin: querystringToNumber(effectifMin),
		effectifMax: querystringToNumber(effectifMax),
		effectifVarTypes: querystringToArray(effectifVarTypes)
			.map(varTypeValidate)
			.flatMap((d) => (d ? [d] : [])),
		effectifVarMin: querystringToNumber(effectifVarMin),
		micro: querystringToBoolean(micro),
		effectifVarMax: querystringToNumber(effectifVarMax),
		ess: querystringToBoolean(ess),
		cas: querystringToArray(cas),
		caVarTypes: querystringToArray(caVarTypes)
			.map(varTypeValidate)
			.flatMap((d) => (d ? [d] : [])),
		caVarMin: querystringToNumber(caVarMin),
		caVarMax: querystringToNumber(caVarMax),
		procApres: querystringToDate(procApres),
		procedure: querystringToBoolean(procedure),
		procAvant: querystringToDate(procAvant),
		filiales: querystringToArray(filiales),
		detentions: querystringToArray(detentions),
		subventions: querystringToArray(subventions)
			.map(subventionEtablissementRecueValidate)
			.flatMap((d) => (d ? [d] : [])),
		subAnneeMin: querystringToNumber(subAnneeMin),
		subAnneeMax: querystringToNumber(subAnneeMax),
		subMontantMin: querystringToNumber(subMontantMin),
		subMontantMax: querystringToNumber(subMontantMax),
		creeApres: querystringToDate(creeApres),
		creeAvant: querystringToDate(creeAvant),
		fermeApres: querystringToDate(fermeApres),
		fermeAvant: querystringToDate(fermeAvant),
		entrepriseTypes: querystringToArray(types)
			.map(entrepriseTypeValidate)
			.flatMap((d) => (d ? [d] : [])),

		// adresse
		cp: querystringToString(cp),
		numero: querystringToString(numero),
		rue: querystringToString(rue),

		// geolocalisation
		geoloc: querystringToArray(geoloc)
			.map(geolocValidate)
			.flatMap((d) => (d ? [d] : [])),
		longitude: longitudeValidate(querystringToNumber(longitude)),
		latitude: latitudeValidate(querystringToNumber(latitude)),
		rayon: querystringToNumber(rayon),
		nwse,
		z: querystringToNumber(z),

		// territoire
		qpvs: querystringToArray(qpvs),
		zrrs: querystringToArray(zrrs),
		acv: querystringToBoolean(acv),
		pvd: querystringToBoolean(pvd),
		va: querystringToBoolean(va),
		afrs: querystringToArray(afrs),
		territoireIndustries: querystringToArray(territoireIndustries),
		communes: querystringToArray(communes),
		epcis: querystringToArray(epcis),
		departements: querystringToArray(departements),
		regions: querystringToArray(regions),
		egaproIndiceMin: querystringToNumber(egaproIndiceMin),
		egaproIndiceMax: querystringToNumber(egaproIndiceMax),
		egaproCadresMin: querystringToNumber(egaproCadresMin),
		egaproCadresMax: querystringToNumber(egaproCadresMax),
		egaproInstancesMin: querystringToNumber(egaproInstancesMin),
		egaproInstancesMax: querystringToNumber(egaproInstancesMax),
	};
}

export function etablissementGetManyPersoParamsBuild({
	equipeId,
	devecoId,
	query,
}: {
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
	query: ParsedUrlQuery;
}): dbQueryEquipeEtablissement.PersoParams {
	const {
		activites,
		activitesToutes,
		demandes,
		localisations,
		localisationsToutes,
		motsCles,
		motsClesTous,
		accompagnes,
		accompagnesApres,
		accompagnesAvant,
		avecContact,
		ancienCreateur,
		favori,
	} = query;

	const parsedFavori = querystringToBoolean(favori);

	const params = {
		equipeId,
		demandeTypeIds: querystringToArray(demandes)
			.map(demandeTypeIdValidate)
			.flatMap((d) => (d ? [d] : [])),
		// etiquettes
		activites: querystringToArray(activites),
		activitesToutes: !!querystringToBoolean(activitesToutes),
		localisations: querystringToArray(localisations),
		localisationsToutes: !!querystringToBoolean(localisationsToutes),
		motsCles: querystringToArray(motsCles),
		motsClesTous: !!querystringToBoolean(motsClesTous),
		// suivi
		accompagnes: querystringToArray(accompagnes)
			.map(accompagnesValidate)
			.flatMap((d) => (d ? [d] : [])),
		accompagnesApres: querystringToDate(accompagnesApres),
		accompagnesAvant: querystringToDate(accompagnesAvant),
		avecContact: querystringToBoolean(avecContact),
		ancienCreateur: querystringToBoolean(ancienCreateur),
		favori: parsedFavori,
	};

	// NOTE on n'a besoin du devecoId seulement quand on recherche les établissements favoris
	if (parsedFavori !== null) {
		return {
			...params,
			favori: parsedFavori,
			devecoId,
		};
	}

	return {
		...params,
		favori: null,
		devecoId: null,
	};
}

export function etablissementGetManyParamsBuild({
	query,
	equipeId,
	devecoId,
}: {
	query: ParsedUrlQuery;
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
}): dbQueryEquipeEtablissement.EtablissementsParams {
	const publicParams = etablissementGetManyPublicParamsBuild({ query });
	const geoParams = controllerEquipe.geoParamsBuild({ equipeId, query });

	const persoParams = etablissementGetManyPersoParamsBuild({ equipeId, devecoId, query });

	const {
		// pagination
		elementsParPage,
		tri,
		direction,
		page,
	} = query;

	const parsedTri =
		tri === 'creation' ? 'creation' : tri === 'fermeture' ? 'fermeture' : 'consultation';

	return {
		// public
		...publicParams,

		// geo
		...geoParams,

		// perso
		...persoParams,

		// pagination
		elementsParPage: Math.min(
			querystringToNumber(elementsParPage) || elementsParPageParDefaut,
			elementsParPageParDefaut
		),
		page: Math.min(querystringToNumber(page) || 1, 10000 / elementsParPageParDefaut),
		direction: (direction ?? 'desc') as Direction,
		tri: parsedTri as TriEtablissement,
	};
}

async function filtreHydrateMany(
	params: Pick<
		dbQueryEquipeEtablissement.EtablissementsParams,
		// meta
		| 'categoriesJuridiques'
		| 'categoriesJuridiquesSans'
		| 'codesNaf'
		| 'codesNafSans'
		// territoires
		| 'communes'
		| 'epcis'
		| 'territoireIndustries'
		| 'departements'
		| 'regions'
		| 'qpvs'
		| 'filiales'
		| 'detentions'
	>
) {
	// meta

	const nafTypes = params.codesNaf.length
		? await dbQueryEtablissement.nafTypeGetManyById({ nafTypeIds: params.codesNaf })
		: params.codesNafSans.length
			? await dbQueryEtablissement.nafTypeGetManyById({ nafTypeIds: params.codesNafSans })
			: [];

	const catJurTypes = params.categoriesJuridiques.length
		? await dbQueryEtablissement.categorieJuridiqueGetManyById({
				categorieJuridiqueTypeIds: params.categoriesJuridiques,
			})
		: params.categoriesJuridiquesSans.length
			? await dbQueryEtablissement.categorieJuridiqueGetManyById({
					categorieJuridiqueTypeIds: params.categoriesJuridiquesSans,
				})
			: [];

	// territoires

	const communes = params.communes.length
		? await dbQueryTerritoire.communeGetMany({ communeIds: params.communes })
		: [];

	const epcis = params.epcis.length
		? await dbQueryTerritoire.epciGetMany({ epciIds: params.epcis })
		: [];

	const territoireIndustries = params.territoireIndustries.length
		? await dbQueryTerritoire.territoireIndustrieGetManyById({
				territoireIndustrieIds: params.territoireIndustries,
			})
		: [];

	const departements = params.departements.length
		? await dbQueryTerritoire.departementGetManyById({ departementIds: params.departements })
		: [];

	const regions = params.regions.length
		? await dbQueryTerritoire.regionGetManyById({ regionIds: params.regions })
		: [];

	const qpvs = params.qpvs.length
		? await dbQueryTerritoire.qpvGetManyById({
				qpvIds: params.qpvs,
			})
		: [];

	// participation

	const filiales = params.filiales.length
		? (await dbQueryEtablissement.entrepriseGetManyById({ sirens: params.filiales })).map((ul) =>
				entrepriseApercuFormat({ ul })
			)
		: [];

	const detentions = params.detentions.length
		? (await dbQueryEtablissement.entrepriseGetManyById({ sirens: params.detentions })).map((ul) =>
				entrepriseApercuFormat({ ul })
			)
		: [];

	return {
		// meta
		nafTypes,
		catJurTypes,
		// territoires
		communes,
		epcis,
		departements,
		regions,
		qpvs,
		territoireIndustries,
		// participartion
		filiales,
		detentions,
	};
}

export async function eqEtabStatFormatMany(stats: Stats) {
	function nafStatsFormat(nafStats: Stat[] = [], nafTypes: NafType[]) {
		return nafStats.map((naf) => {
			const label = nafTypes.find(({ id }) => id === naf.id)?.nom;

			return {
				...naf,
				label: label ? `${label} (${naf.id})` : naf.id,
			};
		});
	}

	function catjurStatsFormat(catjurStats: Stat[] = [], catJurTypes: CategorieJuridiqueType[]) {
		return catjurStats.map((catjur) => ({
			...catjur,
			label: catJurTypes.find(({ id }) => id === catjur.id)?.nom ?? catjur.id,
		}));
	}

	// meta

	const nafTypeIds = [...stats.nafs, ...stats.nafsCas, ...stats.nafsEffectifs].map((c) => c.id);
	const nafTypes = nafTypeIds.length
		? await dbQueryEtablissement.nafTypeGetManyById({ nafTypeIds })
		: [];

	const categorieJuridiqueTypeIds = [
		...stats.catjurs,
		...stats.catjursCas,
		...stats.catjursEffectifs,
	].map((c) => c.id);
	const catJurTypes = categorieJuridiqueTypeIds.length
		? await dbQueryEtablissement.categorieJuridiqueGetManyById({
				categorieJuridiqueTypeIds,
			})
		: [];

	// territoires

	const communeIds = stats.communes.map((c) => c.id);
	const communes = communeIds.length ? await dbQueryTerritoire.communeGetMany({ communeIds }) : [];

	const epciIds = stats.epcis.map((c) => c.id);
	const epcis = epciIds.length ? await dbQueryTerritoire.epciGetMany({ epciIds }) : [];

	const metropoleIds = stats.metropoles.map((c) => c.id);
	const metropoles = metropoleIds.length
		? await dbQueryTerritoire.metropoleGetMany({ metropoleIds })
		: [];

	const territoireIndustrieIds = stats.territoireIndustries.map((c) => c.id);
	const territoireIndustries = territoireIndustrieIds.length
		? await dbQueryTerritoire.territoireIndustrieGetManyById({ territoireIndustrieIds })
		: [];

	const petrIds = stats.petrs.map((c) => c.id);
	const petrs = petrIds.length ? await dbQueryTerritoire.petrGetMany({ petrIds }) : [];

	const departementIds = stats.departements.map((c) => c.id);
	const departements = departementIds.length
		? await dbQueryTerritoire.departementGetManyById({ departementIds })
		: [];

	const regionIds = stats.regions.map((c) => c.id);
	const regions = regionIds.length ? await dbQueryTerritoire.regionGetManyById({ regionIds }) : [];

	const qpvIds = [...stats.qpvs, ...stats.qpvsCas, ...stats.qpvsEffectifs].map((c) => c.id);
	const qpvs = qpvIds.length ? await dbQueryTerritoire.qpvGetManyById({ qpvIds }) : [];

	return {
		etablissements: stats.etablissements,

		creations: stats.creations.map((creation) => ({
			...creation,
			label: new Date(Number(creation.id)).toISOString().substring(0, 4),
		})),

		fermetures: stats.fermetures.map((fermeture) => ({
			...fermeture,
			label: new Date(Number(fermeture.id)).toISOString().substring(0, 4),
		})),

		cas: stats.cas.map((ca) => ({
			...ca,
			label: codeChiffreDAffairesToTrancheChiffreDAffaires(ca.id) ?? '',
		})),

		effectifs: stats.effectifs.map((effectif) => ({
			...effectif,
			label: effectifCodeToDescription(effectif.id) ?? '',
		})),

		subventionsAnnees: stats.subventionsAnnees.map((subventionParAnnee) => ({
			...subventionParAnnee,
			id: subventionParAnnee.id.toString(),
			label: subventionParAnnee.id.toString(),
		})),

		// meta

		nafs: nafStatsFormat(stats.nafs, nafTypes),
		nafsEffectifs: nafStatsFormat(stats.nafsEffectifs, nafTypes),
		nafsCas: nafStatsFormat(stats.nafsCas, nafTypes),

		catjurs: catjurStatsFormat(stats.catjurs, catJurTypes),
		catjursEffectifs: catjurStatsFormat(stats.catjursEffectifs, catJurTypes),
		catjursCas: catjurStatsFormat(stats.catjursCas, catJurTypes),

		// territoires

		communes: stats.communes.map((commune) => ({
			...commune,
			label: communes.find(({ id }) => id === commune.id)?.nom ?? commune.id,
		})),

		epcis: stats.epcis.map((epci) => ({
			...epci,
			label: epcis.find(({ id }) => id === epci.id)?.nom ?? epci.id,
		})),

		metropoles: stats.metropoles.map((metropole) => ({
			...metropole,
			label: metropoles.find(({ id }) => id === metropole.id)?.nom ?? metropole.id,
		})),

		territoireIndustries: stats.territoireIndustries.map((territoireIndustrie) => ({
			...territoireIndustrie,
			label:
				territoireIndustries.find(({ id }) => id === territoireIndustrie.id)?.nom ??
				territoireIndustrie.id,
		})),

		petrs: stats.petrs.map((petr) => ({
			...petr,
			label: petrs.find(({ id }) => id === petr.id)?.nom ?? petr.id,
		})),

		departements: stats.departements.map((departement) => ({
			...departement,
			label: departements.find(({ id }) => id === departement.id)?.nom ?? departement.id,
		})),

		regions: stats.regions.map((region) => ({
			...region,
			label: regions.find(({ id }) => id === region.id)?.nom ?? region.id,
		})),

		qpvs: stats.qpvs.map((qpv) => ({
			...qpv,
			label: qpvs.find(({ id }) => id === qpv.id)?.nom ?? qpv.id,
		})),
		qpvsEffectifs: stats.qpvsEffectifs.map((qpv) => ({
			...qpv,
			label: qpvs.find(({ id }) => id === qpv.id)?.nom ?? qpv.id,
		})),
		qpvsCas: stats.qpvsCas.map((qpv) => ({
			...qpv,
			label: qpvs.find(({ id }) => id === qpv.id)?.nom ?? qpv.id,
		})),
	};
}

function seulementTouteLaFranceCalc(
	params: dbQueryEquipeEtablissement.EtablissementsParams,
	equipeId: Equipe['id']
) {
	const paramsAvecPaginationParDefaut = {
		...params,
		equipeId,
		elementsParPage: null,
		page: null,
		direction: null,
		tri: null,
	};

	const paramsInitAvecFranceUniquement = {
		...etablissementParamsInit({
			equipeId,
		}),
		france: true,
	};

	try {
		deepEqual(paramsAvecPaginationParDefaut, paramsInitAvecFranceUniquement);
	} catch (_e: any) {
		return false;
	}

	try {
		deepEqual(paramsAvecPaginationParDefaut, paramsInitAvecFranceUniquement);
	} catch (_e: any) {
		return false;
	}

	return true;
}

export const controllerEtablissements = {
	async etablissementCheck(
		etablissementId: string,
		ctx: CtxDeveco,
		next: (ctx: CtxDeveco) => Promise<unknown>
	) {
		const siret = etablissementId.replaceAll(/\s/g, '');
		if (!siret) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'Siret vide');
		}

		if (!isSiret(siret)) {
			ctx.throw(StatusCodes.BAD_REQUEST, `Siret invalide : ${siret}`);
		}

		const etab = await dbQueryEtablissement.etablissementGet({
			etablissementId: siret,
		});
		if (!etab) {
			ctx.throw(StatusCodes.BAD_REQUEST, `Établissement inexistant : ${siret}`);
		}

		ctx.params.etablissementId = siret;

		return next(ctx);
	},

	async etiquetteAddMany(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const devecoId = deveco.id;
		const compteId = deveco.compteId;

		const { activites, localisations, motsCles } = ctx.request
			.body as EquipeEtablissementEtiquetteInput;

		const params = etablissementGetManyParamsBuild({
			query: ctx.query,
			equipeId,
			devecoId,
		});

		const total = await elasticQueryEquipeEtablissement.eqEtabIdCount(params);

		if (total > 1000) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'Impossible de qualifier plus de 1000 établissements');
		}

		const { elementsParPage } = params;

		// on veut récupérer tous les établissements
		params.elementsParPage = 0;

		const cursor = elasticQueryEquipeEtablissement.eqEtabIdGetCursor(params);

		const etablissementIds: Etablissement['siret'][] = [];

		for await (const etablissementIdsRaw of cursor) {
			etablissementIds.push(...etablissementIdsRaw.map((e) => e.etablissement_id));
		}

		if (etablissementIds.length) {
			const [evenement] = await dbQueryEvenement.evenementAdd({
				compteId,
				typeId: 'equipe_etablissement_qualification_de_masse',
			});

			const evenementId = evenement.id;

			await serviceEquipeEtablissement.etiquetteAddMany({
				evenementId,
				equipeId,
				etablissementIds,
				activites,
				localisations,
				motsCles,
			});
		}

		// restaure la pagination
		// pour ne retourner que les établissements de la page courante
		params.elementsParPage = elementsParPage;

		return next(ctx);
	},

	async descriptionUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const compteId = ctx.state.deveco.compteId;
		const equipeId = ctx.state.deveco.equipeId;
		const etablissementId = ctx.params.etablissementId;

		// TODO utiliser une fonction pour parser la requête
		const { description } = ctx.request.body as DescriptionInput;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_etablissement_etiquette_description',
		});

		await serviceEquipeEtablissement.descriptionUpdate({
			evenementId: evenement.id,
			equipeId,
			compteId,
			etablissementId,
			description,
		});

		return next(ctx);
	},

	async etiquetteUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const compteId = ctx.state.deveco.compteId;
		const equipeId = ctx.state.deveco.equipeId;
		const etablissementId = ctx.params.etablissementId;

		// TODO utiliser une fonction pour parser la requête
		const { activites, localisations, motsCles } = ctx.request
			.body as EquipeEtablissementEtiquetteInput;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_etablissement_etiquette_description',
		});

		await serviceEquipeEtablissement.etiquetteUpdate({
			evenementId: evenement.id,
			equipeId,
			etablissementId,
			activites,
			localisations,
			motsCles,
		});

		return next(ctx);
	},

	async favoriUpdate(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const compteId = deveco.compteId;
		const devecoId = deveco.id;

		const { favori } = ctx.request.body as FavoriUpdateInput;

		const etablissementId = ctx.params.etablissementId;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_etablissement_favori',
		});

		await serviceEquipeEtablissement.favoriUpdate({
			evenementId: evenement.id,
			equipeId,
			etablissementId,
			devecoId,
			favori: !!favori,
		});

		ctx.status = StatusCodes.OK;
		ctx.body = {};
	},

	async view(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const etablissementId = ctx.params.etablissementId;

		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const compteId = deveco.compteId;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_etablissement_affichage',
		});

		await serviceEquipeEtablissement.view({
			evenementId: evenement.id,
			equipeId,
			etablissementId,
		});

		return next(ctx);
	},

	async get(ctx: CtxDeveco) {
		const etablissementId = ctx.params.etablissementId;

		const deveco = ctx.state.deveco;
		const devecoId = deveco.id;
		const equipeId = deveco.equipeId;

		const result = await serviceEquipeEtablissement.get({
			equipeId,
			etablissementId,
			devecoId,
		});

		if (!result) {
			ctx.throw(
				StatusCodes.NOT_FOUND,
				`Aucun établissement trouvé avec le SIRET ${etablissementId}`
			);
		}

		ctx.status = StatusCodes.OK;
		ctx.body = {
			equipeEtablissement: etabWithPersoFormat(result),
		};
	},

	async suiviParEquipeNomGetMany(ctx: CtxDeveco) {
		const etablissementId = ctx.params.etablissementId;

		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const suivis = await serviceEquipeEtablissement.suiviParEquipeNomGetManyByEtablissementId({
			equipeId,
			etablissementId,
		});

		ctx.status = StatusCodes.OK;
		ctx.body = {
			suivis,
		};
	},

	async getBySiret(ctx: CtxDeveco) {
		const query = ctx.query;
		const equipeId = ctx.state.deveco.equipeId;

		const siret = query.recherche as string;
		if (!siret) {
			// NOTE: on retourne une 200 avec un body pour que le front puisse l'afficher
			ctx.body = {
				error: `Siret vide`,
			};
			return;
		}

		if (!isSiret(siret)) {
			// NOTE: on retourne une 200 avec un body pour que le front puisse l'afficher
			ctx.body = {
				error: `Siret invalide : ${siret}`,
			};
			return;
		}

		const etablissementId = siret.replaceAll(/\s/g, '');

		const etab = await dbQueryEtablissement.etablissementWithRelationsGet({
			equipeId,
			etablissementId,
		});
		if (!etab) {
			// NOTE: on retourne une 200 avec un body pour que le front puisse l'afficher
			ctx.body = {
				error: `Aucun établissement avec ce SIRET : ${etablissementId}`,
			};
			return;
		}

		ctx.status = StatusCodes.OK;
		ctx.body = {
			etablissement: etablissementApercuFormat({ etab }),
		};
	},

	//  ?ess=true&equipeId=6&activites=[]=Boulangerie-P%3C%A2tisserie&localisations[]=CHERBOURG%20EN%20COTENTIN&motsCles[]=&demandes=rh&etat=actif&codesNaf=96.09Z&categoriesJuridiques=1000&communes[]=50402&ess=true&zonages[]=1&page=1
	// curl http://127.0.0.1:4000/health/test?equipeId=6&etatAdministratif=A&zonageId=1&communes[]=50402&communes[]=50442&codesNaf[]=96.03Z&categoriesJuridiques[]=1200&ess=true&p=2
	async viewMany(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const devecoId = deveco.id;

		const params = etablissementGetManyParamsBuild({
			query: ctx.query,
			equipeId,
			devecoId,
		});

		const format = fileFormatValidate(querystringToString(ctx.query.format) ?? '');
		if (format) {
			if (['geojson', 'ndgeojson'].includes(format)) {
				return controllerFluxEtablissements.geojsonStreamMany(
					ctx,
					params,
					format as 'geojson' | 'ndgeojson'
				);
			} else if (format === 'xlsx') {
				return await controllerFluxEtablissements.xlsxStreamMany(ctx, params);
			}

			return;
		}

		return next(ctx);
	},

	async getMany(ctx: CtxDeveco) {
		const affichage = querystringToString(ctx.query.affichage);
		if (affichage === 'geo') {
			return controllerEtablissementsGeo.getMany(ctx);
		}

		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const devecoId = deveco.id;

		const params = etablissementGetManyParamsBuild({
			query: ctx.query,
			equipeId,
			devecoId,
		});

		const statsGet = affichage === 'stats';

		const seulementTouteLaFrance = seulementTouteLaFranceCalc(params, equipeId);

		if (params.geo === 'france' && statsGet && seulementTouteLaFrance) {
			ctx.body = {
				// NOTE : le front attend exactement cette string, ne pas la changer
				error: `Impossible d'exporter avec l'option "Toute la France"`,
			};
			return;
		}

		params.elementsParPage = elementsParPageParDefaut;

		const query = await elasticQueryEquipeEtablissement.eqEtabIdSortedGetMany(params, statsGet);

		const etablissementIds = query.etablissementIds;
		const total = query.total;
		const stats = query.stats ? await eqEtabStatFormatMany(query.stats) : null;

		let elements: ReturnType<typeof etabAvecPortefeuilleInfosFormat>[] = [];

		if (etablissementIds.length > 0) {
			const etablissements =
				await dbQueryEquipeEtablissement.etablissementWithEqEtabWithRelationsGetMany({
					equipeId,
					devecoId,
					etablissementIds,
				});

			elements = etablissementsFormatMany({
				etablissements,
				etablissementIds,
			});
		}

		const { elementsParPage, tri, page, direction, ...paramsFiltre } = params;

		const filtres = await filtreHydrateMany(paramsFiltre);

		ctx.status = StatusCodes.OK;
		ctx.body = {
			total,
			elements,
			// pagination
			elementsParPage,
			page,
			pages: Math.ceil(total / elementsParPage),
			direction,
			tri,
			// filtres
			filtres,
			// statistiques
			stats,
		};
	},

	async lookupMany(ctx: CtxDeveco) {
		const { contenu: base64String } = ctx.request.body as LookupManyInput;
		if (!base64String) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'fichier requis');
		}

		const excelFile = Buffer.from(base64String.split(',')[1], 'base64');

		await serviceSiretisation.siretisation({
			ctx,
			excelFile,
		});
	},

	async zonageGetMany(ctx: CtxDeveco) {
		const etablissementId = ctx.params.etablissementId;

		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const zonages = await dbQueryEquipeEtablissement.zonageGetManyByEquipeEtablissementId({
			equipeId,
			etablissementId,
		});

		ctx.status = StatusCodes.OK;
		ctx.body = zonages.map(({ description, ...z }) => ({
			...z,
			description: description ?? '',
		}));
	},

	async rechercheSauvegardeeLienGet(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const devecoId = deveco.id;

		const params = etablissementGetManyParamsBuild({
			query: ctx.query,
			equipeId,
			devecoId,
		});

		const seulementTouteLaFrance = seulementTouteLaFranceCalc(params, equipeId);

		if (params.geo === 'france' && seulementTouteLaFrance) {
			ctx.throw(
				StatusCodes.BAD_REQUEST,
				`Impossible de créer un lien de visualisation avec l'option "Toute la France"`
			);
		}

		const query = encode(ctx.query);

		const token = await serviceEquipe.rechercheSauvegardeeAdd({
			equipeId,
			devecoId,
			query,
		});

		const url = encodeURIComponent(
			`${appUrl}/api/geojson/recherche-sauvegardee/etablissements/${token}`
		);

		ctx.body = {
			url,
		};
	},

	async actionEtablissementGetMany(ctx: CtxDeveco) {
		const equipeId = ctx.state.deveco.equipeId;
		const etablissementId = ctx.params.etablissementId;

		const actionEquipeEtablissements = await dbQueryEvenement.actionEquipeEtablissementGetMany({
			equipeId,
			etablissementId,
		});

		ctx.body = {
			evenements: actionEquipeEtablissements.map(actionEquipeEtablissementFormatMany),
		};
	},

	async ressourceAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const equipeId = ctx.state.deveco.equipeId;
		const etablissementId = ctx.params.etablissementId;

		const partialRessource = ctx.request.body as New<typeof eqEtabRessource>;

		await serviceEquipeEtablissement.ressourceAdd({
			equipeId,
			etablissementId,
			partialRessource,
		});

		return next(ctx);
	},

	async ressourceUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const ressourceId = ctx.params.ressourceId;
		const partialRessource = ctx.request.body as Partial<RessourceEtablissement>;

		await serviceEquipeEtablissement.ressourceUpdate({ ressourceId, partialRessource });

		return next(ctx);
	},

	async ressourceRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const ressourceId = ctx.params.ressourceId;

		await serviceEquipeEtablissement.ressourceRemove({ ressourceId });

		return next(ctx);
	},

	async partage(ctx: CtxDeveco) {
		async function partageEtablissement() {
			const equipeId = ctx.state.deveco.equipeId;
			const etablissementId = ctx.params.etablissementId;

			const etab = await dbQueryEquipeEtablissement.etablissementWithEqEtabWithContributionGet({
				etablissementId,
				equipeId,
			});

			if (!etab) {
				ctx.throw(
					StatusCodes.NOT_FOUND,
					`Aucun établissement trouvé avec le SIRET ${etablissementId}`
				);
			}

			const descriptif = etablissementNomBuild(etab);

			const redirectUrl = `/etablissements/${etablissementId}`;

			// TODO créer un événement ?

			return { descriptif, redirectUrl };
		}

		await controllerEquipe.partageGenerique(ctx, partageEtablissement);
	},

	async clotureDateUpdate(ctx: CtxDeveco) {
		const compteId = ctx.state.deveco.compteId;
		const equipeId = ctx.state.deveco.equipeId;
		const etablissementId = ctx.params.etablissementId;

		const { cloture } = ctx.request.body as ClotureDateUpdateInput;
		if (typeof cloture === 'undefined') {
			ctx.throw(StatusCodes.BAD_REQUEST, `paramètre cloture obligatoire`);
		}

		const clotureDate = cloture ? new Date() : null;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_etablissement_cloture_date',
		});

		const contribution = await serviceEquipeEtablissement.clotureDateUpdate({
			evenementId: evenement.id,
			equipeId,
			compteId,
			etablissementId,
			clotureDate,
		});

		ctx.body = {
			clotureDateContribution: contribution?.clotureDate ?? null,
		};
	},

	async enseigneUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const compteId = ctx.state.deveco.compteId;
		const equipeId = ctx.state.deveco.equipeId;
		const etablissementId = ctx.params.etablissementId;

		const { enseigne } = ctx.request.body as EnseigneUpdateInput;
		if (typeof enseigne === 'undefined') {
			ctx.throw(StatusCodes.BAD_REQUEST, `paramètre enseigne obligatoire`);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_etablissement_enseigne',
		});

		await serviceEquipeEtablissement.enseigneUpdate({
			evenementId: evenement.id,
			equipeId,
			compteId,
			etablissementId,
			enseigne: enseigne === '' ? null : enseigne,
		});

		return next(ctx);
	},

	async geolocalisationUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const compteId = ctx.state.deveco.compteId;
		const equipeId = ctx.state.deveco.equipeId;
		const etablissementId = ctx.params.etablissementId;

		const contributionEquipe =
			await dbQueryEquipeEtablissement.etablissementWithEqEtabGeolocalisationContributionGet({
				etablissementId,
			});
		if (contributionEquipe?.equipeId && contributionEquipe.equipeId !== equipeId) {
			ctx.throw(
				StatusCodes.BAD_REQUEST,
				`la géolocalisation de cet établissement a déjà été modifiée par l'équipe #${equipeId}`
			);
		}

		const { geolocalisation } = ctx.request.body as GeolocalisationUpdateInput;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_etablissement_geolocalisation',
		});

		await serviceEquipeEtablissement.geolocalisationUpdate({
			evenementId: evenement.id,
			equipeId,
			compteId,
			etablissementId,
			geolocalisation,
		});

		return next(ctx);
	},

	async contactRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const compteId = deveco.compteId;
		const contactId = parseInt(ctx.params.contactId);

		if (!contactId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `id de contact requis`);
		}

		const etablissementId = ctx.params.etablissementId;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_etablissement_contact',
		});

		const deletedContact = await serviceEquipeEtablissement.contactRemove({
			evenementId: evenement.id,
			equipeId,
			etablissementId,
			contactId,
		});

		if (!deletedContact) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'aucun contact avec ces ids');
		}

		return next(ctx);
	},

	async contactAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const body = ctx.request.body as ContactAddInput;
		const { fonction, type } = body;
		let contactId: Contact['id'];

		if (!fonction && fonction !== '') {
			ctx.throw(StatusCodes.BAD_REQUEST, `fonction requise`);
		}

		if (type === 'nouveau' && !body.contact) {
			ctx.throw(StatusCodes.BAD_REQUEST, `contact requis pour l'ajout d'un nouveau contact`);
		}
		if (type === 'existant' && !body.contactId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `contactId requis pour l'ajout d'un contact existant`);
		}

		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const etablissementId = ctx.params.etablissementId;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_etablissement_contact',
		});

		const evenementId = evenement.id;

		if (type === 'nouveau') {
			const { id, ...contactSansId } = body.contact;
			const newContact = await serviceContact.contactAdd({
				evenementId,
				equipeId,
				contact: {
					...contactSansId,
					equipeId,
					naissanceDate: body.contact.naissanceDate ? new Date(body.contact.naissanceDate) : null,
				},
			});

			contactId = newContact.id;
		} else {
			contactId = body.contactId;
		}

		if (contactId) {
			await serviceEquipeEtablissement.contactAdd({
				evenementId,
				equipeId,
				etablissementId,
				contactId,
				fonction,
				contactSourceTypeId: 'deveco',
			});
		}

		return next(ctx);
	},

	async contactUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		// TODO utiliser une fonction pour parser la requête
		const body = ctx.request.body as ContactUpdateInput;
		const { fonction, contact } = body;

		const etablissementId = ctx.params.etablissementId;

		if (!contact) {
			ctx.throw(StatusCodes.BAD_REQUEST, `contact requis`);
		}

		if (!fonction && fonction !== '') {
			ctx.throw(StatusCodes.BAD_REQUEST, `fonction requise`);
		}

		const contactId = ctx.params.contactId;

		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const oldContact = await dbQueryContact.contactGet({ contactId });
		if (!oldContact || oldContact.equipeId !== equipeId) {
			ctx.throw(StatusCodes.NOT_FOUND, `contact ${contactId} inexistant`);
		}

		// si on doit mettre à jour la fonction,
		// vérifier l'existence du contact dans lequel on met à jour l'information
		if (fonction) {
			const contacts = await dbQueryEquipeEtablissement.eqEtabContactGet({
				equipeId,
				etablissementId,
				contactId,
			});

			if (!contacts?.length) {
				ctx.throw(
					StatusCodes.NOT_FOUND,
					`eqEtabContact inexistant: contactId ${contactId}, equipeId ${equipeId}, etablissementId ${etablissementId}`
				);
			}
		}

		const { id, ...contactSansId } = contact;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_etablissement_contact',
		});

		await serviceEquipeEtablissement.contactUpdate({
			evenementId: evenement.id,
			equipeId,
			etablissementId,
			contactId,
			fonction,
			partialContact: {
				...contactSansId,
				naissanceDate: contact.naissanceDate ? new Date(contact.naissanceDate) : null,
			},
			oldContact,
		});

		return next(ctx);
	},

	...controllerEtablissementsSuivi,
};
