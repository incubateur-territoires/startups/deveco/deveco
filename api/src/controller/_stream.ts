import { StatusCodes } from 'http-status-codes';

import { Readable } from 'stream';

import { CtxBase } from '../middleware/auth';

export async function dataStream({
	ctx,
	dataGenerator,
	fileOptions,
	headersOnly,
}: {
	ctx: CtxBase;
	dataGenerator: Generator | AsyncGenerator;
	fileOptions?: {
		filename: string;
		mimetype: string;
	};
	headersOnly?: boolean;
}): Promise<void> {
	ctx.status = StatusCodes.OK;
	ctx.type = fileOptions?.mimetype ?? 'application/octet-stream';
	ctx.set('Content-disposition', `attachment; filename=${fileOptions?.filename ?? 'data'}`);

	if (headersOnly) {
		ctx.body = '';
		return;
	}

	ctx.body = Readable.from(dataGenerator, { objectMode: false });
}
