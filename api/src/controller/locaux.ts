import { StatusCodes } from 'http-status-codes';

import { ParsedUrlQuery, encode } from 'querystring';

import { controllerFluxLocaux } from './flux-locaux';

import { CtxDeveco } from '../middleware/auth';
import {
	querystringToArray,
	querystringToBoolean,
	querystringToNumber,
	querystringToDate,
	querystringToString,
} from '../lib/querystring';
import { isDefined } from '../lib/utils';
import { longitudeValidate, latitudeValidate } from '../lib/geo';
import {
	Direction,
	TriLocal,
	New,
	RessourceLocal,
	localCategorieTypeIdValidate,
	localNatureTypeIdValidate,
	demandeTypeIdValidate,
	accompagnesValidate,
	fileFormatValidate,
	Equipe,
	Deveco,
	localFavoriValidate,
	Contact,
	geolocValidate,
} from '../db/types';
import {
	ContactAddInput,
	ContactUpdateInput,
	EquipeLocalEtiquetteInput,
	EquipeLocalContributionInput,
	FavoriUpdateInput,
	GeolocalisationUpdateInput,
	OccupantAddInput,
} from '../controller/types';
import { eqLocRessource } from '../db/schema/equipe-local';
import * as dbQueryContact from '../db/query/contact';
import * as dbQueryEquipeLocal from '../db/query/equipe-local';
import * as dbQueryEvenement from '../db/query/evenement';
import * as dbQueryEquipe from '../db/query/equipe';
import * as dbQueryTerritoire from '../db/query/territoire';
import * as elasticQueryEquipeLocal from '../elastic/query/equipe-local';
import { serviceEquipe } from '../service/equipe';
import { serviceEquipeLocal } from '../service/equipe-local';
import { serviceContact } from '../service/contact';
import * as controllerEquipe from '../controller/equipe';
import { controllerLocauxSuivi } from '../controller/locaux-suivi';
import { controllerEtablissements } from '../controller/etablissements';
import { controllerLocauxGeo } from '../controller/locaux-geo';
import {
	eqLocFormat,
	localWithRelationsFormatMany,
	localNomBuild,
	eqLocApercuFormat,
} from '../controller/_format/local';

const appUrl = process.env.APP_URL || '';

const elementsParPageParDefaut = 20;

const elementsParPage = 20;

function serializeContribution(contrib?: Record<string, unknown> | null) {
	if (!contrib) {
		return '- aucun -';
	}

	const payload = Object.keys(contrib).reduce((acc: Record<string, unknown>, key) => {
		if (contrib[key]) {
			acc[key] = contrib[key];
		}

		return acc;
	}, {});

	return JSON.stringify(payload, null, 1);
}

export const occupeToString = (occ: boolean | null) => {
	if (occ === null) {
		return 'inconnu';
	}
	if (occ === false) {
		return 'vacant';
	}
	if (occ === true) {
		return 'occupé';
	}
};

export const localGetManyParamsBuild = ({
	query,
	equipeId,
	devecoId,
}: {
	query: ParsedUrlQuery;
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
}): dbQueryEquipeLocal.EquipeLocalParams => {
	const {
		numero,
		nom,
		invariant,
		etages,
		natures,
		categories,
		stmax,
		stmin,
		svmax,
		svmin,
		srmax,
		srmin,
		semax,
		semin,
		ssncmax,
		ssncmin,
		sscmax,
		sscmin,
		vacance,
		contributionRemembre,
		contributionLoyerEurosMin,
		contributionLoyerEurosMax,
		contributionFondsEurosMin,
		contributionFondsEurosMax,
		contributionVenteEurosMin,
		contributionVenteEurosMax,
		demandes,
		aCommune,
		aVoie,
		aNumero,
		aSection,
		aParcelle,
		localisations,
		motsCles,
		accompagnes,
		accompagnesApres,
		accompagnesAvant,
		proprio,
		favoris,
		// geoloc
		geoloc,
		longitude,
		latitude,
		rayon,
		nwse: nwseRaw,
		z,
		// territoire
		acv,
		pvd,
		va,
		afrs,
		qpvs,
		zrrs,
		territoireIndustries,
		communes,
		epcis,
		departements,
		regions,
		// pagination
		tri,
		direction,
		page,
	} = query;

	const nwseString = querystringToString(nwseRaw);

	const nwseArr = nwseString?.split(',').map(Number) ?? [];

	const lat1 = latitudeValidate(nwseArr[0]);
	const lon1 = longitudeValidate(nwseArr[1]);
	const lat2 = latitudeValidate(nwseArr[2]);
	const lon2 = longitudeValidate(nwseArr[3]);

	const nwse =
		isDefined(lat1) && isDefined(lon1) && isDefined(lat2) && isDefined(lon2)
			? [lat1, lon1, lat2, lon2]
			: [];

	const parsedTri = tri === 'alphabetique' ? 'alphabetique' : 'consultation';

	return {
		devecoId,
		//public
		invariant: querystringToString(invariant),
		adresseCommune: querystringToString(aCommune),
		adresseVoie: querystringToString(aVoie),
		adresseNumero: querystringToString(aNumero),
		section: querystringToString(aSection),
		parcelle: querystringToString(aParcelle),
		numero: querystringToString(numero),
		natures: querystringToArray(natures).filter(localNatureTypeIdValidate),
		categories: querystringToArray(categories).filter(localCategorieTypeIdValidate),
		etages: querystringToArray(etages),
		surfaces: {
			totale: {
				min: querystringToString(stmin),
				max: querystringToString(stmax),
			},
			vente: {
				min: querystringToString(svmin),
				max: querystringToString(svmax),
			},
			reserve: {
				min: querystringToString(srmin),
				max: querystringToString(srmax),
			},
			exterieure: {
				min: querystringToString(semin),
				max: querystringToString(semax),
			},
			stationnementNonCouverte: {
				min: querystringToString(ssncmin),
				max: querystringToString(ssncmax),
			},
			stationnementCouverte: {
				min: querystringToString(sscmin),
				max: querystringToString(sscmax),
			},
		},
		rechercheProprietaire: querystringToString(proprio),

		// geolocalisation
		geoloc: querystringToArray(geoloc)
			.map(geolocValidate)
			.flatMap((d) => (d ? [d] : [])),
		longitude: longitudeValidate(querystringToNumber(longitude)),
		latitude: latitudeValidate(querystringToNumber(latitude)),
		rayon: querystringToNumber(rayon),
		nwse,
		z: querystringToNumber(z),

		// territoire
		qpvs: querystringToArray(qpvs),
		zrrs: querystringToArray(zrrs),
		acv: querystringToBoolean(acv),
		pvd: querystringToBoolean(pvd),
		va: querystringToBoolean(va),
		afrs: querystringToArray(afrs),
		territoireIndustries: querystringToArray(territoireIndustries),
		communes: querystringToArray(communes),
		epcis: querystringToArray(epcis),
		departements: querystringToArray(departements),
		regions: querystringToArray(regions),

		// perso
		nom: querystringToString(nom),
		contributionOccupe: querystringToArray(vacance).reduce((acc: (true | false | null)[], item) => {
			if (item === 'occupe') {
				acc.push(true);
			}
			if (item === 'vacant') {
				acc.push(false);
			}
			if (item === 'nr') {
				acc.push(null);
			}

			return acc;
		}, []),
		contributionRemembre: querystringToBoolean(contributionRemembre),
		contributionLoyerEuros: {
			min: querystringToNumber(contributionLoyerEurosMin),
			max: querystringToNumber(contributionLoyerEurosMax),
		},
		contributionFondsEuros: {
			min: querystringToNumber(contributionFondsEurosMin),
			max: querystringToNumber(contributionFondsEurosMax),
		},
		contributionVenteEuros: {
			min: querystringToNumber(contributionVenteEurosMin),
			max: querystringToNumber(contributionVenteEurosMax),
		},
		demandeTypeIds: querystringToArray(demandes)
			.map(demandeTypeIdValidate)
			.flatMap((d) => (d ? [d] : [])),
		localisations: querystringToArray(localisations),
		motsCles: querystringToArray(motsCles),
		accompagnes: querystringToArray(accompagnes)
			.map(accompagnesValidate)
			.flatMap((d) => (d ? [d] : [])),
		accompagnesApres: querystringToDate(accompagnesApres),
		accompagnesAvant: querystringToDate(accompagnesAvant),
		favoris: querystringToArray(favoris)
			.map(localFavoriValidate)
			.flatMap((d) => (d ? [d] : [])),

		//pagination
		elementsParPage,
		page: Math.min(querystringToNumber(page) || 1, 10000 / elementsParPage),
		direction: (direction ?? 'desc') as Direction,
		tri: parsedTri as TriLocal,

		...controllerEquipe.geoParamsBuild({ equipeId, query }),
	};
};

// TODO: utiliser un middleware pour toutes les routes avec le paramètre `:localId`
export function paramsFromContextGet(ctx: CtxDeveco, localNeeded = true) {
	const deveco = ctx.state.deveco;
	const equipeId = deveco.equipeId;
	const devecoId = deveco.id;
	const compteId = deveco.compteId;
	const localId = ctx.params.localId;

	if (localNeeded && !localId) {
		ctx.throw(StatusCodes.BAD_REQUEST, 'Id de local vide');
	}

	return {
		equipeId,
		devecoId,
		compteId,
		localId,
	};
}

async function filtreHydrateMany(
	params: Pick<
		dbQueryEquipeLocal.EquipeLocalParams,
		// territoires
		| 'adresseCommune'
		| 'communes'
		| 'epcis'
		| 'territoireIndustries'
		| 'departements'
		| 'regions'
		| 'qpvs'
	>
) {
	// territoires

	const communesParams = [
		...params.communes,
		...(params.adresseCommune ? [params.adresseCommune] : []),
	];

	const communes = communesParams.length
		? await dbQueryTerritoire.communeGetMany({ communeIds: communesParams })
		: [];

	const epcis = params.epcis.length
		? await dbQueryTerritoire.epciGetMany({ epciIds: params.epcis })
		: [];

	const territoireIndustries = params.territoireIndustries.length
		? await dbQueryTerritoire.territoireIndustrieGetManyById({
				territoireIndustrieIds: params.territoireIndustries,
			})
		: [];

	const departements = params.departements.length
		? await dbQueryTerritoire.departementGetManyById({ departementIds: params.departements })
		: [];

	const regions = params.regions.length
		? await dbQueryTerritoire.regionGetManyById({ regionIds: params.regions })
		: [];

	const qpvs = params.qpvs.length
		? await dbQueryTerritoire.qpvGetManyById({
				qpvIds: params.qpvs,
			})
		: [];

	return {
		// territoires
		communes,
		epcis,
		departements,
		regions,
		qpvs,
		territoireIndustries,
	};
}

export const controllerLocaux = {
	async getById(ctx: CtxDeveco) {
		const query = ctx.query;
		const equipeId = ctx.state.deveco.equipeId;
		const devecoId = ctx.state.deveco.id;

		const localId = query.recherche as string;
		if (!localId) {
			// NOTE: on retourne une 200 avec un body pour que le front puisse l'afficher
			ctx.body = {
				error: `Id vide`,
			};
			return;
		}

		const local = await dbQueryEquipeLocal.localWithRelationsAndEquipeCommuneGet({
			equipeId,
			devecoId,
			localId,
		});
		if (!local) {
			// NOTE: on retourne une 200 avec un body pour que le front puisse l'afficher
			ctx.body = {
				error: `Aucun local avec cet id : ${localId}`,
			};
			return;
		}

		if (local.equipeCommune === null) {
			const equipe = await dbQueryEquipe.equipeGet({ equipeId });
			if (!equipe) {
				ctx.body = {
					error: `Equipe ${equipeId} introuvable`,
				};
				return;
			}

			if (equipe.territoireTypeId !== 'france') {
				ctx.body = {
					error: `Local ${localId} pas sur l'équipe ${equipeId}`,
				};
				return;
			}
		}

		ctx.status = StatusCodes.OK;
		ctx.body = {
			local: eqLocApercuFormat(local),
		};
	},

	async eqLocCheck(localId: string, ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		localId = localId.replaceAll(/\s/g, '');

		if (!localId) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'Id de local vide');
		}

		const local = await dbQueryEquipeLocal.localWithEquipeCommuneGet({
			equipeId,
			localId,
		});
		if (!local) {
			ctx.throw(StatusCodes.BAD_REQUEST, `Local ${localId} introuvable`);
		}

		if (local.equipeCommune === null) {
			const equipe = await dbQueryEquipe.equipeGet({ equipeId });
			if (!equipe) {
				ctx.throw(StatusCodes.BAD_REQUEST, `Equipe ${equipeId} introuvable`);
			}

			if (equipe.territoireTypeId !== 'france') {
				ctx.throw(StatusCodes.BAD_REQUEST, `Local ${localId} pas sur l'équipe ${equipeId}`);
			}
		}

		return next(ctx);
	},

	async viewMany(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { equipeId, devecoId } = paramsFromContextGet(ctx, false);

		const params = localGetManyParamsBuild({
			equipeId,
			devecoId,
			query: ctx.query,
		});

		const format = fileFormatValidate(querystringToString(ctx.query.format) ?? '');

		if (format) {
			if (['geojson', 'ndgeojson'].includes(format)) {
				return controllerFluxLocaux.geojsonStreamMany(
					ctx,
					params,
					format as 'geojson' | 'ndgeojson'
				);
			} else if (format === 'xlsx') {
				return await controllerFluxLocaux.xlsxStreamMany(ctx, params);
			}

			return;
		}

		return next(ctx);
	},

	async getMany(ctx: CtxDeveco) {
		const affichage = querystringToString(ctx.query.affichage);
		if (affichage === 'geo') {
			return controllerLocauxGeo.getMany(ctx);
		}

		const equipeId = ctx.state.deveco.equipeId;
		const devecoId = ctx.state.deveco.id;
		const params = localGetManyParamsBuild({
			query: ctx.query,
			equipeId,
			devecoId,
		});

		params.elementsParPage = elementsParPageParDefaut;

		const { localIds, total } = await elasticQueryEquipeLocal.eqLocIdSortedGetMany({
			...params,
			devecoId,
			equipeId,
		});

		const localWithRelations = localIds.length
			? await dbQueryEquipeLocal.localWithRelationsGetMany({
					equipeId,
					devecoId,
					localIds,
				})
			: [];

		const elements = localWithRelationsFormatMany(localWithRelations, localIds);

		const { elementsParPage, tri, page, direction, ...paramsFiltre } = params;

		const filtres = await filtreHydrateMany(paramsFiltre);

		ctx.status = StatusCodes.OK;
		ctx.body = {
			total,
			elements,
			elementsParPage: params.elementsParPage,
			page: params.page,
			pages: Math.ceil(total / elementsParPage),
			direction: params.direction,
			tri: params.tri,
			// filtres
			filtres,
		};
	},

	async favoriUpdate(ctx: CtxDeveco) {
		const { equipeId, devecoId, compteId, localId } = paramsFromContextGet(ctx);

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_local_favori',
		});

		const { favori } = ctx.request.body as FavoriUpdateInput;

		await serviceEquipeLocal.favoriUpdate({
			evenementId: evenement.id,
			equipeId,
			localId,
			devecoId,
			favori: !!favori,
		});

		ctx.status = StatusCodes.OK;
		ctx.body = {};
	},

	async view(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { equipeId, compteId, localId } = paramsFromContextGet(ctx);

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_local',
		});

		await serviceEquipeLocal.view({
			evenementId: evenement.id,
			equipeId,
			localId,
		});

		return next(ctx);
	},

	async get(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;
		const devecoId = deveco.id;

		const { localId } = ctx.params;

		const eqLoc = await dbQueryEquipeLocal.localWithRelationsGet({
			equipeId,
			devecoId,
			localId,
		});

		if (!eqLoc) {
			ctx.throw(StatusCodes.NOT_FOUND, `Local ${localId} introuvable`);
		}

		ctx.status = StatusCodes.OK;
		ctx.body = {
			local: eqLocFormat(eqLoc),
		};
	},

	async contributionNomUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { equipeId, compteId, localId } = paramsFromContextGet(ctx);

		const contribution = await dbQueryEquipeLocal.eqLocContributionGet({ equipeId, localId });

		const {
			contribution: { nom: nomRaw },
		} = ctx.request.body as EquipeLocalContributionInput;

		const nom = nomRaw?.trim();

		if (!nom && nom !== '') {
			ctx.throw(StatusCodes.BAD_REQUEST, `Nom invalide : ${nom}`);
		}

		const contributionNom = contribution?.nom?.trim();

		if (nom === contributionNom) {
			return next(ctx);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_local_contribution_nom',
		});

		await serviceEquipeLocal.contributionUpdate({
			evenementId: evenement.id,
			equipeId,
			compteId,
			localId,
			contribution: {
				nom,
			},
			diff: {
				message: 'Modification du nom',
				changes: {
					table: 'equipe_local_contribution',
					after: nom,
					before: contributionNom ?? '- aucun -',
				},
			},
		});

		return next(ctx);
	},

	async contributionVacanceUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { equipeId, compteId, localId } = paramsFromContextGet(ctx);

		const contribution = await dbQueryEquipeLocal.eqLocContributionGet({ equipeId, localId });

		const {
			contribution: { occupe },
		} = ctx.request.body as EquipeLocalContributionInput;

		if (occupe === contribution?.occupe) {
			return next(ctx);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_local_contribution_vacance',
		});

		await serviceEquipeLocal.contributionUpdate({
			evenementId: evenement.id,
			equipeId,
			compteId,
			localId,
			contribution: {
				occupe,
			},
			diff: {
				message: 'Modification de la vacance',
				changes: {
					table: 'equipe_local_contribution',
					after: occupeToString(occupe),
					before: occupeToString(contribution?.occupe ?? null),
				},
			},
		});

		return next(ctx);
	},

	async contributionDonneesUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { equipeId, compteId, localId } = paramsFromContextGet(ctx);

		const oldContribution = await dbQueryEquipeLocal.eqLocContributionGet({ equipeId, localId });

		const { contribution } = ctx.request.body as EquipeLocalContributionInput;

		if (contribution.description) {
			contribution.description = contribution.description.trim();
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_local_contribution_donnees',
		});

		await serviceEquipeLocal.contributionUpdate({
			evenementId: evenement.id,
			equipeId,
			compteId,
			localId,
			contribution,
			diff: {
				message: 'Modification des données personnalisées',
				changes: {
					table: 'equipe_local_contribution',
					after: serializeContribution(contribution),
					before: serializeContribution(oldContribution),
				},
			},
		});

		return next(ctx);
	},

	async contactAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const body = ctx.request.body as ContactAddInput;
		const { fonction, type } = body;
		let contactId: Contact['id'];

		if (!fonction && fonction !== '') {
			ctx.throw(StatusCodes.BAD_REQUEST, `fonction requise`);
		}

		if (type === 'nouveau' && !body.contact) {
			ctx.throw(StatusCodes.BAD_REQUEST, `contact requis pour l'ajout d'un nouveau contact`);
		}

		if (type === 'existant' && !body.contactId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `contactId requis pour l'ajout d'un contact existant`);
		}

		const { equipeId, compteId, localId } = paramsFromContextGet(ctx);

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_local_contact',
		});

		const evenementId = evenement.id;

		if (type === 'nouveau') {
			const { id, ...contactSansId } = body.contact;
			const newContact = await serviceContact.contactAdd({
				evenementId,
				equipeId,
				contact: {
					...contactSansId,
					naissanceDate: body.contact.naissanceDate ? new Date(body.contact.naissanceDate) : null,
				},
			});

			contactId = newContact.id;
		} else {
			contactId = body.contactId;
		}

		if (contactId) {
			await serviceEquipeLocal.contactAdd({
				evenementId,
				equipeId,
				localId,
				contactId,
				fonction,
				contactSourceTypeId: 'deveco',
			});
		}

		return next(ctx);
	},

	async contactUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		// TODO utiliser une fonction pour parser la requête
		const body = ctx.request.body as ContactUpdateInput;
		const { fonction, contact } = body;

		if (!contact) {
			ctx.throw(StatusCodes.BAD_REQUEST, `contact requis`);
		}

		if (!fonction && fonction !== '') {
			ctx.throw(StatusCodes.BAD_REQUEST, `fonction requise`);
		}

		const { equipeId, compteId, localId } = paramsFromContextGet(ctx);

		const contactId = ctx.params.contactId;

		const oldContact = await dbQueryContact.contactGet({ contactId });
		if (!oldContact || oldContact.equipeId !== equipeId) {
			ctx.throw(StatusCodes.NOT_FOUND, `contact ${contactId} inexistant`);
		}

		// si on doit mettre à jour la fonction,
		// vérifier l'existence du contact dans lequel on met à jour l'information
		if (fonction) {
			const contacts = await dbQueryEquipeLocal.eqLocContactGet({
				contactId,
				equipeId,
				localId,
			});

			if (!contacts?.length) {
				ctx.throw(
					StatusCodes.NOT_FOUND,
					`eqLocContact inexistant: contactId ${contactId}, equipeId ${equipeId}, localId ${localId}`
				);
			}
		}

		const { id, ...contactSansId } = contact;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_local_contact',
		});

		await serviceEquipeLocal.contactUpdate({
			evenementId: evenement.id,
			equipeId,
			localId,
			contactId,
			fonction,
			partialContact: {
				...contactSansId,
				naissanceDate: contact.naissanceDate ? new Date(contact.naissanceDate) : null,
			},
			oldContact,
		});

		return next(ctx);
	},

	async contactRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const contactId = parseInt(ctx.params.contactId);
		if (!contactId) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'Au moins une valeur requise pour contactId');
		}

		const { equipeId, compteId, localId } = paramsFromContextGet(ctx);

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_local_contact',
		});

		const deletedContact = await serviceEquipeLocal.contactRemove({
			evenementId: evenement.id,
			equipeId,
			localId,
			contactId,
		});

		if (!deletedContact) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'aucun contact avec ces ids');
		}

		return next(ctx);
	},

	async etablissementSuggestionGet(ctx: CtxDeveco) {
		const { localId } = paramsFromContextGet(ctx);

		const localAdresse = await dbQueryEquipeLocal.localAdresseGet({ localId });
		if (!localAdresse) {
			ctx.throw(StatusCodes.NOT_FOUND, `Adresse du local #${localId} introuvable`);
		}

		const { numero, voieNom, communeId } = localAdresse;

		ctx.query = {
			numero: numero ?? undefined,
			rue: voieNom ?? undefined,
			communes: [communeId],
			page: '1',
			elementsParPage: '5',
		};

		await controllerEtablissements.getMany(ctx);
	},

	async occupantAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { siret: etablissementId } = ctx.request.body as OccupantAddInput;
		if (!etablissementId) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'Etablissement vide');
		}

		const { equipeId, compteId, localId } = paramsFromContextGet(ctx);

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_local_occupant',
		});

		await serviceEquipeLocal.occupantAdd({
			evenementId: evenement.id,
			equipeId,
			compteId,
			localId,
			etablissementId,
		});

		return next(ctx);
	},

	async occupantRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const etablissementId = ctx.params.etablissementId;
		if (!etablissementId) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'Etablissement vide');
		}

		const { equipeId, compteId, localId } = paramsFromContextGet(ctx);

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_local_occupant',
		});

		await serviceEquipeLocal.occupantRemove({
			evenementId: evenement.id,
			equipeId,
			compteId,
			localId,
			etablissementId,
		});

		return next(ctx);
	},

	async geolocalisationUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { equipeId, compteId, localId } = paramsFromContextGet(ctx);

		const contributionEquipe =
			await dbQueryEquipeLocal.localWithEqLocGeolocalisationContributionGet({
				localId,
			});
		if (contributionEquipe?.equipeId && contributionEquipe.equipeId !== equipeId) {
			ctx.throw(
				StatusCodes.BAD_REQUEST,
				`la géolocalisation de ce local a déjà été modifiée par l'équipe #${equipeId}`
			);
		}

		const { geolocalisation } = ctx.request.body as GeolocalisationUpdateInput;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_local_geolocalisation',
		});

		await serviceEquipeLocal.geolocalisationUpdate({
			evenementId: evenement.id,
			equipeId,
			compteId,
			localId,
			geolocalisation,
		});

		return next(ctx);
	},

	async ressourceAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { equipeId, compteId, localId } = paramsFromContextGet(ctx);

		const partialRessource = ctx.request.body as New<typeof eqLocRessource>;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_local',
		});

		await serviceEquipeLocal.ressourceAdd({
			evenementId: evenement.id,
			equipeId,
			localId,
			partialRessource,
		});

		return next(ctx);
	},

	async ressourceUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { equipeId, compteId, localId } = paramsFromContextGet(ctx);

		const ressourceId = ctx.params.ressourceId;

		const partialRessource = ctx.request.body as Partial<RessourceLocal>;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_local',
		});

		await serviceEquipeLocal.ressourceUpdate({
			evenementId: evenement.id,
			equipeId,
			localId,
			ressourceId,
			partialRessource,
		});

		return next(ctx);
	},

	async ressourceRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { equipeId, compteId, localId } = paramsFromContextGet(ctx);

		const ressourceId = ctx.params.ressourceId;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_local',
		});

		await serviceEquipeLocal.ressourceRemove({
			evenementId: evenement.id,
			equipeId,
			localId,
			ressourceId,
		});

		return next(ctx);
	},

	async partage(ctx: CtxDeveco) {
		async function partageLocal() {
			const { localId, equipeId } = paramsFromContextGet(ctx);

			const local = await dbQueryEquipeLocal.localWithContributionGet({ equipeId, localId });
			if (!local) {
				ctx.throw(StatusCodes.NOT_FOUND, `Local #${localId} introuvable`);
			}

			const descriptif = localNomBuild(local);

			const redirectUrl = `/locaux/${localId}`;

			// TODO créer un événement ?

			return { descriptif, redirectUrl };
		}

		await controllerEquipe.partageGenerique(ctx, partageLocal);
	},

	async zonageGetMany(ctx: CtxDeveco) {
		const { equipeId, localId } = paramsFromContextGet(ctx);

		const zonages = await dbQueryEquipeLocal.zonageGetManyByEquipeLocal({
			equipeId,
			localId,
		});

		ctx.status = StatusCodes.OK;
		ctx.body = zonages.map(({ description, ...z }) => ({
			...z,
			description: description ?? '',
		}));
	},

	async etiquetteUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { equipeId, compteId, localId } = paramsFromContextGet(ctx);

		// TODO utiliser une fonction pour parser la requête
		const { localisations, motsCles } = ctx.request.body as EquipeLocalEtiquetteInput;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_local_etiquette_description',
		});

		await serviceEquipeLocal.etiquetteUpdate({
			evenementId: evenement.id,
			equipeId,
			localId,
			localisations,
			motsCles,
		});

		return next(ctx);
	},

	async rechercheSauvegardeeLienGet(ctx: CtxDeveco) {
		const { equipeId, devecoId } = paramsFromContextGet(ctx, false);

		const query = encode(ctx.query);

		const token = await serviceEquipe.rechercheSauvegardeeAdd({
			equipeId,
			devecoId,
			query,
		});

		const url = encodeURIComponent(`${appUrl}/api/geojson/recherche-sauvegardee/locaux/${token}`);

		ctx.body = {
			url,
		};
	},

	...controllerLocauxSuivi,
};
