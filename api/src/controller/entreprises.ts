import { StatusCodes } from 'http-status-codes';
import { ParsedUrlQuery } from 'querystring';

import {
	entrepriseBeneficiaireFormatMany,
	entrepriseMandatairesFormatMany,
	entrepriseParticipationFormat,
	exerciceFormat,
	liasseFiscaleFormat,
	entreprisesApercuFormatMany,
} from './_format/etablissements';

import { Direction } from '../db/types';
import { CtxDeveco } from '../middleware/auth';
import { querystringToNumber, querystringToString } from '../lib/querystring';
import * as dbQueryEtablissement from '../db/query/etablissement';
import * as dbQueryEquipeEtablissement from '../db/query/equipe-etablissement';
import * as elasticQueryEtablissement from '../elastic/query/etablissement';
import { serviceEquipeEtablissement } from '../service/equipe-etablissement';
import { wait } from '../lib/utils';
import { isSiren } from '../lib/regexp';

export function entrepriseGetManyPublicParamsBuild({
	query,
}: {
	query: ParsedUrlQuery;
}): Pick<dbQueryEquipeEtablissement.PublicParams, 'recherche'> {
	const { recherche } = query;

	return {
		recherche: querystringToString(recherche),
	};
}

const elementsParPageParDefaut = 20;

export function entrepriseGetManyParamsBuild({
	query,
}: {
	query: ParsedUrlQuery;
}): dbQueryEquipeEtablissement.EntreprisesParams {
	const publicParams = entrepriseGetManyPublicParamsBuild({ query });

	const {
		// pagination
		elementsParPage,
		direction,
		page,
	} = query;

	return {
		// public
		...publicParams,

		// pagination
		elementsParPage: Math.min(
			querystringToNumber(elementsParPage) || elementsParPageParDefaut,
			elementsParPageParDefaut
		),
		page: Math.min(querystringToNumber(page) || 1, 10000 / elementsParPageParDefaut),
		direction: (direction ?? 'desc') as Direction,
	};
}

export const controllerEntreprises = {
	entrepriseCheck(siren: string, ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		if (!siren) {
			ctx.throw(StatusCodes.BAD_REQUEST, 'Siren vide');
		}

		if (!isSiren(siren)) {
			ctx.throw(StatusCodes.BAD_REQUEST, `Siren invalide : ${siren}`);
		}

		ctx.params.entrepriseId = siren.replaceAll(/\s/g, '');

		return next(ctx);
	},

	async getMany(ctx: CtxDeveco) {
		const params = entrepriseGetManyParamsBuild({
			query: ctx.query,
		});

		params.elementsParPage = elementsParPageParDefaut;

		const query = await elasticQueryEtablissement.entrepriseIdSortedGetMany(params);

		const entrepriseIds = query.entrepriseIds;

		ctx.body = entrepriseIds;

		let elements: ReturnType<typeof entreprisesApercuFormatMany> = [];

		if (entrepriseIds.length > 0) {
			const entreprises = await dbQueryEtablissement.entrepriseGetManyById({
				sirens: entrepriseIds,
			});

			elements = entreprisesApercuFormatMany({
				entreprises,
				entrepriseIds,
			});
		}

		const { elementsParPage, page, direction } = params;

		ctx.status = StatusCodes.OK;
		ctx.body = {
			elements,
			// pagination
			elementsParPage,
			page,
			direction,
		};
	},

	async apiEntrepriseInfosGet(ctx: CtxDeveco) {
		const entrepriseId = ctx.params.entrepriseId;

		const entreprise = await dbQueryEtablissement.entrepriseGet({ entrepriseId });
		if (!entreprise) {
			ctx.throw(StatusCodes.NOT_FOUND, `Aucune entreprise trouvée avec le SIREN ${entrepriseId}`);
		}

		let exercices = null;

		let mandataires = null;
		let mandatairesPersonnesPhysiques = null;
		let mandatairesPersonnesMorales = null;

		let beneficiairesEffectifs = null;

		let liasseFiscale = null;

		for (let boucle = 0; boucle < 3; boucle += 1) {
			if (!exercices) {
				const exos = await dbQueryEtablissement.apiEntrepriseEntrepriseExerciceGet({
					entrepriseId,
				});

				exercices = exos ? exerciceFormat(exos) : null;
			}

			if (!mandataires) {
				mandataires = await dbQueryEtablissement.apiEntrepriseEntrepriseMandataireSocialGet({
					entrepriseId,
				});

				const mandatairesFormated =
					(mandataires && entrepriseMandatairesFormatMany(mandataires)) ?? null;

				mandatairesPersonnesPhysiques = mandatairesFormated?.mandatairesPersonnesPhysiques ?? null;
				mandatairesPersonnesMorales = mandatairesFormated?.mandatairesPersonnesMorales ?? null;
			}

			if (!beneficiairesEffectifs) {
				const beneficiaires =
					await dbQueryEtablissement.apiEntrepriseEntrepriseBeneficiaireEffectifGet({
						entrepriseId,
					});

				beneficiairesEffectifs = beneficiaires
					? entrepriseBeneficiaireFormatMany(beneficiaires)
					: null;
			}

			if (!liasseFiscale) {
				const liasse = await dbQueryEtablissement.apiEntrepriseEntrepriseLiasseFiscaleGet({
					entrepriseId,
				});

				liasseFiscale = liasse ? liasseFiscaleFormat(liasse) : null;
			}

			if (!exercices || !mandataires || !beneficiairesEffectifs || !liasseFiscale) {
				await wait(1500);
			} else {
				break;
			}
		}

		ctx.status = StatusCodes.OK;
		ctx.body = {
			exercices,
			mandatairesPersonnesPhysiques,
			mandatairesPersonnesMorales,
			beneficiairesEffectifs,
			liasseFiscale,
		};
	},

	async participationGet(ctx: CtxDeveco) {
		const entrepriseId = ctx.params.entrepriseId;

		const entreprise = await dbQueryEtablissement.entrepriseGet({ entrepriseId });
		if (!entreprise) {
			ctx.throw(StatusCodes.NOT_FOUND, `Aucune entreprise trouvée avec le SIREN ${entrepriseId}`);
		}

		const result = await serviceEquipeEtablissement.participationGet({
			entrepriseId,
		});

		ctx.status = StatusCodes.OK;
		ctx.body = {
			entrepriseParticipation: entrepriseParticipationFormat(result),
		};
	},
};
