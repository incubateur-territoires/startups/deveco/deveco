import { Local, QueryResult, NotUndefined } from '../../db/types';
import * as dbQueryEquipeLocal from '../../db/query/equipe-local';
import { etablissementApercuFormat } from '../../controller/_format/etablissements';
import {
	EquipeLocalForNomBuild,
	EquipeLocalForAdresseBuild,
	LocalComplet,
	LocalSimple,
} from '../../controller/types';
import {
	elementToModificationADate,
	contactFormat,
	etiquettesByTypeBuild,
} from '../../controller/_format/equipe';

export function localNomBuild(local: EquipeLocalForNomBuild) {
	const contribution = local.contributions[0];

	return contribution?.nom ? `${contribution.nom} (${local.id})` : local.id;
}

export function localWithRelationsFormatMany(
	localWithRelations: QueryResult<typeof dbQueryEquipeLocal.localWithRelationsGetMany>,
	localIds: Local['id'][]
) {
	// On conserve le tri original des ids ce que ne fait pas In()
	localWithRelations.sort((e1, e2) => localIds.indexOf(e1.id) - localIds.indexOf(e2.id));

	return localWithRelations.map(eqLocSimpleFormat);
}

function localAvecPortefeuilleInfosFormat(
	localWithRelations: QueryResult<typeof dbQueryEquipeLocal.localWithRelationsGetMany>[0]
) {
	return {
		portefeuilleInfos: {
			contacts: !!localWithRelations.contacts.length,
			echanges: !!localWithRelations.echanges.length,
			demandes: !!localWithRelations.demandes.length,
			rappels: !!localWithRelations.rappels.length,
			favori: !!localWithRelations.favoris.length,
		},
	};
}

export function localAdresseFormat({ adresse }: EquipeLocalForAdresseBuild) {
	return {
		...adresse,
		codePostal: adresse.codePostal ?? '',
	};
}

export function localSurfaceTotaleToString(
	local: Pick<Local, 'surfaceVente' | 'surfaceReserve' | 'surfaceExterieureNonCouverte'>
) {
	return `${
		(local.surfaceVente ?? 0) +
		(local.surfaceReserve ?? 0) +
		(local.surfaceExterieureNonCouverte ?? 0)
	}`;
}

export function eqLocApercuFormat(
	local: Pick<Local, 'id'> & EquipeLocalForNomBuild & EquipeLocalForAdresseBuild
) {
	return {
		id: local.id,
		nomAffichage: localNomBuild(local),
		adresse1: localAdresseFormat(local),
	};
}

export function eqLocSimpleFormat(
	local: QueryResult<typeof dbQueryEquipeLocal.localWithRelationsGetMany>[0]
): LocalSimple | null {
	if (!local) {
		return null;
	}

	const { contributions } = local;

	const contribution = contributions[0];

	const contributionGeolocalisationEquipe = local.contributionsGeolocs?.[0];

	return {
		id: local.id,
		invariant: local.invariant,
		nom: contribution?.nom ?? '',
		section: local.section,
		parcelle: local.parcelle,
		adresse: localAdresseFormat(local),
		occupe: contribution?.occupe ?? null,
		niveau: local.niveau ?? '',
		surfaceTotale: localSurfaceTotaleToString(local),
		categorieType: local.categorieType ?? null,
		actif: local.actif ?? true,
		// geolocalisationContribution: contribution?.geolocalisation ?? null,
		geolocalisationContribution: contributionGeolocalisationEquipe?.geolocalisation ?? null,
		geolocalisationEquipe: contributionGeolocalisationEquipe?.eq?.nom ?? null,

		...localAvecPortefeuilleInfosFormat(local),
	};
}

export function eqLocFormat(
	local: QueryResult<typeof dbQueryEquipeLocal.localWithRelationsGet>
): LocalComplet | null {
	if (!local) {
		return null;
	}

	const { adresse, contributions, echanges, demandes, rappels, contacts, occupants, ressources } =
		local;

	const contribution = contributions[0];

	const etiquettes = local.etiquettes
		.map(
			(
				ete: NotUndefined<
					QueryResult<typeof dbQueryEquipeLocal.localWithRelationsGet>
				>['etiquettes'][0]
			) => ete.etiquette
		)
		.sort((a, b) => a.nom.localeCompare(b.nom));

	const { localisations, motsCles } = etiquettesByTypeBuild(etiquettes);

	const contributionGeolocalisationEquipe = local.contributionsGeolocs?.[0];

	return {
		id: local.id,
		invariant: local.invariant,
		nom: contribution?.nom ?? '',
		section: local.section,
		parcelle: local.parcelle,
		niveau: local.niveau,
		surfaceTotale: localSurfaceTotaleToString(local),
		surfaceVente: local.surfaceVente,
		surfaceReserve: local.surfaceReserve,
		surfaceExterieureNonCouverte: local.surfaceExterieureNonCouverte,
		surfaceStationnementCouvert: local.surfaceStationnementCouvert,
		surfaceStationnementNonCouvert: local.surfaceStationnementNonCouvert,
		natureType: local.natureType ?? null,
		categorieType: local.categorieType ?? null,
		adresse: {
			...adresse,
			codePostal: adresse.codePostal ?? '',
		},
		propPPhys: [
			...local.propPPhys.map(({ propPPhy }) => ({
				...propPPhy,
				naissanceDate: propPPhy.naissanceDate?.toISOString() ?? null,
			})),
		],
		propPMors: [
			...local.propPMors.map(({ propPMor }) => ({
				nom: propPMor.nom,
				entrepriseId: propPMor.entrepriseId,
				siegeEtablissementId: propPMor.ul?.siege?.siret,
				proprietaireCompteId: propPMor.proprietaireCompteId,
			})),
		],
		mutations: local.mutations
			.map(({ mutation }) => mutation)
			.sort((a, b) => b.date.getTime() - a.date.getTime()),
		echanges: echanges.map(({ echange }) => ({
			...echange,
			modification: elementToModificationADate(echange),
		})),
		rappels: rappels.map(({ rappel: { affecteCompte, ...rappel } }) => ({
			...rappel,
			affecte: affecteCompte,
		})),
		demandes: demandes.map(({ demande: { echanges, ...demande } }) => ({
			...demande,
			nombreEchanges: echanges.length,
		})),
		contacts: contacts.map(({ fonction, ctct }) => ({
			...contactFormat(ctct),
			lien: {
				type: 'local',
				fonction,
			},
		})),
		occupants: occupants.map(({ occupant }) => ({
			...etablissementApercuFormat({
				etab: occupant,
			}),
		})),
		commentaires: contribution?.description ?? null,
		ressources,
		occupe: contribution?.occupe ?? null,
		loyer: contribution?.loyerEuros?.toString() ?? null,
		bailType: contribution?.bailTypeId ?? null,
		vacanceType: contribution?.vacanceTypeId ?? null,
		vacanceMotif: contribution?.vacanceMotifTypeId ?? null,
		remembre: contribution?.remembre ?? false,
		fonds: contribution?.fondsEuros?.toString() ?? null,
		vente: contribution?.venteEuros?.toString() ?? null,
		modification: elementToModificationADate(contribution),
		actif: local.actif ?? true,
		// geolocalisationContribution: contribution?.geolocalisation ?? null,
		geolocalisationContribution: contributionGeolocalisationEquipe?.geolocalisation ?? null,
		geolocalisationEquipe: contributionGeolocalisationEquipe?.eq?.nom ?? null,
		etiquettes: {
			activites: [],
			localisations,
			motsCles,
		},
		...localAvecPortefeuilleInfosFormat(local),
	};
}
