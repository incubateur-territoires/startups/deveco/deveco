import { etabCreaNomBuild } from './etablissement-creation';
import { etablissementNomBuild } from './etablissements';
import { localNomBuild } from './local';

import {
	NotUndefined,
	ModificationCompte,
	QueryResult,
	RappelWithEntite,
	BrouillonWithEntite,
	DemandeWithEntite,
} from '../../db/types';
import * as dbQueryContact from '../../db/query/contact';
import * as dbQueryEchange from '../../db/query/echange';
import * as dbQueryRappel from '../../db/query/rappel';
import { EtiquetteWithCount, Etiquettes, ContactLien, ContactAvecFonctions } from '../types';

export function etiquettesByTypeBuild(etiquettes: EtiquetteWithCount[]): Etiquettes {
	return etiquettes.reduce(
		(acc: Etiquettes, etiquette: EtiquetteWithCount) => {
			const { typeId } = etiquette;

			const { activites, localisations, motsCles } = acc;

			let target: EtiquetteWithCount[] = [];

			switch (typeId) {
				case 'activite':
					target = activites;
					break;
				case 'localisation':
					target = localisations;
					break;
				case 'mot_cle':
					target = motsCles;
					break;
			}

			target.push(etiquette);

			return acc;
		},
		{ activites: [], localisations: [], motsCles: [] }
	);
}

function etiquetteWithCountFormat(etiquetteWithCount: EtiquetteWithCount) {
	return {
		id: etiquetteWithCount.id,
		nom: etiquetteWithCount.nom,
		usages: etiquetteWithCount?.count ?? 0,
	};
}

export function etiquetteWithCountFormatMany({
	etiquetteWithCounts,
}: {
	etiquetteWithCounts: EtiquetteWithCount[];
}) {
	const { activites, localisations, motsCles } = etiquettesByTypeBuild(etiquetteWithCounts);

	return {
		activites: activites
			.map(etiquetteWithCountFormat)
			.sort((a, b) => a.nom.localeCompare(b.nom, 'fr', { ignorePunctuation: true })),
		localisations: localisations
			.map(etiquetteWithCountFormat)
			.sort((a, b) => a.nom.localeCompare(b.nom, 'fr', { ignorePunctuation: true })),
		motsCles: motsCles
			.map(etiquetteWithCountFormat)
			.sort((a, b) => a.nom.localeCompare(b.nom, 'fr', { ignorePunctuation: true })),
	};
}

export function elementToModificationADate(
	element?: ({ modificationDate: Date | null } & (ModificationCompte | null)) | null
) {
	if (!element?.modificationDate) {
		return null;
	}

	return {
		date: element.modificationDate,
		compte: element.modifCompte,
	};
}

export function contactFormat(
	contact: QueryResult<typeof dbQueryContact.contactWithRelationGetManyByEquipe>[0]
) {
	const liens: ContactLien[] = [];

	if (contact.etabCs.length) {
		liens.push(
			...contact.etabCs.map(
				(etabContact) =>
					({
						type: 'etablissement',
						etablissementId: etabContact.etab.siret,
						etablissementNom: etablissementNomBuild(etabContact.etab),
						fonction: etabContact.fonction,
					}) as const
			)
		);
	}

	if (contact.creaCs.length) {
		liens.push(
			...contact.creaCs.map(
				(creaContact) =>
					({
						type: 'createur',
						etablissementCreationId: creaContact.eCreation.id,
						etablissementCreationNom: etabCreaNomBuild(creaContact.eCreation),
					}) as const
			)
		);
	}

	if (contact.localCs.length) {
		liens.push(
			...contact.localCs.map(
				(localContact) =>
					({
						type: 'local',
						localId: localContact.local.id,
						localNom: localContact.local.contributions[0]?.nom ?? null,
						fonction: localContact.fonction,
					}) as const
			)
		);
	}

	return {
		id: contact.id,
		nom: contact.nom,
		prenom: contact.prenom,
		telephone: contact.telephone,
		telephone2: contact.telephone2,
		email: contact.email,
		naissanceDate: contact.naissanceDate,
		liens,
		demarchageAccepte: contact.demarchageAccepte,
	};
}

export function contactFormatMany({
	contacts,
}: {
	contacts: QueryResult<typeof dbQueryContact.contactWithRelationGetManyByEquipe>;
}): ContactAvecFonctions[] {
	return contacts.map(contactFormat);
}

export function echangeFormat(
	echange: NotUndefined<QueryResult<typeof dbQueryEchange.echangeWithRelationsGetMany>[0]>
) {
	const { id, nom, typeId, description, date, demandes, deveco, eqEtabs, eqLocaux, creations } =
		echange;

	const echangePayload = {
		id,
		nom,
		typeId,
		description: description ?? '',
		date,
		demandes,
		modification: elementToModificationADate(echange),
		deveco,
	};

	if (eqEtabs.length) {
		const eqEtab = eqEtabs[0];

		return {
			...echangePayload,
			etablissementId: eqEtab.etablissementId,
			etablissementNom: etablissementNomBuild(eqEtab.etab),
		};
	}

	if (creations.length) {
		const creation = creations[0];

		return {
			...echangePayload,
			etablissementCreationId: creation.etabCreaId,
			createurNom: etabCreaNomBuild(creation.etabCrea),
		};
	}

	if (eqLocaux.length) {
		const local = eqLocaux[0].local;

		return {
			...echangePayload,
			localId: local.id,
			localNom: localNomBuild(local),
		};
	}
}

export function echangeFormatSimple(
	echange: NotUndefined<QueryResult<typeof dbQueryEchange.echangeWithRelationsGetManyById>[0]>
) {
	const eqEtab = echange?.eqEtab;
	const etabCrea = echange?.etabCreaEch?.etabCrea;
	const local = echange?.eqLoc?.local;

	const nom = eqEtab
		? etablissementNomBuild(eqEtab.etab)
		: etabCrea
			? etabCreaNomBuild(etabCrea)
			: localNomBuild(local);

	return {
		nom,
		date: new Date(),
		contenu: '',
		etablissementId: eqEtab?.etab?.siret,
		etablissementCreationId: etabCrea?.id,
		localId: local?.id,
	};
}

export function demandeFormat(demande: DemandeWithEntite) {
	const { id, nom, typeId, description, clotureDate, date, eqEtabs, eqLocaux, creations } = demande;

	const demandePayload = {
		id,
		nom,
		description: description ?? '',
		date,
		clotureDate,
		typeId,
	};

	if (eqEtabs.length) {
		const eqEtab = eqEtabs[0];

		return {
			...demandePayload,
			etablissementId: eqEtab.etablissementId,
			etablissementNom: etablissementNomBuild(eqEtab.etab),
		};
	}

	if (creations.length) {
		const creation = creations[0];

		return {
			...demandePayload,
			etablissementCreationId: creation.etabCreaId,
			createurNom: etabCreaNomBuild(creation.etabCrea),
		};
	}

	if (eqLocaux.length) {
		const local = eqLocaux[0].local;

		return {
			...demandePayload,
			localId: local.id,
			localNom: localNomBuild(local),
		};
	}
}

export function rappelFormat(rappel: RappelWithEntite) {
	const { id, nom, description, date, eqEtabs, eqLocaux, creations, clotureDate, affecteCompte } =
		rappel;

	const rappelPayload = {
		id,
		nom,
		description: description ?? '',
		date,
		clotureDate,
		affecte: affecteCompte,
	};

	if (eqEtabs.length) {
		const eqEtab = eqEtabs[0];

		return {
			...rappelPayload,
			etablissementId: eqEtab.etablissementId,
			etablissementNom: etablissementNomBuild(eqEtab.etab),
		};
	}

	if (creations.length) {
		const creation = creations[0];

		return {
			...rappelPayload,
			etablissementCreationId: creation.etabCreaId,
			createurNom: etabCreaNomBuild(creation.etabCrea),
		};
	}

	if (eqLocaux.length) {
		const local = eqLocaux[0].local;

		return {
			...rappelPayload,
			localId: local.id,
			localNom: localNomBuild(local),
		};
	}
}

export function rappelFormatSimple(
	rappel: NotUndefined<QueryResult<typeof dbQueryRappel.rappelWithRelationsGetMany>[0]>
) {
	const eqEtab = rappel?.eqEtabs[0];
	const etabCrea = rappel?.creations[0]?.etabCrea;
	const local = rappel?.eqLocaux[0]?.local;

	const nom = eqEtab
		? etablissementNomBuild(eqEtab.etab)
		: etabCrea
			? etabCreaNomBuild(etabCrea)
			: localNomBuild(local);

	return {
		nom,
		date: rappel.date,
		clotureDate: rappel.clotureDate,
		contenu: rappel.nom,
		etablissementId: eqEtab?.etab?.siret,
		etablissementCreationId: etabCrea?.id,
	};
}

export function brouillonFormat(brouillon: BrouillonWithEntite) {
	const { id, nom, description, date, compte, eqEtabs, eqLocaux, creations } = brouillon;

	const brouillonPayload = {
		id,
		nom,
		description: description ?? '',
		date,
		compte,
		modification: elementToModificationADate(brouillon),
	};

	if (eqEtabs.length) {
		const eqEtab = eqEtabs[0];

		return {
			...brouillonPayload,
			etablissementId: eqEtab.etablissementId,
			etablissementNom: etablissementNomBuild(eqEtab.etab),
		};
	}

	if (creations.length) {
		const creation = creations[0];

		return {
			...brouillonPayload,
			etablissementCreationId: creation.etabCreaId,
			createurNom: etabCreaNomBuild(creation.etabCrea),
		};
	}

	if (eqLocaux.length) {
		const local = eqLocaux[0].local;

		return {
			...brouillonPayload,
			localId: local.id,
			localNom: localNomBuild(local),
		};
	}

	return brouillonPayload;
}
