import { eqLocSimpleFormat } from '../../controller/_format/local';
import {
	etiquettesByTypeBuild,
	elementToModificationADate,
	contactFormat,
} from '../../controller/_format/equipe';
import { etabCreaNomBuild } from '../../controller/_format/etablissement-creation';
import { EntrepriseParticipation, EtablissementCreationForNomBuild } from '../../controller/types';

import {
	NotUndefined,
	Equipe,
	EquipeEtablissementContribution,
	ActionEquipeEtablissement,
	RessourceEtablissement,
	EtablissementSubvention,
	BodaccProcedureCollective,
	Entreprise,
	Etablissement,
	EquipeEtablissementContact,
	EquipeEtablissementEchange,
	EquipeEtablissementDemande,
	EquipeEtablissementRappel,
	DevecoEtablissementFavori,
	EchangeWithModification,
	DemandeWithEchangeDemandeWithEchange,
	RappelWithAffecteCompte,
	Etiquette,
	EquipeEtablissementContributionWithModification,
	EntrepriseRatioFinancier,
	EtablissementEffectifMoyenMensuel,
	ApiEntrepriseEntrepriseMandataireSocial,
	ApiEntrepriseEntrepriseBeneficiaireEffectif,
	ApiEntrepriseEntrepriseExercice,
	ApiEntrepriseEntrepriseLiasseFiscale,
} from '../../db/types';
import { formatJSDateToHumanFriendly } from '../../lib/dateUtils';
import { filterNullish, isDefined } from '../../lib/utils';
import * as dbQueryEquipeEtablissement from '../../db/query/equipe-etablissement';
import { serviceEquipeEtablissement } from '../../service/equipe-etablissement';
import { isApiEntrepriseSourceRecent } from '../../service/api-entreprise';
import { effectifCodeToDescription } from '../../lib/etablissement-effectif';
import {
	EntrepriseData,
	EtabCreaTransformes,
	EntrepriseApercu,
	EtablissementApercu,
	EtablissementExtra,
	EtablissementForAdresseBuild,
	EtablissementForNomBuild,
	EtablissementPageComplete,
	EtablissementSimple,
	EtablissementWithEquipeEtablissementRelations,
	EtablissementWithPortefeuilleInfos,
	EtablissementWithRelations,
	LiasseFiscaleFormatted,
	MandatairePersonneMoraleFormatted,
	MandatairePersonnePhysiqueFormatted,
	PortefeuilleInfos,
} from '../../controller/types';

export function entrepriseNomSourceFormat({
	entreprise: uniteLegale,
}: {
	entreprise: Entreprise | null;
}): string | null {
	if (!uniteLegale) {
		return null;
	}

	const nom = new Set();

	if (uniteLegale.pseudonymeUniteLegale) {
		nom.add(uniteLegale.pseudonymeUniteLegale);
	}

	//  on ne garde que le premier prénom
	//  peu remplis et souvent identiques

	if (uniteLegale.prenomUsuelUniteLegale) {
		nom.add(uniteLegale.prenomUsuelUniteLegale);
	} else if (uniteLegale.prenom1UniteLegale) {
		nom.add(uniteLegale.prenom1UniteLegale);
	}

	if (uniteLegale.sigleUniteLegale) {
		nom.add(uniteLegale.sigleUniteLegale);
	}

	if (uniteLegale.nomUsageUniteLegale) {
		nom.add(`${uniteLegale.nomUsageUniteLegale} (${uniteLegale.nomUniteLegale})`);
	} else if (uniteLegale.nomUniteLegale) {
		nom.add(uniteLegale.nomUniteLegale);
	}

	if (uniteLegale.denominationUsuelle1UniteLegale) {
		nom.add(uniteLegale.denominationUsuelle1UniteLegale);
	} else if (uniteLegale.denominationUsuelle2UniteLegale) {
		nom.add(uniteLegale.denominationUsuelle2UniteLegale);
	} else if (uniteLegale.denominationUsuelle3UniteLegale) {
		nom.add(uniteLegale.denominationUsuelle3UniteLegale);
	} else if (uniteLegale.denominationUniteLegale) {
		nom.add(uniteLegale.denominationUniteLegale);
	}

	if (!nom.size) {
		return null;
	}

	return [...nom].join(' ');
}

export function etablissementNomSourceFormat({
	etablissement: e,
}: {
	etablissement: Etablissement;
}) {
	const nom = new Set();

	if (e.denominationUsuelleEtablissement) {
		nom.add(e.denominationUsuelleEtablissement);
	} else if (e.enseigne1Etablissement) {
		nom.add(e.enseigne1Etablissement);
	} else if (e.enseigne2Etablissement) {
		nom.add(e.enseigne2Etablissement);
	} else if (e.enseigne3Etablissement) {
		nom.add(e.enseigne3Etablissement);
	}

	// si aucune information n'est disponible
	if (!nom.size) {
		return null;
	}

	return [...nom].join(' ');
}

export function etablissementNomBuild(
	etab: EtablissementForNomBuild & {
		contributions: Pick<EquipeEtablissementContribution, 'enseigne'>[];
	}
) {
	const entrepriseNom = entrepriseNomSourceFormat({
		entreprise: etab.ul,
	});

	if (!entrepriseNom) {
		return `${etab.siret} (nom inconnu)`;
	}

	const etablissementNom = etablissementNomSourceFormat({
		etablissement: etab,
	});

	const contribution = etab.contributions[0];

	if (!etablissementNom) {
		if (contribution?.enseigne) {
			return `${contribution.enseigne} (${entrepriseNom})`;
		}
		return entrepriseNom;
	}

	if (contribution?.enseigne) {
		return `${contribution.enseigne} - ${etablissementNom} (${entrepriseNom})`;
	}

	return `${etablissementNom} (${entrepriseNom})`;
}

export function etablissementNumeroBuild(etab: Etablissement) {
	return [etab.numeroVoieEtablissement, etab.indiceRepetitionEtablissement]
		.filter(Boolean)
		.join('');
}

export function etablissementVoieNomBuild(etab: Etablissement) {
	return [etab.typeVoieEtablissement, etab.libelleVoieEtablissement].filter(Boolean).join(' ');
}

export function etablissementAdresseBuild(etab: EtablissementForAdresseBuild) {
	const geolocalisation = etab.geoloc?.geolocalisation ?? etab.ban?.geolocalisation ?? null;

	const ban = !!etab.ban?.geolocalisation;

	return {
		numero: etablissementNumeroBuild(etab),
		voieNom: etablissementVoieNomBuild(etab),
		complementAdresse: etab.complementAdresseEtablissement,
		codePostal: etab.codePostalEtablissement,
		communeId: etab.codeCommuneEtablissement,
		cedex: etab.codeCedexEtablissement,
		cedexNom: etab.libelleCedexEtablissement,
		distributionSpeciale: etab.distributionSpecialeEtablissement,
		etrangerCommune: etab.libelleCommuneEtrangerEtablissement,
		etrangerPays: etab.codePaysEtrangerEtablissement,
		commune: etab.libelleCommuneEtrangerEtablissement
			? {
					nom: etab.libelleCommuneEtrangerEtablissement,
				}
			: (etab.commune ?? {
					nom: '?',
				}),
		geolocalisation,
		ban,
	};
}

export function etablissementFermetureDateBuild(etablissement: {
	fermeture: { fermetureDate: Date | null } | null;
	etatAdministratifEtablissement: Etablissement['etatAdministratifEtablissement'];
	dateDebut: Etablissement['dateDebut'];
}) {
	return (
		etablissement.fermeture?.fermetureDate ??
		(etablissement.etatAdministratifEtablissement === 'F' ? etablissement.dateDebut : null)
	);
}

export function entrepriseFermetureDateBuild(entreprise: {
	fermeture: { fermetureDate: Date | null } | null;
	etatAdministratifUniteLegale: Entreprise['etatAdministratifUniteLegale'];
	dateDebut: Entreprise['dateDebut'];
}) {
	return (
		entreprise.fermeture?.fermetureDate ??
		((entreprise.etatAdministratifUniteLegale === 'C' && entreprise.dateDebut) || null)
	);
}

export function etablissementApercuFormat({
	etab,
}: {
	etab: EtablissementForNomBuild &
		EtablissementForAdresseBuild & {
			contributions: Pick<EquipeEtablissementContribution, 'enseigne' | 'clotureDate'>[];
		};
}): EtablissementApercu {
	return {
		id: etab.siret,
		nomAffichage: etablissementNomBuild(etab),
		// TODO l'adresse est "trop complète" pour le mode "Aperçu", car on utilise pas la géoloc
		adresse1: etablissementAdresseBuild(etab),
		actif: etab.etatAdministratifEtablissement === 'A',
		clotureDateContribution: etab.contributions[0]?.clotureDate ?? null,
		siege: !!etab.etablissementSiege,
	};
}

export function etablissementSimpleFormat({
	etab,
}: {
	etab: EtablissementWithRelations & {
		contributions: Pick<
			EquipeEtablissementContribution,
			'enseigne' | 'clotureDate' | 'geolocalisation'
		>[];
		contributionsGeolocs: (Pick<EquipeEtablissementContribution, 'geolocalisation'> & {
			eq: Pick<Equipe, 'nom'>;
		})[];
	};
}): EtablissementSimple {
	const entreprise = etab.ul;

	const entrepriseNom = entrepriseNomSourceFormat({
		entreprise,
	});

	const etablissementNom = etablissementNomSourceFormat({
		etablissement: etab,
	});

	const procedures = entreprise?.procedures ?? [];

	// TODO est-ce qu'il est normal qu'une entreprise manque ?
	const procedure = procedures.length > 0;

	const inpiCaVariation = entreprise.ratiosFinanciers
		? entrepriseCaVariationFormat(entreprise.ratiosFinanciers)
		: null;

	const france = !etab.equipeCommune;

	const emaVariation = etablissementEffectifVariationFormat(
		etab.effectifMoyenMensuels,
		etab.effectifMoyenAnnuel?.effectif
	);

	const contributionGeolocalisationEquipe = etab.contributionsGeolocs?.[0];

	return {
		...etablissementApercuFormat({ etab }),

		nom: etablissementNom,
		nomEntreprise: entrepriseNom ?? 'Entreprise inconnue',
		enseigneContribution: etab.contributions[0]?.enseigne ?? null,
		// geolocalisationContribution: etab.contributions[0]?.geolocalisation ?? null,
		geolocalisationContribution: contributionGeolocalisationEquipe?.geolocalisation ?? null,
		geolocalisationEquipe: contributionGeolocalisationEquipe?.eq?.nom ?? null,
		activiteNaf: etab.nafType ?? null,
		diffusible: etab.statutDiffusionEtablissement === 'O',
		creationDate: etab.dateCreationEtablissement,
		fermetureDate: etablissementFermetureDateBuild(etab),
		procedure,
		inpiCaVariation,
		emaVariation,
		france,
	};
}

export function entrepriseApercuFormat({ ul: entreprise }: { ul: Entreprise }): EntrepriseApercu {
	const entrepriseNom = entrepriseNomSourceFormat({
		entreprise,
	});

	return {
		id: entreprise.siren,
		nomAffichage: entrepriseNom ?? 'Entreprise inconnue',
		actif: entreprise.statutDiffusionUniteLegale === 'O',
	};
}

export function portefeuilleInfosFormat({
	etab,
}: {
	etab: {
		contacts: EquipeEtablissementContact[];
		echanges: EquipeEtablissementEchange[];
		demandes: EquipeEtablissementDemande[];
		rappels: EquipeEtablissementRappel[];
		favoris: DevecoEtablissementFavori[];
	};
}): PortefeuilleInfos {
	return {
		contacts: !!etab.contacts.length,
		echanges: !!etab.echanges.length,
		demandes: !!etab.demandes.length,
		rappels: !!etab.rappels.length,
		favori: !!etab.favoris.length,
	};
}

export function etabAvecPortefeuilleInfosFormat({
	etab,
}: {
	etab: EtablissementWithRelations & {
		contacts: EquipeEtablissementContact[];
		echanges: EquipeEtablissementEchange[];
		demandes: EquipeEtablissementDemande[];
		rappels: EquipeEtablissementRappel[];
		favoris: DevecoEtablissementFavori[];
		contributions:
			| Pick<EquipeEtablissementContribution, 'enseigne' | 'geolocalisation' | 'clotureDate'>[]
			| null;
	};
}): EtablissementWithPortefeuilleInfos {
	return {
		etablissement: etablissementSimpleFormat({ etab }),
		portefeuilleInfos: portefeuilleInfosFormat({ etab }),
	};
}

export function subventionsFormat(subventions: EtablissementSubvention[]) {
	return subventions
		.sort((s1, s2) => s2.conventionDate.getTime() - s1.conventionDate.getTime())
		.map(({ montant, objet, conventionDate, type }) => ({
			objet,
			annee: conventionDate.toISOString().slice(0, 4),
			type,
			montant: `${montant} €`,
		}));
}

export function procedureFormat({ date, famille, nature }: BodaccProcedureCollective) {
	return {
		famille: famille ?? '',
		nature: nature ?? '',
		annee: date ? `${date.getFullYear()}` : '',
		date: date ? `${formatJSDateToHumanFriendly(date)}` : '',
	};
}

export function entrepriseMandatairesFormatMany({
	payload,
}: ApiEntrepriseEntrepriseMandataireSocial) {
	return payload.reduce(
		({ mandatairesPersonnesPhysiques, mandatairesPersonnesMorales }, mandataire) => {
			if (mandataire.type === 'personne_physique') {
				mandatairesPersonnesPhysiques.push({
					fonction: mandataire.fonction,
					contact: {
						id: -1,
						nom: mandataire.nom ?? '',
						prenom: mandataire.prenom ?? '',
						email: '',
						telephone: '',
						telephone2: '',
						naissanceDate: mandataire.date_naissance ? new Date(mandataire.date_naissance) : null,
						//
						// naissanceLieu: mandataire.lieu_naissance,
						// naissancePays: mandataire.pays_naissance,
						// naissancePaysCode: mandataire.code_pays_naissance,
						// nationalite: mandataire.nationalite,
						// nationaliteCode: mandataire.code_nationalite,
					},
				});
			} else {
				// possible coquille chez le fournisseur de données
				if (mandataire.numero_identification !== '000000000') {
					mandatairesPersonnesMorales.push({
						fonction: mandataire.fonction,
						siren: mandataire.numero_identification ?? '',
						nom: mandataire.raison_sociale ?? '',
						//
						// greffeCode: mandataire.code_greffe,
						// greffeLibelle: mandataire.libelle_greffe,
					});
				}
			}

			return { mandatairesPersonnesPhysiques, mandatairesPersonnesMorales };
		},
		{
			mandatairesPersonnesPhysiques: [] as MandatairePersonnePhysiqueFormatted[],
			mandatairesPersonnesMorales: [] as MandatairePersonneMoraleFormatted[],
		}
	);
}

export function entrepriseParticipationFormat(
	participationReseau: NotUndefined<
		Awaited<ReturnType<typeof serviceEquipeEtablissement.participationGet>>
	>
): EntrepriseParticipation {
	return participationReseau.flatMap(({ a, a_siren, b, b_siren }) => {
		const source = a ? `${a} (${a_siren})` : a_siren;
		const cible = b ? `${b} (${b_siren})` : b_siren;

		return [[source, cible]];
	});
}

export function entrepriseBeneficiaireFormatMany({
	payload,
}: ApiEntrepriseEntrepriseBeneficiaireEffectif) {
	return payload.map((beneficiaire) => ({
		nom: beneficiaire.nom ?? '',
		nomUsage: beneficiaire.nom_usage ?? '',
		prenoms: beneficiaire.prenoms?.join(' ') ?? '',
		// on invente une date car on ne connaît pas le jour
		naissanceDate: new Date(
			`${beneficiaire.date_naissance.annee}-${beneficiaire.date_naissance.mois}-01T12:00:00.000Z`
		),
		totalParts: beneficiaire.modalites.detention_de_capital.parts_totale ?? 0,
		nationalite: beneficiaire.nationalite,
		paysResidence: beneficiaire.pays_residence,
	}));
}

export function exerciceFormat({ payload }: ApiEntrepriseEntrepriseExercice) {
	return payload.map((exercice) => {
		const clotureDate = new Date(exercice.date_fin_exercice);
		const chiffreDAffaires = Number(exercice.chiffre_affaires);

		return {
			ca: `${chiffreDAffaires}`,
			annee: `${clotureDate.getFullYear()}`,
			date: `${formatJSDateToHumanFriendly(clotureDate)}`,
		};
	});
}

export function liasseFiscaleFormat(
	liasse: ApiEntrepriseEntrepriseLiasseFiscale
): LiasseFiscaleFormatted {
	const declarations = liasse.payload?.declarations ?? [];

	let imprimeFiliales: any = null;
	let imprimeDetentions: any = null;
	let imprimeResultat: any = null;

	for (const declaration of declarations) {
		if (declaration.numero_imprime === '2058C') {
			imprimeResultat = declaration;
		}

		if (declaration.numero_imprime === '2059F') {
			imprimeDetentions = declaration;
		}

		if (declaration.numero_imprime === '2059G') {
			imprimeFiliales = declaration;
		}

		if (imprimeFiliales && imprimeDetentions && imprimeResultat) {
			break;
		}
	}

	if (!imprimeFiliales && !imprimeDetentions && !imprimeResultat) {
		return {
			estDetenue: null,
			sirensMere: [],
			aFiliales: null,
			filiales: [],
			detentions: [],
		};
	}

	type Donnee =
		| {
				code_absolu: '2003838';
				valeurs: string[];
		  }
		| {
				code_absolu: '2003839';
				valeurs: (string | null)[];
		  }
		| {
				code_absolu: '2003847';
				valeurs: string[];
		  };

	function resultatFormat(imprime: undefined | { donnees: Donnee[] }) {
		const donnees = imprime?.donnees ?? [];

		let estDetenue: boolean | null = null;
		let aFiliales: boolean | null = null;
		let sirensMere: string[] = [];

		for (const donnee of donnees) {
			if (donnee.code_absolu === '2003838') {
				estDetenue = (donnee.valeurs ?? [])[0] === 'FIL';
			}

			if (donnee.code_absolu === '2003839') {
				sirensMere =
					donnee.valeurs?.flatMap((siret: string | null) => (siret ? [siret.slice(0, 9)] : [])) ??
					[];
			}

			if (donnee.code_absolu === '2003847') {
				aFiliales = (donnee.valeurs ?? [])[0] === '1';
			}
		}

		return {
			estDetenue,
			sirensMere,
			aFiliales,
		};
	}

	function detentionsFormat(imprime: any) {
		const donnees = imprime?.donnees ?? [];

		let noms: (string | null)[] = [];
		let sirens: (string | null)[] = [];
		let parts: (string | null)[] = [];
		let pourcentages: (string | null)[] = [];

		for (const donnee of donnees) {
			if (donnee.code_absolu === '2005722') {
				noms = donnee.valeurs ?? [];
			}

			if (donnee.code_absolu === '2005724') {
				sirens = donnee.valeurs ?? [];
			}

			if (donnee.code_absolu === '2005725') {
				parts = donnee.valeurs ?? [];
			}

			if (donnee.code_absolu === '2005726') {
				pourcentages = donnee.valeurs ?? [];
			}
		}

		return noms?.flatMap((nom: string | null, i: number) => {
			return nom
				? [
						{
							nom,
							siren: sirens[i]?.replaceAll(' ', '')?.slice(0, 9) ?? null,
							parts: parts[i] ? Number(parts[i]) / 100 : null,
							pourcentage: pourcentages[i] ? Number(pourcentages[i]) / 100 : null,
						},
					]
				: [];
		});
	}

	function filialesFormat(imprime: any) {
		let sirens: (string | null)[] = [];
		let noms: (string | null)[] = [];
		// const adresses: (string | null)[] = [];
		// const codesPostaux: (string | null)[] = [];
		// const communes: (string | null)[] = [];
		// const pays: (string | null)[] = [];
		let pourcentages: (string | null)[] = [];

		const donnees = imprime?.donnees ?? [];

		for (const donnee of donnees) {
			if (donnee.code_absolu === '2005836') {
				sirens = donnee.valeurs ?? [];
			}

			if (donnee.code_absolu === '2005834') {
				noms = donnee.valeurs ?? [];
			}

			// if (donnee.code_absolu === '2005837') {
			// 	adresses = donnee.valeurs ?? [];
			// }

			// if (donnee.code_absolu === '2005841') {
			// 	codesPostaux = donnee.valeurs ?? [];
			// }

			// if (donnee.code_absolu === '2005842') {
			// 	communes = donnee.valeurs ?? [];
			// }

			// if (donnee.code_absolu === '2005843') {
			// 	pays = donnee.valeurs ?? [];
			// }

			if (donnee.code_absolu === '2005844') {
				pourcentages = donnee.valeurs ?? [];
			}
		}

		return noms?.flatMap((nom: string | null, i: number) => {
			return nom
				? [
						{
							nom,
							pourcentage: pourcentages[i] ? Number(pourcentages[i]) / 100 : null,
							// adresse: adresses[i] ? adresses[i].replace(/^0+/, '') : null,
							// codePostal: codesPostaux[i],
							// commune: communes[i],
							// pays: pays[i],
							siren: sirens[i]?.replaceAll(' ', '').slice(0, 9) ?? null,
						},
					]
				: [];
		});
	}

	const filiales = filialesFormat(imprimeFiliales);
	const detentions = detentionsFormat(imprimeDetentions);
	const resultat = resultatFormat(imprimeResultat);

	return {
		...resultat,
		filiales,
		detentions,
	};
}

function etablissementEffectifVariationFormat(
	effectifs: Pick<EtablissementEffectifMoyenMensuel, 'effectif' | 'date'>[],
	effectifMoyenAnnuel?: number | null
) {
	const anneeNM1 = effectifs[0];
	if (!anneeNM1?.effectif || anneeNM1.effectif <= 0) {
		return null;
	}

	return (
		Math.round((10000 * ((effectifMoyenAnnuel ?? 0) - anneeNM1.effectif)) / anneeNM1.effectif) / 100
	);
}

// TODO refactorer ce bazar
export function etabWithPersoFormat({
	etab,
	freres,
}: NotUndefined<
	Awaited<ReturnType<typeof serviceEquipeEtablissement.get>>
>): EtablissementPageComplete | null {
	if (!etab) {
		return null;
	}

	const dateDernierTraitementEtablissement = etab.dateDernierTraitementEtablissement;

	const sireneTraitementDate = dateDernierTraitementEtablissement
		? new Date(dateDernierTraitementEtablissement)
		: null;

	const qpv2024 = etab.qpv2024?.qpv;
	const qpv2015 = etab.qpv2015?.qpv;

	const qpv2024Nom = qpv2024 ? `${qpv2024.nom} (QPV)` : '';
	const qpv2015Nom = qpv2015 ? `${qpv2015.nom} (QPV 2015)` : '';

	const zrr =
		etab.commune?.zrrTypeId === 'C'
			? 'ZRR'
			: etab.commune?.zrrTypeId === 'P'
				? 'ZRR (partielle)'
				: '';
	const acv = etab.acv ? `ACV` : '';
	const pvd = etab.pvd ? `PVD` : '';
	const va = etab.va ? `VA` : '';
	const afr =
		etab.afr?.integral === true ? 'AFR' : etab.afr?.integral === false ? 'AFR (partielle)' : '';
	const territoireIndustrie = etab.ti?.ti?.nom
		? `${etab.ti?.ti?.nom} (Territoire d'industrie)`
		: '';

	const etablissementExtra: EtablissementExtra = {
		categorieNaf: etab.nafType?.parent?.nom ?? null,
		categorieNafId: etab.nafType?.parent?.id?.substring(0, 2) ?? null,
		emms: etab.effectifMoyenMensuels.map(({ date, effectif }) => ({
			annee: date.getFullYear(),
			mois: date.getMonth() + 1,
			valeur: effectif ?? 0,
		})),
		ema: etab.effectifMoyenAnnuel
			? {
					annee: etab.effectifMoyenAnnuel.date.getFullYear(),
					mois: etab.effectifMoyenAnnuel.date.getMonth() + 1,
					valeur: etab.effectifMoyenAnnuel.effectif ?? 0,
				}
			: null,
		zonages: [qpv2024Nom, qpv2015Nom, zrr, acv, pvd, va, territoireIndustrie, afr].filter(Boolean),
		acv: Boolean(acv),
		pvd: Boolean(pvd),
		va: Boolean(va),
		subventions: subventionsFormat(etab.subventions),
		sireneTraitementDate,
		// TODO envoyer tous les noms d'enseigne
		enseigne: etab.enseigne1Etablissement,
	};

	const entrepriseData = entrepriseDataFormat({
		ul: etab.ul,
	});

	const equipeEtablissement = equipeEtablissementFormat({
		etab,
		freres,
	});

	return {
		etablissement: etablissementSimpleFormat({ etab }),
		portefeuilleInfos: portefeuilleInfosFormat({ etab }),
		etablissementExtra,
		entreprise: entrepriseData,
		equipeEtablissement,
	};
}

function entrepriseCaVariationFormat(
	ratiosFinanciers: Pick<EntrepriseRatioFinancier, 'dateClotureExercice' | 'chiffreDAffaires'>[]
) {
	if (ratiosFinanciers.length < 2) {
		return null;
	}

	// on prend les deux dernières années
	const [anneeN0, anneeNM1] = ratiosFinanciers
		.sort((a, b) => b.dateClotureExercice.getTime() - a.dateClotureExercice.getTime())
		.slice(0, 2);

	if (!anneeNM1?.chiffreDAffaires || anneeNM1.chiffreDAffaires <= 0) {
		return null;
	}

	return (
		Math.round(
			(10000 * ((anneeN0.chiffreDAffaires ?? 0) - anneeNM1.chiffreDAffaires)) /
				anneeNM1.chiffreDAffaires
		) / 100
	);
}

function entrepriseRatioFinancierFormat({
	chiffreDAffaires,
	margeBrute,
	ebe,
	resultatNet,
	dateClotureExercice,
}: Pick<
	EntrepriseRatioFinancier,
	'chiffreDAffaires' | 'margeBrute' | 'ebe' | 'resultatNet' | 'dateClotureExercice'
>) {
	return {
		ca: `${chiffreDAffaires}`,
		marge: `${margeBrute}`,
		ebe: `${ebe}`,
		resultatNet: `${resultatNet}`,
		annee: `${dateClotureExercice?.getFullYear()}`,
	};
}

export function entrepriseDataFormat({
	ul: entreprise,
}: {
	ul:
		| NotUndefined<
				NotUndefined<Awaited<ReturnType<typeof serviceEquipeEtablissement.get>>>['etab']
		  >['ul']
		| null;
}): EntrepriseData | null {
	if (!entreprise) {
		return null;
	}

	const etablissementsActifsFrance = entreprise?.etabs;
	const etablissementsActifsTerritoire = etablissementsActifsFrance?.filter(
		({ equipeCommune }) => equipeCommune
	);
	const etablissementsActifs = etablissementsActifsFrance?.length
		? {
				france: etablissementsActifsFrance?.length,
				territoire: etablissementsActifsTerritoire?.length,
			}
		: null;

	const etablissementsActifsTerritoireAvecEffectif = etablissementsActifsTerritoire?.filter(
		({ effectifMoyenAnnuel }) => isDefined(effectifMoyenAnnuel?.effectif)
	);

	const effectifTotalTerritoire = etablissementsActifsTerritoireAvecEffectif?.length
		? etablissementsActifsTerritoireAvecEffectif.reduce(
				(sum, { effectifMoyenAnnuel }) => sum + (effectifMoyenAnnuel?.effectif ?? 0),
				0
			)
		: null;

	const inpi = entreprise.ratiosFinanciers?.map(entrepriseRatioFinancierFormat);

	const inpiCaVariation = entreprise.ratiosFinanciers
		? entrepriseCaVariationFormat(entreprise.ratiosFinanciers)
		: null;

	const procedures = entreprise.procedures ?? [];

	const procedure = procedures.length > 0;

	const entrepriseNom = entrepriseNomSourceFormat({
		entreprise,
	});

	const mandataires =
		isApiEntrepriseSourceRecent(entreprise.mandataires) && entreprise.mandataires
			? entrepriseMandatairesFormatMany(entreprise.mandataires)
			: null;

	const egapro = entreprise.egapro[0]
		? {
				annee: entreprise.egapro[0].annee,
				note: entreprise.egapro[0].noteIndex,
				ecartRemunerations: entreprise.egapro[0].noteEcartRemuneration,
				ecartAugmentations: entreprise.egapro[0].noteEcartTauxAugmentation,
				retourMaternite: entreprise.egapro[0].noteRetourCongeMaternite,
				hautesRemunerations: entreprise.egapro[0].noteHautesRemunerations,
				cadres: entreprise.egapro[0].cadresPourcentageFemmes,
				instances: entreprise.egapro[0].membresPourcentageFemmes,
			}
		: null;

	return {
		entrepriseId: entreprise.siren,
		entrepriseNom: entrepriseNom ?? '',
		siegeAdresse: entreprise.siege ? etablissementAdresseBuild(entreprise.siege) : undefined,
		categorieJuridique: entreprise.catJur?.parent?.nom ?? '?',
		categorieJuridiqueId: entreprise.catJur?.parent?.id ?? '?',
		formeJuridique: entreprise.catJur?.nom ?? '?',
		formeJuridiqueId: entreprise.catJur?.id ?? '?',
		inpi,
		inpiCaVariation,
		micro: !!entreprise.micro,
		ess: !!entreprise.ess,
		exercices:
			isApiEntrepriseSourceRecent(entreprise.exercices) && entreprise.exercices
				? exerciceFormat(entreprise.exercices)
				: null,
		mandatairesPersonnesPhysiques: mandataires?.mandatairesPersonnesPhysiques ?? null,
		mandatairesPersonnesMorales: mandataires?.mandatairesPersonnesMorales ?? null,
		beneficiairesEffectifs:
			isApiEntrepriseSourceRecent(entreprise.benef) && entreprise.benef
				? entrepriseBeneficiaireFormatMany(entreprise.benef)
				: null,
		liasseFiscale:
			isApiEntrepriseSourceRecent(entreprise.liasse) && entreprise.liasse
				? liasseFiscaleFormat(entreprise.liasse)
				: null,
		procedure,
		procedures: procedures.map(procedureFormat),
		creationDate: entreprise.dateCreationUniteLegale,
		fermetureDate: entrepriseFermetureDateBuild(entreprise),
		effectif: effectifCodeToDescription(entreprise.trancheEffectifsUniteLegale ?? '') ?? 'Inconnu',
		active: entreprise.etatAdministratifUniteLegale === 'A',
		categorieNaf: entreprise.nafType?.parent?.nom ?? null,
		categorieNafId: entreprise.nafType?.parent?.id?.substring(0, 2) ?? null,
		categorieEntreprise: entreprise.categorieEntreprise ?? 'Inconnu',
		activiteNaf: entreprise.nafType,
		etablissementsActifs,
		effectifTotalTerritoire,
		egapro,
	};
}

function equipeEtablissementFormat({
	etab,
	freres,
}: {
	etab: {
		contributions: EquipeEtablissementContributionWithModification[];
		preds: {
			pred: Parameters<typeof etabAvecPortefeuilleInfosFormat>[0]['etab'];
		}[];
		succs: {
			succ: Parameters<typeof etabAvecPortefeuilleInfosFormat>[0]['etab'];
		}[];
	} & {
		contacts: {
			fonction: string | null;
			ctct: Parameters<typeof contactFormat>[0];
		}[];
		echanges: {
			echange: EchangeWithModification;
		}[];
		demandes: {
			demande: DemandeWithEchangeDemandeWithEchange;
		}[];
		rappels: {
			rappel: RappelWithAffecteCompte;
		}[];
		etiquettes: {
			etiquette: Etiquette;
		}[];
		transfos: {
			crea: EtablissementCreationForNomBuild & {
				creas: unknown[];
			};
		}[];
		ressources: RessourceEtablissement[];
		occups: {
			local: Parameters<typeof eqLocSimpleFormat>[0];
		}[];
	};
	freres: Awaited<
		ReturnType<typeof dbQueryEquipeEtablissement.etablissementWithEqEtabWithRelationsGetMany>
	>;
}) {
	const contacts = etab.contacts.map(({ fonction, ctct }) => ({
		...contactFormat(ctct),
		lien: {
			type: 'etablissement',
			fonction,
		},
	}));

	const echanges = etab.echanges.map(({ echange }) => {
		return {
			...echange,
			modification: elementToModificationADate(echange),
		};
	});

	const demandes = etab.demandes.map(({ demande }) => {
		const { echanges, ...demandeLight } = demande;
		return {
			...demandeLight,
			nombreEchanges: echanges.length,
		};
	});

	const rappels = etab.rappels.map(({ rappel: { affecteCompte, ...rappel } }) => ({
		...rappel,
		affecte: affecteCompte,
	}));

	const etiquettes = etab.etiquettes
		.map(({ etiquette }) => etiquette)
		.sort((a, b) => a.nom.localeCompare(b.nom));

	const etabCreaTransformes = etab.transfos.reduce(
		(etabCreaTransformes: EtabCreaTransformes[], transformation) => {
			if (transformation.crea && !transformation.crea.creas.length) {
				console.logError(
					`Impossible de trouver un créateur pour la transformation d'établissement #${transformation.crea.id}`
				);

				return etabCreaTransformes;
			}

			etabCreaTransformes.push({
				etabCreaId: transformation.crea.id,
				nom: etabCreaNomBuild(transformation.crea),
			});

			return etabCreaTransformes;
		},
		[]
	);

	const contribution = etab.contributions[0] ?? null;

	const { activites, localisations, motsCles } = etiquettesByTypeBuild(etiquettes);

	const equipeEtablissement = {
		description: contribution?.description ?? '',
		clotureDateContribution: contribution?.clotureDate ?? null,
		etiquettes: {
			activites,
			localisations,
			motsCles,
		},
		contacts,
		echanges,
		rappels,
		demandes,
		modification: elementToModificationADate(contribution),
		freres: freres.map((etab) => etabAvecPortefeuilleInfosFormat({ etab })),
		// TODO chercher un moyen de garantir qu'on a 0 ou 1 eqEtab pour les prédécesseurs
		predecesseurs: etab.preds
			.filter((p) => p.pred)
			.map((p) => etabAvecPortefeuilleInfosFormat({ etab: p.pred })),
		// TODO chercher un moyen de garantir qu'on a 0 ou 1 eqEtab pour les successeurs
		successeurs: etab.succs
			.filter((p) => p.succ)
			.map((s) => etabAvecPortefeuilleInfosFormat({ etab: s.succ })),
		ressources: etab.ressources,
		occupations: filterNullish(etab.occups.map((o) => eqLocSimpleFormat(o.local))),
		etabCreaTransformes,
	};
	return equipeEtablissement;
}

export function etablissementsFormatMany({
	etablissements,
	etablissementIds,
}: {
	etablissements: EtablissementWithEquipeEtablissementRelations[];
	etablissementIds: Etablissement['siret'][];
}) {
	// On conserve le tri original des ids ce que ne fait pas In()
	return etablissements
		.sort((e1, e2) => etablissementIds.indexOf(e1.siret) - etablissementIds.indexOf(e2.siret))
		.map((etab) => etabAvecPortefeuilleInfosFormat({ etab }));
}

export function entreprisesApercuFormatMany({
	entreprises,
	entrepriseIds,
}: {
	entreprises: Entreprise[];
	entrepriseIds: Entreprise['siren'][];
}) {
	// On conserve le tri original des ids ce que ne fait pas In()
	return entreprises
		.sort((e1, e2) => entrepriseIds.indexOf(e1.siren) - entrepriseIds.indexOf(e2.siren))
		.map((ul) => entrepriseApercuFormat({ ul }));
}

export function actionEquipeEtablissementFormatMany(
	actionEquipeEtablissement: ActionEquipeEtablissement
) {
	return {
		...actionEquipeEtablissement,
		date: actionEquipeEtablissement.createdAt.getTime(),
	};
}
