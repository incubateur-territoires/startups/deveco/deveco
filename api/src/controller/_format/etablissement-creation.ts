import { etiquettesByTypeBuild, elementToModificationADate, contactFormat } from './equipe';
import { etablissementNomBuild } from './etablissements';

import {
	EtablissementCreationForNomBuild,
	EtablissementCreationSimple,
	EtablissementCreationPageComplete,
} from '../../controller/types';
import { QueryResult, NotUndefined, EtablissementCreation, Qpv2024 } from '../../db/types';
import * as dbQueryEtablissementCreation from '../../db/query/etablissement-creation';

export function etabCreaIdBuild(id: EtablissementCreation['id']): string {
	return id.toString().padStart(6, '0');
}

export function etabCreaNomBuild({ id, creas }: EtablissementCreationForNomBuild): string {
	const createurs = creas
		.map((createur) => {
			const { nom, prenom } = createur.ctct;
			let n = '';

			if (prenom && nom) {
				n = `${prenom} ${nom}`;
			} else if (prenom) {
				n = prenom;
			} else if (nom) {
				n = nom;
			}

			return n;
		})
		.join(', ');

	return etabCreaIdBuild(id) + (createurs ? ` (${createurs})` : '');
}

export function etabCreaApercuFormat(
	etabCrea: Pick<EtablissementCreation, 'id'> & EtablissementCreationForNomBuild
) {
	return {
		id: etabCrea.id,
		nomAffichage: etabCreaNomBuild(etabCrea),
	};
}

export function etabCreaSimpleFormat(
	etabCrea: QueryResult<typeof dbQueryEtablissementCreation.etabCreaWithSimpleRelationsGetMany>[0]
): EtablissementCreationSimple {
	const { id, creas } = etabCrea;

	const etiquettes = etabCrea.etiquettes
		.map((ete) => ete.etiquette)
		.sort((a, b) => a.nom.localeCompare(b.nom));

	const { activites } = etiquettesByTypeBuild(etiquettes);

	return {
		id,
		nom: etabCreaNomBuild(etabCrea),
		etiquettes: {
			activites,
		},
		futureEnseigne: etabCrea.enseigne ?? '',
		demandes: etabCrea.demandes.map(({ demande }) => demande),
		createurs: creas.map(({ fonction, diplomeNiveauId, professionSituationId, ctct }) => ({
			...contactFormat(ctct),
			lien: {
				type: 'createur',
				fonction,
				niveauDiplome: diplomeNiveauId ?? '',
				situationProfessionnelle: professionSituationId ?? '',
			},
		})),
		creationAbandonnee: etabCrea.creationAbandonnee,
	};
}

export function etabCreaFormat(
	etabCrea: NotUndefined<QueryResult<typeof dbQueryEtablissementCreation.etabCreaWithRelationsGet>>,
	qpvs: Pick<Qpv2024, 'id' | 'nom'>[]
): EtablissementCreationPageComplete {
	const ec = {
		...etabCrea,
		echanges: etabCrea.echanges.map(({ echange, ...rest }) => {
			return {
				...rest,
				echange: {
					...echange,
					modification: elementToModificationADate(echange),
				},
			};
		}),
	};

	const etiquettes = ec.etiquettes.map((ete) => ete.etiquette);

	const { activites, localisations, motsCles } = etiquettesByTypeBuild(etiquettes);

	const etabCreaFormatted = etabCreaSimpleFormat(ec);

	const zonagesPrioritaires = qpvs.map((q) => `${q.nom}`);

	return {
		...etabCreaFormatted,
		communes: etabCrea.communes.map(({ commune }) => commune),
		description: ec.description,
		commentaire: ec.commentaire,
		echanges: ec.echanges.map(({ echange }) => echange),
		rappels: ec.rappels.map(({ rappel: { affecteCompte, ...rappel } }) => ({
			...rappel,
			affecte: affecteCompte,
		})),
		demandes: ec.demandes.map(({ demande }) => {
			const { echanges, ...demandeLight } = demande;
			return {
				...demandeLight,
				nombreEchanges:
					echanges?.map(({ echange }) => echange).filter((echange) => !echange.suppressionDate)
						.length ?? 0,
			};
		}),
		etiquettes: {
			activites,
			localisations,
			motsCles,
		},
		zonagesPrioritaires,
		// transformation
		etablissement: etabCrea.transfo?.etab
			? {
					id: etabCrea.transfo.etab.siret,
					nomAffichage: etablissementNomBuild(etabCrea.transfo.etab),
				}
			: null,
		modification: elementToModificationADate(ec),
		contactOrigine: ec.contactOrigine,
		contactDate: ec.contactDate,
		projetType: ec.projetType,
		secteurActivite: ec.secteurActivite,
		sourceFinancement: ec.sourceFinancement,
		formeJuridique: ec.categorieJuridiqueEnvisagee ?? '',
		contratAccompagnement: ec.contratAccompagnement,
		rechercheLocal: ec.rechercheLocal,
		ressources: ec.ressources ?? [],
	};
}
