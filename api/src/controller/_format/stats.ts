import { etablissementNomBuild } from './etablissements';

import { NotUndefined, QueryResult, Etablissement, Demande, Rappel } from '../../db/types';
import * as dbQueryEquipeEtablissement from '../../db/query/equipe-etablissement';

export function demandeNouvelleOuverteClotureCount({
	demandes,
	debut: debut,
	fin,
}: {
	demandes: Demande[];
	debut: Date;
	fin: Date;
}) {
	const [nouvellesDemandes, ouvertesDemandesTypes, clotureesDemandes] = demandes.reduce<
		[number, { type: string }[], number]
	>(
		([nouvelles, ouvertes, cloturees], demande) => {
			if (demande.creationDate && demande.creationDate >= debut) {
				nouvelles = nouvelles + 1;
			}

			if (!demande.clotureDate || demande.clotureDate < debut) {
				ouvertes.push({ type: demande.typeId });
			}

			if (demande.clotureDate && demande.clotureDate >= debut && demande.clotureDate < fin) {
				cloturees = cloturees + 1;
			}

			return [nouvelles, ouvertes, cloturees] as [number, { type: string }[], number];
		},
		[0, [], 0]
	);

	return {
		nouvelles: nouvellesDemandes,
		ouvertes: ouvertesDemandesTypes,
		cloturees: clotureesDemandes,
	};
}

export function rappelNouveauOuvertClotureCount({
	rappels,
	debut,
	fin,
}: {
	rappels: Rappel[];
	debut: Date;
	fin: Date;
}) {
	const [nouveauxRappels, expiresRappels, cloturesRappels] = rappels.reduce<
		[number, number, number]
	>(
		([nouveaux, ouverts, clotures], rappel) => {
			if (rappel.creationDate && rappel.creationDate >= debut) {
				nouveaux = nouveaux + 1;
			}

			if (new Date(rappel.date) >= debut && new Date(rappel.date) < fin) {
				ouverts = ouverts + 1;
			}

			if (rappel.clotureDate && rappel.clotureDate >= debut && rappel.clotureDate < fin) {
				clotures = clotures + 1;
			}

			return [nouveaux, ouverts, clotures];
		},
		[0, 0, 0]
	);

	return {
		nouveaux: nouveauxRappels,
		expires: expiresRappels,
		clotures: cloturesRappels,
	};
}

interface EtablissementOuvertFerme {
	siret: Etablissement['siret'];
	nomAffichage: string;
	trancheEffectifs: string;
}

type EquipeEtablissementOuvertFerme = NotUndefined<
	QueryResult<typeof dbQueryEquipeEtablissement.eqEtabOuvertFermeWithRelationsGetMany>[0]
>;

export function eqEtabFermeFormat(etab: EquipeEtablissementOuvertFerme): EtablissementOuvertFerme {
	return {
		siret: etab.siret,
		nomAffichage: etablissementNomBuild(etab),
		trancheEffectifs: `${etab.effectifMoyenAnnuel?.effectif ?? '?'}`,
	};
}

export function eqEtabOuvertFormat(etab: EquipeEtablissementOuvertFerme): EtablissementOuvertFerme {
	return {
		siret: etab.siret,
		nomAffichage: etablissementNomBuild(etab),
		trancheEffectifs: '?',
	};
}
