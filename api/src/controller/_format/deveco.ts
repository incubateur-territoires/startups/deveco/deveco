import {
	Compte,
	Deveco,
	DevecoWithRelations,
	Equipe,
	EquipePourAdmin,
	NotUndefined,
	QueryResult,
	TerritoireCategorie,
} from '../../db/types';
import * as dbQueryEquipe from '../../db/query/equipe';

interface UtilisateurBase {
	aUnMotDePasse: boolean | null;
	montrerTutoriel: boolean | null;
}

type UtilisateurDeveco = UtilisateurBase & {
	compte: Pick<Compte, 'id' | 'email' | 'nom' | 'prenom' | 'actif' | 'cgu'> & {
		typeId: 'deveco';
	};
	equipe: Pick<Equipe, 'id' | 'nom'> & { isFrance: boolean; hasCarto: boolean };
};

type UtilisateurAdmin = UtilisateurBase & {
	compte: Pick<Compte, 'id' | 'email' | 'nom' | 'prenom' | 'actif' | 'cgu'> & {
		typeId: 'superadmin';
	};
};

export type Utilisateur = UtilisateurAdmin | UtilisateurDeveco;

// TODO vérifier ce dont a réellement besoin le front dans les pages Admin/Utilisateurs
type UtilisateurPourAdmin = {
	id: Deveco['id'];
	identifiant: Compte['identifiant'];
	email: Compte['email'];
	nom: Compte['nom'] | null;
	prenom: Compte['prenom'] | null;
	aUnMotDePasse: boolean;
	montrerTutoriel?: boolean;
	derniereConnexion: Compte['connexionAt'] | null;
	derniereActionAuthentifiee: Compte['authAt'] | null;
	urlAcces: string | null;
	actif: boolean;
} & (
	| {
			compteTypeId: 'superadmin';
	  }
	| {
			compteTypeId: 'deveco';
			equipe: Equipe['nom'];
			bienvenueEmail: Deveco['bienvenueEmail'];
	  }
);

export function formatEquipe(
	equipe: NotUndefined<QueryResult<typeof dbQueryEquipe.equipeWithTerritoireWithNomGetMany>[0]>
): EquipePourAdmin {
	let territoireCategorie: TerritoireCategorie | null = null;
	let territoireNom;

	switch (equipe.territoireTypeId) {
		case 'commune':
			territoireCategorie = equipe.groupement ? 'intercommunal' : 'communal';
			territoireNom = equipe.communes.map((c) => c.commune.nom);
			break;
		case 'metropole':
			territoireCategorie = 'métropolitain';
			territoireNom = equipe.metropoles.map((c) => c.metropole.nom);
			break;
		case 'epci':
			territoireCategorie = 'intercommunal';
			territoireNom = equipe.epcis.map((c) => c.epci.nom);
			break;
		case 'petr':
			territoireCategorie = 'intercommunal';
			territoireNom = equipe.petrs.map((c) => c.petr.nom);
			break;
		case 'territoire_industrie':
			territoireCategorie = 'intercommunal';
			territoireNom = equipe.territoireIndustries.map((c) => c.territoireIndustrie.nom);
			break;
		case 'departement':
			territoireCategorie = equipe.groupement ? 'interdépartemental' : 'départemental';
			territoireNom = equipe.departements.map((c) => c.departement.nom);
			break;
		case 'region':
			territoireCategorie = equipe.groupement ? 'interrégional' : 'régional';
			territoireNom = equipe.regions.map((c) => c.region.nom);
			break;
		case 'france':
			territoireCategorie = 'national';
			territoireNom = ['France'];
			break;
		default:
			throw new Error(`type d'équipe inconnu: ${equipe.territoireTypeId}`);
	}

	return {
		id: equipe.id,
		nom: equipe.nom,
		groupement: equipe.groupement,
		territoireType: { nom: equipe.territoireType.nom },
		type: { nom: equipe.type.nom },
		territoireCategorie,
		territoireNom: territoireNom.join(', ') ?? '',
		siret: equipe.siret,
	};
}

export function utilisateurFormat(jwtPayload: Utilisateur): Utilisateur {
	const compte = jwtPayload.compte;

	if (compte.typeId === 'deveco') {
		const { equipe, aUnMotDePasse, montrerTutoriel } = jwtPayload as UtilisateurDeveco;
		return {
			compte,
			equipe: {
				id: equipe.id,
				nom: equipe.nom,
				isFrance: equipe.isFrance,
				hasCarto: equipe.hasCarto,
			},
			aUnMotDePasse,
			montrerTutoriel,
		};
	} else {
		const { aUnMotDePasse, montrerTutoriel } = jwtPayload as UtilisateurAdmin;
		return {
			compte,
			aUnMotDePasse,
			montrerTutoriel,
		};
	}
}

export function utilisateurPourAdminFormat(deveco: DevecoWithRelations): UtilisateurPourAdmin {
	const appUrl = process.env.APP_URL || '';

	const { compte, equipe } = deveco;

	return {
		id: compte.id,
		identifiant: compte.identifiant,
		email: compte.email,
		nom: compte.nom,
		prenom: compte.prenom,
		aUnMotDePasse: !!deveco.motDePasse,
		montrerTutoriel: !compte.connexionAt,
		compteTypeId: 'deveco',
		equipe: equipe.nom,
		bienvenueEmail: deveco.bienvenueEmail,
		derniereConnexion: compte.connexionAt,
		derniereActionAuthentifiee: compte.authAt,
		urlAcces: compte.cle ? `${appUrl}/auth/jwt/${compte.cle}` : '',
		actif: !!compte.actif,
	};
}
