import { CtxDeveco } from '../middleware/auth';
import { Local } from '../db/types';
import { serviceEquipeLocal } from '../service/equipe-local';
import { controllerEchange } from '../controller/echange';
import { controllerDemande } from '../controller/demande';
import { controllerRappel } from '../controller/rappel';

export const controllerLocauxSuivi = {
	async echangeAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, echange } = await controllerEchange.echangeAdd(
			ctx,
			'equipe_local_echange'
		);

		const localId = ctx.params.localId as Local['id'];

		await serviceEquipeLocal.echangeAdd({
			evenementId,
			equipeId,
			localId,
			echange,
		});

		return next(ctx);
	},

	async echangeUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, echange } = await controllerEchange.echangeUpdate(
			ctx,
			'equipe_local_echange'
		);

		const localId = ctx.params.localId as Local['id'];

		await serviceEquipeLocal.echangeUpdate({
			evenementId,
			equipeId,
			localId,
			echange,
		});

		return next(ctx);
	},

	async echangeRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const result = await controllerEchange.echangeRemove(ctx, 'equipe_local_echange');

		if (!result) {
			return next(ctx);
		}

		const { evenementId, equipeId, echangeId } = result;

		const localId = ctx.params.localId as Local['id'];

		await serviceEquipeLocal.echangeRemove({
			evenementId,
			equipeId,
			localId,
			echangeId,
		});

		return next(ctx);
	},

	async demandeAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, demande } = await controllerDemande.demandeAdd(
			ctx,
			'equipe_local_demande'
		);

		const localId = ctx.params.localId as Local['id'];

		await serviceEquipeLocal.demandeAdd({
			evenementId,
			equipeId,
			localId,
			demande,
		});

		return next(ctx);
	},

	async demandeUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, demande } = await controllerDemande.demandeUpdate(
			ctx,
			'equipe_local_demande'
		);

		const localId = ctx.params.localId as Local['id'];

		await serviceEquipeLocal.demandeUpdate({
			evenementId,
			equipeId,
			localId,
			demande,
		});

		return next(ctx);
	},

	async demandeCloture(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, demande } = await controllerDemande.demandeCloture(
			ctx,
			'equipe_local_demande'
		);

		const localId = ctx.params.localId as Local['id'];

		await serviceEquipeLocal.demandeCloture({
			evenementId,
			equipeId,
			localId,
			demande,
		});

		return next(ctx);
	},

	async demandeReouverture(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, demande } = await controllerDemande.demandeReouverture(
			ctx,
			'equipe_local_demande'
		);

		const localId = ctx.params.localId as Local['id'];

		await serviceEquipeLocal.demandeReouverture({
			evenementId,
			equipeId,
			localId,
			demande,
		});

		return next(ctx);
	},

	async demandeRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const result = await controllerDemande.demandeRemove(ctx, 'equipe_local_demande');

		if (!result) {
			return next(ctx);
		}

		const { evenementId, equipeId, demandeId } = result;

		const localId = ctx.params.localId as Local['id'];

		await serviceEquipeLocal.demandeRemove({
			evenementId,
			equipeId,
			localId,
			demandeId,
		});

		return next(ctx);
	},

	async rappelAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, rappel } = await controllerRappel.rappelAdd(
			ctx,
			'equipe_local_rappel'
		);

		const localId = ctx.params.localId as Local['id'];

		await serviceEquipeLocal.rappelAdd({
			evenementId,
			equipeId,
			localId,
			rappel,
		});

		return next(ctx);
	},

	async rappelUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, rappel } = await controllerRappel.rappelUpdate(
			ctx,
			'equipe_local_rappel'
		);

		const localId = ctx.params.localId as Local['id'];

		await serviceEquipeLocal.rappelUpdate({
			evenementId,
			equipeId,
			localId,
			rappel,
		});

		return next(ctx);
	},

	async rappelCloture(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, rappel } = await controllerRappel.rappelCloture(
			ctx,
			'equipe_local_rappel'
		);

		const localId = ctx.params.localId as Local['id'];

		await serviceEquipeLocal.rappelCloture({
			evenementId,
			equipeId,
			localId,
			rappel,
		});

		return next(ctx);
	},

	async rappelReouverture(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const { evenementId, equipeId, rappel } = await controllerRappel.rappelReouverture(
			ctx,
			'equipe_local_rappel'
		);

		const localId = ctx.params.localId as Local['id'];

		await serviceEquipeLocal.rappelReouverture({
			evenementId,
			equipeId,
			localId,
			rappel,
		});

		return next(ctx);
	},

	async rappelRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const result = await controllerRappel.rappelRemove(ctx, 'equipe_local_rappel');

		if (!result) {
			return next(ctx);
		}

		const { evenementId, equipeId, rappelId } = result;

		const localId = ctx.params.localId as Local['id'];

		await serviceEquipeLocal.rappelRemove({
			evenementId,
			equipeId,
			rappelId,
			localId,
		});

		return next(ctx);
	},
};
