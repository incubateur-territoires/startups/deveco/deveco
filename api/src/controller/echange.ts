import { StatusCodes } from 'http-status-codes';

import { EchangeAddInput, EchangeUpdateInput } from './types';

import { Demande, EchangeTypeId, Evenement } from '../db/types';
import { CtxDeveco } from '../middleware/auth';
import { querystringToArray, querystringToNumber } from '../lib/querystring';
import * as dbQueryEvenement from '../db/query/evenement';
import * as dbQueryEchange from '../db/query/echange';
import * as dbQueryDemande from '../db/query/demande';
import * as dbQueryBrouillon from '../db/query/brouillon';
import { serviceEchange } from '../service/echange';
import { serviceBrouillon } from '../service/brouillon';

export const controllerEchange = {
	async echangeAdd(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const devecoId = deveco.id;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		// TODO utiliser une fonction pour parser la requête
		const {
			echange: {
				description,
				typeId,
				date,
				nom,
				demandeIds: demandeIdsRaw,
				brouillonId: brouillonIdRaw,
				brouillonSuppression,
			},
		} = ctx.request.body as EchangeAddInput;

		const partialEchange = {
			equipeId,
			nom,
			date: new Date(date),
			typeId: typeId as EchangeTypeId,
			description,
			devecoId,
		};

		const brouillonId = querystringToNumber(brouillonIdRaw);
		if (brouillonId) {
			const brouillon = await dbQueryBrouillon.brouillonGet({
				equipeId,
				brouillonId,
			});
			if (!brouillon) {
				ctx.throw(StatusCodes.BAD_REQUEST, `aucun brouillon avec cet id : ${brouillonId}`);
			}
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		// TODO il faudra aussi vérifier que la demande appartient au bon établissement/créateur/local et pas juste à la bonne équipe
		let demandes: Demande[] = [];
		if (querystringToArray(demandeIdsRaw).length > 0) {
			demandes = await dbQueryDemande.demandeGetManyById({
				equipeId,
				demandeIds: querystringToArray(demandeIdsRaw).map(Number),
			});
		}

		const echange = await serviceEchange.echangeAdd({
			evenementId: evenement.id,
			partialEchange,
			demandeIds: demandes.map(({ id }) => id),
			brouillonId,
		});

		if (brouillonId && brouillonSuppression) {
			await serviceBrouillon.brouillonRemove({
				evenementId: evenement.id,
				brouillonId,
			});
		}

		return {
			evenementId: evenement.id,
			equipeId,
			echange,
		};
	},

	async echangeUpdate(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const echangeId = ctx.params.echangeId ? parseInt(ctx.params.echangeId) : null;
		if (!echangeId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `echangeId obligatoire`);
		}

		const oldEchange = await dbQueryEchange.echangeWithRelationsGet({
			equipeId,
			echangeId,
		});
		if (!oldEchange) {
			ctx.throw(StatusCodes.BAD_REQUEST, `aucun échange avec cet id : ${echangeId}`);
		}

		const {
			echange: { typeId, date, nom, description, demandeIds: demandeIdsRaw },
		} = ctx.request.body as EchangeUpdateInput;

		// TODO il faut aussi vérifier que la demande appartient au bon établissement et pas juste à la bonne équipe
		let demandes: Demande[] = [];
		if (querystringToArray(demandeIdsRaw).length > 0) {
			demandes = await dbQueryDemande.demandeGetManyById({
				equipeId,
				demandeIds: querystringToArray(demandeIdsRaw).map(Number),
			});
		}

		const partialEchange = {
			typeId: typeId as EchangeTypeId,
			date: new Date(date),
			nom,
			description,
			modificationCompteId: compteId,
			modificationDate: new Date(),
		};

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		const echange = await serviceEchange.echangeUpdate({
			evenementId: evenement.id,
			echangeId,
			partialEchange,
			demandeIds: demandes.map(({ id }) => id),
		});

		return {
			evenementId: evenement.id,
			equipeId,
			oldEchange,
			echange,
		};
	},

	async echangeRemove(ctx: CtxDeveco, evenementTypeId: Evenement['typeId']) {
		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const echangeId = ctx.params.echangeId ? parseInt(ctx.params.echangeId) : null;
		if (!echangeId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `echangeId obligatoire`);
		}

		const oldEchange = await dbQueryEchange.echangeWithRelationsGet({
			equipeId,
			echangeId,
		});
		if (!oldEchange) {
			ctx.throw(StatusCodes.BAD_REQUEST, `aucun échange avec cet id : ${echangeId}`);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: evenementTypeId,
		});

		await serviceEchange.echangeRemove({
			evenementId: evenement.id,
			echangeId,
		});

		return {
			evenementId: evenement.id,
			equipeId,
			echangeId,
		};
	},
};
