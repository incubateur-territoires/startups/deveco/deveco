import { StatusCodes } from 'http-status-codes';
import * as turf from '@turf/turf';

import { etiquettesByTypeBuild } from './_format/equipe';

import { CtxDeveco } from '../middleware/auth';
import { Demande, territoireTypeIdValidate, Contour } from '../db/types';
import * as dbQueryEquipe from '../db/query/equipe';
import * as dbQueryDemande from '../db/query/demande';
import * as dbQueryLocal from '../db/query/local';
import * as dbQueryTerritoire from '../db/query/territoire';
import * as ElasticQueryEquipeLocal from '../elastic/query/equipe-local';
import { serviceTerritoire } from '../service/territoire';
import { geoParamsBuild } from '../controller/equipe';
import { dbQuoteEscape } from '../lib/utils';

const demandeTypeFormat = ({ typeId }: Pick<Demande, 'typeId'>) => typeId;

const FRANCE_BOX = JSON.stringify({
	type: 'Polygon',
	coordinates: [
		[
			[-110.93, 64.06],
			[99.83, 64.06],
			[99.83, -49.08],
			[-110.93, -49.08],
			[-110.93, 64.06],
		],
	],
});

export const controllerEquipeMeta = {
	async filtresGet(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const demandes = await dbQueryDemande.demandeTypeIdGetMany(equipeId);
		const qualifications = await dbQueryEquipe.etiquetteGetManyByEquipe({ equipeId });

		ctx.body = {
			...etiquettesByTypeBuild(qualifications),
			demandes: demandes.map(demandeTypeFormat),
		};
	},

	async qpvGetManyByEquipe(ctx: CtxDeveco) {
		const rechercheRaw = ctx.query.recherche as string;

		const recherche = rechercheRaw ? dbQuoteEscape(rechercheRaw.trim()) : undefined;

		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const equipe = await dbQueryEquipe.equipeGet({ equipeId });
		if (!equipe) {
			ctx.throw(StatusCodes.NOT_FOUND, `l'équipe ${equipeId} est introuvable`);
		}

		const geo = equipe.territoireTypeId !== 'france' ? ctx.query.geo : 'france';

		const qpvs = await serviceTerritoire.qpvGetMany({
			recherche,
			equipeId: geo === 'territoire' ? equipeId : null,
		});

		ctx.body = {
			qpvs: (qpvs ?? []).sort((a, b) => a.nom.localeCompare(b.nom, 'fr')),
		};
	},

	async territoireGetManyByTypeAndEquipe(ctx: CtxDeveco) {
		const nomRaw = ctx.query.nom as string;
		const territoireTypeIdRaw = ctx.params.territoireType;

		const territoireTypeId = territoireTypeIdValidate(territoireTypeIdRaw);
		if (!territoireTypeId) {
			ctx.throw(
				StatusCodes.BAD_REQUEST,
				`le type de territoire ${territoireTypeIdRaw} est inconnu`
			);
		}

		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const equipe = await dbQueryEquipe.equipeGet({ equipeId });
		if (!equipe) {
			ctx.throw(StatusCodes.NOT_FOUND, `l'équipe ${equipeId} est introuvable`);
		}

		const geo = equipe.territoireTypeId !== 'france' ? ctx.query.geo : 'france';

		const recherche = nomRaw ? dbQuoteEscape(nomRaw.trim()) : undefined;

		const territoires = await serviceTerritoire.territoireGetManyByType({
			territoireTypeId,
			recherche,
			equipeId: geo === 'territoire' ? equipeId : null,
		});

		ctx.body = {
			territoires: (territoires ?? []).sort((a, b) => a.nom.localeCompare(b.nom, 'fr')),
		};
	},

	async territoireIndustriesForEquipeGet(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const territoireIndustries = await dbQueryEquipe.territoireIndustrieGetManyByEquipeAndNomOrId({
			equipeId,
		});

		ctx.body = {
			territoireIndustries: territoireIndustries.sort((a, b) => a.nom.localeCompare(b.nom)),
		};
	},

	async contourGet(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const equipe = await dbQueryEquipe.equipeGet({ equipeId });
		if (!equipe) {
			ctx.throw(StatusCodes.BAD_REQUEST, `Equipe #${equipeId} introuvable`);
		}

		const { geo } = geoParamsBuild({
			query: ctx.query,
			equipeId,
		});

		const [territoireEquipe] =
			geo !== 'france'
				? await dbQueryTerritoire.etalabContourGetManyByEquipe({
						equipeId,
					})
				: [];

		let territoireFrance: Contour | null = null;
		if ((geo && geo !== 'sur') || equipe.territoireTypeId === 'france') {
			const [contour] = await dbQueryTerritoire.etalabContourFranceGet();

			territoireFrance = {
				...contour,
				box: FRANCE_BOX,
			};
		}

		let territoire: Contour | null = null;
		if (geo === 'hors' && territoireFrance && territoireEquipe) {
			const geometryFrance = JSON.parse(territoireFrance.geometrie);
			const geometryEquipe = JSON.parse(territoireEquipe.geometrie);

			const geometryHors = turf.difference(
				turf.featureCollection([turf.feature(geometryFrance), turf.feature(geometryEquipe)])
			);

			territoire = geometryHors
				? {
						...territoireFrance,
						geometrie: JSON.stringify(geometryHors.geometry),
					}
				: null;
		} else {
			territoire = territoireFrance ?? territoireEquipe;
		}

		ctx.body = {
			territoire,
		};
	},

	async localTypeGetMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const natureIds = await ElasticQueryEquipeLocal.eqLocNatureIdGetMany({ equipeId });
		const categorieIds = await ElasticQueryEquipeLocal.eqLocCategorieIdGetMany({ equipeId });

		ctx.body = {
			categories: categorieIds.length
				? await dbQueryLocal.localCategorieTypeGetManyById({ categorieIds })
				: [],
			natures: natureIds.length ? await dbQueryLocal.localNatureTypeGetManyById({ natureIds }) : [],
		};
	},

	async localCommuneVoieNomGetMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const communeId = ctx.params.communeId;

		const voieNoms = await ElasticQueryEquipeLocal.eqLocCommuneVoieNomGetManyByCommune({
			equipeId,
			communeId,
		});

		ctx.body = {
			voieNoms,
		};
	},

	async localCommuneVoieNomNumeroGetMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const communeId = ctx.params.communeId;
		const voieNom = ctx.params.voieNom;

		const numeros =
			await ElasticQueryEquipeLocal.eqLocCommuneVoieNomNumeroGetManyByCommuneAndVoieNom({
				equipeId,
				communeId,
				voieNom,
			});

		ctx.body = {
			numeros,
		};
	},

	async localCommuneSectionGetMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const communeId = ctx.params.communeId;

		const sections = await ElasticQueryEquipeLocal.eqLocCommuneSectionGetManyByCommune({
			equipeId,
			communeId,
		});

		ctx.body = {
			sections,
		};
	},

	async localCommuneSectionParcelleGetMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const communeId = ctx.params.communeId;
		const section = ctx.params.section;

		const parcelles =
			await ElasticQueryEquipeLocal.eqLocCommuneSectionParcelleGetManyByCommuneAndSection({
				equipeId,
				communeId,
				section,
			});

		ctx.body = {
			parcelles,
		};
	},
};
