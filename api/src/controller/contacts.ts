import { StatusCodes } from 'http-status-codes';

import { ParsedUrlQuery } from 'querystring';

import { querystringToNumber, querystringToString } from '../lib/querystring';
import { TriContact, Direction, fileFormatValidate } from '../db/types';
import { ContactAddInput, ContactUpdateInput } from '../controller/types';
import { CtxDeveco } from '../middleware/auth';
import * as dbQueryContact from '../db/query/contact';
import * as dbQueryEvenement from '../db/query/evenement';
import { serviceContact } from '../service/contact';
import { contactFormatMany } from '../controller/_format/equipe';
import { controllerFluxContact } from '../controller/flux-contact';

const contactTri: TriContact[] = ['nom', 'prenom', 'email'];

function contactGetManyParamsBuild(query: ParsedUrlQuery) {
	const {
		recherche,
		etablissementId,
		localId,
		page,
		elementsParPage,
		tri: triRaw,
		direction,
		format,
	} = query;

	const tri = querystringToString(triRaw);
	const parsedTri = tri ? contactTri.find((t) => t === tri) : null;

	return {
		recherche: querystringToString(recherche)?.trim(),
		etablissementId: querystringToString(etablissementId),
		localId: querystringToString(localId),

		// pagination
		elementsParPage:
			querystringToNumber(elementsParPage) || dbQueryContact.elementsParPageParDefaut,
		page: querystringToNumber(page) || 1,
		tri: parsedTri ?? null,
		direction: (direction === 'desc' ? 'desc' : 'asc') as Direction,
		format: fileFormatValidate(querystringToString(format) ?? ''),
	};
}

export const controllerContacts = {
	async contactGetMany(ctx: CtxDeveco) {
		const deveco = ctx.state.deveco;
		const equipeId = deveco.equipeId;

		const params = contactGetManyParamsBuild(ctx.query);

		if (params.format) {
			if (params.format === 'xlsx') {
				return await controllerFluxContact.xlsxStreamMany(ctx, { ...params, equipeId });
			}

			return;
		}

		const [contacts, [{ count: total }]] = await Promise.all([
			dbQueryContact.contactWithRelationGetManyByEquipe({
				equipeId,
				...params,
			}),
			dbQueryContact.contactCountByEquipe({
				equipeId,
				...params,
			}),
		]);

		ctx.body = {
			total: total,
			page: params.page,
			pages: Math.ceil(total / params.elementsParPage),
			elements: contactFormatMany({ contacts }),
		};
	},

	async contactAdd(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const body = ctx.request.body as ContactAddInput;
		const { type } = body;

		if (type === 'existant') {
			ctx.throw(
				StatusCodes.BAD_REQUEST,
				`impossible de créer un contact existant depuis l'annuaire`
			);
		}

		if (type === 'nouveau' && !body.contact) {
			ctx.throw(StatusCodes.BAD_REQUEST, `contact requis`);
		}

		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'contact_creation',
		});

		const { id, ...contactSansId } = body.contact;
		await serviceContact.contactAdd({
			evenementId: evenement.id,
			equipeId,
			contact: {
				...contactSansId,
				equipeId,
				naissanceDate: body.contact.naissanceDate ? new Date(body.contact.naissanceDate) : null,
			},
		});

		return next(ctx);
	},

	async contactUpdate(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		// TODO utiliser une fonction pour parser la requête
		const body = ctx.request.body as ContactUpdateInput;
		const { contact } = body;

		if (!contact) {
			ctx.throw(StatusCodes.BAD_REQUEST, `contact requis`);
		}

		const contactId = ctx.params.contactId ? parseInt(ctx.params.contactId) : null;
		if (!contactId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `contactId obligatoire`);
		}

		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const oldContact = await dbQueryContact.contactGet({ contactId });
		if (!oldContact || oldContact.equipeId !== equipeId) {
			ctx.throw(StatusCodes.NOT_FOUND, `contact ${contactId} inexistant`);
		}

		const { id, ...contactSansId } = contact;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'contact_modification',
		});

		await serviceContact.contactUpdate({
			evenementId: evenement.id,
			contactId,
			partialContact: {
				...contactSansId,
				naissanceDate: contact.naissanceDate ? new Date(contact.naissanceDate) : null,
			},
			oldContact,
		});

		return next(ctx);
	},

	async contactRemove(ctx: CtxDeveco, next: (ctx: CtxDeveco) => Promise<unknown>) {
		const contactId = ctx.params.contactId ? parseInt(ctx.params.contactId) : null;
		if (!contactId) {
			ctx.throw(StatusCodes.BAD_REQUEST, `contactId obligatoire`);
		}

		const deveco = ctx.state.deveco;
		const compteId = deveco.compteId;
		const equipeId = deveco.equipeId;

		const oldContact = await dbQueryContact.contactGet({ contactId });
		if (!oldContact || oldContact.equipeId !== equipeId) {
			ctx.throw(StatusCodes.NOT_FOUND, `contact ${contactId} inexistant`);
		}

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'contact_suppression',
		});

		await serviceContact.contactRemove({
			evenementId: evenement.id,
			equipeId,
			contactId,
		});

		return next(ctx);
	},
};
