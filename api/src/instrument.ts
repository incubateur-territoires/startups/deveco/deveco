import * as Sentry from '@sentry/node';

const dsn = process.env.SENTRY_DSN;

if (dsn) {
	// Ensure to call this before importing any other modules!
	Sentry.init({
		dsn,
		integrations: [Sentry.koaIntegration()],

		// Add Tracing by setting tracesSampleRate
		// We recommend adjusting this value in production
		tracesSampleRate: 1.0,

		// Set sampling rate for profiling
		// This is relative to tracesSampleRate
		profilesSampleRate: 1.0,
	});
}
