/* eslint-disable @typescript-eslint/unbound-method */
import Router from 'koa-router';

import { authMiddleware } from './middleware/auth';
import { roleMiddleware } from './middleware/role';
import { rechercheMiddleware } from './middleware/recherche';
import { UpdateNouveautesMiddleware, controllerAdmin } from './controller/admin';
import { controllerAuth } from './controller/auth';
import { controllerCompte } from './controller/compte';
import { controllerEquipe } from './controller/equipe';
import { controllerContacts } from './controller/contacts';
import { controllerEtablissements } from './controller/etablissements';
import { controllerEntreprises } from './controller/entreprises';
import {
	controllerEtablissementCreations,
	controllerEtablissementCreationsParam,
} from './controller/etablissement-creations';
import { controllerFluxEtablissements } from './controller/flux-etablissements';
import { controllerFluxLocaux } from './controller/flux-locaux';
import { controllerHealth } from './controller/health';
import { controllerTerritoires } from './controller/territoires';
import { controllerMeta } from './controller/meta';
import { controllerEquipeMeta } from './controller/equipe-meta';
import { controllerLocaux } from './controller/locaux';
import { controllerAutorisation } from './controller/autorisation';
import { controllerTest, TestMiddleware } from './controller/test';
import { controllerImport } from './controller/import';
import { controllerLogger } from './controller/logger';

// --------------------------------------------------
//
// 		health
//
// --------------------------------------------------

export const healthRoutePrefix = '/health';
export const health = new Router({ prefix: healthRoutePrefix });

health.get('/', controllerHealth.check);

// --------------------------------------------------
//
// 		logger
//
// --------------------------------------------------

export const loggerRoutePrefix = '/logger';
export const logger = new Router({ prefix: loggerRoutePrefix });

logger.post('/', controllerLogger.logMessage);

// --------------------------------------------------
//
// 		flux
//
// --------------------------------------------------

export const geojsonRoutePrefix = '/geojson';
export const geojson = new Router({ prefix: geojsonRoutePrefix });

geojson.get(
	'/recherche-sauvegardee/etablissements/:token',
	controllerFluxEtablissements.geojsonWithRechercheSauvegardeeTokenStreamMany
);
geojson.get(
	'/recherche-sauvegardee/locaux/:token',
	controllerFluxLocaux.geojsonWithRechercheSauvegardeeTokenStreamMany
);
geojson.get(
	'/recherche-sauvegardee/:token',
	controllerFluxEtablissements.geojsonWithRechercheSauvegardeeTokenStreamMany
);
geojson.get('/:token', controllerFluxEtablissements.geojsonWithTokenStreamMany);

// --------------------------------------------------
//
// 		auth
//
// --------------------------------------------------

export const auth = new Router({ prefix: '/auth' });

auth.get('/', controllerAuth.status);

auth.post('/jwt/:cle', controllerAuth.jwt);
auth.post('/connexion', controllerAuth.login);
auth.post('/deconnexion', controllerAuth.logout);

// --------------------------------------------------
//
// 		admin
//
// --------------------------------------------------

export const admin: Router = new Router({ prefix: '/admin' });

admin.param('token', UpdateNouveautesMiddleware);
admin.get('/nouveautes/:token/:amount', controllerAdmin.nouveauteAdd);
admin.get('/sirene/:token', controllerAdmin.apiSireneUpdate);
admin.get('/geocode/:token', controllerAdmin.geocodeUpdate);
admin.get('/index/:token', controllerAdmin.indexUpdate);
admin.get('/brevo/:token/:equipeId?', controllerAdmin.brevoUpdate);

admin.use(authMiddleware());
admin.use(roleMiddleware('superadmin'));

admin.get('/deveco', controllerAdmin.devecoGetMany);
admin.get('/deveco/csv', controllerAdmin.devecoAsCsvGetMany);
admin.get('/equipe', controllerAdmin.equipeGetMany);
admin.get('/equipe/demandes', controllerAdmin.equipeDemandeGetMany);
admin.get('/equipe/:equipeId/etiquettes', controllerAdmin.etiquetteGetManyByEquipe);
admin.get('/recherche/:devecoId', controllerAdmin.rechercheEnTantQue);

admin.post('/compte', controllerAdmin.compteAdd, controllerAdmin.devecoGetMany);
admin.post(
	'/compte/:id/mot-de-passe',
	controllerAdmin.compteMotDePasseAdd,
	controllerAdmin.devecoGetMany
);
admin.post('/compte/actif', controllerAdmin.compteActifUpdate, controllerAdmin.devecoGetMany);
admin.post('/equipe', controllerAdmin.equipeAdd);
admin.post('/equipe/demandes', controllerAdmin.equipeDemandeAdd);
admin.post('/equipe/demandes/:equipeDemandeId/cloture', controllerAdmin.equipeDemandeCloture);

admin.delete(
	'/equipe/:equipeId/etiquettes',
	controllerAdmin.equipeEtiquetteRemoveMany,
	controllerAdmin.etiquetteGetManyByEquipe
);

admin.post('/import/:compteId', controllerImport.fromFile);
admin.post('/siretisation', controllerEtablissements.lookupMany);

// --------------------------------------------------
//
// 		autorisation
//
// --------------------------------------------------

export const autorisation: Router = new Router({ prefix: '/autorisation' });

autorisation.get('/:token', controllerAutorisation.equipeDemandeGet);
autorisation.post(
	'/:token',
	controllerAutorisation.equipeDemandeAccept,
	controllerAutorisation.equipeDemandeGet
);

// --------------------------------------------------
//
// 		compte
//
// --------------------------------------------------

export const compte = new Router({ prefix: '/compte' });

compte.post('/cgu', authMiddleware(true), controllerCompte.cguUpdate);

compte.use(authMiddleware());

compte.get('/stats', controllerCompte.statsGet);

compte.post('/info', controllerCompte.infosUpdate);
compte.post('/mot-de-passe', controllerCompte.motDePasseUpdate);

compte.use(roleMiddleware('deveco'));

compte.get('/geotokens', controllerCompte.geoTokenGetMany);

compte.get('/notifications', controllerCompte.notificationsGet);
compte.post(
	'/notifications',
	controllerCompte.notificationsUpdate,
	controllerCompte.notificationsGet
);

compte.get('/filtres/etablissements', controllerCompte.rechercheEnregistreeGetMany);
compte.post(
	'/filtres/etablissements',
	controllerCompte.rechercheEnregistreeAdd,
	controllerCompte.rechercheEnregistreeGetMany
);
compte.delete(
	'/filtres/etablissements',
	controllerCompte.rechercheEnregistreeRemove,
	controllerCompte.rechercheEnregistreeGetMany
);

// --------------------------------------------------
//
// 		territoires
//
// --------------------------------------------------

export const territoires = new Router({ prefix: '/territoires' });
territoires.use(authMiddleware());

territoires.get('/communes', controllerTerritoires.communeGetMany);
territoires.get('/communes/:id', controllerTerritoires.communeGet);
territoires.get('/epcis', controllerTerritoires.epciGetMany);
territoires.get('/epcis/petr/:id', controllerTerritoires.epciGetManyByPetr);
territoires.get('/epcis/:id', controllerTerritoires.epciGet);
territoires.get('/territoireType/:territoireType', controllerTerritoires.territoireGetManyByType);

// --------------------------------------------------
//
// 		équipe
//
// --------------------------------------------------

export const equipe = new Router({ prefix: '/equipe' });

equipe.use(authMiddleware());
equipe.use(roleMiddleware('deveco'));

// deprecated
equipe.get('/annuaire', rechercheMiddleware('annuaire'), controllerContacts.contactGetMany);
// deprecated
equipe.get('/stats/etablissements', controllerEquipe.etablissementStatsGet);
equipe.get('/stats/etablissement-creations', controllerEquipe.etabCreaStatsGet);
equipe.get('/stats/locaux', controllerEquipe.localStatsGet);
equipe.get('/devecos', controllerEquipe.devecoGetMany);
equipe.get('/etiquettes', controllerEquipe.etiquetteGetMany);
equipe.get('/etiquettes/:etiquetteTypeId', controllerEquipe.etiquetteGetManyByType);
equipe.get('/suivis', controllerEquipe.suiviGetMany);
// deprecated
equipe.get('/rappels', controllerEquipe.rappelGetMany);
equipe.get('/zonages', controllerEquipe.zonageGetMany);
equipe.get('/zonages/:zonageId/fichier', controllerEquipe.zonageFichierGet);

// SUIVI

// echanges établissements
equipe.post(
	'/etablissements/:etablissementId/echanges/:echangeId',
	controllerEtablissements.echangeUpdate,
	controllerEquipe.suiviGetMany
);
equipe.delete(
	'/etablissements/:etablissementId/echanges/:echangeId',
	controllerEtablissements.echangeRemove,
	controllerEquipe.suiviGetMany
);
// echanges établissement-création
equipe.post(
	'/etablissement-creations/:etabCreaId/echanges/:echangeId',
	controllerEtablissementCreations.echangeUpdate,
	controllerEquipe.suiviGetMany
);
equipe.delete(
	'/etablissement-creations/:etabCreaId/echanges/:echangeId',
	controllerEtablissementCreations.echangeRemove,
	controllerEquipe.suiviGetMany
);
// echanges locaux
equipe.post(
	'/locaux/:localId/echanges/:echangeId',
	controllerLocaux.echangeUpdate,
	controllerEquipe.suiviGetMany
);
equipe.delete(
	'/locaux/:localId/echanges/:echangeId',
	controllerLocaux.echangeRemove,
	controllerEquipe.suiviGetMany
);

// demandes établissements
equipe.post(
	'/etablissements/:etablissementId/demandes/:demandeId',
	controllerEtablissements.demandeUpdate,
	controllerEquipe.suiviGetMany
);
equipe.post(
	'/etablissements/:etablissementId/demandes/:demandeId/cloture',
	controllerEtablissements.demandeCloture,
	controllerEquipe.suiviGetMany
);
equipe.post(
	'/etablissements/:etablissementId/demandes/:demandeId/reouverture',
	controllerEtablissements.demandeReouverture,
	controllerEquipe.suiviGetMany
);
equipe.delete(
	'/etablissements/:etablissementId/demandes/:demandeId',
	controllerEtablissements.demandeRemove,
	controllerEquipe.suiviGetMany
);
// demandes établissement-création
equipe.post(
	'/etablissement-creations/:etabCreaId/demandes/:demandeId',
	controllerEtablissementCreations.demandeUpdate,
	controllerEquipe.suiviGetMany
);
equipe.post(
	'/etablissement-creations/:etabCreaId/demandes/:demandeId/cloture',
	controllerEtablissementCreations.demandeCloture,
	controllerEquipe.suiviGetMany
);
equipe.post(
	'/etablissement-creations/:etabCreaId/demandes/:demandeId/reouverture',
	controllerEtablissementCreations.demandeReouverture,
	controllerEquipe.suiviGetMany
);
equipe.delete(
	'/etablissement-creations/:etabCreaId/demandes/:demandeId',
	controllerEtablissementCreations.demandeRemove,
	controllerEquipe.suiviGetMany
);
// demandes locaux
equipe.post(
	'/locaux/:localId/demandes/:demandeId',
	controllerLocaux.demandeUpdate,
	controllerEquipe.suiviGetMany
);
equipe.post(
	'/locaux/:localId/demandes/:demandeId/cloture',
	controllerLocaux.demandeCloture,
	controllerEquipe.suiviGetMany
);
equipe.post(
	'/locaux/:localId/demandes/:demandeId/reouverture',
	controllerLocaux.demandeReouverture,
	controllerEquipe.suiviGetMany
);
equipe.delete(
	'/locaux/:localId/demandes/:demandeId',
	controllerLocaux.demandeRemove,
	controllerEquipe.suiviGetMany
);

// rappels établissements
equipe.post(
	'/etablissements/:etablissementId/rappels/:rappelId',
	controllerEtablissements.rappelUpdate,
	controllerEquipe.suiviGetMany
);
equipe.post(
	'/etablissements/:etablissementId/rappels/:rappelId/cloture',
	controllerEtablissements.rappelCloture,
	controllerEquipe.suiviGetMany
);
equipe.post(
	'/etablissements/:etablissementId/rappels/:rappelId/reouverture',
	controllerEtablissements.rappelReouverture,
	controllerEquipe.suiviGetMany
);
equipe.delete(
	'/etablissements/:etablissementId/rappels/:rappelId',
	controllerEtablissements.rappelRemove,
	controllerEquipe.suiviGetMany
);
// rappels établissement-création
equipe.post(
	'/etablissement-creations/:etabCreaId/rappels/:rappelId',
	controllerEtablissementCreations.rappelUpdate,
	controllerEquipe.suiviGetMany
);
equipe.post(
	'/etablissement-creations/:etabCreaId/rappels/:rappelId/cloture',
	controllerEtablissementCreations.rappelCloture,
	controllerEquipe.suiviGetMany
);
equipe.post(
	'/etablissement-creations/:etabCreaId/rappels/:rappelId/reouverture',
	controllerEtablissementCreations.rappelReouverture,
	controllerEquipe.suiviGetMany
);
equipe.delete(
	'/etablissement-creations/:etabCreaId/rappels/:rappelId',
	controllerEtablissementCreations.rappelRemove,
	controllerEquipe.suiviGetMany
);
// rappels locaux
equipe.post(
	'/locaux/:localId/rappels/:rappelId',
	controllerLocaux.rappelUpdate,
	controllerEquipe.suiviGetMany
);
equipe.post(
	'/locaux/:localId/rappels/:rappelId/cloture',
	controllerLocaux.rappelCloture,
	controllerEquipe.suiviGetMany
);
equipe.post(
	'/locaux/:localId/rappels/:rappelId/reouverture',
	controllerLocaux.rappelReouverture,
	controllerEquipe.suiviGetMany
);
equipe.delete(
	'/locaux/:localId/rappels/:rappelId',
	controllerLocaux.rappelRemove,
	controllerEquipe.suiviGetMany
);

// brouillon
equipe.post('/brouillons', controllerEquipe.brouillonAdd, controllerEquipe.suiviGetMany);
equipe.post(
	'/brouillons/:brouillonId',
	controllerEquipe.brouillonUpdate,
	controllerEquipe.suiviGetMany
);
equipe.delete(
	'/brouillons/:brouillonId',
	controllerEquipe.brouillonRemove,
	controllerEquipe.suiviGetMany
);

// etiquettes
equipe.post(
	'/etiquettes/:etiquetteId',
	controllerEquipe.etiquetteEdit,
	controllerEquipe.etiquetteGetMany
);
equipe.post('/etiquettes', controllerEquipe.etiquetteAdd, controllerEquipe.etiquetteGetMany);
equipe.delete(
	'/etiquettes/:etiquetteId',
	controllerEquipe.etiquetteRemove,
	controllerEquipe.etiquetteGetMany
);

// zonages
equipe.post('/zonages', controllerEquipe.zonageAdd);
equipe.post('/zonages/:zonageId', controllerEquipe.zonageUpdate);
equipe.delete('/zonages/:zonageId', controllerEquipe.zonageRemove);

// --------------------------------------------------
//
// 		métas
//
// --------------------------------------------------

export const meta = new Router({ prefix: '/meta' });

meta.use(authMiddleware());

meta.get('/naf', controllerMeta.nafTypeGetMany);
meta.get('/categories-juridiques', controllerMeta.categorieJuridiqueGetMany);
meta.get('/equipe-types', controllerMeta.equipeTypeGetMany);
meta.get('/local-categories', controllerMeta.localCategorieGetMany);
meta.get('/local-natures', controllerMeta.localNatureGetMany);
meta.get('/createurs', controllerMeta.createursSourcesGet);

// --------------------------------------------------
//
// 		métas équipe
//
// --------------------------------------------------

export const equipeMeta = new Router({ prefix: '/equipe-meta' });

equipeMeta.use(authMiddleware());
equipeMeta.use(roleMiddleware('deveco'));

equipeMeta.get('/filtres/:context', controllerEquipeMeta.filtresGet);
equipeMeta.get('/qpvs', controllerEquipeMeta.qpvGetManyByEquipe);
equipeMeta.get('/contour', controllerEquipeMeta.contourGet);
equipeMeta.get(
	'/territoires/:territoireType',
	controllerEquipeMeta.territoireGetManyByTypeAndEquipe
);
equipeMeta.get('/local-types', controllerEquipeMeta.localTypeGetMany);
equipeMeta.get(
	'/local-adresse/communes/:communeId/voies-noms',
	controllerEquipeMeta.localCommuneVoieNomGetMany
);
equipeMeta.get(
	'/local-adresse/communes/:communeId/voies-noms/:voieNom/numeros',
	controllerEquipeMeta.localCommuneVoieNomNumeroGetMany
);
equipeMeta.get(
	'/local-adresse/communes/:communeId/sections',
	controllerEquipeMeta.localCommuneSectionGetMany
);
equipeMeta.get(
	'/local-adresse/communes/:communeId/sections/:section/parcelles',
	controllerEquipeMeta.localCommuneSectionParcelleGetMany
);

// --------------------------------------------------
//
// 		contacts
//
// --------------------------------------------------

export const contact = new Router({ prefix: '/contacts' });

contact.use(authMiddleware());
contact.use(roleMiddleware('deveco'));

contact.get('/', rechercheMiddleware('annuaire'), controllerContacts.contactGetMany);
contact.post('/', controllerContacts.contactAdd, controllerContacts.contactGetMany);
contact.post('/:contactId', controllerContacts.contactUpdate, controllerContacts.contactGetMany);
contact.delete('/:contactId', controllerContacts.contactRemove, controllerContacts.contactGetMany);

// --------------------------------------------------
//
// 		entreprise
//
// --------------------------------------------------

export const entreprises = new Router({ prefix: '/entreprises' });

entreprises.use(authMiddleware());
entreprises.use(roleMiddleware('deveco'));

entreprises.get('/', controllerEntreprises.getMany);

entreprises.param('entrepriseId', controllerEntreprises.entrepriseCheck);
entreprises.get('/:entrepriseId/participation', controllerEntreprises.participationGet);
entreprises.get('/:entrepriseId/api-entreprise', controllerEntreprises.apiEntrepriseInfosGet);

// --------------------------------------------------
//
// 		établissements
//
// --------------------------------------------------

export const etablissements = new Router({ prefix: '/etablissements' });

etablissements.use(authMiddleware());
etablissements.use(roleMiddleware('deveco'));

// recherche par CSV pour de la siretisation
etablissements.get(
	'/csv',
	rechercheMiddleware('etablissement'),
	controllerEtablissements.lookupMany
);

etablissements.get(
	'/',
	rechercheMiddleware('etablissement'),
	controllerEtablissements.viewMany,
	controllerEtablissements.getMany
);

etablissements.get('/par-siret', controllerEtablissements.getBySiret);

// TODO remplacer par un POST
etablissements.get('/recherche-sauvegardee', controllerEtablissements.rechercheSauvegardeeLienGet);

etablissements.param('etablissementId', controllerEtablissements.etablissementCheck);

etablissements.get(
	'/:etablissementId',
	controllerEtablissements.view,
	controllerEtablissements.get
);
etablissements.get(
	'/:etablissementId/evenements',
	controllerEtablissements.actionEtablissementGetMany
);
etablissements.get(
	'/:etablissementId/suivi-par',
	controllerEtablissements.suiviParEquipeNomGetMany
);
etablissements.get('/:etablissementId/zonages', controllerEtablissements.zonageGetMany);

etablissements.post(
	'/etiquettes',
	controllerEtablissements.etiquetteAddMany,
	controllerEtablissements.getMany
);

etablissements.post(
	'/:etablissementId/etiquettes',
	controllerEtablissements.etiquetteUpdate,
	controllerEtablissements.get
);
etablissements.post(
	'/:etablissementId/commentaire',
	controllerEtablissements.descriptionUpdate,
	controllerEtablissements.get
);
etablissements.post('/:etablissementId/cloture-date', controllerEtablissements.clotureDateUpdate);
etablissements.post(
	'/:etablissementId/enseigne',
	controllerEtablissements.enseigneUpdate,
	controllerEtablissements.get
);
etablissements.post(
	'/:etablissementId/geolocalisation',
	controllerEtablissements.geolocalisationUpdate,
	controllerEtablissements.get
);

etablissements.post('/:etablissementId/favori', controllerEtablissements.favoriUpdate);

etablissements.post(
	'/:etablissementId/ressource',
	controllerEtablissements.ressourceAdd,
	controllerEtablissements.get
);
etablissements.post(
	'/:etablissementId/ressource/:ressourceId',
	controllerEtablissements.ressourceUpdate,
	controllerEtablissements.get
);
etablissements.delete(
	'/:etablissementId/ressource/:ressourceId',
	controllerEtablissements.ressourceRemove,
	controllerEtablissements.get
);
etablissements.post('/:etablissementId/partage', controllerEtablissements.partage);

// suivi
// contact
etablissements.post(
	'/:etablissementId/contacts',
	controllerEtablissements.contactAdd,
	controllerEtablissements.get
);
etablissements.post(
	'/:etablissementId/contacts/:contactId',
	controllerEtablissements.contactUpdate,
	controllerEtablissements.get
);
etablissements.delete(
	'/:etablissementId/contacts/:contactId',
	controllerEtablissements.contactRemove,
	controllerEtablissements.get
);

// echange
etablissements.post(
	'/:etablissementId/echanges',
	controllerEtablissements.echangeAdd,
	controllerEtablissements.get
);
etablissements.post(
	'/:etablissementId/echanges/:echangeId',
	controllerEtablissements.echangeUpdate,
	controllerEtablissements.get
);
etablissements.delete(
	'/:etablissementId/echanges/:echangeId',
	controllerEtablissements.echangeRemove,
	controllerEtablissements.get
);

// demande
etablissements.post(
	'/:etablissementId/demandes',
	controllerEtablissements.demandeAdd,
	controllerEtablissements.get
);
etablissements.post(
	'/:etablissementId/demandes/:demandeId',
	controllerEtablissements.demandeUpdate,
	controllerEtablissements.get
);
etablissements.post(
	'/:etablissementId/demandes/:demandeId/cloture',
	controllerEtablissements.demandeCloture,
	controllerEtablissements.get
);
etablissements.post(
	'/:etablissementId/demandes/:demandeId/reouverture',
	controllerEtablissements.demandeReouverture,
	controllerEtablissements.get
);
etablissements.delete(
	'/:etablissementId/demandes/:demandeId',
	controllerEtablissements.demandeRemove,
	controllerEtablissements.get
);

// rappel
etablissements.post(
	'/:etablissementId/rappels',
	controllerEtablissements.rappelAdd,
	controllerEtablissements.get
);
etablissements.post(
	'/:etablissementId/rappels/:rappelId',
	controllerEtablissements.rappelUpdate,
	controllerEtablissements.get
);
etablissements.post(
	'/:etablissementId/rappels/:rappelId/cloture',
	controllerEtablissements.rappelCloture,
	controllerEtablissements.get
);
etablissements.post(
	'/:etablissementId/rappels/:rappelId/reouverture',
	controllerEtablissements.rappelReouverture,
	controllerEtablissements.get
);
etablissements.delete(
	'/:etablissementId/rappels/:rappelId',
	controllerEtablissements.rappelRemove,
	controllerEtablissements.get
);

// --------------------------------------------------
//
// 		locaux
//
// --------------------------------------------------

export const locaux: Router = new Router({ prefix: '/locaux' });

locaux.use(authMiddleware());
locaux.use(roleMiddleware('deveco'));

locaux.get('/recherche-sauvegardee', controllerLocaux.rechercheSauvegardeeLienGet);
locaux.get('/', rechercheMiddleware('local'), controllerLocaux.viewMany, controllerLocaux.getMany);

locaux.get('/par-id', controllerLocaux.getById);

// middleware de vérification du paramètre localId
// de l'existence du local
// et de l'appartenance au territoire de l'équipe
locaux.param('localId', controllerLocaux.eqLocCheck);

locaux.get('/:localId', controllerLocaux.view, controllerLocaux.get);
locaux.get('/:localId/etablissement-suggestions', controllerLocaux.etablissementSuggestionGet);

locaux.post('/:localId/nom', controllerLocaux.contributionNomUpdate, controllerLocaux.get);
locaux.post('/:localId/vacance', controllerLocaux.contributionVacanceUpdate, controllerLocaux.get);
locaux.post('/:localId/donnees', controllerLocaux.contributionDonneesUpdate, controllerLocaux.get);
locaux.post('/:localId/occupants', controllerLocaux.occupantAdd, controllerLocaux.get);
locaux.delete(
	'/:localId/occupants/:etablissementId',
	controllerLocaux.occupantRemove,
	controllerLocaux.get
);
locaux.post(
	'/:localId/geolocalisation',
	controllerLocaux.geolocalisationUpdate,
	controllerLocaux.get
);

locaux.post('/:localId/favori', controllerLocaux.favoriUpdate);

locaux.post('/:localId/ressource', controllerLocaux.ressourceAdd, controllerLocaux.get);
locaux.post(
	'/:localId/ressource/:ressourceId',
	controllerLocaux.ressourceUpdate,
	controllerLocaux.get
);
locaux.delete(
	'/:localId/ressource/:ressourceId',
	controllerLocaux.ressourceRemove,
	controllerLocaux.get
);

locaux.post('/:localId/partage', controllerLocaux.partage);

// suivi
// contact
locaux.post('/:localId/contacts', controllerLocaux.contactAdd, controllerLocaux.get);
locaux.post('/:localId/contacts/:contactId', controllerLocaux.contactUpdate, controllerLocaux.get);
locaux.delete(
	'/:localId/contacts/:contactId',
	controllerLocaux.contactRemove,
	controllerLocaux.get
);

// echange
locaux.post('/:localId/echanges', controllerLocaux.echangeAdd, controllerLocaux.get);
locaux.post('/:localId/echanges/:echangeId', controllerLocaux.echangeUpdate, controllerLocaux.get);
locaux.delete(
	'/:localId/echanges/:echangeId',
	controllerLocaux.echangeRemove,
	controllerLocaux.get
);

// demande
locaux.post('/:localId/demandes', controllerLocaux.demandeAdd, controllerLocaux.get);
locaux.post('/:localId/demandes/:demandeId', controllerLocaux.demandeUpdate, controllerLocaux.get);
locaux.post(
	'/:localId/demandes/:demandeId/cloture',
	controllerLocaux.demandeCloture,
	controllerLocaux.get
);
locaux.post(
	'/:localId/demandes/:demandeId/reouverture',
	controllerLocaux.demandeReouverture,
	controllerLocaux.get
);
locaux.delete(
	'/:localId/demandes/:demandeId',
	controllerLocaux.demandeRemove,
	controllerLocaux.get
);

// rappel
locaux.post('/:localId/rappels', controllerLocaux.rappelAdd, controllerLocaux.get);
locaux.post('/:localId/rappels/:rappelId', controllerLocaux.rappelUpdate, controllerLocaux.get);
locaux.post(
	'/:localId/rappels/:rappelId/cloture',
	controllerLocaux.rappelCloture,
	controllerLocaux.get
);
locaux.post(
	'/:localId/rappels/:rappelId/reouverture',
	controllerLocaux.rappelReouverture,
	controllerLocaux.get
);
locaux.delete('/:localId/rappels/:rappelId', controllerLocaux.rappelRemove, controllerLocaux.get);

locaux.get('/:localId/zonages', controllerLocaux.zonageGetMany);

locaux.post('/:localId/etiquettes', controllerLocaux.etiquetteUpdate, controllerLocaux.get);

// --------------------------------------------------
//
// 		établissement-créations
//
// --------------------------------------------------

export const etabCreas = new Router({ prefix: '/etablissement-creations' });

etabCreas.use(authMiddleware());
etabCreas.use(roleMiddleware('deveco'));

etabCreas.get(
	'/',
	rechercheMiddleware('createurs'),
	controllerEtablissementCreations.viewMany,
	controllerEtablissementCreations.getMany
);

etabCreas.post('/', controllerEtablissementCreations.add);

etabCreas.get('/par-id', controllerEtablissementCreations.getById);

etabCreas.post(
	'/etiquettes',
	controllerEtablissementCreations.etiquetteAddMany,
	controllerEtablissementCreations.getMany
);

etabCreas.param('etabCreaId', controllerEtablissementCreationsParam.etabCreaCheck);
etabCreas.post('/:etabCreaId/partage', controllerEtablissementCreations.partage);
etabCreas.get(
	'/:etabCreaId',
	controllerEtablissementCreations.view,
	controllerEtablissementCreations.get
);
etabCreas.post(
	'/:etabCreaId/supprimer-transformation',
	controllerEtablissementCreations.transformationRemove,
	controllerEtablissementCreations.get
);

// toutes les routes suivantes sont protégées si la création d'établissement a déjà été transformée
etabCreas.use('/:etabCreaId', controllerEtablissementCreationsParam.etabCreaTransformationCheck);
etabCreas.post(
	'/:etabCreaId',
	controllerEtablissementCreations.update,
	controllerEtablissementCreations.get
);
etabCreas.delete(
	'/:etabCreaId',
	controllerEtablissementCreations.remove,
	controllerEtablissementCreations.getMany
);
etabCreas.post(
	'/:etabCreaId/ressource',
	controllerEtablissementCreations.ressourceAdd,
	controllerEtablissementCreations.get
);
etabCreas.post(
	'/:etabCreaId/ressource/:ressourceId',
	controllerEtablissementCreations.ressourceUpdate,
	controllerEtablissementCreations.get
);
etabCreas.delete(
	'/:etabCreaId/ressource/:ressourceId',
	controllerEtablissementCreations.ressourceRemove,
	controllerEtablissementCreations.get
);
etabCreas.post(
	'/:etabCreaId/creationAbandonnee',
	controllerEtablissementCreations.creationAbandonneeUpdate,
	controllerEtablissementCreations.get
);

// suivi
// contact
etabCreas.post(
	'/:etabCreaId/createurs',
	controllerEtablissementCreations.createurAdd,
	controllerEtablissementCreations.get
);
etabCreas.post(
	'/:etabCreaId/createurs/:createurId',
	controllerEtablissementCreations.createurUpdate,
	controllerEtablissementCreations.get
);
etabCreas.delete(
	'/:etabCreaId/createurs/:createurId',
	controllerEtablissementCreations.createurRemove,
	controllerEtablissementCreations.get
);

// echange
etabCreas.post(
	'/:etabCreaId/echanges',
	controllerEtablissementCreations.echangeAdd,
	controllerEtablissementCreations.get
);
etabCreas.post(
	'/:etabCreaId/echanges/:echangeId',
	controllerEtablissementCreations.echangeUpdate,
	controllerEtablissementCreations.get
);
etabCreas.delete(
	'/:etabCreaId/echanges/:echangeId',
	controllerEtablissementCreations.echangeRemove,
	controllerEtablissementCreations.get
);

// demande
etabCreas.post(
	'/:etabCreaId/demandes',
	controllerEtablissementCreations.demandeAdd,
	controllerEtablissementCreations.get
);
etabCreas.post(
	'/:etabCreaId/demandes/:demandeId',
	controllerEtablissementCreations.demandeUpdate,
	controllerEtablissementCreations.get
);
etabCreas.post(
	'/:etabCreaId/demandes/:demandeId/cloture',
	controllerEtablissementCreations.demandeCloture,
	controllerEtablissementCreations.get
);
etabCreas.post(
	'/:etabCreaId/demandes/:demandeId/reouverture',
	controllerEtablissementCreations.demandeReouverture,
	controllerEtablissementCreations.get
);
etabCreas.delete(
	'/:etabCreaId/demandes/:demandeId',
	controllerEtablissementCreations.demandeRemove,
	controllerEtablissementCreations.get
);

// rappel
etabCreas.post(
	'/:etabCreaId/rappels',
	controllerEtablissementCreations.rappelAdd,
	controllerEtablissementCreations.get
);
etabCreas.post(
	'/:etabCreaId/rappels/:rappelId',
	controllerEtablissementCreations.rappelUpdate,
	controllerEtablissementCreations.get
);
etabCreas.post(
	'/:etabCreaId/rappels/:rappelId/cloture',
	controllerEtablissementCreations.rappelCloture,
	controllerEtablissementCreations.get
);
etabCreas.post(
	'/:etabCreaId/rappels/:rappelId/reouverture',
	controllerEtablissementCreations.rappelReouverture,
	controllerEtablissementCreations.get
);
etabCreas.delete(
	'/:etabCreaId/rappels/:rappelId',
	controllerEtablissementCreations.rappelRemove,
	controllerEtablissementCreations.get
);

// transformation
etabCreas.post(
	'/:etabCreaId/creer-transformation/:etablissementId',
	controllerEtablissementCreations.transformationAdd,
	controllerEtablissementCreations.get
);

// --------------------------------------------------
//
// 		tests
//
// --------------------------------------------------

export const test = new Router({ prefix: '/test' });

test.use(TestMiddleware);

test.get('/resetAndSeed', controllerTest.resetAndSeed);

test.post('/upsert/:entity', controllerTest.upsert);
