import { Brouillon, Evenement, New } from '../db/types';
import { brouillon } from '../db/schema/suivi';
import * as dbQueryBrouillon from '../db/query/brouillon';
import * as dbQueryEvenement from '../db/query/evenement';
import * as dbQueryEquipe from '../db/query/equipe';
import { Mail } from '../service/email-server';

export const serviceBrouillon = {
	async brouillonAdd({
		evenementId,
		partialBrouillon,
	}: {
		evenementId: Evenement['id'];
		partialBrouillon: New<typeof brouillon>;
	}) {
		const [brouillon] = await dbQueryBrouillon.brouillonAdd({
			brouillon: partialBrouillon,
		});

		await dbQueryEvenement.actionBrouillonAdd({
			evenementId,
			brouillonId: brouillon.id,
			typeId: 'creation',
			diff: {
				message: `Création d'un brouillon`,
			},
		});

		return brouillon;
	},

	async brouillonAddByMail({ from, to, date, sujet, contenu }: Mail) {
		const devecos = await dbQueryEquipe.devecosActifsGetByEmails({
			emails: [...from, ...to],
		});

		if (!devecos.length) {
			console.logInfo(
				`[brouillonAddByMail] Aucun compte avec cet(s) email(s) : ${from.join(', ')}`
			);

			return;
		}

		for (const deveco of devecos) {
			await dbQueryBrouillon.brouillonAdd({
				brouillon: {
					equipeId: deveco.equipeId,
					nom: sujet.trim(),
					date,
					// description: emailContenuCleanup(contenu),
					description: `De: ${from.join(', ')}
À: ${to.join(', ')}
Sujet: ${sujet}

${contenu.trim()}`,
					creationCompteId: deveco.compteId,
				},
			});

			console.logInfo(
				`[brouillonAddByMail] Enregistrement d'un brouillon pour l'email : ${deveco.email} - identifiant : ${deveco.identifiant} - equipe : ${deveco.equipe}`
			);
		}
	},

	async brouillonUpdate({
		evenementId,
		partialBrouillon,
		oldBrouillon,
	}: {
		evenementId: Evenement['id'];
		partialBrouillon: Partial<Brouillon>;
		oldBrouillon: Brouillon;
	}) {
		const { nom, description, date, modificationCompteId, modificationDate } = partialBrouillon;

		const [brouillon] = await dbQueryBrouillon.brouillonUpdate({
			brouillonId: oldBrouillon.id,
			partialBrouillon: {
				nom,
				description,
				date,
				modificationCompteId,
				modificationDate,
			},
		});

		await dbQueryEvenement.actionBrouillonAdd({
			evenementId,
			brouillonId: brouillon.id,
			typeId: 'modification',
			diff: {
				message: `modification d'un brouillon`,
			},
		});

		return brouillon;
	},

	async brouillonRemove({
		evenementId,
		brouillonId,
	}: {
		evenementId: Evenement['id'];
		brouillonId: Brouillon['id'];
	}) {
		await dbQueryBrouillon.brouillonRemove({
			brouillonId,
		});

		await dbQueryEvenement.actionBrouillonAdd({
			evenementId,
			brouillonId,
			typeId: 'suppression',
			diff: {
				message: 'suppression du brouillon',
			},
		});

		return {
			id: brouillonId,
		};
	},
};
