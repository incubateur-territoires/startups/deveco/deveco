import Excel from 'exceljs';

import { ServerResponse } from 'http';
import { ReadStream } from 'fs';

import {
	Equipe,
	Local,
	Etablissement,
	EtablissementCreation,
	EtablissementCreationDemandeWithRelations,
	Cursor,
	QueryResult,
} from '../db/types';
import * as dbQueryEquipeEtablissement from '../db/query/equipe-etablissement';
import * as dbQueryEtablissementCreation from '../db/query/etablissement-creation';
import * as dbQueryEquipeLocal from '../db/query/equipe-local';
import * as dbQueryContact from '../db/query/contact';
import {
	EntiteInfos,
	PersoInfos,
	EtablissementOrEtablissementCreationOrLocalFormatted,
	EntiteType,
	AdresseInfos,
	LocalAdresseInfos,
	etabCreaForExcelFormatMany,
	EntiteNom,
	contactForExcelFormat,
	etabWithDemandesAndEchangesFormat,
	eqLocWithDemandesAndEchangesFormat,
	etabCreaToRowFormat,
	etabToRowFormat,
	eqLocToRowFormat,
} from '../service/_format-export';

export type EntityReadStream<T> = ReadStream & {
	on(event: 'data', listener: (chunk: T) => void): EntityReadStream<T>;
};

type TypedColumn<T> = Partial<Excel.Column> & { key: T };

interface DemandeInfo {
	typeDemande: string;
	titre: string | null;
	description: string | null;
	echanges: string;
	state: string;
	motifCloture: string;
	clotureDate: string;
}

export type WsDemandeRow<T extends EntiteType> = EntiteNom & EntiteInfos<T> & DemandeInfo;

type WsDemandeColumns<T extends EntiteType> = TypedColumn<keyof WsDemandeRow<T>>[];

interface ContactBase {
	email: string | null;
	nom: string | null;
	prenom: string | null;
	telephone: string | null;
	telephone2: string | null;
	dateDeNaissance: string | null;
	demarchageAccepte: string | null;
}

interface ContactInfo extends ContactBase {
	fonction: string | null;
	niveauDiplome: string | null;
	situationProfessionnelle: string | null;
}

export type WsEntiteAndContactRow<T extends EntiteType> = EntiteNom & EntiteInfos<T> & ContactInfo;

type WsEntiteAndContactColumns<T extends EntiteType> = TypedColumn<
	keyof WsEntiteAndContactRow<T>
>[];

interface EchangeInfo {
	titre: string;
	type: string;
	date: Date;
	contenu: string;
	deveco: string;
}

interface LocalOccupantInfo {
	siret: string;
	occupantNom: string;
}

export type WsEchangeRow<T extends EntiteType> = EntiteNom & EntiteInfos<T> & EchangeInfo;

type WsEchangeColumns<T extends EntiteType> = TypedColumn<keyof WsEchangeRow<T>>[];

type WsEntiteColumns<T extends EntiteType> = TypedColumn<keyof PersoInfos<T>>[];

export type WsLocalOccupantRow = EntiteNom & EntiteInfos<Local> & LocalOccupantInfo;

export type WsStructureRow<T extends EntiteType> = EntiteNom &
	PersoInfos<T> &
	EntiteInfos<T> &
	EtablissementOrEtablissementCreationOrLocalFormatted<T>;

type WsEtablissementColums = TypedColumn<keyof WsStructureRow<Etablissement>>[];

type WsEtabCreaColumns = TypedColumn<keyof WsStructureRow<EtablissementCreation>>[];

type WsLocalColumns = TypedColumn<keyof WsStructureRow<Local>>[];

type WsLocalOccupantsColumns = TypedColumn<keyof WsLocalOccupantRow>[];

export type Criteria<T extends EntiteType> = T extends Etablissement
	? dbQueryEquipeEtablissement.EtablissementsParams
	: T extends EtablissementCreation
		? dbQueryEtablissementCreation.EtablissementCreationsParams
		: dbQueryEquipeLocal.EquipeLocalParams;

const adresseColumns: { header: string; key: keyof AdresseInfos }[] = [
	{ header: 'Numéro', key: 'numero' },
	{ header: 'Voie', key: 'voieNom' },
	{ header: 'Code postal', key: 'codePostal' },
	{ header: 'Commune', key: 'commune' },
];

const wsEtablissementColumns: WsEtablissementColums = [
	{ header: 'SIRET', key: 'siret' },
	{ header: 'SIREN', key: 'siren' },
	{ header: 'Nom', key: 'entiteNom' },
	{ header: 'Sigle', key: 'sigle' },
	{ header: 'Siège', key: 'siege' },
	{ header: 'Effectifs', key: 'effectifs' },
	{ header: 'Date des effectifs', key: 'dateEffectifs' },
	{ header: 'Code NAF', key: 'codeNaf' },
	{ header: 'Libellé NAF', key: 'libelleNaf' },
	{ header: 'Catégorie NAF', key: 'libelleCategorieNaf' },
	{ header: 'Date création', key: 'dateCreation' },
	{ header: 'Date fermeture', key: 'dateFermeture' },
	{ header: 'État administratif', key: 'etatAdministratif' },
	{ header: 'Forme juridique', key: 'formeJuridique' },
	{ header: 'Microentreprise', key: 'micro' },
	{ header: `Taille d'entreprise`, key: 'tailleEntreprise' },
	{ header: 'ESS', key: 'ess' },
	{ header: 'Subventions', key: 'subventions' },
	{ header: 'Procédure collective', key: 'procedure' },
	{ header: 'Zonages', key: 'zonages' },
	{ header: 'Local occupé', key: 'local' },
	{ header: 'Diffusible', key: 'diffusible' },
];

const wsEtabCreaColumns: WsEtabCreaColumns = [
	{ header: 'Identifiant', key: 'id' },
	{ header: 'Nom du projet', key: 'nom' },
	{ header: `Nom d'enseigne pressenti`, key: 'enseigne' },
	{ header: 'Description du projet', key: 'description' },
	{ header: 'Type du projet', key: 'projetType' },
	{ header: 'Origine du contact', key: 'contactOrigine' },
	{ header: 'Date du contact', key: 'contactDate' },
	{ header: 'Secteur activité', key: 'secteurActivite' },
	{ header: 'Source de financement', key: 'sourceFinancement' },
	{ header: 'Catégorie juridique envisagée', key: 'categorieJuridiqueEnvisagee' },
	{ header: 'Contrat accompagnement', key: 'contratAccompagnement' },
	{ header: `Recherche d'un local`, key: 'rechercheLocal' },
	{ header: `Date de création`, key: 'dateCreation' },
	{ header: `Commune(s) d'implantation`, key: 'communes' },
];

const etiquettesColumns = [
	{ header: 'Zones géographiques', key: 'zonesGeographiques' },
	{ header: 'Mots-clés', key: 'motsCles' },
	{ header: 'Commentaires', key: 'commentaires' },
] as const;

const wsEntiteEtablissementColumns: WsEntiteColumns<Etablissement> = [
	{ header: 'Activités réelles et filières', key: 'activitesReelles' },
	...etiquettesColumns,
];

const wsEntiteCreateurColumns: WsEntiteColumns<EtablissementCreation> = [
	{ header: 'Activités réelles et filières', key: 'activitesReelles' },
	...etiquettesColumns,
];

const wsEntiteLocalColumns: WsEntiteColumns<Local> = [...etiquettesColumns];

const localAdresseColumns: { header: string; key: keyof LocalAdresseInfos }[] = [
	{ header: 'Numéro', key: 'numero' },
	{ header: 'Voie', key: 'voieNom' },
	// NOTE : pas de code postal pour les locaux
	{ header: 'Code commune INSEE', key: 'communeId' },
	{ header: 'Commune', key: 'commune' },
];

const localNomsColumns = [
	{ header: 'Identifiant', key: 'localId' },
	{ header: 'Invariant', key: 'invariant' },
	{ header: 'Nom', key: 'entiteNom' },
] as const;

const localDonneesPubliquesColumns = [
	{ header: 'Section', key: 'section' },
	{ header: 'Parcelle', key: 'parcelle' },
	{ header: 'Occupé', key: 'occupe' },
	{ header: 'Niveau', key: 'niveau' },
	{ header: 'Surface totale (hors stationnement)', key: 'surfaceTotale' },
	{ header: 'Surface de vente', key: 'surfaceVente' },
	{ header: 'Surface de réserve', key: 'surfaceReserve' },
	{ header: 'Surface extérieure non couverte', key: 'surfaceExterieureNonCouverte' },
	{ header: 'Surface de stationnement couvert', key: 'surfaceStationnementCouvert' },
	{ header: 'Surface de stationnement non couvert', key: 'surfaceStationnementNonCouvert' },
	{ header: 'Catégorie', key: 'categorie' },
	{ header: 'Nature', key: 'nature' },
	{ header: 'Actif', key: 'actif' },
] as const;

const localDonneesPersoColumns = [
	{ header: 'Loyer', key: 'loyer' },
	{ header: 'Type de bail', key: 'bailType' },
	{ header: 'Type de vacance', key: 'vacanceType' },
	{ header: 'Motif de vacance', key: 'vacanceMotif' },
	{ header: 'Remembré', key: 'remembre' },
	{ header: 'Fonds', key: 'fonds' },
	{ header: 'Vente', key: 'vente' },
] as const;

const wsLocalColumns: WsLocalColumns = [
	...localNomsColumns,
	...localAdresseColumns,
	...localDonneesPubliquesColumns,
	...localDonneesPersoColumns,
];

const wsLocalOccupantsColumns: WsLocalOccupantsColumns = [
	...localNomsColumns,
	{ header: 'SIRET', key: 'siret' },
	{ header: "Nom d'établissement", key: 'occupantNom' },
	...localAdresseColumns,
];

const wsDemandesColumns = [
	{ header: 'Titre', key: 'titre' },
	{ header: 'Type', key: 'typeDemande' },
	{ header: 'Description', key: 'description' },
	{ header: 'Échanges', key: 'echanges' },
	{ header: 'État', key: 'state' },
	{ header: 'Motif de clôture', key: 'motifCloture' },
	{ header: 'Date de clôture', key: 'clotureDate' },
] as const;

const wsEchangesColumns = [
	{ header: 'Auteur', key: 'deveco' },
	{ header: 'Titre', key: 'titre' },
	{ header: 'Type', key: 'type' },
	{ header: 'Date', key: 'date' },
	{ header: 'Contenu', key: 'contenu' },
] as const;

const contactColumns = [
	{ header: 'Prénom', key: 'prenom' },
	{ header: 'Nom', key: 'nom' },
	{ header: 'Date de naissance', key: 'dateDeNaissance' },
	{ header: 'Email', key: 'email' },
	{ header: 'Téléphone', key: 'telephone' },
	{ header: 'Téléphone 2', key: 'telephone2' },
	{ header: 'Accord contact', key: 'demarchageAccepte' },
] as const;

const wsEtablissementAndContactsColumns: WsEntiteAndContactColumns<Etablissement> = [
	{ header: 'SIRET', key: 'siret' },
	{ header: "Nom d'établissement", key: 'entiteNom' },
	...adresseColumns,
	{ header: 'Code NAF', key: 'codeNaf' },
	{ header: 'Libellé NAF', key: 'libelleNaf' },
	{ header: 'Fonction', key: 'fonction' },
	...contactColumns,
];

const wsEtablissementCreationAndCreateursColumns: WsEntiteAndContactColumns<EtablissementCreation> =
	[
		{ header: 'Identifiant', key: 'id' },
		{ header: 'Nom du projet', key: 'entiteNom' },
		...contactColumns,
		{ header: 'Niveau de diplôme', key: 'niveauDiplome' },
		{ header: 'Situation professionnelle', key: 'situationProfessionnelle' },
	];

const wsLocalAndContactsColumns: WsEntiteAndContactColumns<Local> = [
	...localNomsColumns,
	...localAdresseColumns,
	{ header: 'Fonction', key: 'fonction' },
	...contactColumns,
];

const wsDemandesEtablissementColumns: WsDemandeColumns<Etablissement> = [
	{ header: 'SIRET', key: 'siret' },
	{ header: "Nom d'établissement", key: 'entiteNom' },
	...adresseColumns,
	{ header: 'Code NAF', key: 'codeNaf' },
	{ header: 'Libellé NAF', key: 'libelleNaf' },
	...wsDemandesColumns,
];

const wsDemandesEtablissementCreationColumns: WsDemandeColumns<EtablissementCreation> = [
	{ header: 'Identifiant', key: 'id' },
	{ header: 'Nom du projet', key: 'entiteNom' },
	...wsDemandesColumns,
];

const wsDemandesLocalColumns: WsDemandeColumns<Local> = [
	...localNomsColumns,
	...localAdresseColumns,
	...wsDemandesColumns,
];

const wsEchangesEtablissementColumns: WsEchangeColumns<Etablissement> = [
	{ header: 'SIRET', key: 'siret' },
	{ header: "Nom d'établissement", key: 'entiteNom' },
	...adresseColumns,
	...wsEchangesColumns,
];

const wsEchangesEtablissementCreationColumns: WsEchangeColumns<EtablissementCreation> = [
	{ header: `Identifiant`, key: 'id' },
	{ header: 'Nom du projet', key: 'entiteNom' },
	...wsEchangesColumns,
];

const wsEchangesLocalColumns: WsEchangeColumns<Local> = [
	...localNomsColumns,
	...localAdresseColumns,
	...wsEchangesColumns,
];

export type WsAnnuaireContactRow = ContactBase & {
	roles: string | null;
};

type WsAnnuaireContactColumns = TypedColumn<keyof WsAnnuaireContactRow>[];

const wsContactColumns: WsAnnuaireContactColumns = [
	...contactColumns,
	{ header: 'Rôle(s)', key: 'roles' },
];

async function etabWithDemandesAndEchangesGetMany({
	equipeId,
	etablissementIds,
}: {
	equipeId: Equipe['id'];
	etablissementIds: Etablissement['siret'][];
}) {
	if (!etablissementIds.length) {
		return;
	}

	const etabs = await dbQueryEquipeEtablissement.etablissementWithEqEtabForExcelGetMany({
		etablissementIds,
		equipeId,
	});

	const demandes = await dbQueryEquipeEtablissement.eqEtabDemandeGetMany({
		equipeId,
		etablissementIds,
		avecClotureDate: true,
	});

	return etabWithDemandesAndEchangesFormat(etabs, demandes);
}

async function eqLocWithDemandesAndEchangesGetMany({
	equipeId,
	localIds,
}: {
	equipeId: Equipe['id'];
	localIds: Local['id'][];
}) {
	if (!localIds.length) {
		return;
	}

	const eqLocs = await dbQueryEquipeLocal.localForExcelGetMany({
		localIds,
		equipeId,
	});

	const demandes = await dbQueryEquipeLocal.eqLocDemandeGetMany({
		equipeId,
		localIds,
		avecClotureDate: true,
	});

	return eqLocWithDemandesAndEchangesFormat(eqLocs, demandes);
}

export const serviceExcel = {
	async etabLookupStreamMany({
		etablissementCursor,
		output,
	}: {
		etablissementCursor: Cursor;
		output: ServerResponse;
	}) {
		const workbook = new Excel.stream.xlsx.WorkbookWriter({
			stream: output,
		});

		const wsEtablissements = workbook.addWorksheet('Établissements');

		const headerRecherche: { header: string; key: string }[] = [];
		const headerResult = [
			{ header: 'SIRET', key: 'rSiret' },
			{ header: 'Nom', key: 'rNom' },
			{ header: 'Nom entreprise', key: 'rNomEntreprise' },
			{ header: 'Siège', key: 'rSiege' },
			{ header: 'Numero', key: 'rNumero' },
			{ header: 'Rue', key: 'rVoieNom' },
			{ header: 'Code commune', key: 'rCommuneId' },
		];

		const MAX_BATCH = 10;

		const etablissements = [];

		function rowsAdd(etabs: any[]) {
			for (const etab of etabs) {
				// console.log('excel', { etab });

				const { rSiret, rNom, rNomEntreprise, rSiege, rNumero, rVoieNom, rCommuneId, ...rest } =
					etab;

				if (headerRecherche.length === 0) {
					headerRecherche.push(
						...Object.keys(rest).map((key) => ({
							header: key,
							key,
						}))
					);

					wsEtablissements.columns = [...headerRecherche, ...headerResult];
				}

				wsEtablissements.addRow(etab).commit();
			}
		}

		for await (const etab of etablissementCursor) {
			etablissements.push(...etab);

			if (etablissements.length >= MAX_BATCH) {
				const batchEtablissements = etablissements.splice(0, MAX_BATCH);

				rowsAdd(batchEtablissements);
			}
		}

		//		console.log('length restante:', etablissements.length);

		if (etablissements.length) {
			rowsAdd(etablissements);
		}

		try {
			await workbook.commit();
		} catch (_e: any) {
			throw new Error('Failed to commit workbook');
		}
	},

	async etabCreaStreamMany({
		etabCreas,
		demandes,
		params,
		output,
	}: {
		etabCreas: QueryResult<typeof dbQueryEtablissementCreation.etabCreaWithRelationsGetManyById>;
		demandes: EtablissementCreationDemandeWithRelations[];
		params: dbQueryEtablissementCreation.EtablissementCreationsParams;
		output: ServerResponse;
	}): Promise<void> {
		const etabCreasForExcel = etabCreaForExcelFormatMany(etabCreas, demandes);

		const workbook = new Excel.stream.xlsx.WorkbookWriter({
			stream: output,
		});

		const wsEtabCrea = workbook.addWorksheet(`Créateurs d'entreprise`);
		const etabCreasColumns: WsEtabCreaColumns = [...wsEtabCreaColumns, ...wsEntiteCreateurColumns];
		wsEtabCrea.columns = etabCreasColumns;

		const wsDemandes = workbook.addWorksheet('Demandes');
		wsDemandes.columns = wsDemandesEtablissementCreationColumns;

		const wsEchanges = workbook.addWorksheet('Échanges');
		wsEchanges.columns = wsEchangesEtablissementCreationColumns;

		const wsEntiteAndCreateurs = workbook.addWorksheet('Créateurs');
		wsEntiteAndCreateurs.columns = wsEtablissementCreationAndCreateursColumns;

		for (const etabCrea of etabCreasForExcel) {
			const {
				row,
				echanges,
				demandes,
				createurs,
			}: {
				row: WsStructureRow<EtablissementCreation>;
				demandes: WsDemandeRow<EtablissementCreation>[];
				echanges: WsEchangeRow<EtablissementCreation>[];
				createurs: WsEntiteAndContactRow<EtablissementCreation>[];
			} = etabCreaToRowFormat({
				params,
				etabCrea,
			});

			wsEtabCrea.addRow(row).commit();

			if (createurs.length > 0) {
				createurs.forEach((createur) => wsEntiteAndCreateurs.addRow(createur).commit());
			}

			if (demandes.length > 0) {
				demandes.forEach((demande) => wsDemandes.addRow(demande).commit());
			}

			if (echanges.length > 0) {
				echanges.forEach((echange) => wsEchanges.addRow(echange).commit());
			}
		}

		try {
			await workbook.commit();
		} catch (_e: any) {
			throw new Error('Failed to commit workbook');
		}
	},

	async eqEtabStreamMany({
		equipeId,
		params,
		etablissementIdCursor,
		output,
	}: {
		equipeId: Equipe['id'];
		params: dbQueryEquipeEtablissement.EtablissementsParams;
		etablissementIdCursor: Cursor<{ etablissement_id: Etablissement['siret'] }>;
		output: ServerResponse;
	}) {
		const workbook = new Excel.stream.xlsx.WorkbookWriter({
			stream: output,
		});

		const wsEtablissements = workbook.addWorksheet('Établissements');
		wsEtablissements.columns = [
			...wsEtablissementColumns,
			...adresseColumns,
			...wsEntiteEtablissementColumns,
		];

		const wsDemandes = workbook.addWorksheet('Demandes');
		wsDemandes.columns = wsDemandesEtablissementColumns;

		const wsEntiteAndContacts = workbook.addWorksheet('Contacts');
		wsEntiteAndContacts.columns = wsEtablissementAndContactsColumns;

		const wsEchanges = workbook.addWorksheet('Échanges');
		wsEchanges.columns = wsEchangesEtablissementColumns;

		async function eqEtabToRowBuildMany(etablissementIds: Etablissement['siret'][]) {
			const etabs = await etabWithDemandesAndEchangesGetMany({
				equipeId,
				etablissementIds,
			});

			if (!etabs) {
				return;
			}

			for (const etab of etabs) {
				const {
					row,
					demandes,
					echanges,
					contacts,
				}: {
					row: WsStructureRow<Etablissement>;
					demandes: WsDemandeRow<Etablissement>[];
					echanges: WsEchangeRow<Etablissement>[];
					contacts: WsEntiteAndContactRow<Etablissement>[];
				} = etabToRowFormat({
					params,
					etab,
				});

				wsEtablissements.addRow(row).commit();

				if (contacts.length > 0) {
					contacts.forEach((contact) => wsEntiteAndContacts.addRow(contact).commit());
				}

				if (demandes.length > 0) {
					demandes.forEach((demande) => wsDemandes.addRow(demande).commit());
				}

				if (echanges.length > 0) {
					echanges.forEach((echange) => wsEchanges.addRow(echange).commit());
				}
			}
		}

		const etablissementIds: string[] = [];

		const MAX_BATCH = 10;

		for await (const idsRaw of etablissementIdCursor) {
			etablissementIds.push(...idsRaw.map((r) => r.etablissement_id));

			while (etablissementIds.length >= MAX_BATCH) {
				const batchEtablissementIds = etablissementIds.splice(0, MAX_BATCH);

				await eqEtabToRowBuildMany(batchEtablissementIds);
			}
		}

		if (etablissementIds.length) {
			await eqEtabToRowBuildMany(etablissementIds);
		}

		await workbook.commit();
	},

	async contactStreamMany({
		input,
		output,
	}: {
		input: AsyncIterable<QueryResult<typeof dbQueryContact.contactWithRelationGetManyById>>;
		output: ServerResponse;
	}) {
		const workbook = new Excel.stream.xlsx.WorkbookWriter({
			stream: output,
		});

		const wsContact = workbook.addWorksheet('Contacts');
		wsContact.columns = wsContactColumns;

		// remarque générale concernant la récupération de requête en cursor :
		// l'ordre des colonnes dépend de la requête { columns: { ... } }
		// or l'ordre des clés d'un objet JavaScript n'est pas garanti
		for await (const rawContacts of input) {
			for (const contact of rawContacts) {
				const contactFormated = contactForExcelFormat(contact);

				wsContact.addRow(contactFormated).commit();
			}
		}

		await workbook.commit();
	},

	async eqLocStreamMany({
		equipeId,
		params,
		localIdCursor,
		output,
	}: {
		equipeId: Equipe['id'];
		params: dbQueryEquipeLocal.EquipeLocalParams;
		localIdCursor: Cursor<{ local_id: string }>;
		output: ServerResponse;
	}) {
		const workbook = new Excel.stream.xlsx.WorkbookWriter({
			stream: output,
		});

		const wsLocaux = workbook.addWorksheet('Locaux');
		wsLocaux.columns = [...wsLocalColumns, ...wsEntiteLocalColumns];

		const wsDemandes = workbook.addWorksheet('Demandes');
		wsDemandes.columns = wsDemandesLocalColumns;

		const wsEntiteAndContacts = workbook.addWorksheet('Contacts');
		wsEntiteAndContacts.columns = wsLocalAndContactsColumns;

		const wsEchanges = workbook.addWorksheet('Échanges');
		wsEchanges.columns = wsEchangesLocalColumns;

		const wsOccupants = workbook.addWorksheet('Occupants');
		wsOccupants.columns = wsLocalOccupantsColumns;

		async function eqLocToRowBuildMany(localIds: Local['id'][]) {
			const eqLocs = await eqLocWithDemandesAndEchangesGetMany({
				equipeId,
				localIds,
			});

			if (!eqLocs) {
				return;
			}

			for (const eqLoc of eqLocs) {
				const { row, demandes, echanges, contacts, occupants } = eqLocToRowFormat({
					params,
					eqLoc,
				});

				wsLocaux.addRow(row).commit();

				if (contacts.length > 0) {
					contacts.forEach((contact) => wsEntiteAndContacts.addRow(contact).commit());
				}

				if (demandes.length > 0) {
					demandes.forEach((demande) => wsDemandes.addRow(demande).commit());
				}

				if (echanges.length > 0) {
					echanges.forEach((echange) => wsEchanges.addRow(echange).commit());
				}

				if (occupants.length > 0) {
					occupants.forEach((occupant) => wsOccupants.addRow(occupant).commit());
				}
			}
		}

		const localIds: string[] = [];

		const MAX_BATCH = 10;

		for await (const idsRaw of localIdCursor) {
			localIds.push(...idsRaw.map((r) => r.local_id));

			while (localIds.length >= MAX_BATCH) {
				const batchLocalIds = localIds.splice(0, MAX_BATCH);

				await eqLocToRowBuildMany(batchLocalIds);
			}
		}

		await eqLocToRowBuildMany(localIds);

		await workbook.commit();
	},
};
