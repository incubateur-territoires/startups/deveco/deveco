import { apiGeo, GeocodeParam } from '../api/api-geo';
import { wait } from '../lib/utils';
import {
	replaceInseeVoieToBanVoie,
	replaceInseeNumeroToBanNumero,
} from '../lib/normalisationAdresse';
import { Etablissement } from '../db/types';
import { Position } from '../db/schema/custom-types/postgis';
import * as dbQueryAdmin from '../db/query/admin';
import * as dbQueryEtablissement from '../db/query/etablissement';
import * as elasticQueryEtablissement from '../elastic/query/etablissement';
import * as controllerFormatEtablissement from '../controller/_format/etablissements';

async function adresseToGeocodeGetMany({ sirets }: { sirets: Etablissement['siret'][] }) {
	const etablissements = await dbQueryEtablissement.etablissementGetManyById({
		sirets,
	});
	if (!etablissements.length) {
		return [];
	}

	return etablissements.reduce((etablissementsWithCommuneId: GeocodeParam[], etablissement) => {
		// on filtre les établissements étrangers et les établissements sans code commune
		if (etablissement.codePaysEtrangerEtablissement || !etablissement.codeCommuneEtablissement) {
			return etablissementsWithCommuneId;
		}

		const numero = controllerFormatEtablissement.etablissementNumeroBuild(etablissement);
		const voieNom = controllerFormatEtablissement.etablissementVoieNomBuild(etablissement);

		etablissementsWithCommuneId.push({
			siret: etablissement.siret,
			communeId: etablissement.codeCommuneEtablissement,
			codePostal: etablissement.codePostalEtablissement ?? '',
			query: [
				// on replace pour améliorer le score auprès de la BAN
				numero ? replaceInseeNumeroToBanNumero(numero) : numero,
				voieNom ? replaceInseeVoieToBanVoie(voieNom) : voieNom,
			]
				.filter(Boolean)
				.join(' '),
		});

		return etablissementsWithCommuneId;
	}, []);
}

async function adresseGeocodeAndUpdateMany({
	adressesToGeocode,
	skipIndexation,
}: {
	adressesToGeocode: GeocodeParam[];
	skipIndexation?: boolean;
}): Promise<number> {
	await wait(100);

	console.logInfo(`[Geocode] geocodage des adresses vers etalab : ${adressesToGeocode.length}`);

	const etablissementAdressesEtalab = await apiGeo.adresseGeocodeGetMany(adressesToGeocode);
	if (!etablissementAdressesEtalab.length) {
		return 0;
	}

	console.logInfo(
		`[Geocode] insertion des adresses etalab : ${etablissementAdressesEtalab.length}`
	);

	await dbQueryEtablissement.etablissementAdresseEtalabUpsertMany({
		etablissementAdressesEtalab,
	});

	await dbQueryEtablissement.etablissementMiseAJourAtUpdateMany({
		etablissementIds: etablissementAdressesEtalab.map(({ siret }) => siret),
	});

	if (!skipIndexation) {
		// on compile les géolocalisations d'établissement à mettre à jour dans l'index de recherche
		const etablissementGeolocalisations = etablissementAdressesEtalab.reduce(
			(
				etablissementGeolocalisations: {
					etablissementId: Etablissement['siret'];
					geolocalisation: Position;
				}[],
				{ siret: etablissementId, score, geolocalisation }
			) => {
				// on n'indexe que les adresses avec géolocalisation et score supérieur à 0.5
				if (!geolocalisation || !score || score <= 0.5) {
					return etablissementGeolocalisations;
				}

				etablissementGeolocalisations.push({
					etablissementId,
					geolocalisation,
				});

				return etablissementGeolocalisations;
			},
			[]
		);

		console.logInfo(
			`[Geocode] indexation des géolocalisations - ${etablissementGeolocalisations.length} documents - début`
		);

		await elasticQueryEtablissement.etablissementGeolocalisationUpdateMany({
			etablissementGeolocalisations,
		});

		console.logInfo(
			`[Geocode] indexation des géolocalisations - ${etablissementGeolocalisations.length} documents - fin`
		);
	}

	return etablissementAdressesEtalab.length;
}

async function* etablissementWithoutGeolocalisationGetCursor({
	miseAJourAt,
}: {
	miseAJourAt?: Date;
}) {
	const etablissementWithSiretsWithoutGeolocalisationCursor =
		dbQueryEtablissement.etablissementAdresseWithoutGeolocalisationCursor({
			miseAJourAt,
		});

	const adressesToGeocode: GeocodeParam[] = [];
	const BATCH_SIZE = 1000;

	for await (const rows of etablissementWithSiretsWithoutGeolocalisationCursor) {
		const sirets = rows.map((r) => r.siret);

		const adresses = await adresseToGeocodeGetMany({
			sirets,
		});

		adressesToGeocode.push(...adresses);

		if (adressesToGeocode.length >= BATCH_SIZE) {
			yield adressesToGeocode.splice(0, 1000);
		}
	}

	// si le buffer contient des dernières adresses à géocoder
	if (adressesToGeocode.length) {
		yield adressesToGeocode;
	}
}

export const serviceGeocode = {
	async etablissementGeocodeUpsertMany({
		tacheNom,
		miseAJourAt,
		skipIndexation,
	}: {
		tacheNom: string;
		miseAJourAt?: Date;
		skipIndexation?: boolean;
	}) {
		console.logInfo(
			`[etablissementGeocodeUpsertMany] - début - ${
				miseAJourAt ? miseAJourAt : 'tous les établissments'
			}`
		);

		const [tache] = await dbQueryAdmin.tacheAdd({ tache: { nom: tacheNom } });
		const tacheId = tache.id;

		// récupère les établissements mis à jour qui ne possèdent pas de géolocalisation INSEE
		const cursor = etablissementWithoutGeolocalisationGetCursor({
			miseAJourAt,
		});

		let total = 0;

		for await (const adressesToGeocode of cursor) {
			total += await adresseGeocodeAndUpdateMany({
				adressesToGeocode,
				skipIndexation,
			});

			console.logInfo(
				`[etablissementGeocodeUpsertMany] - fait ${adressesToGeocode.length} (total ${total})`
			);

			await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { enCoursAt: new Date() } });
		}

		console.logInfo(`[etablissementGeocodeUpsertMany] - fin`);

		await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { finAt: new Date() } });
	},
};
