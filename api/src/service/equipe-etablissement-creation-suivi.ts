import { Equipe, EtablissementCreation, Evenement, Echange, Demande, Rappel } from '../db/types';
import * as dbQueryEtablissementCreation from '../db/query/etablissement-creation';
import * as dbQueryEvenement from '../db/query/evenement';

export const serviceEtablissementCreationSuivi = {
	async echangeAdd({
		evenementId,
		equipeId,
		etabCreaId,
		echange,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		echange: Echange;
	}) {
		const echangeId = echange.id;

		await dbQueryEtablissementCreation.etabCreaEchangeAdd({
			equipeId,
			etabCreaId,
			echangeId,
		});

		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `creation d'un échange`,
				changes: {
					table: `etabCreaEchange`,
					after: {
						equipeId,
						etabCreaId,
						echangeId,
					},
				},
			},
		});
	},

	async echangeUpdate({
		evenementId,
		equipeId,
		etabCreaId,
		echange,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		echange: Echange;
	}) {
		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `modification d'un échange`,
				changes: {
					table: `etabCreaEchange`,
					after: {
						equipeId,
						etabCreaId,
						echangeId: echange.id,
					},
				},
			},
		});
	},

	async echangeRemove({
		evenementId,
		equipeId,
		etabCreaId,
		echangeId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		echangeId: Echange['id'];
	}) {
		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `suppression d'un échange`,
				changes: {
					table: `etabCreaEchange`,
					before: {
						equipeId,
						etabCreaId,
						echangeId,
					},
				},
			},
		});
	},

	async demandeAdd({
		evenementId,
		equipeId,
		etabCreaId,
		demande,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		demande: Demande;
	}) {
		await dbQueryEtablissementCreation.etabCreaDemandeAdd({
			equipeId,
			etabCreaId,
			demandeId: demande.id,
		});

		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `creation d'une demande`,
				changes: {
					table: `etabCreaDemande`,
					after: {
						equipeId,
						etabCreaId,
						demandeId: demande.id,
					},
				},
			},
		});
	},

	async demandeUpdate({
		evenementId,
		equipeId,
		etabCreaId,
		demande,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		demande: Partial<Demande>;
	}) {
		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `modification d'une demande`,
				changes: {
					table: `etabCreaDemande`,
					after: {
						equipeId,
						etabCreaId,
						demandeId: demande.id,
					},
				},
			},
		});
	},

	async demandeCloture({
		evenementId,
		equipeId,
		etabCreaId,
		demande,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		demande: Demande;
	}) {
		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `cloture d'une demande`,
				changes: {
					table: `etabCreaDemande`,
					after: {
						equipeId,
						etabCreaId,
						demandeId: demande.id,
					},
				},
			},
		});
	},

	async demandeReouverture({
		evenementId,
		equipeId,
		etabCreaId,
		demande,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		demande: Demande;
	}) {
		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `reouverture d'une demande`,
				changes: {
					table: `etabCreaDemande`,
					after: {
						equipeId,
						etabCreaId,
						demandeId: demande.id,
					},
				},
			},
		});
	},

	async demandeRemove({
		evenementId,
		equipeId,
		etabCreaId,
		demandeId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		demandeId: Demande['id'];
	}) {
		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `suppression d'une demande`,
				changes: {
					table: `etabCreaDemande`,
					before: {
						equipeId,
						etabCreaId,
						demandeId,
					},
				},
			},
		});
	},

	async rappelAdd({
		evenementId,
		equipeId,
		etabCreaId,
		rappel,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		rappel: Rappel;
	}) {
		await dbQueryEtablissementCreation.etabCreaRappelAdd({
			equipeId,
			etabCreaId,
			rappelId: rappel.id,
		});

		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `creation d'un rappel`,
				changes: {
					table: `etabCreaRappel`,
					after: {
						equipeId,
						etabCreaId,
						rappelId: rappel.id,
					},
				},
			},
		});
	},

	async rappelUpdate({
		evenementId,
		equipeId,
		etabCreaId,
		rappel,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		rappel: Rappel;
	}) {
		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `modification d'un rappel`,
				changes: {
					table: `etabCreaRappel`,
					after: {
						equipeId,
						etabCreaId,
						rappelId: rappel.id,
					},
				},
			},
		});
	},

	async rappelCloture({
		evenementId,
		equipeId,
		etabCreaId,
		rappel,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		rappel: Rappel;
	}) {
		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `cloture d'un rappel`,
				changes: {
					table: `etabCreaRappel`,
					after: {
						equipeId,
						etabCreaId,
						rappelId: rappel.id,
					},
				},
			},
		});
	},

	async rappelReouverture({
		evenementId,
		equipeId,
		etabCreaId,
		rappel,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		rappel: Rappel;
	}) {
		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `reouverture d'un rappel`,
				changes: {
					table: `etabCreaRappel`,
					after: {
						equipeId,
						etabCreaId,
						rappelId: rappel.id,
					},
				},
			},
		});
	},

	async rappelRemove({
		evenementId,
		equipeId,
		etabCreaId,
		rappelId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		rappelId: Rappel['id'];
	}) {
		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `suppression d'un rappel`,
				changes: {
					table: `etabCreaRappel`,
					before: {
						equipeId,
						etabCreaId,
						rappelId,
					},
				},
			},
		});
	},
};
