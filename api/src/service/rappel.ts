import { Rappel, Evenement, New, Brouillon } from '../db/types';
import { rappel } from '../db/schema/suivi';
import * as dbQueryRappel from '../db/query/rappel';
import * as dbQueryEvenement from '../db/query/evenement';

export const serviceRappel = {
	async rappelAdd({
		evenementId,
		partialRappel,
		brouillonId,
	}: {
		evenementId: Evenement['id'];
		partialRappel: New<typeof rappel>;
		brouillonId: Brouillon['id'] | null;
	}) {
		const [rappel] = await dbQueryRappel.rappelAdd({
			rappel: partialRappel,
		});

		await dbQueryEvenement.actionRappelAdd({
			evenementId,
			rappelId: rappel.id,
			typeId: 'creation',
			diff: {
				message: `Création d'un rappel`,
			},
		});

		if (brouillonId) {
			await dbQueryEvenement.actionBrouillonAdd({
				evenementId,
				brouillonId,
				typeId: 'transformation',
				diff: {
					message: `Transformation d'un brouillon en rappel : ${rappel.id}`,
				},
			});
		}

		return rappel;
	},

	async rappelUpdate({
		evenementId,
		partialRappel,
		oldRappel,
	}: {
		evenementId: Evenement['id'];
		partialRappel: Partial<Rappel>;
		oldRappel: Rappel;
	}) {
		const [rappel] = await dbQueryRappel.rappelUpdate({
			rappelId: oldRappel.id,
			partialRappel,
		});

		await dbQueryEvenement.actionRappelAdd({
			evenementId,
			rappelId: rappel.id,
			typeId: 'modification',
			diff: {
				message: `modification d'un rappel`,
			},
		});

		return rappel;
	},

	async rappelCloture({
		evenementId,
		oldRappel,
	}: {
		evenementId: Evenement['id'];
		oldRappel: Pick<Rappel, 'id' | 'clotureDate'>;
	}) {
		const [rappel] = await dbQueryRappel.rappelUpdate({
			rappelId: oldRappel.id,
			partialRappel: {
				clotureDate: new Date(),
			},
		});

		await dbQueryEvenement.actionRappelAdd({
			evenementId,
			rappelId: rappel.id,
			typeId: 'fermeture',
			diff: {
				message: 'cloture du rappel',
			},
		});

		return rappel;
	},

	async rappelReouverture({
		evenementId,
		oldRappel,
	}: {
		evenementId: Evenement['id'];
		oldRappel: Pick<Rappel, 'id' | 'clotureDate'>;
	}) {
		const [rappel] = await dbQueryRappel.rappelUpdate({
			rappelId: oldRappel.id,
			partialRappel: {
				clotureDate: null,
			},
		});

		await dbQueryEvenement.actionRappelAdd({
			evenementId,
			rappelId: rappel.id,
			typeId: 'reouverture',
			diff: {
				message: 'reouverture de la rappel',
			},
		});

		return rappel;
	},

	async rappelRemove({
		evenementId,
		rappelId,
	}: {
		evenementId: Evenement['id'];
		rappelId: Rappel['id'];
	}) {
		await dbQueryRappel.rappelRemove({
			rappelId,
		});

		await dbQueryEvenement.actionRappelAdd({
			evenementId,
			rappelId,
			typeId: 'suppression',
			diff: {
				message: 'suppression du rappel',
			},
		});

		return {
			id: rappelId,
		};
	},
};
