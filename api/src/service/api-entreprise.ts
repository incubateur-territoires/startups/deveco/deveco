import { subWeeks } from 'date-fns';

import { Evenement, Etablissement, Entreprise } from '../db/types';
import * as dbQueryAdmin from '../db/query/admin';
import * as dbQueryEtablissement from '../db/query/etablissement';
import * as elasticQueryEtablissement from '../elastic/query/etablissement';
import * as dbQueryEvenement from '../db/query/evenement';
import { apiEntreprise } from '../api/api-entreprise';
import { wait } from '../lib/utils';
import { Processor } from '../lib/processor';
import { liasseFiscaleFormat } from '../controller/_format/etablissements';
import { isSiren } from '../lib/regexp';

export function isApiEntrepriseSourceRecent(arg: { miseAJourAt: Date | null } | null | undefined) {
	if (!arg?.miseAJourAt) {
		return false;
	}

	const now = new Date();
	const lastWeek = subWeeks(now, 1);

	return arg.miseAJourAt > lastWeek;
}

export const serviceApiEntreprise = {
	async exerciceFix() {
		const processor = new Processor({
			concurrent: 200,
			time: 60 * 1000,
			cooldown: 300,
		});

		const cursor = dbQueryEtablissement.apiEntrepriseEntrepriseExerciceGetCursor({
			size: 100,
		});

		for await (const exercices of cursor) {
			await Promise.all(
				exercices.map(({ siren }) =>
					processor.process(async () => {
						const siege = await dbQueryEtablissement.etablissementSiegeGetBySiren({
							entrepriseId: siren,
						});
						if (!siege) {
							console.logError(`[exerciceFix] Pas de siège pour ce siren : ${siren}`);
							return;
						}

						const siret = siege.siret;

						const payload = await apiEntreprise.exerciceGetMany({ siret });
						if (!payload) {
							return;
						}

						await dbQueryEtablissement.apiEntrepriseEntrepriseExerciceUpsert({
							siren,
							payload,
						});
					})
				)
			);
		}
	},

	async liasseFiscaleReseau() {
		const compte = await dbQueryAdmin.compteGetByNomAndType({
			nom: 'api_entreprise',
			typeId: 'api',
		});
		if (!compte) {
			throw new Error(`[liasseFiscaleReseau] Pas de compte pour l'API Entreprise`);
		}

		const processor = new Processor({
			concurrent: 400,
			time: 60 * 1000,
			cooldown: 100,
		});

		let liassesCount = 0;

		const stop = false;

		while (!stop) {
			let liasse = await dbQueryEtablissement.apiEntrepriseEntrepriseLiasseFiscaleNotProcessedGet();
			if (!liasse) {
				// console.logInfo(`[liasseFiscaleReseau] Pas de liasse fiscale, attente d'1 seconde`);

				await wait(1000);
				continue;
			}

			let evenementId: Evenement['id'] | null = null;

			// Si la liasse est vide (ex : insertion manuelle), on la redemande à l'API Entreprise
			if (!liasse.payload) {
				await processor.process(async () => {
					if (!liasse) {
						return;
					}

					const [evenement] = await dbQueryEvenement.evenementAdd({
						compteId: compte.id,
						typeId: `api_entreprise_update`,
					});

					evenementId = evenement.id;

					console.logInfo(
						`[liasseFiscaleReseau] Payload vide, récupération de la liasse # ${liasse.siren}`
					);

					await serviceApiEntreprise.etablissementLiasseFiscaleUpsert({
						evenementId,
						entrepriseId: liasse.siren,
					});

					liasse = await dbQueryEtablissement.apiEntrepriseEntrepriseLiasseFiscaleGet({
						entrepriseId: liasse.siren,
					});
				});
			}

			const liasseFiscale = liasse?.payload ? liasseFiscaleFormat(liasse) : null;
			if (!liasseFiscale) {
				await dbQueryEtablissement.apiEntrepriseLiasseFiscaleSetReseauFait({
					entrepriseId: liasse.siren,
				});

				console.logInfo(
					`[liasseFiscaleReseau] Pas d'imprimé avec filiale ou détention pour la liasse # ${liasse.siren}, skip`
				);
				continue;
			}

			const sirens = [
				...new Set(
					liasseFiscale.filiales
						.map((f) => f.siren)
						.concat(liasseFiscale.detentions.map((d) => d.siren))
						.concat(liasseFiscale.sirensMere ?? [])
				),
			].flatMap((siren) => (siren && isSiren(siren) ? [siren] : []));

			const sirensToProcess = [];

			if (sirens.length === 0) {
				await dbQueryEtablissement.apiEntrepriseLiasseFiscaleSetReseauFait({
					entrepriseId: liasse.siren,
				});

				console.logInfo(`[liasseFiscaleReseau] Aucun siren pour la liasse # ${liasse.siren}, skip`);
				continue;
			}

			for (const siren of sirens) {
				const liasse = await dbQueryEtablissement.apiEntrepriseEntrepriseLiasseFiscaleGet({
					entrepriseId: siren,
				});
				if (liasse?.reseauFait) {
					console.logInfo(`[liasseFiscaleReseau] Liasse fiscale # ${siren} déjà parcourue, skip`);
					continue;
				}

				sirensToProcess.push(siren);
			}

			console.logInfo(
				`[liasseFiscaleReseau] Récupération des filiales et détentions de la liasse # ${liasse.siren}, sirens : ${sirens}`
			);

			if (!evenementId) {
				const [evenement] = await dbQueryEvenement.evenementAdd({
					compteId: compte.id,
					typeId: `api_entreprise_update`,
				});

				evenementId = evenement.id;
			}

			await Promise.all(
				sirensToProcess.map((siren) =>
					processor.process(async () => {
						if (!evenementId) {
							return;
						}

						try {
							liassesCount += 1;

							if (liassesCount % 100 === 0) {
								console.logInfo(
									`[liasseFiscaleReseau] Liasses fiscales parcourues, total : ${liassesCount}`
								);
							}

							await serviceApiEntreprise.etablissementLiasseFiscaleUpsert({
								evenementId,
								entrepriseId: siren,
							});

							console.logInfo(`[liasseFiscaleReseau] Liasse fiscale # ${siren} récupérée`);
						} catch (e: any) {
							console.logError(
								`[liasseFiscaleReseau] Erreur pendant la récupération de la liasse fiscale # ${siren} : ${e}`
							);
						}
					})
				)
			);

			await dbQueryEtablissement.apiEntrepriseLiasseFiscaleSetReseauFait({
				entrepriseId: liasse.siren,
			});

			console.logInfo(
				`[liasseFiscaleReseau] Liasse fiscale # ${liasse.siren} totalement parcourue`
			);
		}
	},

	async etablissementBeneficiaireUpsert({
		evenementId,
		entrepriseId,
	}: {
		evenementId: Evenement['id'];
		entrepriseId: Entreprise['siren'];
	}) {
		const beneficiaires = await dbQueryEtablissement.apiEntrepriseEntrepriseBeneficiaireEffectifGet(
			{ entrepriseId }
		);
		if (isApiEntrepriseSourceRecent(beneficiaires)) {
			return;
		}

		const payload = await apiEntreprise.beneficiaireEffectifGetMany(entrepriseId);
		if (!payload) {
			return;
		}

		await dbQueryEtablissement.apiEntrepriseEntrepriseBeneficiaireEffectifUpsert({
			entrepriseId,
			payload,
		});

		await dbQueryEvenement.actionEntrepriseForApiEntrepriseAdd({
			entrepriseId,
			evenementId,
			type: 'beneficiaires_effectifs',
		});
	},

	async etablissementMandatairesUpsert({
		evenementId,
		entrepriseId,
	}: {
		evenementId: Evenement['id'];
		entrepriseId: Entreprise['siren'];
	}) {
		const mandataires = await dbQueryEtablissement.apiEntrepriseEntrepriseMandataireSocialGet({
			entrepriseId,
		});
		if (isApiEntrepriseSourceRecent(mandataires)) {
			return;
		}

		const payload = await apiEntreprise.mandataireSocialGetMany(entrepriseId);
		if (!payload) {
			return;
		}

		await dbQueryEtablissement.apiEntrepriseMandataireSocialUpsert({
			entrepriseId,
			payload,
		});

		await dbQueryEvenement.actionEntrepriseForApiEntrepriseAdd({
			entrepriseId,
			evenementId,
			type: 'mandataires_sociaux',
		});
	},

	async etablissementExerciceUpsert({
		evenementId,
		etablissementId,
	}: {
		evenementId: Evenement['id'];
		etablissementId: Etablissement['siret'];
	}) {
		const entrepriseId = etablissementId.substring(0, 9);

		const exercices = await dbQueryEtablissement.apiEntrepriseEntrepriseExerciceGet({
			entrepriseId,
		});
		if (isApiEntrepriseSourceRecent(exercices)) {
			return;
		}

		const payload = await apiEntreprise.exerciceGetMany({ siret: etablissementId });
		if (!payload) {
			return;
		}

		await dbQueryEtablissement.apiEntrepriseEntrepriseExerciceUpsert({
			siren: entrepriseId,
			payload,
		});

		await dbQueryEvenement.actionEntrepriseForApiEntrepriseAdd({
			entrepriseId,
			evenementId,
			type: 'exercices',
		});
	},

	async etablissementLiasseFiscaleUpsert({
		evenementId,
		entrepriseId,
	}: {
		evenementId: Evenement['id'];
		entrepriseId: Etablissement['siret'];
	}) {
		const liasseFiscale = await dbQueryEtablissement.apiEntrepriseEntrepriseLiasseFiscaleGet({
			entrepriseId,
		});
		if (isApiEntrepriseSourceRecent(liasseFiscale)) {
			return;
		}

		const payload = await apiEntreprise.liasseFiscaleGet({ siren: entrepriseId });
		if (!payload) {
			return;
		}

		const [liasse] = await dbQueryEtablissement.apiEntrepriseLiasseFiscaleUpsert({
			entrepriseId,
			payload,
		});

		const { filiales, detentions } = liasseFiscaleFormat(liasse);

		if (filiales.length || detentions.length) {
			const participations: { source: string; cible: string; lien: 'filiale' | 'detention' }[] = [];

			filiales.forEach(({ siren }) => {
				if (!siren) {
					return;
				}

				participations.push({
					source: entrepriseId,
					cible: siren,
					lien: 'filiale',
				});
			});

			detentions.forEach(({ siren }) => {
				if (!siren) {
					return;
				}

				participations.push({
					source: siren,
					cible: entrepriseId,
					lien: 'detention',
				});
			});

			if (participations.length) {
				await dbQueryEtablissement.apiEntrepriseParticipartionUpsertMany({
					participations,
				});

				await elasticQueryEtablissement.etablissementParticipationUpdateMany({
					participations,
				});
			}
		}

		await dbQueryEvenement.actionEntrepriseForApiEntrepriseAdd({
			entrepriseId,
			evenementId,
			type: 'liasse_fiscale',
		});
	},
};
