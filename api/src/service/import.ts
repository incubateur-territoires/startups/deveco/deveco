import ExcelJS from 'exceljs';

import { Readable } from 'stream';

import { serviceEmailTemplatesCompte } from './email-templates/compte';

import { emailSend, smtpCheck } from '../lib/email';
import { Compte, Deveco } from '../db/types';
import * as dbQueryEquipe from '../db/query/equipe';
import * as dbQueryAdmin from '../db/query/admin';
import {
	CallbackResult,
	isError,
	serviceEquipeEtablissement,
} from '../service/equipe-etablissement';

export interface CreateurImport {
	nom: string;
	prenom: string;
	email: string;
	telephone: string;
	futureEnseigne: string;
	description: string;
}

export interface ContactImport {
	siret: string;
	fonction: string;
	nom: string;
	prenom: string;
	email: string;
	telephone: string;
	dateDeNaissance: string | Date;
}

export interface QualificationImport {
	siret: string;
	activites: string[];
	zones: string[];
	mots: string[];
	autre?: string;
}

export interface EchangeImport {
	siret: string;
	auteur: string;
	titre: string;
	type: string;
	date: string | Date;
	contenu: string;
}

type ImportCallback<T> = ({
	deveco,
}: {
	deveco: Deveco;
}) => Promise<(ligne: T) => Promise<CallbackResult>>;

interface SheetConfig<T> {
	sheet: 'Etiquettes' | 'Contacts' | 'Echanges' | "Créateurs d'entreprise";
	rowToData: (row: any) => T;
	callback: ImportCallback<T>;
}

const etiquettesSheetConfig: SheetConfig<QualificationImport> = {
	sheet: 'Etiquettes',
	rowToData: (row: Record<string, string>) => ({
		siret: `${row[0] || ''}`,
		activites: `${row[2] ?? ''}`
			.split(';')
			.map((s) => s.trim())
			.filter(Boolean),
		zones: `${row[3] ?? ''}`
			.split(';')
			.map((s) => s.trim())
			.filter(Boolean),
		mots: `${row[4] ?? ''}`
			.split(';')
			.map((s) => s.trim())
			.filter(Boolean),
		autre: `${row[5] ?? ''}`,
	}),
	/* eslint-disable-next-line @typescript-eslint/unbound-method */
	callback: serviceEquipeEtablissement.etiquetteImportBuild,
};

const contactsSheetConfig: SheetConfig<ContactImport> = {
	sheet: 'Contacts',
	rowToData: (row) => ({
		siret: `${row[0] || ''}`,
		fonction: row[2] || '',
		nom: row[3] || '',
		prenom: row[4] || '',
		email: row[5] || '',
		telephone: row[6] || '',
		dateDeNaissance: row[7] || '',
	}),
	/* eslint-disable-next-line @typescript-eslint/unbound-method */
	callback: serviceEquipeEtablissement.contactImportBuild,
};

const echangesSheetConfig: SheetConfig<EchangeImport> = {
	sheet: 'Echanges',
	rowToData: (row) => ({
		siret: `${row[0] || ''}`,
		auteur: row[2] || '',
		titre: row[3] || '',
		type: row[4] || '',
		date: row[5] || '',
		contenu: row[6] || '',
	}),
	/* eslint-disable-next-line @typescript-eslint/unbound-method */
	callback: serviceEquipeEtablissement.echangeImportBuild,
};

// const createursSheetConfig: SheetConfig<CreateurImport> = {
//   sheet: "Créateurs d'entreprise",
//   rowToData: (row) => ({
//     nom: row[0] || '',
//     prenom: row[1] || '',
//     email: row[2] || '',
//     telephone: row[3] || '',
//     futureEnseigne: row[4] || '',
//     description: row[5] || '',
//   }),
//   callback: serviceEtablissementCreation.importBuild,
// };

const importSheet = async <T>({
	deveco,
	excelFile,
	sheetConfig,
}: {
	deveco: Deveco;
	excelFile: Buffer;
	sheetConfig: SheetConfig<T>;
}) => {
	const { sheet, rowToData, callback } = sheetConfig;

	console.logInfo(`[Deveco ${deveco.id}] Import, feuille "${sheet}"...`);

	let stream;

	try {
		const workbookReader = new ExcelJS.stream.xlsx.WorkbookReader(Readable.from(excelFile), {});

		for await (const worksheetReader of workbookReader) {
			const { name } = worksheetReader as unknown as any;

			if (name === sheet) {
				stream = worksheetReader;

				break;
			}
		}
	} catch (e: any) {
		console.logError(`[Deveco ${deveco.id}]`, e.message);
		return '';
	}

	if (!stream) {
		return '';
	}

	const results = [];

	const dataHandlerWithEvenementCreation = await callback({ deveco });

	let i = 0;
	let rows = 0;

	for await (const row of stream) {
		i += 1;

		if (i <= 2 || !Array.isArray(row.values)) {
			continue;
		}

		/* eslint-disable @typescript-eslint/no-unsafe-call */
		const values = row.values.slice(1).map((s: any): string => {
			if (Array.isArray(s?.richText)) {
				return s.richText
					.map((t: { text: string } | undefined) => t?.text || '')
					.join('') as string;
			}

			if (typeof s === 'string') {
				return s.trim();
			}

			return (s?.toString() as string) ?? null;
		});
		/* eslint-enable @typescript-eslint/no-unsafe-call */

		if (values.filter((v) => v).length === 0) {
			continue;
		}

		rows += 1;

		const data = rowToData(values);

		try {
			// TODO ajouter les lignes en batch plutôt qu'une par une
			const result = await dataHandlerWithEvenementCreation(data);

			if (isError(result)) {
				results.push(result);
			}
		} catch (e) {
			console.logError(`[Deveco ${deveco.id}]`, e);
		}
	}

	if (rows === 0) {
		console.logInfo(`[Deveco ${deveco.id}] Import, feuille "${sheet}": Feuille vide, ignorée`);
	}

	return results.map(({ error }) => error).join('\n');
};

export const importSheets = async ({
	excelFile,
	compteId,
	admin,
}: {
	excelFile: Buffer;
	compteId: Compte['id'];
	admin: Pick<Compte, 'email'>;
}) => {
	const deveco = await dbQueryEquipe.devecoGetByCompte({ compteId });

	if (!deveco) {
		throw new Error(`Impossible de trouver un Deveco avec le compte ${compteId}`);
	}

	const reports = [`Bilan de l'import\n`];
	let empty = true;

	const bilanEtiquettes = await importSheet({
		excelFile,
		deveco,
		sheetConfig: etiquettesSheetConfig,
	});

	if (bilanEtiquettes && bilanEtiquettes.length > 0) {
		reports.push('\nÉtiquettes :', bilanEtiquettes);
		empty = false;
	}

	const bilanEchanges = await importSheet({
		excelFile,
		deveco,
		sheetConfig: echangesSheetConfig,
	});

	if (bilanEchanges && bilanEchanges.length > 0) {
		reports.push('\nÉchanges :', bilanEchanges);
		empty = false;
	}

	const bilanContacts = await importSheet({
		excelFile,
		deveco,
		sheetConfig: contactsSheetConfig,
	});

	if (bilanContacts && bilanContacts.length > 0) {
		reports.push('\nContacts', bilanContacts);
		empty = false;
	}

	if (empty) {
		reports.push('Aucune erreur !');
	}

	const content = reports.join('\n');

	const appUrl = process.env.APP_URL || '';

	const compte = await dbQueryAdmin.compteGet({ compteId });
	if (!compte) {
		console.logError(`[importSheets] - Impossible ! Pas de compte pour cet id : ${compteId}`);
		return;
	}

	if (!smtpCheck()) {
		console.logInfo(`Pas de configuration SMTP`);
		console.logInfo(`Compte-rendu d'import`);
		console.logInfo(`=====================`);
		console.logInfo(content);
		console.logInfo(`Fin du compte-rendu d'import`);
		console.logInfo(`============================`);
		return;
	}

	const html = serviceEmailTemplatesCompte.importTermine({
		url: {
			appUrl,
			cle: compte.cle ?? undefined,
		},
		compte,
	});

	try {
		await emailSend({
			to: compte.email,
			bcc: admin.email,
			subject: `Deveco - L'import de vos données est terminé !`,
			html,
			attachments: [
				{
					filename: 'Deveco-bilan-import.txt',
					content,
				},
			],
		});
	} catch (error) {
		console.logError(error);
	}
};
