import isDate from 'date-fns/isDate';

import { isSiret } from '../lib/regexp';
import {
	ActionDiff,
	Etiquette,
	Equipe,
	Contact,
	Compte,
	Deveco,
	EquipeEtablissementContact,
	Evenement,
	New,
	EquipeEtablissementContribution,
	echangeTypeIds,
	EchangeTypeId,
	RessourceEtablissement,
	Etablissement,
	Entreprise,
	EquipeEtablissementEtiquette,
} from '../db/types';
import { eqEtabRessource } from '../db/schema/equipe-etablissement';
import * as dbQueryEtablissement from '../db/query/etablissement';
import * as dbQueryEquipeEtablissement from '../db/query/equipe-etablissement';
import * as dbQueryEquipe from '../db/query/equipe';
import * as dbQueryEvenement from '../db/query/evenement';
import * as dbQueryAdmin from '../db/query/admin';
import * as elasticQueryEquipeEtablissement from '../elastic/query/equipe-etablissement';
import * as elasticQueryEtablissement from '../elastic/query/etablissement';
import elastic from '../elastic';
import { serviceEchange } from '../service/echange';
import { serviceContact } from '../service/contact';
import { serviceApiEntreprise } from '../service/api-entreprise';
import { equipeEtiquetteUpsertMany } from '../service/equipe';
import { ContactImport, EchangeImport, QualificationImport } from '../service/import';
import { serviceEquipeEtablissementSuivi } from '../service/equipe-etablissement-suivi';
import { etablissementParamsInit } from '../controller/flux-etablissements';

async function etiquetteUpdate({
	evenementId,
	equipeId,
	etablissementId,
	etiquettes,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	etiquettes: Etiquette[];
}) {
	const etiquetteIds = etiquettes.map(({ id }) => id);

	const { ajoutees, supprimees } = await dbQueryEquipeEtablissement.eqEtabEtiquetteUpdate({
		equipeId,
		etablissementId,
		etiquetteIds,
	});

	const etiquettesAjoutees = etiquettes.filter((etiquette) => ajoutees.includes(etiquette.id));
	const etiquettesSupprimees = etiquettes.filter((etiquette) => supprimees.includes(etiquette.id));

	if (!etiquettesAjoutees.length && !etiquettesSupprimees.length) {
		return;
	}

	const messageAjout = etiquettesAjoutees.length ? `Ajout d'étiquettes` : '';
	const messageSuppression = etiquettesSupprimees.length ? `Suppression d'étiquettes` : '';

	await dbQueryEvenement.actionEquipeEtablissementAdd({
		evenementId,
		equipeId,
		etablissementId,
		typeId: 'modification',
		diff: {
			message: `Modification des étiquettes :
${messageAjout}
${messageSuppression}`,
			changes: {
				after: {
					etiquetteIds,
				},
			},
		},
	});

	await dbQueryEvenement.actionEtiquetteEtablissementAddMany({
		evenementId,
		equipeId,
		etablissementIds: [etablissementId],
		etiquettesAjoutees,
		etiquettesSupprimees,
	});
}

async function eqEtabEtiquettesUpdate({
	evenementId,
	equipeId,
	etablissementId,
	activites,
	localisations,
	motsCles,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	activites: Etiquette['nom'][];
	localisations: Etiquette['nom'][];
	motsCles: Etiquette['nom'][];
}) {
	// récupère les étiquettes existantes,
	// crée celles qui manquent
	const etiquettes = await equipeEtiquetteUpsertMany({
		evenementId,
		equipeId,
		activites,
		localisations,
		motsCles,
	});

	// modifie les étiquettes sur l'établissement
	await etiquetteUpdate({
		evenementId,
		equipeId,
		etablissementId,
		etiquettes,
	});

	// met à jour ElasticSearch
	await elasticQueryEquipeEtablissement.eqEtabEtiquetteUpdate({
		equipeId,
		etablissementId,
		etiquetteIds: etiquettes.map((e) => e.id),
	});
}

async function eqEtabEtiquetteAndDescriptionAdd({
	evenementId,
	equipeId,
	compteId,
	etablissementId,
	activites,
	localisations,
	motsCles,
	description,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	compteId: Compte['id'];
	etablissementId: Etablissement['siret'];
	activites: Etiquette['nom'][];
	localisations: Etiquette['nom'][];
	motsCles: Etiquette['nom'][];
	description: EquipeEtablissementContribution['description'];
}) {
	// ajoute les étiquettes sur l'établissement
	const etiquettes = await serviceEquipeEtablissement.etiquetteAddMany({
		evenementId,
		equipeId,
		etablissementIds: [etablissementId],
		activites,
		localisations,
		motsCles,
	});

	// ajoute la description sur l'établissement
	await serviceEquipeEtablissement.descriptionUpdate({
		evenementId,
		equipeId,
		compteId,
		etablissementId,
		description,
	});

	if (etiquettes.length) {
		// met à jour ElasticSearch
		await elasticQueryEquipeEtablissement.eqEtabEtiquetteUpdate({
			equipeId,
			etablissementId,
			etiquetteIds: etiquettes.map((e) => e.id),
		});
	}
}

export type CallbackResult =
	| { success: string; __kind: 'success' }
	| { error: string; __kind: 'error' };

export const callbackError = (message: string) => ({ error: message, __kind: 'error' }) as const;
export const callbackSuccess = (message: string) =>
	({ success: message, __kind: 'success' }) as const;
export const isError = (test: CallbackResult): test is { error: string; __kind: 'error' } => {
	return test.__kind === 'error';
};

export const serviceEquipeEtablissement = {
	async contributionUpdate({
		evenementId,
		equipeId,
		compteId,
		etablissementId,
		contribution,
		diff,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		compteId: Compte['id'];
		etablissementId: Etablissement['siret'];
		contribution: Partial<EquipeEtablissementContribution>;
		diff: ActionDiff;
	}) {
		// modification de l'etablissement
		const [newContribution] = await dbQueryEquipeEtablissement.eqEtabContributionUpsert({
			equipeId,
			etablissementId,
			contribution: {
				...contribution,
				modificationDate: new Date(),
				modificationCompteId: compteId,
			},
		});

		if (
			typeof newContribution['clotureDate'] !== 'undefined' ||
			typeof newContribution['enseigne'] !== 'undefined' ||
			typeof newContribution['geolocalisation'] !== 'undefined'
		) {
			await elasticQueryEquipeEtablissement.eqEtabContributionUpdate({
				equipeId,
				etablissementId,
				contribution: newContribution,
			});

			if (typeof newContribution['geolocalisation'] !== 'undefined') {
				let geolocalisation;

				// si on assigne une géolocalisation, alors on l'utilise pour toutes les équipes
				if (newContribution['geolocalisation']) {
					geolocalisation = newContribution['geolocalisation'];
				} else {
					// si on supprime la géolocalisation, alors on réindexe la géolocalisation d'origine
					const [etabGeolocalisation] = await dbQueryEtablissement.etablissementGeolocalisationGet({
						etablissementId,
					});
					geolocalisation = etabGeolocalisation?.geolocalisation ?? null;
				}

				await elasticQueryEtablissement.etablissementGeolocalisationUpdateMany({
					etablissementGeolocalisations: [
						{
							etablissementId,
							geolocalisation,
						},
					],
				});
			}
		}

		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff,
		});

		return newContribution;
	},

	clotureDateUpdate({
		evenementId,
		equipeId,
		compteId,
		etablissementId,
		clotureDate,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		compteId: Compte['id'];
		etablissementId: Etablissement['siret'];
		clotureDate: Date | null;
	}) {
		return serviceEquipeEtablissement.contributionUpdate({
			evenementId,
			equipeId,
			compteId,
			etablissementId,
			contribution: {
				clotureDate,
			},
			diff: {
				message: `mise à jour de la date de cloture : ${clotureDate}.`,
				changes: {
					after: {
						clotureDate,
					},
				},
			},
		});
	},

	enseigneUpdate({
		evenementId,
		equipeId,
		compteId,
		etablissementId,
		enseigne,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		compteId: Compte['id'];
		etablissementId: Etablissement['siret'];
		enseigne: EquipeEtablissementContribution['enseigne'];
	}) {
		return serviceEquipeEtablissement.contributionUpdate({
			evenementId,
			equipeId,
			compteId,
			etablissementId,
			contribution: {
				enseigne,
			},
			diff: {
				message: `mise à jour de l'enseigne : ${enseigne ?? 'suppression'}.`,
				changes: {
					after: {
						enseigne: enseigne ?? 'suppression',
					},
				},
			},
		});
	},

	async geolocalisationUpdate({
		evenementId,
		equipeId,
		compteId,
		etablissementId,
		geolocalisation,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		compteId: Compte['id'];
		etablissementId: Etablissement['siret'];
		geolocalisation: EquipeEtablissementContribution['geolocalisation'];
	}) {
		await serviceEquipeEtablissement.contributionUpdate({
			evenementId,
			equipeId,
			compteId,
			etablissementId,
			contribution: {
				geolocalisation,
			},
			diff: {
				message: `mise à jour de la géolocalisation : ${geolocalisation ?? 'suppression'}.`,
				changes: {
					after: {
						geolocalisation: geolocalisation ?? 'suppression',
					},
				},
			},
		});

		const [{ qpv2024Id } = { qpv2024Id: null }] =
			await dbQueryEtablissement.etablissementOnQpv2024GetMany({
				etablissementIds: [etablissementId],
			});

		const [{ qpv2015Id } = { qpv2015Id: null }] =
			await dbQueryEtablissement.etablissementOnQpv2015GetMany({
				etablissementIds: [etablissementId],
			});

		await elasticQueryEtablissement.etablissementQpvUpdate({
			etablissementId,
			qpv2024Id,
			qpv2015Id,
		});
	},

	descriptionUpdate({
		evenementId,
		equipeId,
		compteId,
		etablissementId,
		description,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		compteId: Compte['id'];
		etablissementId: Etablissement['siret'];
		description: EquipeEtablissementContribution['description'];
	}) {
		return serviceEquipeEtablissement.contributionUpdate({
			evenementId,
			equipeId,
			compteId,
			etablissementId,
			contribution: {
				description,
			},
			diff: {
				message: `mise à jour de la description`,
				changes: {
					after: {
						description: description ?? 'suppression',
					},
				},
			},
		});
	},

	async favoriUpdate({
		evenementId,
		equipeId,
		devecoId,
		etablissementId,
		favori,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		devecoId: Deveco['id'];
		etablissementId: Etablissement['siret'];
		favori: boolean;
	}) {
		if (favori) {
			await dbQueryEquipeEtablissement.eqEtabFavoriAdd({
				devecoId,
				etablissementId,
			});
		} else {
			await dbQueryEquipeEtablissement.eqEtabFavoriRemove({
				devecoId,
				etablissementId,
			});
		}

		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `${favori ? 'ajout' : 'suppression'} du favori`,
				changes: {
					after: {
						favori,
					},
				},
			},
		});

		// met à jour ElasticSearch
		await elasticQueryEquipeEtablissement.eqEtabFavoriUpdate({
			equipeId,
			devecoId,
			etablissementId,
			favori,
		});
	},

	async etiquetteAddMany({
		evenementId,
		equipeId,
		etablissementIds,
		activites,
		localisations,
		motsCles,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementIds: Etablissement['siret'][];
		activites: Etiquette['nom'][];
		localisations: Etiquette['nom'][];
		motsCles: Etiquette['nom'][];
	}) {
		const etiquettes = await equipeEtiquetteUpsertMany({
			evenementId,
			equipeId,
			activites,
			localisations,
			motsCles,
		});
		if (!etiquettes.length) {
			return [];
		}

		const etiquetteIds = etiquettes.map(({ id }) => id);

		// TODO:
		// actuellement, la qualification en masse est limitée à 1000 établissements
		// le batch n'est pas nécessaire. Il faudrait utiliser un curseur.
		const batch = 1000;
		while (etablissementIds.length) {
			const batchEtablissementIds = etablissementIds.splice(0, batch);

			// on crée les liens entre établissement et étiquette, un par un

			const eqEtabEtiquettes = batchEtablissementIds.reduce(
				(values, etablissementId) => [
					...values,
					...etiquetteIds.map((etiquetteId) => ({
						equipeId,
						etablissementId,
						etiquetteId,
						auto: false,
					})),
				],
				[] as EquipeEtablissementEtiquette[]
			);

			const eqEtabEtiquettesAdded = await dbQueryEquipeEtablissement.eqEtabEtiquetteAddMany({
				eqEtabEtiquettes,
			});

			if (eqEtabEtiquettesAdded.length === 0) {
				continue;
			}

			// met à jour l'index de recherche en ajoutant les étiquettes pour tous les établissements
			await elasticQueryEquipeEtablissement.eqEtabEtiquetteAddMany({
				equipeId,
				etablissementIds: eqEtabEtiquettesAdded.map((e) => e.etablissementId),
				etiquetteIds,
			});

			// ajoute l'événement d'ajout d'une étiquette à un établissement (etiquette x établissement)
			await dbQueryEvenement.actionEtiquetteEtablissementAddMany({
				evenementId,
				equipeId,
				etablissementIds: batchEtablissementIds,
				etiquettesAjoutees: etiquettes,
				etiquettesSupprimees: [],
			});

			// on ajoute un événement de modification d'eqEtab par établissement
			await dbQueryEvenement.actionEquipeEtablissementAddMany({
				evenementId,
				equipeId,
				etablissementIds: batchEtablissementIds,
				diff: {
					message: `Modification des étiquettes`,
					changes: {
						after: {
							etiquetteIds,
						},
					},
				},
				typeId: 'modification',
			});
		}

		return etiquettes;
	},

	async etiquetteImportBuild({ deveco }: { deveco: Deveco }) {
		const equipeId = deveco.equipeId;
		const compteId = deveco.compteId;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_etablissement_etiquette_description_import',
		});

		const evenementId = evenement.id;

		return async (ligne: QualificationImport) => {
			let message = 'OK';

			const { siret: fuzzy, activites, zones, mots, autre } = ligne;

			try {
				if (!isSiret(fuzzy)) {
					throw new Error(`SIRET invalide : ${fuzzy}`);
				}

				const etablissementId = fuzzy.replaceAll(/\s/g, '');

				const etablissement = await dbQueryEtablissement.etablissementGet({
					etablissementId,
				});

				if (!etablissement) {
					throw new Error(`Aucun établissement trouvé pour le SIRET ${etablissementId}`);
				}

				await eqEtabEtiquetteAndDescriptionAdd({
					evenementId,
					equipeId,
					compteId,
					etablissementId,
					activites: activites ?? [],
					localisations: zones ?? [],
					motsCles: mots ?? [],
					description: autre ?? '',
				});
				return callbackSuccess(`${fuzzy}: ${message}`);
			} catch (e: unknown) {
				message = e instanceof Error ? (message = e.message) : 'Erreur inconnue';
				return callbackError(`${fuzzy}: ${message}`);
			}
		};
	},

	async etiquetteUpdate({
		evenementId,
		equipeId,
		etablissementId,
		activites,
		localisations,
		motsCles,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		activites: Etiquette['nom'][];
		localisations: Etiquette['nom'][];
		motsCles: Etiquette['nom'][];
	}) {
		await eqEtabEtiquettesUpdate({
			evenementId,
			equipeId,
			etablissementId,
			activites,
			localisations,
			motsCles,
		});
	},

	async get({
		etablissementId,
		devecoId,
		equipeId,
	}: {
		etablissementId: Etablissement['siret'];
		devecoId: Deveco['id'];
		equipeId: Equipe['id'];
	}) {
		const etab = await dbQueryEtablissement.etablissementWithAllRelationsGet({
			etablissementId,
			equipeId,
			devecoId,
		});

		if (!etab) {
			return undefined;
		}

		const entrepriseId = etab.siren;

		if (!etab.ul) {
			console.logError(
				`[etablissements.get] Pas d'entreprise pour cet établissement : ${etablissementId}`
			);
		} else if (!etab.ul.siege) {
			console.logError(`[etablissements.get] Pas de siège pour cette entreprise : ${entrepriseId}`);
		}

		const freres = await serviceEquipeEtablissement.frereGetMany({
			equipeId,
			devecoId,
			etablissementId,
			entrepriseId: entrepriseId,
		});

		return {
			etab,
			eqEtab: etab,
			freres,
		};
	},

	async view({
		evenementId,
		equipeId,
		etablissementId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
	}) {
		setTimeout(() => {
			void (async () => {
				if (process.env.NODE_ENV === 'test') {
					return;
				}

				const compte = await dbQueryAdmin.compteGetByNomAndType({
					nom: 'api_entreprise',
					typeId: 'api',
				});
				if (!compte) {
					throw new Error(`Pas de compte pour l'API Entreprise`);
				}

				const [evenement] = await dbQueryEvenement.evenementAdd({
					compteId: compte.id,
					typeId: `api_entreprise_update`,
				});

				const evenementId = evenement.id;

				const entrepriseId = etablissementId.substring(0, 9);

				// fire and forget
				serviceApiEntreprise
					.etablissementMandatairesUpsert({
						evenementId,
						entrepriseId,
					})
					.catch((e) =>
						console.logError(
							`[etablissement.view] Erreur pendant la récupération des mandataires ${e}`
						)
					);

				// fire and forget
				serviceApiEntreprise
					.etablissementBeneficiaireUpsert({
						evenementId,
						entrepriseId,
					})
					.catch((e) =>
						console.logError(
							`[etablissement.view] Erreur pendant la récupération des bénéficiaires ${e}`
						)
					);

				// fire and forget
				serviceApiEntreprise
					.etablissementLiasseFiscaleUpsert({
						evenementId,
						entrepriseId,
					})
					.catch((e) =>
						console.logError(
							`[etablissement.view] Erreur pendant la récupération de la liasse fiscale ${e}`
						)
					);

				// fire and forget
				serviceApiEntreprise
					.etablissementExerciceUpsert({
						evenementId,
						etablissementId,
					})
					.catch((e) =>
						console.logError(
							`[etablissement.view] Erreur pendant la récupération des exercices ${e}`
						)
					);
			})();
		}, 100);

		await elasticQueryEquipeEtablissement.eqEtabConsultationUpdate({
			equipeId,
			etablissementId,
			consultationAt: new Date(),
		});

		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'affichage',
		});
	},

	participationGet({ entrepriseId }: { entrepriseId: Entreprise['siren'] }) {
		return dbQueryEtablissement.entrepriseParticipationReseauGet({
			entrepriseId,
		});
	},

	ressourceAdd({
		etablissementId,
		equipeId,
		partialRessource,
	}: {
		etablissementId: Etablissement['siret'];
		equipeId: Equipe['id'];
		partialRessource: New<typeof eqEtabRessource>;
	}) {
		return dbQueryEquipeEtablissement.ressourceAdd({
			equipeId,
			etablissementId,
			partialRessource,
		});
	},

	ressourceUpdate({
		ressourceId,
		partialRessource,
	}: {
		ressourceId: RessourceEtablissement['id'];
		partialRessource: Partial<RessourceEtablissement>;
	}) {
		return dbQueryEquipeEtablissement.ressourceUpdate({ ressourceId, partialRessource });
	},

	ressourceRemove({ ressourceId }: { ressourceId: RessourceEtablissement['id'] }) {
		return dbQueryEquipeEtablissement.ressourceRemove({ ressourceId });
	},

	async suiviParEquipeNomGetManyByEtablissementId({
		equipeId,
		etablissementId,
	}: {
		etablissementId: Etablissement['siret'];
		equipeId: Equipe['id'];
	}) {
		const ee = await dbQueryEquipeEtablissement.suiviParEquipeNomGetManyByEtablissementId({
			equipeId,
			etablissementId,
		});

		return ee.map(({ nom, values }) => ({
			nom,
			...Object.fromEntries(values.map(({ type, count }) => [type, count])),
		}));
	},

	async frereGetMany({
		etablissementId,
		entrepriseId,
		equipeId,
		devecoId,
	}: {
		etablissementId: Etablissement['siret'];
		entrepriseId: Entreprise['siren'];
		equipeId: Equipe['id'];
		devecoId: Deveco['id'];
	}) {
		const etablissementIds = await elasticQueryEquipeEtablissement.eqEtabFrereIdGetMany({
			equipeId,
			etablissementId,
			entrepriseId,
		});

		if (!etablissementIds.length) {
			return [];
		}

		return dbQueryEquipeEtablissement.etablissementWithEqEtabWithRelationsGetMany({
			equipeId,
			devecoId,
			etablissementIds,
		});
	},

	async ouvertFermeGetMany({
		equipeId,
		debut,
		fin,
	}: {
		equipeId: Equipe['id'];
		debut: Date;
		fin: Date;
	}) {
		const ouv = await elasticQueryEquipeEtablissement.eqEtabIdSortedGetMany({
			...etablissementParamsInit({
				equipeId,
			}),
			creeApres: debut,
			creeAvant: fin,
			elementsParPage: 20,
			page: 1,
			direction: 'desc',
			tri: 'creation',
		});

		const ferm = await elasticQueryEquipeEtablissement.eqEtabIdSortedGetMany({
			...etablissementParamsInit({
				equipeId,
			}),
			equipeId,
			fermeApres: debut,
			fermeAvant: fin,
			elementsParPage: 20,
			page: 1,
			direction: 'desc',
			tri: 'creation',
		});

		const proc = await elasticQueryEquipeEtablissement.eqEtabIdSortedGetMany({
			...etablissementParamsInit({
				equipeId,
			}),
			equipeId,
			procedure: true,
			procApres: debut,
			procAvant: fin,
			elementsParPage: 20,
			page: 1,
			direction: 'desc',
			tri: 'creation',
		});

		return { ouverts: ouv.total, fermes: ferm.total, procedures: proc.total };
	},

	async eqEtabCountGetByCreationDate({
		equipeId,
		debut,
		fin,
	}: {
		equipeId: Equipe['id'];
		debut: Date;
		fin: Date;
	}) {
		const params = etablissementParamsInit({
			equipeId,
		});

		const { nouveaux, qpvs } = await elasticQueryEquipeEtablissement.eqEtabAggregate(
			{
				...params,
				creeApres: debut,
				creeAvant: fin,
			},
			[
				elastic.helpers.builder.filterAggregation(
					'nouveaux',
					elastic.helpers.builder.matchAllQuery()
				),
				elastic.helpers.builder.filterAggregation(
					'qpvs',
					elastic.helpers.builder.existsQuery('adresse.qpv_2024_id')
				),
			]
		);

		const { horsTerritoire } = await elasticQueryEquipeEtablissement.eqEtabAggregate(
			{
				...params,
				geo: 'hors',
				creeApres: debut,
				creeAvant: fin,
			},
			[
				elastic.helpers.builder.filterAggregation(
					'horsTerritoire',
					elastic.helpers.builder
						.nestedQuery()
						.path('equipes')
						.query(elastic.helpers.builder.termQuery('equipes.equipe_id', equipeId))
				),
			]
		);

		return {
			nouveaux: nouveaux.doc_count as number,
			qpvs: qpvs.doc_count as number,
			horsTerritoire: horsTerritoire.doc_count as number,
		};
	},

	async contactAdd({
		evenementId,
		equipeId,
		etablissementId,
		contactId,
		fonction,
		contactSourceTypeId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		contactId: Contact['id'];
		fonction: EquipeEtablissementContact['fonction'];
		contactSourceTypeId: EquipeEtablissementContact['contactSourceTypeId'];
	}) {
		await dbQueryEvenement.actionContactAdd({
			evenementId,
			contactId,
			typeId: 'modification',
			diff: {
				message: 'lien vers un établissement',
				changes: {
					after: {
						equipeId,
						etablissementId,
					},
				},
			},
		});

		await dbQueryEquipeEtablissement.eqEtabContactAdd({
			equipeId,
			etablissementId,
			contactId,
			fonction,
			contactSourceTypeId,
		});

		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: "ajout d'un contact",
				changes: {
					table: `eqEtabContact`,
					after: {
						equipeId,
						etablissementId,
						contactId,
					},
				},
			},
		});

		await dbQueryEvenement.actionEquipeEtablissementContactAdd({
			evenementId,
			equipeId,
			etablissementId,
			contactId,
			typeId: 'creation',
		});
	},

	async contactUpdate({
		evenementId,
		equipeId,
		etablissementId,
		contactId,
		fonction,
		partialContact,
		oldContact,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		contactId: Contact['id'];
		fonction: EquipeEtablissementContact['fonction'];
		partialContact: Partial<Contact>;
		oldContact: Contact;
	}) {
		await serviceContact.contactUpdate({
			evenementId,
			contactId,
			partialContact,
			oldContact,
		});

		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `mise à jour d'un contact`,
			},
		});

		if (!fonction) {
			return;
		}

		await dbQueryEquipeEtablissement.eqEtabContactUpdate({
			contactId,
			etablissementId,
			equipeId,
			fonction,
		});

		await dbQueryEvenement.actionEquipeEtablissementContactAdd({
			evenementId,
			equipeId,
			etablissementId,
			contactId,
			typeId: 'modification',
		});
	},

	async contactRemove({
		evenementId,
		equipeId,
		etablissementId,
		contactId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		contactId: Contact['id'];
	}) {
		const [deletedContact] = await dbQueryEquipeEtablissement.eqEtabContactRemove({
			equipeId,
			etablissementId,
			contactId,
		});

		if (!deletedContact) {
			return;
		}

		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `suppression d'un contact`,
				changes: {
					table: `eqEtabContact`,
					before: {
						equipeId,
						etablissementId,
						contactId,
					},
				},
			},
		});

		return deletedContact;
	},

	async contactImportBuild({ deveco }: { deveco: Deveco }) {
		const equipeId = deveco.equipeId;
		const compteId = deveco.compteId;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_etablissement_contact_import',
		});

		const evenementId = evenement.id;

		return async (ligne: ContactImport) => {
			let message = 'OK';

			const { siret: fuzzy, nom, prenom, telephone, email, dateDeNaissance, fonction } = ligne;

			try {
				if (!isSiret(fuzzy)) {
					throw new Error(`siret invalid ${fuzzy}`);
				}

				const etablissementId = fuzzy.replaceAll(/\s/g, '');

				const etablissement = await dbQueryEtablissement.etablissementGet({
					etablissementId,
				});

				if (!etablissement) {
					throw new Error(`Aucun établissement trouvé pour le SIRET ${etablissementId}`);
				}

				// TODO: attention à la mise à jour ElasticSearch ?

				const newContact = await serviceContact.contactAdd({
					evenementId,
					equipeId,
					contact: {
						equipeId,
						nom,
						prenom,
						telephone,
						email,
						naissanceDate: dateDeNaissance ? new Date(dateDeNaissance) : null,
					},
				});

				await serviceEquipeEtablissement.contactAdd({
					evenementId,
					equipeId,
					etablissementId,
					contactId: newContact.id,
					fonction,
					contactSourceTypeId: 'import',
				});

				return callbackSuccess(`${fuzzy}-${nom}-${prenom}: ${message}`);
			} catch (e: unknown) {
				message = e instanceof Error ? (message = e.message) : 'Erreur inconnue';
				return callbackError(`${fuzzy}-${nom}-${prenom}: ${message}`);
			}
		};
	},

	async echangeImportBuild({ deveco }: { deveco: Deveco }) {
		const equipeId = deveco.equipeId;
		const compteId = deveco.compteId;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_etablissement_echange_import',
		});

		const evenementId = evenement.id;

		return async (ligne: EchangeImport) => {
			let message = 'OK';

			const { siret: fuzzy, auteur, titre, type, date: dateRaw, contenu } = ligne;

			try {
				if (!isSiret(fuzzy)) {
					throw new Error(`siret invalid ${fuzzy}`);
				}

				const etablissementId = fuzzy.replaceAll(/\s/g, '');

				const etablissement = await dbQueryEtablissement.etablissementGet({
					etablissementId,
				});

				if (!etablissement) {
					throw new Error(`Aucun établissement trouvé pour le SIRET ${etablissementId}`);
				}

				const [deveco] = await dbQueryEquipe.devecoGetByEmail({ email: auteur });
				if (!deveco) {
					throw new Error(`Aucun compte trouvé pour l'email ${auteur}`);
				}

				if (deveco.equipeId !== equipeId) {
					throw new Error(`L'auteur ${auteur} n'appartient pas à l'équipe demandée (#${equipeId})`);
				}

				if (typeof dateRaw === 'string' && !dateRaw.match(/^\d\d\d\d\/\d\d\/\d\d$/)) {
					throw new Error(`Date avec un format invalide (doit être AAAA/MM/JJ) : ${dateRaw}`);
				}

				// on veut un "jour subjectif", pas une "date avec timezone", même si la colonne est
				// en "mode date" dans le schema, donc on force l'heure en milieu de journée pour éviter tout problème
				const date = new Date(dateRaw);
				if (!isDate(date)) {
					throw new Error(`Date invalide : ${dateRaw}`);
				}

				date.setHours(12);

				if (!echangeTypeIds.includes(type as EchangeTypeId)) {
					throw new Error(`Type invalide : ${type}`);
				}

				const echange = await serviceEchange.echangeAdd({
					evenementId,
					partialEchange: {
						equipeId,
						nom: titre,
						typeId: type as EchangeTypeId,
						date,
						description: contenu,
						devecoId: deveco.id,
					},
					demandeIds: [],
					brouillonId: null,
				});

				await serviceEquipeEtablissement.echangeAdd({
					evenementId,
					equipeId,
					etablissementId,
					echange,
				});

				return callbackSuccess(`${fuzzy}-${titre}-${dateRaw}: ${message}`);
			} catch (e: unknown) {
				message = e instanceof Error ? (message = e.message) : 'Erreur inconnue';
				return callbackError(`${fuzzy}-${titre}-${dateRaw}: ${message}`);
			}
		};
	},

	...serviceEquipeEtablissementSuivi,
};
