import * as dbQueryTerritoire from '../db/query/territoire';
import * as dbQueryEquipe from '../db/query/equipe';
import * as dbQueryEquipeLocal from '../db/query/equipe-local';
import * as dbQueryEquipeEtablissement from '../db/query/equipe-etablissement';
import { Equipe, Etiquette, Contour } from '../db/types';

export const serviceTerritoire = {
	async qpvGetMany({
		recherche,
		equipeId,
	}: {
		recherche?: string;
		equipeId?: Equipe['id'] | null;
	}) {
		return equipeId
			? await dbQueryEquipe.qpvGetManyByEquipeAndNomOrId({
					equipeId,
					recherche,
				})
			: await dbQueryTerritoire.qpvGetManyByNomOrId({
					recherche,
				});
	},

	async territoireGetManyByType({
		territoireTypeId,
		recherche,
		equipeId,
	}: {
		territoireTypeId: string;
		recherche?: string;
		equipeId?: Equipe['id'] | null;
	}) {
		if (territoireTypeId === 'commune') {
			return equipeId
				? dbQueryEquipe.communeGetManyByEquipeAndNomOrId({ equipeId, recherche })
				: dbQueryTerritoire.communeGetManyByNomOrId({ recherche });
		}

		if (territoireTypeId === 'epci') {
			return equipeId
				? dbQueryEquipe.epciGetManyByEquipeAndNomOrId({ equipeId, recherche })
				: dbQueryTerritoire.epciGetManyByNomOrId({ recherche });
		}

		if (territoireTypeId === 'petr') {
			return equipeId ? undefined : dbQueryTerritoire.petrGetManyByNomOrId({ recherche });
		}

		if (territoireTypeId === 'territoire_industrie') {
			return equipeId
				? dbQueryEquipe.territoireIndustrieGetManyByEquipeAndNomOrId({ equipeId, recherche })
				: dbQueryTerritoire.territoireIndustrieGetManyByNomOrId({ recherche });
		}

		if (territoireTypeId === 'metropole') {
			return equipeId ? undefined : dbQueryTerritoire.metropoleGetManyByNom({ recherche });
		}

		if (territoireTypeId === 'departement') {
			return equipeId
				? dbQueryEquipe.departementGetManyByEquipeAndNomOrId({ equipeId, recherche })
				: dbQueryTerritoire.departementGetManyByNomOrId({ recherche });
		}

		if (territoireTypeId === 'region') {
			return equipeId
				? dbQueryEquipe.regionGetManyByEquipeAndNomOrId({ equipeId, recherche })
				: dbQueryTerritoire.regionGetManyByNomOrId({ recherche });
		}
	},

	async qpvContourGetManyById({
		qpvs,
		equipeId,
		geo,
		nwse,
	}: Pick<
		dbQueryEquipeEtablissement.EtablissementsParams | dbQueryEquipeLocal.EquipeLocalParams,
		'qpvs' | 'equipeId' | 'geo' | 'nwse'
	>): Promise<Contour[] | null> {
		const tous = qpvs?.includes('tous');

		const contours =
			geo === 'france'
				? await dbQueryTerritoire.contourQpvGetMany({
						qpvIds: tous ? [] : qpvs,
						simplifyTolerance: tous ? 0.001 : null,
						nwse,
					})
				: await dbQueryEquipe.contourQpvGetManyByEquipe({
						equipeId,
						qpvIds: tous ? [] : qpvs,
						simplifyTolerance: null,
						nwse,
					});

		return contours ?? null;
	},

	async zonageContourGetManyById({
		equipeId,
		etiquetteNoms,
		nwse,
	}: {
		equipeId: Equipe['id'];
		etiquetteNoms: Etiquette['nom'][] | null;
		nwse?: number[];
	}): Promise<Contour[] | null> {
		const contours = await dbQueryTerritoire.contourZonageGetMany({
			equipeId,
			etiquetteNoms,
			nwse,
		});

		return contours ?? null;
	},
};
