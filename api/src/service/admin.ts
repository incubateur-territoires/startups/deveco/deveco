import { v4 as uuidv4 } from 'uuid';
import { addYears, isBefore } from 'date-fns';

import {
	ActionDiff,
	ActionTypeId,
	Compte,
	CompteTypeId,
	Equipe,
	EvenementTypeId,
} from '../db/types';
import { brevoCheck } from '../lib/email';
import * as dbQueryAdmin from '../db/query/admin';
import * as dbQueryEvenement from '../db/query/evenement';
import * as dbQueryEquipe from '../db/query/equipe';
import * as dbQueryEtablissementCreation from '../db/query/etablissement-creation';
import * as dbQueryContact from '../db/query/contact';
import { serviceBrevo } from '../service/brevo';

async function evenementActionCompteAdd({
	compteId,
	evenementTypeId,
	actionTypeId,
	actionDiffMessage,
}: {
	compteId: Compte['id'];
	evenementTypeId: EvenementTypeId;
	actionTypeId: ActionTypeId;
	actionDiffMessage: ActionDiff['message'];
}) {
	const [evenement] = await dbQueryEvenement.evenementAdd({
		compteId,
		typeId: evenementTypeId,
	});

	await dbQueryEvenement.actionCompteAdd({
		evenementId: evenement.id,
		typeId: actionTypeId,
		compteId,
		diff: {
			message: actionDiffMessage,
		},
	});
}

export const serviceAdmin = {
	async emailBienvenueSend(compte: Compte, motDePasse: string) {
		if (!brevoCheck()) {
			return;
		}

		try {
			await serviceBrevo.bienvenueEmail(compte, motDePasse);

			await dbQueryEquipe.devecoUpdate({
				compteId: compte.id,
				partialDeveco: {
					bienvenueEmail: true,
				},
			});
		} catch (emailError) {
			console.logError(emailError);
		}
	},

	async emailRelanceDixJoursSend(compte: Compte) {
		if (!brevoCheck()) {
			return;
		}

		try {
			await serviceBrevo.relanceDixJoursEmail(compte);
		} catch (emailError) {
			console.logError(emailError);
		}
	},

	async emailRelanceTrenteJoursSend(compte: Compte) {
		if (!brevoCheck()) {
			return;
		}

		try {
			await serviceBrevo.relanceTrenteJoursEmail(compte);
		} catch (emailError) {
			console.logError(emailError);
		}
	},

	async compteConnexionLien({ compteId, cle }: { compteId: Compte['id']; cle: Compte['cle'] }) {
		await dbQueryAdmin.compteUpdate({
			compteId,
			partialCompte: {
				cle,
			},
		});

		await evenementActionCompteAdd({
			compteId,
			evenementTypeId: 'connexion_lien',
			actionTypeId: 'modification',
			actionDiffMessage: 'ajoute la clé de connexion',
		});
	},

	async compteConnexion({ compteId }: { compteId: Compte['id'] }) {
		await dbQueryAdmin.compteUpdate({
			compteId,
			partialCompte: {
				cle: null,
				connexionAt: new Date(),
			},
		});

		await evenementActionCompteAdd({
			compteId,
			evenementTypeId: 'connexion',
			actionTypeId: 'modification',
			actionDiffMessage: 'efface la clé de connexion',
		});
	},

	async compteUpdate({
		compteId,
		partialCompte,
	}: {
		compteId: Compte['id'];
		partialCompte: Partial<Compte>;
	}) {
		const [compte] = await dbQueryAdmin.compteUpdate({
			compteId,
			partialCompte,
		});

		if (typeof partialCompte.actif === 'undefined') {
			await evenementActionCompteAdd({
				compteId,
				evenementTypeId: 'compte_modification',
				actionTypeId: 'modification',
				actionDiffMessage: 'met à jour les informations',
			});
		} else {
			await evenementActionCompteAdd({
				compteId,
				evenementTypeId: 'compte_modification',
				actionTypeId: partialCompte.actif ? 'activation' : 'desactivation',
				actionDiffMessage: partialCompte.actif ? 'activation du compte' : 'désactivation du compte',
			});
		}

		return compte;
	},

	async compteMotDePasseUpdate({
		compteId,
		motDePasse,
	}: {
		compteId: Compte['id'];
		motDePasse: string;
	}) {
		await dbQueryAdmin.motDePasseUpsert({
			compteId,
			motDePasse,
		});

		await evenementActionCompteAdd({
			compteId,
			evenementTypeId: 'compte_mot_de_passe_modification',
			actionTypeId: 'modification',
			actionDiffMessage: 'modifie le mot de passe',
		});
	},

	async equipeAdd({
		compteId,
		nom,
		equipeTypeId,
		territoireTypeId,
		territoireIds,
		siret,
	}: {
		compteId: Compte['id'];
		nom: Equipe['nom'];
		equipeTypeId: Equipe['typeId'];
		territoireTypeId: Equipe['territoireTypeId'];
		territoireIds: string[];
		siret: string | null;
	}) {
		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'equipe_creation',
		});

		const groupement = territoireIds.length > 1;

		const [equipe] = await dbQueryEquipe.equipeAdd({
			nom,
			equipeTypeId: equipeTypeId ? equipeTypeId : 'autre',
			territoireTypeId,
			groupement,
			siret,
		});

		const equipeId = equipe.id;

		await dbQueryEquipe.equipeTerritoireAdd({
			equipeId,
			territoireTypeId,
			territoireIds,
		});

		await dbQueryEvenement.actionEquipeAdd({
			evenementId: evenement.id,
			typeId: 'creation',
			equipeId,
			diff: {
				message: `création d'une équipe`,
			},
		});

		return equipe;
	},

	async compteAdd({
		compteId,
		identifiant,
		email,
		nom,
		prenom,
		typeId,
	}: {
		compteId: Compte['id'];
		identifiant: Compte['identifiant'];
		email: Compte['email'];
		nom: Compte['nom'];
		prenom: Compte['prenom'];
		typeId: CompteTypeId;
	}) {
		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'compte_creation',
		});

		const [compte] = await dbQueryAdmin.compteAdd({
			compte: {
				identifiant,
				email,
				nom,
				prenom,
				typeId,
				actif: true,
				cle: uuidv4(),
			},
		});

		await dbQueryEvenement.actionCompteAdd({
			evenementId: evenement.id,
			typeId: 'creation',
			compteId,
			diff: {
				message: `création d'un compte`,
				changes: {
					after: { compteId: compte.id },
				},
			},
		});

		return compte;
	},

	async anonymisation() {
		const now = new Date();

		// ANONYMISATION DES COMPTES
		const ilYaUnAn = addYears(now, -1);

		// récupération des comptes inactifs
		const comptesInactifs = await dbQueryAdmin.compteInactifNonAnonymeGetMany();

		// vérification de leur date de désactivation
		const compteIds = comptesInactifs
			.filter(({ desactivationAt }) => {
				return desactivationAt && isBefore(desactivationAt, ilYaUnAn);
			})
			.map(({ id }) => id);

		// anonymisation des données personnelles (nom, prenom, email, identifiant)
		if (compteIds.length) {
			await dbQueryAdmin.anonymisationCompteUpdateMany({ compteIds });
		}

		// ANONYMISATION DES CRÉATEURS TRANSFORMÉS
		const ilYaDeuxAn = addYears(now, -2);

		// récupération des fiches de créateurs transformés
		const transformations = await dbQueryEtablissementCreation.etabCreaTransformationGetMany();

		// vérification de leur date de transformation et vérification de leur non-implication dans d'autres créations/contacts/locaux
		const transformationContactIds = transformations
			.filter(({ date }) => isBefore(date, ilYaDeuxAn))
			.flatMap(({ crea, etablissementId, etabCreaId }) => {
				return crea.creas
					.map(({ ctct }) => ctct)
					.filter((createur) => {
						const createurAutresEtablissements =
							createur.creaCs.filter((c) => c.etabCreaId !== etabCreaId).length > 0;
						const contactAutresEtablissements =
							createur.etabCs.filter((c) => c.etablissementId !== etablissementId).length > 0;
						const contactsLocaux = createur.localCs.length > 0;

						return !(createurAutresEtablissements || contactAutresEtablissements || contactsLocaux);
					});
			})
			.map(({ id }) => id);

		// anonymisation des données personnelles (nom, prenom, email, telephone, telephone_2, naissance_date)
		if (transformationContactIds.length) {
			await dbQueryContact.anonymisationContactUpdateMany({ contactIds: transformationContactIds });
		}

		// ANONYMISATION DES CRÉATEURS SANS INTERACTION

		// récupération des fiches de créateurs transformés
		const etabCreas = await dbQueryEtablissementCreation.etabCreaWithLastActionGetMany();

		// vérification de leur date de transformation et vérification de leur non-implication dans d'autres créations/contacts/locaux
		const creationContactIds = etabCreas
			.filter(({ actions }) => isBefore(actions[0]?.createdAt, ilYaDeuxAn))
			.flatMap(({ id, creas }) => {
				return creas
					.map(({ ctct }) => ctct)
					.filter((createur) => {
						const createurAutresEtablissements =
							createur.creaCs.filter((c) => c.etabCreaId !== id).length > 0;
						const contactAutresEtablissements = createur.etabCs.length > 0;
						const contactsLocaux = createur.localCs.length > 0;

						return !(createurAutresEtablissements || contactAutresEtablissements || contactsLocaux);
					});
			})
			.map(({ id }) => id);

		// anonymisation des données personnelles (nom, prenom, email, telephone, telephone_2, naissance_date)
		if (creationContactIds.length) {
			await dbQueryContact.anonymisationContactUpdateMany({ contactIds: creationContactIds });
		}

		return;
	},

	async metabaseRefresh() {
		await dbQueryAdmin.metabaseRefresh();
	},
};
