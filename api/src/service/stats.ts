import { subDays, setHours } from 'date-fns';

import { Equipe, Deveco } from '../db/types';
import * as dbQueryEchange from '../db/query/echange';
import * as dbQueryDemande from '../db/query/demande';
import * as dbQueryRappel from '../db/query/rappel';
import * as dbQueryEquipeLocal from '../db/query/equipe-local';
import * as dbQueryEquipeEtablissement from '../db/query/equipe-etablissement';
import * as dbQueryEtablissementCreation from '../db/query/etablissement-creation';
import * as dbQueryEvenement from '../db/query/evenement';
import { serviceEquipeEtablissement } from '../service/equipe-etablissement';
import { Stats } from '../service/email-templates/stats';
import { echangeFormatSimple } from '../controller/_format/equipe';

export const serviceStats = {
	// TODO préciser les types de retour
	async etabCreaStatsGet({
		equipeId,
		debut,
		fin,
	}: {
		equipeId: Equipe['id'];
		debut: Date;
		fin: Date;
	}) {
		const etabCreaRappels = await dbQueryEtablissementCreation.etabCreaRappelWithRelationGetMany({
			equipeId,
		});

		const etabCreaDemandes = await dbQueryEtablissementCreation.etabCreaDemandeWithRelationGetMany({
			equipeId,
		});

		const etabCreaWithQpv = await dbQueryEtablissementCreation.etabCreaWithQpvGetMany({
			equipeId,
			debut,
			fin,
		});

		const etabCreaTransformation =
			await dbQueryEtablissementCreation.etabCreaTransformationGetManyFromDate({
				equipeId,
				debut,
				fin,
			});

		const echangeWithRelations = await dbQueryEchange.echangeWithRelationsGetManyFromDate({
			tableName: 'etabCrea',
			equipeId,
			debut,
			fin,
		});

		const actionEtablissementCreationAffichages =
			await dbQueryEvenement.actionEtablissementCreationAffichageCount({
				equipeId,
				debut,
				fin,
			});

		const actionEtiquetteEtablissementCreations =
			await dbQueryEvenement.actionEtiquetteEtablissementCreationCount({
				equipeId,
				debut,
				fin,
			});

		return {
			etabCreaRappels,
			etabCreaDemandes,
			etabCreaWithQpv,
			etabCreaTransformation,
			echangeWithRelations,
			actionEtablissementCreationAffichages,
			actionEtiquetteEtablissementCreations,
		};
	},

	// TODO préciser les types de retour
	async eqEtabStatsGet({
		equipeId,
		debut,
		fin,
	}: {
		equipeId: Equipe['id'];
		debut: Date;
		fin: Date;
	}) {
		const eqEtabRappels = await dbQueryEquipeEtablissement.eqEtabRappelGetMany({
			equipeId,
		});

		const eqEtabDemandes = await dbQueryEquipeEtablissement.eqEtabDemandeGetMany({
			equipeId,
		});

		const eqEtabCounts = await serviceEquipeEtablissement.eqEtabCountGetByCreationDate({
			equipeId,
			debut,
			fin,
		});

		const echangeWithRelations = await dbQueryEchange.echangeWithRelationsGetManyFromDate({
			tableName: 'eqEtab',
			equipeId,
			debut,
			fin,
		});

		const evenementEquipeEtablissementAffichages =
			await dbQueryEvenement.actionEquipeEtablissementAffichageGetManyFromDate({
				equipeId,
				debut,
				fin,
			});

		const eqEtabWithContacts = await dbQueryEquipeEtablissement.etabIdGetManyByContactCreationDate({
			equipeId,
			debut,
			fin,
		});

		const evenementEtiquetteEtablissements =
			await dbQueryEvenement.actionEtiquetteEtablissementGetManyFromDate({
				equipeId,
				debut,
				fin,
			});

		return {
			eqEtabRappels,
			eqEtabDemandes,
			eqEtabCounts,
			echangeWithRelations,
			evenementEquipeEtablissementAffichages,
			eqEtabWithContacts,
			evenementEtiquetteEtablissements,
		};
	},

	// TODO préciser les types de retour
	async eqLocStatsGet({
		equipeId,
		debut,
		fin,
	}: {
		equipeId: Equipe['id'];
		debut: Date;
		fin: Date;
	}) {
		const echangeWithRelations = await dbQueryEchange.echangeWithRelationsGetManyFromDate({
			tableName: 'eqLoc',
			equipeId,
			debut,
			fin,
		});

		const localDemandes = await dbQueryEquipeLocal.eqLocDemandeGetMany({
			equipeId,
		});

		const localRappels = await dbQueryEquipeLocal.eqLocRappelGetMany({
			equipeId,
		});

		const localContactsProprio = await dbQueryEquipeLocal.eqLocContactProprioGetManyByCreationDate({
			equipeId,
			debut,
			fin,
		});

		const eqLocsWithAffichages = await dbQueryEquipeLocal.localIdGetManyByAffichageDate({
			equipeId,
			debut,
			fin,
		});

		const [{ occupes, vacants }] =
			await dbQueryEquipeLocal.eqLocIdCountOccupantsVacantsBetweenDates({
				equipeId,
				debut,
				fin,
			});

		const actionEtiquetteLocal = await dbQueryEvenement.actionEtiquetteLocalGetManyFromDate({
			equipeId,
			debut,
			fin,
		});

		return {
			echangeWithRelations,
			localDemandes,
			localRappels,
			localContactsProprio,
			eqLocsWithAffichages,
			eqLocsOccupes: occupes,
			eqLocsVacants: vacants,
			actionEtiquetteLocal,
		};
	},

	async equipeStatsGet({
		equipeId,
		devecoId,
		debut,
		fin,
	}: {
		equipeId: Equipe['id'];
		devecoId: Deveco['id'];
		debut: Date;
		fin: Date;
	}) {
		const eqEtabs = await serviceEquipeEtablissement.ouvertFermeGetMany({
			equipeId,
			debut,
			fin,
		});

		const echangesTotal = await dbQueryEchange.echangeCountFromDate({
			equipeId,
			debut,
		});

		const demandesTotal = await dbQueryDemande.demandeCountFromDate({
			equipeId,
			debut,
		});

		const rappelsTotal = await dbQueryRappel.rappelOuvertCountFromDate({
			equipeId,
			debut,
		});

		const [mesRappels] = await dbQueryRappel.rappelOuvertCountByDevecoFromDate({
			equipeId,
			devecoId,
			debut,
		});

		const rappelsAVenir = Number(mesRappels?.aVenir ?? 0);
		const rappelsEnRetard = Number(mesRappels?.enRetard ?? 0);

		return {
			eqEtabs,
			echanges: echangesTotal[0].total ?? 0,
			demandes: demandesTotal[0].total ?? 0,
			rappels: rappelsTotal[0].total ?? 0,
			rappelsAVenir,
			rappelsEnRetard,
		};
	},

	async equipeStatsBuild(equipe: Equipe): Promise<Stats | null> {
		const today = new Date();
		const twoWeeksAgo = setHours(subDays(today, 14), 12);

		const equipeId = equipe.id;

		const affichagesTotal = await dbQueryEvenement.actionEquipeEtablissementAffichageCountByEquipe({
			equipeId,
		});
		const affichagesPeriod = await dbQueryEvenement.actionEquipeEtablissementAffichageCountByEquipe(
			{
				equipeId,
				debut: twoWeeksAgo,
			}
		);

		const affichages = {
			total: affichagesTotal[0]?.count,
			period: affichagesPeriod[0]?.count,
			periodList: [],
		};

		// //

		const rappelsTotal = await dbQueryEvenement.actionRappelCountActionTypeId({
			equipeId,
			typeIds: ['creation'],
		});
		const rappelsPeriod = await dbQueryEvenement.actionRappelCountActionTypeId({
			equipeId,
			typeIds: ['creation'],
			debut: twoWeeksAgo,
		});

		const rappels = {
			total: rappelsTotal[0]?.count,
			period: rappelsPeriod[0]?.count,
			periodList: [],
		};

		// //

		const etablissementsPortefeuilleTotal =
			await dbQueryEvenement.actionEquipeEtablissementAjouteAuPortefeuilleCountByEquipe({
				equipeId,
			});
		const etablissementsPortefeuillePeriod =
			await dbQueryEvenement.actionEquipeEtablissementAjouteAuPortefeuilleCountByEquipe({
				equipeId,
				debut: twoWeeksAgo,
			});

		const etablissementsPortefeuille = {
			total: etablissementsPortefeuilleTotal[0]?.count,
			period: etablissementsPortefeuillePeriod[0]?.count,
			periodList: [],
		};

		const etabCreasTotal =
			await dbQueryEvenement.actionEtablissementCreationCountByEquipeAndActionTypeId({
				equipeId,
				typeIds: ['creation'],
			});
		const etabCreasPeriod =
			await dbQueryEvenement.actionEtablissementCreationCountByEquipeAndActionTypeId({
				equipeId,
				typeIds: ['creation'],
				debut: twoWeeksAgo,
			});

		const etabCreas = {
			total: etabCreasTotal[0]?.count,
			period: etabCreasPeriod[0]?.count,
			periodList: [],
		};

		// //

		const contactsTotal =
			await dbQueryEvenement.actionEquipeEtablissementContactCountByEquipeAndActionTypeId({
				equipeId,
				typeIds: ['creation'],
			});
		const contactsPeriod =
			await dbQueryEvenement.actionEquipeEtablissementContactCountByEquipeAndActionTypeId({
				equipeId,
				typeIds: ['creation'],
				debut: twoWeeksAgo,
			});

		const contacts = {
			total: contactsTotal[0]?.count,
			period: contactsPeriod[0]?.count,
			periodList: [],
		};

		// //

		const echangesTotal = await dbQueryEvenement.actionEchangeCountByEquipeAndActionTypeId({
			equipeId,
			typeIds: ['creation'],
		});

		const actionEchangeIdsPeriod =
			await dbQueryEvenement.actionEchangeGetManyByEquipeAndActionTypeId({
				equipeId,
				typeIds: ['creation'],
				debut: twoWeeksAgo,
			});

		const echangeIds = actionEchangeIdsPeriod.map(({ echangeId }) => echangeId);

		const echangesWithEntite = echangeIds.length
			? await dbQueryEchange.echangeWithRelationsGetManyById({
					echangeIds: actionEchangeIdsPeriod.map(({ echangeId }) => echangeId),
				})
			: [];

		const echanges = {
			total: echangesTotal[0]?.count,
			period: actionEchangeIdsPeriod.length,
			periodList: echangesWithEntite.map(echangeFormatSimple),
		};

		// //

		const qualificationEtablissementTotal =
			await dbQueryEvenement.actionEtiquetteForEntiteCountByEquipeAndActionTypeId({
				equipeId,
				tableName: 'eqEtab',
				typeIds: ['modification'],
			});
		const qualificationEtablissementCreationTotal =
			await dbQueryEvenement.actionEtiquetteForEntiteCountByEquipeAndActionTypeId({
				equipeId,
				tableName: 'etabCrea',
				typeIds: ['modification'],
			});

		const qualificationEtablissementPeriod =
			await dbQueryEvenement.actionEtiquetteForEntiteCountByEquipeAndActionTypeId({
				equipeId,
				tableName: 'eqEtab',
				typeIds: ['modification'],
				debut: twoWeeksAgo,
			});
		const qualificationEtablissementCreationPeriod =
			await dbQueryEvenement.actionEtiquetteForEntiteCountByEquipeAndActionTypeId({
				equipeId,
				tableName: 'etabCrea',
				typeIds: ['modification'],
				debut: twoWeeksAgo,
			});

		const qualifications = {
			total:
				qualificationEtablissementTotal[0].count + qualificationEtablissementCreationTotal[0].count,
			period:
				qualificationEtablissementPeriod[0].count +
				qualificationEtablissementCreationPeriod[0].count,
			// TODO: discuter de remettre ça en place dans le mail ou non
			periodList: [],
		};

		const totalActivites =
			affichages.period +
			contacts.period +
			rappels.period +
			echanges.period +
			qualifications.period +
			etablissementsPortefeuille.period +
			etabCreas.period;

		return totalActivites > 0
			? {
					consultations: affichages,
					echanges,
					contacts,
					rappels,
					qualifications,
					etablissements: etablissementsPortefeuille,
					createurs: etabCreas,
					equipe: equipe.nom,
				}
			: null;
	},
};
