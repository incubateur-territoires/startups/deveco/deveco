import { devecoIdentiteFormat } from '../../lib/utils';

export interface Compte {
	nom?: string | null;
	prenom?: string | null;
	email?: string | null;
}

export interface LoginUrl {
	appUrl: string;
	cle?: string;
	redirectUrl?: string;
}

export const salutation = ({
	nom,
	prenom,
	email,
}: Pick<Compte, 'nom' | 'prenom' | 'email'>): string =>
	`<p>
		Bonjour ${devecoIdentiteFormat({ nom, prenom, email })},
	</p>`;

export const salutationAnonyme = (): string =>
	`<p>
		Bonjour,
	</p>`;

export const remerciement = () =>
	`<p>
		Merci d’utiliser Deveco, la solution numérique pour les développeurs économiques.
	</p>`;

export const documentationUrl = '/documentation';
export const helpLink = (appUrl: string): string =>
	`<p>
L'équipe Deveco met à votre disposition <a rel="nofollow" href="${appUrl}${documentationUrl}/"><span>un guide d'utilisation</span></a>.
	</p>`;

export const devecoContactEmail = process.env.SMTP_FROM;

export const emailLink = () =>
	`<p>
		Pour nous écrire&nbsp;: <a href="mailto:${devecoContactEmail}">${devecoContactEmail}</a>.
	</p>`;

export const signature = () =>
	`<p>
		L'équipe Deveco.
	</p>`;
