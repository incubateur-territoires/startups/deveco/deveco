import {
	emailLink,
	signature,
	remerciement,
	helpLink,
	salutation,
	salutationAnonyme,
	Compte,
} from './_common';

import { plural } from '../../lib/utils';
import { formatJSDateToHumanFriendly } from '../../lib/dateUtils';

export interface EntiteData {
	nom: string;
	date: Date;
	contenu: string;
	etablissementId?: string;
	etabCreaId?: number;
	localId?: string;
}

export interface StatsData {
	period: number;
	periodList: EntiteData[];
	total: number;
}

export interface Stats {
	consultations: StatsData;
	echanges: StatsData;
	contacts: StatsData;
	rappels: StatsData;
	qualifications: StatsData;
	etablissements: StatsData;
	createurs: StatsData;
	equipe: string;
}

const formatEntiteData =
	(appUrl: string) =>
	({ nom, contenu, date, etablissementId, etabCreaId, localId }: EntiteData) => {
		let path;

		if (etablissementId) {
			path = `etablissements/${etablissementId}`;
		} else if (etabCreaId) {
			path = `createurs/${etabCreaId}`;
		} else if (localId) {
			path = `lcoaux/${localId}`;
		}
		const link = `<a href="${appUrl}/${path}">${nom}</a>`;

		const displayDate = date ? `${formatJSDateToHumanFriendly(date)}&nbsp;: ` : '';

		contenu = contenu ? ` - ${contenu}` : '';

		return `${displayDate}${link}${contenu}`;
	};

const showStats = (appUrl: string) => {
	const formatter = formatEntiteData(appUrl);

	return (stats: Stats): string =>
		`
			<p>Nous sommes heureux de vous partager quelque éléments de votre tableau de bord.</p>

			<p>Voici les éléments clés des deux dernières semaines dans le Deveco de votre collectivité (${
				stats.equipe
			}).</p>

			<h2>Établissements</h2>
			<p><strong>${stats.etablissements.period ? `+${stats.etablissements.period}` : 'Aucun'} ${plural(
				stats.etablissements.period,
				'établissement'
			)}</strong> dans le portefeuille sur les deux dernières semaines.
			<br>
			${stats.etablissements.total} depuis le début.
			</p>

			<h2>Créateurs d'entreprise</h2>
			<p><strong>${stats.createurs.period ? `+${stats.createurs.period}` : 'Aucun'} ${plural(
				stats.createurs.period,
				'créateur'
			)}</strong> dans le portefeuille sur les deux dernières semaines.
			<br>
			${stats.createurs.total} depuis le début.
			</p>

			<h2>Consultations</h2>
			<p><strong>${stats.consultations.period ? `+${stats.consultations.period}` : 'Aucun'} ${plural(
				stats.consultations.period,
				'action'
			)} de consultation</strong> sur les deux dernières semaines.
			<br>
			${stats.consultations.total} depuis le début.
			</p>

			<h2>Échanges</h2>
			<p><strong>${stats.echanges.period ? `+${stats.echanges.period}` : 'Aucun'} ${plural(
				stats.echanges.period,
				'échange'
			)}</strong> sur les deux dernières semaines.</p>
			${
				stats.echanges.periodList?.length
					? `<p>Établissements et créateurs concernés&nbsp;:<p>
			<ul>
				${stats.echanges.periodList
					.map((r) => formatter(r))
					.map((formatted) => `<li>${formatted}</li>`)
					.join('')}
			</ul>`
					: ''
			}

			<h2>Qualifications</h2>
			<p><strong>${stats.qualifications.period ? `+${stats.qualifications.period}` : 'Aucune'} ${plural(
				stats.qualifications.period,
				'action'
			)} de qualification</strong> sur les deux dernières semaines.
			<br>
			${stats.qualifications.total} depuis le début.
			</p>
			${
				stats.qualifications.periodList?.length
					? `<p>Établissements et créateurs concernés&nbsp;:<p>
			<ul>
				${stats.qualifications.periodList
					.map((r) => formatter(r))
					.map((formatted) => `<li>${formatted}</li>`)
					.join('')}
			</ul>`
					: ''
			}

			<h2>Contacts</h2>
			<p><strong>${stats.contacts.period ? `+${stats.contacts.period}` : 'Aucun'} ${plural(
				stats.contacts.period,
				'contact'
			)} ${plural(stats.contacts.period, 'créé')}</strong> sur les deux dernières semaines.
			<br>
			${stats.contacts.total} depuis le début.
			</p>

			<h2>Rappels</h2>
			<p><strong>${stats.rappels.period ? `+${stats.rappels.period}` : 'Aucun'} ${plural(
				stats.rappels.period,
				'rappel'
			)} ${plural(stats.rappels.period, 'créé')}</strong> sur les deux dernières semaines.
			<br>
			${stats.rappels.total} depuis le début.
			</p>
	`;
};

const suivisButton = ({ appUrl, page }: { appUrl: string; page: string }) =>
	`
		<p>Pour retrouver tous vos suivis sur Deveco, veuillez cliquer sur le lien ci-dessous&nbsp;:</p>
		<p style="padding-left: 20%">
		<a
			rel="nofollow"
			href="${appUrl}${page ?? '/connexion'}"
			style="
				background-color: #000091;
				font-size: 14px;
				font-family: Marianne;
				font-weight: bold;
				text-decoration: none;
				padding: 8px 10px;
				color: #ffffff;
				display: inline-block;
			"
		>
		<span>Mes suivis</span>
		</a>
		</p>
	`;

const rappels = (appUrl: string) => {
	const formatter = formatEntiteData(appUrl);

	return (rappelsData: EntiteData[]): string =>
		`<p>
			Voici les actions Deveco à réaliser dans deux jours&nbsp;:<br>
			<ul>
				${rappelsData
					.map((r) => formatter(r))
					.map((formatted) => `<li>${formatted}</li>`)
					.join('')}
			</ul>
		</p>
	`;
};

export const serviceEmailTemplateStats = {
	rappelsReminder({
		appUrl,
		page,
		rappelsData,
	}: {
		appUrl: string;
		page: string;
		rappelsData: EntiteData[];
	}) {
		return `
			${salutationAnonyme()}
			${rappels(appUrl)(rappelsData)}
			${suivisButton({ appUrl, page })}
			${remerciement()}
			${helpLink(appUrl)}
			${emailLink()}
			${signature()}
		`;
	},

	statsRecap({
		appUrl,
		compte,
		stats,
	}: {
		appUrl: string;
		compte: Pick<Compte, 'nom' | 'prenom' | 'email'>;
		stats: Stats;
	}) {
		return `
			${salutation(compte)}
			${showStats(appUrl)(stats)}
			${remerciement()}
			${helpLink(appUrl)}
			${emailLink()}
			${signature()}
		`;
	},
};
