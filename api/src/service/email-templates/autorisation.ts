import { AyantDroit } from '../../db/types';

interface AutorisationUrl {
	appUrl: string;
	cle: string;
}

interface Requerant {
	nom: string;
	prenom: string;
	equipe: string;
}

const demandeConfirmationAutorisation = ({
	appUrl,
	requerant,
	ayantDroit,
	cle,
}: {
	appUrl: string;
	requerant: Requerant;
	ayantDroit: AyantDroit;
	cle: string;
}) =>
	`
<p>Bonjour ${ayantDroit.prenom} ${ayantDroit.nom},</p>
<p>L’organisme ${requerant.equipe}, représenté par ${requerant.prenom} ${
		requerant.nom
	} a sollicité un accès à la plateforme numérique Deveco, déployée par l’Incubateur des territoires de l’Agence nationale de la cohésion des territoires. Deveco est une solution numérique d’appui au développement économique territorial à destination des collectivités territoriales et des services déconcentrés de l'Etat.</p>
<p>Cette demande d’accès pourra être validée par Deveco sous réserve d’une autorisation de la part de votre collectivité territoriale. Cette autorisation est accordée sous votre responsabilité et en aucune circonstance la plateforme Deveco ne peut être tenue responsable des éventuels préjudices qui peuvent en résulter.</p>
<p>Pour autoriser cette demande d’accès, nous vous invitons à valider en cliquant sur le lien suivant et à confirmer ensuite votre accord&nbsp;: <a href="${autorisationLink(
		{ appUrl, cle }
	)}">${autorisationLink({ appUrl, cle })}</a></p>
<p>Dès validation de votre autorisation, l’Équipe Deveco pourra ouvrir l’accès à tout personnel de l’organisme demandeur.</p>
<p>Pour toute question, n’hésitez pas à nous contacter directement à l’adresse suivante&nbsp;: <a href="mailto:contact@deveco.incubateur.anct.gouv.fr">contact@deveco.incubateur.anct.gouv.fr</a></p>
<p>Bien cordialement.<br />L’Équipe Deveco</p>
  `;

const autorisationLink = ({ appUrl, cle }: AutorisationUrl) => `${appUrl}/autorisation/${cle}`;

export const serviceEmailTemplatesAutorisation = {
	confirmationAyantDroit({
		appUrl,
		requerant,
		ayantDroit,
		cle,
	}: {
		appUrl: string;
		requerant: Requerant;
		ayantDroit: AyantDroit;
		cle: string;
	}) {
		return `${demandeConfirmationAutorisation({ appUrl, requerant, ayantDroit, cle })}`;
	},

	equipeDemandePrete({
		appUrl,
		requerant,
		admin,
	}: {
		appUrl: string;
		requerant: Requerant;
		admin: {
			nom: string | null;
			prenom: string | null;
		};
	}) {
		const lienDemandes = `${appUrl}/admin/equipes/demandes`;
		return `
<p>Bonjour ${admin.prenom} ${admin.nom},</p>
<p>La demande de création d'équipe ${requerant.equipe}, formulée par ${requerant.prenom} ${
			requerant.nom
		} a été validée par l'intégralité des ayant-droits.</p>
<p>Pour passer à la création de l'équipe, rendez-vous sur ${lienDemandes}.</p>
<p>Bien cordialement.<br />L’Équipe Deveco</p>
  `;
	},
};
