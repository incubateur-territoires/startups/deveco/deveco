import he from 'he';

import {
	Compte,
	LoginUrl,
	emailLink,
	helpLink,
	remerciement,
	salutation,
	signature,
	devecoContactEmail,
} from './_common';

import { devecoIdentiteFormat } from '../../lib/utils';

const loginLink = ({ appUrl, cle, redirectUrl }: LoginUrl) =>
	`${appUrl}${cle ? `/auth/jwt/${cle}` : '/connexion'}${redirectUrl ? `?url=${redirectUrl}` : ''}`;

const loginButton = (url: LoginUrl) =>
	`
    <p>Pour accéder à Deveco, veuillez cliquer sur le lien ci-dessous&nbsp;:</p>
    <p style="padding-left: 20%">
      <a
      rel="nofollow"
      href="${loginLink(url)}"
      style="
          background-color: #000091;
          font-size: 14px;
          font-family: Marianne;
          font-weight: bold;
          text-decoration: none;
          padding: 8px 10px;
          color: #ffffff;
          display: inline-block;
        "
      >
      <span>Accédez à Deveco</span>
      </a>
    </p>
    <p>
      Si le lien ne fonctionne pas, veuillez copier l'url ci-dessous dans votre navigateur&nbsp;:
      <code>${loginLink(url)}</code>
    </p>
  `;

export const serviceEmailTemplatesCompte = {
	login({ url, compte: { nom, prenom, email } }: { url: LoginUrl; compte: Compte }) {
		return `
      ${salutation({ nom, prenom, email })}
      ${loginButton(url)}
      ${remerciement()}
      ${helpLink(url.appUrl)}
      ${emailLink()}
      ${signature()}
    `;
	},

	connexionAdmin({ compte: { email }, date }: { compte: Compte; date: string }) {
		return `
      ${salutation({ email })}
			<p>Une connexion à votre compte superadmin Deveco vient d'avoir lieu à cette date&nbsp;: ${date}.</p>
			<p>Si vous êtes à l'origine de cette demande, vous pouvez ignorer cet email.</p>
			<p>Sinon, contactez l'équipe Deveco dans les plus brefs délais&nbsp;: <a href="mailto:${devecoContactEmail}">${devecoContactEmail}</a>.
</p>
    `;
	},

	connexionDeveco({ compte: { email }, date }: { compte: Compte; date: string }) {
		return `
      ${salutation({ email })}
			<p>Une connexion par mot de passe à votre compte Deveco vient d'avoir lieu à cette date&nbsp;: ${date}.</p>
			<p>Si vous êtes à l'origine de cette demande, vous pouvez ignorer cet email.</p>
			<p>Sinon, contactez l'équipe Deveco dans les plus brefs délais&nbsp;: <a href="mailto:${devecoContactEmail}">${devecoContactEmail}</a>.
</p>
    `;
	},

	importTermine({ url, compte: { nom, prenom, email } }: { url: LoginUrl; compte: Compte }) {
		return `
		${salutation({ nom, prenom, email })}
		<p>Les données que vous avez communiquées ont été importées. Vous pouvez les retrouver sur Deveco en cliquant ici&nbsp;:</p>
		${loginButton(url)}
		<p>Un compte-rendu des erreurs est disponible en pièce jointe.</p>
		${remerciement()}
		${helpLink(url.appUrl)}
		${emailLink()}
		${signature()}
		`;
	},

	partage({
		url,
		compte,
		expediteur,
		descriptif,
		commentaire,
	}: {
		url: LoginUrl;
		compte: Compte;
		expediteur: Compte;
		descriptif: string;
		commentaire: string;
	}) {
		const blocCommentaire = commentaire ? `<quote>${he.encode(commentaire)}</quote>` : '';

		return `
		${salutation(compte)}
		<p>Votre collègue ${devecoIdentiteFormat(
			expediteur
		)} souhaite vous partager la fiche de ${descriptif}&nbsp;:</p>
		${blocCommentaire}
		<p>Vous pouvez la consulter sur Deveco en cliquant ici&nbsp;:</p>
		${loginButton(url)}
		${remerciement()}
		${helpLink(url.appUrl)}
		${emailLink()}
		${signature()}
		`;
	},
};
