import {
	Demande,
	DemandeWithRelations,
	EchangeDemandeWithEchange,
	EchangeWithDevecoAndDemandes,
	EquipeEtablissementContactWithContact,
	EquipeEtablissementDemandeWithRelations,
	EquipeLocalContactWithContact,
	EquipeLocalDemandeWithRelations,
	EtablissementCreation,
	EtablissementCreationDemandeWithRelations,
	EtablissementEffectifMoyenAnnuel,
	Etablissement,
	Local,
	LocalAdresseWithCommune,
	NotUndefined,
	QueryResult,
} from '../db/types';
import { effectifValeurToDescription } from '../lib/etablissement-effectif';
import * as dbQueryEquipeEtablissement from '../db/query/equipe-etablissement';
import * as dbQueryEtablissementCreation from '../db/query/etablissement-creation';
import * as dbQueryEquipeLocal from '../db/query/equipe-local';
import * as dbQueryContact from '../db/query/contact';
import { occupeToString } from '../controller/locaux';
import {
	subventionsFormat,
	etablissementFermetureDateBuild,
	etablissementNomBuild,
	etablissementAdresseBuild,
} from '../controller/_format/etablissements';
import { EtablissementForAdresseBuild } from '../controller/types';
import { localAdresseFormat, localSurfaceTotaleToString } from '../controller/_format/local';
import { etabCreaIdBuild, etabCreaNomBuild } from '../controller/_format/etablissement-creation';
import {
	Criteria,
	WsAnnuaireContactRow,
	WsDemandeRow,
	WsEchangeRow,
	WsEntiteAndContactRow,
	WsLocalOccupantRow,
	WsStructureRow,
} from './excel';
import { etiquettesByTypeBuild } from '../controller/_format/equipe';
import { dateToString, intlFormatFr } from '../lib/dateUtils';

export type EntiteType = EtablissementCreation | Etablissement | Local;

export interface EntiteNom {
	entiteNom: string;
}

export type DemandeEntiteInfo<T extends EntiteType> = EntiteNom & {
	entiteInfo: EntiteInfos<T>;
};

export interface AdresseInfos {
	numero: string | null;
	voieNom: string | null;
	codePostal: string | null;
	commune: string | null;
}

export interface LocalAdresseInfos {
	numero: string | null;
	voieNom: string | null;
	communeId: string | null;
	commune: string | null;
}

export type EntiteInfos<T extends EntiteType> = T extends Etablissement
	? AdresseInfos & {
			siret: Etablissement['siret'];
			codeNaf: string | null;
			libelleNaf: string | null;
		}
	: T extends EtablissementCreation
		? {
				id: string;
			}
		: LocalAdresseInfos & {
				localId: Local['id'];
				invariant: Local['invariant'];
			};

export type PersoInfos<T extends EntiteType> = {
	zonesGeographiques: string;
	motsCles: string;
	commentaires: string;
} & (T extends Local ? {} : { activitesReelles: string });

type EtablissementFormatted = AdresseInfos & {
	siren: string;
	siege: string | null;
	sigle: string | null;
	codePostal: string | null;
	commune: string | null;
	effectifs: string | null;
	dateEffectifs: string | null;
	libelleCategorieNaf: string | null;
	dateCreation: string | null;
	dateFermeture: string | null;
	etatAdministratif: string | null;
	formeJuridique: string | null;
	micro: string | null;
	tailleEntreprise: string | null;
	ess: string | null;
	subventions: string | null;
	procedure: string | null;
	zonages: string | null;
	local: string | null;
	diffusible: string | null;
};

interface EtablissementCreationFormatted {
	id: string;
	nom: string;
	enseigne: string | null;
	description: string | null;
	communes: string | null;
	projetType: string | null;
	contactOrigine: string | null;
	contactDate: string | null;
	secteurActivite: string | null;
	sourceFinancement: string | null;
	categorieJuridiqueEnvisagee: string | null;
	contratAccompagnement: string | null;
	rechercheLocal: string | null;
	dateCreation: string | null;
	creationAbandonnee: string | null;
}

type LocalFormatted = LocalAdresseInfos & {
	localId: string;
	section: string;
	parcelle: string;
	occupe: string;
	niveau: string;
	surfaceTotale: string;
	surfaceVente: number;
	surfaceReserve: number;
	surfaceExterieureNonCouverte: number;
	surfaceStationnementCouvert: number;
	surfaceStationnementNonCouvert: number;
	categorie: string;
	nature: string;
	actif: string;
	// perso
	commentaires: string;
	loyer: string;
	bailType: string;
	vacanceType: string;
	vacanceMotif: string;
	remembre: string;
	fonds: string;
	vente: string;
};

export type EtablissementOrEtablissementCreationOrLocalFormatted<T extends EntiteType> =
	T extends Etablissement
		? EtablissementFormatted
		: T extends EtablissementCreation
			? EtablissementCreationFormatted
			: LocalFormatted;

function effectifDateFormat(effectifMoyenAnnuel: EtablissementEffectifMoyenAnnuel) {
	return effectifMoyenAnnuel.date.toISOString().slice(0, 4);
}

export function subventionToLigne({
	montant,
	objet,
	annee,
	type,
}: {
	montant: string;
	objet: string;
	annee: string;
	type: string;
}) {
	return `${type} - ${annee} - ${objet} - ${montant}`;
}

export function etablissementInfosFormat(
	etab: NotUndefined<
		QueryResult<
			typeof dbQueryEquipeEtablissement.etablissementWithEqEtabWithRelationsForGeojsonGetMany
		>[0]
	>
): EtablissementFormatted {
	const qpv2024 = etab.qpv2024?.qpv;
	const qpv2015 = etab.qpv2015?.qpv;
	const qpv2024Nom = qpv2024 ? `${qpv2024.nom} (QPV)` : '';
	const qpv2015Nom = qpv2015 ? `${qpv2015.nom} (QPV 2015)` : '';

	const zrr =
		etab.commune?.zrrTypeId === 'C'
			? 'ZRR'
			: etab.commune?.zrrTypeId === 'P'
				? 'ZRR (partielle)'
				: '';
	const acv = etab.acv ? `ACV` : '';
	const pvd = etab.pvd ? `PVD` : '';
	const va = etab.va ? `VA` : '';
	const afr =
		etab.afr?.integral === true ? 'AFR' : etab.afr?.integral === false ? 'AFR (partielle)' : '';
	const territoireIndustrie = etab.ti?.ti?.nom
		? `${etab.ti?.ti?.nom} (Territoire d'industrie)`
		: '';

	const zonages = [qpv2024Nom, qpv2015Nom, zrr, acv, pvd, va, territoireIndustrie, afr]
		.filter(Boolean)
		.join(' ; ');

	return {
		siren: etab.siren,
		siege: etab.etablissementSiege ? 'oui' : 'non',
		sigle: etab.ul?.sigleUniteLegale ?? '',
		...adresseExcelFormat(etab),
		effectifs:
			etab.effectifMoyenAnnuel?.effectif !== undefined
				? effectifValeurToDescription(etab.effectifMoyenAnnuel.effectif ?? 0)
				: 'Non renseigné',
		dateEffectifs: etab.effectifMoyenAnnuel ? effectifDateFormat(etab.effectifMoyenAnnuel) : '',
		dateCreation: etab.dateCreationEtablissement?.toISOString()?.slice(0, 10) ?? '',
		dateFermeture: etablissementFermetureDateBuild(etab)?.toISOString()?.slice(0, 10) ?? '',
		formeJuridique: etab.ul.catJur?.parent?.nom ?? 'Indéfini',
		micro: etab.ul.micro ? 'oui' : 'non',
		tailleEntreprise: etab.ul.categorieEntreprise,
		ess: etab.ul.ess ? 'oui' : 'non',
		libelleCategorieNaf: etab.nafType?.parent?.nom ?? '',
		etatAdministratif: etab.etatAdministratifEtablissement === 'A' ? 'Actif' : 'Inactif',
		subventions: subventionsFormat(etab.subventions).map(subventionToLigne).join('\n\n'),
		procedure: etab.ul.procedures.length > 0 ? 'oui' : 'non',
		zonages,
		local: etab.occups.map(({ localId }) => localId).join(' ; '),
		diffusible: etab.statutDiffusionEtablissement === 'O' ? 'oui' : 'non',
	};
}

export function etabCreaContactFormatMany<T extends EntiteType>({
	etabCrea,
	demandeEntiteInfo,
}: {
	etabCrea: NotUndefined<ReturnType<typeof etabCreaForExcelFormatMany>[0]>['etabCrea'];
	demandeEntiteInfo: DemandeEntiteInfo<T>;
}): WsEntiteAndContactRow<T>[] {
	return etabCrea.creas.map(
		({
			fonction,
			ctct: contact,
			diplomeNiveau,
			professionSituation,
		}): WsEntiteAndContactRow<T> => ({
			entiteNom: demandeEntiteInfo.entiteNom,
			...demandeEntiteInfo.entiteInfo,
			nom: contact.nom,
			prenom: contact.prenom,
			email: contact.email,
			telephone: contact.telephone,
			telephone2: contact.telephone2,
			dateDeNaissance: contact.naissanceDate ? dateToString(contact.naissanceDate) : null,
			demarchageAccepte: contact.demarchageAccepte ? 'OUI' : 'NON',
			fonction,
			niveauDiplome: diplomeNiveau?.nom ?? null,
			situationProfessionnelle: professionSituation?.nom ?? null,
		})
	);
}

export function etabCreaForExcelFormatMany(
	etabCreas: QueryResult<typeof dbQueryEtablissementCreation.etabCreaWithRelationsGetManyById>,
	demandes: EtablissementCreationDemandeWithRelations[]
) {
	const etabCreaDemandesByIds: Map<
		EtablissementCreation['id'],
		EtablissementCreationDemandeWithRelations[]
	> = demandes.reduce(
		(
			result: Map<EtablissementCreation['id'], EtablissementCreationDemandeWithRelations[]>,
			etabCreaDemande: EtablissementCreationDemandeWithRelations
		) => {
			const etabCreaDemandes = result.get(etabCreaDemande.etabCreaId) || [];

			etabCreaDemandes.push(etabCreaDemande);

			result.set(etabCreaDemande.etabCreaId, etabCreaDemandes);

			return result;
		},
		new Map()
	);

	return etabCreas.map((etabCrea) => {
		const etabCreaEchanges = etabCrea.echanges;

		const demandeIdToEchanges: Map<Demande['id'], EchangeDemandeWithEchange[]> =
			etabCreaEchanges.reduce(
				(result: Map<Demande['id'], EchangeDemandeWithEchange[]>, etabCreaEchange) => {
					const { demandes: demandeEchanges, ...resteEchange } = etabCreaEchange.echange;

					demandeEchanges.forEach(({ demandeId }) => {
						const echanges = result.get(demandeId) || [];

						const echangeDemandeWithEchange = {
							demandeId,
							echangeId: resteEchange.id,
							echange: resteEchange,
						};
						echanges.push(echangeDemandeWithEchange);

						result.set(demandeId, echanges);
					});

					return result;
				},
				new Map()
			);

		const demandesEtablissementCreation = etabCreaDemandesByIds.get(etabCrea.id) ?? [];

		const etabCreaId = etabCreaIdBuild(etabCrea.id);
		const etabCreaNom = etabCreaNomBuild(etabCrea);

		return {
			id: etabCreaId,
			nom: etabCreaNom,
			etabCrea,
			demandes: demandesEtablissementCreation.map((etabCreaDemande) => ({
				...etabCreaDemande,
				demande: {
					...etabCreaDemande.demande,
					echanges: demandeIdToEchanges.get(etabCreaDemande.demandeId) ?? [],
				},
			})),
		};
	});
}

export function etabCreaFormatRow({
	id,
	nom,
	etabCrea,
}: NotUndefined<ReturnType<typeof etabCreaForExcelFormatMany>[0]>): EtablissementCreationFormatted {
	return {
		id,
		nom,
		enseigne: etabCrea.enseigne,
		communes: etabCrea.communes?.map(({ commune }) => commune.nom).join(', ') ?? '',
		description: etabCrea.description,
		projetType: etabCrea.projetType?.nom ?? '',
		contactOrigine: etabCrea.contactOrigine?.nom ?? '',
		contactDate: etabCrea.contactDate?.toISOString()?.slice(0, 10) ?? '',
		secteurActivite: etabCrea.secteurActivite?.nom ?? '',
		sourceFinancement: etabCrea.sourceFinancement?.nom ?? '',
		categorieJuridiqueEnvisagee: etabCrea.categorieJuridiqueEnvisagee,
		contratAccompagnement: etabCrea.contratAccompagnement ? 'oui' : 'non',
		rechercheLocal: etabCrea.rechercheLocal ? 'oui' : 'non',
		dateCreation: etabCrea.creationDate.toISOString().slice(0, 10),
		creationAbandonnee: etabCrea.creationAbandonnee ? 'oui' : 'non',
	};
}

export function eqLocInfosFormat(
	local: NotUndefined<QueryResult<typeof dbQueryEquipeLocal.localForExcelGetMany>[0]>
): LocalFormatted {
	const contribution = local.contributions[0];
	const adresse = local.adresse;

	return {
		localId: local.id,
		section: local.section ?? '',
		parcelle: local.parcelle ?? '',
		occupe: occupeToString(contribution?.occupe ?? null) ?? '',
		niveau: local.niveau ?? '',
		surfaceTotale: localSurfaceTotaleToString(local),
		surfaceVente: local.surfaceVente ?? 0,
		surfaceReserve: local.surfaceReserve ?? 0,
		surfaceExterieureNonCouverte: local.surfaceExterieureNonCouverte ?? 0,
		surfaceStationnementCouvert: local.surfaceStationnementCouvert ?? 0,
		surfaceStationnementNonCouvert: local.surfaceStationnementNonCouvert ?? 0,
		categorie: local.categorieType?.nom ?? '',
		nature: local.natureType?.nom ?? '',
		actif: local.actif ? 'oui' : 'non',
		numero: adresse?.numero ?? '',
		voieNom: adresse?.voieNom ?? '',
		communeId: adresse.commune.id,
		commune: adresse.commune.nom,
		commentaires: contribution?.description ?? '',
		loyer: contribution?.loyerEuros?.toString() ?? '',
		bailType: contribution?.bailTypeId ?? '',
		vacanceType: contribution?.vacanceTypeId ?? '',
		vacanceMotif: contribution?.vacanceMotifTypeId ?? '',
		remembre: contribution?.remembre ? 'oui' : 'non',
		fonds: contribution?.fondsEuros?.toString() ?? '',
		vente: contribution?.venteEuros?.toString() ?? '',
	};
}

export function etabWithDemandesAndEchangesFormat(
	etabs: QueryResult<typeof dbQueryEquipeEtablissement.etablissementWithEqEtabForExcelGetMany>,
	demandes: EquipeEtablissementDemandeWithRelations[]
) {
	const etablissementIdToDemandes: Map<
		Etablissement['siret'],
		EquipeEtablissementDemandeWithRelations[]
	> = demandes.reduce(
		(
			result: Map<Etablissement['siret'], EquipeEtablissementDemandeWithRelations[]>,
			eqEtabDemande: EquipeEtablissementDemandeWithRelations
		) => {
			const eqEtabDemandes = result.get(eqEtabDemande.etablissementId) || [];

			eqEtabDemandes.push(eqEtabDemande);

			result.set(eqEtabDemande.etablissementId, eqEtabDemandes);

			return result;
		},
		new Map()
	);

	return etabs.map((etab) => {
		const eqEtabEchanges = etab.echanges;

		const demandeIdToEchanges: Map<Demande['id'], EchangeDemandeWithEchange[]> =
			eqEtabEchanges?.reduce(
				(result: Map<Demande['id'], EchangeDemandeWithEchange[]>, eqEtabEchange) => {
					const { demandes: demandeEchanges, ...resteEchange } = eqEtabEchange.echange;

					demandeEchanges.forEach(({ demandeId }) => {
						const echanges = result.get(demandeId) || [];

						echanges.push({
							demandeId,
							echangeId: resteEchange.id,
							echange: resteEchange,
						});

						result.set(demandeId, echanges);
					});

					return result;
				},
				new Map()
			) ?? [];

		const demandesEtablissement = etablissementIdToDemandes.get(etab.siret) ?? [];

		return {
			...etab,
			echanges: eqEtabEchanges,
			demandes: demandesEtablissement.map((eqEtabDemande) => ({
				...eqEtabDemande,
				demande: {
					...eqEtabDemande.demande,
					echanges: demandeIdToEchanges.get(eqEtabDemande.demandeId) ?? [],
				},
			})),
		};
	});
}

export function eqLocWithDemandesAndEchangesFormat(
	locals: QueryResult<typeof dbQueryEquipeLocal.localForExcelGetMany>,
	demandes: EquipeLocalDemandeWithRelations[]
) {
	const localIdToDemandes: Map<Local['id'], EquipeLocalDemandeWithRelations[]> = demandes.reduce(
		(
			result: Map<Local['id'], EquipeLocalDemandeWithRelations[]>,
			eqLocDemande: EquipeLocalDemandeWithRelations
		) => {
			const eqLocDemandes = result.get(eqLocDemande.localId) || [];

			eqLocDemandes.push(eqLocDemande);

			result.set(eqLocDemande.localId, eqLocDemandes);

			return result;
		},
		new Map()
	);

	return locals.map((local) => {
		const eqLocEchanges = local.echanges;

		const demandeIdToEchanges: Map<Demande['id'], EchangeDemandeWithEchange[]> =
			eqLocEchanges.reduce(
				(result: Map<Demande['id'], EchangeDemandeWithEchange[]>, eqLocEchange) => {
					const { demandes: demandeEchanges, ...resteEchange } = eqLocEchange.echange;

					demandeEchanges.forEach(({ demandeId }) => {
						const echanges = result.get(demandeId) || [];

						const echangeDemandeWithEchange = {
							demandeId,
							echangeId: resteEchange.id,
							echange: resteEchange,
						};
						echanges.push(echangeDemandeWithEchange);

						result.set(demandeId, echanges);
					});

					return result;
				},
				new Map()
			);

		const demandesLocal = localIdToDemandes.get(local.id) ?? [];

		return {
			...local,
			echanges: eqLocEchanges,
			demandes: demandesLocal.map((eqLocDemande) => ({
				...eqLocDemande,
				demande: {
					...eqLocDemande.demande,
					echanges: demandeIdToEchanges.get(eqLocDemande.demandeId) ?? [],
				},
			})),
		};
	});
}

function eqEtabContactFormatMany<T extends EntiteType>({
	eqEtabContacts,
	demandeEntiteInfo,
}: {
	eqEtabContacts: EquipeEtablissementContactWithContact[];
	demandeEntiteInfo: DemandeEntiteInfo<T>;
}): WsEntiteAndContactRow<T>[] {
	return eqEtabContacts.map(
		({
			fonction,
			ctct: contact,
		}: EquipeEtablissementContactWithContact): WsEntiteAndContactRow<T> => ({
			entiteNom: demandeEntiteInfo.entiteNom,
			...demandeEntiteInfo.entiteInfo,
			nom: contact.nom,
			prenom: contact.prenom,
			email: contact.email,
			telephone: contact.telephone,
			telephone2: contact.telephone2,
			dateDeNaissance: contact.naissanceDate ? dateToString(contact.naissanceDate) : null,
			demarchageAccepte: contact.demarchageAccepte ? 'OUI' : 'NON',
			fonction,
			niveauDiplome: null,
			situationProfessionnelle: null,
		})
	);
}

function eqLocContactFormatMany<T extends EntiteType>({
	eqLocContacts,
	demandeEntiteInfo,
}: {
	eqLocContacts: EquipeLocalContactWithContact[];
	demandeEntiteInfo: DemandeEntiteInfo<T>;
}): WsEntiteAndContactRow<T>[] {
	return eqLocContacts.map(
		({ fonction, ctct: contact }: EquipeLocalContactWithContact): WsEntiteAndContactRow<T> => ({
			entiteNom: demandeEntiteInfo.entiteNom,
			...demandeEntiteInfo.entiteInfo,
			fonction,
			nom: contact.nom,
			prenom: contact.prenom,
			email: contact.email,
			telephone: contact.telephone,
			telephone2: contact.telephone2,
			dateDeNaissance: contact.naissanceDate ? dateToString(contact.naissanceDate) : null,
			demarchageAccepte: contact.demarchageAccepte ? 'OUI' : 'NON',
			niveauDiplome: null,
			situationProfessionnelle: null,
		})
	);
}

function eqLocOccupantFormatMany({
	entiteNom,
	entiteInfo,
	occupants,
}: {
	entiteNom: string;
	entiteInfo: EntiteInfos<Local>;
	occupants: NotUndefined<
		QueryResult<typeof dbQueryEquipeLocal.localForExcelGetMany>[0]
	>['occupants'];
}): WsLocalOccupantRow[] {
	return occupants.map(
		({ occupant }): WsLocalOccupantRow => ({
			entiteNom,
			...entiteInfo,
			siret: occupant.siret,
			occupantNom: etablissementNomBuild(occupant),
		})
	);
}

function echangesFormatMany<T extends EntiteType>({
	echanges,
	demandeEntiteInfo,
}: {
	echanges: EchangeWithDevecoAndDemandes[];
	demandeEntiteInfo: DemandeEntiteInfo<T>;
}): WsEchangeRow<T>[] {
	return echanges.map((echange) => {
		const echangeRow: WsEchangeRow<T> = {
			entiteNom: demandeEntiteInfo.entiteNom,
			...demandeEntiteInfo.entiteInfo,
			type: echange.typeId,
			titre: echange.nom ?? '',
			date: echange.date,
			contenu: echange.description ?? '',
			deveco: echange.deveco.compte.email,
		};

		return echangeRow;
	});
}

function demandesFormatMany<T extends EntiteType>({
	demandes,
	demandeEntiteInfo,
}: {
	demandes: DemandeWithRelations[];
	demandeEntiteInfo: DemandeEntiteInfo<T>;
}): WsDemandeRow<T>[] {
	return demandes.map((demande) => {
		const relevantEchanges = demande.echanges;
		const summaryEchanges = relevantEchanges
			.map(
				(echangeDemande) => `${echangeDemande.echange.nom} : ${echangeDemande.echange.description}`
			)
			.join('\n\n')
			.trim();

		const demandeRow: WsDemandeRow<T> = {
			entiteNom: demandeEntiteInfo.entiteNom,
			...demandeEntiteInfo.entiteInfo,
			typeDemande: demande.typeId,
			titre: demande.nom,
			description: demande.description,
			echanges: summaryEchanges,
			state: demande.clotureMotif !== null ? 'Clôturé' : 'Ouvert',
			motifCloture: demande.clotureMotif ?? '',
			clotureDate: demande.clotureDate ? intlFormatFr(demande.clotureDate) : '',
		};

		return demandeRow;
	});
}

type EtabForExcel = NotUndefined<
	QueryResult<typeof dbQueryEquipeEtablissement.etablissementWithEqEtabForExcelGetMany>[0]
>;

export function adresseExcelFormat(etab: EtablissementForAdresseBuild) {
	const adresse = etablissementAdresseBuild(etab);

	return {
		numero: adresse?.numero || '',
		voieNom:
			[adresse?.voieNom, adresse?.complementAdresse, adresse?.distributionSpeciale]
				.filter(Boolean)
				.join(' ') || '',
		codePostal: adresse?.cedex ? adresse.cedex : adresse?.codePostal || '',
		commune: adresse.etrangerCommune
			? [adresse.etrangerCommune, adresse.etrangerPays].filter(Boolean).join(' ')
			: (adresse?.cedexNom ? adresse.cedexNom : adresse?.commune?.nom) || '',
	};
}

export function etabToRowFormat({
	params,
	etab,
}: {
	params: Criteria<Etablissement>;
	etab: EtabForExcel;
}): {
	row: WsStructureRow<Etablissement>;
	demandes: WsDemandeRow<Etablissement>[];
	echanges: WsEchangeRow<Etablissement>[];
	contacts: WsEntiteAndContactRow<Etablissement>[];
} {
	const etablissementNom = etablissementNomBuild(etab);

	const entiteInfo: EntiteInfos<Etablissement> = {
		siret: etab.siret,
		...adresseExcelFormat(etab),
		codeNaf: etab.nafType?.id ?? '',
		libelleNaf: etab.nafType?.nom ?? '',
	};

	const commonRow = {
		entiteNom: etablissementNom,
	};

	const etiquettes = etab.etiquettes ?? [];
	const { activites, localisations, motsCles } = etiquettesByTypeBuild(
		etiquettes.map((ete) => ete.etiquette)
	);

	const entiteRow: PersoInfos<Etablissement> = {
		activitesReelles: activites.map((e) => e.nom).join(', '),
		zonesGeographiques: localisations.map((e) => e.nom).join(', '),
		motsCles: motsCles.map((e) => e.nom).join(', '),
		commentaires: etab.contributions[0]?.description ?? '',
	};

	const echanges: WsEchangeRow<Etablissement>[] = [];

	const demandes: WsDemandeRow<Etablissement>[] = [];

	const demandeEntiteInfo: DemandeEntiteInfo<Etablissement> = {
		entiteNom: etablissementNom,
		entiteInfo,
	};

	const demandesForFilter = (etab.demandes ?? [])
		.map((d) => d.demande)
		// on garde seulement les demandes des types qui ont été filtrés, et pas toutes les demandes de l'établissement
		.filter(
			(d) =>
				!params.demandeTypeIds ||
				!params.demandeTypeIds.length ||
				params.demandeTypeIds.includes(d.typeId)
		);

	const formattedDemandes = demandesFormatMany({
		demandes: demandesForFilter,
		demandeEntiteInfo,
	});

	demandes.push(...formattedDemandes);

	const formattedEchanges = echangesFormatMany({
		echanges: (etab.echanges ?? []).map(({ echange }) => echange),
		demandeEntiteInfo,
	});

	echanges.push(...formattedEchanges);

	const etablissementInfos = etablissementInfosFormat(etab);

	const row: WsStructureRow<Etablissement> = {
		...commonRow,
		...entiteRow,
		...entiteInfo,
		...etablissementInfos,
	};

	const contacts: WsEntiteAndContactRow<Etablissement>[] = [];

	const formattedContacts = eqEtabContactFormatMany({
		eqEtabContacts: etab.contacts ?? [],
		demandeEntiteInfo,
	});

	contacts.push(...formattedContacts);

	return { row, contacts, echanges, demandes };
}

export function etabCreaToRowFormat({
	params,
	etabCrea: etabCreaFormatted,
}: {
	params: Criteria<EtablissementCreation>;
	etabCrea: NotUndefined<ReturnType<typeof etabCreaForExcelFormatMany>[0]>;
}): {
	row: WsStructureRow<EtablissementCreation>;
	echanges: WsEchangeRow<EtablissementCreation>[];
	demandes: WsDemandeRow<EtablissementCreation>[];
	createurs: WsEntiteAndContactRow<EtablissementCreation>[];
} {
	const { etabCrea, id, nom } = etabCreaFormatted;

	const entiteInfo: EntiteInfos<EtablissementCreation> = {
		id,
	};

	const demandeEntiteInfo: DemandeEntiteInfo<EtablissementCreation> = {
		entiteNom: nom,
		entiteInfo,
	};

	const commonRow = {
		entiteNom: nom,
	};

	const echanges: WsEchangeRow<EtablissementCreation>[] = [];

	const demandes: WsDemandeRow<EtablissementCreation>[] = [];

	const createurs: WsEntiteAndContactRow<EtablissementCreation>[] = [];

	const etiquettes = etabCrea.etiquettes;
	const { activites, localisations, motsCles } = etiquettesByTypeBuild(
		etiquettes.map((ete) => ete.etiquette)
	);

	const entiteRow: PersoInfos<EtablissementCreation> = {
		activitesReelles: activites.map((e) => e.nom).join(', '),
		zonesGeographiques: localisations.map((e) => e.nom).join(', '),
		motsCles: motsCles.map((e) => e.nom).join(', '),
		commentaires: etabCrea.description ?? '',
	};

	const demandesForFilter = etabCrea.demandes
		.map((d) => d.demande)
		// on garde seulement les demandes des types qui ont été filtrés, et pas toutes les demandes de l'établissement
		.filter(
			(d) =>
				!params.demandeTypeIds ||
				!params.demandeTypeIds.length ||
				params.demandeTypeIds.includes(d.typeId)
		);

	const augmentedDemandes = demandesFormatMany({
		demandes: demandesForFilter,
		demandeEntiteInfo,
	});

	demandes.push(...augmentedDemandes);

	const formattedEchanges = echangesFormatMany({
		echanges: etabCrea.echanges.map(({ echange }) => echange),
		demandeEntiteInfo,
	});

	echanges.push(...formattedEchanges);

	const formattedContacts = etabCreaContactFormatMany({
		etabCrea,
		demandeEntiteInfo,
	});

	createurs.push(...formattedContacts);

	const data = etabCreaFormatRow(etabCreaFormatted);

	const row: WsStructureRow<EtablissementCreation> = {
		...commonRow,
		...entiteRow,
		...entiteInfo,
		...data,
	};

	return {
		row,
		echanges,
		demandes,
		createurs,
	};
}

export function eqLocToRowFormat({
	params,
	eqLoc: local,
}: {
	params: Criteria<Local>;
	eqLoc: NotUndefined<QueryResult<typeof dbQueryEquipeLocal.localForExcelGetMany>[0]>;
}): {
	row: WsStructureRow<Local>;
	demandes: WsDemandeRow<Local>[];
	echanges: WsEchangeRow<Local>[];
	contacts: WsEntiteAndContactRow<Local>[];
	occupants: WsLocalOccupantRow[];
} {
	const contribution = local.contributions[0];

	const localNom = contribution?.nom ?? '-';

	const adresse = localAdresseFormat(local);

	const entiteInfo: EntiteInfos<Local> = {
		localId: local.id,
		invariant: local.invariant,
		numero: adresse?.numero ?? '',
		voieNom: adresse?.voieNom ?? '',
		communeId: adresse?.commune?.id ?? '',
		commune: adresse?.commune?.nom ?? '',
	};

	const commonRow = {
		entiteNom: localNom,
	};

	const etiquettes = local.etiquettes;
	const { localisations, motsCles } = etiquettesByTypeBuild(etiquettes.map((etl) => etl.etiquette));

	const entiteRow: PersoInfos<Local> = {
		zonesGeographiques: localisations.map((e) => e.nom).join(', '),
		motsCles: motsCles.map((e) => e.nom).join(', '),
		commentaires: contribution?.description ?? '',
	};

	const echanges: WsEchangeRow<Local>[] = [];

	const demandes: WsDemandeRow<Local>[] = [];

	const demandeEntiteInfo: DemandeEntiteInfo<Local> = {
		entiteNom: localNom,
		entiteInfo,
	};

	const demandesForFilter = local.demandes
		.map((d) => d.demande)
		// on garde seulement les demandes des types qui ont été filtrés, et pas toutes les demandes de l'établissement
		.filter(
			(d) =>
				!params.demandeTypeIds ||
				!params.demandeTypeIds.length ||
				params.demandeTypeIds.includes(d.typeId)
		);

	const formattedDemandes = demandesFormatMany({
		demandes: demandesForFilter,
		demandeEntiteInfo,
	});

	demandes.push(...formattedDemandes);

	const formattedEchanges = echangesFormatMany({
		echanges: local.echanges.map(({ echange }) => echange),
		demandeEntiteInfo,
	});

	echanges.push(...formattedEchanges);

	const localInfos = eqLocInfosFormat(local);

	const row: WsStructureRow<Local> = {
		...commonRow,
		...entiteRow,
		...entiteInfo,
		...localInfos,
	};

	const contacts: WsEntiteAndContactRow<Local>[] = [];

	const formattedContacts = eqLocContactFormatMany({
		eqLocContacts: local.contacts,
		demandeEntiteInfo,
	});
	contacts.push(...formattedContacts);

	const occupants: WsLocalOccupantRow[] = eqLocOccupantFormatMany({
		entiteNom: localNom,
		entiteInfo,
		occupants: local.occupants,
	});

	return {
		row,
		contacts,
		echanges,
		demandes,
		occupants,
	};
}

function localAdresseCompleteToString(
	adresse: Pick<LocalAdresseWithCommune, 'numero' | 'voieNom' | 'commune'>
) {
	return [adresse.numero, adresse.voieNom, adresse.commune?.id, adresse.commune?.nom]
		.filter(Boolean)
		.join(' ');
}

export function contactForExcelFormat(
	contact: NotUndefined<QueryResult<typeof dbQueryContact.contactWithRelationGetManyById>[0]>
) {
	const eqEtabContacts = contact.etabCs.map(({ fonction, etab }) => {
		return `${fonction || 'Contact'} - ${etablissementNomBuild(etab)} - ${etab.siret}`;
	});

	const etabCreaCreateurs = contact.creaCs.map(
		({ eCreation }) => `Créateur - ${etabCreaNomBuild(eCreation)}`
	);

	const eqLocContacts = contact.localCs.map(
		({ fonction, local: { contributions }, local }) =>
			`${fonction || 'Contact'} : Invariant ${local.id} - ${contributions[0]?.nom ?? '-'} - ${
				localAdresseCompleteToString(local.adresse) ?? '?'
			}`
	);

	const contactFormated: WsAnnuaireContactRow = {
		email: contact.email,
		nom: contact.nom,
		prenom: contact.prenom,
		telephone: contact.telephone,
		telephone2: contact.telephone2,
		dateDeNaissance: contact.naissanceDate ? dateToString(contact.naissanceDate) : null,
		demarchageAccepte: contact.demarchageAccepte ? 'OUI' : 'NON',
		roles: [...eqEtabContacts, ...etabCreaCreateurs, ...eqLocContacts].join('\n'),
	};

	return contactFormated;
}
