import { addDays } from 'date-fns';

import { serviceGeocode } from '../service/geocode';
import { TacheFrequence } from '../db/types';
import * as dbQueryAdmin from '../db/query/admin';
import * as dbQueryEvenement from '../db/query/evenement';

export const serviceApiGeo = {
	async geocodeUpdate({
		frequence,
		jours,
		skipIndexation,
	}: {
		frequence: TacheFrequence;
		jours: number;
		skipIndexation?: boolean;
	}) {
		const geocodeUpdateTacheNom = `geocode-update-${frequence}`;
		console.logInfo(`[Cron] ${geocodeUpdateTacheNom} : début`);

		const compteGeo = await dbQueryAdmin.compteGetByNomAndType({ nom: 'adresse', typeId: 'api' });
		if (!compteGeo) {
			throw new Error(`Pas de compte pour l'API Adresse`);
		}

		await dbQueryEvenement.evenementAdd({
			compteId: compteGeo.id,
			typeId: `sirene_update_${frequence}`,
		});

		try {
			const today = new Date();
			const startDate = addDays(today, -1 - jours);

			await serviceGeocode.etablissementGeocodeUpsertMany({
				tacheNom: geocodeUpdateTacheNom,
				miseAJourAt: startDate,
				skipIndexation,
			});
		} catch (e: any) {
			console.logError(`Impossible de mettre à jour le géocodage, erreur : ${e.message}`);
		}

		console.logInfo('[Cron] Geocode - Stop');
	},

	async geocodeFix({ frequence }: { frequence: TacheFrequence }) {
		const geocodeUpdateTacheNom = `geocode-fix-${frequence}`;
		console.logInfo(`[geocodeFix] ${geocodeUpdateTacheNom} : début`);

		const compteGeo = await dbQueryAdmin.compteGetByNomAndType({ nom: 'adresse', typeId: 'api' });
		if (!compteGeo) {
			throw new Error(`Pas de compte pour l'API Adresse`);
		}

		await dbQueryEvenement.evenementAdd({
			compteId: compteGeo.id,
			typeId: `sirene_update_${frequence}`,
		});

		try {
			await serviceGeocode.etablissementGeocodeUpsertMany({
				tacheNom: geocodeUpdateTacheNom,
			});
		} catch (e: any) {
			console.logError(`Impossible de mettre à jour le géocodage, erreur : ${e.message}`);
		}

		console.logInfo('[geocodeFix] Geocode - Stop');
	},
};
