import { startOfDay, addDays } from 'date-fns';

import { smtpCheck, emailSend } from '../lib/email';
import { wait } from '../lib/utils';
import {
	Compte,
	Deveco,
	Equipe,
	Etiquette,
	Evenement,
	RappelWithEntite,
	Notifications,
	Zonage,
} from '../db/types';
import * as dbQueryEvenement from '../db/query/evenement';
import * as dbQueryEquipe from '../db/query/equipe';
import * as dbQueryRappel from '../db/query/rappel';
import * as dbQueryAdmin from '../db/query/admin';
import * as elasticQueryZonage from '../elastic/query/zonage';
import { serviceStats } from '../service/stats';
import { serviceEmailTemplateStats, EntiteData, Stats } from '../service/email-templates/stats';
import { rappelFormatSimple } from '../controller/_format/equipe';

export async function equipeEtiquetteUpsertMany({
	evenementId,
	equipeId,
	activites,
	localisations,
	motsCles,
}: {
	evenementId: Evenement['id'];
	equipeId: number;
	activites: string[];
	localisations: string[];
	motsCles: string[];
}) {
	// insère toutes les nouvelles étiquettes
	// ne retourne que celles qui sont insérées

	const newEtiquetteActivites = await dbQueryEquipe.etiquetteAddMany({
		equipeId,
		etiquetteTypeId: 'activite',
		noms: activites,
	});

	const newEtiquetteLocalisations = await dbQueryEquipe.etiquetteAddMany({
		equipeId,
		etiquetteTypeId: 'localisation',
		noms: localisations,
	});

	const newEtiquetteMotCles = await dbQueryEquipe.etiquetteAddMany({
		equipeId,
		etiquetteTypeId: 'mot_cle',
		noms: motsCles,
	});

	const newEtiquettes = [
		...newEtiquetteActivites,
		...newEtiquetteLocalisations,
		...newEtiquetteMotCles,
	];

	const newEtiquetteIds = newEtiquettes.map(({ id }) => id);

	// crée un événement par nouvelle étiquette

	await dbQueryEvenement.actionEtiquetteAddMany({
		evenementId,
		etiquetteIds: newEtiquetteIds,
		typeId: 'creation',
		diff: {
			message: `création des étiquettes`,
		},
	});

	// récupère toutes les étiquettes existantes pour les retourner

	const existingEtiquetteActivites = await dbQueryEquipe.etiquetteGetManyByTypeAndNoms({
		equipeId,
		etiquetteTypeId: 'activite',
		noms: activites,
	});

	const existingEtiquetteLocalisations = await dbQueryEquipe.etiquetteGetManyByTypeAndNoms({
		equipeId,
		etiquetteTypeId: 'localisation',
		noms: localisations,
	});

	const existingEtiquetteMotsCles = await dbQueryEquipe.etiquetteGetManyByTypeAndNoms({
		equipeId,
		etiquetteTypeId: 'mot_cle',
		noms: motsCles,
	});

	return [
		...existingEtiquetteActivites,
		...existingEtiquetteLocalisations,
		...existingEtiquetteMotsCles,
	];
}

export const serviceEquipe = {
	async activiteSend() {
		if (!smtpCheck()) {
			return;
		}

		const appUrl = process.env.APP_URL || '';

		const equipes = await dbQueryEquipe.equipeGetMany();

		// for each equipe
		for (const equipe of equipes) {
			// 1) compute stats
			const stats = await serviceStats.equipeStatsBuild(equipe);

			if (!stats) {
				continue;
			}

			// 2) get devecos
			const devecos = await dbQueryEquipe.devecoWithCompteColumnsGetManyByEquipe({
				equipeId: equipe.id,
			});

			// // 3) send mails
			const mailSender = sendStatsToDeveco(appUrl, stats);
			for (const deveco of devecos) {
				if (process.env.CAPTURE_TERRITORY_ACTIVITY) {
					console.logInfo('Captured reminders to', process.env.CAPTURE_TERRITORY_ACTIVITY);

					const compte = {
						nom: 'capture',
						prenom: 'capture',
						email: process.env.CAPTURE_TERRITORY_ACTIVITY,
					};

					mailSender(compte);

					const delay = 500;
					await wait(delay);
				} else {
					// Est-ce que le compte du deveco est désactivé ?
					if (!deveco.actif) {
						continue;
					}

					// Est-ce que le deveco a désactivé l'envoi du mail sur les activités ?
					if (deveco.activite === false) {
						continue;
					}

					console.logInfo(
						`[activiteSend] Envoi de l'activité de l'équipe au deveco : ${deveco.email}`
					);
					mailSender(deveco);
				}
			}
		}
	},

	async etiquetteUpdate({
		evenementId,
		etiquetteId,
		nouveauNom,
	}: {
		evenementId: Evenement['id'];
		etiquetteId: Etiquette['id'];
		nouveauNom: Etiquette['nom'];
	}) {
		const etiquette = await dbQueryEquipe.etiquetteGet({ etiquetteId });
		if (!etiquette) {
			return;
		}

		const etiquetteAvecNom = await dbQueryEquipe.etiquetteGetByTypeAndNom({
			nom: nouveauNom,
			equipeId: etiquette.equipeId,
			etiquetteTypeId: etiquette.typeId,
		});
		if (etiquetteAvecNom) {
			return;
		}

		const ancienNom = etiquette.nom;

		await dbQueryEquipe.etiquetteUpdateById({
			etiquetteId,
			nom: nouveauNom,
		});

		await dbQueryEvenement.actionEtiquetteAddMany({
			evenementId,
			etiquetteIds: [etiquetteId],
			typeId: 'modification',
			diff: {
				message: `mise à jour de l'étiquette : ${etiquetteId}.`,
				changes: {
					after: nouveauNom,
					before: ancienNom,
				},
			},
		});
	},

	async rappelRemindersSend() {
		if (!smtpCheck()) {
			return;
		}

		const [tache] = await dbQueryAdmin.tacheAdd({ tache: { nom: 'rappel-reminders' } });
		const tacheId = tache.id;

		const captured = process.env.CAPTURE_RAPPELS_REMINDERS;

		const appUrl = process.env.APP_URL || '';

		const today = new Date();
		const inTwoDays = startOfDay(addDays(today, 2));

		const rappels = await dbQueryRappel.rappelOuvertWithRelationsGetManyAtDate({
			debut: inTwoDays,
		});

		if (!rappels.length) {
			console.logInfo('aucun rappel à envoyer');

			return;
		}

		const equipesWithDevecosWithCompte = await dbQueryEquipe.equipeWithDevecosWithCompteGetMany();

		// tous les mails des dévécos pour chaque équipe
		const equipesEmails = equipesWithDevecosWithCompte.reduce(
			(equipesEmails: Map<Equipe['id'], Compte['email'][]>, equipeWithDevecosWithCompte) => {
				// on ne garde les emails que des comptes non désactivés
				// et qui n'ont pas refusé l'envoi des emails de rappels
				const equipeEmails = equipeWithDevecosWithCompte.devecos
					.filter(({ compte, notifications }) => compte.actif && notifications?.rappels !== false)
					.map((deveco) => deveco.compte.email);

				equipesEmails.set(equipeWithDevecosWithCompte.id, equipeEmails);

				return equipesEmails;
			},
			new Map()
		);

		const rappelsByCompteEmail = rappels.reduce(
			(rappelsGroupByCompteId, rappel) => {
				const email = rappel.affecteCompte?.email;

				if (email) {
					if (!rappelsGroupByCompteId[email]) {
						rappelsGroupByCompteId[email] = [];
					}

					rappelsGroupByCompteId[email].push(rappel);
				} else {
					// le rappel n'est assigné à personne
					// on envoie le mail à toute l'équipe
					const equipeEmails = equipesEmails.get(rappel.equipeId);

					if (equipeEmails) {
						for (const email of equipeEmails) {
							if (!rappelsGroupByCompteId[email]) {
								rappelsGroupByCompteId[email] = [];
							}
							rappelsGroupByCompteId[email].push(rappel);
						}
					}
				}

				return rappelsGroupByCompteId;
			},
			{} as Record<Compte['email'], RappelWithEntite[]>
		);

		for (const [email, rappels] of Object.entries(rappelsByCompteEmail)) {
			if (!rappels.length) {
				continue;
			}

			const rappelsData: EntiteData[] = rappels.map(rappelFormatSimple);

			try {
				await emailSend({
					to: captured ?? email,
					subject: `Rappels Dévéco`,
					html: serviceEmailTemplateStats.rappelsReminder({
						appUrl,
						page: '/suivis?suivi=rappels',
						rappelsData,
					}),
				});

				console.logInfo(
					`${captured ? "Capture d'" : 'Envoi '}un email pour ${
						rappels.length
					} rappel(s) à ${email}`
				);
			} catch (error) {
				console.logError(error);
			}

			const delay = 500;
			await wait(delay);
		}

		await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { finAt: new Date() } });
	},

	async rechercheSauvegardeeAdd({
		equipeId,
		devecoId,
		query,
	}: {
		equipeId: Equipe['id'];
		devecoId: Deveco['id'];
		query: string;
	}) {
		const [{ token }] = await dbQueryEquipe.rechercheSauvegardeeAdd({
			equipeId,
			devecoId,
			query,
		});

		return token;
	},

	async zonageAdd({
		evenementId,
		equipeId,
		nom,
		description,
		geometrie,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		nom: Etiquette['nom'];
		description: Zonage['description'];
		geometrie: string;
	}) {
		const [etiquette] = await dbQueryEquipe.etiquetteAddMany({
			equipeId,
			etiquetteTypeId: 'localisation',
			noms: [nom],
		});

		const etiquetteId = etiquette.id;

		await dbQueryEvenement.actionEtiquetteAddMany({
			evenementId,
			etiquetteIds: [etiquetteId],
			typeId: 'creation',
			diff: {
				message: `création de l'étiquette ${nom} de zonage`,
			},
		});

		const [zonage] = await dbQueryEquipe.zonageAdd({
			equipeId,
			etiquetteId,
			geometrie,
			description,
		});

		const zonageId = zonage.id;

		await elasticQueryZonage.zonageAdd({
			equipeId,
			zonageId,
			zonageNom: nom,
			etiquetteId,
			geometrie,
		});

		return {
			...zonage,
			etiquette,
		};
	},

	async zonageUpdate({
		evenementId,
		equipeId,
		zonageId,
		description,
		geometrie,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		zonageId: Zonage['id'];
		description: Zonage['description'];
		geometrie: string | null;
	}) {
		const [zonage] = await dbQueryEquipe.zonageUpdate({
			equipeId,
			zonageId,
			description,
			geometrie,
		});

		await dbQueryEvenement.actionZonageAdd({
			evenementId,
			zonageId,
			typeId: 'modification',
		});

		if (geometrie) {
			await elasticQueryZonage.zonageUpdate({
				zonageId,
				geometrie,
			});
		}

		return zonage;
	},

	async zonageRemove({
		evenementId,
		equipeId,
		zonageId,
		etiquetteId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etiquetteId: Zonage['etiquetteId'];
		zonageId: Zonage['id'];
	}) {
		// on ajoute une action, mais elle va disparaitre par cascade
		// TODO : soft delete ?
		await dbQueryEvenement.actionZonageAdd({
			evenementId,
			zonageId,
			typeId: 'suppression',
		});

		// supprime l'étiquette liée au zonage et le zonage par cascade
		await dbQueryEquipe.etiquetteRemoveManyByEquipe({
			equipeId,
			etiquetteIds: [etiquetteId],
		});
	},

	async notificationsGet({ devecoId }: { devecoId: Deveco['id'] }) {
		const notifications = await dbQueryEquipe.notificationsGetByDevecoId({ devecoId });
		let notifs;

		if (!notifications) {
			notifs = {
				connexion: true,
				rappels: true,
				activite: true,
			};
		} else {
			notifs = {
				connexion: notifications.connexion,
				rappels: notifications.rappels,
				activite: notifications.activite,
			};
		}

		return notifs;
	},

	async notificationsUpdate({
		devecoId,
		notifications,
	}: {
		devecoId: Deveco['id'];
		notifications: Pick<Notifications, 'connexion' | 'rappels' | 'activite'>;
	}) {
		await dbQueryEquipe.notificationsUpdate({ notifications: { devecoId, ...notifications } });

		return;
	},
};

function sendStatsToDeveco(appUrl: string, stats: Stats) {
	const subject = `Mon tableau de bord Deveco${' '}: les deux semaines précédentes de ${
		stats.equipe
	}`;

	return (compte: Pick<Compte, 'nom' | 'prenom' | 'email'>) => {
		const html = serviceEmailTemplateStats.statsRecap({
			appUrl,
			compte,
			stats,
		});

		const mailOptions = {
			to: compte.email,
			subject,
			html,
		};

		emailSend(mailOptions).catch((emailError) => {
			console.logError(emailError);
		});
	};
}
