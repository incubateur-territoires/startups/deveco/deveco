import { CtxDefault, CtxUser } from '../middleware/auth';

function setCookie({
	ctx,
	token,
	expires,
}: {
	ctx: CtxDefault | CtxUser;
	token: string;
	expires?: Date | undefined;
}) {
	ctx.cookies.set(serviceCookie.jwtName, token, {
		path: '/',
		httpOnly: true,
		secure: false, // on envoie false car 1) la connexion publique est via HTTPS et 2) la connexion interne est en HTTP, donc secure=true empêche le bon fonctionnement
		sameSite: 'strict',
		expires,
	});
	ctx.headers['cache-control'] = 'private';
}

export const serviceCookie = {
	jwtName: 'jwt',

	add({ ctx, token }: { ctx: CtxDefault | CtxUser; token: string }) {
		setCookie({ ctx, token });
	},

	remove(ctx: CtxDefault) {
		setCookie({ ctx, token: 'delete', expires: new Date(0) });
	},
};
