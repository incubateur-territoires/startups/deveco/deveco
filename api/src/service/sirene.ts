import {
	apiSirene,
	SireneEtablissement,
	SireneEtablissementPeriode,
	SireneLienSuccession,
	SireneUniteLegale,
	SireneUniteLegalePeriode,
} from '../api/api-sirene';
import { dateToString } from '../lib/dateUtils';
import {
	Etablissement,
	EtablissementPeriode,
	LienSuccession,
	Entreprise,
	EntreprisePeriode,
} from '../db/types';
import * as dbQueryAdmin from '../db/query/admin';
import * as dbQueryEtablissement from '../db/query/etablissement';

function etablissementFormat(sireneEtablissement: SireneEtablissement): Etablissement {
	const {
		periodesEtablissement,
		uniteLegale,
		adresseEtablissement,
		dateDernierTraitementEtablissement,
		dateCreationEtablissement,
		...rest
	} = sireneEtablissement;

	const currentPeriode =
		periodesEtablissement.find(({ dateFin }) => dateFin === null) || periodesEtablissement[0];

	return {
		...rest,
		...adresseEtablissement,
		...currentPeriode,
		dateDernierTraitementEtablissement: new Date(dateDernierTraitementEtablissement),
		dateCreationEtablissement: new Date(dateCreationEtablissement),
		dateDebut: currentPeriode.dateDebut ? new Date(currentPeriode.dateDebut) : null,
		//
		miseAJourAt: new Date(),
	};
}

function entrepriseFormat(sireneUniteLegale: SireneUniteLegale): Entreprise {
	const {
		periodesUniteLegale,
		dateDernierTraitementUniteLegale,
		dateCreationUniteLegale,
		...rest
	} = sireneUniteLegale;

	const currentPeriode =
		periodesUniteLegale.find(({ dateFin }) => dateFin === null) || periodesUniteLegale[0];

	return {
		...rest,
		...currentPeriode,
		economieSocialeSolidaireUniteLegale: currentPeriode.economieSocialeSolidaireUniteLegale,
		dateDebut: new Date(currentPeriode.dateDebut),
		dateDernierTraitementUniteLegale: new Date(dateDernierTraitementUniteLegale),
		dateCreationUniteLegale: new Date(dateCreationUniteLegale),
		miseAJourAt: new Date(),
	};
}

function sourceEtablissementPeriodeFormat(
	siret: string,
	sourceEtablissementPeriodes: SireneEtablissementPeriode[]
): EtablissementPeriode[] {
	return sourceEtablissementPeriodes.map((sep) => {
		const { dateDebut, dateFin } = sep;

		return {
			siret,
			...sep,
			dateDebut: dateDebut ? new Date(dateDebut) : null,
			dateFin: dateFin ? new Date(dateFin) : null,
			miseAJourAt: new Date(),
		};
	});
}

function etablissementAndPeriodesFormat(sireneEtablissements: SireneEtablissement[]) {
	const { sirets, etablissements, etablissementPeriodes } = sireneEtablissements.reduce(
		(
			{
				sirets,
				etablissements,
				etablissementPeriodes,
			}: {
				sirets: Etablissement['siret'][];
				etablissements: Etablissement[];
				etablissementPeriodes: EtablissementPeriode[];
			},
			sireneEtablissement
		) => {
			const sourceEtablissement = etablissementFormat(sireneEtablissement);
			etablissements.push(sourceEtablissement);

			const sourceEtablissementPeriodes = sourceEtablissementPeriodeFormat(
				sireneEtablissement.siret,
				sireneEtablissement.periodesEtablissement
			);

			etablissementPeriodes.push(...sourceEtablissementPeriodes);

			sirets.push(sireneEtablissement.siret);

			return {
				sirets,
				etablissements,
				etablissementPeriodes,
			};
		},
		{
			sirets: [],
			etablissements: [],
			etablissementPeriodes: [],
		}
	);

	return {
		sirets,
		etablissements,
		etablissementPeriodes,
	};
}

function sourceUniteLegalePeriodeFormatMany(
	siren: string,
	sireneUniteLegalePeriodes: SireneUniteLegalePeriode[]
): EntreprisePeriode[] {
	return sireneUniteLegalePeriodes.map((sep) => {
		const { dateDebut, dateFin } = sep;

		return {
			siren,
			...sep,
			dateDebut: dateDebut ? new Date(dateDebut) : null,
			dateFin: dateFin ? new Date(dateFin) : null,
			miseAJourAt: new Date(),
		};
	});
}

function entrepriseAndPeriodeFormatMany(sireneUniteLegales: SireneUniteLegale[]) {
	return sireneUniteLegales.reduce(
		(
			{
				entreprises,
				entreprisePeriodes,
			}: {
				entreprises: Entreprise[];
				entreprisePeriodes: EntreprisePeriode[];
			},
			sireneUniteLegale
		) => {
			const sourceUniteLegale = entrepriseFormat(sireneUniteLegale);
			entreprises.push(sourceUniteLegale);

			const sourceUniteLegalePeriodes = sourceUniteLegalePeriodeFormatMany(
				sireneUniteLegale.siren,
				sireneUniteLegale.periodesUniteLegale
			);

			entreprisePeriodes.push(...sourceUniteLegalePeriodes);

			return {
				entreprises,
				entreprisePeriodes,
			};
		},
		{
			entreprises: [],
			entreprisePeriodes: [],
		}
	);
}

async function etablissementAndPeriodesUpsertMany({
	etablissements: es,
}: {
	etablissements: SireneEtablissement[];
}) {
	const { sirets, etablissements, etablissementPeriodes } = etablissementAndPeriodesFormat(es);

	await dbQueryEtablissement.etablissementUpsertMany({
		etablissements: etablissements,
	});
	await dbQueryEtablissement.etablissementPeriodeRemoveMany({ sirets });

	const batch = 1000;
	while (etablissementPeriodes.length) {
		const batchInseeSireneApiEtablissementPeriodes = etablissementPeriodes.splice(0, batch);

		await dbQueryEtablissement.etablissementPeriodeAddMany({
			etablissementPeriodes: batchInseeSireneApiEtablissementPeriodes,
		});
	}
}

function lienSuccessionFormat(lienSuccession: SireneLienSuccession): LienSuccession {
	const { dateDernierTraitementLienSuccession, dateLienSuccession, ...rest } = lienSuccession;

	return {
		...rest,
		dateDernierTraitementLienSuccession: new Date(dateDernierTraitementLienSuccession),
		dateLienSuccession: new Date(dateLienSuccession),
		miseAJourAt: new Date(),
	};
}

function lienSuccessionFormatMany(lienSuccessions: SireneLienSuccession[]): LienSuccession[] {
	return lienSuccessions.map(lienSuccessionFormat);
}

function lienSuccessionDeDuplicate(lienSuccessions: SireneLienSuccession[]) {
	return lienSuccessions.reduce((dedups, l) => {
		const dup = dedups.find(
			(d) =>
				d.siretEtablissementPredecesseur === l.siretEtablissementPredecesseur &&
				d.siretEtablissementSuccesseur === l.siretEtablissementSuccesseur &&
				d.dateLienSuccession === l.dateLienSuccession
		);

		if (!dup) {
			dedups.push(l);
		}

		return dedups;
	}, [] as SireneLienSuccession[]);
}

async function entrepriseAndPeriodeUpsertMany({
	unitesLegales,
}: {
	unitesLegales: SireneUniteLegale[];
}) {
	const { entreprises, entreprisePeriodes } = entrepriseAndPeriodeFormatMany(unitesLegales);

	await dbQueryEtablissement.entrepriseUpsertMany({ entreprises });

	const sirens = entreprises.map(({ siren }) => siren);

	await dbQueryEtablissement.entreprisePeriodeRemoveMany({ sirens });

	const batch = 1000;
	while (entreprisePeriodes.length) {
		const batchInseeSireneApiUniteLegalePeriodes = entreprisePeriodes.splice(0, batch);

		await dbQueryEtablissement.entreprisePeriodeAddMany({
			entreprisePeriodes: batchInseeSireneApiUniteLegalePeriodes,
		});
	}
}

export const serviceSirene = {
	async etablissementUpsertManyBySiren({
		tacheNom,
		sirens,
	}: {
		tacheNom: string;
		sirens: string[];
	}): Promise<void> {
		return serviceSirene.etablissementUpsertMany({
			tacheNom,
			extraParams: [sirens.map((siren) => `siren:${siren}`).join(' OR ')],
		});
	},

	async etablissementUpsertMany({
		tacheNom,
		startDate,
		endDate,
		extraParams,
	}: {
		tacheNom: string;
		startDate?: Date;
		endDate?: Date;
		extraParams?: string[];
	}): Promise<void> {
		if (!startDate && !endDate && !extraParams?.length) {
			throw new Error('paramètres obligatoires: startDate, endDate et extraParams');
		}

		const [tache] = await dbQueryAdmin.tacheAdd({ tache: { nom: tacheNom } });
		const tacheId = tache.id;

		let nextCursor = '';
		let loop = 0;
		let lastTotal = 0;
		let processedEtablissements = 0;
		let lastEtablissement = null;

		const apiSireneEtablissementsCursor = apiSirene.etablissementGetAllByCursor({
			start: startDate ? dateToString(startDate) : undefined,
			end: endDate ? dateToString(endDate) : undefined,
			extraParams,
		});

		try {
			for await (const sireneResponseEtablissements of apiSireneEtablissementsCursor) {
				const { header, etablissements } = sireneResponseEtablissements;

				lastTotal = header.total;
				loop += 1;
				nextCursor = header.curseurSuivant;
				lastEtablissement = etablissements.slice(-1)[0];

				await etablissementAndPeriodesUpsertMany({ etablissements });
				await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { enCoursAt: new Date() } });

				processedEtablissements += etablissements.length;

				console.logInfo(
					`[etablissementUpsertMany] mise à jour des établissements source : ${processedEtablissements} / ${lastTotal} (boucle ${loop})`
				);
			}
		} catch (e) {
			console.logInfo(
				`[sireneEtablissementUpsertMany] erreur: mise à jour des établissements source`,
				e
			);
			console.logInfo(
				`[etablissementUpsertMany] traité jusqu'à ${lastTotal} (boucle ${loop}), dernier curseur: ${nextCursor}`
			);
			console.logInfo(
				`[etablissementUpsertMany] dernier établissement traité :
					${lastEtablissement?.siret},
					${lastEtablissement?.dateDernierTraitementEtablissement}`
			);

			throw e;
		}

		await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { finAt: new Date() } });

		console.logInfo(
			`[sireneEtablissementUpsertMany] récapitulatif: ${processedEtablissements} / ${lastTotal} établissements mis à jour`
		);
	},

	async lienSuccessionUpsertManyByDate({
		startDate,
		endDate,
		tacheNom,
	}: {
		startDate: Date;
		endDate: Date;
		tacheNom: string;
	}): Promise<void> {
		if (!startDate && !endDate) {
			throw new Error('paramètres obligatoires: startDate, endDate');
		}

		const [tache] = await dbQueryAdmin.tacheAdd({ tache: { nom: tacheNom } });
		const tacheId = tache.id;

		let nextCursor = '*';
		let loop = 0;
		let lastTotal = 0;
		let processedLienSuccessions = 0;
		let lastLienSuccession = null;

		const apiSireneLienSuccessionsCursor = apiSirene.lienSuccessionGetAllByCursor({
			start: dateToString(startDate),
			end: dateToString(endDate),
		});

		try {
			for await (const sireneResponseLienSuccessions of apiSireneLienSuccessionsCursor) {
				const { header, liensSuccession } = sireneResponseLienSuccessions;

				const lienSuccessions = lienSuccessionFormatMany(
					lienSuccessionDeDuplicate(liensSuccession)
				);

				lastTotal = header.total;
				loop += 1;
				nextCursor = header.curseurSuivant;
				lastLienSuccession = liensSuccession.slice(-1)[0];

				await dbQueryEtablissement.lienSuccessionUpsertMany({
					lienSuccessions,
				});
				await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { enCoursAt: new Date() } });

				processedLienSuccessions += liensSuccession.length;

				console.logInfo(
					`[lienSuccessionUpsertManyByDate] mise à jour des liens d'établissement source : ${processedLienSuccessions} / ${lastTotal} (boucle ${loop})`
				);
			}
		} catch (e) {
			console.logInfo(
				`[lienSuccessionUpsertManyByDate] erreur : mise à jour des liens d'établissement source`,
				e
			);
			console.logInfo(
				`[lienSuccessionUpsertManyByDate] traité jusqu'à ${lastTotal} (boucle ${loop}), curseur: ${nextCursor}`
			);
			console.logInfo(
				`[lienSuccessionUpsertManyByDate] dernière unité légale traitée :
					${lastLienSuccession?.siretEtablissementSuccesseur},
					${lastLienSuccession?.dateDernierTraitementLienSuccession}`
			);
		}

		await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { finAt: new Date() } });

		console.logInfo(
			`[sireneLienSuccessionManyByDate] récapitulatif: ${processedLienSuccessions} / ${lastTotal} liens d'établissement mises à jour`
		);
	},

	async lienSuccessionCheckMany({
		tacheNom,
		miseAJourAt,
	}: {
		miseAJourAt: Date;
		tacheNom: string;
	}) {
		const [tache] = await dbQueryAdmin.tacheAdd({ tache: { nom: tacheNom } });
		const tacheId = tache.id;

		const sourceApiSireneLienSuccessionsCursor =
			dbQueryEtablissement.lienSuccessionMissingGetCursorByMiseAJourDate({
				miseAJourAt,
			});

		for await (const lienSuccessionsRaw of sourceApiSireneLienSuccessionsCursor) {
			for (const lien of lienSuccessionsRaw) {
				console.logError(
					'[sireneLienSuccessionCheckMany]',
					[
						`date lien : ${lien.date_lien_succession}`,
						`date de dernier traitement : ${lien.date_dernier_traitement_lien_succession}`,
						`prédecesseur : ${lien.siret_etablissement_predecesseur}`,
						`successeur : ${lien.siret_etablissement_successeur}`,
						`transfert de siège: ${lien.transfert_siege}`,
						`prédecesseur manquant : ${lien.predecesseur_manquant}`,
						`successeur manquant : ${lien.successeur_manquant}`,
					].join(', ')
				);
			}
		}

		await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { finAt: new Date() } });
	},

	async uniteLegaleUpsertMany({
		tacheNom,
		startDate,
		endDate,
		extraParams,
	}: {
		tacheNom: string;
		startDate?: Date;
		endDate?: Date;
		extraParams?: string[];
	}): Promise<void> {
		if (!startDate && !endDate && !extraParams?.length) {
			throw new Error('paramètres obligatoires: startDate, endDate ou extraParams');
		}

		const [tache] = await dbQueryAdmin.tacheAdd({ tache: { nom: tacheNom } });
		const tacheId = tache.id;

		let nextCursor = '';
		let loop = 0;
		let lastTotal = 0;
		let processedUniteLegales = 0;
		let lastUniteLegale = null;

		const apiSireneUniteLegalesCursor = apiSirene.uniteLegaleGetAllByCursor({
			start: startDate ? dateToString(startDate) : undefined,
			end: endDate ? dateToString(endDate) : undefined,
			extraParams,
		});

		try {
			for await (const sireneResponseUniteLegales of apiSireneUniteLegalesCursor) {
				const { header, unitesLegales } = sireneResponseUniteLegales;

				lastTotal = header.total;
				loop += 1;
				nextCursor = header.curseurSuivant;
				lastUniteLegale = unitesLegales.slice(-1)[0];

				await entrepriseAndPeriodeUpsertMany({ unitesLegales });
				await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { enCoursAt: new Date() } });

				processedUniteLegales += unitesLegales.length;

				console.logInfo(
					`[uniteLegaleUpsertMany] mise à jour des unités légales source : ${processedUniteLegales} / ${lastTotal} (boucle ${loop})`
				);
			}
		} catch (e) {
			console.logInfo(
				`[sireneUniteLegaleUpsertMany] erreur: mise à jour des unités légales source`,
				e
			);
			console.logInfo(
				`[uniteLegaleUpsertMany] traité jusqu'à ${lastTotal} (boucle ${loop}), dernier curseur: ${nextCursor}`
			);
			console.logInfo(
				`[uniteLegaleUpsertMany] dernier établissement traité :
					${lastUniteLegale?.siren},
					${lastUniteLegale?.dateDernierTraitementUniteLegale}`
			);

			throw e;
		}

		await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { finAt: new Date() } });

		console.logInfo(
			`[sireneUniteLegaleUpsertMany] récapitulatif: ${processedUniteLegales} / ${lastTotal} unités légales mises à jour`
		);
	},

	async entrepriseUpsertManyBySiren({
		tacheNom,
		sirens,
	}: {
		tacheNom: string;
		sirens: string[];
	}): Promise<void> {
		return serviceSirene.uniteLegaleUpsertMany({
			tacheNom,
			extraParams: [sirens.map((siren) => `siren:${siren}`).join(' OR ')],
		});
	},

	async uniteLegaleUpsertManyByUpdatedEtablissements({
		miseAJourAt,
		tacheNom,
	}: {
		miseAJourAt: Date;
		tacheNom: string;
	}): Promise<void> {
		const [tache] = await dbQueryAdmin.tacheAdd({ tache: { nom: tacheNom } });
		const tacheId = tache.id;

		console.logInfo(
			`[sireneUniteLegaleUpsertManyByUpdatedEtablissements] - Début, depuis : ${miseAJourAt}`
		);

		const cursor = dbQueryEtablissement.entrepriseMissingWithIdGetCursor({
			miseAJourAt,
		});

		let count = 0;

		for await (const idsRaw of cursor) {
			const batchSirens = idsRaw.map(({ siren }) => siren);

			count += batchSirens.length;

			console.logInfo(
				`[sireneUniteLegaleUpsertManyByUpdatedEtablissements] - mise à jour des entreprises d'établissements non mises à jour, ${count}, sirens : ${batchSirens.join(
					', '
				)}`
			);

			await serviceSirene.entrepriseUpsertManyBySiren({
				tacheNom: `${tacheNom}-${count}`,
				sirens: batchSirens,
			});

			await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { enCoursAt: new Date() } });
		}

		await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { finAt: new Date() } });

		console.logInfo(
			`[sireneUniteLegaleUpsertManyByUpdatedEtablissements] - Fin, depuis : ${miseAJourAt}`
		);
	},
};
