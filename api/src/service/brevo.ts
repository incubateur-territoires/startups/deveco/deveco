import fetch, { RequestInit } from 'node-fetch';

import { Compte, Equipe } from '../db/types';
import * as dbQueryEquipe from '../db/query/equipe';

const brevoApiKey = process.env.BREVO_API_KEY ?? '';
const listNumber = Number(process.env.BREVO_LISTE);
const brevoList = isNaN(listNumber) ? 0 : listNumber;

interface AttributesBrevo {
	EMAIL: string;
	LASTNAME: string | null;
	FIRSTNAME: string | null;
	COLLECTIVITE: string | null;
}

interface ContactBrevo {
	email: string;
	attributes: AttributesBrevo;
}

interface BrevoTransactionalOptions {
	to: { email: string; nom?: string }[];
	templateId: number;
	params: Record<string, unknown>;
}

function contactToBrevo({
	nom,
	prenom,
	email,
	actif,
	equipeNom,
}: Pick<Compte, 'email' | 'nom' | 'prenom' | 'actif'> & {
	equipeNom: Equipe['nom'];
}): ContactBrevo {
	email = email.toLocaleLowerCase();

	const attributes = {
		EMAIL: email,
		LASTNAME: nom,
		FIRSTNAME: prenom,
		COLLECTIVITE: equipeNom,
		ACTIF: actif,
		// FONCTION: '',
		// "DATE D'AJOUT": new Date(),
	};

	return {
		email,
		attributes,
	};
}

async function requeteBrevo({
	nom,
	url,
	body,
}: {
	nom: string;
	url: string;
	body: RequestInit['body'];
}) {
	const requestTime = new Date().getTime();

	const options = {
		method: 'POST',
		headers: {
			accept: 'application/json',
			'content-type': 'application/json',
			'api-key': brevoApiKey,
		},
		body,
	};

	console.logInfo(`[serviceBrevo.${nom}] Options d'envoi :`, options, `- requête ${requestTime}`);

	try {
		const res = await fetch(url, options);
		const status = res.status;

		const json = res.status === 204 ? {} : await res.json();

		console.logInfo(
			`[serviceBrevo.${nom}] Réponse de l'API Brevo (${res.status}) :`,
			json,
			`- requête ${requestTime}`
		);

		return { status, json, requestTime };
	} catch (err: any) {
		console.logError(
			`[serviceBrevo.${nom}] Erreur de l'API Brevo :`,
			err,
			`- requête ${requestTime}`
		);
		return { status: 500, json: {}, requestTime };
	}
}

async function templateSend(opts: BrevoTransactionalOptions) {
	const url = 'https://api.brevo.com/v3/smtp/email';

	console.logInfo(`[brevoSend] Envoie d'un email templaté`, { templateId: opts.templateId });

	return requeteBrevo({
		nom: 'templateSend',
		url,
		body: JSON.stringify(opts),
	});
}

export const serviceBrevo = {
	bienvenueEmail(compte: Compte, motDePasse: string) {
		const appUrl = process.env.APP_URL || '';

		return templateSend({
			to: [{ email: compte.email, nom: `${compte.prenom} ${compte.nom}` }],
			templateId: 268,
			params: {
				appUrl,
				email: compte.email,
				cle: compte.cle,
				prenom: compte.prenom,
				nom: compte.nom,
				motDePasse,
			},
		});
	},

	relanceDixJoursEmail(compte: Compte) {
		const appUrl = process.env.APP_URL || '';

		return templateSend({
			to: [{ email: compte.email, nom: `${compte.prenom} ${compte.nom}` }],
			templateId: 267,
			params: {
				appUrl,
				email: compte.email,
				cle: compte.cle,
				prenom: compte.prenom,
				nom: compte.nom,
			},
		});
	},

	relanceTrenteJoursEmail(compte: Compte) {
		const appUrl = process.env.APP_URL || '';

		return templateSend({
			to: [{ email: compte.email, nom: `${compte.prenom} ${compte.nom}` }],
			templateId: 269,
			params: {
				appUrl,
				email: compte.email,
				cle: compte.cle,
				prenom: compte.prenom,
				nom: compte.nom,
			},
		});
	},

	async contactAdd(compte: Compte, equipe: Pick<Equipe, 'nom'>) {
		if (!brevoList || !brevoApiKey) {
			return;
		}

		const url = 'https://api.brevo.com/v3/contacts';

		const { email, attributes } = contactToBrevo({
			...compte,
			equipeNom: equipe.nom,
		});

		const body = JSON.stringify({
			email,
			listIds: [brevoList],
			updateEnabled: true,
			attributes,
		});

		const nom = 'contactAdd';

		try {
			const res = await requeteBrevo({
				nom,
				url,
				body,
			});

			if (res.status !== 204) {
				// TODO gérer les éventuelles erreurs
				return;
			}

			console.logInfo(
				`[serviceBrevo.${nom}] Réponse de l'API Brevo (${res.status}) : contact créé`,
				`- requête ${res.requestTime}`
			);
		} catch (e: any) {
			console.logError(`[serviceBrevo.${nom}] Erreur : ${e?.message ?? 'erreur inconnue'}`);
		}
	},

	async contactUpdateMany({ equipeId }: { equipeId?: Equipe['id'] }) {
		if (!brevoList || !brevoApiKey) {
			return;
		}

		const url = 'https://api.brevo.com/v3/contacts/batch';

		let finished = false;
		let page = 1;

		while (!finished) {
			const utilisateurs = await dbQueryEquipe.compteUniqueWithEquipeGetMany({
				equipeId,
				elementsParPage: 100,
				page,
			});

			if (utilisateurs.length === 0) {
				finished = true;

				continue;
			}

			const contacts: ContactBrevo[] = utilisateurs.map(contactToBrevo);

			const body = JSON.stringify({
				contacts,
			});

			await requeteBrevo({
				nom: 'contactUpdateMany',
				url,
				body,
			});

			page = page + 1;
		}
	},
};
