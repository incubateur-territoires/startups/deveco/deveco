import Imap from 'node-imap';
import { simpleParser, AddressObject } from 'mailparser';

import { serviceBrouillon } from '../service/brouillon';

export type Mail = {
	from: string[];
	to: string[];
	date: Date;
	sujet: string;
	contenu: string;
};

function adresseParse(adresse?: AddressObject | AddressObject[]): string[] {
	if (!adresse) {
		return [];
	}

	const adresses = !Array.isArray(adresse) ? [adresse] : adresse;

	return adresses.flatMap(
		(adresse) => adresse?.value.flatMap((a) => (a.address ? [a.address.toLowerCase()] : [])) ?? []
	);
}

function inboxOpen(imap: Imap, boxName: string): Promise<Imap.Box> {
	return new Promise((resolve, reject) => {
		imap.openBox(boxName, false, (err, box) => {
			if (err) {
				return reject(err);
			}
			resolve(box);
		});
	});
}

function mailSearch(imap: Imap, criteria: (string | string[])[]): Promise<number[]> {
	return new Promise((resolve, reject) => {
		imap.search(criteria, (err, results: number[]) => {
			if (err) {
				return reject(err);
			}
			resolve(results);
		});
	});
}

function mailFlagDeleted(imap: Imap, uid: number) {
	return new Promise<void>((resolve, reject) => {
		imap.addFlags(uid, ['\\Deleted'], (err) => {
			if (err) {
				return reject(err);
			}
			resolve();
		});
	});
}

// TODO Quand on aura décidé de purger les emails de brouillon
// function mailExpunge(imap: Imap, uid: number) {
// 	return new Promise<void>((resolve, reject) => {
// 		imap.expunge(uid, (err) => {
// 			if (err) return reject(err);
// 			resolve();
// 		});
// 	});
// }

function messageHandle(imap: Imap, msg: Imap.ImapMessage, seqno: number) {
	return new Promise<void>((resolveMsg, rejectMsg) => {
		let emailBuffer = '';

		msg.on('body', (stream) => {
			stream.on('data', (chunk: Buffer) => {
				emailBuffer += chunk.toString('utf8');
			});
		});

		let attributes: Imap.ImapMessageAttributes | null = null;

		msg.once('attributes', (attrs) => {
			attributes = attrs;
		});

		msg.once(
			'end',
			() =>
				void (async () => {
					console.logInfo(
						`[emailServer.mailFetchMany] Récupération du mail de brouillon #${seqno}`
					);

					try {
						const parsed = await simpleParser(emailBuffer);

						const adressesFrom = adresseParse(parsed.from);
						if (!adressesFrom.length) {
							return;
						}

						const adressesTo = [
							...adresseParse(parsed.to),
							...adresseParse(parsed.cc),
							...adresseParse(parsed.bcc),
						].filter((adresse) => !adresse.startsWith('brouillon@'));

						await serviceBrouillon.brouillonAddByMail({
							from: adressesFrom,
							to: adressesTo,
							date: parsed.date ?? new Date(),
							sujet: parsed.subject || '',
							contenu: parsed.text || '',
						});

						if (attributes) {
							await mailFlagDeleted(imap, attributes.uid);

							// await mailExpunge(imap, attributes.uid);
						}

						resolveMsg();
					} catch (error: any) {
						rejectMsg(
							new Error(
								`[emailServer.mailFetchMany] Erreur pendant la récupération du mail de brouillon #${seqno} : ${error.message}`
							)
						);
					}
				})()
		);
	});
}

function mailFetchMany(imap: Imap, results: number[]): Promise<void> {
	return new Promise((resolve, reject) => {
		const fetch = imap.fetch(results, { bodies: '', markSeen: true });
		const messagePromises: Promise<void>[] = [];

		fetch.on('message', (msg, seqno) => {
			const p = messageHandle(imap, msg, seqno);
			messagePromises.push(p);
		});

		fetch.once('error', (err: Error) => {
			reject(err);
		});

		fetch.once('end', () => {
			Promise.all(messagePromises)
				.then(() => resolve())
				.catch(reject);
		});
	});
}

export const serviceEmailServer = {
	async emailProcessMany(): Promise<void> {
		if (!process.env.MAIL_IMAP_PASSWORD) {
			console.logInfo('Pas de configuration IMAP');
			return;
		}

		const imapConfig: Imap.Config = {
			user: process.env.MAIL_IMAP_USER || '',
			password: process.env.MAIL_IMAP_PASSWORD || '',
			host: process.env.MAIL_IMAP_HOST || '',
			port: process.env.MAIL_IMAP_PORT ? parseInt(process.env.MAIL_IMAP_PORT) : 993,
			tls: true,
		};

		const imap = new Imap(imapConfig);

		try {
			await new Promise<void>((resolve, reject) => {
				imap.once('ready', resolve);
				imap.once('error', reject);
				imap.connect();
			});

			const box = await inboxOpen(imap, 'Brouillons-Deveco');
			console.logInfo(
				`[emailServer.emailProcessMany] Ouverture de la boîte mail de brouillon : ${box.name}`
			);

			const results = await mailSearch(imap, [`UNSEEN`]);
			if (!results || results.length === 0) {
				console.logInfo('[emailServer.emailProcessMany] Aucun mail de brouillon à traiter.');
			} else {
				await mailFetchMany(imap, results);
				console.logInfo(
					'[emailServer.emailProcessMany] Tous les mails de brouillon ont été traités.'
				);
			}
		} catch (err) {
			console.logError(
				'[emailServer.emailProcessMany] Erreur pendant le traitement des mails de brouillon :',
				err
			);
			throw err;
		} finally {
			await new Promise<void>((resolve) => {
				imap.once('end', resolve);
				imap.end();
			});
		}
	},
};
