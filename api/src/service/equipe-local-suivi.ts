import { Equipe, Local, Evenement, Echange, Demande, Rappel } from '../db/types';
import * as dbQueryEquipeLocal from '../db/query/equipe-local';
import * as dbQueryEvenement from '../db/query/evenement';
import * as ElasticQueryEquipeLocal from '../elastic/query/equipe-local';

async function eqLocSuiviDatesUpdate({
	equipeId,
	localId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
}) {
	const suiviDates = await dbQueryEquipeLocal.eqLocSuiviDateGetMany({
		equipeId,
		localId,
	});

	return ElasticQueryEquipeLocal.eqLocSuiviDatesUpdate({
		equipeId,
		localId,
		suiviDates: suiviDates.map((s) => s.suiviDates),
	});
}

export const serviceEquipeLocalSuivi = {
	async echangeAdd({
		evenementId,
		equipeId,
		localId,
		echange,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		echange: Echange;
	}) {
		await dbQueryEquipeLocal.eqLocEchangeAdd({
			equipeId,
			localId,
			echangeId: echange.id,
		});

		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `creation d'un échange`,
				changes: {
					table: `eqLocEchange`,
					after: {
						equipeId,
						localId,
						echangeId: echange.id,
					},
				},
			},
		});

		await eqLocSuiviDatesUpdate({
			equipeId,
			localId,
		});
	},

	async echangeUpdate({
		evenementId,
		equipeId,
		localId,
		echange,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		echange: Echange;
	}) {
		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `modification d'un échange`,
				changes: {
					table: `eqLocEchange`,
					after: {
						equipeId,
						localId,
						echangeId: echange.id,
					},
				},
			},
		});

		await eqLocSuiviDatesUpdate({
			equipeId,
			localId,
		});
	},

	async echangeRemove({
		evenementId,
		equipeId,
		localId,
		echangeId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		echangeId: Echange['id'];
	}) {
		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `suppression d'un échange`,
				changes: {
					table: `eqLocEchange`,
					before: {
						equipeId,
						localId,
						echangeId,
					},
				},
			},
		});

		await eqLocSuiviDatesUpdate({
			equipeId,
			localId,
		});
	},

	async demandeAdd({
		evenementId,
		demande,
		equipeId,
		localId,
	}: {
		evenementId: Evenement['id'];
		demande: Demande;
		equipeId: Equipe['id'];
		localId: Local['id'];
	}) {
		await dbQueryEquipeLocal.eqLocDemandeAdd({
			demandeId: demande.id,
			localId,
			equipeId,
		});

		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `creation d'une demande`,
				changes: {
					table: `eqLocDemande`,
					after: {
						equipeId,
						localId,
						demandeId: demande.id,
					},
				},
			},
		});

		await ElasticQueryEquipeLocal.eqLocDemandeAdd({
			equipeId,
			localId,
			demandeTypeIds: [demande.typeId],
		});

		await eqLocSuiviDatesUpdate({
			equipeId,
			localId,
		});
	},

	async demandeUpdate({
		evenementId,
		equipeId,
		localId,
		demande,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		demande: Demande;
	}) {
		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `modification d'une demande`,
				changes: {
					table: `eqLocDemande`,
					after: {
						equipeId,
						localId,
						demandeId: demande.id,
					},
				},
			},
		});
	},

	async demandeReouverture({
		evenementId,
		equipeId,
		localId,
		demande,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		demande: Demande;
	}) {
		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `reouverture d'une demande`,
				changes: {
					table: `eqLocDemande`,
					after: {
						equipeId,
						localId,
						demandeId: demande.id,
					},
				},
			},
		});

		await ElasticQueryEquipeLocal.eqLocDemandeAdd({
			equipeId,
			localId,
			demandeTypeIds: [demande.typeId],
		});
	},

	async demandeCloture({
		evenementId,
		equipeId,
		localId,
		demande,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		demande: Demande;
	}) {
		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `cloture d'une demande`,
				changes: {
					table: `eqLocDemande`,
					after: {
						equipeId,
						localId,
						demandeId: demande.id,
					},
				},
			},
		});

		await ElasticQueryEquipeLocal.eqLocDemandeRemove({
			equipeId,
			localId,
			demandeTypeId: demande.typeId,
		});
	},

	async demandeRemove({
		evenementId,
		equipeId,
		localId,
		demandeId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		demandeId: Demande['id'];
	}) {
		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `suppression d'une demande`,
				changes: {
					table: `eqLocDemande`,
					before: {
						equipeId,
						localId,
						demandeId,
					},
				},
			},
		});

		await eqLocSuiviDatesUpdate({
			equipeId,
			localId,
		});
	},

	async rappelAdd({
		evenementId,
		equipeId,
		localId,
		rappel,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		rappel: Rappel;
	}) {
		await dbQueryEquipeLocal.eqLocRappelAdd({
			equipeId,
			localId,
			rappelId: rappel.id,
		});

		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `creation d'un rappel`,
				changes: {
					table: `eqLocRappel`,
					after: {
						equipeId,
						localId,
						rappelId: rappel.id,
					},
				},
			},
		});

		await eqLocSuiviDatesUpdate({
			equipeId,
			localId,
		});
	},

	async rappelUpdate({
		evenementId,
		equipeId,
		localId,
		rappel,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		rappel: Rappel;
	}) {
		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `modification d'un rappel`,
				changes: {
					table: `eqLocRappel`,
					after: {
						equipeId,
						localId,
						rappelId: rappel.id,
					},
				},
			},
		});
	},

	async rappelCloture({
		evenementId,
		equipeId,
		localId,
		rappel,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		rappel: Rappel;
	}) {
		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `cloture d'un rappel`,
				changes: {
					table: `eqLocRappel`,
					after: {
						equipeId,
						localId,
						rappelId: rappel.id,
					},
				},
			},
		});
	},

	async rappelReouverture({
		evenementId,
		equipeId,
		localId,
		rappel,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		rappel: Rappel;
	}) {
		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `reouverture d'un rappel`,
				changes: {
					table: `eqLocRappel`,
					after: {
						equipeId,
						localId,
						rappelId: rappel.id,
					},
				},
			},
		});
	},

	async rappelRemove({
		evenementId,
		equipeId,
		localId,
		rappelId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		rappelId: Rappel['id'];
	}) {
		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `suppression d'un rappel`,
				changes: {
					table: `eqLocRappel`,
					before: {
						equipeId,
						localId,
						rappelId,
					},
				},
			},
		});

		await eqLocSuiviDatesUpdate({
			equipeId,
			localId,
		});
	},
};
