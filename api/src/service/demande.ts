import { Evenement, Demande, New, Brouillon } from '../db/types';
import { demande } from '../db/schema/suivi';
import * as dbQueryDemande from '../db/query/demande';
import * as dbQueryEvenement from '../db/query/evenement';

export const serviceDemande = {
	async demandeAdd({
		evenementId,
		partialDemande,
		brouillonId,
	}: {
		evenementId: Evenement['id'];
		partialDemande: New<typeof demande>;
		brouillonId: Brouillon['id'] | null;
	}) {
		const [demande] = await dbQueryDemande.demandeAdd({
			demande: partialDemande,
		});

		await dbQueryEvenement.actionDemandeAdd({
			evenementId,
			demandeId: demande.id,
			typeId: 'creation',
			diff: {
				message: `creation d'une demande`,
			},
		});

		if (brouillonId) {
			await dbQueryEvenement.actionBrouillonAdd({
				evenementId,
				brouillonId,
				typeId: 'transformation',
				diff: {
					message: `Transformation d'un brouillon en demande : ${demande.id}`,
				},
			});
		}

		return demande;
	},

	async demandeUpdate({
		evenementId,
		demandeId,
		partialDemande,
	}: {
		evenementId: Evenement['id'];
		demandeId: Demande['id'];
		partialDemande: Partial<Demande>;
	}) {
		const [demande] = await dbQueryDemande.demandeUpdate({
			demandeId,
			partialDemande,
		});

		await dbQueryEvenement.actionDemandeAdd({
			evenementId,
			demandeId,
			typeId: 'modification',
			diff: {
				message: 'modification de la demande',
				changes: {
					after: partialDemande,
				},
			},
		});

		return demande;
	},

	async demandeCloture({
		evenementId,
		demandeId,
		partialDemande: { clotureMotif },
	}: {
		evenementId: Evenement['id'];
		demandeId: Demande['id'];
		partialDemande: Pick<Demande, 'clotureMotif'>;
	}) {
		const [demande] = await dbQueryDemande.demandeUpdate({
			demandeId,
			partialDemande: {
				clotureMotif,
				clotureDate: new Date(),
			},
		});

		await dbQueryEvenement.actionDemandeAdd({
			evenementId,
			demandeId,
			typeId: 'fermeture',
			diff: {
				message: 'cloture de la demande',
				changes: {
					after: {
						motif: clotureMotif,
					},
				},
			},
		});

		return demande;
	},

	async demandeReouverture({
		evenementId,
		demandeId,
	}: {
		evenementId: Evenement['id'];
		demandeId: Demande['id'];
	}) {
		const [demande] = await dbQueryDemande.demandeUpdate({
			demandeId,
			partialDemande: {
				clotureDate: null,
			},
		});

		await dbQueryEvenement.actionDemandeAdd({
			evenementId,
			demandeId,
			typeId: 'reouverture',
			diff: {
				message: 'reouverture de la demande',
			},
		});

		return demande;
	},

	async demandeRemove({
		evenementId,
		demandeId,
	}: {
		evenementId: Evenement['id'];
		demandeId: Demande['id'];
	}) {
		await dbQueryDemande.demandeRemove({
			demandeId,
		});

		await dbQueryEvenement.actionDemandeAdd({
			evenementId,
			demandeId,
			typeId: 'suppression',
			diff: {
				message: 'suppression de la demande',
			},
		});

		return {
			id: demandeId,
		};
	},
};
