import { StatusCodes } from 'http-status-codes';
import ExcelJS from 'exceljs';

import { Readable } from 'stream';

import * as dbQueryEquipeEtablissement from '../db/query/equipe-etablissement';
import * as elasticQueryEquipeEtablissement from '../elastic/query/equipe-etablissement';
import { CtxBase } from '../middleware/auth';
import * as controllerEtablissements from '../controller/etablissements';
import { serviceExcel } from '../service/excel';

function rowToData(keys: unknown[], values: unknown[]) {
	return keys.reduce((result: Record<string, unknown>, h, i) => {
		result[h?.toString() ?? ''] = values[i];

		return result;
	}, {});
}

async function feuilleGetStream(excelFile: Buffer) {
	try {
		const workbookReader = new ExcelJS.stream.xlsx.WorkbookReader(Readable.from(excelFile), {});

		for await (const worksheetReader of workbookReader) {
			const { name } = worksheetReader as unknown as { name: string };

			return { stream: worksheetReader, sheet: name };
		}
	} catch (e: any) {
		console.logError(`[Siretisation] ${e}`);
	}
	return null;
}

function queryToParams(data: { recherche?: string } & Record<string, unknown>) {
	const { recherche, ...rest } = data;

	const params = controllerEtablissements.etablissementGetManyParamsBuild({
		query: {
			// retire tous les caractères spéciaux de la recherche
			recherche: recherche
				?.normalize('NFKD')
				.replace(/[\u0300-\u036f]/g, '')
				.replace(/[^A-Z0-9a-z']+/g, ' ')
				.trim()
				.toLowerCase(),
			...rest,
			geo: 'france',
			elementsParPage: '1',
		},
		equipeId: 0,
		devecoId: 0,
	});

	params.elementsParPage = 1;

	return params;
}

async function etabLookupMany(
	paramsBatch: dbQueryEquipeEtablissement.EtablissementsParams[],
	dataBatch: Record<string, unknown>[]
) {
	try {
		const results = await elasticQueryEquipeEtablissement.eqEtabUnsortedGetMany(paramsBatch, [
			'etablissement_id',
			'etablissement_noms',
			'etablissement_siege',
			'entreprise_noms',
			'adresse_numero',
			'adresse_voie_nom',
			'territoire_commune_id',
		]);

		return results.map(
			([etab], i: number) =>
				({
					...dataBatch[i],
					...(etab
						? {
								rSiret: etab.etablissement_id,
								rNom: etab.etablissement_noms.filter(Boolean).join(', '),
								rNomEntreprise: etab.entreprise_noms.filter(Boolean).join(', '),
								rSiege: etab.etablissement_siege,
								rNumero: etab.adresse_numero,
								rVoieNom: etab.adresse_voie_nom,
								rCommuneId: etab.territoire_commune_id,
							}
						: null),
				}) as unknown as Iterable<any>
		);
	} catch (e) {
		console.logError(`[Siretisation] erreur: ${e}`);
		return [];
	}
}

async function* etablissementParamsGetCursor(
	stream: ExcelJS.stream.xlsx.WorksheetReader,
	sheet: string
) {
	let i = 0;
	let rows = 0;

	const header: unknown[] = [];

	const dataBatch: Record<string, unknown>[] = [];
	const paramsBatch: dbQueryEquipeEtablissement.EtablissementsParams[] = [];

	const MAX_BATCH = 1000;

	for await (const row of stream) {
		i += 1;

		if (i === 1 && Array.isArray(row.values)) {
			header.push(...row.values.slice(1));
		}

		if (i < 2 || !Array.isArray(row.values)) {
			continue;
		}

		// TODO vérifier comme récupérer du texte brut même dans les autres cas de cellule
		const values = row.values
			.slice(1)
			/* eslint-disable-next-line @typescript-eslint/no-base-to-string */
			.map((s) => (typeof s === 'string' ? s.trim() : (s?.toString() ?? '')));

		if (values.filter((v) => v).length === 0) {
			continue;
		}

		rows += 1;

		const data = rowToData(header, values);

		const params = queryToParams(data);

		dataBatch.push(data);
		paramsBatch.push(params);

		if (paramsBatch.length >= MAX_BATCH) {
			yield { params: paramsBatch, data: dataBatch };

			dataBatch.length = 0;
			paramsBatch.length = 0;
		}
	}

	if (paramsBatch.length) {
		yield { params: paramsBatch, data: dataBatch };
	}

	if (rows === 0) {
		console.logInfo(`[Siretisation] feuille "${sheet}": Feuille vide, ignorée`);
	}
}

export async function siretisation({ ctx, excelFile }: { ctx: CtxBase; excelFile: Buffer }) {
	console.logInfo(`[Siretisation] ...`);

	const feuilleStream = await feuilleGetStream(excelFile);
	if (!feuilleStream) {
		ctx.throw('Aucune feuille trouvée dans le fichier');
		return;
	}
	const { stream, sheet } = feuilleStream;

	async function* generator(stream: ExcelJS.stream.xlsx.WorksheetReader) {
		const etablissementCursor = etablissementParamsGetCursor(stream, sheet);

		let total = 0;

		for await (const { params, data } of etablissementCursor) {
			const results = await etabLookupMany(params, data);

			total += data.length;

			if (total % 1000 === 0) {
				console.logInfo('[Siretisation] ... 1000 lignes, total:', total);
			}

			yield results;
		}
	}

	try {
		ctx.status = StatusCodes.OK;

		await serviceExcel.etabLookupStreamMany({
			etablissementCursor: generator(stream),
			output: ctx.res,
		});
	} catch (e) {
		console.logError(e);
	}
}
