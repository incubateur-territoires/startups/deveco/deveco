import {
	devecoGetManyNeverConnectedAfter10Days,
	devecoGetManyNeverConnectedAfter30Days,
} from '../db/query/equipe';
import { serviceAdmin } from '../service/admin';
import { smtpCheck } from '../lib/email';

export const serviceDeveco = {
	async relance10Jours(): Promise<void> {
		if (!smtpCheck()) {
			return;
		}

		const devecos = await devecoGetManyNeverConnectedAfter10Days();

		for (const deveco of devecos) {
			if (!deveco.compte.actif) {
				continue;
			}

			console.logInfo(`Envoi d'un email de relance à 10 jours à ${deveco.compte.email}`);
			await serviceAdmin.emailRelanceDixJoursSend(deveco.compte);
		}
	},

	async relance30Jours(): Promise<void> {
		if (!smtpCheck()) {
			return;
		}

		const devecos = await devecoGetManyNeverConnectedAfter30Days();

		for (const deveco of devecos) {
			if (!deveco.compte.actif) {
				continue;
			}

			console.logInfo(`Envoi d'un email de relance à 30 jours à ${deveco.compte.email}`);
			await serviceAdmin.emailRelanceTrenteJoursSend(deveco.compte);
		}
	},
};
