import { New, Equipe, Contact, Evenement } from '../db/types';
import * as schemaContact from '../db/schema/contact';
import * as dbQueryEvenement from '../db/query/evenement';
import * as dbQueryContact from '../db/query/contact';
import * as dbQueryEquipeEtablissement from '../db/query/equipe-etablissement';
import * as dbQueryEquipeLocal from '../db/query/equipe-local';

export const serviceContact = {
	async contactAdd({
		evenementId,
		equipeId,
		contact,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		contact: New<typeof schemaContact.contact>;
	}) {
		const [newContact] = await dbQueryContact.contactAdd({
			contact: {
				...contact,
				equipeId,
			},
		});

		await dbQueryEvenement.actionContactAdd({
			evenementId,
			contactId: newContact.id,
			typeId: 'creation',
		});

		return newContact;
	},

	async contactUpdate({
		evenementId,
		contactId,
		partialContact,
		oldContact,
	}: {
		evenementId: Evenement['id'];
		contactId: Contact['id'];
		partialContact: Partial<Contact>;
		oldContact: Contact;
	}) {
		const [contact] = await dbQueryContact.contactUpdate({
			contactId,
			contact: partialContact,
		});

		await dbQueryEvenement.actionContactAdd({
			evenementId,
			contactId,
			typeId: 'modification',
			diff: {
				message: `mise à jour des infos`,
				changes: {
					before: oldContact,
					after: contact,
					table: `contact`,
				},
			},
		});

		return contact;
	},

	async contactRemove({
		equipeId,
		evenementId,
		contactId,
	}: {
		equipeId: Equipe['id'];
		evenementId: Evenement['id'];
		contactId: Contact['id'];
	}) {
		const contact = await dbQueryContact.contactWithRelationsIdGet({ contactId });
		if (!contact) {
			return;
		}

		await dbQueryEvenement.actionContactAdd({
			evenementId,
			contactId,
			typeId: 'suppression',
			diff: {
				message: `suppression du contact`,
				changes: {
					table: `contact`,
				},
			},
		});

		if (contact.etabCs?.length) {
			const etablissementIds = contact.etabCs.map((e) => e.etablissementId);

			await dbQueryEquipeEtablissement.eqEtabFromManyContactRemove({
				equipeId,
				etablissementIds,
				contactId,
			});
		}

		if (contact.localCs?.length) {
			const localIds = contact.localCs.map((e) => e.localId);

			await dbQueryEquipeLocal.eqLocContactRemoveMany({
				localIds,
				contactId,
			});
		}

		// Si le contact est lié à une création d'établissement
		// alors on ne le supprime pas
		if (contact.creaCs?.length > 0) {
			return;
		}

		await dbQueryContact.contactRemove({
			contactId,
		});
	},
};
