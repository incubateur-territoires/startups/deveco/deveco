import { addDays } from 'date-fns';

import { dateToString } from '../lib/dateUtils';
import { wait } from '../lib/utils';
import { TacheFrequence } from '../db/types';
import * as dbQueryAdmin from '../db/query/admin';
import * as dbQueryEvenement from '../db/query/evenement';
import { serviceSirene } from '../service/sirene';
import { serviceElastic } from '../service/elastic';
import { serviceGeocode } from '../service/geocode';

const taches = [
	'api-sirene-etablissement',
	'api-sirene-unite-legale',
	'api-sirene-unite-legale-by-updated-etablissement',
	'api-sirene-lien-succession',
	'entreprise',
	'etablissement',
	'etablissement-lien',
	'equipe-etablissement',
	'api-sirene-lien-succession-check',
	'entreprise-by-sirens-fix',
	'geocode-etablissement-dry-run',
	'geocode-etablissement-fix',
	'elasticsearch',
];

const tachesMiseAJourAt = [
	'entreprise',
	'etablissement',
	'equipe-etablissement',
	'etablissement-lien',
	'api-sirene-lien-succession-check',
	'geocode-etablissement-fix',
	'elasticsearch',
];

export const serviceApiSirene = {
	taches,
	tachesMiseAJourAt,

	async updateBySirens({ sirens, frequence }: { sirens: string[]; frequence: TacheFrequence }) {
		const miseAJourAt = new Date();

		const total = sirens.length;
		let count = 0;

		// il y a parfois plusieurs dizaine d'établissements pour un siren
		// comme la requête à l'API sirene limite le nombre d'éléments retournés à 1000 (cf `amount`),
		// on ne veut pas augmenter la valeur du batch, pour ne pas récupérer plus de 1000 établissements d'un coup
		const batch = 1000;

		while (sirens.length) {
			const batchSirens = sirens.splice(0, batch);

			console.logInfo(
				`[serviceApiSirene.updateBySirens] - mise à jour des entreprises, ${count} / ${total}, sirens : ${batchSirens.join(
					', '
				)}`
			);

			await serviceSirene.entrepriseUpsertManyBySiren({
				tacheNom: `api-sirene-unite-legale-update-by-siren-${frequence}-${count}`,
				sirens: batchSirens,
			});

			await serviceSirene.etablissementUpsertManyBySiren({
				tacheNom: `api-sirene-etablissement-update-by-siren-${frequence}-${count}`,
				sirens: batchSirens,
			});

			count += batchSirens.length;

			// on attend une seconde pour éviter de créer la même tâche à la même seconde
			await wait(1000);
		}

		await serviceGeocode.etablissementGeocodeUpsertMany({
			tacheNom: `etablissement-geocode-update-by-siren-${frequence}`,
			miseAJourAt,
		});

		await serviceElastic.indexUpdateByMiseAJourAt({
			tacheNom: `etablissement-index-update-by-siren-${frequence}`,
			miseAJourAtDebut: miseAJourAt,
			resetIndexes: true,
		});
	},

	async etablissementUpdateByQuery({
		q,
		frequence,
		miseAJourAt,
		skipSirene,
	}: {
		q: string;
		frequence: TacheFrequence;
		miseAJourAt: Date;
		skipSirene?: boolean | null;
	}) {
		if (!skipSirene) {
			await serviceSirene.etablissementUpsertMany({
				tacheNom: `api-sirene-etablissement-update-by-query-${frequence}`,
				extraParams: [q],
			});
		}

		await serviceSirene.uniteLegaleUpsertManyByUpdatedEtablissements({
			tacheNom: `api-sirene-unite-legale-update-by-updated-etablissement-by-query-${frequence}`,
			miseAJourAt,
		});

		const compteSirene = await dbQueryAdmin.compteGetByNomAndType({ nom: 'sirene', typeId: 'api' });
		if (!compteSirene) {
			throw new Error(`Pas de compte pour l'API Sirene`);
		}

		await dbQueryEvenement.evenementAdd({
			compteId: compteSirene.id,
			typeId: `sirene_update_sirens_${frequence}`,
		});

		await serviceGeocode.etablissementGeocodeUpsertMany({
			tacheNom: `etablissement-geocode-update-by-query-${frequence}`,
			miseAJourAt,
		});

		await serviceElastic.indexUpdateByMiseAJourAt({
			tacheNom: `etablissement-index-update-by-query-${frequence}`,
			miseAJourAtDebut: miseAJourAt,
			resetIndexes: true,
		});
	},

	async updateByCursor({
		frequence,
		debutDate,
		miseAJourAt,
		finDate,
		debutJours,
		finJours = 0,
		tache,
		depuis,
		skipSirene,
	}: {
		frequence: TacheFrequence;
		debutJours: number;
		miseAJourAt: Date;
		finJours?: number;
		debutDate?: string;
		finDate?: string;
		tache?: string;
		depuis?: number;
		skipSirene?: boolean | null;
	}) {
		const sireneUpdateTacheNom = `api-sirene-update-${frequence}`;
		console.logInfo(`[Cron] ${sireneUpdateTacheNom} : début`);

		if (tache && !taches.includes(tache)) {
			console.logError(`Tâche inconnue : ${tache}`);
			return;
		}

		if (tache && tachesMiseAJourAt.includes(tache) && !depuis) {
			console.logError(`Tâche nécessitant le paramètre "depuis" : ${tache}`);
			return;
		}

		if (debutDate && debutJours) {
			console.logError(`N'utilisez pas debutDate et debutJours en même temps`);
			return;
		}

		if (finDate && finJours) {
			console.logError(`N'utilisez pas finDate et finJours en même temps`);
			return;
		}

		const compteSirene = await dbQueryAdmin.compteGetByNomAndType({ nom: 'sirene', typeId: 'api' });
		if (!compteSirene) {
			throw new Error(`Pas de compte pour l'API Sirene`);
		}

		await dbQueryEvenement.evenementAdd({
			compteId: compteSirene.id,
			typeId: `sirene_update_sirens_${frequence}`,
		});

		try {
			const startDate = debutDate ? new Date(debutDate) : addDays(miseAJourAt, -1 - debutJours);

			const endDate = finDate ? new Date(finDate) : addDays(miseAJourAt, -1 - finJours);

			if (!skipSirene) {
				console.logInfo(
					`[Cron] ${sireneUpdateTacheNom} : début (${dateToString(startDate)}), fin (${dateToString(
						endDate
					)}), mise à jour (${dateToString(miseAJourAt)})`
				);

				// met à jour les sources établissement et période d'établissement
				// depuis l'api sirene

				if (!tache || tache === 'api-sirene-etablissement') {
					const sireneEtablissementTacheNom = `api-sirene-etablissement-update-${frequence}`;
					console.logInfo(`[Cron] ${sireneEtablissementTacheNom} : début`);

					await serviceSirene.etablissementUpsertMany({
						tacheNom: sireneEtablissementTacheNom,
						startDate,
						endDate,
					});

					console.logInfo(`[Cron] ${sireneEtablissementTacheNom} : fin`);
				}

				// met à jour les sources unité légale et période d'unité légale
				// depuis l'api sirene

				if (!tache || tache === 'api-sirene-unite-legale') {
					const sireneUniteLegaleTacheNom = `api-sirene-unite-legale-update-${frequence}`;
					console.logInfo(`[Cron] ${sireneUniteLegaleTacheNom} : début`);

					await serviceSirene.uniteLegaleUpsertMany({
						tacheNom: sireneUniteLegaleTacheNom,
						startDate,
						endDate,
					});

					console.logInfo(`[Cron] ${sireneUniteLegaleTacheNom} : fin`);
				}

				// met à jour les sources unité légale et période d'unité légale
				// des établissements mis à jour
				// depuis l'api sirene

				if (!tache || tache === 'api-sirene-unite-legale-by-updated-etablissement') {
					const sireneUniteLegaleTacheNom = `api-sirene-unite-legale-update-by-updated-etablissement-${frequence}`;
					console.logInfo(`[Cron] ${sireneUniteLegaleTacheNom} : début`);

					await serviceSirene.uniteLegaleUpsertManyByUpdatedEtablissements({
						tacheNom: sireneUniteLegaleTacheNom,
						miseAJourAt,
					});

					console.logInfo(`[Cron] ${sireneUniteLegaleTacheNom} : fin`);
				}

				if (!tache || tache === 'api-sirene-lien-succession') {
					const sireneLienSuccessionTacheNom = `api-sirene-lien-succession-update-${frequence}`;
					console.logInfo(`[Cron] ${sireneLienSuccessionTacheNom} : début`);

					await serviceSirene.lienSuccessionUpsertManyByDate({
						tacheNom: sireneLienSuccessionTacheNom,
						startDate,
						endDate,
					});

					console.logInfo(`[Cron] ${sireneLienSuccessionTacheNom} : fin`);
				}
			} else {
				console.logInfo(
					`[Cron] ${sireneUpdateTacheNom} SKIPPED : début (${dateToString(
						startDate
					)}), fin (${dateToString(endDate)}), mise à jour (${dateToString(miseAJourAt)})`
				);
			}

			// cherche les établissements manquants dans les liens de successions

			if (!tache || tache === 'api-sirene-lien-succession-check') {
				const sireneLienSuccessionCheckTacheNom = `api-sirene-lien-succession-check-${frequence}`;
				console.logInfo(`[Cron] ${sireneLienSuccessionCheckTacheNom} : début`);

				await serviceSirene.lienSuccessionCheckMany({
					tacheNom: sireneLienSuccessionCheckTacheNom,
					miseAJourAt,
				});

				console.logInfo(`[Cron] ${sireneLienSuccessionCheckTacheNom} : fin`);
			}

			// fin
			console.logInfo(`[Cron] ${sireneUpdateTacheNom} : fin`);
		} catch (e: any) {
			console.logError(
				`[Cron] ${sireneUpdateTacheNom} : Impossible de mettre à jour, erreur : ${e.message}`
			);
		}
	},
};
