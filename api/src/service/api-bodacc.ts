import fetch from 'node-fetch';
import gunzip from 'gunzip-maybe';
import tar from 'tar-stream';
import { XMLParser } from 'fast-xml-parser';

import { Readable } from 'stream';
import path from 'path';

import { BodaccProcedureCollective } from '../db/types';
import * as dbQueryEtablissement from '../db/query/etablissement';
import { serviceElastic } from '../service/elastic';

const BASE_URL = 'https://echanges.dila.gouv.fr/OPENDATA/BODACC/FluxAnneeCourante/';

const parser = new XMLParser();

async function streamFetch(urlPath = '') {
	const url = path.join(BASE_URL, urlPath);

	console.logInfo('[bodaccProcedureCollectiveUpdate] downloading', url);

	const response = await fetch(url);

	return response.body;
}

function streamToString(stream: Readable | NodeJS.ReadableStream): Promise<string> {
	return new Promise((resolve, reject) => {
		const chunks: Buffer[] = [];

		stream
			.on('error', reject)
			.on('data', (chunk: Buffer) => chunks.push(Buffer.from(chunk)))
			.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')));
	});
}

async function archiveExtract(archiveStream: NodeJS.ReadableStream | null): Promise<string> {
	if (!archiveStream) {
		return '';
	}

	return new Promise((resolve, reject) => {
		const gzipStream = gunzip();
		const extractStream = tar.extract();

		let fileContent = '';

		extractStream
			.on('error', reject)
			.on(
				'entry',
				(_header, entry, callback) =>
					void (async () => {
						fileContent = await streamToString(entry);

						callback();
					})()
			)
			.on('finish', () => {
				resolve(fileContent);
			});

		archiveStream.pipe(gzipStream).pipe(extractStream);
	});
}

// const annonce = {
// 	typeAnnonce: { creation: '' },
// 	nojo: 401103880,
// 	numeroAnnonce: 1801,
// 	numeroDepartement: 4,
// 	tribunal: 'TRIBUNAL DE COMMERCE DE MANOSQUE',
// 	identifiantClient: 401103880,
// 	personnePhysique: { denominationEIRL: 'M. SALAS Antonio (EI)' },
// 	nonInscrit: 'RCS non inscrit',
// 	inscriptionRM: {
// 		numeroIdentificationRM: '433 104 189',
// 		codeRM: 'RM',
// 		numeroDepartement: 4,
// 	},
// 	activite: "Travaux d'installation électrique dans tous locaux",
// 	adresse: {
// 		france: {
// 			numeroVoie: 5,
// 			typeVoie: 'chemin',
// 			nomVoie: 'des Augiers',
// 			codePostal: 4000,
// 			ville: 'Digne-les-Bains',
// 		},
// 	},
// 	jugement: {
// 		famille: "Jugement d'ouverture",
// 		nature: "Jugement d'ouverture de liquidation judiciaire",
// 		date: '2024-03-05',
// 		complementJugement:
// 			"Jugement prononçant la liquidation judiciaire, en application du III de l'article L. 681-2 du code de commerce, , date de cessation des paiements le 04 Mai 2023, désignant liquidateur Me Lageat Anne Scp JP Louis & A. Lageat - 264 rue Berthelot - 04100 Manosque . Les créances sont à adresser, dans les deux mois de la présente publication, auprès du liquidateur ou sur le portail électronique prévu par les articles L. 814-2 et L. 814-13 du code de commerce.",
// 	},
// };

interface BodaccAnnonce {
	typeAnnonce: { creation: string };
	nojo: number;
	numeroAnnonce: number;
	numeroImmatriculation: {
		numeroIdentificationRCS: string;
	};
	numeroDepartement: number;
	tribunal: string;
	identifiantClient: number;
	personnePhysique: { denominationEIRL: string };
	nonInscrit: string;
	inscriptionRM: {
		numeroIdentificationRM: string;
		codeRM: string;
		numeroDepartement: number;
	};
	activite: string;
	adresse: {
		france: {
			numeroVoie: number;
			typeVoie: string;
			nomVoie: string;
			codePostal: number;
			ville: string;
		};
	};
	jugement: {
		famille: string;
		nature: string;
		date: string;
		complementJugement: string;
	};
}

function annonceFormat(annonce: BodaccAnnonce): BodaccProcedureCollective | undefined {
	if (!annonce.numeroImmatriculation) {
		return;
	}

	const {
		numeroAnnonce,
		numeroImmatriculation: { numeroIdentificationRCS },
		jugement,
	} = annonce;

	if (!numeroAnnonce || !numeroIdentificationRCS || !jugement) {
		return;
	}

	const numero = Number(numeroAnnonce);

	const { date } = jugement;
	const { famille, nature } = jugement;

	const siren = numeroIdentificationRCS.replace(/ /g, '');

	if (!siren || siren === '000000000') {
		return;
	}

	const formattedDate = date ? new Date(date) : null;

	return {
		numero,
		siren,
		famille,
		nature,
		date: formattedDate,
		miseAJourAt: new Date(),
	};
}

function xmlToProcedureCollectiveParse(content: string) {
	const xml = parser.parse(content);

	const ann = xml.PCL_REDIFF.annonces.annonce;
	const annonces = Array.isArray(ann) ? ann : [ann];

	const results = [];

	for (const annonce of annonces) {
		try {
			const result = annonceFormat(annonce);

			if (result) {
				results.push(result);
			}
		} catch (e) {
			console.logError(e, annonce);

			process.exit(1);
		}
	}

	return results;
}

async function bodaccProcedureCollectiveUpdate() {
	try {
		const fileListStream = await streamFetch('');
		if (!fileListStream) {
			return;
		}

		const fileListString = await streamToString(fileListStream);
		if (!fileListString) {
			return;
		}

		const filesMatched = fileListString.match(/href="(PCL[^"]+)"/g);
		if (!filesMatched) {
			return;
		}

		const files = filesMatched.map((l) => l.split('"')[1]);

		const annee = new Date().getFullYear();

		const importNumerosRaw =
			await dbQueryEtablissement.bodaccProcedureCollectiveImportNumeroGetMany({
				annee,
			});

		const importNumeros = importNumerosRaw.map((i) => i.numero);

		const importNumeroDernier = importNumeros.slice(-1).pop() ?? 0;

		console.logInfo('[bodaccProcedureCollectiveUpdate]', {
			importNumeros: importNumeros.join(', '),
			importNumeroDernier,
		});

		for (const file of files) {
			const match = file.match(/(\d{4}).taz$/);
			if (!match || !match[1]) {
				continue;
			}

			const numero = Number(match[1].replace(/^0+/, ''));

			console.logInfo('[bodaccProcedureCollectiveUpdate]', {
				file,
				numero,
				importNumeroDernier,
			});

			// le numéro de fichier existe déjà en base, on l'ignore
			if (importNumeros.indexOf(numero) !== -1) {
				continue;
			}

			let xmlString: string;

			try {
				const archiveStream = await streamFetch(file);
				if (!archiveStream) {
					continue;
				}

				xmlString = await archiveExtract(archiveStream);
				if (!xmlString) {
					continue;
				}
			} catch (e) {
				console.logError(e);
				continue;
			}

			const bodaccProcedureCollectives = xmlToProcedureCollectiveParse(xmlString);

			await dbQueryEtablissement.bodaccProcedureCollectiveAddMany({
				bodaccProcedureCollectives,
			});

			await dbQueryEtablissement.bodaccProcedureCollectiveImportAdd({
				bodaccProcedureCollectiveImport: {
					annee,
					numero,
					miseAJourAt: new Date(),
				},
			});
		}
	} catch (e) {
		console.logError(e);
	}
}

export const serviceApiBodacc = {
	async update() {
		const bodaccProcedurcollectiveTacheNom = `bodacc-procedure-collective-update`;

		console.logInfo(`[Cron] ${bodaccProcedurcollectiveTacheNom} : début`);

		const miseAJourAt = new Date();

		await bodaccProcedureCollectiveUpdate();

		await serviceElastic.etablissementIndexProcedureCollectiveUpdate({
			tacheNom: bodaccProcedurcollectiveTacheNom,
			miseAJourAt,
		});

		console.logInfo(`[Cron] ${bodaccProcedurcollectiveTacheNom} : fin`);
	},
};
