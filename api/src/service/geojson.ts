import { EntiteInfos, PersoInfos, etablissementInfosFormat } from './_format-export';

import { etiquettesByTypeBuild } from '../controller/_format/equipe';
import {
	etablissementNomBuild,
	etablissementAdresseBuild,
} from '../controller/_format/etablissements';
import {
	EtablissementsParams,
	etablissementWithEqEtabWithRelationsForGeojsonGetMany,
} from '../db/query/equipe-etablissement';
import { NotUndefined, QueryResult, Etablissement } from '../db/types';
import * as elasticEtablissement from '../elastic/query/equipe-etablissement';
import * as elasticLocal from '../elastic/query/equipe-local';
import * as dbQueryEquipeEtablissement from '../db/query/equipe-etablissement';
import * as dbQueryEquipeLocal from '../db/query/equipe-local';
import { localNomBuild } from '../controller/_format/local';

function geojsonEtablissementFeatureFormat(
	etab: NotUndefined<
		QueryResult<
			typeof dbQueryEquipeEtablissement.etablissementWithEqEtabWithRelationsForGeojsonGetMany
		>[0]
	>
) {
	const adresse = etablissementAdresseBuild(etab);

	const entiteInfos: EntiteInfos<Etablissement> = {
		siret: etab.siret,
		numero: adresse.numero ?? null,
		voieNom: adresse.voieNom ?? null,
		codePostal: adresse.codePostal ?? null,
		commune: adresse.commune?.nom ?? null,
		codeNaf: etab.nafType?.id ?? null,
		libelleNaf: etab.nafType?.parent?.nom ?? '',
	};

	const coordinates = adresse.geolocalisation;

	const etiquettes = etab.etiquettes ?? [];
	const { activites, localisations, motsCles } = etiquettesByTypeBuild(
		etiquettes.map((ete) => ete.etiquette)
	);

	const persoInfos: PersoInfos<Etablissement> = {
		activitesReelles: activites.map((e) => e.nom).join(', '),
		zonesGeographiques: localisations.map((e) => e.nom).join(', '),
		motsCles: motsCles.map((e) => e.nom).join(', '),
		commentaires: etab.contributions[0]?.description ?? '',
	};

	const etablissementInfos = etablissementInfosFormat(etab);

	return JSON.stringify(
		{
			type: 'Feature',
			properties: {
				nom: etablissementNomBuild(etab),
				...persoInfos,
				...entiteInfos,
				...etablissementInfos,
			},
			geometry: {
				type: 'Point',
				coordinates: coordinates ?? [],
			},
		},
		null,
		0
	);
}

function geojsonLocalFeatureFormat(
	local: NotUndefined<QueryResult<typeof dbQueryEquipeLocal.localWithRelationsForGeoJsonGetMany>[0]>
) {
	const entiteInfos = {
		invariant: local.invariant,
		numero: local.adresse.numero ?? null,
		voie: local.adresse.voieNom ?? null,
		commune: local.adresse.commune?.nom ?? null,
	};

	const coordinates = local.adresse.geolocalisation;

	return JSON.stringify(
		{
			type: 'Feature',
			properties: {
				nom: localNomBuild(local),
				...entiteInfos,
			},
			geometry: coordinates
				? {
						type: 'Point',
						coordinates,
					}
				: null,
		},
		null,
		0
	);
}

function geoJsonFeaturesStreamOptionsBuild(
	format: 'geojson' | 'ndgeojson',
	entite: 'etablissements' | 'locaux'
) {
	const streamOpts =
		format === 'geojson'
			? {
					separator: ',',
					dataPrefix: `{ "type": "FeatureCollection", "features": [`,
					dataSuffix: '] }',
				}
			: {
					separator: '\n',
				};
	const timestamp = Date.now();
	const extension = format === 'geojson' ? 'geojson' : 'ndjson';
	const filename = `deveco-export-${entite}-${timestamp}.${extension}`;

	const fileOptions = {
		filename,
		mimetype: `application/${format === 'geojson' ? 'geo+json' : 'x-ndjson'}`,
	};

	return {
		streamOpts,
		fileOptions,
	};
}

async function* dataGeneratorPrefixAdd({
	dataGenerator,
	streamOpts,
}: {
	dataGenerator: AsyncGenerator;
	streamOpts?: {
		separator?: string;
		dataPrefix?: string;
		dataSuffix?: string | null;
		cleanup?: () => void;
	};
}) {
	if (streamOpts?.dataPrefix) {
		yield streamOpts.dataPrefix;
	}

	let i = 0;

	for await (const data of dataGenerator) {
		if (streamOpts?.separator && i > 0) {
			yield streamOpts.separator;
		}

		yield data;

		i++;
	}

	if (typeof streamOpts?.dataSuffix !== 'undefined') {
		yield streamOpts.dataSuffix;
	}

	streamOpts?.cleanup?.();
}

const BUFFER_SIZE = 50;

async function* etablissementFeaturesGeneratorBuild(params: EtablissementsParams) {
	const etablissementIdCursor = elasticEtablissement.eqEtabIdGetCursor(params);

	const etablissementIdsBuffers: string[] = [];

	// TODO Si les params demandent "Toute la France" cela ne marchera pas car
	// on fait la requête en partant des EquipeEtablissements
	async function* process() {
		const etablissementIds = etablissementIdsBuffers.splice(0, etablissementIdsBuffers.length);

		const etablissements = etablissementIds.length
			? await etablissementWithEqEtabWithRelationsForGeojsonGetMany({
					etablissementIds,
					equipeId: params.equipeId,
				})
			: [];

		for (const etablissement of etablissements) {
			yield geojsonEtablissementFeatureFormat(etablissement);
		}
	}

	for await (const idRaws of etablissementIdCursor) {
		etablissementIdsBuffers.push(...idRaws.map((r) => r.etablissement_id));

		if (etablissementIdsBuffers.length >= BUFFER_SIZE) {
			for await (const eqEtabFeatureString of process()) {
				yield eqEtabFeatureString;
			}
		}
	}

	if (etablissementIdsBuffers.length > 0) {
		for await (const eqEtabFeatureString of process()) {
			yield eqEtabFeatureString;
		}
	}
}

async function* localFeaturesGeneratorBuild(params: dbQueryEquipeLocal.EquipeLocalParams) {
	const localIdCursor = elasticLocal.eqLocIdGetCursor(params);

	const localIdsBuffers: string[] = [];

	async function* process() {
		const localIds = localIdsBuffers.splice(0, localIdsBuffers.length);

		const locaux = localIds.length
			? await dbQueryEquipeLocal.localWithRelationsForGeoJsonGetMany({
					localIds,
					equipeId: params.equipeId,
				})
			: [];

		for (const local of locaux) {
			yield geojsonLocalFeatureFormat(local);
		}
	}

	for await (const idRaws of localIdCursor) {
		localIdsBuffers.push(...idRaws.map((r) => r.local_id));

		if (localIdsBuffers.length >= BUFFER_SIZE) {
			for await (const eqLocalFeatureString of process()) {
				yield eqLocalFeatureString;
			}
		}
	}

	if (localIdsBuffers.length > 0) {
		for await (const eqLocalFeatureString of process()) {
			yield eqLocalFeatureString;
		}
	}
}

export const serviceGeojson = {
	etablissementStreamBuild({
		params,
		format,
	}: {
		params: EtablissementsParams;
		format: 'geojson' | 'ndgeojson';
	}) {
		const dataGenerator = etablissementFeaturesGeneratorBuild(params);

		const { streamOpts, fileOptions } = geoJsonFeaturesStreamOptionsBuild(format, 'etablissements');

		const dataGeneratorPrefixed = dataGeneratorPrefixAdd({ dataGenerator, streamOpts });

		return { fileOptions, dataGenerator: dataGeneratorPrefixed };
	},

	localStreamBuild({
		params,
		format,
	}: {
		params: dbQueryEquipeLocal.EquipeLocalParams;
		format: 'geojson' | 'ndgeojson';
	}) {
		const dataGenerator = localFeaturesGeneratorBuild(params);

		const { streamOpts, fileOptions } = geoJsonFeaturesStreamOptionsBuild(format, 'locaux');

		const dataGeneratorPrefixed = dataGeneratorPrefixAdd({ dataGenerator, streamOpts });

		return { fileOptions, dataGenerator: dataGeneratorPrefixed };
	},
};
