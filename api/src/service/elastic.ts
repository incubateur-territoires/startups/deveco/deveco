import { sql, SQL } from 'drizzle-orm';
import * as wkx from 'wkx';

import { readFileSync } from 'fs';
import * as path from 'path';

import { wait } from '../lib/utils';
import { Tache } from '../db/types';
import db from '../db';
import * as dbQueryAdmin from '../db/query/admin';
import * as dbQueryTerritoire from '../db/query/territoire';
import elastic from '../elastic';
import * as elasticQueryQpv from '../elastic/query/qpv';
import * as elasticQueryZonage from '../elastic/query/zonage';
import * as elasticQueryEtablissement from '../elastic/query/etablissement';
import { mappings as elasticQueryEquipeEtablissementMappings } from '../elastic/query/equipe-etablissement';
import * as elasticQueryLocal from '../elastic/query/local';

const geometryToPoint = (geometry: string | null) => {
	if (!geometry) {
		return null;
	}

	if (typeof geometry === 'object') {
		return wkx.Geometry.parseGeoJSON(geometry).toGeoJSON();
	}

	const buff = Buffer.from(geometry, 'hex');

	return wkx.Geometry.parse(buff).toGeoJSON();
};

const indexUpdateMapping = async (index: string, mapping: any) => {
	try {
		await elastic.client.indices.putMapping({
			index,
			body: mapping,
		});
	} catch (e) {
		console.logError('impossible de pousser le mapping', e);
	}
};

const indexInit = async (index: string, mapping: any) => {
	try {
		await elastic.client.indices.delete({
			index,
		});
	} catch (e) {
		console.logError('index déjà supprimé ou autre erreur', e);
	}

	try {
		await elastic.client.indices.create({
			index,
		});
	} catch (e) {
		console.logError("impossible de créer l'index", e);
	}

	await indexUpdateMapping(index, mapping);
};

function executeRaw(query: string) {
	return db.execute(sql.raw(query));
}

function executeProcedure(nom: string) {
	return executeRaw(
		readFileSync(path.join(__dirname, `../db/query/sql/${nom}`), { encoding: 'utf8' })
	);
}
export async function eqEtabPersoIndexCreate() {
	await executeProcedure(`etablissement-index/perso-create-procedure.sql`);
	return executeRaw(`CALL temp_index_etab_perso_etab_create()`);
}

export async function eqEtabPersoIndexCleanup() {
	await executeProcedure(`etablissement-index/perso-cleanup-procedure.sql`);
	return executeRaw(`CALL temp_index_etab_perso_etab_cleanup()`);
}

export async function eqEtabPersoIndexUpdate(
	miseAJourAtDebut: Date | null,
	miseAJourAtFin: Date | null
) {
	await executeProcedure(`etablissement-index/perso-update-procedure.sql`);

	const dateDebutFormatted = miseAJourAtDebut
		? `'${miseAJourAtDebut.toISOString().slice(0, 10)}'`
		: 'NULL';
	const dateFinFormatted = miseAJourAtFin
		? `'${miseAJourAtFin.toISOString().slice(0, 10)}'`
		: 'NULL';

	return executeRaw(
		`CALL temp_index_etab_perso_etab_update(${dateDebutFormatted}, ${dateFinFormatted})`
	);
}

export async function eqEtabPublicIndexCreate() {
	await executeProcedure(`etablissement-index/public-create-procedure.sql`);
	return executeRaw(`CALL temp_index_etab_public_etab_create()`);
}

export async function eqEtabPublicIndexCleanup() {
	await executeProcedure(`etablissement-index/public-cleanup-procedure.sql`);
	return executeRaw(`CALL temp_index_etab_public_etab_cleanup()`);
}

export async function eqEtabPublicIndexUpdate(
	miseAJourAtDebut: Date | null,
	miseAJourAtFin: Date | null
) {
	await executeProcedure(`etablissement-index/public-update-procedure.sql`);

	const dateDebutFormatted = miseAJourAtDebut
		? `'${miseAJourAtDebut.toISOString().slice(0, 10)}'`
		: 'NULL';
	const dateFinFormatted = miseAJourAtFin
		? `'${miseAJourAtFin.toISOString().slice(0, 10)}'`
		: 'NULL';

	return executeRaw(
		`CALL temp_index_etab_public_etab_update(${dateDebutFormatted}, ${dateFinFormatted})`
	);
}

export async function eqLocPersoIndexCreate() {
	await executeProcedure('local-index/perso-create-procedure.sql');
	return executeRaw(`CALL temp_index_local_perso_local_create()`);
}

export async function eqLocPersoIndexCleanup() {
	await executeProcedure('local-index/perso-cleanup-procedure.sql');
	return executeRaw(`CALL temp_index_local_perso_local_cleanup()`);
}

export async function eqLocPublicIndexCreate() {
	await executeProcedure(`local-index/public-create-procedure.sql`);
	return executeRaw(`CALL temp_index_local_public_local_create()`);
}

export async function eqLocPublicIndexCleanup() {
	await executeProcedure(`local-index/public-cleanup-procedure.sql`);
	return executeRaw(`CALL temp_index_local_public_local_cleanup()`);
}

function etablissementDocumentFormat(
	e: {
		equipes: any[];
		adresse_geolocalisation: string | null;
		adresse_geolocalisation_perso: string | null;
	} & Record<string, any>
) {
	const { equipes, adresse_geolocalisation, adresse_geolocalisation_perso, ...etablissement } = e;

	const doc = etablissement;

	// s'il y a une géoloc perso, alors on l'utilise
	// sinon, on utilise la géoloc publique
	if (adresse_geolocalisation_perso) {
		doc.adresse_geolocalisation =
			(geometryToPoint(adresse_geolocalisation_perso) as any)?.coordinates ?? null;
	} else if (adresse_geolocalisation) {
		doc.adresse_geolocalisation =
			(geometryToPoint(adresse_geolocalisation) as any)?.coordinates ?? null;
	}

	if (equipes) {
		doc.equipes =
			equipes.map((e: any) => {
				if (e.contribution?.adresse_geolocalisation) {
					e.contribution.adresse_geolocalisation =
						(geometryToPoint(e.contribution.adresse_geolocalisation) as any)?.coordinates ?? null;
				}

				/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
				return e;
			}) ?? [];
	}

	return doc;
}

function localDocumentFormat(
	e: {
		equipes: any[];
		adresse_geolocalisation: string | null;
		adresse_geolocalisation_perso: string | null;
	} & Record<string, any>
) {
	const { equipes, adresse_geolocalisation, adresse_geolocalisation_perso, ...local } = e;

	const doc = local;

	// s'il y a une géoloc perso, alors on l'utilise
	// sinon, on utilise la géoloc publique
	if (adresse_geolocalisation_perso) {
		doc.adresse_geolocalisation =
			(geometryToPoint(adresse_geolocalisation_perso) as any)?.coordinates ?? null;
	} else if (adresse_geolocalisation) {
		doc.adresse_geolocalisation =
			(geometryToPoint(adresse_geolocalisation) as any)?.coordinates ?? null;
	}

	if (equipes) {
		doc.equipes =
			equipes.map((e: any) => {
				if (e.contribution?.adresse_geolocalisation) {
					const { coordinates, type } = e.contribution.adresse_geolocalisation;
					e.contribution.adresse_geolocalisation = { coordinates, type };
				}

				/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
				return e;
			}) ?? [];
	}

	return doc;
}

async function qpvIndexInit() {
	const indexCible = `${elasticQueryQpv.index}_index`;

	// initialisation de l'index
	await indexInit(indexCible, elasticQueryQpv.mapping);

	let total = 0;
	let boucles = 0;

	const qpvGetManyQuery = `
SELECT
	id as qpv_id,
	nom as qpv_nom,
	ST_AsText(ST_Buffer(geometrie, 0)) as qpv_geometrie,
	'2024' as qpv_annee
FROM qpv_2024
UNION
SELECT
	id as qpv_id,
	nom as qpv_nom,
	ST_AsText(ST_Buffer(geometrie, 0)) as qpv_geometrie,
	'2015' as qpv_annee
FROM qpv_2015
`;

	const qpvsCursor = db.cursor<{
		qpv_id: string;
		qpv_nom: string;
		qpv_geometrie: string | null;
	}>({ toSQL: () => ({ sql: qpvGetManyQuery, params: {} }) }, { size: 10 });

	async function* stream() {
		for await (const qpvs of qpvsCursor) {
			if (!qpvs.length) {
				break;
			}

			for (const qpv of qpvs) {
				yield qpv;
			}

			total += qpvs.length;
			boucles += 1;

			if (boucles % 100 === 1) {
				console.logInfo(`[qpvIndexInit]`, {
					boucles,
					length: qpvs.length,
					total,
					first: qpvs[0].qpv_id,
				});
			}
		}

		return;
	}

	// indexation dans ElasticSearch
	const results = await elastic.bulkIndexStream(
		indexCible,
		stream(),
		{},
		/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
		(doc: any) => doc.qpv_id,
		(doc) => {
			console.logError(`[qpvIndexInit] Impossible d'indexer ce document:`, doc);
		}
	);

	console.logInfo(`[qpvIndexInit]`, { boucles, total, results });
}

async function zonageIndexInit() {
	const indexCible = `${elasticQueryZonage.index}_index`;

	// initialisation de l'index
	await indexInit(indexCible, elasticQueryZonage.mapping);

	let total = 0;
	let boucles = 0;

	const zonageGetManyQuery = `
SELECT
	zonage.id as zonage_id,
	etiquette.nom as zonage_nom,
	zonage.etiquette_id as zonage_etiquette_id,
	ST_AsText(ST_Buffer(geometrie, 0)) as zonage_geometrie,
	zonage.equipe_id as zonage_equipe_id
FROM zonage
JOIN etiquette ON etiquette.id = zonage.etiquette_id
`;

	const zonagesCursor = db.cursor<{
		zonage_id: string;
		zonage_nom: string;
		zonage_geometrie: string | null;
		zonage_equipe_id: string;
	}>(
		{ toSQL: () => ({ sql: zonageGetManyQuery, params: {} }) },
		{ size: elasticQueryZonage.BATCH }
	);

	async function* stream() {
		for await (const zonages of zonagesCursor) {
			if (!zonages.length) {
				break;
			}

			for (const zonage of zonages) {
				yield zonage;
			}

			total += zonages.length;
			boucles += 1;

			if (boucles % 100 === 1) {
				console.logInfo(`[zonageIndexInit]`, {
					boucles,
					length: zonages.length,
					total,
					first: zonages[0].zonage_id,
				});
			}
		}
	}

	// indexation dans ElasticSearch
	const results = await elastic.bulkIndexStream(
		indexCible,
		stream(),
		{},
		/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
		(doc: any) => doc.zonage_id,
		(doc) => {
			console.logError(`[zonageIndexInit] Impossible d'indexer ce document:`, doc);
		}
	);

	console.logInfo(`[zonageIndexInit]`, { boucles, total, results });
}

async function etablissementIndexInit({ resetIndexes = false }: { resetIndexes: boolean }) {
	const indexCible = `${elasticQueryEtablissement.index}_index`;

	// initialisation de l'index
	await indexInit(indexCible, elasticQueryEtablissement.mapping);

	if (resetIndexes) {
		// suppression de toutes les tables temporaires et indexées
		await eqEtabPersoIndexCleanup();
		await eqEtabPublicIndexCleanup();
	}

	if (resetIndexes) {
		// création de toutes les tables temporaires et indexation
		await eqEtabPersoIndexCreate();
		try {
			await eqEtabPublicIndexCreate();
		} catch (e) {
			console.logError(
				`[etablissementIndexInit] Erreur dans la procédure de construction d'index public pour ElasticSearch`,
				e
			);
			return;
		}
	}

	let total = 0;
	let boucles = 0;

	const etablissementIndexGetManyQuery = `SELECT * FROM temp_index_etab_complet`;

	const etablissementsCursor = db.cursor<{
		equipes: any[];
		etablissement_id: string;
		adresse_geolocalisation: string | null;
		adresse_geolocalisation_perso: string | null;
	}>(
		{ toSQL: () => ({ sql: etablissementIndexGetManyQuery, params: {} }) },
		{ size: elasticQueryEtablissement.BATCH }
	);

	async function* stream() {
		for await (const etablissements of etablissementsCursor) {
			if (!etablissements.length) {
				break;
			}

			for (const etablissement of etablissements) {
				yield etablissementDocumentFormat(etablissement);
			}

			total += etablissements.length;
			boucles += 1;

			if (boucles % 100 === 1) {
				console.logInfo(`[etablissementIndexInit]`, {
					boucles,
					length: etablissements.length,
					total,
					first: etablissements[0].etablissement_id,
				});
			}
		}

		return;
	}

	// indexation dans ElasticSearch
	const results = await elastic.bulkIndexStream(
		indexCible,
		stream(),
		{},
		/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
		(doc: any) => doc.etablissement_id,
		(doc) => {
			console.logError(`[etablissementIndexInit] Impossible d'indexer ce document:`, doc);
		}
	);

	console.logInfo(`[etablissementIndexInit]`, { boucles, total, results });
}

async function localIndexInit({ resetIndexes = false }: { resetIndexes: boolean }) {
	const indexCible = `${elasticQueryLocal.index}_index`;
	// initialisation de l'index
	await indexInit(indexCible, elasticQueryLocal.mapping);

	if (resetIndexes) {
		// suppression de toutes les tables temporaires et indexées
		await eqLocPersoIndexCleanup();
		await eqLocPublicIndexCleanup();
	}

	if (resetIndexes) {
		// création de toutes les tables temporaires et indexation
		await eqLocPersoIndexCreate();
		try {
			await eqLocPublicIndexCreate();
		} catch (e) {
			console.logError(
				`Erreur dans la procédure de construction d'index public pour ElasticSearch`,
				e
			);
			return;
		}
	}

	let total = 0;
	let boucles = 0;

	const localIndexGetManyQuery = `SELECT * FROM temp_index_local_complet`;

	const localsCursor = db.cursor<{
		equipes: any[];
		local_id: string;
		adresse_geolocalisation: string | null;
		adresse_geolocalisation_perso: string | null;
	}>(
		{ toSQL: () => ({ sql: localIndexGetManyQuery, params: {} }) },
		{ size: elasticQueryLocal.BATCH }
	);

	async function* stream() {
		for await (const locals of localsCursor) {
			if (!locals.length) {
				break;
			}

			for (const local of locals) {
				yield localDocumentFormat(local);
			}

			total += locals.length;
			boucles += 1;

			if (boucles % 100 === 1) {
				console.logInfo(`[localIndexInit]`, {
					boucles,
					length: locals.length,
					total,
					first: locals[0].local_id,
				});
			}
		}

		return;
	}

	// indexation dans ElasticSearch
	const results = await elastic.bulkIndexStream(
		indexCible,
		stream(),
		{},
		/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
		(doc: any) => doc.local_id,
		(doc) => {
			console.logError(`[localIndexInit] Impossible d'indexer ce document:`, doc);
		}
	);

	console.logInfo(`[localIndexInit]`, { boucles, total, results });
}

async function etablissementIndexUpdate({
	tacheNom,
	tacheId,
	sqlCount,
	sqlSelect,
	options: { upsert } = { upsert: false },
}: {
	tacheNom: Tache['nom'];
	tacheId: Tache['id'];
	sqlCount: SQL<unknown>;
	sqlSelect: string;
	options?: {
		upsert: boolean;
	};
}) {
	const [{ count: total }] = await db.execute(sqlCount);

	const etablissementsCursor = db.cursor<{
		equipes: any[];
		etablissement_id: string;
		adresse_geolocalisation: string | null;
		adresse_geolocalisation_perso: string | null;
	}>({ toSQL: () => ({ sql: sqlSelect, params: {} }) }, { size: elasticQueryEtablissement.BATCH });

	let processedEtablissements = 0;

	async function* stream() {
		for await (const etablissements of etablissementsCursor) {
			for (const etablissement of etablissements) {
				yield etablissementDocumentFormat(etablissement);
			}

			processedEtablissements += etablissements.length;

			console.logInfo(
				`[etablissementIndexUpdate] - ${tacheNom} - ${processedEtablissements} / ${total}`
			);

			await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { enCoursAt: new Date() } });
		}

		return;
	}

	try {
		await elastic.bulkUpdateStream(
			elasticQueryEtablissement.index,
			stream(),
			/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
			(doc: any) => doc.etablissement_id,
			{ doc_as_upsert: upsert }
		);
	} catch (e: any) {
		console.logError(`[etablissementIndexUpdate] erreur: ${tacheNom} - `, e.message);

		throw e;
	}

	console.logInfo(`[etablissementIndexUpdate] - ${tacheNom} - fin ${total}`);
}

async function localIndexUpdate({
	tacheNom,
	tacheId,
	sqlCount,
	sqlSelect,
	options: { upsert } = { upsert: false },
}: {
	tacheNom: Tache['nom'];
	tacheId: Tache['id'];
	sqlCount: SQL<unknown>;
	sqlSelect: string;
	options?: {
		upsert: boolean;
	};
}) {
	const [{ count: total }] = await db.execute(sqlCount);

	const localsCursor = db.cursor<{
		equipes: any[];
		local_id: string;
		adresse_geolocalisation: string | null;
		adresse_geolocalisation_perso: string | null;
	}>({ toSQL: () => ({ sql: sqlSelect, params: {} }) }, { size: elasticQueryLocal.BATCH });

	let processedLocals = 0;

	async function* stream() {
		for await (const locals of localsCursor) {
			for (const local of locals) {
				yield localDocumentFormat(local);
			}

			processedLocals += locals.length;

			console.logInfo(`[localIndexUpdate] - ${tacheNom} - ${processedLocals} / ${total}`);

			await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { enCoursAt: new Date() } });
		}

		return;
	}

	try {
		/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
		await elastic.bulkUpdateStream(elasticQueryLocal.index, stream(), (doc: any) => doc.local_id, {
			doc_as_upsert: upsert,
		});
	} catch (e: any) {
		console.logError(`[localIndexUpdate] erreur: ${tacheNom} - `, e.message);

		throw e;
	}

	console.logInfo(`[localIndexUpdate] - ${tacheNom} - fin ${total}`);
}

async function localIndexPartialUpdate({
	tacheNom,
	sqlCount,
	sqlSelect,
}: {
	tacheNom: Tache['nom'];
	sqlCount: SQL<unknown>;
	sqlSelect: string;
}) {
	const [tache] = await dbQueryAdmin.tacheAdd({ tache: { nom: tacheNom } });
	const tacheId = tache.id;

	console.logInfo(`[localIndexPartialUpdate] - ${tacheNom} - début`);

	await localIndexUpdate({
		tacheNom,
		tacheId,
		sqlCount,
		sqlSelect,
	});

	await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { finAt: new Date() } });
}

async function etablissementIndexPartialUpdate({
	tacheNom,
	sqlCount,
	sqlSelect,
}: {
	tacheNom: Tache['nom'];
	sqlCount: SQL<unknown>;
	sqlSelect: string;
}) {
	const [tache] = await dbQueryAdmin.tacheAdd({ tache: { nom: tacheNom } });
	const tacheId = tache.id;

	console.logInfo(`[etablissementIndexPartialUpdate] - ${tacheNom} - début`);

	await etablissementIndexUpdate({
		tacheNom,
		tacheId,
		sqlCount,
		sqlSelect,
	});

	await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { finAt: new Date() } });
}

export const serviceElastic = {
	async qpvIndexInit() {
		try {
			return qpvIndexInit();
		} catch (e) {
			console.logError(e);
		}
	},

	async zonageIndexInit() {
		try {
			return zonageIndexInit();
		} catch (e) {
			console.logError(e);
		}
	},

	async etablissementIndexInit({ resetIndexes }: { resetIndexes: boolean }) {
		try {
			return etablissementIndexInit({
				resetIndexes,
			});
		} catch (e) {
			console.logError(e);
		}
	},

	async localIndexInit({ resetIndexes }: { resetIndexes: boolean }) {
		try {
			return localIndexInit({
				resetIndexes,
			});
		} catch (e) {
			console.logError(e);
		}
	},

	async etablissementIndexCopy({ isIndexInit }: { isIndexInit: boolean }) {
		const index = elasticQueryEtablissement.index;
		const mapping = elasticQueryEtablissement.mapping;

		if (!isIndexInit) {
			await etablissementIndexInit({
				resetIndexes: true,
			});

			// on attend 1 seconde, que le refresh de l'index se fasse
			await wait(1000);

			await indexInit(`${index}_template`, mapping);
		} else {
			await indexInit(`${index}_index`, mapping);
		}

		await elastic.reindex(`${index}_index`, {
			indexTemplate: `${index}_template`,
			isIndexInit,
		});

		await elastic.alias(`${index}_index`, index);
	},

	async localIndexCopy({ isIndexInit }: { isIndexInit: boolean }) {
		const index = elasticQueryLocal.index;
		const mapping = elasticQueryLocal.mapping;

		if (!isIndexInit) {
			await localIndexInit({
				resetIndexes: true,
			});

			// on attend 1 seconde, que le refresh de l'index se fasse
			await wait(1000);

			await indexInit(`${index}_template`, mapping);
		} else {
			await indexInit(`${index}_index`, mapping);
		}

		await elastic.reindex(`${index}_index`, {
			indexTemplate: `${index}_template`,
			isIndexInit,
		});

		await elastic.alias(`${index}_index`, index);
	},

	async qpvIndexCopy({ isIndexInit }: { isIndexInit: boolean }) {
		const index = elasticQueryQpv.index;
		const mapping = elasticQueryQpv.mapping;

		if (!isIndexInit) {
			await qpvIndexInit();

			// on attend 1 seconde, que le refresh de l'index se fasse
			await wait(1000);

			await indexInit(`${index}_template`, mapping);
		} else {
			await indexInit(`${index}_index`, mapping);
		}

		await elastic.reindex(`${index}_index`, {
			indexTemplate: `${index}_template`,
			isIndexInit,
		});

		await elastic.alias(`${index}_index`, index);
	},

	async zonageIndexCopy({ isIndexInit }: { isIndexInit: boolean }) {
		const index = elasticQueryQpv.index;
		const mapping = elasticQueryQpv.mapping;

		if (!isIndexInit) {
			await zonageIndexInit();

			// on attend 1 seconde, que le refresh de l'index se fasse
			await wait(1000);

			await indexInit(`${index}_template`, mapping);
		} else {
			await indexInit(`${index}_index`, mapping);
		}

		await elastic.reindex(`${index}_index`, {
			indexTemplate: `${index}_template`,
			isIndexInit,
		});

		await elastic.alias(`${index}_index`, index);
	},

	async etablissementIndexUpdateMapping() {
		try {
			return indexUpdateMapping(elasticQueryEtablissement.index, elasticQueryEtablissement.mapping);
		} catch (e) {
			console.logError(e);
		}
	},

	async localIndexQpv2015Update({ tacheNom }: { tacheNom: string }) {
		const [tache] = await dbQueryAdmin.tacheAdd({ tache: { nom: tacheNom } });
		const tacheId = tache.id;

		console.logInfo(
			`[localIndexQpv2015Update] - effacement du champ ${elasticQueryEquipeEtablissementMappings.properties.adresseQpv2015Id} pour tous les documents`
		);

		await elastic.updateByQuery(
			elasticQueryEtablissement.index,
			elastic.helpers.builder.existsQuery(
				elasticQueryEquipeEtablissementMappings.properties.adresseQpv2015Id
			),
			{
				script: elastic.helpers.fieldSet({
					field: elasticQueryEquipeEtablissementMappings.properties.adresseQpv2015Id,
					value: null,
				}),
			},
			{ refresh: true }
		);

		console.logInfo(`[localIndexQpv2015Update] - effacement - fin`);

		const qpvs = await dbQueryTerritoire.qpvGetManyById({});

		console.logInfo(`[localIndexQpv2015Update] - indexation - début`);

		let processed = 1;

		for (const qpv of qpvs) {
			console.logInfo(
				`[localIndexQpv2015Update] - indexation ${qpv.id} - début - ${processed}/${qpvs.length}`
			);

			await elastic.updateByQuery(
				elasticQueryEtablissement.index,
				elastic.helpers.builder
					.geoShapeQuery(elasticQueryEquipeEtablissementMappings.properties.adresseGeolocalisation)
					.indexedShape(
						elastic.helpers.builder
							.indexedShape()
							.index(elasticQueryQpv.index)
							.id(qpv.id)
							.path('qpv_geometrie')
					),
				{
					script: elastic.helpers.fieldSet({
						field: elasticQueryEquipeEtablissementMappings.properties.adresseQpv2015Id,
						value: qpv.id,
					}),
				},
				{ refresh: true, wait_for_completion: true }
			);

			processed += 1;

			console.logInfo(
				`[localIndexQpv2015Update] - indexation ${qpv.id} - fin - ${processed}/${qpvs.length}`
			);

			await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { enCoursAt: new Date() } });
		}

		console.logInfo(`[localIndexQpv2015Update] - indexation - fin`);

		await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { finAt: new Date() } });
	},

	async localIndexQpv2024Update({ tacheNom }: { tacheNom: string }) {
		const [tache] = await dbQueryAdmin.tacheAdd({ tache: { nom: tacheNom } });
		const tacheId = tache.id;

		console.logInfo(
			`[localIndexQpv2024Update] - effacement du champ ${elasticQueryEquipeEtablissementMappings.properties.adresseQpv2024Id} pour tous les documents`
		);

		await elastic.updateByQuery(
			elasticQueryEtablissement.index,
			elastic.helpers.builder.existsQuery(
				elasticQueryEquipeEtablissementMappings.properties.adresseQpv2024Id
			),
			{
				script: elastic.helpers.fieldSet({
					field: elasticQueryEquipeEtablissementMappings.properties.adresseQpv2024Id,
					value: null,
				}),
			},
			{ refresh: true }
		);

		console.logInfo(`[localIndexQpv2024Update] - effacement - fin`);

		const qpvs = await dbQueryTerritoire.qpvGetManyById({});

		console.logInfo(`[localIndexQpv2024Update] - indexation - début`);

		let processed = 1;

		for (const qpv of qpvs) {
			console.logInfo(`[localIndexQpv2024Update] - indexation ${qpv.id} - début`);

			await elastic.updateByQuery(
				elasticQueryEtablissement.index,
				elastic.helpers.builder
					.geoShapeQuery(elasticQueryEquipeEtablissementMappings.properties.adresseGeolocalisation)
					.indexedShape(
						elastic.helpers.builder
							.indexedShape()
							.index(elasticQueryQpv.index)
							.id(qpv.id)
							.path('qpv_geometrie')
					),
				{
					script: elastic.helpers.fieldSet({
						field: elasticQueryEquipeEtablissementMappings.properties.adresseQpv2024Id,
						value: qpv.id,
					}),
				},
				{ refresh: true, wait_for_completion: true }
			);

			processed += 1;

			console.logInfo(
				`[localIndexQpv2024Update] - indexation ${qpv.id} - fin - ${processed}/${qpvs.length}`
			);

			await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { enCoursAt: new Date() } });
		}

		console.logInfo(`[localIndexQpv2024Update] - indexation - fin`);

		await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { finAt: new Date() } });
	},

	async localIndexUpdateMapping() {
		try {
			return indexUpdateMapping(elasticQueryLocal.index, elasticQueryLocal.mapping);
		} catch (e) {
			console.logError(e);
		}
	},

	async localIndexEquipeUpdate({ tacheNom }: { tacheNom: string }) {
		// suppression de toutes les tables temporaires et indexées
		await eqLocPersoIndexCleanup();

		// création de toutes les tables temporaires et indexation
		await eqLocPersoIndexCreate();

		return localIndexPartialUpdate({
			tacheNom,
			sqlCount: sql`SELECT count(*) FROM temp_index_local_perso_equipe`,
			sqlSelect: `SELECT * FROM temp_index_local_perso_equipe`,
		});
	},

	async indexUpdateByMiseAJourAt({
		tacheNom,
		miseAJourAtDebut,
		miseAJourAtFin,
		resetIndexes,
	}: {
		tacheNom: Tache['nom'];
		miseAJourAtDebut?: Date | null;
		miseAJourAtFin?: Date | null;
		resetIndexes: boolean | null;
	}) {
		const [tache] = await dbQueryAdmin.tacheAdd({ tache: { nom: tacheNom } });
		const tacheId = tache.id;

		console.logInfo(
			`[indexUpdateByMiseAJourAt] dateDebut: ${miseAJourAtDebut}, dateFin: ${miseAJourAtFin}, resetIndexes: ${resetIndexes}`
		);

		if (resetIndexes) {
			// suppression de toutes les tables temporaires et indexées
			await eqEtabPersoIndexCleanup();
			await eqEtabPublicIndexCleanup();

			// création de toutes les tables temporaires et indexation
			// utilisation du mise_a_jour_at pour sélectionner une liste d'établissements restreinte
			await eqEtabPersoIndexUpdate(miseAJourAtDebut ?? null, miseAJourAtFin ?? null);
			try {
				await eqEtabPublicIndexUpdate(miseAJourAtDebut ?? null, miseAJourAtFin ?? null);
			} catch (e) {
				console.logError(
					`Erreur dans la procédure de construction d'index public pour ElasticSearch`,
					e
				);
				await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { finAt: new Date() } });
				return;
			}
		}

		await etablissementIndexUpdate({
			tacheNom,
			tacheId,
			sqlCount: sql`SELECT count(*) FROM temp_index_etab_complet`,
			sqlSelect: `SELECT * FROM temp_index_etab_complet`,
			options: {
				upsert: true,
			},
		});
	},

	async etablissementIndexEquipeUpdate({ tacheNom }: { tacheNom: string }) {
		// suppression de toutes les tables temporaires et indexées
		await eqEtabPersoIndexCleanup();

		// création de toutes les tables temporaires et indexation
		await eqEtabPersoIndexCreate();

		return etablissementIndexPartialUpdate({
			tacheNom,
			sqlCount: sql`SELECT count(*) FROM temp_index_etab_perso_equipe`,
			sqlSelect: `SELECT * FROM temp_index_etab_perso_equipe`,
		});
	},

	async etablissementIndexFermetureUpdate({ tacheNom }: { tacheNom: string }) {
		return etablissementIndexPartialUpdate({
			tacheNom,
			sqlCount: sql`
	SELECT count(*)
	FROM etablissement_periode_fermeture_vue
`,
			sqlSelect: `
	SELECT
		etablissement_id,
		fermeture_date AS etablissement_fermeture_date
	FROM etablissement_periode_fermeture_vue
`,
		});
	},

	async etablissementIndexEffectifUpdate({ tacheNom }: { tacheNom: string }) {
		return etablissementIndexPartialUpdate({
			tacheNom,
			sqlCount: sql`
	SELECT count(*)
	FROM source.rcd__etablissement_effectif_emm_13_mois_annuel
	JOIN source.insee_sirene_api__etablissement USING (siret)
`,
			sqlSelect: `
	WITH temp_effectif AS (
		SELECT
			siret AS etablissement_id,
			effectif AS etablissement_effectif_ema,
			date AS etablissement_effectif_ema_date
		FROM source.rcd__etablissement_effectif_emm_13_mois_annuel
		JOIN source.insee_sirene_api__etablissement USING (siret)
	)
	SELECT
		N_0.etablissement_id,
		N_0.etablissement_effectif_ema_date,
		N_0.etablissement_effectif_ema,
		CASE WHEN N_M_1.effectif IS NOT NULL AND N_M_1.effectif > 0
			THEN ((N_0.etablissement_effectif_ema - N_M_1.effectif) / N_M_1.effectif * 1000) / 10
			ELSE NULL
		END AS etablissement_effectif_ema_variation
	FROM temp_effectif N_0
	LEFT JOIN source.rcd__etablissement_effectif_emm_13_mois N_M_1
		ON N_M_1.siret = N_0.etablissement_id
		AND N_M_1.date + INTERVAL '1 year' = N_0.etablissement_effectif_ema_date
`,
		});
	},

	async etablissementIndexProcedureCollectiveUpdate({
		tacheNom,
		miseAJourAt,
	}: {
		tacheNom: string;
		miseAJourAt?: Date;
	}) {
		const where = miseAJourAt
			? `WHERE source.bodacc__procedure_collective.mise_a_jour_at >= '${miseAJourAt
					.toISOString()
					.slice(0, 10)}'`
			: '';

		const sqlSelect = `
	SELECT
		siret as etablissement_id,
		TRUE AS entreprise_procedure_collective,
		json_agg(DISTINCT date) AS entreprise_procedure_collective_dates
	FROM source.bodacc__procedure_collective
	JOIN source.insee_sirene_api__etablissement USING (siren)
	${where}
	GROUP BY siret
`;

		return etablissementIndexPartialUpdate({
			tacheNom,
			sqlCount: sql.raw(`SELECT count(*) FROM (${sqlSelect}) t`),
			sqlSelect,
		});
	},

	async etablissementIndexEgaproUpdate({ tacheNom }: { tacheNom: string }) {
		const sqlSelect = `
	WITH egapro_siren AS (
		SELECT DISTINCT ON (siren)
			siren,
			note_index as entreprise_egapro_indice,
			cadres_pourcentage_femmes as entreprise_egapro_cadres,
			membres_pourcentage_femmes as entreprise_egapro_instances
		FROM source.egapro__entreprise
		ORDER BY siren, annee DESC
	)
	SELECT
		siret as etablissement_id,
		entreprise_egapro_indice,
		entreprise_egapro_cadres,
		entreprise_egapro_instances
	FROM egapro_siren
	JOIN source.insee_sirene_api__etablissement USING (siren)
	GROUP BY siret,
		entreprise_egapro_indice,
		entreprise_egapro_cadres,
		entreprise_egapro_instances
`;

		return etablissementIndexPartialUpdate({
			tacheNom,
			sqlCount: sql.raw(`SELECT count(*) FROM (${sqlSelect}) t`),
			sqlSelect,
		});
	},

	async etablissementIndexParticipationUpdate({
		tacheNom,
		miseAJourAt,
	}: {
		tacheNom: string;
		miseAJourAt?: Date;
	}) {
		const where = miseAJourAt
			? `WHERE source.api_entreprise__entreprise_participation.mise_a_jour_at >= '${miseAJourAt
					.toISOString()
					.slice(0, 10)}'`
			: '';

		const sqlSelect = `
	WITH sirens AS (
		SELECT source AS entreprise_id FROM source.api_entreprise__entreprise_participation
		UNION
		SELECT cible FROM source.api_entreprise__entreprise_participation
	)
	SELECT
		siret as etablissement_id,
		array_agg(DISTINCT p1.cible) AS entreprise_participation_detient,
		array_agg(DISTINCT p2.source) AS entreprise_participation_filiale_de
	FROM sirens
	LEFT JOIN source.api_entreprise__entreprise_participation p1 ON p1.source = sirens.entreprise_id
	LEFT JOIN source.api_entreprise__entreprise_participation p2 ON p2.cible = p1.source
	JOIN source.insee_sirene_api__etablissement ON sirens.entreprise_id = siren
	${where}
	GROUP BY siret
`;

		return etablissementIndexPartialUpdate({
			tacheNom,
			sqlCount: sql.raw(`SELECT count(*) FROM (${sqlSelect}) t`),
			sqlSelect,
		});
	},

	async etablissementIndexCaUpdate({ tacheNom }: { tacheNom: string }) {
		console.logInfo(`[etablissementIndexCaUpdate] - effacement - début`);

		await elastic.updateByQuery(
			elasticQueryEtablissement.index,
			elastic.helpers.builder.existsQuery(
				elasticQueryEquipeEtablissementMappings.properties.entrepriseChiffreDAffaires
			),
			{
				script: elastic.helpers.objectMerge({
					value: {
						[elasticQueryEquipeEtablissementMappings.properties.entrepriseChiffreDAffaires]: null,
						[elasticQueryEquipeEtablissementMappings.properties.entrepriseExerciceClotureDate]:
							null,
						[elasticQueryEquipeEtablissementMappings.properties
							.entrepriseChiffreDAffairesVariation]: null,
					},
				}),
			},
			{ refresh: true }
		);

		console.logInfo(`[etablissementIndexCaUpdate] - effacement - fin`);

		const sqlSelect = `
	WITH temp_chiffre_d_affaire AS (
		SELECT DISTINCT ON (siren)
			siret AS etablissement_id,
			siren AS entreprise_id,
			date_cloture_exercice AS entreprise_exercice_cloture_date,
			chiffre_d_affaires AS entreprise_chiffre_d_affaires
		FROM source.inpi__ratio_financier
		JOIN source.insee_sirene_api__etablissement USING (siren)
		WHERE type_bilan = 'C'
			AND date_cloture_exercice >= (now() - INTERVAL '3 year')
		ORDER BY siren, date_cloture_exercice DESC
	)
	SELECT
		N_0.etablissement_id,
		N_0.entreprise_exercice_cloture_date,
		N_0.entreprise_chiffre_d_affaires,
		CASE WHEN N_M_1.chiffre_d_affaires IS NOT NULL AND N_M_1.chiffre_d_affaires > 0
			THEN ((N_0.entreprise_chiffre_d_affaires - N_M_1.chiffre_d_affaires) / N_M_1.chiffre_d_affaires * 1000) / 10
			ELSE NULL
		END AS entreprise_chiffre_d_affaires_variation
	FROM temp_chiffre_d_affaire N_0
	LEFT JOIN source.inpi__ratio_financier N_M_1 ON siren = entreprise_id
		AND N_M_1.type_bilan = 'C'
		AND N_M_1.date_cloture_exercice + INTERVAL '1 year' = N_0.entreprise_exercice_cloture_date
`;

		return etablissementIndexPartialUpdate({
			tacheNom,
			sqlCount: sql.raw(`SELECT count(*) FROM (${sqlSelect}) t`),
			sqlSelect,
		});
	},

	async etablissementIndexQpv2015Update({ tacheNom }: { tacheNom: string }) {
		const [tache] = await dbQueryAdmin.tacheAdd({ tache: { nom: tacheNom } });
		const tacheId = tache.id;

		console.logInfo(
			`[etablissementIndexQpv2015Update] - effacement du champ ${elasticQueryEquipeEtablissementMappings.properties.adresseQpv2015Id} pour tous les documents`
		);

		await elastic.updateByQuery(
			elasticQueryEtablissement.index,
			elastic.helpers.builder.existsQuery(
				elasticQueryEquipeEtablissementMappings.properties.adresseQpv2015Id
			),
			{
				script: elastic.helpers.fieldSet({
					field: elasticQueryEquipeEtablissementMappings.properties.adresseQpv2015Id,
					value: null,
				}),
			},
			{ refresh: true }
		);

		console.logInfo(`[etablissementIndexQpv2015Update] - effacement - fin`);

		const qpvs = await dbQueryTerritoire.qpvGetManyById({});

		console.logInfo(`[etablissementIndexQpv2015Update] - indexation - début`);

		let processed = 1;

		for (const qpv of qpvs) {
			console.logInfo(
				`[etablissementIndexQpv2015Update] - indexation ${qpv.id} - début - ${processed}/${qpvs.length}`
			);

			await elastic.updateByQuery(
				elasticQueryEtablissement.index,
				elastic.helpers.builder
					.geoShapeQuery(elasticQueryEquipeEtablissementMappings.properties.adresseGeolocalisation)
					.indexedShape(
						elastic.helpers.builder
							.indexedShape()
							.index(elasticQueryQpv.index)
							.id(qpv.id)
							.path('qpv_geometrie')
					),
				{
					script: elastic.helpers.fieldSet({
						field: elasticQueryEquipeEtablissementMappings.properties.adresseQpv2015Id,
						value: qpv.id,
					}),
				},
				{ refresh: true, wait_for_completion: true }
			);

			processed += 1;

			console.logInfo(
				`[etablissementIndexQpv2015Update] - indexation ${qpv.id} - fin - ${processed}/${qpvs.length}`
			);

			await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { enCoursAt: new Date() } });
		}

		console.logInfo(`[etablissementIndexQpv2015Update] - indexation - fin`);

		await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { finAt: new Date() } });
	},

	async etablissementIndexQpv2024Update({ tacheNom }: { tacheNom: string }) {
		const [tache] = await dbQueryAdmin.tacheAdd({ tache: { nom: tacheNom } });
		const tacheId = tache.id;

		console.logInfo(
			`[etablissementIndexQpv2024Update] - effacement du champ ${elasticQueryEquipeEtablissementMappings.properties.adresseQpv2024Id} pour tous les documents`
		);

		await elastic.updateByQuery(
			elasticQueryEtablissement.index,
			elastic.helpers.builder.existsQuery(
				elasticQueryEquipeEtablissementMappings.properties.adresseQpv2024Id
			),
			{
				script: elastic.helpers.fieldSet({
					field: elasticQueryEquipeEtablissementMappings.properties.adresseQpv2024Id,
					value: null,
				}),
			},
			{ refresh: true }
		);

		console.logInfo(`[etablissementIndexQpv2024Update] - effacement - fin`);

		const qpvs = await dbQueryTerritoire.qpvGetManyById({});

		console.logInfo(`[etablissementIndexQpv2024Update] - indexation - début`);

		let processed = 1;

		for (const qpv of qpvs) {
			console.logInfo(
				`[etablissementIndexQpv2024Update] - indexation ${qpv.id} - début - ${processed}/${qpvs.length}`
			);

			await elastic.updateByQuery(
				elasticQueryEtablissement.index,
				elastic.helpers.builder
					.geoShapeQuery(elasticQueryEquipeEtablissementMappings.properties.adresseGeolocalisation)
					.indexedShape(
						elastic.helpers.builder
							.indexedShape()
							.index(elasticQueryQpv.index)
							.id(qpv.id)
							.path('qpv_geometrie')
					),
				{
					script: elastic.helpers.fieldSet({
						field: elasticQueryEquipeEtablissementMappings.properties.adresseQpv2024Id,
						value: qpv.id,
					}),
				},
				{ refresh: true, wait_for_completion: true }
			);

			processed += 1;

			console.logInfo(
				`[etablissementIndexQpv2024Update] - indexation ${qpv.id} - fin - ${processed}/${qpvs.length}`
			);

			await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { enCoursAt: new Date() } });
		}

		console.logInfo(`[etablissementIndexQpv2024Update] - indexation - fin`);

		await dbQueryAdmin.tacheUpdate({ tacheId, partialTache: { finAt: new Date() } });
	},

	async etablissementIndexAdresseSansEtalabCorrectFix({ tacheNom }: { tacheNom: string }) {
		return etablissementIndexPartialUpdate({
			tacheNom,
			sqlCount: sql`
	SELECT count(*)
	FROM source.insee_sirene_api__etablissement
	LEFT JOIN source.etalab__etablissement_geoloc geoloc USING (siret)
	WHERE geoloc.siret IS NULL OR geoloc.geo_score <= 0.5;
`,
			sqlSelect: `
	SELECT
		siret as etablissement_id,
		EA1.numero_voie_etablissement || COALESCE(EA1.indice_repetition_etablissement, '') AS adresse_numero,
		COALESCE(EA1.type_voie_etablissement || ' ', '') || EA1.libelle_voie_etablissement AS adresse_voie_nom
	FROM source.insee_sirene_api__etablissement EA1
	LEFT JOIN source.etalab__etablissement_geoloc geoloc USING (siret)
	WHERE geoloc.siret IS NULL OR geoloc.geo_score <= 0.5;
`,
		});
	},

	async etablissementIndexEssUpdate({ tacheNom }: { tacheNom: string }) {
		console.logInfo(
			`[etablissementIndexEssUpdate] - écrasement du champ entreprise_economie_sociale_solidaire à false pour tous les documents à true`
		);

		await elastic.updateByQuery(
			elasticQueryEtablissement.index,
			elastic.helpers.builder.termQuery(
				elasticQueryEquipeEtablissementMappings.properties.entrepriseEconomieSocialeSolidaire,
				true
			),
			{
				script: elastic.helpers.fieldSet({
					field:
						elasticQueryEquipeEtablissementMappings.properties.entrepriseEconomieSocialeSolidaire,
					value: false,
				}),
			},
			{ refresh: true }
		);

		console.logInfo(`[etablissementIndexEssUpdate] - écrasement - fin`);

		const sqlSelect = `
	SELECT
		siret AS etablissement_id,
		true AS entreprise_economie_sociale_solidaire
	FROM source.ess_france__entreprise_ess
	JOIN source.insee_sirene_api__etablissement ON entreprise_id = siren
`;

		return etablissementIndexPartialUpdate({
			tacheNom,
			sqlCount: sql.raw(`SELECT count(*) FROM (${sqlSelect}) t`),
			sqlSelect,
		});
	},

	async etablissementIndexMicroUpdate({ tacheNom }: { tacheNom: string }) {
		console.logInfo(
			`[etablissementIndexMicroUpdate] - écrasement du champ entreprise_micro à false pour tous les documents à true`
		);

		await elastic.updateByQuery(
			elasticQueryEtablissement.index,
			elastic.helpers.builder.termQuery(
				elasticQueryEquipeEtablissementMappings.properties.entrepriseMicro,
				true
			),
			{
				script: elastic.helpers.fieldSet({
					field: elasticQueryEquipeEtablissementMappings.properties.entrepriseMicro,
					value: false,
				}),
			},
			{ refresh: true }
		);

		console.logInfo(`[etablissementIndexMicroUpdate] - écrasement - fin`);

		const sqlSelect = `
	SELECT
		siret AS etablissement_id,
		true AS entreprise_micro
	FROM source.inpi_rne__entreprise_micro
	JOIN source.insee_sirene_api__etablissement ON entreprise_id = siren
`;

		return etablissementIndexPartialUpdate({
			tacheNom,
			sqlCount: sql.raw(`SELECT count(*) FROM (${sqlSelect}) t`),
			sqlSelect,
		});
	},

	async etablissementIndexSubventionUpdate({ tacheNom }: { tacheNom: string }) {
		return etablissementIndexPartialUpdate({
			tacheNom,
			sqlCount: sql`
	SELECT count(distinct siret)
	FROM source.dgcl__subvention
	JOIN source.insee_sirene_api__etablissement USING (siret)
`,
			sqlSelect: `
	WITH subvention_annee AS (
		SELECT
			siret AS etablissement_id,
			date_part('year', convention_date) AS convention_annee,
			SUM(montant) AS montant_total
		FROM source.dgcl__subvention
		JOIN source.insee_sirene_api__etablissement USING (siret)
		GROUP BY siret, date_part('year', convention_date)
	)
	SELECT
		etablissement_id,
		TRUE AS etablissement_subvention,
		json_agg(
			json_build_object(
				'convention_annee', convention_annee,
				'montant_total', montant_total
			)
		) AS etablissement_subventions
	FROM subvention_annee
	GROUP BY etablissement_id
`,
		});
	},
};
