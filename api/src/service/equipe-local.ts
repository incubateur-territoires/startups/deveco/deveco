import { equipeEtiquetteUpsertMany } from './equipe';

import {
	Equipe,
	EquipeLocalContribution,
	Contact,
	Local,
	EquipeLocalContact,
	Etablissement,
	New,
	RessourceLocal,
	Evenement,
	ActionDiff,
	Etiquette,
	Deveco,
	Compte,
} from '../db/types';
import { eqLocRessource } from '../db/schema/equipe-local';
import * as dbQueryEquipeLocal from '../db/query/equipe-local';
import * as dbQueryLocal from '../db/query/local';
import * as dbQueryEvenement from '../db/query/evenement';
import * as ElasticQueryEquipeLocal from '../elastic/query/equipe-local';
import * as ElasticQueryLocal from '../elastic/query/local';
import { serviceEquipeLocalSuivi } from '../service/equipe-local-suivi';
import { serviceContact } from '../service/contact';
import { occupeToString } from '../controller/locaux';

export async function etiquetteUpdate({
	evenementId,
	equipeId,
	localId,
	etiquettes,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	localId: Local['id'];
	etiquettes: Etiquette[];
}) {
	const etiquetteIds = etiquettes.map(({ id }) => id);

	const { ajoutees, supprimees } = await dbQueryEquipeLocal.eqLocEtiquetteUpdate({
		equipeId,
		localId,
		etiquetteIds,
	});

	const etiquettesAjoutees = etiquettes.filter((etiquette) => ajoutees.includes(etiquette.id));
	const etiquettesSupprimees = etiquettes.filter((etiquette) => supprimees.includes(etiquette.id));

	if (!etiquettesAjoutees.length && !etiquettesSupprimees.length) {
		return;
	}

	const messageAjout = etiquettesAjoutees.length ? `Ajout d'étiquettes` : '';
	const messageSuppression = etiquettesSupprimees.length ? `Suppression d'étiquettes` : '';

	await dbQueryEvenement.actionEquipeLocalAdd({
		evenementId,
		equipeId,
		localId,
		typeId: 'modification',
		diff: {
			message: `Modification des étiquettes :
${messageAjout}
${messageSuppression}`,
			changes: {
				after: {
					etiquetteIds,
				},
			},
		},
	});

	await dbQueryEvenement.actionEtiquetteLocalAddMany({
		evenementId,
		equipeId,
		localIds: [localId],
		etiquettesAjoutees,
		etiquettesSupprimees,
	});
}

async function eqLocEtiquettesUpdate({
	evenementId,
	equipeId,
	localId,
	localisations,
	motsCles,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	localId: Local['id'];
	localisations: Etiquette['nom'][];
	motsCles: Etiquette['nom'][];
}) {
	// récupère les étiquettes existantes,
	// crée celles qui manquent
	const etiquettes = await equipeEtiquetteUpsertMany({
		evenementId,
		equipeId,
		activites: [],
		localisations,
		motsCles,
	});
	if (!etiquettes.length) {
		return;
	}

	// modifie les étiquettes sur le local
	await etiquetteUpdate({
		evenementId,
		equipeId,
		localId,
		etiquettes,
	});

	// met à jour ElasticSearch
	await ElasticQueryEquipeLocal.eqLocEtiquetteUpdate({
		equipeId,
		localId,
		etiquetteIds: etiquettes.map((e) => e.id),
	});
}

export const serviceEquipeLocal = {
	async contributionUpdate({
		evenementId,
		equipeId,
		compteId,
		localId,
		contribution,
		diff,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		compteId: Compte['id'];
		localId: Local['id'];
		contribution: Partial<EquipeLocalContribution>;
		diff: ActionDiff;
	}): Promise<Local['id']> {
		// modification du local
		const [newContribution] = await dbQueryEquipeLocal.eqLocContributionUpsert({
			equipeId,
			localId,
			contribution: {
				...contribution,
				modificationDate: new Date(),
				modificationCompteId: compteId,
			},
		});

		await ElasticQueryEquipeLocal.eqLocContributionUpdate({
			equipeId,
			localId,
			contribution: newContribution,
		});

		if (typeof newContribution['geolocalisation'] !== 'undefined') {
			let geolocalisation;

			// si on assigne une géolocalisation, alors on l'utilise pour toutes les équipes
			if (newContribution['geolocalisation']) {
				geolocalisation = newContribution['geolocalisation'];
			} else {
				// si on supprime la géolocalisation, alors on réindexe la géolocalisation d'origine
				const [localGeolocalisation] = await dbQueryLocal.localGeolocalisationGet({
					localId,
				});
				geolocalisation = localGeolocalisation?.geolocalisation ?? null;
			}

			await ElasticQueryLocal.localGeolocalisationUpdateMany({
				localGeolocalisations: [
					{
						localId,
						geolocalisation,
					},
				],
			});

			const [{ qpv2024Id } = { qpv2024Id: null }] = await dbQueryLocal.localOnQpv2024GetMany({
				localIds: [localId],
			});

			const [{ qpv2015Id } = { qpv2015Id: null }] = await dbQueryLocal.localOnQpv2015GetMany({
				localIds: [localId],
			});

			await ElasticQueryLocal.localQpvUpdate({
				localId,
				qpv2024Id,
				qpv2015Id,
			});
		}

		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff,
		});

		return localId;
	},

	async occupantAdd({
		evenementId,
		equipeId,
		compteId,
		localId,
		etablissementId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		compteId: Compte['id'];
		localId: Local['id'];
		etablissementId: Etablissement['siret'];
	}) {
		await dbQueryEquipeLocal.eqLocOccupantAdd({
			equipeId,
			localId,
			etablissementId,
		});

		await dbQueryEvenement.actionEquipeLocalOccupantAdd({
			evenementId,
			equipeId,
			localId,
			etablissementId,
			typeId: 'creation',
		});

		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `Ajout d'occupant - ${etablissementId}.`,
			},
		});

		const oldContribution = await dbQueryEquipeLocal.eqLocContributionGet({
			equipeId,
			localId,
		});

		// le statut de la contribution est déjà 'occupé'
		if (oldContribution?.occupe === true) {
			return;
		}

		const occupe = true;

		const contribution = {
			...oldContribution,
			occupe,
		};

		// on met le statut 'occupé' automatiquement
		await serviceEquipeLocal.contributionUpdate({
			evenementId: evenementId,
			equipeId,
			compteId,
			localId,
			contribution,
			diff: {
				message: 'Modification des données personnalisées',
				changes: {
					table: 'equipe_local_contribution',
					after: occupeToString(occupe),
					before: occupeToString(oldContribution?.occupe ?? null),
				},
			},
		});
	},

	async occupantRemove({
		evenementId,
		equipeId,
		compteId,
		localId,
		etablissementId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		compteId: Compte['id'];
		localId: Local['id'];
		etablissementId: Etablissement['siret'];
	}) {
		await dbQueryEquipeLocal.eqLocOccupantRemove({
			equipeId,
			localId,
			etablissementId,
		});

		await dbQueryEvenement.actionEquipeLocalOccupantAdd({
			evenementId,
			equipeId,
			localId,
			etablissementId,
			typeId: 'suppression',
		});

		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `Suppression d'occupant - ${etablissementId}.`,
			},
		});

		const occupant = await dbQueryEquipeLocal.eqLocOccupantGetFirst({
			equipeId,
			localId,
		});
		if (occupant) {
			return;
		}

		// on retire le statut 'occupé' automatiquement
		// car il n'y a plus d'occupant au local
		const oldContribution = await dbQueryEquipeLocal.eqLocContributionGet({
			equipeId,
			localId,
		});

		const occupe = null;

		const contribution = {
			...oldContribution,
			occupe,
		};

		await serviceEquipeLocal.contributionUpdate({
			evenementId: evenementId,
			equipeId,
			compteId,
			localId,
			contribution,
			diff: {
				message: 'Modification des données personnalisées',
				changes: {
					table: 'equipe_local_contribution',
					after: occupeToString(occupe),
					before: occupeToString(oldContribution?.occupe ?? null),
				},
			},
		});
	},

	async geolocalisationUpdate({
		evenementId,
		equipeId,
		compteId,
		localId,
		geolocalisation,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		compteId: Compte['id'];
		localId: Local['id'];
		geolocalisation: EquipeLocalContribution['geolocalisation'];
	}) {
		await serviceEquipeLocal.contributionUpdate({
			evenementId,
			equipeId,
			compteId,
			localId,
			contribution: {
				geolocalisation,
			},
			diff: {
				message: `mise à jour de la géolocalisation : ${geolocalisation ?? 'suppression'}.`,
				changes: {
					after: {
						geolocalisation: geolocalisation ?? 'suppression',
					},
				},
			},
		});
	},

	async view({
		evenementId,
		equipeId,
		localId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
	}) {
		await ElasticQueryEquipeLocal.eqLocConsultationUpdate({
			equipeId,
			localId,
			consultationAt: new Date(),
		});

		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'affichage',
		});
	},

	async ressourceAdd({
		evenementId,
		equipeId,
		localId,
		partialRessource,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		partialRessource: New<typeof eqLocRessource>;
	}) {
		const [ressource] = await dbQueryEquipeLocal.ressourceAdd({
			equipeId,
			localId,
			partialRessource,
		});

		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `Ajout d'une ressource`,
				changes: {
					after: ressource,
					table: 'ressource',
				},
			},
		});
	},

	async ressourceUpdate({
		evenementId,
		equipeId,
		localId,
		ressourceId,
		partialRessource,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		ressourceId: RessourceLocal['id'];
		partialRessource: Partial<RessourceLocal>;
	}) {
		const [ressource] = await dbQueryEquipeLocal.ressourceUpdate({ ressourceId, partialRessource });

		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `Mise à jour d'une ressource`,
				changes: {
					after: ressource,
					table: 'ressource',
				},
			},
		});
	},

	async ressourceRemove({
		evenementId,
		equipeId,
		localId,
		ressourceId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		ressourceId: RessourceLocal['id'];
	}) {
		await dbQueryEquipeLocal.ressourceRemove({ ressourceId });

		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `Suppression d'une ressource`,
				changes: {
					before: ressourceId,
					after: null,
					table: 'ressource',
				},
			},
		});
	},

	async contactAdd({
		evenementId,
		equipeId,
		localId,
		contactId,
		fonction,
		contactSourceTypeId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		contactId: Contact['id'];
		fonction: EquipeLocalContact['fonction'];
		contactSourceTypeId: EquipeLocalContact['contactSourceTypeId'];
	}) {
		await dbQueryEquipeLocal.eqLocContactAdd({
			equipeId,
			localId,
			contactId,
			fonction,
			contactSourceTypeId,
		});

		await dbQueryEvenement.actionContactAdd({
			evenementId,
			contactId,
			typeId: 'modification',
			diff: {
				message: 'lien vers un local',
				changes: {
					after: {
						equipeId,
						localId,
						contactId,
					},
				},
			},
		});

		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `Ajout de contact de type ${fonction}.`,
				changes: {
					table: `eqLocContact`,
					after: {
						equipeId,
						localId,
						contactId,
					},
				},
			},
		});

		await dbQueryEvenement.actionEquipeLocalContactAdd({
			evenementId,
			equipeId,
			localId,
			contactId,
			typeId: 'creation',
		});
	},

	async contactUpdate({
		evenementId,
		equipeId,
		localId,
		contactId,
		fonction,
		partialContact,
		oldContact,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		contactId: Contact['id'];
		fonction: EquipeLocalContact['fonction'];
		partialContact: Partial<Contact>;
		oldContact: Contact;
	}) {
		await serviceContact.contactUpdate({
			evenementId,
			contactId,
			partialContact,
			oldContact,
		});

		// met à jour les noms de contacts dans la table de recherche
		await dbQueryEquipeLocal.eqLocContactNomsUpdate({
			equipeId,
			localId,
		});

		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `mise à jour d'un contact`,
			},
		});

		if (fonction === null) {
			return;
		}

		await dbQueryEquipeLocal.eqLocContactUpdate({
			equipeId,
			localId,
			contactId,
			fonction,
		});

		await dbQueryEvenement.actionEquipeLocalContactAdd({
			evenementId,
			equipeId,
			localId,
			contactId,
			typeId: 'modification',
		});
	},

	async contactRemove({
		evenementId,
		equipeId,
		localId,
		contactId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		contactId: Contact['id'];
	}) {
		const [deletedContact] = await dbQueryEquipeLocal.eqLocContactRemove({
			equipeId,
			localId,
			contactId,
		});

		if (!deletedContact) {
			return;
		}

		const diff = {
			message: `suppression d'un contact de local`,
			changes: {
				before: { contactId, localId, equipeId },
			},
		};

		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff,
		});

		return deletedContact;
	},

	async favoriUpdate({
		evenementId,
		equipeId,
		devecoId,
		localId,
		favori,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		devecoId: Deveco['id'];
		localId: Local['id'];
		favori: boolean;
	}) {
		if (favori) {
			await dbQueryEquipeLocal.eqLocFavoriAdd({
				devecoId,
				localId,
			});
		} else {
			await dbQueryEquipeLocal.eqLocFavoriRemove({
				devecoId,
				localId,
			});
		}

		await dbQueryEvenement.actionEquipeLocalAdd({
			evenementId,
			equipeId,
			localId,
			typeId: 'modification',
			diff: {
				message: `${favori ? 'ajout' : 'suppression'} du favori`,
				changes: {
					after: {
						favori,
					},
				},
			},
		});

		// met à jour ElasticSearch
		await ElasticQueryEquipeLocal.eqLocFavoriUpdate({
			equipeId,
			devecoId,
			localId,
			favori,
		});
	},

	async etiquetteUpdate({
		evenementId,
		equipeId,
		localId,
		localisations,
		motsCles,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		localId: Local['id'];
		localisations: Etiquette['nom'][];
		motsCles: Etiquette['nom'][];
	}) {
		await eqLocEtiquettesUpdate({
			evenementId,
			equipeId,
			localId,
			localisations,
			motsCles,
		});
	},

	...serviceEquipeLocalSuivi,
};
