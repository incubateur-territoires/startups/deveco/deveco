import { Equipe, Etablissement, Evenement, Echange, Demande, Rappel, Brouillon } from '../db/types';
import * as dbQueryEquipeEtablissement from '../db/query/equipe-etablissement';
import * as dbQueryEvenement from '../db/query/evenement';
import * as ElasticQueryEquipeEtablissement from '../elastic/query/equipe-etablissement';

async function eqEtabSuiviDatesUpdate({
	equipeId,
	etablissementId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
}) {
	const suiviDates = await dbQueryEquipeEtablissement.eqEtabSuiviDateGetMany({
		equipeId,
		etablissementId,
	});

	return ElasticQueryEquipeEtablissement.eqEtabSuiviDatesUpdate({
		equipeId,
		etablissementId,
		suiviDates: suiviDates.map((s) => s.suiviDates),
	});
}

export const serviceEquipeEtablissementSuivi = {
	async echangeAdd({
		evenementId,
		equipeId,
		etablissementId,
		echange,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		echange: Echange;
	}) {
		await dbQueryEquipeEtablissement.eqEtabEchangeAdd({
			equipeId,
			etablissementId,
			echangeId: echange.id,
		});

		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `creation d'un échange`,
				changes: {
					table: `eqEtabEchange`,
					after: {
						equipeId,
						etablissementId,
						echangeId: echange.id,
					},
				},
			},
		});

		await eqEtabSuiviDatesUpdate({
			equipeId,
			etablissementId,
		});
	},

	async echangeUpdate({
		evenementId,
		equipeId,
		etablissementId,
		echange,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		echange: Echange;
	}) {
		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `modification d'un échange`,
				changes: {
					table: `eqEtabEchange`,
					after: {
						equipeId,
						etablissementId,
						echangeId: echange.id,
					},
				},
			},
		});
	},

	async echangeRemove({
		evenementId,
		equipeId,
		etablissementId,
		echangeId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		echangeId: Echange['id'];
	}) {
		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `suppression d'un échange`,
				changes: {
					table: `eqEtabEchange`,
					before: {
						equipeId,
						etablissementId,
						echangeId,
					},
				},
			},
		});

		await eqEtabSuiviDatesUpdate({
			equipeId,
			etablissementId,
		});
	},

	async demandeAdd({
		evenementId,
		equipeId,
		etablissementId,
		demande,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		demande: Demande;
	}) {
		await dbQueryEquipeEtablissement.eqEtabDemandeAdd({
			equipeId,
			etablissementId,
			demandeId: demande.id,
		});

		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `creation d'une demande`,
				changes: {
					table: `eqEtabDemande`,
					after: {
						equipeId,
						etablissementId,
						demandeId: demande.id,
					},
				},
			},
		});

		await ElasticQueryEquipeEtablissement.eqEtabDemandeAdd({
			equipeId,
			etablissementId,
			demandeTypeIds: [demande.typeId],
		});

		await eqEtabSuiviDatesUpdate({
			equipeId,
			etablissementId,
		});
	},

	async demandeUpdate({
		evenementId,
		equipeId,
		etablissementId,
		demande,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		demande: Demande;
	}) {
		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `modification d'une demande`,
				changes: {
					table: `eqEtabDemande`,
					after: {
						equipeId,
						etablissementId,
						demandeId: demande.id,
					},
				},
			},
		});
	},

	async demandeCloture({
		evenementId,
		equipeId,
		etablissementId,
		demande,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		demande: Demande;
	}) {
		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `cloture d'une demande`,
				changes: {
					table: `eqEtabDemande`,
					after: {
						equipeId,
						etablissementId,
						demandeId: demande.id,
					},
				},
			},
		});

		await ElasticQueryEquipeEtablissement.eqEtabDemandeRemove({
			equipeId,
			etablissementId,
			demandeTypeId: demande.typeId,
		});
	},

	async demandeReouverture({
		evenementId,
		equipeId,
		etablissementId,
		demande,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		demande: Demande;
	}) {
		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `reouverture d'une demande`,
				changes: {
					table: `eqEtabDemande`,
					after: {
						equipeId,
						etablissementId,
						demandeId: demande.id,
					},
				},
			},
		});

		await ElasticQueryEquipeEtablissement.eqEtabDemandeAdd({
			equipeId,
			etablissementId,
			demandeTypeIds: [demande.typeId],
		});
	},

	async demandeRemove({
		evenementId,
		equipeId,
		etablissementId,
		demandeId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		demandeId: Demande['id'];
	}) {
		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `suppression d'une demande`,
				changes: {
					table: `eqEtabDemande`,
					before: {
						equipeId,
						etablissementId,
						demandeId,
					},
				},
			},
		});
	},

	async rappelAdd({
		evenementId,
		equipeId,
		etablissementId,
		rappel,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		rappel: Rappel;
	}) {
		await dbQueryEquipeEtablissement.eqEtabRappelAdd({
			equipeId,
			etablissementId,
			rappelId: rappel.id,
		});

		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `creation d'un rappel`,
				changes: {
					table: `eqEtabRappel`,
					after: {
						equipeId,
						etablissementId,
						rappelId: rappel.id,
					},
				},
			},
		});

		await eqEtabSuiviDatesUpdate({
			equipeId,
			etablissementId,
		});
	},

	async rappelUpdate({
		evenementId,
		equipeId,
		etablissementId,
		rappel,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		rappel: Rappel;
	}) {
		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `modification d'un rappel`,
				changes: {
					table: `eqEtabRappel`,
					after: {
						equipeId,
						etablissementId,
						rappelId: rappel.id,
					},
				},
			},
		});
	},

	async rappelCloture({
		evenementId,
		equipeId,
		etablissementId,
		rappel,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		rappel: Rappel;
	}) {
		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `cloture d'un rappel`,
				changes: {
					table: `eqEtabRappel`,
					after: {
						equipeId,
						etablissementId,
						rappelId: rappel.id,
					},
				},
			},
		});
	},

	async rappelReouverture({
		evenementId,
		equipeId,
		etablissementId,
		rappel,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		rappel: Rappel;
	}) {
		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `reouverture d'un rappel`,
				changes: {
					table: `eqEtabRappel`,
					after: {
						equipeId,
						etablissementId,
						rappelId: rappel.id,
					},
				},
			},
		});
	},

	async rappelRemove({
		evenementId,
		equipeId,
		etablissementId,
		rappelId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		rappelId: Rappel['id'];
	}) {
		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `suppression d'un rappel`,
				changes: {
					table: `eqEtabRappel`,
					before: {
						equipeId,
						etablissementId,
						rappelId,
					},
				},
			},
		});
	},

	async brouillonAdd({
		evenementId,
		equipeId,
		etablissementId,
		brouillon,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		brouillon: Brouillon;
	}) {
		await dbQueryEquipeEtablissement.eqEtabBrouillonAdd({
			equipeId,
			etablissementId,
			brouillonId: brouillon.id,
		});

		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `creation d'un brouillon`,
				changes: {
					table: `eqEtabBrouillon`,
					after: {
						equipeId,
						etablissementId,
						brouillonId: brouillon.id,
					},
				},
			},
		});

		await eqEtabSuiviDatesUpdate({
			equipeId,
			etablissementId,
		});
	},

	async brouillonUpdate({
		evenementId,
		equipeId,
		etablissementId,
		brouillon,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		brouillon: Brouillon;
	}) {
		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `modification d'un brouillon`,
				changes: {
					table: `eqEtabBrouillon`,
					after: {
						equipeId,
						etablissementId,
						brouillonId: brouillon.id,
					},
				},
			},
		});
	},

	async brouillonRemove({
		evenementId,
		equipeId,
		etablissementId,
		brouillonId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etablissementId: Etablissement['siret'];
		brouillonId: Brouillon['id'];
	}) {
		await dbQueryEvenement.actionEquipeEtablissementAdd({
			evenementId,
			equipeId,
			etablissementId,
			typeId: 'modification',
			diff: {
				message: `suppression d'un brouillon`,
				changes: {
					table: `eqEtabBrouillon`,
					before: {
						equipeId,
						etablissementId,
						brouillonId,
					},
				},
			},
		});
	},
};
