import { getDay, getWeek, getWeekOfMonth, isSaturday } from 'date-fns';

import { TacheFrequence } from '../db/types';
import { serviceAdmin } from '../service/admin';
import { serviceDeveco } from '../service/deveco';
import { serviceEquipe } from '../service/equipe';
import { serviceBrevo } from '../service/brevo';
import { serviceApiSirene } from '../service/api-sirene';
import { serviceApiBodacc } from '../service/api-bodacc';
import { serviceApiGeo } from '../service/api-geo';
import { serviceElastic } from '../service/elastic';
import { serviceApiEntreprise } from '../service/api-entreprise';
import { serviceEmailServer } from '../service/email-server';

function triggerOnlyOnTuesdaysOnWeeksTwoAndFour(date: Date): boolean {
	const dayNumber = getDay(date);
	const weekNumber = getWeek(date, {
		locale: {
			code: 'fr-FR',
		},
		weekStartsOn: 1,
		firstWeekContainsDate: 4,
	});

	// if we're on a second week (weeks 4, 8, 12, 16, 20, 24, ..., 48, 52)
	if (weekNumber % 2 === 0) {
		// true on Tuesdays only
		return dayNumber === 2;
	}

	return false;
}

export const serviceCron = {
	async equipeActiviteSend() {
		const now = new Date();

		if (!triggerOnlyOnTuesdaysOnWeeksTwoAndFour(now)) {
			return;
		}

		console.logInfo('[Cron] Territory activity - Start');

		await serviceEquipe.activiteSend();

		console.logInfo('[Cron] Territory activity - Stop');
	},

	async rappelRemindersSend() {
		console.logInfo('[Cron] Rappels - Start');

		await serviceEquipe.rappelRemindersSend();

		console.logInfo('[Cron] Rappels - Stop');
	},

	async relance10JoursSend() {
		console.logInfo('[Cron] Relance à 10 jours - Start');

		await serviceDeveco.relance10Jours();

		console.logInfo('[Cron] Relance à 10 jours - Stop');
	},

	async relance30JoursSend() {
		console.logInfo('[Cron] Relance à 30 jours - Start');

		await serviceDeveco.relance30Jours();

		console.logInfo('[Cron] Relance à 30 jours - Stop');
	},

	async brevoContactUpdateMany() {
		console.logInfo('[Cron] Mise à jour des contacts Brevo - Start');

		await serviceBrevo.contactUpdateMany({});

		console.logInfo('[Cron] Mise à jour des contacts Brevo - Stop');
	},

	async sireneUpdate() {
		const date = new Date();

		let frequence: TacheFrequence;

		let jours;

		if (isSaturday(date)) {
			if (
				getWeekOfMonth(date, {
					weekStartsOn: 1,
				}) === 4
			) {
				jours = 30;
				frequence = 'monthly';
			} else {
				jours = 7;
				frequence = 'weekly';
			}
		} else {
			jours = 0;
			frequence = 'daily';
		}

		const miseAJourAt = new Date();

		await serviceApiSirene.updateByCursor({
			frequence,
			debutJours: jours,
			miseAJourAt,
		});

		// On ne met pas à jour l’index car ce sera fait à la fin
		await serviceApiGeo.geocodeUpdate({
			frequence,
			jours,
			skipIndexation: true,
		});

		await serviceElastic.indexUpdateByMiseAJourAt({
			tacheNom: `etablissement-index-update-${frequence}`,
			miseAJourAtDebut: miseAJourAt,
			resetIndexes: true,
		});
	},

	async bodaccUpdate() {
		await serviceApiBodacc.update();
	},

	async anonymisation() {
		await serviceAdmin.anonymisation();
	},

	async liasseFiscaleReseau() {
		await serviceApiEntreprise.liasseFiscaleReseau();
	},

	async brouillonMailFetch() {
		await serviceEmailServer.emailProcessMany();
	},

	async metabaseRefresh() {
		await serviceAdmin.metabaseRefresh();
	},
};
