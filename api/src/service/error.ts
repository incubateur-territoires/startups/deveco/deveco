type ValueOf<T> = T[keyof T];

const codes = {
	ETABLISSEMENT_DEJA_COUVERT: 12345,
	ETABLISSEMENT_EXISTE_PAS: 12346,
} as const;

class ErrorDeveco extends Error {
	code?: ValueOf<typeof codes>;

	constructor(m: string, code?: ValueOf<typeof codes>) {
		super(m);

		if (code) {
			this.code = code;
		}

		// Set the prototype explicitly.
		Object.setPrototypeOf(this, ErrorDeveco.prototype);
	}
}

export const serviceError = {
	ErrorDeveco,

	...codes,
};
