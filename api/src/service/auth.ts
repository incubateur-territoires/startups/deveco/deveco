import { Jwt, JwtPayload } from 'jsonwebtoken';
import addDays from 'date-fns/addDays';

import { NotUndefined, QueryResult, UserRole } from '../db/types';
import {
	compteWithRelationsGet,
	refreshTokenAdd,
	refreshTokenInvalidate,
	refreshTokenGetByRefresh,
} from '../db/query/admin';
import * as dbQueryEvenement from '../db/query/evenement';
import * as jwt from '../lib/jwt';
import { CtxDefault, JwtPayloadForCtxState } from '../middleware/auth';
import { Utilisateur } from '../controller/_format/deveco';

export type JwtPayloadCustom = JwtPayloadForCtxState & Utilisateur & { refresh: string };

async function compteIdToJwtPayload({
	compte,
	refresh,
}: {
	compte: NotUndefined<QueryResult<typeof compteWithRelationsGet>> & { typeId: UserRole };
	refresh: string;
}): Promise<JwtPayloadCustom> {
	const c = {
		id: compte.id,
		nom: compte.nom,
		prenom: compte.prenom,
		email: compte.email,
		actif: compte.actif,
		cgu: compte.cgu,
		typeId: compte.typeId,
	};

	let jwtPayload: JwtPayloadCustom;

	const lastLogin = await dbQueryEvenement.evenementConnexionLastGet({ compteId: compte.id });
	const montrerTutoriel = !lastLogin;

	const deveco = compte.deveco;
	const equipe = deveco?.equipe;

	if (c.typeId === 'deveco') {
		if (!deveco || !equipe) {
			throw new Error(`Impossible de générer un JWT pour un compte deveco sans deveco ou equipe`);
		}

		jwtPayload = {
			compte: { ...c, typeId: c.typeId },
			typeId: c.typeId,
			deveco: {
				id: deveco.id,
				equipeId: deveco.equipeId,
				compteId: deveco.compteId,
			},
			equipe: {
				id: equipe.id,
				nom: equipe.nom,
				isFrance: equipe.territoireTypeId === 'france',
				hasCarto: equipe.carto === true,
			},
			aUnMotDePasse: !!compte.motDePasse,
			montrerTutoriel,
			refresh,
		};
	} else {
		jwtPayload = {
			compte: { ...c, typeId: c.typeId },
			typeId: c.typeId,
			aUnMotDePasse: !!compte.motDePasse,
			montrerTutoriel,
			refresh,
		};
	}

	return jwtPayload;
}

async function getUserWithRefreshToken({
	refresh: currentRefreshToken,
	token,
	userAgent,
}: {
	refresh: string;
	token: string;
	userAgent: string;
}) {
	const result = await refreshTokenGetByRefresh({
		refresh: currentRefreshToken,
	});
	if (!result) {
		throw new Error(`Pas de refreshToken pour le token ${currentRefreshToken}`);
	}

	const tenDaysAgo = addDays(new Date(), -10);

	// TODO on veut vraiment le faire expirer ?
	if (result.createdAt < tenDaysAgo) {
		throw new Error(`Le refreshToken est expiré (plus de 10j) : ${result.createdAt}`);
	}

	if (result.jwtExpired && result.jwtExpired === token && result.jwtRefreshed) {
		// TODO lastAccessed ? si non, renommer cette colonne car elle n'est pas vraiment "last accessed"
		const jwtPayload = jwt.decodeWithoutVerifying(result.jwtRefreshed);

		if (!checkJwtPayloadCustom(jwtPayload)) {
			throw new Error(`Le token décodé est invalide ${result.jwtRefreshed}`);
		}

		return {
			token: result.jwtRefreshed,
			jwtPayload,
		};
	}

	const compteId = result.compteId;

	const compte = await compteWithRelationsGet({ compteId });
	if (!compte) {
		throw new Error(
			`Le compte mentionné par le jeton de refresh ${currentRefreshToken} n'a pas été trouvé`
		);
	}

	const { token: newToken, jwtPayload } = await createJwt({ userAgent, compte });

	await refreshTokenInvalidate({
		id: result.id,
		jwtExpired: token,
		jwtRefreshed: newToken,
		userAgent,
	});

	return { token: newToken, jwtPayload };
}

function checkJwtPayloadCustom(
	content: JwtPayload | string | null
): content is JwtPayload & JwtPayloadCustom {
	// TODO à faire
	return !!content && typeof content !== 'string';
}

async function oldToNew(
	old: JwtPayload
): Promise<null | { token: string; jwtPayload: JwtPayload & JwtPayloadCustom }> {
	if (old.id) {
		const compte = await compteWithRelationsGet({ compteId: old.id });
		if (!compte) {
			return null;
		}

		console.logInfo('Ancien jeton converti en nouveau !', { compteId: compte.id });

		return await serviceAuth.createJwt({
			userAgent: '',
			compte,
		});
	}

	return null;
}

async function validateJwtPayloadCustom({
	jwtPayload,
	token,
}: {
	jwtPayload: Jwt | JwtPayload;
	token: string;
}): Promise<null | {
	jwtPayload: JwtPayload & JwtPayloadCustom;
	token: string;
	converti: boolean;
}> {
	const decodedContent: JwtPayload = jwtPayload.payload ? jwtPayload.payload : jwtPayload;

	// si on est dans le cas d'un jeton ancien modèle
	if (!decodedContent.typeId) {
		// on tente de convertir
		const newContent = await serviceAuth.oldToNew(decodedContent);
		if (!newContent) {
			console.logError(`Impossible de convertir un ancien jeton`, {
				ancien: decodedContent,
				nouveau: newContent,
			});

			return null;
		}

		// si ça a marché, on est joie
		return { jwtPayload: newContent.jwtPayload, token: newContent.token, converti: true };
	}

	// on est dans le cas d'un jeton nouveau, on cherche à le valider
	if (!serviceAuth.checkJwtPayloadCustom(decodedContent)) {
		console.logError('Jeton nouveau mais invalide ?!', {
			jwt: decodedContent,
		});

		return null;
	}

	return {
		jwtPayload: decodedContent,
		token,
		converti: false,
	};
}

async function createJwt({
	userAgent,
	compte,
}: {
	userAgent: CtxDefault['userAgent'];
	compte: NotUndefined<QueryResult<typeof compteWithRelationsGet>>;
}) {
	if (!compte.actif) {
		throw new Error(`Le compte pour l'id ${compte.id} est inactif`);
	}

	if (compte.typeId === 'api') {
		throw new Error(`Impossible de générer un JWT pour un compte api`);
	}

	const [newRefreshToken] = await refreshTokenAdd({
		compteId: compte.id,
		userAgent: JSON.stringify(userAgent, undefined, 0),
	});

	const jwtPayload = await serviceAuth.compteIdToJwtPayload({
		compte: {
			...compte,
			typeId: compte.typeId,
		},
		refresh: newRefreshToken.token,
	});

	const token = jwt.create(jwtPayload);

	return {
		token,
		jwtPayload,
	};
}

export const serviceAuth = {
	compteIdToJwtPayload,
	getUserWithRefreshToken,
	checkJwtPayloadCustom,
	validateJwtPayloadCustom,
	oldToNew,
	createJwt,
};
