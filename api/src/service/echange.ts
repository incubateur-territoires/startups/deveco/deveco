import { Evenement, Echange, Demande, New, EchangeDemande, Brouillon } from '../db/types';
import { echange } from '../db/schema/suivi';
import * as dbQueryEchange from '../db/query/echange';
import * as dbQueryEvenement from '../db/query/evenement';

export const serviceEchange = {
	async echangeAdd({
		evenementId,
		partialEchange,
		demandeIds,
		brouillonId,
	}: {
		evenementId: Evenement['id'];
		partialEchange: New<typeof echange>;
		demandeIds: Demande['id'][];
		brouillonId: Brouillon['id'] | null;
	}) {
		const [echange] = await dbQueryEchange.echangeAdd({
			echange: partialEchange,
		});

		const echangeId = echange.id;

		await dbQueryEvenement.actionEchangeAdd({
			evenementId,
			echangeId,
			typeId: 'creation',
			diff: {
				message: `creation d'un échange`,
			},
		});

		// ajout des demandes affectées
		let demandesAjoutees: EchangeDemande[] = [];
		if (demandeIds.length) {
			demandesAjoutees = await dbQueryEchange.echangeDemandeAddMany({
				echangeId,
				demandeIds,
			});
		}

		const ajoutees = demandesAjoutees.map(({ demandeId }) => demandeId);

		await dbQueryEvenement.actionEchangeAdd({
			evenementId,
			echangeId: echange.id,
			typeId: 'modification',
			diff: {
				message: `modification d'un échange`,
				changes: {
					after: {
						demandes: {
							ajoutees,
						},
					},
				},
			},
		});

		if (brouillonId) {
			await dbQueryEvenement.actionBrouillonAdd({
				evenementId,
				brouillonId,
				typeId: 'transformation',
				diff: {
					message: `Transformation d'un brouillon en échange : ${echange.id}`,
				},
			});
		}

		return echange;
	},

	async echangeUpdate({
		evenementId,
		echangeId,
		partialEchange,
		demandeIds,
	}: {
		evenementId: Evenement['id'];
		echangeId: Echange['id'];
		partialEchange: Partial<Echange>;
		demandeIds: Demande['id'][];
	}) {
		const [echange] = await dbQueryEchange.echangeUpdate({
			echangeId,
			partialEchange,
		});

		// suppression de toutes les demandes affectées à l'échange
		const demandesSupprimees = await dbQueryEchange.echangeDemandeRemoveAll({
			echangeId,
		});

		// ajout des demandes affectées
		let demandesAjoutees: EchangeDemande[] = [];
		if (demandeIds.length) {
			demandesAjoutees = await dbQueryEchange.echangeDemandeAddMany({
				echangeId,
				demandeIds,
			});
		}

		const supprimees = demandesSupprimees.filter(
			({ demandeId: demandeIdSupprimee }) =>
				!demandesAjoutees.find(({ demandeId }) => demandeIdSupprimee === demandeId)
		);

		const ajoutees = demandesAjoutees.map(({ demandeId }) => demandeId);

		await dbQueryEvenement.actionEchangeAdd({
			evenementId,
			echangeId: echange.id,
			typeId: 'modification',
			diff: {
				message: `modification d'un échange`,
				changes: {
					after: {
						demandes: {
							supprimees,
							ajoutees,
						},
					},
				},
			},
		});

		return echange;
	},

	async echangeRemove({
		evenementId,
		echangeId,
	}: {
		evenementId: Evenement['id'];
		echangeId: Echange['id'];
	}) {
		await dbQueryEchange.echangeRemove({
			echangeId,
		});

		await dbQueryEvenement.actionEchangeAdd({
			evenementId,
			echangeId,
			typeId: 'suppression',
			diff: {
				message: `suppression d'un échange`,
			},
		});

		return {
			id: echangeId,
		};
	},
};
