import {
	Etiquette,
	EtablissementCreation,
	Equipe,
	Contact,
	Deveco,
	Etablissement,
	Evenement,
	Compte,
	EtablissementCreationCreateur,
	New,
	Commune,
	RessourceEtablissementCreation,
} from '../db/types';
import { CreateurAddInput } from '../controller/types';
import * as dbQueryEvenement from '../db/query/evenement';
import * as dbQueryDemande from '../db/query/demande';
import * as dbQueryEquipeEtablissement from '../db/query/equipe-etablissement';
import * as dbQueryEquipeEtablissementTransformation from '../db/query/equipe-etablissement-transformation';
import * as dbQueryEtablissementCreation from '../db/query/etablissement-creation';
import * as ElasticQueryEquipeEtablissement from '../elastic/query/equipe-etablissement';
import { CreateurImport } from '../service/import';
import { equipeEtiquetteUpsertMany } from '../service/equipe';
import * as schemaEtablissementCreation from '../db/schema/etablissement-creation';
import { serviceEtablissementCreationSuivi } from '../service/equipe-etablissement-creation-suivi';
import { serviceContact } from '../service/contact';

export async function etabCreaRelationAddMany({
	evenementId,
	equipeId,
	etabCrea,
	createurs,
	communes,
	activites,
	localisations,
	motsCles,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
	etabCrea: EtablissementCreation;
	createurs: CreateurAddInput[];
	communes: Commune['id'][];
	activites: Etiquette['nom'][];
	localisations: Etiquette['nom'][];
	motsCles: Etiquette['nom'][];
}) {
	// récupère les étiquettes existantes,
	// crée celles qui manquent
	const etiquettes = await equipeEtiquetteUpsertMany({
		evenementId,
		equipeId,
		activites,
		localisations,
		motsCles,
	});

	//  ajoute les étiquettes sur la création d'établissement
	await etiquetteUpdate({
		evenementId,
		equipeId,
		etabCreaId: etabCrea.id,
		etiquettes,
	});

	if (communes.length) {
		await dbQueryEtablissementCreation.etabCreaCommuneRemoveMany({
			etabCreaId: etabCrea.id,
			equipeId,
		});
		await dbQueryEtablissementCreation.etabCreaCommuneAddMany({
			etabCreaId: etabCrea.id,
			equipeId,
			communes,
		});
	}

	// ajout des contacts
	for (const createur of createurs) {
		const { fonction, niveauDiplome, situationProfessionnelle, type } = createur;
		let contactId: Contact['id'];

		if (type === 'nouveau') {
			const newContact = await serviceContact.contactAdd({
				evenementId,
				equipeId,
				contact: createur.contact,
			});

			contactId = newContact.id;
		} else {
			contactId = createur.contactId;
		}

		await dbQueryEtablissementCreation.etabCreaCreateurAdd({
			equipeId,
			etabCreaId: etabCrea.id,
			contactId,
			fonction,
			niveauDiplome: niveauDiplome || null,
			situationProfessionnelle: situationProfessionnelle || null,
		});
	}

	// ajout de l'adresse si nécessaire
	// if (adresse) {
	// 	const { numero, voieNom, codePostal, communeId, geolocalisation } = adresse;
	// 	if (communeId) {
	// 		await dbQueryContact.contactAdresseUpsert({
	// 			contactAdresse: {
	// 				contactId: createurNew.contactId,
	// 				numero,
	// 				voieNom,
	// 				codePostal,
	// 				communeId,
	// 				geolocalisation,
	// 			},
	// 		});
	// 	}
	// }
}

export async function etabCreaWithRelationAdd({
	evenementId,
	equipeId,
	devecoId,
	etabCrea,
	createurs,
	communes,
	activites,
	localisations,
	motsCles,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
	etabCrea: New<typeof schemaEtablissementCreation.etabCrea>;
	createurs: CreateurAddInput[];
	communes: Commune['id'][];
	activites: Etiquette['nom'][];
	localisations: Etiquette['nom'][];
	motsCles: Etiquette['nom'][];
}) {
	// crée la création d'établissement
	const newEtablissementCreation = await dbQueryEtablissementCreation.etabCreaAdd({
		etabCrea: {
			...etabCrea,
			equipeId,
		},
	});

	await dbQueryEvenement.actionEtablissementCreationAdd({
		evenementId,
		etabCreaId: newEtablissementCreation.id,
		typeId: 'creation',
		diff: {
			message: `création d'une création d'établissement`,
			changes: {
				table: `etabCrea`,
				after: {
					equipeId,
					etabCreaId: newEtablissementCreation.id,
				},
			},
		},
	});

	await etabCreaRelationAddMany({
		evenementId,
		equipeId,
		devecoId,
		createurs,
		etabCrea: newEtablissementCreation,
		communes,
		activites,
		localisations,
		motsCles,
	});

	return newEtablissementCreation;
}

async function etiquetteUpdate({
	equipeId,
	etabCreaId,
	etiquettes,
	evenementId,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	etabCreaId: EtablissementCreation['id'];
	etiquettes: Etiquette[];
}) {
	if (!etiquettes) {
		return;
	}

	const etiquetteIds = etiquettes.map(({ id }) => id);

	const { ajoutees, supprimees } = await dbQueryEtablissementCreation.etabCreaEtiquetteUpdate({
		equipeId,
		etabCreaId,
		etiquetteIds,
	});

	const etiquettesAjoutees = etiquettes.filter((etiquette) => ajoutees.includes(etiquette.id));
	const etiquettesSupprimees = etiquettes.filter((etiquette) => supprimees.includes(etiquette.id));

	if (!etiquettesAjoutees.length && !etiquettesSupprimees.length) {
		return;
	}

	const messageAjout = etiquettesAjoutees.length ? `Ajout d'étiquettes` : '';
	const messageSuppression = etiquettesSupprimees.length ? `Suppression d'étiquettes` : '';

	await dbQueryEvenement.actionEtablissementCreationAddMany({
		evenementId,
		etabCreaIds: [etabCreaId],
		typeId: 'modification',
		diff: {
			message: `Modification des étiquettes :
${messageAjout}
${messageSuppression}`,
			changes: {
				after: {
					etiquetteIds,
				},
			},
		},
	});

	await dbQueryEvenement.actionEtiquetteEtablissementCreationAddMany({
		evenementId,
		etabCreaIds: [etabCreaId],
		etiquettesAjoutees,
		etiquettesSupprimees,
	});
}

export const serviceEtablissementCreation = {
	async view({
		evenementId,
		etabCreaId,
	}: {
		evenementId: Evenement['id'];
		etabCreaId: EtablissementCreation['id'];
	}) {
		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'affichage',
		});
	},

	async importBuild({ deveco }: { deveco: Deveco }) {
		const equipeId = deveco.equipeId;
		const compteId = deveco.compteId;
		const devecoId = deveco.id;

		const [evenement] = await dbQueryEvenement.evenementAdd({
			compteId,
			typeId: 'etablissement_creation_import',
		});

		return async (ligne: CreateurImport) => {
			let message = 'OK';

			const { nom, prenom, email, telephone, futureEnseigne, description } = ligne;

			try {
				await etabCreaWithRelationAdd({
					evenementId: evenement.id,
					equipeId,
					devecoId,
					etabCrea: {
						equipeId,
						enseigne: futureEnseigne,
						description,
						commentaire: null,
					},
					createurs: [
						{
							type: 'nouveau',
							contact: {
								equipeId,
								nom,
								prenom,
								email,
								telephone,
							},
							fonction: 'Créateur',
							niveauDiplome: '',
							situationProfessionnelle: '',
						},
					],
					communes: [],
					activites: [],
					localisations: [],
					motsCles: [],
				});
			} catch (e) {
				message = e instanceof Error ? (message = e.message) : 'Erreur inconnue';
			}

			return `${nom}-${prenom}: ${message}`;
		};
	},

	async add({
		evenementId,
		equipeId,
		compteId,
		devecoId,
		etabCrea,
		createurs,
		communes,
		activites,
		localisations,
		motsCles,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		compteId: Compte['id'];
		devecoId: Deveco['id'];
		etabCrea: Pick<
			EtablissementCreation,
			| 'contactOrigineId'
			| 'contactDate'
			| 'enseigne'
			| 'description'
			| 'commentaire'
			| 'projetTypeId'
			| 'secteurActiviteId'
			| 'sourceFinancementId'
			| 'categorieJuridiqueEnvisagee'
			| 'contratAccompagnement'
			| 'rechercheLocal'
		>;
		createurs: CreateurAddInput[];
		communes: Commune['id'][];
		activites: Etiquette['nom'][];
		localisations: Etiquette['nom'][];
		motsCles: Etiquette['nom'][];
	}) {
		// crée la création d'établissement
		const newEtablissementCreation = await dbQueryEtablissementCreation.etabCreaAdd({
			etabCrea: {
				...etabCrea,
				creationDate: new Date(),
				modificationDate: new Date(),
				modificationCompteId: compteId,
				equipeId,
			},
		});

		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId: newEtablissementCreation.id,
			typeId: 'creation',
			diff: {
				message: `création d'une création d'établissement`,
			},
		});

		await etabCreaRelationAddMany({
			evenementId,
			equipeId,
			devecoId,
			etabCrea: newEtablissementCreation,
			createurs,
			communes,
			activites,
			localisations,
			motsCles,
		});

		return newEtablissementCreation;
	},

	async get({
		equipeId,
		etabCreaId,
	}: {
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
	}) {
		const etabCrea = await dbQueryEtablissementCreation.etabCreaWithRelationsGet({
			etabCreaId,
			equipeId,
		});

		if (!etabCrea) {
			return {};
		}

		const [rappels, demandes, qpvs] = await Promise.all([
			dbQueryEtablissementCreation.etabCreaRappelWithRelationGetMany({
				equipeId,
				etabCreaId,
			}),
			dbQueryEtablissementCreation.etabCreaDemandeWithRelationGetMany({
				equipeId,
				etabCreaId,
			}),
			dbQueryEtablissementCreation.etabCreaQpvGetMany({
				equipeId,
				etabCreaId,
			}),
		]);

		return {
			etabCrea,
			rappels,
			demandes,
			qpvs,
		};
	},

	async update({
		evenementId,
		equipeId,
		etabCreaId,
		etabCrea,
		communes,
		activites,
		localisations,
		motsCles,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		etabCrea: Partial<EtablissementCreation>;
		communes: Commune['id'][];
		activites: Etiquette['nom'][];
		localisations: Etiquette['nom'][];
		motsCles: Etiquette['nom'][];
	}) {
		// met à jour les propriétés de la création d'établissement
		await dbQueryEtablissementCreation.etabCreaUpdate({
			etabCreaId,
			etabCrea,
		});

		if (communes.length) {
			await dbQueryEtablissementCreation.etabCreaCommuneRemoveMany({
				etabCreaId,
				equipeId,
			});

			await dbQueryEtablissementCreation.etabCreaCommuneAddMany({
				etabCreaId,
				equipeId,
				communes,
			});
		}

		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: "mise à jour de la création d'établissement",
			},
		});

		// récupère les étiquettes existantes,
		// crée celles qui manquent
		const etiquettes = await equipeEtiquetteUpsertMany({
			evenementId,
			equipeId,
			activites,
			localisations,
			motsCles,
		});

		//  modifie les étiquettes sur l'établissement
		await etiquetteUpdate({
			evenementId,
			equipeId,
			etabCreaId,
			etiquettes,
		});
	},

	async remove({
		evenementId,
		etabCreaId,
	}: {
		evenementId: Evenement['id'];
		etabCreaId: EtablissementCreation['id'];
	}) {
		await dbQueryEtablissementCreation.etabCreaRemove({
			etabCreaId,
		});

		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'suppression',
			diff: {
				message: `suppression de la création d'établissement ${etabCreaId}`,
				changes: {},
			},
		});
	},

	async etiquetteAddMany({
		evenementId,
		equipeId,
		etabCreaIds,
		activites,
		localisations,
		motsCles,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaIds: EtablissementCreation['id'][];
		activites: string[];
		localisations: string[];
		motsCles: string[];
	}) {
		// récupère les étiquettes existantes,
		// crée celles qui manquent
		const etiquettes = await equipeEtiquetteUpsertMany({
			evenementId,
			equipeId,
			activites,
			localisations,
			motsCles,
		});
		if (!etiquettes.length) {
			return;
		}

		const etiquetteIds = etiquettes.map(({ id }) => id);

		// on créé les liens entre création d'établissement et étiquette, un par un
		await dbQueryEtablissementCreation.etabCreaEtiquetteAddMany({
			equipeId,
			etabCreaIds,
			etiquetteIds,
		});

		// ajoute l'événement d'ajout d'une étiquette à une création d'établissement (etiquette x établissement)
		await dbQueryEvenement.actionEtiquetteEtablissementCreationAddMany({
			evenementId,
			etabCreaIds,
			etiquettesAjoutees: etiquettes,
			etiquettesSupprimees: [],
		});

		// on ajoute un événement de modification de création d'établissement à chaque création d'établissement
		await dbQueryEvenement.actionEtablissementCreationAddMany({
			evenementId,
			etabCreaIds,
			typeId: 'modification',
			diff: {
				message: `Modification des étiquettes`,
				changes: {
					after: {
						etiquetteIds,
					},
				},
			},
		});
	},

	async transformationAdd({
		evenementId,
		equipeId,
		etabCreaId,
		etablissementId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		etablissementId: Etablissement['siret'];
	}): Promise<unknown> {
		const etabCreas = await dbQueryEtablissementCreation.etabCreaWithCreateurGet({
			equipeId,
			etabCreaId,
		});
		if (!etabCreas?.creas.length) {
			return;
		}

		await dbQueryEtablissementCreation.etabCreaTransformationAdd({
			etabCreaId,
			etablissementId,
			equipeId,
		});

		// met à jour equipes.ancien_createur à true dans ElasticSearch
		await ElasticQueryEquipeEtablissement.eqEtabAncienCreateurUpdate({
			equipeId,
			etablissementId,
			ancienCreateur: true,
		});

		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `Création d'établissement ${etabCreaId} transformée en établissement avec le SIRET ${etablissementId}`,
			},
		});

		const contactIds = etabCreas.creas.map(({ contactId }) => contactId);

		for (const contactId of contactIds) {
			await dbQueryEquipeEtablissement.eqEtabContactAdd({
				equipeId,
				etablissementId,
				contactId,
				fonction: 'Créateur',
				contactSourceTypeId: '',
			});

			await dbQueryEvenement.actionEquipeEtablissementContactAdd({
				evenementId,
				equipeId,
				etablissementId,
				contactId,
				typeId: 'creation',
			});
		}

		await dbQueryEquipeEtablissementTransformation.eqEtabRappelFromEtablissementCreationAddMany({
			equipeId,
			etabCreaId,
			etablissementId,
		});

		await dbQueryEquipeEtablissementTransformation.eqEtabDemandeFromEtablissementCreationAddMany({
			equipeId,
			etabCreaId,
			etablissementId,
		});

		const eqEtabDemandes = await dbQueryEquipeEtablissement.eqEtabDemandeGetMany({
			equipeId,
			etablissementId,
		});

		if (eqEtabDemandes.length) {
			const demandes = await dbQueryDemande.demandeGetManyById({
				equipeId,
				demandeIds: eqEtabDemandes.map(({ demandeId }) => demandeId),
			});

			if (demandes.length) {
				// on met à jour l'index de recherche
				await ElasticQueryEquipeEtablissement.eqEtabDemandeAdd({
					equipeId,
					etablissementId,
					demandeTypeIds: demandes.map(({ typeId }) => typeId),
				});
			}
		}

		await dbQueryEquipeEtablissementTransformation.eqEtabEchangeFromEtablissementCreationAddMany({
			equipeId,
			etabCreaId,
			etablissementId,
		});

		await dbQueryEquipeEtablissementTransformation.eqEtabEtiquetteFromEtablissementCreationAddMany({
			equipeId,
			etabCreaId,
			etablissementId,
		});

		const eqEtabEtiquettes = await dbQueryEquipeEtablissement.eqEtabEtiquetteIdGetMany({
			equipeId,
			etablissementId,
		});

		if (eqEtabEtiquettes.length) {
			// met à jour ElasticSearch
			await ElasticQueryEquipeEtablissement.eqEtabEtiquetteUpdate({
				equipeId,
				etablissementId,
				etiquetteIds: eqEtabEtiquettes.map((e) => e.etiquetteId),
			});
		}
	},

	async transformationRemove({
		evenementId,
		etabCreaId,
		equipeId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
	}): Promise<unknown> {
		const etabCreaTransformation = await dbQueryEtablissementCreation.etabCreaTransformationGet({
			equipeId,
			etabCreaId,
		});

		if (!etabCreaTransformation) {
			return;
		}

		const { etablissementId, crea } = etabCreaTransformation;

		await dbQueryEtablissementCreation.etabCreaTransformationRemove({
			equipeId,
			etabCreaId,
		});

		// met à jour equipes.ancien_createur à false dans ElasticSearch
		await ElasticQueryEquipeEtablissement.eqEtabAncienCreateurUpdate({
			equipeId,
			etablissementId,
			ancienCreateur: false,
		});

		const contactIds = crea.creas.map(({ contactId }) => contactId);

		for (const contactId of contactIds) {
			await dbQueryEquipeEtablissement.eqEtabContactRemove({
				equipeId,
				etablissementId,
				contactId,
			});
		}

		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'suppression',
			diff: {
				message: `Suppression de la transformation de la création d'établissement ${etabCreaId} en établissement`,
			},
		});

		await dbQueryEquipeEtablissementTransformation.eqEtabRappelFromEtablissementCreationRemoveMany({
			equipeId,
			etabCreaId,
			etablissementId,
		});

		await dbQueryEquipeEtablissementTransformation.eqEtabDemandeFromEtablissementCreationRemoveMany(
			{
				equipeId,
				etabCreaId,
				etablissementId,
			}
		);

		const eqEtabDemandes = await dbQueryEquipeEtablissement.eqEtabDemandeGetMany({
			equipeId,
			etablissementId,
		});

		if (eqEtabDemandes.length) {
			const demandes = await dbQueryDemande.demandeGetManyById({
				equipeId,
				demandeIds: eqEtabDemandes.map(({ demandeId }) => demandeId),
			});

			if (demandes.length) {
				// on met à jour l'index de recherche
				await ElasticQueryEquipeEtablissement.eqEtabDemandeAdd({
					equipeId,
					etablissementId,
					demandeTypeIds: demandes.map(({ typeId }) => typeId),
				});
			}
		}

		await dbQueryEquipeEtablissementTransformation.eqEtabEchangeFromEtablissementCreationRemoveMany(
			{
				equipeId,
				etabCreaId,
				etablissementId,
			}
		);

		// NOTE : on ne supprime pas les étiquettes, pour peur de supprimer celles qui étaient en commun entre les deux entités avant la transformation
		// ex : établissement création (localisation : ZI de la mare), établissement (localisation : ZI de la mare)
		// les utilisateur(rice)s doivent supprimer les nouvelles étiquettes manuellement
	},

	async createurAdd({
		evenementId,
		equipeId,
		etabCreaId,
		contactId,
		fonction,
		niveauDiplome,
		situationProfessionnelle,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		contactId: Contact['id'];
		fonction?: EtablissementCreationCreateur['fonction'];
		niveauDiplome?: EtablissementCreationCreateur['diplomeNiveauId'];
		situationProfessionnelle?: EtablissementCreationCreateur['professionSituationId'];
	}) {
		await dbQueryEvenement.actionContactAdd({
			evenementId,
			contactId,
			typeId: 'modification',
			diff: {
				message: `lien vers une création d'établissement`,
				changes: {
					after: {
						equipeId,
						etabCreaId,
					},
				},
			},
		});

		await dbQueryEtablissementCreation.etabCreaCreateurAdd({
			equipeId,
			etabCreaId,
			contactId,
			fonction,
			niveauDiplome: niveauDiplome === '' ? undefined : niveauDiplome,
			situationProfessionnelle:
				situationProfessionnelle === '' ? undefined : situationProfessionnelle,
		});

		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: "ajout d'un contact",
				changes: {
					table: `etabCreaContact`,
					after: {
						equipeId,
						etabCreaId,
						contactId,
					},
				},
			},
		});

		await dbQueryEvenement.actionEtablissementCreationCreateurAdd({
			evenementId,
			equipeId,
			etabCreaId,
			contactId,
			typeId: 'creation',
		});
	},

	async createurUpdate({
		evenementId,
		equipeId,
		etabCreaId,
		contactId,
		fonction,
		niveauDiplome,
		situationProfessionnelle,
		partialContact,
		oldContact,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		contactId: Contact['id'];
		fonction: EtablissementCreationCreateur['fonction'];
		niveauDiplome: EtablissementCreationCreateur['diplomeNiveauId'];
		situationProfessionnelle: EtablissementCreationCreateur['professionSituationId'];
		partialContact: Partial<Contact>;
		oldContact: Contact;
	}) {
		await serviceContact.contactUpdate({
			evenementId,
			contactId,
			partialContact,
			oldContact,
		});

		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `mise à jour d'un contact`,
			},
		});

		await dbQueryEtablissementCreation.etabCreaCreateurUpdate({
			equipeId,
			etabCreaId,
			contactId,
			fonction,
			niveauDiplome,
			situationProfessionnelle,
		});

		await dbQueryEvenement.actionEtablissementCreationCreateurAdd({
			evenementId,
			equipeId,
			etabCreaId,
			contactId,
			typeId: 'modification',
		});
	},

	async createurRemove({
		evenementId,
		equipeId,
		etabCreaId,
		contactId,
	}: {
		evenementId: Evenement['id'];
		equipeId: Equipe['id'];
		etabCreaId: EtablissementCreation['id'];
		contactId: Contact['id'];
	}) {
		const [deletedContact] = await dbQueryEtablissementCreation.etabCreaCreateurRemove({
			equipeId,
			etabCreaId,
			contactId,
		});

		if (!deletedContact) {
			return;
		}

		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `suppression d'un contact`,
				changes: {
					table: `etabCreaContact`,
					before: {
						equipeId,
						etabCreaId,
						contactId,
					},
				},
			},
		});

		return deletedContact;
	},

	ressourceAdd({
		etabCreaId,
		equipeId,
		partialRessource,
	}: {
		etabCreaId: EtablissementCreation['id'];
		equipeId: Equipe['id'];
		partialRessource: New<typeof schemaEtablissementCreation.etabCreaRessource>;
	}) {
		return dbQueryEtablissementCreation.ressourceAdd({
			equipeId,
			etabCreaId,
			partialRessource,
		});
	},

	ressourceUpdate({
		ressourceId,
		partialRessource,
	}: {
		ressourceId: RessourceEtablissementCreation['id'];
		partialRessource: Partial<RessourceEtablissementCreation>;
	}) {
		return dbQueryEtablissementCreation.ressourceUpdate({ ressourceId, partialRessource });
	},

	ressourceRemove({ ressourceId }: { ressourceId: RessourceEtablissementCreation['id'] }) {
		return dbQueryEtablissementCreation.ressourceRemove({ ressourceId });
	},

	async creationAbandonneeUpdate({
		etabCreaId,
		equipeId,
		creationAbandonnee,
		evenementId,
	}: {
		etabCreaId: EtablissementCreation['id'];
		equipeId: Equipe['id'];
		creationAbandonnee: EtablissementCreation['creationAbandonnee'];
		evenementId: Evenement['id'];
	}) {
		await dbQueryEvenement.actionEtablissementCreationAdd({
			evenementId,
			etabCreaId,
			typeId: 'modification',
			diff: {
				message: `modification d'une création d'établissement`,
				changes: {
					table: `etabCrea`,
					before: { creationAbandonnee: !creationAbandonnee },
					after: { creationAbandonnee },
				},
			},
		});
		return dbQueryEtablissementCreation.etabCreaCreationAbandonneeUpdate({
			equipeId,
			etabCreaId,
			creationAbandonnee,
		});
	},

	...serviceEtablissementCreationSuivi,
};
