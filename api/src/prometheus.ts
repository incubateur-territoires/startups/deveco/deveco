import { collectDefaultMetrics, register } from 'prom-client';
import Koa from 'koa';
import Router from 'koa-router';

// Enable collection of default metrics
collectDefaultMetrics();

const app = new Koa();

const metricsRouter: Router = new Router();

// Setup server to Prometheus scrapes:
metricsRouter.get('/metrics', async (ctx: Koa.Context) => {
	ctx.set('Content-Type', register.contentType);
	ctx.body = await register.metrics();
});

app.use(metricsRouter.routes()).use(metricsRouter.allowedMethods());

// Application error logging.
/* eslint-disable-next-line @typescript-eslint/unbound-method */
app.on('metrics error', console.logError);

export default app;
