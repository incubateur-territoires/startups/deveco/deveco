import Koa from 'koa';

import 'dotenv/config';
import { cronStart } from './cron';
import metrics from './prometheus';

export function server(app: Koa<Koa.DefaultState, Koa.DefaultContext>) {
	const host = process.env.API_HOST ?? '127.0.0.1';

	const port = process.env.API_PORT ? parseInt(process.env.API_PORT, 10) : 4000;

	app.listen(port, host, () => {
		console.logInfo(`Deveco API: http://${host}:${port}`);
	});

	cronStart();

	const monitoringPort = process.env.MONITORING_PORT
		? parseInt(process.env.MONITORING_PORT, 10)
		: 9102;

	metrics.listen(monitoringPort, host, () => {
		console.logInfo(`Server metrics: http://${host}:${monitoringPort}/metrics`);
	});
}
