import * as Sentry from '@sentry/node';

import { CtxUser } from './middleware/auth';

export default function (error: { message: string }, ctx: CtxUser) {
	const state = { ...ctx.state } as const;

	const start = state.reqStart;

	const duration = Date.now() - (start?.getTime() ?? 0);

	const isDeveco = state.typeId === 'deveco';
	const isSuperadmin = state.typeId === 'superadmin';

	const log = {
		url: `${ctx.request.method} - ${state.reqId ?? '????'} - ${ctx.originalUrl}`,
		state: {
			reqId: state.reqId,
			reqStart: state.reqStart,
			compteId: isSuperadmin || isDeveco ? state.compte?.id : null,
			devecoId: isDeveco ? state.deveco.id : null,
			equipeId: isDeveco ? state.equipe.id : null,
		},
		start,
		duration,
	};

	if (error.message.includes('ECONNRESET')) {
		if (ctx.originalUrl.includes('xlsx') || ctx.originalUrl.includes('geojson')) {
			console.logInfo({
				...log,
				message: 'ECONNRESET sur xlsx ou geojson',
			});
			return;
		}

		if (start && duration < 5000) {
			console.logInfo({
				...log,
				message: 'ECONNRESET après moins de 5 secondes',
			});
			return;
		}

		Sentry.setContext('log', log);
		Sentry.captureException(error);

		console.logError({
			...log,
			message: 'ECONNRESET après plus de 5 secondes',
		});
		return;
	}

	Sentry.setContext('log', log);
	Sentry.captureException(error);

	console.logError({
		log,
		error,
		ctx,
	});
}
