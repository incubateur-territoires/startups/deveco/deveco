import fs from 'fs';
import path from 'path';
import { Parser } from 'json2csv';
import toJson from 'csvtojson';

async function jsonImport(file: string) {
	console.log(`[Import QPV-communes, fichier "${file}"...`);

	const fichier = await toJson({
		delimiter: '\t',
	}).fromString(fs.readFileSync(file).toString());

	const results = [];

	for (const line of fichier) {
		const communeIds = line.insee_com.split(/ *, */);

		results.push(
			...communeIds.map((communeId: string) => ({
				commune_id: communeId,
				qpv_2024_id: line.code_qp,
			}))
		);
	}

	return results.sort(
		(a, b) => a.commune_id.localeCompare(b.commune_id) || a.qpv_2024_id.localeCompare(b.qpv_2024_id)
	);
}

async function jsonImportMany() {
	const file = process.argv[2];

	if (!file) {
		console.error('usage: npx ts-node qpv-commune-import.ts /path/vers/fichier.csv');
		process.exit(1);
	}

	const qpvs = await jsonImport(file);

	const parserCsv = new Parser({});

	fs.writeFileSync(
		path.join(__dirname, '../../_full/_sources/anct_commune__qpv_2024.csv'),
		parserCsv.parse(qpvs)
	);
}

if (!module.exports.parent) {
	void jsonImportMany();
}
