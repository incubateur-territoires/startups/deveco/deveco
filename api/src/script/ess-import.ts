import ExcelJS from 'exceljs';

import fs from 'fs';

async function sheetImport(file: string) {
	console.log(`[Import ESS, fichier "${file}"...`);

	let readStream;

	try {
		const workbookReader = new ExcelJS.stream.xlsx.WorkbookReader(fs.createReadStream(file), {});

		for await (const worksheetReader of workbookReader) {
			readStream = worksheetReader;

			break;
		}
	} catch (e: any) {
		console.error(`[Import ESS], ${e.message}`);
		return [];
	}

	if (!readStream) {
		return [];
	}

	let i = 0;
	let rows = 0;

	const results = [];

	for await (const row of readStream) {
		i += 1;

		if (i <= 1 || !Array.isArray(row.values)) {
			continue;
		}

		const values = row.values.slice(1).map((s) => (typeof s === 'string' ? s.trim() : s));

		if (values.filter((v) => v).length === 0) {
			continue;
		}

		rows += 1;

		if (rows % 100000 === 0) {
			console.log(`processed ${rows}`);
		}

		/* eslint-disable-next-line @typescript-eslint/no-base-to-string */
		const siren = values[0]?.toString();
		if (!siren) {
			continue;
		}

		results.push(siren.padStart(9, '0'));
	}

	if (rows === 0) {
		const { name } = readStream as unknown as any;

		console.log(`[Import ESSS, feuille "${name}": Feuille vide, ignorée`);
	}

	console.log(file, rows);

	return results;
}

async function sheetImportMany() {
	const writeStream = fs.createWriteStream('./_full/_sources/ess-france_entreprise.csv');

	writeStream.write('siren\n');

	const resultsAll = [];

	for (const file of process.argv.slice(2)) {
		const results = await sheetImport(file);

		resultsAll.push(results);
	}

	const sirens = new Set(resultsAll.reduce((r, a) => r.concat(a), []));

	for (const siren of sirens) {
		writeStream.write(`${siren}\n`);
	}
}

if (!module.exports.parent) {
	void sheetImportMany();
}
