import fs from 'fs';
import path from 'path';
import { Parser } from 'json2csv';
import * as wkx from 'wkx';

function jsonImport(file: string) {
	console.log(`[Import QPV, fichier "${file}"...`);

	const fichier = JSON.parse(fs.readFileSync(file).toString());

	const results = [];

	for (const feature of fichier.features) {
		const { fid, code_qp, lib_qp, lib_com } = feature.properties;

		console.log(feature.properties);

		// const geom = JSON.stringify(feature.geometry.coordinates);

		const geom = wkx.Geometry.parseGeoJSON(feature.geometry).toWkb().toString('hex');

		results.push({
			gid: fid,
			fid,
			code_qp,
			lib_qp,
			commune: lib_com,
			geom,
		});
	}

	return results;
}

async function jsonImportMany() {
	const file = process.argv[2];

	if (!file) {
		console.error('usage: npx ts-node qpv-import.ts /path/vers/fichier.json');
		process.exit(1);
	}

	const qpvs = jsonImport(file);

	const parserCsv = new Parser({});

	fs.writeFileSync(
		path.join(__dirname, '../../_full/_sources/anct_qpv_2024.csv'),
		parserCsv.parse(qpvs)
	);
}

if (!module.exports.parent) {
	void jsonImportMany();
}
