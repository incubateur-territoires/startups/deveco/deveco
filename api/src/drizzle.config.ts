import 'dotenv/config';
import type { Config } from 'drizzle-kit';

const config: Config = {
	dialect: 'postgresql',
	schema: './src/db/schema/*',
	out: './drizzle',
	dbCredentials: {
		url: process.env.DATABASE_URL
			? process.env.DATABASE_URL
			: `postgresql://${process.env.POSTGRES_USER || process.env.POSTGRES_USERNAME}:${
					process.env.POSTGRES_PASSWORD
				}@${process.env.POSTGRES_HOST}:${process.env.POSTGRES_PORT}/${process.env.POSTGRES_DB}`,
		// host: process.env.POSTGRES_HOST ? process.env.POSTGRES_HOST : '',
		// port: process.env.POSTGRES_PORT ? Number(process.env.POSTGRES_PORT) : undefined,
		// user: process.env.POSTGRES_USER || process.env.POSTGRES_USERNAME || undefined,
		// password: process.env.POSTGRES_PASSWORD,
		// database: process.env.POSTGRES_DB as string,
		// ssl: false,
	},
	verbose: true,
	strict: true,
};

console.log('Postgres config:', config);

export default config;
