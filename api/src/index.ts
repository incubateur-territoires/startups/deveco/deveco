import './instrument';
import './lib/logger';
import { app } from './app';
import { server } from './server';

server(app);
