import { keysOf } from './utils';

export const banVoieToInseeVoie = {
	Allée: 'ALL',
	Avenue: 'AV',
	Boulevard: 'BD',
	Carrefour: 'CAR',
	Chemin: 'CHE',
	Chaussée: 'CHS',
	Cité: 'CITE',
	Corniche: 'COR',
	Cours: 'CRS',
	Domaine: 'DOM',
	Descente: 'DSC',
	Ecart: 'ECA',
	Esplanade: 'ESP',
	Faubourg: 'FG',
	Galerie: 'GAL',
	Groupe: 'GPE',
	'Grande Rue': 'GR',
	Hameau: 'HAM',
	Halle: 'HLE',
	Impasse: 'IMP',
	'Lieu-dit': 'LD',
	Lotissement: 'LOT',
	Marché: 'MAR',
	Montée: 'MTE',
	Passage: 'PAS',
	Place: 'PL',
	Plaine: 'PLN',
	Plateau: 'PLT',
	Promenade: 'PRO',
	Parvis: 'PRV',
	Quartier: 'QUA',
	Quai: 'QUAI',
	Résidence: 'RES',
	Ruelle: 'RLE',
	Rocade: 'ROC',
	'Rond-point': 'RPT',
	Route: 'RTE',
	Rue: 'RUE',
	Sente: 'SEN',
	Sentier: 'SEN',
	Square: 'SQ',
	'Terre-plein': 'TPL',
	Traverse: 'TRA',
	Villa: 'VLA',
	Village: 'VLGE',
};

export const inseeVoieToBanVoie = {
	ALL: 'Allée',
	AV: 'Avenue',
	BD: 'Boulevard',
	CAR: 'Carrefour',
	CHE: 'Chemin',
	CHS: 'Chaussée',
	CITE: 'Cité',
	COR: 'Corniche',
	CRS: 'Cours',
	DOM: 'Domaine',
	DSC: 'Descente',
	ECA: 'Ecart',
	ESP: 'Esplanade',
	FG: 'Faubourg',
	GAL: 'Galerie',
	GPE: 'Groupe',
	GR: 'Grande Rue',
	HAM: 'Hameau',
	HLE: 'Halle',
	IMP: 'Impasse',
	LD: 'Lieu-dit',
	LOT: 'Lotissement',
	MAR: 'Marché',
	MTE: 'Montée',
	PAS: 'Passage',
	PL: 'Place',
	PLN: 'Plaine',
	PLT: 'Plateau',
	PRO: 'Promenade',
	PRV: 'Parvis',
	QUA: 'Quartier',
	QUAI: 'Quai',
	RES: 'Résidence',
	RLE: 'Ruelle',
	ROC: 'Rocade',
	RPT: 'Rond-point',
	RTE: 'Route',
	RUE: 'Rue',
	SEN: 'Sentier',
	SQ: 'Square',
	TPL: 'Terre-plein',
	TRA: 'Traverse',
	VLA: 'Villa',
	VLGE: 'Village',
};

const banToInseeVoieKeys = keysOf(banVoieToInseeVoie);
const inseeToBanVoieKeys = keysOf(inseeVoieToBanVoie);

export const banIndiceToInseeIndice = {
	bis: 'B',
	ter: 'T',
	quater: 'Q',
};

export const inseeIndiceToBanIndice = {
	B: 'bis',
	T: 'ter',
	Q: 'quater',
};

const banToInseeIndiceKeys = keysOf(banIndiceToInseeIndice);
const inseeToBanIndiceKeys = keysOf(inseeIndiceToBanIndice);

export const replaceBanNumeroToInseeNumero = (numero: string) => {
	for (const banIndice of banToInseeIndiceKeys) {
		if (numero.match(banIndice)) {
			return numero.replace(banIndice, banIndiceToInseeIndice[banIndice]);
		}
	}

	return numero;
};

export const replaceInseeNumeroToBanNumero = (numero: string) => {
	for (const inseeIndice of inseeToBanIndiceKeys) {
		if (numero.match(inseeIndice)) {
			return numero.replace(inseeIndice, inseeIndiceToBanIndice[inseeIndice]);
		}
	}

	return numero;
};

export const replaceBanVoieToInseeVoie = (rue: string) => {
	for (const banVoie of banToInseeVoieKeys) {
		if (rue.startsWith(`${banVoie} `)) {
			return rue.replace(`${banVoie} `, `${banVoieToInseeVoie[banVoie]} `);
		}
	}

	return rue;
};

export const replaceInseeVoieToBanVoie = (rue: string) => {
	for (const inseeVoie of inseeToBanVoieKeys) {
		if (rue.startsWith(`${inseeVoie} `)) {
			return rue.replace(`${inseeVoie} `, `${inseeVoieToBanVoie[inseeVoie]} `);
		}
	}

	return rue;
};
