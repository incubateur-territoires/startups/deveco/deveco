import { intlFormat } from 'date-fns';

export function getNWeeksAgo(today: Date, weeks: number) {
	const newDate = new Date(today.getTime() - 7 * 24 * 60 * 60 * 1000 * weeks);

	newDate.setHours(0, 0, 0, 0);

	return newDate;
}

export function getMonthAgo(today: Date) {
	const newDate = new Date(today.getFullYear(), today.getMonth() - 1, today.getDate());

	newDate.setHours(0, 0, 0, 0);

	return newDate;
}

function days(amount: number) {
	return 1000 * 60 * 60 * 24 * amount;
}

export function getPeriodStart(periode: string | undefined): Date {
	const now = new Date();

	switch (periode) {
		case 'mois':
			return new Date(now.getTime() - days(30));
		case 'semaine':
			return new Date(now.getTime() - days(7));
		case 'tout':
		case undefined:
		default:
			return new Date(0);
	}
}

export function formatJSDateToHumanFriendly(d: Date) {
	const year = d.getFullYear();
	const month = d.getMonth() + 1;
	const day = d.getDate();
	return `${day.toString().padStart(2, '0')}/${month.toString().padStart(2, '0')}/${year}`;
}

export function intlFormatLocale(locale: string) {
	return function (d: Date) {
		return intlFormat(
			d,
			{
				day: 'numeric',
				month: 'long',
				year: 'numeric',
				hour: 'numeric',
				minute: 'numeric',
			},
			{ locale }
		);
	};
}

export function intlFormatFr(d: Date) {
	return intlFormatLocale('fr-FR')(d);
}

export function dateToString(date: Date) {
	return date.toISOString().slice(0, 10);
}
