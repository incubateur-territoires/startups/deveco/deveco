interface ProcessorOptions {
	/**
	 * Maximum number of tasks *in flight* at once.
	 * Defaults to 1 (serial).
	 */
	concurrent?: number;

	/**
	 * A rolling time window (ms). You cannot *start* more than `concurrent`
	 * tasks within the last `time` ms. Defaults to 0 (no rolling window limit).
	 */
	time?: number;

	/**
	 * The minimum delay (ms) between task *start times*.
	 * Defaults to 0 (no forced delay).
	 *
	 * Even if the previous task is still running, we wait this `cooldown`
	 * from the last task's *start time* before starting the next one.
	 */
	cooldown?: number;
}

/**
 * A "throttled processor" that allows controlling:
 * - Concurrency (max tasks in flight)
 * - A rolling time window limiting how many tasks start
 * - A cooldown between consecutive task starts
 */
export class Processor {
	private concurrency: number;
	private timeWindow: number;
	private cooldown: number;

	/**
	 * We keep tasks in a queue. Each task is an async function returning some T.
	 * We store them with a uniform "unknown" signature and cast later.
	 */
	private queue: {
		fn: () => Promise<unknown>;
		resolve: (value: unknown) => void;
		reject: (reason?: any) => void;
	}[] = [];

	private activeCount = 0;

	/**
	 * We'll track the start times of tasks to enforce the "timeWindow" rule:
	 *   - Not more than `concurrency` tasks *started* within the last `timeWindow` ms.
	 */
	private recentStarts: number[] = [];

	/**
	 * We'll track the last time we *started* a task to enforce the `cooldown`.
	 */
	private lastStartTime = 0;

	private timer: NodeJS.Timeout | null = null;

	constructor({ concurrent = 1, time = 0, cooldown = 0 }: ProcessorOptions) {
		this.concurrency = concurrent;
		this.timeWindow = time;
		this.cooldown = cooldown;
	}

	/**
	 * Enqueue a new async function. Returns a Promise<T> for that function's result.
	 */
	public process<T>(fn: () => Promise<T>): Promise<T> {
		return new Promise<T>((resolve, reject) => {
			this.queue.push({
				fn: async () => fn(),
				resolve: (val: unknown) => resolve(val as T),
				reject,
			});

			this.scheduleNext();
		});
	}

	/**
	 * Internal method that attempts to start tasks from the queue,
	 * respecting:
	 *  - concurrency (in-flight)
	 *  - the rolling `timeWindow` limit
	 *  - the `cooldown` between consecutive starts
	 */
	private scheduleNext() {
		// Clear any pending timer so we don't set multiple
		if (this.timer) {
			clearTimeout(this.timer);
			this.timer = null;
		}

		while (this.queue.length > 0) {
			// 1) Concurrency check: Are we already at max in-flight tasks?
			if (this.activeCount >= this.concurrency) {
				break; // wait until a running task finishes
			}

			// 2) Rolling time window check
			if (this.timeWindow > 0) {
				const now = Date.now();

				// Remove expired entries from `recentStarts`
				while (this.recentStarts.length > 0 && now - this.recentStarts[0] >= this.timeWindow) {
					this.recentStarts.shift();
				}

				// If we've started `concurrency` tasks in the last `timeWindow` ms,
				// we must wait for at least one to fall outside that window
				// before starting another.
				if (this.recentStarts.length >= this.concurrency) {
					const earliestStart = this.recentStarts[0];
					const waitMs = earliestStart + this.timeWindow - now;
					this.timer = setTimeout(() => this.scheduleNext(), waitMs);
					break;
				}
			}

			// 3) Cooldown check: We must wait `cooldown` ms from the last *start time*
			if (this.cooldown > 0) {
				const now = Date.now();
				const earliestNextStart = this.lastStartTime + this.cooldown;
				if (now < earliestNextStart) {
					const waitMs = earliestNextStart - now;
					this.timer = setTimeout(() => this.scheduleNext(), waitMs);
					break;
				}
			}

			// If we get here, we can start a new task right now
			const { fn, resolve, reject } = this.queue.shift()!;
			this.launchTask(fn, resolve, reject);
		}
	}

	/**
	 * Actually start (launch) a task.
	 * - increments `activeCount`
	 * - records a start time (for both timeWindow & cooldown logic)
	 */
	private launchTask(
		fn: () => Promise<unknown>,
		resolve: (value: unknown) => void,
		reject: (reason?: any) => void
	) {
		this.activeCount++;

		const now = Date.now();
		this.recentStarts.push(now);
		this.lastStartTime = now; // For cooldown

		fn()
			.then(resolve)
			.catch(reject)
			.finally(() => {
				this.activeCount--;
				this.scheduleNext(); // maybe we can start more
			});
	}
}
