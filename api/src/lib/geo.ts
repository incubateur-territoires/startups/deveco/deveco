import { isDefined } from './utils';

export function longitudeValidate(longitude: number | null) {
	return isDefined(longitude) ? Math.max(-180, Math.min(180, longitude)) : null;
}

export function latitudeValidate(latitude: number | null) {
	return isDefined(latitude) ? Math.max(-90, Math.min(90, latitude)) : null;
}
