// type Code =
//   | 'NN'
//   | '00'
//   | '01'
//   | '02'
//   | '03'
//   | '11'
//   | '12'
//   | '21'
//   | '22'
//   | '31'
//   | '32'
//   | '41'
//   | '42'
//   | '51'
//   | '52'
//   | '53';
//
type EffectifDescription =
	| 'Unité non employeuse'
	| 'Pas de salarié(e)'
	| '1 ou 2 salarié(e)s'
	| '3 à 5 salarié(e)s'
	| '6 à 9 salarié(e)s'
	| '10 à 19 salarié(e)s'
	| '20 à 49 salarié(e)s'
	| '50 à 99 salarié(e)s'
	| '100 à 199 salarié(e)s'
	| '200 à 249 salarié(e)s'
	| '250 à 499 salarié(e)s'
	| '500 à 999 salarié(e)s'
	| '1 000 à 1 999 salarié(e)s'
	| '2 000 à 4 999 salarié(e)s'
	| '5 000 à 9 999 salarié(e)s'
	| '10 000 salarié(e)s et plus';

const codeToDescriptionDict: Record<string, EffectifDescription> = {
	NN: 'Unité non employeuse',
	'00': 'Pas de salarié(e)',
	'01': '1 ou 2 salarié(e)s',
	'02': '3 à 5 salarié(e)s',
	'03': '6 à 9 salarié(e)s',
	'11': '10 à 19 salarié(e)s',
	'12': '20 à 49 salarié(e)s',
	'21': '50 à 99 salarié(e)s',
	'22': '100 à 199 salarié(e)s',
	'31': '200 à 249 salarié(e)s',
	'32': '250 à 499 salarié(e)s',
	'41': '500 à 999 salarié(e)s',
	'42': '1 000 à 1 999 salarié(e)s',
	'51': '2 000 à 4 999 salarié(e)s',
	'52': '5 000 à 9 999 salarié(e)s',
	'53': '10 000 salarié(e)s et plus',
};

export const codeToRangeDict: Record<string, [number | null, number | null]> = {
	NN: [null, null],
	'00': [0, 1],
	'01': [1, 3],
	'02': [3, 6],
	'03': [6, 10],
	'11': [10, 20],
	'12': [20, 50],
	'21': [50, 100],
	'22': [100, 200],
	'31': [200, 250],
	'32': [250, 500],
	'41': [500, 1000],
	'42': [1000, 2000],
	'51': [2000, 5000],
	'52': [5000, 10000],
	'53': [10000, null],
};

const valeurToCodeArray: [number, string][] = [
	[10000, '53'],
	[5000, '52'],
	[2000, '51'],
	[1000, '42'],
	[500, '41'],
	[250, '32'],
	[200, '31'],
	[100, '22'],
	[50, '21'],
	[20, '12'],
	[10, '11'],
	[6, '03'],
	[3, '02'],
	[1, '01'],
	[0, '00'],
];

export function effectifCodeToDescription(code: string) {
	return codeToDescriptionDict[code] ?? undefined;
}

export function effectifCodeToRange(code: string) {
	return codeToRangeDict[code] ?? undefined;
}

export function effectifAnneeToDate(anneeEffectifs: number): number | undefined {
	// les valeurs enregistrées dans la base ne sont pas forcément propres
	// on transforme la valeur numérique en string pour pouvoir ensuite la parser
	const annee = parseInt(`${anneeEffectifs}`);
	// si l'année n'est pas comprise entre 2000 et 2050, on ne retourne rien
	return annee > 2000 && annee < 2050 ? annee : undefined;
}

function effectifValeurToCode(effectif: number) {
	const valeurCodeTuple = valeurToCodeArray.find((vc) => effectif >= vc[0]);

	return valeurCodeTuple ? valeurCodeTuple[1] : undefined;
}

export function effectifValeurToDescription(effectif: number) {
	const code = effectifValeurToCode(effectif);

	return code ? effectifCodeToDescription(code) : 'indéterminé';
}
