import nodemailer from 'nodemailer';
import type Mail from 'nodemailer/lib/mailer';
import type SMTPTransport from 'nodemailer/lib/smtp-transport';

export function smtpCheck() {
	if (!process.env.SMTP_HOST || !process.env.SMTP_USER || !process.env.SMTP_PASS) {
		console.logInfo('configuration SMTP manquante');

		return false;
	}

	return true;
}

const smtpConfig: SMTPTransport.Options = {
	host: process.env.SMTP_HOST,
	ignoreTLS: false,
	port: Number(process.env.SMTP_PORT),
	requireTLS: true,
	secure: false,
	auth: {
		pass: process.env.SMTP_PASS,
		user: process.env.SMTP_USER,
	},
};

export function brevoCheck() {
	if (!process.env.BREVO_API_KEY) {
		console.logInfo('configuration Brevo manquante');

		return false;
	}

	return true;
}

export async function emailSend({
	to,
	from,
	subject,
	text,
	html,
	cc,
	bcc,
	attachments = [],
}: Mail.Options): Promise<SMTPTransport.SentMessageInfo> {
	const transporter = nodemailer.createTransport(smtpConfig);
	const mailOptions: Mail.Options = {
		cc,
		bcc,
		from: from ?? `L'équipe Deveco<${process.env.SMTP_FROM}>`,
		replyTo: process.env.SMTP_REPLYTO,
		html,
		subject,
		text,
		to,
		attachments,
	};

	return transporter.sendMail(mailOptions);
}
