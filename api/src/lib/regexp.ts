import { Branded } from './utils';

export type Siret = Branded<string, 'siret'>;
export type Siren = Branded<string, 'siren'>;
export type Invariant = Branded<string, 'invariant'>;

const regExpSiren = '^[0-9]{9}$';
const regExpSiret = '^[0-9]{14}$';
const regExpInvariant = '^[0-9]{10}$';

export function isSiret(fuzzy: string): fuzzy is Siret {
	const search = fuzzy.replaceAll(/\s/g, '');

	return search.match(regExpSiret) !== null;
}

export function isSiren(fuzzy: string): fuzzy is Siren {
	const search = fuzzy.replaceAll(/\s/g, '');

	return search.match(regExpSiren) !== null;
}

export function isInvariant(fuzzy: string): fuzzy is Invariant {
	const search = fuzzy.replaceAll(/\s/g, '');

	return search.match(regExpInvariant) !== null;
}
