import jwt, { TokenExpiredError, SignOptions } from 'jsonwebtoken';

import { serviceAuth, JwtPayloadCustom } from '../service/auth';

export function create(jwtPayload: JwtPayloadCustom) {
	const signOptions: SignOptions = {
		algorithm: 'HS512',
		expiresIn: '1h',
		subject: `${jwtPayload.compte.id}`,
	};

	const key = process.env.JWT_KEY || '';

	const token = jwt.sign(jwtPayload, key, signOptions);

	return token;
}

const verificationOptions = {
	complete: false,
};

type JwtVerifyResult =
	| { result: 'invalid'; reason: string }
	| { result: 'oldVersion'; decoded: jwt.JwtPayload & JwtPayloadCustom; token: string }
	| { result: 'expired'; expiredAt: Date }
	| { result: 'expiredWithRefresh'; refresh: string; token: string }
	| { result: 'ok'; decoded: jwt.JwtPayload & JwtPayloadCustom; token: string };

export async function verify(token: unknown): Promise<JwtVerifyResult> {
	const key = process.env.JWT_KEY || '';

	if (typeof token === 'undefined') {
		return { result: 'invalid', reason: 'token is undefined' };
	}

	if (typeof token !== 'string') {
		return { result: 'invalid', reason: `token is not a string: ${typeof token}` };
	}

	if (token === 'deleted') {
		return { result: 'invalid', reason: 'token is "deleted"' };
	}

	try {
		const decoded = jwt.verify(token, key, verificationOptions);

		if (typeof decoded === 'string') {
			return { result: 'invalid', reason: `unknown decoded content ${decoded}` };
		}

		const result = await serviceAuth.validateJwtPayloadCustom({ jwtPayload: decoded, token });

		if (!result) {
			return { result: 'invalid', reason: `decoded content is invalid` };
		}

		const { jwtPayload, token: newToken, converti } = result;

		if (converti) {
			return { result: 'oldVersion', decoded: jwtPayload, token: newToken };
		}

		return { result: 'ok', decoded: jwtPayload, token };
	} catch (error) {
		if (error instanceof TokenExpiredError) {
			console.logInfo('token expiré, on cherche un jeton de refresh si possible', {
				expiredAt: error.expiredAt,
			});

			const decoded = decodeWithoutVerifying(token);
			if (decoded && typeof decoded !== 'string' && decoded.refresh) {
				console.logInfo(`présence d'un token de refresh, on le renvoie au demandeur`);
				return { result: 'expiredWithRefresh', refresh: decoded.refresh, token };
			}

			return { result: 'expired', expiredAt: error.expiredAt };
		}

		console.logInfo('erreur lors de la vérification du jeton', { token, error });

		return { result: 'invalid', reason: 'unknown error' };
	}
}

export function decodeWithoutVerifying(token: string) {
	return jwt.decode(token, verificationOptions);
}
