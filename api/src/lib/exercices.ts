type LabelChiffreDAffaires =
	| 'Données indisponibles'
	| 'Entre 0 et 5 000 €'
	| 'Entre 5 001 € et 10 000 €'
	| 'Entre 10 001 € et 32 600 €'
	| 'Entre 32 601 € et 100 000 €'
	| 'Entre 100 001 € et 250 000 €'
	| 'Entre 250 001 € et 500 000 €'
	| 'Entre 500 001 € et 1M €'
	| 'Entre 1M € et 10M €'
	| 'Entre 10M € et 50M €'
	| 'Entre 50M € et 100M €'
	| 'Entre 100M € et 500M €'
	| 'Entre 500M € et 1 Mds €'
	| 'Entre 1 Mds €et 50 Mds €'
	| '50 Mds € et plus';

const mapChiffreDAffaires: Record<string, LabelChiffreDAffaires> = {
	ND: 'Données indisponibles',
	'01': 'Entre 0 et 5 000 €',
	'11': 'Entre 5 001 € et 10 000 €',
	'12': 'Entre 10 001 € et 32 600 €',
	'13': 'Entre 32 601 € et 100 000 €',
	'14': 'Entre 100 001 € et 250 000 €',
	'15': 'Entre 250 001 € et 500 000 €',
	'16': 'Entre 500 001 € et 1M €',
	'21': 'Entre 1M € et 10M €',
	'22': 'Entre 10M € et 50M €',
	'23': 'Entre 50M € et 100M €',
	'24': 'Entre 100M € et 500M €',
	'25': 'Entre 500M € et 1 Mds €',
	'31': 'Entre 1 Mds €et 50 Mds €',
	'32': '50 Mds € et plus',
};

export const mapChiffresChiffreDAffaires: Record<string, [number | null, number | null]> = {
	ND: [null, null],
	'01': [0, 5000],
	'11': [5001, 10000],
	'12': [10001, 32600],
	'13': [32601, 100000],
	'14': [100001, 250000],
	'15': [250001, 500000],
	'16': [500001, 1000000],
	'21': [1000001, 10000000],
	'22': [10000001, 50000000],
	'23': [50000001, 100000000],
	'24': [100000001, 500000000],
	'25': [500000001, 1000000000],
	'31': [1000000001, 5000000000],
	'32': [5000000001, null],
};

export const codeChiffreDAffairesToTrancheChiffreDAffaires = (code: string) =>
	mapChiffreDAffaires[code] ?? undefined;

export const codeChiffreDAffairesToChiffres = (code: string) =>
	mapChiffresChiffreDAffaires[code] ?? undefined;
