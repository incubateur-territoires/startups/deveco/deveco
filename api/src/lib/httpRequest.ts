import fetch from 'node-fetch';

interface ErrorResponse {
	__success: false;
	error: 'KO';
	status: number;
	url: string;
}
interface DataResponse<Data> {
	__success: true;
	data: Data;
}

export async function httpGet<Data>(
	url: string,
	headers?: Record<string, string>
): Promise<DataResponse<Data> | ErrorResponse> {
	if (!headers) {
		headers = {
			Accept: 'application/json',
		};
	}

	const response = await fetch(url, {
		method: 'GET',
		headers,
	});

	if (!response.ok) {
		return {
			__success: false,
			error: 'KO',
			status: response.status,
			url: response.url,
		};
	}

	const data = (await response.json()) as Data;

	return {
		__success: true,
		data,
	};
}

export async function httpPost<Data>(
	url: string,
	body: Record<string, unknown> = {},
	headers?: Record<string, string>
): Promise<DataResponse<Data> | ErrorResponse> {
	if (!headers) {
		headers = {
			Accept: 'application/json',
		};
	}

	const formData: string[] = [];

	for (const [key, value] of Object.entries(body)) {
		formData.push(`${key}=${value}`);
	}

	const response = await fetch(url, {
		method: 'POST',
		body: formData.join('&'),
		headers: {
			...headers,
			'Content-Type': 'application/x-www-form-urlencoded',
		},
	});

	if (!response.ok) {
		return {
			__success: false,
			error: 'KO',
			status: response.status,
			url: response.url,
		};
	}

	const data = (await response.json()) as Data;

	return {
		__success: true,
		data,
	};
}
