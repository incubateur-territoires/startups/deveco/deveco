export const querystringToArray = (input: string | string[] | undefined): string[] =>
	(Array.isArray(input) ? input : typeof input !== 'undefined' ? [input] : []).filter(
		(s) => s !== ''
	);

export const querystringToString = (input: string | string[] | undefined): string | null =>
	Array.isArray(input) ? input[0] : typeof input !== 'undefined' ? input : null;

export const querystringToBoolean = (input: string | string[] | undefined): boolean | null =>
	typeof input === 'undefined'
		? null
		: Array.isArray(input)
			? ['true', 'oui'].includes(input[0])
			: ['true', 'oui'].includes(input);

export const querystringToNumber = (input: string | string[] | undefined): number | null =>
	input && !isNaN(Number(input)) ? Number(input) : null;

export const querystringToDate = (input: string | string[] | undefined): Date | null => {
	const inputString = Array.isArray(input) ? input[0] : input;

	const inputDate = typeof inputString === 'string' ? new Date(inputString) : null;

	return inputDate && inputDate.toString() !== 'Invalid Date' ? inputDate : null;
};
