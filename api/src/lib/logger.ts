function getMemUsed() {
	const mem = process.memoryUsage().heapUsed / 1024 / 1024;
	const memPrint = Math.round(mem).toString().padStart(3, ' ');

	return `Mem(${memPrint}MB)`;
}

/* eslint-disable-next-line @typescript-eslint/no-unused-vars */
interface Console {
	logInfo(...args: unknown[]): void;
	logError(...args: unknown[]): void;
}

Object.defineProperties(console, {
	logInfo: {
		value: function (...args: unknown[]) {
			console.info(new Date().toISOString(), getMemUsed(), ...args);
		},
	},
	logError: {
		value: function (...args: unknown[]) {
			console.error(new Date().toISOString(), getMemUsed(), ...args);
		},
	},
});
