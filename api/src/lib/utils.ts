export type Branded<K, T> = K & { __brand: T };

// pris sur https://stackoverflow.com/a/65015868
export type CamelCase<S extends string> = S extends `${infer P1}_${infer P2}${infer P3}`
	? `${Lowercase<P1>}${Uppercase<P2>}${CamelCase<P3>}`
	: Lowercase<S>;

export function filterNullish<T>(args: (T | null | undefined)[]) {
	return args.filter((a) => typeof a !== 'undefined' && a !== undefined && a !== null) as T[];
}

export function keysOf<T extends Record<string, unknown>>(arg: T) {
	return Object.keys(arg) as (keyof T)[];
}

export function fromEntries<T extends string | number | symbol, U>(arg: [T, U][]) {
	return Object.fromEntries(arg) as Record<T, U>;
}

export function snakeToCamel(str: string): CamelCase<typeof str> {
	return str
		.toLowerCase()
		.replace(/([-_][a-z0-9])/g, (group) =>
			group.toUpperCase().replace('-', '').replace('_', '')
		) as CamelCase<typeof str>;
}

export function fromEntriesToCamelCaseKeys<T extends Record<string, unknown>>(
	obj: T
): { [K in keyof T as CamelCase<K & string>]: K };

export function fromEntriesToCamelCaseKeys<T extends Record<string, unknown>, P extends string>(
	obj: T,
	prefix: P
): { [K in keyof T as CamelCase<K & string>]: `${P}.${K & string}` };

export function fromEntriesToCamelCaseKeys<T extends Record<string, unknown>, P extends string>(
	obj: T,
	prefix?: P
) {
	const result: Partial<Record<string, unknown>> = {};
	(Object.keys(obj) as Array<keyof T & string>).forEach((key) => {
		const camelKey = key.replace(/_([a-z0-9])/g, (_, letter: string) => letter.toUpperCase());

		result[camelKey] = prefix !== undefined ? `${prefix}.${key}` : key;
	});
	return prefix !== undefined
		? (result as { [K in keyof T as CamelCase<K & string>]: `${P}.${K & string}` })
		: (result as { [K in keyof T as CamelCase<K & string>]: K });
}

export function isDefined<T>(arg: T | undefined | null): arg is T {
	return arg !== undefined && arg !== null;
}

export function devecoIdentiteFormat({
	nom,
	prenom,
	email,
}: {
	nom?: string | null;
	prenom?: string | null;
	email?: string | null;
}) {
	if (prenom && nom) {
		return `${prenom} ${nom}`;
	}

	if (prenom) {
		return prenom;
	}

	if (nom) {
		return nom;
	}

	if (email) {
		return email;
	}

	return '';
}

export function abbreviateName(nom: string): string {
	return `${nom.slice(0, 1)}.`;
}

// for now, ignore space-separated names, since it does not handle particles well (Kelvin De La Soul, Olivia van der Wilde, etc.)
export function abbreviateCompoundNames(nom: string): string {
	return nom.includes('-') ? nom.split('-').map(abbreviateName).join('-') : abbreviateName(nom);
}

export function displayFirstName({ nom, prenom }: { nom?: string | null; prenom?: string | null }) {
	if (prenom && nom) {
		const noms = abbreviateCompoundNames(nom);
		return `${prenom} ${noms}`;
	}

	if (prenom) {
		return prenom;
	}

	return '-';
}

export function wait(ms: number): Promise<void> {
	return new Promise((resolve) => setTimeout(resolve, ms));
}

export function dedupe<T extends string | number>(list: T[]): T[] {
	return list.filter((v, i) => list.indexOf(v) === i);
}

// usage:
// `plural(1, 'thing') => 'thing'`
// `plural(4, 'thing') => 'things'`
// `plural(4, 'thief', 'thieves') => 'thieves'`
export function plural(num: number, singular: string, plur?: string) {
	return `${num > 1 ? (plur ?? singular + 's') : singular}`;
}

export function toPercent(num: number, total?: number) {
	return Number(num / (total ?? 100)).toLocaleString('fr-FR', {
		style: 'percent',
		minimumFractionDigits: 0,
		maximumFractionDigits: 0,
	});
}

export function typeToDict<T extends { id: string | number }>(array: T[]): Record<string, T> {
	return Object.fromEntries(array.map((obj) => [`${obj.id}`, obj]));
}

const emailRegex = /^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,})$/;
export function emailIsValid(email: string) {
	return emailRegex.test(email);
}

export function regexpEscape(text: string) {
	return text.replace(/[-&[\]{}()*+?.,\\^$|#\s<>]/g, '\\$&').replace(/"/g, '\\"');
}

export function dbQuoteEscape(text: string) {
	return text.replaceAll("'", "''");
}

export function unaccent(text: string) {
	return text.normalize('NFKD').replace(/[\u0300-\u036f]/g, '');
}
