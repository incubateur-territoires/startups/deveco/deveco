import Koa from 'koa';
import { StatusCodes } from 'http-status-codes';

import { CtxUser } from '../middleware/auth';
import { UserRole } from '../db/types';

export function roleMiddleware(role: UserRole) {
	return async (ctx: CtxUser, next: (ctx: Koa.Context) => Promise<unknown>) => {
		if (!ctx.state.typeId) {
			ctx.throw(StatusCodes.UNAUTHORIZED);
		}

		const compte = ctx.state.compte;

		if (role !== compte.typeId) {
			ctx.throw(StatusCodes.UNAUTHORIZED);
		}

		return next(ctx);
	};
}
