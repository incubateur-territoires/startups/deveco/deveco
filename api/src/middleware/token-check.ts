import { StatusCodes } from 'http-status-codes';

import { CtxUser } from './auth';

const tokenMiddleware =
	(referenceToken: string | undefined, referenceTokenName: string) =>
	async (token: string, ctx: CtxUser, next: (ctx: CtxUser) => Promise<unknown>) => {
		if (!referenceToken || token !== referenceToken) {
			ctx.throw(
				StatusCodes.FORBIDDEN,
				`Jeton ${referenceTokenName} invalide : ${ctx.params.token}`
			);
		}

		return next(ctx);
	};

export default tokenMiddleware;
