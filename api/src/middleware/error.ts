import * as Sentry from '@sentry/node';
import { StatusCodes } from 'http-status-codes';
import Koa, { HttpError } from 'koa';

export default async function errorMiddleware(ctx: Koa.Context, next: Koa.Next) {
	try {
		await next();
	} catch (error: unknown) {
		if (error instanceof HttpError) {
			ctx.status = error.statusCode ?? error.status ?? StatusCodes.INTERNAL_SERVER_ERROR;
			error.status = ctx.status;

			if (!ctx.body) {
				ctx.body = { error };
			}

			if (error.status === (StatusCodes.INTERNAL_SERVER_ERROR as number)) {
				ctx.app.emit('error', error, ctx);
				return;
			}

			Sentry.captureException(error);

			if (error.message?.includes('La vérification du jeton a échoué : token is not a string')) {
				console.logInfo('[auth] Aucun JWT (undefined)');
				return;
			}

			if (error.message?.includes('Le jeton a expiré à la date')) {
				console.logInfo('[auth] JWT expiré');
				return;
			}

			if (error.message?.includes('Le refreshToken est expiré')) {
				console.logInfo('[auth] refreshToken expiré');
				return;
			}

			console.logInfo(ctx.state?.reqId ?? 'reqId inconnu', 'error:', {
				status: ctx.status,
				error: error.message ?? 'Erreur inconnue',
			});
		} else if (error instanceof Error) {
			ctx.status = StatusCodes.INTERNAL_SERVER_ERROR;

			if (!ctx.body) {
				ctx.body = { error };
			}

			if (ctx.status === (StatusCodes.INTERNAL_SERVER_ERROR as number)) {
				ctx.app.emit('error', error, ctx);
				return;
			}

			Sentry.captureException(error);

			if (error.message?.includes('La vérification du jeton a échoué : token is not a string')) {
				console.logInfo('[auth] Aucun JWT (undefined)');
				return;
			}

			if (error.message?.includes('Le jeton a expiré à la date')) {
				console.logInfo('[auth] JWT expiré');
				return;
			}

			if (error.message?.includes('Le refreshToken est expiré')) {
				console.logInfo('[auth] refreshToken expiré');
				return;
			}

			console.logInfo(ctx.state?.reqId ?? 'reqId inconnu', 'error:', {
				status: ctx.status,
				error: error.message ?? 'Erreur inconnue',
			});
		} else {
			console.logInfo(ctx.state?.reqId ?? 'reqId inconnu', 'error:', {
				status: ctx.status,
				error,
			});
		}
	}
}
