import Koa from 'koa';

import { CtxUser } from './auth';

const queryStringBracketsMiddleware = async (
	ctx: CtxUser,
	next: (ctx: Koa.Context) => Promise<unknown>
) => {
	const searchParams = new URLSearchParams(ctx.querystring);

	const arrays: Record<string, string[]> = {};

	searchParams.forEach((value, key) => {
		if (key.endsWith('[]')) {
			const newKey = key.slice(0, -2);
			if (!arrays[newKey]) {
				arrays[newKey] = [];
			}
			arrays[newKey].push(value);
		}
	});

	for (const key in arrays) {
		ctx.query[key] = arrays[key];
	}

	return next(ctx);
};

export default queryStringBracketsMiddleware;
