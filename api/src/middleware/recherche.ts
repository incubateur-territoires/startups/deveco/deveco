import Koa from 'koa';

import { CtxDeveco } from './auth';

import { FileFormat, EntiteTypeId } from '../db/types';
import * as dbQueryEquipe from '../db/query/equipe';

export function rechercheMiddleware(entiteTypeId: EntiteTypeId) {
	return async (ctx: CtxDeveco, next: (ctx: Koa.Context) => Promise<unknown>) => {
		const deveco = ctx.state.deveco;
		if (!deveco) {
			return next(ctx);
		}

		const requete = ctx.request.querystring;
		const format = ctx.query.format as FileFormat | null;

		await dbQueryEquipe.rechercheAdd(deveco.id, requete, entiteTypeId, format);

		return next(ctx);
	};
}
