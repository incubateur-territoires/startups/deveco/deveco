import { ParameterizedContext } from 'koa';
import { StatusCodes } from 'http-status-codes';

import { logger } from './logger';

import { serviceCookie } from '../service/cookie';
import { Compte, Deveco, Equipe } from '../db/types';
import { getOrRefreshUserAndToken } from '../controller/auth';
import * as dbQueryEvenement from '../db/query/evenement';
import * as dbQueryAdmin from '../db/query/admin';

export type CtxDefault = ParameterizedContext<Record<string, unknown>>;

interface CtxState {
	reqId?: string;
	reqStart?: Date;
}

export interface JwtPayloadForCtxStateAdmin {
	compte: Pick<Compte, 'id' | 'typeId' | 'email'> & { typeId: 'superadmin' };
	typeId: 'superadmin';
}

export interface JwtPayloadForCtxStateDeveco {
	compte: Pick<Compte, 'id' | 'typeId' | 'email'> & { typeId: 'deveco' };
	deveco: Pick<Deveco, 'id' | 'compteId' | 'equipeId'>;
	equipe: Pick<Equipe, 'id'>;
	typeId: 'deveco';
}

export type JwtPayloadForCtxState = JwtPayloadForCtxStateAdmin | JwtPayloadForCtxStateDeveco;

export type CtxBase = ParameterizedContext<CtxState>;

export type CtxStateDeveco = CtxState & JwtPayloadForCtxStateDeveco;

type CtxStateAdmin = CtxState & JwtPayloadForCtxStateAdmin;

type CtxStateUnidentified = CtxState & { typeId?: undefined };

export type CtxAdmin = ParameterizedContext<CtxStateAdmin>;

export type CtxDeveco = ParameterizedContext<CtxStateDeveco>;

export type CtxUnidentified = ParameterizedContext<CtxStateUnidentified>;

export type CtxUser = ParameterizedContext<CtxStateDeveco | CtxStateAdmin>;

export function authMiddleware(allowEmptyCGUs?: boolean) {
	return async (ctx: CtxDefault, next: (ctx: CtxDefault) => Promise<unknown>) => {
		const result = await getOrRefreshUserAndToken(ctx);

		if (!result) {
			ctx.throw(StatusCodes.UNAUTHORIZED, `Aucun jeton d'authentification trouvé.`);
		}

		if (result.refreshed) {
			serviceCookie.add({ ctx, token: result.newToken });
		}

		const { compte } = result.jwtPayload;
		if (!compte) {
			ctx.throw(StatusCodes.UNAUTHORIZED, `Impossible de récupérer le compte dans le JWT`);
		}

		if (!compte.actif) {
			ctx.throw(StatusCodes.UNAUTHORIZED, `Ce compte n'est pas actif`);
		}

		if (!compte.cgu && !allowEmptyCGUs) {
			ctx.throw(StatusCodes.UNAUTHORIZED, `Ce compte n'a pas accepté les CGUs`);
		}

		ctx.state.typeId = compte.typeId;

		const compteId = compte.id;

		ctx.state.compte = {
			id: compteId,
			email: compte.email,
			typeId: compte.typeId,
		};

		if (ctx.state.typeId === 'deveco' && result.jwtPayload.typeId === 'deveco') {
			const { deveco, equipe } = result.jwtPayload;

			if (!equipe) {
				ctx.throw(StatusCodes.UNAUTHORIZED, `Impossible de récupérer l'équipe dans le JWT`);
			}

			if (!deveco) {
				ctx.throw(StatusCodes.UNAUTHORIZED, `Impossible de récupérer le deveco dans le JWT`);
			}

			ctx.state.deveco = {
				id: deveco.id,
				compteId,
				equipeId: deveco.equipeId,
			};

			ctx.state.equipe = {
				id: equipe.id,
			};
		}

		if (ctx.state.reqId) {
			logger(ctx, 'AUTH');
		}

		void (async () => {
			await dbQueryAdmin.compteUpdate({
				compteId,
				partialCompte: {
					authAt: new Date(),
				},
			});

			const [evenement] = await dbQueryEvenement.evenementAdd({
				compteId,
				typeId: 'authentification',
			});

			await dbQueryEvenement.actionCompteAdd({
				evenementId: evenement.id,
				typeId: 'modification',
				compteId,
				diff: {
					message: 'action authentifiée',
				},
			});
		})();

		return next(ctx);
	};
}
