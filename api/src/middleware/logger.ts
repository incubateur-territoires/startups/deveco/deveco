import { CtxUnidentified, CtxUser } from './auth';

import { healthRoutePrefix } from '../router';

const loggerDirections = {
	loggerIn: '<=',
	loggerMid: '==',
	loggerOut: '=>',
};

const logger =
	(dir: string) =>
	(ctx: CtxUser | CtxUnidentified, ...info: unknown[]) => {
		if (ctx.req.url === healthRoutePrefix) {
			return;
		}

		const reqId = ctx.state.reqId;

		const status = dir === '=>' ? ctx.res.statusCode : '...';

		const log = `${ctx.req.method} ${status} ${ctx.req.url}`;

		let devecoLog: string;

		if (dir === loggerDirections.loggerIn) {
			devecoLog = '';
		} else if (ctx.state.typeId) {
			const compteId = ctx.state.compte.id;

			if (ctx.state.typeId === 'deveco') {
				const devecoId = ctx.state.deveco.id;
				const equipeId = ctx.state.equipe.id;
				devecoLog = `[E(${equipeId}) C(${compteId}) D(${devecoId})]`;
			} else {
				devecoLog = `[Superadmin ${compteId}]`;
			}
		} else {
			devecoLog = '[Non-identifié]';
		}

		console.logInfo(reqId, dir, log, info.join(' '), devecoLog);
	};

const loggerIn = logger(loggerDirections.loggerIn);
const loggerOut = logger(loggerDirections.loggerOut);
const loggerMid = logger(loggerDirections.loggerMid);
export { loggerMid as logger };

let reqCount = 0;
const MAX_COUNT = 10000;
const ZERO_PAD = MAX_COUNT.toString().length - 1;

const loggerMiddleware = async (ctx: CtxUser, next: () => Promise<unknown>) => {
	const start = new Date();
	ctx.state.reqStart = start;

	const reqId = (reqCount++ % MAX_COUNT).toString().padStart(ZERO_PAD, '0');
	ctx.state.reqId = reqId;

	const done = ((ctx: CtxUser) => {
		ctx.res.removeListener('finish', done);
		ctx.res.removeListener('close', done);

		const time = Date.now() - start.getTime();

		loggerOut(ctx, `${time}ms`);
	}).bind(this, ctx);

	ctx.res.once('finish', done);
	ctx.res.once('close', done);

	loggerIn(ctx);

	await next();
};

export default loggerMiddleware;
