import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import compress from 'koa-compress';
import json from 'koa-json';
import cookie from 'koa-cookie';
import etag from 'koa-etag';
import conditional from 'koa-conditional-get';
import userAgent from 'koa-useragent';

import { constants } from 'zlib';

import timestampLogger from './middleware/logger';
import errorMiddleware from './middleware/error';
import querystringBrackets from './middleware/querystring-brackets';
import {
	auth,
	compte,
	contact,
	equipe,
	etablissements,
	entreprises,
	etabCreas,
	health,
	geojson,
	admin,
	territoires,
	meta,
	equipeMeta,
	locaux,
	test,
	autorisation,
	logger,
} from './router';
import errorHandler from './error';

export const app = new Koa();

app.use(
	compress({
		threshold: 2048,
		gzip: {
			flush: constants.Z_SYNC_FLUSH,
		},
		deflate: {
			flush: constants.Z_SYNC_FLUSH,
		},
		br: false, // disable brotli
	})
);
app.use(timestampLogger);
app.use(cookie());
app.use(conditional());
app.use(etag());
app.use(userAgent);

app.use(errorMiddleware);
app.use(json());
app.use(bodyParser({ jsonLimit: '50mb' }));
app.use(querystringBrackets);

// routes de l'API
app.use(auth.routes()).use(auth.allowedMethods());
app.use(admin.routes()).use(admin.allowedMethods());
app.use(compte.routes()).use(compte.allowedMethods());
app.use(contact.routes()).use(contact.allowedMethods());
app.use(equipe.routes()).use(equipe.allowedMethods());
app.use(etablissements.routes()).use(etablissements.allowedMethods());
app.use(entreprises.routes()).use(entreprises.allowedMethods());
app.use(etabCreas.routes()).use(etabCreas.allowedMethods());
app.use(health.routes()).use(health.allowedMethods());
app.use(geojson.routes()).use(geojson.allowedMethods());
app.use(territoires.routes()).use(territoires.allowedMethods());
app.use(meta.routes()).use(meta.allowedMethods());
app.use(equipeMeta.routes()).use(equipeMeta.allowedMethods());
app.use(locaux.routes()).use(locaux.allowedMethods());
app.use(autorisation.routes()).use(autorisation.allowedMethods());
app.use(logger.routes()).use(logger.allowedMethods());
app.use(test.routes()).use(test.allowedMethods());

// Application error logging.
app.on('error', errorHandler);
