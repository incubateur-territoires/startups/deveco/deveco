import 'dotenv/config';
import { drizzle, PostgresJsDatabase } from 'drizzle-orm/postgres-js';
import postgres, { Row } from 'postgres';

// import { MyLogger } from './_my-logger';
import * as contact from './schema/contact';
import * as admin from './schema/admin';
import * as eqEtab from './schema/equipe-etablissement';
import * as etablissement from './schema/etablissement';
import * as eqLoc from './schema/equipe-local';
import * as equipe from './schema/equipe';
import * as suivi from './schema/suivi';
import * as etabCrea from './schema/etablissement-creation';
import * as evenement from './schema/evenement';
import * as local from './schema/local';
import * as source from './schema/source';
import * as territoire from './schema/territoire';

const schema = {
	...contact,
	...admin,
	...eqEtab,
	...etablissement,
	...eqLoc,
	...etabCrea,
	...equipe,
	...suivi,
	...local,
	...evenement,
	...territoire,
	...source,
};

const dbProxyConfig = {
	connectionString: '',
	connectionOptions: {},
	db: null as PostgresJsDatabase<typeof schema> | null,
	queryClient: null as postgres.Sql<Record<string, unknown>> | null,
	async disconnect() {
		await this.queryClient?.end();
	},
	reconnect() {
		const queryClient = postgres(this.connectionString, this.connectionOptions);
		this.queryClient = queryClient;
		this.db = drizzle(queryClient, {
			schema,
			// logger: new MyLogger(),
		});
	},
	connect(connectionString: string, connectionOptions: Record<string, unknown>) {
		this.connectionString = connectionString;
		this.connectionOptions = connectionOptions;

		this.reconnect();
	},
	cursor<T extends Row = Row>(
		query: any,
		{ size }: { size: number }
	): AsyncIterable<NonNullable<T & Iterable<T>>[]> {
		if (!this.queryClient) {
			throw new Error("Le client de base de données n'est pas configuré");
		}
		// NonNullable<Row & Iterable<Row>>[]
		// NonNullable<T   & Iterable<T  >>[]

		/* eslint-disable-next-line @typescript-eslint/no-unsafe-call */
		const { sql, params } = query.toSQL();

		const q = this.queryClient.unsafe(sql, params);

		return q.cursor(size) as AsyncIterable<NonNullable<T & Iterable<T>>[]>;
	},
};

const dbProxy = new Proxy(dbProxyConfig, {
	get(target, name, receiver) {
		if (!Reflect.has(target, name)) {
			if (target.db) {
				/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
				return Reflect.get(target.db, name, receiver);
			}
			return undefined;
		}
		/* eslint-disable-next-line @typescript-eslint/no-unsafe-return */
		return Reflect.get(target, name, receiver);
	},
	set(target, name, value, receiver) {
		return Reflect.set(target, name, value, receiver);
	},
}) as PostgresJsDatabase<typeof schema> & typeof dbProxyConfig;

class DbService {
	private static instance: DbService;
	public db: typeof dbProxy;

	private constructor() {
		const connectionString = process.env.DATABASE_URL
			? process.env.DATABASE_URL
			: `postgresql://${process.env.POSTGRES_USER || process.env.POSTGRES_USERNAME}:${
					process.env.POSTGRES_PASSWORD
				}@${process.env.POSTGRES_HOST}:${process.env.POSTGRES_PORT}/${process.env.POSTGRES_DB}`;

		const connectionOptions = JSON.parse(process.env.POSTGRES_CONNECTION_OPTIONS ?? '{}');

		dbProxy.connect(connectionString, connectionOptions);

		this.db = dbProxy;
	}

	public static getInstance(): DbService {
		if (!DbService.instance) {
			DbService.instance = new DbService();
		}

		return DbService.instance;
	}
}

const instance = DbService.getInstance();

export default instance.db;
