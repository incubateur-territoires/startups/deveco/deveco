import { Logger } from 'drizzle-orm';
import { format } from 'date-fns';

function isDate(arg: unknown): arg is Date {
	return Object.prototype.toString.call(arg) === '[object Date]';
}

function paramFormat(p: unknown) {
	if (p === undefined || p === null) {
		return 'NULL';
	}

	// ne fonctionne pas
	// à ce stade, les dates sont converties en string
	if (isDate(p)) {
		const date = `'${format(p, 'yyyy-MM-dd H:mm:ss')}'`;
		return date;
	}

	if (typeof p === 'string') {
		return `'${p}'`;
	}

	return `${p}`;
}

function expandParametersInQuery(query: string, params: unknown[]): string {
	return params.reduce(
		(q: string, p: unknown, i) => q.replace(new RegExp(`\\$${i + 1}\\b`), paramFormat(p)),
		query
	);
}

export class MyLogger implements Logger {
	logQuery(query: string, params: unknown[]): void {
		const sql = expandParametersInQuery(query, params);

		console.info(sql);
	}
}
