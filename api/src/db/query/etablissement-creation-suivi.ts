import { and, eq, inArray, notInArray, isNull, isNotNull, getTableColumns } from 'drizzle-orm';

import db from '../../db';
import {
	EtablissementCreation,
	Equipe,
	EtablissementCreationRappel,
	EtablissementCreationBrouillon,
	Compte,
	Echange,
	Rappel,
	Demande,
	Brouillon,
} from '../../db/types';
import {
	etabCreaEchange,
	etabCreaDemande,
	etabCreaRappel,
	etabCreaBrouillon,
} from '../../db/schema/etablissement-creation';
import { echange, demande, rappel, brouillon } from '../../db/schema/suivi';
import { compte } from '../../db/schema/admin';

export const etabCreaEchangeSupprimeSub = notInArray(
	etabCreaEchange.echangeId,
	db
		.select({
			echangeId: etabCreaEchange.echangeId,
		})
		.from(etabCreaEchange)
		.innerJoin(echange, eq(echange.id, etabCreaEchange.echangeId))
		.where(isNotNull(echange.suppressionDate))
);

export const etabCreaDemandeSupprimeSub = notInArray(
	etabCreaDemande.demandeId,
	db
		.select({
			demandeId: etabCreaDemande.demandeId,
		})
		.from(etabCreaDemande)
		.innerJoin(demande, eq(demande.id, etabCreaDemande.demandeId))
		.where(isNotNull(demande.suppressionDate))
);

export const etabCreaRappelSupprimeSub = notInArray(
	etabCreaRappel.rappelId,
	db
		.select({
			rappelId: etabCreaRappel.rappelId,
		})
		.from(etabCreaRappel)
		.innerJoin(rappel, eq(rappel.id, etabCreaRappel.rappelId))
		.where(isNotNull(rappel.suppressionDate))
);

export const etabCreaBrouillonSupprimeSub = notInArray(
	etabCreaBrouillon.brouillonId,
	db
		.select({
			brouillonId: etabCreaBrouillon.brouillonId,
		})
		.from(etabCreaBrouillon)
		.innerJoin(brouillon, eq(brouillon.id, etabCreaBrouillon.brouillonId))
		.where(isNotNull(brouillon.suppressionDate))
);

export function etabCreaEchangeAdd({
	echangeId,
	etabCreaId,
	equipeId,
}: {
	equipeId: Equipe['id'];
	etabCreaId: EtablissementCreation['id'];
	echangeId: Echange['id'];
}) {
	return db.insert(etabCreaEchange).values({
		equipeId,
		etabCreaId,
		echangeId,
	});
}

export function etabCreaEchangeIdGetMany({
	etabCreaId,
	equipeId,
}: {
	etabCreaId: EtablissementCreation['id'];
	equipeId: Equipe['id'];
}) {
	return db
		.select({
			echangeId: etabCreaEchange.echangeId,
		})
		.from(etabCreaEchange)
		.where(and(eq(etabCreaEchange.etabCreaId, etabCreaId), eq(etabCreaEchange.equipeId, equipeId)));
}

export function etabCreaDemandeAdd({
	equipeId,
	etabCreaId,
	demandeId,
}: {
	equipeId: Equipe['id'];
	etabCreaId: EtablissementCreation['id'];
	demandeId: Demande['id'];
}) {
	return db.insert(etabCreaDemande).values({
		equipeId,
		etabCreaId,
		demandeId,
	});
}

export function etabCreaDemandeWithRelationGetMany({
	equipeId,
	etabCreaId,
	etabCreaIds,
}: {
	equipeId: Equipe['id'];
	etabCreaId?: EtablissementCreation['id'];
	etabCreaIds?: EtablissementCreation['id'][];
}) {
	const wheres = [eq(etabCreaDemande.equipeId, equipeId), isNull(demande.suppressionDate)];

	if (etabCreaIds) {
		wheres.push(
			inArray(etabCreaDemande.etabCreaId, etabCreaIds),
			eq(etabCreaDemande.equipeId, equipeId)
		);
	} else if (etabCreaId) {
		wheres.push(eq(etabCreaDemande.etabCreaId, etabCreaId), eq(etabCreaDemande.equipeId, equipeId));
	}

	return db
		.select({
			...getTableColumns(etabCreaDemande),
			demande,
		})
		.from(etabCreaDemande)
		.innerJoin(demande, eq(demande.id, etabCreaDemande.demandeId))
		.where(and(...wheres));
}

export function etabCreaDemandeIdGetMany({
	equipeId,
	etabCreaId,
}: {
	equipeId: Equipe['id'];
	etabCreaId: EtablissementCreation['id'];
}) {
	return db
		.select({
			demandeId: etabCreaDemande.demandeId,
		})
		.from(etabCreaDemande)
		.innerJoin(demande, eq(demande.id, etabCreaDemande.demandeId))
		.where(
			and(
				isNull(demande.suppressionDate),
				eq(etabCreaDemande.etabCreaId, etabCreaId),
				eq(etabCreaDemande.equipeId, equipeId)
			)
		);
}

export function etabCreaRappelAdd({
	rappelId,
	etabCreaId,
	equipeId,
}: {
	equipeId: Equipe['id'];
	etabCreaId: EtablissementCreation['id'];
	rappelId: Rappel['id'];
}) {
	return db.insert(etabCreaRappel).values({
		equipeId,
		etabCreaId,
		rappelId,
	});
}

export function etabCreaRappelIdGetMany({
	etabCreaId,
	equipeId,
}: {
	etabCreaId: EtablissementCreation['id'];
	equipeId: Equipe['id'];
}) {
	return db
		.select({
			rappelId: etabCreaRappel.rappelId,
		})
		.from(etabCreaRappel)
		.where(and(eq(etabCreaRappel.etabCreaId, etabCreaId), eq(etabCreaRappel.equipeId, equipeId)));
}

export async function etabCreaRappelWithRelationGetMany({
	equipeId,
	etabCreaId,
}: {
	equipeId: Equipe['id'];
	etabCreaId?: EtablissementCreation['id'];
}) {
	const where = etabCreaId
		? and(eq(etabCreaRappel.etabCreaId, etabCreaId), eq(etabCreaRappel.equipeId, equipeId))
		: eq(etabCreaRappel.equipeId, equipeId);

	const etabCreaRappels = (await db
		.select({
			...getTableColumns(etabCreaRappel),
			rappel: getTableColumns(rappel),
			affecte: compte,
		})
		.from(etabCreaRappel)
		.innerJoin(rappel, eq(rappel.id, etabCreaRappel.rappelId))
		.leftJoin(compte, eq(rappel.affecteCompteId, compte.id))
		.where(where)) as (EtablissementCreationRappel & {
		rappel: Rappel;
		affecte: Compte;
	})[];

	const rappels = etabCreaRappels.map(({ rappel, affecte, ...etabCreaRappel }) => ({
		...etabCreaRappel,
		rappel: {
			...rappel,
			affecte,
		},
	}));

	return rappels;
}

export function etabCreaBrouillonAdd({
	brouillonId,
	etabCreaId,
	equipeId,
}: {
	equipeId: Equipe['id'];
	etabCreaId: EtablissementCreation['id'];
	brouillonId: Brouillon['id'];
}) {
	return db.insert(etabCreaBrouillon).values({
		equipeId,
		etabCreaId,
		brouillonId,
	});
}

export function etabCreaBrouillonIdGetMany({
	etabCreaId,
	equipeId,
}: {
	etabCreaId: EtablissementCreation['id'];
	equipeId: Equipe['id'];
}) {
	return db
		.select({
			brouillonId: etabCreaBrouillon.brouillonId,
		})
		.from(etabCreaBrouillon)
		.where(
			and(eq(etabCreaBrouillon.etabCreaId, etabCreaId), eq(etabCreaBrouillon.equipeId, equipeId))
		);
}

export async function etabCreaBrouillonWithRelationGetMany({
	equipeId,
	etabCreaId,
}: {
	equipeId: Equipe['id'];
	etabCreaId?: EtablissementCreation['id'];
}) {
	const where = etabCreaId
		? and(eq(etabCreaBrouillon.etabCreaId, etabCreaId), eq(etabCreaBrouillon.equipeId, equipeId))
		: eq(etabCreaBrouillon.equipeId, equipeId);

	const etabCreaBrouillons = (await db
		.select({
			...getTableColumns(etabCreaBrouillon),
			brouillon: getTableColumns(brouillon),
			affecte: compte,
		})
		.from(etabCreaBrouillon)
		.innerJoin(brouillon, eq(brouillon.id, etabCreaBrouillon.brouillonId))
		.where(where)) as Array<
		EtablissementCreationBrouillon & {
			brouillon: Brouillon;
			affecte: Compte;
		}
	>;

	const brouillons = etabCreaBrouillons.map(({ brouillon, affecte, ...etabCreaBrouillon }) => ({
		...etabCreaBrouillon,
		brouillon: {
			...brouillon,
			affecte,
		},
	}));

	return brouillons;
}
