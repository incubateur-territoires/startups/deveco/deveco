import { sql, eq, and, inArray } from 'drizzle-orm';

import db from '../../db';
import { Etablissement, Equipe, EtablissementCreation } from '../types';
import {
	eqEtabDemande,
	eqEtabEchange,
	eqEtabRappel,
	eqEtabEtiquette,
} from '../../db/schema/equipe-etablissement';
import * as dbQueryEtablissementCreation from '../../db/query/etablissement-creation';

export async function eqEtabEtiquetteFromEtablissementCreationAddMany({
	equipeId,
	etablissementId,
	etabCreaId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	etabCreaId: EtablissementCreation['id'];
}) {
	const etabCreaEtiquetteIdsQuery = dbQueryEtablissementCreation
		.etabCreaEtiquetteIdGetMany({
			etabCreaId,
			equipeId,
		})
		.as('query');

	return db
		.insert(eqEtabEtiquette)
		.select(
			db
				.select({
					equipeId: sql<number>`${equipeId}`.as('equipeId'),
					etablissementId: sql<string>`${etablissementId}`.as('etablissementId'),
					etiquetteId: etabCreaEtiquetteIdsQuery.etiquetteId,
					auto: sql<boolean>`${false}`.as('auto'),
				})
				.from(etabCreaEtiquetteIdsQuery)
		)

		.onConflictDoNothing()
		.returning();
}

export async function eqEtabEchangeFromEtablissementCreationAddMany({
	equipeId,
	etablissementId,
	etabCreaId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	etabCreaId: EtablissementCreation['id'];
}) {
	const etabCreaEchangeIdsQuery = dbQueryEtablissementCreation
		.etabCreaEchangeIdGetMany({
			etabCreaId,
			equipeId,
		})
		.as('query');

	return db
		.insert(eqEtabEchange)
		.select(
			db
				.select({
					equipeId: sql<number>`${equipeId}`.as('equipeId'),
					etablissementId: sql<string>`${etablissementId}`.as('etablissementId'),
					echangeId: etabCreaEchangeIdsQuery.echangeId,
				})
				.from(etabCreaEchangeIdsQuery)
		)

		.onConflictDoNothing()
		.returning();
}

export async function eqEtabDemandeFromEtablissementCreationAddMany({
	equipeId,
	etablissementId,
	etabCreaId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	etabCreaId: EtablissementCreation['id'];
}) {
	const etabCreaDemandeIdsQuery = dbQueryEtablissementCreation
		.etabCreaDemandeIdGetMany({
			etabCreaId,
			equipeId,
		})
		.as('query');

	return db
		.insert(eqEtabDemande)
		.select(
			db
				.select({
					equipeId: sql<number>`${equipeId}`.as('equipeId'),
					etablissementId: sql<string>`${etablissementId}`.as('etablissementId'),
					demandeId: etabCreaDemandeIdsQuery.demandeId,
				})
				.from(etabCreaDemandeIdsQuery)
		)

		.onConflictDoNothing()
		.returning();
}

export async function eqEtabRappelFromEtablissementCreationAddMany({
	equipeId,
	etablissementId,
	etabCreaId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	etabCreaId: EtablissementCreation['id'];
}) {
	const etabCreaRappelIdsQuery = dbQueryEtablissementCreation
		.etabCreaRappelIdGetMany({
			etabCreaId,
			equipeId,
		})
		.as('query');

	return db
		.insert(eqEtabRappel)
		.select(
			db
				.select({
					equipeId: sql<number>`${equipeId}`.as('equipeId'),
					etablissementId: sql<string>`${etablissementId}`.as('etablissementId'),
					rappelId: etabCreaRappelIdsQuery.rappelId,
				})
				.from(etabCreaRappelIdsQuery)
		)

		.onConflictDoNothing()
		.returning();
}

export async function eqEtabEchangeFromEtablissementCreationRemoveMany({
	etabCreaId,
	etablissementId,
	equipeId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	etabCreaId: EtablissementCreation['id'];
}) {
	const etabCreaEchangeIdsQuery = dbQueryEtablissementCreation.etabCreaEchangeIdGetMany({
		etabCreaId,
		equipeId,
	});

	return db
		.delete(eqEtabEchange)
		.where(
			and(
				inArray(eqEtabEchange.echangeId, etabCreaEchangeIdsQuery),
				eq(eqEtabEchange.etablissementId, etablissementId),
				eq(eqEtabEchange.equipeId, equipeId)
			)
		);
}

export async function eqEtabDemandeFromEtablissementCreationRemoveMany({
	etabCreaId,
	etablissementId,
	equipeId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	etabCreaId: EtablissementCreation['id'];
}) {
	const etabCreaDemandeIdsQuery = dbQueryEtablissementCreation.etabCreaDemandeIdGetMany({
		etabCreaId,
		equipeId,
	});

	return db
		.delete(eqEtabDemande)
		.where(
			and(
				inArray(eqEtabDemande.demandeId, etabCreaDemandeIdsQuery),
				eq(eqEtabDemande.etablissementId, etablissementId),
				eq(eqEtabDemande.equipeId, equipeId)
			)
		)
		.returning();
}

export async function eqEtabRappelFromEtablissementCreationRemoveMany({
	equipeId,
	etablissementId,
	etabCreaId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	etabCreaId: EtablissementCreation['id'];
}) {
	const etabCreaRappelIdsQuery = dbQueryEtablissementCreation.etabCreaRappelIdGetMany({
		etabCreaId,
		equipeId,
	});

	return db
		.delete(eqEtabRappel)
		.where(
			and(
				inArray(eqEtabRappel.rappelId, etabCreaRappelIdsQuery),
				eq(eqEtabRappel.etablissementId, etablissementId),
				eq(eqEtabRappel.equipeId, equipeId)
			)
		);
}
