import { inArray, eq, desc, gte, and, sql, count, lt, getTableColumns } from 'drizzle-orm';

import db from '..';
import {
	evenement,
	actionCompte,
	actionEquipeEtablissementContact,
	actionDemande,
	actionEchange,
	actionEntreprise,
	actionEtiquetteEtablissement,
	actionEtiquetteEtablissementCreation,
	actionEquipeEtablissement,
	actionEtablissementCreation,
	actionEtablissementCreationCreateur,
	actionZonage,
	actionEtiquette,
	actionEquipeLocal,
	actionContact,
	actionEquipeLocalContact,
	actionEquipeLocalOccupant,
	actionRappel,
	actionBrouillon,
	actionEquipe,
	actionEtiquetteLocal,
} from '../../db/schema/evenement';
import { deveco } from '../../db/schema/equipe';
import { compte } from '../../db/schema/admin';
import { etabCrea } from '../../db/schema/etablissement-creation';
import {
	Entreprise,
	Etablissement,
	Compte,
	Demande,
	Echange,
	Equipe,
	EtablissementCreation,
	Etiquette,
	Evenement,
	Contact,
	ActionTypeId,
	ActionDiff,
	New,
	Local,
	Zonage,
	EtablissementCreationCreateur,
} from '../../db/types';

type ActionEntrepriseTypeId =
	| 'exercices'
	| 'mandataires_sociaux'
	| 'beneficiaires_effectifs'
	| 'liasse_fiscale';

interface ActionEntrepriseDiff {
	message:
		| 'mise à jour des exercices'
		| 'mise à jour des mandataires sociaux'
		| 'mise à jour des bénéficiaires effectifs'
		| 'mise à jour de la liasse fiscale';
}

export function evenementConnexionLastGet({ compteId }: { compteId: Compte['id'] }) {
	return db.query.evenement.findFirst({
		where: and(eq(evenement.compteId, compteId), eq(evenement.typeId, 'connexion')),
		orderBy: desc(evenement.createdAt),
	});
}

export function evenementAddMany(values: Pick<Evenement, 'compteId' | 'typeId'>[]) {
	return db.insert(evenement).values(values).returning({ id: evenement.id });
}

export function evenementAdd(value: Pick<Evenement, 'compteId' | 'typeId'>) {
	return evenementAddMany([value]);
}

export function actionEquipeAdd({
	evenementId,
	equipeId,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	typeId: ActionTypeId;
	diff?: ActionDiff;
}) {
	return db
		.insert(actionEquipe)
		.values({
			evenementId,
			equipeId,
			typeId,
			diff,
		})
		.returning();
}

export function actionCompteAdd({
	evenementId,
	typeId,
	diff,
	compteId,
}: {
	evenementId: Evenement['id'];
	compteId: Compte['id'];
	typeId: ActionTypeId;
	diff?: ActionDiff;
}) {
	return db
		.insert(actionCompte)
		.values({
			evenementId,
			typeId,
			compteId,
			diff,
		})
		.returning();
}

export function actionEquipeEtablissementContactAdd({
	evenementId,
	equipeId,
	contactId,
	etablissementId,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	contactId: Contact['id'];
	etablissementId: string;
	typeId: ActionTypeId;
	diff?: ActionDiff;
}) {
	return db
		.insert(actionEquipeEtablissementContact)
		.values({
			evenementId,
			equipeId,
			contactId,
			etablissementId,
			typeId,
			diff,
		})
		.returning();
}

export function actionEtablissementCreationCreateurAdd({
	evenementId,
	equipeId,
	contactId,
	etabCreaId,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	equipeId: EtablissementCreationCreateur['equipeId'];
	contactId: EtablissementCreationCreateur['contactId'];
	etabCreaId: EtablissementCreationCreateur['etabCreaId'];
	typeId: ActionTypeId;
	diff?: ActionDiff;
}) {
	return db
		.insert(actionEtablissementCreationCreateur)
		.values({
			evenementId,
			equipeId,
			contactId,
			etabCreaId,
			typeId,
			diff,
		})
		.returning();
}

export function actionEquipeEtablissementContactCountByEquipeAndActionTypeId({
	equipeId,
	typeIds,
	debut,
}: {
	equipeId: Equipe['id'];
	typeIds: ActionTypeId[];
	debut?: Date;
}) {
	const andConditions = [
		inArray(actionEquipeEtablissementContact.typeId, typeIds),
		eq(deveco.equipeId, equipeId),
	];

	if (debut) {
		andConditions.push(gte(evenement.createdAt, debut));
	}

	return db
		.select({ count: count() })
		.from(actionEquipeEtablissementContact)
		.innerJoin(evenement, eq(evenement.id, actionEquipeEtablissementContact.evenementId))
		.innerJoin(deveco, eq(deveco.compteId, evenement.compteId))
		.where(and(...andConditions));
}

export function actionEquipeEtablissementAffichageCountByEquipe({
	equipeId,
	debut,
}: {
	equipeId: Equipe['id'];
	debut?: Date;
}) {
	const andConditions = [
		eq(actionEquipeEtablissement.typeId, 'affichage'),
		eq(deveco.equipeId, equipeId),
	];

	if (debut) {
		andConditions.push(gte(actionEquipeEtablissement.createdAt, debut));
	}

	return db
		.select({ count: count() })
		.from(actionEquipeEtablissement)
		.innerJoin(evenement, eq(evenement.id, actionEquipeEtablissement.evenementId))
		.innerJoin(deveco, eq(deveco.compteId, evenement.compteId))
		.where(and(...andConditions));
}

export function actionDemandeAdd({
	evenementId,
	demandeId,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	demandeId: Demande['id'];
	typeId: ActionTypeId;
	diff: ActionDiff;
}) {
	return db
		.insert(actionDemande)
		.values({
			evenementId,
			typeId,
			demandeId,
			diff,
		})
		.returning();
}

export function actionEchangeAdd({
	evenementId,
	echangeId,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	echangeId: Echange['id'];
	typeId: ActionTypeId;
	diff: ActionDiff;
}) {
	return db
		.insert(actionEchange)
		.values({
			evenementId,
			typeId,
			echangeId,
			diff,
		})
		.returning();
}

export function actionEchangeGetManyByEquipeAndActionTypeId({
	equipeId,
	typeIds,
	debut,
}: {
	equipeId: Equipe['id'];
	typeIds: ActionTypeId[];
	debut?: Date;
}): Promise<{ echangeId: Echange['id'] }[]> {
	const andConditions = [inArray(actionEchange.typeId, typeIds), eq(deveco.equipeId, equipeId)];

	if (debut) {
		andConditions.push(gte(actionEchange.createdAt, debut));
	}

	return db
		.select({
			echangeId: actionEchange.echangeId,
		})
		.from(actionEchange)
		.innerJoin(evenement, eq(evenement.id, actionEchange.evenementId))
		.innerJoin(deveco, eq(deveco.compteId, evenement.compteId))
		.where(and(...andConditions));
}

export function actionEchangeCountByEquipeAndActionTypeId({
	equipeId,
	typeIds,
	debut,
}: {
	equipeId: Equipe['id'];
	typeIds: ActionTypeId[];
	debut?: Date;
}) {
	const andConditions = [inArray(actionEchange.typeId, typeIds), eq(deveco.equipeId, equipeId)];

	if (debut) {
		andConditions.push(gte(actionEchange.createdAt, debut));
	}

	return db
		.select({ count: count() })
		.from(actionEchange)
		.innerJoin(evenement, eq(evenement.id, actionEchange.evenementId))
		.innerJoin(deveco, eq(deveco.compteId, evenement.compteId))
		.where(and(...andConditions));
}

export function actionEntrepriseAdd({
	evenementId,
	entrepriseId,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	entrepriseId: Entreprise['siren'];
	typeId: ActionTypeId;
	diff: ActionEntrepriseDiff;
}) {
	return db
		.insert(actionEntreprise)
		.values({
			evenementId,
			typeId,
			entrepriseId,
			diff,
		})
		.returning();
}

export function actionEntrepriseForApiEntrepriseAdd({
	evenementId,
	entrepriseId,
	type,
}: {
	evenementId: Evenement['id'];
	entrepriseId: Entreprise['siren'];
	type: ActionEntrepriseTypeId;
}) {
	const message =
		type === 'exercices'
			? `mise à jour des exercices`
			: type === 'mandataires_sociaux'
				? 'mise à jour des mandataires sociaux'
				: type === 'liasse_fiscale'
					? 'mise à jour de la liasse fiscale'
					: 'mise à jour des bénéficiaires effectifs';

	return actionEntrepriseAdd({
		entrepriseId,
		typeId: 'modification',
		diff: {
			message,
		},
		evenementId,
	});
}

export function actionEtablissementCreationAdd({
	evenementId,
	etabCreaId,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	etabCreaId: EtablissementCreation['id'];
	typeId: ActionTypeId;
	diff?: ActionDiff;
}) {
	return db
		.insert(actionEtablissementCreation)
		.values({
			evenementId,
			typeId,
			etabCreaId,
			diff,
		})
		.returning();
}

export function actionEtablissementCreationAddMany({
	evenementId,
	etabCreaIds,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	etabCreaIds: EtablissementCreation['id'][];
	typeId: ActionTypeId;
	diff?: ActionDiff;
}) {
	const actionEtablissementCreationValues = etabCreaIds.map((etabCreaId) => ({
		evenementId,
		typeId,
		etabCreaId,
		diff,
	}));

	return db
		.insert(actionEtablissementCreation)
		.values(actionEtablissementCreationValues)
		.returning();
}

export function actionEtablissementCreationCountByEquipeAndActionTypeId({
	equipeId,
	typeIds,
	debut,
}: {
	equipeId: Equipe['id'];
	typeIds: ActionTypeId[];
	debut?: Date;
}) {
	const andConditions = [
		inArray(actionEtablissementCreation.typeId, typeIds),
		eq(deveco.equipeId, equipeId),
	];

	if (debut) {
		andConditions.push(gte(actionEtablissementCreation.createdAt, debut));
	}

	return db
		.select({ count: count() })
		.from(actionEtablissementCreation)
		.innerJoin(evenement, eq(evenement.id, actionEtablissementCreation.evenementId))
		.innerJoin(deveco, eq(deveco.compteId, evenement.compteId))
		.where(and(...andConditions));
}

export function actionEtablissementCreationAffichageCount({
	equipeId,
	debut,

	fin,
}: {
	equipeId: Equipe['id'];
	debut: Date;
	fin: Date;
}) {
	return db
		.select({ count: count() })
		.from(actionEtablissementCreation)
		.innerJoin(
			etabCrea,
			and(
				eq(etabCrea.id, actionEtablissementCreation.etabCreaId),
				eq(etabCrea.equipeId, equipeId),
				eq(etabCrea.actif, true)
			)
		)
		.where(
			and(
				eq(actionEtablissementCreation.typeId, 'affichage'),
				gte(actionEtablissementCreation.createdAt, debut),
				lt(actionEtablissementCreation.createdAt, fin)
			)
		);
}

export function actionEtablissementCreationGetManyByTypeId({
	equipeId,
	typeId,
}: {
	typeId: ActionTypeId;
	equipeId: Equipe['id'];
}) {
	return db
		.selectDistinctOn([actionEtablissementCreation.etabCreaId], {
			etabCreaId: actionEtablissementCreation.etabCreaId,
			date: actionEtablissementCreation.createdAt,
		})
		.from(actionEtablissementCreation)
		.innerJoin(
			etabCrea,
			and(
				eq(etabCrea.id, actionEtablissementCreation.etabCreaId),
				eq(etabCrea.equipeId, equipeId),
				eq(etabCrea.actif, true)
			)
		)
		.where(and(eq(actionEtablissementCreation.typeId, typeId), eq(etabCrea.actif, true)))
		.orderBy(
			sql`${actionEtablissementCreation.etabCreaId}, ${actionEtablissementCreation.createdAt} desc nulls last`
		);
}

export function actionZonageAdd({
	evenementId,
	zonageId,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	zonageId: Zonage['id'];
	typeId: ActionTypeId;
	diff?: ActionDiff;
}) {
	return db
		.insert(actionZonage)
		.values({
			evenementId,
			zonageId,
			typeId,
			diff,
		})
		.returning();
}

export function actionEtiquetteAddMany({
	evenementId,
	etiquetteIds,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	etiquetteIds: Etiquette['id'][];
	typeId: ActionTypeId;
	diff: ActionDiff;
}) {
	if (!etiquetteIds.length) {
		return [];
	}

	const actionEtiquetteValues = etiquetteIds.map((etiquetteId) => ({
		evenementId,
		etiquetteId,
		typeId,
		diff,
	}));

	return db.insert(actionEtiquette).values(actionEtiquetteValues).returning();
}

export function actionEtiquetteEtablissementAddMany({
	evenementId,
	equipeId,
	etablissementIds,
	etiquettesAjoutees: etiquetteAjoutees,
	etiquettesSupprimees: etiquetteSupprimees,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	etablissementIds: Etablissement['siret'][];
	etiquettesAjoutees: Etiquette[];
	etiquettesSupprimees: Etiquette[];
}) {
	if (!etablissementIds.length) {
		return [];
	}

	if (!etiquetteAjoutees.length && !etiquetteSupprimees.length) {
		return [];
	}

	const messageAjout = etiquetteAjoutees.length ? `Ajout d'étiquettes` : '';
	const messageSuppression = etiquetteSupprimees.length ? `Suppression d'étiquettes` : '';

	// le plan maléfique :
	// - on crée un événement d'ajout/suppression de "masse" pour une "masse" d'établissementIds
	// - on lie cet unique événement à X événements individuels d'ajout/suppression d'UNE étiquette à UN établissement
	// - on crée un événement d'ajout/suppression de "masse" d'étiquettes pour une "masse" d'établissementIds
	const diff = {
		message: `Qualification d'établissements : ${etablissementIds.join(', ')}`,
		changes: {
			message: `Modification des étiquettes :
${messageAjout}
${messageSuppression}`,
			after: etiquetteAjoutees.length ? etiquetteAjoutees.map(({ id }) => id) : null,
			before: etiquetteSupprimees.length ? etiquetteSupprimees.map(({ id }) => id) : null,
		},
	};

	// - on lie cet unique événement à X événements individuels d'ajout/suppression d'UNE étiquette à UN établissement
	const actionEtiquetteEtablissementValues = etablissementIds.reduce(
		(values: New<typeof actionEtiquetteEtablissement>[], etablissementId) => {
			const ajoutEvenements = etiquetteAjoutees.map(({ id }) => ({
				evenementId,
				equipeId,
				etablissementId,
				etiquetteId: id,
				typeId: 'modification' as const,
				diff,
			}));
			const suppressionEvenements = etiquetteSupprimees.map(({ id }) => ({
				evenementId,
				equipeId,
				etablissementId,
				etiquetteId: id,
				typeId: 'modification' as const,
				diff,
			}));

			return [...values, ...ajoutEvenements, ...suppressionEvenements];
		},
		[]
	);

	return db
		.insert(actionEtiquetteEtablissement)
		.values(actionEtiquetteEtablissementValues)
		.returning();
}

export function actionEtiquetteEtablissementCreationAddMany({
	evenementId,
	etabCreaIds,
	etiquettesAjoutees: etiquetteAjoutees,
	etiquettesSupprimees: etiquetteSupprimees,
}: {
	evenementId: Evenement['id'];
	etabCreaIds: EtablissementCreation['id'][];
	etiquettesAjoutees: Etiquette[];
	etiquettesSupprimees: Etiquette[];
}) {
	if (!etiquetteAjoutees.length && !etiquetteSupprimees.length) {
		return [];
	}

	const typeId: ActionTypeId = 'modification';

	// - on lie cet unique événement à X actions individuelles d'ajout/suppression d'UNE étiquette à UNE création d'établissement
	const actionEtiquetteEtablissementCreationValues = etabCreaIds.reduce(
		(
			values: {
				evenementId: Evenement['id'];
				etiquetteId: Etiquette['id'];
				etabCreaId: EtablissementCreation['id'];
				typeId: ActionTypeId;
			}[],
			etabCreaId
		) => {
			const ajoutEvenements = etiquetteAjoutees.map(({ id }) => ({
				evenementId,
				etiquetteId: id,
				etabCreaId,
				typeId,
			}));
			const suppressionEvenements = etiquetteSupprimees.map(({ id }) => ({
				evenementId,
				etiquetteId: id,
				etabCreaId,
				typeId,
			}));

			return [...values, ...ajoutEvenements, ...suppressionEvenements];
		},
		[]
	);

	return db
		.insert(actionEtiquetteEtablissementCreation)
		.values(actionEtiquetteEtablissementCreationValues)
		.returning();
}

export function actionEtiquetteEtablissementGetManyFromDate({
	equipeId,
	debut,
	fin,
}: {
	equipeId: Equipe['id'];
	debut: Date;
	fin: Date;
}) {
	return db
		.select({
			evenementId: actionEtiquetteEtablissement.evenementId,
		})
		.from(actionEtiquetteEtablissement)
		.where(
			and(
				eq(actionEtiquetteEtablissement.equipeId, equipeId),
				gte(actionEtiquetteEtablissement.createdAt, debut),
				lt(actionEtiquetteEtablissement.createdAt, fin)
			)
		);
}

export function actionEtiquetteLocalGetManyFromDate({
	equipeId,
	debut,
	fin,
}: {
	equipeId: Equipe['id'];
	debut: Date;
	fin: Date;
}) {
	return db
		.select({
			evenementId: actionEtiquetteLocal.evenementId,
		})
		.from(actionEtiquetteLocal)
		.where(
			and(
				eq(actionEtiquetteLocal.equipeId, equipeId),
				gte(actionEtiquetteLocal.createdAt, debut),
				lt(actionEtiquetteLocal.createdAt, fin)
			)
		);
}

export function actionEtiquetteEtablissementCreationCount({
	equipeId,
	debut,
	fin,
}: {
	equipeId: Equipe['id'];
	debut: Date;
	fin: Date;
}) {
	return db
		.select({ count: count() })
		.from(actionEtiquetteEtablissementCreation)
		.innerJoin(
			etabCrea,
			and(
				eq(etabCrea.id, actionEtiquetteEtablissementCreation.etabCreaId),
				eq(etabCrea.equipeId, equipeId),
				eq(etabCrea.actif, true)
			)
		)
		.where(
			and(
				gte(actionEtiquetteEtablissementCreation.createdAt, debut),
				lt(actionEtiquetteEtablissementCreation.createdAt, fin)
			)
		);
}

export function actionEquipeEtablissementAdd({
	evenementId,
	equipeId,
	etablissementId,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	typeId: ActionTypeId;
	diff?: ActionDiff;
}) {
	return db.insert(actionEquipeEtablissement).values({
		evenementId,
		equipeId,
		etablissementId,
		typeId,
		diff,
	});
}

export function actionEquipeEtablissementAddMany({
	evenementId,
	equipeId,
	etablissementIds,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	etablissementIds: Etablissement['siret'][];
	typeId: ActionTypeId;
	diff?: ActionDiff;
}) {
	if (!etablissementIds.length) {
		return [];
	}

	const actionEquipeEtablissementValues = etablissementIds.map((etablissementId) => ({
		evenementId,
		equipeId,
		etablissementId,
		typeId,
		diff,
	}));

	return db.insert(actionEquipeEtablissement).values(actionEquipeEtablissementValues).returning();
}

export function actionEquipeEtablissementAffichageGetManyFromDate({
	equipeId,
	debut,
	fin,
}: {
	equipeId: Equipe['id'];
	debut: Date;
	fin: Date;
}) {
	return db
		.select({
			evenementId: actionEquipeEtablissement.evenementId,
		})
		.from(actionEquipeEtablissement)
		.innerJoin(evenement, eq(evenement.id, actionEquipeEtablissement.evenementId))
		.where(
			and(
				eq(actionEquipeEtablissement.equipeId, equipeId),
				eq(actionEquipeEtablissement.typeId, 'affichage'),
				gte(evenement.createdAt, debut),
				lt(evenement.createdAt, fin)
			)
		);
}

export function actionEquipeEtablissementGetMany({
	equipeId,
	etablissementId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
}) {
	return db
		.select({
			...getTableColumns(actionEquipeEtablissement),
			compte,
		})
		.from(actionEquipeEtablissement)
		.innerJoin(evenement, eq(evenement.id, actionEquipeEtablissement.evenementId))
		.innerJoin(compte, eq(compte.id, evenement.compteId))
		.where(
			and(
				eq(actionEquipeEtablissement.equipeId, equipeId),
				eq(actionEquipeEtablissement.etablissementId, etablissementId)
			)
		);
}

export function actionEquipeEtablissementAjouteAuPortefeuilleCountByEquipe({
	equipeId,
	debut,
}: {
	equipeId: Equipe['id'];
	debut?: Date;
}) {
	const andConditions = [
		eq(actionEquipeEtablissement.typeId, 'modification'),
		eq(deveco.equipeId, equipeId),
	];

	if (debut) {
		andConditions.push(gte(evenement.createdAt, debut));
	}

	return db
		.select({ count: count() })
		.from(actionEquipeEtablissement)
		.innerJoin(evenement, eq(evenement.id, actionEquipeEtablissement.evenementId))
		.innerJoin(deveco, eq(deveco.compteId, evenement.compteId))
		.where(and(...andConditions));
}

export function actionEquipeLocalAdd({
	evenementId,
	equipeId,
	localId,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	localId: Local['id'];
	typeId: ActionTypeId;
	diff?: ActionDiff;
}) {
	return db
		.insert(actionEquipeLocal)
		.values({
			evenementId,
			equipeId,
			localId,
			typeId,
			diff,
		})
		.returning();
}

export function actionContactAdd({
	evenementId,
	contactId,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	contactId: Contact['id'];
	typeId: ActionTypeId;
	diff?: ActionDiff;
}) {
	return db
		.insert(actionContact)
		.values({
			evenementId,
			typeId,
			contactId,
			diff,
		})
		.returning();
}

export function actionEquipeLocalContactAdd({
	evenementId,
	equipeId,
	localId,
	contactId,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	localId: Local['id'];
	contactId: Contact['id'];
	typeId: ActionTypeId;
	diff?: ActionDiff;
}) {
	return db.insert(actionEquipeLocalContact).values({
		evenementId,
		equipeId,
		localId,
		contactId,
		typeId,
		diff,
	});
}

export function actionEquipeLocalOccupantAdd({
	evenementId,
	equipeId,
	localId,
	etablissementId,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	localId: Local['id'];
	etablissementId: Etablissement['siret'];
	typeId: ActionTypeId;
	diff?: ActionDiff;
}) {
	return db.insert(actionEquipeLocalOccupant).values({
		evenementId,
		equipeId,
		localId,
		etablissementId,
		typeId,
		diff,
	});
}

export function actionRappelAdd({
	evenementId,
	rappelId,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	rappelId: number;
	typeId: ActionTypeId;
	diff: ActionDiff;
}) {
	return db
		.insert(actionRappel)
		.values({
			evenementId,
			typeId,
			rappelId,
			diff,
		})
		.returning();
}

export function actionRappelCountActionTypeId({
	equipeId,
	typeIds,
	debut,
}: {
	equipeId: Equipe['id'];
	typeIds: ActionTypeId[];
	debut?: Date;
}) {
	const andConditions = [inArray(actionRappel.typeId, typeIds), eq(deveco.equipeId, equipeId)];

	if (debut) {
		andConditions.push(gte(evenement.createdAt, debut));
	}

	return db
		.select({ count: count() })
		.from(actionRappel)
		.innerJoin(evenement, eq(evenement.id, actionRappel.evenementId))
		.innerJoin(deveco, eq(deveco.compteId, evenement.compteId))
		.where(and(...andConditions));
}

export function actionBrouillonAdd({
	evenementId,
	brouillonId,
	typeId,
	diff,
}: {
	evenementId: Evenement['id'];
	brouillonId: number;
	typeId: ActionTypeId;
	diff: ActionDiff;
}) {
	return db
		.insert(actionBrouillon)
		.values({
			evenementId,
			typeId,
			brouillonId,
			diff,
		})
		.returning();
}

export function actionBrouillonCountActionTypeId({
	equipeId,
	typeIds,
	debut,
}: {
	equipeId: Equipe['id'];
	typeIds: ActionTypeId[];
	debut?: Date;
}) {
	const andConditions = [inArray(actionBrouillon.typeId, typeIds), eq(deveco.equipeId, equipeId)];

	if (debut) {
		andConditions.push(gte(evenement.createdAt, debut));
	}

	return db
		.select({ count: count() })
		.from(actionBrouillon)
		.innerJoin(evenement, eq(evenement.id, actionBrouillon.evenementId))
		.innerJoin(deveco, eq(deveco.compteId, evenement.compteId))
		.where(and(...andConditions));
}

export function actionEtiquetteForEntiteCountByEquipeAndActionTypeId({
	equipeId,
	tableName,
	typeIds,
	debut,
}: {
	equipeId: Equipe['id'];
	tableName: 'eqEtab' | 'etabCrea';
	typeIds: ActionTypeId[];
	debut?: Date;
}) {
	const table =
		tableName === 'eqEtab' ? actionEtiquetteEtablissement : actionEtiquetteEtablissementCreation;

	const andConditions = [inArray(table.typeId, typeIds)];

	if (debut) {
		andConditions.push(gte(evenement.createdAt, debut));
	}

	return db
		.select({ count: count() })
		.from(table)
		.innerJoin(evenement, eq(evenement.id, table.evenementId))
		.innerJoin(deveco, and(eq(deveco.compteId, evenement.compteId), eq(deveco.equipeId, equipeId)))
		.where(and(...andConditions));
}

export function actionEtiquetteLocalAddMany({
	evenementId,
	equipeId,
	localIds,
	etiquettesAjoutees: etiquetteAjoutees,
	etiquettesSupprimees: etiquetteSupprimees,
}: {
	evenementId: Evenement['id'];
	equipeId: Equipe['id'];
	localIds: Local['id'][];
	etiquettesAjoutees: Etiquette[];
	etiquettesSupprimees: Etiquette[];
}) {
	if (!localIds.length) {
		return [];
	}

	if (!etiquetteAjoutees.length && !etiquetteSupprimees.length) {
		return [];
	}

	const messageAjout = etiquetteAjoutees.length ? `Ajout d'étiquettes` : '';
	const messageSuppression = etiquetteSupprimees.length ? `Suppression d'étiquettes` : '';

	// le plan maléfique :
	// - on crée un événement d'ajout/suppression de "masse" pour une "masse" de localIds
	// - on lie cet unique événement à X événements individuels d'ajout/suppression d'UNE étiquette à UN local
	// - on crée un événement d'ajout/suppression de "masse" d'étiquettes pour une "masse" de localIds
	const diff = {
		message: `Qualification de locaux : ${localIds.join(', ')}`,
		changes: {
			message: `Modification des étiquettes :
${messageAjout}
${messageSuppression}`,
			after: etiquetteAjoutees.length ? etiquetteAjoutees.map(({ id }) => id) : null,
			before: etiquetteSupprimees.length ? etiquetteSupprimees.map(({ id }) => id) : null,
		},
	};

	// - on lie cet unique événement à X événements individuels d'ajout/suppression d'UNE étiquette à UN local
	const actionEtiquetteLocalValues = localIds.reduce(
		(values: New<typeof actionEtiquetteLocal>[], localId) => {
			const ajoutEvenements = etiquetteAjoutees.map(({ id }) => ({
				evenementId,
				equipeId,
				localId,
				etiquetteId: id,
				typeId: 'modification' as const,
				diff,
			}));
			const suppressionEvenements = etiquetteSupprimees.map(({ id }) => ({
				evenementId,
				equipeId,
				localId,
				etiquetteId: id,
				typeId: 'modification' as const,
				diff,
			}));

			return [...values, ...ajoutEvenements, ...suppressionEvenements];
		},
		[]
	);

	return db.insert(actionEtiquetteLocal).values(actionEtiquetteLocalValues).returning();
}
