import { eq, ne, and, or, ilike, like, inArray, exists, sql } from 'drizzle-orm';

import db from '..';
import {
	commune,
	departement,
	epci,
	metropole,
	petr,
	territoireIndustrie,
	region,
} from '../schema/territoire';
import {
	laposteCodeInseeCodePostal,
	etalabCommuneContour,
	etalabRegionContour,
} from '../schema/source';
import { equipe, equipeCommuneVue, etiquette, zonage } from '../schema/equipe';
import { qpv2024 } from '../schema/contact';
import {
	Equipe,
	Region,
	Departement,
	Epci,
	TerritoireIndustrie,
	Petr,
	Metropole,
	Commune,
	Qpv2024,
	Etiquette,
	Contour,
} from '../types';
import { dbQuoteEscape } from '../../lib/utils';
import { rechercheBuild } from '../../db/utils';

export function etalabContourGetManyByEquipe({ equipeId }: { equipeId: Equipe['id'] }) {
	return db
		.select({
			id: sql<string>`'territoire'`,
			nom: equipe.nom,
			geometrie: sql<string>`ST_AsGeoJSON(
				ST_Union(${etalabCommuneContour.wkbGeometry})
			)`,
			box: sql<string>`ST_AsGeoJSON(
				ST_Extent(${etalabCommuneContour.wkbGeometry})
			)`,
			surface: sql<number>`ST_Area(
				ST_Transform(
					ST_Union(${etalabCommuneContour.wkbGeometry}),
					3857
				)
			)`,
		})
		.from(equipeCommuneVue)
		.innerJoin(equipe, eq(equipe.id, equipeCommuneVue.equipeId))
		.innerJoin(etalabCommuneContour, eq(etalabCommuneContour.code, equipeCommuneVue.communeId))
		.where(eq(equipeCommuneVue.equipeId, equipeId))
		.groupBy(equipe.id);
}

let contourFrance: Contour | null = null;

export async function etalabContourFranceGet() {
	if (contourFrance) {
		return [contourFrance];
	}

	const [contour] = await db
		.select({
			id: sql<string>`'territoire'`,
			nom: sql<string>`'France'`,
			geometrie: sql<string>`ST_AsGeoJSON(
				ST_SimplifyPreserveTopology(
					ST_Union(${etalabRegionContour.wkbGeometry}, 0.01),
					0.001
				)
			)`,
			box: sql<string>`ST_AsGeoJSON(
				ST_Extent(${etalabRegionContour.wkbGeometry})
			)`,
			surface: sql<number>`ST_Area(
				ST_Transform(
					ST_Union(${etalabRegionContour.wkbGeometry}, 0.01),
					3857
				)
			)`,
		})
		.from(etalabRegionContour)
		.where(ne(etalabRegionContour.code, '984'));

	contourFrance = contour;

	return [contourFrance];
}

export function contourQpvGetMany({
	qpvIds,
	simplifyTolerance,
	nwse,
}: {
	qpvIds: Qpv2024['id'][] | null;
	simplifyTolerance?: number | null;
	nwse?: number[];
}) {
	const query = db
		.select({
			id: qpv2024.id,
			nom: qpv2024.nom,
			geometrie: simplifyTolerance
				? sql<string>`ST_AsGeoJSON(ST_SimplifyPreserveTopology(${qpv2024.geometrie}, ${simplifyTolerance}))`
				: sql<string>`ST_AsGeoJSON(${qpv2024.geometrie})`,
			box: sql<string>`ST_AsGeoJSON(ST_Extent(${qpv2024.geometrie}))`,
			surface: sql<number>`ST_Area(ST_Transform(${qpv2024.geometrie}, 3857))`,
		})
		.from(qpv2024)
		.groupBy(qpv2024.id);

	const wheres = [];

	if (qpvIds?.length) {
		wheres.push(inArray(qpv2024.id, qpvIds));
	}

	if (nwse?.length === 4) {
		const [north, west, south, east] = nwse;

		wheres.push(
			sql`ST_Intersects(${qpv2024.geometrie}, ST_MakeEnvelope(${west}, ${south}, ${east}, ${north}, 4326))`
		);
	}

	if (wheres.length) {
		query.where(and(...wheres));
	}

	return query;
}

export function contourZonageGetMany({
	equipeId,
	etiquetteNoms,
	nwse,
}: {
	equipeId: Etiquette['equipeId'];
	etiquetteNoms: Etiquette['nom'][] | null;
	nwse?: number[];
}) {
	const query = db
		.select({
			id: sql<string>`${zonage.id}::text`,
			nom: etiquette.nom,
			geometrie: sql<string>`ST_AsGeoJSON(${zonage.geometrie})`,
			box: sql<string>`ST_AsGeoJSON(ST_Extent(${zonage.geometrie}))`,
			surface: sql<number>`ST_Area(ST_Transform(${zonage.geometrie}, 3857))`,
		})
		.from(etiquette)
		.innerJoin(zonage, eq(zonage.etiquetteId, etiquette.id))
		.groupBy(zonage.id, etiquette.nom);

	const wheres = [eq(etiquette.equipeId, equipeId)];

	if (etiquetteNoms?.length) {
		wheres.push(eq(etiquette.equipeId, equipeId), inArray(etiquette.nom, etiquetteNoms));
	}

	if (nwse?.length === 4) {
		const [north, west, south, east] = nwse;

		wheres.push(
			sql`ST_Intersects(${zonage.geometrie}, ST_MakeEnvelope(${west}, ${south}, ${east}, ${north}, 4326))`
		);
	}

	query.where(and(...wheres));

	return query;
}

export function communeGet({ communeId }: { communeId: string }) {
	return db.query.commune.findFirst({
		where: eq(commune.id, communeId),
	});
}

export function communeGetMany({ communeIds }: { communeIds: Commune['id'][] | null }) {
	const query = db.select().from(commune);

	if (communeIds?.length) {
		query.where(inArray(commune.id, communeIds));
	}

	return query;
}

export function communeGetManyByNomOrId({ recherche }: { recherche?: string }) {
	const query = db
		.select({
			id: commune.id,
			nom: commune.nom,
			departementId: commune.departementId,
		})
		.from(commune)
		.orderBy(sql`unaccent(${commune.nom})`)
		.limit(20);

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		query.where(
			or(
				rechercheBuild(commune.nom, rechercheSanitized),
				like(commune.id, `${rechercheSanitized}%`),
				// recherche sur le code postal
				exists(
					db
						.select({ codePostal: laposteCodeInseeCodePostal.codeInsee })
						.from(laposteCodeInseeCodePostal)
						.where(
							and(
								eq(laposteCodeInseeCodePostal.codeInsee, commune.id),
								like(laposteCodeInseeCodePostal.codePostal, `${rechercheSanitized}%`)
							)
						)
				)
			)
		);
	}

	return query;
}

export function epciGet({ epciId }: { epciId: Epci['id'] }) {
	return db.query.epci.findFirst({
		where: eq(epci.id, epciId),
	});
}

export function epciGetMany({ epciIds }: { epciIds: Epci['id'][] | null }) {
	const query = db.select().from(epci);

	if (epciIds?.length) {
		query.where(inArray(epci.id, epciIds));
	}

	return query;
}

export function epciGetManyByPetr({ petrId }: { petrId: Petr['id'] }) {
	return db.query.epci.findMany({
		where: eq(epci.petrId, petrId),
	});
}

export function epciGetManyByNomOrId({ recherche }: { recherche?: string }) {
	const query = db
		.select()
		.from(epci)
		.limit(20)
		.orderBy(sql`unaccent(${epci.nom})`);

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		query.where(
			or(rechercheBuild(epci.nom, rechercheSanitized), like(epci.id, `${rechercheSanitized}%`))
		);
	}

	return query;
}

export function metropoleGetManyByNom({ recherche }: { recherche?: string }) {
	const query = db
		.select()
		.from(metropole)
		.limit(20)
		.orderBy(sql`unaccent(${metropole.nom})`);

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		query.where(
			or(
				rechercheBuild(metropole.nom, rechercheSanitized),
				like(metropole.id, `${rechercheSanitized}%`)
			)
		);
	}

	return query;
}

export function metropoleGetMany({ metropoleIds }: { metropoleIds: Metropole['id'][] | null }) {
	const query = db.select().from(metropole);

	if (metropoleIds?.length) {
		query.where(inArray(metropole.id, metropoleIds));
	}

	return query;
}

export function petrGetMany({ petrIds }: { petrIds: Petr['id'][] | null }) {
	const query = db.select().from(petr);

	if (petrIds?.length) {
		query.where(inArray(petr.id, petrIds));
	}

	return query;
}

export function petrGetManyByNomOrId({ recherche }: { recherche?: string }) {
	const query = db
		.select()
		.from(petr)
		.limit(20)
		.orderBy(sql`unaccent(${petr.nom})`);

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		query.where(
			or(rechercheBuild(petr.nom, rechercheSanitized), like(petr.id, `${rechercheSanitized}%`))
		);
	}

	return query;
}

export function territoireIndustrieGetManyByNomOrId({ recherche }: { recherche?: string }) {
	const query = db
		.select()
		.from(territoireIndustrie)
		.limit(20)
		.orderBy(sql`unaccent(${territoireIndustrie.nom})`);

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		query.where(
			or(
				rechercheBuild(territoireIndustrie.nom, rechercheSanitized),
				like(territoireIndustrie.id, `${rechercheSanitized}%`)
			)
		);
	}

	return query;
}

export function territoireIndustrieGetManyById({
	territoireIndustrieIds,
}: {
	territoireIndustrieIds: TerritoireIndustrie['id'][] | null;
}) {
	const query = db.select().from(territoireIndustrie);

	if (territoireIndustrieIds?.length) {
		query.where(inArray(territoireIndustrie.id, territoireIndustrieIds));
	}

	return query;
}

export function departementGetManyById({
	departementIds,
}: {
	departementIds: Departement['id'][] | null;
}) {
	const query = db.select().from(departement);

	if (departementIds?.length) {
		query.where(inArray(departement.id, departementIds));
	}

	return query;
}

export function departementGetManyByNomOrId({ recherche }: { recherche?: string }) {
	const query = db
		.select()
		.from(departement)
		.limit(20)
		.orderBy(sql`unaccent(${departement.nom})`);

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		query.where(
			or(
				rechercheBuild(departement.nom, rechercheSanitized),
				like(departement.id, `${rechercheSanitized}%`)
			)
		);
	}

	return query;
}

export function regionGetManyById({ regionIds }: { regionIds?: Region['id'][] }) {
	const query = db.select().from(region);

	if (regionIds?.length) {
		query.where(inArray(region.id, regionIds));
	}

	return query;
}

export function regionGetManyByNomOrId({ recherche }: { recherche?: string }) {
	const query = db
		.select()
		.from(region)
		.limit(20)
		.orderBy(sql`unaccent(${region.nom})`);

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		query.where(
			or(rechercheBuild(region.nom, rechercheSanitized), like(region.id, `${rechercheSanitized}%`))
		);
	}

	return query;
}

export function qpvGetManyById({ qpvIds }: { qpvIds?: Qpv2024['id'][] }) {
	const query = db
		.select({
			id: qpv2024.id,
			nom: qpv2024.nom,
		})
		.from(qpv2024);

	if (qpvIds?.length) {
		query.where(inArray(qpv2024.id, qpvIds));
	}

	return query;
}

export function qpvGetManyByNomOrId({ recherche }: { recherche?: string }) {
	const query = db
		.select({
			id: qpv2024.id,
			nom: qpv2024.nom,
		})
		.from(qpv2024)
		.limit(20)
		.orderBy(sql`unaccent(${qpv2024.nom})`);

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		query.where(
			or(
				rechercheBuild(qpv2024.nom, rechercheSanitized),
				ilike(qpv2024.id, `%${rechercheSanitized}%`)
			)
		);
	}

	return query;
}
