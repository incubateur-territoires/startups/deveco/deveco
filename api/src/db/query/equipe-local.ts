import { and, eq, gte, lt, isNotNull, inArray, notInArray, sql, desc } from 'drizzle-orm';

import db from '../../db';
import {
	DemandeTypeId,
	Equipe,
	EquipeLocalContribution,
	Deveco,
	Etiquette,
	Contact,
	EquipeLocalEtiquette,
	Local,
	EquipeLocalContact,
	LocalCategorieTypeId,
	LocalNatureTypeId,
	PaginationParamsLocal,
	RessourceLocal,
	Commune,
	Etablissement,
	GeolocParam,
} from '../types';
import { equipeCommuneVue, etiquette, zonage } from '../schema/equipe';
import {
	eqLocEchange,
	eqLocDemande,
	eqLocRappel,
	eqLocEtiquette,
	eqLocContact,
	eqLocContribution,
	eqLocOccupant,
	eqLocRessource,
	devecoLocalFavori,
} from '../../db/schema/equipe-local';
import { evenement, actionEquipeLocal, actionEquipeLocalContact } from '../../db/schema/evenement';
import { contact } from '../../db/schema/contact';
import * as dbQueryEtablissement from '../../db/query/etablissement';
import * as dbQueryContact from '../../db/query/contact';
import * as dbQueryLocal from '../../db/query/local';
import * as dbQueryEquipe from '../../db/query/equipe';
import * as dbQueryEquipeLocalSuivi from '../../db/query/equipe-local-suivi';
import { local, localAdresse } from '../schema/local';

export * from '../../db/query/equipe-local-suivi';

export type Surfaces = {
	totale: {
		min: string | null;
		max: string | null;
	};
	vente: {
		min: string | null;
		max: string | null;
	};
	reserve: {
		min: string | null;
		max: string | null;
	};
	exterieure: {
		min: string | null;
		max: string | null;
	};
	stationnementNonCouverte: {
		min: string | null;
		max: string | null;
	};
	stationnementCouverte: {
		min: string | null;
		max: string | null;
	};
};

interface PublicParams {
	equipeId: Equipe['id'];
	invariant: string | null;
	numero: string | null;
	etages: string[];
	natures: LocalNatureTypeId[];
	categories: LocalCategorieTypeId[];
	surfaces: Surfaces;
	adresseCommune: Commune['id'] | null;
	adresseVoie: string | null;
	adresseNumero: string | null;
	section: string | null;
	parcelle: string | null;
	rechercheProprietaire: string | null;
	// geoloc
	geoloc: GeolocParam[];
	longitude: number | null;
	latitude: number | null;
	rayon: number | null;
	z: number | null;
	nwse: number[];
	// territoire
	qpvs: string[];
	zrrs: string[];
	acv: boolean | null;
	pvd: boolean | null;
	va: boolean | null;
	afrs: string[];
	territoireIndustries: string[];
	communes: string[];
	epcis: string[];
	departements: string[];
	regions: string[];
}

interface PersoParams {
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
	nom: string | null;
	contributionOccupe: (boolean | null)[];
	contributionRemembre: boolean | null;
	contributionLoyerEuros: {
		min: number | null;
		max: number | null;
	};
	contributionFondsEuros: {
		min: number | null;
		max: number | null;
	};
	contributionVenteEuros: {
		min: number | null;
		max: number | null;
	};
	demandeTypeIds: DemandeTypeId[];
	localisations: string[];
	motsCles: string[];
	favoris: string[];
	accompagnes: string[];
	accompagnesApres: Date | null;
	accompagnesAvant: Date | null;
}

export type EquipeLocalParams = PublicParams &
	PersoParams &
	PaginationParamsLocal &
	dbQueryEquipe.GeoParams;

export const eqLocContactSupprimeSub = notInArray(
	eqLocContact.contactId,
	db
		.select({
			contactId: eqLocContact.contactId,
		})
		.from(eqLocContact)
		.innerJoin(contact, eq(contact.id, eqLocContact.contactId))
		.where(isNotNull(contact.suppressionDate))
);

export function localRelationsBuild() {
	return {
		natureType: true,
		categorieType: true,
		adresse: {
			with: {
				commune: {
					columns: {
						id: true,
						nom: true,
					},
				},
			},
		},
	} as const;
}

export function eqLocRelationsBuild({
	equipeId,
	devecoId,
}: {
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
}) {
	return {
		favoris: {
			where: eq(devecoLocalFavori.devecoId, devecoId),
			limit: 1,
		},
		contributions: {
			where: eq(eqLocContribution.equipeId, equipeId),
			columns: {
				// utilisée dans LocalSimple
				nom: true,
				occupe: true,
				geolocalisation: true,
			},
			with: {
				modifCompte: {
					columns: {
						nom: true,
						prenom: true,
						email: true,
					},
				},
			},
			limit: 1,
		},
		contributionsGeolocs: {
			where: isNotNull(eqLocContribution.geolocalisation),
			columns: {
				geolocalisation: true,
			},
			with: {
				eq: {
					columns: {
						nom: true,
					},
				},
			},
			limit: 1,
		},
		// différent de la vue complète
		contacts: {
			where: eq(eqLocContact.equipeId, equipeId),
			columns: {
				fonction: true,
			},
			limit: 1,
		},
		echanges: {
			where: and(
				dbQueryEquipeLocalSuivi.eqLocEchangeSupprimeSub,
				eq(eqLocEchange.equipeId, equipeId)
			),
			limit: 1,
		},
		demandes: {
			where: and(
				dbQueryEquipeLocalSuivi.eqLocDemandeSupprimeSub,
				eq(eqLocDemande.equipeId, equipeId)
			),
			limit: 1,
		},
		rappels: {
			where: and(
				dbQueryEquipeLocalSuivi.eqLocRappelSupprimeSub,
				eq(eqLocRappel.equipeId, equipeId)
			),
			limit: 1,
		},
	} as const;
}

export function localWithRelationsGetMany({
	equipeId,
	devecoId,
	localIds,
}: {
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
	localIds: Local['id'][];
}) {
	return db.query.local.findMany({
		where: and(inArray(local.id, localIds)),
		columns: dbQueryLocal.localColumns(),
		with: {
			...localRelationsBuild(),
			...eqLocRelationsBuild({
				devecoId,
				equipeId,
			}),
		},
	});
}

function localAllRelationsBuild() {
	return {
		// utilisée dans LocalSimple
		adresse: {
			with: {
				commune: {
					columns: {
						id: true,
						nom: true,
					},
				},
			},
		},
		// en plus par rapport à la vue simple
		natureType: true,
		categorieType: true,
		propPPhys: {
			with: {
				propPPhy: {
					columns: {
						nom: true,
						prenom: true,
						prenoms: true,
						naissanceNom: true,
						naissanceDate: true,
						naissanceLieu: true,
						proprietaireCompteId: true,
					},
				},
			},
		},
		propPMors: {
			with: {
				propPMor: {
					columns: {
						nom: true,
						entrepriseId: true,
						proprietaireCompteId: true,
					},
					with: {
						ul: {
							columns: {},
							with: {
								siege: {
									columns: {
										siret: true,
									},
								},
							},
						},
					},
				},
			},
		},
		mutations: {
			columns: {},
			with: {
				mutation: {
					with: {
						natureType: true,
					},
				},
			},
		},
	} as const;
}

function eqLocAllRelationsBuild({
	equipeId,
	devecoId,
}: {
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
}) {
	return {
		// données perso
		favoris: {
			where: eq(devecoLocalFavori.devecoId, devecoId),
			limit: 1,
		},
		contacts: {
			columns: {
				fonction: true,
			},
			where: and(eqLocContactSupprimeSub, eq(eqLocContact.equipeId, equipeId)),
			with: {
				ctct: {
					with: dbQueryContact.contactRelationsBuild({ equipeId }),
				},
			},
		},
		contributions: {
			where: eq(eqLocContribution.equipeId, equipeId),
			columns: {
				// utilisée dans LocalSimple
				nom: true,
				occupe: true,
				geolocalisation: true,
				// en plus par rapport à la vue simple
				loyerEuros: true,
				bailTypeId: true,
				vacanceTypeId: true,
				vacanceMotifTypeId: true,
				remembre: true,
				fondsEuros: true,
				venteEuros: true,
				description: true,
				modificationDate: true,
			},
			with: {
				modifCompte: {
					columns: {
						nom: true,
						prenom: true,
						email: true,
					},
				},
			},
			limit: 1,
		},
		contributionsGeolocs: {
			where: isNotNull(eqLocContribution.geolocalisation),
			columns: {
				geolocalisation: true,
			},
			with: {
				eq: {
					columns: {
						nom: true,
					},
				},
			},
			limit: 1,
		},
		occupants: {
			where: eq(eqLocOccupant.equipeId, equipeId),
			columns: {},
			with: {
				occupant: {
					with: dbQueryEtablissement.etablissementRelationsBuild({ equipeId, __kind: 'colonne' }),
				},
			},
		},
		ressources: {
			where: eq(eqLocRessource.equipeId, equipeId),
		},
		echanges: {
			where: and(
				dbQueryEquipeLocalSuivi.eqLocEchangeSupprimeSub,
				eq(eqLocEchange.equipeId, equipeId)
			),
			with: {
				echange: {
					with: {
						modifCompte: {
							columns: {
								nom: true,
								prenom: true,
								email: true,
							},
						},
						demandes: {
							where: dbQueryEquipeLocalSuivi.eqLocDemandeSupprimeSub,
							with: {
								demande: true,
							},
						},
						deveco: {
							with: {
								compte: {
									columns: {
										nom: true,
										prenom: true,
										email: true,
									},
								},
							},
						},
					},
				},
			},
		},
		// en plus par rapport à la vue simple
		demandes: {
			where: and(
				dbQueryEquipeLocalSuivi.eqLocDemandeSupprimeSub,
				eq(eqLocDemande.equipeId, equipeId)
			),
			with: {
				demande: {
					with: {
						echanges: {
							where: dbQueryEquipeLocalSuivi.eqLocEchangeSupprimeSub,
							with: { echange: true },
						},
					},
				},
			},
		},
		rappels: {
			where: and(
				dbQueryEquipeLocalSuivi.eqLocRappelSupprimeSub,
				eq(eqLocRappel.equipeId, equipeId)
			),
			with: {
				rappel: {
					with: {
						affecteCompte: {
							columns: {
								// TODO supprimer id et actif dans le front
								id: true,
								nom: true,
								prenom: true,
								email: true,
								actif: true,
							},
						},
					},
				},
			},
		},
		etiquettes: {
			where: eq(eqLocEtiquette.equipeId, equipeId),
			with: {
				etiquette: true,
			},
		},
	} as const;
}

function eqLocEquipeCommuneRelationBuild({ equipeId }: { equipeId: Equipe['id'] }) {
	return {
		equipeCommune: {
			where: eq(equipeCommuneVue.equipeId, equipeId),
			columns: {
				equipeId: true,
			},
			limit: 1,
		},
	} as const;
}

export function localWithRelationsGet({
	equipeId,
	devecoId,
	localId,
}: {
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
	localId: Local['id'];
}) {
	return db.query.local.findFirst({
		where: and(eq(local.id, localId)),
		columns: dbQueryLocal.localColumns(),
		with: {
			...localAllRelationsBuild(),
			...eqLocAllRelationsBuild({
				equipeId,
				devecoId,
			}),
		},
	});
}

function localRelationsForExcelBuild() {
	return {
		categorieType: {
			columns: {
				nom: true,
			},
		},
		natureType: {
			columns: {
				nom: true,
			},
		},
		adresse: {
			with: {
				commune: {
					columns: {
						id: true,
						nom: true,
					},
				},
			},
		},
	} as const;
}

function localRelationsForGeojsonBuild() {
	return {
		adresse: {
			with: {
				commune: {
					columns: {
						id: true,
						nom: true,
					},
				},
			},
		},
	} as const;
}

function eqLocRelationsForGeojsonBuild({ equipeId }: { equipeId: Equipe['id'] }) {
	return {
		contributions: {
			where: eq(eqLocContribution.equipeId, equipeId),
			limit: 1,
		},
		etiquettes: {
			where: eq(eqLocEtiquette.equipeId, equipeId),
			with: {
				etiquette: true,
			},
		},
	} as const;
}

export function localWithRelationsForGeoJsonGetMany({
	equipeId,
	localIds,
}: {
	equipeId: Equipe['id'];
	localIds: Local['id'][];
}) {
	return db.query.local.findMany({
		where: inArray(local.id, localIds),
		columns: dbQueryLocal.localColumns(),
		with: {
			...localRelationsForGeojsonBuild(),
			...eqLocRelationsForGeojsonBuild({ equipeId }),
		},
	});
}

function localWithRelationsForExcelBuild({ equipeId }: { equipeId: Equipe['id'] }) {
	return {
		contributions: {
			where: eq(eqLocContribution.equipeId, equipeId),
			limit: 1,
		},
		etiquettes: {
			where: eq(eqLocEtiquette.equipeId, equipeId),
			with: {
				etiquette: true,
			},
		},
		contacts: {
			where: and(eqLocContactSupprimeSub, eq(eqLocContact.equipeId, equipeId)),
			with: {
				ctct: true,
			},
		},
		echanges: {
			where: and(
				dbQueryEquipeLocalSuivi.eqLocEchangeSupprimeSub,
				eq(eqLocEchange.equipeId, equipeId)
			),
			with: {
				echange: {
					with: {
						modifCompte: {
							columns: {
								nom: true,
								prenom: true,
								email: true,
							},
						},
						deveco: {
							with: {
								compte: {
									columns: {
										nom: true,
										prenom: true,
										email: true,
									},
								},
							},
						},
						demandes: {
							where: dbQueryEquipeLocalSuivi.eqLocDemandeSupprimeSub,
							with: {
								demande: true,
							},
						},
					},
				},
			},
		},
		demandes: {
			where: and(
				dbQueryEquipeLocalSuivi.eqLocDemandeSupprimeSub,
				eq(eqLocDemande.equipeId, equipeId)
			),
			with: {
				demande: {
					with: {
						echanges: {
							with: {
								echange: true,
							},
						},
					},
				},
			},
		},
		occupants: {
			where: eq(eqLocOccupant.equipeId, equipeId),
			columns: {},
			with: {
				occupant: {
					with: dbQueryEtablissement.etablissementRelationsBuild({ equipeId, __kind: 'colonne' }),
				},
			},
		},
	} as const;
}

export function localForExcelGetMany({
	equipeId,
	localIds,
}: {
	equipeId: Equipe['id'];
	localIds: Local['id'][];
}) {
	return db.query.local.findMany({
		where: inArray(local.id, localIds),
		columns: dbQueryLocal.localForStreamRelations(),
		with: {
			...localRelationsForExcelBuild(),
			...localWithRelationsForExcelBuild({ equipeId }),
		},
	});
}

export function localWithEquipeCommuneGet({
	equipeId,
	localId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
}) {
	return db.query.local.findFirst({
		where: eq(local.id, localId),
		with: eqLocEquipeCommuneRelationBuild({ equipeId }),
	});
}

export function localWithRelationsAndEquipeCommuneGet({
	equipeId,
	devecoId,
	localId,
}: {
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
	localId: Local['id'];
}) {
	return db.query.local.findFirst({
		where: and(eq(local.id, localId)),
		columns: dbQueryLocal.localColumns(),
		with: {
			...localRelationsBuild(),
			...eqLocAllRelationsBuild({
				equipeId,
				devecoId,
			}),
			...eqLocEquipeCommuneRelationBuild({ equipeId }),
		},
	});
}

export function localWithContributionGet({
	equipeId,
	localId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
}) {
	return db.query.local.findFirst({
		where: eq(local.id, localId),
		with: {
			contributions: {
				where: eq(eqLocContribution.equipeId, equipeId),
				columns: {
					nom: true,
				},
				limit: 1,
			},
		},
	});
}

export function localWithEqLocGeolocalisationContributionGet({
	localId,
}: {
	localId: Local['id'];
}) {
	return db.query.eqLocContribution.findFirst({
		columns: {
			equipeId: true,
		},
		where: and(
			eq(eqLocContribution.localId, localId),
			isNotNull(eqLocContribution.geolocalisation)
		),
	});
}

export function eqLocContributionGet({
	equipeId,
	localId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
}) {
	return db.query.eqLocContribution.findFirst({
		where: and(eq(eqLocContribution.localId, localId), eq(eqLocContribution.equipeId, equipeId)),
	});
}

export function localAdresseGet({ localId }: { localId: Local['id'] }) {
	return db.query.localAdresse.findFirst({
		where: eq(localAdresse.localId, localId),
		columns: {
			numero: true,
			voieNom: true,
			communeId: true,
		},
	});
}

export function eqLocFavoriAdd({
	devecoId,
	localId,
}: {
	devecoId: Deveco['id'];
	localId: Local['id'];
}) {
	return db
		.insert(devecoLocalFavori)
		.values({ devecoId, localId })
		.onConflictDoNothing({
			target: [devecoLocalFavori.devecoId, devecoLocalFavori.localId],
		});
}

export function eqLocFavoriRemove({
	devecoId,
	localId,
}: {
	devecoId: Deveco['id'];
	localId: Local['id'];
}) {
	return db
		.delete(devecoLocalFavori)
		.where(and(eq(devecoLocalFavori.devecoId, devecoId), eq(devecoLocalFavori.localId, localId)));
}

export function eqLocEtiquetteAddMany({
	localEtiquettes,
}: {
	localEtiquettes: EquipeLocalEtiquette[];
}) {
	return db
		.insert(eqLocEtiquette)
		.values(localEtiquettes)
		.onConflictDoNothing({
			target: [eqLocEtiquette.localId, eqLocEtiquette.equipeId, eqLocEtiquette.etiquetteId],
		})
		.returning();
}

export function eqLocEtiquetteRemoveMany({
	equipeId,
	localId,
	etiquetteIds,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	etiquetteIds: Etiquette['id'][];
}) {
	return db
		.delete(eqLocEtiquette)
		.where(
			and(
				and(eq(eqLocEtiquette.localId, localId), eq(eqLocEtiquette.equipeId, equipeId)),
				inArray(eqLocEtiquette.etiquetteId, etiquetteIds)
			)
		)
		.returning({
			etiquetteId: eqLocEtiquette.etiquetteId,
		});
}

export function zonageGetManyByEquipeLocal({
	equipeId,
	localId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
}) {
	return db
		.select({
			id: zonage.id,
			nom: etiquette.nom,
			enCours: zonage.enCours,
			description: zonage.description,
		})
		.from(zonage)
		.innerJoin(etiquette, eq(etiquette.id, zonage.etiquetteId))
		.innerJoin(localAdresse, eq(localAdresse.localId, localId))
		.where(
			and(
				eq(zonage.equipeId, equipeId),
				isNotNull(localAdresse.geolocalisation),
				sql`ST_Covers(${zonage.geometrie}, ${localAdresse.geolocalisation})`
			)
		);
}

export function eqLocContributionUpsert({
	equipeId,
	localId,
	contribution,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	contribution: Partial<EquipeLocalContribution>;
}) {
	return db
		.insert(eqLocContribution)
		.values({
			localId,
			equipeId,
			...contribution,
		})
		.onConflictDoUpdate({
			target: [eqLocContribution.localId, eqLocContribution.equipeId],
			set: contribution,
		})
		.returning();
}

export function eqLocContactProprioGetManyByCreationDate({
	equipeId,
	debut,
	fin,
}: {
	equipeId: Equipe['id'];
	debut: Date;
	fin: Date;
}) {
	return db
		.selectDistinct({
			contactId: actionEquipeLocalContact.contactId,
			localId: actionEquipeLocalContact.localId,
		})
		.from(eqLocContact)
		.innerJoin(actionEquipeLocalContact, eq(actionEquipeLocalContact.localId, eqLocContact.localId))
		.innerJoin(evenement, eq(evenement.id, actionEquipeLocalContact.evenementId))
		.where(
			and(
				eq(actionEquipeLocalContact.equipeId, equipeId),
				gte(evenement.createdAt, debut),
				lt(evenement.createdAt, fin),
				// TODO : très très flaky si on n'a pas de synchro BDD <-> front
				eq(eqLocContact.fonction, 'Propriétaire')
			)
		);
}

export function localIdGetManyByAffichageDate({
	equipeId,
	debut,
	fin,
}: {
	equipeId: Equipe['id'];
	debut: Date;
	fin: Date;
}) {
	return db
		.selectDistinct({
			id: actionEquipeLocal.localId,
		})
		.from(actionEquipeLocal)
		.innerJoin(evenement, eq(evenement.id, actionEquipeLocal.evenementId))
		.where(
			and(
				eq(actionEquipeLocal.equipeId, equipeId),
				eq(actionEquipeLocal.typeId, 'affichage'),
				gte(evenement.createdAt, debut),
				lt(evenement.createdAt, fin)
			)
		);
}

export function eqLocIdCountOccupantsVacantsBetweenDates({
	equipeId,
	debut,
	fin,
}: {
	equipeId: Equipe['id'];
	debut: Date;
	fin: Date;
}) {
	const actions = db
		.select({
			localId: actionEquipeLocal.localId,
			// TODO vérifier si la sérialisation fonctionne bien avec Drizzle en forçant manuellement
			// le type Date (ça ne fonctionnait plus avec "created_at" pour action_compte)
			date: actionEquipeLocal.createdAt,
			statutDebut: sql<string>`RIGHT(
                STRING_AGG(
                  CASE
                    WHEN ${actionEquipeLocal.diff} -> 'changes' ->> 'after' = 'vacant' THEN 'V'
                    WHEN ${actionEquipeLocal.diff} -> 'changes' ->> 'after' = 'occupé' THEN 'O'
                    ELSE 'N'
                  END,
                  ''
                ) FILTER(
                  WHERE
                    ${actionEquipeLocal.createdAt} < ${debut.toISOString()}
                ) OVER(
                  PARTITION BY ${actionEquipeLocal.localId}
                  ORDER BY
                    ${actionEquipeLocal.createdAt} ASC ROWS BETWEEN UNBOUNDED PRECEDING
                    AND CURRENT ROW
                ),
                1
              )`.as('statut_debut'),
			statutFinal: sql<string>`CASE
			  WHEN ${actionEquipeLocal.diff} -> 'changes' ->> 'after' = 'vacant' THEN 'V'
			  WHEN ${actionEquipeLocal.diff} -> 'changes' ->> 'after' = 'occupé' THEN 'O'
			  ELSE 'N'
			END`.as('statut_final'),
		})
		.from(actionEquipeLocal)
		.where(eq(actionEquipeLocal.equipeId, equipeId))
		.as('actions');

	const sousBilan = db
		.selectDistinctOn([actions.localId])
		.from(actions)
		.where(and(gte(actions.date, debut), lt(actions.date, fin)))
		.orderBy(actions.localId, desc(actions.date))
		.as('sousBilan');

	const bilan = db
		.select({
			localId: sousBilan.localId,
			vacantDiff: sql<number>`
		  CASE
			WHEN ${sousBilan.statutFinal} = ${actions.statutDebut} THEN 0
			ELSE (
			  CASE
				WHEN ${actions.statutDebut} = 'V' THEN -1
				WHEN ${sousBilan.statutFinal} = 'V' THEN 1
				ELSE 0
			  END
			)
		  END
		`.as('vacant_diff'),
			occupeDiff: sql<number>`
		  CASE
			WHEN ${sousBilan.statutFinal} = ${actions.statutDebut} THEN 0
			ELSE (
			  CASE
				WHEN ${actions.statutDebut} = 'O' THEN -1
				WHEN ${sousBilan.statutFinal} = 'O' THEN 1
				ELSE 0
			  END
			)
		  END`.as('occupe_diff'),
		})
		.from(sousBilan)
		.as('bilan');

	return db
		.select({
			vacants: sql<string | null>`sum(${bilan.vacantDiff})`.as('vacants'),
			occupes: sql<string | null>`sum(${bilan.occupeDiff})`.as('occupes'),
		})
		.from(bilan);
}

export function eqLocContactGet({
	equipeId,
	localId,
	contactId,
}: {
	equipeId: Equipe['id'];
	contactId: Contact['id'];
	localId: Local['id'];
}) {
	return db
		.select()
		.from(eqLocContact)
		.where(
			and(
				eq(eqLocContact.contactId, contactId),
				eq(eqLocContact.equipeId, equipeId),
				eq(eqLocContact.localId, localId)
			)
		);
}
export function eqLocContactAdd({
	equipeId,
	localId,
	contactId,
	fonction,
	contactSourceTypeId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	contactId: Contact['id'];
	fonction?: EquipeLocalContact['fonction'];
	contactSourceTypeId: EquipeLocalContact['contactSourceTypeId'];
}) {
	return db.insert(eqLocContact).values({
		localId,
		equipeId,
		contactId,
		fonction,
		contactSourceTypeId,
	});
}

export function eqLocContactUpdate({
	equipeId,
	localId,
	contactId,
	fonction,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	contactId: Contact['id'];
	fonction: EquipeLocalContact['fonction'];
}) {
	return db
		.update(eqLocContact)
		.set({ fonction })
		.where(
			and(
				eq(eqLocContact.contactId, contactId),
				eq(eqLocContact.localId, localId),
				eq(eqLocContact.equipeId, equipeId)
			)
		)
		.returning();
}

export function eqLocContactRemove({
	equipeId,
	localId,
	contactId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	contactId: Contact['id'];
}) {
	return db
		.delete(eqLocContact)
		.where(
			and(
				eq(eqLocContact.contactId, contactId),
				eq(eqLocContact.localId, localId),
				eq(eqLocContact.equipeId, equipeId)
			)
		)
		.returning();
}

export function eqLocContactRemoveMany({
	localIds,
	contactId,
}: {
	localIds: Local['id'][];
	contactId: Contact['id'];
}) {
	return db
		.delete(eqLocContact)
		.where(and(inArray(eqLocContact.localId, localIds), eq(eqLocContact.contactId, contactId)))
		.returning();
}

export function eqLocOccupantAdd({
	equipeId,
	localId,
	etablissementId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	etablissementId: Etablissement['siret'];
}) {
	return db
		.insert(eqLocOccupant)
		.values({ localId, equipeId, etablissementId })
		.onConflictDoNothing();
}

export function eqLocOccupantGetFirst({
	equipeId,
	localId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
}) {
	return db.query.eqLocOccupant.findFirst({
		where: and(eq(eqLocOccupant.equipeId, equipeId), eq(eqLocOccupant.localId, localId)),
	});
}

export function eqLocOccupantRemove({
	equipeId,
	localId,
	etablissementId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	etablissementId: Etablissement['siret'];
}) {
	return db
		.delete(eqLocOccupant)
		.where(
			and(
				eq(eqLocOccupant.etablissementId, etablissementId),
				eq(eqLocOccupant.localId, localId),
				eq(eqLocOccupant.equipeId, equipeId)
			)
		);
}

export function eqLocContactNomsUpdate({
	equipeId,
	localId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
}) {
	const nom = sql<string>`${contact.prenom} || ' ' || ${contact.nom}`;

	return db
		.select({ nom })
		.from(eqLocContact)
		.innerJoin(contact, eq(contact.id, eqLocContact.contactId))
		.where(and(eq(eqLocContact.equipeId, equipeId), eq(eqLocContact.localId, localId)));
}

export function ressourceAdd({
	equipeId,
	localId,
	partialRessource,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	partialRessource: Partial<RessourceLocal>;
}) {
	return db
		.insert(eqLocRessource)
		.values({ localId, equipeId, ...partialRessource })
		.returning();
}

export function ressourceUpdate({
	ressourceId,
	partialRessource,
}: {
	ressourceId: RessourceLocal['id'];
	partialRessource: Partial<RessourceLocal>;
}) {
	return db
		.update(eqLocRessource)
		.set({ ...partialRessource })
		.where(eq(eqLocRessource.id, ressourceId))
		.returning();
}

export function ressourceRemove({ ressourceId }: { ressourceId: RessourceLocal['id'] }) {
	return db.delete(eqLocRessource).where(eq(eqLocRessource.id, ressourceId));
}

export async function eqLocEtiquetteUpdate({
	equipeId,
	localId,
	etiquetteIds,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	etiquetteIds: Etiquette['id'][];
}) {
	const eqLocEtiquetteFormat = (etiquetteId: Etiquette['id']) => ({
		equipeId,
		localId,
		etiquetteId,
		auto: false,
	});

	const currentValues = await db.query.eqLocEtiquette.findMany({
		where: and(eq(eqLocEtiquette.equipeId, equipeId), eq(eqLocEtiquette.localId, localId)),
	});
	const currentEtiquetteIds = currentValues.map((eee) => eee.etiquetteId);

	const toAdd = etiquetteIds
		.filter((etiquetteId) => !currentEtiquetteIds.includes(etiquetteId))
		.map(eqLocEtiquetteFormat);

	const toRemove = currentEtiquetteIds.filter((etiquetteId) => !etiquetteIds.includes(etiquetteId));

	const added = toAdd.length
		? await db
				.insert(eqLocEtiquette)
				.values(toAdd)
				.onConflictDoNothing({
					target: [eqLocEtiquette.equipeId, eqLocEtiquette.localId, eqLocEtiquette.etiquetteId],
				})
				.returning({
					etiquetteId: eqLocEtiquette.etiquetteId,
				})
		: [];

	const removed = toRemove.length
		? await db
				.delete(eqLocEtiquette)
				.where(
					and(
						eq(eqLocEtiquette.equipeId, equipeId),
						eq(eqLocEtiquette.localId, localId),
						inArray(eqLocEtiquette.etiquetteId, toRemove)
					)
				)
				.returning({
					etiquetteId: eqLocEtiquette.etiquetteId,
				})
		: [];

	return {
		ajoutees: added.map(({ etiquetteId }) => etiquetteId),
		supprimees: removed.map(({ etiquetteId }) => etiquetteId),
	};
}
