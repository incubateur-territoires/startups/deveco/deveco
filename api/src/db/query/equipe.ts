import {
	sql,
	and,
	eq,
	inArray,
	desc,
	ilike,
	gte,
	isNull,
	lt,
	getTableColumns,
	count,
	or,
	InferInsertModel,
	like,
} from 'drizzle-orm';
import { unionAll } from 'drizzle-orm/pg-core';
import { addDays } from 'date-fns';
import { v4 as uuidv4 } from 'uuid';

import db from '../../db';
import {
	TerritoireTypeId,
	EquipeTypeId,
	GeoToken,
	Zonage,
	Qpv2024,
	Etiquette,
	Deveco,
	Equipe,
	EtiquetteTypeId,
	FileFormat,
	New,
	EntiteTypeId,
	TriDeveco,
	RechercheSauvegardee,
	RechercheEnregistree,
	SituationGeographique,
} from '../types';
import {
	deveco,
	equipe,
	equipeCommune,
	equipeMetropole,
	equipeEpci,
	equipePetr,
	equipeTerritoireIndustrie,
	equipeDepartement,
	equipeRegion,
	etiquette,
	geoToken,
	recherche,
	zonage,
	geoTokenRequest,
	equipeCommuneVue,
	rechercheSauvegardee,
	rechercheEnregistree,
	notifications,
} from '../schema/equipe';
import { qpv2024, communeQpv2024 } from '../schema/contact';
import { evenement } from '../schema/evenement';
import { eqEtabEtiquette } from '../schema/equipe-etablissement';
import { etabCreaEtiquette } from '../schema/etablissement-creation';
import { compte, motDePasse } from '../schema/admin';
import {
	commune,
	epci,
	territoireIndustrie,
	communeTerritoireIndustrie,
	departement,
	region,
} from '../schema/territoire';
import { eqLocEtiquette } from '../schema/equipe-local';
import { laposteCodeInseeCodePostal } from '../schema/source';
import { dbQuoteEscape } from '../../lib/utils';
import { rechercheBuild } from '../../db/utils';

export interface GeoParams {
	equipeId: Equipe['id'];
	geo: SituationGeographique | null;
}

export function notificationsGetByDevecoId({ devecoId }: { devecoId: Deveco['id'] }) {
	return db.query.notifications.findFirst({
		where: eq(notifications.devecoId, devecoId),
	});
}

export function notificationsUpdate({
	notifications: partialNotifications,
}: {
	notifications: New<typeof notifications>;
}) {
	return db
		.insert(notifications)
		.values(partialNotifications)
		.onConflictDoUpdate({
			target: notifications.devecoId,
			set: Object.fromEntries(
				Object.entries(getTableColumns(notifications)).map(([columnAlias, column]) => [
					columnAlias,
					sql.raw(`excluded.${column.name}`),
				])
			),
		})
		.returning();
}

export function devecoAdd({ deveco: devecoValues }: { deveco: New<typeof deveco> }) {
	return db.insert(deveco).values(devecoValues).returning();
}

export function devecoWithCompteGet({ devecoId }: { devecoId: number }) {
	return db.query.deveco.findFirst({
		with: {
			compte: true,
			equipe: true,
		},
		where: eq(deveco.id, devecoId),
	});
}

export function devecoUpdate({
	compteId,
	partialDeveco,
}: {
	compteId: number;
	partialDeveco: Partial<Deveco>;
}) {
	return db
		.update(deveco)
		.set({
			...partialDeveco,
			updatedAt: new Date(),
		})
		.where(eq(deveco.compteId, compteId))
		.returning();
}

export function devecoGetByCompte({ compteId }: { compteId: number }) {
	return db.query.deveco.findFirst({
		where: eq(deveco.compteId, compteId),
		with: {
			equipe: true,
		},
	});
}

export function devecoGetByEmail({ email }: { email: string }) {
	return db
		.select({
			id: deveco.id,
			equipeId: deveco.equipeId,
			compteId: deveco.compteId,
		})
		.from(deveco)
		.innerJoin(compte, eq(compte.id, deveco.compteId))
		.where(eq(compte.email, email))
		.limit(1);
}

export function devecosActifsGetByEmails({ emails }: { emails: string[] }) {
	return db
		.select({
			id: deveco.id,
			equipeId: deveco.equipeId,
			compteId: deveco.compteId,
			email: compte.email,
			identifiant: compte.identifiant,
			equipe: equipe.nom,
		})
		.from(deveco)
		.innerJoin(compte, eq(compte.id, deveco.compteId))
		.innerJoin(equipe, eq(equipe.id, deveco.equipeId))
		.where(
			and(
				inArray(
					compte.email,
					emails.map((e) => e.toLowerCase())
				),
				eq(compte.actif, true)
			)
		);
}

function compteRechercheWhereBuild({ recherche }: { recherche: string }) {
	const rechercheSanitized = dbQuoteEscape(recherche.trim());

	return or(
		rechercheBuild(compte.nom, rechercheSanitized),
		sql.join(
			[sql`unaccent(${compte.prenom})`, sql`ilike`, sql.raw(`unaccent('%${rechercheSanitized}%')`)],
			sql.raw(' ')
		),
		sql.join(
			[sql`unaccent(${compte.email})`, sql`ilike`, sql.raw(`unaccent('%${rechercheSanitized}%')`)],
			sql.raw(' ')
		),
		sql.join(
			[
				sql`unaccent(${compte.identifiant})`,
				sql`ilike`,
				sql.raw(`unaccent('%${rechercheSanitized}%')`),
			],
			sql.raw(' ')
		)
	);
}

export function devecoCount({
	recherche,
	equipeId,
}: {
	recherche?: string;
	equipeId?: Equipe['id'] | null;
}) {
	const query = db
		.select({
			count: count(),
		})
		.from(deveco)
		.innerJoin(equipe, eq(equipe.id, deveco.equipeId))
		.innerJoin(compte, eq(compte.id, deveco.compteId));

	const wheres = [];

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		wheres.push(
			inArray(
				deveco.compteId,
				db
					.select({ id: compte.id })
					.from(compte)
					.where(compteRechercheWhereBuild({ recherche: rechercheSanitized }))
			)
		);
	}

	if (equipeId) {
		wheres.push(eq(deveco.equipeId, equipeId));
	}

	query.where(and(...wheres));

	return query;
}

export function compteUniqueWithEquipeGetMany({
	page,
	equipeId,
	elementsParPage,
}: {
	equipeId?: Equipe['id'];
	page?: number;
	elementsParPage?: number;
}) {
	const elements = elementsParPage ?? 20;

	const query = db
		.selectDistinctOn([compte.email], {
			...getTableColumns(compte),
			equipeNom: equipe.nom,
		})
		.from(deveco)
		.innerJoin(equipe, eq(equipe.id, deveco.equipeId))
		.innerJoin(compte, eq(compte.id, deveco.compteId));

	if (equipeId) {
		query.where(eq(deveco.equipeId, equipeId));
	}

	const orderBy = [sql`${compte.email}`, desc(sql`${compte.actif}`)].map(
		(o) => sql`${o} NULLS LAST`
	);

	query.orderBy(...orderBy);

	if (elements > 0) {
		query.limit(elements).offset(elements ? ((page || 1) - 1) * elements : 0);
	}

	return query;
}

export function devecoWithRelationsGetMany({
	recherche,
	equipeId,
	page,
	elementsParPage,
	tri,
	direction,
}: {
	recherche?: string;
	equipeId?: Equipe['id'] | null;
	page?: number;
	elementsParPage?: number;
	tri?: TriDeveco;
	direction?: 'asc' | 'desc';
}) {
	const elements = elementsParPage ?? 20;

	const wheres = [];

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		wheres.push(
			inArray(
				deveco.compteId,
				db
					.select({ id: compte.id })
					.from(compte)
					.where(compteRechercheWhereBuild({ recherche: rechercheSanitized }))
			)
		);
	}

	if (equipeId) {
		wheres.push(eq(deveco.equipeId, equipeId));
	}

	const query = db
		.select({
			...getTableColumns(deveco),
			compte,
			equipe,
			motDePasse,
		})
		.from(deveco)
		.innerJoin(equipe, eq(equipe.id, deveco.equipeId))
		.innerJoin(compte, eq(compte.id, deveco.compteId))
		.leftJoin(motDePasse, eq(motDePasse.compteId, deveco.compteId))
		.where(and(...wheres));

	let orderBy = [sql`lower(${compte.nom})`, sql`lower(${compte.prenom})`];
	if (tri === 'prenom') {
		orderBy = [sql`lower(${compte.prenom})`, sql`lower(${compte.nom})`];
	} else if (tri === 'identifiant') {
		orderBy = [sql`lower(${compte.identifiant})`];
	} else if (tri === 'email') {
		orderBy = [sql`lower(${compte.email})`];
	} else if (tri === 'connexion') {
		orderBy = [sql`${compte.connexionAt}`];
	} else if (tri === 'action') {
		orderBy = [sql`${compte.authAt}`];
	}

	if (direction === 'desc') {
		orderBy = orderBy.map((o) => desc(o));
	}

	orderBy = orderBy.map((o) => sql`${o} NULLS LAST`);

	query.orderBy(...orderBy);

	if (elements > 0) {
		query.limit(elements).offset(elements ? ((page || 1) - 1) * elements : 0);
	}

	return query;
}

export function devecoWithCompteColumnsGetManyByEquipe({ equipeId }: { equipeId: Equipe['id'] }) {
	return db
		.select({
			id: compte.id,
			nom: compte.nom,
			prenom: compte.prenom,
			email: compte.email,
			actif: compte.actif,
			connexion: notifications.connexion,
			rappels: notifications.rappels,
			activite: notifications.activite,
		})
		.from(compte)
		.innerJoin(deveco, eq(deveco.compteId, compte.id))
		.leftJoin(notifications, eq(notifications.devecoId, deveco.id))
		.where(eq(deveco.equipeId, equipeId))
		.orderBy(sql`lower(${compte.nom})`, sql`lower(${compte.prenom})`);
}

export function devecoGetManyNeverConnectedAfter10Days() {
	const elevenDaysAgo = addDays(new Date(), -11);
	const tenDaysAgo = addDays(new Date(), -10);

	return db
		.select({
			deveco,
			compte,
		})
		.from(deveco)
		.innerJoin(compte, eq(compte.id, deveco.compteId))
		.leftJoin(evenement, and(eq(evenement.compteId, compte.id), eq(evenement.typeId, 'connexion')))
		.where(
			and(
				gte(deveco.createdAt, elevenDaysAgo),
				lt(deveco.createdAt, tenDaysAgo),
				isNull(evenement.compteId)
			)
		);
}

export function devecoGetManyNeverConnectedAfter30Days() {
	const thirtyOneDaysAgo = addDays(new Date(), -31);
	const thirtyDaysAgo = addDays(new Date(), -30);

	return db
		.select({
			deveco,
			compte,
		})
		.from(deveco)
		.innerJoin(compte, eq(compte.id, deveco.compteId))
		.leftJoin(evenement, and(eq(evenement.compteId, compte.id), eq(evenement.typeId, 'connexion')))
		.where(
			and(
				gte(deveco.createdAt, thirtyOneDaysAgo),
				lt(deveco.createdAt, thirtyDaysAgo),
				isNull(evenement.compteId)
			)
		);
}

export function equipeTypeGetMany() {
	return db.query.equipeType.findMany();
}

export function equipeAdd({
	nom,
	territoireTypeId,
	equipeTypeId,
	groupement,
	siret,
}: {
	nom: Equipe['nom'];
	territoireTypeId: TerritoireTypeId;
	equipeTypeId: EquipeTypeId;
	groupement: Equipe['groupement'];
	siret: string | null;
}) {
	return db
		.insert(equipe)
		.values({
			nom,
			typeId: equipeTypeId,
			territoireTypeId,
			groupement,
			siret,
		})
		.returning();
}

export function equipeTerritoireAdd({
	equipeId,
	territoireTypeId,
	territoireIds,
}: {
	equipeId: Equipe['id'];
	territoireTypeId: TerritoireTypeId;
	territoireIds: string[];
}) {
	switch (territoireTypeId) {
		case 'commune':
			return db
				.insert(equipeCommune)
				.values(territoireIds.map((territoireId) => ({ equipeId, communeId: territoireId })));

		case 'metropole':
			return db
				.insert(equipeMetropole)
				.values(territoireIds.map((territoireId) => ({ equipeId, metropoleId: territoireId })));

		case 'epci':
			return db
				.insert(equipeEpci)
				.values(territoireIds.map((territoireId) => ({ equipeId, epciId: territoireId })));

		case 'petr':
			return db
				.insert(equipePetr)
				.values(territoireIds.map((territoireId) => ({ equipeId, petrId: territoireId })));

		case 'territoire_industrie':
			return db
				.insert(equipeTerritoireIndustrie)
				.values(
					territoireIds.map((territoireId) => ({ equipeId, territoireIndustrieId: territoireId }))
				);

		case 'departement':
			return db
				.insert(equipeDepartement)
				.values(territoireIds.map((territoireId) => ({ equipeId, departementId: territoireId })));

		case 'region':
			return db
				.insert(equipeRegion)
				.values(territoireIds.map((territoireId) => ({ equipeId, regionId: territoireId })));

		case 'france':
	}
}

export function equipeGet({ equipeId }: { equipeId: Equipe['id'] }) {
	return db.query.equipe.findFirst({
		where: eq(equipe.id, equipeId),
	});
}

export function equipeWithDevecosWithCompteGetMany() {
	return db.query.equipe.findMany({
		with: {
			devecos: {
				with: {
					notifications: true,
					compte: {
						columns: {
							email: true,
							actif: true,
						},
					},
				},
			},
		},
	});
}

function equipeTerritoireWithNomRelationsBuild() {
	return {
		territoireType: { columns: { nom: true } },
		type: { columns: { nom: true } },
		communes: { with: { commune: { columns: { nom: true } } } },
		epcis: { with: { epci: { columns: { nom: true } } } },
		metropoles: { with: { metropole: { columns: { nom: true } } } },
		petrs: { with: { petr: { columns: { nom: true } } } },
		territoireIndustries: { with: { territoireIndustrie: { columns: { nom: true } } } },
		departements: { with: { departement: { columns: { nom: true } } } },
		regions: { with: { region: { columns: { nom: true } } } },
	} as const;
}

export function equipeWithTerritoireWithNomGetMany() {
	return db.query.equipe.findMany({
		with: equipeTerritoireWithNomRelationsBuild(),
	});
}

function equipeTerritoireRelationsBuild() {
	return {
		communes: {
			columns: {
				communeId: true,
			},
			with: {
				commune: { columns: {}, with: { communeFilles: { columns: { id: true } } } },
			},
		},
		metropoles: { columns: { metropoleId: true } },
		epcis: { columns: { epciId: true } },
		petrs: { columns: { petrId: true } },
		territoireIndustries: { columns: { territoireIndustrieId: true } },
		departements: { columns: { departementId: true } },
		regions: { columns: { regionId: true } },
	} as const;
}

export function equipeWithTerritoireGet({ equipeId }: { equipeId: Equipe['id'] }) {
	return db.query.equipe.findFirst({
		where: eq(equipe.id, equipeId),
		with: equipeTerritoireRelationsBuild(),
	});
}

export function equipeGetByNom({ nom }: { nom: string }) {
	return db.query.equipe.findFirst({
		where: eq(equipe.nom, nom),
	});
}

export function equipeGetMany() {
	return db.query.equipe.findMany();
}

export function etiquetteAddMany({
	equipeId,
	etiquetteTypeId,
	noms,
}: {
	equipeId: Equipe['id'];
	etiquetteTypeId: EtiquetteTypeId;
	noms: string[];
}) {
	const values = noms
		.map((nom) => nom?.trim())
		.filter(Boolean)
		.map((nom) => ({ typeId: etiquetteTypeId, nom, equipeId }));

	if (!values.length) {
		return [];
	}

	return db
		.insert(etiquette)
		.values(values)
		.onConflictDoNothing({
			target: [etiquette.typeId, etiquette.nom, etiquette.equipeId],
		})
		.returning();
}

export function etiquetteGet({ etiquetteId }: { etiquetteId: Etiquette['id'] }) {
	return db.query.etiquette.findFirst({
		where: eq(etiquette.id, etiquetteId),
	});
}

export function etiquetteGetMany({ etiquetteIds }: { etiquetteIds: Etiquette['id'][] }) {
	return db.query.etiquette.findMany({
		where: inArray(etiquette.id, etiquetteIds),
	});
}

export function etiquetteGetManyByTypeAndNoms({
	equipeId,
	etiquetteTypeId,
	noms,
}: {
	equipeId: Equipe['id'];
	etiquetteTypeId: EtiquetteTypeId;
	noms: string[];
}) {
	if (!noms.length) {
		return [];
	}

	return db.query.etiquette.findMany({
		where: and(
			eq(etiquette.equipeId, equipeId),
			eq(etiquette.typeId, etiquetteTypeId),
			inArray(etiquette.nom, noms)
		),
		with: {
			zonage: {
				columns: {
					id: true,
				},
			},
		},
		orderBy: sql`lower(unaccent(${etiquette.nom}))`,
	});
}

export function etiquetteGetByTypeAndNom({
	equipeId,
	etiquetteTypeId,
	nom,
}: {
	equipeId: Equipe['id'];
	etiquetteTypeId: EtiquetteTypeId;
	nom?: string;
}) {
	return db.query.etiquette.findFirst({
		where: and(
			...[
				eq(etiquette.equipeId, equipeId),
				eq(etiquette.typeId, etiquetteTypeId),
				...(nom ? [eq(etiquette.nom, nom)] : []),
			]
		),
	});
}

export function etiquetteGetManyByTypeAndNom({
	equipeId,
	etiquetteTypeId,
	nom,
}: {
	equipeId: Equipe['id'];
	etiquetteTypeId: EtiquetteTypeId;
	nom: string;
}) {
	const rechercheSanitized = dbQuoteEscape(nom);

	return db.query.etiquette.findMany({
		where: and(
			eq(etiquette.equipeId, equipeId),
			eq(etiquette.typeId, etiquetteTypeId),
			rechercheBuild(etiquette.nom, rechercheSanitized)
		),
		orderBy: sql`lower(unaccent(${etiquette.nom}))`,
	});
}

export function etiquetteGetManyByEquipe({ equipeId }: { equipeId: Equipe['id'] }) {
	return db.query.etiquette.findMany({
		where: and(eq(etiquette.equipeId, equipeId)),
		orderBy: sql`lower(unaccent(${etiquette.nom}))`,
	});
}

export function etiquetteWithCountGetManyByEquipe({ equipeId }: { equipeId: Equipe['id'] }) {
	const eqEtabEtiquettesQuery = db
		.select({
			etiquetteId: eqEtabEtiquette.etiquetteId,
		})
		.from(eqEtabEtiquette)
		.where(eq(eqEtabEtiquette.equipeId, equipeId));

	const etabCreaEtiquettesQuery = db
		.select({
			etiquetteId: etabCreaEtiquette.etiquetteId,
		})
		.from(etabCreaEtiquette)
		.where(eq(etabCreaEtiquette.equipeId, equipeId));

	const eqLocEtiquettesQuery = db
		.select({
			etiquetteId: eqLocEtiquette.etiquetteId,
		})
		.from(eqLocEtiquette)
		.where(eq(eqLocEtiquette.equipeId, equipeId));

	const union = unionAll(eqEtabEtiquettesQuery, etabCreaEtiquettesQuery, eqLocEtiquettesQuery).as(
		'union'
	);

	return db
		.select({
			...getTableColumns(etiquette),
			etiquetteId: etiquette.id,
			count: count(union.etiquetteId),
		})
		.from(etiquette)
		.leftJoin(union, eq(union.etiquetteId, etiquette.id))
		.where(eq(etiquette.equipeId, equipeId))
		.groupBy(etiquette.id);
}

export function etiquetteRemoveManyByEquipe({
	etiquetteIds,
	equipeId,
}: {
	equipeId: Equipe['id'];
	etiquetteIds: Etiquette['id'][];
}) {
	return db
		.delete(etiquette)
		.where(and(inArray(etiquette.id, etiquetteIds), eq(etiquette.equipeId, equipeId)));
}

export function geoTokenAdd({ devecoId, token }: { devecoId: number; token: string }) {
	return db.insert(geoToken).values({ devecoId, token }).returning();
}

export function geoTokenGet(token: string) {
	return db.query.geoToken.findFirst({
		where: eq(geoToken.token, token),
		with: {
			deveco: {
				with: {
					equipe: true,
				},
			},
		},
	});
}

export function geoTokenGetManyByDeveco(devecoId: Deveco['id']) {
	return db.select().from(geoToken).where(eq(geoToken.devecoId, devecoId));
}

export function rechercheEnregistreeGetManyByDeveco({ devecoId }: { devecoId: Deveco['id'] }) {
	return db.select().from(rechercheEnregistree).where(eq(rechercheEnregistree.devecoId, devecoId));
}

export function rechercheEnregistreeAdd(
	partialRechercheEnregistree: InferInsertModel<typeof rechercheEnregistree>
) {
	return db.insert(rechercheEnregistree).values(partialRechercheEnregistree).returning();
}

export function rechercheEnregistreeDelete({
	devecoId,
	rechercheEnregistreeId,
}: {
	devecoId: number;
	rechercheEnregistreeId: RechercheEnregistree['id'];
}) {
	return db
		.delete(rechercheEnregistree)
		.where(
			and(
				eq(rechercheEnregistree.devecoId, devecoId),
				eq(rechercheEnregistree.id, rechercheEnregistreeId)
			)
		)
		.returning();
}

export function geoTokenRequestAdd({ url, userAgent }: { url: string; userAgent: string | null }) {
	return db
		.insert(geoTokenRequest)
		.values({
			url,
			userAgent,
		})
		.returning();
}

export function geoTokenRequestUpdate({
	geoTokenRequestId,
	geoTokenId,
}: {
	geoTokenRequestId: number;
	geoTokenId: GeoToken['id'];
}) {
	return db
		.update(geoTokenRequest)
		.set({ geoTokenId })
		.where(eq(geoTokenRequest.id, geoTokenRequestId));
}

export function rechercheAdd(
	devecoId: number,
	requete: string,
	entite: EntiteTypeId,
	format: FileFormat | null
) {
	return db.insert(recherche).values({
		devecoId,
		requete,
		entite,
		format,
	});
}

export function rechercheSauvegardeeAdd({
	devecoId,
	equipeId,
	query,
}: {
	devecoId: Deveco['id'];
	equipeId: Equipe['id'];
	query: string;
}) {
	const token = uuidv4();

	return db
		.insert(rechercheSauvegardee)
		.values({
			devecoId,
			equipeId,
			query,
			token,
		})
		.returning({
			token: rechercheSauvegardee.token,
		});
}

export function rechercheSauvegardeeGetByToken({
	token,
}: {
	token: RechercheSauvegardee['token'];
}) {
	return db.query.rechercheSauvegardee.findFirst({
		where: eq(rechercheSauvegardee.token, token),
	});
}

function zonageRelationsBuild() {
	return {
		etiquette: {
			columns: {
				nom: true,
			},
		},
	} as const;
}

export function zonageWithEtiquetteGet({ zonageId }: { zonageId: Zonage['id'] }) {
	return db.query.zonage.findFirst({
		where: eq(zonage.id, zonageId),
		extras: {
			geojson: sql<string>`ST_AsGeoJSON(${zonage.geometrie})`.as('geojson'),
		},
		with: zonageRelationsBuild(),
	});
}

export function zonageGetManyByEquipe({ equipeId }: { equipeId: Equipe['id'] }) {
	return db.query.zonage.findMany({
		where: eq(zonage.equipeId, equipeId),
		with: zonageRelationsBuild(),
	});
}

export function zonageGet({ zonageId }: { zonageId: Zonage['id'] }) {
	return db.query.zonage.findFirst({
		where: eq(zonage.id, zonageId),
	});
}

export function zonageAdd({
	equipeId,
	etiquetteId,
	geometrie,
	description,
}: {
	equipeId: Equipe['id'];
	etiquetteId: Zonage['etiquetteId'];
	geometrie: string;
	description: Zonage['description'];
}) {
	return db
		.insert(zonage)
		.values({
			equipeId,
			etiquetteId,
			geometrie: sql`${geometrie}`,
			description,
		})
		.returning();
}

export function zonageUpdate({
	equipeId,
	zonageId,
	description,
	geometrie,
}: {
	equipeId: Equipe['id'];
	zonageId: Zonage['id'];
	geometrie: string | null;
	description: Zonage['description'];
}) {
	return db
		.update(zonage)
		.set({
			equipeId,
			description,
			...(geometrie ? { geometrie: sql`${geometrie}` } : {}),
		})
		.where(eq(zonage.id, zonageId))
		.returning();
}

export function etiquetteUpdateById({
	etiquetteId,
	nom,
}: {
	etiquetteId: Etiquette['id'];
	nom: Etiquette['nom'];
}) {
	return db.update(etiquette).set({ nom }).where(eq(etiquette.id, etiquetteId));
}

export function communeGetManyByEquipeAndNomOrId({
	equipeId,
	recherche,
	limit = 20,
}: {
	equipeId: number;
	recherche?: string;
	limit?: number | null;
}) {
	const query = db
		.select({
			id: commune.id,
			nom: commune.nom,
			departementId: commune.departementId,
			codePostaux: sql`array_remove(array_agg(${laposteCodeInseeCodePostal.codePostal}), null)`,
		})
		.from(commune)
		.innerJoin(equipeCommuneVue, eq(equipeCommuneVue.communeId, commune.id))
		.leftJoin(laposteCodeInseeCodePostal, eq(laposteCodeInseeCodePostal.codeInsee, commune.id))
		.groupBy(commune.id)
		.orderBy(sql`unaccent(${commune.nom})`);

	if (limit) {
		query.limit(limit);
	}

	const wheres = [eq(equipeCommuneVue.equipeId, equipeId)];

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		const cond = or(
			rechercheBuild(commune.nom, rechercheSanitized),
			like(commune.id, `${rechercheSanitized}%`),
			like(laposteCodeInseeCodePostal.codePostal, `${rechercheSanitized}%`)
		);

		if (cond) {
			wheres.push(cond);
		}
	}

	query.where(and(...wheres));

	return query;
}

export function epciGetManyByEquipeAndNomOrId({
	equipeId,
	recherche,
}: {
	equipeId: Equipe['id'];
	recherche?: string;
}) {
	const query = db
		.select({
			id: epci.id,
			nom: epci.nom,
		})
		.from(epci)
		.innerJoin(commune, eq(commune.epciId, epci.id))
		.innerJoin(equipeCommuneVue, eq(equipeCommuneVue.communeId, commune.id))
		.groupBy(epci.id)
		.limit(20);

	const wheres = [eq(equipeCommuneVue.equipeId, equipeId)];

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		const cond = or(
			rechercheBuild(epci.nom, rechercheSanitized),
			like(epci.id, `${rechercheSanitized}%`)
		);

		if (cond) {
			wheres.push(cond);
		}
	}

	query.where(and(...wheres));

	return query;
}

export function territoireIndustrieGetManyByEquipeAndNomOrId({
	equipeId,
	recherche,
}: {
	equipeId: Equipe['id'];
	recherche?: string;
}) {
	const query = db
		.select({
			id: territoireIndustrie.id,
			nom: territoireIndustrie.nom,
		})
		.from(territoireIndustrie)
		.innerJoin(
			communeTerritoireIndustrie,
			eq(communeTerritoireIndustrie.territoireIndustrieId, territoireIndustrie.id)
		)
		.innerJoin(
			equipeCommuneVue,
			eq(equipeCommuneVue.communeId, communeTerritoireIndustrie.communeId)
		)
		.groupBy(territoireIndustrie.id);

	const wheres = [eq(equipeCommuneVue.equipeId, equipeId)];

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		const cond = or(
			rechercheBuild(territoireIndustrie.nom, rechercheSanitized),
			like(territoireIndustrie.id, `${rechercheSanitized}%`)
		);

		if (cond) {
			wheres.push(cond);
		}
	}

	query.where(and(...wheres));

	return query;
}

export function departementGetManyByEquipeAndNomOrId({
	equipeId,
	recherche,
}: {
	equipeId: Equipe['id'];
	recherche?: string;
}) {
	const query = db
		.select({
			id: departement.id,
			nom: departement.nom,
		})
		.from(departement)
		.innerJoin(commune, eq(commune.departementId, departement.id))
		.innerJoin(equipeCommuneVue, eq(equipeCommuneVue.communeId, commune.id))
		.groupBy(departement.id)
		.limit(20);

	const wheres = [eq(equipeCommuneVue.equipeId, equipeId)];

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		const cond = or(
			rechercheBuild(departement.nom, rechercheSanitized),
			like(departement.id, `${rechercheSanitized}%`)
		);

		if (cond) {
			wheres.push(cond);
		}
	}

	query.where(and(...wheres));

	return query;
}

export function regionGetManyByEquipeAndNomOrId({
	equipeId,
	recherche,
}: {
	equipeId: Equipe['id'];
	recherche?: string;
}) {
	const query = db
		.select({
			id: region.id,
			nom: region.nom,
		})
		.from(region)
		.innerJoin(commune, eq(commune.regionId, region.id))
		.innerJoin(equipeCommuneVue, eq(equipeCommuneVue.communeId, commune.id))
		.groupBy(region.id)
		.limit(20);

	const wheres = [eq(equipeCommuneVue.equipeId, equipeId)];

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		const cond = or(
			rechercheBuild(region.nom, rechercheSanitized),
			like(region.id, `${rechercheSanitized}%`)
		);

		if (cond) {
			wheres.push(cond);
		}
	}

	query.where(and(...wheres));

	return query;
}

export function qpvGetManyByEquipeAndNomOrId({
	equipeId,
	recherche,
}: {
	equipeId: Equipe['id'];
	recherche?: string;
}) {
	const query = db
		.selectDistinctOn([qpv2024.id, qpv2024.nom], {
			id: qpv2024.id,
			nom: qpv2024.nom,
		})
		.from(qpv2024)
		.innerJoin(communeQpv2024, eq(communeQpv2024.qpv2024Id, qpv2024.id))
		.innerJoin(equipeCommuneVue, eq(equipeCommuneVue.communeId, communeQpv2024.communeId))
		.limit(20);

	const wheres = [eq(equipeCommuneVue.equipeId, equipeId)];

	if (recherche) {
		const rechercheSanitized = dbQuoteEscape(recherche);

		const cond = or(
			rechercheBuild(qpv2024.nom, rechercheSanitized),
			ilike(qpv2024.id, `%${rechercheSanitized}%`)
		);

		if (cond) {
			wheres.push(cond);
		}
	}

	query.where(and(...wheres));

	return query;
}

export function contourQpvGetManyByEquipe({
	equipeId,
	qpvIds,
	simplifyTolerance,
	nwse,
}: {
	equipeId: Equipe['id'];
	qpvIds: Qpv2024['id'][] | null;
	simplifyTolerance?: number | null;
	nwse?: number[];
}) {
	const query = db
		.select({
			id: qpv2024.id,
			nom: qpv2024.nom,
			geometrie: simplifyTolerance
				? sql<string>`ST_AsGeoJSON(ST_SimplifyPreserveTopology(${qpv2024.geometrie}, ${simplifyTolerance}))`
				: sql<string>`ST_AsGeoJSON(${qpv2024.geometrie})`,
			box: sql<string>`ST_AsGeoJSON(ST_Extent(${qpv2024.geometrie}))`,
			surface: sql<number>`ST_Area(ST_Transform(${qpv2024.geometrie}, 3857))`,
		})
		.from(qpv2024)
		.innerJoin(communeQpv2024, eq(communeQpv2024.qpv2024Id, qpv2024.id))
		.innerJoin(equipeCommuneVue, eq(equipeCommuneVue.communeId, communeQpv2024.communeId))
		.groupBy(qpv2024.id);

	const wheres = [eq(equipeCommuneVue.equipeId, equipeId)];

	if (qpvIds?.length) {
		wheres.push(inArray(qpv2024.id, qpvIds));
	}

	if (nwse?.length === 4) {
		const [north, west, south, east] = nwse;

		wheres.push(
			sql`ST_Intersects(${qpv2024.geometrie}, ST_MakeEnvelope(${west}, ${south}, ${east}, ${north}, 4326))`
		);
	}

	query.where(and(...wheres));

	return query;
}
