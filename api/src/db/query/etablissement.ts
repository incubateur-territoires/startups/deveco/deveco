import {
	eq,
	ne,
	inArray,
	sql,
	and,
	like,
	gte,
	isNull,
	getTableName,
	getTableColumns,
	desc,
	isNotNull,
	or,
} from 'drizzle-orm';
import { alias } from 'drizzle-orm/pg-core';

import db from '..';
import {
	New,
	Etablissement,
	EtablissementPeriode,
	Entreprise,
	EntreprisePeriode,
	LienSuccession,
	Equipe,
	Deveco,
	BodaccProcedureCollective,
	BodaccProcedureCollectiveImport,
	NafType,
	CategorieJuridiqueType,
	ApiEntrepriseEntrepriseParticipation,
} from '../types';
import { dbQuoteEscape } from '../../lib/utils';
import {
	ApiEntrepriseExercice,
	ApiEntrepriseMandataireSocial,
	ApiEntrepriseBeneficiaireEffectif,
	ApiEntrepriseLiasseFiscale,
} from '../../api/api-entreprise';
import {
	nafType,
	categorieJuridiqueType,
	etablissement,
	etablissementPeriode,
	entreprise,
	entreprisePeriode,
	lienSuccession,
	bodaccProcedureCollective,
	bodaccProcedureCollectiveImport,
	apiEntrepriseEntrepriseBeneficiaireEffectif,
	apiEntrepriseEntrepriseMandataireSocial,
	apiEntrepriseEntrepriseExercice,
	apiEntrepriseEntrepriseLiasseFiscale,
	apiEntrepriseEntrepriseParticipation,
	etalabEtablissementAdresse,
	etablissementGeoloc,
	inpiRatioFinancier,
	egaproEntreprise,
} from '../../db/schema/etablissement';
import { etablissementGeolocalisationVue } from '../../db/schema/equipe-etablissement';
import { equipeCommuneVue } from '../../db/schema/equipe';
import { rappel, echange } from '../../db/schema/suivi';
import {
	devecoEtablissementFavori,
	eqEtabContribution,
} from '../../db/schema/equipe-etablissement';
import * as dbQueryEquipeEtablissement from '../../db/query/equipe-etablissement';
import { qpv2015 } from '../../db/schema/contact';
import { qpv2024 } from '../../db/schema/contact';

export function apiEntrepriseLiasseFiscaleUpsert({
	entrepriseId,
	payload,
	reseauFait = false,
}: {
	entrepriseId: string;
	payload: ApiEntrepriseLiasseFiscale;
	reseauFait?: boolean;
}) {
	return db
		.insert(apiEntrepriseEntrepriseLiasseFiscale)
		.values({
			siren: entrepriseId,
			payload,
			reseauFait,
		})
		.onConflictDoUpdate({
			target: [apiEntrepriseEntrepriseLiasseFiscale.siren],
			set: {
				payload,
				miseAJourAt: new Date(),
				reseauFait,
			},
		})
		.returning();
}

export function apiEntrepriseLiasseFiscaleSetReseauFait({
	entrepriseId,
	reseauFait = true,
}: {
	entrepriseId: string;
	reseauFait?: boolean;
}) {
	return db
		.update(apiEntrepriseEntrepriseLiasseFiscale)
		.set({
			reseauFait,
		})
		.where(eq(apiEntrepriseEntrepriseLiasseFiscale.siren, entrepriseId));
}

export function apiEntrepriseEntrepriseLiasseFiscaleGet({
	entrepriseId,
}: {
	entrepriseId: Entreprise['siren'];
}) {
	return db.query.apiEntrepriseEntrepriseLiasseFiscale.findFirst({
		where: eq(apiEntrepriseEntrepriseLiasseFiscale.siren, entrepriseId),
	});
}

export function apiEntrepriseEntrepriseLiasseFiscaleNotProcessedGet() {
	return db.query.apiEntrepriseEntrepriseLiasseFiscale.findFirst({
		where: eq(apiEntrepriseEntrepriseLiasseFiscale.reseauFait, false),
	});
}

export function apiEntrepriseParticipartionUpsertMany({
	participations,
}: {
	participations: Pick<ApiEntrepriseEntrepriseParticipation, 'source' | 'cible'>[];
}) {
	return db
		.insert(apiEntrepriseEntrepriseParticipation)
		.values(participations)
		.onConflictDoNothing()
		.returning();
}

export function apiEntrepriseEntrepriseBeneficiaireEffectifUpsert({
	entrepriseId,
	payload,
}: {
	entrepriseId: string;
	payload: ApiEntrepriseBeneficiaireEffectif[];
}) {
	return db
		.insert(apiEntrepriseEntrepriseBeneficiaireEffectif)
		.values({
			siren: entrepriseId,
			payload,
		})
		.onConflictDoUpdate({
			target: [apiEntrepriseEntrepriseBeneficiaireEffectif.siren],
			set: {
				payload,
				miseAJourAt: new Date(),
			},
		});
}

export function apiEntrepriseEntrepriseBeneficiaireEffectifGet({
	entrepriseId,
}: {
	entrepriseId: Entreprise['siren'];
}) {
	return db.query.apiEntrepriseEntrepriseBeneficiaireEffectif.findFirst({
		where: eq(apiEntrepriseEntrepriseBeneficiaireEffectif.siren, entrepriseId),
	});
}

export function apiEntrepriseMandataireSocialUpsert({
	entrepriseId,
	payload,
}: {
	entrepriseId: string;
	payload: ApiEntrepriseMandataireSocial[];
}) {
	return db
		.insert(apiEntrepriseEntrepriseMandataireSocial)
		.values({
			siren: entrepriseId,
			payload,
		})
		.onConflictDoUpdate({
			target: apiEntrepriseEntrepriseMandataireSocial.siren,
			set: {
				payload,
				miseAJourAt: new Date(),
			},
		});
}

export function apiEntrepriseEntrepriseMandataireSocialGet({
	entrepriseId,
}: {
	entrepriseId: Entreprise['siren'];
}) {
	return db.query.apiEntrepriseEntrepriseMandataireSocial.findFirst({
		where: eq(apiEntrepriseEntrepriseMandataireSocial.siren, entrepriseId),
	});
}

export function apiEntrepriseEntrepriseExerciceUpsert({
	siren,
	payload,
}: {
	siren: string;
	payload: ApiEntrepriseExercice[];
}) {
	return db
		.insert(apiEntrepriseEntrepriseExercice)
		.values({ siren, payload })
		.onConflictDoUpdate({
			target: [apiEntrepriseEntrepriseExercice.siren],
			set: {
				payload,
				miseAJourAt: new Date(),
			},
		});
}

export function apiEntrepriseEntrepriseExerciceGet({
	entrepriseId,
}: {
	entrepriseId: Entreprise['siren'];
}) {
	return db.query.apiEntrepriseEntrepriseExercice.findFirst({
		where: eq(apiEntrepriseEntrepriseExercice.siren, entrepriseId),
	});
}

export function apiEntrepriseEntrepriseExerciceGetAll() {
	return db.query.apiEntrepriseEntrepriseExercice.findMany({
		columns: {
			siren: true,
		},
		where: isNull(apiEntrepriseEntrepriseExercice.miseAJourAt),
	});
}

export function apiEntrepriseEntrepriseExerciceGetCursor({ size = 100 }: { size?: number }) {
	const query = apiEntrepriseEntrepriseExerciceGetAll();

	return db.cursor(query, { size });
}

export function bodaccProcedureCollectiveAddMany({
	bodaccProcedureCollectives,
}: {
	bodaccProcedureCollectives: BodaccProcedureCollective[];
}) {
	return db.insert(bodaccProcedureCollective).values(bodaccProcedureCollectives);
}

export function bodaccProcedureCollectiveImportNumeroGetMany({ annee }: { annee: number }) {
	return db
		.selectDistinct({
			numero: bodaccProcedureCollectiveImport.numero,
		})
		.from(bodaccProcedureCollectiveImport)
		.where(eq(bodaccProcedureCollectiveImport.annee, annee))
		.orderBy(bodaccProcedureCollectiveImport.numero);
}

export function bodaccProcedureCollectiveImportAdd({
	bodaccProcedureCollectiveImport: bodaccProcedureCollectiveImportValues,
}: {
	bodaccProcedureCollectiveImport: BodaccProcedureCollectiveImport;
}) {
	return db.insert(bodaccProcedureCollectiveImport).values(bodaccProcedureCollectiveImportValues);
}

export function etablissementGetManyById({ sirets }: { sirets: Etablissement['siret'][] }) {
	return db.query.etablissement.findMany({
		where: inArray(etablissement.siret, sirets),
	});
}

export function entrepriseGetManyById({ sirens }: { sirens: Entreprise['siren'][] }) {
	return db.query.entreprise.findMany({
		where: inArray(entreprise.siren, sirens),
	});
}

export function etablissementUpsertMany({
	etablissements: etablissementsValues,
}: {
	etablissements: Etablissement[];
}) {
	return db
		.insert(etablissement)
		.values(etablissementsValues)
		.onConflictDoUpdate({
			target: etablissement.siret,
			set: Object.fromEntries(
				Object.entries(getTableColumns(etablissement)).map(([columnAlias, column]) => [
					columnAlias,
					sql.raw(`excluded.${column.name}`),
				])
			),
		});
}

export function etablissementPeriodeRemoveMany({
	sirets,
}: {
	sirets: EtablissementPeriode['siret'][];
}) {
	return db.delete(etablissementPeriode).where(inArray(etablissementPeriode.siret, sirets));
}

export function etablissementPeriodeAddMany({
	etablissementPeriodes,
}: {
	etablissementPeriodes: EtablissementPeriode[];
}) {
	return db.insert(etablissementPeriode).values(etablissementPeriodes);
}

export function lienSuccessionMissingGetManyByMiseAJourDate({
	miseAJourAt,
}: {
	miseAJourAt: Date;
}) {
	const etablissementPredecesseur = alias(etablissement, 'predecesseur');
	const etablissementSuccesseur = alias(etablissement, 'successeur');

	return db
		.select({
			lien: lienSuccession,
			predecesseurManquant: sql<
				string | null
			>`CASE WHEN ${etablissementPredecesseur.siret} IS NULL THEN ${lienSuccession.siretEtablissementPredecesseur} ELSE NULL END`.as(
				'predecesseur_manquant'
			),
			successeurManquant: sql<
				string | null
			>`CASE WHEN ${etablissementSuccesseur.siret} IS NULL THEN ${lienSuccession.siretEtablissementSuccesseur} ELSE NULL END`.as(
				'successeur_manquant'
			),
		})
		.from(lienSuccession)
		.leftJoin(
			etablissementPredecesseur,
			eq(etablissementPredecesseur.siret, lienSuccession.siretEtablissementPredecesseur)
		)
		.leftJoin(
			etablissementSuccesseur,
			eq(etablissementSuccesseur.siret, lienSuccession.siretEtablissementSuccesseur)
		)
		.where(
			and(
				gte(lienSuccession.miseAJourAt, miseAJourAt),
				or(isNull(etablissementPredecesseur.siret), isNull(etablissementSuccesseur.siret))
			)
		)
		.orderBy(desc(lienSuccession.miseAJourAt));
}

export function lienSuccessionMissingGetCursorByMiseAJourDate({
	miseAJourAt,
	size = 100,
}: {
	miseAJourAt: Date;
	size?: number;
}) {
	const query = lienSuccessionMissingGetManyByMiseAJourDate({ miseAJourAt });

	return db.cursor(query, { size });
}

export function lienSuccessionUpsertMany({
	lienSuccessions,
}: {
	lienSuccessions: LienSuccession[];
}) {
	return db
		.insert(lienSuccession)
		.values(lienSuccessions)
		.onConflictDoUpdate({
			target: [
				lienSuccession.siretEtablissementPredecesseur,
				lienSuccession.siretEtablissementSuccesseur,
				lienSuccession.dateLienSuccession,
			],
			set: Object.fromEntries(
				Object.entries(getTableColumns(lienSuccession)).map(([columnAlias, column]) => [
					columnAlias,
					sql.raw(`excluded.${column.name}`),
				])
			),
		});
}

export function entrepriseUpsertMany({ entreprises }: { entreprises: Entreprise[] }) {
	return db
		.insert(entreprise)
		.values(entreprises)
		.onConflictDoUpdate({
			target: entreprise.siren,
			set: Object.fromEntries(
				Object.entries(getTableColumns(entreprise)).map(([columnAlias, column]) => [
					columnAlias,
					sql.raw(`excluded.${column.name}`),
				])
			),
		});
}

export function entreprisePeriodeRemoveMany({ sirens }: { sirens: Entreprise['siren'][] }) {
	return db.delete(entreprisePeriode).where(inArray(entreprisePeriode.siren, sirens));
}

export function entreprisePeriodeAddMany({
	entreprisePeriodes,
}: {
	entreprisePeriodes: EntreprisePeriode[];
}) {
	return db.insert(entreprisePeriode).values(entreprisePeriodes).returning();
}

function entrepriseMissingWithIdGetMany({ miseAJourAt }: { miseAJourAt: Date }) {
	return db
		.select({ siren: etablissement.siren })
		.from(etablissement)
		.leftJoin(
			entreprise,
			and(eq(entreprise.siren, etablissement.siren), gte(entreprise.miseAJourAt, miseAJourAt))
		)
		.where(and(gte(etablissement.miseAJourAt, miseAJourAt), isNull(entreprise.siren)))
		.groupBy(etablissement.siren);
}

export function entrepriseMissingWithIdGetCursor({ miseAJourAt }: { miseAJourAt: Date }) {
	const query = entrepriseMissingWithIdGetMany({ miseAJourAt });

	return db.cursor<{ siren: Etablissement['siret'] }>(query, { size: 1000 });
}

export function etablissementWithSiretWithoutGeolocalisationGetMany({
	etablissementIds,
	miseAJourAt,
}: {
	etablissementIds?: Etablissement['siret'][];
	miseAJourAt?: Date;
}) {
	return db
		.select({
			siret: etablissement.siret,
		})
		.from(etablissement)
		.leftJoin(etablissementGeoloc, eq(etablissementGeoloc.siret, etablissement.siret))
		.leftJoin(etalabEtablissementAdresse, eq(etalabEtablissementAdresse.siret, etablissement.siret))
		.where(
			and(
				...(miseAJourAt ? [gte(etablissement.miseAJourAt, miseAJourAt)] : []),
				...(etablissementIds?.length ? [inArray(etablissement.siret, etablissementIds)] : []),
				// l'établissement ne possède pas de géolocalisation INSEE
				isNull(etablissementGeoloc.siret),
				// l'établissement ne possède pas de géolocalisation Etalab
				isNull(etalabEtablissementAdresse.siret),
				// l'adresse n'est pas à l'étranger
				isNull(etablissement.codePaysEtrangerEtablissement),
				// l'adresse comporte au moins un nom de voie ou un code postal
				or(
					and(
						ne(etablissement.libelleVoieEtablissement, ''),
						isNotNull(etablissement.libelleVoieEtablissement)
					),
					and(
						ne(etablissement.codePostalEtablissement, ''),
						isNotNull(etablissement.codePostalEtablissement)
					)
				)
			)
		);
}

export function etablissementAdresseWithoutGeolocalisationCursor({
	etablissementIds,
	miseAJourAt,
}: {
	etablissementIds?: Etablissement['siret'][];
	miseAJourAt?: Date;
}) {
	const query = etablissementWithSiretWithoutGeolocalisationGetMany({
		etablissementIds,
		miseAJourAt,
	});

	return db.cursor<{ siret: Etablissement['siret'] }>(query, { size: 1000 });
}

export function uniteLegaleRelations() {
	return {
		catJur: {
			with: {
				parent: true,
			},
		},
		procedures: {
			limit: 1,
		},
		fermeture: {
			columns: {
				fermetureDate: true,
			},
		},
		ratiosFinanciers: {
			columns: {
				chiffreDAffaires: true,
				margeBrute: true,
				ebe: true,
				resultatNet: true,
				dateClotureExercice: true,
			},
			orderBy: inpiRatioFinancier.dateClotureExercice,
			limit: 3,
			where: gte(
				inpiRatioFinancier.dateClotureExercice,
				sql.raw(`'${new Date().getFullYear() - 3}-01-01'`)
			),
		},
	} as const;
}

export function etablissementCommuneRelations() {
	return {
		commune: {
			columns: {
				nom: true,
				zrrTypeId: true,
			},
		},
	} as const;
}

export function etablissementAdresseRelations() {
	return {
		...etablissementCommuneRelations(),
		geoloc: {
			columns: {
				geolocalisation: true,
			},
		},
		ban: {
			columns: {
				geolocalisation: true,
			},
		},
	} as const;
}

export function etablissementRelationsBuild(
	arg:
		| {
				__kind: 'colonne';
				equipeId: Equipe['id'];
		  }
		| { __kind: 'table'; table: typeof rappel | typeof echange }
) {
	return {
		...etablissementAdresseRelations(),
		nafType: {
			with: {
				parent: true,
			},
		},
		fermeture: {
			columns: {
				fermetureDate: true,
			},
		},
		ul: {
			with: uniteLegaleRelations(),
		},
		contributions: {
			where: eq(
				eqEtabContribution.equipeId,
				arg.__kind === 'table' ? arg.table.equipeId : arg.equipeId
			),
			limit: 1,
		},
		contributionsGeolocs: {
			where: isNotNull(eqEtabContribution.geolocalisation),
			columns: {
				geolocalisation: true,
			},
			with: {
				eq: {
					columns: {
						nom: true,
					},
				},
			},
			limit: 1,
		},
		effectifMoyenMensuels: {
			columns: {
				date: true,
				effectif: true,
			},
		},
		effectifMoyenAnnuel: true,
		// permet de récupérer la commune si elle fait partie de l'équipe
		equipeCommune: {
			where:
				/*
				TODO trouver une autre solution pour contourner le bug Drizzle
				qui fait que, quand on lui passe une colonne de table (e.g.,
				rappel.equipeId), il sérialise incorrectement en utilisant l'autre
				table aliasée avant ('rappel_eqEtabs_etab_equipeCommuneVue') au
				lieu de 'rappel'.
			*/
				arg.__kind === 'table'
					? sql.raw(
							`"${equipeCommuneVue.equipeId.name}" = "${getTableName(arg.table)}"."${arg.table.equipeId.name}"`
						)
					: eq(equipeCommuneVue.equipeId, arg.equipeId),
			columns: {
				equipeId: true,
			},
			limit: 1,
		},
	} as const;
}

export function etablissementForStreamRelations() {
	return {
		...etablissementAdresseRelations(),
		nafType: {
			with: {
				parent: true,
			},
		},
		fermeture: {
			columns: {
				fermetureDate: true,
			},
		},
		ul: {
			with: {
				...uniteLegaleRelations(),
				ess: true,
				micro: true,
				procedures: true,
			},
		},
		effectifMoyenAnnuel: true,
		subventions: true,
		acv: true,
		pvd: true,
		va: true,
		afr: true,
		ti: {
			with: {
				ti: true,
			},
		},
		qpv2024: {
			with: {
				qpv: {
					columns: {
						id: true,
						nom: true,
					},
				},
			},
		},
		qpv2015: {
			with: {
				qpv: {
					columns: {
						id: true,
						nom: true,
					},
				},
			},
		},
	} as const;
}

function etablissementFrereActifRelationsBuild({ equipeId }: { equipeId: Equipe['id'] }) {
	return {
		effectifMoyenAnnuel: true,
		equipeCommune: {
			where: eq(equipeCommuneVue.equipeId, equipeId),
			columns: {
				equipeId: true,
			},
			limit: 1,
		},
	} as const;
}

export function etablissementAllRelationsBuild({
	equipeId,
	devecoId,
}: {
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
}) {
	return {
		...etablissementRelationsBuild({ equipeId, __kind: 'colonne' }),
		ul: {
			with: {
				...uniteLegaleRelations(),
				// en plus, par rapport à la vue simple
				procedures: true,
				ess: true,
				micro: true,
				mandataires: true,
				exercices: true,
				benef: true,
				liasse: true,
				siege: {
					with: etablissementAdresseRelations(),
				},
				nafType: {
					with: {
						parent: true,
					},
				},
				etabs: {
					where: eq(etablissement.etatAdministratifEtablissement, 'A'),
					columns: {
						etatAdministratifEtablissement: true,
						codeCommuneEtablissement: true,
					},
					with: etablissementFrereActifRelationsBuild({ equipeId }),
				},
				egapro: {
					orderBy: desc(egaproEntreprise.annee),
					limit: 1,
				},
			},
		},
		// en plus, par rapport à la vue simple
		subventions: true,
		effectifMoyenAnnuel: true,
		acv: true,
		pvd: true,
		va: true,
		afr: true,
		ti: {
			with: {
				ti: true,
			},
		},
		qpv2024: {
			with: {
				qpv: {
					columns: {
						id: true,
						nom: true,
					},
				},
			},
		},
		qpv2015: {
			with: {
				qpv: {
					columns: {
						id: true,
						nom: true,
					},
				},
			},
		},
		preds: {
			with: {
				pred: {
					with: {
						...etablissementRelationsBuild({ equipeId, __kind: 'colonne' }),
						...dbQueryEquipeEtablissement.eqEtabRelationsBuild({ equipeId }),
						favoris: {
							where: eq(devecoEtablissementFavori.devecoId, devecoId),
							limit: 1,
						},
					},
				},
			},
		},
		succs: {
			with: {
				succ: {
					with: {
						...etablissementRelationsBuild({ equipeId, __kind: 'colonne' }),
						...dbQueryEquipeEtablissement.eqEtabRelationsBuild({ equipeId }),
						favoris: {
							where: eq(devecoEtablissementFavori.devecoId, devecoId),
							limit: 1,
						},
					},
				},
			},
		},
		favoris: {
			where: eq(devecoEtablissementFavori.devecoId, devecoId),
			limit: 1,
		},
		...dbQueryEquipeEtablissement.eqEtabAllRelationsBuild({ equipeId, devecoId }),
	} as const;
}

export function etablissementGet({ etablissementId }: { etablissementId: Etablissement['siret'] }) {
	return db.query.etablissement.findFirst({
		where: eq(etablissement.siret, etablissementId),
	});
}

export function entrepriseGet({ entrepriseId }: { entrepriseId: Entreprise['siren'] }) {
	return db.query.entreprise.findFirst({
		where: eq(entreprise.siren, entrepriseId),
	});
}

export function etablissementSiegeGetBySiren({
	entrepriseId,
}: {
	entrepriseId: Entreprise['siren'];
}) {
	return db.query.etablissement.findFirst({
		columns: {
			siret: true,
		},
		where: and(eq(etablissement.siren, entrepriseId), eq(etablissement.etablissementSiege, true)),
	});
}

export function etablissementWithRelationsGet({
	equipeId,
	etablissementId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
}) {
	return db.query.etablissement.findFirst({
		where: eq(etablissement.siret, etablissementId),
		with: etablissementRelationsBuild({ equipeId, __kind: 'colonne' }),
	});
}

export function etablissementWithAllRelationsGet({
	etablissementId,
	equipeId,
	devecoId,
}: {
	etablissementId: Etablissement['siret'];
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
}) {
	return db.query.etablissement.findFirst({
		where: eq(etablissement.siret, etablissementId),
		with: etablissementAllRelationsBuild({
			equipeId,
			devecoId,
		}),
	});
}

export function nafTypeGetMany({ niveaux }: { niveaux: NafType['niveau'][] }) {
	return db.query.nafType.findMany({
		orderBy: desc(nafType.id),
		where: and(eq(nafType.revisionTypeId, 'NAFRev2'), inArray(nafType.niveau, niveaux)),
	});
}

export function nafTypeGetManyByParent({
	parentNafTypeIds,
}: {
	parentNafTypeIds: NafType['id'][];
}) {
	return db.query.nafType.findMany({
		orderBy: desc(nafType.id),
		where: and(eq(nafType.revisionTypeId, 'NAFRev2'), inArray(nafType.parentId, parentNafTypeIds)),
	});
}

export function nafTypeGetManyById({ nafTypeIds }: { nafTypeIds: NafType['id'][] }) {
	return db.query.nafType.findMany({
		orderBy: desc(nafType.id),
		where: and(eq(nafType.revisionTypeId, 'NAFRev2'), inArray(nafType.id, nafTypeIds)),
	});
}

export function nafTypeGetManyByNomOrId(recherche: string, niveaux: NafType['niveau'][]) {
	const sanitizedRecherche = dbQuoteEscape(recherche.replaceAll('.', '').trim());

	return db.query.nafType.findMany({
		orderBy: nafType.id,
		where: and(
			eq(nafType.revisionTypeId, 'NAFRev2'),
			or(
				sql.join(
					[
						sql`unaccent(${nafType.nom})`,
						sql`ilike`,
						sql.raw(`unaccent('%${sanitizedRecherche}%')`),
					],
					sql.raw(' ')
				),
				sql.join(
					[sql`replace(${nafType.id}, '.', '')`, sql`ilike`, sql.raw(`'%${sanitizedRecherche}%'`)],
					sql.raw(' ')
				)
			),
			inArray(nafType.niveau, niveaux)
		),
	});
}

export function categorieJuridiqueGetManyByNiveau({ niveaux }: { niveaux: number[] }) {
	return db.query.categorieJuridiqueType.findMany({
		orderBy: categorieJuridiqueType.id,
		where: inArray(categorieJuridiqueType.niveau, niveaux),
	});
}

export function categorieJuridiqueGetManyById({
	categorieJuridiqueTypeIds,
}: {
	categorieJuridiqueTypeIds: CategorieJuridiqueType['id'][];
}) {
	return db.query.categorieJuridiqueType.findMany({
		orderBy: categorieJuridiqueType.id,
		where: inArray(categorieJuridiqueType.id, categorieJuridiqueTypeIds),
	});
}

export function categorieJuridiqueGetManyByNomOrId(recherche: string, niveaux: number[]) {
	const sanitizedRecherche = dbQuoteEscape(recherche.trim());

	return db.query.categorieJuridiqueType.findMany({
		orderBy: categorieJuridiqueType.id,
		where: and(
			or(
				sql.join(
					[
						sql`unaccent(${categorieJuridiqueType.nom})`,
						sql`ilike`,
						sql.raw(`unaccent('%${sanitizedRecherche}%')`),
					],
					sql.raw(' ')
				),
				like(categorieJuridiqueType.id, `${recherche}%`)
			),
			inArray(categorieJuridiqueType.niveau, niveaux)
		),
	});
}

export function etablissementAdresseEtalabUpsertMany({
	etablissementAdressesEtalab: etablissementAdressesValues,
}: {
	etablissementAdressesEtalab: New<typeof etalabEtablissementAdresse>[];
}) {
	return db
		.insert(etalabEtablissementAdresse)
		.values(etablissementAdressesValues)
		.onConflictDoUpdate({
			target: etalabEtablissementAdresse.siret,
			set: Object.fromEntries(
				Object.entries(getTableColumns(etalabEtablissementAdresse)).map(([columnAlias, column]) => [
					columnAlias,
					sql.raw(`excluded.${column.name}`),
				])
			),
		});
}

export function etablissementMiseAJourAtUpdateMany({
	etablissementIds,
}: {
	etablissementIds: Etablissement['siret'][];
}) {
	return db
		.update(etablissement)
		.set({ miseAJourAt: new Date() })
		.where(inArray(etablissement.siret, etablissementIds));
}

export function etablissementGeolocalisationGet({
	etablissementId,
}: {
	etablissementId: Etablissement['siret'];
}) {
	return db
		.select({
			geolocalisation: etablissementGeolocalisationVue.geolocalisation,
		})
		.from(etablissementGeolocalisationVue)
		.where(eq(etablissementGeolocalisationVue.siret, etablissementId))
		.limit(1);
}

export function etablissementOnQpv2024GetMany({
	etablissementIds,
}: {
	etablissementIds: Etablissement['siret'][];
}) {
	return db
		.select({
			etablissementId: etablissementGeolocalisationVue.siret,
			qpv2024Id: qpv2024.id,
		})
		.from(etablissementGeolocalisationVue)
		.innerJoin(
			qpv2024,
			and(
				inArray(etablissementGeolocalisationVue.siret, etablissementIds),
				sql`ST_Covers(${qpv2024.geometrie}, ${etablissementGeolocalisationVue.geolocalisation})`
			)
		);
}

export function etablissementOnQpv2015GetMany({
	etablissementIds,
}: {
	etablissementIds: Etablissement['siret'][];
}) {
	return db
		.select({
			etablissementId: etablissementGeolocalisationVue.siret,
			qpv2015Id: qpv2015.id,
		})
		.from(etablissementGeolocalisationVue)
		.innerJoin(
			qpv2015,
			and(
				inArray(etablissementGeolocalisationVue.siret, etablissementIds),
				sql`ST_Covers(${qpv2015.geometrie}, ${etablissementGeolocalisationVue.geolocalisation})`
			)
		);
}

export function entrepriseParticipationReseauGet({
	entrepriseId,
}: {
	entrepriseId: Entreprise['siren'];
}) {
	const query = `
WITH RECURSIVE
	adjacency AS (
		SELECT
			source AS node1,
			cible AS node2
		FROM source.api_entreprise__entreprise_participation
		UNION ALL
		SELECT
			cible AS node1,
			source AS node2
		FROM source.api_entreprise__entreprise_participation
	),
	discovered_nodes AS (
		SELECT '${entrepriseId}'::text AS node,
					0 AS level,
					ARRAY['${entrepriseId}'::text] AS path
		UNION ALL
		SELECT
			adj.node2 AS node,
			dn.level + 1 AS level,
			dn.path || adj.node2
		FROM discovered_nodes dn
		JOIN adjacency adj
			ON adj.node1 = dn.node
		WHERE dn.level < 3
			AND adj.node2 <> ALL(dn.path)
	),
	edges AS (
		SELECT
			source,
			cible
		FROM source.api_entreprise__entreprise_participation t
		WHERE t.source IN (SELECT node FROM discovered_nodes)
				AND t.cible IN (SELECT node FROM discovered_nodes)
	)
	SELECT DISTINCT ON (source, cible)
		source as a_siren,
		COALESCE(u1.denomination_usuelle1_unite_legale, u1.denomination_unite_legale) as a,
		cible as b_siren,
		COALESCE(u2.denomination_usuelle1_unite_legale, u2.denomination_unite_legale) as b
	FROM edges
	LEFT JOIN source.insee_sirene_api__unite_legale u1 on u1.siren = source
	LEFT JOIN source.insee_sirene_api__unite_legale u2 ON u2.siren = cible
	ORDER BY source, cible
`;

	return db.execute<{ a: string; a_siren: string; b: string; b_siren: string }>(query);
}
