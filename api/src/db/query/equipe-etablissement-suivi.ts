import { eq, and, inArray, notInArray, isNull, isNotNull, getTableColumns } from 'drizzle-orm';

import db from '../../db';
import {
	Equipe,
	Echange,
	Demande,
	Rappel,
	Brouillon,
	EquipeEtablissementDemandeWithRelations,
	Etablissement,
} from '../../db/types';
import { echange, demande, rappel, brouillon } from '../../db/schema/suivi';
import {
	eqEtabDemande,
	eqEtabEchange,
	eqEtabRappel,
	eqEtabBrouillon,
} from '../../db/schema/equipe-etablissement';
import { compte } from '../../db/schema/admin';

export const eqEtabEchangeSupprimeSub = notInArray(
	eqEtabEchange.echangeId,
	db
		.select({
			echangeId: eqEtabEchange.echangeId,
		})
		.from(eqEtabEchange)
		.innerJoin(echange, eq(echange.id, eqEtabEchange.echangeId))
		.where(isNotNull(echange.suppressionDate))
);

export const eqEtabDemandeSupprimeSub = notInArray(
	eqEtabDemande.demandeId,
	db
		.select({
			demandeId: eqEtabDemande.demandeId,
		})
		.from(eqEtabDemande)
		.innerJoin(demande, eq(demande.id, eqEtabDemande.demandeId))
		.where(isNotNull(demande.suppressionDate))
);

export const eqEtabRappelSupprimeSub = notInArray(
	eqEtabRappel.rappelId,
	db
		.select({
			rappelId: eqEtabRappel.rappelId,
		})
		.from(eqEtabRappel)
		.innerJoin(rappel, eq(rappel.id, eqEtabRappel.rappelId))
		.where(isNotNull(rappel.suppressionDate))
);

export const eqEtabBrouillonSupprimeSub = notInArray(
	eqEtabBrouillon.brouillonId,
	db
		.select({
			brouillonId: eqEtabBrouillon.brouillonId,
		})
		.from(eqEtabBrouillon)
		.innerJoin(brouillon, eq(brouillon.id, eqEtabBrouillon.brouillonId))
		.where(isNotNull(brouillon.suppressionDate))
);

export function eqEtabEchangeAdd({
	equipeId,
	echangeId,
	etablissementId,
}: {
	equipeId: Equipe['id'];
	echangeId: Echange['id'];
	etablissementId: Etablissement['siret'];
}) {
	return db.insert(eqEtabEchange).values({
		etablissementId,
		equipeId,
		echangeId,
	});
}

export function eqEtabDemandeGetMany({
	equipeId,
	etablissementId,
	etablissementIds,
}: {
	equipeId: Equipe['id'];
	etablissementId?: Etablissement['siret'];
	etablissementIds?: Etablissement['siret'][];
	avecClotureDate?: boolean;
	avecCreationDate?: boolean;
}): Promise<EquipeEtablissementDemandeWithRelations[]> {
	const wheres = [eq(eqEtabDemande.equipeId, equipeId), isNull(demande.suppressionDate)];

	if (etablissementIds) {
		wheres.push(
			inArray(eqEtabDemande.etablissementId, etablissementIds),
			eq(eqEtabDemande.equipeId, equipeId)
		);
	} else if (etablissementId) {
		wheres.push(
			eq(eqEtabDemande.etablissementId, etablissementId),
			eq(eqEtabDemande.equipeId, equipeId)
		);
	}

	return db
		.select({
			...getTableColumns(eqEtabDemande),
			demande,
		})
		.from(eqEtabDemande)
		.innerJoin(demande, eq(demande.id, eqEtabDemande.demandeId))
		.where(and(...wheres));
}

export function eqEtabDemandeAdd({
	equipeId,
	etablissementId,
	demandeId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	demandeId: Demande['id'];
}) {
	return db.insert(eqEtabDemande).values({
		etablissementId,
		equipeId,
		demandeId,
	});
}

export async function eqEtabRappelGetMany({
	equipeId,
	etablissementId,
}: {
	equipeId: Equipe['id'];
	etablissementId?: Etablissement['siret'];
}) {
	const where = etablissementId
		? and(eq(eqEtabRappel.etablissementId, etablissementId), eq(eqEtabRappel.equipeId, equipeId))
		: eq(eqEtabRappel.equipeId, equipeId);

	const eqEtabRappels = await db
		.select({
			...getTableColumns(eqEtabRappel),
			rappel,
			affecteCompte: compte,
		})
		.from(eqEtabRappel)
		.innerJoin(rappel, eq(rappel.id, eqEtabRappel.rappelId))
		.leftJoin(compte, eq(rappel.affecteCompteId, compte.id))
		.where(where);

	return eqEtabRappels.map(({ rappel, affecteCompte, ...eqEtabRappel }) => ({
		...eqEtabRappel,
		rappel: {
			...rappel,
			affecteCompte,
		},
	}));
}

export function eqEtabRappelAdd({
	equipeId,
	etablissementId,
	rappelId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	rappelId: Rappel['id'];
}) {
	return db
		.insert(eqEtabRappel)
		.values({
			equipeId,
			etablissementId,
			rappelId,
		})
		.returning();
}

export async function eqEtabBrouillonGetMany({
	equipeId,
	etablissementId,
}: {
	equipeId: Equipe['id'];
	etablissementId?: Etablissement['siret'];
}) {
	const where = etablissementId
		? and(
				eq(eqEtabBrouillon.etablissementId, etablissementId),
				eq(eqEtabBrouillon.equipeId, equipeId)
			)
		: eq(eqEtabBrouillon.equipeId, equipeId);

	const eqEtabBrouillons = await db
		.select({
			...getTableColumns(eqEtabBrouillon),
			brouillon,
		})
		.from(eqEtabBrouillon)
		.innerJoin(brouillon, eq(brouillon.id, eqEtabBrouillon.brouillonId))
		.where(where);

	return eqEtabBrouillons.map(({ brouillon, ...eqEtabBrouillon }) => ({
		...eqEtabBrouillon,
		brouillon: {
			...brouillon,
		},
	}));
}

export function eqEtabBrouillonAdd({
	equipeId,
	etablissementId,
	brouillonId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	brouillonId: Brouillon['id'];
}) {
	return db
		.insert(eqEtabBrouillon)
		.values({
			equipeId,
			etablissementId,
			brouillonId,
		})
		.returning();
}

export async function eqEtabSuiviDateGetMany({
	equipeId,
	etablissementId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
}) {
	const query = db
		.select({
			suiviDates: echange.date,
		})
		.from(eqEtabEchange)
		.innerJoin(echange, eq(echange.id, eqEtabEchange.echangeId))
		.where(
			and(
				eq(eqEtabEchange.equipeId, equipeId),
				eq(eqEtabEchange.etablissementId, etablissementId),
				isNull(echange.suppressionDate)
			)
		)
		.union(
			db
				.select({
					suiviDates: demande.date,
				})
				.from(eqEtabDemande)
				.innerJoin(demande, eq(demande.id, eqEtabDemande.demandeId))
				.where(
					and(
						eq(eqEtabDemande.equipeId, equipeId),
						eq(eqEtabDemande.etablissementId, etablissementId),
						isNull(demande.suppressionDate)
					)
				)
		)
		.union(
			db
				.select({
					suiviDates: rappel.date,
				})
				.from(eqEtabRappel)
				.innerJoin(rappel, eq(rappel.id, eqEtabRappel.rappelId))
				.where(
					and(
						eq(eqEtabRappel.equipeId, equipeId),
						eq(eqEtabRappel.etablissementId, etablissementId),
						isNull(rappel.suppressionDate)
					)
				)
		)
		.as('suivi_dates');

	return db
		.selectDistinct({
			suiviDates: query.suiviDates,
		})
		.from(query);
}
