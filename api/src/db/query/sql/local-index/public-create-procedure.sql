CREATE OR REPLACE PROCEDURE temp_index_local_public_local_create () AS $$ BEGIN

--
-- COMMUNES
--

RAISE NOTICE '% INDEX PUBLIC COMMUNE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_local_public_commune AS (
	SELECT
		DISTINCT id AS territoire_commune_id,
		commune.metropole_id AS territoire_metropole_id,
		commune.epci_id AS territoire_epci_id,
		commune.petr_id AS territoire_petr_id,
		commune.departement_id AS territoire_departement_id,
		commune.region_id AS territoire_region_id,
		commune.zrr_type_id AS territoire_zrr_type_id,
		commune.territoire_industrie_id AS territoire_territoire_industrie_id,
		pvd.commune_id IS NOT NULL AS territoire_pvd,
		acv.commune_id IS NOT NULL AS territoire_acv,
		va.commune_id IS NOT NULL AS territoire_va,
		CASE
			WHEN afr.commune_id IS NULL THEN NULL
			WHEN afr.integral THEN TRUE
			WHEN NOT afr.integral THEN FALSE
		END AS territoire_afr
	FROM commune
	LEFT JOIN commune_pvd pvd ON pvd.commune_id = commune.id
	LEFT JOIN commune_acv acv ON acv.commune_id = commune.id
	LEFT JOIN commune_va va ON va.commune_id = commune.id
	LEFT JOIN commune_afr afr ON afr.commune_id = commune.id
);
CREATE INDEX IF NOT EXISTS temp_index_local_public_idx_commune ON temp_index_local_public_commune(territoire_commune_id);
COMMIT;

--
-- LOCAL GEOLOCALISATION
--

RAISE NOTICE '% INDEX PUBLIC LOCAL GEOLOCALISATION', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_local_public_local_geolocalisation AS (
	SELECT
		DISTINCT ON (local_id)
		local_id,
		COALESCE(
			G.geolocalisation,
			V.geolocalisation
		) AS adresse_geolocalisation
	FROM local_adresse V
	LEFT JOIN equipe__local_contribution G USING (local_id)
	ORDER BY local_id, G.geolocalisation
);
CREATE INDEX IF NOT EXISTS temp_index_local_public_idx_geolocalisation ON temp_index_local_public_local_geolocalisation(local_id);
COMMIT;

--
-- LOCAL ADRESSE
--

RAISE NOTICE '% INDEX PUBLIC LOCAL ADRESSE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_local_public_local_adresse AS (
	SELECT
		LA.local_id,
		LA.numero AS adresse_numero,
		LA.voie_nom AS adresse_voie_nom,
		LA.code_postal AS adresse_code_postal,
		G.adresse_geolocalisation AS adresse_geolocalisation
	FROM local_adresse LA
	LEFT JOIN temp_index_local_public_local_geolocalisation G USING (local_id)
);
CREATE INDEX IF NOT EXISTS temp_index_local_public_idx_local_adresse ON temp_index_local_public_local_adresse(local_id);
COMMIT;

--
-- LOCAL QPV 2015
--

RAISE NOTICE '% INDEX PUBLIC LOCAL QPV 2015', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_local_public_local_qpv_2015 AS (
	SELECT
		local_id,
		qpv.id AS adresse_qpv_2015_id
	FROM temp_index_local_public_local_adresse adresse
	JOIN qpv_2015 qpv ON ST_Covers(qpv.geometrie, adresse.adresse_geolocalisation)
	WHERE adresse.adresse_geolocalisation IS NOT NULL
);
CREATE INDEX IF NOT EXISTS temp_index_local_public_idx_local_qpv_2015 ON temp_index_local_public_local_qpv_2015(local_id);
COMMIT;

--
-- LOCAL QPV 2024
--

RAISE NOTICE '% INDEX PUBLIC LOCAL QPV 2024', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_local_public_local_qpv_2024 AS (
	SELECT
		local_id,
		qpv.id AS adresse_qpv_2024_id
	FROM temp_index_local_public_local_adresse adresse
	JOIN qpv_2024 qpv ON ST_Covers(qpv.geometrie, adresse.adresse_geolocalisation)
	WHERE adresse.adresse_geolocalisation IS NOT NULL
);
CREATE INDEX IF NOT EXISTS temp_index_local_public_idx_local_qpv_2024 ON temp_index_local_public_local_qpv_2024(local_id);
COMMIT;

--
-- LOCAL PROPRIETAIRE MORALE
--

RAISE NOTICE '% INDEX PUBLIC LOCAL PROPRIETAIRE MORALE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_local_public_local_proprietaire_morale AS (
	SELECT
		lpm.local_id,
		json_agg(entreprise_id) AS proprietaire_morale_entreprise_ids,
		json_agg(nom) AS proprietaire_morale_noms
	FROM local__proprietaire_personne_morale lpm
	JOIN proprietaire_personne_morale pm ON pm.id = lpm.proprietaire_personne_morale_id
	GROUP BY local_id
);
CREATE INDEX IF NOT EXISTS temp_index_local_public_idx_local_proprietaire_morale ON temp_index_local_public_local_proprietaire_morale(local_id);
COMMIT;

--
-- LOCAL PROPRIETAIRE PHYSIQUE
--

RAISE NOTICE '% INDEX PUBLIC LOCAL PROPRIETAIRE PHYSIQUE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_local_public_local_proprietaire_physique AS (
	SELECT
		lpp.local_id,
		json_agg(prenom || ' ' || nom) AS proprietaire_physique_noms
	FROM local__proprietaire_personne_physique lpp
	JOIN proprietaire_personne_physique pp ON pp.id = lpp.proprietaire_personne_physique_id
	GROUP BY local_id
);
CREATE INDEX IF NOT EXISTS temp_index_local_public_idx_local_proprietaire_physique ON temp_index_local_public_local_proprietaire_physique(local_id);
COMMIT;

--
-- LOCAL
--

RAISE NOTICE '% INDEX PUBLIC LOCAL', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_local_public_local AS (
	SELECT
		local.id AS local_id,
		local.commune_id AS commune_id,
		local.invariant AS local_invariant,
		local.section AS local_section,
		local.parcelle AS local_parcelle,
		local.local_categorie_type_id AS local_categorie_type_id,
		local.local_nature_type_id AS local_nature_type_id,
		local.niveau AS local_niveau,
		local.surface_vente + local.surface_reserve + local.surface_exterieure_non_couverte AS local_surface_totale,
		local.surface_vente AS local_surface_vente,
		local.surface_reserve AS local_surface_reserve,
		local.surface_exterieure_non_couverte AS local_surface_exterieure_non_couverte,
		local.surface_stationnement_couvert AS local_surface_stationnement_couvert,
		local.surface_stationnement_non_couvert AS local_surface_stationnement_non_couvert,
		local.actif AS local_actif
	FROM local
);
CREATE INDEX IF NOT EXISTS temp_index_local_public_idx_local ON temp_index_local_public_local(local_id);
COMMIT;

--
-- LOCAL RELATIONS
--

RAISE NOTICE '% INDEX PUBLIC RELATIONS', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_local_public_local_relations AS (
	SELECT
		local.local_id,
		local.local_invariant,
		local.local_section,
		local.local_parcelle,
		local.local_categorie_type_id,
		local.local_nature_type_id,
		local.local_niveau,
		local.local_surface_totale,
		local.local_surface_vente,
		local.local_surface_reserve,
		local.local_surface_exterieure_non_couverte,
		local.local_surface_stationnement_couvert,
		local.local_surface_stationnement_non_couvert,
		local.local_actif,
		adresse.adresse_numero,
		adresse.adresse_voie_nom,
		adresse.adresse_code_postal,
		adresse.adresse_geolocalisation,
		proprietaire_morale.proprietaire_morale_entreprise_ids,
		proprietaire_morale.proprietaire_morale_noms,
		proprietaire_physique.proprietaire_physique_noms,
		commune.*
	FROM temp_index_local_public_local local
	JOIN temp_index_local_public_local_adresse adresse USING (local_id)
	LEFT JOIN temp_index_local_public_local_proprietaire_morale proprietaire_morale USING (local_id)
	LEFT JOIN temp_index_local_public_local_proprietaire_physique proprietaire_physique USING (local_id)
	LEFT JOIN temp_index_local_public_commune commune ON territoire_commune_id = commune_id
);
CREATE INDEX IF NOT EXISTS temp_index_local_public_idx_local_relations ON temp_index_local_public_local_relations(local_id);
COMMIT;

--
-- INDEXATION LOCAL ET DONNEE PERSO
--

RAISE NOTICE '% INDEX PUBLIC LOCAL ET DONNEE PERSO', timeofday();

CREATE TABLE IF NOT EXISTS temp_index_local_complet AS (
	SELECT *
	FROM temp_index_local_public_local_relations
	LEFT JOIN temp_index_local_perso_equipe epi USING (local_id)
);
COMMIT;

CREATE INDEX IF NOT EXISTS temp_index_local_complet_idx_local ON temp_index_local_complet(local_id);
COMMIT;

END $$ LANGUAGE plpgsql;
