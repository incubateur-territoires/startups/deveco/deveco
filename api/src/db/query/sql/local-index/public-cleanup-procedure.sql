CREATE OR REPLACE PROCEDURE temp_index_local_public_local_cleanup () AS $$ BEGIN

DROP TABLE IF EXISTS temp_index_local_complet;
DROP TABLE IF EXISTS temp_index_local_public_local;
DROP TABLE IF EXISTS temp_index_local_public_local_relations;
DROP TABLE IF EXISTS temp_index_local_public_local_proprietaire_physique;
DROP TABLE IF EXISTS temp_index_local_public_local_proprietaire_morale;
DROP TABLE IF EXISTS temp_index_local_public_local_geolocalisation;
DROP TABLE IF EXISTS temp_index_local_public_local_adresse;

DROP TABLE IF EXISTS temp_index_local_public_commune;

COMMIT;

END $$ LANGUAGE plpgsql;
