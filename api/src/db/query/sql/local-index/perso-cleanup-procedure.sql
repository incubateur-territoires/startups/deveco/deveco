CREATE
OR REPLACE PROCEDURE temp_index_local_perso_local_cleanup () AS $$ BEGIN

DROP TABLE IF EXISTS temp_index_local_perso_local_maj;
DROP TABLE IF EXISTS temp_index_local_perso_etiquette;
DROP TABLE IF EXISTS temp_index_local_perso_contact;
DROP TABLE IF EXISTS temp_index_local_perso_demande;
DROP TABLE IF EXISTS temp_index_local_perso_favori;
DROP TABLE IF EXISTS temp_index_local_perso_exogene;
DROP TABLE IF EXISTS temp_index_local_perso_consultation;
DROP TABLE IF EXISTS temp_index_local_perso_contribution;
DROP TABLE IF EXISTS temp_index_local_perso_suivi_dates;
DROP TABLE IF EXISTS temp_index_local_perso_local_geolocalisation;
DROP TABLE IF EXISTS temp_index_local_perso_equipe;

COMMIT;

END $$ LANGUAGE plpgsql;
