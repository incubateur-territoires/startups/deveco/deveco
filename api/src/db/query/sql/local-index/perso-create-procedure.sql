CREATE OR REPLACE PROCEDURE temp_index_local_perso_local_create() AS $$ BEGIN

--
-- CONTACT
--

RAISE NOTICE '% INDEX PERSO CONTACT', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_local_perso_contact AS (
	SELECT
		eec.local_id,
		eec.equipe_id,
		json_agg(prenom || ' ' || nom) AS json
	FROM equipe__local__contact eec
	JOIN contact ON contact.id = eec.contact_id
	WHERE trim(prenom || ' ' || nom) <> '' AND (prenom IS NOT NULL OR nom IS NOT NULL)
	GROUP BY
		eec.local_id,
		eec.equipe_id
);
CREATE INDEX IF NOT EXISTS temp_index_local_perso_idx_contact ON temp_index_local_perso_contact(local_id);
COMMIT;

--
-- ETIQUETTE
--

RAISE NOTICE '% INDEX PERSO ETIQUETTE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_local_perso_etiquette AS (
	SELECT
		local_id,
		equipe_id,
		json_agg(etiquette_id) AS json
	FROM equipe__local__etiquette
	GROUP BY
		local_id,
		equipe_id
);
CREATE INDEX IF NOT EXISTS temp_index_local_perso_idx_etiquette ON temp_index_local_perso_etiquette(local_id);
COMMIT;

--
-- CONSULTATION
--

RAISE NOTICE '% INDEX PERSO CONSULTATION', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_local_perso_consultation AS (
	SELECT
		DISTINCT ON (local_id, equipe_id) local_id,
		equipe_id,
		json_strip_nulls(json_build_object(
				'consultation_at', created_at,
				'consultation_str', created_at::text
		)) AS json
	FROM action_equipe_local
	WHERE action_type_id = 'affichage'
	ORDER BY
		local_id,
		equipe_id,
		created_at DESC
);
CREATE INDEX IF NOT EXISTS temp_index_local_perso_idx_consultation ON temp_index_local_perso_consultation(local_id);
COMMIT;

--
-- LOCAL CONTRIBUTION
--

RAISE NOTICE '% INDEX PERSO LOCAL CONTRIBUTION', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_local_perso_contribution AS (
	SELECT
		local_id,
		equipe_id,
		json_strip_nulls(json_build_object('nom', nom, 'occupe', occupe)) AS json
	FROM equipe__local_contribution
	WHERE nom IS NOT NULL OR occupe IS NOT NULL
	GROUP BY
		local_id,
		equipe_id
);
CREATE INDEX IF NOT EXISTS temp_index_local_perso_idx_contribution ON temp_index_local_perso_contribution(local_id);
COMMIT;

--
-- LOCAL SUIVI DATES
--

RAISE NOTICE '% INDEX PERSO LOCAL SUIVI DATES', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_local_perso_suivi_dates AS (
	SELECT
		local_id,
		equipe_id,
		json_agg(DISTINCT date::date) AS json
		FROM (
			SELECT eee.local_id,
				eee.equipe_id,
				rappel.date
			FROM equipe__local__rappel eee
			JOIN rappel on rappel.id = eee.rappel_id
			UNION
			SELECT eee.local_id,
				eee.equipe_id,
				echange.date
			FROM equipe__local__echange eee
			JOIN echange on echange.id = eee.echange_id
			UNION
			SELECT eee.local_id,
				eee.equipe_id,
				demande.date
			FROM equipe__local__demande eee
			JOIN demande on demande.id = eee.demande_id
		) t
	GROUP BY
		local_id,
		equipe_id
);
CREATE INDEX IF NOT EXISTS temp_index_local_perso_idx_suivi_dates ON temp_index_local_perso_suivi_dates(local_id);
COMMIT;


--
-- LOCAL GEOLOCALISATION
--

RAISE NOTICE '% INDEX PERSO LOCAL GEOLOCALISATION', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_local_perso_local_geolocalisation AS (
	SELECT
		DISTINCT ON (local_id)
		local_id,
		geolocalisation AS adresse_geolocalisation
	FROM equipe__local_contribution
	ORDER BY local_id, geolocalisation
);
CREATE INDEX IF NOT EXISTS temp_index_local_perso_idx_geolocalisation ON temp_index_local_perso_local_geolocalisation(local_id);

--
-- EQUIPE
--

RAISE NOTICE '% INDEX PERSO EQUIPE', timeofday();

CREATE TABLE IF NOT EXISTS temp_index_local_perso_equipe AS (
	WITH equipe_perso_local AS (
	  SELECT DISTINCT local_id, equipe_id
		FROM (
			SELECT local_id, equipe_id FROM temp_index_local_perso_etiquette
			UNION
			SELECT local_id, equipe_id FROM temp_index_local_perso_contact
			UNION
			SELECT local_id, equipe_id FROM temp_index_local_perso_etiquette
			UNION
			SELECT local_id, equipe_id FROM temp_index_local_perso_consultation
			UNION
			SELECT local_id, equipe_id FROM temp_index_local_perso_contribution
			UNION
			SELECT local_id, equipe_id FROM temp_index_local_perso_suivi_dates
		) t
  ),
  equipe_perso_par_local AS (
		SELECT
			local_id,
			jsonb_strip_nulls(
					jsonb_build_object(
					'equipe_id', equipe_id,
					'etiquettes', etiquettes.json,
					'contacts_noms', contacts.json,
					'consultation_at', consultation.json,
					'contribution', contribution.json,
					'suivi_dates', suivi_dates.json
				) || COALESCE(consultation.json::jsonb, '{}'::jsonb)
			) AS equipe
		FROM equipe_perso_local
		LEFT JOIN LATERAL (
			SELECT json
			FROM temp_index_local_perso_etiquette
			WHERE temp_index_local_perso_etiquette.local_id = equipe_perso_local.local_id
				AND temp_index_local_perso_etiquette.equipe_id = equipe_perso_local.equipe_id
		) AS etiquettes ON TRUE
		LEFT JOIN LATERAL  (
			SELECT json
			FROM temp_index_local_perso_contact
			WHERE temp_index_local_perso_contact.local_id = equipe_perso_local.local_id
				AND temp_index_local_perso_contact.equipe_id = equipe_perso_local.equipe_id
		) AS contacts ON TRUE
		LEFT JOIN LATERAL  (
			SELECT json
			FROM temp_index_local_perso_consultation
			WHERE temp_index_local_perso_consultation.local_id = equipe_perso_local.local_id
				AND temp_index_local_perso_consultation.equipe_id = equipe_perso_local.equipe_id
		) AS consultation ON TRUE
		LEFT JOIN LATERAL  (
			SELECT json
			FROM temp_index_local_perso_contribution
			WHERE temp_index_local_perso_contribution.local_id = equipe_perso_local.local_id
				AND temp_index_local_perso_contribution.equipe_id = equipe_perso_local.equipe_id
		) AS contribution ON TRUE
		LEFT JOIN LATERAL  (
			SELECT json
			FROM temp_index_local_perso_suivi_dates
			WHERE temp_index_local_perso_suivi_dates.local_id = equipe_perso_local.local_id
				AND temp_index_local_perso_suivi_dates.equipe_id = equipe_perso_local.equipe_id
		) AS suivi_dates ON TRUE
	)
	SELECT
		local_id,
		json_agg(equipe) AS equipes,
		COALESCE(G.adresse_geolocalisation, LA.geolocalisation) AS adresse_geolocalisation_perso
	FROM equipe_perso_par_local
	LEFT JOIN temp_index_local_perso_local_geolocalisation G USING (local_id)
	LEFT JOIN local_adresse LA USING (local_id)
	GROUP BY local_id, G.adresse_geolocalisation, LA.geolocalisation
);
CREATE INDEX IF NOT EXISTS temp_index_local_perso_idx_equipe ON temp_index_local_perso_equipe(local_id);

END $$ LANGUAGE plpgsql;
