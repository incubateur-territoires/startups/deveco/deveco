CREATE OR REPLACE PROCEDURE import_local(departement_id text) AS $$ BEGIN

INSERT INTO local (
	id,
	invariant,
	local_nature_type_id,
	local_categorie_type_id,
	commune_id,
	section,
	parcelle,
	niveau,
	surface_vente,
	surface_reserve,
	surface_exterieure_non_couverte,
	surface_stationnement_couvert,
	surface_stationnement_non_couvert
)
SELECT
	LEFT(idcom, 2) || invar,
	invar,
	cconlc,
	typeact,
	idcom,
	ccosec,
	dnupla,
	dniv,
	sprincp,
	ssecp,
	ssecncp,
	sparkp,
	sparkncp
FROM source.cerema__local
WHERE LEFT(idcom, 2) = $1
ON CONFLICT (id) DO NOTHING;
COMMIT;
--
INSERT INTO local_adresse (
	local_id,
	commune_id,
	voie_nom,
	numero,
	geolocalisation
)
SELECT
	LEFT(idcom, 2) || invar,
	idcom,
	replace(dvoilib, '  ', ' '),
	COALESCE(regexp_replace(dnvoiri, '^0*', '', 'g'), '') || COALESCE(dindic, ''),
	geometrie_wgs
FROM source.cerema__local
WHERE LEFT(idcom, 2) = $1
ON CONFLICT (local_id) DO UPDATE SET geolocalisation = excluded.geolocalisation;
COMMIT;

END $$ LANGUAGE plpgsql;
