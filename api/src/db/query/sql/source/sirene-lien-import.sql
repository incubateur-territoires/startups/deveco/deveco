-- Suppression des tables temporaires si elles existent
DROP TABLE IF EXISTS temp_sirene_lien;
DROP TABLE IF EXISTS temp_sirene_lien_process;
DROP TABLE IF EXISTS temp_sirene_lien_done;

-- Création des tables temporaires
CREATE TABLE temp_sirene_lien (LIKE source.insee_sirene_api__lien_succession);
CREATE TABLE temp_sirene_lien_process (LIKE source.insee_sirene_api__lien_succession INCLUDING ALL);
CREATE TABLE temp_sirene_lien_done (LIKE source.insee_sirene_api__lien_succession INCLUDING ALL);

-- Import des données dans la table temporaire
\copy temp_sirene_lien (siret_etablissement_predecesseur, siret_etablissement_successeur, date_lien_succession, transfert_siege, continuite_economique, date_dernier_traitement_lien_succession) FROM '../../sources/sirene/StockEtablissementLiensSuccession_utf8.csv' CSV HEADER DELIMITER ',';

-- suppression des doublons
-- on garde la plus récente `date_dernier_traitement_lien_succession`
DELETE FROM temp_sirene_lien T1
USING temp_sirene_lien T2
WHERE
	T1.ctid < T2.ctid
	AND T1.date_dernier_traitement_lien_succession <= T2.date_dernier_traitement_lien_succession
	AND T1.siret_etablissement_predecesseur = T2.siret_etablissement_predecesseur
	AND T1.siret_etablissement_successeur = T2.siret_etablissement_successeur
	AND T1.date_lien_succession = T2.date_lien_succession;

DO $$
DECLARE
	counter INTEGER := 1;
BEGIN
	WHILE (SELECT 1 FROM temp_sirene_lien LIMIT 1) LOOP
		RAISE NOTICE '% DEBUT 10K / %', timeofday(), counter;

		-- Extraction de 10K lignes pour traitement
		INSERT INTO temp_sirene_lien_process
		SELECT * FROM temp_sirene_lien
		LIMIT 10000;

		RAISE NOTICE '% DELETE LIEN / %', timeofday(), counter;

		-- Suppression des lignes existantes de la table si elles ont une date inférieures au fichier
		DELETE FROM source.insee_sirene_api__lien_succession
		USING temp_sirene_lien_process
		WHERE insee_sirene_api__lien_succession.siret_etablissement_predecesseur = temp_sirene_lien_process.siret_etablissement_predecesseur
			AND insee_sirene_api__lien_succession.siret_etablissement_successeur = temp_sirene_lien_process.siret_etablissement_successeur
			AND insee_sirene_api__lien_succession.date_lien_succession = temp_sirene_lien_process.date_lien_succession
			AND insee_sirene_api__lien_succession.date_dernier_traitement_lien_succession < temp_sirene_lien_process.date_dernier_traitement_lien_succession;

		RAISE NOTICE '% INSERT LIEN / %', timeofday(), counter;

		-- Mise à jour de la table
		INSERT INTO source.insee_sirene_api__lien_succession
		SELECT * FROM temp_sirene_lien_process
		ON CONFLICT (siret_etablissement_predecesseur, siret_etablissement_successeur, date_lien_succession)
		DO UPDATE SET
			date_dernier_traitement_lien_succession = excluded.date_dernier_traitement_lien_succession,
			transfert_siege = excluded.transfert_siege,
			continuite_economique = excluded.continuite_economique;

		RAISE NOTICE '% INSERT DONE / %', timeofday(), counter;

		-- Ajout des données traitées dans la table "done"
		INSERT INTO temp_sirene_lien_done
		SELECT * FROM temp_sirene_lien_process;

		RAISE NOTICE '% DELETE TEMP / %', timeofday(), counter;

		-- Suppression des données déjà traitées
		DELETE FROM temp_sirene_lien
		USING temp_sirene_lien_process
		WHERE temp_sirene_lien.siret_etablissement_predecesseur = temp_sirene_lien_process.siret_etablissement_predecesseur
			AND temp_sirene_lien.siret_etablissement_successeur = temp_sirene_lien_process.siret_etablissement_successeur
			AND temp_sirene_lien.date_lien_succession = temp_sirene_lien_process.date_lien_succession;

		RAISE NOTICE '% TRUNCATE PROCESS / %', timeofday(), counter;

		-- Nettoyage de la table process
		TRUNCATE temp_sirene_lien_process;

		COMMIT;

		counter := counter + 1;
	END LOOP;
END;
$$;
