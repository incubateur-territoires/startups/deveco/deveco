CREATE TABLE IF NOT EXISTS temp_egapro__representation (
	annee VARCHAR,
	siren VARCHAR,
	nom_entreprise VARCHAR,
	region VARCHAR,
	departement VARCHAR,
	pays VARCHAR,
	code_naf VARCHAR,
	cadres_pourcentage_femmes VARCHAR,
	cadres_pourcentage_hommes VARCHAR,
	membres_pourcentage_femmes VARCHAR,
	membres_pourcentage_hommes VARCHAR
);

\copy temp_egapro__representation (annee, siren, nom_entreprise, region, departement, pays, code_naf, cadres_pourcentage_femmes, cadres_pourcentage_hommes, membres_pourcentage_femmes, membres_pourcentage_hommes) FROM '_sources/egapro__representation_2025.csv' delimiter ',' csv header;

-- on corrige le format du montant en virgule vers point
UPDATE temp_egapro__representation
SET
	cadres_pourcentage_femmes = REPLACE(cadres_pourcentage_femmes, ',', '.'),
	cadres_pourcentage_hommes = REPLACE(cadres_pourcentage_hommes, ',', '.'),
	membres_pourcentage_femmes = REPLACE(membres_pourcentage_femmes, ',', '.'),
	membres_pourcentage_hommes = REPLACE(membres_pourcentage_hommes, ',', '.');

CREATE TABLE IF NOT EXISTS temp_egapro__index_egalite (
	annee VARCHAR,
	structure VARCHAR,
	tranche_effectifs VARCHAR,
	siren VARCHAR,
	raison_sociale VARCHAR,
	nom_ues VARCHAR,
	entreprises_ues VARCHAR,
	region VARCHAR,
	departement VARCHAR,
	pays VARCHAR,
	code_naf VARCHAR,
	note_ecart_remuneration VARCHAR,
	note_ecart_taux__augmentation_hors_promotion VARCHAR,
	note_ecart_taux_promotion VARCHAR,
	note_ecart_taux_augmentation VARCHAR,
	note_retour_conge_maternite VARCHAR,
	note_hautes_remunerations VARCHAR,
	note_index VARCHAR
);

\copy temp_egapro__index_egalite (annee, structure, tranche_effectifs, siren, raison_sociale, nom_ues, entreprises_ues, region, departement, pays, code_naf, note_ecart_remuneration, note_ecart_taux__augmentation_hors_promotion, note_ecart_taux_promotion, note_ecart_taux_augmentation, note_retour_conge_maternite, note_hautes_remunerations, note_index) FROM '_sources/egapro__egalite_fh_2025.csv' delimiter ',' csv header;

-- on corrige le format du montant en virgule vers point
UPDATE temp_egapro__index_egalite
SET
	note_ecart_remuneration = REPLACE(note_ecart_remuneration, ',', '.'),
	note_ecart_taux__augmentation_hors_promotion = REPLACE(note_ecart_taux__augmentation_hors_promotion, ',', '.'),
	note_ecart_taux_promotion = REPLACE(note_ecart_taux_promotion, ',', '.'),
	note_ecart_taux_augmentation = REPLACE(note_ecart_taux_augmentation, ',', '.'),
	note_retour_conge_maternite = REPLACE(note_retour_conge_maternite, ',', '.'),
	note_hautes_remunerations = REPLACE(note_hautes_remunerations, ',', '.'),
	note_index = REPLACE(note_index, ',', '.');

INSERT INTO source.egapro__entreprise (
	siren,
	annee,
	note_ecart_remuneration,
	note_ecart_taux__augmentation_hors_promotion,
	note_ecart_taux_promotion,
	note_ecart_taux_augmentation,
	note_retour_conge_maternite,
	note_hautes_remunerations,
	note_index,
	cadres_pourcentage_femmes,
	cadres_pourcentage_hommes,
	membres_pourcentage_femmes,
	membres_pourcentage_hommes
)
WITH siren_tout AS (
	SELECT siren, annee
	FROM temp_egapro__representation
	UNION
	SELECT siren, annee
	FROM temp_egapro__index_egalite
)
SELECT DISTINCT ON (siren, annee)
	siren,
	annee::integer,
	-- index égalité
	(CASE WHEN note_ecart_remuneration = 'NC' THEN NULL ELSE note_ecart_remuneration END)::integer,
	(CASE WHEN note_ecart_taux__augmentation_hors_promotion = 'NC' THEN NULL ELSE note_ecart_taux__augmentation_hors_promotion END)::integer,
	(CASE WHEN note_ecart_taux_promotion = 'NC' THEN NULL ELSE note_ecart_taux_promotion END)::integer,
	(CASE WHEN note_ecart_taux_augmentation = 'NC' THEN NULL ELSE note_ecart_taux_augmentation END)::integer,
	(CASE WHEN note_retour_conge_maternite = 'NC' THEN NULL ELSE note_retour_conge_maternite END)::integer,
	(CASE WHEN note_hautes_remunerations = 'NC' THEN NULL ELSE note_hautes_remunerations END)::integer,
	(CASE WHEN note_index = 'NC' THEN NULL ELSE note_index END)::integer,
	-- représentation équilibrée
	(CASE WHEN cadres_pourcentage_femmes = 'NC' THEN NULL ELSE cadres_pourcentage_femmes END)::double precision,
	(CASE WHEN cadres_pourcentage_hommes = 'NC' THEN NULL ELSE cadres_pourcentage_hommes END)::double precision,
	(CASE WHEN membres_pourcentage_femmes = 'NC' THEN NULL ELSE membres_pourcentage_femmes END)::double precision,
	(CASE WHEN membres_pourcentage_hommes = 'NC' THEN NULL ELSE membres_pourcentage_hommes END)::double precision
FROM siren_tout
LEFT JOIN temp_egapro__representation USING (siren, annee)
LEFT JOIN temp_egapro__index_egalite USING (siren, annee)
ORDER BY siren, annee DESC;

DROP TABLE temp_egapro__representation;
DROP TABLE temp_egapro__index_egalite;
