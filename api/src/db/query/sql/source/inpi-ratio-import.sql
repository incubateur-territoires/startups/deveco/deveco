-- Suppression des tables temporaires si elles existent
DROP TABLE IF EXISTS temp_inpi_ratio;
DROP TABLE IF EXISTS temp_inpi_ratio_process;
DROP TABLE IF EXISTS temp_inpi_ratio_done;


-- Création des tables temporaires
CREATE TABLE temp_inpi_ratio (LIKE source.inpi__ratio_financier INCLUDING ALL);
CREATE TABLE temp_inpi_ratio_process (LIKE source.inpi__ratio_financier INCLUDING ALL);
CREATE TABLE temp_inpi_ratio_done (LIKE source.inpi__ratio_financier INCLUDING ALL);

-- Import des données dans la table temporaire
\copy temp_ratio_financier (
	siren,
	date_cloture_exercice,
	chiffre_d_affaires,
	marge_brute,
	ebe,
	ebit,
	resultat_net,
	taux_d_endettement,
	ratio_de_liquidite,
	ratio_de_vetuste,
	autonomie_financiere,
	poids_bfr_exploitation_sur_ca,
	couverture_des_interets,
	caf_sur_ca,
	capacite_de_remboursement,
	marge_ebe,
	resultat_courant_avant_impots_sur_ca,
	poids_bfr_exploitation_sur_ca_jours,
	rotation_des_stocks_jours,
	credit_clients_jours,
	credit_fournisseurs_jours,
	type_bilan,
	confidentiality
) from 'ratios_inpi_bce.csv' delimiter ';' csv header;

DO $$
DECLARE
	counter INTEGER := 1;
BEGIN
	WHILE (SELECT 1 FROM temp_inpi_ratio LIMIT 1) LOOP
		RAISE NOTICE '% DEBUT 10K / %', timeofday(), counter;

		-- Extraction de 10K lignes pour traitement
		INSERT INTO temp_inpi_ratio_process
		SELECT * FROM temp_inpi_ratio
		LIMIT 10000;

		RAISE NOTICE '% DELETE RATIO / %', timeofday(), counter;

		-- Suppression des lignes existantes de la table si elles ont une date inférieures au fichier
		DELETE FROM source.inpi__ratio_financier
		USING temp_inpi_ratio_process
		WHERE inpi__ratio_financier.siren = temp_inpi_ratio_process.siren
			AND inpi__ratio_financier.type_bilan = temp_inpi_ratio_process.type_bilan
			AND inpi__ratio_financier.date_cloture_exercice = temp_inpi_ratio_process.date_cloture_exercice;

		RAISE NOTICE '% INSERT RATIO / %', timeofday(), counter;

		-- Mise à jour de la table
		INSERT INTO source.inpi__ratio_financier
		SELECT * FROM temp_inpi_ratio_process;

		RAISE NOTICE '% INSERT DONE / %', timeofday(), counter;

		-- Ajout des données traitées dans la table "done"
		INSERT INTO temp_inpi_ratio_done
		SELECT * FROM temp_inpi_ratio_process;

		RAISE NOTICE '% DELETE TEMP / %', timeofday(), counter;

		-- Suppression des données déjà traitées
		DELETE FROM temp_inpi_ratio
		USING temp_inpi_ratio_process
		WHERE temp_inpi_ratio.siren = temp_inpi_ratio_process.siren
			AND temp_inpi_ratio.type_bilan = temp_inpi_ratio_process.type_bilan
			AND temp_inpi_ratio.date_cloture_exercice = temp_inpi_ratio_process.date_cloture_exercice;

		RAISE NOTICE '% TRUNCATE PROCESS / %', timeofday(), counter;

		-- Nettoyage de la table process
		TRUNCATE temp_inpi_ratio_process;

		COMMIT;

		counter := counter + 1;
	END LOOP;
END;
$$;
