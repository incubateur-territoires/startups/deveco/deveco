-- Suppression des tables temporaires si elles existent
DROP TABLE IF EXISTS temp_rcd_etablissement_emm;
DROP TABLE IF EXISTS temp_rcd_etablissement_emm_process;
DROP TABLE IF EXISTS temp_rcd_etablissement_emm_done;
DROP TABLE IF EXISTS temp_rcd_etablissement_emm_annuel;

-- Création des tables temporaires
CREATE TABLE temp_rcd_etablissement_emm (LIKE source.rcd__etablissement_effectif_emm_13_mois INCLUDING ALL);
CREATE TABLE temp_rcd_etablissement_emm_process (LIKE source.rcd__etablissement_effectif_emm_13_mois INCLUDING ALL);
CREATE TABLE temp_rcd_etablissement_emm_done (LIKE source.rcd__etablissement_effectif_emm_13_mois INCLUDING ALL);
CREATE TABLE temp_rcd_etablissement_emm_annuel (LIKE source.rcd__etablissement_effectif_emm_13_mois_annuel INCLUDING ALL);

set DateStyle to ISO, DMY;
-- Import des données dans la table temporaire
\copy temp_rcd_etablissement_emm (siret, date, effectif) FROM './export_rcd_anct.csv' CSV HEADER DELIMITER ',';

DO $$
DECLARE
    counter INTEGER := 1;
BEGIN
    WHILE (SELECT 1 FROM temp_rcd_etablissement_emm LIMIT 1) LOOP
        RAISE NOTICE '% DEBUT 100K / %', timeofday(), counter;

        -- Extraction de 100K lignes pour traitement
        INSERT INTO temp_rcd_etablissement_emm_process
        SELECT * FROM temp_rcd_etablissement_emm
				LIMIT 100000;

        RAISE NOTICE '% DELETE EMM_HISTORIQUE / %', timeofday(), counter;

        -- Suppression des lignes existantes de la table historique
        DELETE FROM source.rcd__etablissement_effectif_emm_historique
        USING temp_rcd_etablissement_emm_process
        WHERE rcd__etablissement_effectif_emm_historique.siret = temp_rcd_etablissement_emm_process.siret
          AND rcd__etablissement_effectif_emm_historique.date = temp_rcd_etablissement_emm_process.date;

        RAISE NOTICE '% INSERT EMM_HISTORIQUE / %', timeofday(), counter;

        -- Mise à jour de la table historique
        INSERT INTO source.rcd__etablissement_effectif_emm_historique
        SELECT * FROM temp_rcd_etablissement_emm_process;

        RAISE NOTICE '% INSERT DONE / %', timeofday(), counter;

        -- Ajout des données traitées dans la table "done"
        INSERT INTO temp_rcd_etablissement_emm_done
        SELECT * FROM temp_rcd_etablissement_emm_process;

        RAISE NOTICE '% DELETE TEMP / %', timeofday(), counter;

        -- Suppression des données déjà traitées
        DELETE FROM temp_rcd_etablissement_emm
        USING temp_rcd_etablissement_emm_process
        WHERE temp_rcd_etablissement_emm.siret = temp_rcd_etablissement_emm_process.siret
          AND temp_rcd_etablissement_emm.date = temp_rcd_etablissement_emm_process.date;

        RAISE NOTICE '% TRUNCATE PROCESS / %', timeofday(), counter;

        -- Nettoyage de la table process
        TRUNCATE temp_rcd_etablissement_emm_process;

				COMMIT;

        counter := counter + 1;
    END LOOP;
END;
$$;

-- Recalcul des effectifs moyens annuels
INSERT INTO temp_rcd_etablissement_emm_annuel(siret, date, effectif)
SELECT
    siret,
    max(date) AS date,
    floor(100 * sum(effectif) / count(*)) / 100
FROM temp_rcd_etablissement_emm_done
GROUP BY siret;

-- Renommage de la table 13 mois
DROP TABLE source.rcd__etablissement_effectif_emm_13_mois;
ALTER TABLE temp_rcd_etablissement_emm_done
	RENAME CONSTRAINT "temp_rcd_etablissement_emm_done_pkey" TO "pk_rcd__etablissement_effectif_emm_13_mois";
ALTER TABLE temp_rcd_etablissement_emm_done
	RENAME TO rcd__etablissement_effectif_emm_13_mois;
ALTER TABLE rcd__etablissement_effectif_emm_13_mois SET SCHEMA "source";

-- Renommage de la table annuel
DROP TABLE source.rcd__etablissement_effectif_emm_13_mois_annuel;
ALTER TABLE temp_rcd_etablissement_emm_annuel
	RENAME CONSTRAINT "temp_rcd_etablissement_emm_annuel_pkey"
	TO "pk_rcd__etablissement_effectif_emm_13_mois_annuel";
ALTER TABLE temp_rcd_etablissement_emm_annuel
	RENAME
	TO "rcd__etablissement_effectif_emm_13_mois_annuel";
ALTER TABLE rcd__etablissement_effectif_emm_13_mois_annuel SET SCHEMA "source";
