DROP TABLE IF EXISTS temp_sirene_doublon;

CREATE TABLE temp_sirene_doublon (LIKE source.insee_sirene_api__unite_legale_doublon);

\copy temp_sirene_doublon (
	 siren,
	 siren_doublon,
	 date_dernier_traitement_doublon
) FROM './StockDoublons_utf8.csv' CSV HEADER DELIMITER ',';

-- suppression des doublons
-- on garde la plus récente `date_dernier_traitement_doublon`
DELETE FROM temp_sirene_doublon T1
	 USING temp_sirene_doublon T2
WHERE
	 T1.ctid < T2.ctid
	 AND T1.date_dernier_traitement_doublon < T2.date_dernier_traitement_doublon
	 AND T1.siren = T2.siren
	 AND T1.siren_doublon = T2.siren_doublon;

INSERT INTO source.insee_sirene_api__unite_legale_doublon
	 SELECT *
	 FROM temp_sirene_doublon
	 ON CONFLICT (
		siren,
		siren_doublon
	 )
	 DO NOTHING;
