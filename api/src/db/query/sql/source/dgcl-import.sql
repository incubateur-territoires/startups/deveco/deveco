ALTER TABLE source.dgcl__subventions
	ADD COLUMN nom_attr varchar,
	ADD COLUMN ref varchar,
	ADD COLUMN nom_benef varchar,
	ADD COLUMN conditions varchar,
	ADD COLUMN rae varchar,
	ADD COLUMN notif varchar,
	ADD COLUMN pourcentage varchar,
	ADD COLUMN code_dep varchar,
	ADD COLUMN libelle_dep varchar
	ADD COLUMN montant_str varchar;

\copy source.dgcl__subventions (
		nom_attr,
		code_dep,
		libelle_dep,
		attributaire,
		convention_date,
		ref,
		siret,
		nom_benef,
		objet,
		montant_str,
		nature,
		conditions,
		versement_date,
		rae,
		notif,
		pourcentage
	)
	from '_sources/data-gouv-subventions-p147-en-2023.csv' delimiter ';' csv header;

-- on corrige le format du montant en virgule vers point
UPDATE source.dgcl__subventions
	SET montant = REPLACE(montant_str, ',', '.')::double precision
	WHERE montant_str IS NOT NULL;

ALTER TABLE source.dgcl__subventions
	DROP COLUMN nom_attr,
	DROP COLUMN ref,
	DROP COLUMN nom_benef,
	DROP COLUMN conditions,
	DROP COLUMN rae,
	DROP COLUMN notif,
	DROP COLUMN pourcentage,
	DROP COLUMN code_dep,
	DROP COLUMN libelle_dep,
	DROP COLUMN montant_str;

UPDATE source.dgcl__subventions SET type = 'P147';
