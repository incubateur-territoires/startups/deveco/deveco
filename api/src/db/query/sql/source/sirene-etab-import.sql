DROP TABLE IF EXISTS temp_sirene_etablissement;

CREATE TABLE temp_sirene_etablissement (LIKE source.insee_sirene_api__etablissement);

ALTER TABLE temp_sirene_etablissement ALTER column mise_a_jour_at SET DEFAULT NOW();

\copy temp_sirene_etablissement (
	 siren,
	 nic,
	 siret,
	 statut_diffusion_etablissement,
	 date_creation_etablissement,
	 tranche_effectifs_etablissement,
	 annee_effectifs_etablissement,
	 activite_principale_registre_metiers_etablissement,
	 date_dernier_traitement_etablissement,
	 etablissement_siege,
	 nombre_periodes_etablissement,
	 complement_adresse_etablissement,
	 numero_voie_etablissement,
	 indice_repetition_etablissement,
	 dernier_numero_voie_etablissement,
	 indice_repetition_dernier_numero_voie_etablissement,
	 type_voie_etablissement,
	 libelle_voie_etablissement,
	 code_postal_etablissement,
	 libelle_commune_etablissement,
	 libelle_commune_etranger_etablissement,
	 distribution_speciale_etablissement,
	 code_commune_etablissement,
	 code_cedex_etablissement,
	 libelle_cedex_etablissement,
	 code_pays_etranger_etablissement,
	 libelle_pays_etranger_etablissement,
	 identifiant_adresse_etablissement,
	 coordonnee_lambert_abscisse_etablissement,
	 coordonnee_lambert_ordonnee_etablissement,
	 date_debut,
	 etat_administratif_etablissement,
	 enseigne1_etablissement,
	 enseigne2_etablissement,
	 enseigne3_etablissement,
	 denomination_usuelle_etablissement,
	 activite_principale_etablissement,
	 nomenclature_activite_principale_etablissement,
	 caractere_employeur_etablissement
) FROM './StockEtablissement_utf8.csv' CSV HEADER DELIMITER ',';

ALTER TABLE temp_sirene_etablissement ADD CONSTRAINT temp_pk_sirene_etablissement PRIMARY KEY (siret);

-- process etablissement

DROP TABLE IF EXISTS temp_sirene_process;
DROP TABLE IF EXISTS temp_sirene_done;

CREATE TABLE temp_sirene_process (LIKE temp_sirene_etablissement INCLUDING ALL);
CREATE TABLE temp_sirene_done (LIKE temp_sirene_etablissement INCLUDING ALL);

do $$
	 declare
		counter integer := 1;

	 BEGIN
		WHILE (SELECT 1 FROM temp_sirene_etablissement LIMIT 1) loop
				RAISE NOTICE '% DEBUT 100K / %', timeofday(), counter;

				INSERT INTO temp_sirene_process
				SELECT * FROM temp_sirene_etablissement
				LIMIT 100000;

				RAISE NOTICE '% DELETE 100K / %', timeofday(), counter;

				DELETE FROM source.insee_sirene_api__etablissement
				USING temp_sirene_process
				WHERE
					temp_sirene_process.siret = source.insee_sirene_api__etablissement.siret
					AND (
						source.insee_sirene_api__etablissement.mise_a_jour_at is null
						OR source.insee_sirene_api__etablissement.mise_a_jour_at < '2024-08-01'
					);

				RAISE NOTICE '% INSERT 100K / %', timeofday(), counter;

				INSERT INTO source.insee_sirene_api__etablissement
				SELECT *
				FROM temp_sirene_process
				ON CONFLICT (siret) DO NOTHING;

				RAISE NOTICE '% DONE 100K / %', timeofday(), counter;

				INSERT INTO temp_sirene_done
				SELECT * FROM temp_sirene_process;

				RAISE NOTICE '% DELETE 100K / %', timeofday(), counter;

				DELETE FROM temp_sirene_etablissement
				USING temp_sirene_process
				WHERE temp_sirene_process.siret = temp_sirene_etablissement.siret;

				RAISE NOTICE '% TRUNCATE 100K / %', timeofday(), counter;

				TRUNCATE temp_sirene_process;

				COMMIT;

				counter := counter + 1;
		end loop;
	 END;
$$;
