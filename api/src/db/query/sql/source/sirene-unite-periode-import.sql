DROP TABLE IF EXISTS temp_sirene_unite_legale_periode;

CREATE TABLE temp_sirene_unite_legale_periode (LIKE source.insee_sirene_api__unite_legale_periode);

ALTER TABLE temp_sirene_unite_legale_periode ALTER column mise_a_jour_at SET DEFAULT NOW();

\copy temp_sirene_unite_legale_periode (
	 siren,
	 date_fin,
	 date_debut,
	 etat_administratif_unite_legale,
	 changement_etat_administratif_unite_legale,
	 nom_unite_legale,
	 changement_nom_unite_legale,
	 nom_usage_unite_legale,
	 changement_nom_usage_unite_legale,
	 denomination_unite_legale,
	 changement_denomination_unite_legale,
	 denomination_usuelle_1_unite_legale,
	 denomination_usuelle_2_unite_legale,
	 denomination_usuelle_3_unite_legale,
	 changement_denomination_usuelle_unite_legale,
	 categorie_juridique_unite_legale,
	 changement_categorie_juridique_unite_legale,
	 activite_principale_unite_legale,
	 nomenclature_activite_principale_unite_legale,
	 changement_activite_principale_unite_legale,
	 nic_siege_unite_legale,
	 changement_nic_siege_unite_legale,
	 economie_sociale_solidaire_unite_legale,
	 changement_economie_sociale_solidaire_unite_legale,
	 societe_mission_unite_legale,
	 changement_societe_mission_unite_legale,
	 caractere_employeur_unite_legale,
	 changement_caractere_employeur_unite_legale
) FROM './StockUniteLegaleHistorique_utf8.csv' CSV HEADER DELIMITER ',';

ALTER TABLE temp_sirene_unite_legale_periode ADD CONSTRAINT temp_uk_sirene_unite_legale_periode UNIQUE (siren, date_debut);

-- process unite legale periode

DROP TABLE IF EXISTS temp_sirene_process;
DROP TABLE IF EXISTS temp_sirene_done;

CREATE TABLE temp_sirene_process (LIKE temp_sirene_unite_legale_periode INCLUDING ALL);
CREATE TABLE temp_sirene_done (LIKE temp_sirene_unite_legale_periode INCLUDING ALL);

do $$
	 declare
		counter integer := 1;

	 BEGIN
		WHILE (SELECT 1 FROM temp_sirene_unite_legale_periode LIMIT 1) loop
			RAISE NOTICE '% DEBUT 100K / %', timeofday(), counter;

			INSERT INTO temp_sirene_process
			SELECT * FROM temp_sirene_unite_legale_periode
			LIMIT 100000;

			RAISE NOTICE '% DELETE 100K / %', timeofday(), counter;

			DELETE FROM source.insee_sirene_api__unite_legale
			USING temp_sirene_process
			WHERE
				temp_sirene_process.siren = source.insee_sirene_api__unite_legale.siren
				AND temp_sirene_process.debut_date = source.insee_sirene_api__unite_legale.debut_date
				AND source.insee_sirene_api__unite_legale.mise_a_jour_at < '2024-08-01';

			RAISE NOTICE '% INSERT 100K / %', timeofday(), counter;

			INSERT INTO source.insee_sirene_api__unite_legale
			SELECT *
			FROM temp_sirene_process
			ON CONFLICT (siren, date_debut) DO NOTHING;

			RAISE NOTICE '% DONE 100K / %', timeofday(), counter;

			INSERT INTO temp_sirene_done
			SELECT * FROM temp_sirene_process;

			RAISE NOTICE '% DELETE 100K / %', timeofday(), counter;

			DELETE FROM temp_sirene_unite_legale_periode
			USING temp_sirene_process
			WHERE temp_sirene_process.siren = temp_sirene_unite_legale_periode.siren;

			RAISE NOTICE '% TRUNCATE 100K / %', timeofday(), counter;

			TRUNCATE temp_sirene_process;

			COMMIT;

			counter := counter + 1;
		end loop;
	 END;
$$;
