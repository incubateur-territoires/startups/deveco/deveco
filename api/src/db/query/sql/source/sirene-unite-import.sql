DROP TABLE IF EXISTS temp_sirene_unite_legale;

CREATE TABLE temp_sirene_unite_legale (LIKE source.insee_sirene_api__unite_legale);

ALTER TABLE temp_sirene_unite_legale ALTER column mise_a_jour_at SET DEFAULT NOW();

\copy temp_sirene_unite_legale (
	 siren,
	 statut_diffusion_unite_legale,
	 unite_purgee_unite_legale,
	 date_creation_unite_legale,
	 sigle_unite_legale,
	 sexe_unite_legale,
	 prenom1_unite_legale,
	 prenom2_unite_legale,
	 prenom3_unite_legale,
	 prenom4_unite_legale,
	 prenom_usuel_unite_legale,
	 pseudonyme_unite_legale,
	 identifiant_association_unite_legale,
	 tranche_effectifs_unite_legale,
	 annee_effectifs_unite_legale,
	 date_dernier_traitement_unite_legale,
	 nombre_periodes_unite_legale,
	 categorie_entreprise,
	 annee_categorie_entreprise,
	 date_debut,
	 etat_administratif_unite_legale,
	 nom_unite_legale,
	 nom_usage_unite_legale,
	 denomination_unite_legale,
	 denomination_usuelle1_unite_legale,
	 denomination_usuelle2_unite_legale,
	 denomination_usuelle3_unite_legale,
	 categorie_juridique_unite_legale,
	 activite_principale_unite_legale,
	 nomenclature_activite_principale_unite_legale,
	 nic_siege_unite_legale,
	 economie_sociale_solidaire_unite_legale,
	 societe_mission_unite_legale,
	 caractere_employeur_unite_legale
) FROM './StockUniteLegale_utf8.csv' CSV HEADER DELIMITER ',';

ALTER TABLE temp_sirene_unite_legale ADD CONSTRAINT temp_pk_sirene_unite_legale PRIMARY KEY (siren);

-- process unite legale

DROP TABLE IF EXISTS temp_sirene_process;
DROP TABLE IF EXISTS temp_sirene_done;

CREATE TABLE temp_sirene_process (LIKE temp_sirene_unite_legale INCLUDING ALL);
CREATE TABLE temp_sirene_done (LIKE temp_sirene_unite_legale INCLUDING ALL);

do $$
	 declare
		counter integer := 1;

	 BEGIN
		WHILE (SELECT 1 FROM temp_sirene_unite_legale LIMIT 1) loop
				RAISE NOTICE '% DEBUT 100K / %', timeofday(), counter;

				INSERT INTO temp_sirene_process
				SELECT * FROM temp_sirene_unite_legale
				LIMIT 100000;

				RAISE NOTICE '% DELETE 100K / %', timeofday(), counter;

				DELETE FROM source.insee_sirene_api__unite_legale
				USING temp_sirene_process
				WHERE temp_sirene_process.siren = source.insee_sirene_api__unite_legale.siren
				AND source.insee_sirene_api__unite_legale.mise_a_jour_at < '2024-08-01';

				RAISE NOTICE '% INSERT 100K / %', timeofday(), counter;

				INSERT INTO source.insee_sirene_api__unite_legale
				SELECT *
				FROM temp_sirene_process
				ON CONFLICT (siren) DO NOTHING;

				RAISE NOTICE '% DONE 100K / %', timeofday(), counter;

				INSERT INTO temp_sirene_done
				SELECT * FROM temp_sirene_process;

				RAISE NOTICE '% DELETE 100K / %', timeofday(), counter;

				DELETE FROM temp_sirene_unite_legale
				USING temp_sirene_process
				WHERE temp_sirene_process.siren = temp_sirene_unite_legale.siren;

				RAISE NOTICE '% TRUNCATE 100K / %', timeofday(), counter;

				TRUNCATE temp_sirene_process;

				COMMIT;

				counter := counter + 1;
		end loop;
	 END;
$$;
