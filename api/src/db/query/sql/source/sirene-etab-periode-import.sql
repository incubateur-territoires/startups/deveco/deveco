DROP TABLE IF EXISTS temp_sirene_etablissement_periode;

CREATE TABLE temp_sirene_etablissement_periode (LIKE source.insee_sirene_api__etablissement_periode);

ALTER TABLE temp_sirene_etablissement_periode ALTER column mise_a_jour_at SET DEFAULT NOW();

ALTER TABLE temp_sirene_etablissement_periode ADD COLUMN siren VARCHAR(9);
ALTER TABLE temp_sirene_etablissement_periode ADD COLUMN nic VARCHAR(5);
ALTER TABLE temp_sirene_etablissement_periode ADD COLUMN changement_denomination_usuelle_etablissement BOOLEAN;

\copy temp_sirene_etablissement_periode (
	 siren,
	 nic,
	 siret,
	 date_fin,
	 date_debut,
	 etat_administratif_etablissement,
	 changement_etat_administratif_etablissement,
	 enseigne1_etablissement,
	 enseigne2_etablissement,
	 enseigne3_etablissement,
	 changement_enseigne_etablissement,
	 denomination_usuelle_etablissement,
	 changement_denomination_usuelle_etablissement,
	 activite_principale_etablissement,
	 nomenclature_activite_principale_etablissement,
	 changement_activite_principale_etablissement,
	 caractere_employeur_etablissement,
	 changement_caractere_employeur_etablissement
) FROM './StockEtablissementHistorique_utf8.csv' CSV HEADER DELIMITER ',';

ALTER TABLE temp_sirene_etablissement_periode ADD CONSTRAINT temp_uk_sirene_etablissement_periode UNIQUE (siret, date_debut);

-- process etablissement periode

DROP TABLE IF EXISTS temp_sirene_process;
DROP TABLE IF EXISTS temp_sirene_done;

CREATE TABLE temp_sirene_process (LIKE temp_sirene_etablissement_periode INCLUDING ALL);
CREATE TABLE temp_sirene_done (LIKE temp_sirene_etablissement_periode INCLUDING ALL);

do $$
	 declare
		counter integer := 1;

	 BEGIN
		WHILE (SELECT 1 FROM temp_sirene_etablissement_periode LIMIT 1) loop
			RAISE NOTICE '% DEBUT 100K / %', timeofday(), counter;

			INSERT INTO temp_sirene_process
			SELECT * FROM temp_sirene_etablissement_periode
			LIMIT 100000;

			RAISE NOTICE '% DELETE 100K / %', timeofday(), counter;

			DELETE FROM source.insee_sirene_api__etablissement
			USING temp_sirene_process
			WHERE
				temp_sirene_process.siret = source.insee_sirene_api__etablissement.siret
				AND temp_sirene_process.date_debut = source.insee_sirene_api__etablissement.date_debut
				AND (
					source.insee_sirene_api__etablissement.mise_a_jour_at is null
					OR source.insee_sirene_api__etablissement.mise_a_jour_at < '2024-08-01'
				);

			RAISE NOTICE '% INSERT 100K / %', timeofday(), counter;

			INSERT INTO source.insee_sirene_api__etablissement
			SELECT *
			FROM temp_sirene_process
			ON CONFLICT (siret, date_debut) DO NOTHING;

			RAISE NOTICE '% DONE 100K / %', timeofday(), counter;

			INSERT INTO temp_sirene_done
			SELECT * FROM temp_sirene_process;

			RAISE NOTICE '% DELETE 100K / %', timeofday(), counter;

			DELETE FROM temp_sirene_etablissement_periode
			USING temp_sirene_process
			WHERE temp_sirene_process.siret = temp_sirene_etablissement_periode.siret;

			RAISE NOTICE '% TRUNCATE 100K / %', timeofday(), counter;

			TRUNCATE temp_sirene_process;

			COMMIT;

			counter := counter + 1;
		end loop;
	 END;
$$;
