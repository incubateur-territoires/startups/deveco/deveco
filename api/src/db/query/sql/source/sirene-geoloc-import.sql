DROP TABLE IF EXISTS temp_sirene_geolocalisation;
CREATE TABLE temp_sirene_geolocalisation (LIKE source.insee_sirene__etablissement_geoloc INCLUDING ALL);
-->
\copy temp_sirene_geolocalisation (
	siret,
	x,
	y,
	qualite_xy,
	epsg,
	plg_qp24,
	plg_iris,
	plg_zus,
	plg_qp15,
	plg_qva,
	plg_code_commune,
	distance_precision,
	qualite_qp24,
	qualite_iris,
	qualite_zus,
	qualite_qp15,
	qualite_qva,
	y_latitude,
	x_longitude
) FROM './GeolocalisationEtablissement_Sirene_pour_etudes_statistiques_utf8.csv' CSV HEADER DELIMITER ';';

DROP TABLE IF EXISTS temp_sirene_geolocalisation_process;
DROP TABLE IF EXISTS temp_sirene_geolocalisation_done;

CREATE TABLE temp_sirene_geolocalisation_process (LIKE source.insee_sirene__etablissement_geoloc INCLUDING ALL);
CREATE TABLE temp_sirene_geolocalisation_done (LIKE source.insee_sirene__etablissement_geoloc INCLUDING ALL);

-- remplace la table `source.insee_sirene__etablissement_geoloc` par la table temporaire, car aucun traitement supplémentaire n'est effectué après-coup

DO $$
	DECLARE
		counter integer := 1;

	BEGIN
		WHILE (SELECT 1 FROM temp_sirene_geolocalisation LIMIT 1) LOOP
			RAISE NOTICE '% DEBUT 100K / %', timeofday(), counter;

			INSERT INTO temp_sirene_geolocalisation_process
			SELECT *
			FROM temp_sirene_geolocalisation limit 100000;

			RAISE NOTICE '% DELETE 100K / %', timeofday(), counter;

			DELETE FROM source.insee_sirene__etablissement_geoloc
			USING temp_sirene_geolocalisation_process
			WHERE temp_sirene_geolocalisation_process.siret = source.insee_sirene__etablissement_geoloc.siret;

			RAISE NOTICE '% INSERT 100K / %', timeofday(), counter;

			INSERT INTO source.insee_sirene__etablissement_geoloc
			SELECT *
			FROM temp_sirene_geolocalisation_process;

			RAISE NOTICE '% DONE 100K / %', timeofday(), counter;

			INSERT INTO temp_sirene_geolocalisation_done
			SELECT *
			FROM temp_sirene_geolocalisation_process;

			RAISE NOTICE '% DELETE 100K / %', timeofday(), counter;

			DELETE FROM temp_sirene_geolocalisation
			USING temp_sirene_geolocalisation_process
			WHERE temp_sirene_geolocalisation.siret = temp_sirene_geolocalisation_process.siret;

			RAISE NOTICE '% TRUNCATE 100K / %', timeofday(), counter;

			TRUNCATE temp_sirene_geolocalisation_process;

			COMMIT;

			counter := counter + 1;
		END LOOP;
	END;
$$;

DROP TABLE IF EXISTS temp_sirene_geolocalisation;
DROP TABLE IF EXISTS temp_sirene_geolocalisation_process;
DROP TABLE IF EXISTS temp_sirene_geolocalisation_done;

-- supprime les BANifications qui ne sont plus nécessaires, remplacées par les nouvelles données

CREATE TABLE temp_sirene_etalab_adresse AS (
	SELECT siret
	FROM source.etalab__etablissement_adresse
	JOIN source.insee_sirene__etablissement_geoloc USING (siret)
);

CREATE TABLE temp_sirene_etalab_adresse_process (LIKE temp_sirene_etalab_adresse INCLUDING ALL);
CREATE TABLE temp_sirene_etalab_adresse_done (LIKE temp_sirene_etalab_adresse INCLUDING ALL);

do $$
	declare
		counter integer := 1;

	BEGIN
		WHILE (SELECT 1 FROM temp_sirene_etalab_adresse LIMIT 1) loop
			RAISE NOTICE '% DEBUT 100K / %', timeofday(), counter;

			INSERT INTO temp_sirene_etalab_adresse_process
			SELECT * FROM temp_sirene_etalab_adresse
			LIMIT 100000;

			RAISE NOTICE '% DELETE 100K / %', timeofday(), counter;

			DELETE FROM source.etalab__etablissement_adresse
			USING temp_sirene_etalab_adresse_process
			WHERE temp_sirene_etalab_adresse_process.siret = source.etalab__etablissement_adresse.siret;

			RAISE NOTICE '% DONE 100K / %', timeofday(), counter;

			INSERT INTO temp_sirene_etalab_adresse_done
			SELECT * FROM temp_sirene_etalab_adresse_process;

			RAISE NOTICE '% DELETE 100K / %', timeofday(), counter;

			DELETE FROM temp_sirene_etalab_adresse
			USING temp_sirene_etalab_adresse_process
			WHERE temp_sirene_etalab_adresse_process.siret = temp_sirene_etalab_adresse.siret;

			RAISE NOTICE '% TRUNCATE 100K / %', timeofday(), counter;

			TRUNCATE temp_sirene_etalab_adresse_process;

			COMMIT;

			counter := counter + 1;
		end loop;
	END;
$$;

DROP TABLE IF EXISTS temp_sirene_etalab_adresse;
DROP TABLE IF EXISTS temp_sirene_etalab_adresse_process;
DROP TABLE IF EXISTS temp_sirene_etalab_adresse_done;
