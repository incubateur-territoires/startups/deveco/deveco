CREATE OR REPLACE PROCEDURE temp_index_etab_public_etab_cleanup () AS $$ BEGIN

DROP TABLE IF EXISTS temp_index_etab_complet;
DROP TABLE IF EXISTS temp_index_etab_public_etab;
DROP TABLE IF EXISTS temp_index_etab_public_etab_relations;
DROP TABLE IF EXISTS temp_index_etab_public_etab_effectif;
DROP TABLE IF EXISTS temp_index_etab_public_etab_maj;
DROP TABLE IF EXISTS temp_index_etab_public_etab_entre_maj;
DROP TABLE IF EXISTS temp_index_etab_public_etab_sirene_maj;
DROP TABLE IF EXISTS temp_index_etab_public_etab_geolocalisation;
DROP TABLE IF EXISTS temp_index_etab_public_etab_adresse;
DROP TABLE IF EXISTS temp_index_etab_public_etab_qpv_2015;
DROP TABLE IF EXISTS temp_index_etab_public_etab_qpv_2024;
DROP TABLE IF EXISTS temp_index_etab_public_etab_subvention;
DROP TABLE IF EXISTS temp_index_etab_public_etab_predecesseur;
DROP TABLE IF EXISTS temp_index_etab_public_etab_successeur;
DROP TABLE IF EXISTS temp_index_etab_public_etab_pred_succ;

DROP TABLE IF EXISTS temp_index_etab_public_categorie_juridique_type;
DROP TABLE IF EXISTS temp_index_etab_public_entre;
DROP TABLE IF EXISTS temp_index_etab_public_entre_ess;
DROP TABLE IF EXISTS temp_index_etab_public_entre_micro;
DROP TABLE IF EXISTS temp_index_etab_public_entre_egapro;
DROP TABLE IF EXISTS temp_index_etab_public_entre_relations;
DROP TABLE IF EXISTS temp_index_etab_public_entre_maj;
DROP TABLE IF EXISTS temp_index_etab_public_entre_etab_maj;
DROP TABLE IF EXISTS temp_index_etab_public_entre_sirene_maj;
DROP TABLE IF EXISTS temp_index_etab_public_entre_chiffre_d_affaire;
DROP TABLE IF EXISTS temp_index_etab_public_entre_procedure_collective;
DROP TABLE IF EXISTS temp_index_etab_public_entre_participation;

DROP TABLE IF EXISTS temp_index_etab_public_commune;

COMMIT;

END $$ LANGUAGE plpgsql;
