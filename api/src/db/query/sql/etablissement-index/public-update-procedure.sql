CREATE OR REPLACE PROCEDURE temp_index_etab_public_etab_update(mise_a_jour_debut date, mise_a_jour_fin date) AS $$ BEGIN

--
-- ETABLISSEMENT MIS A JOUR PAR SIRENE
--

RAISE NOTICE '% INDEX PUBLIC ETABLISSEMENT MIS A JOUR PAR SIRENE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_etab_sirene_maj AS (
	SELECT
		etablissement.siret AS etablissement_id,
		etablissement.siren AS entreprise_id
	FROM source.insee_sirene_api__etablissement etablissement
	WHERE (mise_a_jour_debut IS NULL OR mise_a_jour_at >= mise_a_jour_debut)
	AND (mise_a_jour_fin IS NULL OR mise_a_jour_at < mise_a_jour_fin)
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_etab_sirene_maj ON temp_index_etab_public_etab_sirene_maj(etablissement_id);
COMMIT;

--
-- ENTREPRISE MISE A JOUR PAR SIRENE
--

RAISE NOTICE '% INDEX PUBLIC ENTREPRISE MISE A JOUR PAR SIRENE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_entre_sirene_maj AS (
	SELECT
		entreprise.siren AS entreprise_id
	FROM source.insee_sirene_api__unite_legale entreprise
	WHERE (mise_a_jour_debut IS NULL OR mise_a_jour_at >= mise_a_jour_debut)
	AND (mise_a_jour_fin IS NULL OR mise_a_jour_at < mise_a_jour_fin)
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_entre_sirene_maj ON temp_index_etab_public_entre_sirene_maj(entreprise_id);
COMMIT;

--
-- ETABLISSEMENT MIS A JOUR PAR ENTREPRISE
--

RAISE NOTICE '% INDEX PUBLIC ETABLISSEMENT MIS A JOUR PAR ENTREPRISE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_etab_entre_maj AS (
	SELECT
		etablissement.siret AS etablissement_id,
		etablissement.siren AS entreprise_id
	FROM source.insee_sirene_api__etablissement etablissement
	JOIN temp_index_etab_public_entre_sirene_maj maj ON maj.entreprise_id = etablissement.siren
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_etab_entre_maj ON temp_index_etab_public_etab_entre_maj(etablissement_id);
COMMIT;

--
-- ENTREPRISE MISE A JOUR PAR ETABLISSEMENT
--

RAISE NOTICE '% INDEX PUBLIC ENTREPRISE MISE A JOUR PAR ETABLISSEMENT', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_entre_etab_maj AS (
	SELECT
		DISTINCT entreprise.siren AS entreprise_id
	FROM source.insee_sirene_api__unite_legale entreprise
	JOIN temp_index_etab_public_etab_sirene_maj maj ON maj.entreprise_id = entreprise.siren
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_entre_etab_maj ON temp_index_etab_public_entre_etab_maj(entreprise_id);
COMMIT;

--
-- ETABLISSEMENT MIS A JOUR
--

RAISE NOTICE '% INDEX PUBLIC ETABLISSEMENT MIS A JOUR', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_etab_maj AS (
	SELECT
		etablissement_id,
		entreprise_id
	FROM temp_index_etab_public_etab_sirene_maj
	UNION
	SELECT
		etablissement_id,
		entreprise_id
	FROM temp_index_etab_public_etab_entre_maj
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_etab_maj ON temp_index_etab_public_etab_maj(etablissement_id);
COMMIT;

--
-- ENTREPRISE MIS A JOUR
--

RAISE NOTICE '% INDEX PUBLIC ENTREPRISE MIS A JOUR', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_entre_maj AS (
	SELECT
		entreprise_id
	FROM temp_index_etab_public_entre_sirene_maj
	UNION
	SELECT
		entreprise_id
	FROM temp_index_etab_public_entre_etab_maj
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_entre_maj ON temp_index_etab_public_entre_maj(entreprise_id);
COMMIT;

--
-- COMMUNES
--

RAISE NOTICE '% INDEX PUBLIC COMMUNE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_commune AS (
	SELECT
		DISTINCT id AS territoire_commune_id,
		commune.metropole_id AS territoire_metropole_id,
		commune.epci_id AS territoire_epci_id,
		commune.petr_id AS territoire_petr_id,
		commune.departement_id AS territoire_departement_id,
		commune.region_id AS territoire_region_id,
		commune.zrr_type_id AS territoire_zrr_type_id,
		commune.territoire_industrie_id AS territoire_territoire_industrie_id,
		pvd.commune_id IS NOT NULL AS territoire_pvd,
		acv.commune_id IS NOT NULL AS territoire_acv,
		va.commune_id IS NOT NULL AS territoire_va,
		CASE
			WHEN afr.commune_id IS NULL THEN NULL
			WHEN afr.integral THEN TRUE
			WHEN NOT afr.integral THEN FALSE
		END AS territoire_afr
	FROM commune
	LEFT JOIN commune_acv acv ON acv.commune_id = commune.id
	LEFT JOIN commune_pvd pvd ON pvd.commune_id = commune.id
	LEFT JOIN commune_va va ON va.commune_id = commune.id
	LEFT JOIN commune_afr afr ON afr.commune_id = commune.id
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_commune ON temp_index_etab_public_commune(territoire_commune_id);
COMMIT;

--
-- ETABLISSEMENT SUBVENTION
--

RAISE NOTICE '% INDEX PUBLIC ETABLISSEMENT SUBVENTION', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_etab_subvention AS (
	WITH subvention_annee AS (
		SELECT
			siret AS etablissement_id,
			date_part('year', convention_date) AS convention_annee,
			SUM(montant) AS montant_total
		FROM source.dgcl__subvention
		JOIN temp_index_etab_public_etab_maj maj ON maj.etablissement_id = siret
		GROUP BY siret, date_part('year', convention_date)
	)
	SELECT
		etablissement_id,
		TRUE AS etablissement_subvention,
		json_agg(
			json_build_object(
				'convention_annee', convention_annee,
				'montant_total', montant_total
			)
		) AS etablissement_subventions
	FROM subvention_annee
	GROUP BY etablissement_id
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_etab_subvention ON temp_index_etab_public_etab_subvention(etablissement_id);
COMMIT;

--
-- ETABLISSEMENT PREDECESSEUR SUCCESSEUR
--

RAISE NOTICE '% INDEX PUBLIC ETABLISSEMENT SUCCESSEUR', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_etab_successeur AS (
	SELECT
		siret_etablissement_predecesseur AS etablissement_id,
		json_agg(siret_etablissement_successeur) AS successeurs
	FROM source.insee_sirene_api__lien_succession
	JOIN temp_index_etab_public_etab_maj maj ON maj.etablissement_id = siret_etablissement_predecesseur
	GROUP BY siret_etablissement_predecesseur
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_etab_successeur ON temp_index_etab_public_etab_successeur(etablissement_id);
COMMIT;

RAISE NOTICE '% INDEX PUBLIC ETABLISSEMENT PREDECESSEUR', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_etab_predecesseur AS (
	SELECT
		siret_etablissement_successeur AS etablissement_id,
		json_agg(siret_etablissement_predecesseur) AS predecesseurs
	FROM source.insee_sirene_api__lien_succession
	JOIN temp_index_etab_public_etab_maj maj ON maj.etablissement_id = siret_etablissement_successeur
	GROUP BY siret_etablissement_successeur
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_etab_predecesseur ON temp_index_etab_public_etab_predecesseur(etablissement_id);
COMMIT;

RAISE NOTICE '% INDEX PUBLIC ETABLISSEMENT PRED_SUC', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_etab_pred_succ AS (
	SELECT
		COALESCE(s.etablissement_id, p.etablissement_id) AS etablissement_id,
		predecesseurs,
		successeurs
	FROM temp_index_etab_public_etab_successeur s
	LEFT JOIN temp_index_etab_public_etab_predecesseur p USING (etablissement_id)
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_etab_pred_suc ON temp_index_etab_public_etab_pred_succ(etablissement_id);
COMMIT;

-- DROP TABLE IF EXISTS temp_index_etab_public_etab_predecesseur;
-- DROP TABLE IF EXISTS temp_index_etab_public_etab_successeur;
COMMIT;

--
-- ETABLISSEMENT GEOLOCALISATION
--

RAISE NOTICE '% INDEX PUBLIC ETABLISSEMENT GEOLOCALISATION', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_etab_geolocalisation AS (
	SELECT
		DISTINCT ON (etablissement_id)
		V.siret AS etablissement_id,
		COALESCE(
			G.geolocalisation,
			V.geolocalisation
		) AS adresse_geolocalisation
	FROM etablissement_geolocalisation_vue V
	JOIN temp_index_etab_public_etab_maj maj ON maj.etablissement_id = V.siret
	LEFT JOIN equipe__etablissement_contribution G ON G.etablissement_id = V.siret
	ORDER BY V.siret, G.geolocalisation
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_geolocalisation ON temp_index_etab_public_etab_geolocalisation(etablissement_id);
COMMIT;

--
-- ETABLISSEMENT ADRESSE
--

RAISE NOTICE '% INDEX PUBLIC ETABLISSEMENT ADRESSE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_etab_adresse AS (
	SELECT
		E.siret AS etablissement_id,
		E.numero_voie_etablissement || COALESCE(E.indice_repetition_etablissement, '') AS adresse_numero,
		COALESCE(E.type_voie_etablissement || ' ', '') || E.libelle_voie_etablissement AS adresse_voie_nom,
		E.code_postal_etablissement AS adresse_code_postal,
		G.adresse_geolocalisation
	FROM source.insee_sirene_api__etablissement E
	JOIN temp_index_etab_public_etab_maj maj ON maj.etablissement_id = E.siret
	LEFT JOIN temp_index_etab_public_etab_geolocalisation G ON G.etablissement_id = E.siret
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_etab_adresse ON temp_index_etab_public_etab_adresse(etablissement_id);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_etab_adresse_geo ON temp_index_etab_public_etab_adresse USING gist(adresse_geolocalisation);
COMMIT;

--
-- ETABLISSEMENT QPV 2015
--

RAISE NOTICE '% INDEX PUBLIC ETABLISSEMENT QPV 2015', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_etab_qpv_2015 AS (
	SELECT
		etablissement_id,
		qpv.id AS adresse_qpv_2015_id
	FROM temp_index_etab_public_etab_adresse adresse
	LEFT JOIN qpv_2015 qpv ON ST_Covers(qpv.geometrie, adresse.adresse_geolocalisation)
	JOIN temp_index_etab_public_etab_maj maj USING (etablissement_id)
	WHERE adresse.adresse_geolocalisation IS NOT NULL
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_etab_qpv_2015 ON temp_index_etab_public_etab_qpv_2015(etablissement_id);
COMMIT;

--
-- ETABLISSEMENT QPV 2024
--

RAISE NOTICE '% INDEX PUBLIC ETABLISSEMENT QPV 2024', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_etab_qpv_2024 AS (
	SELECT
		etablissement_id,
		qpv.id AS adresse_qpv_2024_id
	FROM temp_index_etab_public_etab_adresse adresse
	LEFT JOIN qpv_2024 qpv ON ST_Covers(qpv.geometrie, adresse.adresse_geolocalisation)
	JOIN temp_index_etab_public_etab_maj maj USING (etablissement_id)
	WHERE adresse.adresse_geolocalisation IS NOT NULL
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_etab_qpv_2024 ON temp_index_etab_public_etab_qpv_2024(etablissement_id);
COMMIT;

--
-- ETABLISSEMENT EFFECTIF
--

RAISE NOTICE '% INDEX PUBLIC ETABLISSEMENT EFFECTIF', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_etab_effectif AS (
	WITH temp_effectif AS (
		SELECT
			siret AS etablissement_id,
			effectif AS etablissement_effectif_ema,
			date AS etablissement_effectif_ema_date
		FROM source.rcd__etablissement_effectif_emm_13_mois_annuel
		JOIN temp_index_etab_public_etab_maj maj ON siret = etablissement_id
	)
	SELECT
		N_0.etablissement_id,
		N_0.etablissement_effectif_ema_date,
		N_0.etablissement_effectif_ema,
		CASE WHEN N_M_1.effectif IS NOT NULL AND N_M_1.effectif > 0
			THEN ((N_0.etablissement_effectif_ema - N_M_1.effectif) / N_M_1.effectif * 1000) / 10
			ELSE NULL
		END AS etablissement_effectif_ema_variation
	FROM temp_effectif N_0
	LEFT JOIN source.rcd__etablissement_effectif_emm_13_mois N_M_1
		ON N_M_1.siret = N_0.etablissement_id
		AND N_M_1.date + INTERVAL '1 year' = N_0.etablissement_effectif_ema_date
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_etab_effectif ON temp_index_etab_public_etab_effectif(etablissement_id);
COMMIT;

--
-- ETABLISSEMENT
--

RAISE NOTICE '% INDEX PUBLIC ETABLISSEMENT', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_etab AS (
	SELECT
		etablissement.siret AS etablissement_id,
		etablissement.siren AS entreprise_id,
		etablissement.date_creation_etablissement AS etablissement_creation_date,
		etablissement.etablissement_siege IS TRUE AS etablissement_siege,
		etablissement.caractere_employeur_etablissement = 'O' AS etablissement_employeur,
		etablissement.etat_administratif_etablissement = 'A' AS etablissement_actif,
		etablissement.activite_principale_etablissement AS etablissement_naf_type_id,
		etablissement.nomenclature_activite_principale_etablissement AS etablissement_naf_revision_type_id,
		-- NOMS
		array(SELECT DISTINCT unnest(array_remove(
			array[
				etablissement.enseigne1_etablissement,
				etablissement.enseigne2_etablissement,
				etablissement.enseigne3_etablissement,
				etablissement.denomination_usuelle_etablissement
			],
			NULL
		))) AS etablissement_noms,
		--
		etablissement.date_debut,
		to_char (etablissement.mise_a_jour_at, 'YYYY-MM-DD"T"HH24:MI:SS"Z"') AS mise_a_jour_date,
		to_char (etablissement.date_dernier_traitement_etablissement, 'YYYY-MM-DD"T"HH24:MI:SS"Z"') AS mise_a_jour_sirene_date,
		etablissement.code_commune_etablissement AS commune_id
	FROM source.insee_sirene_api__etablissement etablissement
	JOIN temp_index_etab_public_etab_maj maj ON maj.etablissement_id = etablissement.siret
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_etab ON temp_index_etab_public_etab(etablissement_id);
COMMIT;

--
-- ETABLISSEMENT ET RELATIONS
--

RAISE NOTICE '% INDEX PUBLIC ETABLISSEMENT ET RELATIONS', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_etab_relations AS (
	SELECT
		etablissement.etablissement_id,
		etablissement.entreprise_id,
		etablissement.etablissement_creation_date,
		etablissement.etablissement_siege,
		etablissement.mise_a_jour_date,
		etablissement.mise_a_jour_sirene_date,
		etablissement.etablissement_employeur,
		etablissement.etablissement_actif,
		etablissement.etablissement_naf_type_id,
		etablissement.etablissement_naf_revision_type_id,
		etablissement.etablissement_noms,
		CASE WHEN etablissement_periode_fermeture.fermeture_date IS NOT NULL
			THEN etablissement_periode_fermeture.fermeture_date
			-- Si on a pas trouvé de période avec un changement d'état et
			-- une date de fermeture mais que l'établissement est fermé,
			-- alors on annonce comme date de fermeture la date la plus
			-- ancienne dont on dispose, à savoir sa date de début.
			WHEN etablissement.etablissement_actif IS FALSE
				THEN etablissement.date_debut
				ELSE NULL
		END AS etablissement_fermeture_date,
		-- TODO : transformer en tranche ?
		etablissement_effectif.etablissement_effectif_ema,
		etablissement_effectif.etablissement_effectif_ema_date,
		etablissement_effectif.etablissement_effectif_ema_variation,
		subvention.etablissement_subvention,
		subvention.etablissement_subventions,
		pred_succ.predecesseurs AS etablissement_predecesseurs,
		pred_succ.successeurs AS etablissement_successeurs,
		adresse.adresse_numero,
		adresse.adresse_voie_nom,
		adresse.adresse_code_postal,
		adresse.adresse_geolocalisation,
		qpv_2015.adresse_qpv_2015_id,
		qpv_2024.adresse_qpv_2024_id,
		commune.*
	FROM temp_index_etab_public_etab etablissement
	JOIN temp_index_etab_public_etab_adresse adresse USING(etablissement_id)
	LEFT JOIN etablissement_periode_fermeture_vue etablissement_periode_fermeture USING (etablissement_id)
	LEFT JOIN temp_index_etab_public_etab_effectif etablissement_effectif USING (etablissement_id)
	LEFT JOIN temp_index_etab_public_etab_qpv_2015 qpv_2015 USING(etablissement_id)
	LEFT JOIN temp_index_etab_public_etab_qpv_2024 qpv_2024 USING(etablissement_id)
	LEFT JOIN temp_index_etab_public_etab_subvention subvention USING (etablissement_id)
	LEFT JOIN temp_index_etab_public_etab_pred_succ pred_succ USING (etablissement_id)
	LEFT JOIN temp_index_etab_public_commune commune ON territoire_commune_id = commune_id
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_etab_relations ON temp_index_etab_public_etab_relations(etablissement_id);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_etab_relations_entreprise ON temp_index_etab_public_etab_relations(entreprise_id);
COMMIT;

--
-- ENTREPRISE CHIFFRE D'AFFAIRE
--

RAISE NOTICE '% INDEX PUBLIC ENTREPRISE CHIFFRE D''AFFAIRE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_entre_chiffre_d_affaire AS (
	WITH temp_chiffre_d_affaire AS (
		SELECT DISTINCT ON (siren)
			siren AS entreprise_id,
			date_cloture_exercice AS entreprise_exercice_cloture_date,
			chiffre_d_affaires AS entreprise_chiffre_d_affaires
		FROM source.inpi__ratio_financier
		JOIN temp_index_etab_public_entre_maj ON entreprise_id = siren
		WHERE type_bilan = 'C'
			AND date_cloture_exercice >= (now() - INTERVAL '3 year')
		ORDER BY siren, date_cloture_exercice DESC
	)
	SELECT
		N_0.entreprise_id,
		N_0.entreprise_exercice_cloture_date,
		N_0.entreprise_chiffre_d_affaires,
		CASE WHEN N_M_1.chiffre_d_affaires IS NOT NULL AND N_M_1.chiffre_d_affaires > 0
			THEN ((N_0.entreprise_chiffre_d_affaires - N_M_1.chiffre_d_affaires) / N_M_1.chiffre_d_affaires * 1000) / 10
			ELSE NULL
		END AS entreprise_chiffre_d_affaires_variation
	FROM temp_chiffre_d_affaire N_0
	LEFT JOIN source.inpi__ratio_financier N_M_1 ON siren = entreprise_id
		AND N_M_1.type_bilan = 'C'
		AND N_M_1.date_cloture_exercice + INTERVAL '1 year' = N_0.entreprise_exercice_cloture_date
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_entre_chiffre_d_affaire ON temp_index_etab_public_entre_chiffre_d_affaire(entreprise_id);
COMMIT;

--
-- ENTREPRISE ESS
--

RAISE NOTICE '% INDEX PUBLIC ENTREPRISE ESS', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_entre_ess AS (
	SELECT
		entreprise_id,
		TRUE AS entreprise_economie_sociale_solidaire
	FROM source.ess_france__entreprise_ess
	JOIN temp_index_etab_public_entre_maj USING (entreprise_id)
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_entre_ess ON temp_index_etab_public_entre_ess(entreprise_id);
COMMIT;

--
-- ENTREPRISE MICRO
--

RAISE NOTICE '% INDEX PUBLIC ENTREPRISE MICRO', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_entre_micro AS (
	SELECT
		entreprise_id,
		TRUE AS entreprise_micro
	FROM source.inpi_rne__entreprise_micro
	JOIN temp_index_etab_public_entre_maj USING (entreprise_id)
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_entre_micro ON temp_index_etab_public_entre_micro(entreprise_id);
COMMIT;

--
-- ENTREPRISE PROCEDURE COLLECTIVE
--

RAISE NOTICE '% INDEX PUBLIC ENTREPRISE PROCEDURE COLLECTIVE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_entre_procedure_collective AS (
	SELECT
		siren AS entreprise_id,
		TRUE AS entreprise_procedure_collective,
		json_agg(DISTINCT date) AS entreprise_procedure_collective_dates
	FROM source.bodacc__procedure_collective
	JOIN temp_index_etab_public_entre_maj ON entreprise_id = siren
	GROUP BY siren
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_entre_procedure_collective ON temp_index_etab_public_entre_procedure_collective(entreprise_id);
COMMIT;

--
-- ENTREPRISE EGAPRO
--

RAISE NOTICE '% INDEX PUBLIC ENTREPRISE EGAPRO', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_entre_egapro AS (
	SELECT DISTINCT ON (siren)
		siren AS entreprise_id,
		note_index as entreprise_egapro_indice,
		cadres_pourcentage_femmes as entreprise_egapro_cadres,
		membres_pourcentage_femmes as entreprise_egapro_instances
	FROM source.egapro__entreprise
	JOIN temp_index_etab_public_entre_maj ON entreprise_id = siren
	ORDER BY siren, annee DESC
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_entreprise_egapro ON temp_index_etab_public_entre_egapro(entreprise_id);
COMMIT;

--
-- ENTREPRISE PARTICIPATION
--

RAISE NOTICE '% INDEX PUBLIC ENTREPRISE PARTICIPATION', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_entre_participation AS (
	WITH sirens AS (
		SELECT source AS entreprise_id FROM source.api_entreprise__entreprise_participation
		UNION
		SELECT cible FROM source.api_entreprise__entreprise_participation
	)
	SELECT
		sirens.entreprise_id,
		array_agg(DISTINCT p1.cible) AS entreprise_participation_detient,
		array_agg(DISTINCT p2.source) AS entreprise_participation_filiale_de
	FROM sirens
	JOIN temp_index_etab_public_entre_maj USING (entreprise_id)
	LEFT JOIN source.api_entreprise__entreprise_participation p1 ON p1.source = entreprise_id
	LEFT JOIN source.api_entreprise__entreprise_participation p2 ON p2.cible = p1.source
	GROUP BY sirens.entreprise_id
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_entreprise_participation ON temp_index_etab_public_entre_participation(entreprise_id);
COMMIT;

--
-- CATEGORIE JURIDIQUE
--

RAISE NOTICE '% INDEX PUBLIC CATEGORIE JURIDIQUE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_categorie_juridique_type AS (
	SELECT DISTINCT ON(categorie_juridique_type.id)
		categorie_juridique_type.id,
		categorie_juridique_type.parent_categorie_juridique_type_id
	FROM categorie_juridique_type
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_categorie_juridique_type ON temp_index_etab_public_categorie_juridique_type(id);
COMMIT;

--
-- ENTREPRISE
--

RAISE NOTICE '% INDEX PUBLIC ENTREPRISE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_entre AS (
	SELECT
		entreprise.siren AS entreprise_id,
		entreprise.categorie_juridique_unite_legale,
		entreprise.categorie_entreprise,
		entreprise.prenom1_unite_legale,
		entreprise.prenom2_unite_legale,
		entreprise.prenom3_unite_legale,
		entreprise.prenom4_unite_legale,
		entreprise.prenom_usuel_unite_legale,
		entreprise.pseudonyme_unite_legale,
		entreprise.sigle_unite_legale,
		entreprise.nom_unite_legale,
		entreprise.nom_usage_unite_legale,
		entreprise.denomination_unite_legale,
		entreprise.denomination_usuelle1_unite_legale,
		entreprise.denomination_usuelle2_unite_legale,
		entreprise.denomination_usuelle3_unite_legale
	FROM source.insee_sirene_api__unite_legale entreprise
	JOIN temp_index_etab_public_entre_maj maj ON maj.entreprise_id = entreprise.siren
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_entreprise ON temp_index_etab_public_entre(entreprise_id);
COMMIT;

--
-- ENTREPRISE ET RELATIONS
--

RAISE NOTICE '% INDEX PUBLIC ENTREPRISE ET RELATIONS', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_public_entre_relations AS (
	SELECT
		entreprise.entreprise_id,
		entreprise.categorie_juridique_unite_legale AS entreprise_categorie_juridique_3_type_id,
		categorie_juridique_type.parent_categorie_juridique_type_id AS entreprise_categorie_juridique_2_type_id,
		entreprise.categorie_entreprise AS entreprise_categorie_type_id,
		-- NOMS
		array(SELECT DISTINCT unnest(array_remove(
			array[
				entreprise.prenom1_unite_legale,
				entreprise.prenom2_unite_legale,
				entreprise.prenom3_unite_legale,
				entreprise.prenom4_unite_legale,
				entreprise.prenom_usuel_unite_legale,
				entreprise.pseudonyme_unite_legale,
				entreprise.sigle_unite_legale,
				entreprise.nom_unite_legale,
				entreprise.nom_usage_unite_legale,
				entreprise.denomination_unite_legale,
				entreprise.denomination_usuelle1_unite_legale,
				entreprise.denomination_usuelle2_unite_legale,
				entreprise.denomination_usuelle3_unite_legale
			],
			NULL
		))) AS entreprise_noms,
		--
		ess.entreprise_economie_sociale_solidaire,
		micro.entreprise_micro,
		pcl.entreprise_procedure_collective,
		pcl.entreprise_procedure_collective_dates,
		ega.entreprise_egapro_indice,
		ega.entreprise_egapro_cadres,
		ega.entreprise_egapro_instances,
		ca.entreprise_chiffre_d_affaires,
		ca.entreprise_exercice_cloture_date,
		part.entreprise_participation_filiale_de,
		part.entreprise_participation_detient
	FROM temp_index_etab_public_entre entreprise
	JOIN temp_index_etab_public_categorie_juridique_type categorie_juridique_type ON categorie_juridique_type.id = entreprise.categorie_juridique_unite_legale
	LEFT JOIN temp_index_etab_public_entre_ess ess USING (entreprise_id)
	LEFT JOIN temp_index_etab_public_entre_micro micro USING (entreprise_id)
	LEFT JOIN temp_index_etab_public_entre_procedure_collective pcl USING (entreprise_id)
	LEFT JOIN temp_index_etab_public_entre_egapro ega USING (entreprise_id)
	LEFT JOIN temp_index_etab_public_entre_chiffre_d_affaire ca USING (entreprise_id)
	LEFT JOIN temp_index_etab_public_entre_participation part USING (entreprise_id)
);
CREATE INDEX IF NOT EXISTS temp_index_etab_public_idx_entre_relations ON temp_index_etab_public_entre_relations(entreprise_id);
COMMIT;

--
-- INDEXATION ETABLISSEMENT/ENTREPRISE ET DONNEE PERSO
--

RAISE NOTICE '% INDEX PUBLIC ETABLISSEMENT ENTREPRISE ET DONNEE PERSO', timeofday();

CREATE TABLE IF NOT EXISTS temp_index_etab_complet AS (
	SELECT *
	FROM temp_index_etab_public_etab_relations etablissement
	JOIN temp_index_etab_public_entre_relations entreprise USING (entreprise_id)
	LEFT JOIN temp_index_etab_perso_equipe perso USING (etablissement_id)
);
COMMIT;

CREATE INDEX IF NOT EXISTS temp_index_etab_complet_idx ON temp_index_etab_complet(etablissement_id);
COMMIT;

END $$ LANGUAGE plpgsql;
