CREATE OR REPLACE PROCEDURE temp_index_etab_perso_etab_cleanup () AS $$ BEGIN

DROP TABLE IF EXISTS temp_index_etab_perso_etab_maj;
DROP TABLE IF EXISTS temp_index_etab_perso_etiquette;
DROP TABLE IF EXISTS temp_index_etab_perso_contact;
DROP TABLE IF EXISTS temp_index_etab_perso_contact_nom;
DROP TABLE IF EXISTS temp_index_etab_perso_demande;
DROP TABLE IF EXISTS temp_index_etab_perso_favori;
DROP TABLE IF EXISTS temp_index_etab_perso_ancien_createur;
DROP TABLE IF EXISTS temp_index_etab_perso_consultation;
DROP TABLE IF EXISTS temp_index_etab_perso_contribution;
DROP TABLE IF EXISTS temp_index_etab_perso_suivi_dates;
DROP TABLE IF EXISTS temp_index_etab_perso_etab_geolocalisation;
DROP TABLE IF EXISTS temp_index_etab_perso_equipe;

COMMIT;

END $$ LANGUAGE plpgsql;
