CREATE OR REPLACE PROCEDURE temp_index_etab_perso_etab_create() AS $$ BEGIN

--
-- ETIQUETTE
--

RAISE NOTICE '% INDEX PERSO ETIQUETTE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_perso_etiquette AS (
	SELECT
		etablissement_id,
		equipe_id,
		json_agg(etiquette_id) AS json
	FROM equipe__etablissement__etiquette
	GROUP BY
		etablissement_id,
		equipe_id
);
CREATE INDEX IF NOT EXISTS temp_index_etab_perso_idx_etiquette ON temp_index_etab_perso_etiquette(etablissement_id);
COMMIT;

--
-- DEMANDE
--

RAISE NOTICE '% INDEX PERSO DEMANDE', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_perso_demande AS (
	SELECT
		eed.etablissement_id,
		eed.equipe_id,
		json_agg(demande_type_id) AS json
	FROM equipe__etablissement__demande eed
	JOIN demande ON demande.id = eed.demande_id
	WHERE demande.cloture_motif IS NULL
	GROUP BY
		eed.etablissement_id,
		eed.equipe_id
);
CREATE INDEX IF NOT EXISTS temp_index_etab_perso_idx_demande ON temp_index_etab_perso_demande(etablissement_id);
COMMIT;

--
-- CONTACT NOMS
--

RAISE NOTICE '% INDEX PERSO CONTACT NOMS', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_perso_contact_nom AS (
	SELECT
		eec.etablissement_id,
		eec.equipe_id,
		json_agg(unaccent(prenom || ' ' || nom)) AS json
	FROM equipe__etablissement__contact eec
	JOIN contact ON contact.id = eec.contact_id
	WHERE trim(prenom || ' ' || nom) <> '' AND (prenom IS NOT NULL OR nom IS NOT NULL)
	GROUP BY
		eec.etablissement_id,
		eec.equipe_id
);
CREATE INDEX IF NOT EXISTS temp_index_etab_perso_idx_contact_nom ON temp_index_etab_perso_contact_nom(etablissement_id);
COMMIT;

--
-- CONTACT
--

RAISE NOTICE '% INDEX PERSO CONTACT', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_perso_contact AS (
	SELECT
		eec.etablissement_id,
		eec.equipe_id,
		true AS json
	FROM equipe__etablissement__contact eec
	JOIN contact ON contact.id = eec.contact_id
	GROUP BY
		eec.etablissement_id,
		eec.equipe_id
);
CREATE INDEX IF NOT EXISTS temp_index_etab_perso_idx_contact ON temp_index_etab_perso_contact(etablissement_id);
COMMIT;

--
-- FAVORI
--

RAISE NOTICE '% INDEX PERSO FAVORI', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_perso_favori AS (
	SELECT
		etablissement_id,
		equipe_id,
		json_agg(deveco_id) AS json
	FROM deveco__etablissement_favori f
	JOIN deveco ON deveco.id = f.deveco_id
	GROUP BY
		etablissement_id,
		equipe_id
);
CREATE INDEX IF NOT EXISTS temp_index_etab_perso_idx_favori ON temp_index_etab_perso_favori(etablissement_id);
COMMIT;

--
-- ANCIEN CREATEUR
--

RAISE NOTICE '% INDEX PERSO ANCIEN CREATEUR', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_perso_ancien_createur AS (
	SELECT
		etablissement_id,
		equipe_id,
		true AS json
	FROM etablissement_creation_transformation
	GROUP BY
		etablissement_id,
		equipe_id
);
CREATE INDEX IF NOT EXISTS temp_index_etab_perso_idx_ancien_createur ON temp_index_etab_perso_ancien_createur(etablissement_id);
COMMIT;

--
-- CONSULTATION
--

RAISE NOTICE '% INDEX PERSO CONSULTATION', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_perso_consultation AS (
	SELECT
		DISTINCT ON (etablissement_id, equipe_id) etablissement_id,
		equipe_id,
		json_strip_nulls(json_build_object(
				'consultation_at', created_at,
				'consultation_str', created_at::text
		)) AS json
	FROM action_equipe_etablissement
	WHERE action_type_id = 'affichage'
	ORDER BY
		etablissement_id,
		equipe_id,
		created_at DESC
);
CREATE INDEX IF NOT EXISTS temp_index_etab_perso_idx_consultation ON temp_index_etab_perso_consultation(etablissement_id);
COMMIT;

--
-- ETABLISSEMENT CONTRIBUTION
--

RAISE NOTICE '% INDEX PERSO ETABLISSEMENT CONTRIBUTION', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_perso_contribution AS (
	SELECT
		etablissement_id,
		equipe_id,
		json_strip_nulls(json_build_object(
			'cloture_date', cloture_date,
			'enseigne', unaccent(enseigne),
			'adresse_geolocalisation', geolocalisation::json
		)) AS json
	FROM equipe__etablissement_contribution
	WHERE cloture_date IS NOT NULL OR enseigne IS NOT NULL OR geolocalisation IS NOT NULL
	GROUP BY
		etablissement_id,
		equipe_id
);
CREATE INDEX IF NOT EXISTS temp_index_etab_perso_idx_contribution ON temp_index_etab_perso_contribution(etablissement_id);
COMMIT;

--
-- ETABLISSEMENT SUIVI DATES
--

RAISE NOTICE '% INDEX PERSO ETABLISSEMENT SUIVI DATES', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_perso_suivi_dates AS (
	SELECT
		etablissement_id,
		equipe_id,
		json_agg(DISTINCT date::date) AS json
		FROM (
			SELECT eee.etablissement_id,
				eee.equipe_id,
				rappel.date
			FROM equipe__etablissement__rappel eee
			JOIN rappel on rappel.id = eee.rappel_id
			UNION
			SELECT eee.etablissement_id,
				eee.equipe_id,
				echange.date
			FROM equipe__etablissement__echange eee
			JOIN echange on echange.id = eee.echange_id
			UNION
			SELECT eee.etablissement_id,
				eee.equipe_id,
				demande.date
			FROM equipe__etablissement__demande eee
			JOIN demande on demande.id = eee.demande_id
		) t
	GROUP BY
		etablissement_id,
		equipe_id
);
CREATE INDEX IF NOT EXISTS temp_index_etab_perso_idx_suivi_dates ON temp_index_etab_perso_suivi_dates(etablissement_id);
COMMIT;

--
-- ETABLISSEMENT GEOLOCALISATION
--

RAISE NOTICE '% INDEX PERSO ETABLISSEMENT GEOLOCALISATION', timeofday();

CREATE UNLOGGED TABLE IF NOT EXISTS temp_index_etab_perso_etab_geolocalisation AS (
	SELECT
		DISTINCT ON (etablissement_id)
		etablissement_id,
		geolocalisation AS adresse_geolocalisation
	FROM equipe__etablissement_contribution
	WHERE geolocalisation IS NOT NULL
	ORDER BY etablissement_id, geolocalisation
);
CREATE INDEX IF NOT EXISTS temp_index_etab_perso_idx_geolocalisation ON temp_index_etab_perso_etab_geolocalisation(etablissement_id);
COMMIT;

--
-- EQUIPE
--

RAISE NOTICE '% INDEX PERSO EQUIPE', timeofday();

CREATE TABLE IF NOT EXISTS temp_index_etab_perso_equipe AS (
	WITH equipe_perso_etablissement AS (
	  SELECT DISTINCT etablissement_id, equipe_id
		FROM (
			SELECT etablissement_id, equipe_id FROM temp_index_etab_perso_etiquette
			UNION
			SELECT etablissement_id, equipe_id FROM temp_index_etab_perso_contact_nom
			UNION
			SELECT etablissement_id, equipe_id FROM temp_index_etab_perso_contact
			UNION
			SELECT etablissement_id, equipe_id FROM temp_index_etab_perso_demande
			UNION
			SELECT etablissement_id, equipe_id FROM temp_index_etab_perso_favori
			UNION
			SELECT etablissement_id, equipe_id FROM temp_index_etab_perso_ancien_createur
			UNION
			SELECT etablissement_id, equipe_id FROM temp_index_etab_perso_consultation
			UNION
			SELECT etablissement_id, equipe_id FROM temp_index_etab_perso_contribution
			UNION
			SELECT etablissement_id, equipe_id FROM temp_index_etab_perso_suivi_dates
		) t
  ),
  equipe_perso_par_etablissement AS (
		SELECT
			etablissement_id,
			jsonb_strip_nulls(
				jsonb_build_object(
					'equipe_id', equipe_id,
					'etiquettes', etiquettes.json,
					'contacts_noms', contacts_noms.json,
					'contacts', contacts.json,
					'demandes', demandes.json,
					'favoris', favoris.json,
					'ancien_createur', ancien_createur.json,
					'contribution', contribution.json,
					'suivi_dates', suivi_dates.json
				) || COALESCE(consultation.json::jsonb, '{}'::jsonb)
			) AS equipe
		FROM equipe_perso_etablissement
		LEFT JOIN LATERAL (
			SELECT json
			FROM temp_index_etab_perso_etiquette
			WHERE temp_index_etab_perso_etiquette.etablissement_id = equipe_perso_etablissement.etablissement_id
				AND temp_index_etab_perso_etiquette.equipe_id = equipe_perso_etablissement.equipe_id
		) AS etiquettes ON TRUE
		LEFT JOIN LATERAL (
			SELECT json
			FROM temp_index_etab_perso_contact_nom
			WHERE temp_index_etab_perso_contact_nom.etablissement_id = equipe_perso_etablissement.etablissement_id
				AND temp_index_etab_perso_contact_nom.equipe_id = equipe_perso_etablissement.equipe_id
		) AS contacts_noms ON TRUE
		LEFT JOIN LATERAL (
			SELECT json
			FROM temp_index_etab_perso_contact
			WHERE temp_index_etab_perso_contact.etablissement_id = equipe_perso_etablissement.etablissement_id
				AND temp_index_etab_perso_contact.equipe_id = equipe_perso_etablissement.equipe_id
		) AS contacts ON TRUE
		LEFT JOIN LATERAL (
			SELECT json
			FROM temp_index_etab_perso_demande
			WHERE temp_index_etab_perso_demande.etablissement_id = equipe_perso_etablissement.etablissement_id
				AND temp_index_etab_perso_demande.equipe_id = equipe_perso_etablissement.equipe_id
		) AS demandes ON TRUE
		LEFT JOIN LATERAL (
			SELECT json
			FROM temp_index_etab_perso_favori
			WHERE temp_index_etab_perso_favori.etablissement_id = equipe_perso_etablissement.etablissement_id
				AND temp_index_etab_perso_favori.equipe_id = equipe_perso_etablissement.equipe_id
		) AS favoris ON TRUE
		LEFT JOIN LATERAL (
			SELECT json
			FROM temp_index_etab_perso_ancien_createur
			WHERE temp_index_etab_perso_ancien_createur.etablissement_id = equipe_perso_etablissement.etablissement_id
				AND temp_index_etab_perso_ancien_createur.equipe_id = equipe_perso_etablissement.equipe_id
		) AS ancien_createur ON TRUE
		LEFT JOIN LATERAL (
			SELECT json
			FROM temp_index_etab_perso_consultation
			WHERE temp_index_etab_perso_consultation.etablissement_id = equipe_perso_etablissement.etablissement_id
				AND temp_index_etab_perso_consultation.equipe_id = equipe_perso_etablissement.equipe_id
		) AS consultation ON TRUE
		LEFT JOIN LATERAL (
			SELECT json
			FROM temp_index_etab_perso_contribution
			WHERE temp_index_etab_perso_contribution.etablissement_id = equipe_perso_etablissement.etablissement_id
				AND temp_index_etab_perso_contribution.equipe_id = equipe_perso_etablissement.equipe_id
		) AS contribution ON TRUE
		LEFT JOIN LATERAL (
			SELECT json
			FROM temp_index_etab_perso_suivi_dates
			WHERE temp_index_etab_perso_suivi_dates.etablissement_id = equipe_perso_etablissement.etablissement_id
				AND temp_index_etab_perso_suivi_dates.equipe_id = equipe_perso_etablissement.equipe_id
		) AS suivi_dates ON TRUE
	)
	SELECT
		etablissement_id,
		json_agg(equipe) AS equipes,
		COALESCE(G.adresse_geolocalisation, V.geolocalisation) AS adresse_geolocalisation_perso
	FROM equipe_perso_par_etablissement
	LEFT JOIN temp_index_etab_perso_etab_geolocalisation G USING(etablissement_id)
	LEFT JOIN etablissement_geolocalisation_vue V ON V.siret = etablissement_id
	GROUP BY etablissement_id, G.adresse_geolocalisation, V.geolocalisation
);
CREATE INDEX IF NOT EXISTS temp_index_etab_perso_idx_equipe ON temp_index_etab_perso_equipe(etablissement_id);

END $$ LANGUAGE plpgsql;
