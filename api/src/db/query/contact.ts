import { and, or, eq, sql, count, desc, isNull, inArray, notInArray } from 'drizzle-orm';

import db from '..';
import { contact, contactAdresse } from '../schema/contact';
import {
	EtablissementCreation,
	New,
	Contact,
	Equipe,
	PaginationParamsContact,
	Local,
	Etablissement,
} from '../types';
import { etabCreaCreateur, etabCrea } from '../schema/etablissement-creation';
import { eqEtabContact } from '../schema/equipe-etablissement';
import { eqLocContact, eqLocContribution } from '../schema/equipe-local';
import * as dbQueryEtablissement from '../query/etablissement';
import { dbQuoteEscape } from '../../lib/utils';

export type ContactParams = {
	equipeId: Equipe['id'];
	etablissementId?: Etablissement['siret'] | null;
	etabCreaId?: EtablissementCreation['id'] | null;
	localId?: Local['id'] | null;
	recherche?: string;
} & PaginationParamsContact;

export function contactAdd({ contact: contactValues }: { contact: New<typeof contact> }) {
	return db.insert(contact).values(contactValues).returning();
}

export function contactRemove({ contactId }: { contactId: Contact['id'] }) {
	return db
		.update(contact)
		.set({
			suppressionDate: new Date(),
		})
		.where(eq(contact.id, contactId));
}

export function contactAdresseUpsert({
	contactAdresse: contactAdresseValues,
}: {
	contactAdresse: New<typeof contactAdresse>;
}) {
	return db
		.insert(contactAdresse)
		.values(contactAdresseValues)
		.onConflictDoUpdate({
			target: contactAdresse.contactId,
			set: { ...contactAdresseValues },
		});
}

export function contactGet({ contactId }: { contactId: Contact['id'] }) {
	return db.query.contact.findFirst({
		where: eq(contact.id, contactId),
	});
}

export function contactWithRelationsIdGet({ contactId }: { contactId: Contact['id'] }) {
	return db.query.contact.findFirst({
		where: eq(contact.id, contactId),
		columns: {
			id: true,
		},
		with: {
			etabCs: {
				columns: {
					etablissementId: true,
				},
			},
			creaCs: {
				columns: {
					etabCreaId: true,
				},
			},
			localCs: {
				columns: {
					localId: true,
				},
			},
		},
	});
}

export function contactWithRelationGetManyById({
	equipeId,
	contactIds,
}: {
	equipeId: Equipe['id'];
	contactIds: Contact['id'][];
}) {
	return db.query.contact.findMany({
		columns: {
			id: true,
			nom: true,
			prenom: true,
			email: true,
			telephone: true,
			telephone2: true,
			naissanceDate: true,
			demarchageAccepte: true,
		},
		where: and(inArray(contact.id, contactIds), isNull(contact.suppressionDate)),
		with: contactRelationsBuild({ equipeId }),
	});
}

function contactMotWhereBuild({ mot }: { mot: string }) {
	const sanitizedMot = dbQuoteEscape(mot.trim());

	return or(
		sql.join(
			[sql`unaccent(${contact.nom})`, sql`ilike`, sql.raw(`unaccent('%${sanitizedMot}%')`)],
			sql.raw(' ')
		),
		sql.join(
			[sql`unaccent(${contact.prenom})`, sql`ilike`, sql.raw(`unaccent('%${sanitizedMot}%')`)],
			sql.raw(' ')
		),
		sql.join(
			[sql`unaccent(${contact.email})`, sql`ilike`, sql.raw(`unaccent('%${sanitizedMot}%')`)],
			sql.raw(' ')
		),
		and(
			sql.raw(`length(regexp_replace('${sanitizedMot}', '[^0-9]', '', 'g')) > 0`),
			sql.join(
				[
					sql`regexp_replace(${contact.telephone}, '[^0-9]', '', 'g')`,
					sql`like`,
					sql.raw(`('%' || regexp_replace('${sanitizedMot}', '[^0-9]', '', 'g') || '%')`),
				],
				sql.raw(' ')
			)
		)
	);
}

function contactRechercheWhereBuild({ recherche }: { recherche: string }) {
	return and(...recherche.split(/ /).map((mot) => contactMotWhereBuild({ mot })));
}

function contactWithEtablissementCreationActifGetMany({ equipeId }: { equipeId: Equipe['id'] }) {
	return db
		.select({ contactId: contact.id })
		.from(contact)
		.leftJoin(
			etabCreaCreateur,
			and(eq(etabCreaCreateur.contactId, contact.id), eq(etabCreaCreateur.equipeId, equipeId))
		)
		.leftJoin(etabCrea, eq(etabCrea.id, etabCreaCreateur.etabCreaId))
		.where(or(eq(etabCrea.actif, true), isNull(etabCrea.id)));
}

export function contactCountByEquipe({
	equipeId,
	recherche,
}: {
	equipeId: Equipe['id'];
	recherche?: string | null;
}) {
	const contactWithEtablissementCreationActif = contactWithEtablissementCreationActifGetMany({
		equipeId,
	});

	const wheres = [
		eq(contact.equipeId, equipeId),
		inArray(contact.id, contactWithEtablissementCreationActif),
		isNull(contact.suppressionDate),
	];

	if (recherche) {
		const rechercheWheres = contactRechercheWhereBuild({ recherche });
		if (rechercheWheres) {
			wheres.push(rechercheWheres);
		}
	}

	return db
		.select({
			count: count(),
		})
		.from(contact)
		.where(and(...wheres));
}

export const contactRelationsBuild = ({ equipeId }: { equipeId: Equipe['id'] }) =>
	({
		etabCs: {
			columns: {
				fonction: true,
			},
			with: {
				etab: {
					with: dbQueryEtablissement.etablissementRelationsBuild({ equipeId, __kind: 'colonne' }),
				},
			},
		},
		creaCs: {
			columns: {},
			where: inArray(
				etabCreaCreateur.etabCreaId,
				db
					.select({
						etabCreaId: etabCrea.id,
					})
					.from(etabCrea)
					.where(and(and(eq(etabCrea.equipeId, equipeId), eq(etabCrea.actif, true))))
			),
			with: {
				eCreation: {
					columns: {
						id: true,
						enseigne: true,
					},
					with: {
						creas: {
							columns: {},
							with: {
								ctct: {
									columns: {
										nom: true,
										prenom: true,
									},
								},
							},
						},
					},
				},
			},
		},
		localCs: {
			columns: {
				fonction: true,
			},
			with: {
				local: {
					columns: {
						id: true,
					},
					with: {
						adresse: {
							columns: {
								numero: true,
								voieNom: true,
								codePostal: true,
							},
							with: {
								commune: {
									columns: {
										id: true,
										nom: true,
									},
								},
							},
						},
						contributions: {
							where: eq(eqLocContribution.equipeId, contact.equipeId),
							columns: {
								nom: true,
							},
							limit: 1,
						},
					},
				},
			},
		},
	}) as const;

export const elementsParPageParDefaut = 20;

export function contactGetManyByEquipeQueryBuild({
	equipeId,
	recherche,
	etablissementId,
	etabCreaId,
	localId,
	tri,
	direction,
}: ContactParams) {
	const contactWithEtablissementCreationActif = contactWithEtablissementCreationActifGetMany({
		equipeId,
	});

	const wheres = [
		eq(contact.equipeId, equipeId),
		inArray(contact.id, contactWithEtablissementCreationActif),
		isNull(contact.suppressionDate),
	];

	if (recherche) {
		const rechercheWheres = contactRechercheWhereBuild({ recherche });
		if (rechercheWheres) {
			wheres.push(rechercheWheres);
		}
	}

	if (etablissementId) {
		wheres.push(
			notInArray(
				contact.id,
				db
					.select({ contactId: eqEtabContact.contactId })
					.from(eqEtabContact)
					.where(eq(eqEtabContact.etablissementId, etablissementId))
			)
		);
	}

	if (etabCreaId) {
		wheres.push(
			notInArray(
				contact.id,
				db
					.select({ contactId: eqEtabContact.contactId })
					.from(etabCreaCreateur)
					.where(eq(etabCreaCreateur.etabCreaId, etabCreaId))
			)
		);
	}

	if (localId) {
		wheres.push(
			notInArray(
				contact.id,
				db
					.select({ contactId: eqLocContact.contactId })
					.from(eqLocContact)
					.where(eq(eqLocContact.localId, localId))
			)
		);
	}

	let orderBy =
		tri === 'email'
			? [contact.email]
			: tri === 'prenom'
				? [sql`lower(${contact.prenom})`, sql`lower(${contact.nom})`]
				: [sql`lower(${contact.nom})`, sql`lower(${contact.prenom})`];

	if (direction === 'desc') {
		orderBy = orderBy.map((o) => desc(o));
	}

	return {
		wheres,
		orderBy,
	};
}

export function contactWithRelationGetManyByEquipe(params: ContactParams) {
	const { wheres, orderBy } = contactGetManyByEquipeQueryBuild(params);

	return db.query.contact.findMany({
		columns: {
			id: true,
			nom: true,
			prenom: true,
			email: true,
			telephone: true,
			telephone2: true,
			naissanceDate: true,
			demarchageAccepte: true,
		},
		where: and(...wheres),
		with: contactRelationsBuild({ equipeId: params.equipeId }),
		limit: params.elementsParPage ?? elementsParPageParDefaut,
		offset: params.elementsParPage ? ((params.page || 1) - 1) * params.elementsParPage : 0,
		orderBy,
	});
}

export function contactIdGetManyByEquipe(params: ContactParams) {
	const { wheres, orderBy } = contactGetManyByEquipeQueryBuild(params);

	// si `elementsParPage === 0`, on ne donne pas de paramètre `limit` à la requête
	const limit: { limit: number } | Record<never, never> = params.elementsParPage
		? { limit: params.elementsParPage ?? elementsParPageParDefaut }
		: {};

	return db.query.contact.findMany({
		columns: {
			id: true,
		},
		where: and(...wheres),
		...limit,
		offset: params.elementsParPage ? ((params.page || 1) - 1) * params.elementsParPage : 0,
		orderBy,
	});
}

export async function* contactGetCursorByEquipe(params: ContactParams) {
	const query = contactIdGetManyByEquipe(params);

	const cursor = db.cursor<Pick<Contact, 'id'>>(query, { size: 100 });

	for await (const ids of cursor) {
		const contactIds = ids.map(({ id }) => id);
		if (!contactIds.length) {
			continue;
		}

		yield contactWithRelationGetManyById({
			equipeId: params.equipeId,
			contactIds,
		});
	}
}

export function contactUpdate({
	contactId,
	contact: contactValues,
}: {
	contactId: Contact['id'];
	contact: Partial<Contact>;
}) {
	return db.update(contact).set(contactValues).where(eq(contact.id, contactId)).returning();
}

export function anonymisationContactUpdateMany({ contactIds }: { contactIds: Contact['id'][] }) {
	return db
		.update(contact)
		.set({
			email: 'anonyme@deveco.incubateur.anct.gouv.fr',
			prenom: 'Anne',
			nom: 'Onyme',
			telephone: null,
			telephone2: null,
			naissanceDate: null,
		})
		.where(inArray(contact.id, contactIds));
}
