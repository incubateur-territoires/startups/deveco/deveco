import { and, eq, count, desc, gt, inArray, isNull, notExists } from 'drizzle-orm';

import db from '..';
import { Demande, Equipe, New } from '../../db/types';
import { demande } from '../../db/schema/suivi';
import { eqEtabDemande } from '../../db/schema/equipe-etablissement';
import { eqLocContribution } from '../../db/schema/equipe-local';
import { etabCrea, etabCreaDemande } from '../../db/schema/etablissement-creation';
import * as dbQueryEtablissement from '../../db/query/etablissement';
import * as dbQueryEquipeEtablissementSuivi from '../../db/query/equipe-etablissement-suivi';

export function demandeAdd({ demande: demandeValues }: { demande: New<typeof demande> }) {
	return db.insert(demande).values(demandeValues).returning();
}

export function demandeWithRelationsGet({ demandeId }: { demandeId: number }) {
	return db.query.demande.findFirst({
		where: and(eq(demande.id, demandeId), isNull(demande.suppressionDate)),
		with: {
			echanges: true,
		},
	});
}

export function demandeCountFromDate({ debut, equipeId }: { debut: Date; equipeId: Equipe['id'] }) {
	return db
		.select({
			total: count(demande.id).as('total'),
		})
		.from(demande)
		.where(
			and(
				eq(demande.equipeId, equipeId),
				isNull(demande.suppressionDate),
				gt(demande.creationDate, debut)
			)
		);
}

function demandeEtablissementCreationInactifSubQueryBuild({
	demandeIdColumn,
}: {
	demandeIdColumn:
		| (typeof demande._.columns)['id']
		| (typeof eqEtabDemande._.columns)['demandeId']
		| (typeof etabCreaDemande._.columns)['demandeId'];
}) {
	return db
		.select({
			demandeId: etabCreaDemande.demandeId,
		})
		.from(etabCreaDemande)
		.innerJoin(etabCrea, eq(etabCreaDemande.etabCreaId, etabCrea.id))
		.where(and(eq(etabCreaDemande.demandeId, demandeIdColumn), eq(etabCrea.actif, false)));
}

export function demandeWithRelationsGetMany({
	equipeId,
	limit,
}: {
	equipeId: Equipe['id'];
	limit: number;
}) {
	const demandeEtablissementCreationInactifSub = demandeEtablissementCreationInactifSubQueryBuild({
		demandeIdColumn: demande.id,
	});

	return db.query.demande.findMany({
		where: and(
			eq(demande.equipeId, equipeId),
			isNull(demande.suppressionDate),
			notExists(demandeEtablissementCreationInactifSub)
		),
		with: {
			echanges: {
				where: dbQueryEquipeEtablissementSuivi.eqEtabEchangeSupprimeSub,
			},
			eqEtabs: {
				with: {
					etab: {
						with: dbQueryEtablissement.etablissementRelationsBuild({ equipeId, __kind: 'colonne' }),
					},
				},
			},
			creations: {
				with: {
					etabCrea: {
						with: {
							creas: {
								with: {
									ctct: true,
								},
							},
						},
					},
				},
			},
			eqLocaux: {
				with: {
					local: {
						with: {
							contributions: {
								where: eq(eqLocContribution.equipeId, demande.equipeId),
								limit: 1,
							},
						},
					},
				},
			},
		},
		orderBy: [desc(demande.date), desc(demande.id)],
		limit: limit ?? 100,
	});
}

export function demandeGetManyById({
	equipeId,
	demandeIds,
}: {
	equipeId: Equipe['id'];
	demandeIds: Demande['id'][];
}) {
	if (!demandeIds.length) {
		return [];
	}

	return db.query.demande.findMany({
		where: and(
			eq(demande.equipeId, equipeId),
			isNull(demande.suppressionDate),
			inArray(demande.id, demandeIds)
		),
		with: { type: true },
	});
}

export function demandeTypeIdGetMany(equipeId: Equipe['id']) {
	return db
		.selectDistinct({
			typeId: demande.typeId,
		})
		.from(demande)
		.where(
			and(
				eq(demande.equipeId, equipeId),
				isNull(demande.clotureMotif),
				isNull(demande.suppressionDate)
			)
		);
}

export function demandeUpdate({
	demandeId,
	partialDemande,
}: {
	demandeId: number;
	partialDemande: Partial<Demande>;
}) {
	return db.update(demande).set(partialDemande).where(eq(demande.id, demandeId)).returning();
}

export function demandeRemove({ demandeId }: { demandeId: Demande['id'] }) {
	return db
		.update(demande)
		.set({
			suppressionDate: new Date(),
		})
		.where(eq(demande.id, demandeId));
}
