import { sql, and, eq, count, desc, gte, isNull, notExists } from 'drizzle-orm';

import db from '..';
import { Deveco, Equipe, New, Rappel } from '../types';
import { rappel } from '../schema/suivi';
import { eqEtabRappel } from '../schema/equipe-etablissement';
import { eqLocContribution } from '../schema/equipe-local';
import { etabCrea, etabCreaRappel } from '../schema/etablissement-creation';
import * as dbQueryEtablissement from '../query/etablissement';

export function rappelAdd({ rappel: rappelValues }: { rappel: New<typeof rappel> }) {
	return db.insert(rappel).values(rappelValues).returning();
}

export function rappelGet({
	equipeId,
	rappelId,
}: {
	equipeId: Equipe['id'];
	rappelId: Rappel['id'];
}) {
	return db.query.rappel.findFirst({
		where: and(
			eq(rappel.equipeId, equipeId),
			eq(rappel.id, rappelId),
			isNull(rappel.suppressionDate)
		),
	});
}

export function rappelWithRelationsGet({ rappelId }: { rappelId: number }) {
	return db.query.rappel.findFirst({
		where: and(eq(rappel.id, rappelId), isNull(rappel.suppressionDate)),
		with: {
			eqEtabs: true,
			creations: true,
			eqLocaux: true,
		},
	});
}

function rappelEtablissementCreationInactifSubQueryBuild({
	rappelIdColumn,
}: {
	rappelIdColumn:
		| (typeof rappel._.columns)['id']
		| (typeof eqEtabRappel._.columns)['rappelId']
		| (typeof etabCreaRappel._.columns)['rappelId'];
}) {
	return db
		.select({
			rappelId: etabCreaRappel.rappelId,
		})
		.from(etabCreaRappel)
		.innerJoin(etabCrea, eq(etabCreaRappel.etabCreaId, etabCrea.id))
		.where(and(eq(etabCreaRappel.rappelId, rappelIdColumn), eq(etabCrea.actif, false)));
}

export function rappelOuvertWithRelationsGetManyAtDate({ debut }: { debut: Date }) {
	const rappelEtablissementCreationInactifSub = rappelEtablissementCreationInactifSubQueryBuild({
		rappelIdColumn: rappel.id,
	});

	return db.query.rappel.findMany({
		where: and(
			and(
				eq(rappel.date, debut),
				isNull(rappel.clotureDate),
				isNull(rappel.suppressionDate),
				notExists(rappelEtablissementCreationInactifSub)
			)
		),
		orderBy: rappel.date,
		with: {
			affecteCompte: true,
			eqEtabs: {
				with: {
					etab: {
						with: dbQueryEtablissement.etablissementRelationsBuild({
							__kind: 'table',
							table: rappel,
						}),
					},
				},
			},
			creations: {
				with: {
					etabCrea: {
						with: {
							creas: {
								with: {
									ctct: true,
								},
							},
						},
					},
				},
			},
			eqLocaux: {
				with: {
					local: {
						with: {
							contributions: {
								where: eq(eqLocContribution.equipeId, rappel.equipeId),
								limit: 1,
							},
						},
					},
				},
			},
		},
	});
}

export function rappelOuvertCountFromDate({
	debut,
	equipeId,
}: {
	debut: Date;
	equipeId: Equipe['id'];
}) {
	const rappelEtablissementCreationInactifSub = rappelEtablissementCreationInactifSubQueryBuild({
		rappelIdColumn: rappel.id,
	});

	return db
		.select({
			total: count(rappel.id).as('total'),
		})
		.from(rappel)
		.where(
			and(
				gte(rappel.creationDate, debut),
				isNull(rappel.clotureDate),
				isNull(rappel.suppressionDate),
				notExists(rappelEtablissementCreationInactifSub),
				eq(rappel.equipeId, equipeId)
			)
		);
}

export function rappelOuvertCountByDevecoFromDate({
	equipeId,
	devecoId,
	debut,
}: {
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
	debut: Date;
}) {
	const rappelEtablissementCreationInactifSub = rappelEtablissementCreationInactifSubQueryBuild({
		rappelIdColumn: rappel.id,
	});

	return db
		.select({
			aVenir: sql<number>`SUM(CASE WHEN date >= NOW() THEN 1 ELSE 0 END)`.as('aVenir'),
			enRetard: sql<number>`SUM(CASE WHEN date < NOW() THEN 1 ELSE 0 END)`.as('enRetard'),
		})
		.from(rappel)
		.where(
			and(
				gte(rappel.creationDate, debut),
				isNull(rappel.clotureDate),
				isNull(rappel.suppressionDate),
				notExists(rappelEtablissementCreationInactifSub),
				eq(rappel.equipeId, equipeId),
				eq(rappel.affecteCompteId, devecoId)
			)
		);
}

export function rappelWithRelationsGetMany({ equipeId }: { equipeId: Equipe['id'] }) {
	const rappelEtablissementCreationInactifSub = rappelEtablissementCreationInactifSubQueryBuild({
		rappelIdColumn: rappel.id,
	});

	return db.query.rappel.findMany({
		where: and(
			eq(rappel.equipeId, equipeId),
			isNull(rappel.suppressionDate),
			notExists(rappelEtablissementCreationInactifSub)
		),
		with: {
			affecteCompte: true,
			eqEtabs: {
				with: {
					etab: {
						with: dbQueryEtablissement.etablissementRelationsBuild({
							table: rappel,
							__kind: 'table',
						}),
					},
				},
			},
			creations: {
				with: {
					etabCrea: {
						with: {
							creas: {
								with: {
									ctct: true,
								},
							},
						},
					},
				},
			},
			eqLocaux: {
				with: {
					local: {
						with: {
							contributions: {
								where: eq(eqLocContribution.equipeId, rappel.equipeId),
								limit: 1,
							},
						},
					},
				},
			},
		},
		orderBy: [desc(rappel.date), desc(rappel.id)],
	});
}

export function rappelUpdate({
	rappelId,
	partialRappel,
}: {
	rappelId: number;
	partialRappel: Partial<Rappel>;
}) {
	return db.update(rappel).set(partialRappel).where(eq(rappel.id, rappelId)).returning();
}

export function rappelRemove({ rappelId }: { rappelId: Rappel['id'] }) {
	return db
		.update(rappel)
		.set({
			suppressionDate: new Date(),
		})
		.where(eq(rappel.id, rappelId));
}
