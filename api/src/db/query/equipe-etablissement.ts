import {
	eq,
	and,
	inArray,
	notInArray,
	isNull,
	isNotNull,
	not,
	sql,
	gte,
	lt,
	count,
} from 'drizzle-orm';

import db from '../../db';
import {
	DemandeTypeId,
	EtatEtablissement,
	GeolocParam,
	PaginationParams,
	PaginationParamsEtablissement,
	Equipe,
	Deveco,
	Contact,
	Etiquette,
	EquipeEtablissementEtiquette,
	EquipeEtablissementContact,
	EquipeEtablissementContribution,
	RessourceEtablissement,
	SubventionEtablissementRecue,
	Etablissement,
	EntrepriseType,
} from '../types';
import { equipe, etiquette, zonage } from '../../db/schema/equipe';
import {
	devecoEtablissementFavori,
	eqEtabEtiquette,
	eqEtabRessource,
	eqEtabContact,
	eqEtabContribution,
	eqEtabEchange,
	eqEtabDemande,
	eqEtabRappel,
	etablissementGeolocalisationVue,
} from '../../db/schema/equipe-etablissement';
import { eqLocOccupant } from '../../db/schema/equipe-local';
import { etabCreaTransformation } from '../../db/schema/etablissement-creation';
import { etablissement } from '../../db/schema/etablissement';
import { evenement, actionEquipeEtablissementContact } from '../../db/schema/evenement';
import { contact } from '../../db/schema/contact';
import * as dbQueryEtablissement from '../../db/query/etablissement';
import * as dbQueryContact from '../../db/query/contact';
import * as dbQueryEquipe from '../../db/query/equipe';
import * as dbQueryEquipeLocal from '../../db/query/equipe-local';
import * as dbQueryEquipeEtablissementSuivi from '../../db/query/equipe-etablissement-suivi';
import * as ElasticQueryEquipeEtablissement from '../../elastic/query/equipe-etablissement';

export * from '../../db/query/equipe-etablissement-suivi';

export interface PublicParams {
	recherche: string | null;
	etat: EtatEtablissement[];
	geoloc: GeolocParam[];
	categoriesJuridiques: string[];
	categoriesJuridiquesSans: string[];
	categorieJuridiqueCourante: boolean | null;
	codesNaf: string[];
	codesNafSans: string[];
	effectifInconnu: boolean | null;
	effectifMin: number | null;
	effectifMax: number | null;
	effectifVarTypes: string[];
	effectifVarMin: number | null;
	effectifVarMax: number | null;
	ess: boolean | null;
	micro: boolean | null;
	siege: boolean | null;
	cas: string[];
	caVarTypes: string[];
	caVarMin: number | null;
	caVarMax: number | null;
	procedure: boolean | null;
	procApres: Date | null;
	procAvant: Date | null;
	filiales: string[];
	detentions: string[];
	subventions: SubventionEtablissementRecue[];
	subAnneeMin: number | null;
	subAnneeMax: number | null;
	subMontantMin: number | null;
	subMontantMax: number | null;
	creeApres: Date | null;
	creeAvant: Date | null;
	fermeApres: Date | null;
	fermeAvant: Date | null;
	entrepriseTypes: EntrepriseType[];
	// adresse
	cp: string | null;
	numero: string | null;
	rue: string | null;
	// geoloc
	longitude: number | null;
	latitude: number | null;
	rayon: number | null;
	z: number | null;
	nwse: number[];
	// territoire
	qpvs: string[];
	zrrs: string[];
	acv: boolean | null;
	pvd: boolean | null;
	va: boolean | null;
	afrs: string[];
	territoireIndustries: string[];
	communes: string[];
	epcis: string[];
	departements: string[];
	regions: string[];
	egaproIndiceMin: number | null;
	egaproIndiceMax: number | null;
	egaproCadresMin: number | null;
	egaproCadresMax: number | null;
	egaproInstancesMin: number | null;
	egaproInstancesMax: number | null;
}

export type PersoParams = {
	equipeId: Equipe['id'];
	demandeTypeIds: DemandeTypeId[];
	activites: string[];
	activitesToutes: boolean;
	localisations: string[];
	localisationsToutes: boolean;
	motsCles: string[];
	motsClesTous: boolean;
	accompagnes: string[];
	accompagnesApres: Date | null;
	accompagnesAvant: Date | null;
	avecContact: boolean | null;
	ancienCreateur: boolean | null;
} & ({ favori: boolean; devecoId: Deveco['id'] } | { favori: null; devecoId: null });

export type EtablissementsParams = PublicParams &
	dbQueryEquipe.GeoParams &
	PersoParams &
	PaginationParamsEtablissement;

export type EntreprisesParams = Pick<PublicParams, 'recherche'> & PaginationParams;

export const eqEtabContactSupprimeSub = notInArray(
	eqEtabContact.contactId,
	db
		.select({
			contactId: eqEtabContact.contactId,
		})
		.from(eqEtabContact)
		.innerJoin(contact, eq(contact.id, eqEtabContact.contactId))
		.where(isNotNull(contact.suppressionDate))
);

export function etablissementWithEqEtabWithRelationsForGeojsonGetMany({
	equipeId,
	etablissementIds,
}: {
	equipeId: Equipe['id'];
	etablissementIds: Etablissement['siret'][];
}) {
	return db.query.etablissement.findMany({
		where: and(inArray(etablissement.siret, etablissementIds)),
		with: {
			...dbQueryEtablissement.etablissementForStreamRelations(),
			...eqEtabRelationsForGeojsonBuild({ equipeId }),
		},
	});
}

export function etablissementWithEqEtabForExcelGetMany({
	equipeId,
	etablissementIds,
}: {
	equipeId: Equipe['id'];
	etablissementIds: Etablissement['siret'][];
}) {
	return db.query.etablissement.findMany({
		where: and(inArray(etablissement.siret, etablissementIds)),
		with: {
			...dbQueryEtablissement.etablissementForStreamRelations(),
			...eqEtabRelationsForExcelBuild({ equipeId }),
		},
	});
}

export function eqEtabOuvertFermeWithRelationsGetMany({
	equipeId,
	etablissementIds,
}: {
	equipeId: Equipe['id'];
	etablissementIds: Etablissement['siret'][];
}) {
	return db.query.etablissement.findMany({
		where: inArray(etablissement.siret, etablissementIds),
		with: {
			...dbQueryEtablissement.etablissementRelationsBuild({ equipeId, __kind: 'colonne' }),
			effectifMoyenAnnuel: true,
		},
	});
}

export function eqEtabRelationsBuild({ equipeId }: { equipeId: Equipe['id'] }) {
	return {
		// différent de la vue complète
		contacts: {
			where: and(eq(eqEtabContact.equipeId, equipeId), eqEtabContactSupprimeSub),
			limit: 1,
		},
		etiquettes: {
			where: eq(eqEtabEtiquette.equipeId, equipeId),
			limit: 1,
		},
		echanges: {
			where: and(
				eq(eqEtabEchange.equipeId, equipeId),
				dbQueryEquipeEtablissementSuivi.eqEtabEchangeSupprimeSub
			),
			limit: 1,
		},
		demandes: {
			where: and(
				eq(eqEtabDemande.equipeId, equipeId),
				dbQueryEquipeEtablissementSuivi.eqEtabDemandeSupprimeSub
			),
			limit: 1,
		},
		rappels: {
			where: and(
				eq(eqEtabRappel.equipeId, equipeId),
				dbQueryEquipeEtablissementSuivi.eqEtabRappelSupprimeSub
			),
			limit: 1,
		},
	} as const;
}

export function eqEtabAllRelationsBuild({
	equipeId,
	devecoId,
}: {
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
}) {
	return {
		contributions: {
			where: eq(eqEtabContribution.equipeId, equipeId),
			// en plus par rapport à la vue simple
			with: {
				modifCompte: {
					columns: {
						nom: true,
						prenom: true,
						email: true,
					},
				},
			},
			limit: 1,
		},
		contributionsGeolocs: {
			where: isNotNull(eqEtabContribution.geolocalisation),
			columns: {
				geolocalisation: true,
			},
			with: {
				eq: {
					columns: {
						nom: true,
					},
				},
			},
			limit: 1,
		},
		// différent de la vue simple
		etiquettes: {
			where: eq(eqEtabContact.equipeId, equipeId),
			with: {
				etiquette: true,
			},
		},
		contacts: {
			where: and(eq(eqEtabContact.equipeId, equipeId), eqEtabContactSupprimeSub),
			with: {
				ctct: {
					with: dbQueryContact.contactRelationsBuild({ equipeId }),
				},
			},
		},
		echanges: {
			where: and(
				eq(eqEtabEchange.equipeId, equipeId),
				dbQueryEquipeEtablissementSuivi.eqEtabEchangeSupprimeSub
			),
			with: {
				echange: {
					with: {
						modifCompte: {
							columns: {
								nom: true,
								prenom: true,
								email: true,
							},
						},
						demandes: {
							where: dbQueryEquipeEtablissementSuivi.eqEtabDemandeSupprimeSub,
							with: {
								demande: true,
							},
						},
						deveco: {
							with: {
								compte: {
									columns: {
										nom: true,
										prenom: true,
										email: true,
									},
								},
							},
						},
					},
				},
			},
		},
		// en plus par rapport à la vue simple
		demandes: {
			where: and(
				eq(eqEtabDemande.equipeId, equipeId),
				dbQueryEquipeEtablissementSuivi.eqEtabDemandeSupprimeSub
			),
			with: {
				demande: {
					with: {
						echanges: {
							where: dbQueryEquipeEtablissementSuivi.eqEtabEchangeSupprimeSub,
							with: { echange: true },
						},
					},
				},
			},
		},
		rappels: {
			where: and(
				eq(eqEtabRappel.equipeId, equipeId),
				dbQueryEquipeEtablissementSuivi.eqEtabRappelSupprimeSub
			),
			with: {
				rappel: {
					with: {
						affecteCompte: {
							columns: {
								// TODO supprimer id et actif dans le front
								id: true,
								nom: true,
								prenom: true,
								email: true,
								actif: true,
							},
						},
					},
				},
			},
		},
		ressources: {
			where: eq(eqEtabRessource.equipeId, equipeId),
		},
		transfos: {
			where: eq(etabCreaTransformation.equipeId, equipeId),
			with: {
				crea: {
					with: {
						modifCompte: {
							columns: {
								nom: true,
								prenom: true,
								email: true,
							},
						},
						creas: {
							with: {
								ctct: {
									with: {
										adresse: true,
									},
								},
							},
						},
					},
				},
			},
		},
		occups: {
			where: eq(eqLocOccupant.equipeId, equipeId),
			with: {
				local: {
					with: {
						...dbQueryEquipeLocal.localRelationsBuild(),
						...dbQueryEquipeLocal.eqLocRelationsBuild({ equipeId, devecoId }),
					},
				},
			},
		},
	} as const;
}

export function eqEtabRelationsForGeojsonBuild({ equipeId }: { equipeId: Equipe['id'] }) {
	return {
		contributions: {
			where: eq(eqEtabContribution.equipeId, equipeId),
			limit: 1,
		},
		etiquettes: {
			where: eq(eqEtabContact.equipeId, equipeId),
			columns: {},
			with: {
				etiquette: true,
			},
		},
		occups: {
			columns: {
				localId: true,
			},
		},
	} as const;
}

export function eqEtabRelationsForExcelBuild({ equipeId }: { equipeId: Equipe['id'] }) {
	return {
		contributions: {
			where: eq(eqEtabContribution.equipeId, equipeId),
			limit: 1,
		},
		etiquettes: {
			where: eq(eqEtabContact.equipeId, equipeId),
			with: {
				etiquette: true,
			},
		},
		occups: {
			columns: {
				localId: true,
			},
		},
		// en plus par rapport au GeoJSON
		contacts: {
			where: and(eq(eqEtabContact.equipeId, equipeId), eqEtabContactSupprimeSub),
			with: {
				ctct: true,
			},
		},
		echanges: {
			where: and(
				eq(eqEtabEchange.equipeId, equipeId),
				dbQueryEquipeEtablissementSuivi.eqEtabEchangeSupprimeSub
			),
			with: {
				echange: {
					with: {
						modifCompte: {
							columns: {
								nom: true,
								prenom: true,
								email: true,
							},
						},
						deveco: {
							with: {
								compte: {
									columns: {
										nom: true,
										prenom: true,
										email: true,
									},
								},
							},
						},
						demandes: {
							where: dbQueryEquipeEtablissementSuivi.eqEtabDemandeSupprimeSub,
							with: {
								demande: true,
							},
						},
					},
				},
			},
		},
		demandes: {
			where: and(
				eq(eqEtabDemande.equipeId, equipeId),
				dbQueryEquipeEtablissementSuivi.eqEtabDemandeSupprimeSub
			),
			with: {
				demande: {
					with: {
						echanges: {
							with: {
								echange: true,
							},
						},
					},
				},
			},
		},
	} as const;
}

export function etablissementWithEqEtabWithRelationsGetMany({
	equipeId,
	devecoId,
	etablissementIds,
}: {
	equipeId: Equipe['id'];
	devecoId: Deveco['id'];
	etablissementIds: Etablissement['siret'][];
}) {
	return db.query.etablissement.findMany({
		where: and(inArray(etablissement.siret, etablissementIds)),
		with: {
			...dbQueryEtablissement.etablissementRelationsBuild({ equipeId, __kind: 'colonne' }),
			...eqEtabRelationsBuild({ equipeId }),
			favoris: {
				where: eq(devecoEtablissementFavori.devecoId, devecoId),
				limit: 1,
			},
		},
	});
}

export function etablissementWithEqEtabWithContributionGet({
	equipeId,
	etablissementId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
}) {
	return db.query.etablissement.findFirst({
		where: eq(etablissement.siret, etablissementId),
		with: dbQueryEtablissement.etablissementRelationsBuild({ equipeId, __kind: 'colonne' }),
	});
}

export function etablissementWithEqEtabGeolocalisationContributionGet({
	etablissementId,
}: {
	etablissementId: Etablissement['siret'];
}) {
	return db.query.eqEtabContribution.findFirst({
		columns: {
			equipeId: true,
		},
		where: and(
			eq(eqEtabContribution.etablissementId, etablissementId),
			isNotNull(eqEtabContribution.geolocalisation)
		),
	});
}

async function eqEtabFromManyContactNomsUpdate({
	equipeId,
	etablissementIds,
}: {
	equipeId: Equipe['id'];
	etablissementIds: Etablissement['siret'][];
}) {
	const contactSql = sql<string>`${contact.prenom} || ' ' || ${contact.nom}`;

	const eqEtabContacts = await db
		.select({
			etablissementId: eqEtabContact.etablissementId,
			nom: contactSql,
		})
		.from(eqEtabContact)
		.innerJoin(contact, eq(contact.id, eqEtabContact.contactId))
		.where(
			and(
				eq(eqEtabContact.equipeId, equipeId),
				inArray(eqEtabContact.etablissementId, etablissementIds),
				isNull(contact.suppressionDate)
			)
		);

	const eqEtabContactsMap = eqEtabContacts.reduce(
		(eqEtabContactsMap: Map<string, Set<string>>, { etablissementId, nom }) => {
			let contactNoms = eqEtabContactsMap.get(etablissementId);
			if (!contactNoms) {
				contactNoms = new Set<string>();
				eqEtabContactsMap.set(etablissementId, contactNoms);
			}

			contactNoms.add(nom);

			return eqEtabContactsMap;
		},
		new Map()
	);

	for (const [etablissementId, noms] of eqEtabContactsMap.entries()) {
		// met à jour ElasticSearch
		// TODO : faire en une seule fois, avec un batch
		await ElasticQueryEquipeEtablissement.eqEtabAvecContactUpdate({
			equipeId,
			etablissementId,
			avecContact: noms.size > 0,
		});

		await ElasticQueryEquipeEtablissement.eqEtabContactsNomsUpdate({
			equipeId,
			etablissementId,
			contactsNoms: [...noms],
		});
	}
}

export async function eqEtabContactAdd({
	equipeId,
	etablissementId,
	contactId,
	fonction,
	contactSourceTypeId,
}: {
	equipeId: EquipeEtablissementContact['equipeId'];
	etablissementId: EquipeEtablissementContact['etablissementId'];
	contactId: EquipeEtablissementContact['contactId'];
	fonction: EquipeEtablissementContact['fonction'];
	contactSourceTypeId: EquipeEtablissementContact['contactSourceTypeId'];
}) {
	const c = await db
		.insert(eqEtabContact)
		.values({
			equipeId,
			etablissementId,
			contactId,
			fonction,
			contactSourceTypeId,
		})
		// si le contact a déjà été ajouté à l'établissement, on ne fait rien
		.onConflictDoNothing()
		.returning();

	await eqEtabFromManyContactNomsUpdate({
		equipeId,
		etablissementIds: [etablissementId],
	});

	return c;
}

export function eqEtabContactGet({
	equipeId,
	etablissementId,
	contactId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	contactId: Contact['id'];
}) {
	return db
		.select()
		.from(eqEtabContact)
		.where(
			and(
				eq(eqEtabContact.contactId, contactId),
				eq(eqEtabContact.equipeId, equipeId),
				eq(eqEtabContact.etablissementId, etablissementId)
			)
		);
}

export async function eqEtabContactUpdate({
	equipeId,
	etablissementId,
	contactId,
	fonction,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	contactId: Contact['id'];
	fonction?: EquipeEtablissementContact['fonction'];
}) {
	const c = await db
		.update(eqEtabContact)
		.set({ fonction })
		.where(
			and(
				eq(eqEtabContact.contactId, contactId),
				eq(eqEtabContact.etablissementId, etablissementId),
				eq(eqEtabContact.equipeId, equipeId)
			)
		)
		.returning();

	await eqEtabFromManyContactNomsUpdate({
		equipeId,
		etablissementIds: [etablissementId],
	});

	return c;
}

export async function eqEtabContactRemove({
	equipeId,
	etablissementId,
	contactId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	contactId: Contact['id'];
}) {
	const c = await db
		.delete(eqEtabContact)
		.where(
			and(
				eq(eqEtabContact.etablissementId, etablissementId),
				eq(eqEtabContact.contactId, contactId)
			)
		)
		.returning();

	await eqEtabFromManyContactNomsUpdate({
		equipeId,
		etablissementIds: [etablissementId],
	});

	return c;
}

export async function eqEtabFromManyContactRemove({
	equipeId,
	etablissementIds,
	contactId,
}: {
	equipeId: Equipe['id'];
	etablissementIds: Etablissement['siret'][];
	contactId: Contact['id'];
}) {
	const c = await db
		.delete(eqEtabContact)
		.where(
			and(
				inArray(eqEtabContact.etablissementId, etablissementIds),
				eq(eqEtabContact.contactId, contactId)
			)
		)
		.returning();

	await eqEtabFromManyContactNomsUpdate({
		equipeId,
		etablissementIds,
	});

	return c;
}

export async function eqEtabEtiquetteUpdate({
	equipeId,
	etablissementId,
	etiquetteIds,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	etiquetteIds: Etiquette['id'][];
}) {
	const eqEtabEtiquetteFormat = (etiquetteId: Etiquette['id']) => ({
		equipeId,
		etablissementId,
		etiquetteId,
		auto: false,
	});

	const currentValues = await db.query.eqEtabEtiquette.findMany({
		where: and(
			eq(eqEtabEtiquette.equipeId, equipeId),
			eq(eqEtabEtiquette.etablissementId, etablissementId)
		),
	});
	const currentEtiquetteIds = currentValues.map((eee) => eee.etiquetteId);

	const toAdd = etiquetteIds
		.filter((etiquetteId) => !currentEtiquetteIds.includes(etiquetteId))
		.map(eqEtabEtiquetteFormat);

	const toRemove = currentEtiquetteIds.filter((etiquetteId) => !etiquetteIds.includes(etiquetteId));

	const added = toAdd.length
		? await db
				.insert(eqEtabEtiquette)
				.values(toAdd)
				.onConflictDoNothing({
					target: [
						eqEtabEtiquette.equipeId,
						eqEtabEtiquette.etablissementId,
						eqEtabEtiquette.etiquetteId,
					],
				})
				.returning({
					etiquetteId: eqEtabEtiquette.etiquetteId,
				})
		: [];

	const removed = toRemove.length
		? await db
				.delete(eqEtabEtiquette)
				.where(
					and(
						eq(eqEtabEtiquette.equipeId, equipeId),
						eq(eqEtabEtiquette.etablissementId, etablissementId),
						inArray(eqEtabEtiquette.etiquetteId, toRemove)
					)
				)
				.returning({
					etiquetteId: eqEtabEtiquette.etiquetteId,
				})
		: [];

	return {
		ajoutees: added.map(({ etiquetteId }) => etiquetteId),
		supprimees: removed.map(({ etiquetteId }) => etiquetteId),
	};
}

export function eqEtabEtiquetteAddMany({
	eqEtabEtiquettes,
}: {
	eqEtabEtiquettes: EquipeEtablissementEtiquette[];
}) {
	if (!eqEtabEtiquettes.length) {
		return [];
	}

	return db
		.insert(eqEtabEtiquette)
		.values(eqEtabEtiquettes)
		.onConflictDoNothing({
			target: [
				eqEtabEtiquette.equipeId,
				eqEtabEtiquette.etablissementId,
				eqEtabEtiquette.etiquetteId,
			],
		})
		.returning({
			equipeId: eqEtabEtiquette.equipeId,
			etiquetteId: eqEtabEtiquette.etiquetteId,
			etablissementId: eqEtabEtiquette.etablissementId,
			auto: eqEtabEtiquette.auto,
		});
}

export function eqEtabEtiquetteIdGetMany({
	equipeId,
	etablissementId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
}) {
	return db
		.select({
			etiquetteId: eqEtabEtiquette.etiquetteId,
		})
		.from(eqEtabEtiquette)
		.where(
			and(
				eq(eqEtabEtiquette.equipeId, equipeId),
				eq(eqEtabEtiquette.etablissementId, etablissementId)
			)
		);
}

export function zonageGetManyByEquipeEtablissementId({
	equipeId,
	etablissementId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
}) {
	return db
		.select({
			id: zonage.id,
			nom: etiquette.nom,
			enCours: zonage.enCours,
			description: zonage.description,
		})
		.from(zonage)
		.leftJoin(etiquette, eq(etiquette.id, zonage.etiquetteId))
		.leftJoin(
			etablissementGeolocalisationVue,
			eq(etablissementGeolocalisationVue.siret, etablissementId)
		)
		.leftJoin(
			eqEtabContribution,
			and(
				eq(eqEtabContribution.equipeId, equipeId),
				eq(eqEtabContribution.etablissementId, etablissementGeolocalisationVue.siret)
			)
		)
		.where(
			and(
				eq(zonage.equipeId, equipeId),
				sql`
ST_Covers(
	${zonage.geometrie},
	COALESCE(
		${eqEtabContribution.geolocalisation},
		${etablissementGeolocalisationVue.geolocalisation}
	)
)`
			)
		);
}

export function eqEtabContributionUpsert({
	equipeId,
	etablissementId,
	contribution,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	contribution: Partial<EquipeEtablissementContribution>;
}) {
	return db
		.insert(eqEtabContribution)
		.values({ etablissementId, equipeId, ...contribution })
		.onConflictDoUpdate({
			target: [eqEtabContribution.etablissementId, eqEtabContribution.equipeId],
			set: { ...contribution },
		})
		.returning();
}

export function eqEtabFavoriAdd({
	devecoId,
	etablissementId,
}: {
	devecoId: Deveco['id'];
	etablissementId: Etablissement['siret'];
}) {
	return db
		.insert(devecoEtablissementFavori)
		.values({ devecoId, etablissementId })
		.onConflictDoNothing({
			target: [devecoEtablissementFavori.devecoId, devecoEtablissementFavori.etablissementId],
		});
}

export function eqEtabFavoriRemove({
	devecoId,
	etablissementId,
}: {
	devecoId: Deveco['id'];
	etablissementId: Etablissement['siret'];
}) {
	return db
		.delete(devecoEtablissementFavori)
		.where(
			and(
				eq(devecoEtablissementFavori.devecoId, devecoId),
				eq(devecoEtablissementFavori.etablissementId, etablissementId)
			)
		);
}

export function etabIdGetManyByContactCreationDate({
	equipeId,
	debut,
	fin,
}: {
	equipeId: Equipe['id'];
	debut: Date;
	fin: Date;
}) {
	return db
		.selectDistinct({
			id: actionEquipeEtablissementContact.etablissementId,
		})
		.from(actionEquipeEtablissementContact)
		.innerJoin(evenement, eq(evenement.id, actionEquipeEtablissementContact.evenementId))
		.where(
			and(
				eq(actionEquipeEtablissementContact.equipeId, equipeId),
				gte(evenement.createdAt, debut),
				lt(evenement.createdAt, fin)
			)
		);
}

export function ressourceAdd({
	equipeId,
	etablissementId,
	partialRessource,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
	partialRessource: Partial<RessourceEtablissement>;
}) {
	return db.insert(eqEtabRessource).values({ etablissementId, equipeId, ...partialRessource });
}

export function ressourceUpdate({
	ressourceId,
	partialRessource,
}: {
	ressourceId: RessourceEtablissement['id'];
	partialRessource: Partial<RessourceEtablissement>;
}) {
	return db
		.update(eqEtabRessource)
		.set({ ...partialRessource })
		.where(eq(eqEtabRessource.id, ressourceId));
}

export function ressourceRemove({ ressourceId }: { ressourceId: RessourceEtablissement['id'] }) {
	return db.delete(eqEtabRessource).where(eq(eqEtabRessource.id, ressourceId));
}

export function suiviParEquipeNomGetManyByEtablissementId({
	equipeId,
	etablissementId,
}: {
	equipeId: Equipe['id'];
	etablissementId: Etablissement['siret'];
}) {
	const query = db
		.select({
			equipeId: eqEtabContact.equipeId,
			count: count().as('count'),
			type: sql`${'contacts'}`.as('type'),
		})
		.from(eqEtabContact)
		.where(
			and(
				eq(eqEtabContact.etablissementId, etablissementId),
				not(eq(eqEtabContact.equipeId, equipeId))
			)
		)
		.groupBy(eqEtabContact.equipeId)
		.union(
			db
				.select({
					equipeId: eqEtabRappel.equipeId,
					count: count().as('count'),
					type: sql`${'rappels'}`.as('type'),
				})
				.from(eqEtabRappel)
				.where(
					and(
						eq(eqEtabRappel.etablissementId, etablissementId),
						not(eq(eqEtabRappel.equipeId, equipeId))
					)
				)
				.groupBy(eqEtabRappel.equipeId)
		)
		.union(
			db
				.select({
					equipeId: eqEtabDemande.equipeId,
					count: count().as('count'),
					type: sql`${'demandes'}`.as('type'),
				})
				.from(eqEtabDemande)
				.where(
					and(
						eq(eqEtabDemande.etablissementId, etablissementId),
						not(eq(eqEtabDemande.equipeId, equipeId))
					)
				)
				.groupBy(eqEtabDemande.equipeId)
		)
		.union(
			db
				.select({
					equipeId: eqEtabEchange.equipeId,
					count: count().as('count'),
					type: sql`${'echanges'}`.as('type'),
				})
				.from(eqEtabEchange)
				.where(
					and(
						eq(eqEtabEchange.etablissementId, etablissementId),
						not(eq(eqEtabEchange.equipeId, equipeId))
					)
				)
				.groupBy(eqEtabEchange.equipeId)
		)
		.as('equipes');

	return db
		.selectDistinctOn([equipe.nom], {
			nom: equipe.nom,
			values: sql<
				{ type: 'contacts' | 'rappels' | 'demandes' | 'echanges'; count: number }[]
			>`json_agg(json_build_object('type', ${query.type}, 'count', ${query.count}))`,
		})
		.from(query)
		.innerJoin(equipe, eq(equipe.id, query.equipeId))
		.groupBy(equipe.nom);
}
