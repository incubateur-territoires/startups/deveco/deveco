import { and, count, desc, eq, inArray, sql } from 'drizzle-orm';
import { hashSync } from 'bcrypt';
import { v4 as uuidv4 } from 'uuid';

import db from '../../db';
import {
	compte,
	motDePasse,
	nouveautes,
	tache,
	refreshToken,
	equipeDemande,
} from '../schema/admin';
import { Compte, New, RefreshToken, Tache, EquipeDemande } from '../../db/types';

export function compteAdd({ compte: compteValues }: { compte: New<typeof compte> }) {
	return db.insert(compte).values(compteValues).returning();
}

export function comptesCount() {
	return db.select({ count: count() }).from(compte);
}

export function compteGet({ compteId }: { compteId: number }) {
	return db.query.compte.findFirst({
		where: eq(compte.id, compteId),
	});
}

export function refreshTokenAdd({
	compteId,
	userAgent,
}: {
	compteId: Compte['id'];
	userAgent: string;
}) {
	const token = uuidv4();

	return db
		.insert(refreshToken)
		.values({ compteId, token, creatorUserAgent: userAgent })
		.returning();
}

export function refreshTokenGetByRefresh({ refresh }: { refresh: string }) {
	return db.query.refreshToken.findFirst({ where: eq(refreshToken.token, refresh) });
}

export function refreshTokenInvalidate({
	id,
	jwtExpired,
	jwtRefreshed,
	userAgent,
}: {
	id: RefreshToken['id'];
	jwtExpired: RefreshToken['jwtExpired'];
	jwtRefreshed: RefreshToken['jwtRefreshed'];
	userAgent: string;
}) {
	return db
		.update(refreshToken)
		.set({
			lastAccessedAt: new Date(),
			lastAccessedUserAgent: userAgent,
			jwtExpired,
			jwtRefreshed,
		})
		.where(and(eq(refreshToken.id, id)));
}

export function compteInactifNonAnonymeGetMany() {
	return db.query.compte.findMany({
		where: and(eq(compte.actif, false), eq(compte.anonymise, false)),
	});
}

export function equipeDemandeGetMany() {
	return db.query.equipeDemande.findMany({
		with: {
			admin: true,
			compteDemande: true,
			autorisations: {
				with: {
					ayantDroit: true,
					commune: true,
					epci: true,
					petr: true,
					territoireIndustrie: true,
					departement: true,
					region: true,
				},
			},
		},
	});
}

export function equipeDemandeUpdate({
	equipeDemandeId,
	partialEquipeDemande,
}: {
	equipeDemandeId: EquipeDemande['id'];
	partialEquipeDemande: Partial<EquipeDemande>;
}) {
	return db
		.update(equipeDemande)
		.set(partialEquipeDemande)
		.where(eq(equipeDemande.id, equipeDemandeId))
		.returning();
}

export function compteWithRelationsGet({ compteId }: { compteId: number }) {
	return db.query.compte.findFirst({
		where: eq(compte.id, compteId),
		with: {
			deveco: {
				with: {
					equipe: true,
					notifications: true,
				},
			},
			motDePasse: true,
		},
	});
}

export function compteGetByCle({ cle }: { cle: string }) {
	return db.query.compte.findFirst({
		where: eq(compte.cle, cle),
		with: {
			deveco: {
				with: {
					equipe: true,
					notifications: true,
				},
			},
			motDePasse: true,
		},
	});
}

export function compteGetByIdentifiant({ identifiant }: { identifiant: string }) {
	return db.query.compte.findFirst({
		where: eq(compte.identifiant, identifiant),
		with: {
			deveco: {
				with: {
					equipe: true,
					notifications: true,
				},
			},
			motDePasse: true,
		},
	});
}

export function compteGetByNomAndType({ nom, typeId }: { nom: string; typeId: Compte['typeId'] }) {
	return db.query.compte.findFirst({
		where: and(eq(compte.nom, nom), eq(compte.typeId, typeId)),
	});
}

export function compteUpdate({
	compteId,
	partialCompte,
}: {
	compteId: number;
	partialCompte: Partial<Compte>;
}) {
	return db.update(compte).set(partialCompte).where(eq(compte.id, compteId)).returning();
}

export function motDePasseGet(compteId: number) {
	return db.query.motDePasse.findFirst({
		where: eq(motDePasse.compteId, compteId),
	});
}

export function motDePasseUpsert({
	compteId,
	motDePasse: newMotDePasse,
}: {
	compteId: number;
	motDePasse: string;
}) {
	return db
		.insert(motDePasse)
		.values({
			compteId,
			hash: hashSync(newMotDePasse, 10),
			updatedAt: new Date(),
		})
		.onConflictDoUpdate({
			target: [motDePasse.compteId],
			set: { hash: sql`excluded.hash` },
		});
}

export function nouveauteGetLatest() {
	return db.query.nouveautes.findFirst({
		orderBy: desc(nouveautes.createdAt),
	});
}

export function nouveauteAdd(amount: number) {
	return db.insert(nouveautes).values({ amount });
}

export function tacheAdd({ tache: tacheValues }: { tache: New<typeof tache> }) {
	return db.insert(tache).values(tacheValues).returning();
}

export function tacheUpdate({
	tacheId,
	partialTache,
}: {
	tacheId: Tache['id'];
	partialTache: Partial<Tache>;
}) {
	return db.update(tache).set(partialTache).where(eq(tache.id, tacheId)).returning();
}

export function anonymisationCompteUpdateMany({ compteIds }: { compteIds: Compte['id'][] }) {
	return db
		.update(compte)
		.set({
			prenom: 'Anne',
			nom: 'Onyme',
			email: 'anonyme@deveco.incubateur.anct.gouv.fr',
			identifiant: sql`'anonyme' || gen_random_uuid() || '@deveco.incubateur.anct.gouv.fr'`,
		})
		.where(inArray(compte.id, compteIds));
}

export function metabaseRefresh() {
	return db.execute(sql`
REFRESH MATERIALIZED VIEW metabase_equipe_daily_events;
REFRESH MATERIALIZED VIEW metabase_equipe_daily_counts;
REFRESH MATERIALIZED VIEW earliest_signup;
REFRESH MATERIALIZED VIEW metabase_deveco_info;
REFRESH MATERIALIZED VIEW metabase_equipe_user_type_daily_snapshot;
REFRESH MATERIALIZED VIEW metabase_recherches_par_equipe_type;
`);
}
