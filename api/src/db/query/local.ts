import { inArray, eq, sql, and } from 'drizzle-orm';

import db from '..';
import { localAdresse, localCategorieType, localNatureType } from '../schema/local';
import { Local, LocalCategorieType, LocalNatureType } from '../types';
import { qpv2024, qpv2015 } from '../schema/contact';

export function localColumns() {
	return {
		id: true,
		invariant: true,
		niveau: true,
		parcelle: true,
		section: true,
		surfaceVente: true,
		surfaceReserve: true,
		surfaceExterieureNonCouverte: true,
		surfaceStationnementNonCouvert: true,
		surfaceStationnementCouvert: true,
		actif: true,
	} as const;
}

export function localCategorieTypeGetManyById({
	categorieIds,
}: {
	categorieIds: LocalCategorieType['id'][];
}) {
	return db.query.localCategorieType.findMany({
		where: inArray(localCategorieType.id, categorieIds),
	});
}

export function localCategorieTypeGetMany() {
	return db.query.localCategorieType.findMany();
}

export function localNatureTypeGetManyById({ natureIds }: { natureIds: LocalNatureType['id'][] }) {
	return db.query.localNatureType.findMany({
		where: inArray(localNatureType.id, natureIds),
	});
}

export function localNatureTypeGetMany() {
	return db.query.localNatureType.findMany();
}

export function localForStreamRelations() {
	return localColumns();
}

export function localGeolocalisationGet({ localId }: { localId: Local['id'] }) {
	return db
		.select({
			geolocalisation: localAdresse.geolocalisation,
		})
		.from(localAdresse)
		.where(eq(localAdresse.localId, localId))
		.limit(1);
}

export function localOnQpv2024GetMany({ localIds }: { localIds: Local['id'][] }) {
	return db
		.select({
			localId: localAdresse.localId,
			qpv2024Id: qpv2024.id,
		})
		.from(localAdresse)
		.innerJoin(
			qpv2024,
			and(
				inArray(localAdresse.localId, localIds),
				sql`ST_Covers(${qpv2024.geometrie}, ${localAdresse.geolocalisation})`
			)
		);
}

export function localOnQpv2015GetMany({ localIds }: { localIds: Local['id'][] }) {
	return db
		.select({
			localId: localAdresse.localId,
			qpv2015Id: qpv2015.id,
		})
		.from(localAdresse)
		.innerJoin(
			qpv2015,
			and(
				inArray(localAdresse.localId, localIds),
				sql`ST_Covers(${qpv2015.geometrie}, ${localAdresse.geolocalisation})`
			)
		);
}
