import {
	and,
	eq,
	count,
	desc,
	inArray,
	isNull,
	gte,
	lt,
	getTableColumns,
	notExists,
	gt,
} from 'drizzle-orm';

import db from '..';
import { Demande, Echange, Equipe, New } from '../types';
import { deveco } from '../../db/schema/equipe';
import { echange, echangeDemande } from '../../db/schema/suivi';
import { eqLocEchange, eqLocContribution } from '../../db/schema/equipe-local';
import { eqEtabEchange } from '../../db/schema/equipe-etablissement';
import { etabCrea, etabCreaEchange } from '../../db/schema/etablissement-creation';
import { compte } from '../../db/schema/admin';
import * as dbQueryEtablissement from '../../db/query/etablissement';
import * as dbQueryEquipeEtablissementSuivi from '../../db/query/equipe-etablissement-suivi';

export function echangeAdd({ echange: echangeValues }: { echange: New<typeof echange> }) {
	return db.insert(echange).values(echangeValues).returning();
}

export function echangeWithRelationsGet({
	equipeId,
	echangeId,
}: {
	equipeId: Equipe['id'];
	echangeId: number;
}) {
	return db.query.echange.findFirst({
		where: and(
			eq(echange.equipeId, equipeId),
			eq(echange.id, echangeId),
			isNull(echange.suppressionDate)
		),
		with: {
			eqEtab: {
				with: {
					etab: {
						with: {
							ul: true,
						},
					},
				},
			},
			etabCreaEch: {
				with: {
					etabCrea: {
						with: {
							transfo: true,
							creas: {
								with: {
									ctct: true,
								},
							},
						},
					},
				},
			},
			eqLoc: true,
			demandes: {
				with: {
					demande: true,
				},
			},
		},
	});
}

function echangeEtablissementCreationInactifSubQueryBuild({
	echangeIdColumn,
}: {
	echangeIdColumn:
		| (typeof echange._.columns)['id']
		| (typeof eqEtabEchange._.columns)['echangeId']
		| (typeof etabCreaEchange._.columns)['echangeId'];
}) {
	return db
		.select({
			echangeId: etabCreaEchange.echangeId,
		})
		.from(etabCreaEchange)
		.innerJoin(etabCrea, eq(etabCreaEchange.etabCreaId, etabCrea.id))
		.where(and(eq(etabCreaEchange.echangeId, echangeIdColumn), eq(etabCrea.actif, false)));
}

export function echangeWithRelationsGetMany({
	equipeId,
	limit,
}: {
	equipeId: Equipe['id'];
	limit?: number;
}) {
	const echangeEtablissementCreationInactifSub = echangeEtablissementCreationInactifSubQueryBuild({
		echangeIdColumn: echange.id,
	});

	return db.query.echange.findMany({
		where: and(
			eq(echange.equipeId, equipeId),
			isNull(echange.suppressionDate),
			notExists(echangeEtablissementCreationInactifSub)
		),
		with: {
			modifCompte: {
				columns: {
					nom: true,
					prenom: true,
					email: true,
				},
			},
			demandes: {
				where: dbQueryEquipeEtablissementSuivi.eqEtabDemandeSupprimeSub,
				with: {
					demande: true,
				},
			},
			deveco: {
				with: {
					compte: {
						columns: {
							nom: true,
							prenom: true,
							email: true,
						},
					},
				},
			},
			// entitées
			eqEtabs: {
				with: {
					etab: {
						with: dbQueryEtablissement.etablissementRelationsBuild({ equipeId, __kind: 'colonne' }),
					},
				},
			},
			creations: {
				with: {
					etabCrea: {
						with: {
							creas: {
								with: {
									ctct: true,
								},
							},
						},
					},
				},
			},
			eqLocaux: {
				with: {
					local: {
						with: {
							contributions: {
								where: eq(eqLocContribution.equipeId, echange.equipeId),
								limit: 1,
							},
						},
					},
				},
			},
		},
		orderBy: [desc(echange.date), desc(echange.id)],
		limit: limit ?? 1000,
	});
}
export function echangeWithRelationsGetManyById({ echangeIds }: { echangeIds: Echange['id'][] }) {
	return db.query.echange.findMany({
		where: and(inArray(echange.id, echangeIds), isNull(echange.suppressionDate)),
		with: {
			eqEtab: {
				with: {
					etab: {
						with: dbQueryEtablissement.etablissementRelationsBuild({
							table: echange,
							__kind: 'table',
						}),
					},
				},
			},
			etabCreaEch: {
				with: {
					etabCrea: {
						with: {
							creas: {
								with: {
									ctct: true,
								},
							},
						},
					},
				},
			},
			eqLoc: {
				with: {
					local: {
						with: {
							contributions: {
								where: eq(eqLocContribution.equipeId, echange.equipeId),
								limit: 1,
							},
						},
					},
				},
			},
		},
		orderBy: [desc(echange.date), desc(echange.id)],
	});
}

export function echangeCountFromDate({ debut, equipeId }: { debut: Date; equipeId: Equipe['id'] }) {
	return db
		.select({
			total: count(echange.id).as('total'),
		})
		.from(echange)
		.where(
			and(
				eq(echange.equipeId, equipeId),
				isNull(echange.suppressionDate),
				gt(echange.creationDate, debut)
			)
		);
}

export function echangeWithRelationsGetManyFromDate({
	tableName,
	equipeId,
	debut,
	fin,
}: {
	tableName: 'eqEtab' | 'etabCrea' | 'eqLoc';
	equipeId: Equipe['id'];
	debut: Date;
	fin: Date;
}) {
	const table =
		tableName === 'etabCrea'
			? etabCreaEchange
			: tableName === 'eqLoc'
				? eqLocEchange
				: eqEtabEchange;

	return db
		.select({
			...getTableColumns(echange),
			deveco: {
				...getTableColumns(deveco),
				...getTableColumns(compte),
			},
		})
		.from(echange)
		.innerJoin(table, eq(echange.id, table.echangeId))
		.innerJoin(deveco, eq(deveco.id, echange.devecoId))
		.innerJoin(compte, eq(compte.id, deveco.compteId))
		.where(
			and(
				eq(table.equipeId, equipeId),
				isNull(echange.suppressionDate),
				gte(echange.date, debut),
				lt(echange.date, fin)
			)
		);
}

export function echangeUpdate({
	echangeId,
	partialEchange,
}: {
	echangeId: number;
	partialEchange: Partial<Echange>;
}) {
	return db.update(echange).set(partialEchange).where(eq(echange.id, echangeId)).returning();
}

export function echangeRemove({ echangeId }: { echangeId: Echange['id'] }) {
	return db
		.update(echange)
		.set({
			suppressionDate: new Date(),
		})
		.where(eq(echange.id, echangeId));
}

export function echangeDemandeAdd({
	echangeId,
	demandeId,
}: {
	echangeId: Echange['id'];
	demandeId: Demande['id'];
}) {
	return db
		.insert(echangeDemande)
		.values({
			echangeId,
			demandeId,
		})
		.returning();
}

export function echangeDemandeAddMany({
	echangeId,
	demandeIds,
}: {
	echangeId: Echange['id'];
	demandeIds: Demande['id'][];
}) {
	return db
		.insert(echangeDemande)
		.values(
			demandeIds.map((demandeId) => ({
				echangeId,
				demandeId,
			}))
		)
		.returning();
}

export function echangeDemandeRemoveAll({ echangeId }: { echangeId: Echange['id'] }) {
	return db.delete(echangeDemande).where(eq(echangeDemande.echangeId, echangeId));
}
