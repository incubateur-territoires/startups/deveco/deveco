import { eq, inArray, notInArray, isNotNull, isNull, and, getTableColumns } from 'drizzle-orm';

import db from '../../db';
import {
	Equipe,
	Local,
	Echange,
	Demande,
	Rappel,
	Brouillon,
	EquipeLocalDemandeWithRelations,
} from '../../db/types';
import {
	eqLocEchange,
	eqLocDemande,
	eqLocRappel,
	eqLocBrouillon,
} from '../../db/schema/equipe-local';
import { echange, demande, rappel, brouillon } from '../../db/schema/suivi';
import { compte } from '../../db/schema/admin';

export const eqLocEchangeSupprimeSub = notInArray(
	eqLocEchange.echangeId,
	db
		.select({
			echangeId: eqLocEchange.echangeId,
		})
		.from(eqLocEchange)
		.innerJoin(echange, eq(echange.id, eqLocEchange.echangeId))
		.where(isNotNull(echange.suppressionDate))
);

export const eqLocDemandeSupprimeSub = notInArray(
	eqLocDemande.demandeId,
	db
		.select({
			demandeId: eqLocDemande.demandeId,
		})
		.from(eqLocDemande)
		.innerJoin(demande, eq(demande.id, eqLocDemande.demandeId))
		.where(isNotNull(demande.suppressionDate))
);

export const eqLocRappelSupprimeSub = notInArray(
	eqLocRappel.rappelId,
	db
		.select({
			rappelId: eqLocRappel.rappelId,
		})
		.from(eqLocRappel)
		.innerJoin(rappel, eq(rappel.id, eqLocRappel.rappelId))
		.where(isNotNull(rappel.suppressionDate))
);

export const eqLocBrouillonSupprimeSub = notInArray(
	eqLocBrouillon.brouillonId,
	db
		.select({
			brouillonId: eqLocBrouillon.brouillonId,
		})
		.from(eqLocBrouillon)
		.innerJoin(brouillon, eq(brouillon.id, eqLocBrouillon.brouillonId))
		.where(isNotNull(brouillon.suppressionDate))
);

export function eqLocEchangeAdd({
	equipeId,
	echangeId,
	localId,
}: {
	equipeId: Equipe['id'];
	echangeId: Echange['id'];
	localId: Local['id'];
}) {
	return db.insert(eqLocEchange).values({
		equipeId,
		localId,
		echangeId,
	});
}

export function eqLocDemandeAdd({
	equipeId,
	localId,
	demandeId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	demandeId: Demande['id'];
}) {
	return db.insert(eqLocDemande).values({
		equipeId,
		localId,
		demandeId,
	});
}

export function eqLocDemandeGetMany({
	equipeId,
	localId,
	localIds,
}: {
	equipeId: Equipe['id'];
	localId?: Local['id'];
	localIds?: Local['id'][];
	avecClotureDate?: boolean;
	avecCreationDate?: boolean;
}): Promise<EquipeLocalDemandeWithRelations[]> {
	const wheres = [eq(eqLocDemande.equipeId, equipeId), isNull(demande.suppressionDate)];

	if (localIds) {
		wheres.push(inArray(eqLocDemande.localId, localIds), eq(eqLocDemande.equipeId, equipeId));
	} else if (localId) {
		wheres.push(eq(eqLocDemande.localId, localId), eq(eqLocDemande.equipeId, equipeId));
	}

	return db
		.select({
			...getTableColumns(eqLocDemande),
			demande,
		})
		.from(eqLocDemande)
		.innerJoin(demande, eq(demande.id, eqLocDemande.demandeId))
		.where(and(...wheres));
}

export function eqLocRappelAdd({
	equipeId,
	localId,
	rappelId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	rappelId: Rappel['id'];
}) {
	return db
		.insert(eqLocRappel)
		.values({
			equipeId,
			localId,
			rappelId,
		})
		.returning();
}

export async function eqLocRappelGetMany({
	equipeId,
	localId,
}: {
	equipeId: Equipe['id'];
	localId?: Local['id'];
}) {
	const where = localId
		? and(eq(eqLocRappel.localId, localId), eq(eqLocRappel.equipeId, equipeId))
		: eq(eqLocRappel.equipeId, equipeId);

	const eqLocRappels = await db
		.select({
			...getTableColumns(eqLocRappel),
			rappel,
			affecteCompte: compte,
		})
		.from(eqLocRappel)
		.innerJoin(rappel, eq(rappel.id, eqLocRappel.rappelId))
		.leftJoin(compte, eq(rappel.affecteCompteId, compte.id))
		.where(where);

	return eqLocRappels.map(({ rappel, affecteCompte, ...eqLocRappel }) => ({
		...eqLocRappel,
		rappel: {
			...rappel,
			affecteCompte,
		},
	}));
}

export function eqLocBrouillonAdd({
	equipeId,
	localId,
	brouillonId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
	brouillonId: Brouillon['id'];
}) {
	return db
		.insert(eqLocBrouillon)
		.values({
			equipeId,
			localId,
			brouillonId,
		})
		.returning();
}

export async function eqLocBrouillonGetMany({
	equipeId,
	localId,
}: {
	equipeId: Equipe['id'];
	localId?: Local['id'];
}) {
	const where = localId
		? and(eq(eqLocBrouillon.localId, localId), eq(eqLocBrouillon.equipeId, equipeId))
		: eq(eqLocBrouillon.equipeId, equipeId);

	const eqLocBrouillons = await db
		.select({
			...getTableColumns(eqLocBrouillon),
			brouillon,
			affecteCompte: compte,
		})
		.from(eqLocBrouillon)
		.innerJoin(brouillon, eq(brouillon.id, eqLocBrouillon.brouillonId))
		.where(where);

	return eqLocBrouillons.map(({ brouillon, affecteCompte, ...eqLocBrouillon }) => ({
		...eqLocBrouillon,
		brouillon: {
			...brouillon,
			affecteCompte,
		},
	}));
}

export async function eqLocSuiviDateGetMany({
	equipeId,
	localId,
}: {
	equipeId: Equipe['id'];
	localId: Local['id'];
}) {
	const query = db
		.select({
			suiviDates: echange.date,
		})
		.from(eqLocEchange)
		.innerJoin(echange, eq(echange.id, eqLocEchange.echangeId))
		.where(
			and(
				eq(eqLocEchange.equipeId, equipeId),
				eq(eqLocEchange.localId, localId),
				isNull(echange.suppressionDate)
			)
		)
		.union(
			db
				.select({
					suiviDates: demande.date,
				})
				.from(eqLocDemande)
				.innerJoin(demande, eq(demande.id, eqLocDemande.demandeId))
				.where(
					and(
						eq(eqLocDemande.equipeId, equipeId),
						eq(eqLocDemande.localId, localId),
						isNull(demande.suppressionDate)
					)
				)
		)
		.union(
			db
				.select({
					suiviDates: rappel.date,
				})
				.from(eqLocRappel)
				.innerJoin(rappel, eq(rappel.id, eqLocRappel.rappelId))
				.where(
					and(
						eq(eqLocRappel.equipeId, equipeId),
						eq(eqLocRappel.localId, localId),
						isNull(rappel.suppressionDate)
					)
				)
		)
		.as('suivi_dates');

	return db
		.selectDistinct({
			suiviDates: query.suiviDates,
		})
		.from(query);
}
