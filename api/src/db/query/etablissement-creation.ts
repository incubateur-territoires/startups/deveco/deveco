import {
	SQL,
	and,
	eq,
	inArray,
	sql,
	exists,
	isNull,
	not,
	or,
	ilike,
	gte,
	lt,
	desc,
	countDistinct,
} from 'drizzle-orm';

import db from '../../db';
import {
	DemandeTypeId,
	PaginationParamsEtablissement,
	EtablissementCreation,
	Equipe,
	SuiviCreationEtablissement,
	Joins,
	Wheres,
	Etiquette,
	New,
	EtiquetteTypeId,
	Etablissement,
	EtablissementCreationCreateur,
	Commune,
	RessourceEtablissementCreation,
} from '../types';
import {
	etabCreaCreateur,
	etabCrea,
	etabCreaDemande,
	etabCreaEtiquette,
	etabCreaTransformation,
	etabCreaCommune,
	etabCreaRessource,
} from '../schema/etablissement-creation';
import { qpv2024, contact, contactAdresse } from '../schema/contact';
import { evenement, actionEtablissementCreation } from '../schema/evenement';
import { etiquette } from '../schema/equipe';
import { demande } from '../schema/suivi';
import * as dbQueryContact from '../../db/query/contact';
import * as dbQueryEvenement from '../../db/query/evenement';
import * as dbQueryEtablissement from '../../db/query/etablissement';
import * as dbQueryEtablissementCreationSuivi from '../../db/query/etablissement-creation-suivi';

export * from '../../db/query/etablissement-creation-suivi';

interface Filters {
	innerJoins: Joins;
	wheres: Wheres;
}

export interface PersoParams {
	equipeId: Equipe['id'];

	recherche: string | null;

	activites: string[];
	localisations: string[];
	motsCles: string[];

	demandeTypeIds: DemandeTypeId[];

	suivi: SuiviCreationEtablissement | null;

	selection?: EtablissementCreation['id'][];
}

export interface PublicParams {
	numero: string | null;
	rue: string | null;
	codePostal: string | null;
	qpvs: string[];
}

export type EtablissementCreationsParams = PersoParams &
	PublicParams &
	PaginationParamsEtablissement;

export async function etabCreaAdd({
	etabCrea: etabCreaValues,
}: {
	etabCrea: New<typeof etabCrea>;
}) {
	const [etabCreaNew] = await db.insert(etabCrea).values(etabCreaValues).returning();

	return etabCreaNew;
}

function etiquetteQueryBuild({
	etiquetteTypeId,
	equipeId,
	noms,
}: {
	equipeId: Equipe['id'];
	etiquetteTypeId?: EtiquetteTypeId;
	noms?: string[];
}) {
	const conditions = [
		eq(etabCreaEtiquette.equipeId, equipeId),
		eq(etabCreaEtiquette.etabCreaId, etabCrea.id),
	];

	if (etiquetteTypeId) {
		conditions.push(eq(etiquette.typeId, etiquetteTypeId));
	}

	if (noms?.length) {
		conditions.push(inArray(etiquette.nom, noms));
	}

	const query = db
		.select({ id: etiquette.id })
		.from(etiquette)
		.innerJoin(etabCreaEtiquette, eq(etabCreaEtiquette.etiquetteId, etiquette.id))
		.where(and(...conditions))
		.limit(1);

	return query;
}

function persoFiltersBuild({
	equipeId,

	activites,
	localisations,
	motsCles,

	demandeTypeIds,
}: PersoParams): Filters {
	const innerJoins: Joins = [];
	const wheres: Wheres = [];

	wheres.push(and(eq(etabCrea.equipeId, equipeId)));

	if (activites.length) {
		wheres.push(
			exists(
				etiquetteQueryBuild({
					equipeId,
					etiquetteTypeId: 'activite',
					noms: activites,
				})
			)
		);
	}

	if (localisations.length) {
		wheres.push(
			exists(
				etiquetteQueryBuild({
					equipeId,
					etiquetteTypeId: 'localisation',
					noms: localisations,
				})
			)
		);
	}

	if (motsCles.length) {
		wheres.push(
			exists(
				etiquetteQueryBuild({
					equipeId,
					etiquetteTypeId: 'mot_cle',
					noms: motsCles,
				})
			)
		);
	}

	if (demandeTypeIds?.length) {
		wheres.push(
			exists(
				db
					.select({ id: demande.id })
					.from(demande)
					.innerJoin(etabCreaDemande, eq(etabCreaDemande.demandeId, demande.id))
					.where(
						and(
							isNull(demande.clotureMotif),
							inArray(demande.typeId, demandeTypeIds),
							eq(demande.equipeId, equipeId),
							eq(etabCreaDemande.etabCreaId, etabCrea.id)
						)
					)
					.limit(1)
			)
		);
	}

	return { innerJoins, wheres };
}

function suiviFiltersBuild({ equipeId, suivi, recherche }: PersoParams): Filters {
	const innerJoins: Joins = [];
	const wheres: Wheres = [];

	const transformation = db
		.select({ id: etabCreaTransformation.id })
		.from(etabCreaTransformation)
		.where(
			and(
				eq(etabCreaTransformation.equipeId, equipeId),
				eq(etabCreaTransformation.etabCreaId, etabCrea.id)
			)
		);

	switch (suivi) {
		case 'createurs': {
			wheres.push(and(eq(etabCrea.creationAbandonnee, false), not(exists(transformation))));
			break;
		}

		case 'etablissements': {
			wheres.push(exists(transformation));
			break;
		}

		case 'abandonnes': {
			wheres.push(eq(etabCrea.creationAbandonnee, true));
			break;
		}

		case null:
		default: {
			break;
		}
	}

	if (recherche) {
		// TODO faut-il splitter sur d'autres caractères ? ; - , & ?
		const recherches = recherche.split(' ');

		wheres.push(
			and(
				...recherches.map((r: string) =>
					or(
						ilike(etabCrea.enseigne, `%${r}%`),
						ilike(contact.nom, `%${r}%`),
						ilike(contact.prenom, `%${r}%`)
					)
				)
			)
		);
	}

	return { innerJoins, wheres };
}

// TODO vérifier, mais il semble que cette fonction n'est _jamais_ appelée.
function publicFiltersBuild({ numero, rue, codePostal, qpvs }: PublicParams): Filters {
	const innerJoins: Joins = [];
	const wheres: Wheres = [];

	if (codePostal || qpvs.length) {
		innerJoins.push({
			relation: contactAdresse,
			condition: eq(contactAdresse.contactId, contact.id),
		});
	}

	if (codePostal) {
		// normalize string to remove accents for search (the data we use is normalized, it would seem)
		const adresseAndConditions = [ilike(contactAdresse.codePostal, codePostal)];

		if (rue) {
			adresseAndConditions.push(
				or(
					// La BAN peut renvoyer des apostrophes dans les deux formats
					ilike(contactAdresse.voieNom, rue.replace(/['’]/g, "''")),
					ilike(contactAdresse.voieNom, rue.replace(/['’]/g, '’'))
				) as SQL<unknown>
			);
		}

		if (numero) {
			adresseAndConditions.push(eq(contactAdresse.numero, numero));
		}

		wheres.push(and(...adresseAndConditions));
	}

	if (qpvs.length) {
		innerJoins.push({
			relation: qpv2024,
			condition: sql`ST_Covers(ST_SetSrid(${qpv2024.geometrie}, 4326), ST_SetSrid(${contactAdresse.geolocalisation}, 4326))`,
		});
		wheres.push(inArray(qpv2024.id, qpvs));
	}

	return { innerJoins, wheres };
}

function filtersBuild(params: EtablissementCreationsParams) {
	const defaultInnerJoins = [
		{
			relation: etabCreaCreateur,
			condition: and(
				eq(etabCreaCreateur.etabCreaId, etabCrea.id),
				eq(etabCreaCreateur.equipeId, params.equipeId)
			),
		},
		{
			relation: contact,
			condition: eq(contact.id, etabCreaCreateur.contactId),
		},
	];

	const { innerJoins: publicInnerJoins, wheres: publicWheres } = publicFiltersBuild(params);
	const { innerJoins: persoInnerJoins, wheres: persoWheres } = persoFiltersBuild(params);
	const { innerJoins: suiviInnerJoins, wheres: suiviWheres } = suiviFiltersBuild(params);

	const result = {
		innerJoins: [...defaultInnerJoins, ...publicInnerJoins, ...persoInnerJoins, ...suiviInnerJoins],
		wheres: [...publicWheres, ...persoWheres, ...suiviWheres, eq(etabCrea.actif, true)],
	};

	return result;
}

export async function etabCreaIdGetManySorted(
	params: EtablissementCreationsParams
): Promise<EtablissementCreation['id'][]> {
	if (params.selection?.length) {
		return params.selection;
	}

	const offset = params.elementsParPage ? ((params.page || 1) - 1) * params.elementsParPage : 0;

	let tri: SQL<unknown>;
	const leftJoins: Joins = [];
	const orderBys = [];

	let evenementSub;

	if (params.tri === 'alphabetique') {
		orderBys.push(
			sql.join(
				[sql`lower(${contact.nom})`, sql.raw(`${params.direction || 'desc'} nulls last`)],
				sql.raw(' ')
			)
		);
		orderBys.push(
			sql.join(
				[sql`lower(${contact.prenom})`, sql.raw(`${params.direction || 'desc'} nulls last`)],
				sql.raw(' ')
			)
		);
		orderBys.push(
			sql.join(
				[sql`lower(${etabCrea.enseigne})`, sql.raw(`${params.direction || 'desc'} nulls last`)],
				sql.raw(' ')
			)
		);
	} else {
		const actionTypeId = params.tri === 'creation' ? 'creation' : 'affichage';

		evenementSub = db.$with(actionTypeId).as(
			dbQueryEvenement.actionEtablissementCreationGetManyByTypeId({
				equipeId: params.equipeId,
				typeId: actionTypeId,
			})
		);

		leftJoins.push({
			// on force le typecheck pour pouvoir utiliser une subquery dans un left join
			relation: evenementSub as any,
			condition: eq(evenementSub.etabCreaId, etabCrea.id),
		});

		tri = sql`${evenementSub.date}`;
		orderBys.push(
			sql.join([tri, sql.raw(`${params.direction || 'desc'} nulls last`)], sql.raw(' '))
		);
	}

	const query = evenementSub
		? db.with(evenementSub).select({ id: etabCrea.id }).from(etabCrea)
		: db.select({ id: etabCrea.id }).from(etabCrea);

	orderBys.push(sql`${etabCrea.id}`);
	const orderBy = sql.join(orderBys, sql.raw(', '));

	const { innerJoins, wheres } = filtersBuild(params);

	innerJoins.forEach(({ relation, condition }) => {
		query.innerJoin(relation, condition);
	});

	leftJoins.forEach(({ relation, condition }) => {
		query.leftJoin(relation, condition);
	});

	query.where(and(...wheres));
	query.orderBy(orderBy);

	if (params.elementsParPage) {
		query.limit(params.elementsParPage).offset(offset);
	}

	const results = await query;

	return results.map(({ id }) => id);
}

export function etabCreaIdCount(params: EtablissementCreationsParams) {
	const { innerJoins, wheres } = filtersBuild(params);

	const query = db
		.select({
			count: countDistinct(etabCrea.id),
		})
		.from(etabCrea);

	innerJoins.forEach(({ relation, condition }) => {
		query.innerJoin(relation, condition);
	});

	query.where(and(...wheres));

	return query;
}

export function etabCreaIdGetManyUnordered(params: EtablissementCreationsParams) {
	const query = db.select({ id: etabCrea.id }).from(etabCrea);
	const { innerJoins, wheres } = filtersBuild(params);

	innerJoins.forEach(({ relation, condition }) => {
		query.innerJoin(relation, condition);
	});

	query.where(and(...wheres));

	return query;
}

export async function etabCreaIdWithTotalGetMany(params: EtablissementCreationsParams) {
	const [etabCreaIds, [{ count: total }]] = await Promise.all([
		etabCreaIdGetManySorted(params),
		etabCreaIdCount(params),
	]);

	return {
		etabCreaIds,
		total: Number(total),
	};
}

export function etabCreaUpdate({
	etabCreaId,
	etabCrea: etabCreaValues,
}: {
	etabCreaId: EtablissementCreation['id'];
	etabCrea: Partial<EtablissementCreation>;
}) {
	return db.update(etabCrea).set(etabCreaValues).where(eq(etabCrea.id, etabCreaId));
}

export function etabCreaEtiquetteIdGetMany({
	equipeId,
	etabCreaId,
}: {
	equipeId: Equipe['id'];
	etabCreaId: EtablissementCreation['id'];
}) {
	return db
		.select({
			etiquetteId: etabCreaEtiquette.etiquetteId,
		})
		.from(etabCreaEtiquette)
		.where(
			and(eq(etabCreaEtiquette.etabCreaId, etabCreaId), eq(etabCreaEtiquette.equipeId, equipeId))
		);
}

export async function etabCreaEtiquetteUpdate({
	equipeId,
	etabCreaId,
	etiquetteIds,
}: {
	equipeId: Equipe['id'];
	etabCreaId: EtablissementCreation['id'];
	etiquetteIds: Etiquette['id'][];
}) {
	if (!etiquetteIds.length) {
		return {
			ajoutees: [],
			supprimees: [],
		};
	}

	const etabCreaEtiquetteFormat = (etiquetteId: Etiquette['id']) => ({
		equipeId,
		etabCreaId,
		etiquetteId,
		auto: false,
	});

	const currentValues = await db.query.etabCreaEtiquette.findMany({
		where: and(
			eq(etabCreaEtiquette.equipeId, equipeId),
			eq(etabCreaEtiquette.etabCreaId, etabCreaId)
		),
	});
	const currentEtiquetteIds = currentValues.map((eee) => eee.etiquetteId);

	const toAdd = etiquetteIds
		.filter((etiquetteId) => !currentEtiquetteIds.includes(etiquetteId))
		.map(etabCreaEtiquetteFormat);

	const added = toAdd.length
		? await db
				.insert(etabCreaEtiquette)
				.values(toAdd)
				.onConflictDoNothing({
					target: [etabCreaEtiquette.etabCreaId, etabCreaEtiquette.etiquetteId],
				})
				.returning({
					etiquetteId: etabCreaEtiquette.etiquetteId,
				})
		: [];

	const toRemove = currentEtiquetteIds.filter((etiquetteId) => !etiquetteIds.includes(etiquetteId));

	const removed = toRemove.length
		? await db
				.delete(etabCreaEtiquette)
				.where(
					and(
						eq(etabCreaEtiquette.etabCreaId, etabCreaId),
						inArray(etabCreaEtiquette.etiquetteId, toRemove)
					)
				)
				.returning({
					etiquetteId: etabCreaEtiquette.etiquetteId,
				})
		: [];

	return {
		ajoutees: added.map(({ etiquetteId }) => etiquetteId),
		supprimees: removed.map(({ etiquetteId }) => etiquetteId),
	};
}

export function etabCreaCheck({
	equipeId,
	etabCreaId,
}: {
	equipeId: EtablissementCreation['equipeId'];
	etabCreaId: EtablissementCreation['id'];
}) {
	return db.query.etabCrea.findFirst({
		where: and(
			eq(etabCrea.id, etabCreaId),
			eq(etabCrea.equipeId, equipeId),
			eq(etabCrea.actif, true)
		),
		columns: {
			id: true,
		},
		with: {
			transfo: true,
		},
	});
}

export function etabCreaWithRelationsGet({
	equipeId,
	etabCreaId,
}: {
	equipeId: Equipe['id'];
	etabCreaId: EtablissementCreation['id'];
}) {
	return db.query.etabCrea.findFirst({
		where: and(
			eq(etabCrea.id, etabCreaId),
			eq(etabCrea.equipeId, equipeId),
			eq(etabCrea.actif, true)
		),
		with: {
			communes: {
				with: {
					commune: true,
				},
			},
			creas: {
				with: {
					ctct: { with: dbQueryContact.contactRelationsBuild({ equipeId }) },
				},
			},
			modifCompte: {
				columns: {
					nom: true,
					prenom: true,
					email: true,
				},
			},
			etiquettes: {
				with: {
					etiquette: true,
				},
			},
			echanges: {
				where: dbQueryEtablissementCreationSuivi.etabCreaEchangeSupprimeSub,
				with: {
					echange: {
						with: {
							modifCompte: {
								columns: {
									nom: true,
									prenom: true,
									email: true,
								},
							},
							deveco: {
								with: {
									compte: {
										columns: {
											nom: true,
											prenom: true,
											email: true,
										},
									},
								},
							},
							demandes: {
								where: dbQueryEtablissementCreationSuivi.etabCreaDemandeSupprimeSub,
								with: {
									demande: true,
								},
							},
						},
					},
				},
			},
			demandes: {
				where: dbQueryEtablissementCreationSuivi.etabCreaDemandeSupprimeSub,
				with: {
					demande: {
						with: { echanges: { with: { echange: true } } },
					},
				},
			},
			rappels: {
				where: dbQueryEtablissementCreationSuivi.etabCreaRappelSupprimeSub,
				with: {
					rappel: {
						with: {
							affecteCompte: {
								columns: {
									// TODO supprimer id et actif dans le front
									id: true,
									nom: true,
									prenom: true,
									email: true,
									actif: true,
								},
							},
						},
					},
				},
			},
			transfo: {
				with: {
					etab: {
						with: dbQueryEtablissement.etablissementRelationsBuild({ __kind: 'colonne', equipeId }),
					},
				},
			},
			contactOrigine: true,
			sourceFinancement: true,
			secteurActivite: true,
			projetType: true,
			ressources: true,
		},
	});
}

export function etabCreaWithCreateurGet({
	equipeId,
	etabCreaId,
}: {
	equipeId: Equipe['id'];
	etabCreaId: EtablissementCreation['id'];
}) {
	return db.query.etabCrea.findFirst({
		where: and(
			eq(etabCrea.equipeId, equipeId),
			eq(etabCrea.id, etabCreaId),
			eq(etabCrea.actif, true)
		),
		with: {
			creas: {
				with: {
					ctct: {
						with: {
							adresse: {
								with: {
									commune: true,
								},
							},
						},
					},
				},
			},
		},
	});
}

export function etabCreaWithQpvGetMany({
	equipeId,
	debut,
	fin,
}: {
	equipeId: Equipe['id'];
	debut: Date;
	fin: Date;
}) {
	return db
		.select({
			qpv: qpv2024.id,
		})
		.from(etabCrea)
		.innerJoin(etabCreaCreateur, eq(etabCreaCreateur.etabCreaId, etabCrea.id))
		.innerJoin(contact, eq(contact.id, etabCreaCreateur.contactId))
		.leftJoin(contactAdresse, eq(contactAdresse.contactId, contact.id))
		.leftJoin(
			qpv2024,
			sql`ST_Covers(ST_SetSrid(${qpv2024.geometrie}, 4326), ST_SetSrid(${contactAdresse.geolocalisation}, 4326))`
		)
		.innerJoin(
			actionEtablissementCreation,
			and(eq(actionEtablissementCreation.etabCreaId, etabCrea.id))
		)
		.innerJoin(evenement, and(eq(evenement.id, actionEtablissementCreation.evenementId)))
		.where(
			and(
				eq(etabCrea.equipeId, equipeId),
				eq(etabCrea.actif, true),
				eq(actionEtablissementCreation.typeId, 'creation'),
				gte(evenement.createdAt, debut),
				lt(evenement.createdAt, fin)
			)
		);
}

export function etabCreaQpvGetMany({
	equipeId,
	etabCreaId,
}: {
	equipeId: Equipe['id'];
	etabCreaId: EtablissementCreation['id'];
}) {
	return db
		.selectDistinctOn([qpv2024.id], {
			id: qpv2024.id,
			nom: qpv2024.nom,
		})
		.from(etabCrea)
		.innerJoin(etabCreaCreateur, eq(etabCreaCreateur.etabCreaId, etabCrea.id))
		.innerJoin(contact, eq(contact.id, etabCreaCreateur.contactId))
		.innerJoin(contactAdresse, eq(contactAdresse.contactId, contact.id))
		.innerJoin(
			qpv2024,
			sql`ST_Covers(ST_SetSrid(${qpv2024.geometrie}, 4326),
			ST_SetSrid(${contactAdresse.geolocalisation}, 4326))`
		)
		.where(and(eq(etabCrea.id, etabCreaId), eq(etabCrea.equipeId, equipeId)));
}

export function etabCreaTransformationGet({
	etabCreaId,
	equipeId,
}: {
	etabCreaId: EtablissementCreation['id'];
	equipeId: Equipe['id'];
}) {
	return db.query.etabCreaTransformation.findFirst({
		where: and(
			eq(etabCreaTransformation.equipeId, equipeId),
			eq(etabCreaTransformation.etabCreaId, etabCreaId)
		),
		columns: {
			id: true,
			etablissementId: true,
		},
		with: {
			crea: {
				columns: {},
				with: {
					creas: {
						columns: {
							contactId: true,
						},
					},
				},
			},
		},
	});
}

export function etabCreaTransformationGetMany() {
	return db.query.etabCreaTransformation.findMany({
		with: {
			crea: {
				columns: {},
				with: {
					creas: {
						with: {
							ctct: {
								with: {
									creaCs: true,
									etabCs: true,
									localCs: true,
								},
							},
						},
					},
				},
			},
		},
	});
}

export function etabCreaTransformationGetManyFromDate({
	equipeId,
	debut,
	fin,
}: {
	equipeId: Equipe['id'];
	debut: Date;
	fin: Date;
}) {
	return db.query.etabCreaTransformation.findMany({
		where: and(
			eq(etabCreaTransformation.equipeId, equipeId),
			// Note : était écrit en sql, pourquoi ?
			gte(etabCreaTransformation.date, debut),
			lt(etabCreaTransformation.date, fin)
		),
	});
}

export function etabCreaTransformationAdd({
	etabCreaId,
	etablissementId,
	equipeId,
}: {
	etabCreaId: EtablissementCreation['id'];
	etablissementId: Etablissement['siret'];
	equipeId: Equipe['id'];
}) {
	return db
		.insert(etabCreaTransformation)
		.values({
			etabCreaId,
			etablissementId,
			equipeId,
			date: new Date(),
		})
		.returning();
}

export function etabCreaTransformationRemove({
	etabCreaId,
	equipeId,
}: {
	etabCreaId: EtablissementCreation['id'];
	equipeId: Equipe['id'];
}) {
	return db
		.delete(etabCreaTransformation)
		.where(
			and(
				eq(etabCreaTransformation.etabCreaId, etabCreaId),
				eq(etabCreaTransformation.equipeId, equipeId)
			)
		);
}

export function etabCreaRemove({ etabCreaId }: { etabCreaId: EtablissementCreation['id'] }) {
	return etabCreaUpdate({
		etabCreaId,
		etabCrea: { actif: false },
	});
}

// TODO penser à vérifier que etabCreaIds n'est pas vide
// sur le site d'appel, on ne peut pas encore le faire dans la fonction
// pour des questions de TypeScript
export function etabCreaWithSimpleRelationsGetMany({
	etabCreaIds,
	equipeId,
}: {
	etabCreaIds: EtablissementCreation['id'][];
	equipeId: Equipe['id'];
}) {
	return db.query.etabCrea.findMany({
		where: and(inArray(etabCrea.id, etabCreaIds), eq(etabCrea.actif, true)),
		with: {
			creas: {
				with: {
					ctct: {
						with: dbQueryContact.contactRelationsBuild({ equipeId }),
					},
				},
			},
			etiquettes: {
				with: {
					etiquette: true,
				},
			},
			demandes: {
				with: {
					demande: true,
				},
			},
		},
	});
}

export function etabCreaWithLastActionGetMany() {
	return db.query.etabCrea.findMany({
		with: {
			creas: {
				with: {
					ctct: {
						with: {
							etabCs: true,
							localCs: true,
							creaCs: true,
						},
					},
				},
			},
			actions: {
				limit: 1,
				orderBy: desc(actionEtablissementCreation.createdAt),
			},
		},
	});
}

export function etabCreaWithRelationsGetManyById({
	etabCreaIds,
}: {
	etabCreaIds: EtablissementCreation['id'][];
}) {
	return db.query.etabCrea.findMany({
		where: inArray(etabCrea.id, etabCreaIds),
		with: {
			creas: {
				with: {
					ctct: {
						with: {
							adresse: {
								with: {
									commune: true,
								},
							},
						},
					},
					diplomeNiveau: true,
					professionSituation: true,
				},
			},
			modifCompte: {
				columns: {
					nom: true,
					prenom: true,
					email: true,
				},
			},
			echanges: {
				where: dbQueryEtablissementCreationSuivi.etabCreaEchangeSupprimeSub,
				with: {
					echange: {
						with: {
							modifCompte: {
								columns: {
									nom: true,
									prenom: true,
									email: true,
								},
							},
							deveco: {
								with: {
									compte: {
										columns: {
											nom: true,
											prenom: true,
											email: true,
										},
									},
								},
							},
							demandes: {
								where: dbQueryEtablissementCreationSuivi.etabCreaDemandeSupprimeSub,
								with: {
									demande: true,
								},
							},
						},
					},
				},
			},
			demandes: {
				where: dbQueryEtablissementCreationSuivi.etabCreaDemandeSupprimeSub,
				with: {
					demande: {
						with: {
							echanges: {
								with: {
									echange: true,
								},
							},
						},
					},
				},
			},
			etiquettes: {
				with: {
					etiquette: true,
				},
			},
			communes: {
				with: {
					commune: {
						columns: {
							nom: true,
						},
					},
				},
			},
			projetType: true,
			contactOrigine: true,
			secteurActivite: true,
			sourceFinancement: true,
		},
	});
}

export function etabCreaEtiquetteAddMany({
	equipeId,
	etabCreaIds,
	etiquetteIds,
}: {
	equipeId: Equipe['id'];
	etabCreaIds: EtablissementCreation['id'][];
	etiquetteIds: Etiquette['id'][];
}) {
	const values = etabCreaIds.reduce(
		(values, etabCreaId) => [
			...values,
			...etiquetteIds.map((etiquetteId) => ({
				equipeId,
				etabCreaId,
				etiquetteId,
			})),
		],
		[] as {
			equipeId: Equipe['id'];
			etabCreaId: EtablissementCreation['id'];
			etiquetteId: Etiquette['id'];
		}[]
	);

	return db
		.insert(etabCreaEtiquette)
		.values(values)
		.onConflictDoNothing({
			target: [etabCreaEtiquette.etabCreaId, etabCreaEtiquette.etiquetteId],
		})
		.returning({
			etablissementId: etabCreaEtiquette.etabCreaId,
			etiquetteId: etabCreaEtiquette.etiquetteId,
		});
}

export function etabCreaTypeProjetGetMany() {
	return db.query.etabCreaProjetType.findMany();
}

export function etabCreaSecteurActiviteGetMany() {
	return db.query.etabCreaSecteurActivite.findMany();
}

export function etabCreaSourceFinancementGetMany() {
	return db.query.etabCreaSourceFinancement.findMany();
}

export function etabCreaSituationProfessionnelleGetMany() {
	return db.query.professionSituation.findMany();
}

export function etabCreaNiveauDiplomeGetMany() {
	return db.query.diplomeNiveau.findMany();
}

export function etabCreaCreateurAdd({
	equipeId,
	etabCreaId,
	contactId,
	fonction,
	niveauDiplome,
	situationProfessionnelle,
}: {
	equipeId: EtablissementCreationCreateur['equipeId'];
	etabCreaId: EtablissementCreationCreateur['etabCreaId'];
	contactId: EtablissementCreationCreateur['contactId'];
	fonction?: EtablissementCreationCreateur['fonction'];
	niveauDiplome?: EtablissementCreationCreateur['diplomeNiveauId'];
	situationProfessionnelle?: EtablissementCreationCreateur['professionSituationId'];
}) {
	return (
		db
			.insert(etabCreaCreateur)
			.values({
				equipeId,
				etabCreaId,
				contactId,
				fonction,
				diplomeNiveauId: niveauDiplome,
				professionSituationId: situationProfessionnelle,
			})
			// si le contact a déjà été ajouté à l'établissement, on ne fait rien
			.onConflictDoNothing()
			.returning()
	);
}

export function etabCreaCreateurGetMany({
	etabCreaId,
}: {
	etabCreaId: EtablissementCreationCreateur['etabCreaId'];
}) {
	return db
		.select()
		.from(etabCreaCreateur)
		.where(and(eq(etabCreaCreateur.etabCreaId, etabCreaId)));
}

export function etabCreaCreateurUpdate({
	equipeId,
	etabCreaId,
	contactId,
	fonction,
	niveauDiplome,
	situationProfessionnelle,
}: {
	equipeId: EtablissementCreationCreateur['equipeId'];
	etabCreaId: EtablissementCreationCreateur['etabCreaId'];
	contactId: EtablissementCreationCreateur['contactId'];
	fonction?: EtablissementCreationCreateur['fonction'];
	niveauDiplome?: EtablissementCreationCreateur['diplomeNiveauId'];
	situationProfessionnelle?: EtablissementCreationCreateur['professionSituationId'];
}) {
	return db
		.update(etabCreaCreateur)
		.set({
			fonction,
			diplomeNiveauId: niveauDiplome,
			professionSituationId: situationProfessionnelle,
		})
		.where(
			and(
				eq(etabCreaCreateur.equipeId, equipeId),
				eq(etabCreaCreateur.etabCreaId, etabCreaId),
				eq(etabCreaCreateur.contactId, contactId)
			)
		)
		.returning();
}

export function etabCreaCreateurRemove({
	equipeId,
	etabCreaId,
	contactId,
}: {
	equipeId: EtablissementCreationCreateur['equipeId'];
	etabCreaId: EtablissementCreationCreateur['etabCreaId'];
	contactId: EtablissementCreationCreateur['contactId'];
}) {
	return db
		.delete(etabCreaCreateur)
		.where(
			and(
				eq(etabCreaCreateur.equipeId, equipeId),
				eq(etabCreaCreateur.etabCreaId, etabCreaId),
				eq(etabCreaCreateur.contactId, contactId)
			)
		)
		.returning();
}

export function etabCreaContactOrigineGetMany() {
	return db.query.etabCreaContactOrigine.findMany();
}

export async function etabCreaCommuneRemoveMany({
	etabCreaId,
	equipeId,
}: {
	etabCreaId: EtablissementCreation['id'];
	equipeId: Equipe['id'];
}) {
	return db
		.delete(etabCreaCommune)
		.where(and(eq(etabCreaCommune.etabCreaId, etabCreaId), eq(etabCreaCommune.equipeId, equipeId)));
}

export async function etabCreaCommuneAddMany({
	etabCreaId,
	equipeId,
	communes,
}: {
	etabCreaId: EtablissementCreation['id'];
	equipeId: Equipe['id'];
	communes: Commune['id'][];
}) {
	return db
		.insert(etabCreaCommune)
		.values(communes.map((communeId) => ({ etabCreaId, equipeId, communeId })));
}

export function ressourceAdd({
	equipeId,
	etabCreaId,
	partialRessource,
}: {
	equipeId: Equipe['id'];
	etabCreaId: EtablissementCreation['id'];
	partialRessource: Partial<RessourceEtablissementCreation>;
}) {
	return db.insert(etabCreaRessource).values({ etabCreaId, equipeId, ...partialRessource });
}

export function ressourceUpdate({
	ressourceId,
	partialRessource,
}: {
	ressourceId: RessourceEtablissementCreation['id'];
	partialRessource: Partial<RessourceEtablissementCreation>;
}) {
	return db
		.update(etabCreaRessource)
		.set({ ...partialRessource })
		.where(eq(etabCreaRessource.id, ressourceId));
}

export function ressourceRemove({
	ressourceId,
}: {
	ressourceId: RessourceEtablissementCreation['id'];
}) {
	return db.delete(etabCreaRessource).where(eq(etabCreaRessource.id, ressourceId));
}

export function etabCreaCreationAbandonneeUpdate({
	etabCreaId,
	equipeId,
	creationAbandonnee,
}: {
	etabCreaId: EtablissementCreation['id'];
	equipeId: Equipe['id'];
	creationAbandonnee: EtablissementCreation['creationAbandonnee'];
}) {
	return db
		.update(etabCrea)
		.set({ creationAbandonnee })
		.where(and(eq(etabCrea.equipeId, equipeId), eq(etabCrea.id, etabCreaId)));
}
