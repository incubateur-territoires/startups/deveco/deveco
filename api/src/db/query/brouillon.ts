import { sql, and, eq, count, desc, gte, isNull, notExists } from 'drizzle-orm';

import db from '..';
import { Deveco, Equipe, New, Brouillon } from '../types';
import { brouillon } from '../schema/suivi';
import { eqEtabBrouillon } from '../schema/equipe-etablissement';
import { eqLocContribution } from '../schema/equipe-local';
import { etabCrea, etabCreaBrouillon } from '../schema/etablissement-creation';
import * as dbQueryEtablissement from '../query/etablissement';
import { deveco } from '../schema/equipe';

export function brouillonAdd({ brouillon: brouillonValues }: { brouillon: New<typeof brouillon> }) {
	return db.insert(brouillon).values(brouillonValues).returning();
}

export function brouillonGet({
	equipeId,
	brouillonId,
}: {
	equipeId: Equipe['id'];
	brouillonId: Brouillon['id'];
}) {
	return db.query.brouillon.findFirst({
		where: and(
			eq(brouillon.equipeId, equipeId),
			eq(brouillon.id, brouillonId),
			isNull(brouillon.suppressionDate)
		),
	});
}

export function brouillonActifsGetForDeveco({
	brouillonId,
	devecoId,
}: {
	brouillonId: Brouillon['id'];
	devecoId: Deveco['id'];
}) {
	// TODO trouver comment récupérer les brouillons de toute l'équipe
	return db
		.select({
			brouillonId: brouillon.id,
			date: brouillon.date,
		})
		.from(brouillon)
		.innerJoin(deveco, eq(deveco.id, devecoId))
		.where(
			and(eq(brouillon.id, brouillonId), isNull(brouillon.suppressionDate), eq(deveco.id, devecoId))
		);
}

export function brouillonWithRelationsGet({ brouillonId }: { brouillonId: number }) {
	return db.query.brouillon.findFirst({
		where: and(eq(brouillon.id, brouillonId), isNull(brouillon.suppressionDate)),
		with: {
			eqEtabs: true,
			creations: true,
			eqLocaux: true,
		},
	});
}

function brouillonEtablissementCreationInactifSubQueryBuild({
	brouillonIdColumn,
}: {
	brouillonIdColumn:
		| (typeof brouillon._.columns)['id']
		| (typeof eqEtabBrouillon._.columns)['brouillonId']
		| (typeof etabCreaBrouillon._.columns)['brouillonId'];
}) {
	return db
		.select({
			brouillonId: etabCreaBrouillon.brouillonId,
		})
		.from(etabCreaBrouillon)
		.innerJoin(etabCrea, eq(etabCreaBrouillon.etabCreaId, etabCrea.id))
		.where(and(eq(etabCreaBrouillon.brouillonId, brouillonIdColumn), eq(etabCrea.actif, false)));
}

export function brouillonOuvertCountFromDate({
	debut,
	equipeId,
}: {
	debut: Date;
	equipeId: Equipe['id'];
}) {
	const brouillonEtablissementCreationInactifSub =
		brouillonEtablissementCreationInactifSubQueryBuild({
			brouillonIdColumn: brouillon.id,
		});

	return db
		.select({
			total: count(brouillon.id).as('total'),
		})
		.from(brouillon)
		.where(
			and(
				gte(brouillon.creationDate, debut),
				isNull(brouillon.suppressionDate),
				notExists(brouillonEtablissementCreationInactifSub),
				eq(brouillon.equipeId, equipeId)
			)
		);
}

export function brouillonOuvertCountByDevecoFromDate({
	equipeId,
	debut,
}: {
	equipeId: Equipe['id'];
	debut: Date;
}) {
	const brouillonEtablissementCreationInactifSub =
		brouillonEtablissementCreationInactifSubQueryBuild({
			brouillonIdColumn: brouillon.id,
		});

	return db
		.select({
			aVenir: sql<number>`SUM(CASE WHEN date >= NOW() THEN 1 ELSE 0 END)`.as('aVenir'),
			enRetard: sql<number>`SUM(CASE WHEN date < NOW() THEN 1 ELSE 0 END)`.as('enRetard'),
		})
		.from(brouillon)
		.where(
			and(
				gte(brouillon.creationDate, debut),
				isNull(brouillon.suppressionDate),
				notExists(brouillonEtablissementCreationInactifSub),
				eq(brouillon.equipeId, equipeId)
			)
		);
}

export function brouillonWithRelationsGetMany({ equipeId }: { equipeId: Equipe['id'] }) {
	const brouillonEtablissementCreationInactifSub =
		brouillonEtablissementCreationInactifSubQueryBuild({
			brouillonIdColumn: brouillon.id,
		});

	return db.query.brouillon.findMany({
		where: and(
			eq(brouillon.equipeId, equipeId),
			isNull(brouillon.suppressionDate),
			notExists(brouillonEtablissementCreationInactifSub)
		),
		with: {
			modifCompte: {
				columns: {
					nom: true,
					prenom: true,
					email: true,
				},
			},
			compte: {
				columns: {
					nom: true,
					prenom: true,
				},
			},
			eqEtabs: {
				with: {
					etab: {
						with: dbQueryEtablissement.etablissementRelationsBuild({ equipeId, __kind: 'colonne' }),
					},
				},
			},
			creations: {
				with: {
					etabCrea: {
						with: {
							creas: {
								with: {
									ctct: true,
								},
							},
						},
					},
				},
			},
			eqLocaux: {
				with: {
					local: {
						with: {
							contributions: {
								where: eq(eqLocContribution.equipeId, brouillon.equipeId),
								limit: 1,
							},
						},
					},
				},
			},
		},
		orderBy: [desc(brouillon.date), desc(brouillon.id)],
	});
}

export function brouillonUpdate({
	brouillonId,
	partialBrouillon,
}: {
	brouillonId: number;
	partialBrouillon: Partial<Brouillon>;
}) {
	return db
		.update(brouillon)
		.set(partialBrouillon)
		.where(eq(brouillon.id, brouillonId))
		.returning();
}

export function brouillonRemove({ brouillonId }: { brouillonId: Brouillon['id'] }) {
	return db
		.update(brouillon)
		.set({
			suppressionDate: new Date(),
		})
		.where(eq(brouillon.id, brouillonId));
}
