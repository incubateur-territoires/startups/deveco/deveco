import {
	pgTable,
	pgEnum,
	integer,
	index,
	doublePrecision,
	varchar,
	boolean,
	foreignKey,
	date,
	unique,
	primaryKey,
	timestamp,
	jsonb,
} from 'drizzle-orm/pg-core';
import { relations } from 'drizzle-orm';

import {
	commune,
	communeAcv,
	communePvd,
	communeVa,
	communeTerritoireIndustrie,
	communeAfr,
} from '../../db/schema/territoire';
import { adresseColumns } from '../../db/schema/_columns';
import { etabCrea, etabCreaTransformation } from '../../db/schema/etablissement-creation';
import { eqLocOccupant } from '../../db/schema/equipe-local';
import {
	devecoEtablissementFavori,
	eqEtabContact,
	eqEtabContribution,
	eqEtabEtiquette,
	eqEtabDemande,
	eqEtabEchange,
	eqEtabRappel,
	eqEtabRessource,
	etablissementGeolocalisationVue,
} from '../../db/schema/equipe-etablissement';
import { equipeCommuneVue } from '../../db/schema/equipe';
import { proprietairePersonneMorale } from '../../db/schema/local';
import { qpv2024, qpv2015 } from '../../db/schema/contact';
import { source } from '../../db/schema/source';
import {
	ApiEntrepriseMandataireSocial,
	ApiEntrepriseBeneficiaireEffectif,
	ApiEntrepriseExercice,
	ApiEntrepriseLiasseFiscale,
} from '../../api/api-entreprise';

export const nafType = pgTable(
	'naf_type',
	{
		revisionTypeId: varchar('naf_revision_type_id', { length: 10 }).notNull(),
		id: varchar('id', { length: 10 }).notNull(),
		nom: varchar('nom').notNull(),
		niveau: integer('niveau').notNull(),
		parentId: varchar('parent_naf_type_id', { length: 10 }),
	},
	(table) => ({
		pkNafType: primaryKey({
			name: 'pk_naf_type',
			columns: [table.revisionTypeId, table.id],
		}),
		fkNafTypeParentNafType: foreignKey({
			name: 'fk_naf_type__parent_naf_type',
			columns: [table.parentId, table.revisionTypeId],
			foreignColumns: [table.id, table.revisionTypeId],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),

		fkNafTypeRevisionType: foreignKey({
			name: 'fk_naf_type__naf_revision_type',
			columns: [table.revisionTypeId],
			foreignColumns: [nafRevisionType.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
	})
);

export const nafTypeRelations = relations(nafType, ({ one }) => ({
	parent: one(nafType, {
		fields: [nafType.parentId, nafType.revisionTypeId],
		references: [nafType.id, nafType.revisionTypeId],
	}),
	revisionType: one(nafRevisionType, {
		fields: [nafType.revisionTypeId],
		references: [nafRevisionType.id],
	}),
}));

export const nafRevisionType = pgTable(
	'naf_revision_type',
	{
		id: varchar('id', { length: 10 }).notNull(),
		nom: varchar('nom', { length: 30 }).notNull(),
	},
	(table) => ({
		pkNafRevisionType: primaryKey({
			name: 'pk_naf_revision_type',
			columns: [table.id],
		}),
	})
);

export const categorieJuridiqueType = pgTable(
	'categorie_juridique_type',
	{
		id: varchar('id', { length: 4 }).notNull(),
		nom: varchar('nom').notNull(),
		parentId: varchar('parent_categorie_juridique_type_id', { length: 4 }),
		niveau: integer('niveau').notNull(),
	},
	(table) => ({
		pkCategorieJuridiqueType: primaryKey({
			name: 'pk_categorie_juridique_type',
			columns: [table.id],
		}),
		fkCategorieJuridiqueTypeParentCategorieJuridiqueType: foreignKey({
			name: 'fk_categorie_juridique_type__parent_categorie_juridique_type',
			columns: [table.parentId],
			foreignColumns: [table.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
	})
);

export const categorieJuridiqueTypeRelations = relations(categorieJuridiqueType, ({ one }) => ({
	parent: one(categorieJuridiqueType, {
		fields: [categorieJuridiqueType.parentId],
		references: [categorieJuridiqueType.id],
	}),
}));

export const entrepriseCategorieType = pgTable(
	'entreprise_categorie_type',
	{
		id: varchar('id', { length: 3 }).notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkEntrepriseCategorieType: primaryKey({
			name: 'pk_entreprise_categorie_type',
			columns: [table.id],
		}),
	})
);

export const effectifType = pgTable(
	'effectif_type',
	{
		id: varchar('id', { length: 2 }).notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkEffectifType: primaryKey({
			name: 'pk_effectif_type',
			columns: [table.id],
		}),
	})
);

// API Entreprise

export const apiEntrepriseEntrepriseMandataireSocial = source.table(
	'api_entreprise__entreprise_mandataire_social',
	{
		siren: varchar('siren', { length: 9 }).notNull(),
		payload: jsonb('payload').$type<ApiEntrepriseMandataireSocial[]>().notNull(),
		miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' }).defaultNow(),
	},
	(table) => [
		primaryKey({
			name: 'pk_api_entreprise__entreprise_mandataire_social',
			columns: [table.siren],
		}),
	]
);

export const apiEntrepriseEntrepriseMandataireSocialRelations = relations(
	apiEntrepriseEntrepriseMandataireSocial,
	({ one }) => ({
		ul: one(entreprise, {
			fields: [apiEntrepriseEntrepriseMandataireSocial.siren],
			references: [entreprise.siren],
		}),
	})
);

export const apiEntrepriseEntrepriseBeneficiaireEffectif = source.table(
	'api_entreprise__entreprise_beneficiaire_effectif',
	{
		siren: varchar('siren', { length: 9 }).notNull(),
		payload: jsonb('payload').$type<ApiEntrepriseBeneficiaireEffectif[]>().notNull(),
		miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' }).defaultNow(),
	},
	(table) => [
		primaryKey({
			name: 'pk_api_entreprise__entreprise_beneficiaire_effectif',
			columns: [table.siren],
		}),
	]
);

export const apiEntrepriseEntrepriseBeneficiaireEffectifRelations = relations(
	apiEntrepriseEntrepriseBeneficiaireEffectif,
	({ one }) => ({
		ul: one(entreprise, {
			fields: [apiEntrepriseEntrepriseBeneficiaireEffectif.siren],
			references: [entreprise.siren],
		}),
	})
);

export const apiEntrepriseEntrepriseExercice = source.table(
	'api_entreprise__entreprise_exercice',
	{
		siren: varchar('siren', { length: 9 }).notNull(),
		payload: jsonb('payload').$type<ApiEntrepriseExercice[]>().notNull(),
		miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' }).defaultNow(),
	},
	(table) => [
		primaryKey({
			name: 'pk_api_entreprise__entreprise_exercice',
			columns: [table.siren],
		}),
	]
);

export const apiEntrepriseEntrepriseExerciceRelations = relations(
	apiEntrepriseEntrepriseExercice,
	({ one }) => ({
		ul: one(entreprise, {
			fields: [apiEntrepriseEntrepriseExercice.siren],
			references: [entreprise.siren],
		}),
	})
);

export const apiEntrepriseEntrepriseLiasseFiscale = source.table(
	'api_entreprise__entreprise_liasse_fiscale',
	{
		siren: varchar('siren', { length: 9 }).notNull(),
		payload: jsonb('payload').$type<ApiEntrepriseLiasseFiscale | null>().notNull(),
		miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' }).defaultNow(),
		reseauFait: boolean('reseau_fait').default(false),
	},
	(table) => [
		primaryKey({
			name: 'pk_api_entreprise__entreprise_liasse_fiscale',
			columns: [table.siren],
		}),
	]
);

export const apiEntrepriseEntrepriseLiasseFiscaleRelations = relations(
	apiEntrepriseEntrepriseLiasseFiscale,
	({ one }) => ({
		ul: one(entreprise, {
			fields: [apiEntrepriseEntrepriseLiasseFiscale.siren],
			references: [entreprise.siren],
		}),
	})
);

export const apiEntrepriseEntrepriseParticipation = source.table(
	'api_entreprise__entreprise_participation',
	{
		source: varchar('source', { length: 9 }).notNull(),
		cible: varchar('cible', { length: 9 }).notNull(),
		miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' }).defaultNow(),
	},
	(table) => [
		primaryKey({
			name: 'pk_api_entreprise__entreprise_participation',
			columns: [table.source, table.cible],
		}),
		index('idx_api_entreprise__entreprise_participation__source').on(table.source),
		index('idx_api_entreprise__entreprise_participation__cible').on(table.cible),
	]
);

// RCD

export const rcdEtablissementEffectifEmm13mois = source.table(
	'rcd__etablissement_effectif_emm_13_mois',
	{
		siret: varchar('siret', { length: 14 }).notNull(),
		date: date('date', { mode: 'date' }).notNull(),
		effectif: doublePrecision('effectif'),
	},
	(table) => ({
		pkRcdEtablissementEffectifEmm13mois: primaryKey({
			name: 'pk_rcd__etablissement_effectif_emm_13_mois',
			columns: [table.siret, table.date],
		}),
	})
);

export const rcdEtablissementEffectifEmm13moisRelations = relations(
	rcdEtablissementEffectifEmm13mois,
	({ one }) => ({
		etab: one(etablissement, {
			fields: [rcdEtablissementEffectifEmm13mois.siret],
			references: [etablissement.siret],
		}),
	})
);

export const rcdEtablissementEffectifEmm13moisAnnuel = source.table(
	'rcd__etablissement_effectif_emm_13_mois_annuel',
	{
		siret: varchar('siret', { length: 14 }).notNull(),
		date: date('date', { mode: 'date' }).notNull(),
		effectif: doublePrecision('effectif'),
	},
	(table) => ({
		pkRcdEtablissementEffectifEmm13moisAnnuel: primaryKey({
			name: 'pk_rcd__etablissement_effectif_emm_13_mois_annuel',
			columns: [table.siret, table.date],
		}),
	})
);

export const rcdEtablissementEffectifEmm13moisAnnuelRelations = relations(
	rcdEtablissementEffectifEmm13moisAnnuel,
	({ one }) => ({
		etab: one(etablissement, {
			fields: [rcdEtablissementEffectifEmm13moisAnnuel.siret],
			references: [etablissement.siret],
		}),
	})
);

export const rcdEtablissementEffectifEmmHistorique = source.table(
	'rcd__etablissement_effectif_emm_historique',
	{
		siret: varchar('siret', { length: 14 }).notNull(),
		date: date('date', { mode: 'date' }).notNull(),
		effectif: doublePrecision('effectif'),
	},
	(table) => ({
		pkRcdEtablissementEffectifEmmHistorique: primaryKey({
			name: 'pk_rcd__etablissement_effectif_emm_historique',
			columns: [table.siret, table.date],
		}),
	})
);

// INPI

export const inpiRneEntreprepriseMicro = source.table(
	'inpi_rne__entreprise_micro',
	{
		entrepriseId: varchar('entreprise_id', { length: 9 }).notNull(),
	},
	(table) => ({
		pkMicroFrance: primaryKey({
			name: 'pk_inpi_rne__entreprise_micro',
			columns: [table.entrepriseId],
		}),
	})
);

export const inpiRneEntreprepriseMicroRelations = relations(
	inpiRneEntreprepriseMicro,
	({ one }) => ({
		ul: one(entreprise, {
			fields: [inpiRneEntreprepriseMicro.entrepriseId],
			references: [entreprise.siren],
		}),
	})
);

export const inpiRatioFinancier = source.table(
	'inpi__ratio_financier',
	{
		siren: varchar('siren', { length: 9 }),
		dateClotureExercice: date('date_cloture_exercice', { mode: 'date' }).notNull(),
		chiffreDAffaires: doublePrecision('chiffre_d_affaires'),
		margeBrute: doublePrecision('marge_brute'),
		ebe: doublePrecision('ebe'),
		ebit: doublePrecision('ebit'),
		resultatNet: doublePrecision('resultat_net'),
		tauxDEndettement: doublePrecision('taux_d_endettement'),
		ratioDeLiquidite: doublePrecision('ratio_de_liquidite'),
		ratioDeVetuste: doublePrecision('ratio_de_vetuste'),
		autonomieFinanciere: doublePrecision('autonomie_financiere'),
		poidsBfrExploitationSurCa: doublePrecision('poids_bfr_exploitation_sur_ca'),
		couvertureDesInterets: doublePrecision('couverture_des_interets'),
		cafSurCa: doublePrecision('caf_sur_ca'),
		capaciteDeRemboursement: doublePrecision('capacite_de_remboursement'),
		margeEbe: doublePrecision('marge_ebe'),
		resultatCourantAvantImpotsSurCa: doublePrecision('resultat_courant_avant_impots_sur_ca'),
		poidsBfrExploitationSurCaJours: doublePrecision('poids_bfr_exploitation_sur_ca_jours'),
		rotationDesStocksJours: doublePrecision('rotation_des_stocks_jours'),
		creditClientsJours: doublePrecision('credit_clients_jours'),
		creditFournisseursJours: doublePrecision('credit_fournisseurs_jours'),
		typeBilan: varchar('type_bilan', { length: 1 }),
		confidentiality: varchar('confidentiality'),
		totalGeneral: doublePrecision('total_general'),
	},
	(table) => ({
		pkInpiRatioFinancier: primaryKey({
			name: 'pk_inpi__ratio_financier',
			columns: [table.siren, table.dateClotureExercice, table.typeBilan],
		}),
		idxInpiRatioFinancier: index('idx_inpi__ratio_financier').on(table.siren),
	})
);

export const inpiRatioFinancierRelations = relations(inpiRatioFinancier, ({ one }) => ({
	ul: one(entreprise, {
		fields: [inpiRatioFinancier.siren],
		references: [entreprise.siren],
	}),
}));

// ESS

export const essFranceEntreprepriseEss = source.table(
	'ess_france__entreprise_ess',
	{
		entrepriseId: varchar('entreprise_id', { length: 9 }).notNull(),
	},
	(table) => ({
		pkEssFrance: primaryKey({
			name: 'pk_ess_france__entreprise_ess',
			columns: [table.entrepriseId],
		}),
	})
);

export const essFranceEntreprepriseEssRelations = relations(
	essFranceEntreprepriseEss,
	({ one }) => ({
		ul: one(entreprise, {
			fields: [essFranceEntreprepriseEss.entrepriseId],
			references: [entreprise.siren],
		}),
	})
);

// DGCL

export const dgclSubvention = source.table(
	'dgcl__subvention',
	{
		siret: varchar('siret', { length: 14 }).notNull(),
		type: varchar('type').notNull(),
		attributaire: varchar('attributaire').notNull(),
		objet: varchar('objet').notNull(),
		nature: varchar('nature').notNull(),
		montant: doublePrecision('montant').notNull(),
		conventionDate: date('convention_date', { mode: 'date' }).notNull(),
		versementDate: date('versement_date', { mode: 'date' }).notNull(),
	},
	(table) => ({
		idxDgclSubvention: index('idx_dgcl__subvention__siret').on(table.siret),
	})
);

export const dgclSubventionRelations = relations(dgclSubvention, ({ one }) => ({
	etab: one(etablissement, {
		fields: [dgclSubvention.siret],
		references: [etablissement.siret],
	}),
}));

// EGAPRO

export const egaproEntreprise = source.table(
	'egapro__entreprise',
	{
		siren: varchar('siren', { length: 9 }).notNull(),
		annee: integer('annee').notNull(),
		// index égalité
		noteEcartRemuneration: integer('note_ecart_remuneration'),
		noteEcartTauxAugmentationHorsPromotion: integer('note_ecart_taux__augmentation_hors_promotion'),
		noteEcartTauxPromotion: integer('note_ecart_taux_promotion'),
		noteEcartTauxAugmentation: integer('note_ecart_taux_augmentation'),
		noteRetourCongeMaternite: integer('note_retour_conge_maternite'),
		noteHautesRemunerations: integer('note_hautes_remunerations'),
		noteIndex: integer('note_index'),
		// représentation équilibrée
		cadresPourcentageFemmes: doublePrecision('cadres_pourcentage_femmes'),
		cadresPourcentageHommes: doublePrecision('cadres_pourcentage_hommes'),
		membresPourcentageFemmes: doublePrecision('membres_pourcentage_femmes'),
		membresPourcentageHommes: doublePrecision('membres_pourcentage_hommes'),
		miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' })
			.defaultNow()
			.notNull(),
	},
	(table) => [
		primaryKey({
			name: 'pk_egapro__entreprise',
			columns: [table.siren, table.annee],
		}),
	]
);

export const egaproEntrepriseRelations = relations(egaproEntreprise, ({ one }) => ({
	entre: one(entreprise, {
		fields: [egaproEntreprise.siren],
		references: [entreprise.siren],
	}),
}));

// INSEE

export const inseeSireneUniteLegaleCategorieEntrepriseEnum = pgEnum(
	'insee_sirene__unite_legale__categorie_entreprise_enum',
	['GE', 'ETI', 'PME']
);
export const inseeSireneUniteLegaleEcoSocSolEnum = pgEnum(
	'insee_sirene__unite_legale__eco_soc_sol_enum',
	['N', 'O']
);
export const inseeSireneUniteLegaleEtatAdministratifEnum = pgEnum(
	'insee_sirene__unite_legale__etat_administratif_enum',
	['C', 'A']
);
export const inseeSireneUniteLegaleSexeEnum = pgEnum('insee_sirene__unite_legale__sexe_enum', [
	'[ND]',
	'M',
	'F',
]);
export const inseeSireneUniteLegaleSocieteMissionEnum = pgEnum(
	'insee_sirene__unite_legale__societe_mission_enum',
	['N', 'O']
);
export const inseeSireneUniteLegaleStatutDiffusionEnum = pgEnum(
	'insee_sirene__unite_legale__statut_diffusion_enum',
	['P', 'N', 'O']
);

export const entreprise = source.table(
	'insee_sirene_api__unite_legale',
	{
		siren: varchar('siren', { length: 9 }).notNull(),
		activitePrincipaleUniteLegale: varchar('activite_principale_unite_legale', {
			length: 6,
		}),
		anneeCategorieEntreprise: integer('annee_categorie_entreprise'),
		anneeEffectifsUniteLegale: integer('annee_effectifs_unite_legale'),
		caractereEmployeurUniteLegale: varchar('caractere_employeur_unite_legale', {
			length: 6,
		}),
		categorieEntreprise: inseeSireneUniteLegaleCategorieEntrepriseEnum('categorie_entreprise'),
		categorieJuridiqueUniteLegale: varchar('categorie_juridique_unite_legale', { length: 4 }),
		dateCreationUniteLegale: date('date_creation_unite_legale', { mode: 'date' }),
		// date de début de la dernière période
		dateDebut: date('date_debut', { mode: 'date' }),
		dateDernierTraitementUniteLegale: timestamp('date_dernier_traitement_unite_legale', {
			withTimezone: true,
			mode: 'date',
		}),
		denominationUniteLegale: varchar('denomination_unite_legale', {
			length: 120,
		}),
		denominationUsuelle1UniteLegale: varchar('denomination_usuelle1_unite_legale'),
		denominationUsuelle2UniteLegale: varchar('denomination_usuelle2_unite_legale'),
		denominationUsuelle3UniteLegale: varchar('denomination_usuelle3_unite_legale'),
		economieSocialeSolidaireUniteLegale: inseeSireneUniteLegaleEcoSocSolEnum(
			'economie_sociale_solidaire_unite_legale'
		),
		etatAdministratifUniteLegale: inseeSireneUniteLegaleEtatAdministratifEnum(
			'etat_administratif_unite_legale'
		),
		identifiantAssociationUniteLegale: varchar('identifiant_association_unite_legale', {
			length: 10,
		}),
		nicSiegeUniteLegale: varchar('nic_siege_unite_legale', { length: 5 }),
		nombrePeriodesUniteLegale: integer('nombre_periodes_unite_legale'),
		nomenclatureActivitePrincipaleUniteLegale: varchar(
			'nomenclature_activite_principale_unite_legale',
			{ length: 8 }
		),
		nomUniteLegale: varchar('nom_unite_legale'),
		nomUsageUniteLegale: varchar('nom_usage_unite_legale'),
		prenom1UniteLegale: varchar('prenom1_unite_legale'),
		prenom2UniteLegale: varchar('prenom2_unite_legale'),
		prenom3UniteLegale: varchar('prenom3_unite_legale'),
		prenom4UniteLegale: varchar('prenom4_unite_legale'),
		prenomUsuelUniteLegale: varchar('prenom_usuel_unite_legale', {
			length: 20,
		}),
		pseudonymeUniteLegale: varchar('pseudonyme_unite_legale'),
		sexeUniteLegale: inseeSireneUniteLegaleSexeEnum('sexe_unite_legale'),
		sigleUniteLegale: varchar('sigle_unite_legale'),
		societeMissionUniteLegale: inseeSireneUniteLegaleSocieteMissionEnum(
			'societe_mission_unite_legale'
		),
		statutDiffusionUniteLegale: inseeSireneUniteLegaleStatutDiffusionEnum(
			'statut_diffusion_unite_legale'
		).notNull(),
		trancheEffectifsUniteLegale: varchar('tranche_effectifs_unite_legale', {
			length: 2,
		}),
		unitePurgeeUniteLegale: boolean('unite_purgee_unite_legale'),
		//
		miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' }),
	},
	(table) => ({
		pkInseeSireneApiUniteLegale: primaryKey({
			name: 'pk_insee_sirene_api__unite_legale',
			columns: [table.siren],
		}),
		idxInseeSireneApiUniteLegaleMiseAjourAt: index(
			'idx_insee_sirene_api__unite_legale__mise_a_jour_at'
		).on(table.miseAJourAt),
	})
);

export const entrepriseRelations = relations(entreprise, ({ many, one }) => ({
	siege: one(etablissement, {
		fields: [entreprise.siren, entreprise.nicSiegeUniteLegale],
		references: [etablissement.siren, etablissement.nic],
	}),
	etabs: many(etablissement),
	etabCreas: many(etabCrea),
	catJur: one(categorieJuridiqueType, {
		fields: [entreprise.categorieJuridiqueUniteLegale],
		references: [categorieJuridiqueType.id],
	}),
	mandataires: one(apiEntrepriseEntrepriseMandataireSocial),
	benef: one(apiEntrepriseEntrepriseBeneficiaireEffectif),
	exercices: one(apiEntrepriseEntrepriseExercice),
	liasse: one(apiEntrepriseEntrepriseLiasseFiscale),
	prds: many(entreprisePeriode),
	fermeture: one(entreprisePeriodeFermetureVue, {
		fields: [entreprise.siren],
		references: [entreprisePeriodeFermetureVue.entrepriseId],
	}),
	categorieType: one(entrepriseCategorieType, {
		fields: [entreprise.categorieEntreprise],
		references: [entrepriseCategorieType.id],
	}),
	ess: one(essFranceEntreprepriseEss),
	micro: one(inpiRneEntreprepriseMicro),
	ratiosFinanciers: many(inpiRatioFinancier),
	procedures: many(bodaccProcedureCollective),
	props: many(proprietairePersonneMorale),
	nafType: one(nafType, {
		fields: [
			entreprise.activitePrincipaleUniteLegale,
			entreprise.nomenclatureActivitePrincipaleUniteLegale,
		],
		references: [nafType.id, nafType.revisionTypeId],
	}),
	egapro: many(egaproEntreprise),
}));

export const entreprisePeriode = source.table(
	'insee_sirene_api__unite_legale_periode',
	{
		siren: varchar('siren').notNull(),
		activitePrincipaleUniteLegale: varchar('activite_principale_unite_legale', {
			length: 6,
		}),
		caractereEmployeurUniteLegale: varchar('caractere_employeur_unite_legale', {
			length: 6,
		}),
		categorieJuridiqueUniteLegale: varchar('categorie_juridique_unite_legale', { length: 4 }),
		dateDebut: date('date_debut', { mode: 'date' }),
		dateFin: date('date_fin', { mode: 'date' }),
		denominationUniteLegale: varchar('denomination_unite_legale', {
			length: 120,
		}),
		denominationUsuelle1UniteLegale: varchar('denomination_usuelle_1_unite_legale'),
		denominationUsuelle2UniteLegale: varchar('denomination_usuelle_2_unite_legale'),
		denominationUsuelle3UniteLegale: varchar('denomination_usuelle_3_unite_legale'),
		economieSocialeSolidaireUniteLegale: inseeSireneUniteLegaleEcoSocSolEnum(
			'economie_sociale_solidaire_unite_legale'
		),
		etatAdministratifUniteLegale: inseeSireneUniteLegaleEtatAdministratifEnum(
			'etat_administratif_unite_legale'
		),
		nicSiegeUniteLegale: varchar('nic_siege_unite_legale', { length: 5 }),
		nomUniteLegale: varchar('nom_unite_legale'),
		nomUsageUniteLegale: varchar('nom_usage_unite_legale'),
		nomenclatureActivitePrincipaleUniteLegale: varchar(
			'nomenclature_activite_principale_unite_legale',
			{ length: 10 }
		),
		societeMissionUniteLegale: inseeSireneUniteLegaleSocieteMissionEnum(
			'societe_mission_unite_legale'
		),
		changementActivitePrincipaleUniteLegale: boolean('changement_activite_principale_unite_legale'),
		changementCaractereEmployeurUniteLegale: boolean('changement_caractere_employeur_unite_legale'),
		changementCategorieJuridiqueUniteLegale: boolean('changement_categorie_juridique_unite_legale'),
		changementDenominationUniteLegale: boolean('changement_denomination_unite_legale'),
		changementDenominationUsuelleUniteLegale: boolean(
			'changement_denomination_usuelle_unite_legale'
		),
		changementEconomieSocialeSolidaireUniteLegale: boolean(
			'changement_economie_sociale_solidaire_unite_legale'
		),
		changementSocieteMissionUniteLegale: boolean('changement_societe_mission_unite_legale'),
		changementEtatAdministratifUniteLegale: boolean('changement_etat_administratif_unite_legale'),
		changementNicSiegeUniteLegale: boolean('changement_nic_siege_unite_legale'),
		changementNomUniteLegale: boolean('changement_nom_unite_legale'),
		changementNomUsageUniteLegale: boolean('changement_nom_usage_unite_legale'),
		//
		miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' }),
	},
	(table) => ({
		ukInseeSireneApiUniteLegalePeriode: unique('uk_insee_sirene_api__unite_legale_periode').on(
			table.siren,
			table.dateDebut
		),
	})
);

export const entreprisePeriodeRelations = relations(entreprisePeriode, ({ one }) => ({
	ul: one(entreprise, {
		fields: [entreprisePeriode.siren],
		references: [entreprise.siren],
	}),
	catJur: one(categorieJuridiqueType, {
		fields: [entreprisePeriode.categorieJuridiqueUniteLegale],
		references: [categorieJuridiqueType.id],
	}),
	nafType: one(nafType, {
		fields: [
			entreprisePeriode.activitePrincipaleUniteLegale,
			entreprisePeriode.nomenclatureActivitePrincipaleUniteLegale,
		],
		references: [nafType.id, nafType.revisionTypeId],
	}),
}));

export const entrepriseDoublon = source.table(
	'insee_sirene_api__unite_legale_doublon',
	{
		siren: varchar('siren', { length: 9 }).notNull(),
		sirenDoublon: varchar('siren_doublon', { length: 9 }).notNull(),
		dateDernierTraitementDoublon: timestamp('date_dernier_traitement_doublon', {
			withTimezone: true,
			mode: 'date',
		}),
		//
		miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' }),
	},
	(table) => ({
		pkInseeSireneApiUniteLegaleDoublon: primaryKey({
			name: 'pk_insee_sirene_api__unite_legale_doublon',
			columns: [table.siren, table.sirenDoublon],
		}),
		idxInseeSireneApiUniteLegaleDoubloMiseAjourAt: index(
			'idx_insee_sirene_api__unite_legale_doublon__mise_a_jour_at'
		).on(table.miseAJourAt),
	})
);

export const inseeSireneEtablissementCaractereEmployeurEnum = pgEnum(
	'insee_sirene__etablissement__caractere_employeur_enum',
	['N', 'O']
);
export const inseeSireneEtablissementEtatAdministratifEnum = pgEnum(
	'insee_sirene__etablissement__etat_administratif_enum',
	['F', 'A']
);
export const inseeSireneEtablissementStatutDiffusionEnum = pgEnum(
	'insee_sirene__etablissement__statut_diffusion_enum',
	['P', 'N', 'O']
);

export const etablissement = source.table(
	'insee_sirene_api__etablissement',
	{
		siret: varchar('siret', { length: 14 }).notNull(),
		siren: varchar('siren', { length: 9 }).notNull(),
		activitePrincipaleEtablissement: varchar('activite_principale_etablissement', { length: 6 }),
		activitePrincipaleRegistreMetiersEtablissement: varchar(
			'activite_principale_registre_metiers_etablissement',
			{ length: 6 }
		),
		anneeEffectifsEtablissement: varchar('annee_effectifs_etablissement', {
			length: 9,
		}),
		caractereEmployeurEtablissement: inseeSireneEtablissementCaractereEmployeurEnum(
			'caractere_employeur_etablissement'
		),
		codeCedexEtablissement: varchar('code_cedex_etablissement', { length: 9 }),
		codeCommuneEtablissement: varchar('code_commune_etablissement', {
			length: 5,
		}),
		codePaysEtrangerEtablissement: varchar('code_pays_etranger_etablissement', {
			length: 5,
		}),
		codePostalEtablissement: varchar('code_postal_etablissement'),
		complementAdresseEtablissement: varchar('complement_adresse_etablissement'),
		dateCreationEtablissement: date('date_creation_etablissement', { mode: 'date' }),
		dateDebut: date('date_debut', { mode: 'date' }),
		dateDernierTraitementEtablissement: timestamp('date_dernier_traitement_etablissement', {
			withTimezone: true,
			mode: 'date',
		}),
		denominationUsuelleEtablissement: varchar('denomination_usuelle_etablissement'),
		dernierNumeroVoieEtablissement: varchar('dernier_numero_voie_etablissement', { length: 9 }),
		distributionSpecialeEtablissement: varchar('distribution_speciale_etablissement'),
		enseigne1Etablissement: varchar('enseigne1_etablissement'),
		enseigne2Etablissement: varchar('enseigne2_etablissement'),
		enseigne3Etablissement: varchar('enseigne3_etablissement'),
		etablissementSiege: boolean('etablissement_siege').notNull(),
		etatAdministratifEtablissement: inseeSireneEtablissementEtatAdministratifEnum(
			'etat_administratif_etablissement'
		),
		indiceRepetitionEtablissement: varchar('indice_repetition_etablissement', {
			length: 10,
		}),
		indiceRepetitionDernierNumeroVoieEtablissement: varchar(
			'indice_repetition_dernier_numero_voie_etablissement',
			{ length: 4 }
		),
		libelleCedexEtablissement: varchar('libelle_cedex_etablissement'),
		libelleCommuneEtablissement: varchar('libelle_commune_etablissement'),
		libelleCommuneEtrangerEtablissement: varchar('libelle_commune_etranger_etablissement'),
		libellePaysEtrangerEtablissement: varchar('libelle_pays_etranger_etablissement'),
		libelleVoieEtablissement: varchar('libelle_voie_etablissement'),
		nic: varchar('nic', { length: 5 }).notNull(),
		nombrePeriodesEtablissement: integer('nombre_periodes_etablissement'),
		nomenclatureActivitePrincipaleEtablissement: varchar(
			'nomenclature_activite_principale_etablissement',
			{ length: 8 }
		),
		numeroVoieEtablissement: varchar('numero_voie_etablissement'),
		statutDiffusionEtablissement: inseeSireneEtablissementStatutDiffusionEnum(
			'statut_diffusion_etablissement'
		).notNull(),
		trancheEffectifsEtablissement: varchar('tranche_effectifs_etablissement', {
			length: 2,
		}),
		typeVoieEtablissement: varchar('type_voie_etablissement'),
		// geoloc
		identifiantAdresseEtablissement: varchar('identifiant_adresse_etablissement', { length: 15 }),
		coordonneeLambertAbscisseEtablissement: varchar('coordonnee_lambert_abscisse_etablissement', {
			length: 18,
		}),
		coordonneeLambertOrdonneeEtablissement: varchar('coordonnee_lambert_ordonnee_etablissement', {
			length: 18,
		}),
		// TODO : https://orm.drizzle.team/docs/generated-columns
		// à décommenter quand les colonnes générées de type "virtual" seront prêtes dans Drizzle
		// geolocalisation: point('geolocalisation').generatedAlwaysAs(
		// 			(): SQL => sql`
		// 				ST_Transform (
		// 					ST_SetSRID (
		// 						ST_MakePoint(${etablissement.coordonneeLambertAbscisseEtablissement}::float, ${etablissement.coordonneeLambertOrdonneeEtablissement}::float),
		// 						CASE
		// 								WHEN LEFT (${etablissement.codeCommuneEtablissement}, 3) in ('971', '972') THEN 5490
		// 								WHEN LEFT (${etablissement.codeCommuneEtablissement}, 3) = '973' THEN 2972
		// 								WHEN LEFT (${etablissement.codeCommuneEtablissement}, 3) = '974' THEN 2975
		// 								WHEN LEFT (${etablissement.codeCommuneEtablissement}, 3) = '976' THEN 4471
		// 								ELSE 2154
		// 						END
		// 					),
		// 					4326
		// )`,
		// 			{ mode: 'virtual' }
		// 		)
		//		,
		//
		miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' }),
	},
	(table) => ({
		pkInseeSireneApiEtablissement: primaryKey({
			name: 'pk_insee_sirene_api__etablissement',
			columns: [table.siret],
		}),
		idxInseeSireneApiEtablissementSiren: index('idx_insee_sirene_api__etablissement__siren').on(
			table.siren
		),
		idxInseeSireneApiEtablissementDateDernierTraitementEtablissement: index(
			'idx_insee_sirene_api__etablissement__date_dernier_traitement_etablissement'
		).on(table.dateDernierTraitementEtablissement),
		idxInseeSireneApiEtablissementMiseAjourAt: index(
			'idx_insee_sirene_api__etablissement__mise_a_jour_at'
		).on(table.miseAJourAt),
	})
);

export const etablissementRelations = relations(etablissement, ({ one, many }) => ({
	ul: one(entreprise, {
		fields: [etablissement.siren],
		references: [entreprise.siren],
	}),
	commune: one(commune, {
		fields: [etablissement.codeCommuneEtablissement],
		references: [commune.id],
	}),
	geoloc: one(etablissementGeolocalisationVue),
	ban: one(etalabEtablissementAdresse),

	effectifType: one(effectifType, {
		fields: [etablissement.trancheEffectifsEtablissement],
		references: [effectifType.id],
	}),
	nafType: one(nafType, {
		fields: [
			etablissement.activitePrincipaleEtablissement,
			etablissement.nomenclatureActivitePrincipaleEtablissement,
		],
		references: [nafType.id, nafType.revisionTypeId],
	}),

	prds: many(etablissementPeriode),
	fermeture: one(etablissementPeriodeFermetureVue, {
		fields: [etablissement.siret],
		references: [etablissementPeriodeFermetureVue.etablissementId],
	}),

	effectifMoyenMensuels: many(rcdEtablissementEffectifEmm13mois),
	effectifMoyenAnnuel: one(rcdEtablissementEffectifEmm13moisAnnuel),
	acv: one(communeAcv, {
		fields: [etablissement.codeCommuneEtablissement],
		references: [communeAcv.communeId],
	}),
	pvd: one(communePvd, {
		fields: [etablissement.codeCommuneEtablissement],
		references: [communePvd.communeId],
	}),
	va: one(communeVa, {
		fields: [etablissement.codeCommuneEtablissement],
		references: [communeVa.communeId],
	}),
	afr: one(communeAfr, {
		fields: [etablissement.codeCommuneEtablissement],
		references: [communeAfr.communeId],
	}),

	ti: one(communeTerritoireIndustrie, {
		fields: [etablissement.codeCommuneEtablissement],
		references: [communeTerritoireIndustrie.communeId],
	}),
	// attention, pour récupérer les prédécesseurs
	// on prend tous les liens pour lesquels l'établissement appartient à la relation "successeur"
	// c'est-à-dire tous les prédécesseurs dans les tuples où l'établissement est le successeur
	preds: many(lienSuccession, {
		relationName: 'successeur',
	}),
	// attention, pour récupérer les successeurs
	// on prend tous les liens pour lesquels l'établissement appartient à la relation "prédécesseur"
	// c'est-à-dire tous les successeurs dans les tuples où l'établissement est le prédécesseur
	succs: many(lienSuccession, {
		relationName: 'predecesseur',
	}),
	subventions: many(dgclSubvention),

	qpv2024: one(etablissementQpv2024Vue),
	qpv2015: one(etablissementQpv2015Vue),

	// si la commune est dans l'équipe ou non
	equipeCommune: one(equipeCommuneVue, {
		fields: [etablissement.codeCommuneEtablissement],
		references: [equipeCommuneVue.communeId],
	}),
	// données perso
	favoris: many(devecoEtablissementFavori),
	contacts: many(eqEtabContact),
	contributions: many(eqEtabContribution),
	contributionsGeolocs: many(eqEtabContribution),
	etiquettes: many(eqEtabEtiquette),
	demandes: many(eqEtabDemande),
	echanges: many(eqEtabEchange),
	rappels: many(eqEtabRappel),
	transfos: many(etabCreaTransformation),
	ressources: many(eqEtabRessource),
	occups: many(eqLocOccupant),
}));

export const etablissementPeriode = source.table(
	'insee_sirene_api__etablissement_periode',
	{
		siret: varchar('siret', { length: 14 }).notNull(),
		activitePrincipaleEtablissement: varchar('activite_principale_etablissement', { length: 6 }),
		caractereEmployeurEtablissement: inseeSireneEtablissementCaractereEmployeurEnum(
			'caractere_employeur_etablissement'
		),
		dateDebut: date('date_debut', { mode: 'date' }),
		dateFin: date('date_fin', { mode: 'date' }),
		denominationUsuelleEtablissement: varchar('denomination_usuelle_etablissement'),
		enseigne1Etablissement: varchar('enseigne1_etablissement'),
		enseigne2Etablissement: varchar('enseigne2_etablissement'),
		enseigne3Etablissement: varchar('enseigne3_etablissement'),
		etatAdministratifEtablissement: inseeSireneEtablissementEtatAdministratifEnum(
			'etat_administratif_etablissement'
		),
		nomenclatureActivitePrincipaleEtablissement: varchar(
			'nomenclature_activite_principale_etablissement',
			{ length: 8 }
		),

		changementActivitePrincipaleEtablissement: boolean(
			'changement_activite_principale_etablissement'
		),
		changementCaractereEmployeurEtablissement: boolean(
			'changement_caractere_employeur_etablissement'
		),
		changementDenominationUsuelleEtablissement: boolean(
			'changement_denomination_usuelle_etablissement'
		),
		changementEnseigneEtablissement: boolean('changement_enseigne_etablissement'),
		changementEtatAdministratifEtablissement: boolean(
			'changement_etat_administratif_etablissement'
		),
		//
		miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' }),
	},
	(table) => ({
		ukInseeSireneApiEtablissementPeriode: unique('uk_insee_sirene_api__etablissement_periode').on(
			table.siret,
			table.dateDebut
		),
		idxInseeSireneApiEtablissementPeriodeMiseAJourAt: index(
			'idx_insee_sirene_api__etablissement_periode__mise_a_jour_at'
		).on(table.miseAJourAt),
	})
);

export const etablissementPeriodeRelations = relations(etablissementPeriode, ({ one }) => ({
	etab: one(etablissement, {
		fields: [etablissementPeriode.siret],
		references: [etablissement.siret],
	}),
	nafType: one(nafType, {
		fields: [
			etablissementPeriode.activitePrincipaleEtablissement,
			etablissementPeriode.nomenclatureActivitePrincipaleEtablissement,
		],
		references: [nafType.id, nafType.revisionTypeId],
	}),
}));

export const etablissementGeoloc = source.table(
	'insee_sirene__etablissement_geoloc',
	{
		siret: varchar('siret').notNull(),
		x: doublePrecision('x').notNull(),
		y: doublePrecision('y').notNull(),
		qualiteXy: varchar('qualite_xy').notNull(),
		epsg: varchar('epsg').notNull(),
		plgQp24: varchar('plg_qp24'),
		plgIris: varchar('plg_iris'),
		plgZus: varchar('plg_zus'),
		plgQp15: varchar('plg_qp15'),
		plgQva: varchar('plg_qva'),
		plgCodeCommune: varchar('plg_code_commune').notNull(),
		distancePrecision: varchar('distance_precision'),
		qualiteQp24: varchar('qualite_qp24'),
		qualiteIris: varchar('qualite_iris'),
		qualiteZus: varchar('qualite_zus'),
		qualiteQp15: varchar('qualite_qp15'),
		qualiteQva: varchar('qualite_qva'),
		xLongitude: doublePrecision('x_longitude').notNull(),
		yLatitude: doublePrecision('y_latitude').notNull(),
	},
	(table) => ({
		pkInseeSireneEtablissementGeoloc: primaryKey({
			name: 'pk_insee_sirene__etablissement_geoloc',
			columns: [table.siret],
		}),
	})
);

export const etablissementGeolocRelations = relations(etablissementGeoloc, ({ one }) => ({
	etab: one(etablissement, {
		fields: [etablissementGeoloc.siret],
		references: [etablissement.siret],
	}),
}));

export const lienSuccession = source.table(
	'insee_sirene_api__lien_succession',
	{
		continuiteEconomique: boolean('continuite_economique').notNull(),
		dateLienSuccession: timestamp('date_lien_succession', {
			withTimezone: true,
			mode: 'date',
		}).notNull(),
		dateDernierTraitementLienSuccession: timestamp('date_dernier_traitement_lien_succession', {
			withTimezone: true,
			mode: 'date',
		}).notNull(),
		siretEtablissementPredecesseur: varchar('siret_etablissement_predecesseur', {
			length: 14,
		}).notNull(),
		siretEtablissementSuccesseur: varchar('siret_etablissement_successeur', {
			length: 14,
		}).notNull(),
		transfertSiege: boolean('transfert_siege').notNull(),
		//
		miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' }),
	},
	(table) => ({
		ukTache: unique('uk_insee_sirene_api__lien_succession').on(
			table.siretEtablissementPredecesseur,
			table.siretEtablissementSuccesseur,
			table.dateLienSuccession
		),
		idxInseeSireneApiLienSuccessionSiretEtablissementPredecesseur: index(
			'idx_insee_sirene__api_lien_succession__siret_etablissement_predecesseur'
		).on(table.siretEtablissementPredecesseur),
		idxInseeSireneApiLienSuccessionSiretEtablissementSuccesseur: index(
			'idx_insee_sirene__api_lien_succession__siret_etablissement_successeur'
		).on(table.siretEtablissementSuccesseur),
	})
);

export const lienSuccessionRelations = relations(lienSuccession, ({ one }) => ({
	pred: one(etablissement, {
		fields: [lienSuccession.siretEtablissementPredecesseur],
		references: [etablissement.siret],
		relationName: 'predecesseur',
	}),
	succ: one(etablissement, {
		fields: [lienSuccession.siretEtablissementSuccesseur],
		references: [etablissement.siret],
		relationName: 'successeur',
	}),
}));

//

// BODACC

export const bodaccProcedureCollectiveImport = source.table(
	'bodacc__procedure_collective_import',
	{
		annee: integer('annee').notNull(),
		numero: integer('numero').notNull(),
		miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' }).defaultNow(),
	},
	(table) => ({
		ukBodaccProcedureCollectiveImport: unique('uk_bodacc__procedure_collective_import').on(
			table.annee,
			table.numero
		),
	})
);

export const bodaccProcedureCollective = source.table(
	'bodacc__procedure_collective',
	{
		numero: integer('numero').notNull(),
		siren: varchar('siren', { length: 9 }).notNull(),
		famille: varchar('famille'),
		nature: varchar('nature'),
		date: date('date', { mode: 'date' }),
		miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' }).defaultNow(),
	},
	(table) => ({
		idxBodaccProcedureCollective: index('idx_bodacc__procedure_collective__entreprise').on(
			table.siren
		),
	})
);

export const bodaccProcedureCollectiveRelations = relations(
	bodaccProcedureCollective,
	({ one }) => ({
		ul: one(entreprise, {
			fields: [bodaccProcedureCollective.siren],
			references: [entreprise.siren],
		}),
	})
);

// etablissement

export const etablissementQpv2024Vue = pgTable(
	'etablissement__qpv_2024_vue',
	{
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
		qpv2024Id: varchar('qpv_2024_id', { length: 8 }).notNull(),
	}
	/*
	 (table) => {
			return {
				ukEtablissementQpv2024Vue: unique('uk_etablissement_qpv_2024_vue').on(
					table.etablissementId
				),
				idxEtablissementQpv2024VueEtablissement: index(
					'idx_etablissement_qpv_2024_vue__etablissement'
				).on(table.etablissementId),
				idxEtablissementQpv2024VueQpv2024: index(
					'idx_etablissement_qpv_2024_vue__qpv_2024'
				).on(table.qpv2024Id),
			};
	 }
	*/
);
/*
 pgView('etablissement_qpv_2024_vue').as((db) => {
		return db
			.select({
				etablissementId: etablissementGeolocalisationVue.etablissementId,
				qpv2024Id: etablissementGeolocalisationVue.debutDate,
			})
			.from(etablissementGeolocalisationVue)
			.innerJoin(qpv2024, sql`ST_Covers(${qpv2024.geometrie}, ${etablissementGeolocalisationVue.geolocalisation})`
 });
 CREATE VIEW public.etablissement__qpv_2024_vue AS (
   SELECT
	 siret AS etablissement_id,
	 qpv_2024.id AS qpv_2024_id
   FROM etablissement_geolocalisation_vue
   JOIN qpv_2024 ON ST_Covers(geometrie, geolocalisation)
 );
*/

export const etablissementQpv2024Relations = relations(etablissementQpv2024Vue, ({ one }) => ({
	etab: one(etablissement, {
		fields: [etablissementQpv2024Vue.etablissementId],
		references: [etablissement.siret],
	}),
	qpv: one(qpv2024, {
		fields: [etablissementQpv2024Vue.qpv2024Id],
		references: [qpv2024.id],
	}),
}));

export const etablissementQpv2015Vue = pgTable(
	'etablissement__qpv_2015_vue',
	{
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
		qpv2015Id: varchar('qpv_2015_id', { length: 8 }).notNull(),
	}
	/*
	 (table) => {
			return {
				ukEtablissementQpv2015Vue: unique('uk_etablissement_qpv_2015_vue').on(
					table.etablissementId
				),
				idxEtablissementQpv2015VueEtablissement: index(
					'idx_etablissement_qpv_2015_vue__etablissement'
				).on(table.etablissementId),
				idxEtablissementQpv2015VueQpv2015: index(
					'idx_etablissement_qpv_2015_vue__qpv_2015'
				).on(table.qpv2015Id),
			};
	 }
	*/
);
/*
 pgView('etablissement_qpv_2015_vue').as((db) => {
		return db
			.select({
				etablissementId: etablissementGeolocalisationVue.etablissementId,
				qpv2015Id: etablissementGeolocalisationVue.debutDate,
			})
			.from(etablissementGeolocalisationVue)
			.innerJoin(qpv2015, sql`ST_Covers(${qpv2015.geometrie}, ${etablissementGeolocalisationVue.geolocalisation})`
 });
 CREATE VIEW public.etablissement__qpv_2015_vue AS (
   SELECT
	 siret AS etablissement_id,
	 qpv_2015.id AS qpv_2015_id
   FROM etablissement_geolocalisation_vue
   JOIN qpv_2015 ON ST_Covers(geometrie, geolocalisation)
 );
*/

export const etablissementQpv2015Relations = relations(etablissementQpv2015Vue, ({ one }) => ({
	etab: one(etablissement, {
		fields: [etablissementQpv2015Vue.etablissementId],
		references: [etablissement.siret],
	}),
	qpv: one(qpv2015, {
		fields: [etablissementQpv2015Vue.qpv2015Id],
		references: [qpv2015.id],
	}),
}));

export const etablissementPeriodeFermetureVue = pgTable(
	'etablissement_periode_fermeture_vue',
	{
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
		fermetureDate: date('fermeture_date', { mode: 'date' }),
	}
	/*
	 (table) => {
			return {
				ukEtablissementPeriodeFermetureVue: unique('uk_etablissement_periode_fermeture_vue').on(
					table.etablissementId
				),
				idxEtablissementPeriodeFermetureVueEtablissement: index(
					'idx_etablissement_periode_fermeture_vue__etablissement'
				).on(table.etablissementId),
				idxEtablissementPeriodeFermetureVueFermetureDate: index(
					'idx_etablissement_periode_fermeture_vue__fermeture_date'
				).on(table.fermetureDate),
			};
	 }
	*/
);
/*
 pgView('etablissement_periode_fermeture_vue').as((db) => {
		const etablissementPeriodeWithActifChangeQuery = db
			.selectDistinctOn([etablissementPeriode.siret], {
				etablissementId: etablissementPeriode.siret.as('etablissementId'),
				debutDate: etablissementPeriode.dateDebut.as('debutDate'),
				finDate: etablissementPeriode.dateFin.as('finDate'),
				actif: etablissementPeriode.changementEtatAdministratifEtablissement.as('actif'),
			})
			.from(etablissementPeriode)
			.where(eq(etablissementPeriode.changementEtatAdministratifEtablissement, true))
			.orderBy(etablissementPeriode.etablissementId, desc(etablissementPeriode.debutDate))
			.as('changeQuery');
		return db
			.select({
				etablissementId: etablissementPeriodeWithActifChangeQuery.etablissementId,
				fermetureDate: etablissementPeriodeWithActifChangeQuery.debutDate,
			})
			.from(etablissementPeriodeWithActifChangeQuery)
			.where(
				and(
					not(etablissementPeriodeWithActifChangeQuery.actif),
					isNotNull(etablissementPeriodeWithActifChangeQuery.finDate)
				)
			);
 })
 CREATE VIEW public.etablissement_periode_fermeture_vue AS (
   SELECT
	 etablissement_id,
	 debut_date AS fermeture_date
   FROM
	 (
	   SELECT
		 DISTINCT ON (siret)
		 siret as etablissement_id,
		 date_debut as debut_date,
		 etat_administratif_etablissement = 'A' as statut
	   FROM
		 source.insee_sirene_api__etablissement_periode
	   WHERE
		 changement_etat_administratif_etablissement
	   ORDER BY
		 siret,
		 date_debut DESC
	 ) t
   WHERE
	 (NOT statut)
 );
*/

export const entreprisePeriodeFermetureVue = pgTable(
	'entreprise_periode_fermeture_vue',
	{
		entrepriseId: varchar('entreprise_id', { length: 9 }).notNull(),
		fermetureDate: date('fermeture_date', { mode: 'date' }),
	}
	/*
	 (table) => {
			return {
				ukEntreprisePeriodeFermetureVue: unique('uk_entreprise_periode_fermeture_vue').on(
					table.entrepriseId
				),
				idxEntreprisePeriodeFermetureVueEntreprise: index(
					'idx_entreprise_periode_fermeture_vue__entreprise'
				).on(table.entrepriseId),
				idxEntreprisePeriodeFermetureVueFermetureDate: index(
					'idx_entreprise_periode_fermeture_vue__fermeture_date'
				).on(table.fermetureDate),
			};
	 }
	*/
);
/*
	pgView('entreprise_periode_fermeture_vue').as((db) => {
		const entreprisePeriodeWithActifChangeQuery = db
			.selectDistinctOn([entreprisePeriode.siren], {
				entrepriseId: entreprisePeriode.siren.as('entrepriseId'),
				debutDate: entreprisePeriode.dateDebut.as('debutDate'),
				finDate: entreprisePeriode.dateFin.as('finDate'),
				actif: entreprisePeriode.changementEtatAdministratifUniteLegale.as('actif'),
			})
			.from(entreprisePeriode)
			.where(eq(entreprisePeriode.changementEtatAdministratifUniteLegale, true))
			.orderBy(entreprisePeriode.entrepriseId, desc(entreprisePeriode.debutDate))
			.as('changeQuery');
		return db
			.select({
				entrepriseId: entreprisePeriodeWithActifChangeQuery.entrepriseId,
				fermetureDate: entreprisePeriodeWithActifChangeQuery.debutDate,
			})
			.from(entreprisePeriodeWithActifChangeQuery)
			.where(
				and(
					not(entreprisePeriodeWithActifChangeQuery.actif),
					isNotNull(entreprisePeriodeWithActifChangeQuery.finDate)
				)
			);
 })
 CREATE VIEW public.entreprise_periode_fermeture_vue AS (
   SELECT
	 entreprise_id,
	 debut_date AS fermeture_date
   FROM
	 (
	   SELECT
		 DISTINCT ON (siren)
		 siren as entreprise_id,
		 date_debut as debut_date,
		 etat_administratif_unite_legale = 'A' as statut
	   FROM
		 source.insee_sirene_api__unite_legale_periode
	   WHERE
		 changement_etat_administratif_unite_legale
	   ORDER BY
		 siren,
		 date_debut DESC
	 ) t
   WHERE
	 (NOT statut)
 );
*/

const { id, ...adresseColumnsWithoutId } = adresseColumns;

const etablissementAdresseColumns = {
	siret: varchar('siret', { length: 14 }).notNull(),
	communeId: varchar('commune_id', { length: 5 }),
	...adresseColumnsWithoutId,
	complementAdresse: varchar('complement_adresse'),
	etrangerCommune: varchar('etranger_commune'),
	etrangerPays: varchar('etranger_pays'),
};

// table uniquement utilisée pour la BANification
// historiquement remplie avec le fichier ETALAB automatisé
// mais présentant trop d'incohérences dans les données
export const etalabEtablissementAdresse = source.table(
	'etalab__etablissement_adresse',
	{
		siret: etablissementAdresseColumns.siret,
		communeId: etablissementAdresseColumns.communeId,
		numero: etablissementAdresseColumns.numero,
		voieNom: etablissementAdresseColumns.voieNom,
		codePostal: etablissementAdresseColumns.codePostal,
		geolocalisation: etablissementAdresseColumns.geolocalisation,
		score: doublePrecision('score').notNull(),
		miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' }).defaultNow(),
	},
	(table) => ({
		pkEtalabEtablisementAdresse: primaryKey({
			name: 'pk_etalab__etablissement_adresse',
			columns: [table.siret],
		}),
		fkEtalabEtablisementAdresseAdresseCommune: foreignKey({
			name: 'fk_etalab__etablissement_adresse__commune',
			columns: [table.communeId],
			foreignColumns: [commune.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),

		idxEtalabEtablisementAdresseCommune: index('idx_etalab__etablissement_adresse__commune').on(
			table.communeId
		),
	})
);

export const etalabEtablissementAdresseRelations = relations(
	etalabEtablissementAdresse,
	({ one }) => ({
		etab: one(etablissement, {
			fields: [etalabEtablissementAdresse.siret],
			references: [etablissement.siret],
		}),
		commune: one(commune, {
			fields: [etalabEtablissementAdresse.communeId],
			references: [commune.id],
		}),
	})
);
