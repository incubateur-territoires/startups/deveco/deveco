import {
	pgSchema,
	varchar,
	date,
	boolean,
	integer,
	doublePrecision,
	serial,
	index,
	primaryKey,
} from 'drizzle-orm/pg-core';

import { multiPolygon } from './custom-types/postgis';

export const source = pgSchema('source');

export const anctQpv2015 = source.table(
	'anct__qpv_2015',
	{
		gid: varchar('gid').notNull(),
		codeQp: varchar('code_qp', { length: 8 }),
		nomQp: varchar('nom_qp'),
		communeQp: varchar('commune_qp'),
		geom: multiPolygon('geom', { epsg: '4326' }),
	},
	(table) => ({
		pkAnctQpv2015: primaryKey({ name: 'pk_anct__qpv_2015', columns: [table.gid] }),
	})
);

export const anctQpv2024 = source.table(
	'anct__qpv_2024',
	{
		gid: varchar('gid').notNull(),
		fid: serial('fid').notNull(),
		codeQp: varchar('code_qp', { length: 8 }),
		libQp: varchar('lib_qp'),
		commune: varchar('commune'),
		geom: multiPolygon('geom', { epsg: '4326' }),
	},
	(table) => ({
		pkAnctQpv2024: primaryKey({ name: 'pk_anct__qpv_2024', columns: [table.gid] }),
	})
);

export const anctCommuneQpv2015 = source.table(
	'anct__commune__qpv_2015',
	{
		communeId: varchar('commune_id', { length: 5 }).notNull(),
		qpv2015Id: varchar('qpv_2015_id', { length: 8 }),
	},
	(table) => ({
		pkAnctQpv2015: primaryKey({
			name: 'pk_anct__commune__qpv_2015',
			columns: [table.communeId, table.qpv2015Id],
		}),
	})
);

export const anctCommuneQpv2024 = source.table(
	'anct__commune__qpv_2024',
	{
		communeId: varchar('commune_id', { length: 5 }).notNull(),
		qpv2024Id: varchar('qpv_2024_id', { length: 8 }),
	},
	(table) => ({
		pkAnctQpv2015: primaryKey({
			name: 'pk_anct__commune__qpv_2024',
			columns: [table.communeId, table.qpv2024Id],
		}),
	})
);

export const datagouvCommuneEpt = source.table(
	'datagouv__commune_ept',
	{
		inseeCom: varchar('insee_com', { length: 5 }).notNull(),
		inseeDep: varchar('insee_dep', { length: 2 }).notNull(),
		inseeEpt: varchar('insee_ept', { length: 3 }).notNull(),
		inseeReg: varchar('insee_reg', { length: 2 }).notNull(),
		libCom: varchar('lib_com').notNull(),
		libEpt: varchar('lib_ept').notNull(),
		siren: varchar('siren', { length: 9 }).notNull(),
	},
	(table) => ({
		pkDatagouvCommuneEpt: primaryKey({
			name: 'pk_datagouv__commune_ept',
			columns: [table.inseeCom],
		}),
	})
);

export const inseeRegion = source.table(
	'insee__region',
	{
		cheflieu: varchar('cheflieu', { length: 5 }).notNull(),
		libelle: varchar('libelle').notNull(),
		ncc: varchar('ncc').notNull(),
		nccenr: varchar('nccenr').notNull(),
		reg: varchar('reg', { length: 2 }).notNull(),
		tncc: varchar('tncc', { length: 1 }).notNull(),
	},
	(table) => ({
		pkInseeRegion: primaryKey({ name: 'pk_insee__region', columns: [table.reg] }),
	})
);

export const anctCommuneZoneRevitalisationRurale = source.table(
	'anct__commune_zone_revitalisation_rurale',
	{
		inseeCom: varchar('insee_com', { length: 5 }).notNull(),
		zrr: varchar('zrr', { length: 1 }).notNull(),
	},
	(table) => ({
		pkAnctCommuneZoneRevitalisationRurale: primaryKey({
			name: 'pk_anct__zone_revitalisation_rurale',
			columns: [table.inseeCom, table.zrr],
		}),
	})
);

export const inseeDepartement = source.table(
	'insee__departement',
	{
		cheflieu: varchar('cheflieu', { length: 5 }).notNull(),
		dep: varchar('dep', { length: 3 }).notNull(),
		libelle: varchar('libelle').notNull(),
		ncc: varchar('ncc').notNull(),
		nccenr: varchar('nccenr').notNull(),
		reg: varchar('reg', { length: 2 }).notNull(),
		tncc: varchar('tncc', { length: 1 }).notNull(),
	},
	(table) => ({
		pkInseeDepartement: primaryKey({
			name: 'pk_insee__departement',
			columns: [table.dep],
		}),
	})
);

export const inseeEpci = source.table(
	'insee__epci',
	{
		epci: varchar('epci', { length: 9 }).notNull(),
		libepci: varchar('libepci').notNull(),
		natureEpci: varchar('nature_epci').notNull(),
		nbCom: integer('nb_com').notNull(),
	},
	(table) => ({
		pkInseeEpci: primaryKey({
			name: 'pk_insee__epci',
			columns: [table.epci],
		}),
	})
);

export const inseeEpciCommune = source.table(
	'insee__epci_commune',
	{
		codgeo: varchar('codgeo', { length: 5 }).notNull(),
		dep: varchar('dep', { length: 3 }).notNull(),
		epci: varchar('epci', { length: 9 }).notNull(),
		libepci: varchar('libepci').notNull(),
		libgeo: varchar('libgeo').notNull(),
		reg: varchar('reg', { length: 2 }).notNull(),
	},
	(table) => ({
		pkInseeEpciCommune: primaryKey({
			name: 'pk_insee__epci_commune',
			columns: [table.codgeo],
		}),
	})
);

export const observatoireDesTerritoiresEpciPetr = source.table(
	'observatoire_des_territoires__epci_petr',
	{
		codgeo: varchar('codgeo', { length: 9 }).notNull(),
		libgeo: varchar('libgeo').notNull(),
		pays: varchar('pays', { length: 4 }),
		paysLibgeo: varchar('pays_libgeo'),
	},
	(table) => ({
		pkObservatoireDesTerritoiresEpciPetr: primaryKey({
			name: 'pk_observatoire_des_territoires__epci_petr',
			columns: [table.codgeo],
		}),
	})
);

export const inseeCommune = source.table('insee__commune', {
	arr: varchar('arr', { length: 4 }),
	can: varchar('can', { length: 5 }),
	com: varchar('com', { length: 5 }),
	comparent: varchar('comparent', { length: 5 }),
	ctcd: varchar('ctcd', { length: 4 }),
	dep: varchar('dep', { length: 3 }),
	libelle: varchar('libelle'),
	ncc: varchar('ncc'),
	nccenr: varchar('nccenr'),
	reg: varchar('reg', { length: 2 }),
	tncc: varchar('tncc', { length: 1 }),
	typecom: varchar('typecom', { length: 4 }),
});

export const inseeCommuneComer = source.table(
	'insee__commune_comer',
	{
		comComer: varchar('com_comer', { length: 5 }),
		tncc: varchar('tncc', { length: 1 }),
		ncc: varchar('ncc'),
		nccenr: varchar('nccenr'),
		libelle: varchar('libelle'),
		natureZonage: varchar('nature_zonage', { length: 3 }),
		comer: varchar('comer', { length: 3 }),
		libelle_comer: varchar('libelle_comer'),
	},
	(table) => ({
		idxInseeCommuneComerComComer: index('idx_insee__commune_comer__com_comer').on(table.comComer),
	})
);

export const inseeCommuneHistorique = source.table('insee__commune_historique', {
	com: varchar('com', { length: 5 }),
	dateDebut: date('date_debut', { mode: 'date' }),
	dateFin: date('date_fin', { mode: 'date' }),
	libelle: varchar('libelle'),
	ncc: varchar('ncc'),
	nccenr: varchar('nccenr'),
	tncc: varchar('tncc', { length: 1 }),
});

export const inseeCommuneMouvement = source.table('insee__commune_mouvement', {
	comAp: varchar('com_ap', { length: 5 }),
	comAv: varchar('com_av', { length: 5 }),
	dateEff: date('date_eff', { mode: 'date' }),
	indicOe: varchar('indic_oe'),
	indicOe2: varchar('indic_oe2'),
	libelleAp: varchar('libelle_ap'),
	libelleAv: varchar('libelle_av'),
	mod: varchar('mod'),
	nccAp: varchar('ncc_ap'),
	nccAv: varchar('ncc_av'),
	nccenrAp: varchar('nccenr_ap'),
	nccenrAv: varchar('nccenr_av'),
	tnccAp: varchar('tncc_ap', { length: 1 }),
	tnccAv: varchar('tncc_av', { length: 1 }),
	typecomAp: varchar('typecom_ap', { length: 4 }),
	typecomAv: varchar('typecom_av', { length: 4 }),
});

export const inseeQpvCommune = source.table('insee__qpv_commune', {
	qp: varchar('qp'),
	libQp: varchar('lib_qp'),
	listCom2023: varchar('list_com_2023'),
	listLibCom2023: varchar('list_lib_com_2023'),
	listComarr2023: varchar('list_comarr_2023'),
	listLibComarr2023: varchar('list_lib_comarr_2023'),
	zoneEpci20231: varchar('zone_epci_2023_1'),
	zoneEpci20232: varchar('zone_epci_2023_2'),
	listZoneEpci2023: varchar('list_zone_epci_2023'),
	listLibZoneEpci2023: varchar('list_lib_zone_epci_2023'),
	reg1: varchar('reg_1'),
	dep1: varchar('dep_1'),
	dep2: varchar('dep_2'),
	uu20201: varchar('uu2020_1'),
	fusionCom2223: varchar('fusion_com_2223'),
	listeComModif2223: varchar('liste_com_modif_2223'),
	fusionEpci2223: varchar('fusion_epci_2223'),
	listeEpciModif2223: varchar('liste_epci_modif_2223'),
});

export const laposteCodeInseeCodePostal = source.table('laposte__code_insee_code_postal', {
	codeInsee: varchar('code_insee', { length: 5 }),
	codePostal: varchar('code_postal', { length: 5 }),
	libelleAcheminement: varchar('libelle_acheminement'),
	ligne5: varchar('ligne_5'),
	nom: varchar('nom'),
});

export const anctCommuneActionCoeurDeVille = source.table(
	'anct__commune_action_coeur_de_ville',
	{
		communeId: varchar('commune_id', { length: 5 }).notNull(),
	},
	(table) => ({
		pkAnctCommuneActionCoeurDeVille: primaryKey({
			name: 'pk_anct__commune_action_coeur_de_ville',
			columns: [table.communeId],
		}),
	})
);

export const anctCommunePetitesVillesDeDemain = source.table(
	'anct__commune_petites_villes_de_demain',
	{
		communeId: varchar('commune_id', { length: 5 }).notNull(),
	},
	(table) => ({
		pkAnctCommunePetitesVillesDeDemain: primaryKey({
			name: 'pk_anct__commune_petites_villes_de_demain',
			columns: [table.communeId],
		}),
	})
);

export const anctCommuneVillageDAvenir = source.table(
	'anct__commune_village_d_avenir',
	{
		communeId: varchar('commune_id', { length: 5 }).notNull(),
	},
	(table) => ({
		pkAnctCommuneVillageDAvenir: primaryKey({
			name: 'pk_anct__commune_village_d_avenir',
			columns: [table.communeId],
		}),
	})
);

export const anctCommuneAideFinaliteRegionale = source.table(
	'anct__commune_aide_finalite_regionale',
	{
		communeId: varchar('commune_id', { length: 5 }).notNull(),
		classement: varchar('classement').notNull(),
	},
	(table) => ({
		pkAnctCommuneAideFinaliteRegionale: primaryKey({
			name: 'pk_anct__commune_aide_finalite_regionale',
			columns: [table.communeId],
		}),
	})
);

export const anctCommuneTerritoireDIndustrie = source.table(
	'anct__commune_territoire_d_industrie',
	{
		insee_com: varchar('insee_com', { length: 5 }).notNull(),
		lib_com: varchar('lib_com'),
		id_ti: varchar('id_ti', { length: 7 }),
		lib_ti: varchar('lib_ti'),
	},
	(table) => ({
		pkAnctCommuneTerritoireDIndustrie: primaryKey({
			name: 'pk_anct__commune_territoire_d_industrie',
			columns: [table.insee_com],
		}),
	})
);

export const etalabCommuneContour = source.table(
	'etalab__commune_contour',
	{
		ogcFid: serial('ogc_fid'),
		wkbGeometry: multiPolygon('wkb_geometry', { epsg: '4326' }),
		code: varchar('code'),
		nom: varchar('nom'),
		departement: varchar('departement'),
		region: varchar('region'),
		epci: varchar('epci'),
		plm: boolean('plm'),
		commune: varchar('commune'),
	},
	(table) => ({
		pkEtalabCommuneContour: primaryKey({
			name: 'pk_etalab__commune_contour',
			columns: [table.ogcFid],
		}),
		idxEtalabCommuneContourWkbGeometry: index('idx_etalab__commune_contour__wkb_geometry').on(
			table.wkbGeometry
		),
	})
);

export const etalabMairiePoint = source.table(
	'etalab__mairie_point',
	{
		ogcFid: serial('ogc_fid'),
		wkbGeometry: multiPolygon('wkb_geometry', { epsg: '4326' }),
		code: varchar('code'),
		nom: varchar('nom'),
		type: varchar('type'),
	},
	(table) => ({
		pkEtalabMairiePoint: primaryKey({
			name: 'pk_etalab__mairie_point',
			columns: [table.ogcFid],
		}),
		idxEtalabMairiePointWkbGeometry: index('idx_etalab__mairie_point__wkb_geometry').on(
			table.wkbGeometry
		),
	})
);

export const etalabEpciContour = source.table(
	'etalab__epci_contour',
	{
		ogcFid: serial('ogc_fid'),
		wkbGeometry: multiPolygon('wkb_geometry', { epsg: '4326' }),
		code: varchar('code'),
		nom: varchar('nom'),
	},
	(table) => ({
		pkEtalabEpciContour: primaryKey({
			name: 'pk_etalab__epci_contour',
			columns: [table.ogcFid],
		}),
		idxEtalabEpciContourWkbGeometry: index('idx_etalab__epci_contour__wkb_geometry').on(
			table.wkbGeometry
		),
	})
);

export const etalabDepartementContour = source.table(
	'etalab__departement_contour',
	{
		ogcFid: serial('ogc_fid'),
		wkbGeometry: multiPolygon('wkb_geometry', { epsg: '4326' }),
		code: varchar('code'),
		nom: varchar('nom'),
		region: varchar('region'),
	},
	(table) => ({
		pkEtalabDepartementContour: primaryKey({
			name: 'pk_etalab__departement_contour',
			columns: [table.ogcFid],
		}),
		idxEtalabDepartementContourWkbGeometry: index(
			'idx_etalab__departement_contour__wkb_geometry'
		).on(table.wkbGeometry),
	})
);

export const etalabRegionContour = source.table(
	'etalab__region_contour',
	{
		ogcFid: serial('ogc_fid'),
		wkbGeometry: multiPolygon('wkb_geometry', { epsg: '4326' }),
		code: varchar('code'),
		nom: varchar('nom'),
	},
	(table) => ({
		pkEtalabRegionContour: primaryKey({
			name: 'pk_etalab__region_contour',
			columns: [table.ogcFid],
		}),
		idxEtalabRegionContourWkbGeometry: index('idx_etalab__region_contour__wkb_geometry').on(
			table.wkbGeometry
		),
	})
);

export const ceremaLocal = source.table(
	'cerema__local',
	{
		invar: varchar('invar', { length: 10 }).notNull(),
		dnvoiri: varchar('dnvoiri', { length: 4 }),
		dindic: varchar('dindic', { length: 1 }),
		dvoilib: varchar('dvoilib', { length: 30 }),
		idcom: varchar('idcom', { length: 5 }).notNull(),
		ccosec: varchar('ccosec', { length: 2 }),
		dnupla: varchar('dnupla', { length: 4 }),
		cconlc: varchar('cconlc', { length: 2 }),
		typeact: varchar('typeact', { length: 4 }),
		dniv: varchar('dniv', { length: 2 }),
		sprincp: integer('sprincp'),
		ssecp: integer('ssecp'),
		ssecncp: integer('ssecncp'),
		sparkp: integer('sparkp'),
		sparkncp: integer('sparkncp'),
		idprocpte: varchar('idprocpte', { length: 11 }),
		geomloc: varchar('geomloc'),
		annee: integer('annee').notNull(),
	},
	(table) => ({
		pkCeremaLocal: primaryKey({
			name: 'pk_cerema__local',
			columns: [table.invar, table.idcom, table.annee],
		}),
	})
);

export const ceremaLocalNature = source.table(
	'cerema__local_nature',
	{
		id: varchar('id', { length: 2 }).notNull(),
		nom: varchar('nom'),
	},
	(table) => ({
		pkCeremaLocalNature: primaryKey({
			name: 'pk_cerema__local_nature',
			columns: [table.id],
		}),
	})
);

export const ceremaLocalCategorie = source.table(
	'cerema__local_categorie',
	{
		id: varchar('id', { length: 4 }).notNull(),
		nom: varchar('nom'),
	},
	(table) => ({
		pkCeremaLocalCategorie: primaryKey({
			name: 'pk_cerema__local_categorie',
			columns: [table.id],
		}),
	})
);

export const ceremaLocalProprietairePhysique = source.table(
	'cerema__local_proprietaire_physique',
	{
		idprocpte: varchar('idprocpte', { length: 11 }).notNull(),
		dnulp: varchar('dnulp', { length: 2 }),
		dnomus: varchar('dnomus'),
		dprnus: varchar('dprnus'),
		dnomlp: varchar('dnomlp'),
		dprnlp: varchar('dprnlp'),
		jdatnss: varchar('jdatnss'),
		dldnss: varchar('dldnss'),
		annee: integer('annee').notNull(),
	},
	(table) => ({
		pkCeremaLocal: primaryKey({
			name: 'pk_cerema__local_proprietaire_physique',
			columns: [table.idprocpte, table.dnulp, table.annee],
		}),
	})
);

export const ceremaLocalProprietaireMorale = source.table(
	'cerema__local_proprietaire_morale',
	{
		idprocpte: varchar('idprocpte', { length: 11 }).notNull(),
		dnulp: varchar('dnulp', { length: 2 }),
		dsiren: varchar('dsiren', { length: 9 }),
		ddenom: varchar('ddenom'),
		annee: integer('annee').notNull(),
	},
	(table) => ({
		pkCeremaLocalProprietaireMorale: primaryKey({
			name: 'pk_cerema__local_poprietaire_morale',
			columns: [table.idprocpte, table.dnulp, table.annee],
		}),
	})
);

export const ceremaLocalMutation = source.table(
	'cerema__local_mutation',
	{
		idmutation: integer('idmutation'),
		idnatmut: integer('idnatmut'),
		datemut: date('datemut', { mode: 'date' }),
		valeurfonc: doublePrecision('valeurfonc'),
	},
	(table) => ({
		pkCeremaLocalMutationNatureType: primaryKey({
			name: 'pk_cerema__local_mutation',
			columns: [table.idmutation],
		}),
	})
);

export const ceremaLocalMutationNatureType = source.table(
	'cerema__local_mutation_nature_type',
	{
		id: integer('id').notNull(),
		nom: varchar('nom'),
	},
	(table) => ({
		pkCeremaLocalMutationNatureType: primaryKey({
			name: 'pk_cerema__local_mutation_nature_type',
			columns: [table.id],
		}),
	})
);

export const ceremaLocalMutationLocal = source.table(
	'cerema__local__local_mutation',
	{
		idloc: varchar('idloc', { length: 12 }),
		idmutation: integer('idmutation'),
	},
	(table) => ({
		pkCeremaLocalMutationNatureType: primaryKey({
			name: 'pk_cerema__local__local_mutation',
			columns: [table.idloc, table.idmutation],
		}),
	})
);
