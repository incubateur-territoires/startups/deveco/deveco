import {
	pgTable,
	integer,
	varchar,
	serial,
	boolean,
	date,
	timestamp,
	primaryKey,
	foreignKey,
	unique,
	index,
} from 'drizzle-orm/pg-core';
import { relations } from 'drizzle-orm';

import { multiPolygon } from '../../db/schema/custom-types/postgis';
import { equipe } from '../../db/schema/equipe';
import { commune } from '../../db/schema/territoire';
import { actionContact } from '../../db/schema/evenement';
import { eqLocContact } from '../../db/schema/equipe-local';
import { eqEtabContact } from '../../db/schema/equipe-etablissement';
import { etabCreaCreateur } from '../../db/schema/etablissement-creation';
import { compte } from '../../db/schema/admin';
import { adresseColumns } from '../../db/schema/_columns';

const adresse = pgTable(
	'adresse',
	{
		...adresseColumns,
		communeId: varchar('commune_id', { length: 5 }),
	},
	(table) => ({
		pkAdresse: primaryKey({ name: 'pk_adresse', columns: [table.id] }),
		fkAdresseCommune: foreignKey({
			name: 'fk_adresse__commune',
			columns: [table.communeId],
			foreignColumns: [commune.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
		idxAdresseGeolocalisation: index('idx_adresse__geolocalisation').on(table.geolocalisation),
	})
);

export const adresseRelations = relations(adresse, ({ one }) => ({
	commune: one(commune, {
		fields: [adresse.communeId],
		references: [commune.id],
	}),
}));

export const contactAdresse = pgTable(
	'contact_adresse',
	{
		...adresseColumns,
		contactId: integer('contact_id').notNull(),
		communeId: varchar('commune_id', { length: 5 }).notNull(),
	},
	(table) => ({
		pkContactAdresse: primaryKey({ name: 'pk_contact_adresse', columns: [table.id] }),
		fkContactAdresseCommune: foreignKey({
			name: 'fk_contact_adresse__commune',
			columns: [table.communeId],
			foreignColumns: [commune.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
		fkContactAdresseContact: foreignKey({
			name: 'fk_contact_adresse__contact',
			columns: [table.contactId],
			foreignColumns: [contact.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		ukContactAdresseContact: unique('uk_contact_adresse__contact').on(table.contactId),
	})
);

export const contactAdresseRelations = relations(contactAdresse, ({ one }) => ({
	contact: one(contact, {
		fields: [contactAdresse.contactId],
		references: [contact.id],
	}),
	commune: one(commune, {
		fields: [contactAdresse.communeId],
		references: [commune.id],
	}),
}));

export const contact = pgTable(
	'contact',
	{
		id: serial('id').notNull(),
		createdAt: timestamp('created_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
		email: varchar('email'),
		equipeId: integer('equipe_id').notNull(),
		naissanceDate: date('naissance_date', { mode: 'date' }),
		nom: varchar('nom').notNull(),
		prenom: varchar('prenom').notNull(),
		telephone: varchar('telephone').default(''),
		telephone2: varchar('telephone_2').default(''),
		demarchageAccepte: boolean('demarchage_accepte').default(true),
		creationDate: timestamp('creation_date', { withTimezone: true, mode: 'date' })
			.defaultNow()
			.notNull(),
		modificationDate: timestamp('modification_date', { withTimezone: true, mode: 'date' }),
		modificationCompteId: integer('modification_compte_id'),
		suppressionDate: timestamp('suppression_date', { withTimezone: true, mode: 'date' }),
	},
	(table) => ({
		pkContact: primaryKey({ name: 'pk_contact', columns: [table.id] }),
		fkContactEquipe: foreignKey({
			name: 'fk_contact__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		idxContactEmail: index('idx_contact__email').on(table.email),
		idxContactEquipe: index('idx_contact__equipe').on(table.equipeId),
		idxContactNom: index('idx_contact__nom').on(table.nom),
		idxContactPrenom: index('idx_contact__prenom').on(table.prenom),
		idxContactTelephone: index('idx_contact__telephone').on(table.telephone),
		fkContactModificationCompte: foreignKey({
			name: 'fk_contact__modification_compte',
			columns: [table.modificationCompteId],
			foreignColumns: [compte.id],
		})
			.onDelete('set null')
			.onUpdate('cascade'),
	})
);

export const contactRelations = relations(contact, ({ one, many }) => ({
	equipe: one(equipe, {
		fields: [contact.equipeId],
		references: [equipe.id],
	}),
	adresse: one(contactAdresse, {
		fields: [contact.id],
		references: [contactAdresse.contactId],
	}),
	actions: many(actionContact),
	localCs: many(eqLocContact),
	etabCs: many(eqEtabContact),
	creaCs: many(etabCreaCreateur),
}));

export const qpv2015 = pgTable(
	'qpv_2015',
	{
		id: varchar('id', { length: 8 }).notNull(),
		geometrie: multiPolygon('geometrie').notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkQpv: primaryKey({ name: 'pk_qpv_2015', columns: [table.id] }),
		idxQpvGeometrieGist: index('idx_qpv_2015__geometrie__gist').on(table.geometrie),
	})
);

export const qpv2015Relations = relations(qpv2015, ({ many }) => ({
	communes: many(commune),
}));

export const communeQpv2015 = pgTable(
	'commune__qpv_2015',
	{
		communeId: varchar('commune_id', { length: 5 }).notNull(),
		qpv2015Id: varchar('qpv_2015_id', { length: 8 }).notNull(),
	},
	(table) => ({
		pkCommuneQpv2015: primaryKey({
			name: 'pk_commune__qpv_2015',
			columns: [table.communeId, table.qpv2015Id],
		}),
		fkCommuneQpv2015Commune: foreignKey({
			name: 'fk_commune__qpv_2015__commune',
			columns: [table.communeId],
			foreignColumns: [commune.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
		fkCommuneQpv2015Qpv2015: foreignKey({
			name: 'fk_commune__qpv_2015__qpv_2015',
			columns: [table.qpv2015Id],
			foreignColumns: [qpv2015.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const communeQpv2015Relations = relations(communeQpv2015, ({ one }) => ({
	commune: one(commune, {
		fields: [communeQpv2015.communeId],
		references: [commune.id],
	}),
	qpv: one(qpv2015, {
		fields: [communeQpv2015.qpv2015Id],
		references: [qpv2015.id],
	}),
}));

export const qpv2024 = pgTable(
	'qpv_2024',
	{
		id: varchar('id', { length: 8 }).notNull(),
		geometrie: multiPolygon('geometrie').notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkQpv: primaryKey({ name: 'pk_qpv_2024', columns: [table.id] }),
		idxQpvGeometrieGist: index('idx_qpv_2024__geometrie__gist').on(table.geometrie),
	})
);

export const qpv2024Relations = relations(qpv2024, ({ many }) => ({
	communes: many(commune),
}));

export const communeQpv2024 = pgTable(
	'commune__qpv_2024',
	{
		communeId: varchar('commune_id', { length: 5 }).notNull(),
		qpv2024Id: varchar('qpv_2024_id', { length: 8 }).notNull(),
	},
	(table) => ({
		pkCommuneQpv2024: primaryKey({
			name: 'pk_commune__qpv_2024',
			columns: [table.communeId, table.qpv2024Id],
		}),
		fkCommuneQpv2024Commune: foreignKey({
			name: 'fk_commune__qpv_2024__commune',
			columns: [table.communeId],
			foreignColumns: [commune.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
		fkCommuneQpv2024Qpv2024: foreignKey({
			name: 'fk_commune__qpv_2024__qpv_2024',
			columns: [table.qpv2024Id],
			foreignColumns: [qpv2024.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const communeQpv2024Relations = relations(communeQpv2024, ({ one }) => ({
	commune: one(commune, {
		fields: [communeQpv2024.communeId],
		references: [commune.id],
	}),
	qpv: one(qpv2024, {
		fields: [communeQpv2024.qpv2024Id],
		references: [qpv2024.id],
	}),
}));
