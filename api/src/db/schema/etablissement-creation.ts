import {
	pgTable,
	integer,
	text,
	varchar,
	serial,
	primaryKey,
	date,
	boolean,
	foreignKey,
	timestamp,
	index,
	customType,
} from 'drizzle-orm/pg-core';
import { relations } from 'drizzle-orm';

import { equipe, etiquette } from './equipe';
import { echange, demande, rappel, brouillon } from './suivi';
import { contact } from './contact';
import { entreprise, etablissement } from './etablissement';
import { ressourceType } from './equipe-etablissement';
import { actionEtablissementCreation } from './evenement';
import { commune } from './territoire';
import { compte } from './admin';

import { RessourceTypeId } from '../types';

export const etabCreaProjetType = pgTable(
	'etablissement_creation_projet_type',
	{
		id: varchar('id').notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkEtablissementCreationProjetType: primaryKey({
			name: 'pk_etablissement_creation_projet_type',
			columns: [table.id],
		}),
	})
);

export const etabCreaContactOrigine = pgTable(
	'etablissement_creation_contact_origine',
	{
		id: varchar('id').notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkEtablissementCreationContactOrigine: primaryKey({
			name: 'pk_etablissement_creation_contact_origine',
			columns: [table.id],
		}),
	})
);

export const etabCreaSecteurActivite = pgTable(
	'etablissement_creation_secteur_activite',
	{
		id: varchar('id').notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkEtablissementCreationSecteurActivite: primaryKey({
			name: 'pk_etablissement_creation_secteur_activite',
			columns: [table.id],
		}),
	})
);

export const etabCreaSourceFinancement = pgTable(
	'etablissement_creation_source_financement',
	{
		id: varchar('id').notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkEtablissementCreationSourceFinancement: primaryKey({
			name: 'pk_etablissement_creation_source_financement',
			columns: [table.id],
		}),
	})
);

export const etabCrea = pgTable(
	'etablissement_creation',
	{
		id: serial('id').notNull(),
		enseigne: varchar('enseigne'),
		description: text('description'),
		commentaire: text('commentaire'),
		equipeId: integer('equipe_id').notNull(),
		entrepriseId: varchar('entreprise_id', { length: 9 }),
		actif: boolean('actif').default(true).notNull(),
		creationDate: timestamp('creation_date', { withTimezone: true, mode: 'date' })
			.defaultNow()
			.notNull(),
		modificationDate: timestamp('modification_date', { withTimezone: true, mode: 'date' }),
		modificationCompteId: integer('modification_compte_id'),
		projetTypeId: varchar('projet_type_id'),
		contactOrigineId: varchar('contact_origine_id'),
		contactDate: date('contact_date', { mode: 'date' }),
		secteurActiviteId: varchar('secteur_activite_id'),
		sourceFinancementId: varchar('source_financement_id'),
		categorieJuridiqueEnvisagee: varchar('categorie_juridique_envisagee'),
		contratAccompagnement: boolean('contrat_accompagnement').default(false),
		rechercheLocal: boolean('recherche_local').default(false),
		creationAbandonnee: boolean('creation_abandonnee').default(false),
	},
	(table) => ({
		pkEtablissementCreation: primaryKey({
			name: 'pk_etablissement_creation',
			columns: [table.id],
		}),
		fkEtablissementCreationEquipe: foreignKey({
			name: 'fk_etablissement_creation__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationEntreprise: foreignKey({
			name: 'fk_etablissement_creation__entreprise',
			columns: [table.entrepriseId],
			foreignColumns: [entreprise.siren],
		})
			.onDelete('set null')
			.onUpdate('cascade'),
		fkEtablissementCreationModificationCompte: foreignKey({
			name: 'fk_etablissement_creation__modification_compte',
			columns: [table.modificationCompteId],
			foreignColumns: [compte.id],
		})
			.onDelete('set null')
			.onUpdate('cascade'),
		fkEtablissementCreationProjetType: foreignKey({
			name: 'fk_etablissement_creation__e_c_projet_type',
			columns: [table.projetTypeId],
			foreignColumns: [etabCreaProjetType.id],
		}),
		fkEtablissementCreationContactOrigine: foreignKey({
			name: 'fk_etablissement_creation__e_c_contact_origine',
			columns: [table.contactOrigineId],
			foreignColumns: [etabCreaContactOrigine.id],
		}),
		fkEtablissementCreationSecteurActivite: foreignKey({
			name: 'fk_etablissement_creation__e_c_secteur_activite',
			columns: [table.secteurActiviteId],
			foreignColumns: [etabCreaSecteurActivite.id],
		}),
		fkEtablissementCreationSourceFinancement: foreignKey({
			name: 'fk_etablissement_creation__e_c_source_financement',
			columns: [table.sourceFinancementId],
			foreignColumns: [etabCreaSourceFinancement.id],
		}),
	})
);

export const etabCreaRelations = relations(etabCrea, ({ one, many }) => ({
	creas: many(etabCreaCreateur),
	uniteLegale: one(entreprise, {
		fields: [etabCrea.entrepriseId],
		references: [entreprise.siren],
	}),
	equipe: one(equipe, {
		fields: [etabCrea.equipeId],
		references: [equipe.id],
	}),
	etiquettes: many(etabCreaEtiquette),
	actions: many(actionEtablissementCreation),
	demandes: many(etabCreaDemande),
	echanges: many(etabCreaEchange),
	rappels: many(etabCreaRappel),
	transfo: one(etabCreaTransformation),
	communes: many(etabCreaCommune),
	ressources: many(etabCreaRessource),
	modifCompte: one(compte, {
		fields: [etabCrea.modificationCompteId],
		references: [compte.id],
	}),
	contactOrigine: one(etabCreaContactOrigine, {
		fields: [etabCrea.contactOrigineId],
		references: [etabCreaContactOrigine.id],
	}),
	projetType: one(etabCreaProjetType, {
		fields: [etabCrea.projetTypeId],
		references: [etabCreaProjetType.id],
	}),
	secteurActivite: one(etabCreaSecteurActivite, {
		fields: [etabCrea.secteurActiviteId],
		references: [etabCreaSecteurActivite.id],
	}),
	sourceFinancement: one(etabCreaSourceFinancement, {
		fields: [etabCrea.sourceFinancementId],
		references: [etabCreaSourceFinancement.id],
	}),
}));

export const etabCreaCommune = pgTable(
	'etablissement_creation__commune',
	{
		equipeId: integer('equipe_id').notNull(),
		etabCreaId: integer('etablissement_creation_id').notNull(),
		communeId: varchar('commune_id', { length: 5 }).notNull(),
	},
	(table) => ({
		pkEtablissementCreationCommunes: primaryKey({
			name: 'pk_etablissement_creation__commune',
			columns: [table.equipeId, table.etabCreaId, table.communeId],
		}),
		fkEtablissementCreationCommunesEquipe: foreignKey({
			name: 'fk_etablissement_creation__commune__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationCommunesEtablissementCreation: foreignKey({
			name: 'fk_etablissement_creation__commune__etablissement_creation',
			columns: [table.etabCreaId],
			foreignColumns: [etabCrea.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationCommunesCommune: foreignKey({
			name: 'fk_etablissement_creation__commune__commune',
			columns: [table.communeId],
			foreignColumns: [commune.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const etabCreaCommuneRelations = relations(etabCreaCommune, ({ one }) => ({
	commune: one(commune, {
		fields: [etabCreaCommune.communeId],
		references: [commune.id],
	}),
	equipe: one(equipe, {
		fields: [etabCreaCommune.equipeId],
		references: [equipe.id],
	}),
	etabCrea: one(etabCrea, {
		fields: [etabCreaCommune.etabCreaId],
		references: [etabCrea.id],
	}),
}));

export const etabCreaTransformation = pgTable(
	'etablissement_creation_transformation',
	{
		id: serial('id').notNull(),
		date: date('date', { mode: 'date' }).notNull(),
		equipeId: integer('equipe_id').notNull(),
		etabCreaId: integer('etablissement_creation_id').notNull(),
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
	},
	(table) => ({
		pkEtablissementCreationTransformationTransformation: primaryKey({
			name: 'pk_etablissement_creation_transformation',
			columns: [table.id],
		}),
		fkEtablissementCreationTransformationEquipe: foreignKey({
			name: 'fk_etablissement_creation_transfo__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationTransformationEtablissementCreation: foreignKey({
			name: 'fk_etablissement_creation_transfo__etablissement_creation',
			columns: [table.etabCreaId],
			foreignColumns: [etabCrea.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkEtablissementCreationTransformationEtablissement: foreignKey({
			name: 'fk_etablissement_creation_transfo__etablissement',
			columns: [table.etablissementId],
			foreignColumns: [etablissement.siret],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const etabCreaTransformationRelations = relations(etabCreaTransformation, ({ one }) => ({
	equipe: one(equipe, {
		fields: [etabCreaTransformation.equipeId],
		references: [equipe.id],
	}),
	crea: one(etabCrea, {
		fields: [etabCreaTransformation.etabCreaId],
		references: [etabCrea.id],
	}),
	etab: one(etablissement, {
		fields: [etabCreaTransformation.etablissementId],
		references: [etablissement.siret],
	}),
}));

export const etabCreaEtiquette = pgTable(
	'etablissement_creation__etiquette',
	{
		equipeId: integer('equipe_id').notNull(),
		etabCreaId: integer('etablissement_creation_id').notNull(),
		etiquetteId: integer('etiquette_id').notNull(),
		auto: boolean('auto').default(false),
	},
	(table) => ({
		pkEtablissementCreationEtiquette: primaryKey({
			name: 'pk_etablissement_creation__etiquette',
			columns: [table.etabCreaId, table.etiquetteId],
		}),
		fkEtablissementCreationEtiquetteEquipe: foreignKey({
			name: 'fk_etablissement_creation__etiquette__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationEtiquetteEtablissementCreation: foreignKey({
			name: 'fk_etablissement_creation__etiquette__etablissement_creation',
			columns: [table.etabCreaId],
			foreignColumns: [etabCrea.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationEtiquetteEtiquette: foreignKey({
			name: 'fk_etablissement_creation__etiquette__etiquette',
			columns: [table.etiquetteId],
			foreignColumns: [etiquette.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const etabCreaEtiquetteRelations = relations(etabCreaEtiquette, ({ one }) => ({
	equipe: one(equipe, {
		fields: [etabCreaEtiquette.equipeId],
		references: [equipe.id],
	}),
	etabCrea: one(etabCrea, {
		fields: [etabCreaEtiquette.etabCreaId],
		references: [etabCrea.id],
	}),
	etiquette: one(etiquette, {
		fields: [etabCreaEtiquette.etiquetteId],
		references: [etiquette.id],
	}),
}));

export const etabCreaEchange = pgTable(
	'etablissement_creation__echange',
	{
		equipeId: integer('equipe_id').notNull(),
		etabCreaId: integer('etablissement_creation_id').notNull(),
		echangeId: integer('echange_id').notNull(),
	},
	(table) => ({
		pkEtablissementCreationEchange: primaryKey({
			name: 'pk_etablissement_creation__echange',
			columns: [table.etabCreaId, table.echangeId],
		}),
		fkEtablissementCreationEchangeEquipe: foreignKey({
			name: 'fk_etablissement_creation__echange__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationEchangeEtablissementCreation: foreignKey({
			name: 'fk_etablissement_creation__echange__etablissement_creation',
			columns: [table.etabCreaId],
			foreignColumns: [etabCrea.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationEchangeEchange: foreignKey({
			name: 'fk_etablissement_creation__echange__echange',
			columns: [table.echangeId],
			foreignColumns: [echange.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const etabCreaEchangeRelations = relations(etabCreaEchange, ({ one }) => ({
	equipe: one(equipe, {
		fields: [etabCreaEchange.equipeId],
		references: [equipe.id],
	}),
	etabCrea: one(etabCrea, {
		fields: [etabCreaEchange.etabCreaId],
		references: [etabCrea.id],
	}),
	echange: one(echange, {
		fields: [etabCreaEchange.echangeId],
		references: [echange.id],
	}),
}));

export const etabCreaDemande = pgTable(
	'etablissement_creation__demande',
	{
		equipeId: integer('equipe_id').notNull(),
		etabCreaId: integer('etablissement_creation_id').notNull(),
		demandeId: integer('demande_id').notNull(),
	},
	(table) => ({
		pkEtablissementCreationDemande: primaryKey({
			name: 'pk_etablissement_creation__demande',
			columns: [table.etabCreaId, table.demandeId],
		}),
		fkEtablissementCreationDemandeEquipe: foreignKey({
			name: 'fk_etablissement_creation__demande__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationDemandeEtablissementCreation: foreignKey({
			name: 'fk_etablissement_creation__demande__etablissement_creation',
			columns: [table.etabCreaId],
			foreignColumns: [etabCrea.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationDemandedemande: foreignKey({
			name: 'fk_etablissement_creation__demande__demande',
			columns: [table.demandeId],
			foreignColumns: [demande.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const etabCreaDemandeRelations = relations(etabCreaDemande, ({ one }) => ({
	equipe: one(equipe, {
		fields: [etabCreaDemande.equipeId],
		references: [equipe.id],
	}),
	etabCrea: one(etabCrea, {
		fields: [etabCreaDemande.etabCreaId],
		references: [etabCrea.id],
	}),
	demande: one(demande, {
		fields: [etabCreaDemande.demandeId],
		references: [demande.id],
	}),
}));

export const etabCreaRappel = pgTable(
	'etablissement_creation__rappel',
	{
		equipeId: integer('equipe_id').notNull(),
		etabCreaId: integer('etablissement_creation_id').notNull(),
		rappelId: integer('rappel_id').notNull(),
	},
	(table) => ({
		pkEtablissementCreationRappel: primaryKey({
			name: 'pk_etablissement_creation__rappel',
			columns: [table.etabCreaId, table.rappelId],
		}),
		fkEtablissementCreationRappelEquipe: foreignKey({
			name: 'fk_etablissement_creation__rappel__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationRappelEtablissementCreation: foreignKey({
			name: 'fk_etablissement_creation__rappel__etablissement_creation',
			columns: [table.etabCreaId],
			foreignColumns: [etabCrea.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationRappelRappel: foreignKey({
			name: 'fk_etablissement_creation__rappel__rappel',
			columns: [table.rappelId],
			foreignColumns: [rappel.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const etabCreaRappelRelations = relations(etabCreaRappel, ({ one }) => ({
	equipe: one(equipe, {
		fields: [etabCreaRappel.equipeId],
		references: [equipe.id],
	}),
	etabCrea: one(etabCrea, {
		fields: [etabCreaRappel.etabCreaId],
		references: [etabCrea.id],
	}),
	rappel: one(rappel, {
		fields: [etabCreaRappel.rappelId],
		references: [rappel.id],
	}),
}));

export const etabCreaBrouillon = pgTable(
	'etablissement_creation__brouillon',
	{
		equipeId: integer('equipe_id').notNull(),
		etabCreaId: integer('etablissement_creation_id').notNull(),
		brouillonId: integer('brouillon_id').notNull(),
	},
	(table) => ({
		pkEtablissementCreationBrouillon: primaryKey({
			name: 'pk_etablissement_creation__brouillon',
			columns: [table.etabCreaId, table.brouillonId],
		}),
		fkEtablissementCreationBrouillonEquipe: foreignKey({
			name: 'fk_etablissement_creation__brouillon__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationBrouillonEtablissementCreation: foreignKey({
			name: 'fk_etablissement_creation__brouillon__etablissement_creation',
			columns: [table.etabCreaId],
			foreignColumns: [etabCrea.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationBrouillonBrouillon: foreignKey({
			name: 'fk_etablissement_creation__brouillon__brouillon',
			columns: [table.brouillonId],
			foreignColumns: [brouillon.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const etabCreaBrouillonRelations = relations(etabCreaBrouillon, ({ one }) => ({
	equipe: one(equipe, {
		fields: [etabCreaBrouillon.equipeId],
		references: [equipe.id],
	}),
	etabCrea: one(etabCrea, {
		fields: [etabCreaBrouillon.etabCreaId],
		references: [etabCrea.id],
	}),
	brouillon: one(brouillon, {
		fields: [etabCreaBrouillon.brouillonId],
		references: [brouillon.id],
	}),
}));

export const diplomeNiveau = pgTable(
	'etablissement_creation__createur_diplome_niveau_type',
	{
		id: varchar('id').notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkDiplomeNiveau: primaryKey({
			name: 'pk_etablissement_creation__createur_diplome_niveau_type',
			columns: [table.id],
		}),
	})
);

export const professionSituation = pgTable(
	'etablissement_creation__createur_profession_situation_type',
	{
		id: varchar('id').notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkProfessionSituation: primaryKey({
			name: 'pk_etablissement_creation__createur_profession_situation_type',
			columns: [table.id],
		}),
	})
);

export const etabCreaCreateur = pgTable(
	'etablissement_creation__createur',
	{
		equipeId: integer('equipe_id').notNull(),
		etabCreaId: integer('etablissement_creation_id').notNull(),
		contactId: integer('contact_id').notNull(),
		fonction: varchar('fonction'),
		diplomeNiveauId: varchar('diplome_niveau_type_id'),
		professionSituationId: varchar('profession_situation_type_id'),
	},
	(table) => ({
		pkEtablissementCreationCreateur: primaryKey({
			name: 'pk_etablissement_creation__createur',
			columns: [table.etabCreaId, table.contactId],
		}),
		fkEtablissementCreationCreateurContact: foreignKey({
			name: 'fk_etablissement_creation__createur__contact',
			columns: [table.contactId],
			foreignColumns: [contact.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationCreateurEtablissementCreation: foreignKey({
			name: 'fk_etablissement_creation__createur__etablissement_creation',
			columns: [table.etabCreaId],
			foreignColumns: [etabCrea.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationCreateurDiplomeNiveau: foreignKey({
			name: 'fk_etablissement_creation__createur__diplome_niveau',
			columns: [table.diplomeNiveauId],
			foreignColumns: [diplomeNiveau.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtablissementCreationCreateurProfessionSituation: foreignKey({
			name: 'fk_etablissement_creation__createur__profession_situation',
			columns: [table.professionSituationId],
			foreignColumns: [professionSituation.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const etabCreaCreateurRelations = relations(etabCreaCreateur, ({ one }) => ({
	equipe: one(equipe, {
		fields: [etabCreaCreateur.equipeId],
		references: [equipe.id],
	}),
	eCreation: one(etabCrea, {
		fields: [etabCreaCreateur.etabCreaId],
		references: [etabCrea.id],
	}),
	ctct: one(contact, {
		fields: [etabCreaCreateur.contactId],
		references: [contact.id],
	}),
	diplomeNiveau: one(diplomeNiveau, {
		fields: [etabCreaCreateur.diplomeNiveauId],
		references: [diplomeNiveau.id],
	}),
	professionSituation: one(professionSituation, {
		fields: [etabCreaCreateur.professionSituationId],
		references: [professionSituation.id],
	}),
}));

// TODO j'aurais bien aimé utiliser celui défini dans equipe-etablissement
// mais ça fait planter la compilation TS.
export const ressourceTypeId = customType<{
	data: RessourceTypeId;
	notNull: true;
	default: true;
}>({
	dataType() {
		return 'varchar';
	},
});

export const etabCreaRessource = pgTable(
	'etablissement_creation_ressource',
	{
		id: serial('id'),
		equipeId: integer('equipe_id').notNull(),
		etabCreaId: integer('etablissement_creation_id').notNull(),
		nom: varchar('nom'),
		lien: varchar('lien'),
		type: ressourceTypeId('ressource_type_id'),
	},
	(table) => ({
		pkEtablissementCreationRessource: primaryKey({
			name: 'pk_etablissement_creation_ressource',
			columns: [table.id],
		}),
		fkEtablissementCreationRessourceType: foreignKey({
			name: 'fk_etablissement_creation_ressource__ressource_type',
			columns: [table.type],
			foreignColumns: [ressourceType.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		idixEtablissementCreationRessource: index('idx_etablissement_creation_ressource').on(
			table.equipeId,
			table.etabCreaId
		),
	})
);

export const etabCreaRessourceRelations = relations(etabCreaRessource, ({ one }) => ({
	equipe: one(equipe, {
		fields: [etabCreaRessource.equipeId],
		references: [equipe.id],
	}),
	etabCrea: one(etabCrea, {
		fields: [etabCreaRessource.etabCreaId, etabCreaRessource.equipeId],
		references: [etabCrea.id, etabCrea.equipeId],
	}),
	etab: one(etablissement, {
		fields: [etabCreaRessource.etabCreaId],
		references: [etablissement.siret],
	}),
}));
