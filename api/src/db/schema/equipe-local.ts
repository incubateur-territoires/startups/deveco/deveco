import {
	pgTable,
	serial,
	integer,
	text,
	varchar,
	primaryKey,
	foreignKey,
	customType,
	boolean,
	index,
	timestamp,
} from 'drizzle-orm/pg-core';
import { relations } from 'drizzle-orm';

import {
	EquipeLocalContributionBailTypeId,
	EquipeLocalContributionVacanceMotifTypeId,
	EquipeLocalContributionVacanceTypeId,
	RessourceTypeId,
} from '../types';
import { deveco, equipe, etiquette } from '../../db/schema/equipe';
import { echange, demande, rappel, brouillon } from '../../db/schema/suivi';
import { contact } from '../../db/schema/contact';
import { etablissement } from '../../db/schema/etablissement';
import { local } from '../../db/schema/local';
import { ressourceType } from '../../db/schema/equipe-etablissement';
import { compte } from '../../db/schema/admin';
import { adresseColumns } from '../../db/schema/_columns';

const eqLocContributionBailTypeId = customType<{
	data: EquipeLocalContributionBailTypeId;
	notNull: true;
	default: true;
}>({
	dataType() {
		return 'varchar';
	},
});

const eqLocContributionVacanceTypeId = customType<{
	data: EquipeLocalContributionVacanceTypeId;
	notNull: true;
	default: true;
}>({
	dataType() {
		return 'varchar';
	},
});

const eqLocContributionVacanceMotifTypeId = customType<{
	data: EquipeLocalContributionVacanceMotifTypeId;
	notNull: true;
	default: true;
}>({
	dataType() {
		return 'varchar';
	},
});

export const eqLocContributionBailType = pgTable(
	'equipe_local_contribution_bail_type',
	{
		id: eqLocContributionBailTypeId('id').notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkEquipeLocalContributionBailType: primaryKey({
			name: 'pk_equipe_local_contribution_bail_type',
			columns: [table.id],
		}),
	})
);

export const eqLocContributionVacanceType = pgTable(
	'equipe_local_contribution_vacance_type',
	{
		id: eqLocContributionVacanceTypeId('id').notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkEquipeLocalContributionVacanceType: primaryKey({
			name: 'pk_equipe_local_contribution_vacance_type',
			columns: [table.id],
		}),
	})
);

export const eqLocContributionVacanceMotifType = pgTable(
	'equipe_local_contribution_vacance_motif_type',
	{
		id: eqLocContributionVacanceMotifTypeId('id').notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkEquipeLocalContributionVacanceMotifType: primaryKey({
			name: 'pk_equipe_local_contribution_vacance_motif_type',
			columns: [table.id],
		}),
	})
);

export const eqLocContribution = pgTable(
	'equipe__local_contribution',
	{
		equipeId: integer('equipe_id').notNull(),
		localId: varchar('local_id', { length: 12 }).notNull(),
		description: text('description'),
		nom: varchar('nom'),
		occupe: boolean('occupe'),
		remembre: boolean('remembre').default(false),
		loyerEuros: integer('loyer_euros'),
		fondsEuros: integer('fonds_euros'),
		venteEuros: integer('vente_euros'),
		bailTypeId: eqLocContributionBailTypeId('equipe_local_contribution_bail_type_id'),
		vacanceTypeId: eqLocContributionVacanceTypeId('equipe_local_contribution_vacance_type_id'),
		vacanceMotifTypeId: eqLocContributionVacanceMotifTypeId(
			'equipe_local_contribution_vacance_motif_type_id'
		),
		creationDate: timestamp('creation_date', { withTimezone: true, mode: 'date' })
			.defaultNow()
			.notNull(),
		geolocalisation: adresseColumns.geolocalisation,
		modificationDate: timestamp('modification_date', { withTimezone: true, mode: 'date' }),
		modificationCompteId: integer('modification_compte_id'),
	},
	(table) => ({
		pkEquipeLocalContribution: primaryKey({
			name: 'pk_equipe__local_contribution',
			columns: [table.equipeId, table.localId],
		}),
		fkEquipeLocalContributionBailType: foreignKey({
			name: 'fk_equipe__local_contribution__bail_type',
			columns: [table.bailTypeId],
			foreignColumns: [eqLocContributionBailType.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkEquipeLocalContributionVacanceType: foreignKey({
			name: 'fk_equipe__local_contribution__vacance_type',
			columns: [table.vacanceTypeId],
			foreignColumns: [eqLocContributionVacanceType.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkEquipeLocalContributionVacanceMotifType: foreignKey({
			name: 'fk_equipe__local_contribution__vacance_motif_type',
			columns: [table.vacanceMotifTypeId],
			foreignColumns: [eqLocContributionVacanceMotifType.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkEquipeLocalContributionModificationCompte: foreignKey({
			name: 'fk_equipe__local_contribution__modification_compte',
			columns: [table.modificationCompteId],
			foreignColumns: [compte.id],
		})
			.onDelete('set null')
			.onUpdate('cascade'),
	})
);

export const eqLocContributionRelations = relations(eqLocContribution, ({ one }) => ({
	eq: one(equipe, {
		fields: [eqLocContribution.equipeId],
		references: [equipe.id],
	}),
	local: one(local, {
		fields: [eqLocContribution.localId],
		references: [local.id],
	}),
	vacanceType: one(eqLocContributionVacanceType, {
		fields: [eqLocContribution.vacanceTypeId],
		references: [eqLocContributionVacanceType.id],
	}),
	vacanceMotifType: one(eqLocContributionVacanceMotifType, {
		fields: [eqLocContribution.vacanceMotifTypeId],
		references: [eqLocContributionVacanceMotifType.id],
	}),
	modifCompte: one(compte, {
		fields: [eqLocContribution.modificationCompteId],
		references: [compte.id],
	}),
}));

// TODO j'aurais bien aimé utiliser celui défini dans equipe-etablissement
// mais ça fait planter la compilation TS.
export const ressourceTypeId = customType<{
	data: RessourceTypeId;
	notNull: true;
	default: true;
}>({
	dataType() {
		return 'varchar';
	},
});

export const eqLocRessource = pgTable(
	'equipe__local_ressource',
	{
		id: serial('id'),
		equipeId: integer('equipe_id').notNull(),
		localId: varchar('local_id', { length: 14 }).notNull(),
		nom: varchar('nom'),
		lien: varchar('lien'),
		type: ressourceTypeId('ressource_type_id'),
	},
	(table) => ({
		pkEquipeLocalRessource: primaryKey({
			name: 'pk_equipe__local_ressource',
			columns: [table.id],
		}),
		fkEquipeLocalRessourceType: foreignKey({
			name: 'fk_equipe__local_ressource__ressource_type',
			columns: [table.type],
			foreignColumns: [ressourceType.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		idxEquipeLocalRessource: index('idx_equipe_local_ressource').on(table.equipeId, table.localId),
	})
);

export const eqLocRessourceRelations = relations(eqLocRessource, ({ one }) => ({
	equipe: one(equipe, {
		fields: [eqLocRessource.equipeId],
		references: [equipe.id],
	}),
	local: one(local, {
		fields: [eqLocRessource.localId],
		references: [local.id],
	}),
}));

export const eqLocEtiquette = pgTable(
	'equipe__local__etiquette',
	{
		equipeId: integer('equipe_id').notNull(),
		localId: varchar('local_id', { length: 12 }).notNull(),
		etiquetteId: integer('etiquette_id').notNull(),
		auto: boolean('auto').default(false),
	},
	(table) => ({
		pkEquipeLocalEtiquette: primaryKey({
			name: 'pk_equipe__local__etiquette',
			columns: [table.localId, table.equipeId, table.etiquetteId],
		}),

		fkEquipeLocalEtiquetteLocal: foreignKey({
			name: 'fk_equipe__local__etiquette__local',
			columns: [table.localId],
			foreignColumns: [local.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEquipeLocalEtiquetteEquipe: foreignKey({
			name: 'fk_equipe__local__etiquette__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEquipeLocalEtiquetteEtiquette: foreignKey({
			name: 'fk_equipe__local__etiquette__etiquette',
			columns: [table.etiquetteId],
			foreignColumns: [etiquette.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const eqLocEtiquetteRelations = relations(eqLocEtiquette, ({ one }) => ({
	local: one(local, {
		fields: [eqLocEtiquette.localId],
		references: [local.id],
	}),
	equipe: one(equipe, {
		fields: [eqLocEtiquette.equipeId],
		references: [equipe.id],
	}),
	etiquette: one(etiquette, {
		fields: [eqLocEtiquette.etiquetteId],
		references: [etiquette.id],
	}),
}));

export const eqLocContact = pgTable(
	'equipe__local__contact',
	{
		equipeId: integer('equipe_id').notNull(),
		localId: varchar('local_id', { length: 12 }).notNull(),
		contactId: integer('contact_id').notNull(),
		fonction: varchar('fonction'),
		contactSourceTypeId: varchar('contact_source_type_id', {
			length: 10,
		}).notNull(),
	},
	(table) => ({
		pkEquipeLocalContact: primaryKey({
			name: 'pk_equipe__local__contact',
			columns: [table.localId, table.equipeId, table.contactId],
		}),
		fkEquipeLocalContactLocal: foreignKey({
			name: 'fk_equipe__local__contact__local',
			columns: [table.localId],
			foreignColumns: [local.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEquipeLocalContactEquipe: foreignKey({
			name: 'fk_equipe__local__contact__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEquipeLocalContactContact: foreignKey({
			name: 'fk_equipe__local__contact__contact',
			columns: [table.contactId],
			foreignColumns: [contact.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const eqLocContactRelations = relations(eqLocContact, ({ one }) => ({
	local: one(local, {
		fields: [eqLocContact.localId],
		references: [local.id],
	}),
	equipe: one(equipe, {
		fields: [eqLocContact.equipeId],
		references: [equipe.id],
	}),
	ctct: one(contact, {
		fields: [eqLocContact.contactId],
		references: [contact.id],
	}),
}));

export const eqLocOccupant = pgTable(
	'equipe__local__occupant',
	{
		localId: varchar('local_id', { length: 12 }).notNull(),
		equipeId: integer('equipe_id').notNull(),
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
	},
	(table) => ({
		pkEquipeLocalOccupant: primaryKey({
			name: 'pk_equipe__local__occupant',
			columns: [table.localId, table.equipeId, table.etablissementId],
		}),
		fkEquipeLocalOccupantLocal: foreignKey({
			name: 'fk_equipe__local__occupant__local',
			columns: [table.localId],
			foreignColumns: [local.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEquipeLocalOccupantEquipe: foreignKey({
			name: 'fk_equipe__local__occupant__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEquipeLocalOccupantOccupant: foreignKey({
			name: 'fk_equipe__local__occupant__occupant',
			columns: [table.etablissementId],
			foreignColumns: [etablissement.siret],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const eqLocOccupantRelations = relations(eqLocOccupant, ({ one }) => ({
	local: one(local, {
		fields: [eqLocOccupant.localId],
		references: [local.id],
	}),
	equipe: one(equipe, {
		fields: [eqLocOccupant.equipeId],
		references: [equipe.id],
	}),
	occupant: one(etablissement, {
		fields: [eqLocOccupant.etablissementId],
		references: [etablissement.siret],
	}),
}));

export const eqLocEchange = pgTable(
	'equipe__local__echange',
	{
		equipeId: integer('equipe_id').notNull(),
		localId: varchar('local_id', { length: 14 }).notNull(),
		echangeId: integer('echange_id').notNull(),
	},
	(table) => ({
		pkEquipeLocalEchange: primaryKey({
			name: 'pk_equipe__local__echange',
			columns: [table.equipeId, table.localId, table.echangeId],
		}),
		fkEquipeLocalEchangeLocal: foreignKey({
			name: 'fk_equipe__local__echange__local',
			columns: [table.localId],
			foreignColumns: [local.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeLocalEchangeEquipe: foreignKey({
			name: 'fk_equipe__local__echange__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeLocalEchangeEchange: foreignKey({
			name: 'fk_equipe__local__echange__echange',
			columns: [table.echangeId],
			foreignColumns: [echange.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const eqLocEchangeRelations = relations(eqLocEchange, ({ one }) => ({
	equipe: one(equipe, {
		fields: [eqLocEchange.equipeId],
		references: [equipe.id],
	}),
	local: one(local, {
		fields: [eqLocEchange.localId],
		references: [local.id],
	}),
	echange: one(echange, {
		fields: [eqLocEchange.echangeId],
		references: [echange.id],
	}),
}));

export const eqLocDemande = pgTable(
	'equipe__local__demande',
	{
		equipeId: integer('equipe_id').notNull(),
		localId: varchar('local_id', { length: 14 }).notNull(),
		demandeId: integer('demande_id').notNull(),
	},
	(table) => ({
		pkEquipeLocalDemande: primaryKey({
			name: 'pk_equipe__local__demande',
			columns: [table.equipeId, table.localId, table.demandeId],
		}),
		fkEquipeLocalDemandeLocal: foreignKey({
			name: 'fk_equipe__local__demande__local',
			columns: [table.localId],
			foreignColumns: [local.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeLocalDemandeEquipe: foreignKey({
			name: 'fk_equipe__local__demande__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeLocalDemandeDemande: foreignKey({
			name: 'fk_equipe__local__demande__demande',
			columns: [table.demandeId],
			foreignColumns: [demande.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const eqLocDemandeRelations = relations(eqLocDemande, ({ one }) => ({
	equipe: one(equipe, {
		fields: [eqLocDemande.equipeId],
		references: [equipe.id],
	}),
	local: one(local, {
		fields: [eqLocDemande.localId],
		references: [local.id],
	}),
	demande: one(demande, {
		fields: [eqLocDemande.demandeId],
		references: [demande.id],
	}),
}));

export const eqLocRappel = pgTable(
	'equipe__local__rappel',
	{
		equipeId: integer('equipe_id').notNull(),
		localId: varchar('local_id', { length: 14 }).notNull(),
		rappelId: integer('rappel_id').notNull(),
	},
	(table) => ({
		pkEquipeLocalRappel: primaryKey({
			name: 'pk_equipe__local__rappel',
			columns: [table.equipeId, table.localId, table.rappelId],
		}),
		fkEquipeLocalRappelLocal: foreignKey({
			name: 'fk_equipe__local__rappel__local',
			columns: [table.localId],
			foreignColumns: [local.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeLocalRappelEquipe: foreignKey({
			name: 'fk_equipe__local__rappel__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeLocalRappelRappel: foreignKey({
			name: 'fk_equipe__local__rappel__rappel',
			columns: [table.rappelId],
			foreignColumns: [rappel.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const eqLocRappelRelations = relations(eqLocRappel, ({ one }) => ({
	equipe: one(equipe, {
		fields: [eqLocRappel.equipeId],
		references: [equipe.id],
	}),
	local: one(local, {
		fields: [eqLocRappel.localId],
		references: [local.id],
	}),
	rappel: one(rappel, {
		fields: [eqLocRappel.rappelId],
		references: [rappel.id],
	}),
}));

export const eqLocBrouillon = pgTable(
	'equipe__local__brouillon',
	{
		equipeId: integer('equipe_id').notNull(),
		localId: varchar('local_id', { length: 14 }).notNull(),
		brouillonId: integer('brouillon_id').notNull(),
	},
	(table) => ({
		pkEquipeLocalBrouillon: primaryKey({
			name: 'pk_equipe__local__brouillon',
			columns: [table.equipeId, table.localId, table.brouillonId],
		}),
		fkEquipeLocalBrouillonLocal: foreignKey({
			name: 'fk_equipe__local__brouillon__local',
			columns: [table.localId],
			foreignColumns: [local.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeLocalBrouillonEquipe: foreignKey({
			name: 'fk_equipe__local__brouillon__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeLocalBrouillonBrouillon: foreignKey({
			name: 'fk_equipe__local__brouillon__brouillon',
			columns: [table.brouillonId],
			foreignColumns: [brouillon.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const eqLocBrouillonRelations = relations(eqLocBrouillon, ({ one }) => ({
	equipe: one(equipe, {
		fields: [eqLocBrouillon.equipeId],
		references: [equipe.id],
	}),
	local: one(local, {
		fields: [eqLocBrouillon.localId],
		references: [local.id],
	}),
	brouillon: one(brouillon, {
		fields: [eqLocBrouillon.brouillonId],
		references: [brouillon.id],
	}),
}));

export const devecoLocalFavori = pgTable(
	'deveco__local_favori',
	{
		devecoId: integer('deveco_id').notNull(),
		localId: varchar('local_id', { length: 14 }).notNull(),
	},
	(table) => ({
		pkDevecoLocalFavori: primaryKey({
			name: 'pk_deveco__local_favori',
			columns: [table.devecoId, table.localId],
		}),
		fkDevecoLocalFavoriDeveco: foreignKey({
			name: 'fk_deveco__local_favori__deveco',
			columns: [table.devecoId],
			foreignColumns: [deveco.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkDevecoLocalFavoriLocal: foreignKey({
			name: 'fk_deveco__local_favori__local',
			columns: [table.localId],
			foreignColumns: [local.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const devecoLocalFavoriRelations = relations(devecoLocalFavori, ({ one }) => ({
	deveco: one(deveco, {
		fields: [devecoLocalFavori.devecoId],
		references: [deveco.id],
	}),
	local: one(local, {
		fields: [devecoLocalFavori.localId],
		references: [local.id],
	}),
}));
