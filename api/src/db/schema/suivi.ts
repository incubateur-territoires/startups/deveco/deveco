import {
	pgTable,
	integer,
	text,
	varchar,
	serial,
	date,
	primaryKey,
	customType,
	foreignKey,
	timestamp,
} from 'drizzle-orm/pg-core';
import { relations } from 'drizzle-orm';

import { equipe, deveco } from '../../db/schema/equipe';
import { compte } from '../../db/schema/admin';
import { DemandeTypeId, EchangeTypeId } from '../../db/types';
import {
	eqEtabEchange,
	eqEtabDemande,
	eqEtabRappel,
	eqEtabBrouillon,
} from '../../db/schema/equipe-etablissement';
import {
	etabCreaEchange,
	etabCreaDemande,
	etabCreaRappel,
	etabCreaBrouillon,
} from '../../db/schema/etablissement-creation';
import {
	eqLocEchange,
	eqLocRappel,
	eqLocDemande,
	eqLocBrouillon,
} from '../../db/schema/equipe-local';

const demandeTypeId = customType<{
	data: DemandeTypeId;
	notNull: true;
	default: true;
}>({
	dataType() {
		return 'varchar';
	},
});

const echangeTypeId = customType<{
	data: EchangeTypeId;
	notNull: true;
	default: true;
}>({
	dataType() {
		return 'varchar';
	},
});

export const echangeType = pgTable(
	'echange_type',
	{
		id: varchar('id', { length: 10 }).notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkEchangeType: primaryKey({
			name: 'pk_echange_type',
			columns: [table.id],
		}),
	})
);

export const demandeType = pgTable(
	'demande_type',
	{
		id: demandeTypeId('id', { length: 20 }).notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkDemandeType: primaryKey({
			name: 'pk_demande_type',
			columns: [table.id],
		}),
	})
);

export const echange = pgTable(
	'echange',
	{
		id: serial('id').notNull(),
		date: date('date', { mode: 'date' }).notNull(),
		description: text('description'),
		devecoId: integer('deveco_id').notNull(),
		equipeId: integer('equipe_id').notNull(),
		nom: varchar('nom'),
		typeId: echangeTypeId('echange_type_id', { length: 10 }).notNull(),
		creationDate: timestamp('creation_date', { withTimezone: true, mode: 'date' })
			.defaultNow()
			.notNull(),
		modificationDate: timestamp('modification_date', { withTimezone: true, mode: 'date' }),
		modificationCompteId: integer('modification_compte_id'),
		suppressionDate: timestamp('suppression_date', { withTimezone: true, mode: 'date' }),
	},
	(table) => ({
		pkEchange: primaryKey({
			name: 'pk_echange',
			columns: [table.id],
		}),
		fkEchangeEquipe: foreignKey({
			name: 'fk_echange__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEchangeType: foreignKey({
			name: 'fk_echange__echange_type',
			columns: [table.typeId],
			foreignColumns: [echangeType.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkEchangeDeveco: foreignKey({
			name: 'fk_echange__deveco',
			columns: [table.devecoId],
			foreignColumns: [deveco.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkEchangeModificationCompte: foreignKey({
			name: 'fk_echange__modification_compte',
			columns: [table.modificationCompteId],
			foreignColumns: [compte.id],
		})
			.onDelete('set null')
			.onUpdate('cascade'),
	})
);

export const echangeRelations = relations(echange, ({ one, many }) => ({
	type: one(echangeType, {
		fields: [echange.typeId],
		references: [echangeType.id],
	}),
	equipe: one(equipe, {
		fields: [echange.equipeId],
		references: [equipe.id],
	}),
	deveco: one(deveco, {
		fields: [echange.devecoId],
		references: [deveco.id],
	}),
	etabCreaEch: one(etabCreaEchange, {
		fields: [echange.id],
		references: [etabCreaEchange.echangeId],
	}),
	eqEtab: one(eqEtabEchange, {
		fields: [echange.id],
		references: [eqEtabEchange.echangeId],
	}),
	eqEtabs: many(eqEtabEchange),
	creations: many(etabCreaEchange),
	eqLocaux: many(eqLocEchange),
	eqLoc: one(eqLocEchange, {
		fields: [echange.id],
		references: [eqLocEchange.echangeId],
	}),
	demandes: many(echangeDemande),
	modifCompte: one(compte, {
		fields: [echange.modificationCompteId],
		references: [compte.id],
	}),
}));

export const echangeDemande = pgTable(
	'echange__demande',
	{
		echangeId: integer('echange_id').notNull(),
		demandeId: integer('demande_id').notNull(),
	},
	(table) => ({
		pkEchangeDemande: primaryKey({
			name: 'pk_echange__demande',
			columns: [table.echangeId, table.demandeId],
		}),
		fkEchangeDemandeDemande: foreignKey({
			name: 'fk_echange__demande__demande',
			columns: [table.demandeId],
			foreignColumns: [demande.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEchangeDemandeEchange: foreignKey({
			name: 'fk_echange__demande__echange',
			columns: [table.echangeId],
			foreignColumns: [echange.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const echangeDemandeRelations = relations(echangeDemande, ({ one }) => ({
	echange: one(echange, {
		fields: [echangeDemande.echangeId],
		references: [echange.id],
	}),
	demande: one(demande, {
		fields: [echangeDemande.demandeId],
		references: [demande.id],
	}),
}));

export const demande = pgTable(
	'demande',
	{
		id: serial('id').notNull(),
		nom: varchar('nom'),
		description: text('description'),
		date: date('date', { mode: 'date' }).notNull(),
		equipeId: integer('equipe_id').notNull(),
		typeId: demandeTypeId('demande_type_id', { length: 20 }).notNull(),
		clotureDate: date('cloture_date', { mode: 'date' }),
		clotureMotif: varchar('cloture_motif'),
		creationDate: timestamp('creation_date', { withTimezone: true, mode: 'date' })
			.defaultNow()
			.notNull(),
		modificationDate: timestamp('modification_date', { withTimezone: true, mode: 'date' }),
		modificationCompteId: integer('modification_compte_id'),
		suppressionDate: timestamp('suppression_date', { withTimezone: true, mode: 'date' }),
	},
	(table) => ({
		pkDemande: primaryKey({
			name: 'pk_demande',
			columns: [table.id],
		}),
		fkDemandeEquipe: foreignKey({
			name: 'fk_demande__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkDemandeType: foreignKey({
			name: 'fk_demande__demande_type',
			columns: [table.typeId],
			foreignColumns: [demandeType.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkDemandeModificationCompte: foreignKey({
			name: 'fk_demande__modification_compte',
			columns: [table.modificationCompteId],
			foreignColumns: [compte.id],
		})
			.onDelete('set null')
			.onUpdate('cascade'),
	})
);

export const demandeRelations = relations(demande, ({ one, many }) => ({
	type: one(demandeType, {
		fields: [demande.typeId],
		references: [demandeType.id],
	}),
	equipe: one(equipe, {
		fields: [demande.equipeId],
		references: [equipe.id],
	}),
	eqEtabs: many(eqEtabDemande),
	creations: many(etabCreaDemande),
	eqLocaux: many(eqLocDemande),
	echanges: many(echangeDemande),
	modifCompte: one(compte, {
		fields: [demande.modificationCompteId],
		references: [compte.id],
	}),
}));

export const rappel = pgTable(
	'rappel',
	{
		id: serial('id').notNull(),
		date: date('date', { mode: 'date' }).notNull(),
		description: text('description'),
		equipeId: integer('equipe_id').notNull(),
		nom: varchar('nom').notNull(),
		affecteCompteId: integer('affecte_compte_id'),
		clotureDate: date('cloture_date', { mode: 'date' }),
		creationDate: timestamp('creation_date', { withTimezone: true, mode: 'date' })
			.defaultNow()
			.notNull(),
		modificationDate: timestamp('modification_date', { withTimezone: true, mode: 'date' }),
		modificationCompteId: integer('modification_compte_id'),
		suppressionDate: timestamp('suppression_date', { withTimezone: true, mode: 'date' }),
	},
	(table) => ({
		pkRappel: primaryKey({
			name: 'pk_rappel',
			columns: [table.id],
		}),
		fkRappelEquipe: foreignKey({
			name: 'fk_rappel__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkRappelAffecteCompte: foreignKey({
			name: 'fk_rappel__affecte_compte',
			columns: [table.affecteCompteId],
			foreignColumns: [compte.id],
		})
			.onDelete('set null')
			.onUpdate('cascade'),
		fkRappelModificationCompte: foreignKey({
			name: 'fk_rappel__modification_compte',
			columns: [table.modificationCompteId],
			foreignColumns: [compte.id],
		})
			.onDelete('set null')
			.onUpdate('cascade'),
	})
);

export const rappelRelations = relations(rappel, ({ one, many }) => ({
	equipe: one(equipe, {
		fields: [rappel.equipeId],
		references: [equipe.id],
	}),
	eqEtabs: many(eqEtabRappel),
	creations: many(etabCreaRappel),
	eqLocaux: many(eqLocRappel),
	affecteCompte: one(compte, {
		fields: [rappel.affecteCompteId],
		references: [compte.id],
	}),
	modifCompte: one(compte, {
		fields: [rappel.modificationCompteId],
		references: [compte.id],
	}),
}));

export const brouillon = pgTable(
	'brouillon',
	{
		id: serial('id').notNull(),
		date: date('date', { mode: 'date' }).notNull(),
		description: text('description'),
		equipeId: integer('equipe_id').notNull(),
		nom: varchar('nom').notNull(),
		creationDate: timestamp('creation_date', { withTimezone: true, mode: 'date' })
			.defaultNow()
			.notNull(),
		creationCompteId: integer('creation_compte_id').notNull(),
		modificationDate: timestamp('modification_date', { withTimezone: true, mode: 'date' }),
		modificationCompteId: integer('modification_compte_id'),
		suppressionDate: timestamp('suppression_date', { withTimezone: true, mode: 'date' }),
	},
	(table) => [
		primaryKey({
			name: 'pk_brouillon',
			columns: [table.id],
		}),
		foreignKey({
			name: 'fk_brouillon__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		foreignKey({
			name: 'fk_brouillon__modification_compte',
			columns: [table.modificationCompteId],
			foreignColumns: [compte.id],
		})
			.onDelete('set null')
			.onUpdate('cascade'),
	]
);

export const brouillonRelations = relations(brouillon, ({ one, many }) => ({
	equipe: one(equipe, {
		fields: [brouillon.equipeId],
		references: [equipe.id],
	}),
	compte: one(compte, {
		fields: [brouillon.creationCompteId],
		references: [compte.id],
	}),
	eqEtabs: many(eqEtabBrouillon),
	creations: many(etabCreaBrouillon),
	eqLocaux: many(eqLocBrouillon),
	modifCompte: one(compte, {
		fields: [brouillon.modificationCompteId],
		references: [compte.id],
	}),
}));
