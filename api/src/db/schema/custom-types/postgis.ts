import { customType } from 'drizzle-orm/pg-core';
import * as wkx from 'wkx';

export type Position = [number, number];
export type Polygon = Position[];
export type MultiPolygon = Polygon[];

export interface GeojsonMultiPolygon {
	type: 'MultiPolygon';
	coordinates: MultiPolygon;
}
export interface GeojsonPoint {
	type: 'Point';
	coordinates: [number, number];
}

const positionToPoint = (position: Position) => new wkx.Point(position[0], position[1]);

export const geometry = customType<{
	data: Position[] | null;
	driverData: GeojsonMultiPolygon | string | null;
	config: { epsg: string };
}>({
	dataType(config) {
		const epsg = typeof config?.epsg !== 'undefined' ? `,${config.epsg}` : ',4326';

		return `geometry(Geometry${epsg})`;
	},
});

export const multiPolygon = customType<{
	data: Position[][] | null;
	driverData: GeojsonMultiPolygon | string | null;
	config: { epsg: string };
}>({
	dataType(config) {
		const epsg = typeof config?.epsg !== 'undefined' ? `,${config.epsg}` : ',4326';

		return `geometry(MultiPolygon${epsg})`;
	},

	fromDriver(value: GeojsonMultiPolygon | string | null): MultiPolygon | null {
		if (!value) {
			return null;
		}

		if (typeof value === 'string') {
			const buff = Buffer.from(value, 'hex');
			const geojson = wkx.Geometry.parse(buff).toGeoJSON() as GeojsonMultiPolygon;

			return geojson.coordinates;
		}

		return value.coordinates;
	},

	toDriver(value: MultiPolygon | null): string | null {
		if (!value) {
			return null;
		}

		const polygons = value.map((polygon) => new wkx.Polygon(polygon.map(positionToPoint)));
		const point = new wkx.MultiPolygon(polygons);

		return point.toWkb().toString('hex');
	},
});

export const positionToPostGISPointHex = (value: Position | null): string | null => {
	if (!value) {
		return null;
	}

	const point = positionToPoint(value);

	return point.toWkb().toString('hex');
};

export const point = customType<{
	data: Position | null;
	driverData: GeojsonPoint | string | null;
	config: { epsg: string };
}>({
	dataType(config) {
		const epsg = typeof config?.epsg !== 'undefined' ? `,${config.epsg}` : ',4326';

		return `geometry(Point${epsg})`;
	},

	fromDriver(value: GeojsonPoint | string | null): Position | null {
		// TODO comprendre pourquoi lors de la lecture de la colonne "geolocalisation"
		// par exemple, il y a 2 à 4 lectures pour un seul passage dans le code,
		// et pourquoi la lecture d'un buffer PostGIS (wkb) se fait tantôt sous forme de string
		// représentant le buffer en hexa et tantôt sous forme de point GeoJson parsé
		// (comme si on avait fait `SELECT ST_AsGeoJSON(geolocalisation)` dans le code
		if (!value) {
			return null;
		}

		if (typeof value === 'string') {
			const buff = Buffer.from(value, 'hex');
			const geojson = wkx.Geometry.parse(buff).toGeoJSON() as GeojsonPoint;

			return geojson.coordinates;
		}

		return value.coordinates;
	},

	toDriver(value: Position | null): string | null {
		return positionToPostGISPointHex(value);
	},
});
