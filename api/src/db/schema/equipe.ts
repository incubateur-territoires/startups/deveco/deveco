import {
	pgTable,
	integer,
	varchar,
	text,
	serial,
	boolean,
	timestamp,
	index,
	unique,
	primaryKey,
	customType,
	uuid,
	foreignKey,
} from 'drizzle-orm/pg-core';
import { relations } from 'drizzle-orm';

import { multiPolygon } from './custom-types/postgis';
import {
	commune,
	departement,
	petr,
	territoireIndustrie,
	epci,
	metropole,
	region,
} from './territoire';
import { etabCreaEtiquette } from './etablissement-creation';
import { actionEquipe, actionEtiquette } from './evenement';
import { compte } from './admin';
import { eqEtabEtiquette, devecoEtablissementFavori } from './equipe-etablissement';

import { TerritoireTypeId, EquipeTypeId, EtiquetteTypeId } from '../types';

const etiquetteTypeId = customType<{
	data: EtiquetteTypeId;
	notNull: true;
	default: true;
}>({
	dataType() {
		return 'varchar';
	},
});

export const equipeTypeId = customType<{
	data: EquipeTypeId;
	notNull: true;
	default: true;
}>({
	dataType() {
		return 'varchar';
	},
});

export const territoireTypeId = customType<{
	data: TerritoireTypeId;
	notNull: true;
	default: true;
}>({
	dataType() {
		return 'varchar';
	},
});

export const territoireType = pgTable(
	'territoire_type',
	{
		id: territoireTypeId('id', { length: 20 }).notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkTerritoireType: primaryKey({
			name: 'pk_territoire_type',
			columns: [table.id],
		}),
	})
);

export const equipeType = pgTable(
	'equipe_type',
	{
		id: equipeTypeId('id', { length: 20 }).notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkEquipeType: primaryKey({
			name: 'pk_equipe_type',
			columns: [table.id],
		}),
	})
);

export const etiquetteType = pgTable(
	'etiquette_type',
	{
		id: varchar('id', { length: 20 }).notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkEtiquetteType: primaryKey({
			name: 'pk_etiquette_type',
			columns: [table.id],
		}),
	})
);

export const equipe = pgTable(
	'equipe',
	{
		id: serial('id').notNull(),
		groupement: boolean('groupement').default(false).notNull(),
		nom: varchar('nom').notNull(),
		territoireTypeId: territoireTypeId('territoire_type_id').notNull(),
		typeId: equipeTypeId('equipe_type_id'),
		siret: varchar({ length: 14 }),
		createdAt: timestamp('created_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
		carto: boolean('carto').default(true),
	},
	(table) => ({
		pkEquipe: primaryKey({
			name: 'pk_equipe',
			columns: [table.id],
		}),
		fkEquipeTerritoireType: foreignKey({
			name: 'fk_equipe__territoire_type',
			columns: [table.territoireTypeId],
			foreignColumns: [territoireType.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkEquipeType: foreignKey({
			name: 'fk_equipe__equipe_type',
			columns: [table.typeId],
			foreignColumns: [equipeType.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),

		ukEquipeNom: unique('uk_equipe_nom').on(table.typeId, table.nom),
	})
);

export const equipeRelations = relations(equipe, ({ one, many }) => ({
	territoireType: one(territoireType, {
		fields: [equipe.territoireTypeId],
		references: [territoireType.id],
	}),
	type: one(equipeType, {
		fields: [equipe.typeId],
		references: [equipeType.id],
	}),
	zonages: many(zonage),
	communes: many(equipeCommune),
	epcis: many(equipeEpci),
	metropoles: many(equipeMetropole),
	petrs: many(equipePetr),
	territoireIndustries: many(equipeTerritoireIndustrie),
	departements: many(equipeDepartement),
	regions: many(equipeRegion),
	devecos: many(deveco),
	actions: many(actionEquipe),
}));

export const equipeCommune = pgTable(
	'equipe__commune',
	{
		equipeId: integer('equipe_id').notNull(),
		communeId: varchar('commune_id', { length: 5 }).notNull(),
	},
	(table) => ({
		pkEquipeCommune: primaryKey({ columns: [table.equipeId, table.communeId] }),
		fkEquipeCommuneEquipe: foreignKey({
			name: 'fk_equipe__commune__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEquipeCommuneCommune: foreignKey({
			name: 'fk_equipe__commune__commune',
			columns: [table.communeId],
			foreignColumns: [commune.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
	})
);

export const equipeCommuneRelations = relations(equipeCommune, ({ one }) => ({
	equipe: one(equipe, {
		fields: [equipeCommune.equipeId],
		references: [equipe.id],
	}),
	commune: one(commune, {
		fields: [equipeCommune.communeId],
		references: [commune.id],
	}),
}));

export const equipeEpci = pgTable(
	'equipe__epci',
	{
		equipeId: integer('equipe_id').notNull(),
		epciId: varchar('epci_id', { length: 9 }).notNull(),
	},
	(table) => ({
		pkEquipeEpci: primaryKey({
			name: 'pk_equipe__epci',
			columns: [table.equipeId, table.epciId],
		}),
		fkEquipeEpciEquipe: foreignKey({
			name: 'fk_equipe__epci__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEquipeEpciEpci: foreignKey({
			name: 'fk_equipe__epci__epci',
			columns: [table.epciId],
			foreignColumns: [epci.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
	})
);

export const equipeEpciRelations = relations(equipeEpci, ({ one }) => ({
	equipe: one(equipe, {
		fields: [equipeEpci.equipeId],
		references: [equipe.id],
	}),
	epci: one(epci, {
		fields: [equipeEpci.epciId],
		references: [epci.id],
	}),
}));

export const equipeMetropole = pgTable(
	'equipe__metropole',
	{
		equipeId: integer('equipe_id').notNull(),
		metropoleId: varchar('metropole_id', { length: 5 }).notNull(),
	},
	(table) => ({
		pkEquipeMetropole: primaryKey({
			name: 'pk_equipe__metropole',
			columns: [table.equipeId, table.metropoleId],
		}),
		fkEquipeMetropoleEquipe: foreignKey({
			name: 'fk_equipe__metropole__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEquipeMetropoleMetropole: foreignKey({
			name: 'fk_equipe__metropole__metropole',
			columns: [table.metropoleId],
			foreignColumns: [metropole.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
	})
);

export const equipeMetropoleRelations = relations(equipeMetropole, ({ one }) => ({
	equipe: one(equipe, {
		fields: [equipeMetropole.equipeId],
		references: [equipe.id],
	}),
	metropole: one(metropole, {
		fields: [equipeMetropole.metropoleId],
		references: [metropole.id],
	}),
}));

export const equipePetr = pgTable(
	'equipe__petr',
	{
		equipeId: integer('equipe_id').notNull(),
		petrId: varchar('petr_id', { length: 5 }).notNull(),
	},
	(table) => ({
		pkEquipePetr: primaryKey({
			name: 'pk_equipe__petr',
			columns: [table.equipeId, table.petrId],
		}),
		fkEquipePetrEquipe: foreignKey({
			name: 'fk_equipe__petr__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEquipePetrPetr: foreignKey({
			name: 'fk_equipe__petr__petr',
			columns: [table.petrId],
			foreignColumns: [petr.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
	})
);

export const equipePetrRelations = relations(equipePetr, ({ one }) => ({
	equipe: one(equipe, {
		fields: [equipePetr.equipeId],
		references: [equipe.id],
	}),
	petr: one(petr, {
		fields: [equipePetr.petrId],
		references: [petr.id],
	}),
}));

export const equipeTerritoireIndustrie = pgTable(
	'equipe__territoire_industrie',
	{
		equipeId: integer('equipe_id').notNull(),
		territoireIndustrieId: varchar('territoire_industrie_id', { length: 7 }).notNull(),
	},
	(table) => ({
		pkEquipeTerritoireIndustrie: primaryKey({
			name: 'pk_equipe__territoire_industrie',
			columns: [table.equipeId, table.territoireIndustrieId],
		}),
		fkEquipeTerritoireIndustrieEquipe: foreignKey({
			name: 'fk_equipe__territoire_industrie__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEquipeTerritoireIndustrieTerritoireIndustrie: foreignKey({
			name: 'fk_equipe__territoire_industrie__territoire_industrie',
			columns: [table.territoireIndustrieId],
			foreignColumns: [territoireIndustrie.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
	})
);

export const equipeTerritoireIndustrieRelations = relations(
	equipeTerritoireIndustrie,
	({ one }) => ({
		equipe: one(equipe, {
			fields: [equipeTerritoireIndustrie.equipeId],
			references: [equipe.id],
		}),
		territoireIndustrie: one(territoireIndustrie, {
			fields: [equipeTerritoireIndustrie.territoireIndustrieId],
			references: [territoireIndustrie.id],
		}),
	})
);

export const equipeDepartement = pgTable(
	'equipe__departement',
	{
		equipeId: integer('equipe_id').notNull(),
		departementId: varchar('departement_id', { length: 3 }).notNull(),
	},
	(table) => ({
		pkEquipeDepartement: primaryKey({
			name: 'pk_equipe__departement',
			columns: [table.equipeId, table.departementId],
		}),
		fkEquipeDepartementEquipe: foreignKey({
			name: 'fk_equipe__departement__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEquipeDepartementDepartement: foreignKey({
			name: 'fk_equipe__departement__departement',
			columns: [table.departementId],
			foreignColumns: [departement.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
	})
);

export const equipeDepartementRelations = relations(equipeDepartement, ({ one }) => ({
	equipe: one(equipe, {
		fields: [equipeDepartement.equipeId],
		references: [equipe.id],
	}),
	departement: one(departement, {
		fields: [equipeDepartement.departementId],
		references: [departement.id],
	}),
}));

export const equipeRegion = pgTable(
	'equipe__region',
	{
		equipeId: integer('equipe_id').notNull(),
		regionId: varchar('region_id', { length: 2 }).notNull(),
	},
	(table) => ({
		pkEquipeRegion: primaryKey({
			name: 'pk_equipe__region',
			columns: [table.equipeId, table.regionId],
		}),
		fkEquipeRegionEquipe: foreignKey({
			name: 'fk_equipe__region__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEquipeRegionRegion: foreignKey({
			name: 'fk_equipe__region__region',
			columns: [table.regionId],
			foreignColumns: [region.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
	})
);

export const equipeRegionRelations = relations(equipeRegion, ({ one }) => ({
	equipe: one(equipe, {
		fields: [equipeRegion.equipeId],
		references: [equipe.id],
	}),
	region: one(region, {
		fields: [equipeRegion.regionId],
		references: [region.id],
	}),
}));

export const deveco = pgTable(
	'deveco',
	{
		id: serial('id').notNull(),
		bienvenueEmail: boolean('bienvenue_email').default(false).notNull(),
		compteId: integer('compte_id').notNull(),
		createdAt: timestamp('created_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
		equipeId: integer('equipe_id').notNull(),
		updatedAt: timestamp('updated_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
	},
	(table) => ({
		pkDeveco: primaryKey({
			name: 'pk_deveco',
			columns: [table.id],
		}),
		fkDevecoEquipe: foreignKey({
			name: 'fk_deveco__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkDevecoCompte: foreignKey({
			name: 'fk_deveco__compte',
			columns: [table.compteId],
			foreignColumns: [compte.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const devecoRelations = relations(deveco, ({ one, many }) => ({
	compte: one(compte, {
		fields: [deveco.compteId],
		references: [compte.id],
	}),
	equipe: one(equipe, {
		fields: [deveco.equipeId],
		references: [equipe.id],
	}),
	geoTokens: many(geoToken),
	recherches: many(recherche),
	favoris: many(devecoEtablissementFavori),
	notifications: one(notifications),
}));

export const zonage = pgTable(
	'zonage',
	{
		id: serial('id').notNull(),
		equipeId: integer('equipe_id').notNull(),
		etiquetteId: integer('etiquette_id').notNull(),
		geometrie: multiPolygon('geometrie', { epsg: '4326' }).notNull(),
		description: text('description'),
		enCours: boolean('en_cours').default(true).notNull(),
	},
	(table) => ({
		pkZonage: primaryKey({
			name: 'pk_zonage',
			columns: [table.id],
		}),
		fkZonageEquipe: foreignKey({
			name: 'fk_zonage__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkZonageEtiquette: foreignKey({
			name: 'fk_zonage__etiquette',
			columns: [table.etiquetteId],
			foreignColumns: [etiquette.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		idxGeometrie: index('idx_zonage__geometrie').on(table.geometrie),
	})
);

export const zonageRelations = relations(zonage, ({ one }) => ({
	equipe: one(equipe, {
		fields: [zonage.equipeId],
		references: [equipe.id],
	}),
	etiquette: one(etiquette, {
		fields: [zonage.etiquetteId],
		references: [etiquette.id],
	}),
}));

export const etiquette = pgTable(
	'etiquette',
	{
		id: serial('id').notNull(),
		equipeId: integer('equipe_id').notNull(),
		nom: varchar('nom').notNull(),
		typeId: etiquetteTypeId('etiquette_type_id', { length: 16 }).notNull(),
	},
	(table) => ({
		pkEtiquette: primaryKey({
			name: 'pk_etiquette',
			columns: [table.id],
		}),
		fkEtiquetteEquipe: foreignKey({
			name: 'fk_etiquette__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkEtiquetteType: foreignKey({
			name: 'fk_etiquette__etiquette_type',
			columns: [table.typeId],
			foreignColumns: [etiquetteType.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		ukEtiquette: unique('uk_etiquette').on(table.typeId, table.nom, table.equipeId),
	})
);

export const etiquetteRelations = relations(etiquette, ({ one, many }) => ({
	type: one(etiquetteType, {
		fields: [etiquette.typeId],
		references: [etiquetteType.id],
	}),
	equipe: one(equipe, {
		fields: [etiquette.equipeId],
		references: [equipe.id],
	}),
	evenement: one(actionEtiquette, {
		fields: [etiquette.id],
		references: [actionEtiquette.etiquetteId],
	}),
	eqEtabEtiquettes: many(eqEtabEtiquette),
	etabCreaEtiquettes: many(etabCreaEtiquette),
	zonage: one(zonage),
}));

export const geoToken = pgTable(
	'geo_token',
	{
		id: serial('id').notNull(),
		active: boolean('active').default(true).notNull(),
		devecoId: integer('deveco_id').notNull(),
		token: varchar('token').notNull(),
	},
	(table) => ({
		pkGeoToken: primaryKey({
			name: 'pk_geo_token',
			columns: [table.id],
		}),
		fkGeoTokenDeveco: foreignKey({
			name: 'fk_geo_token__deveco',
			columns: [table.devecoId],
			foreignColumns: [deveco.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const geoTokenRelations = relations(geoToken, ({ many, one }) => ({
	deveco: one(deveco, {
		fields: [geoToken.devecoId],
		references: [deveco.id],
	}),
	requests: many(geoTokenRequest),
}));

export const geoTokenRequest = pgTable(
	'geo_token_request',
	{
		id: serial('id').notNull(),
		createdAt: timestamp('created_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
		geoTokenId: integer('geo_token_id'),
		url: varchar('url').notNull(),
		userAgent: varchar('user_agent'),
	},
	(table) => ({
		pkGeoTokenRequest: primaryKey({
			name: 'pk_geo_token_request',
			columns: [table.id],
		}),
		fkGeoTokenRequestGeoToken: foreignKey({
			name: 'fk_geo_token_request__geo_token',
			columns: [table.geoTokenId],
			foreignColumns: [geoToken.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const geoTokenRequestRelations = relations(geoTokenRequest, ({ one }) => ({
	geoToken: one(geoToken, {
		fields: [geoTokenRequest.geoTokenId],
		references: [geoToken.id],
	}),
}));

export const recherche = pgTable(
	'recherche',
	{
		id: serial('id').notNull(),
		createdAt: timestamp('created_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
		devecoId: integer('deveco_id').notNull(),
		entite: varchar('entite').notNull(),
		format: varchar('format'),
		requete: varchar('requete').notNull(),
	},
	(table) => ({
		pkRecherche: primaryKey({
			name: 'pk_recherche',
			columns: [table.id],
		}),
		fkRechercheDeveco: foreignKey({
			name: 'fk_recherche__deveco',
			columns: [table.devecoId],
			foreignColumns: [deveco.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const rechercheRelations = relations(recherche, ({ one }) => ({
	deveco: one(deveco, {
		fields: [recherche.devecoId],
		references: [deveco.id],
	}),
}));

export const rechercheSauvegardee = pgTable('recherche_sauvegardee', {
	id: serial('id').notNull(),
	token: uuid('token').notNull(),
	creationDate: timestamp('creation_date', { withTimezone: true, mode: 'date' })
		.defaultNow()
		.notNull(),
	equipeId: integer('equipe_id').notNull(),
	devecoId: integer('deveco_id').notNull(),
	query: varchar('query').notNull(),
});

export const rechercheSauvegardeeRelations = relations(rechercheSauvegardee, ({ one }) => ({
	equipe: one(equipe, {
		fields: [rechercheSauvegardee.equipeId],
		references: [equipe.id],
	}),
	deveco: one(deveco, {
		fields: [rechercheSauvegardee.devecoId],
		references: [deveco.id],
	}),
}));

export const equipeCommuneVue = pgTable('equipe__commune_vue', {
	equipeId: integer('eqcomvue_equipe_id').notNull(),
	communeId: varchar('eqcomvue_commune_id', { length: 5 }).notNull(),
});

// .as((db) =>
// db
//		// distinct car certaines communes peuvent être en double car enfant d'une autre
//		.selectDistinct({
// 		// qui semble ne pas qualifier complètement les colonnes des tables matérialisées (?)
// 		equipeId: sql<Equipe['id']>`${equipe.id}`.as('eqcomvue_equipe_id'),
// 		communeId: sql<Commune['id']>`${commune.id}`.as('eqcomvue_commune_id'),
// 	})
// 	.from(equipe)
// 	.leftJoin(equipeCommune, eq(equipeCommune.equipeId, equipe.id))
// 	.leftJoin(equipeMetropole, eq(equipeMetropole.equipeId, equipe.id))
// 	.leftJoin(equipeEpci, eq(equipeEpci.equipeId, equipe.id))
// 	.leftJoin(equipePetr, eq(equipePetr.equipeId, equipe.id))
// 	.leftJoin(equipeTerritoireIndustrie, eq(equipeTerritoireIndustrie.equipeId, equipe.id))
// 	.leftJoin(equipeDepartement, eq(equipeDepartement.equipeId, equipe.id))
// 	.leftJoin(equipeRegion, eq(equipeRegion.equipeId, equipe.id))
// 	.leftJoin(
// 		commune,
// 		or(
// 			eq(commune.id, equipeCommune.communeId),
// 			eq(commune.parentId, equipeCommune.communeId),
// 			eq(commune.metropoleId, equipeMetropole.metropoleId),
// 			eq(commune.epciId, equipeEpci.epciId),
// 			eq(commune.petrId, equipePetr.petrId),
// 			eq(commune.territoireIndustrieId, equipeTerritoireIndustrie.territoireIndustrieId),
// 			eq(commune.departementId, equipeDepartement.departementId),
// 			eq(commune.regionId, equipeRegion.regionId)
// 		)
// 	)
// );

export const rechercheEnregistree = pgTable(
	'recherche_enregistree',
	{
		id: serial('id').notNull(),
		nom: varchar('nom').notNull(),
		url: varchar('url').notNull(),
		devecoId: integer('deveco_id').notNull(),
		createdAt: timestamp('created_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
	},
	(table) => ({
		pkRechercheEnregistree: primaryKey({
			name: 'pk_recherche_enregistree',
			columns: [table.id],
		}),
		fkRechercheEnregistreeDeveco: foreignKey({
			name: 'fk_recherche_enregistree__deveco',
			columns: [table.devecoId],
			foreignColumns: [deveco.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const rechercheEnregistreeRelations = relations(rechercheEnregistree, ({ one }) => ({
	deveco: one(deveco, {
		fields: [rechercheEnregistree.devecoId],
		references: [deveco.id],
	}),
}));

export const notifications = pgTable(
	'notifications',
	{
		id: serial('id').notNull(),
		devecoId: integer('deveco_id').notNull(),
		connexion: boolean('connexion').default(true).notNull(),
		rappels: boolean('rappels').default(true).notNull(),
		activite: boolean('activite').default(true).notNull(),
	},
	(table) => ({
		pkNotifications: primaryKey({
			name: 'pk_notifications',
			columns: [table.id],
		}),
		fkNotificationsDeveco: foreignKey({
			name: 'fk_notifications__deveco',
			columns: [table.devecoId],
			foreignColumns: [deveco.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		ukNotifications: unique('uk_notifications_deveco').on(table.devecoId),
	})
);

export const notificationsRelations = relations(notifications, ({ one }) => ({
	deveco: one(deveco, {
		fields: [notifications.devecoId],
		references: [deveco.id],
	}),
}));
