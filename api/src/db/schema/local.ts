import {
	pgTable,
	integer,
	index,
	doublePrecision,
	varchar,
	primaryKey,
	unique,
	foreignKey,
	customType,
	date,
	boolean,
} from 'drizzle-orm/pg-core';
import { relations } from 'drizzle-orm';

import { entreprise } from './etablissement';
import { adresseColumns } from './_columns';
import { commune } from './territoire';
import { equipeCommuneVue } from './equipe';

import * as eqLocSchema from '../schema/equipe-local';
import { LocalNatureTypeId, LocalCategorieTypeId, LocalMutationNatureTypeId } from '../types';

export const localCategorieTypeId = customType<{
	data: LocalCategorieTypeId;
	notNull: true;
	default: true;
}>({
	dataType() {
		return 'varchar';
	},
});

export const localNatureTypeId = customType<{
	data: LocalNatureTypeId;
	notNull: true;
	default: true;
}>({
	dataType() {
		return 'varchar';
	},
});

export const localMutationNatureTypeId = customType<{
	data: LocalMutationNatureTypeId;
	notNull: true;
	default: true;
}>({
	dataType() {
		return 'integer';
	},
});

export const local = pgTable(
	'local',
	{
		id: varchar('id', { length: 12 }).notNull(),
		invariant: varchar('invariant', { length: 10 }).notNull(),
		natureTypeId: localNatureTypeId('local_nature_type_id', { length: 2 }).notNull(),
		categorieTypeId: localCategorieTypeId('local_categorie_type_id', { length: 4 }),
		communeId: varchar('commune_id', { length: 5 }),
		section: varchar('section', { length: 2 }),
		parcelle: varchar('parcelle', { length: 4 }),
		niveau: varchar('niveau', { length: 2 }),
		surfaceVente: integer('surface_vente'),
		surfaceReserve: integer('surface_reserve'),
		surfaceExterieureNonCouverte: integer('surface_exterieure_non_couverte'),
		surfaceStationnementCouvert: integer('surface_stationnement_couvert'),
		surfaceStationnementNonCouvert: integer('surface_stationnement_non_couvert'),
		debutAnnee: integer('debut_annee').notNull(),
		annee: integer('annee').notNull(),
		actif: boolean('actif').notNull(),
	},
	(table) => ({
		pklocal: primaryKey({
			name: 'pk_local',
			columns: [table.id],
		}),
		fkLocalCategorieType: foreignKey({
			name: 'fk_local__local_categorie_type',
			columns: [table.categorieTypeId],
			foreignColumns: [localCategorieType.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkLocalNatureType: foreignKey({
			name: 'fk_local__local_nature_type',
			columns: [table.natureTypeId],
			foreignColumns: [localNatureType.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkLocalCommune: foreignKey({
			name: 'fk_local__commune',
			columns: [table.communeId],
			foreignColumns: [commune.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
	})
);

export const localRelations = relations(local, ({ one, many }) => ({
	categorieType: one(localCategorieType, {
		fields: [local.categorieTypeId],
		references: [localCategorieType.id],
	}),
	natureType: one(localNatureType, {
		fields: [local.natureTypeId],
		references: [localNatureType.id],
	}),
	commune: one(commune, {
		fields: [local.communeId],
		references: [commune.id],
	}),
	propPPhys: many(localProprietairePersonnePhysique),
	propPMors: many(localProprietairePersonneMorale),
	adresse: one(localAdresse, {
		fields: [local.id],
		references: [localAdresse.localId],
	}),
	mutations: many(localMutationLocal),

	// si la commune est dans l'équipe ou non
	equipeCommune: one(equipeCommuneVue, {
		fields: [local.communeId],
		references: [equipeCommuneVue.communeId],
	}),
	// données perso
	favoris: many(eqLocSchema.devecoLocalFavori),
	contributions: many(eqLocSchema.eqLocContribution),
	contributionsGeolocs: many(eqLocSchema.eqLocContribution),
	etiquettes: many(eqLocSchema.eqLocEtiquette),
	contacts: many(eqLocSchema.eqLocContact),
	occupants: many(eqLocSchema.eqLocOccupant),
	ressources: many(eqLocSchema.eqLocRessource),
	demandes: many(eqLocSchema.eqLocDemande),
	echanges: many(eqLocSchema.eqLocEchange),
	rappels: many(eqLocSchema.eqLocRappel),
}));

export const localNatureType = pgTable(
	'local_nature_type',
	{
		id: localNatureTypeId('id', { length: 2 }).notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkLocalType: primaryKey({
			name: 'pk_local_nature_type',
			columns: [table.id],
		}),
	})
);

export const localCategorieType = pgTable(
	'local_categorie_type',
	{
		id: localCategorieTypeId('id', { length: 4 }).notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkLocalType: primaryKey({
			name: 'pk_local_categorie_type',
			columns: [table.id],
		}),
	})
);

export const localProprietairePersonnePhysique = pgTable(
	'local__proprietaire_personne_physique',
	{
		localId: varchar('local_id', { length: 12 }).notNull(),
		proprietairePersonnePhysiqueId: varchar('proprietaire_personne_physique_id', {
			length: 13,
		}).notNull(),
	},
	(table) => ({
		pkLocalProprietairePersonnePhysique: primaryKey({
			name: 'pk_local__proprietaire_personne_physique',
			columns: [table.proprietairePersonnePhysiqueId, table.localId],
		}),
		idxLocalProprietairePersonnePhysiqueLocal: index(
			'idx_local__proprietaire_personne_physique__local'
		).on(table.localId),
		idxLocalProprietairePersonnePhysiqueProprio: index(
			'idx_local__proprietaire_personne_physique__proprio'
		).on(table.proprietairePersonnePhysiqueId),
		fkLocalProprietairePersonneProprietairePersonne: foreignKey({
			name: 'fk_local__proprietaire_personne_physique__ppp',
			columns: [table.proprietairePersonnePhysiqueId],
			foreignColumns: [proprietairePersonnePhysique.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkLocalProprietairePersonneLocal: foreignKey({
			name: 'fk_local__proprietaire_personne_physique__local',
			columns: [table.localId],
			foreignColumns: [local.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const localProprietairePersonneRelations = relations(
	localProprietairePersonnePhysique,
	({ one }) => ({
		local: one(local, {
			fields: [localProprietairePersonnePhysique.localId],
			references: [local.id],
		}),
		propPPhy: one(proprietairePersonnePhysique, {
			fields: [localProprietairePersonnePhysique.proprietairePersonnePhysiqueId],
			references: [proprietairePersonnePhysique.id],
		}),
	})
);

export const proprietairePersonnePhysique = pgTable(
	'proprietaire_personne_physique',
	{
		id: varchar('id', { length: 13 }).notNull(),
		proprietaireCompteId: varchar('proprietaire_compte_id', {
			length: 11,
		}).notNull(),
		nom: varchar('nom').notNull(),
		prenom: varchar('prenom'),
		naissanceDate: date('naissance_date', { mode: 'date' }),
		naissanceNom: varchar('naissance_nom'),
		naissanceLieu: varchar('naissance_lieu'),
		prenoms: varchar('prenoms'),
		debutAnnee: integer('debut_annee').notNull(),
		annee: integer('annee').notNull(),
	},
	(table) => ({
		pkLocalProprietairePersonne: primaryKey({
			name: 'pk_proprietaire_personne_physique',
			columns: [table.id],
		}),
	})
);

export const proprietairePersonneRelations = relations(
	proprietairePersonnePhysique,
	({ many }) => ({
		proprietes: many(localProprietairePersonnePhysique),
	})
);

export const proprietairePersonneMorale = pgTable(
	'proprietaire_personne_morale',
	{
		id: varchar('id', { length: 13 }).notNull(),
		proprietaireCompteId: varchar('proprietaire_compte_id', {
			length: 11,
		}).notNull(),
		entrepriseId: varchar('entreprise_id', {
			length: 9,
		}),
		nom: varchar('nom'),
		debutAnnee: integer('debut_annee').notNull(),
		annee: integer('annee').notNull(),
	},
	(table) => ({
		pkProprietairePersonneMorale: primaryKey({
			name: 'pk_proprietaire_personne_morale',
			columns: [table.id],
		}),
	})
);

export const proprietairePersonneMoraleRelations = relations(
	proprietairePersonneMorale,
	({ one, many }) => ({
		ul: one(entreprise, {
			fields: [proprietairePersonneMorale.entrepriseId],
			references: [entreprise.siren],
		}),
		props: many(localProprietairePersonneMorale),
	})
);

export const localProprietairePersonneMorale = pgTable(
	'local__proprietaire_personne_morale',
	{
		proprietairePersonneMoraleId: varchar('proprietaire_personne_morale_id', {
			length: 13,
		}).notNull(),
		localId: varchar('local_id', { length: 12 }).notNull(),
	},
	(table) => ({
		pkLocalProprietairePersonneMorale: primaryKey({
			name: 'pk_local__proprietaire_personne_morale',
			columns: [table.proprietairePersonneMoraleId, table.localId],
		}),
		idxLocalProprietairePersonneMoraleLocal: index(
			'idx_local__proprietaire_personne_morale__local'
		).on(table.localId),
		idxLocalProprietairePersonneMoraleProprio: index(
			'idx_local__proprietaire_personne_morale__proprio'
		).on(table.proprietairePersonneMoraleId),
		fkLocalProprietairePersonneMoraleLocal: foreignKey({
			name: 'fk_local__proprietaire_personne_morale__local',
			columns: [table.localId],
			foreignColumns: [local.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
		fkLocalProprietairePersonneMoralePersonneMorale: foreignKey({
			name: 'fk_local__proprietaire_personne_morale__ppm',
			columns: [table.proprietairePersonneMoraleId],
			foreignColumns: [proprietairePersonneMorale.id],
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const localProprietairePersonneMoraleRelations = relations(
	localProprietairePersonneMorale,
	({ one }) => ({
		local: one(local, {
			fields: [localProprietairePersonneMorale.localId],
			references: [local.id],
		}),
		propPMor: one(proprietairePersonneMorale, {
			fields: [localProprietairePersonneMorale.proprietairePersonneMoraleId],
			references: [proprietairePersonneMorale.id],
		}),
	})
);

export const localAdresse = pgTable(
	'local_adresse',
	{
		...adresseColumns,
		communeId: varchar('commune_id', { length: 5 }).notNull(),
		localId: varchar('local_id', { length: 12 }).notNull(),
	},
	(table) => ({
		pkLocalAdresse: primaryKey({
			name: 'pk_local_adresse',
			columns: [table.id],
		}),
		ukLocalAdresse: unique('uk_local_adresse').on(table.localId),
		fkLocalAdresseCommune: foreignKey({
			name: 'fk_local_adresse__commune',
			columns: [table.communeId],
			foreignColumns: [commune.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const localAdresseRelations = relations(localAdresse, ({ one }) => ({
	local: one(local, {
		fields: [localAdresse.localId],
		references: [local.id],
	}),
	commune: one(commune, {
		fields: [localAdresse.communeId],
		references: [commune.id],
	}),
}));

export const localMutation = pgTable(
	'local_mutation',
	{
		id: integer('id').notNull(),
		natureTypeId: localMutationNatureTypeId('local_mutation_nature_type_id').notNull(),
		date: date('date', { mode: 'date' }).notNull(),
		valeur: doublePrecision('valeur'),
	},
	(table) => ({
		pklocalMutation: primaryKey({
			name: 'pk_local_mutation',
			columns: [table.id],
		}),
		fkLocalMutationNatureType: foreignKey({
			name: 'fk_local_mutation_nature_type',
			columns: [table.natureTypeId],
			foreignColumns: [localMutationNatureType.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
	})
);

export const localMutationRelations = relations(localMutation, ({ one, many }) => ({
	natureType: one(localMutationNatureType, {
		fields: [localMutation.natureTypeId],
		references: [localMutationNatureType.id],
	}),
	locaux: many(localMutationLocal),
}));

export const localMutationNatureType = pgTable(
	'local_mutation_nature_type',
	{
		id: localMutationNatureTypeId('id').notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkLocalMutationType: primaryKey({
			name: 'pk_local_mutation_nature_type',
			columns: [table.id],
		}),
	})
);

export const localMutationLocal = pgTable(
	'local__local_mutation',
	{
		localId: varchar('local_id', { length: 12 }).notNull(),
		localMutationId: integer('local_mutation_id').notNull(),
	},
	(table) => ({
		pklocalMutationLocal: primaryKey({
			name: 'pk_local__local_mutation',
			columns: [table.localId, table.localMutationId],
		}),
		// pas de foreignKey sur localId
		// car on a des mutations sur des locaux qui ne sont pas dans la base
		// fkLocalMutationLocalLocal: foreignKey({
		// 	name: 'fk_local__local_mutation__local',
		// 	columns: [table.localId],
		// 	foreignColumns: [local.id],
		// })
		// 	.onDelete('restrict')
		// 	.onUpdate('cascade'),
		fkLocalMutationLocalMutation: foreignKey({
			name: 'fk_local__local_mutation__mutation',
			columns: [table.localMutationId],
			foreignColumns: [localMutation.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
	})
);

export const localMutationLocalRelations = relations(localMutationLocal, ({ one }) => ({
	local: one(local, {
		fields: [localMutationLocal.localId],
		references: [local.id],
	}),
	mutation: one(localMutation, {
		fields: [localMutationLocal.localMutationId],
		references: [localMutation.id],
	}),
}));
