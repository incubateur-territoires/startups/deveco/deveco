import {
	pgTable,
	integer,
	varchar,
	serial,
	foreignKey,
	timestamp,
	primaryKey,
	customType,
	index,
	jsonb,
} from 'drizzle-orm/pg-core';
import { relations } from 'drizzle-orm';

import { etabCrea, etabCreaCreateur } from './etablissement-creation';
import { equipe, etiquette, zonage } from './equipe';
import { demande, echange, rappel, brouillon } from './suivi';
import { compte } from './admin';
import { eqEtabContact } from './equipe-etablissement';
import { eqLocContact, eqLocOccupant } from './equipe-local';
import { entreprise, etablissement } from './etablissement';
import { contact } from './contact';
import { local } from './local';

import { ActionTypeId, EvenementTypeId } from '../types';

/*
 EVENEMENT
*/

const evenementTypeId = customType<{
	data: EvenementTypeId;
	notNull: true;
	default: false;
}>({
	dataType() {
		return 'varchar';
	},
});

export const evenement = pgTable(
	'evenement',
	{
		id: serial('id').notNull(),
		createdAt: timestamp('created_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
		compteId: integer('compte_id').notNull(),
		typeId: evenementTypeId('evenement_type_id').notNull(),
		log: varchar('log'),
		contexte: jsonb('contexte'),
	},
	(table) => ({
		pkEvenement: primaryKey({
			name: 'pk_evenement',
			columns: [table.id],
		}),
		fkEvenementCompte: foreignKey({
			name: 'fk_evenement_compte',
			columns: [table.compteId],
			foreignColumns: [compte.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),

		idxEvenementType: index('idx_evenement__type').on(table.typeId),
	})
);

export const evenementRelations = relations(evenement, ({ one, many }) => ({
	compte: one(compte, {
		fields: [evenement.compteId],
		references: [compte.id],
	}),
	actionComptes: many(actionCompte),
	actionEquipeEtablissementContacts: many(actionEquipeEtablissementContact),
	actionEtiquettes: many(actionEtiquette),
	actionDemandes: many(actionDemande),
	actionEchanges: many(actionEchange),
	actionRappels: many(actionRappel),
	actionBrouillons: many(actionBrouillon),
	actionLocaux: many(actionEquipeLocal),
	actionEntreprises: many(actionEntreprise),
	actionEtablissements: many(actionEtablissement),
	actionEtiquetteEtablissements: many(actionEtiquetteEtablissement),
	actionEtiquetteEtablissementCreations: many(actionEtiquetteEtablissementCreation),
	actionEquipeEtablissements: many(actionEquipeEtablissement),
	actionEtablissementCreations: many(actionEtablissementCreation),
	actionProprietaireContacts: many(actionEquipeLocalContact),
}));

/*
 ACTION
*/

const actionTypeId = customType<{
	data: ActionTypeId;
	notNull: true;
	default: false;
}>({
	dataType() {
		return 'varchar';
	},
});

export const actionType = pgTable(
	'action_type',
	{
		id: varchar('id').notNull(),
		nom: varchar('nom'),
	},
	(table) => ({
		pkActionType: primaryKey({
			name: 'pk_action_type',
			columns: [table.id],
		}),
	})
);

const actionColumns = {
	evenementId: integer('evenement_id').notNull(),
	createdAt: timestamp('created_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
	typeId: actionTypeId('action_type_id').notNull(),
	diff: jsonb('diff'),
};

/*
 ACTION contact
*/

export const actionContact = pgTable(
	'action_contact',
	{
		...actionColumns,
		contactId: integer('contact_id').notNull(),
	},
	(table) => ({
		idxActionContact: index('idx_action_contact').on(
			table.evenementId,
			table.contactId,
			table.typeId
		),
		fkActionContactEvenement: foreignKey({
			name: 'fk_action_contact__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionContactActionType: foreignKey({
			name: 'fk_action_contact__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionContactRelations = relations(actionContact, ({ one }) => ({
	evenement: one(evenement, {
		fields: [actionContact.evenementId],
		references: [evenement.id],
	}),
	contact: one(contact, {
		fields: [actionContact.contactId],
		references: [contact.id],
	}),
}));

/*
 ACTION EQUIPE
*/

export const actionEquipe = pgTable(
	'action_equipe',
	{
		...actionColumns,
		equipeId: integer('equipe_id').notNull(),
	},
	(table) => ({
		idxActionEquipe: index('idx_action_equipe').on(table.evenementId, table.equipeId, table.typeId),
		fkActionEquipeEquipe: foreignKey({
			name: 'fk_action_equipe__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEquipeEvenement: foreignKey({
			name: 'fk_action_equipe__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEquipeActionType: foreignKey({
			name: 'fk_action_equipe__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionEquipeRelations = relations(actionEquipe, ({ one }) => ({
	evenement: one(evenement, {
		fields: [actionEquipe.evenementId],
		references: [evenement.id],
	}),
	equipe: one(equipe, {
		fields: [actionEquipe.equipeId],
		references: [equipe.id],
	}),
}));

/*
 ACTION COMPTE
*/

export const actionCompte = pgTable(
	'action_compte',
	{
		...actionColumns,
		compteId: integer('compte_id').notNull(),
	},
	(table) => ({
		idxActionCompte: index('idx_action_compte').on(table.evenementId, table.compteId, table.typeId),
		fkActionCompteCompte: foreignKey({
			name: 'fk_action_compte__compte',
			columns: [table.compteId],
			foreignColumns: [compte.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
		fkActionCompteEvenement: foreignKey({
			name: 'fk_action_compte__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionCompteActionType: foreignKey({
			name: 'fk_action_compte__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionCompteRelations = relations(actionCompte, ({ one }) => ({
	evenement: one(evenement, {
		fields: [actionCompte.evenementId],
		references: [evenement.id],
	}),
	compte: one(compte, {
		fields: [actionCompte.compteId],
		references: [compte.id],
	}),
}));

/*
 ACTION ECHANGE
*/

export const actionEchange = pgTable(
	'action_echange',
	{
		...actionColumns,
		echangeId: integer('echange_id').notNull(),
	},
	(table) => ({
		idxActionEchange: index('idx_action_echange').on(
			table.evenementId,
			table.echangeId,
			table.typeId
		),
		fkActionEchangeEchange: foreignKey({
			name: 'fk_action_echange__echange',
			columns: [table.echangeId],
			foreignColumns: [echange.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEchangeEvenement: foreignKey({
			name: 'fk_action_echange__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEchangeActionType: foreignKey({
			name: 'fk_action_echange__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionEchangeRelations = relations(actionEchange, ({ one }) => ({
	evenement: one(evenement, {
		fields: [actionEchange.evenementId],
		references: [evenement.id],
	}),
	echange: one(echange, {
		fields: [actionEchange.echangeId],
		references: [echange.id],
	}),
}));

/*
 ACTION DEMANDE
*/

export const actionDemande = pgTable(
	'action_demande',
	{
		...actionColumns,
		demandeId: integer('demande_id').notNull(),
	},
	(table) => ({
		idxActionDemande: index('idx_action_demande').on(
			table.evenementId,
			table.demandeId,
			table.typeId
		),
		fkActionDemandeDemande: foreignKey({
			name: 'fk_action_demande__demande',
			columns: [table.demandeId],
			foreignColumns: [demande.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionDemandeEvenement: foreignKey({
			name: 'fk_action_demande__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionDemandeActionType: foreignKey({
			name: 'fk_action_demande__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionDemandeRelations = relations(actionDemande, ({ one }) => ({
	evenement: one(evenement, {
		fields: [actionDemande.evenementId],
		references: [evenement.id],
	}),
	demande: one(demande, {
		fields: [actionDemande.demandeId],
		references: [demande.id],
	}),
}));

/*
 ACTION RAPPEL
*/

export const actionRappel = pgTable(
	'action_rappel',
	{
		...actionColumns,
		rappelId: integer('rappel_id').notNull(),
	},
	(table) => ({
		idxActionRappel: index('idx_action_rappel').on(table.evenementId, table.rappelId, table.typeId),
		fkActionRappelRappel: foreignKey({
			name: 'fk_action_rappel__rappel',
			columns: [table.rappelId],
			foreignColumns: [rappel.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionRappelEvenement: foreignKey({
			name: 'fk_action_rappel__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionRappelActionType: foreignKey({
			name: 'fk_action_rappel__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionRappelRelations = relations(actionRappel, ({ one }) => ({
	evenement: one(evenement, {
		fields: [actionRappel.evenementId],
		references: [evenement.id],
	}),
	rappel: one(rappel, {
		fields: [actionRappel.rappelId],
		references: [rappel.id],
	}),
}));

/*
 ACTION BROUILLON
*/

export const actionBrouillon = pgTable(
	'action_brouillon',
	{
		...actionColumns,
		brouillonId: integer('brouillon_id').notNull(),
	},
	(table) => ({
		idxActionBrouillon: index('idx_action_brouillon').on(
			table.evenementId,
			table.brouillonId,
			table.typeId
		),
		fkActionBrouillonBrouillon: foreignKey({
			name: 'fk_action_brouillon__brouillon',
			columns: [table.brouillonId],
			foreignColumns: [brouillon.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionBrouillonEvenement: foreignKey({
			name: 'fk_action_brouillon__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionBrouillonActionType: foreignKey({
			name: 'fk_action_brouillon__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionBrouillonRelations = relations(actionBrouillon, ({ one }) => ({
	evenement: one(evenement, {
		fields: [actionBrouillon.evenementId],
		references: [evenement.id],
	}),
	brouillon: one(brouillon, {
		fields: [actionBrouillon.brouillonId],
		references: [brouillon.id],
	}),
}));

/*
 ACTION CONTACT
*/

export const actionEquipeEtablissementContact = pgTable(
	'action_equipe_etablissement_contact',
	{
		...actionColumns,
		equipeId: integer('equipe_id').notNull(),
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
		contactId: integer('contact_id').notNull(),
	},
	(table) => ({
		idxActionContact: index('idx_action_equipe_etablissement_contact').on(
			table.evenementId,
			table.equipeId,
			table.etablissementId,
			table.contactId,
			table.typeId
		),
		fkActionContactContact: foreignKey({
			name: 'fk_action_equipe_etablissement_contact__contact',
			columns: [table.equipeId, table.etablissementId, table.contactId],
			foreignColumns: [
				eqEtabContact.equipeId,
				eqEtabContact.etablissementId,
				eqEtabContact.contactId,
			],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionContactEvenement: foreignKey({
			name: 'fk_action_equipe_etablissement_contact__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionContactActionType: foreignKey({
			name: 'fk_action_equipe_etablissement_contact__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionEquipeEtablissementContactRelations = relations(
	actionEquipeEtablissementContact,
	({ one }) => ({
		evenement: one(evenement, {
			fields: [actionEquipeEtablissementContact.evenementId],
			references: [evenement.id],
		}),
		contact: one(eqEtabContact, {
			fields: [
				actionEquipeEtablissementContact.contactId,
				actionEquipeEtablissementContact.equipeId,
				actionEquipeEtablissementContact.etablissementId,
			],
			references: [eqEtabContact.contactId, eqEtabContact.equipeId, eqEtabContact.etablissementId],
		}),
	})
);

/*
 ACTION ZONAGE
*/

export const actionZonage = pgTable(
	'action_zonage',
	{
		...actionColumns,
		zonageId: integer('zonage_id').notNull(),
	},
	(table) => ({
		idxActionZonage: index('idx_action_zonage').on(table.evenementId, table.zonageId, table.typeId),
		fkActionZonageZonage: foreignKey({
			name: 'fk_action_zonage__zonage',
			columns: [table.zonageId],
			foreignColumns: [zonage.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionZonageEvenement: foreignKey({
			name: 'fk_action_zonage__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActioneEiquetteActionType: foreignKey({
			name: 'fk_action_zonage__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

/*
 ACTION ETIQUETTE
*/

export const actionEtiquette = pgTable(
	'action_etiquette',
	{
		...actionColumns,
		etiquetteId: integer('etiquette_id').notNull(),
	},
	(table) => ({
		idxActionEtiquette: index('idx_action_etiquette').on(
			table.evenementId,
			table.etiquetteId,
			table.typeId
		),
		fkActionEtiquetteEtiquette: foreignKey({
			name: 'fk_action_etiquette__etiquette',
			columns: [table.etiquetteId],
			foreignColumns: [etiquette.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEtiquetteEvenement: foreignKey({
			name: 'fk_action_etiquette__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActioneEiquetteActionType: foreignKey({
			name: 'fk_action_etiquette__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionEtiquetteRelations = relations(actionEtiquette, ({ one }) => ({
	evenement: one(evenement, {
		fields: [actionEtiquette.evenementId],
		references: [evenement.id],
	}),
	etiquette: one(etiquette, {
		fields: [actionEtiquette.etiquetteId],
		references: [etiquette.id],
	}),
}));

/*
 ACTION ETABLISSEMENT CREATION
*/

export const actionEtablissementCreation = pgTable(
	'action_etablissement_creation',
	{
		...actionColumns,
		etabCreaId: integer('etablissement_creation_id').notNull(),
	},
	(table) => ({
		idxActionEtablissementCreation: index('idx_action_etablissement_creation').on(
			table.evenementId,
			table.etabCreaId,
			table.typeId
		),
		fkActionEtablissementCreationEtablissementCreation: foreignKey({
			name: 'fk_action_etablissement_creation__etablissement_creation',
			columns: [table.etabCreaId],
			foreignColumns: [etabCrea.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEtablissementCreationEvenement: foreignKey({
			name: 'fk_action_etablissement_creation__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEtablissementCreationActionType: foreignKey({
			name: 'fk_action_etablissement_creation__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionEtablissementCreationRelations = relations(
	actionEtablissementCreation,
	({ one }) => ({
		evt: one(evenement, {
			fields: [actionEtablissementCreation.evenementId],
			references: [evenement.id],
		}),
		creation: one(etabCrea, {
			fields: [actionEtablissementCreation.etabCreaId],
			references: [etabCrea.id],
		}),
	})
);

/*
 ACTION ETABLISSEMENT CREATION CREATEUR
*/

export const actionEtablissementCreationCreateur = pgTable(
	'action_etablissement_creation_createur',
	{
		...actionColumns,
		equipeId: integer('equipe_id').notNull(),
		etabCreaId: integer('etablissement_creation_id').notNull(),
		contactId: integer('createur_id').notNull(),
	},
	(table) => ({
		idxActionEtablissementCreationCreateur: index('idx_action_etablissement_creation_createur').on(
			table.evenementId,
			table.etabCreaId,
			table.typeId
		),
		fkActionEtablissementCreationCreateurEquipe: foreignKey({
			name: 'fk_action_etablissement_creation_createur__equipe',
			columns: [table.etabCreaId],
			foreignColumns: [etabCrea.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEtablissementCreationEtablissementCreateurCreation: foreignKey({
			name: 'fk_action_etablissement_creation_createur__etab_crea',
			columns: [table.etabCreaId],
			foreignColumns: [etabCrea.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEtablissementCreationCreateurEvenement: foreignKey({
			name: 'fk_action_etablissement_creation_createur__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEtablissementCreationCreateurActionType: foreignKey({
			name: 'fk_action_etablissement_creation_createur__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionEtablissementCreationCreateurRelations = relations(
	actionEtablissementCreationCreateur,
	({ one }) => ({
		evt: one(evenement, {
			fields: [actionEtablissementCreationCreateur.evenementId],
			references: [evenement.id],
		}),
		equipe: one(equipe, {
			fields: [actionEtablissementCreationCreateur.equipeId],
			references: [equipe.id],
		}),
		creation: one(etabCrea, {
			fields: [actionEtablissementCreationCreateur.etabCreaId],
			references: [etabCrea.id],
		}),
		createur: one(etabCreaCreateur, {
			fields: [
				actionEtablissementCreationCreateur.etabCreaId,
				actionEtablissementCreationCreateur.contactId,
			],
			references: [etabCreaCreateur.etabCreaId, etabCreaCreateur.contactId],
		}),
	})
);

/*
 ACTION ETIQUETTE ETABLISSEMENT
*/

export const actionEtiquetteEtablissement = pgTable(
	'action_etiquette__etablissement',
	{
		...actionColumns,
		etiquetteId: integer('etiquette_id').notNull(),
		equipeId: integer('equipe_id').notNull(),
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
	},
	(table) => ({
		idxActionEtiquetteEtablissement: index('idx_action_etiquette_etablissement').on(
			table.evenementId,
			table.etiquetteId,
			table.etablissementId,
			table.typeId
		),
		idxActionEtiquetteEtablissementStats: index('idx_action_etiquette_etablissement__stats').on(
			table.equipeId,
			table.typeId,
			table.createdAt,
			table.etiquetteId
		),
		fkActionEtiquetteEtablissementEtiquette: foreignKey({
			name: 'fk_action_etiquette_etablissement__etiquette',
			columns: [table.etiquetteId],
			foreignColumns: [etiquette.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEtiquetteEtablissementEquipe: foreignKey({
			name: 'fk_action_etiquette_etablissement__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEtiquetteEtablissementEtablissement: foreignKey({
			name: 'fk_action_etiquette_etablissement__etablissement',
			columns: [table.etablissementId],
			foreignColumns: [etablissement.siret],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEtiquetteEtablissementEvenement: foreignKey({
			name: 'fk_action_etiquette_etablissement__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEtiquetteEtablissementActionType: foreignKey({
			name: 'fk_action_etiquette_etablissement__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionEtiquetteEtablissementRelations = relations(
	actionEtiquetteEtablissement,
	({ one }) => ({
		evenement: one(evenement, {
			fields: [actionEtiquetteEtablissement.evenementId],
			references: [evenement.id],
		}),
		etiquette: one(etiquette, {
			fields: [actionEtiquetteEtablissement.etiquetteId],
			references: [etiquette.id],
		}),
		equipe: one(equipe, {
			fields: [actionEtiquetteEtablissement.equipeId],
			references: [equipe.id],
		}),
		etab: one(etablissement, {
			fields: [actionEtiquetteEtablissement.etablissementId],
			references: [etablissement.siret],
		}),
	})
);

/*
 ACTION EQUIPE ETABLISSEMENT
*/

export const actionEquipeEtablissement = pgTable(
	'action_equipe_etablissement',
	{
		...actionColumns,
		equipeId: integer('equipe_id').notNull(),
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
	},
	(table) => ({
		idxActionEquipeEtablissement: index('idx_action_equipe_etablissement').on(
			table.evenementId,
			table.equipeId,
			table.etablissementId,
			table.typeId
		),

		fkActionEquipeEtablissementEquipe: foreignKey({
			name: 'fk_action_equipe_etablissement__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEquipeEtablissementEtablissement: foreignKey({
			name: 'fk_action_equipe_etablissement__etablissement',
			columns: [table.etablissementId],
			foreignColumns: [etablissement.siret],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEquipeEtablissementEvenement: foreignKey({
			name: 'fk_action_equipe_etablissement__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEquipeEtablissementActionType: foreignKey({
			name: 'fk_action_equipe_etablissement__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionEquipeEtablissementRelations = relations(
	actionEquipeEtablissement,
	({ one }) => ({
		evenement: one(evenement, {
			fields: [actionEquipeEtablissement.evenementId],
			references: [evenement.id],
		}),
		equipe: one(equipe, {
			fields: [actionEquipeEtablissement.equipeId],
			references: [equipe.id],
		}),
		etab: one(etablissement, {
			fields: [actionEquipeEtablissement.etablissementId],
			references: [etablissement.siret],
		}),
	})
);

/*
 ACTION ETIQUETTE ETABLISSEMENT CREATION
*/

export const actionEtiquetteEtablissementCreation = pgTable(
	'action_etiquette__etablissement_creation',
	{
		...actionColumns,
		etiquetteId: integer('etiquette_id').notNull(),
		etabCreaId: integer('etablissement_creation_id').notNull(),
	},
	(table) => ({
		idxActionEtiquetteEtablissementCreation: index(
			'idx_action_etiquette_etablissement_creation'
		).on(table.evenementId, table.etiquetteId, table.etabCreaId, table.typeId),

		fkActionEtiquetteEtablissementCreationEtablissementCreation: foreignKey({
			name: 'fk_action_etiquette_etablissement_creation__ec',
			columns: [table.etabCreaId],
			foreignColumns: [etabCrea.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEtiquetteEtablissementCreationEtiquette: foreignKey({
			name: 'fk_action_etiquette_etablissement_creation__etiquette',
			columns: [table.etiquetteId],
			foreignColumns: [etiquette.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEtiquetteEtablissementCreationEvenement: foreignKey({
			name: 'fk_action_etiquette_etablissement_creation__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEtiquetteEtablissementCreationActionType: foreignKey({
			name: 'fk_action_etiquette_etablissement_creation__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionEtiquetteEtablissementCreationRelations = relations(
	actionEtiquetteEtablissementCreation,
	({ one }) => ({
		evenement: one(evenement, {
			fields: [actionEtiquetteEtablissementCreation.evenementId],
			references: [evenement.id],
		}),
		etiquette: one(etiquette, {
			fields: [actionEtiquetteEtablissementCreation.etiquetteId],
			references: [etiquette.id],
		}),
		etabCrea: one(etabCrea, {
			fields: [actionEtiquetteEtablissementCreation.etabCreaId],
			references: [etabCrea.id],
		}),
	})
);

/*
 ACTION ETABLISSEMENT
*/

export const actionEtablissement = pgTable(
	'action_etablissement',
	{
		...actionColumns,
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
	},
	(table) => ({
		idxActionEtablissement: index('idx_action_etablissement').on(
			table.evenementId,
			table.etablissementId,
			table.typeId
		),

		fkActionEtablissementEtablissement: foreignKey({
			name: 'fk_action_etablissement__etablissement',
			columns: [table.etablissementId],
			foreignColumns: [etablissement.siret],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEtablissementEvenement: foreignKey({
			name: 'fk_action_etablissement__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEtablissementActionType: foreignKey({
			name: 'fk_action_etablissement__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionEtablissementRelations = relations(actionEtablissement, ({ one }) => ({
	evenement: one(evenement, {
		fields: [actionEtablissement.evenementId],
		references: [evenement.id],
	}),
	etab: one(etablissement, {
		fields: [actionEtablissement.etablissementId],
		references: [etablissement.siret],
	}),
}));

/*
 ACTION ENTREPRISE
*/

export const actionEntreprise = pgTable(
	'action_entreprise',
	{
		...actionColumns,
		entrepriseId: varchar('entreprise_id', { length: 14 }).notNull(),
	},
	(table) => ({
		idxActionEntreprise: index('idx_action_entreprise').on(
			table.evenementId,
			table.entrepriseId,
			table.typeId
		),

		fkActionEntrepriseEntreprise: foreignKey({
			name: 'fk_action_entreprise__entreprise',
			columns: [table.entrepriseId],
			foreignColumns: [entreprise.siren],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEntrepriseEvenement: foreignKey({
			name: 'fk_action_entreprise__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEntrepriseActionType: foreignKey({
			name: 'fk_action_entreprise__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionEntrepriseRelations = relations(actionEntreprise, ({ one }) => ({
	evenement: one(evenement, {
		fields: [actionEntreprise.evenementId],
		references: [evenement.id],
	}),
	ul: one(entreprise, {
		fields: [actionEntreprise.entrepriseId],
		references: [entreprise.siren],
	}),
}));

/*
 ACTION LOCAL
*/

export const actionEquipeLocal = pgTable(
	'action_equipe_local',
	{
		...actionColumns,
		localId: varchar('local_id', { length: 12 }).notNull(),
		equipeId: integer('equipe_id').notNull(),
	},
	(table) => ({
		idxActionEquipeLocal: index('idx_action_equipe_local').on(
			table.evenementId,
			table.localId,
			table.equipeId,
			table.typeId
		),

		fkActionEquipeLocalEvenement: foreignKey({
			name: 'fk_action_equipe_local__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEquipeLocalActionType: foreignKey({
			name: 'fk_action_equipe_local__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionEquipeLocalRelations = relations(actionEquipeLocal, ({ one }) => ({
	evenement: one(evenement, {
		fields: [actionEquipeLocal.evenementId],
		references: [evenement.id],
	}),
	local: one(local, {
		fields: [actionEquipeLocal.localId],
		references: [local.id],
	}),
	equipe: one(equipe, {
		fields: [actionEquipeLocal.localId],
		references: [equipe.id],
	}),
}));

/*
 ACTION ÉQUIPE LOCAL CONTACT
*/

export const actionEquipeLocalContact = pgTable(
	'action_equipe_local_contact',
	{
		...actionColumns,
		equipeId: integer('equipe_id').notNull(),
		localId: varchar('local_id', { length: 12 }).notNull(),
		contactId: integer('contact_id').notNull(),
	},
	(table) => ({
		idxActionEquipeLocalContact: index('idx_action_equipe_local_contact').on(
			table.evenementId,
			table.equipeId,
			table.localId,
			table.contactId,
			table.typeId
		),

		fkActionEquipeLocalContactLocal: foreignKey({
			name: 'fk_action_equipe_local_contact__local',
			columns: [table.localId],
			foreignColumns: [local.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEquipeLocalContactEquipe: foreignKey({
			name: 'fk_action_equipe_local_contact__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEquipeLocalContactContact: foreignKey({
			name: 'fk_action_equipe_local_contact__contact',
			columns: [table.contactId],
			foreignColumns: [contact.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEquipeLocalContactEvenement: foreignKey({
			name: 'fk_action_equipe_local_contact__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEquipeLocalContactActionType: foreignKey({
			name: 'fk_action_equipe_local_contact__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionEquipeLocalContactRelations = relations(actionEquipeLocalContact, ({ one }) => ({
	evenement: one(evenement, {
		fields: [actionEquipeLocalContact.evenementId],
		references: [evenement.id],
	}),
	eqLocContact: one(eqLocContact, {
		fields: [
			actionEquipeLocalContact.contactId,
			actionEquipeLocalContact.localId,
			actionEquipeLocalContact.equipeId,
		],
		references: [eqLocContact.contactId, eqLocContact.localId, eqLocContact.equipeId],
	}),
}));

/*
 ACTION ÉQUIPE LOCAL OCCUPANT
*/

export const actionEquipeLocalOccupant = pgTable(
	'action_equipe_local_occupant',
	{
		...actionColumns,
		equipeId: integer('equipe_id').notNull(),
		localId: varchar('local_id', { length: 12 }).notNull(),
		etablissementId: varchar('occupant_id', { length: 14 }).notNull(),
	},
	(table) => ({
		idxActionEquipeLocalOccupant: index('idx_action_equipe_local_occupant').on(
			table.evenementId,
			table.equipeId,
			table.localId,
			table.etablissementId,
			table.typeId
		),

		fkActionEquipeLocalOccupantLocal: foreignKey({
			name: 'fk_action_equipe_local_occupant__local',
			columns: [table.localId],
			foreignColumns: [local.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEquipeLocalOccupantEquipe: foreignKey({
			name: 'fk_action_equipe_local_occupant__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEquipeLocalOccupantOccupant: foreignKey({
			name: 'fk_action_equipe_local_occupant__occupant',
			columns: [table.etablissementId],
			foreignColumns: [etablissement.siret],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEquipeLocalOccupantEvenement: foreignKey({
			name: 'fk_action_equipe_local_occupant__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEquipeLocalOccupantActionType: foreignKey({
			name: 'fk_action_equipe_local_occupant__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionEquipeLocalOccupantRelations = relations(
	actionEquipeLocalOccupant,
	({ one }) => ({
		evenement: one(evenement, {
			fields: [actionEquipeLocalOccupant.evenementId],
			references: [evenement.id],
		}),
		eqLocOccupant: one(eqLocOccupant, {
			fields: [
				actionEquipeLocalOccupant.etablissementId,
				actionEquipeLocalOccupant.localId,
				actionEquipeLocalOccupant.equipeId,
			],
			references: [eqLocOccupant.etablissementId, eqLocOccupant.localId, eqLocOccupant.equipeId],
		}),
	})
);

/*
 ACTION ETIQUETTE LOCAL
*/

export const actionEtiquetteLocal = pgTable(
	'action_etiquette__local',
	{
		...actionColumns,
		etiquetteId: integer('etiquette_id').notNull(),
		equipeId: integer('equipe_id').notNull(),
		localId: varchar('local_id', { length: 12 }).notNull(),
	},
	(table) => ({
		idxActionEtiquetteLocal: index('idx_action_etiquette_local').on(
			table.evenementId,
			table.etiquetteId,
			table.localId,
			table.typeId
		),
		fkActionEtiquetteLocalEtiquette: foreignKey({
			name: 'fk_action_etiquette_local__etiquette',
			columns: [table.etiquetteId],
			foreignColumns: [etiquette.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEtiquetteLocalEquipe: foreignKey({
			name: 'fk_action_etiquette_local__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEtiquetteLocalLocal: foreignKey({
			name: 'fk_action_etiquette_local__local',
			columns: [table.localId],
			foreignColumns: [local.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		fkActionEtiquetteLocalEvenement: foreignKey({
			name: 'fk_action_etiquette_local__evenement',
			columns: [table.evenementId],
			foreignColumns: [evenement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkActionEtiquetteLocalActionType: foreignKey({
			name: 'fk_action_etiquette_local__action_type',
			columns: [table.typeId],
			foreignColumns: [actionType.id],
		})
			.onUpdate('cascade')
			.onDelete('restrict'),
	})
);

export const actionEtiquetteLocalRelations = relations(actionEtiquetteLocal, ({ one }) => ({
	evenement: one(evenement, {
		fields: [actionEtiquetteLocal.evenementId],
		references: [evenement.id],
	}),
	etiquette: one(etiquette, {
		fields: [actionEtiquetteLocal.etiquetteId],
		references: [etiquette.id],
	}),
	equipe: one(equipe, {
		fields: [actionEtiquetteLocal.equipeId],
		references: [equipe.id],
	}),
	local: one(local, {
		fields: [actionEtiquetteLocal.localId],
		references: [local.id],
	}),
}));
