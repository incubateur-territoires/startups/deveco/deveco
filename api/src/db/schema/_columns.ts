import { serial, varchar } from 'drizzle-orm/pg-core';

import { point } from '../../db/schema/custom-types/postgis';

export const adresseColumns = {
	id: serial('id').notNull(),
	numero: varchar('numero'),
	voieNom: varchar('voie_nom'),
	codePostal: varchar('code_postal', { length: 5 }),
	cedexCode: varchar('cedex_code', { length: 9 }),
	cedexNom: varchar('cedex_nom'),
	distributionSpeciale: varchar('distribution_speciale'),
	geolocalisation: point('geolocalisation'),
};
