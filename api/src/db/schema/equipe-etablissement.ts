import {
	pgTable,
	integer,
	boolean,
	text,
	varchar,
	timestamp,
	primaryKey,
	date,
	index,
	foreignKey,
	customType,
	serial,
} from 'drizzle-orm/pg-core';
import { relations } from 'drizzle-orm';

import { point } from '../../db/schema/custom-types/postgis';

import { RessourceTypeId } from '../types';
import { equipe, etiquette, deveco } from '../../db/schema/equipe';
import { echange, demande, rappel, brouillon } from '../../db/schema/suivi';
import { etablissement } from '../../db/schema/etablissement';
import { contact } from '../../db/schema/contact';
import { compte } from '../../db/schema/admin';
import { adresseColumns } from '../../db/schema/_columns';

export const ressourceType = pgTable(
	'ressource_type',
	{
		id: varchar('id').notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkRessourceType: primaryKey({
			name: 'pk_ressource_type',
			columns: [table.id],
		}),
	})
);

export const eqEtabContact = pgTable(
	'equipe__etablissement__contact',
	{
		equipeId: integer('equipe_id').notNull(),
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
		contactId: integer('contact_id').notNull(),
		fonction: varchar('fonction'),
		contactSourceTypeId: varchar('contact_source_type_id', {
			length: 10,
		}).notNull(),
	},
	(table) => ({
		pkEquipeEtablissementContact: primaryKey({
			name: 'pk_equipe__etablissement__contact',
			columns: [table.equipeId, table.etablissementId, table.contactId],
		}),
		fkEquipeEtablissementContactEquipe: foreignKey({
			name: 'fk_equipe__etablissement__contact__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeEtablissementContactEtablissement: foreignKey({
			name: 'fk_equipe__etablissement__contact__etablissement',
			columns: [table.etablissementId],
			foreignColumns: [etablissement.siret],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeEtablissementContactContact: foreignKey({
			name: 'fk_equipe__etablissement__contact__contact',
			columns: [table.contactId],
			foreignColumns: [contact.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		idxEquipeEtablissementContactContact: index('idx_equipe__etablissement__contact__contact').on(
			table.contactId
		),
		idxEquipeEtablissementContactEtablissement: index(
			'idx_equipe__etablissement__contact__etablissement'
		).on(table.etablissementId),
		idxEquipeEtablissementContactEquipe: index('idx_equipe__etablissement__contact__equipe').on(
			table.equipeId
		),
	})
);

export const eqEtabContactRelations = relations(eqEtabContact, ({ one }) => ({
	equipe: one(equipe, {
		fields: [eqEtabContact.equipeId],
		references: [equipe.id],
	}),
	etab: one(etablissement, {
		fields: [eqEtabContact.etablissementId],
		references: [etablissement.siret],
	}),
	ctct: one(contact, {
		fields: [eqEtabContact.contactId],
		references: [contact.id],
	}),
}));

export const eqEtabContribution = pgTable(
	'equipe__etablissement_contribution',
	{
		equipeId: integer('equipe_id').notNull(),
		description: text('description'),
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
		clotureDate: date('cloture_date', { mode: 'date' }),
		enseigne: varchar('enseigne'),
		geolocalisation: adresseColumns.geolocalisation,
		creationDate: timestamp('creation_date', { withTimezone: true, mode: 'date' })
			.defaultNow()
			.notNull(),
		modificationDate: timestamp('modification_date', { withTimezone: true, mode: 'date' }),
		modificationCompteId: integer('modification_compte_id'),
	},
	(table) => ({
		pkEquipeEtablissementContribution: primaryKey({
			name: 'pk_equipe__etablissement_contribution',
			columns: [table.equipeId, table.etablissementId],
		}),
		fkEquipeEtablissementContributionEquipe: foreignKey({
			name: 'fk_equipe__etablissement_contribution__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeEtablissementContributionEtablissement: foreignKey({
			name: 'fk_equipe__etablissement_contribution__etablissement',
			columns: [table.etablissementId],
			foreignColumns: [etablissement.siret],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeEtablissementContributionModificationCompte: foreignKey({
			name: 'fk_equipe__etablissement_contribution__modification_compte',
			columns: [table.modificationCompteId],
			foreignColumns: [compte.id],
		})
			.onDelete('set null')
			.onUpdate('cascade'),
	})
);

export const eqEtabContributionRelations = relations(eqEtabContribution, ({ one }) => ({
	eq: one(equipe, {
		fields: [eqEtabContribution.equipeId],
		references: [equipe.id],
	}),
	etab: one(etablissement, {
		fields: [eqEtabContribution.etablissementId],
		references: [etablissement.siret],
	}),
	modifCompte: one(compte, {
		fields: [eqEtabContribution.modificationCompteId],
		references: [compte.id],
	}),
}));

export const ressourceTypeId = customType<{
	data: RessourceTypeId;
	notNull: true;
	default: true;
}>({
	dataType() {
		return 'varchar';
	},
});

export const eqEtabRessource = pgTable(
	'equipe__etablissement_ressource',
	{
		id: serial('id'),
		equipeId: integer('equipe_id').notNull(),
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
		nom: varchar('nom'),
		lien: varchar('lien'),
		type: ressourceTypeId('ressource_type_id'),
	},
	(table) => ({
		pkEquipeEtablissementRessource: primaryKey({
			name: 'pk_equipe__etablissement_ressource',
			columns: [table.id],
		}),
		fkEquipeEtablissementRessourceType: foreignKey({
			name: 'fk_equipe__etablissement_ressource__ressource_type',
			columns: [table.type],
			foreignColumns: [ressourceType.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),

		idxEquipeEtablissementRessource: index('idx_equipe_etablissement_ressource').on(
			table.equipeId,
			table.etablissementId
		),
	})
);

export const eqEtabRessourceRelations = relations(eqEtabRessource, ({ one }) => ({
	equipe: one(equipe, {
		fields: [eqEtabRessource.equipeId],
		references: [equipe.id],
	}),
	etab: one(etablissement, {
		fields: [eqEtabRessource.etablissementId],
		references: [etablissement.siret],
	}),
}));

export const devecoEtablissementFavori = pgTable(
	'deveco__etablissement_favori',
	{
		devecoId: integer('deveco_id').notNull(),
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
	},
	(table) => ({
		pkDevecoEtablissementFavori: primaryKey({
			name: 'pk_deveco__etablissement_favori',
			columns: [table.devecoId, table.etablissementId],
		}),
		fkDevecoEtablissementFavoriDeveco: foreignKey({
			name: 'fk_deveco__etablissement_favori__deveco',
			columns: [table.devecoId],
			foreignColumns: [deveco.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkDevecoEtablissementFavoriEtablissement: foreignKey({
			name: 'fk_deveco__etablissement_favori__etablissement',
			columns: [table.etablissementId],
			foreignColumns: [etablissement.siret],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const devecoEtablissementFavoriRelations = relations(
	devecoEtablissementFavori,
	({ one }) => ({
		deveco: one(deveco, {
			fields: [devecoEtablissementFavori.devecoId],
			references: [deveco.id],
		}),
		etab: one(etablissement, {
			fields: [devecoEtablissementFavori.etablissementId],
			references: [etablissement.siret],
		}),
	})
);

export const eqEtabEtiquette = pgTable(
	'equipe__etablissement__etiquette',
	{
		equipeId: integer('equipe_id').notNull(),
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
		etiquetteId: integer('etiquette_id').notNull(),
		auto: boolean('auto').default(false),
	},
	(table) => ({
		pkEquipeEtablissementEtiquette: primaryKey({
			name: 'pk_equipe__etablissement__etiquette',
			columns: [table.equipeId, table.etablissementId, table.etiquetteId],
		}),
		fkEquipeEtablissementEtiquetteEtablissement: foreignKey({
			name: 'fk_equipe__etablissement__etiquette__etablissement',
			columns: [table.etablissementId],
			foreignColumns: [etablissement.siret],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeEtablissementEtiquetteEquipe: foreignKey({
			name: 'fk_equipe__etablissement__etiquette__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeEtablissementEtiquetteEtiquette: foreignKey({
			name: 'fk_equipe__etablissement__etiquette__etiquette',
			columns: [table.etiquetteId],
			foreignColumns: [etiquette.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const eqEtabEtiquetteRelations = relations(eqEtabEtiquette, ({ one }) => ({
	equipe: one(equipe, {
		fields: [eqEtabEtiquette.equipeId],
		references: [equipe.id],
	}),
	etab: one(etablissement, {
		fields: [eqEtabEtiquette.etablissementId],
		references: [etablissement.siret],
	}),
	etiquette: one(etiquette, {
		fields: [eqEtabEtiquette.etiquetteId],
		references: [etiquette.id],
	}),
}));

export const eqEtabEchange = pgTable(
	'equipe__etablissement__echange',
	{
		equipeId: integer('equipe_id').notNull(),
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
		echangeId: integer('echange_id').notNull(),
	},
	(table) => ({
		pkEquipeEtablissementEchange: primaryKey({
			name: 'pk_equipe__etablissement__echange',
			columns: [table.equipeId, table.etablissementId, table.echangeId],
		}),
		fkEquipeEtablissementEchangeEtablissement: foreignKey({
			name: 'fk_equipe__etablissement__echange__etablissement',
			columns: [table.etablissementId],
			foreignColumns: [etablissement.siret],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeEtablissementEchangeEquipe: foreignKey({
			name: 'fk_equipe__etablissement__echange__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeEtablissementEchangeEchange: foreignKey({
			name: 'fk_equipe__etablissement__echange__echange',
			columns: [table.echangeId],
			foreignColumns: [echange.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const eqEtabEchangeRelations = relations(eqEtabEchange, ({ one }) => ({
	equipe: one(equipe, {
		fields: [eqEtabEchange.equipeId],
		references: [equipe.id],
	}),
	etab: one(etablissement, {
		fields: [eqEtabEchange.etablissementId],
		references: [etablissement.siret],
	}),
	echange: one(echange, {
		fields: [eqEtabEchange.echangeId],
		references: [echange.id],
	}),
}));

export const eqEtabDemande = pgTable(
	'equipe__etablissement__demande',
	{
		equipeId: integer('equipe_id').notNull(),
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
		demandeId: integer('demande_id').notNull(),
	},
	(table) => ({
		pkEquipeEtablissementDemande: primaryKey({
			name: 'pk_equipe__etablissement__demande',
			columns: [table.equipeId, table.etablissementId, table.demandeId],
		}),
		fkEquipeEtablissementDemandeEtablissement: foreignKey({
			name: 'fk_equipe__etablissement__demande__etablissement',
			columns: [table.etablissementId],
			foreignColumns: [etablissement.siret],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeEtablissementDemandeEquipe: foreignKey({
			name: 'fk_equipe__etablissement__demande__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeEtablissementDemandeDemande: foreignKey({
			name: 'fk_equipe__etablissement__demande__demande',
			columns: [table.demandeId],
			foreignColumns: [demande.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const eqEtabDemandeRelations = relations(eqEtabDemande, ({ one }) => ({
	equipe: one(equipe, {
		fields: [eqEtabDemande.equipeId],
		references: [equipe.id],
	}),
	etab: one(etablissement, {
		fields: [eqEtabDemande.etablissementId],
		references: [etablissement.siret],
	}),
	demande: one(demande, {
		fields: [eqEtabDemande.demandeId],
		references: [demande.id],
	}),
}));

export const eqEtabRappel = pgTable(
	'equipe__etablissement__rappel',
	{
		equipeId: integer('equipe_id').notNull(),
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
		rappelId: integer('rappel_id').notNull(),
	},
	(table) => ({
		pkEquipeEtablissementRappel: primaryKey({
			name: 'pk_equipe__etablissement__rappel',
			columns: [table.equipeId, table.etablissementId, table.rappelId],
		}),
		fkEquipeEtablissementRappelEtablissement: foreignKey({
			name: 'fk_equipe__etablissement__rappel__etablissement',
			columns: [table.etablissementId],
			foreignColumns: [etablissement.siret],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeEtablissementRappelEquipe: foreignKey({
			name: 'fk_equipe__etablissement__rappel__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeEtablissementRappelRappel: foreignKey({
			name: 'fk_equipe__etablissement__rappel__rappel',
			columns: [table.rappelId],
			foreignColumns: [rappel.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const eqEtabRappelRelations = relations(eqEtabRappel, ({ one }) => ({
	equipe: one(equipe, {
		fields: [eqEtabRappel.equipeId],
		references: [equipe.id],
	}),
	etab: one(etablissement, {
		fields: [eqEtabRappel.etablissementId],
		references: [etablissement.siret],
	}),
	rappel: one(rappel, {
		fields: [eqEtabRappel.rappelId],
		references: [rappel.id],
	}),
}));

export const eqEtabBrouillon = pgTable(
	'equipe__etablissement__brouillon',
	{
		equipeId: integer('equipe_id').notNull(),
		etablissementId: varchar('etablissement_id', { length: 14 }).notNull(),
		brouillonId: integer('brouillon_id').notNull(),
	},
	(table) => ({
		pkEquipeEtablissementBrouillon: primaryKey({
			name: 'pk_equipe__etablissement__brouillon',
			columns: [table.equipeId, table.etablissementId, table.brouillonId],
		}),
		fkEquipeEtablissementBrouillonEtablissement: foreignKey({
			name: 'fk_equipe__etablissement__brouillon__etablissement',
			columns: [table.etablissementId],
			foreignColumns: [etablissement.siret],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeEtablissementBrouillonEquipe: foreignKey({
			name: 'fk_equipe__etablissement__brouillon__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeEtablissementBrouillonBrouillon: foreignKey({
			name: 'fk_equipe__etablissement__brouillon__brouillon',
			columns: [table.brouillonId],
			foreignColumns: [brouillon.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const eqEtabBrouillonRelations = relations(eqEtabBrouillon, ({ one }) => ({
	equipe: one(equipe, {
		fields: [eqEtabBrouillon.equipeId],
		references: [equipe.id],
	}),
	etab: one(etablissement, {
		fields: [eqEtabBrouillon.etablissementId],
		references: [etablissement.siret],
	}),
	brouillon: one(brouillon, {
		fields: [eqEtabBrouillon.brouillonId],
		references: [brouillon.id],
	}),
}));

export const etablissementGeolocalisationVue = pgTable('etablissement_geolocalisation_vue', {
	siret: varchar('siret', { length: 14 }).notNull(),
	geolocalisation: point('geolocalisation'),
	miseAJourAt: timestamp('mise_a_jour_at', { withTimezone: true, mode: 'date' }),
});
/*
.as(
	sql`SELECT
		${etablissement.siret} AS siret,
		COALESCE(
			${eqEtabContribution.geolocalisation},
			ST_SetSRID(ST_MakePoint(${etablissementGeoloc.xLongitude}, ${etablissementGeoloc.yLatitude}), 4326),
			ST_Transform (
				ST_SetSRID (
					ST_MakePoint(${etablissement.coordonneeLambertAbscisseEtablissement}::float, ${etablissement.coordonneeLambertOrdonneeEtablissement}::float),
					CASE
							WHEN LEFT (${etablissement.codeCommuneEtablissement}, 3) in ('971', '972') THEN 5490
							WHEN LEFT (${etablissement.codeCommuneEtablissement}, 3) = '973' THEN 2972
							WHEN LEFT (${etablissement.codeCommuneEtablissement}, 3) = '974' THEN 2975
							WHEN LEFT (${etablissement.codeCommuneEtablissement}, 3) = '976' THEN 4471
							ELSE 2154
					END
				),
				4326
			),
			${etalabEtablissementAdresse.geolocalisation}
		) AS geolocalisation,
		${etablissement.miseAJourAt} AS mise_a_jour_at
	FROM ${etablissement}
	LEFT JOIN ${etablissementGeoloc} ON ${etablissementGeoloc.siret} = ${etablissement.siret}
	LEFT JOIN ${etalabEtablissementAdresse} ON ${etalabEtablissementAdresse.siret} = ${etablissement.siret}
	LEFT JOIN ${eqEtabContribution} ON ${eqEtabContribution.etablissementId} = ${etablissement.siret}
	WHERE ${etablissementGeoloc.siret} IS NOT NULL OR ${etalabEtablissementAdresse.siret} IS NOT NULL`
);
 .as((db) =>
 db
		.select({
			siret: sql<Etablissement['siret']>`${etablissement.siret}`.as('siret'),
			geolocalisation: sql<EtalabEtablissementAdresse['geolocalisation']>`COALESCE(
					${eqEtabContribution.geolocalisation},
					ST_SetSRID(ST_MakePoint(${etablissementGeoloc.xLongitude}, ${etablissementGeoloc.yLatitude}), 4326),
					sql`ST_Transform (
						ST_SetSRID (
							ST_MakePoint(${etablissement.coordonneeLambertAbscisseEtablissement}::float, ${etablissement.coordonneeLambertOrdonneeEtablissement}::float),
							CASE
									WHEN LEFT (${etablissement.codeCommuneEtablissement}, 3) in ('971', '972') THEN 5490
									WHEN LEFT (${etablissement.codeCommuneEtablissement}, 3) = '973' THEN 2972
									WHEN LEFT (${etablissement.codeCommuneEtablissement}, 3) = '974' THEN 2975
									WHEN LEFT (${etablissement.codeCommuneEtablissement}, 3) = '976' THEN 4471
									ELSE 2154
							END
						),
						4326
					)`,
					${etalabEtablissementAdresse.geolocalisation}
				)`.as('geolocalisation'),
			miseAJourAt: sql<Etablissement['miseAJourAt']>`${etablissement.miseAJourAt}`.as(
				'mise_a_jour_at'
			),
		})
		.from(etablissement)
		.leftJoin(etablissementGeoloc, eq(etablissementGeoloc.siret, etablissement.siret))
		.leftJoin(etalabEtablissementAdresse, eq(etalabEtablissementAdresse.siret, etablissement.siret))
		.where(or(isNotNull(etablissementGeoloc.siret), isNotNull(etalabEtablissementAdresse.siret)))
 );
 Issue GitHub: https://github.com/drizzle-team/drizzle-orm/issues/2976
 TODO ajouter les indexes en Drizzle dès que possible :
 CREATE UNIQUE INDEX idx_etablissement_geolocalisation_vue_etablissement ON etablissement_geolocalisation_vue (siret);
 CREATE INDEX idx_etablissement_geolocalisation_vue_etablissement ON etablissement_geolocalisation_vue USING gist (geolocalisation);
*/

export const etablissementGeolocalisationVueRelations = relations(
	etablissementGeolocalisationVue,
	({ one }) => ({
		etab: one(etablissement, {
			fields: [etablissementGeolocalisationVue.siret],
			references: [etablissement.siret],
		}),
	})
);
