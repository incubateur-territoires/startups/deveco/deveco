import {
	pgTable,
	integer,
	varchar,
	serial,
	boolean,
	timestamp,
	unique,
	customType,
	primaryKey,
	foreignKey,
	index,
	uuid,
	jsonb,
} from 'drizzle-orm/pg-core';
import { relations } from 'drizzle-orm';

import { deveco, equipe } from './equipe';
import { commune, epci, petr, territoireIndustrie, departement, region } from './territoire';

import { CompteTypeId } from '../types';

const compteTypeId = customType<{
	data: CompteTypeId;
	notNull: true;
	default: true;
}>({
	dataType() {
		return 'varchar';
	},
});

export const tache = pgTable(
	'tache',
	{
		id: serial('id').notNull(),
		nom: varchar('nom').notNull(),
		debutAt: timestamp('debut_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
		enCoursAt: timestamp('en_cours_at', { withTimezone: true, mode: 'date' }),
		finAt: timestamp('fin_at', { withTimezone: true, mode: 'date' }),
		manuel: boolean('manuel').default(false),
		rapport: jsonb('rapport'),
	},
	(table) => ({
		ukTache: unique('uk_tache').on(table.id, table.nom),
		pkTache: primaryKey({ name: 'pk_tache', columns: [table.id] }),
	})
);

export const nouveautes = pgTable(
	'nouveautes',
	{
		id: serial('id').notNull(),
		amount: integer('amount').notNull(),
		createdAt: timestamp('created_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
	},
	(table) => ({
		pkNouveautes: primaryKey({ name: 'pk_nouveautes', columns: [table.id] }),
	})
);

export const compte = pgTable(
	'compte',
	{
		id: serial('id').notNull(),
		nom: varchar('nom'),
		prenom: varchar('prenom'),
		email: varchar('email').notNull(),
		identifiant: varchar('identifiant').notNull(),
		typeId: compteTypeId('compte_type_id').notNull(),
		cle: varchar('cle'),
		actif: boolean('actif').default(false).notNull(),
		anonymise: boolean('anonymise').default(false).notNull(),
		cgu: boolean('cgu').default(false),
		createdAt: timestamp('created_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
		connexionAt: timestamp('connexion_at', { withTimezone: true, mode: 'date' }),
		authAt: timestamp('auth_at', { withTimezone: true, mode: 'date' }),
		desactivationAt: timestamp('desactivation_at', { withTimezone: true, mode: 'date' }),
	},
	(table) => ({
		pkCompte: primaryKey({ name: 'pk_compte', columns: [table.id] }),
		idxCompteEmail: index('idx_compte__email').on(table.email),
		idxCompteIdentifiant: index('idx_compte__identifiant').on(table.identifiant),
	})
);

export const compteRelations = relations(compte, ({ one }) => ({
	motDePasse: one(motDePasse),
	deveco: one(deveco),
}));

export const motDePasse = pgTable(
	'mot_de_passe',
	{
		id: serial('id').notNull(),
		hash: varchar('hash').notNull(),
		updatedAt: timestamp('updated_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
		createdAt: timestamp('created_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
		compteId: integer('compte_id').notNull(),
	},
	(table) => ({
		pkMotDePasse: primaryKey({ name: 'pk_mot_de_passe', columns: [table.id] }),
		ukMotDePasse: unique('uk_mot_de_passe').on(table.compteId),
		fkMotDePasseCompte: foreignKey({
			name: 'fk_mot_de_passe__compte',
			columns: [table.compteId],
			foreignColumns: [compte.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const motDePasseRelations = relations(motDePasse, ({ one }) => ({
	compte: one(compte, {
		fields: [motDePasse.compteId],
		references: [compte.id],
	}),
}));

export const equipeDemande = pgTable(
	'equipe_demande',
	{
		id: serial('id').notNull(),
		nom: varchar('nom').notNull(),
		equipeId: integer('equipe_id'),
		compteId: integer('compte_id').notNull(),
		createdAt: timestamp('created_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
		transformedAt: timestamp('transformed_at', { withTimezone: true, mode: 'date' }),
	},
	(table) => ({
		pkEquipeDemande: primaryKey({ name: 'pk_equipe_demande', columns: [table.id] }),
		fkEquipeDemandeCompte: foreignKey({
			name: 'fk_equipe_demande__compte',
			columns: [table.compteId],
			foreignColumns: [compte.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeDemandeEquipe: foreignKey({
			name: 'fk_equipe_demande__equipe',
			columns: [table.equipeId],
			foreignColumns: [equipe.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const equipeDemandeRelations = relations(equipeDemande, ({ one, many }) => ({
	autorisations: many(equipeDemandeAutorisation),
	admin: one(compte, {
		fields: [equipeDemande.compteId],
		references: [compte.id],
	}),
	compteDemande: one(compteDemande, {
		fields: [equipeDemande.id],
		references: [compteDemande.equipeDemandeId],
	}),
}));

export const equipeDemandeAutorisation = pgTable(
	'equipe_demande_autorisation',
	{
		id: serial('id').notNull(),
		token: uuid('token').notNull(),
		equipeDemandeId: integer('equipe_demande_id').notNull(),
		ayantDroitId: integer('ayant_droit_id').notNull(),
		communeId: varchar('commune_id', { length: 5 }),
		epciId: varchar('epci_id', { length: 9 }),
		petrId: varchar('petr_id', { length: 4 }),
		territoireIndustrieId: varchar('territoire_industrie_id', { length: 7 }),
		departementId: varchar('departement_id', { length: 3 }),
		regionId: varchar('region_id', { length: 2 }),
		createdAt: timestamp('created_at', { withTimezone: true, mode: 'date' }).defaultNow().notNull(),
		acceptedAt: timestamp('accepted_at', { withTimezone: true, mode: 'date' }),
	},
	(table) => ({
		pkEquipeDemandeAutorisation: primaryKey({
			name: 'pk_equipe_demande_autorisation',
			columns: [table.id],
		}),
		ukEquipeDemandeAutorisationAyantDroit: unique('uk_equipe_demande_autorisation__ayant_droit').on(
			table.ayantDroitId
		),
		fkEquipeDemandeAutorisationEquipeDemandeId: foreignKey({
			name: 'fk_equipe_demande_autorisation__equipe_demande',
			columns: [table.equipeDemandeId],
			foreignColumns: [equipeDemande.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeDemandeAutorisationAyantDroitId: foreignKey({
			name: 'fk_equipe_demande_autorisation__ayant_droit',
			columns: [table.ayantDroitId],
			foreignColumns: [ayantDroit.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeDemandeAutorisationCommuneId: foreignKey({
			name: 'fk_equipe_demande_autorisation__commune',
			columns: [table.communeId],
			foreignColumns: [commune.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeDemandeAutorisationEpciId: foreignKey({
			name: 'fk_equipe_demande_autorisation__epci',
			columns: [table.epciId],
			foreignColumns: [epci.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeDemandeAutorisationPetrId: foreignKey({
			name: 'fk_equipe_demande_autorisation__petr',
			columns: [table.petrId],
			foreignColumns: [petr.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeDemandeAutorisationDepartementId: foreignKey({
			name: 'fk_equipe_demande_autorisation__departement',
			columns: [table.departementId],
			foreignColumns: [departement.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
		fkEquipeDemandeAutorisationRegionId: foreignKey({
			name: 'fk_equipe_demande_autorisation__region',
			columns: [table.regionId],
			foreignColumns: [region.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const equipeDemandeAutorisationRelations = relations(
	equipeDemandeAutorisation,
	({ one }) => ({
		ayantDroit: one(ayantDroit, {
			fields: [equipeDemandeAutorisation.ayantDroitId],
			references: [ayantDroit.id],
		}),
		commune: one(commune, {
			fields: [equipeDemandeAutorisation.communeId],
			references: [commune.id],
		}),
		epci: one(epci, {
			fields: [equipeDemandeAutorisation.epciId],
			references: [epci.id],
		}),
		petr: one(petr, {
			fields: [equipeDemandeAutorisation.petrId],
			references: [petr.id],
		}),
		territoireIndustrie: one(territoireIndustrie, {
			fields: [equipeDemandeAutorisation.territoireIndustrieId],
			references: [territoireIndustrie.id],
		}),
		departement: one(departement, {
			fields: [equipeDemandeAutorisation.departementId],
			references: [departement.id],
		}),
		region: one(region, {
			fields: [equipeDemandeAutorisation.regionId],
			references: [region.id],
		}),
		equipeDemande: one(equipeDemande, {
			fields: [equipeDemandeAutorisation.equipeDemandeId],
			references: [equipeDemande.id],
		}),
	})
);

export const compteDemande = pgTable(
	'compte_demande',
	{
		id: serial('id').notNull(),
		equipeDemandeId: integer('equipe_demande_id').notNull(),
		nom: varchar('nom').notNull(),
		prenom: varchar('prenom').notNull(),
		fonction: varchar('fonction').notNull(),
		email: varchar('email').notNull(),
	},
	(table) => ({
		pkCompteDemande: primaryKey({ name: 'pk_compte_demande', columns: [table.id] }),
		ukCompteDemandeEquipeDemande: unique('uk_compte_demande__equipe_demande').on(
			table.equipeDemandeId
		),
		fkCompteDemandeEquipeDemandeId: foreignKey({
			name: 'fk_compte_demande__equipe_demande',
			columns: [table.equipeDemandeId],
			foreignColumns: [equipeDemande.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const compteDemandeRelations = relations(compteDemande, ({ one }) => ({
	equipeDemande: one(equipeDemande, {
		fields: [compteDemande.equipeDemandeId],
		references: [equipeDemande.id],
	}),
}));

export const ayantDroit = pgTable(
	'ayant_droit',
	{
		id: serial('id').notNull(),
		nom: varchar('nom'),
		prenom: varchar('prenom'),
		email: varchar('email').notNull(),
		fonction: varchar('fonction').notNull(),
	},
	(table) => ({
		pkAyantDroit: primaryKey({ name: 'pk_ayant_droit', columns: [table.id] }),
	})
);

export const ayantDroitRelations = relations(ayantDroit, ({ one }) => ({
	equipeDemandeAutorisation: one(equipeDemandeAutorisation),
}));

export const erreurFront = pgTable(
	'erreur_front',
	{
		id: serial('id').notNull(),
		compteId: integer('compte_id').notNull(),
		message: varchar('message').notNull(),
		createdAt: timestamp('date', { withTimezone: true, mode: 'date' }).notNull().defaultNow(),
	},
	(table) => ({
		pkErreurFront: primaryKey({ name: 'pk_erreur_front', columns: [table.id] }),
	})
);

export const refreshToken = pgTable(
	'refresh_token',
	{
		id: serial('id').notNull(),
		compteId: integer('compte_id').notNull(),
		token: uuid('token').notNull(),
		createdAt: timestamp('created_at', { withTimezone: true, mode: 'date' }).notNull().defaultNow(),
		creatorUserAgent: varchar('creator_user_agent').notNull(),
		lastAccessedAt: timestamp('last_accessed_at', { withTimezone: true, mode: 'date' }),
		lastAccessedUserAgent: varchar('last_accessed_user_agent'),
		jwtExpired: varchar('jwt_expired'),
		jwtRefreshed: varchar('jwt_refreshed'),
	},
	(table) => ({
		pkRefreshToken: primaryKey({ name: 'pk_refresh_token', columns: [table.id] }),
		fkRefreshTokenCompte: foreignKey({
			name: 'fk_refresh_token__compte',
			columns: [table.compteId],
			foreignColumns: [compte.id],
		})
			.onUpdate('cascade')
			.onDelete('cascade'),
	})
);

export const refreshTokenRelations = relations(refreshToken, ({ one }) => ({
	compte: one(compte),
}));
