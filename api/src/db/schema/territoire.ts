import { foreignKey, primaryKey, pgTable, varchar, boolean } from 'drizzle-orm/pg-core';
import { relations } from 'drizzle-orm';

export const zrrType = pgTable(
	'zrr_type',
	{
		id: varchar('id', { length: 1 }).notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkZrrType: primaryKey({ name: 'pk_zrr_type', columns: [table.id] }),
	})
);

export const communeType = pgTable(
	'commune_type',
	{
		id: varchar('id').notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkCommuneType: primaryKey({ name: 'pk_commune_type', columns: [table.id] }),
	})
);

export const commune = pgTable(
	'commune',
	{
		id: varchar('id', { length: 5 }).notNull(),
		nom: varchar('nom', { length: 100 }).notNull(),
		typeId: varchar('commune_type_id', { length: 4 }).notNull(),
		parentId: varchar('commune_parent_id', { length: 5 }),
		metropoleId: varchar('metropole_id', { length: 5 }),
		epciId: varchar('epci_id', { length: 9 }).notNull(),
		petrId: varchar('petr_id', { length: 4 }),
		territoireIndustrieId: varchar('territoire_industrie_id', { length: 7 }),
		departementId: varchar('departement_id', { length: 3 }).notNull(),
		regionId: varchar('region_id', { length: 2 }).notNull(),
		zrrTypeId: varchar('zrr_type_id', { length: 1 }).default('N').notNull(),
	},
	(table) => ({
		pkCommune: primaryKey({ name: 'pk_commune', columns: [table.id] }),
		fkCommuneType: foreignKey({
			name: 'fk_commune__commune_type',
			columns: [table.typeId],
			foreignColumns: [communeType.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkCommuneParent: foreignKey({
			name: 'fk_commune__commune_parent',
			columns: [table.parentId],
			foreignColumns: [table.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkCommuneMetropole: foreignKey({
			name: 'fk_commune__metropole',
			columns: [table.metropoleId],
			foreignColumns: [metropole.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkCommuneEpci: foreignKey({
			name: 'fk_commune__epci',
			columns: [table.epciId],
			foreignColumns: [epci.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkCommunePetr: foreignKey({
			name: 'fk_commune__petr',
			columns: [table.petrId],
			foreignColumns: [petr.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkCommuneTerritoireIndustrie: foreignKey({
			name: 'fk_commune__territoireIndustrie',
			columns: [table.territoireIndustrieId],
			foreignColumns: [territoireIndustrie.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkCommuneDepartement: foreignKey({
			name: 'fk_commune__departement',
			columns: [table.departementId],
			foreignColumns: [departement.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkCommuneRegion: foreignKey({
			name: 'fk_commune__region',
			columns: [table.regionId],
			foreignColumns: [region.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkCommuneZrrType: foreignKey({
			name: 'fk_commune__zrr_type',
			columns: [table.zrrTypeId],
			foreignColumns: [zrrType.id],
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
	})
);

export const communeRelations = relations(commune, ({ one, many }) => ({
	acv: one(communeAcv),
	afr: one(communeAfr),
	communeParent: one(commune, {
		fields: [commune.parentId],
		references: [commune.id],
		relationName: 'parent',
	}),
	communeFilles: many(commune, {
		relationName: 'parent',
	}),
	communeType: one(communeType, {
		fields: [commune.typeId],
		references: [communeType.id],
	}),
	departement: one(departement, {
		fields: [commune.departementId],
		references: [departement.id],
	}),
	epci: one(epci, {
		fields: [commune.epciId],
		references: [epci.id],
	}),
	metropole: one(metropole, {
		fields: [commune.metropoleId],
		references: [metropole.id],
	}),
	petr: one(petr, {
		fields: [commune.petrId],
		references: [petr.id],
	}),
	territoireIndustrie: one(territoireIndustrie, {
		fields: [commune.territoireIndustrieId],
		references: [territoireIndustrie.id],
	}),
	pvd: one(communePvd),
	va: one(communeVa),
	region: one(region, {
		fields: [commune.regionId],
		references: [region.id],
	}),
	zrrType: one(zrrType, {
		fields: [commune.zrrTypeId],
		references: [zrrType.id],
	}),
}));

export const communePvd = pgTable(
	'commune_pvd',
	{
		communeId: varchar('commune_id', { length: 5 }).notNull(),
	},
	(table) => ({
		pkCommunePvd: primaryKey({ name: 'pk_commune_pvd', columns: [table.communeId] }),
		fkCommunePvd: foreignKey({
			columns: [table.communeId],
			foreignColumns: [commune.id],
			name: 'fk_commune_pvd__commune',
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const communePvdRelations = relations(communePvd, ({ one }) => ({
	commune: one(commune, {
		fields: [communePvd.communeId],
		references: [commune.id],
	}),
}));

export const communeAcv = pgTable(
	'commune_acv',
	{
		communeId: varchar('commune_id', { length: 5 }).notNull(),
	},
	(table) => ({
		pkCommuneAcv: primaryKey({ name: 'pk_commune_acv', columns: [table.communeId] }),
		fkCommuneAcv: foreignKey({
			columns: [table.communeId],
			foreignColumns: [commune.id],
			name: 'fk_commune_acv__commune',
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const communeAcvRelations = relations(communeAcv, ({ one }) => ({
	commune: one(commune, {
		fields: [communeAcv.communeId],
		references: [commune.id],
	}),
}));

export const communeVa = pgTable(
	'commune_va',
	{
		communeId: varchar('commune_id', { length: 5 }).notNull(),
	},
	(table) => ({
		pkCommuneVa: primaryKey({ name: 'pk_commune_va', columns: [table.communeId] }),
		fkCommuneVa: foreignKey({
			columns: [table.communeId],
			foreignColumns: [commune.id],
			name: 'fk_commune_va__commune',
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const communeVaRelations = relations(communeVa, ({ one }) => ({
	commune: one(commune, {
		fields: [communeVa.communeId],
		references: [commune.id],
	}),
}));

export const communeAfr = pgTable(
	'commune_afr',
	{
		communeId: varchar('commune_id', { length: 5 }).notNull(),
		integral: boolean('integral').default(true).notNull(),
	},
	(table) => ({
		pkCommuneAfr: primaryKey({ name: 'pk_commune_afr', columns: [table.communeId] }),
		fkCommuneAfr: foreignKey({
			columns: [table.communeId],
			foreignColumns: [commune.id],
			name: 'fk_commune_afr__commune',
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const communeAfrRelations = relations(communeAfr, ({ one }) => ({
	commune: one(commune, {
		fields: [communeAfr.communeId],
		references: [commune.id],
	}),
}));

export const territoireIndustrie = pgTable(
	'territoire_industrie',
	{
		id: varchar('id', { length: 7 }).notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkTerritoireIndustrie: primaryKey({
			name: 'pk_territoire_industrie',
			columns: [table.id],
		}),
	})
);

export const communeTerritoireIndustrie = pgTable(
	'commune__territoire_industrie',
	{
		communeId: varchar('commune_id', { length: 5 }).notNull(),
		territoireIndustrieId: varchar('territoire_industrie_id', { length: 7 }).notNull(),
	},
	(table) => ({
		pkCommuneTerritoireIndustrie: primaryKey({
			name: 'pk_commune__territoire_industrie',
			columns: [table.communeId, table.territoireIndustrieId],
		}),

		fkCommuneTerritoireIndustrieCommune: foreignKey({
			columns: [table.communeId],
			foreignColumns: [commune.id],
			name: 'fk_commune__territoire_industrie__commune',
		})
			.onDelete('cascade')
			.onUpdate('cascade'),

		fkCommuneTerritoireIndustrieTerritoireIndustrie: foreignKey({
			columns: [table.territoireIndustrieId],
			foreignColumns: [territoireIndustrie.id],
			name: 'fk_commune__territoire_industrie__territoire_industrie',
		})
			.onDelete('cascade')
			.onUpdate('cascade'),
	})
);

export const communeTerritoireIndustrieRelations = relations(
	communeTerritoireIndustrie,
	({ one }) => ({
		commune: one(commune, {
			fields: [communeTerritoireIndustrie.communeId],
			references: [commune.id],
		}),
		ti: one(territoireIndustrie, {
			fields: [communeTerritoireIndustrie.territoireIndustrieId],
			references: [territoireIndustrie.id],
		}),
	})
);

export const region = pgTable(
	'region',
	{
		id: varchar('id', { length: 2 }).notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkRegion: primaryKey({ name: 'pk_region', columns: [table.id] }),
	})
);

export const regionRelations = relations(region, ({ many }) => ({
	communes: many(commune),
	departements: many(departement),
}));

export const departement = pgTable(
	'departement',
	{
		id: varchar('id', { length: 3 }).notNull(),
		nom: varchar('nom').notNull(),
		regionId: varchar('region_id', { length: 2 }).notNull(),
	},
	(table) => ({
		pkDepartement: primaryKey({ name: 'pk_departement', columns: [table.id] }),
		fkDepartementRegion: foreignKey({
			columns: [table.regionId],
			foreignColumns: [region.id],
			name: 'fk_departement__region',
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
	})
);

export const departementRelations = relations(departement, ({ one, many }) => ({
	region: one(region, {
		fields: [departement.regionId],
		references: [region.id],
	}),
	communes: many(commune),
}));

export const metropole = pgTable(
	'metropole',
	{
		id: varchar('id', { length: 5 }).notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkMetropole: primaryKey({ name: 'pk_metropole', columns: [table.id] }),
	})
);

export const metropoleRelations = relations(metropole, ({ many }) => ({
	communes: many(commune),
}));

export const epciType = pgTable(
	'epci_type',
	{
		id: varchar('id', { length: 2 }).notNull(),
		nom: varchar('nom', { length: 50 }).notNull(),
	},
	(table) => ({
		pkEpciType: primaryKey({ name: 'pk_epci_type', columns: [table.id] }),
	})
);

export const epci = pgTable(
	'epci',
	{
		id: varchar('id', { length: 9 }).notNull(),
		typeId: varchar('epci_type_id', { length: 2 }).notNull(),
		nom: varchar('nom').notNull(),
		petrId: varchar('petr_id', { length: 4 }),
	},
	(table) => ({
		pkEpci: primaryKey({ name: 'pk_epci', columns: [table.id] }),
		fkEpciType: foreignKey({
			columns: [table.typeId],
			foreignColumns: [epciType.id],
			name: 'fk_epci__epci_type',
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
		fkEpciPetr: foreignKey({
			columns: [table.petrId],
			foreignColumns: [petr.id],
			name: 'fk_epci__petr',
		})
			.onDelete('restrict')
			.onUpdate('cascade'),
	})
);

export const epciRelations = relations(epci, ({ many }) => ({
	communes: many(commune),
}));

export const petr = pgTable(
	'petr',
	{
		id: varchar('id', { length: 4 }).notNull(),
		nom: varchar('nom').notNull(),
	},
	(table) => ({
		pkPetr: primaryKey({ name: 'pk_petr', columns: [table.id] }),
	})
);

export const petrRelations = relations(petr, ({ many }) => ({
	communes: many(commune),
}));
