import 'dotenv/config';
import { drizzle, PostgresJsDatabase } from 'drizzle-orm/postgres-js';
import postgres from 'postgres';

import * as contact from './schema/contact';
import * as admin from './schema/admin';
import * as eqEtab from './schema/equipe-etablissement';
import * as eqLoc from './schema/equipe-local';
import * as equipe from './schema/equipe';
import * as etabCrea from './schema/etablissement-creation';
import * as etablissement from './schema/etablissement';
import * as evenement from './schema/evenement';
import * as local from './schema/local';
import * as source from './schema/source';
import * as territoire from './schema/territoire';

const schema = {
	...contact,
	...admin,
	...eqEtab,
	...etabCrea,
	...eqLoc,
	...equipe,
	...etablissement,
	...local,
	...evenement,
	...territoire,
	...source,
};

class DbService {
	private static instance: DbService;
	public db: PostgresJsDatabase<typeof schema>;
	public queryClient: postgres.Sql<Record<string, unknown>>;

	private constructor() {
		const connectionString = process.env.DATABASE_URL
			? process.env.DATABASE_URL
			: `postgresql://${process.env.POSTGRES_USER}:${process.env.POSTGRES_PASSWORD}@${process.env.POSTGRES_HOST}:${process.env.POSTGRES_PORT}/${process.env.POSTGRES_DB_SEED}`;

		const queryClient = postgres(connectionString);

		this.db = drizzle(queryClient, {
			schema,
		});
		this.queryClient = queryClient;
	}

	public static getInstance(): DbService {
		if (!DbService.instance) {
			DbService.instance = new DbService();
		}

		return DbService.instance;
	}
}

const instance = DbService.getInstance();

export default instance.db;
export const queryClient = instance.queryClient;
