import { sql } from 'drizzle-orm';
import { PgColumn } from 'drizzle-orm/pg-core';

import { dbQuoteEscape } from '../lib/utils';

export function rechercheBuild(column: PgColumn, recherche: string) {
	return sql.join(
		[
			sql`REGEXP_REPLACE(unaccent(${column}), '[^a-zA-Z]+', ' ', 'g')`,
			sql`ilike`,
			sql.raw(
				`'%' || REGEXP_REPLACE(unaccent('${dbQuoteEscape(
					recherche.trim()
				)}'), '[^a-zA-Z]+', ' ', 'g') || '%'`
			),
		],
		sql.raw(' ')
	);
}
