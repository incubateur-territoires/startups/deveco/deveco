import { AnyTable, InferSelectModel, InferInsertModel, SQL, TableConfig } from 'drizzle-orm';
import { PgTable } from 'drizzle-orm/pg-core';
import postgres from 'postgres';
import { PgRelationalQuery } from 'drizzle-orm/pg-core/query-builders/query';

import * as dbQueryEchange from '../db/query/echange';
import * as dbQueryDemande from '../db/query/demande';
import * as dbQueryRappel from '../db/query/rappel';
import * as dbQueryBrouillon from '../db/query/brouillon';
import {
	ayantDroit,
	compte,
	compteDemande,
	equipeDemande,
	equipeDemandeAutorisation,
	motDePasse,
	tache,
	refreshToken,
} from '../db/schema/admin';
import {
	eqEtabContact,
	eqEtabDemande,
	eqEtabContribution,
	eqEtabEchange,
	eqEtabEtiquette,
	eqEtabRappel,
	devecoEtablissementFavori,
	eqEtabRessource,
} from '../db/schema/equipe-etablissement';
import {
	deveco,
	equipe,
	etiquette,
	geoToken,
	notifications,
	rechercheEnregistree,
	rechercheSauvegardee,
	zonage,
	territoireType,
} from '../db/schema/equipe';
import { demande, echange, echangeDemande, rappel, brouillon } from '../db/schema/suivi';
import {
	categorieJuridiqueType,
	etablissementGeoloc,
	etalabEtablissementAdresse,
	rcdEtablissementEffectifEmm13moisAnnuel,
	rcdEtablissementEffectifEmm13mois,
	nafType,
	etablissement,
	etablissementPeriode as etablissementPeriode,
	lienSuccession,
	entreprise,
	entreprisePeriode as entreprisePeriode,
	dgclSubvention,
	essFranceEntreprepriseEss,
	inpiRatioFinancier,
	bodaccProcedureCollective,
	bodaccProcedureCollectiveImport,
	apiEntrepriseEntrepriseMandataireSocial,
	apiEntrepriseEntrepriseBeneficiaireEffectif,
	apiEntrepriseEntrepriseExercice,
	apiEntrepriseEntrepriseLiasseFiscale,
	apiEntrepriseEntrepriseParticipation,
} from '../db/schema/etablissement';
import {
	eqLocContribution,
	eqLocEtiquette,
	eqLocContact,
	eqLocOccupant,
	eqLocRessource,
	eqLocRappel,
	eqLocEchange,
	eqLocDemande,
} from '../db/schema/equipe-local';
import {
	evenement,
	actionCompte,
	actionDemande,
	actionEchange,
	actionEquipeEtablissement,
	actionEtablissementCreation,
	actionEtiquetteEtablissement,
	actionEquipeLocal,
	actionRappel,
} from '../db/schema/evenement';
import { contact, contactAdresse, qpv2015, qpv2024 } from '../db/schema/contact';
import {
	etabCreaCreateur,
	etabCrea,
	etabCreaEchange,
	etabCreaDemande,
	etabCreaRappel,
	etabCreaBrouillon,
	etabCreaTransformation,
	etabCreaEtiquette,
	etabCreaProjetType,
	etabCreaSecteurActivite,
	etabCreaSourceFinancement,
	etabCreaRessource,
	etabCreaContactOrigine,
} from '../db/schema/etablissement-creation';
import {
	commune,
	epci,
	petr,
	metropole,
	departement,
	region,
	territoireIndustrie,
	communeTerritoireIndustrie,
} from '../db/schema/territoire';
import {
	local,
	localAdresse,
	localProprietairePersonneMorale,
	proprietairePersonnePhysique,
	localCategorieType,
	localNatureType,
	localMutationNatureType,
	localProprietairePersonnePhysique,
	proprietairePersonneMorale,
} from '../db/schema/local';

export type QueryResult<T extends (...args: any) => PgRelationalQuery<unknown>> =
	ReturnType<T> extends PgRelationalQuery<infer U> ? U : never;
/*  eslint-disable-next-line @typescript-eslint/no-empty-object-type */
export type New<T extends AnyTable<{}>> = InferInsertModel<T>;
export type Joins = { relation: PgTable<TableConfig>; condition: SQL<unknown> | undefined }[];
export type Wheres = (SQL<unknown> | undefined)[];
export type Cursor<T = postgres.Row> = never[] | AsyncIterable<NonNullable<T & Iterable<T>>[]>;

export type CategorieJuridiqueType = InferSelectModel<typeof categorieJuridiqueType>;
export type CategorieJuridiqueTypeWithRelations = CategorieJuridiqueType & {
	parent: CategorieJuridiqueType | null;
};

export type Commune = InferSelectModel<typeof commune>;

export type UserRole = 'deveco' | 'superadmin';
export type CompteTypeId = UserRole | 'api';
export type Compte = InferSelectModel<typeof compte> & {
	typeId: CompteTypeId;
};
export type CompteSimple = Pick<Compte, 'nom' | 'prenom' | 'email'>;
export type CompteAffecte = CompteSimple & Pick<Compte, 'id' | 'actif'>;

export type EquipeEtablissementContact = InferSelectModel<typeof eqEtabContact>;
export type EquipeEtablissementContactWithContact = EquipeEtablissementContact & {
	ctct: Contact;
};

export type EtablissementCreationCreateur = InferSelectModel<typeof etabCreaCreateur>;
export type EtablissementCreationCreateurWithContact = EtablissementCreationCreateur & {
	ctct: Contact;
};
export type EtablissementCreationCreateurWithContactWithAdresse = EtablissementCreationCreateur & {
	ctct: ContactWithAdresse;
};
export type EtablissementCreationContact = InferSelectModel<typeof eqEtabContact>;
export type EtablissementCreationContactWithContact = EtablissementCreationContact & {
	ctct: Contact;
};

export type Direction = 'desc' | 'asc';

export interface ModificationCompte {
	modifCompte: CompteSimple | null;
}

export type Demande = InferSelectModel<typeof demande>;
export type EchangeDemandeWithEchange = InferSelectModel<typeof echangeDemande> & {
	echange: Echange;
};
export type DemandeWithEchangeDemandeWithEchange = Demande & {
	echanges: EchangeDemandeWithEchange[];
};
export type DemandeWithRelations = DemandeWithEchangeDemandeWithEchange;

export type DemandeWithModification = Demande & ModificationCompte;

export type DemandeTypeId =
	| 'autre'
	// établissement
	| 'accompagnement'
	| 'economique'
	| 'fiscalite'
	| 'foncier'
	| 'immobilier'
	| 'rh'
	// local
	| 'urbanisme'
	| 'urgence'
	| 'commercialisation'
	| 'transaction_fonds'
	| 'transaction_murs';

export const echangeTypeIds = [
	'rencontre',
	'telephone',
	'courrier',
	'email',
	'transformation',
] as const;
export type EchangeTypeId = (typeof echangeTypeIds)[number];

export function demandeTypeIdValidate(string: string) {
	return [
		'accompagnement',
		'autre',
		'economique',
		'fiscalite',
		'foncier',
		'immobilier',
		'rh',
	].includes(string)
		? (string as DemandeTypeId)
		: null;
}
export type LocalFavori = 'oui' | 'non';
export function localFavoriValidate(string: string) {
	return ['oui', 'non'].includes(string) ? (string as LocalFavori) : null;
}
export type Deveco = InferSelectModel<typeof deveco>;
export type DevecoWithRelations = Deveco & {
	equipe: Equipe;
	compte: Compte;
	motDePasse: MotDePasse | null;
};
export type DevecoWithEquipeAndCompte = Deveco & {
	equipe: Equipe;
	compte: Compte;
};

export type DevecoWithCompte = Deveco & {
	compte: CompteSimple;
};

export type Echange = InferSelectModel<typeof echange>;
export type EchangeDemande = InferSelectModel<typeof echangeDemande>;

export type EchangeDemandeWithDemande = EchangeDemande & {
	demande: Demande;
};
export type EchangeWithModification = Echange & ModificationCompte;
export type EchangeWithRelations = Echange & {
	deveco: DevecoWithCompte;
};

type EchangeWithAllRelations = EchangeWithModification & {
	deveco: DevecoWithCompte;
	demandes: EchangeDemandeWithDemande[];
};

export type EchangeWithDevecoAndDemandes = Echange & {
	deveco: DevecoWithCompte;
	demandes: EchangeDemandeWithDemande[];
};

export type EchangeWithDemandes = Echange & {
	demandes: EchangeDemande[];
};

export type EntiteTypeId = 'etablissement' | 'createurs' | 'local' | 'annuaire';

export type Epci = InferSelectModel<typeof epci>;
export type Petr = InferSelectModel<typeof petr>;
export type Metropole = InferSelectModel<typeof metropole>;
export type Departement = InferSelectModel<typeof departement>;
export type Region = InferSelectModel<typeof region>;

export type TerritoireIndustrie = InferSelectModel<typeof territoireIndustrie>;
export type CommuneTerritoireIndustrie = InferSelectModel<typeof communeTerritoireIndustrie>;
export type CommuneTerritoireIndustrieWithTerritoireIndustrie = CommuneTerritoireIndustrie & {
	ti: TerritoireIndustrie;
};

export type EquipeTypeId = string;

export type EntrepriseEss = InferSelectModel<typeof essFranceEntreprepriseEss>;

export type EntrepriseRatioFinancier = InferSelectModel<typeof inpiRatioFinancier>;

export type EtablissementCreation = InferSelectModel<typeof etabCrea>;
export type EtablissementCreationWithModification = EtablissementCreation & ModificationCompte;

export type EtablissementCreationTransformation = InferSelectModel<typeof etabCreaTransformation>;

export type EtablissementEffectifMoyenMensuel = InferSelectModel<
	typeof rcdEtablissementEffectifEmm13moisAnnuel
>;
export type EtablissementEffectifMoyenAnnuel = InferSelectModel<
	typeof rcdEtablissementEffectifEmm13mois
>;

type TerritoireType = InferSelectModel<typeof territoireType>;

export type TerritoireCategorie =
	| 'communal'
	| 'intercommunal'
	| 'métropolitain'
	| 'départemental'
	| 'interdépartemental'
	| 'régional'
	| 'interrégional'
	| 'national';

export type Equipe = InferSelectModel<typeof equipe>;
export interface EquipePourAdmin {
	id: Equipe['id'];
	nom: Equipe['nom'];
	groupement: Equipe['groupement'];
	territoireType: { nom: TerritoireType['nom'] };
	type: { nom: EquipeTypeId };
	territoireCategorie: TerritoireCategorie | null;
	territoireNom: string;
	siret: Etablissement['siret'] | null;
}

export type EquipeEtablissementDemande = InferSelectModel<typeof eqEtabDemande>;
export type EquipeEtablissementDemandeWithRelations = EquipeEtablissementDemande & {
	demande: Demande;
};

export type EquipeEtablissementContribution = InferSelectModel<typeof eqEtabContribution>;
export type EquipeEtablissementContributionWithModification = EquipeEtablissementContribution &
	ModificationCompte;

export type EquipeEtablissementEchange = InferSelectModel<typeof eqEtabEchange>;
export type EquipeEtablissementEchangeWithDemande = EquipeEtablissementEchange & {
	echange: EchangeWithDemandes;
};
export type EquipeEtablissementEchangeWithDevecoAndDemande = EquipeEtablissementEchange & {
	echange: EchangeWithDevecoAndDemandes;
};
export type EquipeEtablissementEchangeWithRelations = EquipeEtablissementEchange & {
	echange: EchangeWithAllRelations;
};

export type EquipeEtablissementEtiquette = InferSelectModel<typeof eqEtabEtiquette>;
export type EquipeEtablissementEtiquetteWithRelations = EquipeEtablissementEtiquette & {
	etiquette: Etiquette;
};

export type EquipeEtablissementRappel = InferSelectModel<typeof eqEtabRappel>;
export type EquipeEtablissementRappelWithRelations = EquipeEtablissementRappel & {
	rappel: RappelWithAffecteCompte;
};

export type EquipeLocalContact = InferSelectModel<typeof eqLocContact>;

export type EquipeLocalContactWithContact = EquipeLocalContact & {
	ctct: Contact;
};

export type EquipeLocalOccupant = InferSelectModel<typeof eqLocOccupant>;

export type EquipeLocalContributionBailTypeId =
	| 'commercial'
	| 'derogatoire'
	| 'precaire'
	| 'professionnel';
export function eqLocContributionBailTypeIdValidate(string: string) {
	return ['commercial', 'derogatoire', 'precaire', 'professionnel'].includes(string)
		? (string as EquipeLocalContributionBailTypeId)
		: null;
}

export type EquipeLocalContributionVacanceTypeId =
	| 'conjoncturelle'
	| 'classique'
	| 'structurelle'
	| 'autre';
export function eqLocContributionVacanceTypeIdValidate(string: string) {
	return ['conjoncturelle', 'classique', 'structurelle', 'autre'].includes(string)
		? (string as EquipeLocalContributionVacanceTypeId)
		: null;
}

export type EquipeLocalContributionVacanceMotifTypeId =
	| 'choix'
	| 'commercialisation'
	| 'peril'
	| 'travaux'
	| 'cessation';
export function eqLocContributionVacanceMotifTypeIdValidate(string: string) {
	return ['choix', 'commercialisation', 'peril', 'travaux', 'cessation'].includes(string)
		? (string as EquipeLocalContributionVacanceMotifTypeId)
		: null;
}

export type EquipeLocalContribution = InferSelectModel<typeof eqLocContribution>;
export type EquipeLocalContributionWithModification = EquipeLocalContribution & ModificationCompte;

export type EquipeLocalEtiquette = InferSelectModel<typeof eqLocEtiquette>;
export type EquipeLocalEtiquetteWithRelations = EquipeLocalEtiquette & {
	etiquette: Etiquette;
};

export type EquipeLocalEchange = InferSelectModel<typeof eqLocEchange>;
export type EquipeLocalEchangeWithDevecoAndDemande = EquipeLocalEchange & {
	echange: EchangeWithDevecoAndDemandes;
};
export type EquipeLocalEchangeWithRelations = EquipeLocalEchange & {
	echange: EchangeWithAllRelations;
};
export type EquipeLocalDemande = InferSelectModel<typeof eqLocDemande>;
export type EquipeLocalDemandeWithRelations = EquipeLocalDemande & {
	demande: Demande;
};
export type EquipeLocalRappel = InferSelectModel<typeof eqLocRappel>;
export type EquipeLocalRappelWithRelations = EquipeLocalRappel & {
	rappel: RappelWithAffecteCompte;
};

export type EtablissementGeoloc = InferSelectModel<typeof etablissementGeoloc>;

export type EtalabEtablissementAdresse = InferSelectModel<typeof etalabEtablissementAdresse>;

export type EtablissementCreationDemande = InferSelectModel<typeof etabCreaDemande>;
export type EtablissementCreationDemandeWithRelations = EtablissementCreationDemande & {
	demande: Demande;
};

export type EtablissementCreationEchange = InferSelectModel<typeof etabCreaEchange>;
export type EtablissementCreationEchangeWithRelations = EtablissementCreationEchange & {
	echange: EchangeWithAllRelations;
};
export type EtablissementCreationEchangeWithDevecoAndDemande = EtablissementCreationEchange & {
	echange: EchangeWithDevecoAndDemandes;
};

export type EtablissementCreationEtiquette = InferSelectModel<typeof etabCreaEtiquette>;
export type EtablissementCreationEtiquetteWithRelations = EtablissementCreationEtiquette & {
	etiquette: Etiquette;
};

export type EtablissementCreationRappel = InferSelectModel<typeof etabCreaRappel>;
export type EtablissementCreationRappelWithRelations = EtablissementCreationRappel & {
	rappel: Rappel;
};

export type EtablissementCreationBrouillon = InferSelectModel<typeof etabCreaBrouillon>;
export type EtablissementCreationBrouillonWithRelations = EtablissementCreationBrouillon & {
	brouillon: Brouillon;
};

export type DevecoEtablissementFavori = InferSelectModel<typeof devecoEtablissementFavori>;

export type BodaccProcedureCollective = InferSelectModel<typeof bodaccProcedureCollective>;

export type BodaccProcedureCollectiveImport = InferSelectModel<
	typeof bodaccProcedureCollectiveImport
>;

export type Etiquette = InferSelectModel<typeof etiquette>;
export type EtiquetteTypeId = 'activite' | 'localisation' | 'mot_cle';

export type Evenement = InferSelectModel<typeof evenement>;
export type EvenementWithRelations = Pick<Evenement, 'createdAt'> & {
	compte: CompteSimple | null;
};

export type ActionEquipeEtablissement = InferSelectModel<typeof actionEquipeEtablissement>;
export type ActionEquipeEtablissementWithRelations = ActionEquipeEtablissement & {
	evenement: EvenementWithRelations;
};

export type ActionLocal = InferSelectModel<typeof actionEquipeLocal>;
export type ActionLocalWithRelations = ActionLocal & {
	evenement: EvenementWithRelations;
};

export type ActionCompte = InferSelectModel<typeof actionCompte> & {
	evenement: Evenement;
};
export type ActionDemande = InferSelectModel<typeof actionDemande> & {
	evenement: Evenement;
};

export type ActionRappel = InferSelectModel<typeof actionRappel>;
export type ActionRappelWithRelations = ActionRappel & {
	evenement: Evenement;
};

export type EvenementRoute =
	| 'etablissements'
	| 'etablissements.etiquette'
	| 'etablissement'
	| 'auth';

export type EvenementRouteSub = 'etiquette';

export type TacheFrequence = 'daily' | 'weekly' | 'monthly' | 'manuel';

type EvenementEquipeEtablissement = 'equipe_etablissement';
type EvenementEtablissementCreation = 'etablissement_creation';
type EvenementEquipeLocal = 'equipe_local';

// type EvenementEntite =
// 	| EvenementEtablissementCreation
// 	| EvenementEquipeEtablissement
// 	| 'etablissement'
// 	| 'local'
// 	| 'compte'
// 	| 'zonage'
// 	| 'sirene_update'
// 	| 'sirene_update_sirens';

// TODO on utilise les types "equipe_etablissement" et "etablissement_creation"
// mais pas "equipe_etablissement_creation" et "etablissement_creation_creation",
// est-ce que c'est voulu ?
export type EvenementTypeId =
	| 'equipe_creation'
	| 'compte_creation'
	| 'connexion'
	| 'connexion_lien'
	| 'authentification'
	| 'compte_modification'
	| 'compte_mot_de_passe_modification'
	| `${EvenementEquipeEtablissement}`
	| `${EvenementEquipeEtablissement}_affichage`
	| `${EvenementEquipeEtablissement}_modification`
	| `${EvenementEquipeEtablissement}_etiquette_description`
	| `${EvenementEquipeEtablissement}_etiquette_description_import`
	| `${EvenementEquipeEtablissement}_qualification_de_masse`
	| `${EvenementEquipeEtablissement}_cloture_date`
	| `${EvenementEquipeEtablissement}_enseigne`
	| `${EvenementEquipeEtablissement}_geolocalisation`
	| `${EvenementEquipeEtablissement}_etiquette`
	| `${EvenementEquipeEtablissement}_favori`
	| `${EvenementEquipeEtablissement}_contact`
	| `${EvenementEquipeEtablissement}_contact_import`
	| `${EvenementEquipeEtablissement}_echange`
	| `${EvenementEquipeEtablissement}_echange_import`
	| `${EvenementEquipeEtablissement}_demande`
	| `${EvenementEquipeEtablissement}_rappel`
	| `${EvenementEtablissementCreation}`
	| `${EvenementEtablissementCreation}_affichage`
	| `${EvenementEtablissementCreation}_modification`
	| `${EvenementEtablissementCreation}_etiquette`
	| `${EvenementEtablissementCreation}_qualification_de_masse`
	| `${EvenementEtablissementCreation}_suppression`
	| `${EvenementEtablissementCreation}_transformation`
	| `${EvenementEtablissementCreation}_import`
	| `${EvenementEtablissementCreation}_createur`
	| `${EvenementEtablissementCreation}_echange`
	| `${EvenementEtablissementCreation}_demande`
	| `${EvenementEtablissementCreation}_rappel`
	| `${EvenementEquipeLocal}`
	| `${EvenementEquipeLocal}_etiquette`
	| `${EvenementEquipeLocal}_etiquette_description`
	| `${EvenementEquipeLocal}_geolocalisation`
	| `${EvenementEquipeLocal}_occupant`
	| `${EvenementEquipeLocal}_contribution_nom`
	| `${EvenementEquipeLocal}_contribution_vacance`
	| `${EvenementEquipeLocal}_contribution_donnees`
	| `${EvenementEquipeLocal}_favori`
	| `${EvenementEquipeLocal}_contact`
	| `${EvenementEquipeLocal}_echange`
	| `${EvenementEquipeLocal}_demande`
	| `${EvenementEquipeLocal}_rappel`
	| 'brouillon_creation'
	| 'brouillon_modification'
	| 'brouillon_transformation'
	| 'brouillon_suppression'
	| 'zonage_creation'
	| 'zonage_modification'
	| 'zonage_suppression'
	| 'contact_creation'
	| 'contact_modification'
	| 'contact_suppression'
	| 'etiquette_creation'
	| 'etiquette_modification'
	| 'etiquette_suppression'
	| 'api_entreprise_update'
	| `sirene_update_${TacheFrequence}`
	| `sirene_update_sirens_${TacheFrequence}`;

export type ActionTypeId =
	| 'creation'
	| 'affichage'
	| 'modification'
	| 'fermeture'
	| 'reouverture'
	| 'suppression'
	| 'transformation'
	| 'export'
	| 'activation'
	| 'desactivation';

export interface ActionDiff {
	message: string;
	changes?: {
		table?: string;
		before?: unknown;
		after?: unknown;
	};
}

export type ActionEchange = InferSelectModel<typeof actionEchange>;

export type ActionEchangeWithRelations = ActionEchange & {
	evenement: EvenementWithRelations;
};
export type ActionEtablissementCreation = InferSelectModel<typeof actionEtablissementCreation>;
export type ActionEtablissementCreationWithRelations = ActionEtablissementCreation & {
	evt: EvenementWithRelations;
};

export type ActionEtiquetteEtablissement = InferSelectModel<typeof actionEtiquetteEtablissement>;

export type FileFormat = 'geojson' | 'xlsx';
export function fileFormatValidate(string: string) {
	return ['geojson', 'ndgeojson', 'xlsx'].includes(string) ? (string as FileFormat) : null;
}

export type CarteType = 'cluster' | 'etablissement';
export function carteTypeValidate(string: string) {
	return ['cluster', 'etablissement'].includes(string) ? (string as CarteType) : null;
}

export type GeoToken = InferSelectModel<typeof geoToken>;

const localCategorieTypeIds = [
	'ATE1',
	'ATE2',
	'ATE3',
	'BUR1',
	'BUR2',
	'BUR3',
	'CLI1',
	'CLI2',
	'CLI3',
	'CLI4',
	'DEP1',
	'DEP2',
	'DEP3',
	'DEP4',
	'DEP5',
	'ENS1',
	'ENS2',
	'EXC1',
	'HOT1',
	'HOT2',
	'HOT3',
	'HOT4',
	'HOT5',
	'IND1',
	'IND2',
	'MAG1',
	'MAG2',
	'MAG3',
	'MAG4',
	'MAG5',
	'MAG6',
	'MAG7',
	'SPE1',
	'SPE2',
	'SPE3',
	'SPE4',
	'SPE5',
	'SPE6',
	'SPE7',
] as const;

export type LocalCategorieTypeId = (typeof localCategorieTypeIds)[number];

// https://www.typescriptlang.org/docs/handbook/advanced-types.html
export function localCategorieTypeIdValidate(string: string): string is LocalCategorieTypeId {
	return localCategorieTypeIds.includes(string as LocalCategorieTypeId);
}

const localNatureTypeIds = [
	'AP',
	'AT',
	'AU',
	'CA',
	'CB',
	'CD',
	'CH',
	'CM',
	'DC',
	'DE',
	'LC',
	'MA',
	'ME',
	'MP',
	'PP',
	'SM',
	'U',
	'U1',
	'U2',
	'U3',
	'U4',
	'U5',
	'U6',
	'U7',
	'U8',
	'U9',
	'UE',
	'UG',
	'UN',
	'US',
] as const;

export type LocalNatureTypeId = (typeof localNatureTypeIds)[number];

export type LocalMutationNatureTypeId = 1 | 2 | 3 | 4 | 5 | 6;

// https://www.typescriptlang.org/docs/handbook/advanced-types.html
export function localNatureTypeIdValidate(string: string): string is LocalNatureTypeId {
	return localNatureTypeIds.includes(string as LocalNatureTypeId);
}

export type Local = InferSelectModel<typeof local>;

export type LocalAdresse = InferSelectModel<typeof localAdresse>;
export type LocalCategorieType = InferSelectModel<typeof localCategorieType>;

export type LocalNatureType = InferSelectModel<typeof localNatureType>;

export type LocalMutationNatureType = InferSelectModel<typeof localMutationNatureType>;

export type LocalAdresseWithCommune = LocalAdresse & {
	commune: Pick<Commune, 'id' | 'nom'> | null;
};

export type LocalAdresses = Local & {
	commune: Pick<Commune, 'nom'> | null;
	adresse: LocalAdresseWithCommune | null;
};

export type LocalProprietairePersonne = InferSelectModel<typeof localProprietairePersonnePhysique>;
export type ProprietairePersonne = InferSelectModel<typeof proprietairePersonnePhysique>;
export type LocalProprietaireEntreprise = InferSelectModel<typeof localProprietairePersonneMorale>;
export type ProprietaireEntreprise = InferSelectModel<typeof proprietairePersonneMorale>;

export type MotDePasse = InferSelectModel<typeof motDePasse>;

export interface ModificationParCompteADate {
	date: Date;
	compte: CompteSimple | null;
}

export type NafType = InferSelectModel<typeof nafType>;
export type NafTypeWithRelations = NafType & { parent: NafType | null };

export type TriEtablissement = 'creation' | 'fermeture' | 'consultation' | 'alphabetique';
export type TriDeveco =
	| 'nom'
	| 'prenom'
	| 'identifiant'
	| 'email'
	| 'connexion'
	| 'action'
	| 'statut';
export type TriContact = 'nom' | 'prenom' | 'email';
export type TriLocal = 'consultation' | 'alphabetique';

export interface PaginationParams {
	elementsParPage: number | null;
	total?: number | null;
	page: number | null;
	direction: Direction | null;
}

export type PaginationParamsEtablissement = PaginationParams & {
	tri: TriEtablissement | null;
};

export type PaginationParamsContact = PaginationParams & {
	tri: TriContact | null;
};

export type PaginationParamsLocal = PaginationParams & {
	tri: TriLocal | null;
};

export type Contact = InferSelectModel<typeof contact>;
export type ContactAdresse = InferSelectModel<typeof contactAdresse>;

export type ContactAdresseWithCommune = ContactAdresse & { commune: Commune | null };

export type ContactWithAdresse = Contact & {
	adresse: ContactAdresse | null;
};

export type ContactWithAdresseWithCommune = Contact & {
	adresse: ContactAdresseWithCommune | null;
};

export type Qpv2024 = InferSelectModel<typeof qpv2024>;
export type Qpv2015 = InferSelectModel<typeof qpv2015>;

export type Rappel = InferSelectModel<typeof rappel>;

export type RappelWithModification = Rappel & ModificationCompte;

export type RappelWithAffecteCompte = Rappel & {
	affecteCompte: CompteAffecte | null;
};
export type RappelWithEntite = NotUndefined<
	QueryResult<typeof dbQueryRappel.rappelWithRelationsGetMany>[0]
>;

export type Brouillon = InferSelectModel<typeof brouillon>;
export type BrouillonWithEntite = NotUndefined<
	QueryResult<typeof dbQueryBrouillon.brouillonWithRelationsGetMany>[0]
>;

export type DemandeWithEntite = NotUndefined<
	QueryResult<typeof dbQueryDemande.demandeWithRelationsGetMany>[0]
>;

export type EchangeWithEntite = NotUndefined<
	QueryResult<typeof dbQueryEchange.echangeWithRelationsGetMany>[0]
>;

export type EtablissementSubvention = InferSelectModel<typeof dgclSubvention>;

export type ApiEntrepriseEntrepriseBeneficiaireEffectif = InferSelectModel<
	typeof apiEntrepriseEntrepriseBeneficiaireEffectif
>;

export type ApiEntrepriseEntrepriseMandataireSocial = InferSelectModel<
	typeof apiEntrepriseEntrepriseMandataireSocial
>;

export type ApiEntrepriseEntrepriseExercice = InferSelectModel<
	typeof apiEntrepriseEntrepriseExercice
>;
export type ApiEntrepriseEntrepriseLiasseFiscale = InferSelectModel<
	typeof apiEntrepriseEntrepriseLiasseFiscale
>;
export type ApiEntrepriseEntrepriseParticipation = InferSelectModel<
	typeof apiEntrepriseEntrepriseParticipation
>;

export type Entreprise = InferSelectModel<typeof entreprise>;
export type EntreprisePeriode = InferSelectModel<typeof entreprisePeriode>;

export type Etablissement = InferSelectModel<typeof etablissement>;

export type EtablissementPeriode = InferSelectModel<typeof etablissementPeriode>;

export type LienSuccession = InferSelectModel<typeof lienSuccession>;

export type SituationGeographique = 'sur' | 'hors' | 'france';
export function situationGeographiqueValidate(string: string) {
	return ['sur', 'hors', 'france'].includes(string) ? (string as SituationGeographique) : null;
}
export type SuiviCreationEtablissement = 'createurs' | 'etablissements' | 'abandonnes';
export function suiviEtablissementCreationValidate(string: string) {
	return ['createurs', 'etablissements', 'abandonnes'].includes(string)
		? (string as SuiviCreationEtablissement)
		: null;
}

export type EtatEtablissement = 'A' | 'F' | 'S';
export function etatEtablissementValidate(string: string) {
	return ['A', 'F', 'S'].includes(string) ? (string as EtatEtablissement) : null;
}

export type GeolocParam = 'O' | 'N' | 'P';
export function geolocValidate(string: string) {
	return ['O', 'N', 'P'].includes(string) ? (string as GeolocParam) : null;
}

export type EntrepriseType = 'PME' | 'ETI' | 'GE' | 'Inconnu';
export function entrepriseTypeValidate(string: string) {
	return ['PME', 'ETI', 'GE', 'Inconnu'].includes(string) ? (string as EntrepriseType) : null;
}

export type SubventionEtablissementRecue = 'oui' | 'non';
export function subventionEtablissementRecueValidate(string: string) {
	return ['oui', 'non'].includes(string) ? (string as SubventionEtablissementRecue) : null;
}

export type Accompagnes = 'oui' | 'non';
export function accompagnesValidate(string: string) {
	return ['oui', 'non'].includes(string) ? (string as Accompagnes) : null;
}

export type Tache = InferSelectModel<typeof tache>;

export type TerritoireTypeId =
	| 'epci'
	| 'commune'
	| 'metropole'
	| 'petr'
	| 'territoire_industrie'
	| 'departement'
	| 'region'
	| 'france';
export function territoireTypeIdValidate(string: string) {
	return [
		'epci',
		'commune',
		'metropole',
		'petr',
		'territoire_industrie',
		'departement',
		'region',
		'france',
	].includes(string)
		? (string as TerritoireTypeId)
		: null;
}

export type Zonage = InferSelectModel<typeof zonage>;

export type RechercheSauvegardee = InferSelectModel<typeof rechercheSauvegardee>;

export type RechercheEnregistree = InferSelectModel<typeof rechercheEnregistree>;

export type EquipeDemande = InferSelectModel<typeof equipeDemande>;
export type EquipeDemandeAutorisation = InferSelectModel<typeof equipeDemandeAutorisation>;
export type CompteDemande = InferSelectModel<typeof compteDemande>;
export type AyantDroit = InferSelectModel<typeof ayantDroit>;

// Site Web / Article de presse / Réseaux Sociaux / Autre
export type RessourceTypeId = 'website' | 'article' | 'social' | 'url' | 'autre';

export type RessourceEtablissement = InferSelectModel<typeof eqEtabRessource>;
export type RessourceEtablissementCreation = InferSelectModel<typeof etabCreaRessource>;
export type RessourceLocal = InferSelectModel<typeof eqLocRessource>;

export type RefreshToken = InferSelectModel<typeof refreshToken>;

export type Notifications = InferSelectModel<typeof notifications>;

export type NotUndefined<T> = Exclude<T, undefined>;

export type ProjetType = InferSelectModel<typeof etabCreaProjetType>;
export type SecteurActivite = InferSelectModel<typeof etabCreaSecteurActivite>;
export type SourceFinancement = InferSelectModel<typeof etabCreaSourceFinancement>;
export type ContactOrigine = InferSelectModel<typeof etabCreaContactOrigine>;

export interface Contour {
	id: string;
	nom: string;
	box: string;
	geometrie: string;
}

export type VarType = 'hausse' | 'baisse';
export function varTypeValidate(string: string) {
	return ['hausse', 'baisse'].includes(string) ? (string as VarType) : null;
}
