// TODO résoudre ces erreurs
/* eslint-disable @typescript-eslint/only-throw-error */
import { httpGet } from '../lib/httpRequest';
import { Processor } from '../lib/processor';

export interface ApiEntrepriseBeneficiaireEffectif {
	nom: string | null;
	nom_usage: string | null;
	prenoms: string[] | null;
	date_naissance: {
		annee: string;
		mois: string;
	};
	nationalite: string | null;
	pays_residence: string | null;
	modalites: {
		detention_de_capital: {
			parts_totale: number;
			parts_directes: {
				detention: boolean;
				pleine_propriete: number;
				nue_propriete: number;
			};
			parts_indirectes: {
				detention: boolean;
				par_indivision: {
					total: number;
					pleine_propriete: number;
					nue_propriete: number;
				};
				via_personnes_morales: {
					total: number;
					pleine_propriete: number;
					nue_propriete: number;
				};
			};
		};
		vocation_a_devenir_titulaire_de_parts: {
			parts_directes: {
				pleine_propriete: number;
				nue_propriete: number;
			};
			parts_indirectes: {
				par_indivision: {
					total: number;
					pleine_propriete: number;
					nue_propriete: number;
				};
				via_personnes_morales: {
					total: number;
					pleine_propriete: number;
					nue_propriete: number;
				};
			};
		};
		droits_de_vote: {
			total: number;
			directes: {
				detention: boolean;
				pleine_propriete: number;
				nue_propriete: number;
				usufruit: number;
			};
			indirectes: {
				detention: boolean;
				par_indivision: {
					total: number;
					pleine_propriete: number;
					nue_propriete: number;
					usufruit: number;
				};
				via_personnes_morales: {
					total: number;
					pleine_propriete: number;
					nue_propriete: number;
					usufruit: number;
				};
			};
		};
		pouvoirs_de_controle: {
			decision_ag: boolean;
			nommage_membres_conseil_administratif: boolean;
			autres: boolean;
		};
		representant_legal: boolean;
		representant_legal_placement_sans_gestion_deleguee: boolean;
	};
}

export type ApiEntrepriseMandataireSocial =
	| {
			type: 'personne_morale';
			fonction: string;
			code_greffe: string | null;
			libelle_greffe: string | null;
			raison_sociale: string | null;
			numero_identification: string | null;
	  }
	| {
			type: 'personne_physique';
			nom: string;
			prenom: string | null;
			fonction: string;
			nationalite: string | null;
			date_naissance: string | null;
			date_naissance_timestamp?: number;
			lieu_naissance: string | null;
			pays_naissance: string | null;
			code_pays_naissance: string | null;
			code_nationalite: string | null;
	  };

export interface ApiEntrepriseExercice {
	chiffre_affaires: string;
	date_fin_exercice: string;
}

export interface ApiEntrepriseLiasseFiscale {
	obligations_fiscales: {
		id: string;
		code: string;
		libelle: string;
		reference: null;
		regime: string;
	}[];
	declarations: {
		numero_imprime: string;
		regime: { code: string; libelle: string };
		date_declaration: string;
		date_fin_exercice: string;
		duree_exercice: number;
		millesime: number;
		donnees: {
			code_nref: string;
			valeurs: string[];
			code_absolu: string;
			intitule: string;
			code_EDI: string;
			code: string;
			code_type_donnee: string;
		}[];
	}[];
}

const ANCT_SIRET = '13002603200016';

class ApiEntreprise {
	private static instance: ApiEntreprise;
	private apiEndPoint: string;
	private apiToken: string;
	private processor: Processor;

	private constructor() {
		this.apiEndPoint = 'https://entreprise.api.gouv.fr';
		this.apiToken = process.env.API_ENTREPRISE_TOKEN || '';
		this.processor = new Processor({
			concurrent: 1000,
			time: 60 * 1000,
		});
	}

	public static getInstance(): ApiEntreprise {
		if (!ApiEntreprise.instance) {
			ApiEntreprise.instance = new ApiEntreprise();
		}

		return ApiEntreprise.instance;
	}

	private async requete<T>({
		nom,
		path,
		params,
	}: {
		nom: string;
		path: string;
		params: Record<string, unknown>;
	}) {
		if (!this.apiToken) {
			return null;
		}

		return this.processor.process(async () => {
			const urlApi = `${this.apiEndPoint}/${path}?context=deveco&recipient=${ANCT_SIRET}&object=deveco`;
			const headers = {
				Authorization: `Bearer ${this.apiToken}`,
			};

			try {
				const response = await httpGet<{ data: T }>(urlApi, headers);
				if (response.__success) {
					return response.data.data;
				}

				throw response;
			} catch (e: any) {
				if (e.__success === false) {
					console.logInfo(
						`[apiEntreprise.${nom}] Erreur dans le retour de la requête : ${JSON.stringify(
							params
						)}, statut: ${e.status}`
					);
				} else {
					// `console.info` au lieu de `console.logError` car les erreurs sont trop nombreuses
					console.logInfo(
						`[apiEntreprise.${nom}] Impossible de récupérer les informations : ${JSON.stringify(
							params
						)}`
					);
				}
			}

			return null;
		});
	}

	public async beneficiaireEffectifGetMany(siren: string) {
		const result = await this.requete<{ data: ApiEntrepriseBeneficiaireEffectif }[]>({
			nom: 'beneficiaireEffectifGetMany',
			path: `/v3/inpi/rne/unites_legales/open_data/${siren}/beneficiaires_effectifs`,
			params: {
				siren,
			},
		});

		return result && result.map(({ data }) => data);
	}

	public async mandataireSocialGetMany(siren: string) {
		const result = await this.requete<{ data: ApiEntrepriseMandataireSocial }[]>({
			nom: 'mandataireSocialGetMany',
			path: `/v3/infogreffe/rcs/unites_legales/${siren}/mandataires_sociaux`,
			params: {
				siren,
			},
		});

		return result && result.map(({ data }) => data);
	}

	public async exerciceGetMany({ siret }: { siret: string }) {
		const result = await this.requete<{ data: ApiEntrepriseExercice }[]>({
			nom: 'exerciceGetMany',
			path: `/v3/dgfip/etablissements/${siret}/chiffres_affaires`,
			params: {
				siret,
			},
		});

		return result && result.map(({ data }) => data);
	}

	public async liasseFiscaleGet({ siren }: { siren: string }) {
		// un bilan cloturé au 31 décembre de l'année N est disponible en mai de l'année N + 1
		// un bilan cloturé en décalé (ex : 31 juin) de l'année N est disponible à + 3 mois
		const now = new Date();
		const annee = now.getFullYear() - (now.getMonth() > 4 ? 1 : 2);

		// TODO si liasse année N - X n'est pas (encore) disponible, essayer année N - X - 1 ?

		const result = await this.requete<ApiEntrepriseLiasseFiscale>({
			nom: 'liasseFiscaleGet',
			path: `v3/dgfip/unites_legales/${siren}/liasses_fiscales/${annee}`,
			params: {
				siren,
			},
		});

		return result ?? null;
	}
}

export const apiEntreprise = ApiEntreprise.getInstance();
