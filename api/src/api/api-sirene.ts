import fetch from 'node-fetch';

// TODO résoudre ces erreurs
/* eslint-disable @typescript-eslint/no-misused-promises */
/* eslint-disable @typescript-eslint/only-throw-error */
import { httpGet, httpPost } from '../lib/httpRequest';
import { wait } from '../lib/utils';

export enum DiffusionStatut {
	O = 'O', // unité légale faisant partie de la diffusion publique
	N = 'N', // personne physique ayant demandé à être exclue de la diffusion publique
	P = 'P', // diffusion partielle
}

export enum Sexe {
	FEMININ = 'F',
	MASCULIN = 'M',
	ND = '[ND]',
}

export enum CategorieEntreprise {
	PME = 'PME', //  petite ou moyenne entreprise
	ETI = 'ETI', // entreprise de taille intermédiaire
	GE = 'GE', //grande entreprise
}

export enum EtatAdministratif {
	ACTIVE = 'A',
	CESSEE = 'C',
}

export enum EtatAdministratifEtablissement {
	ACTIF = 'A',
	FERME = 'F',
}

export enum ecoSocSol {
	O = 'O', // l'entreprise appartient au champ de l’économie sociale et solidaire
	N = 'N', // l'entreprise n'appartient pas au champ de l’économie sociale et solidaire
}

export enum societeMissionUniteLegale {
	O = 'O', // l'entreprise appartient au champ des sociétés à mission
	N = 'N', // l'entreprise n'appartient pas au champ des sociétés à mission
}

export enum caractereEmployeur {
	O = 'O', // unité légale employeuse
	N = 'N', // unité légale non employeuse
}

interface Header {
	total: number;
	statut: number;
	message: string;
	debut: number;
	nombre: number;
	curseur: string;
	curseurSuivant: string;
}

export interface SireneResponseUniteLegales {
	header: Header;
	unitesLegales: SireneUniteLegale[];
}

export interface SireneUniteLegale {
	anneeCategorieEntreprise: number;
	anneeEffectifsUniteLegale: number;
	categorieEntreprise: CategorieEntreprise;
	dateCreationUniteLegale: string;
	dateDernierTraitementUniteLegale: string;
	identifiantAssociationUniteLegale: string;
	nombrePeriodesUniteLegale: number;
	periodesUniteLegale: SireneUniteLegalePeriode[];
	prenom1UniteLegale: string;
	prenom2UniteLegale: string;
	prenom3UniteLegale: string;
	prenom4UniteLegale: string;
	prenomUsuelUniteLegale: string;
	pseudonymeUniteLegale: string;
	sexeUniteLegale: Sexe;
	sigleUniteLegale: string;
	siren: string;
	statutDiffusionUniteLegale: DiffusionStatut;
	trancheEffectifsUniteLegale: string;
	unitePurgeeUniteLegale: boolean;
}

export interface SireneUniteLegalePeriode {
	activitePrincipaleUniteLegale: string;
	caractereEmployeurUniteLegale: string;
	categorieJuridiqueUniteLegale: string;
	changementActivitePrincipaleUniteLegale: boolean;
	changementCaractereEmployeurUniteLegale: boolean;
	changementCategorieJuridiqueUniteLegale: boolean;
	changementDenominationUniteLegale: boolean;
	changementDenominationUsuelleUniteLegale: boolean;
	changementEconomieSocialeSolidaireUniteLegale: boolean;
	changementEtatAdministratifUniteLegale: boolean;
	changementNicSiegeUniteLegale: boolean;
	changementNomUniteLegale: boolean;
	changementNomUsageUniteLegale: boolean;
	changementSocieteMissionUniteLegale: boolean;
	dateDebut: string;
	dateFin: string;
	denominationUniteLegale: string;
	denominationUsuelle1UniteLegale: string;
	denominationUsuelle2UniteLegale: string;
	denominationUsuelle3UniteLegale: string;
	economieSocialeSolidaireUniteLegale: ecoSocSol;
	etatAdministratifUniteLegale: EtatAdministratif;
	nicSiegeUniteLegale: string;
	nomenclatureActivitePrincipaleUniteLegale: string;
	nomUniteLegale: string;
	nomUsageUniteLegale: string;
	societeMissionUniteLegale: societeMissionUniteLegale;
}

export interface SireneResponseEtablissements {
	header: Header;
	etablissements: SireneEtablissement[];
}

export interface SireneResponseLiensSuccession {
	header: {
		total: number;
		statut: number;
		message: string;
		debut: number;
		nombre: number;
		curseur: string;
		curseurSuivant: string;
	};
	liensSuccession: SireneLienSuccession[];
}

export interface SireneLienSuccession {
	continuiteEconomique: boolean;
	dateDernierTraitementLienSuccession: string;
	dateLienSuccession: string;
	siretEtablissementPredecesseur: string;
	siretEtablissementSuccesseur: string;
	transfertSiege: boolean;
}

export interface SireneEtablissement {
	siren: string;
	siret: string;
	activitePrincipaleRegistreMetiersEtablissement: string;
	adresseEtablissement: SireneEtablissementAdresse;
	anneeEffectifsEtablissement: string;
	dateCreationEtablissement: string;
	dateDernierTraitementEtablissement: string;
	etablissementSiege: boolean;
	nic: string;
	nombrePeriodesEtablissement: number;
	periodesEtablissement: SireneEtablissementPeriode[];
	statutDiffusionEtablissement: DiffusionStatut;
	trancheEffectifsEtablissement: string;
	uniteLegale: SireneEtablissementUniteLegale;
	etatAdministratifEtablissement?: EtatAdministratif;
}

export interface SireneEtablissementUniteLegale {
	activitePrincipaleUniteLegale: string;
	anneeCategorieEntreprise: number;
	anneeEffectifsUniteLegale: number;
	caractereEmployeurUniteLegale: string;
	categorieEntreprise: CategorieEntreprise;
	categorieJuridiqueUniteLegale: string;
	dateCreationUniteLegale: string;
	dateDernierTraitementUniteLegale: string;
	denominationUniteLegale: string;
	denominationUsuelle1UniteLegale: string;
	denominationUsuelle2UniteLegale: string;
	denominationUsuelle3UniteLegale: string;
	economieSocialeSolidaireUniteLegale: ecoSocSol;
	etatAdministratifUniteLegale: EtatAdministratif;
	identifiantAssociationUniteLegale: string;
	nicSiegeUniteLegale: string;
	nombrePeriodesUniteLegale: number;
	nomenclatureActivitePrincipaleUniteLegale: string;
	nomUniteLegale: string;
	nomUsageUniteLegale: string;
	prenom1UniteLegale: string;
	prenom2UniteLegale: string;
	prenom3UniteLegale: string;
	prenom4UniteLegale: string;
	prenomUsuelUniteLegale: string;
	pseudonymeUniteLegale: string;
	sexeUniteLegale: Sexe;
	sigleUniteLegale: string;
	societeMissionUniteLegale: societeMissionUniteLegale;
	statutDiffusionUniteLegale: DiffusionStatut;
	trancheEffectifsUniteLegale: string;
	unitePurgeeUniteLegale: boolean;
}

export interface SireneEtablissementAdresse {
	codeCedexEtablissement: string;
	codeCommuneEtablissement: string;
	codePaysEtrangerEtablissement: string;
	codePostalEtablissement: string;
	complementAdresseEtablissement: string;
	distributionSpecialeEtablissement: string;
	indiceRepetitionEtablissement: string;
	libelleCedexEtablissement: string;
	libelleCommuneEtablissement: string;
	libelleCommuneEtrangerEtablissement: string;
	libellePaysEtrangerEtablissement: string;
	libelleVoieEtablissement: string;
	numeroVoieEtablissement: string;
	typeVoieEtablissement: string;
	identifiantAdresseEtablissement: string;
	coordonneeLambertAbscisseEtablissement: string;
	coordonneeLambertOrdonneeEtablissement: string;
	dernierNumeroVoieEtablissement: string;
	indiceRepetitionDernierNumeroVoieEtablissement: string;
}

export interface SireneEtablissementPeriode {
	activitePrincipaleEtablissement: string;
	caractereEmployeurEtablissement: caractereEmployeur;
	changementActivitePrincipaleEtablissement: boolean;
	changementCaractereEmployeurEtablissement: boolean;
	changementDenominationUsuelleEtablissement: boolean;
	changementEnseigneEtablissement: boolean;
	changementEtatAdministratifEtablissement: boolean;
	dateDebut: string | null;
	dateFin: string | null;
	denominationUsuelleEtablissement: string | null;
	enseigne1Etablissement: string | null;
	enseigne2Etablissement: string | null;
	enseigne3Etablissement: string | null;
	etatAdministratifEtablissement: EtatAdministratifEtablissement;
	nomenclatureActivitePrincipaleEtablissement: string;
}

type SireneApiToken = {
	access_token: string;
	expires_in: number;
	refresh_expires_in: number;
	token_type: string;
	'not-before-policy': number;
	session_state: string;
	scope: string;
};

class ApiSirene {
	private static instance: ApiSirene;
	private tokenUrl: string;
	private apiUrl: string;
	private clientId: string;
	private clientSecret: string;
	private username: string;
	private password: string;
	private apiToken: string | null;
	private apiTokenExpiresAt: Date | null;

	private constructor() {
		this.tokenUrl = process.env.INSEE_TOKEN_URL || '';
		this.apiUrl = process.env.INSEE_API_URL || '';
		this.clientId = process.env.INSEE_CLIENT_ID || '';
		this.clientSecret = process.env.INSEE_CLIENT_SECRET || '';
		this.username = process.env.INSEE_USERNAME || '';
		this.password = process.env.INSEE_PASSWORD || '';
		this.apiToken = null;
		this.apiTokenExpiresAt = null;
	}

	public static getInstance(): ApiSirene {
		if (!ApiSirene.instance) {
			ApiSirene.instance = new ApiSirene();
		}

		return ApiSirene.instance;
	}

	private async authentication() {
		if (apiSirene.apiToken) {
			return;
		}

		if (
			!apiSirene.clientId ||
			!apiSirene.clientSecret ||
			!apiSirene.username ||
			!apiSirene.password
		) {
			return;
		}

		console.log(`[apiSirene.authentication] récupération d'un JWT`);

		try {
			const formData = new URLSearchParams();

			formData.append('grant_type', 'password');
			formData.append('client_id', apiSirene.clientId);
			formData.append('client_secret', apiSirene.clientSecret);
			formData.append('username', apiSirene.username);
			formData.append('password', apiSirene.password);

			const response = await fetch(apiSirene.tokenUrl, {
				method: 'POST',
				body: formData,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
				},
			});

			if (!response.ok) {
				const text = await response.text();

				throw new Error(text);
			}

			const res = (await response.json()) as SireneApiToken;

			this.apiToken = res.access_token;

			const timeoutAt = res.expires_in * 999;

			console.logInfo(`[apiSirene.authentication] expiration dans ${timeoutAt}ms`);

			setTimeout(() => {
				console.logInfo(
					`[apiSirene.authentication] effacement du jeton au bout de ${res.expires_in} secondes`
				);

				this.apiToken = null;
			}, timeoutAt);
		} catch (e) {
			console.logError(`[apiSirene.authentication] error:`, e);
		}
	}

	public async requete<T>({
		endpoint,
		start,
		end,
		cursor = '*',
		extraParams,
	}: {
		endpoint: 'siret' | 'siren' | 'siret/liensSuccession';
		start?: string;
		end?: string;
		cursor?: string;
		extraParams?: string[];
	}) {
		await apiSirene.authentication();
		if (!apiSirene.apiToken) {
			return null;
		}

		const nombre = 1000;

		const params: string[] = [];

		const dateDernierTraitement =
			endpoint === 'siret/liensSuccession'
				? 'dateDernierTraitementLienSuccession'
				: endpoint === 'siret'
					? 'dateDernierTraitementEtablissement'
					: 'dateDernierTraitementUniteLegale';

		if (start || end) {
			params.push(`${dateDernierTraitement}:[${start ?? '*'} TO ${end ?? '*'}]`);
		}

		if (extraParams?.length) {
			params.push(...extraParams);
		}

		const method = endpoint === 'siret/liensSuccession' ? 'GET' : 'POST';

		const url =
			method === 'GET'
				? `${apiSirene.apiUrl}/${endpoint}?nombre=1000&curseur=${encodeURIComponent(
						cursor
					)}&q=${encodeURIComponent(params.join(' AND '))}`
				: `${apiSirene.apiUrl}/${endpoint}`;

		const body = {
			nombre,
			curseur: cursor,
			q: encodeURIComponent(params.join(' AND ')),
		};

		const headers = {
			Authorization: `Bearer ${apiSirene.apiToken}`,
		};

		try {
			const response =
				method === 'GET' ? await httpGet<T>(url, headers) : await httpPost<T>(url, body, headers);

			if (!response.__success) {
				throw response;
			}

			return response.data;
		} catch (e) {
			console.logError(
				`[apiSirene.requete.${endpoint}] Impossible de récupérer les mises à jour SIRENE`,
				e
			);
		}

		return null;
	}

	public async *requeteByCursor<T extends { header: Header }>({
		endpoint,
		start,
		end,
		extraParams,
	}: {
		endpoint: 'siret' | 'siren' | 'siret/liensSuccession';
		start?: string;
		end?: string;
		extraParams?: string[];
	}) {
		await apiSirene.authentication();
		if (!apiSirene.apiToken) {
			return null;
		}

		const pause = 2500;

		let nextCursor = '*';
		let previousCursor = '';

		while (previousCursor !== nextCursor) {
			// quota = 30 appels par minute
			await wait(2000);

			let timeout: Promise<void> | null = null;
			let hasEnoughTimeElapsed = true;

			// await only if a request has been made
			if (nextCursor !== '*') {
				hasEnoughTimeElapsed = false;

				timeout = new Promise<void>((resolve) =>
					setTimeout(() => {
						hasEnoughTimeElapsed = true;
						return resolve();
					}, pause)
				);
			}

			try {
				const results = await apiSirene.requete<T>({
					endpoint,
					start,
					end,
					cursor: nextCursor,
					extraParams,
				});

				if (!results) {
					throw new Error(`[apiSirene.requeteByCursor.${endpoint}] API Sirene: réponse vide`);
				}

				const { header } = results;

				if (header.nombre === 0) {
					console.logInfo(
						`[apiSirene.requeteByCursor.${endpoint}] aucun résultat retourné par l'API, arrêt`
					);
					break;
				}

				yield results;

				previousCursor = header.curseur;
				nextCursor = header.curseurSuivant;
			} catch (e) {
				console.logInfo(
					`[apiSirene.requeteByCursor.${endpoint}] erreur: récupération des sources`,
					e
				);

				// TODO: sauvegarder le previousCursor dans le rapport de tâche à l'origine du plantage
				// et repartir de lui ?

				break;
			}

			if (timeout && !hasEnoughTimeElapsed) {
				await new Promise<void>((res) => (timeout ? timeout.then(() => res()) : res()));
			}
		}
	}

	public lienSuccessionGetAllByCursor({
		start,
		end,
		extraParams,
	}: {
		start?: string;
		end?: string;
		extraParams?: string[];
	}) {
		return apiSirene.requeteByCursor<SireneResponseLiensSuccession>({
			endpoint: 'siret/liensSuccession',
			start,
			end,
			extraParams,
		});
	}

	public etablissementGetAllByCursor({
		start,
		end,
		extraParams,
	}: {
		start?: string;
		end?: string;
		extraParams?: string[];
	}) {
		return apiSirene.requeteByCursor<SireneResponseEtablissements>({
			endpoint: 'siret',
			start,
			end,
			extraParams,
		});
	}

	public uniteLegaleGetAllByCursor({
		start,
		end,
		extraParams,
	}: {
		start?: string;
		end?: string;
		extraParams?: string[];
	}) {
		return apiSirene.requeteByCursor<SireneResponseUniteLegales>({
			endpoint: 'siren',
			start,
			end,
			extraParams,
		});
	}
}

export const apiSirene = ApiSirene.getInstance();
