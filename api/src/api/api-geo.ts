import fetch from 'node-fetch';
import FormData from 'form-data';
import { Parser } from 'json2csv';
import toJson from 'csvtojson';

import { Commune, Etablissement, EtalabEtablissementAdresse } from '../db/types';

export const API_GEO_MINIMUM_SCORE = 0.5;

const fieldsInfo = ['siret'] as const;
const fieldsQuery = ['query'] as const;
const fieldsParam = ['communeId', 'codePostal'] as const;

const fieldsResult = [
	'longitude',
	'latitude',
	'result_score',
	'result_housenumber',
	'result_street',
	'result_postcode',
	'result_citycode',
	'result_label',
] as const;

const fieldsQueryCsv = [...fieldsInfo, ...fieldsQuery, ...fieldsParam];

const headers = [...fieldsQueryCsv, ...fieldsResult];

type ApiGeoGeocode = Record<(typeof headers)[number], string>;

export interface GeocodeParam {
	siret: Etablissement['siret'];
	query: string;
	communeId: Commune['id'];
	codePostal: Etablissement['codePostalEtablissement'];
}

type AdresseGeocoded = Pick<
	EtalabEtablissementAdresse,
	'siret' | 'communeId' | 'numero' | 'voieNom' | 'codePostal' | 'geolocalisation' | 'score'
>;

function adresseGeocodedFormat(json: ApiGeoGeocode[]): AdresseGeocoded[] {
	const adressesGeocodedFormatted = [];

	for (const adresseGeocoded of json) {
		const {
			siret,
			communeId,
			longitude: longitudeStr,
			latitude: latitudeStr,
			result_housenumber: numero,
			result_street: voieNom,
			result_postcode: codePostal,
			result_citycode: cityCode,
			result_score: score,
		} = adresseGeocoded;

		// si la commune du résultat de l'API est sur une commune différente de la recherche
		// alors on ignore ce résultat
		if (cityCode !== communeId) {
			continue;
		}

		const longitude = longitudeStr ? Number(longitudeStr) : null;
		const latitude = latitudeStr ? Number(latitudeStr) : null;

		// PostGis attend un point dans l'ordre [longitude, latitude]
		// https://postgis.net/docs/ST_Point.html
		const geolocalisation: [number, number] | null =
			longitude && latitude ? [longitude, latitude] : null;

		adressesGeocodedFormatted.push({
			siret,
			communeId,
			numero,
			voieNom,
			codePostal,
			geolocalisation,
			score: Number(score),
		});
	}

	return adressesGeocodedFormatted;
}

class ApiGeo {
	private static instance: ApiGeo;
	private apiEndPoint: string;

	private constructor() {
		this.apiEndPoint = 'https://api-adresse.data.gouv.fr';
	}

	public static getInstance(): ApiGeo {
		if (!ApiGeo.instance) {
			ApiGeo.instance = new ApiGeo();
		}

		return ApiGeo.instance;
	}

	public async adresseGeocodeGetMany(adressesToGeocode: GeocodeParam[]) {
		const urlApiCsv = `${this.apiEndPoint}/search/csv`;

		try {
			const parserCsv = new Parser({ fields: fieldsQueryCsv.slice() });

			const csvString = parserCsv.parse(adressesToGeocode);

			const formData = new FormData();

			formData.append('data', csvString, 'data.csv');
			fieldsQuery.forEach((field) => formData.append('columns', field));
			formData.append('citycode', 'communeId');
			formData.append('postcode', 'codePostal');
			fieldsResult.forEach((field) => formData.append('result_columns', field));

			const response = await fetch(urlApiCsv, {
				method: 'POST',
				body: formData,
			});

			if (!response.ok) {
				const text = await response.text();

				throw new Error(text);
			}

			const res = await response.text();

			const json: ApiGeoGeocode[] = await toJson({
				headers: headers.slice(),
			}).fromString(res);

			return adresseGeocodedFormat(json);
		} catch (e: any) {
			console.logError(
				`[adresseGeocodeGetMany]: Impossible de géocoder les adresses: ${
					adressesToGeocode.length
				}, erreur : ${e.message ?? e}`
			);
			throw e;
		}
	}
}

export const apiGeo = ApiGeo.getInstance();
