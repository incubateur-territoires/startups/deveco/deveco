#!/bin/sh

$PSQL -c "\
DROP TABLE IF EXISTS source.source_acv;\
"

$PSQL -c "CREATE TABLE IF NOT EXISTS source.source_acv (commune_id varchar(5));"

$PSQL -c "\
TRUNCATE source.source_acv\
;"

$PSQL -c "\
copy source.source_acv (commune_id) from stdin CSV HEADER\
;" < $1

$PSQL -c "UPDATE source.source_acv set commune_id = LPAD(commune_id, 5, '0');"

$PSQL -c "\
ALTER TABLE source.source_acv ADD CONSTRAINT pk_source_acv__commune PRIMARY KEY (commune_id)
;"
