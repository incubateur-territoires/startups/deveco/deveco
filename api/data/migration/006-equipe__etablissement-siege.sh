#!/bin/sh

$PSQL -c "ALTER TABLE equipe__etablissement ADD COLUMN siege boolean NOT NULL DEFAULT false;"

$PSQL -c "\
DO \$\$\
	DECLARE\
		e RECORD;\
	BEGIN\
		FOR e IN\
			SELECT id FROM equipe\
		LOOP\
			EXECUTE '\
				update equipe__etablissement_' || e.id || ' ee\
				set\
					siege = etablissement.siege
				from
					etablissement
				where
					etablissement.id = ee.etablissement_id;\
			';\
			COMMIT;\
		END LOOP;\
	END;\
\$\$ LANGUAGE plpgsql;\
"

$PSQL -c "CREATE INDEX IDX_equipe__etablissement__siege ON equipe__etablissement (siege);"
