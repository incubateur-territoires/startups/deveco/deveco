#!/bin/sh

$PSQL -c "\
DROP TABLE IF EXISTS source.source_ess_france;\
"

$PSQL -c "\
CREATE TABLE IF NOT EXISTS source.source_ess_france (\
	entreprise_id varchar(9)\
);"

$PSQL -c "\
TRUNCATE source.source_ess_france\
;"

$PSQL -c "\
copy source.source_ess_france (entreprise_id) from stdin CSV HEADER\
;" < $1

$PSQL -c "\
copy source.source_ess_france (entreprise_id) from stdin CSV HEADER\
;" < $2

$PSQL -c "\
copy source.source_ess_france (entreprise_id) from stdin CSV HEADER\
;" < $3

$PSQL -c "\
copy source.source_ess_france (entreprise_id) from stdin CSV HEADER\
;" < $4

$PSQL -c "\
UPDATE source.source_ess_france \
set entreprise_id = LPAD(entreprise_id, 9, '0')\
;"

$PSQL -c "\
ALTER TABLE source.source_ess_france ADD CONSTRAINT pk_source_ess_france__entreprise PRIMARY KEY (entreprise_id)
;"
