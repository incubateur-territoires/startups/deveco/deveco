
INSERT INTO local (
	id,
	invariant,
	local_nature_type_id,
	local_categorie_type_id,
	commune_id,
	section,
	parcelle,
	niveau,
	surface_vente,
	surface_reserve,
	surface_exterieure_non_couverte,
	surface_stationnement_couvert,
	surface_stationnement_non_couvert
)
SELECT
	LEFT(idcom, 2) || invar,
	invar,
	cconlc,
	typeact,
	idcom,
	ccosec,
	dnupla,
	dniv,
	sprincp,
	ssecp,
	ssecncp,
	sparkp,
	sparkncp
FROM source.cerema__local;

INSERT INTO local_adresse (
	local_id,
		commune_id,
	voie_nom,
	numero,
	geolocalisation
)
SELECT
	LEFT(idcom, 2) || invar,
	idcom,
	replace(dvoilib, '  ', ' '),
	COALESCE(regexp_replace(dnvoiri, '^0*', '', 'g'), '') || COALESCE(dindic, ''),
	ST_Transform(
		ST_SetSRID(
			ST_GeomFromEWKB(geomloc::geometry),
			CASE
				WHEN LEFT(idcom, 3) in ('971', '972') THEN 5490
				WHEN LEFT(idcom, 3) = '973' THEN 2972
				WHEN LEFT(idcom, 3) = '974' THEN 2975
				ELSE 2154
			END
		), 4326
	)
FROM source.cerema__local;


INSERT INTO proprietaire_personne_physique
(id, proprietaire_compte_id, nom, prenom, naissance_date, naissance_nom, naissance_lieu, prenoms)
SELECT distinct CONCAT(idprocpte, dnulp), idprocpte, dnomus, dprnus, TO_DATE(jdatnss, 'DD/MM/YYYY'), dnomlp, dldnss, dprnlp
FROM source.cerema__local_proprietaire_physique;

INSERT INTO proprietaire_personne_morale
(id, proprietaire_compte_id, entreprise_id, nom)
SELECT CONCAT(idprocpte, dnulp), idprocpte, dsiren, ddenom
FROM source.cerema__local_proprietaire_morale;

INSERT INTO local__proprietaire_personne_physique(local_id, proprietaire_personne_physique_id)
select LEFT(l.idcom, 2) || l.invar, CONCAT(pp.idprocpte, pp.dnulp)
from source.cerema__local_proprietaire_physique pp
join source.cerema__local l using(idprocpte);

INSERT INTO local__proprietaire_personne_morale(local_id, proprietaire_personne_morale_id)
select LEFT(l.idcom, 2) || l.invar, CONCAT(pp.idprocpte, pp.dnulp)
from source.cerema__local_proprietaire_morale pp
join source.cerema__local l using(idprocpte);
