#!/bin/sh

dir=/tmp/etalab-territoires

mkdir -p $dir

for territoire in $(echo communes epci departements regions)
do
		precision="1000m"
		file_precision="${territoire}-${precision}"
		file_geojson="${file_precision}.geojson"
		file_sql="${file_precision}.sql"

		echo == $territoire ==
		echo
		
		wget https://etalab-datasets.geo.data.gouv.fr/contours-administratifs/latest/geojson/$file_geojson -O $dir/$file_geojson

		territoire_table="etalab__$(echo $territoire | sed 's/s$//')_contour"

		ogr2ogr -lco SCHEMA=source -nln ${territoire_table} $dir/$file_sql $dir/$file_geojson

		$PSQL < $dir/$file_sql

		$PSQL -c "UPDATE source.${territoire_table} SET wkb_geometry = ST_MakeValid(wkb_geometry);"

		echo
done

territoire="mairies"

file_geojson="${territoire}.geojson"
file_sql="${territoire}.sql"

echo $territoire
		
wget https://etalab-datasets.geo.data.gouv.fr/contours-administratifs/latest/geojson/$file_geojson -O $dir/$file_geojson

territoire_table="etalab__$(echo $territoire | sed 's/s$//')_point"

ogr2ogr -lco SCHEMA=source -nln ${territoire_table} $dir/$file_sql $dir/$file_geojson

$PSQL < $dir/$file_sql

$PSQL -c "UPDATE source.${territoire_table} SET wkb_geometry = ST_MakeValid(wkb_geometry);"
