ALTER TABLE "deveco__etablissement_favori" DROP CONSTRAINT "fk_deveco__etablissement_favori__etablissement";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement" DROP CONSTRAINT "fk_equipe__etablissement__etablissement";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement__contact" DROP CONSTRAINT "fk_equipe__etablissement__contact__etablissement";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement__demande" DROP CONSTRAINT "fk_equipe__etablissement__demande__etablissement";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement__echange" DROP CONSTRAINT "fk_equipe__etablissement__echange__etablissement";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement__etiquette" DROP CONSTRAINT "fk_equipe__etablissement__etiquette__etablissement";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement_exogene" DROP CONSTRAINT "fk_equipe__etablissement_exogene__etablissement";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement_portefeuille" DROP CONSTRAINT "fk_equipe__etablissement_portefeuille__etablissement";
--> statement-breakpoint
ALTER TABLE "equipe__etablissement__rappel" DROP CONSTRAINT "fk_equipe__etablissement__rappel__etablissement";
--> statement-breakpoint
ALTER TABLE "equipe__local__occupant" DROP CONSTRAINT "fk_equipe__local__occupant__occupant";
--> statement-breakpoint
ALTER TABLE "etablissement_creation" DROP CONSTRAINT "fk_etablissementCreation__modification_compte";
--> statement-breakpoint
ALTER TABLE "etablissement_creation" DROP CONSTRAINT "fk_etablissement_creation__entreprise";
--> statement-breakpoint
ALTER TABLE "etablissement_creation_transformation" DROP CONSTRAINT "fk_etablissement_creation_transfo__etablissement";
--> statement-breakpoint
ALTER TABLE "beneficiaire_effectif" DROP CONSTRAINT "fk_beneficiaire_effectif__entreprise";
--> statement-breakpoint
ALTER TABLE "entreprise_exercice" DROP CONSTRAINT "fk_entreprise_exercice__entreprise";
--> statement-breakpoint
ALTER TABLE "etablissement__qpv_2015" DROP CONSTRAINT "fk_etablissement__qpv_2015__etablissement";
--> statement-breakpoint
ALTER TABLE "etablissement__qpv_2024" DROP CONSTRAINT "fk_etablissement__qpv_2024__etablissement";
--> statement-breakpoint
ALTER TABLE "mandataire_personne_morale" DROP CONSTRAINT "fk_mandataire_personne_morale__entreprise";
--> statement-breakpoint
ALTER TABLE "mandataire_personne_physique" DROP CONSTRAINT "fk_mandataire_personne_physique__entreprise";
--> statement-breakpoint
ALTER TABLE "action_entreprise" DROP CONSTRAINT "fk_action_entreprise__entreprise";
--> statement-breakpoint
ALTER TABLE "action_equipe_etablissement" DROP CONSTRAINT "fk_action_equipe_etablissement__etablissement";
--> statement-breakpoint
ALTER TABLE "action_equipe_local_occupant" DROP CONSTRAINT "fk_action_equipe_local_occupant__occupant";
--> statement-breakpoint
ALTER TABLE "action_etablissement" DROP CONSTRAINT "fk_action_etablissement__etablissement";
--> statement-breakpoint
ALTER TABLE "action_etiquette__etablissement" DROP CONSTRAINT "fk_action_etiquette_etablissement__etablissement";
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "deveco__etablissement_favori" ADD CONSTRAINT "fk_deveco__etablissement_favori__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement" ADD CONSTRAINT "fk_equipe__etablissement__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__contact" ADD CONSTRAINT "fk_equipe__etablissement__contact__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__demande" ADD CONSTRAINT "fk_equipe__etablissement__demande__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__echange" ADD CONSTRAINT "fk_equipe__etablissement__echange__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__etiquette" ADD CONSTRAINT "fk_equipe__etablissement__etiquette__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement_exogene" ADD CONSTRAINT "fk_equipe__etablissement_exogene__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement_portefeuille" ADD CONSTRAINT "fk_equipe__etablissement_portefeuille__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__etablissement__rappel" ADD CONSTRAINT "fk_equipe__etablissement__rappel__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "equipe__local__occupant" ADD CONSTRAINT "fk_equipe__local__occupant__occupant" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation" ADD CONSTRAINT "fk_etabCrea__modification_compte" FOREIGN KEY ("modification_compte_id") REFERENCES "compte"("id") ON DELETE set null ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation" ADD CONSTRAINT "fk_etablissement_creation__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "source"."insee_sirene_api__unite_legale"("siren") ON DELETE set null ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement_creation_transformation" ADD CONSTRAINT "fk_etablissement_creation_transfo__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "beneficiaire_effectif" ADD CONSTRAINT "fk_beneficiaire_effectif__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "source"."insee_sirene_api__unite_legale"("siren") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "entreprise_exercice" ADD CONSTRAINT "fk_entreprise_exercice__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "source"."insee_sirene_api__unite_legale"("siren") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement__qpv_2015" ADD CONSTRAINT "fk_etablissement__qpv_2015__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "etablissement__qpv_2024" ADD CONSTRAINT "fk_etablissement__qpv_2024__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "mandataire_personne_morale" ADD CONSTRAINT "fk_mandataire_personne_morale__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "source"."insee_sirene_api__unite_legale"("siren") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "mandataire_personne_physique" ADD CONSTRAINT "fk_mandataire_personne_physique__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "source"."insee_sirene_api__unite_legale"("siren") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_entreprise" ADD CONSTRAINT "fk_action_entreprise__entreprise" FOREIGN KEY ("entreprise_id") REFERENCES "source"."insee_sirene_api__unite_legale"("siren") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_etablissement" ADD CONSTRAINT "fk_action_equipe_etablissement__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_equipe_local_occupant" ADD CONSTRAINT "fk_action_equipe_local_occupant__occupant" FOREIGN KEY ("occupant_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etablissement" ADD CONSTRAINT "fk_action_etablissement__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "action_etiquette__etablissement" ADD CONSTRAINT "fk_action_etiquette_etablissement__etablissement" FOREIGN KEY ("etablissement_id") REFERENCES "source"."insee_sirene_api__etablissement"("siret") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

DROP TABLE "etablissement_adresse_1";--> statement-breakpoint
DROP TABLE "etablissement_periode" CASCADE;--> statement-breakpoint
DROP TABLE "etablissement" CASCADE;--> statement-breakpoint
DROP TABLE "personne_morale";--> statement-breakpoint
DROP TABLE "personne_physique";--> statement-breakpoint
DROP TABLE "entreprise_periode" CASCADE;--> statement-breakpoint
DROP TABLE "entreprise" CASCADE;--> statement-breakpoint
DROP TABLE "entreprise_type";--> statement-breakpoint
