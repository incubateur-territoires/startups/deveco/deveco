#!/bin/sh

$PSQL -c "\
DROP TABLE IF EXISTS source.source_pvd;\
"

$PSQL -c "CREATE TABLE IF NOT EXISTS source.source_pvd (commune_id varchar(5));"

$PSQL -c "\
TRUNCATE source.source_pvd\
;"

$PSQL -c "\
copy source.source_pvd (commune_id) from stdin CSV HEADER\
;" <$1

$PSQL -c "UPDATE source.source_pvd set commune_id = LPAD(commune_id, 5, '0');"

$PSQL -c "\
ALTER TABLE source.source_pvd ADD CONSTRAINT pk_source_pvd__commune PRIMARY KEY (commune_id)
;"
