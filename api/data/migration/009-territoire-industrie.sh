#!/bin/sh

$PSQL -c "\
DROP TABLE IF EXISTS source.source_anct_commune_territoire_industrie;\
"

$PSQL -c "CREATE TABLE IF NOT EXISTS source.source_anct_commune_territoire_industrie (\
    insee_com varchar(5),\
    lib_com varchar,\
    id_ti varchar(7),\
    lib_ti varchar\
);"

$PSQL -c "\
TRUNCATE source.source_anct_commune_territoire_industrie\
;"

$PSQL -c "\
copy source.source_anct_commune_territoire_industrie from stdin CSV HEADER DELIMITER ','\
;" < $1

$PSQL -c "\
DROP TABLE IF EXISTS territoire_industrie;\
"

$PSQL -c "CREATE TABLE IF NOT EXISTS territoire_industrie (\
    id varchar(7),\
    nom varchar,\
		PRIMARY KEY (id)\
);"

$PSQL -c "INSERT INTO territoire_industrie (id, nom)\
 SELECT DISTINCT id_ti, lib_ti\
 FROM source.source_anct_commune_territoire_industrie\
;"

$PSQL -c "\
DROP TABLE IF EXISTS commune__territoire_industrie;\
"

$PSQL -c "CREATE TABLE IF NOT EXISTS commune__territoire_industrie (\
    commune_id varchar(5) REFERENCES commune(id),\
    territoire_industrie_id varchar(7) REFERENCES territoire_industrie(id),\
		PRIMARY KEY (commune_id, territoire_industrie_id)\
);"

$PSQL -c "INSERT INTO commune__territoire_industrie (commune_id, territoire_industrie_id)\
 SELECT insee_com, id_ti\
 FROM source.source_anct_commune_territoire_industrie\
;"
