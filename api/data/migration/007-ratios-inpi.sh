#!/bin/sh

$PSQL -c "\
DROP TABLE IF EXISTS source.source_inpi_ratios_financiers;\
"

$PSQL -c "CREATE TABLE IF NOT EXISTS source.source_inpi_ratios_financiers (
    siren varchar(9),\
    date_cloture_exercice date,\
    chiffre_d_affaires double precision,\
    marge_brute double precision,\
    ebe double precision,\
    ebit double precision,\
    resultat_net double precision,\
    taux_d_endettement double precision,\
    ratio_de_liquidite double precision,\
    ratio_de_vetuste double precision,\
    autonomie_financiere double precision,\
    poids_bfr_exploitation_sur_ca double precision,\
    couverture_des_interets double precision,\
    caf_sur_ca double precision,\
    capacite_de_remboursement double precision,\
    marge_ebe double precision,\
    resultat_courant_avant_impots_sur_ca double precision,\
    poids_bfr_exploitation_sur_ca_jours double precision,\
    rotation_des_stocks_jours double precision,\
    credit_clients_jours double precision,\
    credit_fournisseurs_jours double precision,\
    type_bilan varchar(1)\
);"

$PSQL -c "\
TRUNCATE source.source_inpi_ratios_financiers\
;"

$PSQL -c "\
copy source.source_inpi_ratios_financiers from stdin CSV HEADER DELIMITER ';'\
;" < $1

$PSQL -c "\
CREATE INDEX idx_source_inpi_ratios_financiers__entreprise ON source.source_inpi_ratios_financiers(siren)\
;"
