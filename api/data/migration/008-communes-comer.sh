#!/bin/sh

$PSQL -c "\
DROP TABLE IF EXISTS source.source_insee_commune_comer;\
"

$PSQL -c "CREATE TABLE IF NOT EXISTS source.source_insee_commune_comer (\
    com_comer varchar(5),\
    tncc varchar(1),\
    ncc varchar,\
    nccenr varchar,\
    libelle varchar,\
    nature_zonage varchar(3),\
    comer varchar(3),\
    libelle_comer varchar\
);"

$PSQL -c "\
TRUNCATE source.source_insee_commune_comer\
;"

$PSQL -c "\
copy source.source_insee_commune_comer from stdin CSV HEADER DELIMITER ','\
;" < $1

$PSQL -c "\
CREATE INDEX idx_source_insee_commune_comer__commune ON source.source_insee_commune_comer(com_comer)\
;"
