#!/bin/sh

$PSQL -c "\
DROP TABLE IF EXISTS source.source_rcd_etablissement_effectif_emm\
;"

$PSQL -c "\
CREATE TABLE source.source_rcd_etablissement_effectif_emm (\
		siret varchar(14),\
		date date,\
		effectif double precision\
);"

$PSQL -c "\
TRUNCATE source.source_ess_france\
;"

$PSQL -c "\
set DateStyle to ISO, DMY;\
copy source.source_rcd_etablissement_effectif_emm (siret, date, effectif) from stdin CSV HEADER\
;" <$1

$PSQL -c "\
DROP TABLE IF EXISTS etablissement_effectif_moyen_mensuel\
;"

$PSQL -c "\
CREATE TABLE etablissement_effectif_moyen_mensuel AS\
 SELECT\
		siret as etablissement_id,\
		date_part('year', date) AS annee,\
		date_part('month', date) AS mois,\
		effectif as valeur\
 FROM\
		source.source_rcd_etablissement_effectif_emm;"

$PSQL -c "\
ALTER TABLE etablissement_effectif_moyen_mensuel ADD CONSTRAINT pk_etablissement_effectif_emm PRIMARY KEY (etablissement_id, annee, mois)\
;"

# $PSQL -c "\
# ALTER TABLE etablissement_effectif_moyen_mensuel ADD CONSTRAINT fk_etablissement_effectif_emm__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id);
# "

$PSQL -c "\
CREATE INDEX IDX_etablissement_effectif_emm__etablissement ON etablissement_effectif_moyen_mensuel(etablissement_id)\
;"
