#!/bin/sh

for i in $(echo $(seq -w 1 19) 2a 2b $(seq 21 1 95) $(seq 971 1 974) 976); do echo; echo; echo $i; PGPASSWORD="" psql -U deveco -h 127.0.0.1 -d deveco_prod -p 5432 -c "\copy source.e(idprocpte, dnulp, dsiren, ddenom)
	from './sources/proprio_morale/foncier_dep${i}_proprio_morale.csv' csv header"; done


for i in $(echo $(seq -w 1 19) 2a 2b $(seq 21 1 95) $(seq 971 1 974) 976); do echo; echo; echo $i; PGPASSWORD="" psql -U deveco -h 127.0.0.1 -d deveco_prod -p 5432 -c "\copy source.cerema__local_proprietaire_physique(idprocpte, dnulp, dnomus, dprnus, dnomlp, dprnlp, jdatnss, dldnss)
	from './sources/proprio_physique/foncier_dep${i}_proprio_physique.csv' csv header"; done

for i in $(echo $(seq -w 1 19) 2a 2b $(seq 21 1 95) $(seq 971 1 974) 976); do echo; echo; echo $i; PGPASSWORD="" psql -U deveco -h 127.0.0.1 -d deveco_prod -p 5432 -c "\copy source.cerema__local(invar,dnvoiri,dindic,dvoilib,idcom,geomloc,ccosec,dnupla,cconlc,typeact,dniv,sprincp,ssecp,ssecncp,sparkp,sparkncp,idprocpte)
	from './sources/local/foncier_dep${i}_local.csv' csv header"; done

