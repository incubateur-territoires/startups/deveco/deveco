import Chance from 'chance';
import { Parser } from 'json2csv';
import toJson from 'csvtojson';

import fs from 'fs';
import path from 'path';

async function main() {
	const entreprises = await toJson().fromString(
		fs
			.readFileSync(path.join(__dirname, '../../_fixtures/_sources/sirene_entreprise.csv'))
			.toString()
	);

	const chance = new Chance();

	const communes = [
		'92004',
		'92009',
		'92024',
		'92025',
		'92036',
		'92078',
		'95018',
		'97501',
		'97502',
		'97701',
		'97801',
		'98411',
		'98412',
		'98413',
		'98414',
		'98415',
		'98611',
		'98612',
		'98613',
		'98711',
		'98712',
		'98713',
		'98714',
		'98715',
		'98716',
		'98717',
		'98718',
		'98719',
		'98720',
		'98721',
		'98722',
		'98723',
		'98724',
		'98725',
		'98726',
		'98727',
		'98728',
		'98729',
		'98730',
		'98731',
		'98732',
		'98733',
		'98734',
		'98735',
		'98736',
		'98737',
		'98738',
		'98739',
		'98740',
		'98741',
		'98742',
		'98743',
		'98744',
		'98745',
		'98746',
		'98747',
		'98748',
		'98749',
		'98750',
		'98751',
		'98752',
		'98753',
		'98754',
		'98755',
		'98756',
		'98757',
		'98758',
		'98801',
		'98802',
		'98803',
		'98804',
		'98805',
		'98806',
		'98807',
		'98808',
		'98809',
		'98810',
		'98811',
		'98812',
		'98813',
		'98814',
		'98815',
		'98816',
		'98817',
		'98818',
		'98819',
		'98820',
		'98821',
		'98822',
		'98823',
		'98824',
		'98825',
		'98826',
		'98827',
		'98828',
		'98829',
		'98830',
		'98831',
		'98832',
		'98833',
		'98901',
	];

	const natures = ['CA', 'CB', 'CD', 'CM', 'ME', 'PP', 'U', 'U1', 'US'];

	const categories = [
		'ATE1',
		'ATE2',
		'ATE3',
		'BUR1',
		'BUR2',
		'BUR3',
		'CLI1',
		'CLI2',
		'CLI3',
		'CLI4',
		'DEP1',
		'DEP2',
		'DEP3',
		'DEP4',
		'DEP5',
		'ENS1',
		'ENS2',
		'EXC1',
		'HOT1',
		'HOT2',
		'HOT3',
		'HOT4',
		'HOT5',
		'IND1',
		'IND2',
		'MAG1',
		'MAG2',
		'MAG3',
		'MAG4',
		'MAG5',
		'MAG6',
		'MAG7',
		'SPE1',
		'SPE2',
		'SPE3',
		'SPE4',
		'SPE5',
		'SPE6',
		'SPE7',
	];

	const indics = ['', '', '', '', '', '', '', '', '', '', 'B', 'B', 'T'];

	const prefixes = ['rue de', 'impasse du', 'avenue de', 'boulevard de'];

	const compteIds = chance.n(() => {
		const randomLetter = chance.letter({ casing: 'upper' });
		const randomNumber = chance.string({ length: 5, numeric: true });

		return chance.pickone(communes) + randomLetter + randomNumber;
	}, 100);

	const locals = chance.n(() => {
		const idcom = chance.pickone(communes);

		const idprocpte = chance.pickone(compteIds);

		return {
			invar: chance.string({ length: 10, numeric: true }),
			dnvoiri: `000${chance.integer({ min: 1, max: 9 })}`,
			dindic: chance.pickone(indics),
			dvoilib: `${chance.pickone(prefixes)} ${chance.last()}`,
			idcom,
			geomloc: '01010000206A080000F4E2DFA6487923412215DDEC77FF5941',
			ccosec: chance.string({ length: 2, alpha: true }),
			dnupla: `0${chance.character({ numeric: true })}`,
			cconlc: chance.pickone(natures),
			typeact: chance.pickone(categories),
			dniv: `0${chance.integer({ min: 0, max: 9 })}`,
			sprincp: chance.integer({ min: 0, max: 120 }),
			ssecp: chance.integer({ min: 0, max: 120 }),
			ssecncp: chance.integer({ min: 0, max: 120 }),
			sparkp: chance.integer({ min: 0, max: 120 }),
			sparkncp: chance.integer({ min: 0, max: 120 }),
			idprocpte,
		};
	}, 1000);

	const proprietaireMorals = compteIds.slice(0, compteIds.length / 2 + 10).flatMap((idprocpte) => {
		let i = 1;

		return chance.n(
			() => {
				const noSiren = chance.d100() > 90;
				const entreprise = noSiren
					? { id: null, nom: chance.company() }
					: chance.pickone(entreprises);

				return {
					idprocpte,
					dnulp: `0${i++}`,
					dsiren: entreprise.id,
					ddenom: entreprise.nom,
				};
			},
			chance.integer({ min: 1, max: 2 })
		);
	});

	const proprietairePhysiques = compteIds.slice(compteIds.length / 2 - 10).flatMap((idprocpte) => {
		let i = 1;

		return chance.n(
			() => {
				const dnomus = chance.last();
				const dprnus = chance.first();

				return {
					idprocpte,
					dnulp: `0${i++}`,
					dnomus,
					dprnus,
					dnomlp: chance.bool() ? chance.last() : dnomus,
					jdatnss: (
						chance.date({
							string: true,
							year: chance.integer({ min: 1950, max: 2000 }),
							american: false,
						}) as string
					)
						.replace(/\/([1-9])\//, '/0$1/')
						.replace(/^([1-9])\//, '0$1/'),
					dldnss: `${chance.string({ length: 2, numeric: true })} ${chance.city().toUpperCase()}`,
				};
			},
			chance.integer({ min: 1, max: 2 })
		);
	});

	const parserCsv = new Parser({});

	fs.writeFileSync(
		path.join(__dirname, '../../_fixtures/_sources/cerema_local_fixtures_2022.csv'),
		parserCsv.parse(locals)
	);
	fs.writeFileSync(
		path.join(__dirname, '../../_fixtures/_sources/cerema_local_proprietaire_physique_2022.csv'),
		parserCsv.parse(proprietairePhysiques)
	);
	fs.writeFileSync(
		path.join(__dirname, '../../_fixtures/_sources/cerema_local_proprietaire_morale_2022.csv'),
		parserCsv.parse(proprietaireMorals)
	);
}

main();
