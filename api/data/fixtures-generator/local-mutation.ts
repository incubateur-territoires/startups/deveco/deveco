import Chance from 'chance';
import { Parser } from 'json2csv';
import toJson from 'csvtojson';

import fs from 'fs';
import path from 'path';

async function main() {
	const locaux = await toJson().fromString(
		fs
			.readFileSync(path.join(__dirname, '../../_fixtures/_sources/cerema_local_fixtures_2022.csv'))
			.toString()
	);

	const chance = new Chance();

	let idmutation = 1;

	const localMutations = chance.n(() => {
		return {
			idmutation: idmutation++,
			idnatmut: chance.integer({ min: 1, max: 6 }),
			datemut: (
				chance.date({
					min: new Date('2010-01-01'),
					max: new Date('2023-12-31'),
				}) as Date
			)
				.toISOString()
				.slice(0, 10),
			valeurfonc: chance.integer({ min: 0, max: 10000000 }),
		};
	}, 1000);

	const localMutationLocals = chance.n(() => {
		const local = chance.pickone(locaux);

		const idloc = `${local.idcom.substring(0, 2)}${local.invar}`;

		return {
			idloc,
			idmutation: chance.pickone(localMutations).idmutation,
		};
	}, locaux.length);

	const parserCsv = new Parser({});

	fs.writeFileSync(
		path.join(__dirname, '../../_fixtures/_sources/cerema_local_mutation_fixtures_2023.csv'),
		parserCsv.parse(localMutations)
	);
	fs.writeFileSync(
		path.join(__dirname, '../../_fixtures/_sources/cerema_local__local_mutation_fixtures.csv'),
		parserCsv.parse(localMutationLocals)
	);
}

main();
