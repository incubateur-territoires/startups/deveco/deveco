#!/bin/sh

set -e

export PGPASSWORD="${POSTGRES_PASSWORD}"

eval "export ACTUAL_DB=$`echo POSTGRES_DB${TARGET_DB}`"

export PSQL="psql -h ${POSTGRES_HOST} -p ${POSTGRES_PORT} -U ${POSTGRES_USER} -d ${ACTUAL_DB}"

# ${PSQL} -t -X -c "CREATE TABLE IF NOT EXISTS migrations (id SERIAL NOT NULL, nom varchar, date timestamptz, executee boolean, PRIMARY KEY (id))"
# for migration in ./migrations/*.sql; do
#   MIGRATION_NAME=$(basename "${migration}" .sql)
#
#   DATE=$(date +"%Y-%m-%dT%H:%M:%S%:z")
#
#   ${PSQL} -t -X -c "INSERT INTO migrations(nom, date, executee) VALUES('${MIGRATION_NAME}', '${DATE}', true)"
# done

for migration in ./migrations/*.sql; do
	MIGRATION_NAME=$(basename "${migration}" .sql)

	RESULT=$(${PSQL} -t -X -c "SELECT count(*) FROM migrations WHERE nom = '${MIGRATION_NAME}' and executee")

	if [ ${RESULT} = 0 ];
	then
		echo "${MIGRATION_NAME} non effectuée"

		echo "BEGIN;" > migration_temp.sql
		cat $migration >> migration_temp.sql
		echo ";" >> migration_temp.sql
		echo "COMMIT;" >> migration_temp.sql

		${PSQL} -v ON_ERROR_STOP=1 < migration_temp.sql

		DATE=$(date +"%Y-%m-%dT%H:%M:%S%:z")

		${PSQL} -t -X -c "INSERT INTO migrations(nom, date, executee) VALUES('${MIGRATION_NAME}', '${DATE}', true)"

		rm migration_temp.sql
	else
		echo "${MIGRATION_NAME} déjà effectuée"
	fi
done
