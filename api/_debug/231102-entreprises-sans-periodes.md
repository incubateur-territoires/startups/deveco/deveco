# Entreprises sans périodes

Des entreprises n'ont pas de périodes.

## Erreur

### Combien d'entreprises n'ont pas de période ?

```sql
select count(*)
from entreprise
where not exists (
  select 1 from entreprise_periode
  where entreprise_periode.entreprise_id = entreprise.id
);
```

count: 0

```sql
select id
from entreprise
where not exists (
  select 1 from entreprise_periode
  where entreprise_periode.entreprise_id = entreprise.id
  and entreprise_periode.fin_date is null
);

select * from source.source_api_sirene_unite_legale_periode where siren = '885112771';


```

### Combien d'entreprises n'ont pas de périodes alors que la source existe ?

```sql
select count(*)
from entreprise
where not exists (
  select 1 from entreprise_periode
  where entreprise_periode.entreprise_id = entreprise.id
)
and exists (
  select 1 from source.source_api_sirene_unite_legale_periode
  where source.source_api_sirene_unite_legale_periode.siren = entreprise.id
);
```

count : 0

#### Combien d'établissement n'ont pas de période sans date de fin ?

```sql
select count(*)
from etablissement
where not exists (
  select 1 from etablissement_periode
  where etablissement_periode.etablissement_id = etablissement.id
  and etablissement_periode.fin_date is null
);
```

count: 47

#### Combien d'entreprises n'ont pas d'établissement ?

```sql
select count(*)
from entreprise
left join etablissement on etablissement.entreprise_id = entreprise.id
where etablissement.id is null;
```

count: 275714
