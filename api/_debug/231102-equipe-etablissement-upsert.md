# Pas de lignes construites pour la table `equipe__etablissement`

## Erreur

log:

```log
[equipeEtablissementUpsertMany] Pas de lignes construites pour la table equipe__etablissement pour l'équipe 352, ids d'établissements : 31855042300012
```

- [Grafana](https://grafana.incubateur.anct.gouv.fr/explore?orgId=4&left=%7B%22datasource%22:%22tA89vhW4k%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22editorMode%22:%22builder%22,%22expr%22:%22%7Bcontainer%3D%5C%22backend%5C%22%7D%20%7C%3D%20%60%60%22,%22queryType%22:%22range%22%7D%5D,%22range%22:%7B%22from%22:%221698913500000%22,%22to%22:%221698914100000%22%7D%7D)

## Recherche du bug

### Que retourne la fonction `equipeEtablissementValueGetMany` pour l'équipe 352 et l'établissement '31855042300012' ?

#### sur `equipe__commune_vue`

```sql
select *
from equipe__commune_vue
join etablissement on etablissement.commune_id = equipe__commune_vue.commune_id
join entreprise on entreprise.id = etablissement.entreprise_id
join entreprise_periode on entreprise_periode.fin_date is null and entreprise_periode.entreprise_id = entreprise.id
join categorie_juridique_type on categorie_juridique_type.id = entreprise_periode.categorie_juridique_type_id
join etablissement_periode on etablissement_periode.fin_date is null and etablissement_periode.etablissement_id = etablissement.id
join etablissement_adresse_1 on etablissement_adresse_1.etablissement_id = etablissement.id
join commune on commune.id = etablissement.commune_id
where equipe__commune_vue.equipe_id = 352 and etablissement.id in ('31855042300012');
```

0 résultat

Sans les jointures

```sql
select *
from equipe__commune_vue
join etablissement on etablissement.commune_id = equipe__commune_vue.commune_id
where equipe__commune_vue.equipe_id = 352 and etablissement.id in ('31855042300012');
```

0 résultat

#### sur `equipe__etablissement_exogene`

```sql
select *
from equipe__etablissement_exogene
join etablissement on etablissement.id = equipe__etablissement_exogene.etablissement_id
join entreprise on entreprise.id = etablissement.entreprise_id
join entreprise_periode on entreprise_periode.fin_date is null and entreprise_periode.entreprise_id = entreprise.id
join categorie_juridique_type on categorie_juridique_type.id = entreprise_periode.categorie_juridique_type_id
join etablissement_periode on etablissement_periode.fin_date is null and etablissement_periode.etablissement_id = etablissement.id
join etablissement_adresse_1 on etablissement_adresse_1.etablissement_id = etablissement.id
join commune on commune.id = etablissement.commune_id
where equipe__etablissement_exogene.equipe_id = 352 and etablissement.id in ('31855042300012');
```

0 résultat

sans les jointures

```sql
select *
from equipe__etablissement_exogene
join etablissement on etablissement.id = equipe__etablissement_exogene.etablissement_id
where equipe__etablissement_exogene.equipe_id = 352 and etablissement.id in ('31855042300012');
```

1 résutat

C'est un établissement exogène.

Donc le problème vient d'une jointure dans la requête ci-dessus.

### Que contiennent les jointures ?

#### `entreprise_periode`

```sql
select *
from entreprise_periode
where entreprise_periode.entreprise_id = '318550423';
```

3 résultats dont aucune `entreprise_periode` sans date de fin.

L'absence de résultat dans la requête sur `equipe__etablissement_exogene` vient de cette absence de période sans date de fin.

### Que contient la source ?

#### `source_api_sirene_unite_legale_periode`

```sql
select *
from source.source_api_sirene_unite_legale_periode
where source_api_sirene_unite_legale_periode.siren = '318550423';
```

7 résultats dont une période sans date de fin.

Le problème vient probablement de la copie des périodes de `source_api_sirene_unite_legale_periode` vers la table `entreprise_periode`.

### Quelle est l'ampleur du problème ?

#### Combien d'entreprises n'ont pas de période sans date de fin ?

```sql
select count(*)
from entreprise
where not exists (
  select 1 from entreprise_periode
  where entreprise_periode.entreprise_id = entreprise.id
  and entreprise_periode.fin_date is null
);
```

count: 3901

#### Combien d'entreprises n'ont pas de période sans date de fin alors que les sources mise à jour la veille ont une date de fin ?

```sql
select count(*)
from entreprise
where not exists (
  select 1 from entreprise_periode
  where entreprise_periode.entreprise_id = entreprise.id
  and entreprise_periode.fin_date is null
)
and exists (
  select 1 from source.source_api_sirene_unite_legale_periode
  where source.source_api_sirene_unite_legale_periode.siren = entreprise.id
  and source.source_api_sirene_unite_legale_periode.date_fin is null
  and source.source_api_sirene_unite_legale_periode.mise_a_jour_at > (select current_date - interval '1 day')
);
```

count : 113

#### Combien d'entreprises ont une période sans date de fin et les sources mise à jour la veille ont aussi une date de fin ?

```sql
select count(*)
from entreprise
where exists (
  select 1 from entreprise_periode
  where entreprise_periode.entreprise_id = entreprise.id
  and entreprise_periode.fin_date is null
)
and exists (
  select 1 from source.source_api_sirene_unite_legale_periode
  where source.source_api_sirene_unite_legale_periode.siren = entreprise.id
  and source.source_api_sirene_unite_legale_periode.date_fin is null
  and source.source_api_sirene_unite_legale_periode.mise_a_jour_at > (select current_date - interval '1 day')
);
```

count : 35037

On a donc `113 / 35037` = 0,32% d'erreur lors de la mise à jour quotidienne.

En local,`42 / 20350` = 0,2%.

Pourquoi ?

#### Explication

La fonction de mise à jour des périodes `entreprisePeriodeUpsertMany` supprime toutes les périodes ayant le même `entrepriseId` avant d'insérer les nouvelles.

On utilise un curseur pour mettre à jour les périodes.

Si des périodes d'une même entreprise se trouvent dans 2 itérations différentes du curseur, les périodes insérées lors de la première itération sont supprimées lors de l'itération suivante.

Des erreurs similaires existent lors de la mise à jour des périodes:

- d'unités légales `sireneUniteLegaleUpsertMany`
- d'entreprise `entreprisePeriodeUpsertMany`
- d'établissement `etabissementPeriodeUpsertMany`

#### Correction

- déployer [ce commit](https://gitlab.com/incubateur-territoires/startups/deveco/deveco/-/commit/896d5efc5304b7c8c5be3c0cafe2aa33ee4d1557):
- pour corriger les erreurs, lancer les scripts de migration depuis l'url : `api-url/admin/sirene/token?tache=entreprise&jours=30` et `api-url/admin/sirene/token?tache=etablissement&jours=30`
