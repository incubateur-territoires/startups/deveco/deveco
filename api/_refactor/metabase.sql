create table metabase_s1_evenement AS (
    ------------------------------------------------------------------
    -- STEP 1: Filter only your relevant "evenement" rows
    --         and reduce columns to what you actually need.
    ------------------------------------------------------------------
    SELECT e.created_at::date AS date,
        e.id AS evenement_id,
        eq.id AS equipe_id
    FROM public.evenement e
        JOIN public.compte c ON e.compte_id = c.id
        JOIN public.deveco d ON c.id = d.compte_id
        JOIN public.equipe eq ON eq.id = d.equipe_id
    WHERE c.compte_type_id = 'deveco'
        AND c.actif
);
create table metabase_daily_count AS (
    ------------------------------------------------------------------
    -- STEP 2: Pre-aggregate by (equipe_id, date).
    --         This avoids doing COUNT(DISTINCT ...) repeatedly
    --         over the big CROSS JOIN.
    ------------------------------------------------------------------
    SELECT s1.equipe_id,
        s1.date,
        COUNT(DISTINCT s1.evenement_id) AS activity_count
    FROM metabase_s1_evenement s1
    GROUP BY s1.equipe_id,
        s1.date
);
create table metabase_daily_calendar AS (
    ------------------------------------------------------------------
    -- STEP 3: For each equipe, generate all dates from 2022-06-01
    --         to today, then LEFT JOIN the pre-aggregated counts.
    --         Missing days will come through with 0.
    ------------------------------------------------------------------
    SELECT eq.equipe_id,
        g.date::date,
        COALESCE(dc.activity_count, 0) AS activity_count
    FROM (
            SELECT DISTINCT equipe_id
            FROM public.deveco
        ) eq
        CROSS JOIN generate_series('2022-06-01'::date, CURRENT_DATE, '1 day') g(date)
        LEFT JOIN metabase_daily_count dc ON dc.equipe_id = eq.equipe_id
        AND dc.date = g.date
);
create table metabase_daily_snapshot AS (
    ------------------------------------------------------------------
    -- STEP 4: Compute rolling sums with window functions
    --          in one shot.  We do 7d, 30d, ever, etc.
    ------------------------------------------------------------------
    SELECT equipe_id,
        date,
        /* cumulative all-time sum */
        SUM(activity_count) OVER (
            PARTITION BY equipe_id
            ORDER BY date ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
        ) AS log_count_ever,
        /* rolling 30-day sum */
        SUM(activity_count) OVER (
            PARTITION BY equipe_id
            ORDER BY date ROWS BETWEEN 29 PRECEDING AND CURRENT ROW
        ) AS log_count_30d,
        /* rolling 7-day sum */
        SUM(activity_count) OVER (
            PARTITION BY equipe_id
            ORDER BY date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW
        ) AS log_count_7d,
        /* 1st, 2nd, 3rd, 4th week flags */
        CASE
            WHEN SUM(activity_count) OVER (
                PARTITION BY equipe_id
                ORDER BY date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW
            ) > 0 THEN 1
            ELSE 0
        END AS log_count_1stw,
        CASE
            WHEN SUM(activity_count) OVER (
                PARTITION BY equipe_id
                ORDER BY date ROWS BETWEEN 13 PRECEDING AND 7 PRECEDING
            ) > 0 THEN 1
            ELSE 0
        END AS log_count_2ndw,
        CASE
            WHEN SUM(activity_count) OVER (
                PARTITION BY equipe_id
                ORDER BY date ROWS BETWEEN 20 PRECEDING AND 14 PRECEDING
            ) > 0 THEN 1
            ELSE 0
        END AS log_count_3rdw,
        CASE
            WHEN SUM(activity_count) OVER (
                PARTITION BY equipe_id
                ORDER BY date ROWS BETWEEN 27 PRECEDING AND 21 PRECEDING
            ) > 0 THEN 1
            ELSE 0
        END AS log_count_4thw
    FROM metabase_daily_calendar
);
create table metabase_equipe_created AS (
    ------------------------------------------------------------------
    -- STEP 5: Find the earliest creation date (for sign-in checks).
    ------------------------------------------------------------------
    SELECT equipe_id,
        MIN(created_at::date) AS created_at
    FROM public.deveco
    GROUP BY equipe_id
);
create table metabase_metabase_equipe_user_type_daily_snapshot AS (
    ------------------------------------------------------------------
    -- STEP 6: Combine the daily_snapshot with equipe creation date
    --          and compute "user_type_v1 / v2" logic in one pass.
    ------------------------------------------------------------------
    SELECT ds.equipe_id,
        ds.date,
        ds.log_count_ever,
        ds.log_count_30d,
        ds.log_count_7d,
        ds.log_count_1stw,
        ds.log_count_2ndw,
        ds.log_count_3rdw,
        ds.log_count_4thw,
        CASE
            WHEN ds.date >= ec.created_at THEN TRUE
            ELSE FALSE
        END AS is_signin,
        ----------------------------------------------------------------
        -- user_type_v2
        ----------------------------------------------------------------
        CASE
            WHEN ds.log_count_30d < 25
            AND ds.log_count_30d > 0 THEN 'test_v2'
            WHEN ds.log_count_30d >= 25 THEN 'actif_v2'
            WHEN ds.log_count_30d = 0
            AND ds.log_count_ever = 0 THEN 'never_user_v2'
            WHEN ds.log_count_30d = 0 THEN 'dem_v2'
            ELSE 'no_type'
        END AS user_type_v2
    FROM metabase_daily_snapshot ds
        JOIN metabase_equipe_created ec ON ds.equipe_id = ec.equipe_id
);
----------------------------------------------------------------------
-- STEP 7: Final SELECT, grouping every Sunday (dow = 0)
----------------------------------------------------------------------
SELECT CONCAT(
        TO_CHAR(date -6, 'DD/MM'),
        '-',
        TO_CHAR(date, 'DD/MM')
    ) AS semaine,
    SUM(
        CASE
            WHEN is_signin THEN 1
            ELSE 0
        END
    ) AS "# inscrits",
    SUM(
        CASE
            WHEN user_type_v2 IN ('actif_v2', 'test_v2') THEN 1
            ELSE 0
        END
    ) AS "# actifs"
FROM metabase_metabase_equipe_user_type_daily_snapshot
WHERE EXTRACT(
        DOW
        FROM date
    ) = 0
GROUP BY date
ORDER BY date ASC;
---
WITH metabase_equipe_user_type_daily_snapshot AS (
    --//STEP 4 : Get metabase_equipe_user_type_daily_snapshot
    WITH equipe_stat2_daily_snapshot AS (
        --//STEP 3 : Get equipe_stat2_daily_snapshot -> activity count on 30d period
        WITH equipe_stat1_daily_snapshot AS (
            --//STEP 2 : Get equipe_stat1_daily_snapshot -> cross join 's1_evenement' and daily date range
            SELECT equipe.equipe_id,
                d.date::date,
                count(distinct s1_evenement.id) AS activity_count
            FROM (
                    SELECT DISTINCT "public"."deveco"."equipe_id" AS "equipe_id",
                        "public"."deveco"."id" AS "deveco_id"
                    FROM "public"."deveco"
                ) equipe
                CROSS JOIN (
                    SELECT generate_series('2022-06-01', CURRENT_DATE, '1 day'::interval) AS date
                ) d
                LEFT JOIN (
                    --//STEP 1 : s1_evenement -> Get all log events
                    SELECT "public"."evenement"."created_at" AS "date",
                        "public"."evenement"."id" AS "id",
                        "public"."evenement"."evenement_type_id" AS "type_id",
                        "public"."equipe"."id" AS "equipe_id",
                        "public"."deveco"."id" AS "deveco_id",
                        "public"."compte"."id" AS "compte_id"
                    FROM "public"."evenement"
                        LEFT JOIN "public"."compte" ON "public"."evenement"."compte_id" = "public"."compte"."id"
                        LEFT JOIN "public"."deveco" ON "public"."compte"."id" = "public"."deveco"."compte_id"
                        LEFT JOIN "public"."equipe" ON "public"."equipe"."id" = "public"."deveco"."equipe_id"
                    WHERE "public"."compte"."compte_type_id" = 'deveco'
                        AND "public"."compte"."actif" --- Q? : which other account type ?
                        --\\END STEP 1
                ) s1_evenement ON equipe.equipe_id = s1_evenement.equipe_id
                AND d.date::date = s1_evenement.date::date
            GROUP BY equipe.equipe_id,
                d.date
            ORDER BY equipe.equipe_id,
                d.date --\\END STEP 2
        )
        SELECT equipe_id,
            equipe_stat1_daily_snapshot.date,
            SUM(equipe_stat1_daily_snapshot.activity_count) OVER (
                PARTITION BY equipe_stat1_daily_snapshot.equipe_id
                ORDER BY equipe_stat1_daily_snapshot.date
            ) as log_count_ever,
            SUM(equipe_stat1_daily_snapshot.activity_count) OVER (
                PARTITION BY equipe_stat1_daily_snapshot.equipe_id
                ORDER BY equipe_stat1_daily_snapshot.date ROWS BETWEEN 29 PRECEDING AND 0 PRECEDING
            ) as log_count_30d,
            -- on 30 last days including d day. if not user type change the day after
            SUM(equipe_stat1_daily_snapshot.activity_count) OVER (
                PARTITION BY equipe_stat1_daily_snapshot.equipe_id
                ORDER BY equipe_stat1_daily_snapshot.date ROWS BETWEEN 6 PRECEDING AND 0 PRECEDING
            ) as log_count_7d,
            CASE
                WHEN SUM(equipe_stat1_daily_snapshot.activity_count) OVER (
                    PARTITION BY equipe_stat1_daily_snapshot.equipe_id
                    ORDER BY equipe_stat1_daily_snapshot.date ROWS BETWEEN 6 PRECEDING AND 0 PRECEDING
                ) > 0 THEN 1
                ELSE 0
            END as log_count_1stw,
            CASE
                WHEN SUM(equipe_stat1_daily_snapshot.activity_count) OVER (
                    PARTITION BY equipe_stat1_daily_snapshot.equipe_id
                    ORDER BY equipe_stat1_daily_snapshot.date ROWS BETWEEN 13 PRECEDING AND 7 PRECEDING
                ) > 0 THEN 1
                ELSE 0
            END as log_count_2ndw,
            CASE
                WHEN SUM(equipe_stat1_daily_snapshot.activity_count) OVER (
                    PARTITION BY equipe_stat1_daily_snapshot.equipe_id
                    ORDER BY equipe_stat1_daily_snapshot.date ROWS BETWEEN 20 PRECEDING AND 14 PRECEDING
                ) > 0 THEN 1
                ELSE 0
            END as log_count_3rdw,
            CASE
                WHEN SUM(equipe_stat1_daily_snapshot.activity_count) OVER (
                    PARTITION BY equipe_stat1_daily_snapshot.equipe_id
                    ORDER BY equipe_stat1_daily_snapshot.date ROWS BETWEEN 27 PRECEDING AND 21 PRECEDING
                ) > 0 THEN 1
                ELSE 0
            END as log_count_4thw
        FROM equipe_stat1_daily_snapshot
        GROUP BY equipe_id,
            date,
            activity_count
        ORDER BY equipe_id,
            date --\\END STEP 3
    )
    SELECT equipe_stat2_daily_snapshot.equipe_id,
        date,
        CASE
            WHEN date < equ.created_at THEN FALSE
            ELSE TRUE
        END AS is_signin,
        CASE
            WHEN log_count_30d < 25
            AND log_count_30d > 0 THEN 'test_v2'
            WHEN log_count_30d >= 25 THEN 'actif_v2'
            WHEN log_count_30d = 0
            AND log_count_ever = 0 THEN 'never_user_v2'
            WHEN log_count_30d = 0 THEN 'dem_v2'
            ELSE 'no_type'
        END AS user_type_v2
    FROM equipe_stat2_daily_snapshot
        LEFT JOIN (
            SELECT "public"."deveco"."equipe_id" AS "equipe_id_",
                MIN("public"."deveco"."created_at") AS created_at
            FROM "public"."deveco"
            GROUP BY "public"."deveco"."equipe_id"
        ) equ ON equ.equipe_id_ = equipe_stat2_daily_snapshot.equipe_id --\\ END STEP 4
)
SELECT --date,
    CONCAT(
        TO_CHAR(date -6, 'DD/MM'),
        '-',
        TO_CHAR(date, 'DD/MM')
    ) AS "semaine",
    SUM(
        CASE
            WHEN is_signin THEN 1
            ELSE 0
        END
    ) AS "# inscrits",
    SUM(
        CASE
            WHEN user_type_v2 = 'actif_v2' THEN 1
            ELSE 0
        END
    ) + SUM(
        CASE
            WHEN user_type_v2 = 'test_v2' THEN 1
            ELSE 0
        END
    ) AS "# actifs"
FROM metabase_equipe_user_type_daily_snapshot
WHERE extract(
        dow
        from date
    ) = 0
GROUP BY date
ORDER BY date ASC;
- -