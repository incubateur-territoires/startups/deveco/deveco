# Données publiques

```mermaid
erDiagram
     exercice {
        date cloture
        integer chiffre_affaire
    }

    effectifs {
        date cloture
        integer effectif
    }

    etablissement {
        integer id
        string siren
        string siret
        string nic
        string adresse
        string etat_administratif
        date date_creation
    }

    entreprise {
        string denomination
        boolean ess
    }

    entreprise ||--|{ etablissement  : "possède"
    etablissement |o--o| etablissement : "succède à"
    etablissement ||--o| entreprise : "est siège de"
    etablissement ||--o{ exercice : "possède"
    etablissement ||--o{ effectifs : "possède"

    etablissement }|--|| commune : "se situe sur"
    etablissement }|--o| geolocalisation : "est situé à"
    qpv |o--|{ geolocalisation : "contient"
    qpv }|--|| commune : "se situe sur"
```
