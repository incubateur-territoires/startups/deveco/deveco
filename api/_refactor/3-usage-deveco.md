# Usage Deveco

## Affichage d'une page d'un établissement

- 1ère visualisation : requête API entreprise + mise à jour
- récupère l'`equipe_etablissement` et ses relations
  - `entreprise`
    - `etablissement` dans `equipe_etablissement` sauf lui-même
  - `successeur` et `predecesseur` dans `etablissement` (car pas forcément sur une commune gérée)
  - `personne`
  - `etiquette`
  - `echange`
  - `demande`
  - `rappel`
  - `exercice`
  - `etablissement_creation`
  - `favori`

## Ajout d'un établissement exogène

1. un utilisateur `deveco` sélectionne un siret dans `etablissement`. Valide.
2. étapes :

   - crée un `equipe__etablissement_exogene`
   - rafraîchit la materialized view de cette équipe `equipe_etablissement`
   - enregistre le diff sur `equipe_etablissement_exogene`

## Interactions avec un établissement, données personnalisées

### ajout d'un élément de catégorisation (`étiquette`)

1. un utilisateur `deveco` crée une `étiquette` et il l'ajoute sur un `établissement`
2. étapes :

- crée une `étiquette`

### ajout d'un élément de suivi (`echange`, `demande`, `contact`, `rappel`, etc.)

1. un utilisateur `deveco` crée un `echange` et il l'ajoute sur un `établissement`
2. étapes :

   - crée un `echange`
   - lie l'`etablissement` à l'`échange`

## Recherche

### Sur les établissements

- utilise des filtres :

  - publiques =>

    - `TER` (nom, adresse, naf, cat juridique, zrr, qpv, etc.)
    - exception : `exogène`, qui dépend des communes de l'équipe, mais basé sur des données publiques

  - personalisés =>

    - `entité` (`étiquettes`, `contact`, `future enseigne`, `création d'établissement`, `exogène`)
    - `fiche` (`échange`, `rappel`, `demande`, etc.)

- utilise des tris :

  - publiques =>

    - `creation_date`

  - personalisés =>

    - `consultation_equipe_date`
