# Questions expert

## Contexte

PostgresSQL 14 sous Docker.

Contenu de la base de données :

- Base SIRENE (~34 millions d'établissements, ~25 millions d'entreprises)
- Communes INSEE (35 000 communes)
- Des équipes d'utilisateurs :
  - couvrent une liste de communes
  - ajoutent des informations personnalisées sur les établissements présents sur ces communes

[Modèle de la base de données](https://gitlab.com/incubateur-territoires/startups/deveco/deveco-api/-/tree/tech/refactor/_refactor/modele)

Comment benchmarker les requêtes sans que postgres utilise le cache ?
(ex : explain analyse long, vs req rapide)

## Problèmes de performance

**On souhaite executer une requête sur les établisements qui sont couvert par une équipe.**

Les critères de recherche sont stockés dans différentes tables : `etablissement`, `entreprise_periode` et `etablissement_periode`.

```mermaid
erDiagram
  entreprise ||--o{ entreprise_periode : "possède"
  entreprise |o--o{ etablissement : "possède"

  etablissement ||--o{ etablissement_periode : ""

  etablissement }o--|| commune : "est sur"

  equipe }o--o{ commune : "couvre"
```

### Avec seulement des indexes

```sql
CREATE INDEX IDX_etablissement__commune ON etablissement (commune_id);
CREATE INDEX IDX_etablissement__creation_date ON etablissement (creation_date DESC NULLS LAST);
CREATE INDEX IDX_etablissement__entreprise ON etablissement (entreprise_id);
CREATE INDEX IDX_entreprise_periode__entreprise ON entreprise_periode (entreprise_id);
CREATE INDEX IDX_entreprise_periode__fin_date_partial ON entreprise_periode (entreprise_id) where fin_date IS NULL;
CREATE INDEX IDX_entreprise_periode__categorie_juridique ON entreprise_periode (categorie_juridique_type_id);
CREATE INDEX IDX_entreprise_periode__ess_partial ON entreprise_periode (entreprise_id) WHERE economie_sociale_solidaire;

SELECT etablissement.id, etablissement.creation_date
FROM etablissement
JOIN equipe__commune_vue ON etablissement.commune_id = equipe__commune_vue.commune_id
JOIN entreprise_periode ON (
    entreprise_periode.entreprise_id = etablissement.entreprise_id
    AND entreprise_periode.fin_date IS NULL
)
WHERE equipe__commune_vue.equipe_id = 160
    AND entreprise_periode.categorie_juridique_type_id IN ('1000')
    -- AND entreprise_periode.naf_type_id IN ('96.09Z')
    -- AND entreprise_periode.economie_sociale_solidaire
ORDER BY etablissement.creation_date DESC NULLS LAST
LIMIT 20;

```

Avec les ligne `economie_sociale_solidaire` et `naf_type_id` commentées, cette requête s'execute en 3.21ms.
https://explain.dalibo.com/plan/5de7608299006a1f

Avec uniquement la ligne `naf_type_id` commentée, on passe à 18s.
https://explain.dalibo.com/plan/ef63edda3e725a56

Avec la ligne `economie_sociale_solidaire` commentée:
https://explain.dalibo.com/plan/4f8g5h98e73fa751

### Avec les données publiques à plat dans une vue

```sql
CREATE MATERIALIZED VIEW etablissement__entreprise_vue AS
SELECT
    ET.id AS etablissement_id,
    ET.commune_id,
    ET.creation_date,
    EN.entreprise_id,
    EN.categorie_juridique_type_id as categorie_juridique_3_type_id,
    SUBSTRING(EN.categorie_juridique_type_id, 1, 2) as categorie_juridique_2_type_id,
    EN.economie_sociale_solidaire,
    EP.employeur,
    EP.actif,
    EP.naf_type_id,
    A.geolocalisation
FROM etablissement ET
JOIN entreprise_periode EN using (entreprise_id)
JOIN etablissement_periode EP on EP.etablissement_id = ET.id
JOIN adresse A ON A.id = ET.adresse_1_id
WHERE EN.fin_date IS NULL;

CREATE INDEX IDX_etablissement__entreprise_vue__creation_date ON etablissement__entreprise_vue(creation_date);
CREATE INDEX IDX_etablissement__entreprise_vue__commune ON etablissement__entreprise_vue(commune_id);
CREATE INDEX IDX_etablissement__entreprise_vue__naf_type ON etablissement__entreprise_vue(naf_type_id);
CREATE INDEX IDX_etablissement__entreprise_vue__categorie_juridique_3 ON etablissement__entreprise_vue(categorie_juridique_3_type_id);
CREATE INDEX IDX_etablissement__entreprise_vue__economie_sociale_solidaire ON etablissement__entreprise_vue(economie_sociale_solidaire);
CREATE INDEX IDX_etablissement__entreprise_vue__actif ON etablissement__entreprise_vue(actif);

ALTER TABLE etablissement__entreprise_vue CLUSTER ON IDX_etablissement__entreprise_vue__commune;
CLUSTER etablissement__entreprise_vue USING IDX_etablissement__entreprise_vue__commune;

```

requête

```sql

-- 104ms
SELECT *
FROM etablissement__entreprise_vue ET
WHERE
    categorie_juridique_3_type_id IN ('1000')
    AND V.commune_id IN ('75108')
    AND naf_type_id IN ('81.10Z')
    AND NOT economie_sociale_solidaire
    AND EE.etablissement_id IS NULL
    AND NOT employeur
ORDER BY creation_date DESC NULLS LAST
LIMIT 20;

-- 19s
SELECT *
FROM etablissement__entreprise_vue ET
JOIN equipe__commune_vue V ON ET.commune_id = V.commune_id
LEFT JOIN equipe__etablissement_exogene EE ON EE.etablissement_id = ET.etablissement_id AND EE.equipe_id = V.equipe_id
ORDER BY creation_date DESC NULLS LAST
LIMIT 20;

-- 158ms
SELECT count(*)
FROM etablissement__entreprise_vue ET
JOIN equipe__commune_vue V ON ET.commune_id = V.commune_id
LEFT JOIN equipe__etablissement_exogene EE ON EE.etablissement_id = ET.etablissement_id AND EE.equipe_id = V.equipe_id
WHERE V.equipe_id = 160
    AND categorie_juridique_3_type_id IN ('1000')
    AND V.commune_id IN ('75108')
    AND naf_type_id IN ('81.10Z')
    AND NOT economie_sociale_solidaire
    AND EE.etablissement_id IS NULL
    AND NOT employeur;

-- 6s
SELECT count(*)
FROM etablissement__entreprise_vue ET
JOIN equipe__commune_vue V ON ET.commune_id = V.commune_id
LEFT JOIN equipe__etablissement_exogene EE ON EE.etablissement_id = ET.etablissement_id AND EE.equipe_id = V.equipe_id;

```

### Avec une vue des données publiques par équipe

```sql
CREATE MATERIALIZED VIEW equipe__etablissement_vue_160 AS
SELECT
    ET.id AS etablissement_id,
    ET.commune_id,
    ET.creation_date,
    ET.effectif_type_id,
    EN.entreprise_id,
    EN.categorie_juridique_type_id as categorie_juridique_3_type_id,
    SUBSTRING(EN.categorie_juridique_type_id, 1, 2) as categorie_juridique_2_type_id,
    EN.economie_sociale_solidaire,
    EP.employeur,
    FALSE as exogene,
    EP.actif,
    EP.naf_type_id,
    A.geolocalisation,
    A.code_postal,
    A.voie_nom,
    Q.id as qpv_id,
    C.zrr_type_id,
    COALESCE(LOWER(PP.prenom_1), '') ||
    COALESCE(' ' || LOWER(PP.prenom_2), '') ||
    COALESCE(' ' || LOWER(PP.prenom_3), '') ||
    COALESCE(' ' || LOWER(PP.prenom_4), '') ||
    COALESCE(' ' || LOWER(PP.prenom_usuel), '') ||
    COALESCE(' ' || LOWER(PP.pseudonyme), '') ||
    COALESCE(' ' || LOWER(PM.sigle), '') ||
    COALESCE(' ' || LOWER(EN.nom), '') ||
    COALESCE(' ' || LOWER(EN.nom_usage), '') ||
    COALESCE(' ' || LOWER(EN.denomination), '') ||
    COALESCE(' ' || LOWER(EN.denomination_usuelle_1), '') ||
    COALESCE(' ' || LOWER(EN.denomination_usuelle_2), '') ||
    COALESCE(' ' || LOWER(EN.denomination_usuelle_3), '') ||
    COALESCE(' ' || LOWER(EP.enseigne_1), '') ||
    COALESCE(' ' || LOWER(EP.enseigne_2), '') ||
    COALESCE(' ' || LOWER(EP.enseigne_3), '') ||
    COALESCE(' ' || LOWER(EP.denomination_usuelle), '') as etablissement_noms
FROM
    equipe__commune_vue V
    JOIN etablissement ET ON ET.commune_id = V.commune_id AND V.equipe_id = 160
    JOIN entreprise_periode EN ON EN.entreprise_id = ET.entreprise_id
    JOIN etablissement_periode EP ON EP.etablissement_id = ET.id
    JOIN adresse A ON A.id = ET.adresse_1_id
    JOIN commune C ON ET.commune_id = C.id
    LEFT JOIN qpv Q ON A.geolocalisation IS NOT NULL AND ST_Contains(Q.geometrie, A.geolocalisation)
    LEFT JOIN personne_physique PP ON PP.entreprise_id = ET.entreprise_id
    LEFT JOIN personne_morale PM ON PM.entreprise_id = ET.entreprise_id
UNION
SELECT
    ET.id AS etablissement_id,
    ET.commune_id,
    ET.creation_date,
    ET.effectif_type_id,
    EN.entreprise_id,
    EN.categorie_juridique_type_id as categorie_juridique_3_type_id,
    SUBSTRING(EN.categorie_juridique_type_id, 1, 2) as categorie_juridique_2_type_id,
    EN.economie_sociale_solidaire,
    EP.employeur,
    TRUE as exogene,
    EP.actif,
    EP.naf_type_id,
    A.geolocalisation,
    A.code_postal,
    A.voie_nom,
    Q.id as qpv_id,
    C.zrr_type_id,
    COALESCE(LOWER(PP.prenom_1), '') ||
    COALESCE(' ' || LOWER(PP.prenom_2), '') ||
    COALESCE(' ' || LOWER(PP.prenom_3), '') ||
    COALESCE(' ' || LOWER(PP.prenom_4), '') ||
    COALESCE(' ' || LOWER(PP.prenom_usuel), '') ||
    COALESCE(' ' || LOWER(PP.pseudonyme), '') ||
    COALESCE(' ' || LOWER(PM.sigle), '') ||
    COALESCE(' ' || LOWER(EN.nom), '') ||
    COALESCE(' ' || LOWER(EN.nom_usage), '') ||
    COALESCE(' ' || LOWER(EN.denomination), '') ||
    COALESCE(' ' || LOWER(EN.denomination_usuelle_1), '') ||
    COALESCE(' ' || LOWER(EN.denomination_usuelle_2), '') ||
    COALESCE(' ' || LOWER(EN.denomination_usuelle_3), '') ||
    COALESCE(' ' || LOWER(EP.enseigne_1), '') ||
    COALESCE(' ' || LOWER(EP.enseigne_2), '') ||
    COALESCE(' ' || LOWER(EP.enseigne_3), '') ||
    COALESCE(' ' || LOWER(EP.denomination_usuelle), '') as etablissement_noms
FROM
    equipe__etablissement_exogene EE
    JOIN etablissement ET ON EE.etablissement_id = ET.id AND EE.equipe_id = 160
    JOIN entreprise_periode EN ON EN.entreprise_id = ET.entreprise_id
    JOIN etablissement_periode EP ON EP.etablissement_id = ET.id
    JOIN adresse A ON A.id = ET.adresse_1_id
    JOIN commune C on ET.commune_id = C.id
    LEFT JOIN qpv Q ON A.geolocalisation IS NOT NULL AND ST_Contains(Q.geometrie, A.geolocalisation)
    LEFT JOIN personne_physique PP ON PP.entreprise_id = ET.entreprise_id
    LEFT JOIN personne_morale PM ON PM.entreprise_id = ET.entreprise_id
;


CREATE UNIQUE INDEX IDX_equipe__etablissement_vue_160 ON equipe__etablissement_vue_160 (etablissement_id);
CREATE INDEX IDX_equipe__etablissement_vue_160_entreprise ON equipe__etablissement_vue_160 (entreprise_id);

CREATE INDEX IDX_equipe__etablissement_vue_160_etablissement_gin ON equipe__etablissement_vue_160 USING gin (etablissement_id gin_trgm_ops);

CREATE INDEX IDX_equipe__etablissement_vue_160__creation_date_asc ON equipe__etablissement_vue_160 (creation_date);
CREATE INDEX IDX_equipe__etablissement_vue_160__catjur3 ON equipe__etablissement_vue_160 (categorie_juridique_3_type_id);
CREATE INDEX IDX_equipe__etablissement_vue_160__catjur2 ON equipe__etablissement_vue_160 (categorie_juridique_2_type_id);
CREATE INDEX IDX_equipe__etablissement_vue_160__naf_type ON equipe__etablissement_vue_160 (naf_type_id);
CREATE INDEX IDX_equipe__etablissement_vue_160__commune ON equipe__etablissement_vue_160 (commune_id);
CREATE INDEX IDX_equipe__etablissement_vue_160__qpv ON equipe__etablissement_vue_160 (qpv_id);

CREATE INDEX IDX_equipe__etablissement_vue_160__etablissement_noms ON equipe__etablissement_vue_160 USING gin (etablissement_noms gin_trgm_ops);

ALTER MATERIALIZED VIEW equipe__etablissement_vue_160 CLUSTER ON IDX_equipe__etablissement_vue_160__commune;
CLUSTER equipe__etablissement_vue_160 USING IDX_equipe__etablissement_vue_160__commune;



REFRESH MATERIALIZED VIEW CONCURRENTLY equipe__etablissement_vue_159;
```

requêtes

```sql
-- A
-- 7.5s https://explain.dalibo.com/plan/a88dfagfcd44b13e
SELECT *
FROM equipe__etablissement_vue_160
WHERE
  categorie_juridique_3_type_id IN ('1000')
  AND commune_id IN ('75108')
  AND naf_type_id = '81.10Z'
  AND NOT economie_sociale_solidaire
  AND NOT exogene
  AND NOT employeur
ORDER BY creation_date DESC NULLS LAST
LIMIT 20;

-- B
-- 164ms https://explain.dalibo.com/plan/49g47fce2g555954
SELECT *
FROM equipe__etablissement_vue_160
WHERE
  categorie_juridique_3_type_id IN ('1000')
  AND commune_id IN ('75108')
  AND naf_type_id = '81.10Z'
  AND NOT economie_sociale_solidaire
  AND NOT exogene
  AND NOT employeur
ORDER BY creation_date DESC NULLS LAST;

-- C
-- 77ms https://explain.dalibo.com/plan/8b5b06294263gecg
SELECT *
FROM equipe__etablissement_vue_160
ORDER BY creation_date DESC NULLS LAST
LIMIT 20;

set enable_indexscan = OFF;
-- A: 164ms https://explain.dalibo.com/plan/g6d128a6cgd72d82
-- B: 177ms https://explain.dalibo.com/plan/b5a94g0g5ahg5934
-- C: 4,81ms https://explain.dalibo.com/plan/05421ge498ca87f5

```

### Avec une table des données (perso et publiques), partitionnée par équipe
