# Migrations

1. Coupe le serveur

2. Sauvegarde la base de données

3. Pousse le code de migration et les fichiers sources sur le serveur

4. Copie les fichiers `source` dans `/tmp`

   La commande `COPY` de Postgres est exécuté par un utilisateur qui n'a pas accès à tout le disque dur. On peut copier les fichiers `/sources` dans `/tmp` pour que Postgres puisse les utiliser. (cf: [neilwithdata.com](https://www.neilwithdata.com/copy-permission-denied))

   Si la base de données est dans un conteneur Docker :

   ```sh
      for file in ./_refactor/_sources/*; do docker cp $file deveco_pg:/tmp/; done
   ```

   Si l'OS utilise une couche de sécurité, elle peut bloquer la copie des fichiers par Postgres.
   Par exemple, sur Fedora on désactive temporairement [SELinux](https://docs.fedoraproject.org/en-US/quick-docs/getting-started-with-selinux/) :

   ```sh
   sudo setenforce 0
   ```

5. On lance les fichiers de migration `.sql`

   - crée les "nouvelles" tables, la nouvelle structure
   - transfère les anciennes données vers les nouvelles tables
   - copie les données depuis les `sources`
   - supprime les anciennes tables

6. On relance le serveur

---

## Entités

js : on écrit les nouvelles entités

## DB

sql : on renomme les anciennes tables et colonnes
sql : on génère le sql pour créer les tables (avec TypeOrm)
sql : on écrit la migration data en sql

## Applicatif

sql : on réécrit les queries sql (répertoire `./sql`)
js : on réécrit les queries et le code métier

## Mise à jour des données

- dans la tbale `adresse`, nettoyer les champs `voie_nom` qui contiennent des codes postaux et noms de commune

- Mises à jour sur : `etablissement.adresse_1` et `etablissement.adresse_1_etalab` cf: [#461](https://gitlab.com/incubateur-territoires/startups/deveco/deveco-app/-/issues/461)
- Transformer les `ept` en `epci` et mettre à jour les `epci_id` des communes du grand paris dans `commune`.

- identifier les périodes d'entreprise dont les `naf_type` n'ont pas de correspondance (ex : `NAFRev2,00.00Z`, `NAP,00.97`), les nettoyer et remettre les contraintes dans `entreprise_periode` et `etablissement_periode`.

  ```sql
  -- ssur entreprise_periode
  CONSTRAINT FK_entreprise_periode__naf_type_id_revision_type FOREIGN KEY (naf_type_id, naf_revision_type_id) REFERENCES naf_type(id, naf_revision_type_id) ON DELETE CASCADE ON UPDATE CASCADE

  -- ssur etablissement_periode
  CONSTRAINT FK_entreprise_periode__naf_type_revision_type FOREIGN KEY (naf_type_id, naf_revision_type_id) REFERENCES naf_type(id, naf_revision_type_id) ON DELETE CASCADE ON UPDATE CASCADE
  ```

### Commande drizzle

Générer les définitions `schema.ts` à partir de la base de données existantes

```sh
yarn drizzle-kit introspect:pg
```

Générer les migrations `.sql` à partir du schéma `schema.ts`.

```sh
yarn drizzle-kit generate:pg --schema=./src/db/schema
```

Éxecuter la migration avec psql

exemple:

```sh
psql -h 127.0.0.1 -d deveco_seed -U deveco -p 5432 -a -q -f ./drizzle/0001_pale_patriot.sql
```

 <!-- - sur la table partitionnée `equipe__etablissement`: ajouter les données portefeuille (etiquettes, demandes, rappels, favori) -->

TODO CODE :

- migrer toutes les taches cron et services associés

TODO INVESTIGATION :

- commune nouvelle 72080 pour le siret 41004387100046
