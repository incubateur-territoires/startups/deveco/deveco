-- on récupère un établissement mis à jour
-- on récupère ses QPVs
-- on récupère les QPVs qui sont sur la commune de cet établissement
-- dans cette liste de plusieurs QPVs, on vérifie le ST_Contains
-- on obtient le QPV sur lequel il est
-- ???
-- profit

select a.etablissement_id, q.id
from etablissement_adresse_1 a
join commune__qpv_2024 cq using (commune_id)
join qpv_2024 q on cq.qpv_2024_id = q.id
left join etablissement_adresse_1_etalab ae using (etablissement_id)
where
    ST_Covers(
        q.geometrie,
        CASE WHEN ae.score > 0.5
            THEN ae.geolocalisation
            ELSE a.geolocalisation
        END
    )
limit 10;
--

DROP TABLE commune__qpv_2024;
DROP TABLE etablissement__qpv_2015;
DROP TABLE etablissement__qpv_2024;

--

INSERT INTO etablissement__qpv_2024 (etablissement_id, qpv_2024_id)
SELECT a.etablissement_id, q.id as qpv_2024_id
FROM etablissement_adresse_1 a
JOIN commune__qpv_2024 cq using (commune_id)
JOIN qpv_2024 q on cq.qpv_2024_id = q.id
LEFT JOIN etablissement_adresse_1_etalab ae using (etablissement_id)
WHERE
    (
        ae.geolocalisation IS NOT NULL
        OR a.geolocalisation IS NOT NULL
    )
    AND ST_Covers(
        q.geometrie,
        CASE WHEN ae.score > 0.5
            THEN ae.geolocalisation
            ELSE a.geolocalisation
        END
    )
;

-- 89401414100010
-- 90958588700013

-- geolocalisation à partir des deux tables d'adresses

CREATE UNLOGGED TABLE IF NOT EXISTS etablissement__geolocalisation AS (
	SELECT DISTINCT ON (EA1.etablissement_id)
		EA1.etablissement_id,
		CASE WHEN EAE.etablissement_id IS NOT NULL AND EAE.score > 0.5
			THEN EAE.geolocalisation
			ELSE EA1.geolocalisation
		END
	FROM etablissement_adresse_1 EA1
	LEFT JOIN etablissement_adresse_1_etalab EAE
		ON EAE.etablissement_id = EA1.etablissement_id
  WHERE EAE.geolocalisation IS NOT NULL AND EAE.score > 0.5
			OR EA1.geolocalisation IS NOT NULL
);
CREATE INDEX IF NOT EXISTS idx_etablissement__geolocalisation__etab ON etablissement__geolocalisation(etablissement_id);
CREATE INDEX IF NOT EXISTS idx_etablissement__geolocalisation__geo ON etablissement__geolocalisation USING GiST (geolocalisation);

-- QPV 2015

CREATE UNLOGGED TABLE IF NOT EXISTS etablissement__qpv_2015 AS (
	SELECT
		etablissement_id,
		qpv.id as qpv_2015_id
	FROM etablissement__geolocalisation e
	JOIN qpv ON ST_Covers(qpv.geometrie, e.geolocalisation)
);
CREATE INDEX IF NOT EXISTS idx_etablissement__qpv_2015 ON etablissement__qpv_2015(etablissement_id);

-- QPV 2024

CREATE UNLOGGED TABLE IF NOT EXISTS etablissement__qpv_2024 AS (
	SELECT
		etablissement_id,
		qpv.id as qpv_2024_id
	FROM etablissement__geolocalisation e
	JOIN qpv_2024 qpv ON ST_Covers(qpv.geometrie, e.geolocalisation)
);
CREATE INDEX IF NOT EXISTS idx_etablissement__qpv_2024 ON etablissement__qpv_2024(etablissement_id);

-- grep -E 'QP092011|QP092013|QP092017' _full/_sources/anct_qpv_2015.csv > _fixtures/_sources/anct_qpv_2015.csv
-- grep -E 'QN09217M|QN09208M|QN09211M|QN09214M' _full/_sources/anct_qpv_2024.csv > _fixtures/_sources/anct_qpv_2024.csv

-- migration QPV


\copy source.anct__qpv_2015 from '_sources/anct_qpv_2015.csv' delimiter ',' csv header;
--> statement-breakpoint

\copy source.anct__qpv_2024 from '_sources/anct_qpv_2024.csv' delimiter ',' csv header;
--> statement-breakpoint

\copy source.anct__commune__qpv_2015 from '_sources/anct_commune__qpv_2015.csv' delimiter ',' csv header;
--> statement-breakpoint

\copy source.anct__commune__qpv_2024 from '_sources/anct_commune__qpv_2024.csv' delimiter ',' csv header;
--> statement-breakpoint


INSERT INTO qpv_2015 (id, nom, geometrie)
SELECT code_qp, nom_qp, geom
FROM source.anct__qpv_2015
ON CONFLICT (id) DO NOTHING;
--> statement-breakpoint

INSERT INTO qpv_2024 (id, nom, geometrie)
SELECT code_qp, nom_qp, geom
FROM source.anct__qpv_2015
WHERE codeQp like 'QP97%';
--> statement-breakpoint
INSERT INTO qpv_2024 (id, nom, geometrie)
SELECT code_qp, lib_qp, geom
FROM anct__commune__qpv_2024;
--> statement-breakpoint

INSERT INTO commune__qpv_2015 (commune_id, qpv_2015_id)
SELECT commune_id, qpv_2015_id
FROM source.anct__commune__qpv_2015;
--> statement-breakpoint

INSERT INTO commune__qpv_2024 (commune_id, qpv_2024_id)
SELECT commune_id, qpv_2015_id
FROM source.anct__commune__qpv_2015
WHERE qpv_2015_id like 'QP97%';
--> statement-breakpoint
INSERT INTO commune__qpv_2024 (commune_id, qpv_2024_id)
SELECT commune_id, qpv_2024_id
FROM source.anct__commune__qpv_2024;
--> statement-breakpoint

INSERT INTO etablissement__qpv_2015 (etablissement_id, qpv_2015_id)
SELECT a.etablissement_id, q.id as qpv_2015_id
FROM etablissement_adresse_1 a
JOIN commune__qpv_2015 cq using (commune_id)
JOIN qpv_2015 q on cq.qpv_2015_id = q.id
LEFT JOIN etablissement_adresse_1_etalab ae using (etablissement_id)
WHERE
    (
        ae.geolocalisation IS NOT NULL
        OR a.geolocalisation IS NOT NULL
    )
    AND ST_Covers(
        q.geometrie,
        CASE WHEN ae.score > 0.5
            THEN ae.geolocalisation
            ELSE a.geolocalisation
        END
    )
;
--> statement-breakpoint

INSERT INTO etablissement__qpv_2024 (etablissement_id, qpv_2024_id)
SELECT a.etablissement_id, q.id as qpv_2015_id
FROM etablissement_adresse_1 a
JOIN commune__qpv_2015 cq using (commune_id)
JOIN qpv_2015 q on cq.qpv_2015_id = q.id
LEFT JOIN etablissement_adresse_1_etalab ae using (etablissement_id)
WHERE
    (
        ae.geolocalisation IS NOT NULL
        OR a.geolocalisation IS NOT NULL
    )
    AND ST_Covers(
        q.geometrie,
        CASE WHEN ae.score > 0.5
            THEN ae.geolocalisation
            ELSE a.geolocalisation
        END
    )
	AND q.id like 'QP97%'
;
--> statement-breakpoint
INSERT INTO etablissement__qpv_2024 (etablissement_id, qpv_2024_id)
SELECT a.etablissement_id, q.id as qpv_2024_id
FROM etablissement_adresse_1 a
JOIN commune__qpv_2024 cq using (commune_id)
JOIN qpv_2024 q on cq.qpv_2024_id = q.id
LEFT JOIN etablissement_adresse_1_etalab ae using (etablissement_id)
WHERE
    (
        ae.geolocalisation IS NOT NULL
        OR a.geolocalisation IS NOT NULL
    )
    AND ST_Covers(
        q.geometrie,
        CASE WHEN ae.score > 0.5
            THEN ae.geolocalisation
            ELSE a.geolocalisation
        END
    )
;
--> statement-breakpoint


CREATE TABLE IF NOT EXISTS "source"."anct__qpv" (
        "gid" varchar NOT NULL,
        "code_qp" varchar(8),
        "nom_qp" varchar,
        "commune_qp" varchar,
        "geom" geometry(MultiPolygon,4326),
        CONSTRAINT "pk_anct__qpv" PRIMARY KEY("gid")
);

delete from commune__qpv where qpv_id not in (select code_qp from source.anct__qpv);

delete from qpv where id not in (select code_qp from source.anct__qpv);

-- MAJ SRID

ALTER TABLE source.anct__qpv_2024
  ALTER COLUMN geom
    TYPE geometry(MultiPolygon, 4326)
    USING ST_Transform(ST_SetSRID(geom,2154),4326);


-- CORRECTION DATES DE FERMETURE

select *
from (
    select distinct on (ep.etablissement_id) ep.etablissement_id,
      ep.debut_date,
      ep.fin_date
    from etablissement_periode ep
    where not ep.actif
      and ep.actif_change
    order by ep.etablissement_id,
      ep.debut_date desc
  ) t
where fin_date is not null
limit 10;

--

select *
from (
    select distinct on (ep.etablissement_id) ep.etablissement_id,
      ep.debut_date,
      ep.fin_date,
      ep.actif
    from etablissement_periode ep
    where ep.actif_change
    order by ep.etablissement_id,
      ep.debut_date desc
  ) t
where not actif and fin_date is not null
limit 10;


select *
from (
    select distinct on (ep.etablissement_id) ep.etablissement_id,
      ep.debut_date,
      ep.fin_date,
      ep.actif
    from etablissement_periode ep
    where ep.actif_change
    order by ep.etablissement_id,
      ep.debut_date desc
  ) t
join etablissement_periode ep2 using (etablissement_id) where not t.actif and t.fin_date is not null and ep2.fin_date is null and not ep2.actif;

-- creation de la table temporaire
CREATE TABLE temp_fix_etablissement_date_fermeture AS (
	SELECT t.etablissement_id, t.debut_date AS fermeture_date
	FROM (
    SELECT DISTINCT ON (ep.etablissement_id)
			ep.etablissement_id,
      ep.debut_date,
      ep.fin_date,
      ep.actif
    FROM etablissement_periode ep
    WHERE ep.actif_change
    ORDER BY ep.etablissement_id,
      ep.debut_date DESC
  ) t
	WHERE NOT t.actif AND t.fin_date IS NOT NULL
);

-- indexation de la table temporaire
CREATE INDEX temp_idx_fix_date_fermeture ON temp_fix_etablissement_date_fermeture (etablissement_id);

-- mise à jour d'equipe__etablissement
UPDATE equipe__etablissement_265
SET fermeture_date = t.fermeture_date
FROM temp_fix_etablissement_date_fermeture t
WHERE t.etablissement_id = equipe__etablissement_265.etablissement_id;

-- correction du SRID

update local_adresse
  set geolocalisation = ST_Transform(ST_SetSRID(geolocalisation,2154),4326);

-- maj locaux 2023

-- migration drizzle
ALTER TABLE "source"."cerema__local" ALTER COLUMN "invar" SET NOT NULL;-->statement breakpoint
ALTER TABLE "source"."cerema__local" ALTER COLUMN "idcom" SET NOT NULL;-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_morale" ALTER COLUMN "idprocpte" SET NOT NULL;-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_physique" ALTER COLUMN "idprocpte" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "source"."cerema__local" ADD COLUMN "annee" integer;-->statement breakpoint
UPDATE "source"."cerema__local" SET "annee" = 2022;-->statement breakpoint
ALTER TABLE "source"."cerema__local" ALTER COLUMN "annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_morale" ADD COLUMN "annee" integer;-->statement breakpoint
UPDATE "source"."cerema__local_proprietaire_morale" SET "annee" = 2022;-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_morale" ALTER COLUMN "annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_physique" ADD COLUMN "annee" integer;-->statement breakpoint
UPDATE "source"."cerema__local_proprietaire_physique" SET "annee" = 2022;-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_physique" ALTER COLUMN "annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "source"."cerema__local" ADD CONSTRAINT "pk_cerema__local" PRIMARY KEY("invar","idcom","annee");-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_morale" DROP CONSTRAINT pk_cerema__local_poprietaire_morale;-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_morale" ADD CONSTRAINT "pk_cerema__local_proprietaire_morale" PRIMARY KEY("idprocpte","dnulp","annee");-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_physique" DROP CONSTRAINT "pk_cerema__local_poprietaire_physique";-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_physique" ADD CONSTRAINT "pk_cerema__local_proprietaire_physique" PRIMARY KEY("idprocpte","dnulp","annee");-->statement breakpoint
-->statement breakpoint
ALTER TABLE "local" ADD COLUMN "debut_annee" varchar;-->statement breakpoint
UPDATE "local" SET "debut_annee" = 2022;-->statement breakpoint
ALTER TABLE "local" ALTER COLUMN "debut_annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "local" ADD COLUMN "annee" integer;-->statement breakpoint
UPDATE "local" SET "annee" = 2022;-->statement breakpoint
ALTER TABLE "local" ALTER COLUMN "annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "local" ADD COLUMN "actif" boolean;-->statement breakpoint
UPDATE "local" SET "actif" = false;-->statement breakpoint
ALTER TABLE "local" ALTER COLUMN "actif" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "proprietaire_personne_morale" ADD COLUMN "debut_annee" integer;-->statement breakpoint
UPDATE "proprietaire_personne_morale" SET "debut_annee" = 2022;-->statement breakpoint
ALTER TABLE "proprietaire_personne_morale" ALTER COLUMN "debut_annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "proprietaire_personne_morale" ADD COLUMN "annee" integer;-->statement breakpoint
UPDATE "proprietaire_personne_morale" SET "annee" = 2022;-->statement breakpoint
ALTER TABLE "proprietaire_personne_morale" ALTER COLUMN "annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "proprietaire_personne_physique" ADD COLUMN "debut_annee" integer;-->statement breakpoint
UPDATE "proprietaire_personne_physique" SET "debut_annee" = 2022;-->statement breakpoint
ALTER TABLE "proprietaire_personne_physique" ALTER COLUMN "debut_annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "proprietaire_personne_physique" ADD COLUMN "annee" integer;-->statement breakpoint
UPDATE "proprietaire_personne_physique" SET "annee" = 2022;-->statement breakpoint
ALTER TABLE "proprietaire_personne_physique" ALTER COLUMN "annee" SET NOT NULL;-->statement breakpoint
-->statement breakpoint
ALTER TABLE "source"."cerema__local_proprietaire_morale" ADD CONSTRAINT pk_cerema__local_poprietaire_morale PRIMARY KEY(idprocpte,dnulp,annee);
-->statement breakpoint
\copy source.cerema__local(invar, dnvoiri, dindic, dvoilib, idcom, geomloc, ccosec, dnupla, cconlc, typeact, dniv, sprincp, ssecp, ssecncp, sparkp, sparkncp, idprocpte, annee) FROM '_sources/cerema_local_fixtures_2023.csv' DELIMITER ',' CSV HEADER;
-->statement breakpoint
\copy source.cerema__local_proprietaire_morale(idprocpte, dnulp, dsiren, ddenom, annee) FROM '_sources/cerema_local_proprietaire_morale_2023.csv' DELIMITER ',' CSV HEADER;
-->statement breakpoint
\copy source.cerema__local_proprietaire_physique(idprocpte, dnulp, dnomus, dprnus, dnomlp, jdatnss, dldnss, annee) FROM '_sources/cerema_local_proprietaire_physique_2023.csv' DELIMITER ',' CSV HEADER;
-->statement breakpoint
INSERT INTO local (
	id,
	invariant,
	local_nature_type_id,
	local_categorie_type_id,
	commune_id,
	section,
	parcelle,
	niveau,
	surface_vente,
	surface_reserve,
	surface_exterieure_non_couverte,
	surface_stationnement_couvert,
	surface_stationnement_non_couvert,
	debut_annee,
	annee,
	actif
)
SELECT
	LEFT(idcom, 2) || invar,
	invar,
	cconlc,
	typeact,
	idcom,
	ccosec,
	dnupla,
	dniv,
	sprincp,
	ssecp,
	ssecncp,
	sparkp,
	sparkncp,
	annee as debut_annee,
	annee,
	true as actif
FROM source.cerema__local
WHERE annee = 2023
ON CONFLICT (id) DO UPDATE SET
	local_nature_type_id = excluded.local_nature_type_id,
	local_categorie_type_id = excluded.local_categorie_type_id,
	commune_id = excluded.commune_id,
	section = excluded.section,
	parcelle = excluded.parcelle,
	niveau = excluded.niveau,
	surface_vente = excluded.surface_vente,
	surface_reserve = excluded.surface_reserve,
	surface_exterieure_non_couverte = excluded.surface_exterieure_non_couverte,
	surface_stationnement_couvert = excluded.surface_stationnement_couvert,
	surface_stationnement_non_couvert = excluded.surface_stationnement_non_couvert,
	-- pas de mise à jour de debut_annee, car on veut garder l'ancienne
	annee = excluded.annee,
	actif = true;
-->statement breakpoint
INSERT INTO local_adresse (
	local_id,
	commune_id,
	voie_nom,
	numero,
	geolocalisation
)
SELECT
	LEFT(idcom, 2) || invar,
	idcom,
	replace(dvoilib, '  ', ' '),
	COALESCE(regexp_replace(dnvoiri, '^0*', '', 'g'), '') || COALESCE(dindic, ''),
	ST_Transform(ST_SetSRID(geomloc::geometry,2154),4326)
FROM source.cerema__local
WHERE annee = 2023
ON CONFLICT (local_id) DO UPDATE SET
	commune_id = excluded.commune_id,
	voie_nom = excluded.voie_nom,
	numero = excluded.numero,
	geolocalisation = excluded.geolocalisation;
-->statement breakpoint
INSERT INTO proprietaire_personne_physique (
	id,
	proprietaire_compte_id,
	nom,
	prenom,
	naissance_nom,
	naissance_date,
	naissance_lieu,
	debut_annee,
	annee
)
SELECT
	idprocpte || dnulp,
	idprocpte,
	dnomus,
	dprnus,
	dnomlp,
	TO_DATE(jdatnss, 'DD/MM/YYYY'),
	dldnss,
	annee as debut_annee,
	annee
FROM source.cerema__local_proprietaire_physique
WHERE annee = 2023
ON CONFLICT (id) DO UPDATE SET
	proprietaire_compte_id = excluded.proprietaire_compte_id,
	nom = excluded.nom,
	prenom = excluded.prenom,
	naissance_nom = excluded.naissance_nom,
	naissance_date = excluded.naissance_date,
	naissance_lieu = excluded.naissance_lieu,
	annee = excluded.annee;
-->statement breakpoint
INSERT INTO proprietaire_personne_morale (
	id,
	proprietaire_compte_id,
	entreprise_id,
	nom,
	debut_annee,
	annee
)
SELECT
	idprocpte || dnulp,
	idprocpte,
	dsiren,
	ddenom,
	annee as debut_annee,
	annee
FROM source.cerema__local_proprietaire_morale
WHERE annee = 2023
ON CONFLICT (id) DO UPDATE SET
	proprietaire_compte_id = excluded.proprietaire_compte_id,
	entreprise_id = excluded.entreprise_id,
	nom = excluded.nom,
	annee = excluded.annee;
-->statement breakpoint
DELETE FROM local__proprietaire_personne_physique
-- on supprime les liens des locaux qui existent en 2023
WHERE local_id IN (
	SELECT id
	FROM local
	WHERE annee = 2023
);
-->statement breakpoint
INSERT INTO local__proprietaire_personne_physique (
	local_id,
	proprietaire_personne_physique_id
)
SELECT DISTINCT
	LEFT(l.idcom, 2) || l.invar,
	idprocpte || dnulp
FROM source.cerema__local_proprietaire_physique pp
JOIN source.cerema__local l USING(idprocpte)
WHERE pp.annee = 2023 AND l.annee = 2023 ON
CONFLICT (local_id, proprietaire_personne_physique_id) DO NOTHING;
-->statement breakpoint
DELETE FROM local__proprietaire_personne_morale
-- on supprime les liens des locaux qui existent en 2023
WHERE local_id IN (
	SELECT id
	FROM local
	WHERE annee = 2023
);
-->statement breakpoint
INSERT INTO local__proprietaire_personne_morale (
	local_id,
	proprietaire_personne_morale_id
)
SELECT
	DISTINCT LEFT(l.idcom, 2) || l.invar,
	pm.idprocpte || pm.dnulp
FROM source.cerema__local_proprietaire_morale pm
JOIN source.cerema__local l USING(idprocpte)
WHERE pm.annee = 2023 AND l.annee = 2023 ON
CONFLICT (local_id, proprietaire_personne_morale_id) DO NOTHING;

-- mise à jour des colonnes creation_date / cloture_date / modification_date / modification_compte_id

-- echange
UPDATE echange
SET creation_date = action_echange.created_at
FROM action_echange
WHERE action_echange.echange_id = echange.id
AND action_echange.action_type_id = 'creation';

UPDATE echange
SET
	modification_date = evenement_action.created_at,
	modification_compte_id = evenement_action.compte_id
FROM (
	SELECT DISTINCT ON (action_echange.echange_id)
	action_echange.echange_id, evenement.compte_id, action_echange.created_at
	FROM action_echange
	JOIN evenement ON evenement.id = action_echange.evenement_id
	WHERE action_echange.action_type_id = 'modification'
	ORDER BY action_echange.echange_id, action_echange.created_at DESC
) evenement_action
WHERE echange.id = evenement_action.echange_id;

-- demande
UPDATE demande
SET creation_date = action_demande.created_at
FROM action_demande
WHERE action_demande.demande_id = demande.id
AND action_demande.action_type_id = 'creation';

UPDATE demande
SET
	modification_date = evenement_action.created_at,
	modification_compte_id = evenement_action.compte_id
FROM (
	SELECT DISTINCT ON (action_demande.demande_id)
	action_demande.demande_id, evenement.compte_id, action_demande.created_at
	FROM action_demande
	JOIN evenement ON evenement.id = action_demande.evenement_id
	WHERE action_demande.action_type_id = 'modification'
	ORDER BY action_demande.demande_id, action_demande.created_at DESC
) evenement_action
WHERE demande.id = evenement_action.demande_id;

UPDATE demande
SET
	cloture_date = evenement_action.created_at
FROM (
	SELECT DISTINCT ON (action_demande.demande_id)
	action_demande.demande_id, action_demande.created_at
	FROM action_demande
	JOIN evenement ON evenement.id = action_demande.evenement_id
	WHERE action_demande.action_type_id = 'fermeture'
	ORDER BY action_demande.demande_id, action_demande.created_at DESC
) evenement_action
WHERE demande.id = evenement_action.demande_id;

-- rappel
UPDATE rappel
SET creation_date = action_rappel.created_at
FROM action_rappel
WHERE action_rappel.rappel_id = rappel.id
AND action_rappel.action_type_id = 'creation';

UPDATE rappel
SET
	modification_date = evenement_action.created_at,
	modification_compte_id = evenement_action.compte_id
FROM (
	SELECT DISTINCT ON (action_rappel.rappel_id)
	action_rappel.rappel_id, evenement.compte_id, action_rappel.created_at
	FROM action_rappel
	JOIN evenement ON evenement.id = action_rappel.evenement_id
	WHERE action_rappel.action_type_id = 'modification'
	ORDER BY action_rappel.rappel_id, action_rappel.created_at DESC
) evenement_action
WHERE rappel.id = evenement_action.rappel_id;

UPDATE rappel
SET
	cloture_date = evenement_action.created_at
FROM (
	SELECT DISTINCT ON (action_rappel.rappel_id)
	action_rappel.rappel_id, action_rappel.created_at
	FROM action_rappel
	JOIN evenement ON evenement.id = action_rappel.evenement_id
	WHERE action_rappel.action_type_id = 'fermeture'
	ORDER BY action_rappel.rappel_id, action_rappel.created_at DESC
) evenement_action
WHERE rappel.id = evenement_action.rappel_id;

-- etablissement_creation
UPDATE etablissement_creation
SET creation_date = action_etablissement_creation.created_at
FROM action_etablissement_creation
WHERE action_etablissement_creation.etablissement_creation_id = etablissement_creation.id
AND action_etablissement_creation.action_type_id = 'creation';

UPDATE etablissement_creation
SET
	modification_date = evenement_action.created_at,
	modification_compte_id = evenement_action.compte_id
FROM (
	SELECT DISTINCT ON (action_etablissement_creation.etablissement_creation_id)
	action_etablissement_creation.etablissement_creation_id, evenement.compte_id, action_etablissement_creation.created_at
	FROM action_etablissement_creation
	JOIN evenement ON evenement.id = action_etablissement_creation.evenement_id
	WHERE action_etablissement_creation.action_type_id = 'modification'
	ORDER BY action_etablissement_creation.etablissement_creation_id, action_etablissement_creation.created_at DESC
) evenement_action
WHERE etablissement_creation.id = evenement_action.etablissement_creation_id;

-- equipe_etablissement
UPDATE equipe__etablissement_contribution
SET creation_date = action_equipe_etablissement.created_at
FROM action_equipe_etablissement
WHERE action_equipe_etablissement.etablissement_id = equipe__etablissement_contribution.etablissement_id
AND action_equipe_etablissement.action_type_id = 'creation';

UPDATE equipe__etablissement_contribution
SET
	modification_date = evenement_action.created_at,
	modification_compte_id = evenement_action.compte_id
FROM (
	SELECT DISTINCT ON (action_equipe_etablissement.etablissement_id)
	action_equipe_etablissement.etablissement_id, evenement.compte_id, action_equipe_etablissement.created_at
	FROM action_equipe_etablissement
	JOIN evenement ON evenement.id = action_equipe_etablissement.evenement_id
	WHERE action_equipe_etablissement.action_type_id = 'modification'
	ORDER BY action_equipe_etablissement.etablissement_id, action_equipe_etablissement.created_at DESC
) evenement_action
WHERE equipe__etablissement_contribution.etablissement_id = evenement_action.etablissement_id;

-- equipe_local
UPDATE equipe__local_contribution
SET creation_date = action_equipe_local.created_at
FROM action_equipe_local
WHERE action_equipe_local.local_id = equipe__local_contribution.local_id
AND action_equipe_local.action_type_id = 'creation';

UPDATE equipe__local_contribution
SET
	modification_date = evenement_action.created_at,
	modification_compte_id = evenement_action.compte_id
FROM (
	SELECT DISTINCT ON (action_equipe_local.local_id)
	action_equipe_local.local_id, evenement.compte_id, action_equipe_local.created_at
	FROM action_equipe_local
	JOIN evenement ON evenement.id = action_equipe_local.evenement_id
	WHERE action_equipe_local.action_type_id = 'modification'
	ORDER BY action_equipe_local.local_id, action_equipe_local.created_at DESC
) evenement_action
WHERE equipe__local_contribution.local_id = evenement_action.local_id;


SELECT
	t.etablissement_id,
	t.debut_date AS fermeture_date
FROM (
	SELECT DISTINCT ON (insee_sirene_api__etablissement_periode.siret)
		insee_sirene_api__etablissement_periode.siret AS etablissement_id,
		insee_sirene_api__etablissement_periode.date_debut AS debut_date,
		insee_sirene_api__etablissement_periode.changement_etat_administratif_etablissement AS statut
	FROM
		source.insee_sirene_api__etablissement_periode
	WHERE
		insee_sirene_api__etablissement_periode.changement_etat_administratif_etablissement
	ORDER BY
		insee_sirene_api__etablissement_periode.siret,
		insee_sirene_api__etablissement_periode.date_debut DESC
) t
WHERE NOT t.statut;

-- mise à jour des locaux 2023

do $$
    declare
        table_record record;
        counter integer := 1;

    BEGIN
        FOR table_record IN (
            SELECT id as equipe_id, nom
            FROM equipe
			WHERE id >= 1000
			ORDER BY id
        )
        LOOP

            RAISE NOTICE '% EQUIPE % EQUIPE % (%)', timeofday(), counter, table_record.equipe_id, table_record.nom;

            EXECUTE format(
                'INSERT INTO equipe__local(equipe_id, local_id)
                    SELECT $1, local.id
                    FROM local
                    JOIN equipe__commune_vue USING (commune_id)
                    WHERE equipe_id = $1
                ON CONFLICT (equipe_id, local_id) DO NOTHING;'
            ) USING
                table_record.equipe_id;


        COMMIT;

        counter := counter + 1;
    END LOOP;

END;
$$;

--

INSERT INTO source.rcd__etablissement_effectif_emm_13_mois_annuel (siret, date, effectif)
SELECT
	siret,
	date,
	floor(100 * s / c) / 100 AS effectif
FROM (
	SELECT
		siret,
		count(*) as c,
		sum(effectif) as s,
		max(date) AS date
	FROM source.rcd__etablissement_effectif_emm_13_mois
	GROUP BY siret
) AS calculs;

INSERT INTO etablissement_creation_projet_type (id, nom) VALUES
('creation', 'Création'),
('reprise', 'Reprise'),
('transmission', 'Transmission'),
('autre', 'Autre');


INSERT INTO etablissement_creation_contact_origine (id, nom) VALUES
('evenement_collectivite', 'Suite à un événement de ma collectivité'),
('reseau_accompagnement', 'Par un réseau d''accompagnement et de financement (BGE, France Active,…)'),
('club_entreprises', 'Par un club/réseau d''entreprises'),
('france_travail', 'Par France Travail'),
('structure_insertion', 'Par une structure d''aide à l''insertion et l''emploi'),
('bouche_oreille', 'Par bouche à oreille'),
('reseaux_sociaux', 'Par les réseaux sociaux'),
('internet', 'Par internet'),
('presse', 'Par la presse'),
('autre_collectivite', 'Par une autre CT'),
('elus', 'Par des élus'),
('autre', 'Autre');

INSERT INTO etablissement_creation_secteur_activite (id, nom) VALUES
('agriculture', 'Agriculture, sylviculture et pêche'),
('industries_extractives', 'Industries extractives'),
('industries_manufacturieres', 'Industries manufacturières'),
('production_electricite', 'Production et distribution d’électricité, de gaz, de vapeur et d’air conditionné'),
('production_eau_assainissement', 'Production et distribution d’eau assainissement, gestion des déchets et dépollution'),
('construction', 'Construction'),
('commerce_reparation_automobiles', 'Commerce ; réparation d’automobiles et de motocycles'),
('transports_entreposage', 'Transports et entreposage'),
('hebergement_restauration', 'Hébergement et restauration'),
('information_communication', 'Information et communication'),
('activites_financieres_assurance', 'Activités financières et d’assurance'),
('activites_immobilieres', 'Activités immobilières'),
('activites_specialisees_scientifiques_techniques', 'Activités spécialisées, scientifiques et techniques'),
('services_administratifs_soutien', 'Activités de services administratifs et de soutien'),
('enseignement', 'Enseignement'),
('sante_action_sociale', 'Santé humaine et action sociale'),
('arts_spectacles_recreatifs', 'Arts, spectacles et activités récréatives'),
('autres_services', 'Autres activités de services');


INSERT INTO etablissement_creation_source_financement (id, nom) VALUES
('pret_bancaire_prive', 'Prêt bancaire (banque privée)'),
('pret_bancaire_bpi_france', 'Prêt bancaire (Bpi France)'),
('pret_honneur', 'Prêt sur l''honneur'),
('pret_garanti_etat', 'Prêt Garanti par l''Etat'),
('autofinancement', 'Autofinancement'),
('subvention', 'Subvention'),
('pret_ct', 'Prêt proposé par la CT');


INSERT INTO etablissement_creation__createur_diplome_niveau_type (id, nom) VALUES
('aucun_diplome', 'Aucun diplôme'),
('certificat_etudes_primaires', 'Certificat d’études primaires (CEP)'),
('bepc', 'BEPC, brevet élémentaire, brevet des collèges, DNB'),
('cap_bep', 'CAP, BEP, brevet de compagnon, ou diplôme de niveau équivalent'),
('bac_general_technologique', 'Baccalauréat général ou technologique, brevet supérieur, capacité en droit, DAEU, ESEU'),
('bac_professionnel', 'Baccalauréat professionnel, brevet professionnel, de technicien, de maîtrise ou d’enseignement, diplôme équivalent'),
('diplome_technique_1er_cycle', 'Diplôme technique de 1er cycle : BTS, DUT, Deug, Deust, diplôme de la santé ou du social de niveau bac+2, diplôme équivalent'),
('diplome_universitaire_1er_cycle', 'Diplôme universitaire ou général de 1er cycle : licence, licence professionnelle, maîtrise, diplôme équivalent de niveau bac+3 ou bac+4'),
('diplome_universitaire_3eme_cycle', 'Diplôme universitaire de 3ème cycle : Master, DEA, DESS, diplôme de grande école, diplôme de niveau bac+5, doctorat de santé'),
('doctorat_recherche', 'Doctorat de recherche (hors santé)');


INSERT INTO etablissement_creation__createur_profession_situation_type (id, nom) VALUES
('independant', 'Indépendant ou à votre compte (y.c conjoint collaborateur, aide familial)'),
('chef_entreprise', 'Chef d’entreprise salarié, P.D.G (y.c gérant minoritaire de SARL)'),
('agent_fonction_publique', 'Agent de la fonction publique (État, hospitalière, territoriale, y.c en CDD)*'),
('salarie_prive', 'Salarié du secteur privé (y.c apprenti, stagiaire rémunéré, CDD ou Intérimaire)'),
('demandeur_emploi_moins_1_an', 'Demandeur d’emploi depuis moins d’un an'),
('demandeur_emploi_plus_1_an', 'Demandeur d’emploi depuis un an ou plus'),
('etudiant_scolaire', 'Étudiant ou scolaire'),
('sans_activite_pro', 'Sans activité professionnelle (y.c personne au foyer, bénéficiaire du RSA, congé parental, congé pour création d’entreprise, convenance personnelle)'),
('retraite', 'Retraité');

\COPY etablissement_creation_projet_type (id, nom) FROM 'etablissement_creation_projet_type.csv' WITH CSV HEADER;
\COPY etablissement_creation_secteur_activite (id, nom) FROM 'etablissement_creation_secteur_activite.csv' WITH CSV HEADER;
\COPY etablissement_creation_source_financement (id, nom) FROM 'etablissement_creation_source_financement.csv' WITH CSV HEADER;
\COPY etablissement_creation__createur_diplome_niveau_type (id, nom) FROM 'etablissement_creation__createur_diplome_niveau_type.csv' WITH CSV HEADER;
\COPY etablissement_creation__createur_profession_situation_type (id, nom) FROM 'etablissement_creation__createur_profession_situation_type.csv' WITH CSV HEADER;

-- recopie de commune dans la table de jointure entre etablissement_creation et commune

INSERT INTO etablissement_creation__commune(equipe_id, etablissement_creation_id, commune_id)
SELECT equipe_id, etablissement_creation_id, commune_id
FROM etablissement_creation;

-- on vide la table de jointure entre etablissement_creation et commune

UPDATE etablissement_creation
SET commune_id = NULL;



-- amélioration de la géolocalisation sur tous les sirets
deveco=> create table truc_truc_truc as (select siret, COALESCE(st_setsrid(st_makepoint(x_longitude, y_latitude), 4326), geolocalisation) as geolocalisation from source.insee_sirene_api__etablissement left join source.insee_sirene__etablissement_geoloc using (siret) left join etablissement_adresse_1_etalab on siret = etablissement_id where x_longitude is not null or etablissement_id is not null);
SELECT 39427335
Time: 107823.124 ms (01:47.823)
deveco=> select * from truc_truc_truc limit 1;
     siret      |                  geolocalisation
----------------+----------------------------------------------------
 51373491300017 | 0101000020E6100000113AAF438A220240B0A84E2880724840
(1 row)

Time: 14.130 ms
deveco=> create index idx_truc_truc_truc_geolocalisation on truc_truc_truc using gist(geolocalisation);

CREATE INDEX
Time: 72917.584 ms (01:12.918)
deveco=>
deveco=> select count(*) from truc_truc_truc join zonage on zonage.id = 1335 and ST_Contains(geometrie, geolocalisation);
 count
--------
 278474
(1 row)

Time: 5112.527 ms (00:05.113)

-- migration des adresses banifiées

INSERT INTO source.etalab__etablissement_geoloc (
       siret,
       longitude,
       latitude,
       geo_score,
       geo_adresse,
       numero,
       voie_nom,
       code_postal,
       geolocalisation
)
SELECT
       etablissement_id,
       latitude,
       longitude
       score,
       COALESCE(numero || ' ', '') || COALESCE(voie_nom || ' ', '') || COALESCE(code_postal || ' ', '') commune.nom
       numero,
       voie_nom,
       code_postal,
       geolocalisation
FROM etablissement_adresse_1_etalab etalab
JOIN commune ON commune.id = etalab.commune_id
ON CONFLICT DO UPDATE
SET
       longitude = excluded.longitude,
       latitude = excluded.latitude,
       geo_score = excluded.geo_score,
       numero = excluded.numero,
       voie_nom = excluded.voie_nom,
       geolocalisation = excluded.geolocalisation;

-- copie de la date de création depuis la table equipe_import avant de la supprimer

UPDATE equipe
SET created_at = equipe_import.created_at
FROM equipe_import
WHERE equipe.id = equipe_import.equipe_id;

-- récupération des établissements suivi++ pour l'équipe du CA de Chaumont

create or replace view suivi_157 as
(select etablissement_id
from equipe__etablissement__contact
where equipe_id = 157
union
select etablissement_id
from equipe__etablissement__echange
where equipe_id = 157
union
select etablissement_id
from equipe__etablissement__rappel
where equipe_id = 157
union
select etablissement_id
FROM equipe__etablissement__demande
where equipe_id = 157
union
select etablissement_id
FROM equipe__etablissement__etiquette
where equipe_id = 157
	and not auto
union
select etablissement_id
FROM etablissement_creation_transformation
where equipe_id = 157
union
select etablissement_id
FROM equipe__local__occupant
where equipe_id = 157
union
select etablissement_id
FROM equipe__etablissement_ressource
where equipe_id = 157
union
select etablissement_id
from equipe__etablissement_contribution
where equipe_id = 157);

select siret,
	o is not null as occupant,
	t is not null as transfo,
	r is not null as ressource
from equipe__etablissement_157 eep
	join source.insee_sirene_api__etablissement e on e.siret = eep.etablissement_id
	left join equipe__commune_vue ecv on ecv.eqcomvue_equipe_id = 157
	and ecv.eqcomvue_commune_id = e.code_commune_etablissement
	left join etablissement_creation_transformation t on t.equipe_id = 157
	AND t.etablissement_id = eep.etablissement_id
	left join equipe__local__occupant o on o.equipe_id = 157
	AND o.etablissement_id = eep.etablissement_id
	left join equipe__etablissement_ressource r on r.equipe_id = 157
	AND r.etablissement_id = eep.etablissement_id
where ecv is null
	and (
		o is not null
		OR t is not null
		OR r is not null
	)
	and eep.etablissement_id not in (
		select etablissement_id
		from suivi_157
	);

-- ajout des nouveaux types de demande

INSERT INTO demande_type (id, nom)
VALUES
	('urbanisme', 'démarches d''urbanisme'),
	('urgence', 'procédures d''urgence'),
	('commercialisation', 'commercialisation location'),
	('transaction_fonds', 'transaction - fonds'),
	('transaction_murs', 'transaction - murs');

-- ajout des nouveaux types d'équipe et de territoire

INSERT INTO territoire_type (id, nom)
VALUES
	('france', 'france');

INSERT INTO equipe_type (id, nom)
VALUES
	('admin_central', 'administration centrale');

-- BAN

-- suppression des adresses BAN pour les établissements non-diffusibles sans géolocalisation
-- (sûrement dûr à l'import du fichier etalab)

DELETE FROM source.insee_sirene_api__etablissement e
USING source.etalab__etablissement_geoloc g
WHERE e.siret = g.siret
	AND e.statut_diffusion_etablissement = 'P'
	AND g.geolocalisation IS NULL;

-- suppression des adresses BAN des établissements ayant déjà une géolocalisation INSEE

CREATE TABLE temp_etalab_with_sirene_geoloc AS (
	SELECT siret
	FROM source.etalab__etablissement_geoloc
	JOIN source.insee_sirene__etablissement_geoloc USING (siret)
);

CREATE TABLE temp_sirene_process (LIKE temp_etalab_with_sirene_geoloc INCLUDING ALL);
CREATE TABLE temp_sirene_done (LIKE temp_etalab_with_sirene_geoloc INCLUDING ALL);

do $$
	declare
		counter integer := 1;

	BEGIN
		WHILE (SELECT 1 FROM temp_etalab_with_sirene_geoloc LIMIT 1) loop
			RAISE NOTICE '% DEBUT 100K / %', timeofday(), counter;

			INSERT INTO temp_sirene_process
			SELECT * FROM temp_etalab_with_sirene_geoloc
			LIMIT 100000;

			RAISE NOTICE '% DELETE 100K / %', timeofday(), counter;

			DELETE FROM source.etalab__etablissement_geoloc
			USING temp_sirene_process
			WHERE temp_sirene_process.siret = source.etalab__etablissement_geoloc.siret;

			RAISE NOTICE '% DONE 100K / %', timeofday(), counter;

			INSERT INTO temp_sirene_done
			SELECT * FROM temp_sirene_process;

			RAISE NOTICE '% DELETE 100K / %', timeofday(), counter;

			DELETE FROM temp_etalab_with_sirene_geoloc
			USING temp_sirene_process
			WHERE temp_sirene_process.siret = temp_etalab_with_sirene_geoloc.siret;

			RAISE NOTICE '% TRUNCATE 100K / %', timeofday(), counter;

			TRUNCATE temp_sirene_process;

			COMMIT;

			counter := counter + 1;
		end loop;
	END;
$$;

UPDATE local_adresse
SET geolocalisation =
	ST_Transform(
		ST_SetSRID(
			ST_GeomFromEWKB(geomloc::geometry),
			CASE
				WHEN LEFT(idcom, 3) in ('971', '972') THEN 5490
				WHEN LEFT(idcom, 3) = '973' THEN 2972
				WHEN LEFT(idcom, 3) = '974' THEN 2975
				WHEN LEFT(idcom, 3) = '976' THEN 4471
				ELSE 2154
			END
		), 4326
	)
FROM source.cerema__local
WHERE LEFT(idcom, 2) || invar = local_id
AND idcom LIKE '97%';

-- ajout de types d'équipes

INSERT INTO equipe_type (id, nom)
VALUES
	('test', 'équipe de test'),
	('ddt', 'direction départementale des territoires'),
	('ddets', 'direction départementale de l''emploi, du travail et des solidarités'),
	('dreal', 'direction régionale de l''environnement, de l''aménagement et du logement'),
	('epa', 'établissement public administratif'),
	('epf', 'établissement public foncier'),
	('dgfip', 'direction générale des finances publiques'),
	('prefecture_region', 'préfecture de région'),
	('AUTRE', 'autre')
ON CONFLICT (id) DO NOTHING
;

-- mise en dernier du type 'autre'

UPDATE equipe SET equipe_type_id = 'AUTRE' WHERE equipe_type_id = 'autre';
DELETE FROM equipe_type WHERE id = 'autre';
UPDATE equipe_type SET id = 'autre' WHERE id = 'AUTRE';

-- remplissage des colonnes de connexion et actions authentifiées des comptes

UPDATE compte
SET
	connexion_at = compte_actions.connexion_at
FROM (
	SELECT
		 compte_id,
		 max(created_at) as connexion_at
	FROM evenement
	WHERE evenement_type_id IN ('connexion', 'connexion_lien')
	GROUP BY compte_id
) compte_actions
WHERE compte.id = compte_actions.compte_id;

UPDATE compte
SET
	auth_at = compte_actions.auth_at
FROM (
	SELECT
		 compte_id,
		 max(created_at) as auth_at
	FROM evenement
	WHERE evenement_type_id = 'authentification'
	GROUP BY compte_id
) compte_actions
WHERE compte.id = compte_actions.compte_id;

--- remplissage de la table source des exercices pour garder les sirens

INSERT INTO "source"."api_entreprise__entreprise_exercice"
SELECT DISTINCT
	LEFT(siret, 9) AS siren,
	'{}'::jsonb AS payload
FROM "source"."api_entreprise__etablissement_exercice";

--- remplissage des colonnes de mise à jour pour les tables API Entreprise

UPDATE source.api_entreprise__entreprise_exercice
SET mise_a_jour_at = actions.created_at
FROM (
	SELECT DISTINCT ON (entreprise_id)
		entreprise_id,
		created_at
	FROM action_entreprise
	WHERE diff = '{"message": "mise à jour des exercices"}'
	ORDER BY entreprise_id, created_at DESC
) actions
WHERE source.api_entreprise__entreprise_exercice.siren = actions.entreprise_id;

UPDATE source.api_entreprise__entreprise_beneficiaire_effectif
SET mise_a_jour_at = actions.created_at
FROM (
	SELECT DISTINCT ON (entreprise_id)
		entreprise_id,
		created_at
	FROM action_entreprise
	WHERE diff = '{"message": "mise à jour des bénéficiaires effectifs"}'
	ORDER BY entreprise_id, created_at DESC
) actions
WHERE source.api_entreprise__entreprise_beneficiaire_effectif.siren = actions.entreprise_id;

UPDATE source.api_entreprise__entreprise_mandataire_social
SET mise_a_jour_at = actions.created_at
FROM (
	SELECT DISTINCT ON (entreprise_id)
		entreprise_id,
		created_at
	FROM action_entreprise
	WHERE diff = '{"message": "mise à jour des mandataires sociaux"}'
	ORDER BY entreprise_id, created_at DESC
) actions
WHERE source.api_entreprise__entreprise_mandataire_social.siren = actions.entreprise_id;

UPDATE source.api_entreprise__entreprise_liasse_fiscale
SET mise_a_jour_at = actions.created_at
FROM (
	SELECT DISTINCT ON (entreprise_id)
		entreprise_id,
		created_at
	FROM action_entreprise
	WHERE diff = '{"message": "mise à jour de la liasse fiscale"}'
	ORDER BY entreprise_id, created_at DESC
) actions
WHERE source.api_entreprise__entreprise_liasse_fiscale.siren = actions.entreprise_id;

--

UPDATE compte
SET
	desactivation_at = COALESCE(compte_actions.desactivation_at, NOW())
FROM (
	SELECT
		 compte_id,
		 max(created_at) as desactivation_at
	FROM action_compte
	WHERE action_type_id = 'desactivation'
	GROUP BY compte_id
) compte_actions
WHERE compte.id = compte_actions.compte_id;

-- remplissage des participations

DROP TABLE IF EXISTS temp_entreprise_participation;
INSERT INTO source.api_entreprise__entreprise_participation (source, cible)
WITH extracted AS (
	SELECT DISTINCT
		siren AS source,
		REPLACE(valeurs::text, ' ', '') AS valeur,
		declarations_value->>'numero_imprime' AS numero_imprime,
		donnees_value->>'code_absolu' AS code_absolu
	FROM source.api_entreprise__entreprise_liasse_fiscale,
		jsonb_array_elements(payload->'declarations') AS declarations_value,
		jsonb_array_elements(declarations_value->'donnees') AS donnees_value,
		jsonb_array_elements_text(donnees_value->'valeurs') AS valeurs
	WHERE valeurs IS NOT NULL
		AND declarations_value->>'numero_imprime' IN ('2059G', '2059F')
		AND donnees_value->>'code_absolu' IN ('2005836', '2005724')
		AND LENGTH(REPLACE(valeurs::text, ' ', '')) = 9
		AND REPLACE(valeurs::text, ' ', '') ~ '^[0-9]{9}$'
		AND REPLACE(valeurs::text, ' ', '') <> '000000000'
		AND REPLACE(valeurs::text, ' ', '') <> siren
),
relations AS (
	SELECT
		CASE
			WHEN numero_imprime = '2059G' AND code_absolu = '2005836' THEN source
			WHEN numero_imprime = '2059F' AND code_absolu = '2005724' THEN valeur
		END AS source,
		CASE
			WHEN numero_imprime = '2059G' AND code_absolu = '2005836' THEN valeur
			WHEN numero_imprime = '2059F' AND code_absolu = '2005724' THEN source
		END AS cible
	FROM extracted
	WHERE
		(numero_imprime = '2059G' AND code_absolu = '2005836')
		OR
		(numero_imprime = '2059F' AND code_absolu = '2005724')
)
SELECT DISTINCT ON (source, cible)
	source,
	cible
FROM relations
ORDER BY source, cible DESC
ON CONFLICT (source, cible)
DO NOTHING;

-- suppression des sirens inconnus ?

DELETE FROM source.api_entreprise__entreprise_participation
WHERE source IN (
	SELECT source
	FROM source.api_entreprise__entreprise_participation
	LEFT JOIN source.insee_sirene_api__unite_legale u ON siren = source
	WHERE u IS NULL
);
DELETE FROM source.api_entreprise__entreprise_participation
WHERE cible IN (
	SELECT cible
	FROM source.api_entreprise__entreprise_participation
	LEFT JOIN source.insee_sirene_api__unite_legale u ON siren = cible
	WHERE u IS NULL
);

-- VUE etablissement_geolocalisation_vue

DROP VIEW "public"."etablissement_geolocalisation_vue" CASCADE;
CREATE VIEW "public"."etablissement_geolocalisation_vue" AS (
	SELECT
		"source"."insee_sirene_api__etablissement"."siret" AS siret,
		COALESCE(
			"equipe__etablissement_contribution"."geolocalisation",
			ST_SetSRID(ST_MakePoint("source"."insee_sirene__etablissement_geoloc"."x_longitude", "source"."insee_sirene__etablissement_geoloc"."y_latitude"), 4326),
			ST_Transform (
				ST_SetSRID (
					ST_MakePoint(
						"source"."insee_sirene_api__etablissement"."coordonnee_lambert_abscisse_etablissement"::float,
						"source"."insee_sirene_api__etablissement"."coordonnee_lambert_ordonnee_etablissement"::float
					),
					CASE
						WHEN LEFT ("source"."insee_sirene_api__etablissement"."code_commune_etablissement", 3) in ('971', '972') THEN 5490
						WHEN LEFT ("source"."insee_sirene_api__etablissement"."code_commune_etablissement", 3) = '973' THEN 2972
						WHEN LEFT ("source"."insee_sirene_api__etablissement"."code_commune_etablissement", 3) = '974' THEN 2975
						WHEN LEFT ("source"."insee_sirene_api__etablissement"."code_commune_etablissement", 3) = '976' THEN 4471
						ELSE 2154
					END
				),
				4326
			),
			"source"."etalab__etablissement_adresse"."geolocalisation"
		) AS geolocalisation,
		"source"."insee_sirene_api__etablissement"."mise_a_jour_at" AS mise_a_jour_at
	FROM "source"."insee_sirene_api__etablissement"
	LEFT JOIN "source"."insee_sirene__etablissement_geoloc" ON "source"."insee_sirene__etablissement_geoloc"."siret" = "source"."insee_sirene_api__etablissement"."siret"
	LEFT JOIN "source"."etalab__etablissement_adresse" ON "source"."etalab__etablissement_adresse"."siret" = "source"."insee_sirene_api__etablissement"."siret"
	LEFT JOIN "equipe__etablissement_contribution" ON "equipe__etablissement_contribution"."etablissement_id" = "source"."insee_sirene_api__etablissement"."siret"
	WHERE "source"."insee_sirene__etablissement_geoloc"."siret" IS NOT NULL OR "source"."etalab__etablissement_adresse"."siret" IS NOT NULL
);

CREATE VIEW public.etablissement__qpv_2024_vue AS (
  SELECT
    siret as etablissement_id,
    qpv_2024.id as qpv_2024_id
  FROM etablissement_geolocalisation_vue
  JOIN qpv_2024 ON ST_Covers(geometrie, geolocalisation));

CREATE VIEW public.etablissement__qpv_2015_vue AS (
  SELECT
    siret as etablissement_id,
    qpv_2015.id as qpv_2015_id
  FROM etablissement_geolocalisation_vue
  JOIN qpv_2015 ON ST_Covers(geometrie, geolocalisation));
