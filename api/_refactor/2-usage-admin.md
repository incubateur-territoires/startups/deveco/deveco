# Usage admin

## Ajout d'une équipe

1. un utilisateur `admin` crée un `nom`, choisit un type (`commune`, `epci`, `departement`, etc), coche si l'équipe gère un `groupement`, puis recherche et sélectionne le(s) `nom_administratif`. Enregistre.

2. étapes :

- crée une `equipe`
- crée les jointures `equipe_territoire`
- crée les jointures `equipe_commune`, calculées à partir de `equipe_territoire`
- crée la materialized view `equipe_etablissement`
- crée les `stats` d'analyse territoriale

## Ajout d'utilisateurs `deveco` dans une équipe
