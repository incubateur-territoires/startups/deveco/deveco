psql -U pg -d pg -h localhost -p 5435 -c "\copy source_datagouv_commune_ept(
    insee_com,
    insee_reg,
    insee_dep,
    lib_com,
    lib_ept,
    insee_ept,
    siren
)
FROM 'api/_refactor/_sources/datagouv_commune_ept.csv'
DELIMITER ','
CSV HEADER;"

psql -U pg -d pg -h localhost -p 5435 -c "\copy source_insee_region(
    reg,
    cheflieu,
    tncc,
    ncc,
    nccenr,
		libelle
)
FROM 'api/_refactor/_sources/insee_region_2021.csv'
DELIMITER ','
CSV HEADER;"

psql -U pg -d pg -h localhost -p 5435 -c "\copy source_insee_departement(
    dep,
    reg,
    cheflieu,
    tncc,
    ncc,
    nccenr,
    libelle
)
FROM 'api/_refactor/_sources/insee_departement_2021.csv'
DELIMITER ','
CSV HEADER;"

psql -U pg -d pg -h localhost -p 5435 -c "\copy source_insee_epci_commune(
    codgeo,
    libgeo,
    epci,
    libepci,
    dep,
    reg
)
FROM 'api/_refactor/_sources/insee_epci_commune_2023.csv'
DELIMITER ','
CSV HEADER;"

psql -U pg -d pg -h localhost -p 5435 -c "\copy source_odt_epci_petr(
    codgeo,
    libgeo,
    pays,
    pays_libgeo
)
FROM 'api/_refactor/_sources/odt_epci_petr_2021.csv'
DELIMITER ','
CSV HEADER;"

psql -U pg -d pg -h localhost -p 5435 -c "\copy source_datagouv_zrr(insee_com, zrr)
FROM 'api/_refactor/_sources/anct_commune-zrr_2021.csv'
DELIMITER ','
CSV HEADER;"

psql -U pg -d pg -h localhost -p 5435 -c "\copy source_insee_epci(
    epci,
    libepci,
    nature_epci,
    nb_com
)
FROM 'api/_refactor/_sources/insee_epci_2023.csv'
DELIMITER ','
CSV HEADER;"

psql -U pg -d pg -h localhost -p 5435 -c "\copy source_insee_commune(
    typecom,
    com,
    reg,
    dep,
    ctcd,
    arr,
    tncc,
    ncc,
    nccenr,
    libelle,
    can,
    comparent
)
FROM 'api/_refactor/_sources/insee_commune_2023.csv'
DELIMITER ','
CSV HEADER;"

psql -U pg -d pg -h localhost -p 5435 -c "\copy source_insee_commune_historique(
    com,
    tncc,
    ncc,
    nccenr,
    libelle,
    date_debut,
    date_fin
)
FROM 'api/_refactor/_sources/insee_commune_historique_2023.csv'
DELIMITER ','
CSV HEADER;"

psql -U pg -d pg -h localhost -p 5435 -c "\copy source_insee_commune_mouvement(
    mod,
    date_eff,
    typecom_av,
    com_av,
    tncc_av,
    ncc_av,
    nccenr_av,
    libelle_av,
    typecom_ap,
    com_ap,
    tncc_ap,
    ncc_ap,
    nccenr_ap,
    libelle_ap,
    indic_oe,
    indic_oe2
)
FROM 'api/_refactor/_sources/insee_commune_mouvement_2023.csv'
DELIMITER ','
CSV HEADER;"

psql -U pg -d pg -h localhost -p 5435 -c "\copy source_laposte_code_insee_code_postal(
    code_insee, nom, code_postal, libelle_acheminement, ligne_5
)
FROM 'api/_refactor/_sources/laposte_code_postal_2023.csv'
DELIMITER ';'
CSV HEADER;"

