psql -U pg -d pg -h localhost -p 5435 -c "\copy categorie_juridique_type(id, nom, niveau)
FROM 'api/_refactor/_sources/insee_categorie_juridique_1.csv'
DELIMITER ','
CSV HEADER;"

psql -U pg -d pg -h localhost -p 5435 -c "\copy categorie_juridique_type(id, nom, niveau, parent_categorie_juridique_type_id)
FROM 'api/_refactor/_sources/insee_categorie_juridique_2.csv'
DELIMITER ','
CSV HEADER;"

psql -U pg -d pg -h localhost -p 5435 -c "\copy categorie_juridique_type(id, nom, niveau, parent_categorie_juridique_type_id)
FROM 'api/_refactor/_sources/insee_categorie_juridique_3.csv'
DELIMITER ','
CSV HEADER;"

-- catégories qui existent dans la base SIRENE mais qui n'existent plus dans les sources
psql -U pg -d pg -h localhost -p 5435 -c "\copy categorie_juridique_type(id, nom, niveau, parent_categorie_juridique_type_id)
FROM 'api/_refactor/_sources/insee_categorie_juridique_supplementaire.csv'
DELIMITER ','
CSV HEADER;"


