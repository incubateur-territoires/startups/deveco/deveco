-- install extensions

CREATE EXTENSION IF NOT EXISTS pg_trgm;
CREATE EXTENSION IF NOT EXISTS unaccent;

CREATE SCHEMA old;

ALTER TABLE territory_etablissement_reference_zonages_zonage RENAME TO old_territory_etablissement_reference_zonages_zonage;
ALTER TABLE fiche_archive RENAME TO old_fiche_archive;
ALTER TABLE local_archive RENAME TO old_local_archive;
ALTER TABLE stats RENAME TO old_stats;
ALTER TABLE event_log RENAME TO old_event_log;
ALTER TABLE etablissement_favori RENAME TO old_etablissement_favori;
ALTER TABLE migration_table RENAME TO old_migration_table;
ALTER TABLE typeorm_metadata RENAME TO old_typeorm_metadata;
ALTER TABLE account RENAME TO old_account;
ALTER TABLE qp_metropoleoutremer_wgs84_epsg4326 RENAME TO old_qp_metropoleoutremer_wgs84_epsg4326;
ALTER TABLE naf RENAME TO old_naf;
ALTER TABLE region RENAME TO old_region;
ALTER TABLE departement RENAME TO old_departement;
ALTER TABLE epci_commune RENAME TO old_epci_commune;
ALTER TABLE petr RENAME TO old_petr;
ALTER TABLE metropole RENAME TO old_metropole;
ALTER TABLE epci RENAME TO old_epci;
ALTER TABLE commune RENAME TO old_commune;
ALTER TABLE entreprise RENAME TO old_entreprise;
ALTER TABLE etablissement RENAME TO old_etablissement;
ALTER TABLE territory RENAME TO old_territory;
ALTER TABLE deveco RENAME TO old_deveco;
ALTER SEQUENCE deveco_id_seq RENAME TO old_deveco_id_seq;
ALTER TABLE zonage RENAME TO old_zonage;
ALTER SEQUENCE zonage_id_seq RENAME TO old_zonage_id_seq;
ALTER TABLE perimetre RENAME TO old_perimetre;
ALTER TABLE perimetre_communes_commune RENAME TO old_perimetre_communes_commune;
ALTER TABLE perimetre_departements_departement RENAME TO old_perimetre_departements_departement;
ALTER TABLE perimetre_epcis_epci RENAME TO old_perimetre_epcis_epci;
ALTER TABLE mot_cle RENAME TO old_mot_cle;
ALTER TABLE localisation_entreprise RENAME TO old_localisation_entreprise;
ALTER TABLE activite_entreprise RENAME TO old_activite_entreprise;
ALTER TABLE fiche RENAME TO old_fiche;
ALTER TABLE echange RENAME TO old_echange;
ALTER SEQUENCE echange_id_seq RENAME TO old_echange_id_seq;
ALTER TABLE demande RENAME TO old_demande;
ALTER SEQUENCE demande_id_seq RENAME TO old_demande_id_seq;
ALTER TABLE rappel RENAME TO old_rappel;
ALTER SEQUENCE rappel_id_seq RENAME TO old_rappel_id_seq;
ALTER TABLE contact RENAME TO old_contact;
ALTER SEQUENCE contact_id_seq RENAME TO old_contact_id_seq;
ALTER TABLE particulier RENAME TO old_particulier;
ALTER TABLE exercice RENAME TO old_exercice;
ALTER TABLE proprietaire RENAME TO old_proprietaire;
ALTER TABLE territory_etablissement_reference RENAME TO old_territory_etablissement_reference;
ALTER TABLE entite RENAME TO old_entite;
ALTER TABLE local RENAME TO old_local;
ALTER SEQUENCE local_id_seq RENAME TO old_local_id_seq;
ALTER TABLE local_proprietaires_entite RENAME TO old_local_proprietaires_entite;
ALTER TABLE old_account SET SCHEMA old;
ALTER TABLE old_activite_entreprise SET SCHEMA old;
ALTER TABLE old_commune SET SCHEMA old;
ALTER TABLE old_contact SET SCHEMA old;
ALTER TABLE old_demande SET SCHEMA old;
ALTER TABLE old_departement SET SCHEMA old;
ALTER TABLE old_deveco SET SCHEMA old;
ALTER TABLE old_echange SET SCHEMA old;
ALTER TABLE old_entite SET SCHEMA old;
ALTER TABLE old_entreprise SET SCHEMA old;
ALTER TABLE old_epci SET SCHEMA old;
ALTER TABLE old_epci_commune SET SCHEMA old;
ALTER TABLE old_etablissement SET SCHEMA old;
ALTER TABLE old_etablissement_favori SET SCHEMA old;
ALTER TABLE old_event_log SET SCHEMA old;
ALTER TABLE old_exercice SET SCHEMA old;
ALTER TABLE old_fiche SET SCHEMA old;
ALTER TABLE old_fiche_archive SET SCHEMA old;
ALTER TABLE old_local SET SCHEMA old;
ALTER TABLE old_local_archive SET SCHEMA old;
ALTER TABLE old_local_proprietaires_entite SET SCHEMA old;
ALTER TABLE old_localisation_entreprise SET SCHEMA old;
ALTER TABLE old_metropole SET SCHEMA old;
ALTER TABLE old_migration_table SET SCHEMA old;
ALTER TABLE old_mot_cle SET SCHEMA old;
ALTER TABLE old_naf SET SCHEMA old;
ALTER TABLE old_particulier SET SCHEMA old;
ALTER TABLE old_perimetre SET SCHEMA old;
ALTER TABLE old_perimetre_communes_commune SET SCHEMA old;
ALTER TABLE old_perimetre_departements_departement SET SCHEMA old;
ALTER TABLE old_perimetre_epcis_epci SET SCHEMA old;
ALTER TABLE old_petr SET SCHEMA old;
ALTER TABLE old_proprietaire SET SCHEMA old;
ALTER TABLE old_qp_metropoleoutremer_wgs84_epsg4326 SET SCHEMA old;
ALTER TABLE old_rappel SET SCHEMA old;
ALTER TABLE old_region SET SCHEMA old;
ALTER TABLE old_stats SET SCHEMA old;
ALTER TABLE old_territory SET SCHEMA old;
ALTER TABLE old_territory_etablissement_reference SET SCHEMA old;
ALTER TABLE old_territory_etablissement_reference_zonages_zonage SET SCHEMA old;
ALTER TABLE old_typeorm_metadata SET SCHEMA old;
ALTER TABLE old_zonage SET SCHEMA old;

DROP TABLE login;
DROP TABLE consultation;

ALTER TABLE search_request RENAME TO recherche;
ALTER SEQUENCE search_request_id_seq RENAME TO recherche_id_seq;
ALTER TABLE recherche RENAME COLUMN type TO entite;
ALTER TABLE recherche RENAME COLUMN query TO requete;
ALTER TABLE recherche ADD COLUMN format varchar;
ALTER TABLE recherche DROP CONSTRAINT IF EXISTS "FK_1c22acefb8ac347d31d987e0539";
ALTER TABLE geo_token DROP CONSTRAINT IF EXISTS "FK_e686de08d0bc02c0f9f68997ca4";

-- territory_import_status TO equipe_import
ALTER TABLE territory_import_status RENAME TO equipe_import;
ALTER SEQUENCE territory_import_status_id_seq RENAME TO equipe_import_id_seq;
ALTER TABLE equipe_import RENAME COLUMN territoire_id TO equipe_id;
ALTER TABLE equipe_import DROP CONSTRAINT IF EXISTS uniq_territory;
ALTER TABLE equipe_import DROP CONSTRAINT IF EXISTS "FK_3660db63e1b58b8412a7071e0d0";
ALTER TABLE equipe_import DROP CONSTRAINT IF EXISTS "PK_0a5e37ff6b09e5a72ddf87efb26";
ALTER TABLE equipe_import ADD CONSTRAINT PK_equipe_import PRIMARY KEY (id);

-- sources données publiques

ALTER TABLE sirene_etablissement RENAME TO source_api_sirene_etablissement;
ALTER TABLE source_api_sirene_etablissement ADD COLUMN mise_a_jour_at timestamptz;
ALTER TABLE source_api_sirene_etablissement ALTER COLUMN etat_administratif_etablissement DROP NOT NULL;

ALTER TYPE sirene_etablissement_caractere_employeur_etablissement_enum RENAME TO source_sirene_caractere_employeur_enum;
ALTER TYPE sirene_etablissement_etat_administratif_etablissement_enum RENAME TO source_sirene_etablissement_etat_administratif_enum;
ALTER TYPE sirene_etablissement_statut_diffusion_etablissement_enum RENAME TO source_sirene_etablissement_statut_diffusion_enum;

ALTER TABLE sirene_unite_legale RENAME TO source_api_sirene_unite_legale;
ALTER TABLE source_api_sirene_unite_legale ADD COLUMN mise_a_jour_at timestamptz;
ALTER TABLE source_api_sirene_unite_legale RENAME COLUMN eco_soc_sol_unite_legale TO economie_sociale_solidaire_unite_legale;

ALTER TYPE sirene_unite_legale_categorie_entreprise_enum RENAME TO source_sirene_unite_legale_categorie_entreprise_enum;
ALTER TYPE sirene_unite_legale_eco_soc_sol_unite_legale_enum RENAME TO source_sirene_unite_legale_economie_sociale_solidaire_enum;
ALTER TYPE sirene_unite_legale_etat_administratif_unite_legale_enum RENAME TO source_sirene_unite_legale_etat_administratif_enum;
ALTER TYPE sirene_unite_legale_sexe_unite_legale_enum RENAME TO source_sirene_unite_legale_sexe_enum;
ALTER TYPE sirene_unite_legale_societe_mission_unite_legale_enum RENAME TO source_sirene_unite_legale_societe_mission_enum;
ALTER TYPE sirene_unite_legale_statut_diffusion_unite_legale_enum RENAME TO source_sirene_unite_legale_statut_diffusion_enum;

ALTER TABLE geoloc_entreprise RENAME TO source_geoloc_entreprise;

ALTER TABLE lien_succession RENAME TO source_sirene_lien_succession;
ALTER TABLE source_sirene_lien_succession ALTER COLUMN siret_predecesseur SET NOT NULL;
ALTER TABLE source_sirene_lien_succession ALTER COLUMN siret_successeur SET NOT NULL;

ALTER TABLE lien_succession_temp RENAME TO source_sirene_lien_succession_temp;
ALTER TABLE source_sirene_lien_succession_temp ALTER COLUMN siret_predecesseur SET NOT NULL;
ALTER TABLE source_sirene_lien_succession_temp ALTER COLUMN siret_successeur SET NOT NULL;

-- compte

CREATE TABLE IF NOT EXISTS compte (
    id SERIAL NOT NULL,
    nom varchar,
    prenom varchar,
    email varchar NOT NULL,
    compte_type_id varchar NOT NULL,
    cle varchar,
    actif boolean,

    PRIMARY KEY (id)
);

INSERT INTO compte (
    id,
    nom,
    prenom,
    email,
    compte_type_id,
    cle,
    actif
)
SELECT
    id,
    nom,
    prenom,
    email,
    account_type,
    access_key,
    enabled
FROM old.old_account;

ALTER TABLE password RENAME COLUMN account_id TO compte_id;
ALTER TABLE password RENAME COLUMN password TO hash;
ALTER TABLE password DROP CONSTRAINT IF EXISTS "FK_ad6708d47d7045166fab9c7ea34";
ALTER TABLE password DROP CONSTRAINT IF EXISTS "REL_ad6708d47d7045166fab9c7ea3";
ALTER TABLE password ADD CONSTRAINT fk_password__compte FOREIGN KEY (compte_id) REFERENCES compte(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE password ADD CONSTRAINT uk_password__compte UNIQUE (compte_id);
ALTER TABLE password RENAME TO mot_de_passe;
ALTER SEQUENCE password_id_seq RENAME TO mot_de_passe_id_seq;

--

CREATE TABLE IF NOT EXISTS tache (
    id SERIAL NOT NULL,
    nom VARCHAR NOT NULL,
    debut_at timestamptz DEFAULT NOW() NOT NULL,
    en_cours_at timestamptz,
    fin_at timestamptz,
    manuel boolean DEFAULT FALSE,
    rapport jsonb,

    PRIMARY KEY (id)
);

--

UPDATE recherche
SET
    entite = substring(entite, 8),
    format = 'xlsx'
WHERE entite ilike 'export-%';
DELETE FROM old.old_event_log
WHERE entity_type = 'LOCAL'
AND entity_id NOT IN (
    SELECT id
    FROM old.old_local
);

DROP TYPE etablissement_light_etat_administratif_etablissement_enum;

DROP TABLE IF EXISTS tmp_petr;

-- qpv

CREATE TABLE IF NOT EXISTS source_datagouv_qpv (
    gid serial,
    code_qp varchar(8),
    nom_qp varchar(254),
    commune_qp varchar(254),
    geom geometry(MultiPolygon,4326),

    PRIMARY KEY (gid)
);

INSERT INTO source_datagouv_qpv (
    gid,
    code_qp,
    nom_qp,
    commune_qp,
    geom
)
SELECT
    gid,
    code_qp,
    nom_qp,
    commune_qp,
    geom
FROM old.old_qp_metropoleoutremer_wgs84_epsg4326;

-- naf

CREATE TABLE IF NOT EXISTS naf_revision_type (
    id varchar(10) NOT NULL,
    nom varchar(30) NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO naf_revision_type (id, nom)
VALUES
    ('NAP', 'NAP 1973-1993'),
    ('NAF1993', 'NAF 1993'),
    ('NAFRev1', 'NAF Révision 1 (2003)'),
    ('NAFRev2', 'NAF Révision 2 (2008)');

CREATE TABLE IF NOT EXISTS naf_type (
    naf_revision_type_id varchar(10) NOT NULL,
    id varchar(10) NOT NULL,
    nom varchar NOT NULL,
    niveau int NOT NULL,
    parent_naf_type_id varchar(10),

    PRIMARY KEY (id, naf_revision_type_id)
);

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_nap_niveau_1.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_nap_niveau_2.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_nap_niveau_3.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_nap_niveau_4.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_naf_1993_niveau_1.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_naf_1993_niveau_2.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_naf_1993_niveau_3.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_naf_1993_niveau_4.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_naf_1993_niveau_5.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_naf_rev1_niveau_1.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_naf_rev1_niveau_2.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_naf_rev1_niveau_3.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_naf_rev1_niveau_4.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_naf_rev1_niveau_5.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_naf_rev2_niveau_1.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_naf_rev2_niveau_2.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_naf_rev2_niveau_3.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_naf_rev2_niveau_4.csv'
DELIMITER ','
CSV HEADER;

COPY naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM '/tmp/insee_naf_rev2_niveau_5.csv'
DELIMITER ','
CSV HEADER;

-- etablissement / entreprise

CREATE TABLE IF NOT EXISTS categorie_juridique_type (
    id varchar(4) NOT NULL,
    nom varchar NOT NULL,
    parent_categorie_juridique_type_id varchar(4),
    niveau integer NOT NULL,

    PRIMARY KEY (id)
);

COPY categorie_juridique_type(id, nom, niveau)
FROM '/tmp/insee_categorie_juridique_1.csv'
DELIMITER ','
CSV HEADER;

COPY categorie_juridique_type(id, nom, niveau, parent_categorie_juridique_type_id)
FROM '/tmp/insee_categorie_juridique_2.csv'
DELIMITER ','
CSV HEADER;

COPY categorie_juridique_type(id, nom, niveau, parent_categorie_juridique_type_id)
FROM '/tmp/insee_categorie_juridique_3.csv'
DELIMITER ','
CSV HEADER;

-- catégories qui existent dans la base SIRENE mais qui n'existent plus dans les sources
COPY categorie_juridique_type(id, nom, niveau, parent_categorie_juridique_type_id)
FROM '/tmp/insee_categorie_juridique_supplementaire.csv'
DELIMITER ','
CSV HEADER;

INSERT INTO categorie_juridique_type(id, nom, niveau, parent_categorie_juridique_type_id)
VALUES ('0001', 'indéfini', 3, '00');

-- entreprise_periode

-- personne morale / physique

-- 'F', 'M', 'N'
CREATE TABLE IF NOT EXISTS sexe_type (
    id varchar(1) NOT NULL,
    nom varchar(10) NOT NULL,

    PRIMARY KEY (id)
);
INSERT INTO sexe_type (id, nom)
VALUES
    ('F', 'feminin'),
    ('M', 'masculin'),
    ('N', 'N/D');

-- entreprise type : physique / morale (P, M)
CREATE TABLE IF NOT EXISTS entreprise_type (
    id varchar(1) NOT NULL,
    nom varchar(10) NOT NULL,

    PRIMARY KEY (id)
);
INSERT INTO entreprise_type (id, nom)
VALUES
    ('P', 'physique'),
    ('M', 'morale');

-- entreprise effectif : 'O'
CREATE TABLE IF NOT EXISTS effectif_type (
    id varchar(2) NOT NULL,
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);
INSERT INTO effectif_type (id, nom)
VALUES
  ('NN', 'Unité non employeuse (pas de salarié au cours de l’année de référence et pas d’effectif au 31/12)'),
  ('00', '0 salarié (n’ayant pas d effectif au 31/12 mais ayant employé des salariés au cours de l’année de référence)'),
  ('01', '1 ou 2 salariés'),
  ('02', '3 à 5 salariés'),
  ('03', '6 à 9 salariés'),
  ('11', '10 à 19 salariés'),
  ('12', '20 à 49 salariés'),
  ('21', '50 à 99 salariés'),
  ('22', '100 à 199 salariés'),
  ('31', '200 à 249 salariés'),
  ('32', '250 à 499 salariés'),
  ('41', '500 à 999 salariés'),
  ('42', '1 000 à 1 999 salariés'),
  ('51', '2 000 à 4 999 salariés'),
  ('52', '5 000 à 9 999 salariés'),
  ('53', '10 000 salariés et plus');

-- entreprise catégorie : PME, ETI, GE
CREATE TABLE IF NOT EXISTS categorie_entreprise_type (
    id varchar(3) NOT NULL,
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO categorie_entreprise_type (id, nom)
VALUES
    ('PME', 'petite ou moyenne entreprise'),
    ('ETI', 'entreprise de taille intermédiaire'),
    ('GE', 'grande entreprise');
    -- ept

CREATE TABLE IF NOT EXISTS source_datagouv_commune_ept (
    insee_com varchar(5) NOT NULL,
    insee_reg varchar(2) NOT NULL,
    insee_dep varchar(2) NOT NULL,
    lib_com varchar NOT NULL,
    lib_ept varchar NOT NULL,
    insee_ept varchar(3) NOT NULL,
    siren varchar(9) NOT NULL,

    PRIMARY KEY (insee_com)
);

COPY source_datagouv_commune_ept(
    insee_com,
    insee_reg,
    insee_dep,
    lib_com,
    lib_ept,
    insee_ept,
    siren
)
FROM '/tmp/datagouv_commune_ept.csv'
DELIMITER ','
CSV HEADER;

-- region

CREATE TABLE IF NOT EXISTS source_insee_region (
    reg varchar(2) NOT NULL,
    cheflieu varchar(5) NOT NULL,
    tncc varchar(1) NOT NULL,
    ncc varchar NOT NULL,
    nccenr varchar NOT NULL,
    libelle varchar NOT NULL,

    PRIMARY KEY(reg)
);

COPY source_insee_region(
    reg,
    cheflieu,
    tncc,
    ncc,
    nccenr,
    libelle
)
FROM '/tmp/insee_region_2021.csv'
DELIMITER ','
CSV HEADER;

CREATE TABLE IF NOT EXISTS region (
    id varchar(2) NOT NULL,
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO region (id, nom)
SELECT reg, nccenr
FROM source_insee_region;

-- departement

CREATE TABLE IF NOT EXISTS source_insee_departement (
    dep varchar(3) NOT NULL,
    reg varchar(2) NOT NULL,
    cheflieu varchar(5) NOT NULL,
    tncc varchar(1) NOT NULL,
    ncc varchar NOT NULL,
    nccenr varchar NOT NULL,
    libelle varchar NOT NULL,

    PRIMARY KEY(dep)
);

COPY source_insee_departement(
    dep,
    reg,
    cheflieu,
    tncc,
    ncc,
    nccenr,
    libelle
)
FROM '/tmp/insee_departement_2021.csv'
DELIMITER ','
CSV HEADER;

CREATE TABLE IF NOT EXISTS departement (
    id varchar(3) NOT NULL,
    nom varchar NOT NULL,
    region_id varchar(2) NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO departement (id, nom, region_id)
SELECT dep, nccenr, reg
FROM source_insee_departement;

-- epci_commune
CREATE TABLE IF NOT EXISTS source_insee_epci_commune (
    codgeo varchar(5) NOT NULL,
    libgeo varchar NOT NULL,
    epci varchar(9) NOT NULL,
    libepci varchar NOT NULL,
    dep varchar(3) NOT NULL,
    reg varchar(2) NOT NULL,

    PRIMARY KEY (codgeo)
);

COPY source_insee_epci_commune(
    codgeo,
    libgeo,
    epci,
    libepci,
    dep,
    reg
)
FROM '/tmp/insee_epci_commune_2023.csv'
DELIMITER ','
CSV HEADER;

-- petr

CREATE TABLE IF NOT EXISTS source_odt_epci_petr (
    codgeo varchar(9) NOT NULL,
    libgeo varchar NOT NULL,
    pays varchar(4),
    pays_libgeo varchar,

    PRIMARY KEY (codgeo)
);

COPY source_odt_epci_petr(
    codgeo,
    libgeo,
    pays,
    pays_libgeo
)
FROM '/tmp/odt_epci_petr_2021.csv'
DELIMITER ','
CSV HEADER;

CREATE TABLE IF NOT EXISTS petr (
    id varchar(4) NOT NULL,
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO petr (id, nom)
SELECT DISTINCT pays, pays_libgeo
FROM source_odt_epci_petr
WHERE pays IS NOT NULL;

-- metropole
CREATE TABLE IF NOT EXISTS metropole (
    id varchar(5),
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO metropole (id, nom)
VALUES
    ('75056', 'Paris'),
    ('69123', 'Lyon'),
    ('13055', 'Marseille');

-- zrr_type

CREATE TABLE IF NOT EXISTS zrr_type (
    id varchar(1),
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO zrr_type (id, nom)
VALUES
    ('C', 'complet'),
    ('P', 'partiel'),
    ('N', 'non');

-- CREATION zrr

CREATE TABLE IF NOT EXISTS source_datagouv_zrr (
    insee_com varchar(5) NOT NULL,
    zrr varchar(1) NOT NULL,

    PRIMARY KEY (insee_com, zrr)
);

COPY source_datagouv_zrr(insee_com, zrr)
FROM '/tmp/anct_commune-zrr_2021.csv'
DELIMITER ','
CSV HEADER;

-- corrige le fait que la commune des Trois Lacs a le code INSEE 27058 dans le fichier source ANCT 2022
-- alors qu'il a changé en 2023 pour devenir 27676
UPDATE source_datagouv_zrr
SET insee_com = '27676'
WHERE insee_com = '27058';

-- epci

CREATE TABLE IF NOT EXISTS source_insee_epci (
    epci varchar(9) NOT NULL,
    libepci varchar NOT NULL,
    nature_epci varchar NOT NULL,
    nb_com integer NOT NULL,

    PRIMARY KEY (epci)
);

COPY source_insee_epci(
    epci,
    libepci,
    nature_epci,
    nb_com
)
FROM '/tmp/insee_epci_2023.csv'
DELIMITER ','
CSV HEADER;

CREATE TABLE IF NOT EXISTS epci_type (
    id varchar(2) NOT NULL,
    nom varchar(50) NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO epci_type (id, nom)
VALUES
    ('ZZ', 'sans objet'),
    ('CA', 'communauté d''agglomération'),
    ('CC', 'communauté de communes'),
    ('CU', 'communauté urbaine'),
    ('ME', 'métropole'),
    ('ET', 'établissement public territorial'),
    ('ML', 'métropole de Lyon');

CREATE TABLE IF NOT EXISTS epci (
    id varchar(9) NOT NULL,
    epci_type_id varchar(2) NOT NULL,
    nom varchar NOT NULL,
    petr_id varchar(4),

    PRIMARY KEY (id)
);

INSERT INTO epci (
    id,
    epci_type_id,
    nom,
    petr_id
)
SELECT
    E.epci,
    CASE
        WHEN E.nature_epci = 'METLYON' THEN 'ML'
        WHEN E.nature_epci = 'EPT' THEN 'ET'
        ELSE E.nature_epci
    END,
    E.libepci,
    P.pays
FROM source_insee_epci E
LEFT JOIN source_odt_epci_petr P ON P.codgeo = E.epci
LEFT JOIN source_datagouv_commune_ept T ON E.epci = T.siren
WHERE T.siren IS NULL
UNION
SELECT DISTINCT
    siren,
    'ET',
    lib_ept,
    NULL
FROM source_datagouv_commune_ept;

--

CREATE TABLE IF NOT EXISTS commune_type (
    id varchar NOT NULL,
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO commune_type (id, nom)
VALUES
    ('COMP', 'commune périmée'),
    ('COMA', 'commune associée'),
    ('COMD', 'commune déléguée'),
    ('COM', 'commune'),
    ('ARM', 'arrondissement municipal');

-- équipe

CREATE TABLE IF NOT EXISTS territoire_type (
    id varchar(20),
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO territoire_type (id, nom)
VALUES
    ('commune', 'commune'),
    ('epci', 'EPCI'),
    ('metropole', 'métropole'),
    ('petr', 'PETR'),
    ('departement', 'département'),
    ('region', 'région');

-- equipe

CREATE TABLE IF NOT EXISTS equipe_type (
    id varchar(16),
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO equipe_type (id, nom)
VALUES
    ('commune', 'commune'),
    ('CC', 'communauté de communes'),
    ('CA', 'communauté d''agglomération'),
    ('CU', 'communauté urbaine'),
    ('ME', 'métropole'),
    ('EPT', 'établissement public territorial'),
    ('plm', 'Paris-Lyon-Marseille'),
    ('petr', 'pôle d''équilibre territorial et rural'),
    ('departement', 'conseil départemental'),
    ('prefecture', 'préfecture'),
    ('region', 'conseil régional'),
    ('dreets', 'DREETS'),
    ('deveco_locale', 'agence de développement économique locale'),
    ('deveco_regionale', 'agence de développement économique régionale'),
    ('autre', 'autre');

-- etiquette

CREATE TABLE IF NOT EXISTS etiquette_type (
    id varchar(20) NOT NULL,
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO etiquette_type (id, nom)
VALUES
    ('mot_cle', 'mot-clé'),
    ('activite', 'activité réelle'),
    ('localisation', 'localisation');

-- echange

CREATE TABLE IF NOT EXISTS echange_type (
    id varchar(10) NOT NULL,
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO echange_type (id, nom)
VALUES
    ('telephone', 'téléphone'),
    ('email', 'email'),
    ('rencontre', 'rencontre'),
    ('courrier', 'courrier');

-- demande

CREATE TABLE IF NOT EXISTS demande_type (
    id varchar(20) NOT NULL,
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO demande_type (id, nom)
VALUES
    ('foncier', 'foncier'),
    ('immobilier', 'immobilier et locaux d''activité'),
    ('economique', 'aide économique'),
    ('fiscalite', 'fiscalité'),
    ('accompagnement', 'accompagnement'),
    ('rh', 'RH'),
    ('autre', 'autre');

-- local

CREATE TABLE IF NOT EXISTS local_statut_type (
    id varchar,
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO local_statut_type (id, nom)
VALUES
    ('occupe', 'occupé'),
    ('vacant', 'vacant'),
    ('sans-objet', 'sans-objet');

CREATE TABLE IF NOT EXISTS local_type (
    id varchar,
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO local_type (id, nom)
VALUES
    ('commerce', 'commerce'),
    ('bureaux', 'bureau'),
    ('ateliersartisanaux', 'atelier artisanal'),
    ('batimentsindustriels', 'bâtiment industriel'),
    ('entrepot', 'entrepôt'),
    ('terrain', 'terrain');

-- evenement

CREATE TABLE IF NOT EXISTS evenement_type (
    id varchar,
    nom varchar,

    PRIMARY KEY(id)
);

INSERT INTO evenement_type (id, nom)
VALUES
  ('creation', 'création'),
  ('modification', 'modification'),
  ('connexion', 'connexion'),
  ('authentification', 'authentification'),
  ('fermeture', 'fermeture'),
  ('reouverture', 'ré-ouverture'),
  ('suppression', 'suppression'),
  ('transformation', 'transformation'),
  ('activation', 'activation')
  ('desactivation', 'désactivation')
  ('affichage', 'affichage');
  -- COM,01001,84,01,01D,012,5,ABERGEMENT CLEMENCIAT,Abergement-Clémenciat,L'Abergement-Clémenciat,0108,

CREATE TABLE IF NOT EXISTS source_insee_commune (
    typecom varchar(4),
    com varchar(5),
    reg varchar(2),
    dep varchar(3),
    ctcd varchar(4),
    arr varchar(4),
    tncc varchar(1),
    ncc varchar,
    nccenr varchar,
    libelle varchar,
    can varchar(5),
    comparent varchar(5)
);

COPY source_insee_commune(
    typecom,
    com,
    reg,
    dep,
    ctcd,
    arr,
    tncc,
    ncc,
    nccenr,
    libelle,
    can,
    comparent
)
FROM '/tmp/insee_commune_2023.csv'
DELIMITER ','
CSV HEADER;

CREATE TABLE IF NOT EXISTS source_insee_commune_historique (
    com varchar(5),
    tncc varchar(1),
    ncc varchar,
    nccenr varchar,
    libelle varchar,
    date_debut date,
    date_fin date
);

COPY source_insee_commune_historique(
    com,
    tncc,
    ncc,
    nccenr,
    libelle,
    date_debut,
    date_fin
)
FROM '/tmp/insee_commune_historique_2023.csv'
DELIMITER ','
CSV HEADER;

CREATE TABLE IF NOT EXISTS source_insee_commune_mouvement (
    mod varchar,
    date_eff date,
    typecom_av varchar(4),
    com_av varchar(5),
    tncc_av varchar(1),
    ncc_av varchar,
    nccenr_av varchar,
    libelle_av varchar,
    typecom_ap varchar(4),
    com_ap varchar(5),
    tncc_ap varchar(1),
    ncc_ap varchar,
    nccenr_ap varchar,
    libelle_ap varchar,
    indic_oe varchar,
    indic_oe2 varchar
);

COPY source_insee_commune_mouvement(
    mod,
    date_eff,
    typecom_av,
    com_av,
    tncc_av,
    ncc_av,
    nccenr_av,
    libelle_av,
    typecom_ap,
    com_ap,
    tncc_ap,
    ncc_ap,
    nccenr_ap,
    libelle_ap,
    indic_oe,
    indic_oe2
)
FROM '/tmp/insee_commune_mouvement_2023.csv'
DELIMITER ','
CSV HEADER;

CREATE TABLE IF NOT EXISTS source_laposte_code_insee_code_postal (
    code_insee varchar(5),
    nom varchar,
    code_postal varchar(5),
    libelle_acheminement varchar,
    ligne_5 varchar
);

COPY source_laposte_code_insee_code_postal(
    code_insee, nom, code_postal, libelle_acheminement, ligne_5
)
FROM '/tmp/laposte_code_postal_2023.csv'
DELIMITER ';'
CSV HEADER;

CREATE TABLE IF NOT EXISTS commune (
    id varchar(5) NOT NULL,
    nom varchar(100) NOT NULL,

    commune_type_id varchar(4) NOT NULL,
    commune_parent_id varchar(5),

    metropole_id varchar(5),
    epci_id varchar(9) NOT NULL,
    petr_id varchar(4),
    departement_id varchar(3) NOT NULL,
    region_id varchar(2) NOT NULL,

    zrr_type_id varchar(1) DEFAULT 'N'::varchar(1) NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO commune (
    id,
    nom,
    commune_type_id,
    commune_parent_id,
    metropole_id,
    epci_id,
    petr_id,
    departement_id,
    region_id,
    zrr_type_id
)
SELECT
    DISTINCT ON (C.com)
    C.com,
    C.libelle,
    C.typecom,
    C.comparent,
    CASE
        WHEN C.comparent IN ('75056', '69123', '13055')
        THEN C.comparent ELSE null
    END,
    CASE
        WHEN CE.siren IS NOT NULL THEN CE.siren
        WHEN EC.epci IS NOT NULL THEN EC.epci
        ELSE EP.epci
    END,
    E.pays,
    CASE WHEN C.dep IS NULL THEN CP.dep ELSE C.dep END,
    CASE WHEN C.reg IS NULL THEN CP.reg ELSE C.reg END,
    COALESCE(Z.zrr, 'N')
FROM source_insee_commune AS C
LEFT JOIN source_insee_commune AS CP ON CP.com = C.comparent AND CP.typecom = 'COM'
LEFT JOIN source_insee_epci_commune AS EC ON EC.codgeo = C.com
LEFT JOIN source_insee_epci_commune AS EP ON EP.codgeo = C.comparent
LEFT JOIN source_datagouv_commune_ept AS CE ON (CE.insee_com = C.com OR CE.insee_com = C.comparent)
LEFT JOIN source_odt_epci_petr AS E ON E.codgeo = EC.epci
LEFT JOIN source_datagouv_zrr AS Z ON (Z.insee_com = C.com OR Z.insee_com = C.comparent)
WHERE C.comparent IS NULL OR C.com <> C.comparent;

-- qpv

CREATE TABLE IF NOT EXISTS qpv (
    id varchar(8),
    nom varchar,
    geometrie geometry(MultiPolygon, 4326),

    PRIMARY KEY (id)
);

INSERT INTO qpv(id, nom, geometrie)
SELECT code_qp, nom_qp, geom
FROM source_datagouv_qpv;

-- SELECT AddGeometryColumn('','source_datagouv_qpv','geom','0','MULTIPOLYGON',2);

-- adresse (personne / etablissement / etablissement_creation)

CREATE TABLE IF NOT EXISTS adresse (
    id SERIAL NOT NULL,
    numero varchar,
    voie_nom varchar,
    commune_id varchar(5) NOT NULL,
    geolocalisation geometry(Point, 4326),
    -- champs supplémentaires provenant de la base SIRENE
    code_postal varchar(5),
    distribution_speciale varchar,
    cedex_code varchar(9),
    cedex_nom varchar,

    PRIMARY KEY (id)
);

-- ALTER TABLE adresse
-- ALTER COLUMN geolocalisation
-- TYPE geometry(Point) USING geolocalisation::geometry(Point);

-- entreprise

CREATE TABLE IF NOT EXISTS entreprise (
    id varchar(9) NOT NULL,

    creation_date date,

    effectif_annee int,
    effectif_type_id varchar(2),

    entreprise_type_id varchar(1) NOT NULL,
    diffusible boolean NOT NULL DEFAULT true,

    categorie_entreprise_type_id varchar(3),
    categorie_entreprise_annee int,

    unite_purgee boolean DEFAULT FALSE,

    nom varchar,

    PRIMARY KEY (id)
);

INSERT INTO entreprise (
    id,

    creation_date,

    entreprise_type_id,
    diffusible,

    effectif_annee,
    effectif_type_id,

    categorie_entreprise_type_id,
    categorie_entreprise_annee,

    unite_purgee,

    nom
)
SELECT
    siren,
    U.date_creation_unite_legale,

    CASE
        WHEN U.categorie_juridique_unite_legale = '1000'
        THEN 'P'
        ELSE 'M'
    END,
    CASE
        WHEN U.statut_diffusion_unite_legale = 'O'
        THEN true
        ELSE false
    END,

    U.annee_effectifs_unite_legale,
    U.tranche_effectifs_unite_legale,

    U.categorie_entreprise,
    U.annee_categorie_entreprise,

    U.unite_purgee_unite_legale,

    CONCAT_WS(
        ' ',
        U.pseudonyme_unite_legale,
        COALESCE(
            U.prenom_usuel_unite_legale,
            -- on ne prend que le premier prénom
            -- ils sont très peu remplis, et ce sont souvent les mêmes
            U.prenom1_unite_legale
        ),
        U.sigle_unite_legale,
        COALESCE(
            U.nom_usage_unite_legale,
            U.nom_unite_legale
        ),
        COALESCE(
            U.denomination_usuelle1_unite_legale,
            U.denomination_usuelle2_unite_legale,
            U.denomination_usuelle3_unite_legale,
            U.denomination_unite_legale
        )
    )
FROM source_api_sirene_unite_legale as U;

--

CREATE TABLE IF NOT EXISTS personne_physique (
    entreprise_id varchar(9) NOT NULL,
    sexe_type_id varchar(1),
    prenom_1 varchar,
    prenom_2 varchar,
    prenom_3 varchar,
    prenom_4 varchar,
    prenom_usuel varchar,
    pseudonyme varchar,

    PRIMARY KEY (entreprise_id)
);

INSERT INTO personne_physique (
    entreprise_id,
    sexe_type_id,
    prenom_1,
    prenom_2,
    prenom_3,
    prenom_4,
    prenom_usuel,
    pseudonyme
)
SELECT
    U.siren,
    CASE WHEN U.sexe_unite_legale = '[ND]' THEN NULL ELSE U.sexe_unite_legale END,
    prenom1_unite_legale,
    prenom2_unite_legale,
    prenom3_unite_legale,
    prenom4_unite_legale,
    prenom_usuel_unite_legale,
    pseudonyme_unite_legale
FROM source_api_sirene_unite_legale as U
WHERE U.categorie_juridique_unite_legale = '1000';

CREATE TABLE IF NOT EXISTS personne_morale (
    entreprise_id varchar(9) NOT NULL,
    sigle varchar,
    association_id varchar(10),

    PRIMARY KEY (entreprise_id)
);

INSERT INTO personne_morale (
    entreprise_id,
    sigle,
    association_id
)
SELECT
    U.siren,
    sigle_unite_legale,
    identifiant_association_unite_legale
FROM source_api_sirene_unite_legale as U
WHERE U.categorie_juridique_unite_legale <> '1000';



CREATE TABLE IF NOT EXISTS source_api_sirene_unite_legale_periode (
	  date_debut date,
    date_fin date,

	  siren varchar(9) NOT NULL,
    nic_siege_unite_legale varchar(5),

		nom_unite_legale varchar,
		nom_usage_unite_legale varchar,

    denomination_unite_legale varchar,
    denomination_usuelle_1_unite_legale varchar,
    denomination_usuelle_2_unite_legale varchar,
    denomination_usuelle_3_unite_legale varchar,

    nomenclature_activite_principale_unite_legale varchar(10),
    activite_principale_unite_legale varchar(6),

    categorie_juridique_Unite_legale varchar(4),

    etat_administratif_unite_legale source_sirene_unite_legale_etat_administratif_enum,
    caractere_employeur_unite_legale source_sirene_caractere_employeur_enum,
    economie_sociale_solidaire_unite_legale source_sirene_unite_legale_economie_sociale_solidaire_enum,
    societe_mission_unite_legale source_sirene_unite_legale_societe_mission_enum,

		changement_activite_principale_unite_legale boolean,
		changement_caractere_employeur_unite_legale boolean,
		changement_categorie_juridique_unite_legale boolean,
		changement_denomination_unite_legale boolean,
		changement_denomination_usuelle_unite_legale boolean,
		changement_economie_sociale_solidaire_unite_legale boolean,
		changement_societe_mission_unite_legale boolean,
		changement_etat_administratif_unite_legale boolean,
		changement_nic_siege_unite_legale boolean,
		changement_nom_unite_legale boolean,
		changement_nom_usage_unite_legale boolean,

		mise_a_jour_at timestamptz
);

ALTER TABLE source_api_sirene_unite_legale_periode ADD CONSTRAINT uk_source_api_sirene_unite_legale_periode UNIQUE (siren, date_debut);

INSERT INTO source_api_sirene_unite_legale_periode (
    date_debut,

    siren,
    nic_siege_unite_legale,

    nom_unite_legale,
    nom_usage_unite_legale,

    denomination_unite_legale,
    denomination_usuelle_1_unite_legale,
    denomination_usuelle_2_unite_legale,
    denomination_usuelle_3_unite_legale,

    nomenclature_activite_principale_unite_legale,
    activite_principale_unite_legale,

    categorie_juridique_Unite_legale,

    etat_administratif_unite_legale,
    caractere_employeur_unite_legale,
    economie_sociale_solidaire_unite_legale,
    societe_mission_unite_legale
)
SELECT
    U.date_debut,

    U.siren,
    U.nic_siege_unite_legale,

    nom_unite_legale,
    nom_usage_unite_legale,

    denomination_unite_legale,
    denomination_usuelle1_unite_legale,
    denomination_usuelle2_unite_legale,
    denomination_usuelle3_unite_legale,

    U.nomenclature_activite_principale_unite_legale,
    U.activite_principale_unite_legale,

    U.categorie_juridique_unite_legale,

    U.etat_administratif_unite_legale
    U.caractere_employeur_unite_legale
    U.economie_sociale_solidaire_unite_legale
    U.societe_mission_unite_legale
FROM source_api_sirene_unite_legale as U;


-- entreprise_periode

CREATE TABLE IF NOT EXISTS entreprise_periode (
    debut_date date,
    fin_date date,

    entreprise_id varchar(9) NOT NULL,
    siege_etablissement_id varchar(14) NOT NULL,

    nom varchar,
    nom_usage varchar,

    denomination varchar,
    denomination_usuelle_1 varchar,
    denomination_usuelle_2 varchar,
    denomination_usuelle_3 varchar,

    naf_revision_type_id varchar(10),
    naf_type_id varchar(6),

    categorie_juridique_type_id varchar(4) NOT NULL,

    actif boolean DEFAULT false NOT NULL,
    employeur boolean DEFAULT false NOT NULL,
    economie_sociale_solidaire boolean DEFAULT false NOT NULL,
    societe_mission boolean DEFAULT false,

    siege_etablissement_id_change boolean DEFAULT false NOT NULL,

    nom_change boolean DEFAULT false NOT NULL,
    nom_usage_change boolean DEFAULT false NOT NULL,

    denomination_change boolean DEFAULT false NOT NULL,
    denomination_usuelle_change boolean DEFAULT false NOT NULL,

    naf_type_change boolean DEFAULT false NOT NULL,

    categorie_juridique_change boolean DEFAULT false NOT NULL,

    actif_change boolean DEFAULT false NOT NULL,
    employeur_change boolean DEFAULT false NOT NULL,
    economie_sociale_solidaire_change boolean DEFAULT false NOT NULL,
    societe_mission_change boolean DEFAULT false NOT NULL
);

ALTER TABLE entreprise_periode ADD CONSTRAINT uk_entreprise_periode UNIQUE (entreprise_id, debut_date);

INSERT INTO entreprise_periode (
    debut_date,

    entreprise_id,
    siege_etablissement_id,

    nom,
    nom_usage,

    denomination,
    denomination_usuelle_1,
    denomination_usuelle_2,
    denomination_usuelle_3,

    naf_revision_type_id,
    naf_type_id,

    categorie_juridique_type_id,

    actif,
    employeur,
    economie_sociale_solidaire,
    societe_mission
)
SELECT
    U.date_debut,

    U.siren,
    U.siren || U.nic_siege_unite_legale,

    nom_unite_legale,
    nom_usage_unite_legale,

    denomination_unite_legale,
    denomination_usuelle1_unite_legale,
    denomination_usuelle2_unite_legale,
    denomination_usuelle3_unite_legale,

    U.nomenclature_activite_principale_unite_legale,
    U.activite_principale_unite_legale,

    U.categorie_juridique_unite_legale,

    CASE
        WHEN U.etat_administratif_unite_legale = 'A'
        THEN true
        ELSE false
    END,
    CASE
        WHEN U.caractere_employeur_unite_legale = 'O'
        THEN true
        ELSE false
    END,
    CASE
        WHEN U.economie_sociale_solidaire_unite_legale = 'O'
        THEN true
        ELSE false
    END,

    CASE
        WHEN U.societe_mission_unite_legale = 'O'
        THEN true
        ELSE false
    END
FROM source_api_sirene_unite_legale as U;

-- mandataires sociaux

CREATE TABLE IF NOT EXISTS source_api_entreprise_entreprise_mandataire_social (
    entreprise_id varchar(9) NOT NULL,
    payload jsonb NOT NULL,

    PRIMARY KEY (entreprise_id)
);

INSERT INTO source_api_entreprise_entreprise_mandataire_social (
    entreprise_id,
    payload
)
SELECT
    DISTINCT ON (SUBSTRING(siret, 1, 9))
    SUBSTRING(siret, 1, 9),
    mandataires_sociaux
FROM old.old_entreprise
WHERE
    mandataires_sociaux IS NOT NULL
    AND mandataires_sociaux <> '[]'
ORDER BY SUBSTRING(siret, 1, 9), last_api_update DESC;

CREATE TABLE IF NOT EXISTS mandataire_personne_physique (
    id SERIAL NOT NULL,

    entreprise_id varchar(9) NOT NULL,

    nom varchar NOT NULL,
    prenom varchar,
    fonction varchar NOT NULL,

    naissance_date date,
    naissance_lieu varchar,
    naissance_pays varchar,
    naissance_pays_code varchar,
    nationalite varchar,
    nationalite_code varchar,

    PRIMARY KEY (id)
);

INSERT INTO mandataire_personne_physique (
    entreprise_id,
    nom,
    prenom,
    fonction,
    naissance_date,
    naissance_lieu,
    naissance_pays,
    naissance_pays_code,
    nationalite,
    nationalite_code
)
SELECT
    entreprise_id,
    nom,
    prenom,
    fonction,
    CASE WHEN date_naissance = '' THEN NULL ELSE date_naissance::date END,
    lieu_naissance,
    pays_naissance,
    code_pays_naissance,
    nationalite,
    code_nationalite
FROM source_api_entreprise_entreprise_mandataire_social S,
    jsonb_to_recordset(S.payload) as mandataire(
        siren varchar,
        nom varchar,
        prenom varchar,
        type varchar,
        fonction varchar,
        date_naissance varchar,
        lieu_naissance varchar,
        pays_naissance varchar,
        code_pays_naissance varchar,
        nationalite varchar,
        code_nationalite varchar
    )
WHERE type IN ('PP', 'personne_physique');

CREATE TABLE IF NOT EXISTS mandataire_personne_morale (
    id SERIAL NOT NULL,

    entreprise_id varchar(9) NOT NULL,

    mandataire_entreprise_id varchar(9),

    fonction varchar NOT NULL,
    greffe_code varchar,
    greffe_libelle varchar,

    PRIMARY KEY (id)
);

INSERT INTO mandataire_personne_morale (
    entreprise_id,
    mandataire_entreprise_id,
    fonction,
    greffe_code,
    greffe_libelle
)
SELECT
    entreprise_id,
    NULLIF(numero_identification, '000000000'),
    fonction,
    code_greffe,
    libelle_greffe
FROM source_api_entreprise_entreprise_mandataire_social S,
    jsonb_to_recordset(S.payload) as mandataire(
        type varchar,
        numero_identification varchar,
        fonction varchar,
        code_greffe varchar,
        libelle_greffe varchar
    )
WHERE type IN ('PM', 'personne_morale');

-- etablissement

CREATE TABLE IF NOT EXISTS etablissement (
    id varchar(14),
    nic varchar(5),

    creation_date date,
    sirene_dernier_traitement_date date,

    effectif_type_id varchar(2),
    effectif_annee int,

    entreprise_id varchar(9) NOT NULL,

    commune_id varchar(5) NOT NULL,
    diffusible boolean NOT NULL DEFAULT true,

    siege boolean,
    nom varchar,

    PRIMARY KEY (id)
);

INSERT INTO etablissement (
    id,
    nic,

    creation_date,
    sirene_dernier_traitement_date,

    effectif_type_id,
    effectif_annee,

    entreprise_id,

    commune_id,
    diffusible,

    siege,

    nom
)
SELECT
    E.siret,
    E.nic,

    E.date_creation_etablissement,
    E.date_dernier_traitement_etablissement,

    E.tranche_effectifs_etablissement,
    E.annee_effectifs_etablissement::int,

    E.siren,

    E.code_commune_etablissement,
    CASE
        WHEN E.statut_diffusion_etablissement = 'O'
        THEN true
        ELSE false
    END,

    E.etablissement_siege,

    -- etablissement nom
    -- dans la plupart des cas la dénomination usuelle est identique à l'enseigne
    COALESCE(
        E.denomination_usuelle_etablissement,
        E.enseigne1_etablissement,
        E.enseigne2_etablissement,
        E.enseigne3_etablissement
    )
FROM source_api_sirene_etablissement E
JOIN commune as C ON C.id = e.code_commune_etablissement;

-- HACK (données de seed) insert les sieges d'établissement manquants
-- WITH etablissement_manquant AS (
--     SELECT entreprise_periode.siege_etablissement_id AS id
--     FROM entreprise_periode
--     LEFT JOIN etablissement ON entreprise_periode.siege_etablissement_id = etablissement.id
--     WHERE etablissement.id IS NULL
-- )
-- INSERT INTO etablissement (id, entreprise_id, commune_id)
-- SELECT id, SUBSTRING(id, 1, 9), '31555'
-- FROM etablissement_manquant;

-- Cette contrainte ne fonctionne pas car 321 000 sièges d'établissement ne sont pas dans notre table `etablissement`
-- ALTER TABLE entreprise_periode ADD CONSTRAINT fk_entreprise_periode__siege_etablissement FOREIGN KEY (siege_etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS etablissement_adresse_1 (
    etablissement_id varchar(14)
) INHERITS (adresse);

INSERT INTO etablissement_adresse_1 (
    etablissement_id,
    commune_id,
    numero,
    voie_nom,
    code_postal,
    distribution_speciale,
    cedex_code,
    cedex_nom,
    geolocalisation
)
SELECT
    E.siret,
    E.code_commune_etablissement,
    TRIM(
        CASE
            WHEN numero_voie_etablissement IS NOT NULL AND numero_voie_etablissement <> ''
            THEN numero_voie_etablissement || ' '
            ELSE ''
        END ||
        CASE
            WHEN indice_repetition_etablissement IS NOT NULL AND indice_repetition_etablissement <> ''
            THEN indice_repetition_etablissement || ' '
            ELSE ''
        END
    ),
    TRIM(
        CASE
            WHEN type_voie_etablissement IS NOT NULL AND type_voie_etablissement <> ''
            THEN type_voie_etablissement || ' '
            ELSE ''
        END ||
        CASE
            WHEN libelle_voie_etablissement IS NOT NULL AND libelle_voie_etablissement <> ''
            THEN libelle_voie_etablissement || ' '
            ELSE ''
        END ||
        CASE
            WHEN complement_adresse_etablissement IS NOT NULL AND complement_adresse_etablissement <> ''
            THEN complement_adresse_etablissement
            ELSE ''
        END
    ),
    E.code_postal_etablissement,
    E.distribution_speciale_etablissement,
    E.code_cedex_etablissement,
    E.libelle_cedex_etablissement,
    ST_SetSRID(ST_Point(E.longitude, E.latitude), 4326)
FROM source_api_sirene_etablissement E
-- évite les établissement dont l'adresse est à l'étranger ou dans les comer
JOIN commune as C ON C.id = E.code_commune_etablissement;

-- etablissement_periode

CREATE TABLE IF NOT EXISTS source_api_sirene_etablissement_periode (
    siret varchar(14) NOT NULL,
    activite_principale_etablissement varchar(6),
    caractere_employeur_etablissement source_sirene_caractere_employeur_enum,
    date_debut date,
    date_fin date,
    denomination_usuelle_etablissement varchar,

    enseigne1_etablissement varchar,
    enseigne2_etablissement varchar,
    enseigne3_etablissement varchar,

    etat_administratif_etablissement source_sirene_etablissement_etat_administratif_enum,
    nomenclature_activite_principale_etablissement varchar(8),
    changement_activite_principale_etablissement boolean,
    changement_caractere_employeur_etablissement boolean,
    changement_enseigne_etablissement boolean,
    changement_etat_administratif_etablissement boolean,
    mise_a_jour_at timestamptz
);

INSERT INTO source_api_sirene_etablissement_periode (
    siret,
    activite_principale_etablissement,
    caractere_employeur_etablissement,
    date_debut,
    date_fin,
    denomination_usuelle_etablissement,

    enseigne1_etablissement,
    enseigne2_etablissement,
    enseigne3_etablissement,

    etat_administratif_etablissement,
    nomenclature_activite_principale_etablissement
)
SELECT
    E.siret,
    E.activite_principale_etablissement,
    E.caractere_employeur_etablissement,
    E.date_debut,
    NULL,
    E.denomination_usuelle_etablissement,

    E.enseigne1_etablissement,
    E.enseigne2_etablissement,
    E.enseigne3_etablissement,

    E.etat_administratif_etablissement,
    E.nomenclature_activite_principale_etablissement
FROM source_api_sirene_etablissement E
-- uniquement pour les établissements sur les communes en france
JOIN commune as C ON C.id = E.code_commune_etablissement;

ALTER TABLE source_api_sirene_etablissement_periode ADD CONSTRAINT uk_source_api_sirene_etablissement_periode UNIQUE (siret, date_debut);

CREATE TABLE IF NOT EXISTS etablissement_periode (
    etablissement_id varchar(14),

    debut_date date,
    fin_date date,

    enseigne_1 varchar,
    enseigne_2 varchar,
    enseigne_3 varchar,

    naf_revision_type_id varchar(10),
    naf_type_id varchar(6),

    actif boolean,
    denomination_usuelle varchar,

    employeur boolean DEFAULT false NOT NULL,
    actif_change boolean DEFAULT false NOT NULL,
    enseigne_change boolean DEFAULT false NOT NULL,
    denomination_usuelle_change boolean DEFAULT false NOT NULL,
    naf_type_change boolean DEFAULT false NOT NULL,
    employeur_change boolean DEFAULT false NOT NULL

    -- why? revision inexistante?
    -- CONSTRAINT fk_etablissement_periode__naf_type_revision_type FOREIGN KEY (naf_type_id, naf_revision_type_id) REFERENCES naf_type(id, naf_revision_type_id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO etablissement_periode (
    etablissement_id,

    debut_date,
    fin_date,

    enseigne_1,
    enseigne_2,
    enseigne_3,

    naf_revision_type_id,
    naf_type_id,

    actif,
    denomination_usuelle,
    employeur
)
SELECT
    E.siret,

    E.date_debut,
    NULL,

    E.enseigne1_etablissement,
    E.enseigne2_etablissement,
    E.enseigne3_etablissement,

    E.nomenclature_activite_principale_etablissement,
    E.activite_principale_etablissement,

    CASE
        WHEN E.etat_administratif_etablissement = 'A'
        THEN true
        ELSE false
    END,
    E.denomination_usuelle_etablissement,
		CASE
        WHEN E.caractere_employeur_etablissement = 'O'
        THEN true
        ELSE false
    END
FROM source_api_sirene_etablissement E
-- uniquement pour les établissements sur les communes en france
JOIN commune as C ON C.id = E.code_commune_etablissement;

-- etablissement_exercice

CREATE TABLE IF NOT EXISTS source_api_entreprise_etablissement_exercice (
    siret varchar(14) NOT NULL,
    date_fin_exercice date NOT NULL,
    chiffre_affaires varchar,

    PRIMARY KEY(siret, date_fin_exercice)
);

INSERT INTO source_api_entreprise_etablissement_exercice (
    siret,
    date_fin_exercice,
    chiffre_affaires
)
SELECT
    entreprise_id,
    extract( year FROM date_cloture )::int,
    ca
FROM old.old_exercice;

CREATE TABLE IF NOT EXISTS etablissement_exercice (
    etablissement_id varchar(14) NOT NULL,
    cloture_annee integer NOT NULL,
    chiffre_affaires varchar,

    PRIMARY KEY (etablissement_id, cloture_annee)
);

INSERT INTO etablissement_exercice (
    etablissement_id,
    cloture_annee,
    chiffre_affaires
)
SELECT
    siret,
    date_fin_exercice,
    chiffre_affaires
FROM source_api_entreprise_etablissement_exercice;

-- equipe

CREATE TABLE IF NOT EXISTS equipe (
    id SERIAL,

    nom varchar NOT NULL,
    groupement boolean DEFAULT false NOT NULL,

    territoire_type_id varchar NOT NULL,
    equipe_type_id varchar NOT NULL DEFAULT 'autre',

    PRIMARY KEY(id)
);

INSERT INTO equipe (
    id,
    nom,
    groupement,
    territoire_type_id,
    equipe_type_id
)
SELECT
    id,
    name,
    CASE
        WHEN departement_id = '6AE' THEN true
        WHEN perimetre_id IS NOT NULL THEN true
        ELSE false
    END,
    CASE
        WHEN territory_type IN ('COM', 'ARM') THEN 'commune'
        WHEN territory_type IN ('CA', 'CC', 'CU', 'EPT', 'ME') THEN 'epci'
        ELSE REPLACE(territory_type, 'groupement de ', '')
    END,
    CASE
        WHEN territory_type = 'COM' THEN 'commune'
        WHEN perimetre_id IS NOT NULL THEN 'deveco_locale'
        WHEN metropole_id IS NOT NULL THEN 'plm'
        ELSE territory_type
    END
FROM old.old_territory;

-- deveco

CREATE TABLE IF NOT EXISTS deveco (
    id SERIAL,

    compte_id integer NOT NULL,
    equipe_id integer NOT NULL,
    bienvenue_email boolean NOT NULL default false,

    created_at TIMESTAMP NOT NULL default now(),
    updated_at TIMESTAMP NOT NULL default now(),

    PRIMARY KEY(id)
);

INSERT INTO deveco (
    id,
    compte_id,
    equipe_id,
    bienvenue_email,
    created_at,
    updated_at
)
SELECT
    id,
    account_id,
    territory_id,
    welcome_email_sent,
    created_at,
    updated_at
FROM old.old_deveco;

-- favoris

CREATE TABLE IF NOT EXISTS etablissement_favori (
    deveco_id integer,
    etablissement_id varchar(14),

    PRIMARY KEY(deveco_id, etablissement_id)
);

INSERT INTO etablissement_favori(deveco_id, etablissement_id)
SELECT deveco_id, siret
FROM old.old_etablissement_favori
WHERE
    (siret <> '' OR siret IS NOT NULL)
    AND favori;

-- zonage

CREATE TABLE IF NOT EXISTS zonage (
    id SERIAL,

    nom varchar(254) NOT NULL,
    geometrie geometry(MultiPolygon, 4326),

    equipe_id integer NOT NULL,

    PRIMARY KEY(id)
);

INSERT INTO zonage (
    id,
    nom,
    geometrie,
    equipe_id
)
SELECT
    id,
    nom,
    geometry,
    territoire_id
FROM old.old_zonage;

-- territoire de l'équipe

-- commune

CREATE TABLE IF NOT EXISTS equipe__commune (
    equipe_id integer NOT NULL,
    commune_id varchar(5) NOT NULL,

    PRIMARY KEY(equipe_id, commune_id)
);

INSERT INTO equipe__commune (equipe_id, commune_id)
SELECT id, commune_id
FROM old.old_territory
WHERE commune_id IS NOT NULL;

INSERT INTO equipe__commune (equipe_id, commune_id)
SELECT T.id, P."communeInseeCom"
FROM old.old_territory T
JOIN old.old_perimetre_communes_commune P ON P."perimetreId" = T.perimetre_id;

-- epci

CREATE TABLE IF NOT EXISTS equipe__epci (
    equipe_id integer NOT NULL,
    epci_id varchar(9) NOT NULL,

    PRIMARY KEY(equipe_id, epci_id)
);

INSERT INTO equipe__epci (equipe_id, epci_id)
SELECT id, epci_id
FROM old.old_territory
WHERE epci_id IS NOT NULL;

INSERT INTO equipe__epci (equipe_id, epci_id)
SELECT T.id, P."epciInseeEpci"
FROM old.old_territory T
JOIN old.old_perimetre_epcis_epci P ON P."perimetreId" = T.perimetre_id;

-- metropole

CREATE TABLE IF NOT EXISTS equipe__metropole (
    equipe_id integer NOT NULL,
    metropole_id varchar(5) NOT NULL,

    PRIMARY KEY(equipe_id, metropole_id)
);

INSERT INTO equipe__metropole (equipe_id, metropole_id)
SELECT id, metropole_id
FROM old.old_territory
WHERE metropole_id IS NOT NULL;

-- petr

CREATE TABLE IF NOT EXISTS equipe__petr (
    equipe_id integer NOT NULL,
    petr_id varchar(5) NOT NULL,

    PRIMARY KEY(equipe_id, petr_id)
);

INSERT INTO equipe__petr (equipe_id, petr_id)
SELECT id, petr_id
FROM old.old_territory
WHERE petr_id IS NOT NULL;

-- departement

CREATE TABLE IF NOT EXISTS equipe__departement (
    equipe_id integer NOT NULL,
    departement_id varchar(3) NOT NULL,

    PRIMARY KEY(equipe_id, departement_id)
);

INSERT INTO equipe__departement (equipe_id, departement_id)
SELECT id, left(departement_id, -1)
FROM old.old_territory
WHERE departement_id IS NOT NULL AND departement_id <> '6AE';

-- Collectivité européenne d'Alsace => Bas-Rhin et Haut-Rhin
INSERT INTO equipe__departement (equipe_id, departement_id)
SELECT id, departement
FROM old.old_territory, (VALUES ('67'), ('68')) AS deps(departement)
WHERE departement_id = '6AE';

INSERT INTO equipe__departement (equipe_id, departement_id)
SELECT T.id, P."departementCtcd"
FROM old.old_territory T
JOIN old.old_perimetre_departements_departement P ON P."perimetreId" = T.perimetre_id;

-- region

CREATE TABLE IF NOT EXISTS equipe__region (
    equipe_id integer NOT NULL,
    region_id varchar(2) NOT NULL,

    PRIMARY KEY(equipe_id, region_id)
);

INSERT INTO equipe__region (equipe_id, region_id)
SELECT id, region_id
FROM old.old_territory
WHERE region_id IS NOT NULL;

-- etiquette
CREATE TABLE IF NOT EXISTS etiquette (
    id serial,
    nom varchar NOT NULL,

    equipe_id integer NOT NULL,
    etiquette_type_id varchar(16) NOT NULL,

    PRIMARY KEY(id)
);

INSERT INTO etiquette (
    nom,
    equipe_id,
    etiquette_type_id
)
SELECT
    mot_cle,
    territory_id,
    'mot_cle'
FROM old.old_mot_cle;

INSERT INTO etiquette (
    nom,
    equipe_id,
    etiquette_type_id
)
SELECT
    activite,
    territory_id,
    'activite'
FROM old.old_activite_entreprise;

INSERT INTO etiquette (
    nom,
    equipe_id,
    etiquette_type_id
)
SELECT
    localisation,
    territory_id,
    'localisation'
FROM old.old_localisation_entreprise;

-- echange

CREATE TABLE IF NOT EXISTS echange (
    id serial,

    nom varchar,
    description text,
    date timestamptz NOT NULL,

    echange_type_id varchar(10) NOT NULL,
    equipe_id integer NOT NULL,
    deveco_id integer NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO echange (
    id,
    nom,
    description,
    date,
    echange_type_id,
    equipe_id,
    deveco_id
)
SELECT
    E.id,
    E.titre,
    E.compte_rendu,
    date_echange,
    E.type_echange,
    D.territory_id,
    E.createur_id
FROM old.old_echange E
JOIN old.old_deveco D ON D.id = E.createur_id
WHERE E.type_echange <> 'transformation';

-- demande
CREATE TABLE IF NOT EXISTS demande (
    id SERIAL,

    date timestamptz NOT NULL,

    demande_type_id varchar(20) NOT NULL,
    equipe_id integer NOT NULL,

    cloture_motif varchar,

    PRIMARY KEY (id)
);

-- id fiche où il y a plusieurs demandes de même type :
    -- 9881
    -- 9616
    -- 15391
    -- 9881
    -- 9662

INSERT INTO demande (
    id,
    date,
    demande_type_id,
    equipe_id,
    cloture_motif
)
SELECT
    DISTINCT ON (D.id)
    D.id as demande_id,
    D.created_at,
    D.type_demande,
    F.territoire_id,
    D.motif
FROM old.old_demande D
INNER JOIN old.old_fiche F on D.fiche_id = F.id;

CREATE TABLE IF NOT EXISTS echange__demande (
    echange_id integer NOT NULL,
    demande_id integer NOT NULL,

    PRIMARY KEY (echange_id, demande_id)
);

INSERT INTO echange__demande (
    echange_id, demande_id
)
SELECT
    E.id, D.id
FROM old.old_echange E
JOIN old.old_demande D ON D.fiche_id = E.fiche_id
WHERE D.type_demande = ANY(string_to_array(E.themes, ','));

-- rappel

CREATE TABLE IF NOT EXISTS rappel (
    id SERIAL,

    nom varchar NOT NULL,
    date timestamptz NOT NULL,
    equipe_id integer NOT NULL,
    description text,

    PRIMARY KEY (id)
);

INSERT INTO rappel (
    id,
    nom,
    date,
    equipe_id,
    description
)
SELECT
    R.id,
    CASE WHEN LENGTH(R.titre) > 100
        THEN SUBSTRING(R.titre, 1, 97) || '...'
        ELSE R.titre
    END,
    R.date_rappel,
    D.territory_id,
    CASE WHEN LENGTH(R.titre) > 100
        THEN R.titre
        ELSE NULL
    END
FROM old.old_rappel R
JOIN old.old_deveco D ON D.id = R.createur_id;

-- personne

CREATE TABLE IF NOT EXISTS personne (
    id SERIAL,

    nom varchar NOT NULL,
    prenom varchar NOT NULL,
    email varchar,
    telephone varchar,
    naissance_date date,

    equipe_id integer NOT NULL,

    created_at timestamptz NOT NULL DEFAULT now(),

    PRIMARY KEY (id)
);

-- equipe etablissement

CREATE TABLE IF NOT EXISTS equipe__etablissement_exogene (
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    ajout_at timestamptz NOT NULL DEFAULT now(),

    PRIMARY KEY (equipe_id, etablissement_id)
    -- CONSTRAINT fk_equipe__etablissement__exogene__equipe__etablissement FOREIGN KEY (equipe_id, etablissement_id) REFERENCES equipe__etablissement(equipe_id, etablissement_id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO equipe__etablissement_exogene (
    equipe_id,
    etablissement_id,
    ajout_at
)
SELECT
    territoire_id,
    entreprise_id,
    created_at
FROM old.old_entite T
WHERE exogene;

-- description

CREATE TABLE IF NOT EXISTS equipe__etablissement_description (
    description text NOT NULL,

    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,

    PRIMARY KEY (equipe_id, etablissement_id)
);

INSERT INTO equipe__etablissement_description (
    equipe_id,
    etablissement_id,
    description
)
SELECT
    territoire_id,
    entreprise_id,
    activite_autre
FROM old.old_entite T
WHERE
    entreprise_id IS NOT NULL
    AND activite_autre <> '' AND activite_autre IS NOT NULL;

-- etiquette dans etablissement

CREATE TABLE IF NOT EXISTS equipe__etablissement__etiquette (
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    etiquette_id integer NOT NULL,

    PRIMARY KEY(equipe_id, etablissement_id, etiquette_id)
);

-- mots_cles

WITH etablissement_mot_cle AS (
    SELECT
      DISTINCT UNNEST(string_to_array(mots_cles, ',')) as mot_cle,
      T.territoire_id,
      E.siret as entreprise_id
    FROM old.old_entite T
    JOIN old.old_entreprise E ON E.siret = entreprise_id
)
INSERT INTO equipe__etablissement__etiquette (
    equipe_id,
    etablissement_id,
    etiquette_id
)
SELECT emc.territoire_id, emc.entreprise_id, EE.id
FROM etablissement_mot_cle as emc
JOIN etiquette EE
    ON EE.nom = emc.mot_cle
    AND EE.etiquette_type_id = 'mot_cle'
    AND EE.equipe_id = territoire_id;

-- localisations

WITH etablissement_entreprise_localisation AS (
    SELECT
      DISTINCT UNNEST(string_to_array(entreprise_localisations, ',')) as localisation,
      T.territoire_id,
      E.siret AS entreprise_id
    FROM old.old_entite T
    JOIN old.old_entreprise E ON E.siret = entreprise_id
)
INSERT INTO equipe__etablissement__etiquette (
    equipe_id,
    etablissement_id,
    etiquette_id
)
SELECT eml.territoire_id, eml.entreprise_id, EE.id
FROM etablissement_entreprise_localisation as eml
JOIN etiquette EE
    ON EE.nom = eml.localisation
    AND EE.etiquette_type_id = 'localisation'
    AND EE.equipe_id = territoire_id;

-- activites

WITH etablissement_activites_reelles AS (
    SELECT
      DISTINCT UNNEST(string_to_array(activites_reelles, ',')) as activite,
      T.territoire_id,
      E.siret as entreprise_id
    FROM old.old_entite T
    JOIN old.old_entreprise E ON E.siret = T.entreprise_id
)
INSERT INTO equipe__etablissement__etiquette (
    equipe_id,
    etablissement_id,
    etiquette_id
)
SELECT emr.territoire_id, emr.entreprise_id, EE.id
FROM etablissement_activites_reelles as emr
JOIN etiquette EE
    ON EE.nom = emr.activite
    AND EE.etiquette_type_id = 'activite'
    AND EE.equipe_id = territoire_id;

-- echange

CREATE TABLE IF NOT EXISTS equipe__etablissement__echange (
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    echange_id integer NOT NULL,

    PRIMARY KEY(equipe_id, etablissement_id, echange_id)
    -- CONSTRAINT fk_equipe__etablissement__echange__equipe__etablissement FOREIGN KEY (equipe_id, etablissement_id) REFERENCES equipe__etablissement(equipe_id, etablissement_id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO equipe__etablissement__echange (
    equipe_id,
    etablissement_id,
    echange_id
)
SELECT F.territoire_id, T.entreprise_id, E.id
FROM old.old_echange E
JOIN old.old_fiche F ON F.id = E.fiche_id
JOIN old.old_entite T ON T.id = F.entite_id
WHERE T.entreprise_id IS NOT NULL
AND E.type_echange <> 'transformation';

-- demande

CREATE TABLE IF NOT EXISTS equipe__etablissement__demande (
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    demande_id integer NOT NULL,

    PRIMARY KEY(equipe_id, etablissement_id, demande_id)
    -- CONSTRAINT fk_equipe__etablissement__demande__equipe__etablissement FOREIGN KEY (equipe_id, etablissement_id) REFERENCES equipe__etablissement(equipe_id, etablissement_id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO equipe__etablissement__demande (
    equipe_id,
    etablissement_id,
    demande_id
)
SELECT
    F.territoire_id,
    T.entreprise_id,
    D.id
FROM old.old_demande D
JOIN old.old_fiche F on D.fiche_id = F.id
JOIN old.old_entite T ON T.id = F.entite_id
WHERE T.entreprise_id IS NOT NULL;

-- rappel

CREATE TABLE IF NOT EXISTS equipe__etablissement__rappel (
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    rappel_id integer NOT NULL,

    PRIMARY KEY(equipe_id, etablissement_id, rappel_id)
    -- CONSTRAINT fk_equipe__etablissement__rappel__equipe__etablissement FOREIGN KEY (equipe_id, etablissement_id) REFERENCES equipe__etablissement(equipe_id, etablissement_id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO equipe__etablissement__rappel (
    equipe_id,
    etablissement_id,
    rappel_id
)
SELECT F.territoire_id, T.entreprise_id, R.id
FROM old.old_rappel R
JOIN old.old_fiche F ON F.id = R.fiche_id
JOIN old.old_entite T ON T.id = F.entite_id
WHERE T.entreprise_id IS NOT NULL;

-- contact

CREATE TABLE IF NOT EXISTS contact (
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    personne_id integer NOT NULL,

    fonction varchar,

    contact_source_type_id varchar(10) NOT NULL,

    PRIMARY KEY(equipe_id, etablissement_id, personne_id)
    -- CONSTRAINT fk_contact__equipe__etablissement FOREIGN KEY (equipe_id, etablissement_id) REFERENCES equipe__etablissement(equipe_id, etablissement_id) ON DELETE CASCADE ON UPDATE CASCADE
);

WITH personne as (
    INSERT INTO personne (
        id,
        nom,
        prenom,
        email,
        telephone,
        naissance_date,
        equipe_id,
        created_at
    )
    SELECT
        P.id,
        P.nom,
        P.prenom,
        P.email,
        P.telephone,
        P.date_de_naissance,
        E.territoire_id,
        C.created_at
    FROM old.old_particulier P
    JOIN old.old_contact C ON C.particulier_id = P.id
    JOIN old.old_entite E ON E.id = C.entite_id
    WHERE
        (C.source IS NULL OR C.source = 'import')
        AND E.entreprise_id IS NOT NULL
    ON CONFLICT DO NOTHING
    RETURNING id, equipe_id
)
INSERT INTO contact (
    equipe_id,
    etablissement_id,
    personne_id,
    fonction,
    contact_source_type_id
)
SELECT
    P.equipe_id,
    E.entreprise_id,
    P.id,
    C.fonction,
    CASE
        WHEN C.source IS NULL THEN 'deveco'
        ELSE C.source
    END
FROM old.old_contact C
JOIN personne P ON P.id = C.particulier_id
JOIN old.old_entite E ON E.id = C.entite_id
WHERE
    -- on n'insère pas les mendataires sociaux créés par l'API entreprise
    (C.source IS NULL OR C.source = 'import')
    AND E.entreprise_id IS NOT NULL;

-- equipe etablissement_creation

CREATE TABLE IF NOT EXISTS etablissement_creation (
    id SERIAL,
    enseigne varchar,
    description text,
    commentaire text,

    equipe_id integer NOT NULL,
    entreprise_id varchar(9),

    PRIMARY KEY(id)
);

INSERT INTO etablissement_creation (
    id,
    enseigne,
    description,
    commentaire,
    equipe_id
)
SELECT
    E.particulier_id,
    E.future_enseigne,
    PA.description,
    E.activite_autre,
    E.territoire_id
FROM old.old_entite E
LEFT JOIN old.old_proprietaire P ON P.entite_id = E.id
LEFT JOIN old.old_particulier PA ON PA.id = E.particulier_id
WHERE
    E.particulier_id IS NOT NULL
    AND P.entite_id IS NULL;

-- remise à niveau pour récupérer la description depuis particulier

UPDATE etablissement_creation
set
    description = PA.description,
    commentaire = E.activite_autre
FROM old.old_entite E
LEFT JOIN old.old_particulier PA ON E.id = E.particulier_id
WHERE
    E.particulier_id = etablissement_creation.id;

--

-- etiquette

CREATE TABLE IF NOT EXISTS etablissement_creation__etiquette (
    equipe_id integer NOT NULL,
    etablissement_creation_id integer NOT NULL,
    etiquette_id integer NOT NULL,

    PRIMARY KEY(etablissement_creation_id, etiquette_id)
);

-- mots_cles

WITH etablissement_creation_mots_cles AS (
    SELECT
      DISTINCT UNNEST(string_to_array(mots_cles, ',')) as mot_cle,
      T.territoire_id,
      particulier_id
    FROM old.old_entite T
    JOIN old.old_particulier op ON op.id = particulier_id
)
INSERT INTO etablissement_creation__etiquette (
    equipe_id,
    etablissement_creation_id,
    etiquette_id
)
SELECT territoire_id, particulier_id, EE.id
FROM etablissement_creation_mots_cles as ecmc
JOIN etiquette EE
    ON EE.nom = ecmc.mot_cle
    AND EE.etiquette_type_id = 'mot_cle'
    AND EE.equipe_id = territoire_id;

-- localisations

WITH entreprise_localisations AS (
    SELECT
      DISTINCT UNNEST(string_to_array(entreprise_localisations, ',')) as localisation,
      territoire_id,
      particulier_id
    FROM old.old_entite
    JOIN old.old_particulier AS op ON op.id = particulier_id
)
INSERT INTO etablissement_creation__etiquette (
    equipe_id,
    etablissement_creation_id,
    etiquette_id
)
SELECT territoire_id, particulier_id, EE.id
FROM entreprise_localisations as el
JOIN etiquette EE
    ON EE.nom = el.localisation
    AND EE.etiquette_type_id = 'localisation'
    AND EE.equipe_id = territoire_id;

-- activites

WITH activites_reelles AS (
    SELECT
        DISTINCT UNNEST(string_to_array(activites_reelles, ',')) as activite,
        territoire_id,
        particulier_id
    FROM old.old_entite
    JOIN old.old_particulier AS op ON op.id = particulier_id
)
INSERT INTO etablissement_creation__etiquette (
    equipe_id,
    etablissement_creation_id,
    etiquette_id
)
SELECT territoire_id, particulier_id, EE.id
FROM activites_reelles as ar
JOIN etiquette EE
    ON EE.nom = ar.activite
    AND EE.etiquette_type_id = 'activite'
    AND EE.equipe_id = territoire_id;

-- echange

CREATE TABLE IF NOT EXISTS etablissement_creation__echange (
    equipe_id integer NOT NULL,
    etablissement_creation_id integer NOT NULL,
    echange_id integer NOT NULL,

    PRIMARY KEY(etablissement_creation_id, echange_id)
);

INSERT INTO etablissement_creation__echange (
    equipe_id,
    etablissement_creation_id,
    echange_id
)
SELECT F.territoire_id, T.particulier_id, E.id
FROM old.old_echange E
JOIN old.old_fiche F ON F.id = E.fiche_id
JOIN old.old_entite T ON T.id = F.entite_id
WHERE T.particulier_id IS NOT NULL;

-- echange de type transformation

CREATE TABLE IF NOT EXISTS etablissement_creation_transformation (
    id SERIAL,
    date timestamptz NOT NULL,

    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    etablissement_creation_id integer NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO etablissement_creation_transformation (
    date,
    equipe_id,
    etablissement_creation_id,
    etablissement_id
)
SELECT
    date_echange,
    T.territoire_id,
    T.createur_id,
    T.entreprise_id
FROM old.old_echange E
JOIN old.old_fiche F ON F.id = E.fiche_id
JOIN old.old_entite T on T.id = F.entite_id
WHERE E.type_echange = 'transformation';

-- demande

CREATE TABLE IF NOT EXISTS etablissement_creation__demande (
    equipe_id integer NOT NULL,
    etablissement_creation_id integer NOT NULL,
    demande_id integer NOT NULL,

    PRIMARY KEY(etablissement_creation_id, demande_id)
);

INSERT INTO etablissement_creation__demande (
    equipe_id,
    etablissement_creation_id,
    demande_id
)
SELECT F.territoire_id, T.particulier_id, D.id
FROM old.old_demande D
JOIN old.old_fiche F ON F.id = D.fiche_id
JOIN old.old_entite T ON T.id = F.entite_id
WHERE T.particulier_id IS NOT NULL;

-- rappel

CREATE TABLE IF NOT EXISTS etablissement_creation__rappel (
    equipe_id integer NOT NULL,
    etablissement_creation_id integer NOT NULL,
    rappel_id integer NOT NULL,

    PRIMARY KEY(etablissement_creation_id, rappel_id)
);

INSERT INTO etablissement_creation__rappel (
    equipe_id,
    etablissement_creation_id,
    rappel_id
)
SELECT F.territoire_id, T.particulier_id, R.id
FROM old.old_rappel R
JOIN old.old_fiche F ON F.id = R.fiche_id
JOIN old.old_entite T ON T.id = F.entite_id
WHERE T.particulier_id IS NOT NULL;

-- portefeuille

CREATE TABLE IF NOT EXISTS equipe__etablissement_portefeuille AS
SELECT DISTINCT equipe_id, etablissement_id
FROM (
    SELECT equipe_id, etablissement_id, 'etiquette' as type
    FROM equipe__etablissement__etiquette
    GROUP BY equipe_id, etablissement_id
    UNION
    SELECT equipe_id, etablissement_id, 'description' as type
    FROM equipe__etablissement_description
    GROUP BY equipe_id, etablissement_id
    UNION
    SELECT equipe_id, etablissement_id, 'echange' as type
    FROM equipe__etablissement__echange
    GROUP BY equipe_id, etablissement_id
    UNION
    SELECT equipe_id, etablissement_id, 'demande' as type
    FROM equipe__etablissement__demande
    GROUP BY equipe_id, etablissement_id
    UNION
    SELECT equipe_id, etablissement_id, 'rappel' as type
    FROM equipe__etablissement__rappel
    GROUP BY equipe_id, etablissement_id
    UNION
    SELECT equipe_id, etablissement_id, 'contact' as type
    FROM contact
    GROUP BY equipe_id, etablissement_id
) t;

ALTER TABLE equipe__etablissement_portefeuille ADD CONSTRAINT PK_equipe__etablissement_portefeuille PRIMARY KEY (equipe_id, etablissement_id);

-- personne

INSERT INTO personne (
    id,
    nom,
    prenom,
    email,
    telephone,
    naissance_date,
    equipe_id,
    created_at
)
SELECT
    P.id,
    P.nom,
    P.prenom,
    P.email,
    P.telephone,
    P.date_de_naissance,
    E.territoire_id,
    P.created_at
FROM old.old_particulier P
JOIN old.old_entite E ON E.particulier_id = P.id
LEFT JOIN old.old_proprietaire R ON R.entite_id = E.id
WHERE
    E.particulier_id IS NOT NULL
    AND R.entite_id IS NULL;

CREATE TABLE IF NOT EXISTS personne_adresse (
    personne_id integer NOT NULL
) INHERITS (adresse);

WITH villes as (
    SELECT DISTINCT ON (L.nom, L.code_postal, L.code_insee)
    L.nom, L.code_postal, L.code_insee
    FROM source_laposte_code_insee_code_postal L
)
INSERT INTO personne_adresse (
    personne_id,
    numero,
    voie_nom,
    code_postal,
    geolocalisation,
    commune_id
)
SELECT
    P.id,
    CASE
        WHEN P.adresse ~ '^\d+(bis|ter|quater)?'
        THEN REGEXP_REPLACE(P.adresse, '^(\d+(bis|ter|quater)?)? ?.*', '\1')
        ELSE NULL
    END,
    REGEXP_REPLACE(REGEXP_REPLACE(P.adresse, '\d{5}.*$', ''), '^\d+(bis|ter|quater)? ', ''),
    P.code_postal,
    P.geolocation::point::geometry(Point, 4326),
    L.code_insee
FROM old.old_particulier P
JOIN old.old_entite E ON E.particulier_id = P.id
LEFT JOIN old.old_proprietaire R ON R.entite_id = E.id
LEFT JOIN villes L
    ON L.code_postal = P.code_postal
    AND LOWER(L.nom) LIKE
            REPLACE(
                REPLACE(
                    REPLACE(
                        LOWER(unaccent(P.ville)),
                        '-', ' '),
                    '''', ' '),
                'saint', 'st')
WHERE
    P.code_postal IS NOT NULL AND P.code_postal <> ''
    AND E.particulier_id IS NOT NULL
    AND R.entite_id IS NULL;

-- createur

CREATE TABLE IF NOT EXISTS createur (
    equipe_id integer NOT NULL,
    etablissement_creation_id integer NOT NULL,
    personne_id integer NOT NULL,
    fonction varchar(16),

    PRIMARY KEY(etablissement_creation_id, personne_id)
);

-- insertion des créateurs d'établissement dans la table personne

INSERT INTO personne (
    id,
    nom,
    prenom,
    email,
    telephone,
    naissance_date,
    equipe_id,
    created_at
)
SELECT
    P.id,
    P.nom,
    P.prenom,
    P.email,
    P.telephone,
    P.date_de_naissance,
    E.territoire_id,
    P.created_at
FROM old.old_particulier P
JOIN old.old_entite E ON E.particulier_id = P.id
LEFT JOIN old.old_proprietaire R ON R.entite_id = E.id
WHERE
    R.entite_id IS NULL;

INSERT INTO createur (
    equipe_id,
    etablissement_creation_id,
    personne_id,
    fonction
)
SELECT
    E.territoire_id,
    E.particulier_id,
    E.particulier_id,
    'Créateur'
FROM old.old_particulier P
JOIN old.old_entite E ON E.particulier_id = P.id
LEFT JOIN old.old_proprietaire R ON R.entite_id = E.id
JOIN personne PER ON PER.id = E.particulier_id
WHERE
    E.particulier_id IS NOT NULL
    AND R.entite_id IS NULL;

-- equipe local

CREATE TABLE IF NOT EXISTS local (
    id SERIAL NOT NULL,
    nom varchar,
    surface varchar,
    loyer varchar,
    description text,

    local_statut_type_id varchar,
    equipe_id integer NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO local (
    id,
    nom,
    surface,
    loyer,
    description,
    local_statut_type_id,
    equipe_id
)
SELECT
    L.id,
    L.titre,
    L.surface,
    L.loyer,
    L.commentaire,
    L.local_statut,
    L.territoire_id
FROM old.old_local L;

CREATE TABLE IF NOT EXISTS local_adresse (
    local_id integer NOT NULL
) INHERITS (adresse);

WITH villes as (
    SELECT DISTINCT ON (L.nom, L.code_postal, L.code_insee)
    L.nom, L.code_postal, L.code_insee
    FROM source_laposte_code_insee_code_postal L
)
INSERT INTO local_adresse (
    local_id,
    numero,
    voie_nom,
    code_postal,
    geolocalisation,
    commune_id
)
SELECT
    O.id,
    CASE
        WHEN O.adresse ~ '^\d+(bis|ter|quater)?'
        THEN REGEXP_REPLACE(O.adresse, '^(\d+(bis|ter|quater)?)? ?.*', '\1')
        ELSE NULL
    END,
    REGEXP_REPLACE(REGEXP_REPLACE(O.adresse, '\d{5}.*$', ''), '^\d+(bis|ter|quater)? ', ''),
    O.code_postal,
    O.geolocation::point::geometry(Point, 4326),
    L.code_insee
FROM old.old_local O
LEFT JOIN villes L
    ON L.code_postal = O.code_postal
    AND LOWER(L.nom) LIKE
            REPLACE(
                REPLACE(
                    REPLACE(
                        LOWER(unaccent(O.ville)),
                        '-', ' '),
                    '''', ' '),
                'saint', 'st')
WHERE
    O.code_postal IS NOT NULL AND O.code_postal <> '';

-- types de locaux

CREATE TABLE IF NOT EXISTS local__local_type (
    local_id integer NOT NULL,
    local_type_id varchar(20) NOT NULL,

    PRIMARY KEY(local_id, local_type_id)
);

WITH local_local_types AS (
    SELECT id, UNNEST(string_to_array(local_types, ',')) as type
    FROM old.old_local
)
INSERT INTO local__local_type (
    local_id,
    local_type_id
)
SELECT
    id,
    type
FROM local_local_types;

-- etiquette dans local

CREATE TABLE IF NOT EXISTS local__etiquette (
    local_id integer NOT NULL,
    etiquette_id integer NOT NULL,

    PRIMARY KEY(local_id, etiquette_id)
);

WITH local_localisations AS (
    SELECT territoire_id, id as local_id, UNNEST(string_to_array(localisations, ',')) as localisation
    FROM old.old_local
)
INSERT INTO local__etiquette (
    local_id,
    etiquette_id
)
SELECT local_id, EE.id
FROM local_localisations
JOIN etiquette EE
    ON EE.nom = local_localisations.localisation
    AND EE.etiquette_type_id = 'localisation'
    AND EE.equipe_id = territoire_id;

-- proprietaire personne

CREATE TABLE IF NOT EXISTS proprietaire_personne (
    local_id integer NOT NULL,
    personne_id integer NOT NULL,

    PRIMARY KEY(local_id, personne_id)
);

WITH personne as (
    INSERT INTO personne (
        id,
        nom,
        prenom,
        email,
        telephone,
        naissance_date,
        equipe_id,
        created_at
    )
    SELECT
        P.id,
        P.nom,
        P.prenom,
        P.email,
        P.telephone,
        P.date_de_naissance,
        E.territoire_id,
        R.created_at
    FROM old.old_particulier P
    JOIN old.old_entite E ON E.particulier_id = P.id
    JOIN old.old_proprietaire R ON R.entite_id = E.id
    RETURNING id, equipe_id
)
INSERT INTO proprietaire_personne (
    local_id,
    personne_id
)
SELECT
    R.local_id,
    P.id
FROM old.old_proprietaire R
JOIN old.old_entite E ON E.id = R.entite_id
JOIN personne P ON P.id = E.particulier_id;

-- proprietaire etablissement

CREATE TABLE IF NOT EXISTS proprietaire_etablissement (
    local_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,

    PRIMARY KEY(local_id, etablissement_id)
);

INSERT INTO proprietaire_etablissement (
    local_id,
    etablissement_id
)
SELECT
    C.local_id,
    T.entreprise_id
FROM old.old_proprietaire C
JOIN old.old_entite T ON T.id = C.entite_id
WHERE T.entreprise_id IS NOT NULL;

-- evenement

CREATE TABLE IF NOT EXISTS evenement (
    id SERIAL NOT NULL,
    evenement_type_id varchar NOT NULL,
    date timestamptz NOT NULL DEFAULT now(),
    diff jsonb,
    deveco_id integer,
    compte_id integer,
    api_type_id varchar,

    PRIMARY KEY(id)
);

-- mise à jour de l'établissement par l'API Entreprise

CREATE TABLE IF NOT EXISTS evenement__etablissement (
    evenement_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,

    PRIMARY KEY(evenement_id, etablissement_id)
);

ALTER TABLE evenement ADD COLUMN tmp_id varchar;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    api_type_id
  )
  SELECT
    E.siret,
    'modification',
    E.last_api_update,
    'api_entreprise'
  FROM old.old_entreprise E
	INNER JOIN etablissement ON etablissement.id = E.siret -- pour éviter les SIRET qui ne sont pas dans la table etablissement
  WHERE
    E.last_api_update IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__etablissement (etablissement_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- mise à jour de l'entreprise par l'API Entreprise

CREATE TABLE IF NOT EXISTS evenement__entreprise (
    evenement_id integer NOT NULL,
    entreprise_id varchar(14) NOT NULL,

    PRIMARY KEY(evenement_id, entreprise_id)
);

ALTER TABLE evenement ADD COLUMN tmp_id varchar;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    api_type_id
  )
  SELECT
    SUBSTRING(E.siret, 1, 9),
    'modification',
    E.last_api_update,
    'api_entreprise'
  FROM old.old_entreprise E
  WHERE
    E.last_api_update IS NOT NULL
	INNER JOIN entreprise ON entreprise.id = SUBSTRING(E.siret, 1, 9) -- pour éviter les SIRET qui ne sont pas dans la table etablissement
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__entreprise (entreprise_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- echange

CREATE TABLE IF NOT EXISTS evenement__echange (
    evenement_id integer NOT NULL,
    echange_id integer NOT NULL,

    PRIMARY KEY(evenement_id, echange_id)
);

-- echange creation

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    E.id,
    'creation',
    E.created_at,
    createur_id
  FROM old.old_echange E
  JOIN old.old_deveco D ON D.id = E.createur_id
  WHERE
    E.created_at IS NOT NULL
    AND type_echange <> 'transformation'
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__echange (echange_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- echange modification

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    E.id,
    'modification',
    E.updated_at,
    auteur_modification_id
  FROM old.old_echange E
  JOIN old.old_deveco D ON D.id = E.auteur_modification_id
  WHERE
    E.updated_at IS NOT NULL
    AND type_echange <> 'transformation'
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__echange (echange_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- demande

CREATE TABLE IF NOT EXISTS evenement__demande (
    evenement_id integer NOT NULL,
    demande_id integer NOT NULL,

    PRIMARY KEY(evenement_id, demande_id)
);

-- demande creation

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    DISTINCT ON (M.id)
    M.id,
    'creation',
    M.created_at,
    E.createur_id
  FROM old.old_demande M
  JOIN old.old_echange E on E.fiche_id = M.fiche_id
  JOIN old.old_deveco D ON D.id = E.createur_id
  WHERE M.created_at IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__demande (demande_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- demande modification

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    DISTINCT ON (M.id)
    M.id,
    'fermeture',
    M.date_cloture,
    E.createur_id
  FROM old.old_demande M
  JOIN old.old_echange E on E.fiche_id = M.fiche_id
  JOIN old.old_deveco D ON D.id = E.createur_id
  WHERE M.date_cloture IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__demande (demande_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- rappel

CREATE TABLE IF NOT EXISTS evenement__rappel (
    evenement_id integer NOT NULL,
    rappel_id integer NOT NULL,

    PRIMARY KEY(evenement_id, rappel_id)
);

-- rappel creation

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    E.id,
    'creation',
    E.created_at,
    createur_id
  FROM old.old_rappel E
  JOIN old.old_deveco D ON D.id = E.createur_id
  WHERE E.created_at IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__rappel (rappel_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- rappel fermeture

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    E.id,
    'fermeture',
    E.date_cloture,
    auteur_cloture_id
  FROM old.old_rappel E
  JOIN old.old_deveco D ON D.id = E.createur_id
  WHERE E.date_cloture IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__rappel (rappel_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- etiquette

--  aucun évènement à migrer sur les étiquettes

CREATE TABLE IF NOT EXISTS evenement__etiquette (
    evenement_id integer NOT NULL,
    etiquette_id integer NOT NULL,

    PRIMARY KEY(evenement_id, etiquette_id)
);

-- etiquette etablissement

CREATE TABLE IF NOT EXISTS evenement__etiquette__etablissement (
    evenement_id integer NOT NULL,
    etiquette_id integer NOT NULL,
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,

    PRIMARY KEY(evenement_id, etiquette_id, etablissement_id)
);

-- pas d'insertion dans evenement__etiquette__etablissement
-- car impossible de déterminer la date à laquelle une étiquette a été reliée à un établissement

CREATE TABLE IF NOT EXISTS evenement__etiquette__etablissement_creation (
    evenement_id integer NOT NULL,
    etiquette_id integer NOT NULL,
    etablissement_creation_id integer NOT NULL,

    PRIMARY KEY(evenement_id, etiquette_id, etablissement_creation_id)
);

-- pas d'insertion dans evenement__etiquette__etablissement_creation
-- car impossible de déterminer la date à laquelle une étiquette a été reliée à une création d'établissement

-- contact

CREATE TABLE IF NOT EXISTS evenement__contact (
    evenement_id integer NOT NULL,
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    personne_id integer NOT NULL,

    PRIMARY KEY(evenement_id, equipe_id, etablissement_id, personne_id)
);

-- contact creation

ALTER TABLE evenement ADD COLUMN tmp_equipe_id integer;
ALTER TABLE evenement ADD COLUMN tmp_etablissement_id varchar(14);
ALTER TABLE evenement ADD COLUMN tmp_personne_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_equipe_id,
    tmp_etablissement_id,
    tmp_personne_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    T.territoire_id,
    T.entreprise_id,
    C.particulier_id,
    'creation',
    C.created_at,
    T.deveco_id
  FROM old.old_contact C
  JOIN old.old_entite T ON T.id = C.entite_id
  JOIN old.old_deveco D ON D.id = T.deveco_id
  WHERE
    C.created_at IS NOT NULL
    AND T.entreprise_id IS NOT NULL
    AND (C.source IS NULL OR C.source = 'import')
  RETURNING id AS evenement_id, tmp_equipe_id, tmp_etablissement_id, tmp_personne_id
)
INSERT INTO evenement__contact (equipe_id, etablissement_id, personne_id, evenement_id)
SELECT tmp_equipe_id, tmp_etablissement_id, tmp_personne_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_equipe_id;
ALTER TABLE evenement DROP COLUMN tmp_etablissement_id;
ALTER TABLE evenement DROP COLUMN tmp_personne_id;

-- etablissement_creation

CREATE TABLE IF NOT EXISTS evenement__etablissement_creation (
    evenement_id integer NOT NULL,
    etablissement_creation_id integer NOT NULL,

    PRIMARY KEY(evenement_id, etablissement_creation_id)
);

-- etablissement_creation creation

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    E.particulier_id,
    'creation',
    E.created_at,
    E.deveco_id
  FROM old.old_entite E
  LEFT JOIN old.old_proprietaire P ON P.entite_id = E.id
  JOIN old.old_deveco D ON D.id = E.deveco_id
  WHERE E.created_at IS NOT NULL
    AND P.id is null
    AND E.particulier_id IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__etablissement_creation (etablissement_creation_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- etablissement_creation modification

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    E.particulier_id,
    'modification',
    E.updated_at,
    E.auteur_modification_id
  FROM old.old_entite E
  LEFT JOIN old.old_proprietaire P ON P.entite_id = E.id
  JOIN old.old_deveco D ON D.id = E.auteur_modification_id
  WHERE E.updated_at IS NOT NULL
    AND P.id is null
    AND E.particulier_id IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__etablissement_creation (etablissement_creation_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- etablissement_creation affichage

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    T.particulier_id,
    'affichage',
    E.date,
    E.deveco_id
  FROM old.old_event_log E
  JOIN old.old_entite T ON T.id = E.entity_id
  JOIN old.old_deveco D ON D.id = E.deveco_id
  WHERE
    entity_type = 'FICHE_PP'
    AND action = 'VIEW'
    AND T.particulier_id IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__etablissement_creation (etablissement_creation_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- etablissement_creation lien avec etablissement (transformation)

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    T.particulier_id,
    'transformation',
    E.date,
    E.deveco_id
  FROM old.old_event_log E
  JOIN old.old_entite T ON T.id = E.entity_id
  JOIN old.old_deveco D ON D.id = E.deveco_id
  WHERE
    entity_type = 'FICHE_PP'
    AND action = 'CREATE_ENTREPRISE'
    AND T.particulier_id IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__etablissement_creation (etablissement_creation_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- etablissement

CREATE TABLE IF NOT EXISTS evenement__equipe_etablissement (
    evenement_id integer NOT NULL,
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,

    PRIMARY KEY(evenement_id, equipe_id, etablissement_id)
);

-- etablissement affichage

ALTER TABLE evenement ADD COLUMN tmp_equipe_id integer;
ALTER TABLE evenement ADD COLUMN tmp_etablissement_id varchar(14);

WITH E AS (
  INSERT INTO evenement (
    tmp_equipe_id,
    tmp_etablissement_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    T.territoire_id,
    T.entreprise_id,
    'affichage',
    E.date,
    E.deveco_id
  FROM old.old_event_log E
  JOIN old.old_entite T ON T.id = E.entity_id
  JOIN old.old_deveco D ON D.id = E.deveco_id
  WHERE
    entity_type = 'FICHE_PM'
    AND action = 'VIEW'
    AND T.entreprise_id IS NOT NULL
  RETURNING id AS evenement_id, tmp_equipe_id, tmp_etablissement_id
)
INSERT INTO evenement__equipe_etablissement (equipe_id, etablissement_id, evenement_id)
SELECT tmp_equipe_id, tmp_etablissement_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_equipe_id;
ALTER TABLE evenement DROP COLUMN tmp_etablissement_id;

-- local

CREATE TABLE IF NOT EXISTS evenement__local (
    evenement_id integer NOT NULL,
    local_id integer NOT NULL,

    PRIMARY KEY(evenement_id, local_id)
);

-- local creation

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    L.id,
    'creation',
    L.created_at,
    L.deveco_id
  FROM old.old_local L
  JOIN old.old_deveco D ON D.id = L.deveco_id
  WHERE L.created_at IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__local (local_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- local modification

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    L.id,
    'modification',
    L.updated_at,
    auteur_modification_id
  FROM old.old_local L
  JOIN old.old_deveco D ON D.id = L.auteur_modification_id
  WHERE L.updated_at IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__local (local_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- local affichage

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    E.entity_id,
    'affichage',
    E.date,
    E.deveco_id
  FROM old.old_event_log E
  JOIN old.old_deveco D ON D.id = E.deveco_id
  WHERE
    entity_type = 'LOCAL'
    AND action = 'VIEW'
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__local (local_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- local proprietaire personne

CREATE TABLE IF NOT EXISTS evenement__proprietaire_personne (
    evenement_id integer NOT NULL,
    local_id integer NOT NULL,
    personne_id integer NOT NULL,

    PRIMARY KEY(evenement_id, local_id, personne_id)
);

-- local proprietaire personne creation

ALTER TABLE evenement ADD COLUMN tmp_local_id integer;
ALTER TABLE evenement ADD COLUMN tmp_personne_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_local_id,
    tmp_personne_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    P.local_id,
    T.particulier_id,
    'creation',
    P.created_at,
    P.deveco_id
  FROM old.old_proprietaire P
  JOIN old.old_entite T ON T.id = P.entite_id
  WHERE
    P.created_at IS NOT NULL
    AND T.particulier_id IS NOT NULL
  RETURNING id AS evenement_id, tmp_local_id, tmp_personne_id
)
INSERT INTO evenement__proprietaire_personne (local_id, personne_id, evenement_id)
SELECT tmp_local_id, tmp_personne_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_local_id;
ALTER TABLE evenement DROP COLUMN tmp_personne_id;

-- local proprietaire etablissement

CREATE TABLE IF NOT EXISTS evenement__proprietaire_etablissement (
    evenement_id integer NOT NULL,
    local_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,

    PRIMARY KEY(evenement_id, local_id, etablissement_id)
);

-- local proprietaire etablissement creation

ALTER TABLE evenement ADD COLUMN tmp_local_id integer;
ALTER TABLE evenement ADD COLUMN tmp_etablissement_id varchar(14);

WITH E AS (
  INSERT INTO evenement (
    tmp_local_id,
    tmp_etablissement_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    P.local_id,
    T.entreprise_id,
    'creation',
    P.created_at,
    P.deveco_id
  FROM old.old_proprietaire P
  JOIN old.old_entite T ON T.id = P.entite_id
  WHERE
    P.created_at IS NOT NULL
    AND T.entreprise_id IS NOT NULL
  RETURNING id AS evenement_id, tmp_local_id, tmp_etablissement_id
)
INSERT INTO evenement__proprietaire_etablissement (local_id, etablissement_id, evenement_id)
SELECT tmp_local_id, tmp_etablissement_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_local_id;
ALTER TABLE evenement DROP COLUMN tmp_etablissement_id;

-- personne

CREATE TABLE IF NOT EXISTS evenement__personne (
    evenement_id integer NOT NULL,
    personne_id integer NOT NULL,

    PRIMARY KEY(evenement_id, personne_id)
);

-- personne creation (contact, proprietaire, createur)

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    P.id,
    'creation',
    P.created_at,
    E.deveco_id
  FROM old.old_particulier P
  LEFT JOIN old.old_contact C ON C.particulier_id = P.id
  LEFT JOIN old.old_entite E ON E.particulier_id = P.id OR E.id = C.entite_id
  JOIN old.old_deveco D ON D.id = E.deveco_id
  WHERE
    E.created_at IS NOT NULL
    AND (C.source IS NULL OR C.source = 'import')
    AND (C.fonction IS NULL OR C.fonction <> 'Créateur')
  ON CONFLICT DO NOTHING
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__personne (personne_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- pourquoi en a-t-on le double ?

-- compte

CREATE TABLE IF NOT EXISTS evenement__compte (
    evenement_id integer NOT NULL,
    compte_id integer NOT NULL,

    PRIMARY KEY(evenement_id, compte_id)
);

-- compte creation

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    A.id,
    'creation',
    D.created_at,
    D.id
  FROM old.old_account A
  JOIN old.old_deveco D ON D.account_id = A.id
  WHERE D.created_at IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__compte (compte_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- compte modification

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    A.id,
    'modification',
    D.updated_at,
    D.id
  FROM old.old_account A
  JOIN old.old_deveco D ON D.account_id = A.id
  WHERE D.updated_at IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__compte (compte_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- compte connexion

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    A.id,
    'connexion',
    A.last_login,
    D.id
  FROM old.old_account A
  JOIN old.old_deveco D ON D.account_id = A.id
  WHERE A.last_login IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__compte (compte_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- compte authentification

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    A.id,
    'authentification',
    A.last_auth,
    D.id
  FROM old.old_account A
  JOIN old.old_deveco D ON D.account_id = A.id
  WHERE A.last_auth IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__compte (compte_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- evenements, mise à jour de la colonne compte_id à partir de deveco_id

UPDATE evenement e
SET compte_id = d.compte_id
FROM deveco d
WHERE e.deveco_id = d.id;

ALTER TABLE evenement DROP COLUMN deveco_id;

--

CREATE TABLE IF NOT EXISTS stats (
    id SERIAL NOT NULL,
    commune_id character varying NOT NULL,
    date date NOT NULL,
    content jsonb NOT NULL,

    PRIMARY KEY (commune_id, date)
);

-- --------------------------------------------------------------
--
-- Archivage des anciennes tables
--
-- --------------------------------------------------------------

CREATE SCHEMA source;

ALTER TABLE source_datagouv_commune_ept SET SCHEMA source;
ALTER TABLE source_datagouv_qpv SET SCHEMA source;
ALTER TABLE source_datagouv_zrr SET SCHEMA source;
ALTER TABLE source_geoloc_entreprise SET SCHEMA source;
ALTER TABLE source_insee_commune SET SCHEMA source;
ALTER TABLE source_insee_commune_historique SET SCHEMA source;
ALTER TABLE source_insee_commune_mouvement SET SCHEMA source;
ALTER TABLE source_insee_departement SET SCHEMA source;
ALTER TABLE source_insee_epci SET SCHEMA source;
ALTER TABLE source_insee_epci_commune SET SCHEMA source;
ALTER TABLE source_insee_region SET SCHEMA source;
ALTER TABLE source_laposte_code_insee_code_postal SET SCHEMA source;
ALTER TABLE source_odt_epci_petr SET SCHEMA source;
ALTER TABLE source_sirene_lien_succession SET SCHEMA source;
ALTER TABLE source_sirene_lien_succession_temp SET SCHEMA source;
ALTER TABLE source_api_entreprise_entreprise_mandataire_social SET SCHEMA source;
ALTER TABLE source_api_entreprise_etablissement_exercice SET SCHEMA source;
ALTER TABLE source_api_sirene_etablissement SET SCHEMA source;
ALTER TABLE source_api_sirene_unite_legale SET SCHEMA source;
ALTER TABLE source_api_sirene_etablissement_periode SET SCHEMA source;
ALTER TABLE source_api_sirene_unite_legale_periode SET SCHEMA source;


ALTER TYPE source_sirene_caractere_employeur_enum SET SCHEMA source;
ALTER TYPE source_sirene_etablissement_etat_administratif_enum SET SCHEMA source;
ALTER TYPE source_sirene_etablissement_statut_diffusion_enum SET SCHEMA source;
ALTER TYPE source_sirene_unite_legale_categorie_entreprise_enum SET SCHEMA source;
ALTER TYPE source_sirene_unite_legale_economie_sociale_solidaire_enum SET SCHEMA source;
ALTER TYPE source_sirene_unite_legale_etat_administratif_enum SET SCHEMA source;
ALTER TYPE source_sirene_unite_legale_sexe_enum SET SCHEMA source;
ALTER TYPE source_sirene_unite_legale_societe_mission_enum SET SCHEMA source;
ALTER TYPE source_sirene_unite_legale_statut_diffusion_enum SET SCHEMA source;

-- --------------------------------------------------------------
--
-- Optimisation
--
-- --------------------------------------------------------------

--  Indexes

CREATE INDEX IF NOT EXISTS IDX_contact_equipe ON contact (equipe_id);
CREATE INDEX IF NOT EXISTS IDX_contact_etablissement ON contact (etablissement_id);
CREATE INDEX IF NOT EXISTS IDX_contact_personne ON contact (personne_id);

CREATE INDEX IF NOT EXISTS IDX_etablissement__commune ON etablissement (commune_id);
CREATE INDEX IF NOT EXISTS IDX_etablissement__etablissement_commune ON etablissement (id, commune_id);
CREATE INDEX IF NOT EXISTS IDX_etablissement__creation_date ON etablissement (creation_date);
CREATE INDEX IF NOT EXISTS IDX_etablissement__entreprise ON etablissement (entreprise_id);

CREATE INDEX IF NOT EXISTS IDX_etablissement_adresse_1__etablissement ON etablissement_adresse_1 (etablissement_id);

--

CREATE INDEX IF NOT EXISTS IDX_etablissement_periode__etablissement ON etablissement_periode (etablissement_id);
CREATE INDEX IF NOT EXISTS IDX_etablissement_periode__fin_date ON etablissement_periode (fin_date);
CREATE INDEX IF NOT EXISTS IDX_etablissement_periode__courante_fin_date ON etablissement_periode ((fin_date IS NULL));
CREATE INDEX IF NOT EXISTS IDX_etablissement_periode__actif ON etablissement_periode (actif);
CREATE INDEX IF NOT EXISTS IDX_etablissement_periode__naf_type ON etablissement_periode (naf_type_id);

--

CREATE INDEX IF NOT EXISTS IDX_entreprise_periode__entreprise ON entreprise_periode (entreprise_id);
CREATE INDEX IF NOT EXISTS IDX_entreprise_periode__courante_fin_date ON entreprise_periode ((fin_date IS NULL));
CREATE INDEX IF NOT EXISTS IDX_entreprise_periode__categorie_juridique ON entreprise_periode (categorie_juridique_type_id);
CREATE INDEX IF NOT EXISTS IDX_entreprise_periode__fin_date_partial ON entreprise_periode (entreprise_id) where fin_date IS NULL;

--

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement_exogene__etablissement ON equipe__etablissement_exogene(etablissement_id);
CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement_exogene__equipe ON equipe__etablissement_exogene(equipe_id);

--

CREATE INDEX IF NOT EXISTS IDX_adresse__geolocalisation ON adresse USING GiST (geolocalisation);
CREATE INDEX IF NOT EXISTS IDX_zonage__geometrie ON zonage USING GiST (geometrie);
CREATE INDEX IF NOT EXISTS IDX_qpv__geometrie ON qpv USING GiST (geometrie);
CREATE INDEX IF NOT EXISTS IDX_personne__nom ON personne USING gin (nom gin_trgm_ops);

--

CREATE INDEX IF NOT EXISTS IDX_source_api_sirene_unite_legale ON source.source_api_sirene_unite_legale (mise_a_jour_at);
CREATE INDEX IF NOT EXISTS IDX_source_api_sirene_etablissement ON source.source_api_sirene_etablissement (mise_a_jour_at);
CREATE INDEX IF NOT EXISTS IDX_source_api_sirene_etablissement_periode ON source.source_api_sirene_etablissement_periode (mise_a_jour_at);

-- CREATE INDEX IDX_personne__nom ON personne USING gin ((ts_vector('french', nom)));
-- Création d'une vue pour faire le lien entre les équipes et les communes

-- --------------------------------------------------------------
--
-- Ajout des contraintes
--
-----------------------------------------------------------------

ALTER TABLE tache ADD CONSTRAINT uk_tache UNIQUE (nom, debut_at);
ALTER TABLE naf_type ADD CONSTRAINT fk_naf_type__naf_revision_type FOREIGN KEY (naf_revision_type_id) REFERENCES naf_revision_type(id);
ALTER TABLE naf_type ADD CONSTRAINT fk_naf_type__parent_naf_type FOREIGN KEY (parent_naf_type_id, naf_revision_type_id) REFERENCES naf_type(id, naf_revision_type_id);
ALTER TABLE categorie_juridique_type ADD CONSTRAINT fk_categorie_juridique_type__parent_categorie_juridique_type FOREIGN KEY (parent_categorie_juridique_type_id) REFERENCES categorie_juridique_type(id);
ALTER TABLE departement ADD CONSTRAINT fk_departement__region FOREIGN KEY (region_id) REFERENCES region(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE epci ADD CONSTRAINT fk_epci__epci_type FOREIGN KEY (epci_type_id) REFERENCES epci_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE epci ADD CONSTRAINT fk_epci__petr FOREIGN KEY (petr_id) REFERENCES petr(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE commune ADD CONSTRAINT fk_commune__commune_type FOREIGN KEY (commune_type_id) REFERENCES commune_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE commune ADD CONSTRAINT fk_commune__metropole FOREIGN KEY (metropole_id) REFERENCES metropole(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE commune ADD CONSTRAINT fk_commune__epci FOREIGN KEY (epci_id) REFERENCES epci(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE commune ADD CONSTRAINT fk_commune__petr FOREIGN KEY (petr_id) REFERENCES petr(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE commune ADD CONSTRAINT fk_commune__departement FOREIGN KEY (departement_id) REFERENCES departement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE commune ADD CONSTRAINT fk_commune__region FOREIGN KEY (region_id) REFERENCES region(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE commune ADD CONSTRAINT fk_commune__zrr_type FOREIGN KEY (zrr_type_id) REFERENCES zrr_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE adresse ADD CONSTRAINT fk_adresse__commune FOREIGN KEY (commune_id) REFERENCES commune(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE entreprise ADD CONSTRAINT fk_entreprise__entreprise_type FOREIGN KEY (entreprise_type_id) REFERENCES entreprise_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE entreprise ADD CONSTRAINT fk_entreprise__categorie_entreprise_type FOREIGN KEY (categorie_entreprise_type_id) REFERENCES categorie_entreprise_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE personne_physique ADD CONSTRAINT fk_personne_physique__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE personne_physique ADD CONSTRAINT fk_personne_physique__sexe_type FOREIGN KEY (sexe_type_id) REFERENCES sexe_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE personne_morale ADD CONSTRAINT fk_personne_morale__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE entreprise_periode ADD CONSTRAINT fk_entreprise_periode__categorie_juridique_type FOREIGN KEY (categorie_juridique_type_id) REFERENCES categorie_juridique_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE entreprise_periode ADD CONSTRAINT fk_entreprise_periode__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE mandataire_personne_physique ADD CONSTRAINT fk_mandataire_personne_physique__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise(id);
ALTER TABLE mandataire_personne_morale ADD CONSTRAINT fk_mandataire_personne_morale__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise(id);
ALTER TABLE mandataire_personne_morale ADD CONSTRAINT fk_mandataire_personne_morale__mandataire_entreprise FOREIGN KEY (mandataire_entreprise_id) REFERENCES entreprise(id);
ALTER TABLE etablissement_adresse_1 ADD CONSTRAINT fk_etablissement_adresse_1__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_periode ADD CONSTRAINT uk_etablissement_periode UNIQUE (etablissement_id, debut_date);
ALTER TABLE etablissement_periode ADD CONSTRAINT fk_etablissement_periode__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_exercice ADD CONSTRAINT uk_etablissement_exercice UNIQUE (etablissement_id, cloture_annee);
ALTER TABLE etablissement_exercice ADD CONSTRAINT fk_etablissement_exercice__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe ADD CONSTRAINT fk_equipe__territoire_type FOREIGN KEY (territoire_type_id) REFERENCES territoire_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe ADD CONSTRAINT fk_equipe__equipe_type FOREIGN KEY (equipe_type_id) REFERENCES equipe_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE deveco ADD CONSTRAINT fk_deveco__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE deveco ADD CONSTRAINT fk_deveco__compte FOREIGN KEY (compte_id) REFERENCES compte(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_favori ADD CONSTRAINT fk_etablissement_favori__deveco FOREIGN KEY (deveco_id) REFERENCES deveco(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_favori ADD CONSTRAINT fk_etablissement_favori__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE zonage ADD CONSTRAINT fk_zonage__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__commune ADD CONSTRAINT fk_equipe__commune__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__commune ADD CONSTRAINT fk_equipe__commune__commune FOREIGN KEY (commune_id) REFERENCES commune(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__epci ADD CONSTRAINT fk_equipe__epci__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__epci ADD CONSTRAINT fk_equipe__epci__epci FOREIGN KEY (epci_id) REFERENCES epci(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__metropole ADD CONSTRAINT fk_equipe__metropole__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__metropole ADD CONSTRAINT fk_equipe__metropole__metropole FOREIGN KEY (metropole_id) REFERENCES metropole(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__petr ADD CONSTRAINT fk_equipe__petr__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__petr ADD CONSTRAINT fk_equipe__petr__petr FOREIGN KEY (petr_id) REFERENCES petr(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__departement ADD CONSTRAINT fk_equipe__departement__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__departement ADD CONSTRAINT fk_equipe__departement__departement FOREIGN KEY (departement_id) REFERENCES departement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__region ADD CONSTRAINT fk_equipe__region__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__region ADD CONSTRAINT fk_equipe__region__region FOREIGN KEY (region_id) REFERENCES region(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe_import ADD CONSTRAINT uk_equipe_import_equipe UNIQUE (equipe_id);
ALTER TABLE equipe_import ADD CONSTRAINT fk_equipe_import__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etiquette ADD CONSTRAINT fk_etiquette__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etiquette ADD CONSTRAINT fk_etiquette__etiquette_type FOREIGN KEY (etiquette_type_id) REFERENCES etiquette_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etiquette ADD CONSTRAINT uk_etiquette UNIQUE (etiquette_type_id, nom, equipe_id);
ALTER TABLE echange ADD CONSTRAINT fk_echange__echange_type FOREIGN KEY (echange_type_id) REFERENCES echange_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE echange ADD CONSTRAINT fk_echange__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE echange ADD CONSTRAINT fk_echange__deveco FOREIGN KEY (deveco_id) REFERENCES deveco(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE demande ADD CONSTRAINT fk_demande__demande_type FOREIGN KEY (demande_type_id) REFERENCES demande_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE demande ADD CONSTRAINT fk_demande__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE echange__demande ADD CONSTRAINT fk_echange__demande__echange FOREIGN KEY (echange_id) REFERENCES echange(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE echange__demande ADD CONSTRAINT fk_echange__demande__demande FOREIGN KEY (demande_id) REFERENCES demande(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE rappel ADD CONSTRAINT fk_rappel__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE personne ADD CONSTRAINT fk_personne__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__etiquette ADD CONSTRAINT fk_equipe__etablissement__etiquette__etiquette FOREIGN KEY (etiquette_id) REFERENCES etiquette(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__etiquette ADD CONSTRAINT fk_equipe__etablissement__etiquette__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__etiquette ADD CONSTRAINT fk_equipe__etablissement__etiquette__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__echange ADD CONSTRAINT fk_equipe__etablissement__echange__echange FOREIGN KEY (echange_id) REFERENCES echange(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__echange ADD CONSTRAINT fk_equipe__etablissement__echange__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__echange ADD CONSTRAINT fk_equipe__etablissement__echange__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__demande ADD CONSTRAINT fk_equipe__etablissement__demande__demande FOREIGN KEY (demande_id) REFERENCES demande(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__demande ADD CONSTRAINT fk_equipe__etablissement__demande__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__demande ADD CONSTRAINT fk_equipe__etablissement__demande__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__rappel ADD CONSTRAINT fk_equipe__etablissement__rappel__rappel FOREIGN KEY (rappel_id) REFERENCES rappel(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__rappel ADD CONSTRAINT fk_equipe__etablissement__rappel__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__rappel ADD CONSTRAINT fk_equipe__etablissement__rappel__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE contact ADD CONSTRAINT fk_contact__personne FOREIGN KEY (personne_id) REFERENCES personne(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE contact ADD CONSTRAINT fk_contact__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE contact ADD CONSTRAINT fk_contact__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation ADD CONSTRAINT fk_etablissement_creation__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation ADD CONSTRAINT fk_etablissement_creation__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__etiquette ADD CONSTRAINT fk_etablissement_creation__etiquette__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__etiquette ADD CONSTRAINT fk_etablissement_creation__etiquette__etiquette FOREIGN KEY (etiquette_id) REFERENCES etiquette(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__echange ADD CONSTRAINT fk_etablissement_creation__echange__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__echange ADD CONSTRAINT fk_etablissement_creation__echange__echange FOREIGN KEY (echange_id) REFERENCES echange(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation_transformation ADD CONSTRAINT fk_etablissement_creation_transfo__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation_transformation ADD CONSTRAINT fk_etablissement_creation_transfo__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation_transformation ADD CONSTRAINT fk_etablissement_creation_transfo__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__demande ADD CONSTRAINT fk_etablissement_creation__demande__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__demande ADD CONSTRAINT fk_etablissement_creation__demande__demande FOREIGN KEY (demande_id) REFERENCES demande(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE createur ADD CONSTRAINT fk_createur__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE createur ADD CONSTRAINT fk_createur__personne FOREIGN KEY (personne_id) REFERENCES personne(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__rappel ADD CONSTRAINT fk_etablissement_creation__rappel__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__rappel ADD CONSTRAINT fk_etablissement_creation__rappel__rappel FOREIGN KEY (rappel_id) REFERENCES rappel(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE personne_adresse ADD CONSTRAINT fk_personne_adresse__personne FOREIGN KEY (personne_id) REFERENCES personne(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE local ADD CONSTRAINT fk_local__local_statut_type FOREIGN KEY (local_statut_type_id) REFERENCES local_statut_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE local ADD CONSTRAINT fk_local__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE local__local_type ADD CONSTRAINT fk_local__local_type__local FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE local__local_type ADD CONSTRAINT fk_local__local_type__local_type FOREIGN KEY (local_type_id) REFERENCES local_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE local__etiquette ADD CONSTRAINT fk_local__etiquette__local FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE local__etiquette ADD CONSTRAINT fk_local__etiquette__etiquette FOREIGN KEY (etiquette_id) REFERENCES etiquette(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE local_adresse ADD CONSTRAINT fk_local_adresse__local  FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE proprietaire_personne ADD CONSTRAINT fk_proprietaire_personne__local FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE proprietaire_personne ADD CONSTRAINT fk_proprietaire_personne__personne FOREIGN KEY (personne_id) REFERENCES personne(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE proprietaire_etablissement ADD CONSTRAINT fk_proprietaire_etablissement__local FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE proprietaire_etablissement ADD CONSTRAINT fk_proprietaire_etablissement__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement ADD CONSTRAINT fk_evenement__evenement_type FOREIGN KEY (evenement_type_id) REFERENCES evenement_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement ADD CONSTRAINT fk_evenement__compte FOREIGN KEY (compte_id) REFERENCES compte(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__local ADD CONSTRAINT fk_evenement__local__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__local ADD CONSTRAINT fk_evenement__local__local FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__personne ADD CONSTRAINT fk_evenement__personne__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__personne ADD CONSTRAINT fk_evenement__personne__personne FOREIGN KEY (personne_id) REFERENCES personne(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etablissement ADD CONSTRAINT fk_evenement__etablissement__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etablissement ADD CONSTRAINT fk_evenement__etablissement__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__entreprise ADD CONSTRAINT fk_evenement__entreprise__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__entreprise ADD CONSTRAINT fk_evenement__entreprise__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__echange ADD CONSTRAINT fk_evenement__echange__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__echange ADD CONSTRAINT fk_evenement__echange__echange FOREIGN KEY (echange_id) REFERENCES echange(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__demande ADD CONSTRAINT fk_evenement__demande__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__demande ADD CONSTRAINT fk_evenement__demande__demande FOREIGN KEY (demande_id) REFERENCES demande(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__rappel ADD CONSTRAINT fk_evenement__rappel__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__rappel ADD CONSTRAINT fk_evenement__rappel__rappel FOREIGN KEY (rappel_id) REFERENCES rappel(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette ADD CONSTRAINT fk_evenement__etiquette__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette ADD CONSTRAINT fk_evenement__etiquette__etiquette FOREIGN KEY (etiquette_id) REFERENCES etiquette(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette__etablissement ADD CONSTRAINT fk_evenement__etiquette__etablissement__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette__etablissement ADD CONSTRAINT fk_evenement__etiquette__etablissement__etiquette FOREIGN KEY (etiquette_id) REFERENCES etiquette(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette__etablissement ADD CONSTRAINT fk_evenement__etiquette__etablissement__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette__etablissement ADD CONSTRAINT fk_evenement__etiquette__etablissement__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette__etablissement_creation ADD CONSTRAINT fk_evenement__etiquette__etablissement_creation__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette__etablissement_creation ADD CONSTRAINT fk_evenement__etiquette__etablissement_creation__etiquette FOREIGN KEY (etiquette_id) REFERENCES etiquette(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette__etablissement_creation ADD CONSTRAINT fk_evenement__etiquette__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__contact ADD CONSTRAINT fk_evenement__contact__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__contact ADD CONSTRAINT fk_evenement__contact__contact FOREIGN KEY (equipe_id, etablissement_id, personne_id) REFERENCES contact(equipe_id, etablissement_id, personne_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etablissement_creation ADD CONSTRAINT fk_evenement__etablissement_creation__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etablissement_creation ADD CONSTRAINT fk_evenement__etablissement_creation__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__equipe_etablissement ADD CONSTRAINT fk_evenement__equipe_etablissement__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__equipe_etablissement ADD CONSTRAINT fk_evenement__equipe_etablissement__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__equipe_etablissement ADD CONSTRAINT fk_evenement__equipe_etablissement__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__proprietaire_personne ADD CONSTRAINT fk_evenement__proprietaire_personne__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__proprietaire_personne ADD CONSTRAINT fk_evenement__proprietaire_personne__local FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__proprietaire_personne ADD CONSTRAINT fk_evenement__proprietaire_personne__personne FOREIGN KEY (personne_id) REFERENCES personne(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__proprietaire_etablissement ADD CONSTRAINT fk_evenement__proprietaire_etablissement__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__proprietaire_etablissement ADD CONSTRAINT fk_evenement__proprietaire_etablissement__local FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__proprietaire_etablissement ADD CONSTRAINT fk_evenement__proprietaire_etablissement__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__compte ADD CONSTRAINT fk_evenement__compte__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__compte ADD CONSTRAINT fk_evenement__compte__compte FOREIGN KEY (compte_id) REFERENCES compte(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE stats ADD CONSTRAINT uk_stats__commune_date UNIQUE (commune_id, date);
ALTER TABLE recherche ADD CONSTRAINT fk_recherche__deveco FOREIGN KEY (deveco_id) REFERENCES deveco(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE geo_token ADD CONSTRAINT fk_geo_token__deveco FOREIGN KEY (deveco_id) REFERENCES deveco(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_adresse_1 ADD CONSTRAINT uk_etablissement_adresse_1__etablissement_id UNIQUE (etablissement_id);
ALTER TABLE local_adresse ADD CONSTRAINT uk_local_adresse__etablissement_id UNIQUE (local_id);
ALTER TABLE personne_adresse ADD CONSTRAINT uk_personne_adresse__personne_id UNIQUE (personne_id);

-- equipe__commune_vue

CREATE MATERIALIZED VIEW equipe__commune_vue AS
SELECT
    e.id AS equipe_id,
    c.id AS commune_id
FROM equipe e
LEFT JOIN equipe__commune ec ON ec.equipe_id = e.id
LEFT JOIN equipe__metropole em ON em.equipe_id = e.id
LEFT JOIN equipe__epci ee ON ee.equipe_id = e.id
LEFT JOIN equipe__petr ep ON ep.equipe_id = e.id
LEFT JOIN equipe__departement ed ON ed.equipe_id = e.id
LEFT JOIN equipe__region er ON er.equipe_id = e.id
LEFT JOIN commune c ON c.id = ec.commune_id
    OR c.metropole_id = em.metropole_id
    OR c.epci_id = ee.epci_id
    OR c.petr_id = ep.petr_id
    OR c.departement_id = ed.departement_id
    OR c.region_id = er.region_id;

CREATE INDEX IDX_equipe__commune_vue__commune ON equipe__commune_vue (commune_id);
CREATE INDEX IDX_equipe__commune_vue__equipe ON equipe__commune_vue (equipe_id);
-- cluster
CREATE INDEX IDX_equipe__commune_vue__equipe__commune ON equipe__commune_vue (equipe_id, commune_id);

ALTER TABLE equipe__commune_vue CLUSTER ON IDX_equipe__commune_vue__equipe;
CLUSTER equipe__commune_vue USING IDX_equipe__commune_vue__equipe;
-- Création d'une table partitionnée pour optimiser la recherche d'étblissement

CREATE TABLE IF NOT EXISTS equipe__etablissement (
    equipe_id integer,
    etablissement_id varchar(14),
    exogene boolean DEFAULT false NOT NULL,
    entreprise_id varchar(9),
    commune_id varchar(5),
    creation_date date,
    fermeture_date date,
    effectif_type_id varchar(2),
    categorie_juridique_3_type_id varchar(4) NOT NULL,
    categorie_juridique_2_type_id varchar(2) NOT NULL,
    entreprise_type_id varchar(1) NOT NULL,
    categorie_entreprise_type_id varchar(3),
    economie_sociale_solidaire boolean DEFAULT false NOT NULL,
    employeur boolean DEFAULT false NOT NULL,
    actif boolean,
    naf_type_id varchar(6),
    geolocalisation geometry(Point, 4326),
    code_postal varchar(5),
    numero varchar,
    voie_nom varchar,
    qpv_id varchar(8),
    zrr_type_id varchar(1) DEFAULT 'N'::varchar(1) NOT NULL,
    etablissement_noms varchar,
    contact_noms varchar,

    PRIMARY KEY (equipe_id, etablissement_id)
) PARTITION BY LIST (equipe_id);

-- Insertion du contenu d'une partition pour une équipe

CREATE OR REPLACE PROCEDURE equipe__etablissement_create (eid integer)
AS $$
BEGIN
    EXECUTE format(
        'CREATE TABLE IF NOT EXISTS %1$I PARTITION OF equipe__etablissement FOR VALUES IN (%2$s);',
        'equipe__etablissement_' || eid,
        eid
    );

    INSERT INTO equipe__etablissement (
        equipe_id,
        etablissement_id,
        exogene,
        entreprise_id,
        commune_id,
        creation_date,
        fermeture_date,
        effectif_type_id,
        categorie_juridique_3_type_id,
        categorie_juridique_2_type_id,
        entreprise_type_id,
        categorie_entreprise_type_id,
        economie_sociale_solidaire,
        employeur,
        actif,
        naf_type_id,
        geolocalisation,
        code_postal,
        numero,
        voie_nom,
        qpv_id,
        zrr_type_id,
        etablissement_noms,
        contact_noms
    )
    SELECT
        equipe__commune_vue.equipe_id,
        etablissement.id,
        FALSE,
        etablissement.entreprise_id,
        etablissement.commune_id,
        etablissement.creation_date,
        CASE
            WHEN etablissement_periode.actif IS NULL
            THEN etablissement_periode.fin_date
            ELSE NULL
        END,
        etablissement.effectif_type_id,
        entreprise_periode.categorie_juridique_type_id,
        categorie_juridique_type.parent_categorie_juridique_type_id,
        entreprise.entreprise_type_id,
        entreprise.categorie_entreprise_type_id,
        entreprise_periode.economie_sociale_solidaire,
        etablissement_periode.employeur,
        etablissement_periode.actif,
        etablissement_periode.naf_type_id,
        etablissement_adresse_1.geolocalisation,
        etablissement_adresse_1.code_postal,
        etablissement_adresse_1.numero,
        etablissement_adresse_1.voie_nom,
        qpv.id,
        commune.zrr_type_id,
        CONCAT_WS(
            ' ',
            LOWER(personne_physique.prenom_1),
            LOWER(personne_physique.prenom_2),
            LOWER(personne_physique.prenom_3),
            LOWER(personne_physique.prenom_4),
            LOWER(personne_physique.prenom_usuel),
            LOWER(personne_physique.pseudonyme),
            LOWER(personne_morale.sigle),
            LOWER(entreprise_periode.nom),
            LOWER(entreprise_periode.nom_usage),
            LOWER(entreprise_periode.denomination),
            LOWER(entreprise_periode.denomination_usuelle_1),
            LOWER(entreprise_periode.denomination_usuelle_2),
            LOWER(entreprise_periode.denomination_usuelle_3),
            LOWER(etablissement_periode.enseigne_1),
            LOWER(etablissement_periode.enseigne_2),
            LOWER(etablissement_periode.enseigne_3),
            LOWER(etablissement_periode.denomination_usuelle)
        ),
        LOWER(STRING_AGG(personne.prenom || ' ' || personne.nom, ' '))
    FROM equipe__commune_vue
    JOIN etablissement ON etablissement.commune_id = equipe__commune_vue.commune_id
    JOIN entreprise ON entreprise.id = etablissement.entreprise_id
    JOIN entreprise_periode ON entreprise_periode.entreprise_id = etablissement.entreprise_id
    JOIN categorie_juridique_type ON categorie_juridique_type.id = entreprise_periode.categorie_juridique_type_id
    JOIN etablissement_periode ON etablissement_periode.etablissement_id = etablissement.id
    JOIN etablissement_adresse_1 ON etablissement_adresse_1.etablissement_id = etablissement.id
    JOIN commune ON etablissement.commune_id = commune.id
    LEFT JOIN qpv ON etablissement_adresse_1.geolocalisation IS NOT NULL
        AND ST_Covers(qpv.geometrie, etablissement_adresse_1.geolocalisation)
    LEFT JOIN personne_physique ON personne_physique.entreprise_id = etablissement.entreprise_id
    LEFT JOIN personne_morale ON personne_morale.entreprise_id = etablissement.entreprise_id
    LEFT JOIN contact ON etablissement.id = contact.etablissement_id
    LEFT JOIN personne ON personne.id = contact.personne_id
    WHERE
        equipe__commune_vue.equipe_id = eid
    GROUP BY (
        etablissement.id,
        equipe__commune_vue.equipe_id,
        entreprise_periode.entreprise_id,
        entreprise_periode.debut_date,
				entreprise_periode.categorie_juridique_type_id,
				entreprise_periode.economie_sociale_solidaire,
				entreprise_periode.nom,
        entreprise_periode.nom_usage,
        entreprise_periode.denomination,
        entreprise_periode.denomination_usuelle_1,
        entreprise_periode.denomination_usuelle_2,
        entreprise_periode.denomination_usuelle_3,
        entreprise.id,
				categorie_juridique_type.parent_categorie_juridique_type_id,
        etablissement_periode.etablissement_id,
        etablissement_periode.debut_date,
        etablissement_periode.fin_date,
        etablissement_periode.employeur,
        etablissement_periode.actif,
        etablissement_periode.naf_type_id,
        etablissement_periode.enseigne_1,
        etablissement_periode.enseigne_2,
        etablissement_periode.enseigne_3,
        etablissement_periode.denomination_usuelle,
        etablissement_adresse_1.geolocalisation,
        etablissement_adresse_1.code_postal,
        etablissement_adresse_1.numero,
				etablissement_adresse_1.voie_nom,
        commune.id,
        qpv.id,
        personne_physique.entreprise_id,
        personne_morale.entreprise_id
    )

    UNION

    SELECT
        equipe__etablissement_exogene.equipe_id,
        etablissement.id,
        TRUE,
        etablissement.entreprise_id,
        etablissement.commune_id,
        etablissement.creation_date,
        CASE
            WHEN etablissement_periode.actif IS NULL
            THEN etablissement_periode.fin_date
            ELSE NULL
        END,
        etablissement.effectif_type_id,
        entreprise_periode.categorie_juridique_type_id,
        categorie_juridique_type.parent_categorie_juridique_type_id,
        entreprise.entreprise_type_id,
        entreprise.categorie_entreprise_type_id,
        entreprise_periode.economie_sociale_solidaire,
        etablissement_periode.employeur,
        etablissement_periode.actif,
        etablissement_periode.naf_type_id,
        etablissement_adresse_1.geolocalisation,
        etablissement_adresse_1.code_postal,
        etablissement_adresse_1.numero,
				etablissement_adresse_1.voie_nom,
        qpv.id,
        commune.zrr_type_id,
        CONCAT_WS(
            ' ',
            LOWER(personne_physique.prenom_1),
            LOWER(personne_physique.prenom_2),
            LOWER(personne_physique.prenom_3),
            LOWER(personne_physique.prenom_4),
            LOWER(personne_physique.prenom_usuel),
            LOWER(personne_physique.pseudonyme),
            LOWER(personne_morale.sigle),
            LOWER(entreprise_periode.nom),
            LOWER(entreprise_periode.nom_usage),
            LOWER(entreprise_periode.denomination),
            LOWER(entreprise_periode.denomination_usuelle_1),
            LOWER(entreprise_periode.denomination_usuelle_2),
            LOWER(entreprise_periode.denomination_usuelle_3),
            LOWER(etablissement_periode.enseigne_1),
            LOWER(etablissement_periode.enseigne_2),
            LOWER(etablissement_periode.enseigne_3),
            LOWER(etablissement_periode.denomination_usuelle)
        ),
        STRING_AGG(personne.prenom || ' ' || personne.nom, ' ')
    FROM equipe__etablissement_exogene
    JOIN etablissement ON equipe__etablissement_exogene.etablissement_id = etablissement.id
    JOIN entreprise ON entreprise.id = etablissement.entreprise_id
    JOIN entreprise_periode ON entreprise_periode.entreprise_id = etablissement.entreprise_id
    JOIN categorie_juridique_type ON categorie_juridique_type.id = entreprise_periode.categorie_juridique_type_id
    JOIN etablissement_periode ON etablissement_periode.etablissement_id = etablissement.id
    JOIN etablissement_adresse_1 ON etablissement_adresse_1.etablissement_id = etablissement.id
    JOIN commune on etablissement.commune_id = commune.id
    LEFT JOIN qpv ON etablissement_adresse_1.geolocalisation IS NOT NULL
        AND ST_Covers(qpv.geometrie, etablissement_adresse_1.geolocalisation)
    LEFT JOIN personne_physique ON personne_physique.entreprise_id = etablissement.entreprise_id
    LEFT JOIN personne_morale ON personne_morale.entreprise_id = etablissement.entreprise_id
    LEFT JOIN contact ON etablissement.id = contact.etablissement_id
    LEFT JOIN personne ON personne.id = contact.personne_id
    WHERE equipe__etablissement_exogene.equipe_id = eid
    GROUP BY (
        etablissement.id,
        equipe__etablissement_exogene.equipe_id,
        entreprise_periode.entreprise_id,
        entreprise_periode.debut_date,
				entreprise_periode.categorie_juridique_type_id,
				entreprise_periode.economie_sociale_solidaire,
				entreprise_periode.nom,
        entreprise_periode.nom_usage,
        entreprise_periode.denomination,
        entreprise_periode.denomination_usuelle_1,
        entreprise_periode.denomination_usuelle_2,
        entreprise_periode.denomination_usuelle_3,
        entreprise.id,
				categorie_juridique_type.parent_categorie_juridique_type_id,
        etablissement_periode.etablissement_id,
        etablissement_periode.debut_date,
        etablissement_periode.fin_date,
        etablissement_periode.employeur,
        etablissement_periode.actif,
        etablissement_periode.naf_type_id,
        etablissement_periode.enseigne_1,
        etablissement_periode.enseigne_2,
        etablissement_periode.enseigne_3,
        etablissement_periode.denomination_usuelle,
        etablissement_periode.denomination_usuelle,
        etablissement_adresse_1.geolocalisation,
        etablissement_adresse_1.code_postal,
        etablissement_adresse_1.numero,
				etablissement_adresse_1.voie_nom,
        commune.id,
        qpv.id,
        personne_physique.entreprise_id,
        personne_morale.entreprise_id
    );
END
$$
LANGUAGE plpgsql;

-- insert into table for each team
DO
$$
	DECLARE
		e RECORD;
	BEGIN
		FOR e IN
			SELECT id FROM equipe
		LOOP
			CALL equipe__etablissement_create(e.id);
		END LOOP;
	END;
$$ LANGUAGE plpgsql;

-- on ajoute les index après avoir rempli la table avec toutes les équipes

-- UPDATE equipe__etablissement EE
-- SET fermeture_date = (
--     SELECT debut_date
--     FROM etablissement_periode EP
--     WHERE EP.etablissement_id = EE.etablissement_id
--     AND NOT actif
--     LIMIT 1
-- );

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement_equipe ON equipe__etablissement (equipe_id);
CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement_etablissement ON equipe__etablissement (etablissement_id);
CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement_entreprise ON equipe__etablissement (entreprise_id);

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement_etablissement_gin ON equipe__etablissement USING gin (etablissement_id gin_trgm_ops);

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__creation_date ON equipe__etablissement (creation_date);
CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__fermeture_date ON equipe__etablissement (fermeture_date);
CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__categorie_juridique_3 ON equipe__etablissement (categorie_juridique_3_type_id);
CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__categorie_juridique_2 ON equipe__etablissement (categorie_juridique_2_type_id);
CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__naf ON equipe__etablissement (naf_type_id);
CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__commune ON equipe__etablissement (commune_id);
CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__qpv ON equipe__etablissement (qpv_id);

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__etablissement_noms ON equipe__etablissement USING gin (etablissement_noms gin_trgm_ops);
CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__contact_noms ON equipe__etablissement USING gin (contact_noms gin_trgm_ops);

ALTER TABLE equipe__etablissement ADD CONSTRAINT fk_equipe__etablissement__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement ADD CONSTRAINT fk_equipe__etablissement__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement ADD CONSTRAINT fk_equipe__etablissement__commune FOREIGN KEY (commune_id) REFERENCES commune(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement ADD CONSTRAINT fk_equipe__etablissement__effectif_type FOREIGN KEY (effectif_type_id) REFERENCES effectif_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement ADD CONSTRAINT fk_equipe__etablissement__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement ADD CONSTRAINT fk_equipe__etablissement__entreprise_type FOREIGN KEY (entreprise_type_id) REFERENCES entreprise_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement ADD CONSTRAINT fk_equipe__etablissement__categorie_entreprise_type FOREIGN KEY (categorie_entreprise_type_id) REFERENCES categorie_entreprise_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement ADD CONSTRAINT fk_equipe__etablissement__zrr_type FOREIGN KEY (zrr_type_id) REFERENCES zrr_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement ADD CONSTRAINT fk_equipe__etablissement__qpv FOREIGN KEY (qpv_id) REFERENCES qpv(id) ON DELETE CASCADE ON UPDATE CASCADE;

-- reset sequences

DO $$
DECLARE
    table_record record;
BEGIN
    FOR table_record IN (
        SELECT table_name
        FROM information_schema.columns
        WHERE table_schema = 'public'
        AND column_name = 'id'
        AND column_default LIKE 'nextval%'
				AND table_name NOT LIKE '%_adresse%'
    )
    LOOP
        EXECUTE '
            SELECT
                setval(
                    ''' || table_record.table_name || '_id_seq'',
                    (SELECT MAX(id) FROM ' || table_record.table_name || ')
                );';
    END LOOP;
END $$;
