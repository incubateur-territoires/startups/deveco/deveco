CREATE INDEX IF NOT EXISTS IDX_etablissement_periode__etablissement_actif ON etablissement_periode (etablissement_id, actif);
CREATE INDEX IF NOT EXISTS IDX_etablissement_periode__recherche ON etablissement_periode (etablissement_id, actif, naf_type_id);


CREATE INDEX IDX_entreprise_periode__ess_partial ON entreprise_periode (entreprise_id)
WHERE economie_sociale_solidaire;

--
CREATE INDEX IF NOT EXISTS IDX_entreprise_periode__ess_non ON entreprise_periode (entreprise_id)
WHERE economie_sociale_solidaire = FALSE;

-- index recherche
-- CREATE INDEX IF NOT EXISTS IDX_entreprise_periode__filtres ON entreprise_periode (entreprise_id, economie_sociale_solidaire, categorie_juridique_type_id, actif);

-- test d'index avec WHERE
CREATE INDEX IF NOT EXISTS IDX_etablissement_periode__etablissement_actif ON etablissement_periode (etablissement_id)
WHERE
    actif;

CREATE INDEX IF NOT EXISTS IDX_etablissement_periode__etablissement_actif_2 ON etablissement_periode (etablissement_id, actif);

CREATE INDEX IF NOT EXISTS IDX_entreprise_periode__entreprise_ess ON entreprise_periode (entreprise_id)
WHERE
    economie_sociale_solidaire;


--  trop lent : 40 sec

SELECT
    ET.id AS etablissement_id,
    ET.creation_date,
    ET.commune_id,
    EN.economie_sociale_solidaire,
	EN.categorie_juridique_type_id
FROM
    etablissement ET
JOIN entreprise_periode EN ON EN.entreprise_id = ET.entreprise_id
WHERE ET.commune_id IN (
    select commune_id
    from equipe__commune_vue
    where equipe_id = 160


-- test de full text search

CREATE MATERIALIZED VIEW etablissement_recherche_vue AS
SELECT
    ET.id AS etablissement_id,
    ET.creation_date,
    to_tsvector(
        'commune_' || C.id || ' ' ||
        'metropole_' || COALESCE(C.metropole_id, '') || ' ' ||
        'epci_' || C.epci_id || ' ' ||
        'petr_' || COALESCE(C.petr_id, '') || ' ' ||
        'departement_' || C.departement_id || ' ' ||
        'region_' || C.region_id || ' ' ||
        'actif_' || EN.actif || ' ' ||
        'ess_' || EN.economie_sociale_solidaire || ' ' ||
        'naf_' || EN.naf_type_id || ' ' ||
        'employeur_' || EN.employeur || ' ' ||
        'categorie_juridique_' || EN.categorie_juridique_type_id
    )
        as recherche
FROM
    etablissement ET
JOIN entreprise_periode EN ON EN.entreprise_id = ET.entreprise_id
JOIN commune C ON C.id = ET.commune_id;

-- CREATE INDEX idx_etablissement_recherche_vue ON etablissement_recherche_vue USING GIN (to_tsvector(recherche));
CREATE INDEX IF NOT EXISTS idx_etablissement_recherche_vue ON etablissement_recherche_vue USING GIN (recherche);
CREATE INDEX IF NOT EXISTS idx_etablissement_recherche_vue__creation_date ON etablissement_recherche_vue (creation_date DESC NULLS LAST);



SELECT etablissement_id, creation_date
FROM etablissement_recherche_vue
WHERE recherche @@ to_tsquery(
    '('
      || 'metropole_75056'
    -- || 'commune_75101'
    -- || ' | commune_75102'
    -- || ' | commune_75103'
    -- || ' | commune_75104'
    -- || ' | commune_75105'
    -- || ' | commune_75106'
    -- || ' | commune_75107'
    -- || ' | commune_75108'
    -- || ' | commune_75109'
    || ')'
    || ' & actif_true'
    || ' & ess_false'
    || ' & (categorie_juridique_1000 | categorie_juridique_5410)'
)
ORDER BY creation_date DESC NULLS LAST
LIMIT 20;

CREATE TABLE ter_new (LIKE ter INCLUDING INDEXES);

insert into ter_new
SELECT * from ter
WHERE territory_id = 160;



CREATE MATERIALIZED VIEW entreprise__commune_vue AS
SELECT DISTINCT ON (entreprise_id, commune_id) entreprise_id, commune_id
FROM etablissement e
JOIN entreprise t ON t.id = e.entreprise_id;

CREATE INDEX IDX_entreprise__commune_vue__commune_entreprise ON entreprise__commune_vue(commune_id, entreprise_id);

SELECT E.id
FROM equipe__commune_vue V
JOIN etablissement E USING (commune_id)
JOIN entreprise__commune_vue C USING (commune_id)
JOIN entreprise_periode N ON N.entreprise_id = E.entreprise_id
WHERE equipe_id = 160
AND N.economie_sociale_solidaire
AND N.categorie_juridique_type_id IN ('1000')
ORDER BY E.creation_date DESC NULLS LAST
LIMIT 20;

CREATE MATERIALIZED VIEW entreprise_periode__ess AS
SELECT entreprise_id, economie_sociale_solidaire
FROM entreprise_periode;

CREATE INDEX IDX_entreprise_periode__ess ON entreprise_periode__ess(economie_sociale_solidaire, entreprise_id);

CREATE INDEX IDX_entreprise_periode__ess_true ON entreprise_periode__ess(entreprise_id) where economie_sociale_solidaire = true;
CREATE INDEX IDX_entreprise_periode__ess_false ON entreprise_periode__ess(entreprise_id) where economie_sociale_solidaire = false;


SELECT E.id
FROM equipe__commune_vue V
JOIN etablissement E USING (commune_id)
JOIN entreprise_periode__ess PE ON PE.entreprise_id = E.entreprise_id
JOIN entreprise_periode__catjur PC ON PC.entreprise_id = E.entreprise_id
WHERE equipe_id = 160
AND PE.economie_sociale_solidaire
AND PC.categorie_juridique_type_id IN ('1000')
ORDER BY E.creation_date DESC NULLS LAST
LIMIT 20;

SELECT id, creation_date
FROM equipe__commune_vue v
JOIN etablissement e ON e.commune_Id = v.commune_id
WHERE
    EXISTS (
        SELECT Q.id
        FROM etiquette Q
        JOIN equipe__etablissement__etiquette EEQ ON EEQ.etiquette_id = Q.id
        WHERE EEQ.equipe_id = v.equipe_id AND EEQ.etablissement_id = e.id
        AND Q.etiquette_type_id = 'activite' AND Q.nom IN ('Commerce')
        LIMIT 1
    )
    AND EXISTS (
        SELECT Q.id
        FROM etiquette Q
        JOIN equipe__etablissement__etiquette EEQ ON EEQ.etiquette_id = Q.id
        WHERE EEQ.equipe_id = v.equipe_id AND EEQ.etablissement_id = e.id
        AND Q.etiquette_type_id = 'localisation' AND Q.nom IN ('75014')
        LIMIT 1
    )
    AND EXISTS (
        SELECT Q.id
        FROM etiquette Q
        JOIN equipe__etablissement__etiquette EEQ ON EEQ.etiquette_id = Q.id
        WHERE EEQ.equipe_id = v.equipe_id AND EEQ.etablissement_id = e.id
        AND Q.etiquette_type_id = 'mot_cle' AND Q.nom IN ('AAP')
        LIMIT 1
    )
    AND v.equipe_id = 160
LIMIT 20;

--

select count(*)
FROM sirene_etablissement
WHERE date_dernier_traitement_etablissement > '2023-06-01';

-- test avec Paris t.id 160
-- début 15:42:01

-- test avec L''Arbresle 551
-- début 15:27:10
-- fin 15:28:00

-- adresse en double

SELECT count (
    DISTINCT (
        E.code_commune_etablissement,
        TRIM(
            CASE
                WHEN numero_voie_etablissement IS NOT NULL AND numero_voie_etablissement <> ''
                THEN numero_voie_etablissement || ' '
                ELSE ''
            END ||
            CASE
                WHEN indice_repetition_etablissement IS NOT NULL AND indice_repetition_etablissement <> ''
                THEN indice_repetition_etablissement || ' '
                ELSE ''
            END
        ),
        TRIM(
            CASE
                WHEN type_voie_etablissement IS NOT NULL AND type_voie_etablissement <> ''
                THEN type_voie_etablissement || ' '
                ELSE ''
            END ||
            CASE
                WHEN libelle_voie_etablissement IS NOT NULL AND libelle_voie_etablissement <> ''
                THEN libelle_voie_etablissement || ' '
                ELSE ''
            END ||
            CASE
                WHEN complement_adresse_etablissement IS NOT NULL AND complement_adresse_etablissement <> ''
                THEN complement_adresse_etablissement
                ELSE ''
            END
        ),
        E.code_postal_etablissement,
        E.distribution_speciale_etablissement,
        E.code_cedex_etablissement,
        E.libelle_cedex_etablissement
    )
)
FROM source.source_api_sirene_etablissement E
;

SELECT count (DISTINCT (E.longitude, E.latitude))

FROM source.source_api_sirene_etablissement E;

-- mise à jour de consultationAt à partir de l'évènement affichage
DO $$
DECLARE e RECORD;
BEGIN FOR e IN
	SELECT id FROM equipe
LOOP
	EXECUTE 'with consultation as (
    select
      distinct on (
        eveqet.etablissement_id
      ) ev.date,
      eveqet.etablissement_id
    from
      evenement ev
      join evenement__equipe_etablissement eveqet on eveqet.evenement_id = ev.id
      join equipe__etablissement_' || e.id || ' eqet on eveqet.etablissement_id = eqet.etablissement_id
      and eveqet.equipe_id = eqet.equipe_id
    where
      ev.evenement_type_id = ''affichage'' and ev.date > ''2023-10-10''
    order by
      eveqet.etablissement_id,
      ev.date desc
  )
update
  equipe__etablissement_' || e.id || ' ee
set
  consultation_at = consultation.date
from
  consultation
where
  ee.etablissement_id = consultation.etablissement_id;';
  COMMIT;
   END LOOP;
END;
$$ LANGUAGE plpgsql;


------


SELECT *
FROM equipe__etablissement_exogene
LEFT JOIN equipe__etablissement
    ON equipe__etablissement_exogene.equipe_id = equipe__etablissement.equipe_id
WHERE equipe__etablissement.equipe_id IS NULL;

--------

 equipe_id | etablissement_id |           ajout_at
-----------+------------------+-------------------------------
       620 | 89297556600019   | 2023-10-18 14:02:21.263842+00
       620 | 85680240000098   | 2023-10-18 14:36:58.808644+00
       620 | 82435465800034   | 2023-10-19 07:38:50.876103+00


--------

WITH
  etablissement_periode_active AS (
    select distinct
      on (etablissement_id) *
    from
      etablissement_periode
    where
      etablissement_id IN ('89297556600019', '85680240000098', '82435465800034')
    order by
      etablissement_id,
      debut_date desc
  ),
  entreprise_periode_active AS (
    select distinct
      on (entreprise_id) *
    from
      entreprise_periode
    where
      entreprise_id IN ('892975566', '856802400', '824354658')
    order by
      entreprise_id,
      debut_date desc
  )
INSERT INTO
  equipe__etablissement (
    equipe_id,
    etablissement_id,
    exogene,
    entreprise_id,
    commune_id,
    creation_date,
    fermeture_date,
    effectif_type_id,
    categorie_juridique_3_type_id,
    categorie_juridique_2_type_id,
    entreprise_type_id,
    categorie_entreprise_type_id,
    economie_sociale_solidaire,
    employeur,
    actif,
    naf_type_id,
    geolocalisation,
    code_postal,
    numero,
    voie_nom,
    qpv_id,
    zrr_type_id,
    etablissement_noms,
    contact_noms
  )
SELECT
  620,
  etablissement.id,
  TRUE,
  etablissement.entreprise_id,
  etablissement.commune_id,
  etablissement.creation_date,
  CASE
    WHEN etablissement_periode.actif IS NULL
    OR NOT etablissement_periode.actif THEN etablissement_periode.debut_date
    ELSE NULL
  END,
  etablissement.effectif_type_id,
  entreprise_periode.categorie_juridique_type_id,
  categorie_juridique_type.parent_categorie_juridique_type_id,
  entreprise.entreprise_type_id,
  entreprise.categorie_entreprise_type_id,
  entreprise_periode.economie_sociale_solidaire,
  etablissement_periode.employeur,
  etablissement_periode.actif,
  etablissement_periode.naf_type_id,
  etablissement_adresse_1.geolocalisation,
  etablissement_adresse_1.code_postal,
  etablissement_adresse_1.numero,
  etablissement_adresse_1.voie_nom,
  qpv.id,
  commune.zrr_type_id,
  CONCAT_WS(
    ' ',
    LOWER(personne_physique.prenom_1),
    LOWER(personne_physique.prenom_2),
    LOWER(personne_physique.prenom_3),
    LOWER(personne_physique.prenom_4),
    LOWER(personne_physique.prenom_usuel),
    LOWER(personne_physique.pseudonyme),
    LOWER(personne_morale.sigle),
    LOWER(entreprise_periode.nom),
    LOWER(entreprise_periode.nom_usage),
    LOWER(entreprise_periode.denomination),
    LOWER(entreprise_periode.denomination_usuelle_1),
    LOWER(entreprise_periode.denomination_usuelle_2),
    LOWER(entreprise_periode.denomination_usuelle_3),
    LOWER(etablissement_periode.enseigne_1),
    LOWER(etablissement_periode.enseigne_2),
    LOWER(etablissement_periode.enseigne_3),
    LOWER(etablissement_periode.denomination_usuelle)
  ),
  STRING_AGG(personne.prenom || ' ' || personne.nom, ' ')
FROM
  equipe__etablissement_exogene
  JOIN etablissement ON equipe__etablissement_exogene.etablissement_id = etablissement.id
  JOIN entreprise ON entreprise.id = etablissement.entreprise_id
  JOIN entreprise_periode_active entreprise_periode ON entreprise_periode.entreprise_id = etablissement.entreprise_id
  JOIN categorie_juridique_type ON categorie_juridique_type.id = entreprise_periode.categorie_juridique_type_id
  JOIN etablissement_periode_active etablissement_periode ON etablissement_periode.etablissement_id = etablissement.id
  JOIN etablissement_adresse_1 ON etablissement_adresse_1.etablissement_id = etablissement.id
  JOIN commune on etablissement.commune_id = commune.id
  LEFT JOIN qpv ON etablissement_adresse_1.geolocalisation IS NOT NULL
  AND ST_Covers (
    qpv.geometrie,
    etablissement_adresse_1.geolocalisation
  )
  LEFT JOIN personne_physique ON personne_physique.entreprise_id = etablissement.entreprise_id
  LEFT JOIN personne_morale ON personne_morale.entreprise_id = etablissement.entreprise_id
  LEFT JOIN contact ON etablissement.id = contact.etablissement_id
  LEFT JOIN personne ON personne.id = contact.personne_id
WHERE
  equipe__etablissement_exogene.equipe_id = 620
  AND equipe__etablissement_exogene.etablissement_id IN ('89297556600019', '85680240000098', '82435465800034')
GROUP BY
  (
    equipe__etablissement_exogene.equipe_id,
    etablissement.id,
    entreprise_periode.entreprise_id,
    entreprise_periode.debut_date,
    entreprise_periode.categorie_juridique_type_id,
    entreprise_periode.economie_sociale_solidaire,
    entreprise_periode.nom,
    entreprise_periode.nom_usage,
    entreprise_periode.denomination,
    entreprise_periode.denomination_usuelle_1,
    entreprise_periode.denomination_usuelle_2,
    entreprise_periode.denomination_usuelle_3,
    entreprise.id,
    categorie_juridique_type.parent_categorie_juridique_type_id,
    etablissement_periode.etablissement_id,
    etablissement_periode.debut_date,
    etablissement_periode.fin_date,
    etablissement_periode.employeur,
    etablissement_periode.actif,
    etablissement_periode.naf_type_id,
    etablissement_periode.enseigne_1,
    etablissement_periode.enseigne_2,
    etablissement_periode.enseigne_3,
    etablissement_periode.denomination_usuelle,
    etablissement_periode.denomination_usuelle,
    etablissement_adresse_1.geolocalisation,
    etablissement_adresse_1.code_postal,
    etablissement_adresse_1.numero,
    etablissement_adresse_1.voie_nom,
    commune.id,
    qpv.id,
    personne_physique.entreprise_id,
    personne_morale.entreprise_id
  );


INSERT INTO source.source_api_sirene_unite_legale_periode(
	siren,
	date_debut,
	date_fin,
	nom_unite_legale
)
VALUES (
	'947854030',
	null,
	null,
	'chose'
)
ON CONFLICT  (siren, COALESCE(date_fin, '1901-02-03'))
DO UPDATE
SET (
	siren,
	date_debut,
	date_fin,
	nom_unite_legale
)
= (
	excluded.siren,
	excluded.date_debut,
	excluded.date_fin,
	excluded.nom_unite_legale
)
WHERE excluded.date_debut IS NOT NULL
OR source.source_api_sirene_unite_legale_periode.date_debut IS NULL;

---

SELECT count(*),
  e.id,
  z.nom
FROM zonage z
  JOIN etiquette e ON e.nom = z.nom
  AND e.etiquette_type_id = 'localisation'
  JOIN equipe__etablissement__etiquette ee ON ee.etiquette_id = e.id
WHERE z.equipe_id = 438
GROUP BY e.id,
  z.nom
ORDER BY count DESC;

---

CREATE MATERIALIZED VIEW etablissement_periode_derniere_vue AS
SELECT *
FROM etablissement_periode
WHERE etablissement_periode.fin_date IS NULL;

---- effectifs sirene
-- check de totaux des effectifs des tranches par entreprise

select id,
  effectif_type_id,
  basse,
  haute,
  basse > 9999 as basse_ko,
  haute < 5000 as haute_ko
FROM (
    select id,
      effectif_type_id,
      SUM(total_basse) as basse,
      SUM(total_haute) as haute
    FROM (
        select *,
          basse * c as total_basse,
          haute * c as total_haute
        FROM (
            select en.id,
              en.effectif_type_id,
              case
                when et.effectif_type_id = '00' then 0
                when et.effectif_type_id = '01' then 1
                when et.effectif_type_id = '02' then 3
                when et.effectif_type_id = '03' then 6
                when et.effectif_type_id = '11' then 10
                when et.effectif_type_id = '12' then 20
                when et.effectif_type_id = '21' then 50
                when et.effectif_type_id = '22' then 100
                when et.effectif_type_id = '31' then 200
                when et.effectif_type_id = '32' then 250
                when et.effectif_type_id = '41' then 500
                when et.effectif_type_id = '42' then 1000
                when et.effectif_type_id = '51' then 2000
                when et.effectif_type_id = '52' then 5000
              end as basse,
              case
                when et.effectif_type_id = '00' then 0
                when et.effectif_type_id = '01' then 2
                when et.effectif_type_id = '02' then 5
                when et.effectif_type_id = '03' then 9
                when et.effectif_type_id = '11' then 19
                when et.effectif_type_id = '12' then 49
                when et.effectif_type_id = '21' then 99
                when et.effectif_type_id = '22' then 199
                when et.effectif_type_id = '31' then 249
                when et.effectif_type_id = '32' then 499
                when et.effectif_type_id = '41' then 999
                when et.effectif_type_id = '42' then 1999
                when et.effectif_type_id = '51' then 4999
                when et.effectif_type_id = '52' then 9999
              end as haute,
              count(*) as c
            from etablissement et
              join entreprise en on en.id = et.entreprise_id
            where et.effectif_type_id not in ('NN', 'NA')
              and en.effectif_type_id not in ('NN', 'NA')
              and en.effectif_type_id = '52'
            group by en.id,
              en.effectif_type_id,
              et.effectif_type_id
            order by en.effectif_type_id desc
          ) toto1
      ) toto2
    GROUP BY id,
      effectif_type_id
  ) toto3
WHERE basse > 9999
  OR haute < 5000;

select distinct on (
    source_api_sirene_lien_succession.date_lien_succession,
    source_api_sirene_lien_succession.siret_etablissement_predecesseur,
    source_api_sirene_lien_succession.siret_etablissement_successeur
  ) continuite_economique,
  date_lien_succession,
  date_dernier_traitement_lien_succession,
  siret_etablissement_predecesseur,
  siret_etablissement_successeur,
  transfert_siege,
  mise_a_jour_at
from source.source_api_sirene_lien_succession
where source_api_sirene_lien_succession.mise_a_jour_at > $1
order by source_api_sirene_lien_succession.mise_a_jour_at desc

--- affectation des rappels

ALTER TABLE rappel ADD column affecte_id INTEGER REFERENCES compte(id);

UPDATE rappel set affecte_id = (
  SELECT compte_id
  FROM evenement e
  INNER JOIN evenement__rappel er ON er.evenement_id = e.id AND er.rappel_id = rappel.id
  WHERE e.evenement_type_id = 'creation'
  LIMIT 1
);

--- équipes en doublon

WITH
  equipe_communes AS (
    SELECT
      equipe_id,
      e.nom,
      string_agg(commune_id, ',') as communes
    FROM (
      SELECT equipe_id, commune_id
      FROM equipe__commune_vue
      ORDER BY equipe_id, commune_id
    ) c
    INNER JOIN equipe e ON e.id = c.equipe_id
    GROUP BY equipe_id, nom
    ORDER BY equipe_id
  )
SELECT c1.equipe_id, c1.nom, c2.equipe_id, c2.nom
FROM
  equipe_communes c1,
  equipe_communes c2
WHERE c1.communes = c2.communes
  and c1.equipe_id < c2.equipe_id
ORDER BY c1.equipe_id, c2.equipe_id
;

 equipe_id |                nom                 | equipe_id |                   nom
-----------+------------------------------------+-----------+-----------------------------------------
       637 | CU Le Mans Métropole               |       689 | OCA Le Mans
       765 | CC Lodévois et Larzac              |       947 | CC Lodévois et Larzac (20)
       845 | CA de Sophia Antipolis             |       941 | CA de Sophia Antipolis (24)
       919 | CC Auray Quiberon Terre Atlantique |       942 | CC Auray Quiberon Terre Atlantique (20)
(12 rows)

select "deveco"."id",
  "deveco"."bienvenue_email",
  "deveco"."compte_id",
  "deveco"."created_at",
  "deveco"."equipe_id",
  "deveco"."updated_at",
  "compte"."id",
  "compte"."nom",
  "compte"."prenom",
  "compte"."email",
  "compte"."compte_type_id",
  "compte"."cle",
  "compte"."actif",
  "equipe"."id",
  "equipe"."groupement",
  "equipe"."nom",
  "equipe"."territoire_type_id",
  "equipe"."equipe_type_id",
  "mot_de_passe"."id",
  "mot_de_passe"."hash",
  "mot_de_passe"."updated_at",
  "mot_de_passe"."created_at",
  "mot_de_passe"."compte_id",
  (
    select "evenement"."date"
    from "evenement__compte"
      inner join "evenement" on (
        "evenement"."id" = "evenement__compte"."evenement_id"
        and "evenement"."compte_id" = "compte"."id"
      )
    where "evenement"."evenement_type_id" = $1
    order by "evenement"."date" desc
    limit $2
  ) as "derniereConnexion",
  (
    select "evenement"."date"
    from "evenement__compte"
      inner join "evenement" on (
        "evenement"."id" = "evenement__compte"."evenement_id"
        and "evenement"."compte_id" = "compte"."id"
      )
    where "evenement"."evenement_type_id" = $3
    order by "evenement"."date" desc
    limit $4
  ) as "derniereActionAuthentifiee"
from "deveco"
  inner join "equipe" on "equipe"."id" = "deveco"."equipe_id"
  inner join "compte" on "compte"."id" = "deveco"."compte_id"
  inner join "mot_de_passe" on "mot_de_passe"."compte_id" = "deveco"."compte_id"
order by derniereConnexion
limit $5

----


 [
   160,
   '84974312500015',
   46668,
  '',
  'deveco',
  [
    160,
    '84974312500015',
    true,
    'PME',
    '57',
    '5710',
    '75011',
    '75111',
    'jeremie zeltner',
    '2019-03-29',
    false,
    '02',
    true,
    '849743125',
    'M',
    'plaisir compagnie cadence',
    false,
    null,
    { type: 'Point', coordinates: [Array] },
    '56.10A',
    null,
    '117',
    'AV PARMENTIER',
    null,
    false,
    [
      '84974312500015', '00015',
      'CADENCE',        '2019-03-29',
      '2022-08-29',     '02',
      2020,             '849743125',
      '75111',          true,
      true,             null,
      [Array]
    ]
  ]
]

--- communes historiques

select *
from source.source_insee_commune_mouvement m
  join source.source_insee_commune_historique h on h.com = m.com_av
  left join commune c on c.id = h.com
where c.id is null;

---

select *
from (
    select *
    from source.source_insee_commune_historique h
      left join source.source_insee_commune_mouvement m_av on m_av.com_av = h.com
      left join source.source_insee_commune_mouvement m_ap on m_ap.com_ap = h.com
    where m_ap.com_ap is not null
      or m_av.com_av is not null
  ) h
  left join commune c on c.id = h.com
where c.id is null;

-- insertion des communes avant
-- code exécuté 2 fois pour insérer toutes les communes

INSERT INTO commune (
    id,
    nom,
    commune_type_id,
    commune_parent_id,
    epci_id,
    petr_id,
    departement_id,
    region_id,
    zrr_type_id
)
SELECT
    DISTINCT ON (C.com_av)
    C.com_av,
    C.libelle_av,
    C.typecom_av,
    C.com_ap,
    CP.epci_id,
    CP.petr_id,
    CP.departement_id,
    CP.region_id,
    CP.zrr_type_id
FROM source.source_insee_commune_mouvement C
LEFT JOIN commune CC on CC.id = C.com_av
JOIN commune CP on CP.id = C.com_ap
WHERE CC.id IS NULL AND C.com_av IS NOT NULL
ORDER BY C.com_av, C.date_eff DESC;

-- vérification des communes avant manquantes dans la table commune

select *
from source.source_insee_commune_mouvement m
left join commune c on c.id = m.com_av
where c.id is null AND m.com_av is not null;

-- vérification des communes après manquantes dans la table commune

select DISTINCT *
from source.source_insee_commune_mouvement m
  left join commune c on c.id = m.com_ap
where c.id is null;

--- vérification des communes historiques dans la table commune

select DISTINCT h.com
from source.source_insee_commune_historique h
left join commune c on c.id = h.com
where c.id is null;

--- insertion des communes Saint-Barthélémy et Saint-Martin

INSERT INTO commune (id, nom, commune_type_id, commune_parent_id, metropole_id, epci_id, petr_id, departement_id, region_id, zrr_type_id)
SELECT '97123' as id, nom, commune_type_id, '97701' as commune_parent_id, metropole_id, epci_id, petr_id, departement_id, region_id, zrr_type_id s
FROM commune
WHERE id = '97701';

INSERT INTO commune (id, nom, commune_type_id, commune_parent_id, metropole_id, epci_id, petr_id, departement_id, region_id, zrr_type_id)
SELECT '97127' as id, nom, commune_type_id, '97801' as commune_parent_id, metropole_id, epci_id, petr_id, departement_id, region_id, zrr_type_id
FROM commune
WHERE id = '97801';

---

-- vérification de tous les ids de communes dans la table commune

SELECT distinct t.id FROM (
  SELECT distinct com as id
  FROM source.source_insee_commune_historique
  UNION
  SELECT distinct com_av as id
  FROM source.source_insee_commune_mouvement
  WHERE com_av is not null
  UNION
  SELECT distinct com_ap as id
  FROM source.source_insee_commune_mouvement
) t
LEFT JOIN commune on commune.id = t.id
WHERE commune.id is null;

-- Crépieux-la-Pape
select * from commune where id = '01132';
select * from source.source_insee_commune_historique where com = '01132';
select * from source.source_insee_commune_mouvement where com_av = '01132';

select * from commune where id = '01414';
select * from source.source_insee_commune_historique where com = '01414';
select * from source.source_insee_commune_mouvement where com_av = '01414';

select distinct on (com_av) * from source.source_insee_commune_mouvement where com_av = '01086' order by com_av, date_eff desc;


-- ERROR:  null value in column epci_id of relation commune violates not-null constraint
-- DETAIL:  Failing row contains (01086, Charancin, COMA, 01414, null, null, null, null, null, null).

2023-12-20 11:16:30.315	erreur: [equipeEtablissement.upsertManyByCursor] equipe 438 - établissements endogènes - 30300 traités, 34920844700129, 99882350429828, 30438137900797, 42995529713886, 31221230102233, 42995529713480, 63204104273570, 42995529712110, 42995529711740, 34106769200580, 35028305700270, 30146814600727, 30146814600784, 81402284400017, 30125188000144, 34209083400389, 82429699000701, 82467782700027, 81795680800031, 44271303800473, 31778006201002, 32974735600114, 33003312700102, 39973700600086, 44271303800432, 38836614800509, 38062233201702, 55715006701111, 80454467400190, 42826794201846, 42826802334696, 99882350420314, 99882350405505, 66202519639952, 99882350415074, 66202519612207, 42826802340420, 77567227233398, 77567227214067, 50463123500315, 66202519673092, 63204104253135, 33456277400029, 44460844203871, 44460844203681, 44460844203723, 41178756701590, 44460844203699, 82466501200012, 53929151800014, 43399935605837, 43399935611934, 31890644300482, 48824724800021, 48824724800039, 48824724800054, 44271303800416, 77566502902099, 44271303800408, 44271303800390, 38062233208368, 36780140400628, 36780140400180, 41035886506523, 42826802343861, 32225058000147, 32225058000394, 32225058000444, 41035886502803, 41035886503207, 41035886503660, 41035886503694, 33991643900245, 41035886502753, 41035886502761, 33999316403439, 32305708300616, 33999316404478, 99882350426980, 44271303800481, 33999316402423, 32305708301234, 33999316405749, 82287568800019, 39776783102296, 39776783100365, 39776783101249, 34001885203008, 44156170100026, 34001885202992, 88870644700016, 31333485600314, 77566487303834, 97949488700016, 94826716600023, 41367724600022, 41367724600014, 34001885202984, 43039646500154, 38062233204938 ON CONFLICT DO UPDATE command cannot affect row a second time
2023-12-20 11:16:30.316	[Cron] api-sirene-update-manuel : Impossible de mettre à jour, erreur : ON CONFLICT DO UPDATE command cannot affect row a second time

select entreprise_id, count(*) from entreprise_periode where fin_date is null and entreprise_id in (
'301251880',
'301468146',
'301468146',
'304381379',
'312212301',
'313334856',
'317780062',
'318906443',
'322250580',
'322250580',
'322250580',
'323057083',
'323057083',
'329747356',
'330033127',
'334562774',
'339916439',
'339993164',
'339993164',
'339993164',
'339993164',
'340018852',
'340018852',
'340018852',
'341067692',
'342090834',
'349208447',
'350283057',
'367801404',
'367801404',
'380622332',
'380622332',
'380622332',
'388366148',
'397767831',
'397767831',
'397767831',
'399737006',
'410358865',
'410358865',
'410358865',
'410358865',
'410358865',
'410358865',
'410358865',
'411787567',
'413677246',
'413677246',
'428267942',
'428268023',
'428268023',
'428268023',
'429955297',
'429955297',
'429955297',
'429955297',
'430396465',
'433999356',
'433999356',
'441561701',
'442713038',
'442713038',
'442713038',
'442713038',
'442713038',
'442713038',
'444608442',
'444608442',
'444608442',
'444608442',
'488247248',
'488247248',
'488247248',
'504631235',
'539291518',
'557150067',
'632041042',
'632041042',
'662025196',
'662025196',
'662025196',
'775664873',
'775665029',
'775672272',
'775672272',
'804544674',
'814022844',
'817956808',
'822875688',
'824296990',
'824665012',
'824677827',
'888706447',
'948267166',
'979494887',
'998823504',
'998823504',
'998823504',
'998823504',
'998823504')
group by entreprise_id
order by count desc;

979494887

 debut_date | fin_date | entreprise_id | siege_etablissement_id |  nom  | nom_usage | denomination | denomination_usuelle_1 | denomination_usuelle_2 | denomination_usuelle_3 | naf_revision_type_id | naf_type_id | categorie_juridique_type_id | actif | employeur | economie_sociale_solidaire | societe_mission | siege_etablissement_id_change | nom_change | nom_usage_change | denomination_change | denomination_usuelle_change | naf_type_change | categorie_juridique_change | actif_change | employeur_change | economie_sociale_solidaire_change | societe_mission_change
------------+----------+---------------+------------------------+-------+-----------+--------------+------------------------+------------------------+------------------------+----------------------+-------------+-----------------------------+-------+-----------+----------------------------+-----------------+-------------------------------+------------+------------------+---------------------+-----------------------------+-----------------+----------------------------+--------------+------------------+-----------------------------------+------------------------
 2023-09-15 |          | 979494887     | 97949488700016         | JAMET |           |              |                        |                        |                        | NAFRev2              | 66.22Z      | 1000                        | t     | f         | f                          | f               | f                             | f          | f                | f                   | f                           | f               | f                          | f            | f                | f                                 | f
            |          | 979494887     | 97949488700016         | JAMET |           |              |                        |                        |                        | NAFRev2              | 66.22Z      | 1000                        | t     | f         | f                          | f               | f                             | f          | f                | f                   | f                           | f               | f                          | f            | f                | f                                 | f
(2 rows)


Indexes:
    "idx_entreprise_periode__categorie_juridique" btree (categorie_juridique_type_id)
    "idx_entreprise_periode__courante_fin_date" btree ((fin_date IS NULL))
    "idx_entreprise_periode__entreprise" btree (entreprise_id)
    "idx_entreprise_periode__fin_date_partial" btree (entreprise_id) WHERE fin_date IS NULL
    "uk_entreprise_periode" UNIQUE CONSTRAINT, btree (entreprise_id, debut_date)

select count(*) from (select entreprise_id, count(*) from entreprise_periode where fin_date is null group by entreprise_id having count(*) > 1) t;
 count
-------
    18
(1 row)

 select entreprise_id, count(*) from entreprise_periode where fin_date is null group by entreprise_id having count(*) > 1;
 entreprise_id | count
---------------+-------
 924017890     |     2
 924019474     |     2
 924033657     |     2
 924041965     |     2
 978802619     |     2
 979106663     |     2
 979115490     |     2

 979246170     |     2
 979494887     |     2

 979507712     |     2
 979526944     |     2
 979622917     |     2
 979632247     |     2
 979649555     |     2
 979751559     |     2
 979898830     |     2
 980055511     |     2
 980136949     |     2
(18 rows)

> select count(*) from entreprise_periode where debut_date is null ;
  count
---------
 2 523 261
(1 row)

> select count(etablissement_id) from etablissement_periode where debut_date is null ;
 count
--------
 520144
(1 row)

> select entreprise_id from entreprise_periode where debut_date is null limit 10;
 entreprise_id
---------------
 016250870
 025570391
 017350901
 015442031
 015442940
 015444227
 015450117
 015450349
 015450802
 015470263
(10 rows)



select * from etablissement where entreprise_id = '924017890';
       id       |  nic  | creation_date | sirene_dernier_traitement_date | effectif_type_id | effectif_annee | entreprise_id | commune_id | diffusible | siege | nom |     mise_a_jour_at
----------------+-------+---------------+--------------------------------+------------------+----------------+---------------+------------+------------+-------+-----+------------------------
 92401789000014 | 00014 | 2022-11-25    | 2023-09-15                     |                  |                | 924017890     | 75117      | f          | t     |     | 2023-11-12 04:28:21+00
(1 row)

 debut_date | fin_date | entreprise_id | siege_etablissement_id | nom  | nom_usage | denomination | denomination_usuelle_1 | denomination_usuelle_2 | denomination_usuelle_3 | naf_revision_type_id | naf_type_id | categorie_juridique_type_id | actif | employeur | economie_sociale_solidaire | societe_mission | siege_etablissement_id_change | nom_change | nom_usage_change | denomination_change | denomination_usuelle_change | naf_type_change | categorie_juridique_change | actif_change | employeur_change | economie_sociale_solidaire_change | societe_mission_change
------------+----------+---------------+------------------------+------+-----------+--------------+------------------------+------------------------+------------------------+----------------------+-------------+-----------------------------+-------+-----------+----------------------------+-----------------+-------------------------------+------------+------------------+---------------------+-----------------------------+-----------------+----------------------------+--------------+------------------+-----------------------------------+------------------------
 2022-11-25 |          | 979741238     | 97974123800013         | [ND] | [ND]      | [ND]         | [ND]                   | [ND]                   | [ND]                   | NAFRev2              | 70.22Z      | 1000                        | t     | f         | f                          | f               | f                             | f          | f                | f                   | f                           | f               | f                          | f            | f                | f                                 | f
(1 row)

 debut_date | fin_date | entreprise_id | siege_etablissement_id | nom  | nom_usage | denomination | denomination_usuelle_1 | denomination_usuelle_2 | denomination_usuelle_3 | naf_revision_type_id | naf_type_id | categorie_juridique_type_id | actif | employeur | economie_sociale_solidaire | societe_mission | siege_etablissement_id_change | nom_change | nom_usage_change | denomination_change | denomination_usuelle_change | naf_type_change | categorie_juridique_change | actif_change | employeur_change | economie_sociale_solidaire_change | societe_mission_change
------------+----------+---------------+------------------------+------+-----------+--------------+------------------------+------------------------+------------------------+----------------------+-------------+-----------------------------+-------+-----------+----------------------------+-----------------+-------------------------------+------------+------------------+---------------------+-----------------------------+-----------------+----------------------------+--------------+------------------+-----------------------------------+------------------------
 2022-11-25 |          | 924017890     | 92401789000014         | [ND] | [ND]      | [ND]         | [ND]                   | [ND]                   | [ND]                   | NAFRev2              | 96.09Z      | 1000                        | t     | f         | f                          | f               | f                             | f          | f                | f                   | f                           | f               | f                          | f            | f                | f                                 | f
            |          | 924017890     | 92401789000014         |      |           |              |                        |                        |                        | NAFRev2              | 96.09Z      | 1000                        | t     | f         | f                          | f               | f                             | f          | f                | f                   | f                           | f               | f                          | f            | f                | f                                 | f
(2 rows)

--

CREATE MATERIALIZED VIEW entreprise_chiffre_d_affaires_3_dernier_vue AS
WITH ranked_data AS (
  SELECT
    entreprise_id,
    exercice_cloture_date,
    chiffre_d_affaires,
    ROW_NUMBER() OVER (PARTITION BY entreprise_id ORDER BY exercice_cloture_date DESC) AS rn
  FROM entreprise_ratio_financier
  WHERE exercice_cloture_date >= '2020-01-01'
)
SELECT entreprise_id, exercice_cloture_date, chiffre_d_affaires
FROM ranked_data
WHERE rn <= 3
;

CREATE INDEX idx_entreprise_chiffre_d_affaires_3_dernier_vue__entreprise
ON entreprise_chiffre_d_affaires_3_dernier_vue(entreprise_id);

--

select start_date,
  string_agg(naf_type_id || ' - ' || count, ', ')
from (
    select start_date::date,
      commune_id,
      naf_type_id,
      count(distinct etablissement_id)
    from etablissement_periode p
      join etablissement_adresse_1 a using(etablissement_id)
      join lateral (
        select start_date,
          start_date + interval '1' month as end_date
        from (
            select date_trunc('month', NOW()) - (interval '1' month * generate_series(1, 1)) as start_date
          ) dates
      ) dates on true
    where debut_date <= start_date
      and (
        fin_date is null
        or fin_date > end_date
      )
      and actif
      and commune_id = '75108'
    group by commune_id,
      naf_type_id,
      start_date,
      end_date
    order by 1,
      2,
      3
  ) toto
group by start_date;


select start_date,
  string_agg(categorie_juridique_type_id || ' - ' || count, ', ')
from (
    select start_date::date,
      commune_id,
      categorie_juridique_type_id,
      count(distinct etablissement_id)
    from entreprise_periode p
      join etablissement_adresse_1 a ON LEFT(a.etablissement_id, 9) = p.entreprise_id
      join lateral (
        select start_date,
          start_date + interval '1' month as end_date
        from (
            select date_trunc('month', NOW()) - (interval '1' month * generate_series(1, 1)) as start_date
          ) dates
      ) dates on true
    where debut_date <= start_date
      and (
        fin_date is null
        or fin_date > end_date
      )
      and actif
      and commune_id = '01002'
    group by commune_id,
      categorie_juridique_type_id,
      start_date,
      end_date
    order by 1,
      2,
      3
  ) toto
group by start_date;

{
  "id": 402,
  "email": "deveco.anct.veronique@gmail.com",
  "nom": "Liliane",
  "prenom": "Catherine",
  "actif": true,
  "aUnMotDePasse": true,
  "montrerTutoriel": false,
  "compteTypeId": "deveco",
  "equipe": {
    "id": 265,
    "groupement": false,
    "nom": "CA du Pays de Saint Malo Agglomération",
    "territoireTypeId": "epci",
    "typeId": "CA"
  },
  "bienvenueEmail": true,
  "iat": 1704979674,
  "exp": 1707571674,
  "sub": "402"
}


select *
from evenement__equipe_etablissement eee
join evenement e on eee.evenement_id = e.id
and e.diff::text ilike '"Qualification d''établissements en masse%';

select *
from evenement__equipe_etablissement eee
join evenement e on eee.evenement_id = e.id
where e.diff::text ilike '"Modification des étiquettes%';

evenement__equipe_etablissement_evenement_id_evenement_id_fk
evenement__equipe_etablissement_etablissement_id_etablissement_
evenement__equipe_etablissement_equipe_id_equipe_id_fk

-- lister toutes les contraintes

SELECT con.conname
FROM pg_catalog.pg_constraint con
  INNER JOIN pg_catalog.pg_class rel ON rel.oid = con.conrelid
  INNER JOIN pg_catalog.pg_namespace nsp ON nsp.oid = connamespace;

  -- sur prod : (5055 rows)

SELECT 'ALTER TABLE '||t.relname||' DROP CONSTRAINT '||c.conname||';'
from pg_constraint c
  join pg_class t on c.conrelid = t.oid
  join pg_namespace n on t.relnamespace = n.oid
where t.relname ilike 'action_%' and c.conname ilike 'evenement_%';


DETAIL:  Key (etablissement_id, equipe_id)=(51010824400037, 62) is not present in table "equipe__etablissement".


ALTER TABLE "action__compte" RENAME TO "action_compte";--> statement-breakpoint
ALTER TABLE "action__contact" RENAME TO "action_contact";--> statement-breakpoint
ALTER TABLE "action__demande" RENAME TO "action_demande";--> statement-breakpoint
ALTER TABLE "action__echange" RENAME TO "action_echange";--> statement-breakpoint
ALTER TABLE "action__entreprise" RENAME TO "action_entreprise";--> statement-breakpoint
ALTER TABLE "action__equipe_etablissement" RENAME TO "action_equipe_etablissement";--> statement-breakpoint
ALTER TABLE "action__etablissement" RENAME TO "action_etablissement";--> statement-breakpoint
ALTER TABLE "action__etablissement_creation" RENAME TO "action_etablissement_creation";--> statement-breakpoint
ALTER TABLE "action__etiquette" RENAME TO "action_etiquette";--> statement-breakpoint
ALTER TABLE "action__etiquette__etablissement" RENAME TO "action_etiquette__etablissement";--> statement-breakpoint
ALTER TABLE "action__etiquette__etablissement_creation" RENAME TO "action_etiquette__etablissement_creation";--> statement-breakpoint
ALTER TABLE "action__local" RENAME TO "action_local";--> statement-breakpoint
ALTER TABLE "action__personne" RENAME TO "action_personne";--> statement-breakpoint
ALTER TABLE "action__proprietaire_etablissement" RENAME TO "action_proprietaire_etablissement";--> statement-breakpoint
ALTER TABLE "action__proprietaire_personne" RENAME TO "action_proprietaire_personne";--> statement-breakpoint
ALTER TABLE "action__rappel" RENAME TO "action_rappel";--> statement-breakpoint

-- reset sequences

DO $$
DECLARE
    table_record record;
BEGIN
    FOR table_record IN (
      SELECT sequence_schema, sequence_name
      FROM information_schema.sequences sequence_schema = 'public'
    )
    LOOP
        SELECT setval(
            table_record.sequence_name,
            (SELECT MAX(id) FROM table_record.sequence_name)
        );
    END LOOP;
END $$;

--

INSERT INTO action_etiquette__etablissement (
  "evenement_id",
  "etiquette_id",
  "equipe_id",
  "etablissement_id",
  "diff",
  "created_at",
  "action_type_id"
)
SELECT
    2559304,
    24451,
    558,
    "equipe__etablissement__etiquette"."etablissement_id",
    '{ "changes": { "message": "Modification des étiquettes : Ajout d''étiquettes", "after": [24451]
		}}',
    '2024-01-22 06:50:25.39073+00',
    'modification'
FROM "equipe__etablissement__etiquette"
WHERE "equipe__etablissement__etiquette"."equipe_id" = 558
    AND "equipe__etablissement__etiquette"."etiquette_id" = 24451
;


 ALTER TABLE action_compte DROP CONSTRAINT fk_evenement__compte__compte;
 ALTER TABLE action_compte DROP CONSTRAINT fk_evenement__compte__evenement;
 ALTER TABLE action_contact DROP CONSTRAINT fk_evenement__contact__contact;
 ALTER TABLE action_contact DROP CONSTRAINT fk_evenement__contact__evenement;
 ALTER TABLE action_echange DROP CONSTRAINT fk_evenement__echange__echange;
 ALTER TABLE action_echange DROP CONSTRAINT fk_evenement__echange__evenement;
 ALTER TABLE action_demande DROP CONSTRAINT fk_evenement__demande__demande;
 ALTER TABLE action_demande DROP CONSTRAINT fk_evenement__demande__evenement;
 ALTER TABLE action_entreprise DROP CONSTRAINT fk_evenement__entreprise__entreprise;
 ALTER TABLE action_entreprise DROP CONSTRAINT fk_evenement__entreprise__evenement;
 ALTER TABLE action_etablissement DROP CONSTRAINT fk_evenement__etablissement__etablissement;
 ALTER TABLE action_etablissement DROP CONSTRAINT fk_evenement__etablissement__evenement;
 ALTER TABLE action_etablissement_creation DROP CONSTRAINT fk_evenement__etablissement_creation__etablissement_creation;
 ALTER TABLE action_etablissement_creation DROP CONSTRAINT fk_evenement__etablissement_creation__evenement;
 ALTER TABLE action_equipe_etablissement DROP CONSTRAINT fk_evenement__equipe_etablissement__equipe;
 ALTER TABLE action_equipe_etablissement DROP CONSTRAINT fk_evenement__equipe_etablissement__etablissement;
 ALTER TABLE action_equipe_etablissement DROP CONSTRAINT fk_evenement__equipe_etablissement__evenement;
 ALTER TABLE action_etiquette__etablissement DROP CONSTRAINT fk_evenement__etiquette__etablissement__equipe;
 ALTER TABLE action_etiquette__etablissement DROP CONSTRAINT fk_evenement__etiquette__etablissement__etablissement;
 ALTER TABLE action_etiquette__etablissement DROP CONSTRAINT fk_evenement__etiquette__etablissement__etiquette;
 ALTER TABLE action_etiquette__etablissement DROP CONSTRAINT fk_evenement__etiquette__etablissement__evenement;
 ALTER TABLE action_etiquette__etablissement_creation DROP CONSTRAINT fk_evenement__etiquette__etablissement_creation;
 ALTER TABLE action_etiquette__etablissement_creation DROP CONSTRAINT fk_evenement__etiquette__etablissement_creation__etiquette;
 ALTER TABLE action_etiquette__etablissement_creation DROP CONSTRAINT fk_evenement__etiquette__etablissement_creation__evenement;
 ALTER TABLE action_etiquette DROP CONSTRAINT fk_evenement__etiquette__etiquette;
 ALTER TABLE action_etiquette DROP CONSTRAINT fk_evenement__etiquette__evenement;
 ALTER TABLE action_personne DROP CONSTRAINT fk_evenement__personne__evenement;
 ALTER TABLE action_personne DROP CONSTRAINT fk_evenement__personne__personne;
 ALTER TABLE action_local DROP CONSTRAINT fk_evenement__local__evenement;
 ALTER TABLE action_local DROP CONSTRAINT fk_evenement__local__local;
 ALTER TABLE action_proprietaire_etablissement DROP CONSTRAINT fk_evenement__proprietaire_etablissement__etablissement;
 ALTER TABLE action_proprietaire_etablissement DROP CONSTRAINT fk_evenement__proprietaire_etablissement__evenement;
 ALTER TABLE action_proprietaire_etablissement DROP CONSTRAINT fk_evenement__proprietaire_etablissement__local;
 ALTER TABLE action_proprietaire_personne DROP CONSTRAINT fk_evenement__proprietaire_personne__evenement;
 ALTER TABLE action_proprietaire_personne DROP CONSTRAINT fk_evenement__proprietaire_personne__local;
 ALTER TABLE action_proprietaire_personne DROP CONSTRAINT fk_evenement__proprietaire_personne__personne;
 ALTER TABLE action_rappel DROP CONSTRAINT fk_evenement__rappel__evenement;
 ALTER TABLE action_rappel DROP CONSTRAINT fk_evenement__rappel__rappel;


SELECT count(*)
FROM evenement
WHERE id NOT IN (
  SELECT evenement_id
  FROM action_compte
  UNION
  SELECT evenement_id
  FROM action_contact
  UNION
  SELECT evenement_id
  FROM action_demande
  UNION
  SELECT evenement_id
  FROM action_echange
  UNION
  SELECT evenement_id
  FROM action_entreprise
  UNION
  SELECT evenement_id
  FROM action_equipe_etablissement
  UNION
  SELECT evenement_id
  FROM action_etablissement
  UNION
  SELECT evenement_id
  FROM action_etablissement_creation
  UNION
  SELECT evenement_id
  FROM action_etiquette
  UNION
  SELECT evenement_id
  FROM action_etiquette__etablissement
  UNION
  SELECT evenement_id
  FROM action_etiquette__etablissement_creation
  UNION
  SELECT evenement_id
  FROM action_local
  UNION
  SELECT evenement_id
  FROM action_proprietaire_etablissement
  UNION
  SELECT evenement_id
  FROM action_proprietaire_personne
  UNION
  SELECT evenement_id
  FROM action_rappel
  UNION
  SELECT evenement_id
  FROM action_personne
)
;

alter table mandataire_personne_morale drop constraint fk_mandataire_personne_morale__mandataire_entreprise;

--- correction des géolocalisations d'équipe établissement à partir des adresse étalab

update equipe__etablissement ee
    set geolocalisation = etalab.geolocalisation
from etablissement_adresse_1_etalab etalab
where
  ee.etablissement_id = etalab.etablissement_id and
  equipe_id = 160 and
  ee.geolocalisation is null and
  etalab.geolocalisation is not null and score > 0.5;

SELECT equipe_id, equipe.nom, created_at::date
FROM (
  SELECT DISTINCT ON (equipe_id)
    equipe_id,
    e.created_at
  FROM evenement e
  JOIN compte c ON c.id = e.compte_id
  JOIN deveco d ON d.compte_id = c.id
  ORDER BY d.equipe_id, e.created_at DESC
) data
JOIN equipe ON equipe.id = equipe_id
WHERE created_at < '2023-01-01'
AND created_at > '2022-01-01'
ORDER BY created_at;


 equipe_id |              nom               | created_at
-----------+--------------------------------+------------
       111 | La Rochelle (17)               | 2022-06-07
        56 | CC Haut-Jura Arcade Communauté | 2022-06-07
       110 | Gennevilliers (92)             | 2022-06-07
        86 | CA du Sicoval                  | 2022-06-10
        43 | CA Porte de l''Isère (C.A.P.I) | 2022-07-13
       162 | CC Aure Louron                 | 2022-10-25
       157 | CA de Chaumont                 | 2022-10-26
       152 | CC de Villedieu Intercom       | 2022-10-28
       154 | CA du Sud                      | 2022-12-08
(9 rows)

RAISE unique_violation USING MESSAGE = 'Duplicate user ID: ' || user_id;

DO
$$
  DECLARE
    equipe_a_faire RECORD;
  BEGIN
    FOR equipe_a_faire IN
      SELECT
        data.equipe_id,
        ROW_NUMBER() OVER (ORDER BY data.created_at) as ligne,
        (SELECT count(*) from data) as total
      FROM (
        SELECT DISTINCT ON (d.equipe_id)
          d.equipe_id,
          e.created_at
        FROM evenement e
        JOIN compte c ON c.id = e.compte_id
        JOIN deveco d ON d.compte_id = c.id
        ORDER BY d.equipe_id, e.created_at DESC
      ) data
      JOIN equipe ON equipe.id = data.equipe_id
      AND data.created_at > '2024-01-15'
      GROUP BY data.equipe_id, data.created_at
    LOOP
      RAISE NOTICE '% Equipe : %', timeofday(), equipe_a_faire.equipe_id;

      UPDATE equipe__etablissement ee
      SET geolocalisation = etalab.geolocalisation
      FROM etablissement_adresse_1_etalab etalab
      WHERE
        ee.etablissement_id = etalab.etablissement_id AND
        ee.equipe_id = equipe_a_faire.equipe_id AND
        ee.geolocalisation IS NULL AND
        etalab.geolocalisation IS NOT NULL AND score > 0.5;

      RAISE NOTICE '% Equipe : %, fait', timeofday(), equipe_a_faire.equipe_id;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;



[
  '{{repeat(1000, 1000)}}',
  {
    invar: function (tags) {
      return [0,1,2,3,4,5,6,7,8,9,10].map(function (truc) { return tags.integer(0, 9); } ).join("");
    },
    dnvoiri: function (tags) {
      return '000' + tags.integer(1, 9);
    },
    dindic:  function (tags) {
      var indics = ['', '', '', '', '', '', '', '', '', '', 'bis', 'bis', 'ter'];
      var rand = tags.integer(0, indics.length - 1);

      return indics[rand];
    },
    dvoilib: function (tags) {
      var prefixes = ["rue de", "impasse du", "avenue de", "boulevard de"];
      var rand = tags.integer(0, prefixes.length - 1);
      var prefix = prefixes[rand];

      var name = tags.street();

      return prefix + ' ' + name;
    },
	idcom:   function (tags) {
      var communes = ["92004", "92009", "92024", "92025", "92036", "92078", "95018", "97501", "97502", "97701", "97801", "98411", "98412", "98413", "98414", "98415", "98611", "98612", "98613", "98711", "98712", "98713", "98714", "98715", "98716", "98717", "98718", "98719", "98720", "98721", "98722", "98723", "98724", "98725", "98726", "98727", "98728", "98729", "98730", "98731", "98732", "98733", "98734", "98735", "98736", "98737", "98738", "98739", "98740", "98741", "98742", "98743", "98744", "98745", "98746", "98747", "98748", "98749", "98750", "98751", "98752", "98753", "98754", "98755", "98756", "98757", "98758", "98801", "98802", "98803", "98804", "98805", "98806", "98807", "98808", "98809", "98810", "98811", "98812", "98813", "98814", "98815", "98816", "98817", "98818", "98819", "98820", "98821", "98822", "98823", "98824", "98825", "98826", "98827", "98828", "98829", "98830", "98831", "98832", "98833", "98901"];
      var rand = tags.integer(0, communes.length - 1);

      return communes[rand];
    },
	ccosec: function (tags) {
      var randomLetters = [0,1].map(function (truc) { return String.fromCharCode(65 + tags.integer(0, 25)); } ).join("");

      return randomLetters;
    },
	dnupla: function (tags) {
      var randomNumber = [0,1,2].map(function (truc) { return tags.integer(0, 9); } ).join("");

      return '0' + randomNumber;
    },
    cconlc:  function (tags) {
      var codes = ['CA', 'CB', 'CD', 'CM', 'ME', 'PP', 'U', 'U1', 'US'];
      var rand = tags.integer(0, codes.length - 1);

      return codes[rand];
    },
    typeact: function (tags) {
      var categories = ["ATE1", "ATE2", "ATE3", "BUR1", "BUR2", "BUR3", "CLI1", "CLI2", "CLI3", "CLI4", "DEP1", "DEP2", "DEP3", "DEP4", "DEP5", "ENS1", "ENS2", "EXC1", "HOT1", "HOT2", "HOT3", "HOT4", "HOT5", "IND1", "IND2", "MAG1", "MAG2", "MAG3", "MAG4", "MAG5", "MAG6", "MAG7", "SPE1", "SPE2", "SPE3", "SPE4", "SPE5", "SPE6", "SPE7"];
      var rand = tags.integer(0, categories.length - 1);

      return categories[rand];
    },
    dniv: function (tags) {
      return '0' + tags.integer(0,9);
    },
    sprincp: '{{integer(0,120)}}',
	ssecp: '{{integer(0,120)}}',
	ssecncp: '{{integer(0,120)}}',
	sparkp: '{{integer(0,120)}}',
	sparkncp: '{{integer(0,120)}}',
    idprocpte: function (tags) {
      var randomLetter = String.fromCharCode(65 + tags.integer(0, 25));
      var randomNumber = [0,1,2,3,4].map(function (truc) { return tags.integer(0, 9); } ).join("");

      return this.idcom + randomLetter + randomNumber;
    }
  }
]


-- migration 33
ALTER TABLE source.source_cerema_local ADD COLUMN geomloc geometry(Point,2154);
ALTER TABLE equipe__local ADD COLUMN geolocalisation geometry(Point,4326);

ALTER TABLE equipe_local_contribution ALTER COLUMN actif DROP NOT NULL;
ALTER TABLE equipe__local ALTER COLUMN contribution_actif DROP NOT NULL;
ALTER TABLE equipe__local ALTER COLUMN local_categorie_type_id DROP NOT NULL;


ALTER TABLE equipe__local ALTER COLUMN local_id SET DATA TYPE varchar(12);
ALTER TABLE equipe__local ALTER COLUMN local_id SET NOT NULL;
ALTER TABLE equipe__local ADD COLUMN invariant varchar(10);

ALTER TABLE local ALTER COLUMN local_id SET DATA TYPE varchar(12);
ALTER TABLE local ALTER COLUMN local_id SET NOT NULL;
ALTER TABLE local ADD COLUMN invariant varchar(10);
ALTER TABLE local ALTER COLUMN section SET DATA TYPE varchar(2);
ALTER TABLE local ADD COLUMN parcelle varchar(4);

ALTER TABLE local ALTER COLUMN local__proprietaire_etablissement SET DATA TYPE varchar(12);
ALTER TABLE local ALTER COLUMN local__proprietaire_etablissement SET NOT NULL;
ALTER TABLE local ALTER COLUMN local__proprietaire_personne SET DATA TYPE varchar(12);
ALTER TABLE local ALTER COLUMN local__proprietaire_personne SET NOT NULL;

ALTER TABLE equipe_local_contribution ALTER COLUMN local_id SET DATA TYPE varchar(12);
ALTER TABLE equipe_local_contribution ALTER COLUMN local_id SET NOT NULL;
ALTER TABLE equipe_local__etiquette ALTER COLUMN local_id SET DATA TYPE varchar(12);
ALTER TABLE action_equipe_local ALTER COLUMN local_id SET DATA TYPE varchar(12);
ALTER TABLE action_equipe_local_contact ALTER COLUMN local_id SET DATA TYPE varchar(12);

ALTER TABLE equipe_local__etiquette ALTER COLUMN local_id SET NOT NULL;
ALTER TABLE action_equipe_local ALTER COLUMN local_id SET NOT NULL;
ALTER TABLE action_equipe_local_contact ALTER COLUMN local_id SET NOT NULL;

update local
set invariant = invar, parcelle = dnupla
from source.source_cerema_local s
where local.id = (LEFT(s.idcom, 2) || s.invar);

update equipe__local e
set invariant = l.invariant
from local l
where l.id = e.local_id;

ALTER TABLE equipe__local ALTER COLUMN invariant SET NOT NULL;
ALTER TABLE local ALTER COLUMN invariant SET NOT NULL;

-- fixtures proprio

-- moral

-- idprocpte,dnulp,dsiren
-- 11111111111,01,111111111

-- physique

-- idprocpte,dnulp,dnomus,dprnus,dnomlp,dprnlp,jdatnss,dldnss
-- 11111111111,01,DUPONT,CUNEGONDE,DUPONT,CUNEGONDE BERTILLE,01/01/1901,01 UN



update local set voie_nom = replace(voie_nom, '  ', ' ');
update equipe__local set voie_nom = replace(voie_nom, '  ', ' ');
update equipe__local set numero = regexp_replace(numero, '^0*', '', 'g');

INSERT INTO categorie_juridique_type (id, nom, niveau, parent_categorie_juridique_type_id) VALUES ('0001','Indéfini', 3,'00');

-- suivi de l'établissement par d'autres équipes

select equipe_id, json_agg(json_build_object('type', type, 'count', count))
from (
    select
      equipe_id,
      count(*),
      'contact' as type
    from equipe__etablissement__contact
    where etablissement_id = '30537862200016' and equipe_id <> 161
    group by equipe_id
    union
    select
      equipe_id,
      count(*),
      'demande' as type
    from equipe__etablissement__demande
    where etablissement_id = '30537862200016' and equipe_id <> 161
    group by equipe_id
    union
    select
      equipe_id,
      count(*),
      'rappel' as type
    from equipe__etablissement__rappel
    where etablissement_id = '30537862200016' and equipe_id <> 161
    group by equipe_id
    union
    select
      equipe_id,
      count(*),
      'echange' as type
    from equipe__etablissement__echange
    where etablissement_id = '30537862200016' and equipe_id <> 161
    group by equipe_id
  ) t
  group by equipe_id;

select "equipe__etablissement"."etablissement_id"
from "equipe__etablissement"
  left join "equipe__etablissement_contribution" on (
    "equipe__etablissement_contribution"."etablissement_id" = "equipe__etablissement"."etablissement_id"
    and "equipe__etablissement_contribution"."equipe_id" = "equipe__etablissement"."equipe_id"
  )
where (
    "equipe__etablissement"."equipe_id" = $1
    and exists (
      (
        select 1
        from (
            select SUM(REPLACE("montant", ',', '.')::float) as "total"
            from "source"."dgcl__subventions"
            where (
              DATE_PART('year', "dgcl__subventions"."convention_date"::timestamp) in ('2022')
            )
            group by "dgcl__subventions"."siret"
          ) "t"
        where "total" >= 5000
      )
    )
  )
order by "equipe__etablissement"."consultation_at" desc nulls last
limit $5

--

WITH equipesssss AS (
  select id
  from equipe
  where id not in (
      select distinct equipe_id
      from equipe__local
    )
    and id not in (1441, 1592, 1640)
)
INSERT INTO equipe__local (
    equipe_id,
    local_id,
    commune_id,
    invariant,
    code_postal,
    numero,
    voie_nom,
    local_categorie_type_id,
    local_nature_type_id
  )
SELECT ecv.equipe_id,
  local.id,
  local.commune_id,
  local.invariant,
  adresse.code_postal,
  adresse.numero,
  adresse.voie_nom,
  local.local_categorie_type_id,
  local.local_nature_type_id
FROM local
  JOIN equipe__commune_vue ecv USING (commune_id)
  JOIN local_adresse adresse ON adresse.local_id = local.id
WHERE ecv.equipe_id IN (
    select id
    from equipesssss
  );

-- create table etablissement__adresse__adresse_etalab as (
--   select e.id,
--     ea.geolocalisation,
--     eae.geolocalisation as geolocalisation_etalab
--   from etablissement e
--   left join etablissement_adresse_1 as ea on (ea.etablissement_id = e.id)
--   left join etablissement_adresse_1_etalab as eae on (eae.etablissement_id = e.id)
-- );
