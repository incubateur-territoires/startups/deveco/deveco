# Critère de recherche des établissements

```mermaid
erDiagram
    %% données

    %% données publiques
    etablissement }|--|| siren : "possède"
    etablissement }|--|| siret : "possède"
    etablissement }|--|| nom : "possède"
    etablissement }|--|| etat_administratif : "a"
    etablissement }|--|| code_naf : "possède"
    etablissement }o--|| creation_date : "possède"
    etablissement }o--|| categorie_juridique : "a"
    categorie_juridique }|--o| categorie_juridique_courante : "fait partie"
    etablissement }o--o| ess : "est"
    etablissement }o--o| zrr : "est en"

    %% données personnalisées de l'équipe
    equipe }o--o{ exogene : "ajoute"
    etablissement }o--o| exogene : "est"
    etablissement }o--o| ancien_createur : "est"
    etablissement }o--o| portefeuille : "appartient"
    etablissement }o--o| consultation_date : "possède"

    %% recherche

    %% données publiques
    recherche }o--o{ nom : "filtre"
    recherche }o--o| siren : "filtre"
    recherche }o--o| siret : "filtre"
    recherche }o--o| etat_administratif : "filtre"
    recherche }o--o| categorie_juridique_courante : "filtre"
    recherche }o--o| ess : "filtre"
    recherche }o--o{ effectif : "filtre"
    recherche }o--o{ code_naf : "filtre"
    recherche }o--o| adresse : "filtre"
    recherche }o--o{ commune : "filtre"
    recherche }o--o{ qpv : "filtre"
    recherche }o--o| creation_date : "trie"

    %% données personnalisées de l'équipe
    recherche }o--o{ contact : "filtre"
    recherche }o--o| exogene : "filtre"
    recherche }o--o| ancien_createur : "filtre"
    recherche }o--o| favori : "filtre"
    recherche }o--o| portefeuille : "filtre"
    recherche }o--o{ etiquette : "filtre"
    recherche }o--o{ etiquette_type : "filtre"
    recherche }o--o{ demande : "filtre"
    recherche }o--o{ zonage : "filtre"
    recherche }o--o| consultation_date : "trie"
```
