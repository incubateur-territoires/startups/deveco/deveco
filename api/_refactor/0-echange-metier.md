# Échange métier

## Questions

### Nommage des champs

[#602](https://gitlab.com/incubateur-territoires/startups/deveco/deveco-app/-/issues/602)

JB:

- l'entité = le nom de la structure juridique (aujourd'hui, c'est indiqué "territoire")
- le territoire = le périmètre géographique (aujourd'hui, c'est indiqué "libellé géographique")
- la catégorie de l'entité = commune, CC, CA, CU, EPT, Métropole, PETR, Conseil départemental, Conseil régional, Agence régionale de développement éco, Agence locale de développement éco, Préfecture, DREETS, Autres (aujourd'hui, c'est indiqué " type de la géographie")
- la catégorie du territoire : communal, intercommunal, métropolitain, départemental, interdépartemental, régional, interrégional, autres

Dev:

- Nom de l'équipe : nom choisi par l'admin
- Type de territoire : commune, epci, petr, metropole, département, région
- Territoires : liste du ou des territoires du type sélectionné
- Type d'équipe :

  | type de territoire | type d'équipe                                                                 |
  | ------------------ | ----------------------------------------------------------------------------- |
  | commune            | Agence locale de développement éco, commune, mairie                           |
  | epci               | Agence locale de développement éco ?, CC, CA, CU, EPT, Métropole              |
  | petr               | Agence locale de développement éco ?, PETR                                    |
  | departement        | Agence départementale de développement éco, Conseil départemental, Préfecture |
  | region             | Agence régionale de développement éco, Conseil régional, DREETS               |

```code
Nom de l'équipe : Thionville Commerces
Type de territoire : commune
Territoires : Thionville (57)
Type d'équipe : Agence locale de développement éco

Nom de l'équipe : Thionville
Type de territoire : commune
Territoires : Thionville (57)
Type d'équipe : Commune

Nom de l'équipe : Préfecture du Territoire de Belfort
Type de territoire : département
Territoires : Territoire de Belfort
Type d'équipe : Préfecture

Nom de l'équipe : So Nord Franche-Comté
Type de territoire : département
Territoires : CA Pays de Montbéliard Agglomération + CA Grand Belfort + CC du Sud Territoires + CC des Vosges du Sud
Type d'équipe : Agence locale de développement éco
```

### Nom des établissements et entreprises

Dans la base SIRENE, plusieurs champs qui contiennent des informations sur le nom.

Entreprise

```code
unite_legale {
  - categorie_entreprise 1000 (PP) ou autre (PM)

  personne physique
  - sexe_unite_legale
  - prenom1_unite_legale
  - prenom2_unite_legale
  - prenom3_unite_legale
  - prenom4_unite_legale
  - prenom_usuel_unite_legale
  - pseudonyme_unite_legale
  - nom_unite_legale
  - nom_usage_unite_legale

  personne morale
  - sigle_unite_legale
  - denomination_unite_legale
  - denomination_usuelle1_unite_legale
  - denomination_usuelle2_unite_legale
  - denomination_usuelle3_unite_legale
}

unite_legale_periode {
  - nom_unite_legale
  - nom_usage_unite_legale

  - denomination_unite_legale
  - denomination_usuelle1_unite_legale
  - denomination_usuelle2_unite_legale
  - denomination_usuelle3_unite_legale
}
```

Établissement

```code
etablissement {
  - siret

  - enseigne1_etablissement
  - enseigne2_etablissement
  - enseigne3_etablissement
  - denomination_usuelle_etablissement
}

etablissement_periode {
  - enseigne1_etablissement
  - enseigne2_etablissement
  - enseigne3_etablissement

  - denomination_usuelle_etablissement
}
```

On aimerait pouvoir transformer ces champs, pour afficher :

- un nom pour l'entreprise
- un nom pour l'établissement

Questions :

- Doit-on afficher le même nom dans les différents composants qui représente un établissement ou une entreprise (pe: vue en liste ou page de détail) ? (réponse: oui :))
- Comment générer les noms des établissements et des entreprise ?

```sql
  CASE
    WHEN U.categorie_juridique_unite_legale = '1000' THEN CASE
      WHEN U.sigle_unite_legale IS NOT NULL THEN COALESCE(LOWER(U.prenom1_unite_legale), '') || COALESCE(' ' || LOWER(U.nom_unite_legale), '') || COALESCE(' (' || LOWER(U.sigle_unite_legale) || ')', '')
      ELSE COALESCE(LOWER(U.prenom1_unite_legale), '') || COALESCE(' ' || LOWER(U.nom_unite_legale), '')
    END
    ELSE CASE
      WHEN U.sigle_unite_legale IS NOT NULL THEN COALESCE(LOWER(U.denomination_unite_legale), '') || COALESCE(LOWER(' (' || U.sigle_unite_legale || ')'), '')
      ELSE COALESCE(LOWER(U.denomination_unite_legale), '')
    END
  END as nom_public,

  COALESCE(LOWER(U.prenom1_unite_legale), '') || COALESCE(' ' || LOWER(U.nom_unite_legale), '') || COALESCE(' ' || LOWER(U.nom_usage_unite_legale), '') || COALESCE(' ' || LOWER(U.sigle_unite_legale), '') || COALESCE(' ' || LOWER(U.denomination_unite_legale), '') || COALESCE(
    ' ' || LOWER(U.denomination_usuelle1_unite_legale),
    ''
  ) || COALESCE(
    ' ' || LOWER(U.denomination_usuelle2_unite_legale),
    ''
  ) || COALESCE(
    ' ' || LOWER(U.denomination_usuelle3_unite_legale),
    ''
  ) || COALESCE(' ' || LOWER(SE.enseigne1_etablissement), '') || COALESCE(' ' || LOWER(SE.enseigne2_etablissement), '') || COALESCE(' ' || LOWER(SE.enseigne3_etablissement), '') || COALESCE(
    ' ' || LOWER(SE.denomination_usuelle_etablissement),
    ''
  ) as nom_recherche,
  COALESCE(
    SE.enseigne1_etablissement,
    SE.enseigne2_etablissement,
    SE.enseigne3_etablissement,
    SE.denomination_usuelle_etablissement,
    ''
  ) as enseigne,
```

## Infos

### Entreprises

Aujourd'hui, les interactions (rappels, échanges, demandes, contacts) sont liées à un établissement.
Dans la nouvelle modélisation, on ajoute les entreprises.

Y a-t-il un besoin métier d'interagir avec les entreprises ?

- juste afficher les infos de l'entreprise sur un étabissement
- pouvoir lister les entreprises
- afficher une page d'entreprise avec la liste de ses établissements
- interagir au niveau de l'entreprise (étiquettes, échanges, rappels, demandes, contacts)

### Échange / demande

- un échange doit-il être associé à une personne ?
- une demande peut-elle être liée à plusieurs échanges ?

## Établissements inactifs

- UX: est ce qu'on peut séparer les établissements actifs et inactifs ?

## Mandataires sociaux

- doit-on afficher les mandataire sociaux (personnes morales) ?

## Contacts

- Peut on afficher les mandataires sociaux (personnes physiques) sur l'encart entreprise, pour proposer de les transformer en contacts ?
