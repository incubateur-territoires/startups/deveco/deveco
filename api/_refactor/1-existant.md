# Existant

## Mise à jour des données Sirene

cron tous les jours:

1. requête API SIRENE et on met à jour les donnés dans les tables `sirene_etablissement`, `sirene_unite_legale`

2. on transforme les données de `sirene_etablissement`, `sirene_unite_legale`, `naf` et `geoloc_entreprise` vers `etablissements` :

   - copie de données
   - génère certains champs en fonction des valeurs d'autres, pour la recherche (catégorie juridique courante, nom recherche) ou l'affichage (enseigne)

3. on met à jour la table `entreprise`

4. on met à jour la table `TER`

## Ajout d'une équipe

- on copie longitude,latitude depuis `geoloc_entreprise` vers `etablissement`
  - on peut potentiellement le faire pour un établissement alors que ça a déjà été fait avant
  - c'est potentiellement très long

## Recherche

- on utilise des critères :
  - publiques =>
    - `TER` (nom, adresse, naf, cat juridique, zrr, qpv, etc.)
    - exception : `exogène`, qui dépend des communes de l'équipe, mais basé sur des données publiques
  - personalisés =>
    - `entité` (`étiquettes`, `contact`, `future enseigne`, `création d'établissement`, `exogène`)
    - `fiche` (`échange`, `rappel`, `demande`, etc.)

## Affichage d'une page d'un établissement

- 1ère visualisation : requête API entreprise
  - `mandataires sociaux`
  - `exercices`
- crée `entité` et `entreprise` si besoin
- récupère l'`entité` et ses relations
  - `entreprise`
    - `exercices`
    - `mandataires sociaux`
  - `contacts`
  - `fiche`
    - `demandes`
    - `echanges`
    - `rappels`
    - ...
  - `etablissement créé` ou `créateur lié`
