
ALTER TABLE source_api_sirene_etablissement ADD COLUMN mis_a_jour_date timestamptz;
CREATE INDEX IDX_source_api_sirene_etablissement ON source_api_sirene_etablissement(mise_a_jour_at);

ALTER TABLE source_api_sirene_unite_legale ADD COLUMN mis_a_jour_date timestamptz;
CREATE INDEX IDX_source_api_sirene_unite_legale ON source_api_sirene_unite_legale(mise_a_jour_at);

ALTER TABLE equipe__etablissement_exogene RENAME COLUMN ajout_date to ajout_at;

CREATE TABLE tache (
    id SERIAL NOT NULL,
    nom VARCHAR NOT NULL,
    debut_at timestamptz DEFAULT NULL,
    en_cours_at timestamptz DEFAULT NULL,
    fin_at timestamptz DEFAULT NULL,
    manuel boolean DEFAULT FALSE,
    rapport jsonb,

    PRIMARY KEY (id),
    CONSTRAINT UK_tache UNIQUE (nom, debut_at)
);

ALTER TABLE equipe_exercice ADD CONSTRAINT UK_etablissement_exercice UNIQUE (etablissement_id, cloture_date);


CREATE TABLE IF NOT EXISTS source_api_sirene_etablissement_periode (
    siret varchar(14) NOT NULL,
    activite_principale_etablissement varchar(6),
    caractere_employeur_etablissement source_sirene_etablissement_caractere_employeur_enum,
    date_debut date,
    date_fin date,
    denomination_usuelle_etablissement varchar,

    enseigne1_etablissement varchar,
    enseigne2_etablissement varchar,
    enseigne3_etablissement varchar,

    etat_administratif_etablissement source_sirene_etablissement_etat_administratif_enum,
    nomenclature_activite_principale_etablissement varchar(8),
    changement_activite_principale_etablissement varchar(5),
    changement_caractere_employeur_etablissement varchar(5),
    changement_enseigne_etablissement varchar(5),
    changement_etat_administratif_etablissement varchar(5),
    mise_a_jour_at timestamptz
);

ALTER TABLE equipe__etablissement ADD COLUMN numero VARCHAR;

UPDATE equipe__etablissement SET numero = etablissement_adresse_1.numero FROM etablissement_adresse_1 where equipe__etablissement.etablissement_id = etablissement_adresse_1.etablissement_id;

SELECT REGEXP_REPLACE(voie_nom, '^(\d+(bis|ter|quater)?) .*', '\1')
FROM personne_adresse;

UPDATE personne_adresse
SET
    numero = CASE
            WHEN voie_nom ~ '^\d+(bis|ter|quater)?'
            THEN REGEXP_REPLACE(voie_nom, '^(\d+(bis|ter|quater)?)? ?.*', '\1')
        ELSE NULL
				END,
    voie_nom = REGEXP_REPLACE(voie_nom, '^\d+(bis|ter|quater)? ', '')
		;

UPDATE local_adresse
SET
    numero = CASE
            WHEN voie_nom ~ '^\d+(bis|ter|quater)?'
            THEN REGEXP_REPLACE(voie_nom, '^(\d+(bis|ter|quater)?)? ?.*', '\1')
        ELSE NULL
				END,
    voie_nom = REGEXP_REPLACE(voie_nom, '^\d+(bis|ter|quater)? ', '');



-- source.unite_legale_periode

DELETE FROM source.source_api_sirene_unite_legale_periode
WHERE siren
IN (
	SELECT siren
	FROM (
		SELECT
			siren,
			date_debut,
			mise_a_jour_at,
			ROW_NUMBER() OVER (
				PARTITION BY siren, date_debut
				ORDER BY mise_a_jour_at DESC
			) AS row_num
		FROM source.source_api_sirene_unite_legale_periode
	) t
	WHERE row_num > 1
);

DELETE FROM source.source_api_sirene_unite_legale_periode
WHERE siren
IN (
	SELECT siren
	FROM (
		SELECT
			siren,
			date_fin,
			mise_a_jour_at,
			ROW_NUMBER() OVER (
				PARTITION BY siren, date_fin
				ORDER BY mise_a_jour_at DESC
			) AS row_num
		FROM source.source_api_sirene_unite_legale_periode
	) t
	WHERE row_num > 1
);

ALTER TABLE source.source_api_sirene_unite_legale_periode DROP CONSTRAINT IF EXISTS "uk_source_api_sirene_unite_legale_periode";

CREATE UNIQUE INDEX idx_source_api_sirene_unite_legale_periode ON source.source_api_sirene_unite_legale_periode (siren, COALESCE(date_fin, '1901-02-03'));


--  source.etablissement_periode

DELETE FROM source.source_api_sirene_etablissement_periode
WHERE siret
IN (
	SELECT siret
	FROM (
		SELECT
			siret,
			date_debut,
			mise_a_jour_at,
			ROW_NUMBER() OVER (
				PARTITION BY siret, date_debut
				ORDER BY mise_a_jour_at DESC
			) AS row_num
		FROM source.source_api_sirene_etablissement_periode
	) t
	WHERE row_num > 1
);

DELETE FROM source.source_api_sirene_etablissement_periode
WHERE siret
IN (
	SELECT siret
	FROM (
		SELECT
			siret,
			date_fin,
			mise_a_jour_at,
			ROW_NUMBER() OVER (
				PARTITION BY siret, date_fin
				ORDER BY mise_a_jour_at DESC
			) AS row_num
		FROM source.source_api_sirene_etablissement_periode
	) t
	WHERE row_num > 1
);

ALTER TABLE source.source_api_sirene_etablissement_periode DROP CONSTRAINT IF EXISTS "uk_source_api_sirene_etablissement_periode";

CREATE UNIQUE INDEX idx_source_api_sirene_etablissement_periode ON source.source_api_sirene_etablissement_periode (siret, COALESCE(date_fin, '1901-02-03'));

-- entreprise_periode

DELETE FROM entreprise_periode
WHERE entreprise_id
IN (
	SELECT entreprise_id
	FROM (
		SELECT
			entreprise_id,
			debut_date,
			ROW_NUMBER() OVER (
				PARTITION BY entreprise_id, debut_date
				ORDER BY fin_date DESC NULLS LAST
			) AS row_num
		FROM entreprise_periode
	) t
	WHERE row_num > 1
);

DELETE FROM entreprise_periode
WHERE entreprise_id
IN (
	SELECT entreprise_id
	FROM (
		SELECT
			entreprise_id,
			debut_date,
			fin_date,
			ROW_NUMBER() OVER (
				PARTITION BY entreprise_id, fin_date
				ORDER BY debut_date DESC NULLS LAST
			) AS row_num
		FROM entreprise_periode
	) t
	WHERE row_num > 1
);

ALTER TABLE entreprise_periode DROP CONSTRAINT IF EXISTS "uk_entreprise_periode";

CREATE UNIQUE INDEX idx_entreprise_periode ON entreprise_periode (entreprise_id, COALESCE(fin_date, '1901-02-03'));


--  source.etablissement_periode

DELETE FROM etablissement_periode
WHERE etablissement_id
IN (
	SELECT etablissement_id
	FROM (
		SELECT etablissement_id, debut_date, ROW_NUMBER()
		OVER (
			PARTITION BY etablissement_id, debut_date
			ORDER BY fin_date DESC NULLS LAST
		) AS row_num
		FROM etablissement_periode
	) t
	WHERE row_num > 1
);

DELETE FROM etablissement_periode
WHERE etablissement_id
IN (
	SELECT etablissement_id
	FROM (
		SELECT etablissement_id, fin_date, ROW_NUMBER()
		OVER (
			PARTITION BY etablissement_id, fin_date
			ORDER BY debut_date DESC NULLS LAST
		) AS row_num
		FROM etablissement_periode
	) t
	WHERE row_num > 1
);

ALTER TABLE etablissement_periode DROP CONSTRAINT IF EXISTS "uk_etablissement_periode";

CREATE UNIQUE INDEX idx_etablissement_periode ON etablissement_periode (etablissement_id, COALESCE(fin_date, '1901-02-03'));

ALTER TABLE etablissement ADD COLUMN mise_a_jour_at timestamptz;
ALTER TABLE entreprise_periode ALTER COLUMN siege_etablissement_id DROP NOT NULL;

-- éxcuter lors de la mise en prod de cette MR
-- https://gitlab.com/incubateur-territoires/startups/deveco/deveco/-/merge_requests/12

SELECT setval('compte_id_seq', (SELECT MAX(id) from compte));

INSERT INTO compte (
    nom,
    email,
		identifiant,
    compte_type_id,
    actif
)
VALUES
	('sirene', 'contact@sirene.fr', 'contact@sirene.fr', 'api', true),
	('api_entreprise', 'support@entreprise.api.gouv.fr', 'support@entreprise.api.gouv.fr', 'api', true);

UPDATE evenement
SET compte_id = (SELECT id FROM compte WHERE compte.nom = 'sirene' AND compte_type_id = 'api')
WHERE evenement.api_type_id = 'sirene';

UPDATE evenement
SET compte_id = (SELECT id FROM compte WHERE compte.nom = 'api_entreprise' AND compte_type_id = 'api')
WHERE evenement.api_type_id = 'api_entreprise';

ALTER TABLE evenement DROP COLUMN api_type_id;
ALTER TABLE evenement ALTER COLUMN compte_id SET NOT NULL;


ALTER TABLE etablissement_creation ADD COLUMN commune_id varchar(5);
ALTER TABLE etablissement_creation ADD CONSTRAINT fk_etablissement_creation__commune FOREIGN KEY (commune_id) REFERENCES commune(id);

-- vue effectifs + index

CREATE MATERIALIZED VIEW etablissement_effectif_emm_dernier_vue AS
SELECT DISTINCT ON ("etablissement_id")
  "etablissement_effectif_emm"."etablissement_id",
	"valeur"
FROM "etablissement_effectif_emm"
ORDER BY "etablissement_effectif_emm"."etablissement_id",
  "etablissement_effectif_emm"."annee" DESC,
  "etablissement_effectif_emm"."mois" DESC;

CREATE INDEX idx_etablissement_effectif_emm_dernier_vue ON etablissement_effectif_emm_dernier_vue(etablissement_id);

--- entreprise_exercice

CREATE TABLE IF NOT EXISTS entreprise_exercice (
    entreprise_id varchar(9) NOT NULL REFERENCES entreprise(id),
    cloture_date date NOT NULL,
	chiffre_affaires varchar,

	PRIMARY KEY (entreprise_id, cloture_date)
);

INSERT INTO entreprise_exercice
(entreprise_id, cloture_date, chiffre_affaires)
SELECT DISTINCT LEFT(etablissement_id, 9) as entreprise, cloture_date, chiffre_affaires
FROM etablissement_exercice;

---

INSERT INTO evenement__entreprise (
	evenement_id,
	entreprise_id
)
SELECT
	evenement_id,
	LEFT(etablissement_id, 9)
FROM evenement__etablissement
WHERE evenement_id in (
	SELECT id
	FROM evenement
	WHERE diff = '{ "message": "mise à jour des exercices" }'
);

--- communes COMER

INSERT INTO region (id, nom)
VALUES
('98', 'Collectivités d’outre-mer');

INSERT INTO departement (id, nom, region_id)
SELECT DISTINCT comer, libelle_comer, '98'
FROM source.source_insee_commune_comer
ON CONFLICT DO NOTHING;

INSERT INTO epci (id, nom, epci_type_id)
VALUES
('XXXXXXXXX', 'Collectivités d’outre-mer', 'ZZ');

INSERT INTO commune (
	id,
	nom,
	commune_type_id,
	departement_id,
	region_id,
	epci_id
)
SELECT
	com_comer,
	libelle,
	'COM',
	comer,
	'98',
	'XXXXXXXXX'
FROM source.source_insee_commune_comer;

--- admin / deveco

CREATE INDEX IF NOT EXISTS idx_deveco__equipe ON deveco(equipe_id);
CREATE INDEX IF NOT EXISTS idx_deveco__compte ON deveco(compte_id);
CREATE INDEX IF NOT EXISTS idx_deveco__equipe ON deveco (equipe_id);
CREATE INDEX IF NOT EXISTS idx_compte__prenom ON compte USING gin (prenom gin_trgm_ops);
CREATE INDEX IF NOT EXISTS idx_compte__email ON compte USING gin (email gin_trgm_ops);
CREATE INDEX IF NOT EXISTS idx_compte__equipe ON compte (equipe_id);

CREATE INDEX IF NOT EXISTS idx_evenement__type on evenement(evenement_type_id);

--- annuaire / personne

CREATE INDEX IF NOT EXISTS idx_personne__prenom ON personne USING gin (prenom gin_trgm_ops);
CREATE INDEX IF NOT EXISTS idx_personne__email ON personne USING gin (email gin_trgm_ops);
CREATE INDEX IF NOT EXISTS idx_personne__telephone ON personne USING gin (telephone gin_trgm_ops);
CREATE INDEX IF NOT EXISTS idx_personne__equipe ON personne (equipe_id);

-- geo

INSERT INTO compte (
    nom,
	prenom,
    email,
	identifiant,
    compte_type_id,
    actif
)
VALUES
	('adresse', 'api', 'adresse@data.gouv.fr', 'adresse@data.gouv.fr', 'api', true),
	('deveco', 'deveco', 'contact@deveco.incubateur.anct.fr', 'contact@deveco.incubateur.anct.fr', 'api', true);

-- etiquettes

CREATE INDEX idx_etiquette__equipe ON etiquette (equipe_id);
