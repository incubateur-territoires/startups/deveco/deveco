// import importPlugin from 'eslint-plugin-import';
import js from '@eslint/js';
import globals from 'globals';
import eslintConfigPrettier from 'eslint-config-prettier';
import tseslint from 'typescript-eslint';

export default tseslint.config(
	{
		ignores: [
			'src/migration',
			'node_modules/**',
			'build/',
			'dist/',
			'.github',
			'.husky',
			'.eslintcache',
			'.env',
			'**/_*.ts',
			'**/data/**/*.ts',
			'src/script',
		],
	},
	{
		languageOptions: {
			parserOptions: {
				projectService: true,
				tsconfigRootDir: import.meta.dirname,
			},
			ecmaVersion: 'latest',
			sourceType: 'module',
			globals: {
				...globals.node,
			},
		},
	},
	js.configs.recommended,
	tseslint.configs.recommendedTypeChecked,
	// tseslint.configs.strictTypeChecked,
	// tseslint.configs.stylisticTypeChecked,
	// importPlugin.flatConfigs.recommended,
	{
		rules: {
			'@typescript-eslint/no-explicit-any': 'off',
			'@typescript-eslint/no-unsafe-assignment': 'off',
			'@typescript-eslint/no-unsafe-member-access': 'off',
			'@typescript-eslint/restrict-template-expressions': 'off',
			// '@typescript-eslint/no-unsafe-return': 'off',
			// '@typescript-eslint/no-unsafe-call': 'off',
			'@typescript-eslint/no-unsafe-argument': 'off',
			'@typescript-eslint/no-duplicate-type-constituents': 'off',
			'@typescript-eslint/switch-exhaustiveness-check': 'error',
			'@typescript-eslint/no-unused-vars': [
				'error',
				{
					args: 'all',
					argsIgnorePattern: '^_',
					caughtErrors: 'all',
					caughtErrorsIgnorePattern: '^_',
					destructuredArrayIgnorePattern: '^_',
					varsIgnorePattern: '^_',
					ignoreRestSiblings: true,
				},
			],
			// 'import/order': [
			// 	1,
			// 	{
			// 		'newlines-between': 'always',
			// 		groups: ['external', 'builtin', 'internal', 'sibling', 'parent', 'index'],
			// 	},
			// ],
			curly: 'error',
		},
		// },  {
		// files: ['**/*.{ts,tsx}'],
		// extends: [importPlugin.flatConfigs.recommended, importPlugin.flatConfigs.typescript],
	}
);
// export default tseslint.config(
// 	// importPlugin.flatConfigs.recommended,
// 	{ ...js.configs.recommended, files: ['lib/', 'js/'] },
// 	// tseslint.configs.recommendedTypeChecked,
// 	tseslint.configs.stylisticTypeChecked,
// 	// tseslint.configs.strict,
// 	{
// 		files: ['lib/', 'js/'],
// 		env: {
// 			browser: false,
// 			es2021: true,
// 			node: true,
// 		},
// 		languageOptions: {
// 			ecmaVersion: 'latest',
// 			sourceType: 'module',
// 			globals: {
// 				...globals.browser,
// 				Elm: true,
// 				dsfr: true,
// 			},
// 			parserOptions: {
// 				// project: './tsconfig.json',
// 				// ecmaVersion: 'latest',
// 				// sourceType: 'module',
// 				projectService: true,
// 				tsconfigRootDir: import.meta.dirname,
// 			},
// 		},
// 		rules: {
// 			'@typescript-eslint/no-explicit-any': 'off',
// 			'@typescript-eslint/switch-exhaustiveness-check': 'error',
// 			'@typescript-eslint/no-unused-vars': 'off',
// 			'no-unused-vars': 'off',
// 			// '@typescript-eslint/no-unused-vars': [
// 			// 	'error',
// 			// 	{ ignoreRestSiblings: true, argsIgnorePattern: '^_', varsIgnorePattern: '^_' },
// 			// ],
// 			'import/order': [
// 				1,
// 				{
// 					'newlines-between': 'always',
// 					groups: ['external', 'builtin', 'internal', 'sibling', 'parent', 'index'],
// 				},
// 			],
// 			curly: 'error',
// 		},
// 		ignores: [
// 			'src/migration',
// 			'node_modules/**',
// 			'build/',
// 			'dist/',
// 			'.github',
// 			'.husky',
// 			'.eslintcache',
// 			'.env',
// 			'**/_*.ts',
// 			'data/**/*.ts',
// 		],
// 	},
// 	eslintConfigPrettier,
// );
