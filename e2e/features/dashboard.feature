#language: fr

@dashboard
Fonctionnalité: Dashboard
	Pour pouvoir avoir une vue d'ensemble de mon Territoire
	En tant que Deveco
	Je veux pouvoir consulter le Dashboard

Scénario: Dashboard
	Quand j'ai un compte "DevEco" avec l'email "truc@test.fr" et la clef "bleh"
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Alors je vois "équipe de référence"
	Alors je vois "Gestion des collaborateurs"
	Alors je vois "Gestion des étiquettes"
	Alors je vois "Import de mes données"
	Alors je vois "Nouveautés et évolutions"
	Alors je vois "Actualités du service"
	Alors je vois "Actualités du territoire"
	Alors je vois "Mes rappels créés"
	Alors je vois "Dernières demandes créées"
	Alors je vois "Arrivées sur le territoire"
	Quand je clique sur la carte qui contient "fermé"
	Alors je vois "Fermetures sur le territoire"
