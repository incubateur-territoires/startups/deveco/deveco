#language: fr

@locaux
Fonctionnalité: Visualisation des locaux
	Pour pouvoir utiliser deveco
	En tant que DevEco
	Je veux pouvoir consulter les locaux

Scénario: Ajout, recherche, et suppression de locaux
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah" vers la page "/locaux"
	Alors je suis sur la page Immobilier
	Alors je vois "Locaux"
	# Quand je clique sur "Créer un local"
	# Quand je renseigne "Ancienne pizzeria" dans le champ "Nom"
	# Quand je renseigne "10 rue Camille Buffardel 26150 Die" dans le champ "Adresse"
	# Quand je clique sur "#option-0"
	# Quand je choisis "#locaux-statut-option-vacant"
	# Quand je choisis "#locaux-types-option-bureaux"
	# Quand je renseigne "173" dans le champ "Surface totale (m²)"
	# Quand je renseigne "66000" dans le champ "Loyer annuel (en €)"
	# Quand je renseigne "Centre-ville" dans le champ "Zone géographique"
	# Quand je clique sur "#option-999"
	# Quand je renseigne "Lorem ipsum, etc." dans le champ "Commentaires"
	# Quand je clique sur "Créer la fiche"
	# # TODO vérifier si on veut qu'une création donne une modification, ou pas
	# # Alors je vois "Dernière modification"
	# # Alors je vois "à l'instant par"
	# Quand je clique sur "Immobilier"
	# Alors je suis sur la page "/locaux"
	# Alors je vois "Rechercher dans mes locaux"
	# Quand je renseigne "Ancienne" dans le champ "Rechercher dans mes locaux"
	# Quand je clique sur "Rechercher"
	# Alors je vois "pizzeria"
	# Quand je choisis "#filtres-local-statut-option-occupe"
	# Alors je ne vois pas "pizzeria"
	# Quand je choisis "#filtres-local-statut-option-vacant"
	# Alors je vois "pizzeria"
	# Quand je clique sur "Commerce"
	# Alors je ne vois pas "pizzeria"
	# Quand je clique sur "sans filtre"
	# Alors je vois "pizzeria"
	# Quand je clique sur "Bureau"
	# Alors je vois "pizzeria"
	# Quand je clique sur "Ancienne pizzeria"
	# Alors je suis sur l'url "/locaux/1"
	# Quand je clique sur "#local-delete-btn"
	# Quand je clique sur "Je veux vraiment supprimer"
	# Alors je vois "Le local a été supprimé avec succès"

Scénario: Ajout et suppression de propriétaire du territoire
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah" vers la page "/locaux"
	Alors je suis sur la page Immobilier
	Alors je vois "Locaux"
	# Quand je clique sur "Créer un local"
	# Quand je renseigne "Ancienne pizzeria" dans le champ "Nom"
	# Quand je renseigne "10 rue Camille Buffardel 26150 Die" dans le champ "Adresse"
	# Quand je clique sur "#option-0"
	# Quand je clique sur "Créer la fiche"
	# Quand je clique sur "Ajouter un propriétaire"
	# Alors je vois "Rechercher dans les Établissements de mon territoire"
	# Quand je renseigne "94845956500016" dans le champ "#proprietaire-recherche-siret"
	# Quand je clique sur "Rechercher"
	# Alors je vois "AXIOME"
	# Quand je clique sur "OK"
	# Alors je ne vois pas "Ajouter un propriétaire au local Pizza"
	# Alors je vois "AXIOME"
	# Quand j'attends 2 secondes
	# Quand je vais sur la page du Local 1
	# Alors je vois "Ancienne pizzeria"
	# Alors je vois "Dernière modification"
	# Alors je vois "AXIOME"
	# Quand je clique sur "Supprimer"
	# Alors je vois "Supprimer un propriétaire du local Ancienne pizzeria"
	# Quand je clique sur "OK"
	# Alors je ne vois pas "Supprimer un propriétaire du local Ancienne pizzeria"
	# Alors je ne vois pas "AXIOME"

# Fonctionnalité temporairement désactivée, seuls les établissements du territoire peuvent être propriétaires
# Scénario: Ajout et suppression de propriétaire hors territoire
#   Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
#   Quand je vais sur le lien magique pour "blah" vers la page "/locaux"
#   Alors je suis sur la page Immobilier
#   Quand je clique sur "Créer un local"
#   Quand je renseigne "Ancienne pizzeria" dans le champ "Nom"
#   Quand je renseigne "10 rue Camille Buffardel 26150 Die" dans le champ "Adresse"
#   Quand je clique sur "#option-0"
#   Quand je clique sur "Créer la fiche"
#   Quand je clique sur "Ajouter un propriétaire"
#   Alors je vois "Rechercher dans les Établissements de mon territoire"
#   Quand je renseigne "77567227234537" dans le champ "#proprietaire-recherche-siret"
#   Quand je clique sur "Rechercher"
#   Alors je vois "CROIX ROUGE"
#   Quand je clique sur "OK"
#   Alors je ne vois pas "Ajouter un propriétaire au local Pizza"
#   Alors je vois "CROIX ROUGE"
#   Quand j'attends 2 secondes
#   Quand je vais sur la page du Local 1
#   Alors je vois "Ancienne pizzeria"
#   Alors je vois "CROIX ROUGE"
#   Quand je clique sur "Supprimer"
#   Alors je vois "Supprimer un propriétaire du local Ancienne pizzeria"
#   Quand je clique sur "OK"
#   Alors je ne vois pas "Supprimer un propriétaire du local Ancienne pizzeria"
#   Alors je ne vois pas "CROIX ROUGE"
#   Quand je clique sur l'onglet Établissements
#   Quand je clique sur "Filtres"
#   Quand je clique sur "Données publiques"
#   Quand je clique sur "Exogène au territoire"
#   Quand je clique sur "Rechercher"
#   Alors je vois "CROIX ROUGE"
#   Alors je vois "EXOGÈNE"

Scénario: Ajout et suppression de propriétaire particulier
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah" vers la page "/locaux"
	Alors je suis sur la page Immobilier
	Alors je vois "Locaux"
	# Quand je clique sur "Créer un local"
	# Quand je renseigne "Ancienne pizzeria" dans le champ "Nom"
	# Quand je renseigne "10 rue Camille Buffardel 26150 Die" dans le champ "Adresse"
	# Quand je clique sur "#option-0"
	# Quand je clique sur "Créer la fiche"
	# Quand je clique sur "Ajouter un propriétaire"
	# Alors je vois "Rechercher dans les Établissements de mon territoire"
	# Quand je clique sur "Créer un propriétaire"
	# Alors je vois "Téléphone"
	# Quand je renseigne "Gustin" dans le champ "Nom"
	# Quand je renseigne "Proprio" dans le champ "Prénom"
	# Quand je clique sur "OK"
	# Alors je ne vois pas "Ajouter un propriétaire au local Pizza"
	# Alors je vois "Proprio Gustin"
	# Quand j'attends 2 secondes
	# Quand je vais sur la page du Local 1
	# Alors je vois "Ancienne pizzeria"
	# Alors je vois "Proprio Gustin"
	# Quand je clique sur "Supprimer"
	# Alors je vois "Supprimer un propriétaire du local Ancienne pizzeria"
	# Quand je clique sur "OK"
	# Alors je ne vois pas "Supprimer un propriétaire du local Ancienne pizzeria"
	# Alors je ne vois pas "Proprio Gustin"

	Scénario: Suppression de local avec propriétaire
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah" vers la page "/locaux"
	Alors je suis sur la page Immobilier
	Alors je vois "Locaux"
	# Quand je clique sur "Créer un local"
	# Quand je renseigne "Ancienne pizzeria" dans le champ "Nom"
	# Quand je renseigne "10 rue Camille Buffardel 26150 Die" dans le champ "Adresse"
	# Quand je clique sur "#option-0"
	# Quand je clique sur "Créer la fiche"
	# Quand je clique sur "Ajouter un propriétaire"
	# Alors je vois "Rechercher dans les Établissements de mon territoire"
	# Quand je renseigne "94845956500016" dans le champ "#proprietaire-recherche-siret"
	# Quand je clique sur "Rechercher"
	# Alors je vois "AXIOME"
	# Quand je clique sur "OK"
	# Alors je ne vois pas "Ajouter un propriétaire au local Pizza"
	# Alors je vois "AXIOME"
	# Quand j'attends 2 secondes
	# Quand je vais sur la page du Local 1
	# Alors je vois "Ancienne pizzeria"
	# Alors je vois "AXIOME"
	# Quand je clique sur "#local-delete-btn"
	# Quand je clique sur "Je veux vraiment supprimer"
	# Alors je vois "Le local a été supprimé avec succès"
