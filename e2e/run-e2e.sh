#!/bin/bash

set -ev

pushd api/data
./seed.sh
popd
pushd e2e
yarn
yarn test
popd
