import Sentry from './sentry';
import { elementToPdf } from './pdf';

// gestion des versions du code front
const CHECK_VERSION_EVERY = 60000; // ms

let appVersion;

async function checkVersion() {
	const response = await fetch('/api/health', {
		method: 'GET',
		headers: {
			'Content-type': 'application/json; charset=UTF-8',
		},
	});

	if (response.ok) {
		try {
			const json = await response.json();

			if (!appVersion) {
				appVersion = json.version;
			} else {
				if (appVersion !== json.version) {
					window.location.reload();
				}
			}
		} catch (e) {
			console.error(e);
		}
	}
}

function switchTheme(theme) {
	const options = ['system', 'light', 'dark'];
	const [html] = document.getElementsByTagName('html');
	if (options.includes(theme)) {
		html.dataset.frScheme = theme;
	}
}

setInterval(checkVersion, CHECK_VERSION_EVERY);
// fin de gestion des versions du code front

if (
	typeof Elm !== 'undefined' &&
	Object.prototype.hasOwnProperty.call(Elm, 'Main') &&
	Elm.Main &&
	Object.prototype.hasOwnProperty.call(Elm.Main, 'init') &&
	Elm.Main.init
) {
	const currentTheme = localStorage.getItem('scheme') || 'system';

	const appUrl = window.location.origin;

	const flags = {
		now: new Date().getTime(),
		timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
		currentTheme,
		appUrl,
	};
	const elm = Elm.Main.init({ flags });
	elm.ports.switchTheme.subscribe(switchTheme);

	elm.ports.scrollIntoView.subscribe((id) => {
		const element = document.getElementById(id);

		if (element) {
			element.scrollIntoView({
				block: 'start',
				inline: 'nearest',
				behavior: 'smooth',
			});
		}
	});

	elm.ports.click.subscribe((id) => {
		requestAnimationFrame(() => {
			const element = document.getElementById(id);

			if (element) {
				element.click();
			}
		});
	});

	elm.ports.log.subscribe(({ compteId, message, statut, type }) => {
		try {
			Sentry.captureException({
				statut,
				message,
				type,
				compteId,
			});
			fetch('/api/logger', {
				method: 'POST',
				body: JSON.stringify({
					statut,
					message,
					type,
					compteId,
				}),
				headers: {
					'Content-type': 'application/json; charset=UTF-8',
				},
			});
			console.info("Message d'erreur remonté au serveur", {
				statut,
				message,
				type,
				compteId,
			});
		} catch (e) {
			console.error("Impossible de contacter Sentry pour logger l'erreur", e);
		}
	});

	elm.ports.exportToPdf.subscribe(({ elementId }) => {
		requestAnimationFrame(() => {
			elementToPdf({ elementId });
		});
	});

	elm.ports.forceDisplayModal.subscribe((elementId) => {
		requestAnimationFrame(() => {
			const element = document.getElementById(elementId);
			if (element) {
				dsfr(element).modal.disclose();
			}
		});
	});
}
