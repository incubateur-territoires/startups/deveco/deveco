import '@github/clipboard-copy-element';

document.addEventListener('clipboard-copy', function (event) {
	const button = event.target.querySelector('[data-clipboard-copied]');
	const originalValue = button.textContent || button.innerText;
	const newValue = button.dataset.clipboardCopied;
	button.textContent = newValue;
	button.disabled = true;
	const timeout = button.dataset.clipboardTimeout || 1000;
	setTimeout(function () {
		button.disabled = false;
		button.textContent = originalValue;
	}, Number(timeout));
});
