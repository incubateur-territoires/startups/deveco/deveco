window.customElements.define(
	'currency-display',
	class extends HTMLElement {
		constructor() {
			super();
			this._amount = BigInt(this.getAttribute('amount') || '0');
			this._currency = this.getAttribute('currency');
			this._cultureCode = this.getAttribute('culture-code');
		}
		static get observedAttributes() {
			return ['amount', 'currency', 'culture-code'];
		}

		attributeChangedCallback(name, prev, next) {
			const isDecimal = next?.includes('.');
			if (!next || next === 'null') {
				return;
			}

			switch (name) {
				case 'amount':
					this._amount = isDecimal ? Number(next) : BigInt(next);
					break;
				case 'currency':
					this._currency = next;
					break;
				case 'culture-code':
					this._cultureCode = next;
					break;
				default:
					break;
			}

			this.render(isDecimal);
		}

		render(isDecimal) {
			if (
				!this._currency ||
				!this._cultureCode ||
				typeof this._amount === 'undefined' ||
				this._amount === null
			) {
				return;
			}
			let options = {
				style: 'currency',
				currency: this._currency,
				maximumFractionDigits: isDecimal ? 2 : 0,
			};

			try {
				this.innerHTML = this._amount?.toLocaleString(this._cultureCode, options) ?? '-';
			} catch (err) {
				console.error({ err });
				// Handle the error.
			}
		}
	}
);
