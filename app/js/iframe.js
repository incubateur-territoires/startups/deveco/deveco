import 'iframe-resizer';

// iFrameResize est en fait exposé globalement par le module iframe-resizer mais les fichiers de définition de types sont incorrects
iFrameResize({ log: true }, '#metabase');
