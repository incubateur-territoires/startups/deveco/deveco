export function elementToPdf({ elementId }) {
	const element = document.getElementById(elementId);

	const openWindow = window.open('', 'title', 'attributes');

	openWindow.document.write(element.outerHTML);

	openWindow.document.head.innerHTML = document.head.innerHTML;

	const windowElement = openWindow.document.getElementById(elementId);
	windowElement.classList.remove('print-only');
	windowElement.classList.add('printed');

	setTimeout(() => {
		openWindow.focus();
		openWindow.print();
		openWindow.close();
	}, 1000);
}
