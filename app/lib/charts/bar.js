import BaseChart, { populationTooltip } from './base';

function makeOnClick(config) {
	const filterKey = config.filterKey;
	return function onClick() {
		if (typeof URLSearchParams === 'undefined') {
			const defaultsearchParams =
				config.defaultFilterKeys
					?.map((k, i) => `&${k}=${config.defaultFilterValues[i]}`)
					.join('') ?? '';

			const location = window.location.href;
			const param = `${filterKey}=${this.options.filterValue}`;

			window.location.href = location.match(/\?/)
				? `${location}&${param}${defaultsearchParams}`
				: `${location}?${param}${defaultsearchParams}`;

			return;
		}

		const searchParams = new URLSearchParams(window.location.search);

		searchParams.set(filterKey, this.options.filterValue);

		if (config.defaultFilterKeys?.length) {
			config.defaultFilterKeys.forEach((k, i) => {
				searchParams.set(k, config.defaultFilterValues[i]);
			});
		}

		window.location.search = searchParams.toString();
	};
}

export default class extends BaseChart {
	constructor() {
		super();
	}

	static get observedAttributes() {
		return ['data'];
	}

	get config() {
		return {
			chart: {
				type: 'bar',
				animation: true,
			},
			credits: { enabled: false },
			title: {
				text: '',
			},
			xAxis: {
				categories: [],
				tickPosition: 'outside',
				labels: {
					step: 1,
					style: { fontSize: '10px', color: '#333' },
				},
			},
			yAxis: {
				min: 0,
				title: {
					text: null,
				},
			},
			legend: {
				reversed: false,
			},
			plotOptions: {
				animation: true,
				series: {
					animation: true,
					dataLabels: {
						enabled: true,
					},
				},
			},
			series: [],
		};
	}

	attributeChanged(name, _oldValue, newValue) {
		if (name === 'data') {
			const { values, config } = JSON.parse(newValue);

			const seriesData = config?.maxItems ? values.slice(0, config?.maxItems) : values;

			const xToPopulation = seriesData.map(({ population }) => population);

			if (config?.filterKey) {
				this.chart.update({
					plotOptions: {
						series: {
							cursor: 'pointer',
							point: {
								events: {
									click: makeOnClick(config),
								},
							},
						},
					},
				});
			}

			const title = config?.title || '';
			const series = [
				{
					showInLegend: false,
					name: title,
					data: seriesData.map(({ count, ...rest }) => ({
						...rest,
						y: count,
					})),
					tooltip: populationTooltip(xToPopulation),
				},
			];

			this.chart.update({
				xAxis: {
					categories: seriesData.map(({ label }) => `${label}`),
				},
			});

			// Remove all existing series...
			while (this.chart.series.length) {
				this.chart.series[0].remove();
			}

			// ... and replace them with fresh ones
			for (const serie of series) {
				this.chart.addSeries(serie);
			}
			this.chart.redraw();
		}
	}
}
