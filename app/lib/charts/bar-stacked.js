import BaseChart, { defaultTooltip } from './base';

export default class extends BaseChart {
	constructor() {
		super();
	}

	static get observedAttributes() {
		return ['data'];
	}

	get config() {
		return {
			chart: {
				type: 'bar',
				animation: true,
			},
			credits: { enabled: false },
			title: {
				text: '',
			},
			xAxis: {
				categories: [],
				tickPosition: 'outside',
				labels: {
					step: 1,
					style: { fontSize: '10px', color: '#333' },
				},
			},
			yAxis: {
				min: 0,
				title: {
					text: null,
				},
			},
			legend: {
				reversed: false,
			},
			plotOptions: {
				animation: true,
				series: {
					animation: true,
					stacking: 'normal',
					dataLabels: {
						enabled: true,
					},
				},
			},
			series: [],
		};
	}

	attributeChanged(name, _oldValue, newValue) {
		if (name === 'data') {
			const { values, config } = JSON.parse(newValue);

			const seriesData = config?.maxItems ? values.slice(0, config?.maxItems) : values;

			const title = config?.title || '';
			const titleSub = config?.titleSub || '';
			const series = [
				{
					showInLegend: true,
					name: title,
					tooltip: defaultTooltip,
					data: seriesData.map(({ count, subCount }) => count - subCount),
					color: '#058DC7',
				},
				{
					showInLegend: true,
					name: titleSub,
					tooltip: defaultTooltip,
					data: seriesData.map(({ subCount }) => subCount),
					color: '#50B432',
				},
			];
			this.chart.update({
				xAxis: {
					categories: seriesData.map(({ label }) => `${label}`),
				},
			});

			// Remove all existing series...
			while (this.chart.series.length) {
				this.chart.series[0].remove();
			}

			// ... and replace them with fresh ones
			for (const serie of series) {
				this.chart.addSeries(serie);
			}
			this.chart.redraw();
		}
	}
}
