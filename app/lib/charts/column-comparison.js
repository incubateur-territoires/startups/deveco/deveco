import BaseChart from './base';

export default class extends BaseChart {
	constructor() {
		super();
	}

	static get observedAttributes() {
		return ['data'];
	}

	get config() {
		return {
			chart: {
				type: 'column',
				animation: true,
			},
			credits: { enabled: false },
			title: {
				text: '',
			},
			xAxis: {
				categories: [],
				tickPosition: 'outside',
				labels: {
					step: 1,
					style: { fontSize: '10px', color: '#333' },
				},
			},
			yAxis: {
				min: 0,
				title: {
					text: null,
				},
			},
			legend: {
				reversed: true,
			},
			tooltip: {
				shared: true,
			},
			plotOptions: {
				animation: true,
				series: {
					animation: true,
					dataLabels: {
						enabled: true,
					},
				},
			},
			series: [],
		};
	}

	attributeChanged(name, _oldValue, newValue) {
		if (name === 'data') {
			const { values, config } = JSON.parse(newValue);

			const seriesData = config?.maxItems ? values.slice(0, config?.maxItems) : values;

			const title = config?.title || '';
			const titleSub = config?.titleSub || '';
			const countSubs = seriesData.map(({ countSub }) => countSub);
			const counts = seriesData.map(({ count }) => count);
			const series = [
				{
					showInLegend: true,
					name: titleSub,
					linkedTo: title,
					pointPlacement: 0.2,
					data: countSubs,
					color: '#50B432',
					// tooltip: defaultTooltip,
				},
				{
					showInLegend: true,
					name: title,
					pointPlacement: 0,
					data: counts,
					color: '#058DC7',
					// tooltip: defaultTooltip,
				},
			];
			this.chart.update({
				xAxis: {
					categories: seriesData.map(({ label }) => `${label}`),
				},
			});

			// Remove all existing series...
			while (this.chart.series.length) {
				this.chart.series[0].remove();
			}

			// ... and replace them with fresh ones
			for (const serie of series) {
				this.chart.addSeries(serie);
			}
			this.chart.redraw();
		}
	}
}
