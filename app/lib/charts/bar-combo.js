import BaseChart, { populationTooltip } from './base';

export default class extends BaseChart {
	constructor() {
		super();
	}

	static get observedAttributes() {
		return ['data'];
	}

	get config() {
		return {
			chart: {
				type: 'bar',
				animation: true,
			},
			credits: { enabled: false },
			title: {
				text: '',
			},
			xAxis: {
				categories: [],
				tickPosition: 'outside',
				labels: {
					step: 1,
					style: { fontSize: '10px', color: '#333' },
				},
			},
			yAxis: [
				{
					min: 0,
					title: {
						text: 'Total',
					},
				},
				{
					min: 0,
					title: {
						text: 'Population',
					},
					opposite: true,
				},
			],
			legend: {
				reversed: false,
			},
			plotOptions: {
				animation: true,
				series: {
					animation: true,
					dataLabels: {
						enabled: true,
					},
				},
			},
			series: [],
		};
	}

	attributeChanged(name, _oldValue, newValue) {
		if (name === 'data') {
			const { values, config } = JSON.parse(newValue);

			const seriesData = config?.maxItems ? values.slice(0, config?.maxItems) : values;

			const xToPopulation = seriesData.map(({ population }) => population);

			const title = config?.title || '';

			// TODO: virgule faite exprès ?
			const series = [
				{
					yAxis: 0,
					showInLegend: false,
					name: title,
					tooltip: populationTooltip(xToPopulation),
					data: seriesData.map(({ count }) => {
						return count;
					}),
				},
				{
					type: 'scatter',
					yAxis: 1,
					name: 'Population',
					data: seriesData.map(({ population }) => {
						return population;
					}),
					marker: {
						radius: 4,
					},
				},
			];
			this.chart.update({
				xAxis: {
					categories: seriesData.map(({ label }) => `${label}`),
				},
			});

			// Remove all existing series...
			while (this.chart.series.length) {
				this.chart.series[0].remove();
			}

			// ... and replace them with fresh ones
			for (const serie of series) {
				this.chart.addSeries(serie);
			}
			this.chart.redraw();
		}
	}
}
