import BaseChart, { defaultTooltip } from './base';

export default class extends BaseChart {
	constructor() {
		super();
	}

	static get observedAttributes() {
		return ['data'];
	}

	get config() {
		return {
			chart: {
				type: 'column',
				animation: true,
			},
			credits: { enabled: false },
			title: {
				text: '',
			},
			xAxis: {
				categories: [],
				tickPosition: 'outside',
				labels: {
					step: 1,
					style: { fontSize: '10px', color: '#333' },
				},
			},
			yAxis: {
				title: {
					text: null,
				},
			},
			legend: {
				reversed: false,
			},
			plotOptions: {
				animation: true,
				series: {
					animation: true,
					dataLabels: [
						{
							enabled: true,
						},
					],
				},
			},
			series: [],
		};
	}

	attributeChanged(name, _oldValue, newValue) {
		if (name === 'data') {
			const { values, config } = JSON.parse(newValue);

			const seriesData = config?.maxItems ? values.slice(0, config?.maxItems) : values;

			const title = config?.title || '';
			const series = [
				{
					showInLegend: false,
					name: title,
					data: seriesData.map(({ count }) => (count > 0 ? count : { y: count, color: '#BF0B23' })),
					tooltip: defaultTooltip,
				},
			];
			this.chart.update({
				xAxis: {
					categories: seriesData.map(({ label }) => `${label}`),
				},
			});

			// Remove all existing series...
			while (this.chart.series.length) {
				this.chart.series[0].remove();
			}

			// ... and replace them with fresh ones
			for (const serie of series) {
				this.chart.addSeries(serie);
			}
			this.chart.redraw();
		}
	}
}
