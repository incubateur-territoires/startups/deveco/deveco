import BaseChart, { Highcharts } from './base';
import networkgraph from 'highcharts/modules/networkgraph';

export default class extends BaseChart {
	constructor() {
		super();

		networkgraph(Highcharts);
	}

	static get observedAttributes() {
		return ['data'];
	}

	get config() {
		return {
			chart: {
				type: 'networkgraph',
			},
			plotOptions: {
				networkgraph: {
					keys: ['from', 'to'],
					enableSimulation: true,
					friction: -0.9,
					approximation: true,
				},
			},
			series: [],
		};
	}

	attributeChanged(name, _oldValue, newValue) {
		if (name === 'data') {
			const { values, config } = JSON.parse(newValue);

			const seriesData = config?.maxItems ? values.slice(0, config?.maxItems) : values;

			const series = [
				{
					data: seriesData,
				},
			];

			// Remove all existing series...
			while (this.chart.series.length) {
				this.chart.series[0].remove();
			}

			// ... and replace them with fresh ones
			for (const serie of series) {
				this.chart.addSeries(serie);
			}
			this.chart.redraw();
		}
	}
}
