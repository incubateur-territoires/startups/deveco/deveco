import { chart } from 'highcharts';
export { default as Highcharts } from 'highcharts';

export const frFormat = (value) => new Intl.NumberFormat('fr-FR', {}).format(value);

export const defaultTooltip = {
	pointFormatter: function () {
		return `<strong>${frFormat(this.y)}</strong>`;
	},
};

export const populationTooltip = (xToPopulation) => ({
	pointFormatter: function () {
		const population = xToPopulation[this.x];
		const suffixe = (population ?? 0) > 1 ? 's' : '';
		const textePopulation = population ? ` (${frFormat(population)} établissement${suffixe})` : '';
		return `<strong>${frFormat(this.y)}</strong>${textePopulation}`;
	},
});

export default class extends HTMLElement {
	constructor() {
		super();
	}

	static get observedAttributes() {
		throw new Error('Must be implemented');
	}

	get config() {
		throw new Error('Must be implemented');
	}

	attributeChanged(_name, _oldValue, _newValue) {
		throw new Error('Must be implemented');
	}

	get container() {
		return this.querySelector('.chart-container');
	}

	attributeChangedCallback(name, oldValue, newValue) {
		requestAnimationFrame(() => {
			this.attributeChanged(name, oldValue, newValue);
		});
	}

	connectedCallback() {
		this.appendChild(
			document.createRange().createContextualFragment(`
        <div class="chart-container"></div>
      `)
		);

		this.chart = chart(this.container, this.config);

		// Force reflow
		requestAnimationFrame(() => {
			this.chart.reflow();
		});
	}

	disconnectedCallback() {
		if (this.chart) {
			this.removeChild(this.container);
			this.chart.destroy();
		}
	}
}
