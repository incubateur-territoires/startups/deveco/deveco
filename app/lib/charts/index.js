// Highcharts integration code written by https://github.com/n1k0 and taken from https://github.com/MTES-MCT/ecobalyse/
import Highcharts from 'highcharts';
import enableA11y from 'highcharts/modules/accessibility';

import Bar from './bar';
import BarCombo from './bar-combo';
import BarGrouped from './bar-grouped';
import BarStacked from './bar-stacked';
import Column from './column';
import ColumnComparison from './column-comparison';
import ColumnNegative from './column-negative';
import Line from './line';
import Pie from './pie';
import Network from './network';

// Enable a11y https://www.highcharts.com/docs/accessibility/accessibility-module
enableA11y(Highcharts);

Highcharts.setOptions({
	lang: {
		loading: 'Chargement…',
		months: [
			'janvier',
			'février',
			'mars',
			'avril',
			'mai',
			'juin',
			'juillet',
			'août',
			'septembre',
			'octobre',
			'novembre',
			'décembre',
		],
		weekdays: ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'],
		shortMonths: [
			'jan',
			'fév',
			'mar',
			'avr',
			'mai',
			'juin',
			'juil',
			'aoû',
			'sep',
			'oct',
			'nov',
			'déc',
		],
		exportButtonTitle: 'Exporter',
		printButtonTitle: 'Imprimer',
		rangeSelectorFrom: 'Du',
		rangeSelectorTo: 'au',
		rangeSelectorZoom: 'Période',
		downloadPNG: 'Télécharger en PNG',
		downloadJPEG: 'Télécharger en JPEG',
		downloadPDF: 'Télécharger en PDF',
		downloadSVG: 'Télécharger en SVG',
		resetZoom: 'Réinitialiser le zoom',
		resetZoomTitle: 'Réinitialiser le zoom',
		thousandsSep: ' ',
		decimalPoint: ',',
	},
});

export default {
	registerElements: function () {
		customElements.define('chart-bar', Bar);
		customElements.define('chart-bar-combo', BarCombo);
		customElements.define('chart-bar-stacked', BarStacked);
		customElements.define('chart-bar-grouped', BarGrouped);
		customElements.define('chart-column', Column);
		customElements.define('chart-column-comparison', ColumnComparison);
		customElements.define('chart-column-negative', ColumnNegative);
		customElements.define('chart-line', Line);
		customElements.define('chart-pie', Pie);
		customElements.define('chart-network', Network);
	},
};
