import Graph from 'graphology';
import Sigma from 'sigma';
import forceAtlas2 from 'graphology-layout-forceatlas2';
import circular from 'graphology-layout/circular';

const nodeColor = '#C9191E';
const nodeSirenColor = '#000091';
const edgeColor = '#929292';
const edgeSirenColor = '#000091';

export default class SigmaGraph extends HTMLElement {
	constructor() {
		super();
		this._sigma = null;
		this._data = null;
		this._value = null;
		this._container = null;
	}

	static get observedAttributes() {
		return ['data'];
	}

	connectedCallback() {
		this._container = document.createElement('div');
		this._container.style.width = '100%';
		this._container.style.height = '500px';
		this.appendChild(this._container);

		if (this.hasAttribute('data')) {
			this._data = JSON.parse(this.getAttribute('data'));
			this._renderSigma(this._data);
		}
	}

	attributeChangedCallback(name, oldValue, newValue) {
		if (name === 'data' && newValue && newValue !== this._value) {
			this._data = JSON.parse(newValue);
			this._value = newValue;

			requestAnimationFrame(() => {
				this._renderSigma(this._data);
			});
		}
	}

	_renderSigma(graphData) {
		if (this._sigma) {
			this._sigma.kill();
			this._sigma = null;
		}

		const siren = graphData.siren;

		const graph = new Graph();

		const nodes = [
			...graphData.values
				.flatMap(([a, b]) => [a, b])
				.reduce((result, siren) => result.add(siren), new Set()),
		];

		nodes.forEach((node) =>
			graph.addNode(node, {
				label: node,
				x: Math.random() * 100,
				y: Math.random() * 100,
			})
		);

		const edges = [
			...graphData.values
				.reduce((result, [a, b, lien]) => result.set(`${a}${b}`, [a, b, lien]), new Map())
				.values(),
		];

		edges.forEach(([a, b, lien]) => {
			graph.addEdge(a, b, {
				lien,
				type: 'arrow',
				source: a,
				target: b,
			});
		});

		let currentNode = null;

		// couleur
		graph.forEachNode((node, _attributes) => {
			if (node.match(siren)) {
				graph.setNodeAttribute(node, 'color', nodeSirenColor);

				currentNode = node;
			} else {
				graph.setNodeAttribute(node, 'color', nodeColor);
			}
		});

		// couleur
		graph.forEachEdge((edge, attributes) => {
			if (attributes.source.match(siren) || attributes.target.match(siren)) {
				// graph.setEdgeAttribute(edge, 'size', 2);
				graph.setEdgeAttribute(edge, 'color', edgeSirenColor);
			} else {
				graph.setEdgeAttribute(edge, 'color', edgeColor);
			}
		});

		// taille
		const degrees = graph.nodes().map((node) => graph.degree(node));
		const minDegree = Math.min(...degrees);
		const maxDegree = Math.max(...degrees);
		const minSize = 2,
			maxSize = 15;
		graph.forEachNode((node) => {
			const degree = graph.degree(node);
			graph.setNodeAttribute(
				node,
				'size',
				minSize + ((degree - minDegree) / (maxDegree - minDegree)) * (maxSize - minSize)
			);
		});

		// disposition
		circular.assign(graph);
		const settings = forceAtlas2.inferSettings(graph);
		forceAtlas2.assign(graph, {
			settings,
			iterations: 100,
		});

		this._sigma = new Sigma(graph, this._container, {
			allowInvalidContainer: true,
		});

		// Set cursor to pointer (hand) when hovering over a node
		this._sigma.on('enterNode', (_e) => {
			this._container.style.cursor = 'pointer';
		});

		// Make sure to revert to default cursor again when moving out of nodes
		this._sigma.on('leaveNode', (_e) => {
			this._container.style.cursor = 'default';
		});

		this._sigma.on('clickNode', ({ node }) => {
			const [_match, siren] = node.match(/\(?([0-9]{9})\)?$/) || [];

			if (siren) {
				window.open(`/etablissements?geo=france&recherche=${siren}`);
			}
		});

		setTimeout(() => {
			if (!currentNode) {
				return;
			}

			const x = this._sigma.nodeDataCache[currentNode].x;
			const y = this._sigma.nodeDataCache[currentNode].y;

			this._sigma.camera.animate(
				{
					x,
					y,
					ratio: 0.4,
				},
				{
					easing: 'linear',
					duration: 500,
				}
			);
		}, 500);
	}
}
