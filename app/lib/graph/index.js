import Network from './network';

export default {
	registerElements: function () {
		customElements.define('graph-network', Network);
	},
};
