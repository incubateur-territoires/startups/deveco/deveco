import * as turf from '@turf/turf';
import maplibregl, { Marker } from 'maplibre-gl';

import BaseMap from './base';

function searchMerge(searchParamsNew) {
	if (typeof URLSearchParams === 'undefined') {
		const queryString = window.location.search;
		const existingParams = (queryString || '').split('&').reduce((existingParams, param) => {
			const [key, value] = param.split('=');

			existingParams[key] = decodeURIComponent(value || '');

			return existingParams;
		}, {});

		const mergedParams = { ...existingParams, ...searchParamsNew };

		const newQueryString = Object.entries(mergedParams)
			.map(([key, value]) => `${key}=${encodeURIComponent(value)}`)
			.join('&');

		return newQueryString;
	}

	const searchParams = new URLSearchParams(window.location.search);

	Object.entries(searchParamsNew).forEach(([key, value]) => {
		searchParams.set(key, value);
	});

	return searchParams.toString();
}

function abbreviate(field) {
	return [
		'case',
		['>=', ['get', field], 1000000], // Millions
		[
			'concat',
			['round', ['/', ['get', field], 1000000]], // Divide by 1M
			'M',
		],
		['>=', ['get', field], 1000], // Thousands
		[
			'concat',
			['round', ['/', ['get', field], 1000]], // Divide by 1K
			'K',
		],
		['get', field], // Default: raw count
	];
}

function groupLogarithmically(numbers) {
	// Ensure the input array is sorted
	const sortedNumbers = [...numbers].sort((a, b) => a - b);

	// Helper function to determine the logarithmic bin
	const getLogGroup = (num) => Math.floor(Math.log10(num));

	// Create groups based on logarithmic bins
	const groups = {};
	sortedNumbers.forEach((num) => {
		const groupKey = getLogGroup(num);
		if (!groups[groupKey]) {
			groups[groupKey] = [];
		}
		groups[groupKey].push(num);
	});

	// Convert groups object to an array of group arrays
	let groupedArrays = Object.values(groups);

	// Merge groups if there are more than three
	while (groupedArrays.length > 3) {
		// Merge the first two groups
		groupedArrays[1] = [...groupedArrays[0], ...groupedArrays[1]];
		groupedArrays.shift(); // Remove the merged group
	}

	if (groupedArrays[1] === undefined) {
		if (groupedArrays[2] === undefined) {
			return [groupedArrays[0], groupedArrays[0], groupedArrays[0]];
		}
		return [groupedArrays[0], groupedArrays[1], groupedArrays[1]];
	}

	return groupedArrays;
}

function wait(ms) {
	return new Promise((resolve) => setTimeout(resolve, ms));
}

export default class extends BaseMap {
	constructor() {
		super();
	}

	static get observedAttributes() {
		return ['data', 'requete', 'adresse', 'annee'];
	}

	get config() {
		return {};
	}

	addLocauxSource() {
		const source = this.map.getSource('locaux-source');
		if (source) {
			return;
		}

		this.map.addSource('locaux-source', {
			type: 'geojson',
			data: {
				type: 'FeatureCollection',
				features: [],
			},
			cluster: true,
			clusterRadius: 40,
			clusterProperties: {
				total_count: ['+', ['get', 'count']],
			},
		});
	}

	clusterGroupClick(e) {
		const feature = e.features[0];
		const { geometry } = feature;

		const coordinates = geometry.coordinates.slice();

		this.map.flyTo({
			center: coordinates,
			zoom: this.map.getZoom() + 2,
			duration: 1000,
			essential: true,
		});
	}

	async clusterLeafClick(e) {
		this.map.getCanvas().style.cursor = 'progress';

		const feature = e.features[0];
		const { properties, geometry } = feature;

		const coordinates = geometry.coordinates.slice();

		// Ensure that if the map is zoomed out such that multiple
		// copies of the feature are visible, the popup appears
		// over the copy being pointed to.
		while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
			coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
		}

		// tous les locaux partagent la même géolocalisation
		// on recherche par point
		// on utilise les coordonnées dans les properties
		// plutôt que celles de la feature
		const { longitude, latitude } = feature.properties;

		const searchParamsNew = searchMerge({
			latitude,
			longitude,
			// si le nombre de points est inférieur à 10K, on reçoit des géolocalisation précises
			// alors, on utilise le zoom maximal
			// sinon, on récupère le zoom actuel de la carte
			z: this.total <= 10000 ? 20 : this.map.getZoom(),
		});

		const result = await fetch(`/api/locaux?${searchParamsNew}`).then((r) => r.json());

		let html;

		function localHtmlFormat(local) {
			if (!local) {
				return null;
			}

			const {
				id: id,
				nom,
				adresse: {
					numero,
					voieNom,
					codePostal,
					commune: { nom: communeNom },
				},
			} = local;

			const nomAffichage = nom ? `${nom} (${id})` : id;

			const adresse = [numero, voieNom, codePostal, communeNom].filter(Boolean).join(' ');

			return `
<b>${nomAffichage}</b><br/>
${adresse}<br/>
<b>Id :</b> ${id}<br/>
<a target="_blank" href="/locaux/${id}">Voir la fiche local</a>`;
		}

		if (result?.elements.length) {
			if (properties.count === 1) {
				const local = result.elements[0];

				html = local ? localHtmlFormat(local) : 'Local introuvable';
			} else {
				const locaux = result.elements.slice(0, 3);

				html = `
		${result.total} locaux à cette localisation
		<br/><hr/>
		`;

				html += locaux.map(localHtmlFormat).filter(Boolean).join('<br/><br/>');

				if (properties.count > 3) {
					html += `<br/><br/><hr/>
					<a target="_blank" href="/locaux?${searchParamsNew}">Rechercher tous les locaux</a>
					`;
				}
			}
		} else {
			html = `Erreur pendant la recherche`;
		}

		new maplibregl.Popup().setLngLat(coordinates).setHTML(html).addTo(this.map);

		this.map.getCanvas().style.cursor = 'default';
	}

	addLocauxLayer() {
		// Add a symbol layer
		this.map.addLayer({
			id: 'cluster-circle',
			type: 'circle',
			source: 'locaux-source',
			paint: {},
		});

		this.map.addLayer({
			id: 'cluster-count-group',
			type: 'symbol',
			source: 'locaux-source',
			filter: ['has', 'total_count'],
			layout: {
				'text-field': abbreviate('total_count'),
				'text-size': 12,
			},
		});

		this.map.addLayer({
			id: 'uncluster-count-group',
			type: 'symbol',
			source: 'locaux-source',
			filter: ['>', ['get', 'geolocalisations'], 1],
			layout: {
				'text-field': abbreviate('count'),
				'text-size': 12,
			},
		});

		this.map.addLayer({
			id: 'cluster-count-leaf',
			type: 'symbol',
			source: 'locaux-source',
			filter: ['==', ['get', 'geolocalisations'], 1],
			layout: {
				'text-field': abbreviate('count'),
				'text-size': 12,
			},
		});

		this.map.addLayer({
			id: 'uncluster-circle-leaf',
			type: 'circle',
			source: 'locaux-source',
			filter: ['==', ['get', 'count'], 1],
			paint: {
				'circle-radius': 12,
				'circle-color': '#FFFFFF',
			},
		});

		this.map.addLayer({
			id: 'uncluster-text-leaf',
			type: 'symbol',
			source: 'locaux-source',
			filter: ['==', ['get', 'count'], 1],
			layout: {
				'text-field': 'E',
				'text-size': 12,
			},
		});

		this.map.on('mouseenter', 'cluster-circle', () => {
			this.map.getCanvas().style.cursor = 'pointer';
		});

		this.map.on('mouseleave', 'cluster-circle', () => {
			this.map.getCanvas().style.cursor = 'default';
		});
	}

	updateLocauxLayer(features) {
		this.map.setPaintProperty('cluster-circle', 'circle-opacity', 0.1);

		setTimeout(() => {
			const renderedClusters = this.map
				.queryRenderedFeatures({ layers: ['cluster-circle'] })
				.map((feature) => feature.properties.total_count || 0)
				.filter(Boolean);

			const data =
				renderedClusters.length === 0
					? features.map((feature) => feature.properties.count)
					: renderedClusters;
			if (!data.length) {
				return;
			}

			// const scaler = createMapLibreLogScaler(data, {
			// 	minRadius: 10,
			// 	maxRadius: 20,
			// 	minColor: "#c6c6fb",
			// 	maxColor: "#adadf9",
			// 	logBase: 0.4,
			// 	clipStd: 2,
			// 	expression: ["coalesce", ["get", "total_count"], ["get", "count"]],
			// });

			// const radiusExpression = scaler.createRadiusExpression();
			// const colorExpression = scaler.createColorExpression();
			// this.map.setPaintProperty("cluster-circle", "circle-radius", radiusExpression);
			// this.map.setPaintProperty("cluster-circle", "circle-color", colorExpression);

			const [, second] = groupLogarithmically(data);

			const borne1 = 0;
			// +1 et +2 permettent d'avoir 3 valeurs différentes même avec moins de 3 points
			const borne2 = second[0] + 1;
			const borne3 = second.slice(-1)[0] + 2;

			this.map.setPaintProperty('cluster-circle', 'circle-color', [
				'interpolate',
				['linear'],
				['coalesce', ['get', 'total_count'], ['get', 'count']],
				borne1,
				'#c6c6fb',
				borne2,
				'#adadf9',
				borne3,
				'#8b8bf6',
			]);

			this.map.setPaintProperty('cluster-circle', 'circle-radius', [
				'interpolate',
				['linear'],
				['coalesce', ['get', 'total_count'], ['get', 'count']],
				borne1,
				10,
				borne2,
				20,
				borne3,
				30,
			]);

			this.map.setPaintProperty('cluster-circle', 'circle-opacity', 1);
		}, 200);
	}

	addContourSource({ id }) {
		if (this.map.getSource(id)) {
			return;
		}

		this.map.addSource(id, {
			type: 'geojson',
			data: {
				type: 'FeatureCollection',
				features: [],
			},
		});
	}

	addContourLayer({
		id,
		source,
		type = 'fill',
		fillColor = '#088',
		outlineColor = '#000',
		opacity = 0.1,
	}) {
		if (this.map.getLayer(id)) {
			return;
		}

		this.map.addLayer({
			id,
			type,
			source,
			layout: {},
			paint: {
				'fill-color': fillColor,
				'fill-outline-color': outlineColor,
				'fill-opacity': opacity,
			},
		});
	}

	setContourLayerClick(id) {
		this.map.on('click', id, (e) => {
			if (e.defaultPrevented) {
				return;
			}

			const nom = e.features[0].properties.nom;
			if (!nom) {
				return;
			}

			new maplibregl.Popup().setLngLat(e.lngLat).setHTML(nom).addTo(this.map);

			this.map.getCanvas().style.cursor = 'default';

			e.preventDefault();
		});
	}

	setLocauxLayerClick() {
		this.map.on('click', 'cluster-circle', (e) => {
			if (e.features[0].properties.count === 1 || e.features[0].properties.geolocalisations === 1) {
				this.clusterLeafClick(e);
			} else {
				this.clusterGroupClick(e);
			}

			e.preventDefault();
		});
	}

	setSource({ id, features }) {
		this.map.getSource(id).setData({
			type: 'FeatureCollection',
			features,
		});
	}

	fitBounds(features) {
		if (features.length) {
			const featureCollection = turf.featureCollection(features);
			const bounds = turf.bbox(featureCollection);

			const currentBounds = this.map.getBounds();
			this.map.fitBounds(bounds, {
				padding: 80,
				maxDuration: 0.01,
			});
			const newBounds = this.map.getBounds();

			if (currentBounds[0] === newBounds[0] || currentBounds[1] === newBounds[1]) {
				this.updateMapInfo({ type: 'fake' });
			}
		}
	}

	updateMapInfo(e) {
		if (this.timeout) {
			clearTimeout(this.timeout);
		}

		this.timeout = setTimeout(() => {
			requestAnimationFrame(() => {
				const center = this.map.getCenter();

				const currentZoom = this.map.getZoom();

				const dispatch = () => {
					this.map.getCanvas().style.cursor = 'progress';

					this.center = center;

					const { _sw, _ne } = this.map.getBounds();

					const fixedPrecision = this.zoom > 13 ? 6 : 2;

					const event = new CustomEvent('updatedmap', {
						detail: {
							center: this.center,
							zoom: Math.round(this.zoom),
							south: Number(_sw.lat.toFixed(fixedPrecision)),
							west: Number(_sw.lng.toFixed(fixedPrecision)),
							north: Number(_ne.lat.toFixed(fixedPrecision)),
							east: Number(_ne.lng.toFixed(fixedPrecision)),
						},
					});

					this.dispatchEvent(event);
				};

				const shouldZoom =
					// l'event est autre chose qu'un zoom
					e.type !== 'zoomend' ||
					// ou on n'a pas de zoom enregistré
					!this.zoom ||
					// ou on dézoome
					currentZoom < this.zoom ||
					// ou le niveau de zoom actuel est inférieur au seuil théorique de précision
					// et que le niveau de zoom est différent de plus que 1 niveau
					(this.total >= 10000 && this.zoom < 16 && Math.round(currentZoom) >= this.zoom + 1);

				this.zoom = currentZoom;
				if (shouldZoom) {
					dispatch();
				}
			});
		}, 100);
	}

	async connectedCallback() {
		BaseMap.prototype.connectedCallback.call(this);

		while (!this.map.isStyleLoaded()) {
			await wait(500);
		}

		this.addContourSource({
			id: 'territoire-source',
		});
		this.addContourLayer({
			id: 'territoire-layer',
			source: 'territoire-source',
		});

		this.addContourSource({
			id: 'qpvs-source',
		});
		this.addContourLayer({
			id: 'qpvs-layer',
			source: 'qpvs-source',
			fillColor: '#f00',
			outlineColor: '#000',
			opacity: 0.2,
		});

		this.addContourSource({
			id: 'zonages-source',
		});
		this.addContourLayer({
			id: 'zonages-layer',
			source: 'zonages-source',
			fillColor: '#0f0',
			outlineColor: '#000',
			opacity: 0.2,
		});

		this.addLocauxSource();
		this.addLocauxLayer();

		// note : on configure le click des locaux avant celui des layers
		// pour pouvoir bloquer l'événement et ne pas afficher plusieurs popups
		this.setLocauxLayerClick();

		this.setContourLayerClick('zonages-layer');
		this.setContourLayerClick('qpvs-layer');

		this.started = true;
	}

	async attributeChanged(name, _oldValue, newValue) {
		while (!this.started) {
			await wait(500);
		}

		if (!this.map) {
			// l'initialisation de MapLibre a échoué, inutile de chercher à faire quelque chose
			return;
		}

		if (!this.mapEvented) {
			this.map.on('dragend', (e) => this.updateMapInfo(e));
			this.map.on('zoomend', (e) => this.updateMapInfo(e));
			this.mapEvented = true;
		}

		if (name === 'adresse') {
			if (newValue !== 'null' && this.adresseMarker) {
				this.adresseMarker.remove();
			}

			const adresse = newValue ? JSON.parse(newValue) : null;

			if (adresse) {
				const { center, type } = adresse;
				let zoom;

				switch (type) {
					case 'municipality':
						zoom = 11;
						break;
					case 'locality':
						zoom = 15;
						break;
					case 'street':
						zoom = 17;
						break;
					case 'housenumber':
						zoom = 19;
						break;
					default:
						zoom = 14;
				}

				this.adresseMarker = new Marker({
					color: '#f00',
				}).setLngLat(center);
				this.adresseMarker.addTo(this.map);

				this.map.flyTo({
					center,
					zoom,
				});
			}
			return;
		}

		if (name === 'requete') {
			this.map.getCanvas().style.cursor = 'default';
			return;
		}

		const { total, territoire, qpvs = [], zonages = [], features } = JSON.parse(newValue);

		this.total = total;
		this.annee = this.annee ?? new Date().getFullYear();

		let featureTerritoire = null;

		if (territoire?.geometrie) {
			featureTerritoire = {
				type: 'Feature',
				properties: {},
				geometry: territoire?.geometrie ? JSON.parse(territoire.geometrie) : null,
			};

			this.setSource({
				id: 'territoire-source',
				features: [featureTerritoire],
			});

			if (this.mapIsBound) {
				const {
					_sw: { lng: minX, lat: minY },
					_ne: { lng: maxX, lat: maxY },
				} = this.map.getBounds();

				// BBox bbox extent in [minX, minY, maxX, maxY] order
				const mapBoundingBox = [minX, minY, maxX, maxY];
				const mapBoundingBoxTurf = turf.bboxPolygon(mapBoundingBox).bbox;

				const territoireBoundingBox = turf.bbox(featureTerritoire);

				const estCeQuonABesoinDeDeclencherManuellement =
					mapBoundingBoxTurf[0] === territoireBoundingBox[0] &&
					mapBoundingBoxTurf[1] === territoireBoundingBox[1] &&
					mapBoundingBoxTurf[2] === territoireBoundingBox[2] &&
					mapBoundingBoxTurf[3] === territoireBoundingBox[3];

				if (estCeQuonABesoinDeDeclencherManuellement) {
					this.updateMapInfo({ type: 'fake' });
				} else {
					this.fitBounds([featureTerritoire]);
				}
			}
		}

		// les clics pour afficher la popup risquent de ne pas marcher
		// si un périmètre en contient un autre

		const featuresQpvs = (qpvs || [])
			.sort((a, b) => b.surface - a.surface)
			.map(({ id, nom, geometrie }) => ({
				type: 'Feature',
				properties: {
					id,
					nom: `QPV : ${nom}`,
					type: 'qpv',
				},
				geometry: JSON.parse(geometrie),
			}));

		this.setSource({
			id: 'qpvs-source',
			features: featuresQpvs,
		});

		const featuresZonages = (zonages || [])
			.sort((a, b) => b.surface - a.surface)
			.map(({ id, nom, geometrie }) => ({
				type: 'Feature',
				properties: {
					id,
					nom: `Zonage : ${nom}`,
					type: 'zonage',
				},
				geometry: JSON.parse(geometrie),
			}));

		this.setSource({
			id: 'zonages-source',
			features: featuresZonages,
		});

		const featuresLocaux = features ? JSON.parse(features) : [];

		// on copie les coordonnées dans les properties
		// car les features ajoutées à la map peuvent avoir des coordonnées imprécises
		featuresLocaux.forEach((f) => {
			f.properties = {
				...f.properties,
				longitude: f.geometry.coordinates[0],
				latitude: f.geometry.coordinates[1],
			};
		});

		this.setSource({
			id: 'locaux-source',
			features: featuresLocaux,
		});

		this.updateLocauxLayer(featuresLocaux);

		if (!this.mapIsBound) {
			const featuresPerimetres = [featureTerritoire, ...featuresQpvs, ...featuresZonages].filter(
				(f) => f?.geometry
			);

			if (featuresPerimetres.length > 0) {
				this.mapIsBound = true;

				this.fitBounds(featuresPerimetres);
			}
		}
	}
}
