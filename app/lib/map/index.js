import Geolocalisation from './geolocalisation';
import Etablissements from './etablissements';
import Locaux from './locaux';

export default {
	registerElements: function () {
		customElements.define('maplibre-geolocalisation', Geolocalisation);
		customElements.define('maplibre-etablissements', Etablissements);
		customElements.define('maplibre-locaux', Locaux);
	},
};
