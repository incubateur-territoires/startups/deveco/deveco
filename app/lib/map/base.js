import 'maplibre-gl/dist/maplibre-gl.css';
import maplibregl, { AttributionControl, NavigationControl, ScaleControl } from 'maplibre-gl';

export default class extends HTMLElement {
	constructor() {
		super();
		this.centreDeLaFrance = [2.8906249999922693, 46.81809179606458];
	}

	static get observedAttributes() {
		throw new Error('Must be implemented');
	}

	get config() {
		throw new Error('Must be implemented');
	}

	attributeChanged(_name, _oldValue, _newValue) {
		throw new Error('Must be implemented');
	}

	get container() {
		return this.querySelector('.map-container');
	}

	attributeChangedCallback(name, oldValue, newValue) {
		requestAnimationFrame(() => {
			this.attributeChanged(name, oldValue, newValue);
		});
	}

	connectedCallback() {
		this.appendChild(
			document.createRange().createContextualFragment(`
        <div class="map-container h-full"></div>
      `)
		);

		this.map = new maplibregl.Map({
			container: this.container, // container id
			style: 'https://openmaptiles.geo.data.gouv.fr/styles/osm-bright/style.json', // style URL
			center: this.centreDeLaFrance,
			zoom: 4,
			minZoom: 2,
			maxZoom: 20,
			attributionControl: false,
			pitchWithRotate: false,
			dragRotate: false,
			touchZoomRotate: false,
		});

		this.map.addControl(new NavigationControl({ showCompass: false }));

		const scale = new ScaleControl({
			maxWidth: 80,
			unit: 'metric',
		});
		this.map.addControl(scale);

		this.map.addControl(
			new AttributionControl({
				compact: true,
			})
		);

		// Force redraw
		requestAnimationFrame(() => {
			this.map.redraw();
		});

		window.map = this.map;
	}

	disconnectedCallback() {
		if (this.map) {
			this.removeChild(this.container);
			this.map.remove();
		}
	}
}
