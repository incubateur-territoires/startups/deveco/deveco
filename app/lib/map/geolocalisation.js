import maplibregl from 'maplibre-gl';

import BaseMap from './base';

const wait = (ms) => {
	return new Promise((resolve) => setTimeout(resolve, ms));
};

const bleuDSFR = '#000091';

export default class extends BaseMap {
	constructor() {
		super();
	}

	static get observedAttributes() {
		return ['data'];
	}

	get config() {
		return {};
	}

	updateGeolocalisation({ longitude, latitude }) {
		if (this.timeout) {
			clearTimeout(this.timeout);
		}

		const that = this;

		this.timeout = setTimeout(() => {
			const event = new CustomEvent('movemap', {
				detail: {
					longitude,
					latitude,
				},
			});

			that.dispatchEvent(event);
		}, 100);
	}

	async attributeChanged(name, _oldValue, newValue) {
		if (!this.map) {
			// l'initialisation de MapLibre a échoué, inutile de chercher à faire quelque chose
			return;
		}

		try {
			if (name === 'data') {
				const map = this.map;

				const {
					centre,
					geolocalisationActuelle: geolocActuelle,
					geolocalisationContribution: geolocActuelleContrib,
					nouvelleGeolocalisation,
					zoom,
				} = JSON.parse(newValue);

				while (!map.isStyleLoaded()) {
					await wait(500);
				}

				if (!this.mapIsInitialized) {
					this.mapIsInitialized = true;
					// il faut désactiver les zooms avant de pouvoir les réactiver avec une option
					this.map.scrollZoom.disable();
					this.map.scrollZoom.enable({ around: 'center' });
					this.map.touchZoomRotate.disable();
					this.map.touchZoomRotate.enable({ around: 'center' });
				}

				if (!this.centre) {
					this.centre = centre;

					map.setCenter(centre ?? this.centreDeLaFrance);

					const defaultZoom = zoom ?? (geolocActuelle || geolocActuelleContrib ? 16 : 3);

					map.setZoom(defaultZoom);
				}

				if (nouvelleGeolocalisation) {
					map.setCenter(nouvelleGeolocalisation);
				}

				if (zoom) {
					map.setZoom(zoom);
				}

				if (geolocActuelle && !this.geolocActuelleMarker) {
					const marker = new maplibregl.Marker({
						color: bleuDSFR,
						className: 'marker-fill',
					})
						.setLngLat(geolocActuelle)
						.addTo(map)
						.setPopup(new maplibregl.Popup({ offset: 25 }).setText('Géolocalisation existante'));

					this.geolocActuelleMarker = marker;

					const markerElement = marker.getElement();

					const markerBody = markerElement.querySelectorAll('svg g[fill="' + bleuDSFR + '"]')[0];

					markerBody.setAttribute('fill', '#ffffff');
					markerBody.setAttribute('stroke', bleuDSFR);

					const markerCircle = markerElement.querySelectorAll('svg g circle[fill="#FFFFFF"]')[0];

					markerCircle.setAttribute('fill', bleuDSFR);
				}

				if (!this.centerMarker) {
					this.centerMarker = new maplibregl.Marker({
						color: bleuDSFR,
					})
						.setLngLat(map.getCenter())
						.addTo(map);
				}

				// bouge le marqueur au milieu de la carte à chaque mouvement
				map.on('dragend', () => {
					const { lng, lat } = map.getCenter();

					this.updateGeolocalisation({
						longitude: Number(lng.toFixed(6)),
						latitude: Number(lat.toFixed(6)),
					});
				});

				// bouge le marqueur au milieu de la carte à chaque mouvement
				map.on('move', () => {
					if (!this.centerMarker) {
						return;
					}

					const { lng, lat } = map.getCenter();

					this.centerMarker.setLngLat([lng.toFixed(6), lat.toFixed(6)]);
				});
			}
		} catch (error) {
			console.error(error);
		}
	}
}
