/**
 * Create a MapLibre scaling utility with logarithmic scaling
 * @param {number[]} data - Input data points to be scaled
 * @param {Object} [options={}] - Configuration options
 * @param {number} [options.minRadius=5] - Minimum radius for the smallest representative data point
 * @param {number} [options.maxRadius=50] - Maximum radius for the largest representative data point
 * @param {string} [options.minColor='#0000FF'] - Color for the lowest scaled value (blue)
 * @param {string} [options.maxColor='#FF0000'] - Color for the highest scaled value (red)
 * @param {number} [options.clipStd=2] - Number of standard deviations to use for clipping data
 * @param {string} [options.expression=['get', 'value']] - Expression to use for scaling
 * @param {number} [options.logBase=0.5] - Base for logarithmic scaling (0 < base < 1)
 * @returns {Object} Scaling utility with expressions
 */
function createMapLibreLogScaler(data, options = {}) {
	const {
		minRadius = 5,
		maxRadius = 50,
		minColor = '#0000FF', // Blue
		maxColor = '#FF0000', // Red
		clipStd = 2,
		expression = ['get', 'value'],
		logBase = 0.5, // Logarithmic scaling base
	} = options;

	// Remove zero or negative values which can't be log-scaled
	const positiveData = data.filter((val) => val > 0);

	// Compute statistics on positive data
	const mean = positiveData.reduce((sum, val) => sum + val, 0) / positiveData.length;

	// Define bounds for scaling
	const variance =
		positiveData.reduce((sum, val) => sum + Math.pow(val - mean, 2), 0) / positiveData.length;
	const std = Math.sqrt(variance);

	// Define bounds for scaling
	let lowerBound = Math.max(Math.min(...positiveData), mean - clipStd * std);
	let upperBound = Math.min(Math.max(...positiveData), mean + clipStd * std);
	if (lowerBound === upperBound) {
		upperBound = lowerBound + 1;
		lowerBound = lowerBound - 1;
	}

	/**
	 * Create a MapLibre expression for logarithmic radius scaling
	 * @returns {Array} MapLibre expression for logarithmic radius scaling
	 */
	function createRadiusExpression() {
		return [
			'interpolate',
			['exponential', logBase], // Use exponential with base < 1 for log-like scaling
			['min', ['max', expression, lowerBound], upperBound],
			lowerBound,
			minRadius,
			upperBound,
			maxRadius,
		];
	}

	/**
	 * Create a MapLibre expression for logarithmic color scaling
	 * @returns {Array} MapLibre expression for logarithmic color interpolation
	 */
	function createColorExpression() {
		return [
			'interpolate',
			['exponential', logBase],
			['min', ['max', expression, lowerBound], upperBound],
			lowerBound,
			minColor,
			upperBound,
			maxColor,
		];
	}

	/**
	 * Get scaling information for debugging
	 * @returns {Object} Scaling details
	 */
	function getScalingInfo() {
		return {
			mean,
			std,
			lowerBound,
			upperBound,
			minRadius,
			maxRadius,
			minColor,
			maxColor,
			expression,
			logBase,
		};
	}

	return {
		createRadiusExpression,
		createColorExpression,
		getScalingInfo,
	};
}

// Example usage
// function exampleLogScalingUsage() {
//     // Sample data with large amplitude (10 to 10000)
//     const sampleData = [10, 50, 100, 500, 1000, 5000, 10000];

//     // Create logarithmic scaler
//     const scaler = createMapLibreLogScaler(sampleData, {
//         minRadius: 5,
//         maxRadius: 50,
//         minColor: '#0000FF',  // Blue for low values
//         maxColor: '#FF0000',  // Red for high values
//         logBase: 0.5,         // Aggressive log scaling
//         property: 'value'
//     });

//     // Log scaling information
//     console.log('Scaling Info:', scaler.getScalingInfo());
//     console.log('Radius Expression:', scaler.createRadiusExpression());
//     console.log('Color Expression:', scaler.createColorExpression());
// }

// Export for module usage
export { createMapLibreLogScaler };
