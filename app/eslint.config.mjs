import importPlugin from 'eslint-plugin-import';
import js from '@eslint/js';
import globals from 'globals';
import eslintConfigPrettier from 'eslint-config-prettier';

export default [
	{
		ignores: [
			'node_modules/',
			'elm-stuff/',
			'build/',
			'dist/',
			'explorer/',
			'.github',
			'.husky',
			'.eslintcache',
			'.env',
			'**/elm.js',
			'**/elm.min.js',
		],
	},
	importPlugin.flatConfigs.recommended,
	js.configs.recommended,
	{
		languageOptions: {
			ecmaVersion: 'latest',
			sourceType: 'module',
			globals: {
				...globals.browser,
				iFrameResize: true,
				Elm: true,
				dsfr: true,
			},
		},
		rules: {
			'no-unused-vars': [
				'error',
				{
					ignoreRestSiblings: true,
					argsIgnorePattern: '^_',
					varsIgnorePattern: '^_',
				},
			],
			curly: 'error',
			// 'import/order': [
			// 	1,
			// 	{
			// 		'newlines-between': 'always',
			// 		groups: ['external', 'builtin', 'internal', 'sibling', 'parent', 'index'],
			// 	},
			// ],
		},
	},
	eslintConfigPrettier,
];
