module Explorer exposing (main)

import BaseUI.Button
import BaseUI.Icon
import BaseUI.Input
import Dict exposing (Dict)
import ElmBook exposing (Book, book)
import ElmBook.StatefulOptions
import Etablissement.Carte


main : Book State
main =
    book "Deveco"
        |> ElmBook.withStatefulOptions
            [ ElmBook.StatefulOptions.initialState initialState
            ]
        |> ElmBook.withChapterGroups
            [ ( "UI", [ BaseUI.Button.docs, BaseUI.Icon.docs, BaseUI.Input.docs ] )
            , ( "Établissement", [ Etablissement.Carte.docs ] )
            ]


type alias State =
    { inputs : Dict String String }


initialState : State
initialState =
    { inputs = BaseUI.Input.init }
