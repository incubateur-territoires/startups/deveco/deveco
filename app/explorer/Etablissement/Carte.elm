module Etablissement.Carte exposing (docs)

import Accessibility exposing (Html, div)
import DSFR.Grid as Grid
import Data.Etablissement exposing (EtablissementSimple, EtatAdministratif(..))
import Data.PortefeuilleInfos exposing (PortefeuilleInfos)
import Date
import ElmBook.Chapter as Chapter exposing (Chapter)
import Time
import UI.Etablissement


docs : Chapter state
docs =
    Chapter.chapter "Carte Établissement"
        |> Chapter.renderComponentList
            [ ( "Carte de base", carteBase True )
            , ( "Carte France", carteFrance True )
            , ( "Carte pas siège", cartePasSiege True )
            , ( "Carte France pas siège", carteFrancePasSiege True )
            , ( "Carte tout", carteTout True )
            , ( "Carte contacts", carteContact True )
            , ( "Carte echanges", carteEchange True )
            , ( "Carte rappels", carteRappel True )
            , ( "Carte favoris", carteFavori True )
            , ( "Liste de cartes", listeCartes )
            ]


{-|

    HELPERS

-}
carteBase : Bool -> Html msg
carteBase =
    UI.Etablissement.carte { toggleFavorite = Nothing } etablissement ficheInfos


carteFrance : Bool -> Html msg
carteFrance =
    UI.Etablissement.carte { toggleFavorite = Nothing } { etablissement | france = True } ficheInfos


cartePasSiege : Bool -> Html msg
cartePasSiege =
    UI.Etablissement.carte { toggleFavorite = Nothing } { etablissement | siege = False } ficheInfos


carteFrancePasSiege : Bool -> Html msg
carteFrancePasSiege =
    UI.Etablissement.carte { toggleFavorite = Nothing } { etablissement | france = True, siege = False } ficheInfos


carteTout : Bool -> Html msg
carteTout =
    UI.Etablissement.carte { toggleFavorite = Nothing }
        etablissement
        { ficheInfos
            | contacts = True
            , echanges = True
            , rappels = True
            , favori = True
        }


carteContact : Bool -> Html msg
carteContact =
    UI.Etablissement.carte { toggleFavorite = Nothing }
        etablissement
        { ficheInfos
            | contacts = True
        }


carteEchange : Bool -> Html msg
carteEchange =
    UI.Etablissement.carte { toggleFavorite = Nothing }
        etablissement
        { ficheInfos
            | echanges = True
        }


carteRappel : Bool -> Html msg
carteRappel =
    UI.Etablissement.carte { toggleFavorite = Nothing }
        etablissement
        { ficheInfos
            | rappels = True
        }


carteFavori : Bool -> Html msg
carteFavori =
    UI.Etablissement.carte { toggleFavorite = Nothing }
        etablissement
        { ficheInfos
            | favori = True
        }


listeCartes : Html msg
listeCartes =
    div []
        [ div [ Grid.gridRow ]
            [ carteBase False
            , carteFrance False
            , cartePasSiege False
            , carteFrancePasSiege False
            , carteTout False
            , carteContact False
            , carteEchange False
            , carteRappel False
            , carteFavori False
            ]
        ]


{-|

    FIXTURES

-}
etablissement : EtablissementSimple
etablissement =
    { siret = "90080075600012"
    , nomAffichage = "SAS Sanzot"
    , nom = Nothing
    , nomEntreprise = Just "SAS Sanzot"
    , adresse = "431 avenue du château - Moulinsart"
    , geolocalisation = Nothing
    , dateCreationEtablissement = Just <| Date.fromCalendarDate 2017 Time.Sep 31
    , dateFermetureEtablissement = Nothing
    , activiteNaf = Just "Commerce de détail de viandes et de produits à base de viande en magasin spécialisé (47.22Z)"
    , activiteNafId = Just "47.22Z"
    , etatAdministratif = Actif
    , clotureDateContribution = Nothing
    , enseigneContribution = Nothing
    , france = False
    , siege = True
    , procedure = False
    , diffusible = True
    , geolocalisationContribution = Nothing
    , geolocalisationEquipe = Nothing
    , inpiCaVariation = Nothing
    , emaVariation = Nothing
    }


ficheInfos : PortefeuilleInfos
ficheInfos =
    { contacts = False
    , echanges = False
    , demandes = False
    , rappels = False
    , favori = False
    }
