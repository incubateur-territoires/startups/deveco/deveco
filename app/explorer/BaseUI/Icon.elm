module BaseUI.Icon exposing (docs)

import Accessibility exposing (Html, div, span)
import DSFR.Icons
import DSFR.Icons.Buildings
import DSFR.Icons.Business
import DSFR.Icons.Communication
import DSFR.Icons.Design
import DSFR.Icons.Development
import DSFR.Icons.Device
import DSFR.Icons.Document
import DSFR.Icons.Editor
import DSFR.Icons.Finance
import DSFR.Icons.Health
import DSFR.Icons.Logo
import DSFR.Icons.Map
import DSFR.Icons.Media
import DSFR.Icons.Others
import DSFR.Icons.System
import DSFR.Icons.User
import DSFR.Icons.Weather
import ElmBook.Chapter as Chapter exposing (Chapter)
import Html.Attributes


docs : Chapter state
docs =
    Chapter.chapter "Icônes"
        |> Chapter.renderComponentList
            [ ( "Buildings"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.Buildings.all
              )
            , ( "Business"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.Business.all
              )
            , ( "Communication"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.Communication.all
              )
            , ( "Design"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.Design.all
              )
            , ( "Development"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.Development.all
              )
            , ( "Device"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.Device.all
              )
            , ( "Document"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.Document.all
              )
            , ( "Editor"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.Editor.all
              )
            , ( "Finance"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.Finance.all
              )
            , ( "Health"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.Health.all
              )
            , ( "Logo"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.Logo.all
              )
            , ( "Map"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.Map.all
              )
            , ( "Media"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.Media.all
              )
            , ( "Others"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.Others.all
              )
            , ( "System"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.System.all
              )
            , ( "User"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.User.all
              )
            , ( "Weather"
              , div [] <|
                    List.map viewIcon <|
                        DSFR.Icons.Weather.all
              )
            ]


viewIcon : DSFR.Icons.IconName -> Html msg
viewIcon iconName =
    span [ Html.Attributes.title <| DSFR.Icons.toClassName iconName ] [ DSFR.Icons.icon iconName ]
