module BaseUI.Button exposing (docs)

import DSFR.Button
import DSFR.Icons.System
import ElmBook
import ElmBook.Actions as Actions
import ElmBook.Chapter as Chapter exposing (Chapter)


docs : Chapter state
docs =
    Chapter.chapter "Boutons"
        |> Chapter.renderComponentList
            [ ( "Défaut"
              , default
                    |> DSFR.Button.view
              )
            , ( "Désactivé"
              , default
                    |> DSFR.Button.disable
                    |> DSFR.Button.view
              )
            , ( "Secondaire"
              , default
                    |> DSFR.Button.secondary
                    |> DSFR.Button.view
              )
            , ( "Secondaire désactivé"
              , default
                    |> DSFR.Button.secondary
                    |> DSFR.Button.disable
                    |> DSFR.Button.view
              )
            , ( "Icône à gauche"
              , default
                    |> DSFR.Button.leftIcon DSFR.Icons.System.successFill
                    |> DSFR.Button.view
              )
            , ( "Icône à droite désactivé"
              , default
                    |> DSFR.Button.rightIcon DSFR.Icons.System.errorFill
                    |> DSFR.Button.disable
                    |> DSFR.Button.view
              )
            , ( "Icône seule"
              , default
                    |> DSFR.Button.onlyIcon DSFR.Icons.System.infoFill
                    |> DSFR.Button.view
              )
            , ( "Petit"
              , default
                    |> DSFR.Button.small
                    |> DSFR.Button.view
              )
            , ( "Grand"
              , default
                    |> DSFR.Button.large
                    |> DSFR.Button.view
              )
            ]


onClick : ElmBook.Msg state
onClick =
    Actions.logAction "Clic\u{00A0}!"


default : DSFR.Button.ButtonConfig (ElmBook.Msg state)
default =
    DSFR.Button.new { onClick = Just onClick, label = "C'est parti\u{00A0}!" }
