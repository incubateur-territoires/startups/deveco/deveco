module BaseUI.Input exposing (docs, init)

import Accessibility exposing (text)
import DSFR.Input
import Dict exposing (Dict)
import ElmBook.Actions as Actions
import ElmBook.Chapter as Chapter exposing (Chapter)
import ElmBook.Custom


docs : Chapter (SharedState model)
docs =
    Chapter.chapter "Champs"
        |> Chapter.renderStatefulComponentList
            [ defaultInput "Texte"
            , numberInput "Nombre"
            , emailInput "Adresse email"
            , dateInput "Date"
            , passwordInput "Saisie de mot de passe"
            , newPasswordInput "Changement de mot de passe"
            , confirmPasswordInput "Confirmation du mot de passe"
            , selectInput "Sélecteur"
            , hintInput "Avec suggestion"
            , validInput "Avec validation OK"
            , errorInput "Avec erreur"
            ]


defaultInput : String -> ( String, SharedState model -> Accessibility.Html (ElmBook.Custom.Msg (SharedState model)) )
defaultInput name =
    (\{ inputs } ->
        inputs
            |> baseInput name
            |> DSFR.Input.view
    )
        |> Tuple.pair name


numberInput : String -> ( String, SharedState model -> Accessibility.Html (ElmBook.Custom.Msg (SharedState model)) )
numberInput name =
    (\{ inputs } ->
        inputs
            |> baseInput name
            |> DSFR.Input.number Nothing
            |> DSFR.Input.view
    )
        |> Tuple.pair name


emailInput : String -> ( String, SharedState model -> Accessibility.Html (ElmBook.Custom.Msg (SharedState model)) )
emailInput name =
    (\{ inputs } ->
        inputs
            |> baseInput name
            |> DSFR.Input.email
            |> DSFR.Input.view
    )
        |> Tuple.pair name


dateInput : String -> ( String, SharedState model -> Accessibility.Html (ElmBook.Custom.Msg (SharedState model)) )
dateInput name =
    (\{ inputs } ->
        inputs
            |> baseInput name
            |> DSFR.Input.date { min = Nothing, max = Nothing }
            |> DSFR.Input.view
    )
        |> Tuple.pair name


passwordInput : String -> ( String, SharedState model -> Accessibility.Html (ElmBook.Custom.Msg (SharedState model)) )
passwordInput name =
    (\{ inputs } ->
        inputs
            |> baseInput name
            |> DSFR.Input.password
            |> DSFR.Input.view
    )
        |> Tuple.pair name


newPasswordInput : String -> ( String, SharedState model -> Accessibility.Html (ElmBook.Custom.Msg (SharedState model)) )
newPasswordInput name =
    (\{ inputs } ->
        inputs
            |> baseInput name
            |> DSFR.Input.newPassword
            |> DSFR.Input.view
    )
        |> Tuple.pair name


confirmPasswordInput : String -> ( String, SharedState model -> Accessibility.Html (ElmBook.Custom.Msg (SharedState model)) )
confirmPasswordInput name =
    (\{ inputs } ->
        inputs
            |> baseInput name
            |> DSFR.Input.confirmPassword
            |> DSFR.Input.view
    )
        |> Tuple.pair name


selectInput : String -> ( String, SharedState model -> Accessibility.Html (ElmBook.Custom.Msg (SharedState model)) )
selectInput name =
    (\{ inputs } ->
        inputs
            |> baseInput name
            |> DSFR.Input.select
                { options =
                    [ "Option 1"
                    , "Option 2"
                    , "Option 3"
                    , "Option 4"
                    ]
                , toId = identity
                , toLabel = text
                , toDisabled = Nothing
                }
            |> DSFR.Input.view
    )
        |> Tuple.pair name


hintInput : String -> ( String, SharedState model -> Accessibility.Html (ElmBook.Custom.Msg (SharedState model)) )
hintInput name =
    (\{ inputs } ->
        inputs
            |> baseInput name
            |> DSFR.Input.withHint [ text "Tapez ce que vous voulez..." ]
            |> DSFR.Input.view
    )
        |> Tuple.pair name


validInput : String -> ( String, SharedState model -> Accessibility.Html (ElmBook.Custom.Msg (SharedState model)) )
validInput name =
    (\{ inputs } ->
        inputs
            |> baseInput name
            |> DSFR.Input.withValid (Just [ text "T'es au top\u{00A0}!" ])
            |> DSFR.Input.view
    )
        |> Tuple.pair name


errorInput : String -> ( String, SharedState model -> Accessibility.Html (ElmBook.Custom.Msg (SharedState model)) )
errorInput name =
    (\{ inputs } ->
        inputs
            |> baseInput name
            |> DSFR.Input.withError (Just [ text "Veuillez respecter les consignes de remplissage" ])
            |> DSFR.Input.view
    )
        |> Tuple.pair name


baseInput : String -> Dict String String -> DSFR.Input.InputConfig data (ElmBook.Custom.Msg (SharedState model))
baseInput name inputs =
    let
        value =
            inputs
                |> Dict.get name
                |> Maybe.withDefault ""
    in
    DSFR.Input.new
        { name = name
        , onInput = updateForInput name
        , label = text name
        , value = value
        }


type alias Model =
    Dict String String


type alias SharedState model =
    { model
        | inputs : Model
    }


updateSharedState : String -> String -> SharedState x -> SharedState x
updateSharedState name value model =
    { model
        | inputs = Dict.insert name value model.inputs
    }


init : Model
init =
    Dict.empty


updateForInput : String -> String -> ElmBook.Custom.Msg (SharedState model)
updateForInput name =
    Actions.updateStateWith <|
        updateSharedState name
