// VERSION FOR EXPOSING OBJECTS
//
// const fs = require("fs");
// const icons = require("./icon.json");
//
// const elmIcons = "./vendor/DSFR.Icons..elm";
//
// function capitalizeFirstLetter(string) {
//   return string.charAt(0).toUpperCase() + string.slice(1);
// }
//
// function lowercaseFirstLetter(string) {
//   return string.charAt(0).toLowerCase() + string.slice(1);
// }
//
// const logger = fs.createWriteStream(elmIcons, {
//   flags: "a", // 'a' means appending (old data will be preserved)
// });
//
// const categories = {};
//
// for (const icon of icons) {
//   const { name, family, category, path } = icon;
//   if (!categories[category]) {
//     categories[category] = {};
//   }
//   const normalized = lowercaseFirstLetter(
//     name.split("-").map(capitalizeFirstLetter).join("")
//   );
//   categories[category][normalized] = `fr-icon-${name}`;
// }
//
// for (const [catName, catIcons] of Object.entries(categories)) {
//   logger.write(`\n\n${catName} =\n`);
//   let counter = 0;
//   for (const [catIconName, catIconCode] of Object.entries(
//     categories[catName]
//   )) {
//     logger.write(
//       `    ${counter ? ", " : "{ "}${catIconName} = IconName "${catIconCode}"\n`
//     );
//     counter++;
//   }
//   logger.write(`  }\n`);
// }
//
// ==============================================================================
//
// VERSION FOR CREATING MODULES
//
const fs = require("fs");
const icons = require("./icon.json");

// const elmIcons = "./vendor/DSFR.Icons...elm";

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function lowercaseFirstLetter(string) {
  return string.charAt(0).toLowerCase() + string.slice(1);
}

const categories = {};

for (const icon of icons) {
  const { name, family, category, path } = icon;
  if (!categories[category]) {
    categories[category] = {};
  }
  const normalized = lowercaseFirstLetter(
    name.split("-").map(capitalizeFirstLetter).join("")
  );
  categories[category][normalized] = `fr-icon-${name}`;
}

for (const [catName, catIcons] of Object.entries(categories)) {
  const moduleName = capitalizeFirstLetter(catName);
  const elmIcons = `./vendor/DSFR/Icons/${moduleName}.elm`;
  const logger = fs.createWriteStream(elmIcons);

  logger.write(`module DSFR.Icons.${moduleName} exposing (..)\n`);
  logger.write(`\n`);
  logger.write(`import DSFR.Icons exposing (IconName, iconName)\n`);
  logger.write(`\n`);
  logger.write(`\n`);
  let counter = 0;
  for (const [catIconName, catIconCode] of Object.entries(
    categories[catName]
  )) {
    if (counter) {
      logger.write(`\n`);
      logger.write(`\n`);
    }
    logger.write(`${catIconName} : IconName\n`);
    logger.write(`${catIconName} =\n`);
    logger.write(`    iconName "${catIconCode}"\n`);
    counter++;
  }
}
