import "dotenv/config";
import { defineConfig } from "vite";

function manualChunks(id) {
	if (id.includes("highcharts")) {
		return "highcharts";
	}
	if (id.includes("@gouvfr/dsfr")) {
		return "dsfr";
	}
	if (id.includes("node_modules")) {
		return "vendor";
	}
}

export default defineConfig({
	plugins: [],
	server: {
		host: true,
		hmr: false,
		port: process.env.APP_PORT ? Number(process.env.APP_PORT) : undefined,
		proxy: {
			"/api": {
				target: `${process.env.API_URL}:${process.env.API_PORT}/`,
				rewrite: (path) => path.replace(/^\/api/, ""),
			},
			"/monitoring": {
				target: `${process.env.API_URL}:${process.env.MONITORING_PORT}/`,
				rewrite: (path) => path.replace(/^\/monitoring/, ""),
			},
		},
	},
	preview: {
		host: true,
		port: process.env.APP_PORT ? Number(process.env.APP_PORT) : undefined,
		proxy: {
			"/api": {
				target: `${process.env.API_URL}:${process.env.API_PORT}/`,
				rewrite: (path) => path.replace(/^\/api/, ""),
			},
			"/monitoring": {
				target: `${process.env.API_URL}:${process.env.MONITORING_PORT}/`,
				rewrite: (path) => path.replace(/^\/monitoring/, ""),
			},
		},
		cors: true,
	},
	publicDir: "./public",
	esbuild: {
		pure: [
			"F2",
			"F3",
			"F4",
			"F5",
			"F6",
			"F7",
			"F8",
			"F9",
			"A2",
			"A3",
			"A4",
			"A5",
			"A6",
			"A7",
			"A8",
			"A9",
		],
		minify: true,
	},
	build: {
		rollupOptions: {
			output: {
				manualChunks,
			},
		},
	},
});
