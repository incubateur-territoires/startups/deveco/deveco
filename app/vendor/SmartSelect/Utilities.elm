module SmartSelect.Utilities exposing
    ( KeyCode(..)
    , RemoteQueryAttrs
    , alwaysStopPropagation
    , eventIsOutsideComponent
    , newFocusedOptionIndexAfterSelection
    , preventDefault
    , toKeyCode
    )

{-| Utilities shared by the SmartSelect modules.


# SmartSelect settings

@docs ApiSearchAttrs

-}

import Http exposing (Header)
import Json.Decode as Decode exposing (Decoder)


{-| Fields to be provided to facilitate the external request. The function provided to url takes in searchText in the event it is necessary for the query.
-}
type alias RemoteQueryAttrs a =
    { headers : List Header
    , url : String -> String
    , optionDecoder : Decoder (List a)
    }


eventIsOutsideComponent : String -> Decoder Bool
eventIsOutsideComponent componentId =
    Decode.oneOf
        [ Decode.field "id" Decode.string
            |> Decode.andThen
                (\id ->
                    if componentId == id then
                        -- found match by id
                        Decode.succeed False

                    else
                        -- try next decoder
                        Decode.fail "check parent node"
                )
        , Decode.lazy (\_ -> eventIsOutsideComponent componentId |> Decode.field "parentNode")

        -- fallback if all previous decoders failed
        , Decode.succeed True
        ]


newFocusedOptionIndexAfterSelection : Int -> Int
newFocusedOptionIndexAfterSelection currentFocusedIdx =
    if currentFocusedIdx > 0 then
        currentFocusedIdx - 1

    else
        0


type KeyCode
    = Up
    | Down
    | Enter
    | Escape
    | Other


preventDefault : KeyCode -> Bool
preventDefault key =
    key == Up || key == Down || key == Enter


alwaysStopPropagation : msg -> ( msg, Bool )
alwaysStopPropagation msg =
    ( msg, True )


toKeyCode : String -> KeyCode
toKeyCode string =
    case string of
        "ArrowUp" ->
            Up

        "ArrowDown" ->
            Down

        "Enter" ->
            Enter

        "Escape" ->
            Escape

        _ ->
            Other
