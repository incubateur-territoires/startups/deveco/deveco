module DSFR.Tooltip exposing (clic, survol, wrap)

import Accessibility exposing (Html, span)
import Accessibility.Aria
import Html
import Html.Attributes exposing (class)


type Tooltip
    = Tooltip TooltipConfig


type alias TooltipConfig =
    { label : Html Never
    , id : String
    , type_ : TooltipType
    }


type TooltipType
    = Survol
    | Clic


survol : { label : Html Never, id : String } -> Tooltip
survol { label, id } =
    Tooltip <|
        TooltipConfig label id Survol


clic : { label : Html Never, id : String } -> Tooltip
clic { label, id } =
    Tooltip <|
        TooltipConfig label id Clic


view : Tooltip -> ( List (Html.Attribute Never), Html Never )
view (Tooltip { label, id, type_ }) =
    let
        tooltipId =
            "tooltip-" ++ id

        targetAttributes =
            case type_ of
                Survol ->
                    []

                Clic ->
                    [ class "fr-btn--tooltip" ]
    in
    ( Accessibility.Aria.describedBy [ tooltipId ]
        :: targetAttributes
    , span
        [ Accessibility.Aria.hidden True
        , class "fr-tooltip"
        , class "fr-placement"
        , Html.Attributes.id tooltipId
        ]
        [ label ]
    )


wrap : Tooltip -> Html msg -> Html msg
wrap t content =
    let
        ( attrs, tooltip ) =
            view t
    in
    span attrs
        [ content
        , Accessibility.map never tooltip
        ]
