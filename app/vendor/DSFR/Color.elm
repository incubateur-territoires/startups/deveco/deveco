module DSFR.Color exposing (..)


type CustomColor
    = Standard
    | GreenEmeraude
    | GreenMenthe
    | BlueCumulus
    | BlueEcume
    | BlueFrance
    | BrownCaramel
    | BrownOpera
    | PurpleGlycine


standard : CustomColor
standard =
    blueFrance


greenEmeraude : CustomColor
greenEmeraude =
    GreenEmeraude


greenMenthe : CustomColor
greenMenthe =
    GreenMenthe


blueCumulus : CustomColor
blueCumulus =
    BlueCumulus


blueEcume : CustomColor
blueEcume =
    BlueEcume


blueFrance : CustomColor
blueFrance =
    BlueFrance


brownCaramel : CustomColor
brownCaramel =
    BrownCaramel


brownOpera : CustomColor
brownOpera =
    BrownOpera


purpleGlycine : CustomColor
purpleGlycine =
    PurpleGlycine
