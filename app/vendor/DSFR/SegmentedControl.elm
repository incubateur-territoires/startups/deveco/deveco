module DSFR.SegmentedControl exposing (raw)

import Accessibility exposing (Html, div, fieldset, label, text)
import DSFR.Tooltip
import Html exposing (input)
import Html.Attributes exposing (class)
import Html.Events as Events


raw : { legend : String, toDisabled : option -> Maybe String, selected : option, options : List option, toId : option -> String, toDisplay : option -> String, selectMsg : option -> msg } -> Html msg
raw { legend, selected, toDisabled, options, toId, toDisplay, selectMsg } =
    fieldset
        [ class "fr-segmented fr-segmented--no-legend"
        ]
        [ Accessibility.legend
            [ class "fr-segmented__legend"
            ]
            [ text legend ]
        , div
            [ class "fr-segmented__elements"
            ]
          <|
            List.map (viewOption toDisabled selected toId toDisplay selectMsg) <|
                options
        ]


toTooltip : String -> String -> Html msg -> Html msg
toTooltip id motif =
    DSFR.Tooltip.survol { label = text motif, id = "segmented-control-motif-desactivation-" ++ id }
        |> DSFR.Tooltip.wrap


viewOption : (option -> Maybe String) -> option -> (option -> String) -> (option -> String) -> (option -> msg) -> option -> Html msg
viewOption toDisabled selected toId toDisplay selectMsg option =
    div
        [ class "fr-segmented__element"
        ]
        [ input
            [ Html.Attributes.value <| toId option
            , Html.Attributes.disabled <| (/=) Nothing <| toDisabled option
            , Html.Attributes.checked <| toId selected == toId option
            , Html.Attributes.type_ "radio"
            , Html.Attributes.id <| "segmented-option-" ++ toId option
            , Html.Attributes.name <| toId option
            , Events.onClick <| selectMsg option
            ]
            []
        , label
            [ Html.Attributes.class "fr-label"
            , Html.Attributes.for <| "segmented-option-" ++ toId option
            ]
            [ text <| toDisplay option ]
        ]
        |> (toDisabled option |> Maybe.map (toTooltip (toId option)) |> Maybe.withDefault identity)
