module DSFR.Icons.Document exposing
    ( all
    , articleFill
    , articleLine
    , book2Fill
    , book2Line
    , bookletFill
    , bookletLine
    , clipboardFill
    , clipboardLine
    , draftFill
    , draftLine
    , fileAddFill
    , fileAddLine
    , fileDownloadFill
    , fileDownloadLine
    , fileFill
    , fileLine
    , filePdfFill
    , filePdfLine
    , fileTextFill
    , fileTextLine
    , folder2Fill
    , folder2Line
    , newspaperFill
    , newspaperLine
    , surveyFill
    , surveyLine
    , todoFill
    , todoLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


articleFill : IconName
articleFill =
    nameToIcon "fr-icon-article-fill"


articleLine : IconName
articleLine =
    nameToIcon "fr-icon-article-line"


book2Fill : IconName
book2Fill =
    nameToIcon "fr-icon-book-2-fill"


book2Line : IconName
book2Line =
    nameToIcon "fr-icon-book-2-line"


bookletFill : IconName
bookletFill =
    nameToIcon "fr-icon-booklet-fill"


bookletLine : IconName
bookletLine =
    nameToIcon "fr-icon-booklet-line"


clipboardFill : IconName
clipboardFill =
    nameToIcon "fr-icon-clipboard-fill"


clipboardLine : IconName
clipboardLine =
    nameToIcon "fr-icon-clipboard-line"


draftFill : IconName
draftFill =
    nameToIcon "fr-icon-draft-fill"


draftLine : IconName
draftLine =
    nameToIcon "fr-icon-draft-line"


fileAddFill : IconName
fileAddFill =
    nameToIcon "fr-icon-file-add-fill"


fileAddLine : IconName
fileAddLine =
    nameToIcon "fr-icon-file-add-line"


fileDownloadFill : IconName
fileDownloadFill =
    nameToIcon "fr-icon-file-download-fill"


fileDownloadLine : IconName
fileDownloadLine =
    nameToIcon "fr-icon-file-download-line"


fileFill : IconName
fileFill =
    nameToIcon "fr-icon-file-fill"


fileLine : IconName
fileLine =
    nameToIcon "fr-icon-file-line"


filePdfFill : IconName
filePdfFill =
    nameToIcon "fr-icon-file-pdf-fill"


filePdfLine : IconName
filePdfLine =
    nameToIcon "fr-icon-file-pdf-line"


fileTextFill : IconName
fileTextFill =
    nameToIcon "fr-icon-file-text-fill"


fileTextLine : IconName
fileTextLine =
    nameToIcon "fr-icon-file-text-line"


folder2Fill : IconName
folder2Fill =
    nameToIcon "fr-icon-folder-2-fill"


folder2Line : IconName
folder2Line =
    nameToIcon "fr-icon-folder-2-line"


newspaperFill : IconName
newspaperFill =
    nameToIcon "fr-icon-newspaper-fill"


newspaperLine : IconName
newspaperLine =
    nameToIcon "fr-icon-newspaper-line"


surveyFill : IconName
surveyFill =
    nameToIcon "fr-icon-survey-fill"


surveyLine : IconName
surveyLine =
    nameToIcon "fr-icon-survey-line"


todoFill : IconName
todoFill =
    nameToIcon "fr-icon-todo-fill"


todoLine : IconName
todoLine =
    nameToIcon "fr-icon-todo-line"


all : List IconName
all =
    [ articleFill
    , articleLine
    , book2Fill
    , book2Line
    , bookletFill
    , bookletLine
    , clipboardFill
    , clipboardLine
    , draftFill
    , draftLine
    , fileAddFill
    , fileAddLine
    , fileDownloadFill
    , fileDownloadLine
    , fileFill
    , fileLine
    , filePdfFill
    , filePdfLine
    , fileTextFill
    , fileTextLine
    , folder2Fill
    , folder2Line
    , newspaperFill
    , newspaperLine
    , surveyFill
    , surveyLine
    , todoFill
    , todoLine
    ]
