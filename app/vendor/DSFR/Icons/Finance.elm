module DSFR.Icons.Finance exposing
    ( all
    , bankCardFill
    , bankCardLine
    , coinFill
    , giftFill
    , giftLine
    , moneyEuroBoxFill
    , moneyEuroBoxLine
    , moneyEuroCircleFill
    , moneyEuroCircleLine
    , securePaymentFill
    , securePaymentLine
    , shoppingBagFill
    , shoppingBagLine
    , shoppingCart2Fill
    , shoppingCart2Line
    , trophyFill
    , trophyLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


bankCardFill : IconName
bankCardFill =
    nameToIcon "fr-icon-bank-card-fill"


bankCardLine : IconName
bankCardLine =
    nameToIcon "fr-icon-bank-card-line"


coinFill : IconName
coinFill =
    nameToIcon "fr-icon-coin-fill"


giftFill : IconName
giftFill =
    nameToIcon "fr-icon-gift-fill"


giftLine : IconName
giftLine =
    nameToIcon "fr-icon-gift-line"


moneyEuroBoxFill : IconName
moneyEuroBoxFill =
    nameToIcon "fr-icon-money-euro-box-fill"


moneyEuroBoxLine : IconName
moneyEuroBoxLine =
    nameToIcon "fr-icon-money-euro-box-line"


moneyEuroCircleFill : IconName
moneyEuroCircleFill =
    nameToIcon "fr-icon-money-euro-circle-fill"


moneyEuroCircleLine : IconName
moneyEuroCircleLine =
    nameToIcon "fr-icon-money-euro-circle-line"


securePaymentFill : IconName
securePaymentFill =
    nameToIcon "fr-icon-secure-payment-fill"


securePaymentLine : IconName
securePaymentLine =
    nameToIcon "fr-icon-secure-payment-line"


shoppingBagFill : IconName
shoppingBagFill =
    nameToIcon "fr-icon-shopping-bag-fill"


shoppingBagLine : IconName
shoppingBagLine =
    nameToIcon "fr-icon-shopping-bag-line"


shoppingCart2Fill : IconName
shoppingCart2Fill =
    nameToIcon "fr-icon-shopping-cart-2-fill"


shoppingCart2Line : IconName
shoppingCart2Line =
    nameToIcon "fr-icon-shopping-cart-2-line"


trophyFill : IconName
trophyFill =
    nameToIcon "fr-icon-trophy-fill"


trophyLine : IconName
trophyLine =
    nameToIcon "fr-icon-trophy-line"


all : List IconName
all =
    [ bankCardFill
    , bankCardLine
    , coinFill
    , giftFill
    , giftLine
    , moneyEuroBoxFill
    , moneyEuroBoxLine
    , moneyEuroCircleFill
    , moneyEuroCircleLine
    , securePaymentFill
    , securePaymentLine
    , shoppingBagFill
    , shoppingBagLine
    , shoppingCart2Fill
    , shoppingCart2Line
    , trophyFill
    , trophyLine
    ]
