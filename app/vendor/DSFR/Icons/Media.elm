module DSFR.Icons.Media exposing
    ( all
    , cameraFill
    , cameraLine
    , clapperboardFill
    , clapperboardLine
    , equalizerFill
    , equalizerLine
    , filmFill
    , filmLine
    , galleryFill
    , galleryLine
    , headphoneFill
    , headphoneLine
    , imageAddFill
    , imageAddLine
    , imageEditFill
    , imageEditLine
    , imageFill
    , imageLine
    , liveFill
    , liveLine
    , micFill
    , micLine
    , music2Fill
    , music2Line
    , notification3Fill
    , notification3Line
    , pauseCircleFill
    , pauseCircleLine
    , playCircleFill
    , playCircleLine
    , stopCircleFill
    , stopCircleLine
    , transcription
    , volumeDownFill
    , volumeDownLine
    , volumeMuteFill
    , volumeMuteLine
    , volumeUpFill
    , volumeUpLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


cameraFill : IconName
cameraFill =
    nameToIcon "fr-icon-camera-fill"


cameraLine : IconName
cameraLine =
    nameToIcon "fr-icon-camera-line"


clapperboardFill : IconName
clapperboardFill =
    nameToIcon "fr-icon-clapperboard-fill"


clapperboardLine : IconName
clapperboardLine =
    nameToIcon "fr-icon-clapperboard-line"


equalizerFill : IconName
equalizerFill =
    nameToIcon "fr-icon-equalizer-fill"


equalizerLine : IconName
equalizerLine =
    nameToIcon "fr-icon-equalizer-line"


filmFill : IconName
filmFill =
    nameToIcon "fr-icon-film-fill"


filmLine : IconName
filmLine =
    nameToIcon "fr-icon-film-line"


galleryFill : IconName
galleryFill =
    nameToIcon "fr-icon-gallery-fill"


galleryLine : IconName
galleryLine =
    nameToIcon "fr-icon-gallery-line"


headphoneFill : IconName
headphoneFill =
    nameToIcon "fr-icon-headphone-fill"


headphoneLine : IconName
headphoneLine =
    nameToIcon "fr-icon-headphone-line"


imageAddFill : IconName
imageAddFill =
    nameToIcon "fr-icon-image-add-fill"


imageAddLine : IconName
imageAddLine =
    nameToIcon "fr-icon-image-add-line"


imageEditFill : IconName
imageEditFill =
    nameToIcon "fr-icon-image-edit-fill"


imageEditLine : IconName
imageEditLine =
    nameToIcon "fr-icon-image-edit-line"


imageFill : IconName
imageFill =
    nameToIcon "fr-icon-image-fill"


imageLine : IconName
imageLine =
    nameToIcon "fr-icon-image-line"


liveFill : IconName
liveFill =
    nameToIcon "fr-icon-live-fill"


liveLine : IconName
liveLine =
    nameToIcon "fr-icon-live-line"


micFill : IconName
micFill =
    nameToIcon "fr-icon-mic-fill"


micLine : IconName
micLine =
    nameToIcon "fr-icon-mic-line"


music2Fill : IconName
music2Fill =
    nameToIcon "fr-icon-music-2-fill"


music2Line : IconName
music2Line =
    nameToIcon "fr-icon-music-2-line"


notification3Fill : IconName
notification3Fill =
    nameToIcon "fr-icon-notification-3-fill"


notification3Line : IconName
notification3Line =
    nameToIcon "fr-icon-notification-3-line"


pauseCircleFill : IconName
pauseCircleFill =
    nameToIcon "fr-icon-pause-circle-fill"


pauseCircleLine : IconName
pauseCircleLine =
    nameToIcon "fr-icon-pause-circle-line"


playCircleFill : IconName
playCircleFill =
    nameToIcon "fr-icon-play-circle-fill"


playCircleLine : IconName
playCircleLine =
    nameToIcon "fr-icon-play-circle-line"


stopCircleFill : IconName
stopCircleFill =
    nameToIcon "fr-icon-stop-circle-fill"


stopCircleLine : IconName
stopCircleLine =
    nameToIcon "fr-icon-stop-circle-line"


transcription : IconName
transcription =
    nameToIcon "fr-icon-transcription"


volumeDownFill : IconName
volumeDownFill =
    nameToIcon "fr-icon-volume-down-fill"


volumeDownLine : IconName
volumeDownLine =
    nameToIcon "fr-icon-volume-down-line"


volumeMuteFill : IconName
volumeMuteFill =
    nameToIcon "fr-icon-volume-mute-fill"


volumeMuteLine : IconName
volumeMuteLine =
    nameToIcon "fr-icon-volume-mute-line"


volumeUpFill : IconName
volumeUpFill =
    nameToIcon "fr-icon-volume-up-fill"


volumeUpLine : IconName
volumeUpLine =
    nameToIcon "fr-icon-volume-up-line"


all : List IconName
all =
    [ cameraFill
    , cameraLine
    , clapperboardFill
    , clapperboardLine
    , equalizerFill
    , equalizerLine
    , filmFill
    , filmLine
    , galleryFill
    , galleryLine
    , headphoneFill
    , headphoneLine
    , imageAddFill
    , imageAddLine
    , imageEditFill
    , imageEditLine
    , imageFill
    , imageLine
    , liveFill
    , liveLine
    , micFill
    , micLine
    , music2Fill
    , music2Line
    , notification3Fill
    , notification3Line
    , pauseCircleFill
    , pauseCircleLine
    , playCircleFill
    , playCircleLine
    , stopCircleFill
    , stopCircleLine
    , transcription
    , volumeDownFill
    , volumeDownLine
    , volumeMuteFill
    , volumeMuteLine
    , volumeUpFill
    , volumeUpLine
    ]
