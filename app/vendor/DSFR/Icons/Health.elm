module DSFR.Icons.Health exposing
    ( all
    , capsuleFill
    , capsuleLine
    , dislikeFill
    , dislikeLine
    , dossierFill
    , dossierLine
    , firstAidKitFill
    , firstAidKitLine
    , handSanitizerFill
    , handSanitizerLine
    , healthBookFill
    , healthBookLine
    , heartFill
    , heartLine
    , heartPulseFill
    , heartPulseLine
    , lungsFill
    , lungsLine
    , medicineBottleFill
    , medicineBottleLine
    , mentalHealthFill
    , mentalHealthLine
    , microscopeFill
    , microscopeLine
    , psychotherapyFill
    , psychotherapyLine
    , pulseLine
    , stethoscopeFill
    , stethoscopeLine
    , surgicalMaskFill
    , surgicalMaskLine
    , syringeFill
    , syringeLine
    , testTubeFill
    , testTubeLine
    , thermometerFill
    , thermometerLine
    , virusFill
    , virusLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


capsuleFill : IconName
capsuleFill =
    nameToIcon "fr-icon-capsule-fill"


capsuleLine : IconName
capsuleLine =
    nameToIcon "fr-icon-capsule-line"


dislikeFill : IconName
dislikeFill =
    nameToIcon "fr-icon-dislike-fill"


dislikeLine : IconName
dislikeLine =
    nameToIcon "fr-icon-dislike-line"


dossierFill : IconName
dossierFill =
    nameToIcon "fr-icon-dossier-fill"


dossierLine : IconName
dossierLine =
    nameToIcon "fr-icon-dossier-line"


firstAidKitFill : IconName
firstAidKitFill =
    nameToIcon "fr-icon-first-aid-kit-fill"


firstAidKitLine : IconName
firstAidKitLine =
    nameToIcon "fr-icon-first-aid-kit-line"


handSanitizerFill : IconName
handSanitizerFill =
    nameToIcon "fr-icon-hand-sanitizer-fill"


handSanitizerLine : IconName
handSanitizerLine =
    nameToIcon "fr-icon-hand-sanitizer-line"


healthBookFill : IconName
healthBookFill =
    nameToIcon "fr-icon-health-book-fill"


healthBookLine : IconName
healthBookLine =
    nameToIcon "fr-icon-health-book-line"


heartFill : IconName
heartFill =
    nameToIcon "fr-icon-heart-fill"


heartLine : IconName
heartLine =
    nameToIcon "fr-icon-heart-line"


heartPulseFill : IconName
heartPulseFill =
    nameToIcon "fr-icon-heart-pulse-fill"


heartPulseLine : IconName
heartPulseLine =
    nameToIcon "fr-icon-heart-pulse-line"


lungsFill : IconName
lungsFill =
    nameToIcon "fr-icon-lungs-fill"


lungsLine : IconName
lungsLine =
    nameToIcon "fr-icon-lungs-line"


medicineBottleFill : IconName
medicineBottleFill =
    nameToIcon "fr-icon-medicine-bottle-fill"


medicineBottleLine : IconName
medicineBottleLine =
    nameToIcon "fr-icon-medicine-bottle-line"


mentalHealthFill : IconName
mentalHealthFill =
    nameToIcon "fr-icon-mental-health-fill"


mentalHealthLine : IconName
mentalHealthLine =
    nameToIcon "fr-icon-mental-health-line"


microscopeFill : IconName
microscopeFill =
    nameToIcon "fr-icon-microscope-fill"


microscopeLine : IconName
microscopeLine =
    nameToIcon "fr-icon-microscope-line"


psychotherapyFill : IconName
psychotherapyFill =
    nameToIcon "fr-icon-psychotherapy-fill"


psychotherapyLine : IconName
psychotherapyLine =
    nameToIcon "fr-icon-psychotherapy-line"


pulseLine : IconName
pulseLine =
    nameToIcon "fr-icon-pulse-line"


stethoscopeFill : IconName
stethoscopeFill =
    nameToIcon "fr-icon-stethoscope-fill"


stethoscopeLine : IconName
stethoscopeLine =
    nameToIcon "fr-icon-stethoscope-line"


surgicalMaskFill : IconName
surgicalMaskFill =
    nameToIcon "fr-icon-surgical-mask-fill"


surgicalMaskLine : IconName
surgicalMaskLine =
    nameToIcon "fr-icon-surgical-mask-line"


syringeFill : IconName
syringeFill =
    nameToIcon "fr-icon-syringe-fill"


syringeLine : IconName
syringeLine =
    nameToIcon "fr-icon-syringe-line"


testTubeFill : IconName
testTubeFill =
    nameToIcon "fr-icon-test-tube-fill"


testTubeLine : IconName
testTubeLine =
    nameToIcon "fr-icon-test-tube-line"


thermometerFill : IconName
thermometerFill =
    nameToIcon "fr-icon-thermometer-fill"


thermometerLine : IconName
thermometerLine =
    nameToIcon "fr-icon-thermometer-line"


virusFill : IconName
virusFill =
    nameToIcon "fr-icon-virus-fill"


virusLine : IconName
virusLine =
    nameToIcon "fr-icon-virus-line"


all : List IconName
all =
    [ capsuleFill
    , capsuleLine
    , dislikeFill
    , dislikeLine
    , dossierFill
    , dossierLine
    , firstAidKitFill
    , firstAidKitLine
    , handSanitizerFill
    , handSanitizerLine
    , healthBookFill
    , healthBookLine
    , heartFill
    , heartLine
    , heartPulseFill
    , heartPulseLine
    , lungsFill
    , lungsLine
    , medicineBottleFill
    , medicineBottleLine
    , mentalHealthFill
    , mentalHealthLine
    , microscopeFill
    , microscopeLine
    , psychotherapyFill
    , psychotherapyLine
    , pulseLine
    , stethoscopeFill
    , stethoscopeLine
    , surgicalMaskFill
    , surgicalMaskLine
    , syringeFill
    , syringeLine
    , testTubeFill
    , testTubeLine
    , thermometerFill
    , thermometerLine
    , virusFill
    , virusLine
    ]
