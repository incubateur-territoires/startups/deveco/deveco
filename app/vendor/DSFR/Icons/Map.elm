module DSFR.Icons.Map exposing
    ( all
    , anchorFill
    , anchorLine
    , bikeFill
    , bikeLine
    , busFill
    , busLine
    , carFill
    , carLine
    , caravanFill
    , caravanLine
    , chargingPile2Fill
    , chargingPile2Line
    , compass3Fill
    , compass3Line
    , cupFill
    , cupLine
    , earthFill
    , earthLine
    , franceFill
    , franceLine
    , gasStationFill
    , gasStationLine
    , gobletFill
    , gobletLine
    , mapPin2Fill
    , mapPin2Line
    , mapPinUserFill
    , mapPinUserLine
    , motorbikeFill
    , motorbikeLine
    , passportFill
    , passportLine
    , restaurantFill
    , restaurantLine
    , roadMapFill
    , roadMapLine
    , sailboatFill
    , sailboatLine
    , ship2Fill
    , ship2Line
    , signalTowerFill
    , signalTowerLine
    , suitcase2Fill
    , suitcase2Line
    , taxiFill
    , taxiLine
    , trainFill
    , trainLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


anchorFill : IconName
anchorFill =
    nameToIcon "fr-icon-anchor-fill"


anchorLine : IconName
anchorLine =
    nameToIcon "fr-icon-anchor-line"


bikeFill : IconName
bikeFill =
    nameToIcon "fr-icon-bike-fill"


bikeLine : IconName
bikeLine =
    nameToIcon "fr-icon-bike-line"


busFill : IconName
busFill =
    nameToIcon "fr-icon-bus-fill"


busLine : IconName
busLine =
    nameToIcon "fr-icon-bus-line"


carFill : IconName
carFill =
    nameToIcon "fr-icon-car-fill"


carLine : IconName
carLine =
    nameToIcon "fr-icon-car-line"


caravanFill : IconName
caravanFill =
    nameToIcon "fr-icon-caravan-fill"


caravanLine : IconName
caravanLine =
    nameToIcon "fr-icon-caravan-line"


chargingPile2Fill : IconName
chargingPile2Fill =
    nameToIcon "fr-icon-charging-pile-2-fill"


chargingPile2Line : IconName
chargingPile2Line =
    nameToIcon "fr-icon-charging-pile-2-line"


compass3Fill : IconName
compass3Fill =
    nameToIcon "fr-icon-compass-3-fill"


compass3Line : IconName
compass3Line =
    nameToIcon "fr-icon-compass-3-line"


cupFill : IconName
cupFill =
    nameToIcon "fr-icon-cup-fill"


cupLine : IconName
cupLine =
    nameToIcon "fr-icon-cup-line"


earthFill : IconName
earthFill =
    nameToIcon "fr-icon-earth-fill"


earthLine : IconName
earthLine =
    nameToIcon "fr-icon-earth-line"


franceFill : IconName
franceFill =
    nameToIcon "fr-icon-france-fill"


franceLine : IconName
franceLine =
    nameToIcon "fr-icon-france-line"


gasStationFill : IconName
gasStationFill =
    nameToIcon "fr-icon-gas-station-fill"


gasStationLine : IconName
gasStationLine =
    nameToIcon "fr-icon-gas-station-line"


gobletFill : IconName
gobletFill =
    nameToIcon "fr-icon-goblet-fill"


gobletLine : IconName
gobletLine =
    nameToIcon "fr-icon-goblet-line"


mapPin2Fill : IconName
mapPin2Fill =
    nameToIcon "fr-icon-map-pin-2-fill"


mapPin2Line : IconName
mapPin2Line =
    nameToIcon "fr-icon-map-pin-2-line"


mapPinUserFill : IconName
mapPinUserFill =
    nameToIcon "fr-icon-map-pin-user-fill"


mapPinUserLine : IconName
mapPinUserLine =
    nameToIcon "fr-icon-map-pin-user-line"


motorbikeFill : IconName
motorbikeFill =
    nameToIcon "fr-icon-motorbike-fill"


motorbikeLine : IconName
motorbikeLine =
    nameToIcon "fr-icon-motorbike-line"


passportFill : IconName
passportFill =
    nameToIcon "fr-icon-passport-fill"


passportLine : IconName
passportLine =
    nameToIcon "fr-icon-passport-line"


restaurantFill : IconName
restaurantFill =
    nameToIcon "fr-icon-restaurant-fill"


restaurantLine : IconName
restaurantLine =
    nameToIcon "fr-icon-restaurant-line"


roadMapFill : IconName
roadMapFill =
    nameToIcon "fr-icon-road-map-fill"


roadMapLine : IconName
roadMapLine =
    nameToIcon "fr-icon-road-map-line"


sailboatFill : IconName
sailboatFill =
    nameToIcon "fr-icon-sailboat-fill"


sailboatLine : IconName
sailboatLine =
    nameToIcon "fr-icon-sailboat-line"


ship2Fill : IconName
ship2Fill =
    nameToIcon "fr-icon-ship-2-fill"


ship2Line : IconName
ship2Line =
    nameToIcon "fr-icon-ship-2-line"


signalTowerFill : IconName
signalTowerFill =
    nameToIcon "fr-icon-signal-tower-fill"


signalTowerLine : IconName
signalTowerLine =
    nameToIcon "fr-icon-signal-tower-line"


suitcase2Fill : IconName
suitcase2Fill =
    nameToIcon "fr-icon-suitcase-2-fill"


suitcase2Line : IconName
suitcase2Line =
    nameToIcon "fr-icon-suitcase-2-line"


taxiFill : IconName
taxiFill =
    nameToIcon "fr-icon-taxi-fill"


taxiLine : IconName
taxiLine =
    nameToIcon "fr-icon-taxi-line"


trainFill : IconName
trainFill =
    nameToIcon "fr-icon-train-fill"


trainLine : IconName
trainLine =
    nameToIcon "fr-icon-train-line"


all : List IconName
all =
    [ anchorFill
    , anchorLine
    , bikeFill
    , bikeLine
    , busFill
    , busLine
    , carFill
    , carLine
    , caravanFill
    , caravanLine
    , chargingPile2Fill
    , chargingPile2Line
    , compass3Fill
    , compass3Line
    , cupFill
    , cupLine
    , earthFill
    , earthLine
    , franceFill
    , franceLine
    , gasStationFill
    , gasStationLine
    , gobletFill
    , gobletLine
    , mapPin2Fill
    , mapPin2Line
    , mapPinUserFill
    , mapPinUserLine
    , motorbikeFill
    , motorbikeLine
    , passportFill
    , passportLine
    , restaurantFill
    , restaurantLine
    , roadMapFill
    , roadMapLine
    , sailboatFill
    , sailboatLine
    , ship2Fill
    , ship2Line
    , signalTowerFill
    , signalTowerLine
    , suitcase2Fill
    , suitcase2Line
    , taxiFill
    , taxiLine
    , trainFill
    , trainLine
    ]
