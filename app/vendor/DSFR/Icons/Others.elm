module DSFR.Icons.Others exposing
    ( all
    , leafFill
    , leafLine
    , lightbulbFill
    , lightbulbLine
    , plantFill
    , plantLine
    , recycleFill
    , recycleLine
    , scales3Fill
    , scales3Line
    , seedlingFill
    , seedlingLine
    , umbrellaFill
    , umbrellaLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


leafFill : IconName
leafFill =
    nameToIcon "fr-icon-leaf-fill"


leafLine : IconName
leafLine =
    nameToIcon "fr-icon-leaf-line"


lightbulbFill : IconName
lightbulbFill =
    nameToIcon "fr-icon-lightbulb-fill"


lightbulbLine : IconName
lightbulbLine =
    nameToIcon "fr-icon-lightbulb-line"


plantFill : IconName
plantFill =
    nameToIcon "fr-icon-plant-fill"


plantLine : IconName
plantLine =
    nameToIcon "fr-icon-plant-line"


recycleFill : IconName
recycleFill =
    nameToIcon "fr-icon-recycle-fill"


recycleLine : IconName
recycleLine =
    nameToIcon "fr-icon-recycle-line"


scales3Fill : IconName
scales3Fill =
    nameToIcon "fr-icon-scales-3-fill"


scales3Line : IconName
scales3Line =
    nameToIcon "fr-icon-scales-3-line"


seedlingFill : IconName
seedlingFill =
    nameToIcon "fr-icon-seedling-fill"


seedlingLine : IconName
seedlingLine =
    nameToIcon "fr-icon-seedling-line"


umbrellaFill : IconName
umbrellaFill =
    nameToIcon "fr-icon-umbrella-fill"


umbrellaLine : IconName
umbrellaLine =
    nameToIcon "fr-icon-umbrella-line"


all : List IconName
all =
    [ leafFill
    , leafLine
    , lightbulbFill
    , lightbulbLine
    , plantFill
    , plantLine
    , recycleFill
    , recycleLine
    , scales3Fill
    , scales3Line
    , seedlingFill
    , seedlingLine
    , umbrellaFill
    , umbrellaLine
    ]
