module DSFR.Icons.System exposing
    ( addCircleFill
    , addCircleLine
    , addLine
    , alarmWarningFill
    , alarmWarningLine
    , alertFill
    , alertLine
    , all
    , arrowDownFill
    , arrowDownLine
    , arrowDownSFill
    , arrowDownSLine
    , arrowGoBackFill
    , arrowGoBackLine
    , arrowGoForwardFill
    , arrowGoForwardLine
    , arrowLeftFill
    , arrowLeftLine
    , arrowLeftSFill
    , arrowLeftSFirstLine
    , arrowLeftSLine
    , arrowLeftSLineDouble
    , arrowRightFill
    , arrowRightLine
    , arrowRightSFill
    , arrowRightSLastLine
    , arrowRightSLine
    , arrowRightSLineDouble
    , arrowRightUpLine
    , arrowUpFill
    , arrowUpLine
    , arrowUpSFill
    , arrowUpSLine
    , checkLine
    , checkboxCircleFill
    , checkboxCircleLine
    , checkboxFill
    , checkboxLine
    , closeCircleFill
    , closeCircleLine
    , closeLine
    , deleteFill
    , deleteLine
    , downloadFill
    , downloadLine
    , errorFill
    , errorLine
    , errorWarningFill
    , errorWarningLine
    , externalLinkFill
    , externalLinkLine
    , eyeFill
    , eyeLine
    , eyeOffFill
    , eyeOffLine
    , filterFill
    , filterLine
    , infoFill
    , infoLine
    , informationFill
    , informationLine
    , lockFill
    , lockLine
    , lockUnlockFill
    , lockUnlockLine
    , logoutBoxRFill
    , logoutBoxRLine
    , menu2Fill
    , menuFill
    , moreFill
    , moreLine
    , notificationBadgeFill
    , notificationBadgeLine
    , questionFill
    , questionLine
    , refreshFill
    , refreshLine
    , searchFill
    , searchLine
    , settings5Fill
    , settings5Line
    , shieldFill
    , shieldLine
    , starFill
    , starLine
    , starSFill
    , starSLine
    , subtractLine
    , successFill
    , successLine
    , themeFill
    , thumbDownFill
    , thumbDownLine
    , thumbUpFill
    , thumbUpLine
    , timeFill
    , timeLine
    , timerFill
    , timerLine
    , upload2Fill
    , upload2Line
    , uploadFill
    , uploadLine
    , warningFill
    , warningLine
    , zoomInFill
    , zoomInLine
    , zoomOutFill
    , zoomOutLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


addCircleFill : IconName
addCircleFill =
    nameToIcon "fr-icon-add-circle-fill"


addCircleLine : IconName
addCircleLine =
    nameToIcon "fr-icon-add-circle-line"


addLine : IconName
addLine =
    nameToIcon "fr-icon-add-line"


alarmWarningFill : IconName
alarmWarningFill =
    nameToIcon "fr-icon-alarm-warning-fill"


alarmWarningLine : IconName
alarmWarningLine =
    nameToIcon "fr-icon-alarm-warning-line"


alertFill : IconName
alertFill =
    nameToIcon "fr-icon-alert-fill"


alertLine : IconName
alertLine =
    nameToIcon "fr-icon-alert-line"


arrowDownFill : IconName
arrowDownFill =
    nameToIcon "fr-icon-arrow-down-fill"


arrowDownLine : IconName
arrowDownLine =
    nameToIcon "fr-icon-arrow-down-line"


arrowDownSFill : IconName
arrowDownSFill =
    nameToIcon "fr-icon-arrow-down-s-fill"


arrowDownSLine : IconName
arrowDownSLine =
    nameToIcon "fr-icon-arrow-down-s-line"


arrowGoBackFill : IconName
arrowGoBackFill =
    nameToIcon "fr-icon-arrow-go-back-fill"


arrowGoBackLine : IconName
arrowGoBackLine =
    nameToIcon "fr-icon-arrow-go-back-line"


arrowGoForwardFill : IconName
arrowGoForwardFill =
    nameToIcon "fr-icon-arrow-go-forward-fill"


arrowGoForwardLine : IconName
arrowGoForwardLine =
    nameToIcon "fr-icon-arrow-go-forward-line"


arrowLeftFill : IconName
arrowLeftFill =
    nameToIcon "fr-icon-arrow-left-fill"


arrowLeftLine : IconName
arrowLeftLine =
    nameToIcon "fr-icon-arrow-left-line"


arrowLeftSFill : IconName
arrowLeftSFill =
    nameToIcon "fr-icon-arrow-left-s-fill"


arrowLeftSLine : IconName
arrowLeftSLine =
    nameToIcon "fr-icon-arrow-left-s-line"


arrowRightFill : IconName
arrowRightFill =
    nameToIcon "fr-icon-arrow-right-fill"


arrowRightLine : IconName
arrowRightLine =
    nameToIcon "fr-icon-arrow-right-line"


arrowRightSFill : IconName
arrowRightSFill =
    nameToIcon "fr-icon-arrow-right-s-fill"


arrowRightSLine : IconName
arrowRightSLine =
    nameToIcon "fr-icon-arrow-right-s-line"


arrowRightUpLine : IconName
arrowRightUpLine =
    nameToIcon "fr-icon-arrow-right-up-line"


arrowUpFill : IconName
arrowUpFill =
    nameToIcon "fr-icon-arrow-up-fill"


arrowUpLine : IconName
arrowUpLine =
    nameToIcon "fr-icon-arrow-up-line"


arrowUpSFill : IconName
arrowUpSFill =
    nameToIcon "fr-icon-arrow-up-s-fill"


arrowUpSLine : IconName
arrowUpSLine =
    nameToIcon "fr-icon-arrow-up-s-line"


checkLine : IconName
checkLine =
    nameToIcon "fr-icon-check-line"


checkboxCircleFill : IconName
checkboxCircleFill =
    nameToIcon "fr-icon-checkbox-circle-fill"


checkboxCircleLine : IconName
checkboxCircleLine =
    nameToIcon "fr-icon-checkbox-circle-line"


checkboxFill : IconName
checkboxFill =
    nameToIcon "fr-icon-checkbox-fill"


checkboxLine : IconName
checkboxLine =
    nameToIcon "fr-icon-checkbox-line"


closeCircleFill : IconName
closeCircleFill =
    nameToIcon "fr-icon-close-circle-fill"


closeCircleLine : IconName
closeCircleLine =
    nameToIcon "fr-icon-close-circle-line"


closeLine : IconName
closeLine =
    nameToIcon "fr-icon-close-line"


deleteFill : IconName
deleteFill =
    nameToIcon "fr-icon-delete-fill"


deleteLine : IconName
deleteLine =
    nameToIcon "fr-icon-delete-line"


downloadFill : IconName
downloadFill =
    nameToIcon "fr-icon-download-fill"


downloadLine : IconName
downloadLine =
    nameToIcon "fr-icon-download-line"


errorWarningFill : IconName
errorWarningFill =
    nameToIcon "fr-icon-error-warning-fill"


errorWarningLine : IconName
errorWarningLine =
    nameToIcon "fr-icon-error-warning-line"


externalLinkFill : IconName
externalLinkFill =
    nameToIcon "fr-icon-external-link-fill"


externalLinkLine : IconName
externalLinkLine =
    nameToIcon "fr-icon-external-link-line"


eyeFill : IconName
eyeFill =
    nameToIcon "fr-icon-eye-fill"


eyeLine : IconName
eyeLine =
    nameToIcon "fr-icon-eye-line"


eyeOffFill : IconName
eyeOffFill =
    nameToIcon "fr-icon-eye-off-fill"


eyeOffLine : IconName
eyeOffLine =
    nameToIcon "fr-icon-eye-off-line"


filterFill : IconName
filterFill =
    nameToIcon "fr-icon-filter-fill"


filterLine : IconName
filterLine =
    nameToIcon "fr-icon-filter-line"


arrowLeftSFirstLine : IconName
arrowLeftSFirstLine =
    nameToIcon "fr-icon-arrow-left-s-first-line"


arrowLeftSLineDouble : IconName
arrowLeftSLineDouble =
    nameToIcon "fr-icon-arrow-left-s-line-double"


arrowRightSLastLine : IconName
arrowRightSLastLine =
    nameToIcon "fr-icon-arrow-right-s-last-line"


arrowRightSLineDouble : IconName
arrowRightSLineDouble =
    nameToIcon "fr-icon-arrow-right-s-line-double"


errorFill : IconName
errorFill =
    nameToIcon "fr-icon-error-fill"


errorLine : IconName
errorLine =
    nameToIcon "fr-icon-error-line"


infoFill : IconName
infoFill =
    nameToIcon "fr-icon-info-fill"


infoLine : IconName
infoLine =
    nameToIcon "fr-icon-info-line"


successFill : IconName
successFill =
    nameToIcon "fr-icon-success-fill"


successLine : IconName
successLine =
    nameToIcon "fr-icon-success-line"


themeFill : IconName
themeFill =
    nameToIcon "fr-icon-theme-fill"


warningFill : IconName
warningFill =
    nameToIcon "fr-icon-warning-fill"


warningLine : IconName
warningLine =
    nameToIcon "fr-icon-warning-line"


informationFill : IconName
informationFill =
    nameToIcon "fr-icon-information-fill"


informationLine : IconName
informationLine =
    nameToIcon "fr-icon-information-line"


lockFill : IconName
lockFill =
    nameToIcon "fr-icon-lock-fill"


lockLine : IconName
lockLine =
    nameToIcon "fr-icon-lock-line"


lockUnlockFill : IconName
lockUnlockFill =
    nameToIcon "fr-icon-lock-unlock-fill"


lockUnlockLine : IconName
lockUnlockLine =
    nameToIcon "fr-icon-lock-unlock-line"


logoutBoxRFill : IconName
logoutBoxRFill =
    nameToIcon "fr-icon-logout-box-r-fill"


logoutBoxRLine : IconName
logoutBoxRLine =
    nameToIcon "fr-icon-logout-box-r-line"


menu2Fill : IconName
menu2Fill =
    nameToIcon "fr-icon-menu-2-fill"


menuFill : IconName
menuFill =
    nameToIcon "fr-icon-menu-fill"


moreFill : IconName
moreFill =
    nameToIcon "fr-icon-more-fill"


moreLine : IconName
moreLine =
    nameToIcon "fr-icon-more-line"


notificationBadgeFill : IconName
notificationBadgeFill =
    nameToIcon "fr-icon-notification-badge-fill"


notificationBadgeLine : IconName
notificationBadgeLine =
    nameToIcon "fr-icon-notification-badge-line"


questionFill : IconName
questionFill =
    nameToIcon "fr-icon-question-fill"


questionLine : IconName
questionLine =
    nameToIcon "fr-icon-question-line"


refreshFill : IconName
refreshFill =
    nameToIcon "fr-icon-refresh-fill"


refreshLine : IconName
refreshLine =
    nameToIcon "fr-icon-refresh-line"


searchFill : IconName
searchFill =
    nameToIcon "fr-icon-search-fill"


searchLine : IconName
searchLine =
    nameToIcon "fr-icon-search-line"


settings5Fill : IconName
settings5Fill =
    nameToIcon "fr-icon-settings-5-fill"


settings5Line : IconName
settings5Line =
    nameToIcon "fr-icon-settings-5-line"


shieldFill : IconName
shieldFill =
    nameToIcon "fr-icon-shield-fill"


shieldLine : IconName
shieldLine =
    nameToIcon "fr-icon-shield-line"


starFill : IconName
starFill =
    nameToIcon "fr-icon-star-fill"


starLine : IconName
starLine =
    nameToIcon "fr-icon-star-line"


starSFill : IconName
starSFill =
    nameToIcon "fr-icon-star-s-fill"


starSLine : IconName
starSLine =
    nameToIcon "fr-icon-star-s-line"


subtractLine : IconName
subtractLine =
    nameToIcon "fr-icon-subtract-line"


thumbDownFill : IconName
thumbDownFill =
    nameToIcon "fr-icon-thumb-down-fill"


thumbDownLine : IconName
thumbDownLine =
    nameToIcon "fr-icon-thumb-down-line"


thumbUpFill : IconName
thumbUpFill =
    nameToIcon "fr-icon-thumb-up-fill"


thumbUpLine : IconName
thumbUpLine =
    nameToIcon "fr-icon-thumb-up-line"


timeFill : IconName
timeFill =
    nameToIcon "fr-icon-time-fill"


timeLine : IconName
timeLine =
    nameToIcon "fr-icon-time-line"


timerFill : IconName
timerFill =
    nameToIcon "fr-icon-timer-fill"


timerLine : IconName
timerLine =
    nameToIcon "fr-icon-timer-line"


upload2Fill : IconName
upload2Fill =
    nameToIcon "fr-icon-upload-2-fill"


upload2Line : IconName
upload2Line =
    nameToIcon "fr-icon-upload-2-line"


uploadFill : IconName
uploadFill =
    nameToIcon "fr-icon-upload-fill"


uploadLine : IconName
uploadLine =
    nameToIcon "fr-icon-upload-line"


zoomInFill : IconName
zoomInFill =
    nameToIcon "fr-icon-zoom-in-fill"


zoomInLine : IconName
zoomInLine =
    nameToIcon "fr-icon-zoom-in-line"


zoomOutFill : IconName
zoomOutFill =
    nameToIcon "fr-icon-zoom-out-fill"


zoomOutLine : IconName
zoomOutLine =
    nameToIcon "fr-icon-zoom-out-line"


all : List IconName
all =
    [ addCircleFill
    , addCircleLine
    , addLine
    , alarmWarningFill
    , alarmWarningLine
    , alertFill
    , alertLine
    , arrowDownFill
    , arrowDownLine
    , arrowDownSFill
    , arrowDownSLine
    , arrowGoBackFill
    , arrowGoBackLine
    , arrowGoForwardFill
    , arrowGoForwardLine
    , arrowLeftFill
    , arrowLeftLine
    , arrowLeftSFill
    , arrowLeftSLine
    , arrowRightFill
    , arrowRightLine
    , arrowRightSFill
    , arrowRightSLine
    , arrowRightUpLine
    , arrowUpFill
    , arrowUpLine
    , arrowUpSFill
    , arrowUpSLine
    , checkLine
    , checkboxCircleFill
    , checkboxCircleLine
    , checkboxFill
    , checkboxLine
    , closeCircleFill
    , closeCircleLine
    , closeLine
    , deleteFill
    , deleteLine
    , downloadFill
    , downloadLine
    , errorWarningFill
    , errorWarningLine
    , externalLinkFill
    , externalLinkLine
    , eyeFill
    , eyeLine
    , eyeOffFill
    , eyeOffLine
    , filterFill
    , filterLine
    , arrowLeftSFirstLine
    , arrowLeftSLineDouble
    , arrowRightSLastLine
    , arrowRightSLineDouble
    , errorFill
    , errorLine
    , infoFill
    , infoLine
    , successFill
    , successLine
    , themeFill
    , warningFill
    , warningLine
    , informationFill
    , informationLine
    , lockFill
    , lockLine
    , lockUnlockFill
    , lockUnlockLine
    , logoutBoxRFill
    , logoutBoxRLine
    , menu2Fill
    , menuFill
    , moreFill
    , moreLine
    , notificationBadgeFill
    , notificationBadgeLine
    , questionFill
    , questionLine
    , refreshFill
    , refreshLine
    , searchFill
    , searchLine
    , settings5Fill
    , settings5Line
    , shieldFill
    , shieldLine
    , starFill
    , starLine
    , starSFill
    , starSLine
    , subtractLine
    , thumbDownFill
    , thumbDownLine
    , thumbUpFill
    , thumbUpLine
    , timeFill
    , timeLine
    , timerFill
    , timerLine
    , upload2Fill
    , upload2Line
    , uploadFill
    , uploadLine
    , zoomInFill
    , zoomInLine
    , zoomOutFill
    , zoomOutLine
    ]
