module DSFR.Icons.Business exposing
    ( all
    , archiveFill
    , archiveLine
    , attachmentFill
    , attachmentLine
    , awardFill
    , awardLine
    , barChartBoxFill
    , barChartBoxLine
    , bookmarkFill
    , bookmarkLine
    , briefcaseFill
    , briefcaseLine
    , calendar2Fill
    , calendar2Line
    , calendarEventFill
    , calendarEventLine
    , calendarFill
    , calendarLine
    , cloudFill
    , cloudLine
    , copyrightFill
    , copyrightLine
    , customerServiceFill
    , customerServiceLine
    , flagFill
    , flagLine
    , globalFill
    , globalLine
    , lineChartFill
    , lineChartLine
    , linksFill
    , linksLine
    , mailFill
    , mailLine
    , mailOpenFill
    , mailOpenLine
    , medalFill
    , medalLine
    , pieChart2Fill
    , pieChart2Line
    , pieChartBoxFill
    , pieChartBoxLine
    , printerFill
    , printerLine
    , profilFill
    , profilLine
    , projector2Fill
    , projector2Line
    , sendPlaneFill
    , sendPlaneLine
    , slideshowFill
    , slideshowLine
    , windowFill
    , windowLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


archiveFill : IconName
archiveFill =
    nameToIcon "fr-icon-archive-fill"


archiveLine : IconName
archiveLine =
    nameToIcon "fr-icon-archive-line"


attachmentFill : IconName
attachmentFill =
    nameToIcon "fr-icon-attachment-fill"


attachmentLine : IconName
attachmentLine =
    nameToIcon "fr-icon-attachment-line"


awardFill : IconName
awardFill =
    nameToIcon "fr-icon-award-fill"


awardLine : IconName
awardLine =
    nameToIcon "fr-icon-award-line"


barChartBoxFill : IconName
barChartBoxFill =
    nameToIcon "fr-icon-bar-chart-box-fill"


barChartBoxLine : IconName
barChartBoxLine =
    nameToIcon "fr-icon-bar-chart-box-line"


bookmarkFill : IconName
bookmarkFill =
    nameToIcon "fr-icon-bookmark-fill"


bookmarkLine : IconName
bookmarkLine =
    nameToIcon "fr-icon-bookmark-line"


briefcaseFill : IconName
briefcaseFill =
    nameToIcon "fr-icon-briefcase-fill"


briefcaseLine : IconName
briefcaseLine =
    nameToIcon "fr-icon-briefcase-line"


calendar2Fill : IconName
calendar2Fill =
    nameToIcon "fr-icon-calendar-2-fill"


calendar2Line : IconName
calendar2Line =
    nameToIcon "fr-icon-calendar-2-line"


calendarEventFill : IconName
calendarEventFill =
    nameToIcon "fr-icon-calendar-event-fill"


calendarEventLine : IconName
calendarEventLine =
    nameToIcon "fr-icon-calendar-event-line"


calendarFill : IconName
calendarFill =
    nameToIcon "fr-icon-calendar-fill"


calendarLine : IconName
calendarLine =
    nameToIcon "fr-icon-calendar-line"


cloudFill : IconName
cloudFill =
    nameToIcon "fr-icon-cloud-fill"


cloudLine : IconName
cloudLine =
    nameToIcon "fr-icon-cloud-line"


copyrightFill : IconName
copyrightFill =
    nameToIcon "fr-icon-copyright-fill"


copyrightLine : IconName
copyrightLine =
    nameToIcon "fr-icon-copyright-line"


customerServiceFill : IconName
customerServiceFill =
    nameToIcon "fr-icon-customer-service-fill"


customerServiceLine : IconName
customerServiceLine =
    nameToIcon "fr-icon-customer-service-line"


flagFill : IconName
flagFill =
    nameToIcon "fr-icon-flag-fill"


flagLine : IconName
flagLine =
    nameToIcon "fr-icon-flag-line"


globalFill : IconName
globalFill =
    nameToIcon "fr-icon-global-fill"


globalLine : IconName
globalLine =
    nameToIcon "fr-icon-global-line"


lineChartFill : IconName
lineChartFill =
    nameToIcon "fr-icon-line-chart-fill"


lineChartLine : IconName
lineChartLine =
    nameToIcon "fr-icon-line-chart-line"


linksFill : IconName
linksFill =
    nameToIcon "fr-icon-links-fill"


linksLine : IconName
linksLine =
    nameToIcon "fr-icon-links-line"


mailFill : IconName
mailFill =
    nameToIcon "fr-icon-mail-fill"


mailLine : IconName
mailLine =
    nameToIcon "fr-icon-mail-line"


mailOpenFill : IconName
mailOpenFill =
    nameToIcon "fr-icon-mail-open-fill"


mailOpenLine : IconName
mailOpenLine =
    nameToIcon "fr-icon-mail-open-line"


medalFill : IconName
medalFill =
    nameToIcon "fr-icon-medal-fill"


medalLine : IconName
medalLine =
    nameToIcon "fr-icon-medal-line"


pieChart2Fill : IconName
pieChart2Fill =
    nameToIcon "fr-icon-pie-chart-2-fill"


pieChart2Line : IconName
pieChart2Line =
    nameToIcon "fr-icon-pie-chart-2-line"


pieChartBoxFill : IconName
pieChartBoxFill =
    nameToIcon "fr-icon-pie-chart-box-fill"


pieChartBoxLine : IconName
pieChartBoxLine =
    nameToIcon "fr-icon-pie-chart-box-line"


printerFill : IconName
printerFill =
    nameToIcon "fr-icon-printer-fill"


printerLine : IconName
printerLine =
    nameToIcon "fr-icon-printer-line"


profilFill : IconName
profilFill =
    nameToIcon "fr-icon-profil-fill"


profilLine : IconName
profilLine =
    nameToIcon "fr-icon-profil-line"


projector2Fill : IconName
projector2Fill =
    nameToIcon "fr-icon-projector-2-fill"


projector2Line : IconName
projector2Line =
    nameToIcon "fr-icon-projector-2-line"


sendPlaneFill : IconName
sendPlaneFill =
    nameToIcon "fr-icon-send-plane-fill"


sendPlaneLine : IconName
sendPlaneLine =
    nameToIcon "fr-icon-send-plane-line"


slideshowFill : IconName
slideshowFill =
    nameToIcon "fr-icon-slideshow-fill"


slideshowLine : IconName
slideshowLine =
    nameToIcon "fr-icon-slideshow-line"


windowFill : IconName
windowFill =
    nameToIcon "fr-icon-window-fill"


windowLine : IconName
windowLine =
    nameToIcon "fr-icon-window-line"


all : List IconName
all =
    [ archiveFill
    , archiveLine
    , attachmentFill
    , attachmentLine
    , awardFill
    , awardLine
    , barChartBoxFill
    , barChartBoxLine
    , bookmarkFill
    , bookmarkLine
    , briefcaseFill
    , briefcaseLine
    , calendar2Fill
    , calendar2Line
    , calendarEventFill
    , calendarEventLine
    , calendarFill
    , calendarLine
    , cloudFill
    , cloudLine
    , copyrightFill
    , copyrightLine
    , customerServiceFill
    , customerServiceLine
    , flagFill
    , flagLine
    , globalFill
    , globalLine
    , lineChartFill
    , lineChartLine
    , linksFill
    , linksLine
    , mailFill
    , mailLine
    , mailOpenFill
    , mailOpenLine
    , medalFill
    , medalLine
    , pieChart2Fill
    , pieChart2Line
    , pieChartBoxFill
    , pieChartBoxLine
    , printerFill
    , printerLine
    , profilFill
    , profilLine
    , projector2Fill
    , projector2Line
    , sendPlaneFill
    , sendPlaneLine
    , slideshowFill
    , slideshowLine
    , windowFill
    , windowLine
    ]
