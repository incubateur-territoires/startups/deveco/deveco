module DSFR.Icons.User exposing
    ( accountCircleFill
    , accountCircleLine
    , accountFill
    , accountLine
    , accountPinCircleFill
    , accountPinCircleLine
    , adminFill
    , adminLine
    , all
    , groupFill
    , groupLine
    , parentFill
    , parentLine
    , teamFill
    , teamLine
    , userAddFill
    , userAddLine
    , userFill
    , userHeartFill
    , userHeartLine
    , userLine
    , userSearchFill
    , userSearchLine
    , userSettingFill
    , userSettingLine
    , userStarFill
    , userStarLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


accountCircleFill : IconName
accountCircleFill =
    nameToIcon "fr-icon-account-circle-fill"


accountCircleLine : IconName
accountCircleLine =
    nameToIcon "fr-icon-account-circle-line"


accountFill : IconName
accountFill =
    nameToIcon "fr-icon-account-fill"


accountLine : IconName
accountLine =
    nameToIcon "fr-icon-account-line"


accountPinCircleFill : IconName
accountPinCircleFill =
    nameToIcon "fr-icon-account-pin-circle-fill"


accountPinCircleLine : IconName
accountPinCircleLine =
    nameToIcon "fr-icon-account-pin-circle-line"


adminFill : IconName
adminFill =
    nameToIcon "fr-icon-admin-fill"


adminLine : IconName
adminLine =
    nameToIcon "fr-icon-admin-line"


groupFill : IconName
groupFill =
    nameToIcon "fr-icon-group-fill"


groupLine : IconName
groupLine =
    nameToIcon "fr-icon-group-line"


parentFill : IconName
parentFill =
    nameToIcon "fr-icon-parent-fill"


parentLine : IconName
parentLine =
    nameToIcon "fr-icon-parent-line"


teamFill : IconName
teamFill =
    nameToIcon "fr-icon-team-fill"


teamLine : IconName
teamLine =
    nameToIcon "fr-icon-team-line"


userAddFill : IconName
userAddFill =
    nameToIcon "fr-icon-user-add-fill"


userAddLine : IconName
userAddLine =
    nameToIcon "fr-icon-user-add-line"


userFill : IconName
userFill =
    nameToIcon "fr-icon-user-fill"


userHeartFill : IconName
userHeartFill =
    nameToIcon "fr-icon-user-heart-fill"


userHeartLine : IconName
userHeartLine =
    nameToIcon "fr-icon-user-heart-line"


userLine : IconName
userLine =
    nameToIcon "fr-icon-user-line"


userSearchFill : IconName
userSearchFill =
    nameToIcon "fr-icon-user-search-fill"


userSearchLine : IconName
userSearchLine =
    nameToIcon "fr-icon-user-search-line"


userSettingFill : IconName
userSettingFill =
    nameToIcon "fr-icon-user-setting-fill"


userSettingLine : IconName
userSettingLine =
    nameToIcon "fr-icon-user-setting-line"


userStarFill : IconName
userStarFill =
    nameToIcon "fr-icon-user-star-fill"


userStarLine : IconName
userStarLine =
    nameToIcon "fr-icon-user-star-line"


all : List IconName
all =
    [ accountCircleFill
    , accountCircleLine
    , accountFill
    , accountLine
    , accountPinCircleFill
    , accountPinCircleLine
    , adminFill
    , adminLine
    , groupFill
    , groupLine
    , parentFill
    , parentLine
    , teamFill
    , teamLine
    , userAddFill
    , userAddLine
    , userFill
    , userHeartFill
    , userHeartLine
    , userLine
    , userSearchFill
    , userSearchLine
    , userSettingFill
    , userSettingLine
    , userStarFill
    , userStarLine
    ]
