module DSFR.Icons.Weather exposing
    ( all
    , cloudy2Fill
    , cloudy2Line
    , flashlightFill
    , flashlightLine
    , moonFill
    , moonLine
    , sunFill
    , sunLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


cloudy2Fill : IconName
cloudy2Fill =
    nameToIcon "fr-icon-cloudy-2-fill"


cloudy2Line : IconName
cloudy2Line =
    nameToIcon "fr-icon-cloudy-2-line"


flashlightFill : IconName
flashlightFill =
    nameToIcon "fr-icon-flashlight-fill"


flashlightLine : IconName
flashlightLine =
    nameToIcon "fr-icon-flashlight-line"


moonFill : IconName
moonFill =
    nameToIcon "fr-icon-moon-fill"


moonLine : IconName
moonLine =
    nameToIcon "fr-icon-moon-line"


sunFill : IconName
sunFill =
    nameToIcon "fr-icon-sun-fill"


sunLine : IconName
sunLine =
    nameToIcon "fr-icon-sun-line"


all : List IconName
all =
    [ cloudy2Fill
    , cloudy2Line
    , flashlightFill
    , flashlightLine
    , moonFill
    , moonLine
    , sunFill
    , sunLine
    ]
