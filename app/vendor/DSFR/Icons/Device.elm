module DSFR.Icons.Device exposing
    ( all
    , bluetoothFill
    , bluetoothLine
    , computerFill
    , computerLine
    , dashboard3Fill
    , dashboard3Line
    , databaseFill
    , databaseLine
    , deviceFill
    , deviceLine
    , hardDrive2Fill
    , hardDrive2Line
    , macFill
    , macLine
    , phoneFill
    , phoneLine
    , qrCodeFill
    , qrCodeLine
    , rssFill
    , rssLine
    , save3Fill
    , save3Line
    , saveFill
    , saveLine
    , serverFill
    , serverLine
    , smartphoneFill
    , smartphoneLine
    , tabletFill
    , tabletLine
    , tvFill
    , tvLine
    , wifiFill
    , wifiLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


bluetoothFill : IconName
bluetoothFill =
    nameToIcon "fr-icon-bluetooth-fill"


bluetoothLine : IconName
bluetoothLine =
    nameToIcon "fr-icon-bluetooth-line"


computerFill : IconName
computerFill =
    nameToIcon "fr-icon-computer-fill"


computerLine : IconName
computerLine =
    nameToIcon "fr-icon-computer-line"


dashboard3Fill : IconName
dashboard3Fill =
    nameToIcon "fr-icon-dashboard-3-fill"


dashboard3Line : IconName
dashboard3Line =
    nameToIcon "fr-icon-dashboard-3-line"


databaseFill : IconName
databaseFill =
    nameToIcon "fr-icon-database-fill"


databaseLine : IconName
databaseLine =
    nameToIcon "fr-icon-database-line"


deviceFill : IconName
deviceFill =
    nameToIcon "fr-icon-device-fill"


deviceLine : IconName
deviceLine =
    nameToIcon "fr-icon-device-line"


hardDrive2Fill : IconName
hardDrive2Fill =
    nameToIcon "fr-icon-hard-drive-2-fill"


hardDrive2Line : IconName
hardDrive2Line =
    nameToIcon "fr-icon-hard-drive-2-line"


macFill : IconName
macFill =
    nameToIcon "fr-icon-mac-fill"


macLine : IconName
macLine =
    nameToIcon "fr-icon-mac-line"


phoneFill : IconName
phoneFill =
    nameToIcon "fr-icon-phone-fill"


phoneLine : IconName
phoneLine =
    nameToIcon "fr-icon-phone-line"


qrCodeFill : IconName
qrCodeFill =
    nameToIcon "fr-icon-qr-code-fill"


qrCodeLine : IconName
qrCodeLine =
    nameToIcon "fr-icon-qr-code-line"


rssFill : IconName
rssFill =
    nameToIcon "fr-icon-rss-fill"


rssLine : IconName
rssLine =
    nameToIcon "fr-icon-rss-line"


save3Fill : IconName
save3Fill =
    nameToIcon "fr-icon-save-3-fill"


save3Line : IconName
save3Line =
    nameToIcon "fr-icon-save-3-line"


saveFill : IconName
saveFill =
    nameToIcon "fr-icon-save-fill"


saveLine : IconName
saveLine =
    nameToIcon "fr-icon-save-line"


serverFill : IconName
serverFill =
    nameToIcon "fr-icon-server-fill"


serverLine : IconName
serverLine =
    nameToIcon "fr-icon-server-line"


smartphoneFill : IconName
smartphoneFill =
    nameToIcon "fr-icon-smartphone-fill"


smartphoneLine : IconName
smartphoneLine =
    nameToIcon "fr-icon-smartphone-line"


tabletFill : IconName
tabletFill =
    nameToIcon "fr-icon-tablet-fill"


tabletLine : IconName
tabletLine =
    nameToIcon "fr-icon-tablet-line"


tvFill : IconName
tvFill =
    nameToIcon "fr-icon-tv-fill"


tvLine : IconName
tvLine =
    nameToIcon "fr-icon-tv-line"


wifiFill : IconName
wifiFill =
    nameToIcon "fr-icon-wifi-fill"


wifiLine : IconName
wifiLine =
    nameToIcon "fr-icon-wifi-line"


all : List IconName
all =
    [ bluetoothFill
    , bluetoothLine
    , computerFill
    , computerLine
    , dashboard3Fill
    , dashboard3Line
    , databaseFill
    , databaseLine
    , deviceFill
    , deviceLine
    , hardDrive2Fill
    , hardDrive2Line
    , macFill
    , macLine
    , phoneFill
    , phoneLine
    , qrCodeFill
    , qrCodeLine
    , rssFill
    , rssLine
    , save3Fill
    , save3Line
    , saveFill
    , saveLine
    , serverFill
    , serverLine
    , smartphoneFill
    , smartphoneLine
    , tabletFill
    , tabletLine
    , tvFill
    , tvLine
    , wifiFill
    , wifiLine
    ]
