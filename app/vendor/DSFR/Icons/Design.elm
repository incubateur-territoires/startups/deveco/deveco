module DSFR.Icons.Design exposing
    ( all
    , ballPenFill
    , ballPenLine
    , brush3Fill
    , brush3Line
    , brushFill
    , brushLine
    , contrastFill
    , contrastLine
    , cropFill
    , cropLine
    , dragMove2Fill
    , dragMove2Line
    , dropFill
    , dropLine
    , editBoxFill
    , editBoxLine
    , editFill
    , editLine
    , inkBottleFill
    , inkBottleLine
    , layoutGridFill
    , layoutGridLine
    , markPenFill
    , markPenLine
    , paintBrushFill
    , paintBrushLine
    , paintFill
    , paintLine
    , paletteFill
    , paletteLine
    , pantoneFill
    , pantoneLine
    , penNibFill
    , penNibLine
    , pencilFill
    , pencilLine
    , pencilRulerFill
    , pencilRulerLine
    , sipFill
    , sipLine
    , tableFill
    , tableLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


ballPenFill : IconName
ballPenFill =
    nameToIcon "fr-icon-ball-pen-fill"


ballPenLine : IconName
ballPenLine =
    nameToIcon "fr-icon-ball-pen-line"


brush3Fill : IconName
brush3Fill =
    nameToIcon "fr-icon-brush-3-fill"


brush3Line : IconName
brush3Line =
    nameToIcon "fr-icon-brush-3-line"


brushFill : IconName
brushFill =
    nameToIcon "fr-icon-brush-fill"


brushLine : IconName
brushLine =
    nameToIcon "fr-icon-brush-line"


contrastFill : IconName
contrastFill =
    nameToIcon "fr-icon-contrast-fill"


contrastLine : IconName
contrastLine =
    nameToIcon "fr-icon-contrast-line"


cropFill : IconName
cropFill =
    nameToIcon "fr-icon-crop-fill"


cropLine : IconName
cropLine =
    nameToIcon "fr-icon-crop-line"


dragMove2Fill : IconName
dragMove2Fill =
    nameToIcon "fr-icon-drag-move-2-fill"


dragMove2Line : IconName
dragMove2Line =
    nameToIcon "fr-icon-drag-move-2-line"


dropFill : IconName
dropFill =
    nameToIcon "fr-icon-drop-fill"


dropLine : IconName
dropLine =
    nameToIcon "fr-icon-drop-line"


editBoxFill : IconName
editBoxFill =
    nameToIcon "fr-icon-edit-box-fill"


editBoxLine : IconName
editBoxLine =
    nameToIcon "fr-icon-edit-box-line"


editFill : IconName
editFill =
    nameToIcon "fr-icon-edit-fill"


editLine : IconName
editLine =
    nameToIcon "fr-icon-edit-line"


inkBottleFill : IconName
inkBottleFill =
    nameToIcon "fr-icon-ink-bottle-fill"


inkBottleLine : IconName
inkBottleLine =
    nameToIcon "fr-icon-ink-bottle-line"


layoutGridFill : IconName
layoutGridFill =
    nameToIcon "fr-icon-layout-grid-fill"


layoutGridLine : IconName
layoutGridLine =
    nameToIcon "fr-icon-layout-grid-line"


markPenFill : IconName
markPenFill =
    nameToIcon "fr-icon-mark-pen-fill"


markPenLine : IconName
markPenLine =
    nameToIcon "fr-icon-mark-pen-line"


paintBrushFill : IconName
paintBrushFill =
    nameToIcon "fr-icon-paint-brush-fill"


paintBrushLine : IconName
paintBrushLine =
    nameToIcon "fr-icon-paint-brush-line"


paintFill : IconName
paintFill =
    nameToIcon "fr-icon-paint-fill"


paintLine : IconName
paintLine =
    nameToIcon "fr-icon-paint-line"


paletteFill : IconName
paletteFill =
    nameToIcon "fr-icon-palette-fill"


paletteLine : IconName
paletteLine =
    nameToIcon "fr-icon-palette-line"


pantoneFill : IconName
pantoneFill =
    nameToIcon "fr-icon-pantone-fill"


pantoneLine : IconName
pantoneLine =
    nameToIcon "fr-icon-pantone-line"


penNibFill : IconName
penNibFill =
    nameToIcon "fr-icon-pen-nib-fill"


penNibLine : IconName
penNibLine =
    nameToIcon "fr-icon-pen-nib-line"


pencilFill : IconName
pencilFill =
    nameToIcon "fr-icon-pencil-fill"


pencilLine : IconName
pencilLine =
    nameToIcon "fr-icon-pencil-line"


pencilRulerFill : IconName
pencilRulerFill =
    nameToIcon "fr-icon-pencil-ruler-fill"


pencilRulerLine : IconName
pencilRulerLine =
    nameToIcon "fr-icon-pencil-ruler-line"


sipFill : IconName
sipFill =
    nameToIcon "fr-icon-sip-fill"


sipLine : IconName
sipLine =
    nameToIcon "fr-icon-sip-line"


tableFill : IconName
tableFill =
    nameToIcon "fr-icon-table-fill"


tableLine : IconName
tableLine =
    nameToIcon "fr-icon-table-line"


all : List IconName
all =
    [ ballPenFill
    , ballPenLine
    , brush3Fill
    , brush3Line
    , brushFill
    , brushLine
    , contrastFill
    , contrastLine
    , cropFill
    , cropLine
    , dragMove2Fill
    , dragMove2Line
    , dropFill
    , dropLine
    , editBoxFill
    , editBoxLine
    , editFill
    , editLine
    , inkBottleFill
    , inkBottleLine
    , layoutGridFill
    , layoutGridLine
    , markPenFill
    , markPenLine
    , paintBrushFill
    , paintBrushLine
    , paintFill
    , paintLine
    , paletteFill
    , paletteLine
    , pantoneFill
    , pantoneLine
    , penNibFill
    , penNibLine
    , pencilFill
    , pencilLine
    , pencilRulerFill
    , pencilRulerLine
    , sipFill
    , sipLine
    , tableFill
    , tableLine
    ]
