module DSFR.Icons.Logo exposing
    ( all
    , chromeFill
    , chromeLine
    , dailymotionFill
    , dailymotionLine
    , edgeFill
    , edgeLine
    , facebookCircleFill
    , facebookCircleLine
    , firefoxFill
    , firefoxLine
    , githubFill
    , githubLine
    , googleFill
    , googleLine
    , ieFill
    , ieLine
    , instagramFill
    , instagramLine
    , linkedinBoxFill
    , linkedinBoxLine
    , mastodonFill
    , mastodonLine
    , npmjsFill
    , npmjsLine
    , remixiconFill
    , remixiconLine
    , safariFill
    , safariLine
    , slackFill
    , slackLine
    , snapchatFill
    , snapchatLine
    , telegramFill
    , telegramLine
    , tiktokFill
    , tiktokLine
    , twitchFill
    , twitchLine
    , twitterFill
    , twitterLine
    , vimeoFill
    , vimeoLine
    , vuejsFill
    , vuejsLine
    , youtubeFill
    , youtubeLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


chromeFill : IconName
chromeFill =
    nameToIcon "fr-icon-chrome-fill"


chromeLine : IconName
chromeLine =
    nameToIcon "fr-icon-chrome-line"


edgeFill : IconName
edgeFill =
    nameToIcon "fr-icon-edge-fill"


edgeLine : IconName
edgeLine =
    nameToIcon "fr-icon-edge-line"


facebookCircleFill : IconName
facebookCircleFill =
    nameToIcon "fr-icon-facebook-circle-fill"


facebookCircleLine : IconName
facebookCircleLine =
    nameToIcon "fr-icon-facebook-circle-line"


firefoxFill : IconName
firefoxFill =
    nameToIcon "fr-icon-firefox-fill"


firefoxLine : IconName
firefoxLine =
    nameToIcon "fr-icon-firefox-line"


dailymotionFill : IconName
dailymotionFill =
    nameToIcon "fr-icon-dailymotion-fill"


dailymotionLine : IconName
dailymotionLine =
    nameToIcon "fr-icon-dailymotion-line"


tiktokFill : IconName
tiktokFill =
    nameToIcon "fr-icon-tiktok-fill"


tiktokLine : IconName
tiktokLine =
    nameToIcon "fr-icon-tiktok-line"


githubFill : IconName
githubFill =
    nameToIcon "fr-icon-github-fill"


githubLine : IconName
githubLine =
    nameToIcon "fr-icon-github-line"


googleFill : IconName
googleFill =
    nameToIcon "fr-icon-google-fill"


googleLine : IconName
googleLine =
    nameToIcon "fr-icon-google-line"


ieFill : IconName
ieFill =
    nameToIcon "fr-icon-ie-fill"


ieLine : IconName
ieLine =
    nameToIcon "fr-icon-ie-line"


instagramFill : IconName
instagramFill =
    nameToIcon "fr-icon-instagram-fill"


instagramLine : IconName
instagramLine =
    nameToIcon "fr-icon-instagram-line"


linkedinBoxFill : IconName
linkedinBoxFill =
    nameToIcon "fr-icon-linkedin-box-fill"


linkedinBoxLine : IconName
linkedinBoxLine =
    nameToIcon "fr-icon-linkedin-box-line"


mastodonFill : IconName
mastodonFill =
    nameToIcon "fr-icon-mastodon-fill"


mastodonLine : IconName
mastodonLine =
    nameToIcon "fr-icon-mastodon-line"


npmjsFill : IconName
npmjsFill =
    nameToIcon "fr-icon-npmjs-fill"


npmjsLine : IconName
npmjsLine =
    nameToIcon "fr-icon-npmjs-line"


remixiconFill : IconName
remixiconFill =
    nameToIcon "fr-icon-remixicon-fill"


remixiconLine : IconName
remixiconLine =
    nameToIcon "fr-icon-remixicon-line"


safariFill : IconName
safariFill =
    nameToIcon "fr-icon-safari-fill"


safariLine : IconName
safariLine =
    nameToIcon "fr-icon-safari-line"


slackFill : IconName
slackFill =
    nameToIcon "fr-icon-slack-fill"


slackLine : IconName
slackLine =
    nameToIcon "fr-icon-slack-line"


snapchatFill : IconName
snapchatFill =
    nameToIcon "fr-icon-snapchat-fill"


snapchatLine : IconName
snapchatLine =
    nameToIcon "fr-icon-snapchat-line"


telegramFill : IconName
telegramFill =
    nameToIcon "fr-icon-telegram-fill"


telegramLine : IconName
telegramLine =
    nameToIcon "fr-icon-telegram-line"


twitchFill : IconName
twitchFill =
    nameToIcon "fr-icon-twitch-fill"


twitchLine : IconName
twitchLine =
    nameToIcon "fr-icon-twitch-line"


twitterFill : IconName
twitterFill =
    nameToIcon "fr-icon-twitter-fill"


twitterLine : IconName
twitterLine =
    nameToIcon "fr-icon-twitter-line"


vimeoFill : IconName
vimeoFill =
    nameToIcon "fr-icon-vimeo-fill"


vimeoLine : IconName
vimeoLine =
    nameToIcon "fr-icon-vimeo-line"


vuejsFill : IconName
vuejsFill =
    nameToIcon "fr-icon-vuejs-fill"


vuejsLine : IconName
vuejsLine =
    nameToIcon "fr-icon-vuejs-line"


youtubeFill : IconName
youtubeFill =
    nameToIcon "fr-icon-youtube-fill"


youtubeLine : IconName
youtubeLine =
    nameToIcon "fr-icon-youtube-line"


all : List IconName
all =
    [ chromeFill
    , chromeLine
    , edgeFill
    , edgeLine
    , facebookCircleFill
    , facebookCircleLine
    , firefoxFill
    , firefoxLine
    , dailymotionFill
    , dailymotionLine
    , tiktokFill
    , tiktokLine
    , githubFill
    , githubLine
    , googleFill
    , googleLine
    , ieFill
    , ieLine
    , instagramFill
    , instagramLine
    , linkedinBoxFill
    , linkedinBoxLine
    , mastodonFill
    , mastodonLine
    , npmjsFill
    , npmjsLine
    , remixiconFill
    , remixiconLine
    , safariFill
    , safariLine
    , slackFill
    , slackLine
    , snapchatFill
    , snapchatLine
    , telegramFill
    , telegramLine
    , twitchFill
    , twitchLine
    , twitterFill
    , twitterLine
    , vimeoFill
    , vimeoLine
    , vuejsFill
    , vuejsLine
    , youtubeFill
    , youtubeLine
    ]
