module DSFR.Icons.Development exposing
    ( all
    , bugFill
    , bugLine
    , codeBoxFill
    , codeBoxLine
    , codeSSlashLine
    , cursorFill
    , cursorLine
    , gitBranchFill
    , gitBranchLine
    , gitCommitFill
    , gitCommitLine
    , gitMergeFill
    , gitMergeLine
    , gitPullRequestFill
    , gitPullRequestLine
    , gitRepositoryCommitsFill
    , gitRepositoryCommitsLine
    , gitRepositoryFill
    , gitRepositoryLine
    , gitRepositoryPrivateFill
    , gitRepositoryPrivateLine
    , terminalBoxFill
    , terminalBoxLine
    , terminalLine
    , terminalWindowFill
    , terminalWindowLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


bugFill : IconName
bugFill =
    nameToIcon "fr-icon-bug-fill"


bugLine : IconName
bugLine =
    nameToIcon "fr-icon-bug-line"


codeBoxFill : IconName
codeBoxFill =
    nameToIcon "fr-icon-code-box-fill"


codeBoxLine : IconName
codeBoxLine =
    nameToIcon "fr-icon-code-box-line"


codeSSlashLine : IconName
codeSSlashLine =
    nameToIcon "fr-icon-code-s-slash-line"


cursorFill : IconName
cursorFill =
    nameToIcon "fr-icon-cursor-fill"


cursorLine : IconName
cursorLine =
    nameToIcon "fr-icon-cursor-line"


gitBranchFill : IconName
gitBranchFill =
    nameToIcon "fr-icon-git-branch-fill"


gitBranchLine : IconName
gitBranchLine =
    nameToIcon "fr-icon-git-branch-line"


gitCommitFill : IconName
gitCommitFill =
    nameToIcon "fr-icon-git-commit-fill"


gitCommitLine : IconName
gitCommitLine =
    nameToIcon "fr-icon-git-commit-line"


gitMergeFill : IconName
gitMergeFill =
    nameToIcon "fr-icon-git-merge-fill"


gitMergeLine : IconName
gitMergeLine =
    nameToIcon "fr-icon-git-merge-line"


gitPullRequestFill : IconName
gitPullRequestFill =
    nameToIcon "fr-icon-git-pull-request-fill"


gitPullRequestLine : IconName
gitPullRequestLine =
    nameToIcon "fr-icon-git-pull-request-line"


gitRepositoryCommitsFill : IconName
gitRepositoryCommitsFill =
    nameToIcon "fr-icon-git-repository-commits-fill"


gitRepositoryCommitsLine : IconName
gitRepositoryCommitsLine =
    nameToIcon "fr-icon-git-repository-commits-line"


gitRepositoryFill : IconName
gitRepositoryFill =
    nameToIcon "fr-icon-git-repository-fill"


gitRepositoryLine : IconName
gitRepositoryLine =
    nameToIcon "fr-icon-git-repository-line"


gitRepositoryPrivateFill : IconName
gitRepositoryPrivateFill =
    nameToIcon "fr-icon-git-repository-private-fill"


gitRepositoryPrivateLine : IconName
gitRepositoryPrivateLine =
    nameToIcon "fr-icon-git-repository-private-line"


terminalBoxFill : IconName
terminalBoxFill =
    nameToIcon "fr-icon-terminal-box-fill"


terminalBoxLine : IconName
terminalBoxLine =
    nameToIcon "fr-icon-terminal-box-line"


terminalLine : IconName
terminalLine =
    nameToIcon "fr-icon-terminal-line"


terminalWindowFill : IconName
terminalWindowFill =
    nameToIcon "fr-icon-terminal-window-fill"


terminalWindowLine : IconName
terminalWindowLine =
    nameToIcon "fr-icon-terminal-window-line"


all : List IconName
all =
    [ bugFill
    , bugLine
    , codeBoxFill
    , codeBoxLine
    , codeSSlashLine
    , cursorFill
    , cursorLine
    , gitBranchFill
    , gitBranchLine
    , gitCommitFill
    , gitCommitLine
    , gitMergeFill
    , gitMergeLine
    , gitPullRequestFill
    , gitPullRequestLine
    , gitRepositoryCommitsFill
    , gitRepositoryCommitsLine
    , gitRepositoryFill
    , gitRepositoryLine
    , gitRepositoryPrivateFill
    , gitRepositoryPrivateLine
    , terminalBoxFill
    , terminalBoxLine
    , terminalLine
    , terminalWindowFill
    , terminalWindowLine
    ]
