module DSFR.Icons.Communication exposing
    ( all
    , chat2Fill
    , chat2Line
    , chat3Fill
    , chat3Line
    , chatCheckFill
    , chatCheckLine
    , chatDeleteFill
    , chatDeleteLine
    , chatPollFill
    , chatPollLine
    , discussFill
    , discussLine
    , feedbackFill
    , feedbackLine
    , message2Fill
    , message2Line
    , questionAnswerFill
    , questionAnswerLine
    , questionnaireFill
    , questionnaireLine
    , videoChatFill
    , videoChatLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


chat2Fill : IconName
chat2Fill =
    nameToIcon "fr-icon-chat-2-fill"


chat2Line : IconName
chat2Line =
    nameToIcon "fr-icon-chat-2-line"


chat3Fill : IconName
chat3Fill =
    nameToIcon "fr-icon-chat-3-fill"


chat3Line : IconName
chat3Line =
    nameToIcon "fr-icon-chat-3-line"


chatCheckFill : IconName
chatCheckFill =
    nameToIcon "fr-icon-chat-check-fill"


chatCheckLine : IconName
chatCheckLine =
    nameToIcon "fr-icon-chat-check-line"


chatDeleteFill : IconName
chatDeleteFill =
    nameToIcon "fr-icon-chat-delete-fill"


chatDeleteLine : IconName
chatDeleteLine =
    nameToIcon "fr-icon-chat-delete-line"


chatPollFill : IconName
chatPollFill =
    nameToIcon "fr-icon-chat-poll-fill"


chatPollLine : IconName
chatPollLine =
    nameToIcon "fr-icon-chat-poll-line"


discussFill : IconName
discussFill =
    nameToIcon "fr-icon-discuss-fill"


discussLine : IconName
discussLine =
    nameToIcon "fr-icon-discuss-line"


feedbackFill : IconName
feedbackFill =
    nameToIcon "fr-icon-feedback-fill"


feedbackLine : IconName
feedbackLine =
    nameToIcon "fr-icon-feedback-line"


message2Fill : IconName
message2Fill =
    nameToIcon "fr-icon-message-2-fill"


message2Line : IconName
message2Line =
    nameToIcon "fr-icon-message-2-line"


questionAnswerFill : IconName
questionAnswerFill =
    nameToIcon "fr-icon-question-answer-fill"


questionAnswerLine : IconName
questionAnswerLine =
    nameToIcon "fr-icon-question-answer-line"


questionnaireFill : IconName
questionnaireFill =
    nameToIcon "fr-icon-questionnaire-fill"


questionnaireLine : IconName
questionnaireLine =
    nameToIcon "fr-icon-questionnaire-line"


videoChatFill : IconName
videoChatFill =
    nameToIcon "fr-icon-video-chat-fill"


videoChatLine : IconName
videoChatLine =
    nameToIcon "fr-icon-video-chat-line"


all : List IconName
all =
    [ chat2Fill
    , chat2Line
    , chat3Fill
    , chat3Line
    , chatCheckFill
    , chatCheckLine
    , chatDeleteFill
    , chatDeleteLine
    , chatPollFill
    , chatPollLine
    , discussFill
    , discussLine
    , feedbackFill
    , feedbackLine
    , message2Fill
    , message2Line
    , questionAnswerFill
    , questionAnswerLine
    , questionnaireFill
    , questionnaireLine
    , videoChatFill
    , videoChatLine
    , videoChatLine
    ]
