module DSFR.Icons.Buildings exposing
    ( all
    , ancientGateFill
    , ancientGateLine
    , ancientPavilionFill
    , ancientPavilionLine
    , bankFill
    , bankLine
    , buildingFill
    , buildingLine
    , communityFill
    , communityLine
    , governmentFill
    , governmentLine
    , home4Fill
    , home4Line
    , hospitalFill
    , hospitalLine
    , hotelFill
    , hotelLine
    , storeFill
    , storeLine
    )

import DSFR.Icons exposing (IconName, nameToIcon)


ancientGateFill : IconName
ancientGateFill =
    nameToIcon "fr-icon-ancient-gate-fill"


ancientGateLine : IconName
ancientGateLine =
    nameToIcon "fr-icon-ancient-gate-line"


ancientPavilionFill : IconName
ancientPavilionFill =
    nameToIcon "fr-icon-ancient-pavilion-fill"


ancientPavilionLine : IconName
ancientPavilionLine =
    nameToIcon "fr-icon-ancient-pavilion-line"


bankFill : IconName
bankFill =
    nameToIcon "fr-icon-bank-fill"


bankLine : IconName
bankLine =
    nameToIcon "fr-icon-bank-line"


buildingFill : IconName
buildingFill =
    nameToIcon "fr-icon-building-fill"


buildingLine : IconName
buildingLine =
    nameToIcon "fr-icon-building-line"


communityFill : IconName
communityFill =
    nameToIcon "fr-icon-community-fill"


communityLine : IconName
communityLine =
    nameToIcon "fr-icon-community-line"


governmentFill : IconName
governmentFill =
    nameToIcon "fr-icon-government-fill"


governmentLine : IconName
governmentLine =
    nameToIcon "fr-icon-government-line"


home4Fill : IconName
home4Fill =
    nameToIcon "fr-icon-home-4-fill"


home4Line : IconName
home4Line =
    nameToIcon "fr-icon-home-4-line"


hospitalFill : IconName
hospitalFill =
    nameToIcon "fr-icon-hospital-fill"


hospitalLine : IconName
hospitalLine =
    nameToIcon "fr-icon-hospital-line"


hotelFill : IconName
hotelFill =
    nameToIcon "fr-icon-hotel-fill"


hotelLine : IconName
hotelLine =
    nameToIcon "fr-icon-hotel-line"


storeFill : IconName
storeFill =
    nameToIcon "fr-icon-store-fill"


storeLine : IconName
storeLine =
    nameToIcon "fr-icon-store-line"


all : List IconName
all =
    [ ancientGateFill
    , ancientGateLine
    , ancientPavilionFill
    , ancientPavilionLine
    , bankFill
    , bankLine
    , buildingFill
    , buildingLine
    , communityFill
    , communityLine
    , governmentFill
    , governmentLine
    , home4Fill
    , home4Line
    , hospitalFill
    , hospitalLine
    , hotelFill
    , hotelLine
    , storeFill
    , storeLine
    ]
