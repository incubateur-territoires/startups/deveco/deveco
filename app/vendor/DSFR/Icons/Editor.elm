module DSFR.Icons.Editor exposing
    ( all
    , bold
    , codeView
    , fontSize
    , h1
    , h2
    , h3
    , h4
    , h5
    , h6
    , hashtag
    , highlight
    , italic
    , link
    , linkUnlink
    , listOrdered
    , listUnordered
    , questionMark
    , quoteFill
    , quoteLine
    , separator
    , space
    , subscript
    , superscript
    , table2
    , translate2
    )

import DSFR.Icons exposing (IconName, nameToIcon)


codeView : IconName
codeView =
    nameToIcon "fr-icon-code-view"


fontSize : IconName
fontSize =
    nameToIcon "fr-icon-font-size"


bold : IconName
bold =
    nameToIcon "fr-icon-bold"


highlight : IconName
highlight =
    nameToIcon "fr-icon-highlight"


quoteFill : IconName
quoteFill =
    nameToIcon "fr-icon-quote-fill"


quoteLine : IconName
quoteLine =
    nameToIcon "fr-icon-quote-line"


h1 : IconName
h1 =
    nameToIcon "fr-icon-h-1"


h2 : IconName
h2 =
    nameToIcon "fr-icon-h-2"


h3 : IconName
h3 =
    nameToIcon "fr-icon-h-3"


h4 : IconName
h4 =
    nameToIcon "fr-icon-h-4"


h5 : IconName
h5 =
    nameToIcon "fr-icon-h-5"


h6 : IconName
h6 =
    nameToIcon "fr-icon-h-6"


hashtag : IconName
hashtag =
    nameToIcon "fr-icon-hashtag"


italic : IconName
italic =
    nameToIcon "fr-icon-italic"


linkUnlink : IconName
linkUnlink =
    nameToIcon "fr-icon-link-unlink"


link : IconName
link =
    nameToIcon "fr-icon-link"


listOrdered : IconName
listOrdered =
    nameToIcon "fr-icon-list-ordered"


listUnordered : IconName
listUnordered =
    nameToIcon "fr-icon-list-unordered"


questionMark : IconName
questionMark =
    nameToIcon "fr-icon-question-mark"


separator : IconName
separator =
    nameToIcon "fr-icon-separator"


space : IconName
space =
    nameToIcon "fr-icon-space"


subscript : IconName
subscript =
    nameToIcon "fr-icon-subscript"


superscript : IconName
superscript =
    nameToIcon "fr-icon-superscript"


table2 : IconName
table2 =
    nameToIcon "fr-icon-table-2"


translate2 : IconName
translate2 =
    nameToIcon "fr-icon-translate-2"


all : List IconName
all =
    [ codeView
    , fontSize
    , bold
    , highlight
    , quoteFill
    , quoteLine
    , h1
    , h2
    , h3
    , h4
    , h5
    , h6
    , hashtag
    , italic
    , linkUnlink
    , link
    , listOrdered
    , listUnordered
    , questionMark
    , separator
    , space
    , subscript
    , superscript
    , table2
    , translate2
    ]
