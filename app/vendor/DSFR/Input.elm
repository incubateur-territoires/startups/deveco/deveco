module DSFR.Input exposing (InputConfig, InputType(..), MandatoryInputConfig, OptionalInputConfig, confirmPassword, date, defaultOptions, email, input, new, newPassword, number, password, select, textArea, textDisplay, view, withDisabled, withError, withExtraAttrs, withHint, withOptions, withReadonly, withRequired, withTextInputAttrs, withType, withValid)

import Accessibility exposing (Attribute, Html, div, inputNumber, inputText, label, option, p, span, text, textarea)
import Accessibility.Aria as Aria
import DSFR.Typography
import Html
import Html.Attributes as Attr
import Html.Attributes.Extra exposing (empty)
import Html.Events as Events
import Html.Extra exposing (nothing, static, viewIf)
import Json.Encode as Encode


input : MandatoryInputConfig msg -> Html msg
input config =
    view { mandatory = config, optional = defaultOptions }


type InputType data
    = TextInput (Maybe { minlength : Maybe Int, maxlength : Maybe Int })
    | EmailInput
    | PasswordInput
    | NewPasswordInput
    | ConfirmPasswordInput
    | TextArea (Maybe Int)
    | DateInput { min : Maybe String, max : Maybe String }
    | NumberInput (Maybe { step : Maybe String, min : Maybe String, max : Maybe String })
    | TextDisplay
    | SelectInput (SelectInputOptions data)


type alias SelectInputOptions data =
    { options : List data
    , toId : data -> String
    , toLabel : data -> Html Never
    , toDisabled : Maybe (data -> Bool)
    }


new : MandatoryInputConfig msg -> InputConfig data msg
new mandatory =
    { mandatory = mandatory, optional = defaultOptions }


withOptions : OptionalInputConfig data msg -> InputConfig data msg -> InputConfig data msg
withOptions optional config =
    { config | optional = optional }


withHint : List (Html Never) -> InputConfig data msg -> InputConfig data msg
withHint hint { mandatory, optional } =
    { mandatory = mandatory, optional = { optional | hint = hint } }


withError : Maybe (List (Html msg)) -> InputConfig data msg -> InputConfig data msg
withError errorMsg { mandatory, optional } =
    { mandatory = mandatory, optional = { optional | errorMsg = errorMsg } }


withValid : Maybe (List (Html msg)) -> InputConfig data msg -> InputConfig data msg
withValid validMsg { mandatory, optional } =
    { mandatory = mandatory, optional = { optional | validMsg = validMsg } }


withDisabled : Bool -> InputConfig data msg -> InputConfig data msg
withDisabled disabled { mandatory, optional } =
    { mandatory = mandatory, optional = { optional | disabled = disabled } }


withReadonly : Bool -> InputConfig data msg -> InputConfig data msg
withReadonly readonly { mandatory, optional } =
    { mandatory = mandatory, optional = { optional | readonly = readonly } }


withType : InputType data -> InputConfig data msg -> InputConfig data msg
withType type_ { mandatory, optional } =
    { mandatory = mandatory, optional = { optional | type_ = type_ } }


textArea : Maybe Int -> InputConfig data msg -> InputConfig data msg
textArea rows =
    withType <| TextArea rows


email : InputConfig data msg -> InputConfig data msg
email =
    withType <| EmailInput


password : InputConfig data msg -> InputConfig data msg
password =
    withType <| PasswordInput


newPassword : InputConfig data msg -> InputConfig data msg
newPassword =
    withType <| NewPasswordInput


confirmPassword : InputConfig data msg -> InputConfig data msg
confirmPassword =
    withType <| ConfirmPasswordInput


textDisplay : InputConfig data msg -> InputConfig data msg
textDisplay =
    withType TextDisplay


date : { min : Maybe String, max : Maybe String } -> InputConfig data msg -> InputConfig data msg
date dateConfig =
    withType <| DateInput dateConfig


number : Maybe { step : Maybe String, min : Maybe String, max : Maybe String } -> InputConfig data msg -> InputConfig data msg
number numberConfig =
    withType <| NumberInput numberConfig


select : { options : List data, toId : data -> String, toLabel : data -> Html Never, toDisabled : Maybe (data -> Bool) } -> InputConfig data msg -> InputConfig data msg
select selectOptions =
    withType (SelectInput selectOptions)


withExtraAttrs : List (Attribute Never) -> InputConfig data msg -> InputConfig data msg
withExtraAttrs extraAttrs { mandatory, optional } =
    { mandatory = mandatory, optional = { optional | extraAttrs = extraAttrs } }


withTextInputAttrs : Maybe { minlength : Maybe Int, maxlength : Maybe Int } -> InputConfig data msg -> InputConfig data msg
withTextInputAttrs config { mandatory, optional } =
    { mandatory = mandatory
    , optional =
        { optional
            | type_ =
                case optional.type_ of
                    TextInput _ ->
                        TextInput config

                    _ ->
                        optional.type_
        }
    }


withRequired : Bool -> InputConfig data msg -> InputConfig data msg
withRequired required { mandatory, optional } =
    { mandatory = mandatory, optional = { optional | required = required } }


type alias InputConfig data msg =
    { mandatory : MandatoryInputConfig msg
    , optional : OptionalInputConfig data msg
    }


type alias MandatoryInputConfig msg =
    { value : String
    , onInput : String -> msg
    , label : Html Never
    , name : String
    }


type alias OptionalInputConfig data msg =
    { disabled : Bool
    , readonly : Bool
    , validMsg : Maybe (List (Html msg))
    , errorMsg : Maybe (List (Html msg))
    , hint : List (Html Never)
    , icon : Maybe String
    , type_ : InputType data
    , extraAttrs : List (Attribute Never)
    , required : Bool
    }


defaultOptions : OptionalInputConfig data msg
defaultOptions =
    { disabled = False
    , readonly = False
    , validMsg = Nothing
    , errorMsg = Nothing
    , hint = []
    , icon = Nothing
    , type_ = TextInput Nothing
    , extraAttrs = []
    , required = False
    }


view : InputConfig data msg -> Html msg
view { mandatory, optional } =
    let
        { onInput, value } =
            mandatory

        name =
            "input-" ++ mandatory.name

        { errorMsg, validMsg, disabled, readonly, hint, icon, type_, extraAttrs, required } =
            optional

        defaultInputAttrs =
            [ Attr.class "fr-input"
            , Attr.classList
                [ ( "fr-input--valid", validMsg /= Nothing )
                , ( "fr-input--error", errorMsg /= Nothing )
                ]
            , Html.Attributes.Extra.attributeIf (Nothing /= validMsg) <|
                Aria.describedBy [ name ++ "-desc-valid" ]
            , Html.Attributes.Extra.attributeIf (Nothing /= errorMsg) <|
                Aria.describedBy [ name ++ "-desc-error" ]
            , Attr.id name
            , Attr.name name
            , Attr.value value
            , Attr.disabled disabled
            , Attr.readonly readonly
            , Events.onInput onInput
            , Attr.property "autocomplete" <| Encode.string name
            , Attr.required required
            ]

        iconWrapper =
            case icon of
                Nothing ->
                    identity

                Just iconName ->
                    List.singleton
                        >> div [ Attr.class "fr-input-wrap", Attr.class iconName ]

        ( inp, cl, wrapperAttrs ) =
            case type_ of
                TextInput config ->
                    ( inputText name <|
                        (defaultInputAttrs
                            ++ [ Attr.type_ "text"
                               , config
                                    |> Maybe.andThen .maxlength
                                    |> Maybe.map Attr.maxlength
                                    |> Maybe.withDefault empty
                               , config
                                    |> Maybe.andThen .minlength
                                    |> Maybe.map Attr.minlength
                                    |> Maybe.withDefault empty
                               ]
                        )
                    , "input"
                    , Nothing
                    )

                EmailInput ->
                    ( inputText name <|
                        (defaultInputAttrs ++ [ Attr.type_ "email" ])
                    , "input"
                    , Nothing
                    )

                PasswordInput ->
                    ( inputText name <|
                        (defaultInputAttrs
                            ++ [ Attr.type_ "password"
                               , Attr.class "fr-password__input"
                               , Aria.describedBy [ name ++ "-input-messages" ]
                               ]
                        )
                    , "input"
                    , Just <| Attr.class "fr-password"
                    )

                NewPasswordInput ->
                    ( inputText name <|
                        (defaultInputAttrs
                            ++ [ Attr.type_ "password"
                               , Attr.class "fr-password__input"
                               , Aria.describedBy [ name ++ "-input-messages" ]
                               ]
                        )
                    , "input"
                    , Just <| Attr.class "fr-password"
                    )

                ConfirmPasswordInput ->
                    ( inputText name <|
                        (defaultInputAttrs
                            ++ [ Attr.type_ "password"
                               , Attr.class "fr-password__input"
                               ]
                        )
                    , "input"
                    , Nothing
                    )

                TextArea rows ->
                    ( textarea ((rows |> Maybe.map Attr.rows |> Maybe.withDefault empty) :: defaultInputAttrs) []
                    , "input"
                    , Nothing
                    )

                DateInput dateConfig ->
                    let
                        ( mi, ma ) =
                            ( dateConfig.min |> Maybe.map Attr.min |> Maybe.withDefault empty, dateConfig.max |> Maybe.map Attr.max |> Maybe.withDefault empty )
                    in
                    ( Html.input (defaultInputAttrs ++ [ Attr.type_ "date", mi, ma ]) []
                    , "input"
                    , Nothing
                    )

                NumberInput config ->
                    ( inputNumber name <|
                        (defaultInputAttrs
                            ++ [ Attr.type_ "number"
                               , Attr.attribute "inputmode" "numeric"
                               , Attr.pattern "[0-9,\\.\\s]*"
                               , config
                                    |> Maybe.andThen .min
                                    |> Maybe.map Attr.min
                                    |> Maybe.withDefault empty
                               , config
                                    |> Maybe.andThen .max
                                    |> Maybe.map Attr.max
                                    |> Maybe.withDefault empty
                               , config
                                    |> Maybe.andThen .step
                                    |> Maybe.map Attr.step
                                    |> Maybe.withDefault empty
                               ]
                        )
                    , "input"
                    , Nothing
                    )

                TextDisplay ->
                    ( div [ Attr.class "mt-[0.5rem] py-[0.5rem]", DSFR.Typography.textBold ]
                        [ text <|
                            if value == "" then
                                "-"

                            else
                                value
                        ]
                    , "input"
                    , Nothing
                    )

                SelectInput { options, toId, toLabel, toDisabled } ->
                    ( Accessibility.select
                        (defaultInputAttrs
                            ++ [ Attr.class "fr-select"
                               ]
                        )
                      <|
                        List.map
                            (\s ->
                                option
                                    [ Attr.value <| toId s
                                    , Attr.name <| toId s
                                    , Attr.selected <| (==) value <| toId <| s
                                    , toDisabled
                                        |> Maybe.map (\fn -> Attr.disabled <| fn s)
                                        |> Maybe.withDefault empty
                                    ]
                                    [ static <| toLabel s ]
                            )
                        <|
                            options
                    , "select"
                    , Nothing
                    )

        toggleMotDePasse =
            div
                [ Attr.class "fr-password__checkbox fr-checkbox-group fr-checkbox-group--sm"
                ]
                [ Html.input
                    [ Attr.attribute "aria-label" "Afficher le mot de passe"
                    , Attr.id <| name ++ "-show"
                    , Attr.type_ "checkbox"
                    , Attr.attribute "aria-describedby" <| name ++ "-show-messages"
                    ]
                    []
                , label
                    [ Attr.class "fr-password__checkbox fr-label"
                    , Attr.for <| name ++ "-show"
                    ]
                    [ text "Afficher" ]
                , div
                    [ Attr.class "fr-messages-group"
                    , Attr.id <| name ++ "-show-messages"
                    , Attr.attribute "aria-live" "assertive"
                    ]
                    []
                ]

        informationMotDePasse =
            div
                [ Attr.class "fr-messages-group"
                , Attr.id <| name ++ "-input-messages"
                , Attr.attribute "aria-live" "assertive"
                ]
                [ p
                    [ Attr.class "fr-message"
                    , Attr.id <| name ++ "-input-message"
                    ]
                    [ text "Votre mot de passe doit contenir\u{00A0}:" ]
                , p
                    [ Attr.class "fr-message fr-message--info"
                    , Attr.id <| name ++ "-input-message-info"
                    ]
                    [ text "10 caractères minimum" ]
                ]
    in
    div
        ((Attr.class <| "fr-" ++ cl ++ "-group")
            :: Attr.classList
                [ ( "fr-" ++ cl ++ "-group--valid", Nothing /= validMsg )
                , ( "fr-" ++ cl ++ "-group--error", Nothing /= errorMsg )
                , ( "fr-" ++ cl ++ "-group--disabled", disabled )
                ]
            :: Maybe.withDefault empty wrapperAttrs
            :: extraAttrs
        )
        [ static <|
            label
                [ Attr.class "fr-label"
                , Attr.for name
                ]
                [ mandatory.label
                , viewIf required <| text "*"
                , case hint of
                    [] ->
                        nothing

                    hints ->
                        span [ Attr.class "fr-hint-text" ] hints
                ]
        , iconWrapper <|
            inp
        , case type_ of
            NewPasswordInput ->
                informationMotDePasse

            _ ->
                nothing
        , case type_ of
            PasswordInput ->
                toggleMotDePasse

            NewPasswordInput ->
                toggleMotDePasse

            _ ->
                nothing
        , Html.Extra.viewMaybe
            (p
                [ Attr.id <| name ++ "-desc-valid"
                , Attr.class "fr-valid-text"
                ]
            )
            validMsg
        , Html.Extra.viewMaybe
            (p
                [ Attr.id <| name ++ "-desc-error"
                , Attr.class "fr-error-text"
                ]
            )
            errorMsg
        ]
