module DSFR.Tile exposing (horizontal, isExternal, vertical, view, withBadges, withDetails, withNoArrow, withPicto, withAttrs)

import Accessibility exposing (Html, div, h3, img, p, text)
import DSFR.Typography
import Html exposing (map)
import Html.Attributes
import Html.Extra exposing (viewIf, viewMaybe)


type TileConfig
    = TileConfig MandatoryTileConfig OptionalTileConfig


type alias MandatoryTileConfig =
    { title : Html Never
    , link : Maybe String
    , description : Maybe (Html Never)
    }


type alias OptionalTileConfig =
    { orientation : Orientation
    , external : Bool
    , picto : Maybe String
    , badges : List String
    , arrow : Bool
    , details : Maybe String
    , attrs : List (Html.Attribute Never)
    }


defaultOptionalTileConfig : OptionalTileConfig
defaultOptionalTileConfig =
    { orientation = Vertical
    , external = False
    , picto = Nothing
    , badges = []
    , arrow = True
    , details = Nothing
    , attrs = []
    }


type Orientation
    = Vertical
    | Horizontal


vertical : { title : Html Never, link : Maybe String, description : Maybe (Html Never) } -> TileConfig
vertical mandatory =
    TileConfig mandatory { defaultOptionalTileConfig | orientation = Vertical }


horizontal : { title : Html Never, link : Maybe String, description : Maybe (Html Never) } -> TileConfig
horizontal mandatory =
    TileConfig mandatory { defaultOptionalTileConfig | orientation = Horizontal }


isExternal : TileConfig -> TileConfig
isExternal (TileConfig mandatory optional) =
    TileConfig mandatory { optional | external = True }


withPicto : String -> TileConfig -> TileConfig
withPicto picto (TileConfig mandatory optional) =
    TileConfig mandatory { optional | picto = Just picto }


withBadges : List String -> TileConfig -> TileConfig
withBadges badges (TileConfig mandatory optional) =
    TileConfig mandatory { optional | badges = badges }


withNoArrow : TileConfig -> TileConfig
withNoArrow (TileConfig mandatory optional) =
    TileConfig mandatory { optional | arrow = False }


withDetails : String -> TileConfig -> TileConfig
withDetails details (TileConfig mandatory optional) =
    TileConfig mandatory { optional | details = Just details }


withAttrs : List (Html.Attribute Never) -> TileConfig -> TileConfig
withAttrs attrs (TileConfig mandatory optional) =
    TileConfig mandatory { optional | attrs = attrs }


view : TileConfig -> Html msg
view (TileConfig { title, description, link } { orientation, external, picto, badges, arrow, details, attrs }) =
    map never <|
        div
            (Html.Attributes.class "fr-tile"
                :: Html.Attributes.classList
                    [ ( "fr-enlarge-link", link /= Nothing && arrow )
                    , ( "fr-tile--horizontal", orientation == Horizontal )
                    ]
                :: attrs
            )
            [ div
                [ Html.Attributes.class "fr-tile__body"
                ]
                [ div [ Html.Attributes.class "fr-tile__content" ]
                    [ h3
                        [ Html.Attributes.class "fr-tile__title !mb-0"
                        ]
                        [ case link of
                            Just l ->
                                (if external then
                                    DSFR.Typography.externalLink

                                 else
                                    DSFR.Typography.link
                                )
                                    l
                                    [ Html.Attributes.class "fr-tile__link"
                                    ]
                                    [ title ]

                            Nothing ->
                                title
                        ]
                    , viewMaybe
                        (\desc ->
                            p
                                [ Html.Attributes.class "fr-tile__desc"
                                ]
                                [ desc ]
                        )
                        description
                    , viewMaybe
                        (\d ->
                            p
                                [ Html.Attributes.class "fr-tile__detail"
                                ]
                                [ text d ]
                        )
                        details
                    , viewIf (List.length badges > 0) <|
                        div [ Html.Attributes.class "fr-tile__start !mb-0" ] <|
                            List.map
                                (\b ->
                                    p [ Html.Attributes.class "fr-tag" ]
                                        [ text b ]
                                )
                            <|
                                badges
                    ]
                ]
            , viewMaybe
                (\svgIcon ->
                    div
                        [ Html.Attributes.class "fr-tile__header"
                        ]
                        [ div [ Html.Attributes.class "fr-tile__pictogram" ]
                            [ img "" [ Html.Attributes.src <| "/assets/img/artwork/pictograms/" ++ svgIcon ]
                            ]
                        ]
                )
                picto
            ]
