module DSFR.Tabs exposing (new, view)

import Accessibility exposing (Html, button, div, li, text, ul)
import Accessibility.Aria
import DSFR.Icons
import Html.Attributes
import Html.Attributes.Extra exposing (role)
import Html.Events
import Html.Lazy


type alias TabContent msg =
    { id : String
    , title : String
    , icon : DSFR.Icons.IconName
    , content : Html msg
    }


type TabsConfig msg
    = TabsConfig (String -> msg) (List (TabContent msg))


new :
    { changeTabMsg : String -> msg
    , tabs :
        List
            { id : String
            , title : String
            , icon : DSFR.Icons.IconName
            , content : Html msg
            }
    }
    -> TabsConfig msg
new config =
    config
        |> .tabs
        |> List.map (\{ id, title, icon, content } -> TabContent id title icon content)
        |> TabsConfig config.changeTabMsg


view : String -> TabsConfig msg -> Html msg
view selectedId (TabsConfig changeTabMsg list) =
    let
        tabsLabel =
            Nothing

        ( titles, contents ) =
            list
                |> List.foldl
                    (\{ id, title, icon, content } ( currentTitles, currentContents, index ) ->
                        ( { id = id, icon = icon, title = title, index = index } :: currentTitles, { id = id, content = content, index = index } :: currentContents, index + 1 )
                    )
                    ( [], [], 0 )
                |> (\( t, c, _ ) -> ( List.reverse t, List.reverse c ))
    in
    div
        [ Html.Attributes.class "fr-tabs fr-transition-none h-full"
        ]
        (ul
            [ Html.Attributes.class "fr-tabs__list fr-tabs__shadow "
            , role "tablist"
            , Accessibility.Aria.label <| Maybe.withDefault "Sélection des onglets" <| tabsLabel
            ]
            (List.map
                (\title ->
                    li
                        [ role "presentation"
                        ]
                        [ toTabButton selectedId changeTabMsg title ]
                )
             <|
                titles
            )
            :: (List.map (Html.Lazy.lazy2 toTabContent selectedId) <|
                    contents
               )
        )


toTabButton :
    String
    -> (String -> msg)
    ->
        { id : String
        , title : String
        , icon : DSFR.Icons.IconName
        , index : Int
        }
    -> Html msg
toTabButton selectedId changeTabMsg { id, title, icon } =
    button
        [ Html.Attributes.id <| "tab-button-" ++ id
        , Html.Attributes.class "fr-tabs__tab fr-tabs__tab--icon-left"
        , DSFR.Icons.toClass icon
        , Accessibility.Aria.selected (id == selectedId)
        , Accessibility.Aria.controls [ "tabpanel-panel--" ++ id ]
        , role "tab"
        , Html.Events.onClick <| changeTabMsg id
        ]
        [ text title ]


toTabContent :
    String
    ->
        { id : String
        , content : Html msg
        , index : Int
        }
    -> Html msg
toTabContent selectedId { id, content } =
    div
        [ Html.Attributes.id <| "tabpanel-panel--" ++ id
        , Html.Attributes.class "fr-tabs__panel !p-0"
        , Html.Attributes.style "transition" "none !important"
        , Html.Attributes.classList
            [ ( "fr-tabs__panel--selected", selectedId == id )
            , ( "hidden", selectedId /= id )
            ]
        , Accessibility.Aria.labelledBy <| "tabpanel-panel--" ++ id
        , Accessibility.Aria.selected <| selectedId == id
        , role "tabpanel"
        , Html.Attributes.tabindex <|
            if selectedId == id then
                0

            else
                -1
        ]
        [ content
        ]
