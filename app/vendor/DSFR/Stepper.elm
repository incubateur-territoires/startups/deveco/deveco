module DSFR.Stepper exposing (raw)

import Accessibility exposing (Html, div, h2, p, span, text)
import Html.Attributes exposing (attribute, class)
import Html.Extra exposing (nothing)


raw :
    { stepTotal : Int
    , titreCurrent : String
    , stepCurrent : Int
    , titreNext : Maybe String
    }
    -> Html msg
raw { stepTotal, titreCurrent, stepCurrent, titreNext } =
    div
        [ class "fr-stepper"
        ]
        [ h2
            [ class "fr-stepper__title"
            ]
            [ text titreCurrent
            , span
                [ class "fr-stepper__state"
                ]
                [ text "Étape"
                , text " "
                , text <| String.fromInt stepCurrent
                , text " "
                , text "sur"
                , text " "
                , text <| String.fromInt stepTotal
                ]
            ]
        , div
            [ class "fr-stepper__steps"
            , attribute "data-fr-current-step" <| String.fromInt stepCurrent
            , attribute "data-fr-steps" <| String.fromInt stepTotal
            ]
            []
        , case titreNext of
            Nothing ->
                nothing

            Just tn ->
                p
                    [ class "fr-stepper__details"
                    ]
                    [ span
                        [ class "fr-text--bold"
                        ]
                        [ text "Étape suivante : " ]
                    , text tn
                    ]
        ]
