module DSFR.Toggle exposing (GroupConfig, MandatoryConfig, MandatoryGroupConfig, OptionalConfig, OptionalGroupConfig, ToggleConfig, defaultOptionalConfig, defaultOptionalGroupConfig, group, groupWithDisabled, groupWithExtraAttrs, groupWithHint, groupWithLabelLeft, groupWithNoSeparator, groupWithNoState, groupWithToDisabled, groupWithToHint, single, singleWithDisabled, singleWithHint, singleWithLabelLeft, singleWithNoState, viewGroup, viewSingle)

import Accessibility exposing (Attribute, Html, checkbox, div, li, p, text, ul)
import Html.Attributes
import Html.Attributes.Extra exposing (empty)
import Html.Events as Events
import Html.Extra exposing (viewMaybe)


type alias ToggleConfig msg data =
    ( MandatoryConfig msg data
    , OptionalConfig
    )


type alias MandatoryConfig msg data =
    { value : data
    , checked : Maybe Bool
    , valueAsString : data -> String
    , id : String
    , label : Html msg
    , onChecked : data -> Bool -> msg
    }


type alias OptionalConfig =
    { hint : Maybe String
    , disabled : Bool
    , labelLeft : Bool
    , hideState : Bool
    , separator : Bool
    }


defaultOptionalConfig : OptionalConfig
defaultOptionalConfig =
    { hint = Nothing
    , disabled = False
    , labelLeft = False
    , hideState = False
    , separator = False
    }


viewSingle : ToggleConfig msg data -> Html msg
viewSingle ( { value, checked, valueAsString, id, label, onChecked }, { hint, disabled, labelLeft, hideState, separator } ) =
    div
        [ Html.Attributes.class "fr-toggle"
        , Html.Attributes.classList
            [ ( "fr-toggle--label-left", labelLeft )
            , ( "fr-toggle--border-bottom", separator )
            ]
        ]
        [ checkbox (valueAsString value)
            checked
            [ Html.Attributes.class "fr-toggle__input"
            , Html.Attributes.id id
            , Html.Attributes.name id
            , Html.Attributes.disabled disabled
            , Events.onCheck <| onChecked value
            ]
        , Accessibility.label
            [ Html.Attributes.class "fr-toggle__label"
            , Html.Attributes.for id
            , if hideState then
                empty

              else
                Html.Attributes.attribute "data-fr-checked-label" "Activé"
            , if hideState then
                empty

              else
                Html.Attributes.attribute "data-fr-unchecked-label" "Désactivé"
            ]
            [ label
            ]
        , viewMaybe
            (text >> List.singleton >> p [ Html.Attributes.class "fr-hint-text" ])
            hint
        ]


single : MandatoryConfig msg data -> ( MandatoryConfig msg data, OptionalConfig )
single config =
    Tuple.pair config defaultOptionalConfig


singleWithHint : Maybe String -> ( MandatoryConfig msg data, OptionalConfig ) -> ( MandatoryConfig msg data, OptionalConfig )
singleWithHint hint ( mandatory, optional ) =
    ( mandatory, { optional | hint = hint } )


singleWithDisabled : Bool -> ( MandatoryConfig msg data, OptionalConfig ) -> ( MandatoryConfig msg data, OptionalConfig )
singleWithDisabled disabled ( mandatory, optional ) =
    ( mandatory, { optional | disabled = disabled } )


singleWithLabelLeft : ( MandatoryConfig msg data, OptionalConfig ) -> ( MandatoryConfig msg data, OptionalConfig )
singleWithLabelLeft ( mandatory, optional ) =
    ( mandatory, { optional | labelLeft = True } )


singleWithSeparator : ( MandatoryConfig msg data, OptionalConfig ) -> ( MandatoryConfig msg data, OptionalConfig )
singleWithSeparator ( mandatory, optional ) =
    ( mandatory, { optional | separator = True } )


singleWithNoState : ( MandatoryConfig msg data, OptionalConfig ) -> ( MandatoryConfig msg data, OptionalConfig )
singleWithNoState ( mandatory, optional ) =
    ( mandatory, { optional | hideState = True } )


type alias GroupConfig msg data =
    ( MandatoryGroupConfig msg data
    , OptionalGroupConfig data
    )


type alias MandatoryGroupConfig msg data =
    { id : String
    , onChecked : data -> Bool -> msg
    , values : List data
    , valueAsString : data -> String
    , toChecked : data -> Bool
    , toId : data -> String
    , toLabel : data -> Html msg
    }


type alias OptionalGroupConfig data =
    { hint : Maybe String
    , disabled : Bool
    , labelLeft : Bool
    , hideState : Bool
    , toHint : data -> Maybe String
    , toDisabled : data -> Bool
    , extraAttrs : List (Attribute Never)
    , separator : Bool
    }


defaultOptionalGroupConfig : OptionalGroupConfig data
defaultOptionalGroupConfig =
    { hint = Nothing
    , disabled = False
    , labelLeft = False
    , hideState = False
    , toHint = always Nothing
    , toDisabled = always False
    , extraAttrs = []
    , separator = True
    }


group : MandatoryGroupConfig msg data -> GroupConfig msg data
group config =
    Tuple.pair config defaultOptionalGroupConfig


viewGroup : GroupConfig msg data -> Html msg
viewGroup ( { id, onChecked, values, toChecked, valueAsString, toId, toLabel }, { hint, disabled, labelLeft, hideState, toHint, toDisabled, extraAttrs, separator } ) =
    ul [ Html.Attributes.class "fr-toggle__list" ] <|
        List.map (li [] << List.singleton) <|
            List.map
                (\v ->
                    single
                        { value = v
                        , checked = Just <| toChecked v
                        , valueAsString = valueAsString
                        , id = id ++ "-option-" ++ toId v
                        , label = toLabel v
                        , onChecked = onChecked
                        }
                        |> singleWithHint (toHint v)
                        |> singleWithDisabled (toDisabled v)
                        |> (if separator then
                                singleWithLabelLeft

                            else
                                identity
                           )
                        |> (if separator then
                                singleWithSeparator

                            else
                                identity
                           )
                        |> (if hideState then
                                singleWithNoState

                            else
                                identity
                           )
                        |> viewSingle
                )
            <|
                values


groupWithHint : Maybe String -> ( MandatoryGroupConfig msg data, OptionalGroupConfig data ) -> ( MandatoryGroupConfig msg data, OptionalGroupConfig data )
groupWithHint hint ( mandatory, optional ) =
    ( mandatory, { optional | hint = hint } )


groupWithDisabled : Bool -> ( MandatoryGroupConfig msg data, OptionalGroupConfig data ) -> ( MandatoryGroupConfig msg data, OptionalGroupConfig data )
groupWithDisabled disabled ( mandatory, optional ) =
    ( mandatory, { optional | disabled = disabled } )


groupWithLabelLeft : ( MandatoryGroupConfig msg data, OptionalGroupConfig data ) -> ( MandatoryGroupConfig msg data, OptionalGroupConfig data )
groupWithLabelLeft ( mandatory, optional ) =
    ( mandatory, { optional | labelLeft = True } )


groupWithNoSeparator : ( MandatoryGroupConfig msg data, OptionalGroupConfig data ) -> ( MandatoryGroupConfig msg data, OptionalGroupConfig data )
groupWithNoSeparator ( mandatory, optional ) =
    ( mandatory, { optional | separator = False } )


groupWithToHint : (data -> Maybe String) -> ( MandatoryGroupConfig msg data, OptionalGroupConfig data ) -> ( MandatoryGroupConfig msg data, OptionalGroupConfig data )
groupWithToHint toHint ( mandatory, optional ) =
    ( mandatory, { optional | toHint = toHint } )


groupWithToDisabled : (data -> Bool) -> ( MandatoryGroupConfig msg data, OptionalGroupConfig data ) -> ( MandatoryGroupConfig msg data, OptionalGroupConfig data )
groupWithToDisabled toDisabled ( mandatory, optional ) =
    ( mandatory, { optional | toDisabled = toDisabled } )


groupWithExtraAttrs : List (Attribute Never) -> ( MandatoryGroupConfig msg data, OptionalGroupConfig data ) -> ( MandatoryGroupConfig msg data, OptionalGroupConfig data )
groupWithExtraAttrs extraAttrs ( mandatory, optional ) =
    ( mandatory, { optional | extraAttrs = extraAttrs } )


groupWithNoState : ( MandatoryGroupConfig msg data, OptionalGroupConfig data ) -> ( MandatoryGroupConfig msg data, OptionalGroupConfig data )
groupWithNoState ( mandatory, optional ) =
    ( mandatory, { optional | hideState = True } )
