module DSFR.Notice exposing (notice)

import Accessibility exposing (Html, div, p)
import Html.Attributes


notice : Html msg -> Html msg
notice content =
    div
        [ Html.Attributes.class "fr-notice fr-notice--info"
        ]
        [ div
            [ Html.Attributes.class "fr-container"
            ]
            [ div
                [ Html.Attributes.class "fr-notice__body"
                ]
                [ p
                    [ Html.Attributes.class "fr-notice__title"
                    ]
                    [ content ]
                ]
            ]
        ]
