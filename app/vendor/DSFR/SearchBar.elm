module DSFR.SearchBar exposing (searchBar, searchBarLG)

import Accessibility exposing (Attribute, Html, div, formWithListeners, inputText, label, p, span, text)
import Accessibility.Landmark
import DSFR.Button
import Html.Attributes as Attr exposing (class)
import Html.Attributes.Extra exposing (attributeMaybe, empty)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Json.Decode as Decode
import DSFR.Icons.System exposing (addLine)


type Size
    = MD
    | LG


type alias MandatoryConfig msg =
    { submitMsg : msg
    , inputMsg : String -> msg
    , buttonLabel : String
    , inputLabel : String
    , inputPlaceholder : Maybe String
    , inputId : String
    , inputValue : String
    , hints : List (Html msg)
    , fullLabel : Maybe (Html msg)
    }


type alias AdditionalConfig msg =
    { errors : List String
    , extraAttrs : List (Attribute msg)
    , disabled : Bool
    }


genericSearchBar : Size -> AdditionalConfig msg -> MandatoryConfig msg -> Html msg
genericSearchBar size additionalConfig { submitMsg, inputMsg, buttonLabel, inputLabel, inputPlaceholder, inputId, inputValue, hints, fullLabel } =
    let
        sizeAttr =
            case size of
                MD ->
                    empty

                LG ->
                    class "fr-search-bar--lg"

        hint =
            case hints of
                [] ->
                    nothing

                _ ->
                    span [ Attr.class "fr-hint-text" ] hints

        ( error, errorText ) =
            case additionalConfig.errors of
                [] ->
                    ( False, "" )

                list ->
                    ( True, String.join ", " list )
    in
    div [ class "flex flex-col gap-2" ]
        [ case fullLabel of
            Just l ->
                label
                    [ Attr.class "fr-label"
                    , Attr.for inputId
                    ]
                    [ l, hint ]

            Nothing ->
                hint
        , formWithListeners
            [ class "fr-search-bar"
            , Attr.id <| "header-" ++ inputId
            , Accessibility.Landmark.search
            , sizeAttr
            , Events.onSubmit submitMsg
            ]
            [ case fullLabel of
                Nothing ->
                    label
                        [ class "fr-label"
                        , Attr.for inputId
                        ]
                        [ text inputLabel
                        ]

                Just _ ->
                    nothing
            , inputText inputValue
                ([ class "fr-input"
                 , Attr.type_ "search"
                 , Attr.id inputId
                 , Attr.name inputId
                 , inputPlaceholder
                    |> attributeMaybe Attr.placeholder
                 , Events.onInput inputMsg
                 , Events.on "search" <|
                    Decode.succeed submitMsg
                 , Attr.classList [ ( "fr-input--error", error ) ]
                 ]
                    ++ additionalConfig.extraAttrs
                )
            , DSFR.Button.new { onClick = Nothing, label = buttonLabel }
                |> DSFR.Button.withDisabled additionalConfig.disabled
                |> DSFR.Button.submit
                |> DSFR.Button.withAttrs [ Attr.title buttonLabel ]
                |> DSFR.Button.view
            ]
        , Html.Extra.viewIf error <|
            p [ class "fr-error-text" ] [ text errorText ]
        ]


searchBar : AdditionalConfig msg -> MandatoryConfig msg -> Html msg
searchBar extraAttrs =
    genericSearchBar MD extraAttrs


searchBarLG : AdditionalConfig msg -> MandatoryConfig msg -> Html msg
searchBarLG extraAttrs =
    genericSearchBar LG extraAttrs
