module DSFR.Range exposing (double, new, simple, view, withShowSteps, withSmall, withDescription)

import Accessibility exposing (div, span, text)
import Html exposing (input)
import Html.Attributes as Attr
import Html.Events as Events
import Html.Extra exposing (viewMaybe)
import Accessibility exposing (Html)


type RangeConfig msg
    = RangeConfig ( MandatoryRangeConfig msg) OptionalRangeConfig (RC msg)


type RC msg
    = RangeSimpleConfig
        { value : Float
        , onChange : Float -> msg
        }
    | RangeDoubleConfig
        { valueMin : Float
        , valueMax : Float
        , onChangeMin : Float -> msg
        , onChangeMax : Float -> msg
        }


type alias MandatoryRangeConfig msg =
    { id : String
    , label : Html msg
    , min : Float
    , max : Float
    , step : Float
    }


type alias OptionalRangeConfig =
    { description : Maybe String
    , small : Bool
    , showSteps : Bool
    }


defaultOptionalConfig : OptionalRangeConfig
defaultOptionalConfig =
    { description = Nothing
    , small = False
    , showSteps = False
    }


new :
    { id : String
    , label : Html msg
    , min : Float
    , max : Float
    , step : Float
    }
    -> (RC msg -> RangeConfig msg)
new mandatory =
    RangeConfig mandatory defaultOptionalConfig


simple :
    { value : Float
    , onChange : Float -> msg
    }
    -> (RC msg -> RangeConfig msg)
    -> RangeConfig msg
simple simpleConfig rangeConfig =
    rangeConfig <| RangeSimpleConfig simpleConfig


double :
    { valueMin : Float
    , valueMax : Float
    , onChangeMin : Float -> msg
    , onChangeMax : Float -> msg
    }
    -> (RC msg -> RangeConfig msg)
    -> RangeConfig msg
double doubleConfig rangeConfig =
    rangeConfig <| RangeDoubleConfig doubleConfig


withDescription : String -> RangeConfig msg -> RangeConfig msg
withDescription description (RangeConfig mandatory optional rc) =
    RangeConfig mandatory { optional | description = Just description } rc


withSmall : RangeConfig msg -> RangeConfig msg
withSmall (RangeConfig mandatory optional rc) =
    RangeConfig mandatory { optional | small = True } rc


withShowSteps : RangeConfig msg -> RangeConfig msg
withShowSteps (RangeConfig mandatory optional rc) =
    RangeConfig mandatory { optional | showSteps = True } rc


view : RangeConfig msg -> Accessibility.Html msg
view (RangeConfig { id, label, min, max, step } { description, small, showSteps } rc) =
    let
        ( inputs, valueDisplay, rangeDouble ) =
            case rc of
                RangeSimpleConfig { value, onChange } ->
                    ( [ input
                            [ Attr.id id
                            , Attr.name id
                            , Attr.type_ "range"
                            , Attr.attribute "aria-labelledby" <| id ++ "-label"
                            , Attr.min <| String.fromFloat min
                            , Attr.max <| String.fromFloat max
                            , Attr.value <| String.fromFloat value
                            , Attr.step <| String.fromFloat step
                            , Attr.attribute "aria-describedby" <| id ++ "-messages"
                            , Events.onInput <| (String.toFloat >> Maybe.withDefault value >> onChange)
                            ]
                            []
                      ]
                    , String.fromFloat value
                    , False
                    )

                RangeDoubleConfig { valueMin, valueMax, onChangeMin, onChangeMax } ->
                    let
                        inputId =
                            id

                        input2Id =
                            id ++ "-2"
                    in
                    ( [ input
                            [ Attr.id inputId
                            , Attr.name inputId
                            , Attr.attribute "aria-labelledby" <| inputId ++ "-label"
                            , Attr.attribute "aria-describedby" <| inputId ++ "-messages"
                            , Attr.type_ "range"
                            , Attr.min <| String.fromFloat min
                            , Attr.max <| String.fromFloat max
                            , Attr.value <| String.fromFloat valueMin
                            , Attr.step <| String.fromFloat step
                            , Events.onInput <| (String.toFloat >> Maybe.withDefault valueMin >> onChangeMin)
                            ]
                            []
                      , input
                            [ Attr.id <| input2Id
                            , Attr.name <| input2Id
                            , Attr.attribute "aria-labelledby" <| input2Id ++ "-label"
                            , Attr.attribute "aria-describedby" <| input2Id ++ "-messages"
                            , Attr.type_ "range"
                            , Attr.min <| String.fromFloat min
                            , Attr.max <| String.fromFloat max
                            , Attr.value <| String.fromFloat valueMax
                            , Attr.step <| String.fromFloat step
                            , Events.onInput <| (String.toFloat >> Maybe.withDefault valueMax >> onChangeMax)
                            ]
                            []
                      ]
                    , String.fromFloat valueMin ++ " - " ++ String.fromFloat valueMax
                    , True
                    )
    in
    div
        [ Attr.class "fr-range-group"
        , Attr.id <| id ++ "-group"
        ]
        [ Accessibility.label
            [ Attr.class "fr-label"
            ]
            [ label
            , viewMaybe
                (\desc ->
                    span
                        [ Attr.class "fr-hint-text"
                        ]
                        [ text desc ]
                )
                description
            ]
        , div
            [ Attr.class "fr-range"
            , Attr.classList
                [ ( "fr-range--sm", small )
                , ( "fr-range--step", showSteps )
                , ( "fr-range--double", rangeDouble )
                ]
            ]
            (span
                [ Attr.class "fr-range__output"
                ]
                [ text <| valueDisplay ]
                :: inputs
                ++ [ span
                        [ Attr.class "fr-range__min"
                        , Attr.attribute "aria-hidden" "true"
                        ]
                        [ text <| String.fromFloat min ]
                   , span
                        [ Attr.class "fr-range__max"
                        , Attr.attribute "aria-hidden" "true"
                        ]
                        [ text <| String.fromFloat max ]
                   ]
            )
        , div
            [ Attr.class "fr-messages-group"
            , Attr.id <| id ++ "-messages"
            , Attr.attribute "aria-live" "polite"
            ]
            []
        ]
