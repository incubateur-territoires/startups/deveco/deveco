module UnitTests.String exposing (suite)

import Expect
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "String"
        [ describe "A test"
            [ test "works"
                (\_ ->
                    "Boom"
                        |> Expect.equal "Boom"
                )
            ]
        ]
