# Dévéco

## Dépendances

Ce projet nécessite [la dernière version LTS de NodeJS](https://nodejs.org/fr/download/) et le [gestionnaire de paquets Yarn](https://yarnpkg.com/getting-started).

## Commandes

### Lancement de l'application et du serveur de test

```shell
yarn start
```

Si tout s'est bien passé, l'application est consultable sur [http://localhost:3000/](http://localhost:3000/).

### Installation des dépendances

```shell
# installe les dépendances déclarées dans package.json
yarn install
```

### Lancement en mode dev

```shell
# démarre l'application et le serveur de test en même temps
yarn start:dev
```

### Lancement de l'explorateur d'UI

```shell
# lance un serveur qui permet d'explorer les différents composants UI du projet
yarn explorer
```

Si tout s'est bien passé, l'application est consultable sur [http://localhost:4000/](http://localhost:4000/).

### Lancement de la correction automatique

```shell
# signale des problèmes de formatage (via elm-format)
# ou de bonnes pratiques (via elm-review)
# et les résoud si possible
yarn fix
```

### Lancement des tests unitaires

```shell
# lance les tests et les relance à chaque modification de code
yarn test
# lance les tests une seule fois
yarn test:once
# alias de yarn test
yarn test:watch
```

### Lancement des tests e2e

```shell
# lance les tests avec un navigateur "headless" (dans la console)
yarn e2e
# lance les tests avec une interface graphique
yarn e2e:ui
# alias de yarn e2e
yarn e2e:headless
```

### Création de l'application de production

```shell
# crée une application optimisée pour le déploiement
yarn build
```

### Lancement du lint

```shell
# signale des problèmes de formatage (via elm-format)
# ou de revue de code (via elm-review)
# sans les résoudre automatiquement
yarn lint
```

### Lancement du formatage

```shell
# lance seulement elm-format sur le code Elm
yarn format
```

### Lancement de la revue de code

```shell
# lance la revue de code et la relance à chaque modification de code
yarn review
# lance la revue de code une seule fois
yarn review:once
# alias de yarn review
yarn review:watch
```

## Outils utilisés

- L'application est codée en Elm.
- Elle utilise le framework orus-io/elm-spa.
- Vite est utilisé comme bundler.
- Le style est assuré par le DSFR.
- Le layout est obtenu grâce aux classes fournies par Tailwind.

## Outils à mettre en œuvre

- [x] elm-spa : [https://github.com/orus-io/elm-spa](https://github.com/orus-io/elm-spa)
- [x] tests e2e en BDD : [CodeceptJS](https://codecept.io/quickstart/)
- [x] elm-test-rs : [https://github.com/mpizenberg/elm-test-rs](https://github.com/mpizenberg/elm-test-rs)
- [x] elm-review : [https://github.com/jfmengels/elm-review/](https://github.com/jfmengels/elm-review/)
- [x] elm-review shortlist : [https://gist.github.com/JesterXL/c69c192c756f16ff8ab7d38419176e0f](https://gist.github.com/JesterXL/c69c192c756f16ff8ab7d38419176e0f)
- [x] elm-optimize-level2 : [https://github.com/mdgriffith/elm-optimize-level-2](https://github.com/mdgriffith/elm-optimize-level-2)
- [x] minification : [https://guide.elm-lang.org/optimization/asset_size.html#instructions](https://guide.elm-lang.org/optimization/asset_size.html#instructions)
- [x] elm-book : [https://github.com/dtwrks/elm-book](https://github.com/dtwrks/elm-book)
- [x] elm/accessible-html-with-css : [https://github.com/tesk9/accessible-html-with-css/](https://github.com/tesk9/accessible-html-with-css/)
- [ ] R10 ? [https://package.elm-lang.org/packages/rakutentech/r10/latest/](https://package.elm-lang.org/packages/rakutentech/r10/latest/)
- [ ] Http Trinity ? [https://rakutentech.github.io/http-trinity/](https://rakutentech.github.io/http-trinity/)
- [x] Modélisation des entités en Markdown via Mermaid
- [x] passer à tailwind 3 : [https://tailwindcss.com/](https://tailwindcss.com/)
- [ ] étudier la mise en place de Sentry : [https://github.com/MTES-MCT/wikicarbone/commit/12cc8018df3603c0d426bdf4fad59fbb611d4c7d](https://github.com/MTES-MCT/wikicarbone/commit/12cc8018df3603c0d426bdf4fad59fbb611d4c7d)

## Ressources Elm

- Site officiel : [https://guide.elm-lang.org/](https://guide.elm-lang.org/)
- Elm Patterns : [https://sporto.github.io/elm-patterns/](https://sporto.github.io/elm-patterns/)
- Elm Program Test : [https://elm-program-test.netlify.app/](https://elm-program-test.netlify.app/)
- HTML-to-Elm : [https://html-to-elm.com/](https://html-to-elm.com/)
- json2elm : [https://korban.net/elm/json2elm/](https://korban.net/elm/json2elm/)
- elm-spa : [https://www.elm-spa.dev/](https://www.elm-spa.dev/)
- elm-css patterns : [https://elmcsspatterns.io/layout/sidebar](https://elmcsspatterns.io/layout/sidebar)
- Elm Cookbook : [https://orasund.gitbook.io/elm-cookbook/](https://orasund.gitbook.io/elm-cookbook/)
- Elm tests : [https://github.com/elm-explorations/test/](https://github.com/elm-explorations/test/)
- Elm @ Rakuten : [https://engineering.rakuten.today/post/elm-at-rakuten/](https://engineering.rakuten.today/post/elm-at-rakuten/)
- Codecept : [https://codecept.io/basics/](https://codecept.io/basics/)
- Elm-Book : [https://elm-book-in-elm-book.netlify.app/overview](https://elm-book-in-elm-book.netlify.app/overview)

## Modèle de données

```mermaid
erDiagram
  event_log {
    integer id PK
    timestamp date
    varchar entity_type
    integer entity_id
    varchar action
    integer deveco_id FK
  }
  search_request {
    integer id PK
    varchar query
    varchar type
    timestamp created_at
    integer deveco_id FK
  }
  petr {
    varchar code_petr PK
    varchar lib_petr
  }
  epci {
    varchar insee_epci PK
    varchar lib_epci
    varchar nature
    varchar code_petr FK
  }
  metropole {
    varchar insee_com PK
    varchar lib_com
  }
  departement {
    varchar ctcd PK
    varchar nom
  }
  region {
    varchar insee_reg PK
    varchar nom
  }
  commune {
    varchar typecom
    varchar insee_com PK
    varchar insee_dep
    varchar insee_arr
    varchar tncc
    varchar ncc
    varchar nccenr
    varchar lib_com
    varchar insee_can
    varchar zrr
    varchar insee_reg FK
    varchar ctcd FK
    varchar com_parent FK
    varchar insee_epci FK
  }
  territory {
    integer id PK
    varchar territory_type
    varchar name
    boolean import_in_progress
    timestamp created_at
    timestamp updated_at
    varchar epci_id FK
    varchar metropole_id FK
    varchar petr_id FK
    varchar departement_id FK
    varchar region_id FK
    varchar commune_id FK
  }
  deveco {
    integer id PK
    timestamp created_at
    timestamp updated_at
    boolean welcome_email_sent
    integer account_id FK
    integer territory_id FK
  }
  account {
    integer id PK
    varchar email
    varchar nom
    varchar prenom
    varchar account_type
    varchar access_key
    timestamptz access_key_date
    timestamptz last_login
    timestamptz last_auth
    varchar new_features
    boolean enabled
  }
  demande {
    integer id PK
    varchar type_demande
    boolean cloture
    varchar motif
    date date_cloture
    timestamp created_at
    timestamp updated_at
    integer fiche_id FK
  }
  qp_metropoleoutremer_wgs84_epsg4326 {
    integer gid PK
    varchar code_qp
    varchar nom_qp
    varchar commune_qp
    geometry geom
  }
  particulier {
    integer id PK
    varchar nom
    varchar prenom
    varchar email
    varchar telephone
    varchar adresse
    varchar code_postal
    varchar ville
    varchar geolocation
    varchar description
    timestamp created_at
    integer qpv_id FK
  }
  contact {
    integer id PK
    varchar fonction
    varchar source
    timestamp created_at
    integer entite_id FK
    integer particulier_id FK
  }
  etablissement {
    varchar siren
    varchar siret PK
    varchar nom_public
    varchar nom_recherche
    varchar enseigne
    varchar adresse_complete
    varchar code_commune
    double longitude
    double latitude
    varchar tranche_effectifs
    varchar annee_effectifs
    varchar sigle
    varchar code_naf
    varchar libelle_naf
    varchar libelle_categorie_naf
    date date_creation
    date date_creation_entreprise
    date date_fermeture
    varchar etat_administratif
    varchar categorie_juridique
    varchar type_etablissement
    boolean siege_social
  }
  exercice {
    integer id PK
    bigint ca
    date date_cloture
    varchar entreprise_id FK
  }
  entreprise {
    varchar siret PK
    double longitude
    double latitude
    date date_creation
    date date_fermeture
    varchar nom
    varchar enseigne
    varchar code_naf
    varchar libelle_naf
    varchar libelle_categorie_naf
    varchar adresse
    varchar code_commune
    varchar etat_administratif
    varchar forme_juridique
    varchar effectifs
    date date_effectifs
    jsonb mandataires_sociaux
    timestamptz last_api_update
    boolean siege_social
  }
  local {
    integer id PK
    varchar titre
    varchar adresse
    varchar ville
    varchar code_postal
    varchar geolocation
    text local_types
    varchar local_statut
    varchar surface
    varchar loyer
    text localisations
    varchar commentaire
    timestamptz date_modification
    timestamp created_at
    timestamp updated_at
    integer territoire_id FK
    integer auteur_modification_id FK
    integer deveco_id FK
  }
  proprietaire {
    integer id PK
    timestamp created_at
    timestamp updated_at
    integer local_id FK
    integer entite_id FK
    integer auteur_modification_id FK
    integer deveco_id FK
  }
  entite {
    integer id PK
    varchar entite_type
    text activites_reelles
    text entreprise_localisations
    text mots_cles
    varchar activite_autre
    timestamp created_at
    timestamp updated_at
    varchar future_enseigne
    boolean exogene
    integer territoire_id FK
    integer deveco_id FK
    integer auteur_modification_id FK
    varchar entreprise_id FK
    integer particulier_id FK
    varchar enseigne_id FK
    integer createur_id FK
  }
  echange {
    integer id PK
    varchar titre
    varchar type_echange
    date date_echange
    varchar compte_rendu
    text themes
    timestamp created_at
    timestamp updated_at
    integer fiche_id FK
    integer createur_id FK
    integer auteur_modification_id FK
  }
  rappel {
    integer id PK
    date date_rappel
    varchar titre
    date date_cloture
    timestamp created_at
    integer fiche_id FK
    integer createur_id FK
    integer auteur_cloture_id FK
  }
  fiche {
    integer id PK
    timestamptz date_modification
    timestamp created_at
    timestamp updated_at
    integer entite_id FK
    integer territoire_id FK
    integer auteur_modification_id FK
    integer deveco_id FK
  }
  activite_entreprise {
    integer id PK
    varchar activite
    integer territory_id FK
  }
  localisation_entreprise {
    integer id PK
    varchar localisation
    integer territory_id FK
  }
  mot_cle {
    integer id PK
    varchar mot_cle
    integer territory_id FK
  }
  zonage {
    integer id PK
    varchar nom
    geometry geometry
    geometry geometry_source
    integer territoire_id FK
  }
  territory_etablissement_reference {
    varchar id PK
    varchar siret
    varchar nom_recherche
    varchar etat_administratif
    varchar libelle_naf
    varchar code_naf
    boolean categorie_juridique_courante
    varchar tranche_effectifs
    varchar code_commune
    tsvector tsv
    date date_creation
    varchar adresse_complete
    boolean exogene
    varchar zrr
    boolean ess
    integer territory_id FK
    integer qpv_id FK
  }
  sirene_etablissement {
    varchar siren
    varchar nic
    varchar siret PK
    enum statut_diffusion_etablissement
    date date_creation_etablissement
    varchar tranche_effectifs_etablissement
    varchar annee_effectifs_etablissement
    varchar activite_principale_registre_metiers_etablissement
    timestamp date_dernier_traitement_etablissement
    boolean etablissement_siege
    integer nombre_periodes_etablissement
    varchar complement_adresse_etablissement
    varchar numero_voie_etablissement
    varchar indice_repetition_etablissement
    varchar type_voie_etablissement
    varchar libelle_voie_etablissement
    varchar code_postal_etablissement
    varchar libelle_commune_etablissement
    varchar libelle_commune_etranger_etablissement
    varchar distribution_speciale_etablissement
    varchar code_commune_etablissement
    varchar code_cedex_etablissement
    varchar libelle_cedex_etablissement
    varchar code_pays_etranger_etablissement
    varchar libelle_pays_etranger_etablissement
    varchar complement_adresse2_etablissement
    varchar numero_voie2_etablissement
    varchar indice_repetition2_etablissement
    varchar type_voie2_etablissement
    varchar libelle_voie2_etablissement
    varchar code_postal2_etablissement
    varchar libelle_commune2_etablissement
    varchar libelle_commune_etranger2_etablissement
    varchar distribution_speciale2_etablissement
    varchar code_commune2_etablissement
    varchar code_cedex2_etablissement
    varchar libelle_cedex2_etablissement
    varchar code_pays_etranger2_etablissement
    varchar libelle_pays_etranger2_etablissement
    date date_debut
    enum etat_administratif_etablissement
    varchar enseigne1_etablissement
    varchar enseigne2_etablissement
    varchar enseigne3_etablissement
    varchar denomination_usuelle_etablissement
    varchar activite_principale_etablissement
    varchar nomenclature_activite_principale_etablissement
    enum caractere_employeur_etablissement
    double longitude
    double latitude
    double geo_score
    varchar geo_type
    varchar geo_adresse
    varchar geo_id
    varchar geo_ligne
    varchar geo_l4
    varchar geo_l5
    varchar hash
  }
  sirene_unite_legale {
    varchar siren PK
    enum statut_diffusion_unite_legale
    boolean unite_purgee_unite_legale
    date date_creation_unite_legale
    varchar sigle_unite_legale
    enum sexe_unite_legale
    varchar prenom1_unite_legale
    varchar prenom2_unite_legale
    varchar prenom3_unite_legale
    varchar prenom4_unite_legale
    varchar prenom_usuel_unite_legale
    varchar pseudonyme_unite_legale
    varchar identifiant_association_unite_legale
    varchar tranche_effectifs_unite_legale
    integer annee_effectifs_unite_legale
    timestamp date_dernier_traitement_unite_legale
    integer nombre_periodes_unite_legale
    enum categorie_entreprise
    integer annee_categorie_entreprise
    date date_debut
    enum etat_administratif_unite_legale
    varchar nom_unite_legale
    varchar nom_usage_unite_legale
    varchar denomination_unite_legale
    varchar denomination_usuelle1_unite_legale
    varchar denomination_usuelle2_unite_legale
    varchar denomination_usuelle3_unite_legale
    integer categorie_juridique_unite_legale
    varchar activite_principale_unite_legale
    varchar nomenclature_activite_principale_unite_legale
    varchar nic_siege_unite_legale
    enum eco_soc_sol_unite_legale
    enum societe_mission_unite_legale
    varchar caractere_employeur_unite_legale
    varchar hash
  }
  password {
    integer id PK
    varchar password
    timestamp updated_at
    timestamp created_at
    integer account_id FK
  }
  geo_token {
    integer id PK
    varchar token
    boolean active
    integer deveco_id FK
  }
  lien_succession {
    integer id PK
    varchar siret_predecesseur
    varchar siret_successeur
    timestamptz date_succession
    timestamptz date_traitement
    boolean transfert_siege
    boolean continuite_economique
  }
  fiche_archive {
    integer id PK
    jsonb content
    timestamp created_at
  }
  local_archive {
    integer id PK
    jsonb content
    timestamp created_at
  }
  geo_token_request {
    integer id PK
    varchar url
    varchar user_agent
    timestamp created_at
    integer geo_token_id FK
  }
  lien_succession_temp {
    integer id PK
    varchar siret_predecesseur
    varchar siret_successeur
    timestamptz date_succession
    timestamptz date_traitement
    boolean transfert_siege
    boolean continuite_economique
  }
  connexion {
    integer id PK
    timestamp created_at
    integer account_id FK
  }
  naf {
    varchar id_1
    varchar label_1
    varchar id_2
    varchar label_2
    varchar id_3
    varchar label_3
    varchar id_4
    varchar label_4
    varchar id_5 PK
    varchar label_5
  }
  epci_commune {
    varchar insee_com PK
    varchar insee_epci
    varchar insee_dep
    varchar insee_reg
  }
  etablissement_favori {
    integer id PK
    boolean favori
    timestamp created_at
    timestamp updated_at
    integer deveco_id FK
    integer territory_id FK
    varchar siret FK
  }
  geoloc_entreprise {
    varchar siret PK
    double x
    double y
    varchar qualite_xy
    varchar epsg
    varchar plg_qp
    varchar plg_iris
    varchar plg_zus
    varchar plg_zfu
    varchar plg_qva
    varchar plg_code_commune
    varchar distance_precision
    varchar qualite_qp
    varchar qualite_iris
    varchar qualite_zus
    varchar qualite_zfu
    varchar qualite_qva
    double x_longitude
    double y_latitude
  }
  stats {
    integer id PK
    varchar code_commune
    date date
    jsonb content
  }
  territory_etablissement_reference_zonages_zonage {
    varchar territory_etablissement_reference_id PK
    integer zonage_id PK
  }
  event_log }|--|| deveco: deveco
  search_request }|--|| deveco: deveco
  epci }|--|| petr: petr
  commune }|--|| region: region
  commune }|--|| departement: departement
  commune }|--|| metropole: comParent
  commune }|--|| epci: epci
  territory ||--|| epci: epci
  territory ||--|| metropole: metropole
  territory ||--|| petr: petr
  territory ||--|| departement: departement
  territory ||--|| region: region
  territory ||--|| commune: commune
  deveco ||--|| account: account
  deveco }|--|| territory: territory
  demande }|--|| fiche: fiche
  particulier }|--|| qp_metropoleoutremer_wgs84_epsg4326: qpv
  contact }|--|| entite: entite
  contact ||--|| particulier: particulier
  exercice }|--|| entreprise: entreprise
  entreprise ||--|| etablissement: etablissement
  local }|--|| territory: territoire
  local }|--|| deveco: auteurModification
  local }|--|| deveco: createur
  proprietaire }|--|| local: local
  proprietaire }|--|| entite: entite
  proprietaire }|--|| deveco: auteurModification
  proprietaire }|--|| deveco: createur
  entite }|--|| territory: territoire
  entite }|--|| deveco: createur
  entite }|--|| deveco: auteurModification
  entite }|--|| entreprise: entreprise
  entite ||--|| particulier: particulier
  entite }|--|| entreprise: etablissementCree
  entite }|--|| particulier: createurLie
  echange }|--|| fiche: fiche
  echange }|--|| deveco: createur
  echange }|--|| deveco: auteurModification
  rappel }|--|| fiche: fiche
  rappel }|--|| deveco: createur
  rappel }|--|| deveco: auteurCloture
  fiche ||--|| entite: entite
  fiche }|--|| territory: territoire
  fiche }|--|| deveco: auteurModification
  fiche }|--|| deveco: createur
  activite_entreprise }|--|| territory: territory
  localisation_entreprise }|--|| territory: territory
  mot_cle }|--|| territory: territory
  zonage }|--|| territory: territoire
  territory_etablissement_reference }|--|| territory: territory
  territory_etablissement_reference }|--|| qp_metropoleoutremer_wgs84_epsg4326: qpv
  territory_etablissement_reference ||--|{ territory_etablissement_reference_zonages_zonage: zonages
  password ||--|| account: account
  geo_token }|--|| deveco: deveco
  geo_token_request }|--|| geo_token: geoToken
  connexion }|--|| account: account
  etablissement_favori }|--|| deveco: deveco
  etablissement_favori }|--|| territory_etablissement_reference: ter
  etablissement_favori }|--|| territory: territory
  etablissement_favori }|--|| etablissement: siret
```
