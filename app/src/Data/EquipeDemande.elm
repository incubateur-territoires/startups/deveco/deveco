module Data.EquipeDemande exposing (Autorisation, CompteDemande, EquipeDemande, Territoire(..), decodeEquipeDemande, encodeEquipeDemande, stringToTerritoire, territoireToDisplay, territoireToString, territoires)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode exposing (Value)
import Lib.Date
import Time


type alias EquipeDemande =
    { id : Int
    , nom : String
    , equipeId : Maybe Int
    , admin : Admin
    , compteDemande : CompteDemande
    , autorisations : List Autorisation
    , createdAt : Time.Posix
    , transformedAt : Maybe Time.Posix
    }


type alias Admin =
    { nom : String
    , prenom : String
    , email : String
    }


type alias CompteDemande =
    { nom : String
    , prenom : String
    , email : String
    , fonction : String
    }


type Territoire
    = Commune
    | Epci
    | Petr
    | TerritoireIndustrie
    | Departement
    | Region


territoires : List Territoire
territoires =
    [ Commune
    , Epci
    , Petr
    , TerritoireIndustrie
    , Departement
    , Region
    ]


territoireToString : Territoire -> String
territoireToString territoire =
    case territoire of
        Commune ->
            "commune"

        Epci ->
            "epci"

        Petr ->
            "petr"

        TerritoireIndustrie ->
            "territoire_industrie"

        Departement ->
            "departement"

        Region ->
            "region"


territoireToDisplay : Territoire -> String
territoireToDisplay territoire =
    case territoire of
        Commune ->
            "Commune"

        Epci ->
            "EPCI"

        Petr ->
            "PETR"

        TerritoireIndustrie ->
            "Territoire d'industrie"

        Departement ->
            "Département"

        Region ->
            "Région"


stringToTerritoire : String -> Maybe Territoire
stringToTerritoire string =
    case string of
        "commune" ->
            Just Commune

        "epci" ->
            Just Epci

        "petr" ->
            Just Petr

        "territoire_industrie" ->
            Just TerritoireIndustrie

        "departement" ->
            Just Departement

        "region" ->
            Just Region

        _ ->
            Nothing


type alias Autorisation =
    { territoireType : Territoire
    , territoireId : String
    , territoireNom : String
    , token : String
    , ayantDroit : AyantDroit
    , acceptedAt : Maybe Time.Posix
    }


type alias AyantDroit =
    { nom : String
    , prenom : String
    , fonction : String
    , email : String
    }


decodeEquipeDemande : Decoder EquipeDemande
decodeEquipeDemande =
    Decode.succeed EquipeDemande
        |> andMap (Decode.field "id" Decode.int)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "equipeId" <| Decode.nullable Decode.int)
        |> andMap (Decode.field "admin" decodeAdmin)
        |> andMap (Decode.field "compteDemande" decodeCompteDemande)
        |> andMap (Decode.field "autorisations" <| Decode.list decodeAutorisation)
        |> andMap (Decode.field "createdAt" Lib.Date.decodeDateWithTimeFromISOString)
        |> andMap (Decode.field "transformedAt" <| Decode.nullable <| Lib.Date.decodeDateWithTimeFromISOString)


decodeAdmin : Decoder Admin
decodeAdmin =
    Decode.succeed Admin
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "prenom" Decode.string)
        |> andMap (Decode.field "email" Decode.string)


decodeCompteDemande : Decoder CompteDemande
decodeCompteDemande =
    Decode.succeed CompteDemande
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "prenom" Decode.string)
        |> andMap (Decode.field "email" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.field "fonction" <| Decode.nullable Decode.string)


decodeAutorisation : Decoder Autorisation
decodeAutorisation =
    Decode.succeed Autorisation
        |> andMap
            (Decode.oneOf
                [ Decode.map (\_ -> Commune) <| Decode.field "communeId" Decode.string
                , Decode.map (\_ -> Epci) <| Decode.field "epciId" Decode.string
                , Decode.map (\_ -> Petr) <| Decode.field "petrId" Decode.string
                , Decode.map (\_ -> TerritoireIndustrie) <| Decode.field "territoireIndustrieId" Decode.string
                , Decode.map (\_ -> Departement) <| Decode.field "departementId" Decode.string
                , Decode.map (\_ -> Region) <| Decode.field "regionId" Decode.string
                ]
            )
        |> andMap
            (Decode.oneOf
                [ Decode.field "communeId" Decode.string
                , Decode.field "epciId" Decode.string
                , Decode.field "petrId" Decode.string
                , Decode.field "territoireIndustrieId" Decode.string
                , Decode.field "departementId" Decode.string
                , Decode.field "regionId" Decode.string
                ]
            )
        |> andMap
            (Decode.oneOf
                [ Decode.at [ "commune", "nom" ] Decode.string
                , Decode.at [ "epci", "nom" ] Decode.string
                , Decode.at [ "petr", "nom" ] Decode.string
                , Decode.at [ "territoireIndustrie", "nom" ] Decode.string
                , Decode.at [ "departement", "nom" ] Decode.string
                , Decode.at [ "region", "nom" ] Decode.string
                ]
            )
        |> andMap (Decode.field "token" Decode.string)
        |> andMap (Decode.field "ayantDroit" decodeAyantDroit)
        |> andMap (Decode.field "acceptedAt" <| Decode.nullable <| Lib.Date.decodeDateWithTimeFromISOString)


decodeAyantDroit : Decoder AyantDroit
decodeAyantDroit =
    Decode.succeed AyantDroit
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "prenom" Decode.string)
        |> andMap (Decode.field "fonction" Decode.string)
        |> andMap (Decode.field "email" Decode.string)


type alias EquipeDemandeInput =
    { nom : String
    , territoireType : Territoire
    , compte :
        { nom : String
        , prenom : String
        , email : String
        , fonction : String
        }
    , autorisations :
        List
            { territoireId : String
            , ayantDroit :
                { nom : String
                , prenom : String
                , email : String
                , fonction : String
                }
            }
    }


encodeEquipeDemande : EquipeDemandeInput -> Value
encodeEquipeDemande equipeDemande =
    [ ( "nom", Encode.string equipeDemande.nom )
    , ( "territoireType", encodeTerritoire equipeDemande.territoireType )
    , ( "compte"
      , [ ( "nom", Encode.string equipeDemande.compte.nom )
        , ( "prenom", Encode.string equipeDemande.compte.prenom )
        , ( "email", Encode.string equipeDemande.compte.email )
        , ( "fonction", Encode.string equipeDemande.compte.fonction )
        ]
            |> Encode.object
      )
    , ( "autorisations"
      , equipeDemande.autorisations
            |> Encode.list
                (\autorisation ->
                    [ ( "territoireId", Encode.string autorisation.territoireId )
                    , ( "ayantDroit"
                      , [ ( "nom", Encode.string autorisation.ayantDroit.nom )
                        , ( "prenom", Encode.string autorisation.ayantDroit.prenom )
                        , ( "email", Encode.string autorisation.ayantDroit.email )
                        , ( "fonction", Encode.string autorisation.ayantDroit.fonction )
                        ]
                            |> Encode.object
                      )
                    ]
                        |> Encode.object
                )
      )
    ]
        |> Encode.object


encodeTerritoire : Territoire -> Value
encodeTerritoire =
    territoireToString >> Encode.string
