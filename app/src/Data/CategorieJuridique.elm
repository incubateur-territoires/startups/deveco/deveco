module Data.CategorieJuridique exposing (CategorieJuridique, decodeCategorieJuridique)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)


type alias CategorieJuridique =
    { id : String
    , nom : String
    , niveau : Int
    , parentId : String
    }


decodeCategorieJuridique : Decoder CategorieJuridique
decodeCategorieJuridique =
    Decode.succeed CategorieJuridique
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "nom" Decode.string)
        |> andMap (Decode.field "niveau" Decode.int)
        |> andMap (Decode.field "parentId" Decode.string)
