module Data.Adresse exposing (Adresse, ApiAdresse, ApiStreet, adresseToString, apiAdresseToAdresse, decodeAdresse, decodeApiFeatures, decodeApiStreet, decodeCoordinates, encodeAdresse)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode


type alias Adresse =
    { numero : String
    , voieNom : String
    , ville : String
    , codePostal : String
    , communeId : String
    , geolocalisation : Maybe ( Float, Float )
    }


type alias ApiAdresse =
    { label : String
    , housenumber : String
    , street : String
    , city : String
    , postcode : String
    , citycode : String
    , coordinates : Maybe ( Float, Float )
    , type_ : Maybe String
    }


type alias ApiStreet =
    { label : String
    , housenumber : String
    , street : String
    , postcode : String
    , citycode : Maybe String
    , city : String
    }


apiAdresseToAdresse : ApiAdresse -> Adresse
apiAdresseToAdresse apiAdresse =
    { numero = apiAdresse.housenumber
    , voieNom = apiAdresse.street
    , ville = apiAdresse.city
    , codePostal = apiAdresse.postcode
    , communeId = apiAdresse.citycode
    , geolocalisation = apiAdresse.coordinates
    }


decodeApiAdresse : Decoder ApiAdresse
decodeApiAdresse =
    Decode.succeed ApiAdresse
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.at [ "properties", "label" ] Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.at [ "properties", "housenumber" ] Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.at [ "properties", "street" ] Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.at [ "properties", "city" ] Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.at [ "properties", "postcode" ] Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.at [ "properties", "citycode" ] Decode.string)
        |> andMap
            (Decode.oneOf
                [ Decode.at [ "geometry", "coordinates" ] <|
                    Decode.map Just <|
                        decodeCoordinates
                , Decode.succeed Nothing
                ]
            )
        |> andMap (Decode.maybe <| Decode.at [ "properties", "type" ] Decode.string)


decodeCoordinates : Decoder ( Float, Float )
decodeCoordinates =
    Decode.andThen
        (\list ->
            case list of
                [] ->
                    Decode.fail "Empty list not accepted for coordinates"

                x :: [] ->
                    Decode.fail <| "Only one coordinate: " ++ String.fromFloat x

                x :: y :: [] ->
                    Decode.succeed ( x, y )

                _ ->
                    Decode.fail <|
                        "Too many coordinates: "
                            ++ String.join ", " (List.map String.fromFloat list)
        )
    <|
        Decode.list <|
            Decode.float


decodeApiFeatures : Decoder (List ApiAdresse)
decodeApiFeatures =
    Decode.field "features" <|
        Decode.list <|
            decodeApiAdresse


decodeApiStreet : Decoder (List ApiStreet)
decodeApiStreet =
    Decode.field "features" <|
        Decode.list <|
            Decode.field "properties" <|
                (Decode.succeed ApiStreet
                    |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "label" Decode.string)
                    |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "housenumber" Decode.string)
                    |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "street" Decode.string)
                    |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "postcode" Decode.string)
                    |> andMap (Decode.maybe <| Decode.field "citycode" Decode.string)
                    |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "city" Decode.string)
                )


decodeAdresse : Decoder Adresse
decodeAdresse =
    Decode.succeed Adresse
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.field "numero" <| Decode.maybe Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.field "voieNom" <| Decode.maybe Decode.string)
        |> andMap (Decode.at [ "commune", "nom" ] Decode.string)
        |> andMap (Decode.field "codePostal" Decode.string)
        |> andMap (Decode.field "communeId" Decode.string)
        |> andMap (Decode.maybe <| Decode.field "geolocalisation" decodeCoordinates)


encodeAdresse : Adresse -> Encode.Value
encodeAdresse { numero, voieNom, ville, codePostal, communeId, geolocalisation } =
    Encode.object
        [ ( "numero", Encode.string numero )
        , ( "voieNom", Encode.string voieNom )
        , ( "ville", Encode.string ville )
        , ( "codePostal", Encode.string codePostal )
        , ( "communeId", Encode.string communeId )
        , ( "geolocalisation"
          , Maybe.withDefault Encode.null <|
                Maybe.map
                    (\( x, y ) ->
                        Encode.list Encode.float [ x, y ]
                    )
                <|
                    geolocalisation
          )
        ]


adresseToString : Adresse -> String
adresseToString { numero, voieNom, codePostal, ville } =
    [ numero
    , voieNom
    , codePostal
    ]
        |> List.filterMap
            (\s ->
                if s == "" then
                    Nothing

                else
                    Just s
            )
        |> String.join " "
        |> (\p ->
                if p /= "" then
                    String.join " - " [ p, ville ]

                else
                    ville
           )
