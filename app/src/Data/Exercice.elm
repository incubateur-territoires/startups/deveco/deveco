module Data.Exercice exposing (Exercice, tranchesExercices)


type alias Exercice =
    { id : String
    , label : String
    }


tranchesExercices : List Exercice
tranchesExercices =
    [ { id = "ND", label = "Données indisponibles" }
    , { id = "01", label = "Entre 0 et 5 000 €" }
    , { id = "11", label = "Entre 5 001 € et 10 000 €" }
    , { id = "12", label = "Entre 10 001 € et 32 600 €" }
    , { id = "13", label = "Entre 32 601 € et 100 000 €" }
    , { id = "14", label = "Entre 100 001 € et 250 000 €" }
    , { id = "15", label = "Entre 250 001 € et 500 000 €" }
    , { id = "16", label = "Entre 500 001 € et 1M €" }
    , { id = "21", label = "Entre 1M € et 10M €" }
    , { id = "22", label = "Entre 10M € et 50M €" }
    , { id = "23", label = "Entre 50M € et 100M €" }
    , { id = "24", label = "Entre 100M € et 500M €" }
    , { id = "25", label = "Entre 500M € et 1 Mds €" }
    , { id = "31", label = "Entre 1 Mds €et 50 Mds €" }
    , { id = "32", label = "50 Mds € et plus" }
    ]
