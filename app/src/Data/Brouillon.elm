module Data.Brouillon exposing (Brouillon(..), BrouillonData, BrouillonId, auteur, date, decodeBrouillon, description, id, modification, titre)

import Api.EntityId exposing (EntityId)
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Lib.Date
import Lib.UI exposing (displayPrenomNomPoint)
import Time


type Brouillon
    = Brouillon BrouillonData


type BrouillonId
    = BrouillonId


type alias BrouillonData =
    { id : EntityId BrouillonId
    , nom : String
    , description : String
    , date : Date
    , auteur : String
    , modificationAuteurDate : Maybe ( String, Time.Posix )
    }


id : Brouillon -> EntityId BrouillonId
id (Brouillon data) =
    data.id


titre : Brouillon -> String
titre (Brouillon data) =
    data.nom


description : Brouillon -> String
description (Brouillon data) =
    data.description


date : Brouillon -> Date
date (Brouillon data) =
    data.date


auteur : Brouillon -> String
auteur (Brouillon data) =
    data.auteur


modification : Brouillon -> Maybe ( String, Time.Posix )
modification (Brouillon data) =
    data.modificationAuteurDate


decodeBrouillon : Decoder Brouillon
decodeBrouillon =
    Decode.succeed BrouillonData
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "description" Decode.string)
        |> andMap (Decode.field "date" <| Lib.Date.decodeCalendarDate)
        |> andMap
            (Decode.field "compte" <|
                Decode.map2 (\p n -> displayPrenomNomPoint { prenom = p, nom = n })
                    (Decode.map (Maybe.withDefault "?") <| Decode.field "prenom" <| Decode.nullable Decode.string)
                    (Decode.map (Maybe.withDefault "?") <| Decode.field "nom" <| Decode.nullable Decode.string)
            )
        |> andMap
            (Decode.field "modification" <|
                Decode.nullable <|
                    Decode.map2 Tuple.pair
                        (Decode.field "compte" <|
                            Decode.map2 (\prenom nom -> prenom ++ " " ++ nom)
                                (Decode.field "prenom" Decode.string)
                                (Decode.field "nom" Decode.string)
                        )
                        (Decode.field "date" Lib.Date.decodeDateWithTimeFromISOString)
            )
        |> Decode.map Brouillon
