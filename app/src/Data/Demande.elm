module Data.Demande exposing (Demande, DemandeId, TypeDemande(..), cloture, clotureDate, clotureMotif, date, decodeDemande, decodeTypeDemande, description, encodeDemandeCloture, entite, id, label, listeEtablissement, listeLocal, nom, nombreEchanges, stringToTypeDemande, typeDemandeToDisplay, typeDemandeToString, type_)

import Api.EntityId exposing (EntityId)
import Data.Entite exposing (Entite, decodeEntite)
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode exposing (Value)
import Lib.Date


type Demande
    = Demande DemandeData


type DemandeId
    = DemandeId


type alias DemandeData =
    { id : EntityId DemandeId
    , nom : String
    , description : String
    , type_ : TypeDemande
    , date : Date
    , cloture : Bool
    , motif : String
    , clotureDate : Maybe Date
    , nombreEchanges : Int
    , entite : Maybe Entite
    }


type TypeDemande
    = Autre
      -- établissement
    | Foncier
    | Immobilier
    | Economique
    | Fiscalite
    | Accompagnement
    | RH
      -- local
    | Urbanisme
    | Urgence
    | Commercialisation
    | TransactionFonds
    | TransactionMurs


id : Demande -> EntityId DemandeId
id (Demande data) =
    data.id


nom : Demande -> String
nom (Demande data) =
    data.nom


description : Demande -> String
description (Demande data) =
    data.description


date : Demande -> Date
date (Demande data) =
    data.date


type_ : Demande -> TypeDemande
type_ (Demande data) =
    data.type_


cloture : Demande -> Bool
cloture (Demande data) =
    data.cloture


clotureMotif : Demande -> String
clotureMotif (Demande data) =
    data.motif


clotureDate : Demande -> Maybe Date
clotureDate (Demande data) =
    data.clotureDate


nombreEchanges : Demande -> Int
nombreEchanges (Demande data) =
    data.nombreEchanges


entite : Demande -> Maybe Entite
entite (Demande data) =
    data.entite


label : Demande -> String
label =
    type_ >> typeDemandeToDisplay


typeDemandeToString : TypeDemande -> String
typeDemandeToString typeDemande =
    case typeDemande of
        Foncier ->
            "foncier"

        Immobilier ->
            "immobilier"

        Economique ->
            "economique"

        Fiscalite ->
            "fiscalite"

        Accompagnement ->
            "accompagnement"

        RH ->
            "rh"

        Autre ->
            "autre"

        Urbanisme ->
            "urbanisme"

        Urgence ->
            "urgence"

        Commercialisation ->
            "commercialisation"

        TransactionFonds ->
            "transaction_fonds"

        TransactionMurs ->
            "transaction_murs"


decodeTypeDemande : Decoder TypeDemande
decodeTypeDemande =
    Decode.string
        |> Decode.andThen
            (\s ->
                s
                    |> stringToTypeDemande
                    |> Maybe.map Decode.succeed
                    |> Maybe.withDefault (Decode.fail <| "Unknown typeDemande " ++ s)
            )


stringToTypeDemande : String -> Maybe TypeDemande
stringToTypeDemande s =
    case s of
        "foncier" ->
            Just Foncier

        "immobilier" ->
            Just Immobilier

        "economique" ->
            Just Economique

        "fiscalite" ->
            Just Fiscalite

        "accompagnement" ->
            Just Accompagnement

        "rh" ->
            Just RH

        "autre" ->
            Just Autre

        "urbanisme" ->
            Just Urbanisme

        "urgence" ->
            Just Urgence

        "commercialisation" ->
            Just Commercialisation

        "transaction_fonds" ->
            Just TransactionFonds

        "transaction_murs" ->
            Just TransactionMurs

        _ ->
            Nothing


typeDemandeToDisplay : TypeDemande -> String
typeDemandeToDisplay demande =
    case demande of
        Foncier ->
            "Foncier"

        Immobilier ->
            "Immobilier et locaux d'activité"

        Economique ->
            "Aide économique"

        Fiscalite ->
            "Fiscalité"

        Accompagnement ->
            "Accompagnement"

        RH ->
            "RH"

        Autre ->
            "Autre"

        Urbanisme ->
            "Démarches d'urbanisme"

        Urgence ->
            "Procédures d'urgence"

        Commercialisation ->
            "Commercialisation location"

        TransactionFonds ->
            "Transaction - Fonds de commerce"

        TransactionMurs ->
            "Transaction - Murs"


listeEtablissement : List TypeDemande
listeEtablissement =
    [ Accompagnement
    , Economique
    , Fiscalite
    , Foncier
    , Immobilier
    , RH
    , Autre
    ]


listeLocal : List TypeDemande
listeLocal =
    [ Urbanisme
    , Urgence
    , Commercialisation
    , TransactionFonds
    , TransactionMurs
    , Autre
    ]


decodeDemande : Decoder Demande
decodeDemande =
    Decode.succeed DemandeData
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "nom" <| Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "description" <| Decode.string)
        |> andMap (Decode.field "typeId" <| decodeTypeDemande)
        |> andMap (Decode.field "date" Lib.Date.decodeCalendarDate)
        |> andMap (Decode.map (Maybe.map (\_ -> True) >> Maybe.withDefault False) <| optionalNullableField "clotureDate" <| Lib.Date.decodeCalendarDate)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "clotureMotif" <| Decode.string)
        |> andMap (optionalNullableField "clotureDate" <| Lib.Date.decodeCalendarDate)
        |> andMap (Decode.map (Maybe.withDefault 0) <| optionalNullableField "nombreEchanges" <| Decode.int)
        |> andMap (Decode.oneOf [ Decode.map Just <| decodeEntite, Decode.succeed Nothing ])
        |> Decode.map Demande


encodeDemandeCloture : { demande | id : EntityId DemandeId, motif : String, cloture : Bool } -> Value
encodeDemandeCloture demande =
    [ ( "demande"
      , [ ( "clotureMotif", Encode.string demande.motif )
        , ( "aCloturer", Encode.bool <| not demande.cloture )
        ]
            |> Encode.object
      )
    ]
        |> Encode.object
