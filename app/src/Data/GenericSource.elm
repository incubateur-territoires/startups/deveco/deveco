module Data.GenericSource exposing (GenericSource, decodeGenericSource)

import Json.Decode as Decode exposing (Decoder)


type alias GenericSource =
    { id : String
    , nom : String
    }


decodeGenericSource : Decoder { id : String, nom : String }
decodeGenericSource =
    Decode.map2 (\id label -> { id = id, nom = label })
        (Decode.field "id" Decode.string)
        (Decode.field "nom" Decode.string)
