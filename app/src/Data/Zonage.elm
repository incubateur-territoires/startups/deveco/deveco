module Data.Zonage exposing (Zonage, ZonageId, decodeZonage)

import Api.EntityId
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)


type ZonageId
    = ZonageId


type alias Zonage =
    { id : Api.EntityId.EntityId ZonageId
    , nom : String
    , description : String
    , enCours : Bool
    }


decodeZonage : Decoder Zonage
decodeZonage =
    Decode.succeed Zonage
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "description" Decode.string)
        |> andMap (Decode.field "enCours" Decode.bool)
