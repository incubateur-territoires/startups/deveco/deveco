module Data.GeoToken exposing (GeoToken, decodeGeoToken)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)


type alias GeoToken =
    { id : Int
    , token : String
    , active : Bool
    }


decodeGeoToken : Decoder GeoToken
decodeGeoToken =
    Decode.succeed GeoToken
        |> andMap (Decode.field "id" Decode.int)
        |> andMap (Decode.field "token" Decode.string)
        |> andMap (Decode.field "active" Decode.bool)
