module Data.Role exposing (Role(..), decodeRole, equipe, hasCarto, isDeveco, isFrance)

import Data.Equipe exposing (EquipeLight, decodeEquipeLight)
import Json.Decode as Decode exposing (Decoder)


type Role
    = Deveco EquipeLight
    | Superadmin


decodeRole : Decoder Role
decodeRole =
    Decode.field "compte" (Decode.field "typeId" Decode.string)
        |> Decode.andThen
            (\s ->
                case s of
                    "deveco" ->
                        Decode.map Deveco
                            (Decode.field "equipe" <| decodeEquipeLight)

                    "superadmin" ->
                        Decode.succeed Superadmin

                    _ ->
                        Decode.fail <| "role inconnu\u{00A0}: " ++ s
            )


equipe : Role -> Maybe EquipeLight
equipe role =
    case role of
        Superadmin ->
            Nothing

        Deveco e ->
            Just e


isDeveco : Role -> Bool
isDeveco role =
    case role of
        Superadmin ->
            False

        Deveco _ ->
            True


isFrance : Role -> Bool
isFrance role =
    case role of
        Superadmin ->
            False

        Deveco eq ->
            eq.isFrance


hasCarto : Role -> Bool
hasCarto role =
    case role of
        Superadmin ->
            False

        Deveco eq ->
            eq.hasCarto
