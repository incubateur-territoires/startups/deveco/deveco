module Data.Rappel exposing (Rappel(..), RappelData, RappelId, affectation, clotureDate, date, decodeRappel, description, entite, id, titre)

import Api.EntityId exposing (EntityId, decodeEntityId)
import Data.Collegues exposing (Collegue, decodeCollegue)
import Data.Entite exposing (Entite, decodeEntite)
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Lib.Date


type Rappel
    = Rappel RappelData


type RappelId
    = RappelId


type alias RappelData =
    { id : EntityId RappelId
    , titre : String
    , description : String
    , date : Date
    , affectation : Maybe Collegue
    , clotureDate : Maybe Date
    , entite : Maybe Entite
    }


id : Rappel -> EntityId RappelId
id (Rappel data) =
    data.id


titre : Rappel -> String
titre (Rappel data) =
    data.titre


description : Rappel -> String
description (Rappel data) =
    data.description


date : Rappel -> Date
date (Rappel data) =
    data.date


affectation : Rappel -> Maybe Collegue
affectation (Rappel data) =
    data.affectation


clotureDate : Rappel -> Maybe Date
clotureDate (Rappel data) =
    data.clotureDate


entite : Rappel -> Maybe Entite
entite (Rappel data) =
    data.entite


decodeRappel : Decoder Rappel
decodeRappel =
    Decode.succeed RappelData
        |> andMap (Decode.field "id" decodeEntityId)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "description" Decode.string)
        |> andMap (Decode.field "date" Lib.Date.decodeCalendarDate)
        |> andMap (Decode.field "affecte" <| Decode.nullable <| decodeCollegue)
        |> andMap (optionalNullableField "clotureDate" Lib.Date.decodeCalendarDate)
        |> andMap (Decode.oneOf [ Decode.map Just <| decodeEntite, Decode.succeed Nothing ])
        |> Decode.map Rappel
