module Data.EtablissementCreation exposing (EtablissementCreation, EtablissementCreationApercu, decodeEtablissementCreation, decodeEtablissementCreationApercu)

import Api.EntityId exposing (EntityId, decodeEntityId)
import Data.Contact exposing (Contact, decodeContact)
import Data.Demande exposing (Demande, decodeDemande)
import Data.EtablissementCreationId exposing (EtablissementCreationId)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)


type alias EtablissementCreation =
    { id : EntityId EtablissementCreationId
    , nom : String
    , activitesReelles : List String
    , futureEnseigne : String
    , demandes : List Demande
    , createurs : ( Contact, List Contact )
    , creationAbandonnee : Bool
    }


type alias EtablissementCreationApercu =
    { id : EntityId EtablissementCreationId
    , nom : String
    }


decodeEtablissementCreation : Decoder EtablissementCreation
decodeEtablissementCreation =
    Decode.succeed EtablissementCreation
        |> andMap (Decode.field "id" decodeEntityId)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.at [ "etiquettes", "activites" ] <| Decode.list <| Decode.field "nom" Decode.string)
        |> andMap (Decode.field "futureEnseigne" Decode.string)
        |> andMap (Decode.field "demandes" <| Decode.list decodeDemande)
        |> andMap (Decode.field "createurs" <| Decode.oneOrMore Tuple.pair decodeContact)
        |> andMap (Decode.field "creationAbandonnee" <| Decode.bool)


decodeEtablissementCreationApercu : Decoder EtablissementCreationApercu
decodeEtablissementCreationApercu =
    Decode.succeed EtablissementCreationApercu
        |> andMap (Decode.field "id" decodeEntityId)
        |> andMap (Decode.field "nomAffichage" Decode.string)
