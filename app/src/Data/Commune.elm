module Data.Commune exposing (Commune, decodeCommune)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)


type alias Commune =
    { id : String
    , nom : String
    , departementId : String
    }


decodeCommune : Decoder Commune
decodeCommune =
    Decode.succeed Commune
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "nom" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "departementId" Decode.string)
