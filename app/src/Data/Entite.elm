module Data.Entite exposing (Entite(..), decodeEntite, viewLienEntite)

import Accessibility exposing (Html, div, text)
import Api.EntityId exposing (EntityId, decodeEntityId)
import DSFR.Typography as Typo
import Data.Etablissement exposing (Siret)
import Data.EtablissementCreationId exposing (EtablissementCreationId)
import Html.Extra exposing (nothing)
import Json.Decode as Decode exposing (Decoder)
import Route


type Entite
    = Etab Siret String
    | EtabCrea (EntityId EtablissementCreationId) String
    | Loc String String


decodeEntite : Decoder Entite
decodeEntite =
    Decode.oneOf
        [ Decode.map2 Etab
            (Decode.field "etablissementId" <|
                Decode.string
            )
            (Decode.field "etablissementNom" <|
                Decode.string
            )
        , Decode.map2 EtabCrea
            (Decode.field "etablissementCreationId" <|
                decodeEntityId
            )
            (Decode.field "createurNom" <|
                Decode.string
            )
        , Decode.map2 Loc
            (Decode.field "localId" <|
                Decode.string
            )
            (Decode.field "localNom" <|
                Decode.string
            )
        ]


viewLienEntite : String -> Maybe Entite -> Html msg
viewLienEntite ongletType entite =
    case entite of
        Nothing ->
            nothing

        Just e ->
            let
                ( typeName, name, link ) =
                    case e of
                        Etab siret nom ->
                            ( "Établissement"
                            , nom
                            , Route.toUrl <|
                                Route.Etablissement
                                    -- TODO utiliser la fonction de chaque route, sans faire de dépendance circulaire
                                    ( Just <| "onglet=suivi&suivi=" ++ ongletType
                                    , siret
                                    )
                            )

                        EtabCrea etablissementCreationId nom ->
                            ( "Créateur d'entreprise"
                            , nom
                            , Route.toUrl <|
                                Route.Createur <|
                                    -- TODO utiliser la fonction de chaque route, sans faire de dépendance circulaire
                                    ( Just <| "onglet=suivi&suivi=" ++ ongletType
                                    , etablissementCreationId
                                    )
                            )

                        Loc localId nom ->
                            ( "Local"
                            , nom
                            , Route.toUrl <|
                                Route.Local <|
                                    -- TODO utiliser la fonction de chaque route, sans faire de dépendance circulaire
                                    ( Just <| "onglet=suivi&suivi=" ++ ongletType
                                    , localId
                                    )
                            )
            in
            div [] [ text <| typeName ++ "\u{00A0}: ", Typo.link link [] [ text name ] ]
