module Data.AFR exposing (AFR, afrs)


type alias AFR =
    { id : String
    , label : String
    }


afrs : List AFR
afrs =
    [ { id = "O", label = "Oui" }
    , { id = "P", label = "Partiel" }
    , { id = "N", label = "Non" }
    ]
