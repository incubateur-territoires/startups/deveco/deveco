module Data.Etablissement exposing (EntrepriseType(..), EtablissementApercu, EtablissementSimple, EtablissementSimpleAvecPortefeuille, EtatAdministratif(..), Siret, decodeAdresseToString, decodeEntrepriseType, decodeEtablissementApercu, decodeEtablissementSimple, decodeEtablissementSimpleAvecPortefeuille, decodeNaf, decodeNafId, entrepriseTypeToDisplay, entrepriseTypeToString, stringToEntrepriseType)

import Data.Adresse
import Data.PortefeuilleInfos exposing (PortefeuilleInfos, decodePortefeuilleInfos)
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Lib.Date


type alias Siret =
    String


type alias EtablissementSimple =
    { siret : Siret
    , nomAffichage : String
    , nom : Maybe String
    , nomEntreprise : Maybe String
    , adresse : String
    , geolocalisation : Maybe ( Float, Float )
    , dateCreationEtablissement : Maybe Date
    , dateFermetureEtablissement : Maybe Date
    , activiteNaf : Maybe String
    , activiteNafId : Maybe String
    , etatAdministratif : EtatAdministratif
    , siege : Bool
    , procedure : Bool
    , diffusible : Bool
    , inpiCaVariation : Maybe Float
    , emaVariation : Maybe Float

    -- données perso
    , france : Bool
    , clotureDateContribution : Maybe Date
    , enseigneContribution : Maybe String
    , geolocalisationContribution : Maybe ( Float, Float )
    , geolocalisationEquipe : Maybe String
    }


type alias EtablissementApercu =
    { siret : Siret
    , nomAffichage : String
    , adresse : String
    , etatAdministratif : EtatAdministratif
    , clotureDateContribution : Maybe Date
    , siege : Bool
    }


type alias EtablissementSimpleAvecPortefeuille =
    ( EtablissementSimple, PortefeuilleInfos )


type EtatAdministratif
    = Actif
    | Inactif


decodeNaf : Decoder (Maybe String)
decodeNaf =
    Decode.field "activiteNaf" <|
        Decode.map (Maybe.andThen identity) <|
            Decode.nullable <|
                (Decode.succeed
                    (\libelle code ->
                        case ( libelle, code ) of
                            ( Just l, Just c ) ->
                                l
                                    ++ " ("
                                    ++ c
                                    ++ ")"
                                    |> Just

                            ( Just l, Nothing ) ->
                                l
                                    |> Just

                            ( Nothing, Just c ) ->
                                c
                                    |> Just

                            ( Nothing, Nothing ) ->
                                Nothing
                    )
                    |> andMap
                        (Decode.field
                            "nom"
                         <|
                            Decode.nullable
                                Decode.string
                        )
                    |> andMap
                        (Decode.field
                            "id"
                         <|
                            Decode.nullable
                                Decode.string
                        )
                )


decodeNafId : Decoder (Maybe String)
decodeNafId =
    Decode.field "activiteNaf" <|
        (Decode.nullable <|
            Decode.field "id" <|
                Decode.string
        )


decodeEtatAdministratif : Decoder EtatAdministratif
decodeEtatAdministratif =
    Decode.bool
        |> Decode.map
            (\actif ->
                if actif then
                    Actif

                else
                    Inactif
            )


decodeEtablissementSimple : Decoder EtablissementSimple
decodeEtablissementSimple =
    Decode.succeed EtablissementSimple
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "nomAffichage" Decode.string)
        |> andMap (Decode.field "nom" <| Decode.nullable Decode.string)
        |> andMap (Decode.field "nomEntreprise" <| Decode.nullable Decode.string)
        |> andMap (Decode.field "adresse1" <| decodeAdresseToString)
        |> andMap (Decode.field "adresse1" <| decodeAdresseGeolocalisation)
        |> andMap (Decode.field "creationDate" <| Decode.maybe <| Lib.Date.decodeCalendarDate)
        |> andMap (Decode.field "fermetureDate" <| Decode.maybe <| Lib.Date.decodeCalendarDate)
        |> andMap decodeNaf
        |> andMap decodeNafId
        |> andMap (Decode.field "actif" decodeEtatAdministratif)
        |> andMap (Decode.map (Maybe.withDefault False) <| Decode.maybe <| Decode.field "siege" <| Decode.bool)
        |> andMap (Decode.field "procedure" Decode.bool)
        |> andMap (Decode.field "diffusible" Decode.bool)
        |> andMap (Decode.field "inpiCaVariation" <| Decode.nullable Decode.float)
        |> andMap (Decode.field "emaVariation" <| Decode.nullable Decode.float)
        |> andMap (Decode.map (Maybe.withDefault False) <| optionalNullableField "france" Decode.bool)
        |> andMap (Decode.field "clotureDateContribution" <| Decode.maybe <| Lib.Date.decodeCalendarDate)
        |> andMap (optionalNullableField "enseigneContribution" <| Decode.string)
        |> andMap (optionalNullableField "geolocalisationContribution" <| Data.Adresse.decodeCoordinates)
        |> andMap (optionalNullableField "geolocalisationEquipe" <| Decode.string)


decodeAdresseToString : Decoder String
decodeAdresseToString =
    Decode.map adresseToString <|
        decodeAdresse


decodeAdresseGeolocalisation : Decoder (Maybe ( Float, Float ))
decodeAdresseGeolocalisation =
    Decode.map .geolocalisation <|
        decodeAdresse


adresseToString : Adresse -> String
adresseToString { numero, voie, complementAdresse, codePostal, distributionSpeciale, cedex, libelleCedex, etrangerCommune, etrangerPays, commune } =
    let
        codePostalOuCedex =
            case ( cedex, libelleCedex ) of
                ( Just c, Just l ) ->
                    [ Just c, Just l ]

                _ ->
                    case ( etrangerCommune, etrangerPays ) of
                        ( Just c, Just p ) ->
                            [ Just c, Just p ]

                        _ ->
                            [ codePostal, commune ]
    in
    [ numero
    , voie
    , complementAdresse
    , distributionSpeciale
    ]
        ++ codePostalOuCedex
        |> List.filterMap
            (\s ->
                if List.member (Maybe.withDefault "" s) [ "", "[ND]" ] then
                    Nothing

                else
                    s
            )
        |> String.join " "


type alias Adresse =
    { numero : Maybe String
    , voie : Maybe String
    , complementAdresse : Maybe String
    , distributionSpeciale : Maybe String
    , codePostal : Maybe String
    , commune : Maybe String
    , cedex : Maybe String
    , libelleCedex : Maybe String
    , etrangerCommune : Maybe String
    , etrangerPays : Maybe String
    , geolocalisation : Maybe ( Float, Float )
    }


decodeAdresse : Decoder Adresse
decodeAdresse =
    Decode.succeed Adresse
        |> andMap (Decode.field "numero" <| Decode.nullable Decode.string)
        |> andMap (Decode.field "voieNom" <| Decode.nullable Decode.string)
        |> andMap (Decode.field "complementAdresse" <| Decode.nullable Decode.string)
        |> andMap (Decode.field "distributionSpeciale" <| Decode.nullable Decode.string)
        |> andMap (Decode.field "codePostal" <| Decode.nullable Decode.string)
        |> andMap (Decode.at [ "commune", "nom" ] <| Decode.nullable Decode.string)
        |> andMap (Decode.field "cedex" <| Decode.nullable Decode.string)
        |> andMap (Decode.field "cedexNom" <| Decode.nullable Decode.string)
        |> andMap (Decode.field "etrangerCommune" <| Decode.nullable Decode.string)
        |> andMap (Decode.field "etrangerPays" <| Decode.nullable Decode.string)
        |> andMap
            (Decode.field "geolocalisation" <|
                Decode.nullable <|
                    Data.Adresse.decodeCoordinates
            )


decodeEtablissementSimpleAvecPortefeuille : Decoder EtablissementSimpleAvecPortefeuille
decodeEtablissementSimpleAvecPortefeuille =
    Decode.succeed Tuple.pair
        |> andMap (Decode.field "etablissement" decodeEtablissementSimple)
        |> andMap (Decode.field "portefeuilleInfos" decodePortefeuilleInfos)


decodeEtablissementApercu : Decoder EtablissementApercu
decodeEtablissementApercu =
    Decode.succeed EtablissementApercu
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "nomAffichage" Decode.string)
        |> andMap (Decode.field "adresse1" <| decodeAdresseToString)
        |> andMap (Decode.field "actif" decodeEtatAdministratif)
        |> andMap (Decode.field "clotureDateContribution" <| Decode.maybe <| Lib.Date.decodeCalendarDate)
        |> andMap (Decode.map (Maybe.withDefault False) <| Decode.maybe <| Decode.field "siege" <| Decode.bool)


type EntrepriseType
    = PMEEntrepriseType
    | ETIEntrepriseType
    | GEEntrepriseType
    | InconnuEntrepriseType


stringToEntrepriseType : String -> Maybe EntrepriseType
stringToEntrepriseType entrepriseType =
    case entrepriseType of
        "PME" ->
            Just PMEEntrepriseType

        "ETI" ->
            Just ETIEntrepriseType

        "GE" ->
            Just GEEntrepriseType

        "Inconnu" ->
            Just InconnuEntrepriseType

        _ ->
            Nothing


entrepriseTypeToString : EntrepriseType -> String
entrepriseTypeToString entrepriseType =
    case entrepriseType of
        PMEEntrepriseType ->
            "PME"

        ETIEntrepriseType ->
            "ETI"

        GEEntrepriseType ->
            "GE"

        InconnuEntrepriseType ->
            "Inconnu"


entrepriseTypeToDisplay : EntrepriseType -> String
entrepriseTypeToDisplay entrepriseType =
    case entrepriseType of
        PMEEntrepriseType ->
            "Petite ou Moyenne Entreprise (PME)"

        ETIEntrepriseType ->
            "Entreprise de Taille Intermédiaire (ETI)"

        GEEntrepriseType ->
            "Grande Entreprise (GE)"

        InconnuEntrepriseType ->
            "Inconnu"


decodeEntrepriseType : Decoder EntrepriseType
decodeEntrepriseType =
    Decode.string
        |> Decode.andThen
            (\s ->
                s
                    |> stringToEntrepriseType
                    |> Maybe.map Decode.succeed
                    |> Maybe.withDefault (Decode.fail <| "Type d'entreprise inconnu : " ++ s)
            )
