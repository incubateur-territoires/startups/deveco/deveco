module Data.Personne exposing (Personne, PersonneId, decodePersonne, displayPrenomNomPersonne, emptyPersonne, encodePersonne)

import Api.EntityId exposing (EntityId, encodeEntityId)
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode exposing (Value)
import Lib.Date


type PersonneId
    = PersonneId


type alias Personne =
    { id : EntityId PersonneId
    , prenom : String
    , nom : String
    , email : String
    , telephone : String
    , telephone2 : String
    , naissanceDate : Maybe Date
    , demarchageAccepte : Bool
    }


emptyPersonne : Personne
emptyPersonne =
    { id = Api.EntityId.newId
    , prenom = ""
    , nom = ""
    , email = ""
    , telephone = ""
    , telephone2 = ""
    , naissanceDate = Nothing
    , demarchageAccepte = True
    }


decodePersonne : Decoder Personne
decodePersonne =
    Decode.succeed Personne
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "prenom" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "nom" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "email" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "telephone" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "telephone2" Decode.string)
        |> andMap (optionalNullableField "naissanceDate" Lib.Date.decodeCalendarDate)
        |> andMap (Decode.map (Maybe.withDefault True) <| optionalNullableField "demarchageAccepte" Decode.bool)


encodePersonne : Personne -> Value
encodePersonne personne =
    [ ( "id", encodeEntityId personne.id )
    , ( "prenom", Encode.string personne.prenom )
    , ( "nom", Encode.string personne.nom )
    , ( "email", Encode.string personne.email )
    , ( "telephone", Encode.string personne.telephone )
    , ( "telephone2", Encode.string personne.telephone2 )
    , ( "naissanceDate", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| Maybe.map Date.toIsoString <| personne.naissanceDate )
    , ( "demarchageAccepte", Encode.bool personne.demarchageAccepte )
    ]
        |> Encode.object


displayPrenomNomPersonne : { personne | prenom : String, nom : String } -> String
displayPrenomNomPersonne personne =
    case ( personne.prenom, personne.nom ) of
        ( "", "" ) ->
            ""

        ( prenom, "" ) ->
            prenom

        ( "", nom ) ->
            nom

        ( prenom, nom ) ->
            prenom ++ " " ++ nom
