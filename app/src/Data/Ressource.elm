module Data.Ressource exposing (Ressource, RessourceAction(..), RessourceField(..), decodeRessource, encodeRessource, ressourceTypeToDisplay, ressourceTypeToString, ressourceTypes, updateRessource, viewRessources, viewRessourcesHelper)

import Accessibility exposing (Html, div, h3, span, text)
import DSFR.Button
import DSFR.Icons.Design
import DSFR.Icons.System
import DSFR.Table
import DSFR.Typography as Typo
import Html.Attributes exposing (class)
import Html.Extra exposing (viewIf)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode exposing (Value)
import Lib.UI exposing (withEmptyAs)
import UI.Clipboard


type alias Ressource =
    { id : Int
    , nom : String
    , lien : String
    , type_ : RessourceType
    }


type RessourceField
    = RType
    | RNom
    | RLien


emptyRessource : Ressource
emptyRessource =
    { id = -1
    , nom = ""
    , lien = ""
    , type_ = SiteWeb
    }


{-| 'website' | 'article' | 'social' | 'url' | 'autre'
-}
type RessourceType
    = SiteWeb
    | ArticlePresse
    | ReseauxSociaux
    | URL
    | Autre


ressourceTypes : List RessourceType
ressourceTypes =
    [ SiteWeb
    , ArticlePresse
    , ReseauxSociaux
    , URL
    , Autre
    ]


ressourceTypeToString : RessourceType -> String
ressourceTypeToString r =
    case r of
        SiteWeb ->
            "website"

        ArticlePresse ->
            "article"

        ReseauxSociaux ->
            "social"

        URL ->
            "url"

        Autre ->
            "autre"


ressourceTypeToDisplay : RessourceType -> String
ressourceTypeToDisplay r =
    case r of
        SiteWeb ->
            "Site web"

        ArticlePresse ->
            "Article de presse"

        ReseauxSociaux ->
            "Réseaux sociaux"

        URL ->
            "URL"

        Autre ->
            "Autre"


stringToRessourceType : String -> Maybe RessourceType
stringToRessourceType s =
    case s of
        "website" ->
            Just SiteWeb

        "article" ->
            Just ArticlePresse

        "social" ->
            Just ReseauxSociaux

        "url" ->
            Just URL

        "autre" ->
            Just Autre

        _ ->
            Nothing


type RessourceAction
    = NoRessource
    | NewRessource Ressource
    | DeleteRessource Ressource
    | EditRessource Ressource


decodeRessource : Decoder Ressource
decodeRessource =
    Decode.succeed Ressource
        |> andMap (Decode.field "id" Decode.int)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "lien" Decode.string)
        |> andMap (Decode.field "type" <| decodeRessourceType)


decodeRessourceType : Decoder RessourceType
decodeRessourceType =
    Decode.string
        |> Decode.andThen
            (\s ->
                s
                    |> stringToRessourceType
                    |> Maybe.map Decode.succeed
                    |> Maybe.withDefault (Decode.fail <| "RessourceTypeId inconnu : " ++ s)
            )


encodeRessource : Ressource -> Value
encodeRessource ressource =
    [ ( "type", Encode.string <| ressourceTypeToString <| .type_ <| ressource )
    , ( "nom", Encode.string <| .nom <| ressource )
    , ( "lien", Encode.string <| .lien <| ressource )
    ]
        |> Encode.object


type Header
    = HNom
    | HType
    | HLien
    | HBoutons


headerToDisplay : Header -> String
headerToDisplay header =
    case header of
        HType ->
            "Type"

        HNom ->
            "Nom"

        HLien ->
            "Lien"

        HBoutons ->
            ""


toCell : Bool -> (RessourceAction -> msg) -> Header -> Ressource -> Html msg
toCell disabled setRessourceAction header ({ nom, lien, type_ } as ressource) =
    case header of
        HType ->
            text <|
                ressourceTypeToDisplay <|
                    type_

        HNom ->
            text <|
                withEmptyAs "-" <|
                    nom

        HLien ->
            case lien of
                "" ->
                    text "-"

                _ ->
                    span []
                        [ Typo.externalLink lien [ Html.Attributes.style "overflow-wrap" "break-word" ] [ text lien ]
                        , UI.Clipboard.clipboard
                            { content = lien
                            , label = "Copier"
                            , copiedLabel = "Copié\u{00A0}!"
                            , timeoutMilliseconds = 2000
                            }
                        ]

        HBoutons ->
            [ DSFR.Button.new { onClick = Just <| setRessourceAction <| EditRessource <| ressource, label = "Modifier" }
                |> DSFR.Button.withDisabled disabled
                |> DSFR.Button.withAttrs [ class "!mb-0" ]
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
            , DSFR.Button.new { onClick = Just <| setRessourceAction <| DeleteRessource <| ressource, label = "Supprimer" }
                |> DSFR.Button.withDisabled disabled
                |> DSFR.Button.withAttrs [ class "!mb-0" ]
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.onlyIcon DSFR.Icons.System.deleteFill
            ]
                |> DSFR.Button.group
                |> DSFR.Button.inline
                |> DSFR.Button.alignedRight
                |> DSFR.Button.viewGroup


viewRessources : (RessourceAction -> msg) -> RessourceAction -> List Ressource -> Html msg
viewRessources =
    viewRessourcesHelper False


viewRessourcesHelper : Bool -> (RessourceAction -> msg) -> RessourceAction -> List Ressource -> Html msg
viewRessourcesHelper disabled setRessourceAction ressourceAction ressources =
    div [ class "flex flex-col gap-4" ]
        [ div [ class "flex flex-row justify-between" ]
            [ h3 [ Typo.fr_h4, class "flex flex-row items-center gap-2" ]
                [ text "Ressources"
                , viewIf (List.length ressources > 0) <|
                    span [ Typo.textMd, class "circled !mb-0" ]
                        [ text <| Lib.UI.formatIntWithThousandSpacing <| List.length ressources
                        ]
                ]
            , DSFR.Button.new { label = "Ajouter une ressource", onClick = Just <| setRessourceAction <| NewRessource emptyRessource }
                |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withDisabled (disabled || ressourceAction /= NoRessource)
                |> DSFR.Button.view
            ]
        , viewIf (List.length ressources == 0) <|
            span [ class "italic" ]
                [ text "Aucune ressource pour l'instant" ]
        , viewIf (List.length ressources > 0)
            (DSFR.Table.table
                { id = "tableau-ressources"
                , caption = text "Ressources"
                , headers =
                    [ HType
                    , HNom
                    , HLien
                    , HBoutons
                    ]
                , rows = ressources
                , toHeader = headerToDisplay >> text >> List.singleton >> span []
                , toRowId = .id >> String.fromInt
                , toCell = toCell disabled setRessourceAction
                }
                |> DSFR.Table.withContainerAttrs [ class "!mb-0" ]
                |> DSFR.Table.noBorders
                |> DSFR.Table.captionHidden
                |> DSFR.Table.fixed
                |> DSFR.Table.view
            )
        ]


updateRessource : RessourceField -> String -> Ressource -> Ressource
updateRessource field value ressource =
    case field of
        RType ->
            { ressource
                | type_ =
                    value
                        |> stringToRessourceType
                        |> Maybe.withDefault ressource.type_
            }

        RNom ->
            { ressource | nom = value }

        RLien ->
            { ressource | lien = value }
