module Data.Echange exposing (Echange(..), EchangeData, EchangeId, TypeEchange(..), auteur, createEchange, date, decodeEchange, decodeTypeEchange, demandes, description, encodeTypeEchange, entite, id, modification, stringToTypeEchange, titre, typeEchangeToString, type_)

import Api.EntityId exposing (EntityId)
import Data.Demande exposing (DemandeId)
import Data.Entite exposing (Entite, decodeEntite)
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode exposing (Value)
import Lib.Date
import Lib.UI exposing (displayPrenomNomPoint)
import Time


type Echange
    = Echange EchangeData


type EchangeId
    = EchangeId


type alias EchangeData =
    { id : EntityId EchangeId
    , typeId : TypeEchange
    , nom : String
    , description : String
    , date : Date
    , auteur : String
    , modificationAuteurDate : Maybe ( String, Time.Posix )
    , demandes : List (EntityId DemandeId)
    , entite : Maybe Entite
    }


type TypeEchange
    = Telephone
    | Email
    | Rencontre
    | Courrier


id : Echange -> EntityId EchangeId
id (Echange data) =
    data.id


type_ : Echange -> TypeEchange
type_ (Echange data) =
    data.typeId


titre : Echange -> String
titre (Echange data) =
    data.nom


description : Echange -> String
description (Echange data) =
    data.description


date : Echange -> Date
date (Echange data) =
    data.date


auteur : Echange -> String
auteur (Echange data) =
    data.auteur


modification : Echange -> Maybe ( String, Time.Posix )
modification (Echange data) =
    data.modificationAuteurDate


demandes : Echange -> List (EntityId DemandeId)
demandes (Echange data) =
    data.demandes


entite : Echange -> Maybe Entite
entite (Echange data) =
    data.entite


encodeTypeEchange : TypeEchange -> Value
encodeTypeEchange =
    typeEchangeToString >> Encode.string


typeEchangeToString : TypeEchange -> String
typeEchangeToString t =
    case t of
        Telephone ->
            "telephone"

        Email ->
            "email"

        Rencontre ->
            "rencontre"

        Courrier ->
            "courrier"


stringToTypeEchange : String -> Maybe TypeEchange
stringToTypeEchange s =
    case s of
        "telephone" ->
            Just Telephone

        "email" ->
            Just Email

        "rencontre" ->
            Just Rencontre

        "courrier" ->
            Just Courrier

        _ ->
            Nothing


decodeTypeEchange : Decoder TypeEchange
decodeTypeEchange =
    Decode.string
        |> Decode.andThen
            (\s ->
                s
                    |> stringToTypeEchange
                    |> Maybe.map Decode.succeed
                    |> Maybe.withDefault (Decode.fail <| "Unknown typeEchange " ++ s)
            )


decodeEchange : Decoder Echange
decodeEchange =
    Decode.succeed EchangeData
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap (Decode.field "typeId" decodeTypeEchange)
        |> andMap (Decode.map (Maybe.withDefault "Premier échange") <| Decode.field "nom" <| Decode.nullable Decode.string)
        |> andMap (Decode.field "description" Decode.string)
        |> andMap (Decode.field "date" Lib.Date.decodeCalendarDate)
        |> andMap
            (Decode.field "deveco" <|
                Decode.field "compte" <|
                    Decode.map2 (\p n -> displayPrenomNomPoint { prenom = p, nom = n })
                        (Decode.map (Maybe.withDefault "?") <| Decode.field "prenom" <| Decode.nullable Decode.string)
                        (Decode.map (Maybe.withDefault "?") <| Decode.field "nom" <| Decode.nullable Decode.string)
            )
        |> andMap
            (Decode.field "modification" <|
                Decode.nullable <|
                    Decode.map2 Tuple.pair
                        (Decode.field "compte" <|
                            Decode.map2 (\prenom nom -> prenom ++ " " ++ nom)
                                (Decode.field "prenom" Decode.string)
                                (Decode.field "nom" Decode.string)
                        )
                        (Decode.field "date" Lib.Date.decodeDateWithTimeFromISOString)
            )
        |> andMap (Decode.field "demandes" <| Decode.list <| Decode.field "demandeId" Api.EntityId.decodeEntityId)
        |> andMap (Decode.oneOf [ Decode.map Just <| decodeEntite, Decode.succeed Nothing ])
        |> Decode.map Echange


createEchange : { id : EntityId EchangeId, type_ : TypeEchange, titre : String, compteRendu : String, date : Date, demandes : List (EntityId DemandeId), entite : Maybe Entite } -> Echange
createEchange r =
    Echange <| EchangeData r.id r.type_ r.titre r.compteRendu r.date "" Nothing r.demandes r.entite
