module Data.Collegues exposing (Collegue, decodeCollegue)

import Api.EntityId exposing (EntityId)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import User exposing (UserId)


type alias Collegue =
    { id : EntityId UserId
    , prenom : String
    , nom : String
    , email : String
    , actif : Bool
    }


decodeCollegue : Decoder Collegue
decodeCollegue =
    Decode.succeed Collegue
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap (Decode.field "prenom" Decode.string)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "email" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault False) <| optionalNullableField "actif" Decode.bool)
