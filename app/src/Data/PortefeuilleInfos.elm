module Data.PortefeuilleInfos exposing (PortefeuilleInfos, decodePortefeuilleInfos)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)


type alias PortefeuilleInfos =
    { contacts : Bool
    , echanges : Bool
    , demandes : Bool
    , rappels : Bool
    , favori : Bool
    }


decodePortefeuilleInfos : Decoder PortefeuilleInfos
decodePortefeuilleInfos =
    Decode.succeed PortefeuilleInfos
        |> andMap (Decode.field "contacts" Decode.bool)
        |> andMap (Decode.field "echanges" Decode.bool)
        |> andMap (Decode.field "demandes" Decode.bool)
        |> andMap (Decode.field "rappels" Decode.bool)
        |> andMap (Decode.field "favori" Decode.bool)
