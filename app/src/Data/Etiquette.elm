module Data.Etiquette exposing (Etiquette, EtiquetteAvecUsages, EtiquetteId, Etiquettes, EtiquettesAvecUsages, decodeEtiquetteList, decodeEtiquettes, decodeEtiquettesAvecUsages)

import Api.EntityId exposing (EntityId, decodeEntityId)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)


type EtiquetteId
    = EtiquetteId


type alias Etiquette =
    String


type alias Etiquettes =
    { activites : List Etiquette
    , localisations : List Etiquette
    , motsCles : List Etiquette
    }


type alias EtiquetteAvecUsages =
    { id : EntityId EtiquetteId
    , label : String
    , usages : Int
    }


type alias EtiquettesAvecUsages =
    { activites : List EtiquetteAvecUsages
    , localisations : List EtiquetteAvecUsages
    , motsCles : List EtiquetteAvecUsages
    }


decodeEtiquetteList : Decoder (List String)
decodeEtiquetteList =
    Decode.field "elements" <|
        Decode.list <|
            Decode.field "nom" Decode.string


decodeEtiquettes : Decoder Etiquettes
decodeEtiquettes =
    Decode.succeed Etiquettes
        |> andMap (Decode.field "activites" <| Decode.list <| Decode.field "nom" Decode.string)
        |> andMap (Decode.field "localisations" <| Decode.list <| Decode.field "nom" Decode.string)
        |> andMap (Decode.field "motsCles" <| Decode.list <| Decode.field "nom" Decode.string)


decodeEtiquettesAvecUsages : Decoder EtiquettesAvecUsages
decodeEtiquettesAvecUsages =
    Decode.succeed EtiquettesAvecUsages
        |> andMap (Decode.field "activites" <| Decode.list decodeEtiquetteAvecUsages)
        |> andMap (Decode.field "localisations" <| Decode.list decodeEtiquetteAvecUsages)
        |> andMap (Decode.field "motsCles" <| Decode.list decodeEtiquetteAvecUsages)


decodeEtiquetteAvecUsages : Decoder EtiquetteAvecUsages
decodeEtiquetteAvecUsages =
    Decode.succeed EtiquetteAvecUsages
        |> andMap (Decode.field "id" <| decodeEntityId)
        |> andMap (Decode.field "nom" <| Decode.string)
        |> andMap (Decode.field "usages" <| Decode.int)
