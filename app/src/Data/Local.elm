module Data.Local exposing
    ( Contribution
    , Local
    , LocalApercu
    , LocalSimple
    , Mutation
    , ProprietairePersonneMorale
    , ProprietairePersonnePhysique
    , badgeVacance
    , bailTypeList
    , bailTypeToDisplay
    , bailTypeToString
    , decodeContribution
    , decodeLocal
    , decodeLocalApercu
    , decodeLocalSimple
    , stringToBailType
    , stringToVacanceMotif
    , stringToVacanceType
    , vacanceMotifList
    , vacanceMotifToDisplay
    , vacanceMotifToString
    , vacanceToDisplay
    , vacanceToString
    , vacanceTypeList
    , vacanceTypeToDisplay
    , vacanceTypeToString
    )

import Accessibility exposing (Html, text)
import DSFR.Badge
import DSFR.Color
import Data.Adresse exposing (Adresse, decodeAdresse, decodeCoordinates)
import Data.Etablissement exposing (EtablissementApercu, decodeEtablissementApercu)
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Lib.Date


type alias LocalSimple =
    { id : String
    , invariant : String
    , nom : String
    , adresse : Adresse
    , geolocalisation : Maybe ( Float, Float )
    , niveau : String
    , surfaceTotale : String
    , occupe : Maybe Bool
    , categorie : Maybe LocalCategorieType
    , actif : Bool
    , parcelle : String
    , section : String
    , geolocalisationContribution : Maybe ( Float, Float )
    , geolocalisationEquipe : Maybe String
    }


type alias Local =
    { id : String
    , invariant : String
    , nom : String
    , adresse : Adresse
    , geolocalisation : Maybe ( Float, Float )
    , niveau : String
    , surfaceTotale : String
    , occupe : Maybe Bool
    , categorie : Maybe LocalCategorieType
    , actif : Bool
    , parcelle : String
    , section : String
    , surfaceVente : Int
    , surfaceReserve : Int
    , surfaceExterieureNonCouverte : Int
    , surfaceStationnementNonCouverte : Int
    , surfaceStationnementCouverte : Int
    , nature : LocalNatureType
    , proprietairesPersonnesPhysiques : List ProprietairePersonnePhysique
    , proprietairesPersonnesMorales : List ProprietairePersonneMorale
    , occupants : List EtablissementApercu
    , mutations : List Mutation
    }


type alias LocalApercu =
    { id : String
    , nom : String
    , adresse : Adresse
    }


type alias Contribution =
    { loyer : String
    , bailType : Maybe BailType
    , vacanceType : Maybe VacanceType
    , vacanceMotif : Maybe VacanceMotif
    , remembre : Bool
    , fonds : String
    , vente : String
    , commentaires : String
    }


type alias LocalCategorieType =
    { id : String
    , nom : String
    }


type alias LocalNatureType =
    { id : String
    , nom : String
    }


type BailType
    = Commercial
    | Derogatoire
    | Precaire
    | Professionnel


bailTypeList : List BailType
bailTypeList =
    [ Commercial
    , Derogatoire
    , Precaire
    , Professionnel
    ]


type VacanceType
    = Conjoncturelle
    | Classique
    | Structurelle
    | Autre


vacanceTypeList : List VacanceType
vacanceTypeList =
    [ Conjoncturelle
    , Classique
    , Structurelle
    , Autre
    ]


type VacanceMotif
    = Choix
    | Commercialisation
    | Peril
    | Travaux
    | CessationActivite


vacanceMotifList : List VacanceMotif
vacanceMotifList =
    [ Choix
    , Commercialisation
    , Peril
    , Travaux
    , CessationActivite
    ]


type alias ProprietairePersonnePhysique =
    { nom : Maybe String
    , prenom : Maybe String
    , naissanceNom : Maybe String
    , naissanceDate : Maybe Date
    , naissanceLieu : Maybe String
    , prenoms : Maybe String
    , proprietaireCompteId : String
    }


type alias ProprietairePersonneMorale =
    { nom : String
    , siren : String
    , siretSiege : Maybe String
    , proprietaireCompteId : String
    }


type alias Mutation =
    { date : Date
    , valeur : Maybe Float
    , type_ : String
    }


decodeLocalSimple : Decoder LocalSimple
decodeLocalSimple =
    Decode.succeed LocalSimple
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "invariant" Decode.string)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "adresse" decodeAdresse)
        |> andMap (Decode.field "adresse" <| decodeAdresseGeolocalisation)
        |> andMap (Decode.field "niveau" Decode.string)
        |> andMap (Decode.field "surfaceTotale" Decode.string)
        |> andMap (Decode.field "occupe" <| Decode.nullable Decode.bool)
        |> andMap (Decode.field "categorieType" <| Decode.nullable <| decodeLocalCategorieType)
        |> andMap (Decode.field "actif" <| Decode.bool)
        |> andMap (Decode.field "parcelle" Decode.string)
        |> andMap (Decode.field "section" Decode.string)
        |> andMap (Decode.maybe <| Decode.field "geolocalisationContribution" decodeCoordinates)
        |> andMap (optionalNullableField "geolocalisationEquipe" <| Decode.string)


decodeLocal : Decoder Local
decodeLocal =
    Decode.succeed Local
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "invariant" Decode.string)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "adresse" decodeAdresse)
        |> andMap (Decode.field "adresse" <| decodeAdresseGeolocalisation)
        |> andMap (Decode.field "niveau" Decode.string)
        |> andMap (Decode.field "surfaceTotale" Decode.string)
        |> andMap (Decode.field "occupe" <| Decode.nullable Decode.bool)
        |> andMap (Decode.field "categorieType" <| Decode.nullable <| decodeLocalCategorieType)
        |> andMap (Decode.field "actif" <| Decode.bool)
        |> andMap (Decode.field "parcelle" Decode.string)
        |> andMap (Decode.field "section" Decode.string)
        |> andMap (Decode.field "surfaceVente" Decode.int)
        |> andMap (Decode.field "surfaceReserve" Decode.int)
        |> andMap (Decode.field "surfaceExterieureNonCouverte" Decode.int)
        |> andMap (Decode.field "surfaceStationnementNonCouvert" Decode.int)
        |> andMap (Decode.field "surfaceStationnementCouvert" Decode.int)
        |> andMap (Decode.field "natureType" decodeLocalNatureType)
        |> andMap (Decode.field "propPPhys" <| Decode.list <| decodeProprietairePersonnePhysique)
        |> andMap (Decode.field "propPMors" <| Decode.list <| decodeProprietairePersonneMorale)
        |> andMap (Decode.field "occupants" <| Decode.list <| decodeEtablissementApercu)
        |> andMap (Decode.field "mutations" <| Decode.list <| decodeMutation)


decodeLocalApercu : Decoder LocalApercu
decodeLocalApercu =
    Decode.succeed LocalApercu
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "nomAffichage" Decode.string)
        |> andMap (Decode.field "adresse1" decodeAdresse)


decodeContribution : Decoder Contribution
decodeContribution =
    Decode.succeed Contribution
        |> andMap (Decode.field "loyer" <| Decode.map (Maybe.withDefault "") <| Decode.nullable Decode.string)
        |> andMap (Decode.field "bailType" <| Decode.nullable decodeBailType)
        |> andMap (Decode.field "vacanceType" <| Decode.nullable decodeVacanceType)
        |> andMap (Decode.field "vacanceMotif" <| Decode.nullable decodeVacanceMotif)
        |> andMap (Decode.field "remembre" <| Decode.bool)
        |> andMap (Decode.field "fonds" <| Decode.map (Maybe.withDefault "") <| Decode.nullable Decode.string)
        |> andMap (Decode.field "vente" <| Decode.map (Maybe.withDefault "") <| Decode.nullable Decode.string)
        |> andMap (Decode.field "commentaires" <| Decode.map (Maybe.withDefault "") <| Decode.nullable Decode.string)


decodeLocalCategorieType : Decoder LocalCategorieType
decodeLocalCategorieType =
    Decode.succeed LocalCategorieType
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "nom" Decode.string)


decodeLocalNatureType : Decoder LocalNatureType
decodeLocalNatureType =
    Decode.succeed LocalNatureType
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "nom" Decode.string)


decodeAdresseGeolocalisation : Decoder (Maybe ( Float, Float ))
decodeAdresseGeolocalisation =
    Decode.map .geolocalisation <|
        decodeAdresse


decodeProprietairePersonnePhysique : Decoder ProprietairePersonnePhysique
decodeProprietairePersonnePhysique =
    Decode.succeed ProprietairePersonnePhysique
        |> andMap (Decode.field "nom" <| Decode.nullable Decode.string)
        |> andMap (Decode.field "prenom" <| Decode.nullable Decode.string)
        |> andMap (Decode.field "naissanceNom" <| Decode.nullable Decode.string)
        |> andMap (Decode.field "naissanceDate" <| Decode.nullable <| Lib.Date.decodeCalendarDate)
        |> andMap (Decode.field "naissanceLieu" <| Decode.nullable Decode.string)
        |> andMap (Decode.field "prenoms" <| Decode.nullable <| Decode.string)
        |> andMap (Decode.field "proprietaireCompteId" Decode.string)


decodeProprietairePersonneMorale : Decoder ProprietairePersonneMorale
decodeProprietairePersonneMorale =
    Decode.succeed ProprietairePersonneMorale
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "entrepriseId" Decode.string)
        |> andMap (optionalNullableField "siegeEtablissementId" Decode.string)
        |> andMap (Decode.field "proprietaireCompteId" Decode.string)


decodeMutation : Decoder Mutation
decodeMutation =
    Decode.succeed Mutation
        |> andMap (Decode.field "date" Lib.Date.decodeCalendarDate)
        |> andMap (Decode.field "valeur" <| Decode.nullable Decode.float)
        |> andMap (Decode.at [ "natureType", "nom" ] Decode.string)


badgeVacance : Maybe Bool -> Html msg
badgeVacance occupe =
    case occupe of
        Nothing ->
            text "\u{00A0}"

        Just o ->
            o
                |> vacanceToDisplay
                |> text
                |> DSFR.Badge.default
                |> DSFR.Badge.withColor (vacanceToColor o)
                |> DSFR.Badge.badgeMD


vacanceToDisplay : Bool -> String
vacanceToDisplay vacance =
    if vacance then
        "Occupé"

    else
        "Vacant"


vacanceToString : Bool -> String
vacanceToString occupe =
    if occupe then
        "occupe"

    else
        "vacant"


vacanceToColor : Bool -> DSFR.Color.CustomColor
vacanceToColor occupe =
    if occupe then
        DSFR.Color.blueCumulus

    else
        DSFR.Color.purpleGlycine


bailTypeToString : BailType -> String
bailTypeToString bailType =
    case bailType of
        Commercial ->
            "commercial"

        Derogatoire ->
            "derogatoire"

        Professionnel ->
            "professionnel"

        Precaire ->
            "precaire"


bailTypeToDisplay : BailType -> String
bailTypeToDisplay bailType =
    case bailType of
        Commercial ->
            "Commercial"

        Derogatoire ->
            "Dérogatoire"

        Professionnel ->
            "Professionnel"

        Precaire ->
            "Précaire"


stringToBailType : String -> Maybe BailType
stringToBailType string =
    case string of
        "commercial" ->
            Just Commercial

        "derogatoire" ->
            Just Derogatoire

        "professionnel" ->
            Just Professionnel

        "precaire" ->
            Just Precaire

        _ ->
            Nothing


decodeBailType : Decoder BailType
decodeBailType =
    Decode.string
        |> Decode.andThen
            (\s ->
                s
                    |> stringToBailType
                    |> Maybe.map Decode.succeed
                    |> Maybe.withDefault (Decode.fail <| "Type de bail inconnu\u{00A0}: " ++ s)
            )


vacanceTypeToString : VacanceType -> String
vacanceTypeToString vacanceType =
    case vacanceType of
        Conjoncturelle ->
            "conjoncturelle"

        Classique ->
            "classique"

        Structurelle ->
            "structurelle"

        Autre ->
            "autre"


vacanceTypeToDisplay : VacanceType -> String
vacanceTypeToDisplay vacanceType =
    case vacanceType of
        Conjoncturelle ->
            "Conjoncturelle"

        Classique ->
            "Classique"

        Structurelle ->
            "Structurelle"

        Autre ->
            "Autre"


stringToVacanceType : String -> Maybe VacanceType
stringToVacanceType string =
    case string of
        "conjoncturelle" ->
            Just Conjoncturelle

        "classique" ->
            Just Classique

        "structurelle" ->
            Just Structurelle

        "autre" ->
            Just Autre

        _ ->
            Nothing


decodeVacanceType : Decoder VacanceType
decodeVacanceType =
    Decode.string
        |> Decode.andThen
            (\s ->
                s
                    |> stringToVacanceType
                    |> Maybe.map Decode.succeed
                    |> Maybe.withDefault (Decode.fail <| "Type de vacance inconnu\u{00A0}: " ++ s)
            )


vacanceMotifToString : VacanceMotif -> String
vacanceMotifToString vacanceMotif =
    case vacanceMotif of
        Choix ->
            "choix"

        Commercialisation ->
            "commercialisation"

        Peril ->
            "peril"

        Travaux ->
            "travaux"

        CessationActivite ->
            "cessation"


vacanceMotifToDisplay : VacanceMotif -> String
vacanceMotifToDisplay vacanceMotif =
    case vacanceMotif of
        Choix ->
            "Choix"

        Commercialisation ->
            "Commercialisation"

        Peril ->
            "Péril"

        Travaux ->
            "Travaux"

        CessationActivite ->
            "Cessation d'activité"


stringToVacanceMotif : String -> Maybe VacanceMotif
stringToVacanceMotif string =
    case string of
        "choix" ->
            Just Choix

        "commercialisation" ->
            Just Commercialisation

        "peril" ->
            Just Peril

        "travaux" ->
            Just Travaux

        "cessation" ->
            Just CessationActivite

        _ ->
            Nothing


decodeVacanceMotif : Decoder VacanceMotif
decodeVacanceMotif =
    Decode.string
        |> Decode.andThen
            (\s ->
                s
                    |> stringToVacanceMotif
                    |> Maybe.map Decode.succeed
                    |> Maybe.withDefault (Decode.fail <| "Motif de vacance inconnu\u{00A0}: " ++ s)
            )
