module Data.Equipe exposing (Equipe, EquipeId, EquipeLight, decodeEquipe, decodeEquipeLight, equipeType, id, nom, siret, territoireCategorie, territoireNom, territoireType)

import Api.EntityId exposing (EntityId)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)


type EquipeId
    = EquipeId


id : { equipe | id : EntityId EquipeId } -> EntityId EquipeId
id data =
    data.id


nom : { equipe | nom : String } -> String
nom data =
    data.nom


equipeType : Equipe -> String
equipeType data =
    data.equipeType


territoireType : Equipe -> String
territoireType data =
    data.territoireType


territoireNom : Equipe -> String
territoireNom data =
    data.territoireNom


territoireCategorie : Equipe -> String
territoireCategorie data =
    data.territoireCategorie


siret : { equipe | siret : String } -> String
siret data =
    data.siret


type alias Equipe =
    { id : EntityId EquipeId
    , nom : String
    , groupement : Bool
    , territoireType : String
    , equipeType : String
    , territoireCategorie : String
    , territoireNom : String
    , siret : String
    }


type alias EquipeLight =
    { id : EntityId EquipeId
    , nom : String
    , isFrance : Bool
    , hasCarto : Bool
    }


decodeEquipe : Decoder Equipe
decodeEquipe =
    Decode.succeed Equipe
        |> andMap (Decode.field "id" <| Api.EntityId.decodeEntityId)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "groupement" Decode.bool)
        |> andMap (Decode.at [ "territoireType", "nom" ] Decode.string)
        |> andMap (Decode.at [ "type", "nom" ] Decode.string)
        |> andMap (Decode.field "territoireCategorie" Decode.string)
        |> andMap (Decode.field "territoireNom" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.field "siret" <| Decode.maybe Decode.string)


decodeEquipeLight : Decoder EquipeLight
decodeEquipeLight =
    Decode.succeed EquipeLight
        |> andMap (Decode.field "id" <| Api.EntityId.decodeEntityId)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault False) <| optionalNullableField "isFrance" Decode.bool)
        |> andMap (Decode.map (Maybe.withDefault False) <| optionalNullableField "hasCarto" Decode.bool)
