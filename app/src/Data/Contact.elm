module Data.Contact exposing (Contact, ContactInputType(..), EntiteLien(..), FonctionContact(..), decodeContact, decodeFonctionContact, encodeContact, encodeEntiteLien)

import Api.EntityId exposing (EntityId, decodeEntityId)
import Data.EtablissementCreationId exposing (EtablissementCreationId)
import Data.Personne exposing (Personne, decodePersonne, encodePersonne)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode exposing (Value)
import Lib.UI exposing (encodeSpaceToEmptyString)


type alias Contact =
    { fonction : Maybe EntiteLien
    , personne : Personne
    , liens : List FonctionContact
    }


type EntiteLien
    = FonctionEtablissement { fonction : String }
    | FonctionLocal { fonction : String }
    | FonctionEtablissementCreation { fonction : String }
    | FonctionCreateur
        { fonction : String
        , niveauDiplome : String
        , situationProfessionnelle : String
        }


encodeEntiteLien : EntiteLien -> List ( String, Value )
encodeEntiteLien fonctionEntite =
    case fonctionEntite of
        FonctionEtablissement { fonction } ->
            [ ( "fonction", Encode.string fonction )
            ]

        FonctionLocal { fonction } ->
            [ ( "fonction", Encode.string fonction )
            ]

        FonctionEtablissementCreation { fonction } ->
            [ ( "fonction", Encode.string fonction )
            ]

        FonctionCreateur { fonction, niveauDiplome, situationProfessionnelle } ->
            [ ( "fonction", Encode.string fonction )
            , ( "niveauDiplome"
              , Encode.string <|
                    encodeSpaceToEmptyString <|
                        niveauDiplome
              )
            , ( "situationProfessionnelle"
              , Encode.string <|
                    encodeSpaceToEmptyString <|
                        situationProfessionnelle
              )
            ]


decodeEntiteLien : Decoder EntiteLien
decodeEntiteLien =
    Decode.field "type" Decode.string
        |> Decode.andThen
            (\type_ ->
                case type_ of
                    "etablissement" ->
                        Decode.field "fonction" Decode.string
                            |> Decode.map (\f -> { fonction = f })
                            |> Decode.map FonctionEtablissement

                    "createur" ->
                        Decode.map FonctionCreateur <|
                            Decode.map3 (\f n s -> { fonction = f, niveauDiplome = n, situationProfessionnelle = s })
                                (Decode.field "fonction" Decode.string)
                                (Decode.field "niveauDiplome" Decode.string)
                                (Decode.field "situationProfessionnelle" Decode.string)

                    "local" ->
                        Decode.field "fonction" Decode.string
                            |> Decode.map (\f -> { fonction = f })
                            |> Decode.map FonctionLocal

                    _ ->
                        Decode.fail <| "Entite inconnue : " ++ type_
            )


type alias EtablissementData =
    { etablissementId : String
    , etablissementNom : String
    , fonction : String
    }


type alias LocalData =
    { localId : String
    , localNom : Maybe String
    , fonction : String
    }


type alias CreateurData =
    { etablissementCreationId : EntityId EtablissementCreationId
    , etablissementCreationNom : String
    }


type FonctionContact
    = CEtablissement EtablissementData
    | CCreateur CreateurData
    | CLocal LocalData


decodeFonctionContact : Decoder FonctionContact
decodeFonctionContact =
    Decode.field "type" Decode.string
        |> Decode.andThen
            (\type_ ->
                case type_ of
                    "etablissement" ->
                        Decode.succeed EtablissementData
                            |> andMap (Decode.field "etablissementId" Decode.string)
                            |> andMap
                                (Decode.map (Maybe.withDefault "?") <|
                                    Decode.field "etablissementNom" <|
                                        Decode.nullable Decode.string
                                )
                            |> andMap (Decode.field "fonction" Decode.string)
                            |> Decode.map CEtablissement

                    "createur" ->
                        Decode.succeed CreateurData
                            |> andMap (Decode.field "etablissementCreationId" decodeEntityId)
                            |> andMap (Decode.field "etablissementCreationNom" Decode.string)
                            |> Decode.map CCreateur

                    "local" ->
                        Decode.succeed LocalData
                            |> andMap (Decode.field "localId" Decode.string)
                            |> andMap
                                (Decode.field "localNom" <|
                                    Decode.nullable Decode.string
                                )
                            |> andMap (Decode.field "fonction" Decode.string)
                            |> Decode.map CLocal

                    _ ->
                        Decode.fail <| "type inconnu : " ++ type_
            )


decodeContact : Decoder Contact
decodeContact =
    Decode.succeed Contact
        |> andMap (optionalNullableField "lien" decodeEntiteLien)
        |> andMap decodePersonne
        |> andMap (Decode.field "liens" <| Decode.list <| decodeFonctionContact)


type ContactInputType
    = ContactAddInput
    | ContactUpdateInput


encodeContact :
    ContactInputType
    ->
        { contact
            | fonction : Maybe EntiteLien
            , personne : Personne
        }
    -> Value
encodeContact type_ { fonction, personne } =
    [ ( "contact"
      , encodePersonne personne
      )
    , ( "type"
      , Encode.string <|
            case type_ of
                ContactAddInput ->
                    "nouveau"

                ContactUpdateInput ->
                    ""
      )
    ]
        ++ (fonction
                |> Maybe.map encodeEntiteLien
                |> Maybe.withDefault []
           )
        |> Encode.object
