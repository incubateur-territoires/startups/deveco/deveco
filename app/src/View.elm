module View exposing (View, defaultView, map, mappers, toDocument)

import Accessibility as Html exposing (Html)
import Browser
import Html.Attributes
import Route exposing (Route)
import Shared exposing (Shared)
import Spa
import UI.Layout


type alias View msg =
    { title : String
    , body : List (Html msg)
    , route : Route
    }


defaultView : View msg
defaultView =
    { title = "Outil pour développeur économique"
    , body =
        List.singleton <|
            Html.div [ Html.Attributes.class "p-2" ] <|
                List.singleton <|
                    Html.text "Quelque chose est allé de travers, veuillez nous contacter si cela se reproduit\u{00A0}!"
    , route = Route.NotFound ""
    }


map : (a -> b) -> View a -> View b
map fn { title, body, route } =
    { title = title
    , body = List.map (Html.map fn) body
    , route = route
    }


toDocument : Shared -> View (Spa.Msg Shared.Msg msg) -> Browser.Document (Spa.Msg Shared.Msg msg)
toDocument shared view =
    { title = view.title
    , body =
        view.body
            |> UI.Layout.layout shared view.route
    }


mappers : ( (a -> b) -> View a -> View b, (c -> d) -> View c -> View d )
mappers =
    ( map, map )
