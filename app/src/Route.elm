module Route exposing (Route(..), defaultPageForRole, matchAccessibilite, matchActivite, matchAdminEquipeDemandes, matchAdminEquipes, matchAdminEtiquettes, matchAdminUtilisateurs, matchAnalysis, matchAnnuaire, matchAuthCheck, matchAuthJwtUuid, matchAyantDroit, matchCollegue, matchConnexion, matchCreateurEditId, matchCreateurId, matchCreateurNew, matchCreateurs, matchDashboard, matchDeconnexion, matchEtablissementSiret, matchEtablissements, matchEtiquettes, matchEvenementSiret, matchImport, matchLocalId, matchLocaux, matchNotFound, matchNotifications, matchProfil, matchRappels, matchSIG, matchStats, toRoute, toUrl)

import Api.EntityId exposing (EntityId)
import Data.Etablissement exposing (Siret)
import Data.EtablissementCreationId exposing (EtablissementCreationId)
import Data.Role
import Regex
import Url exposing (Url)
import Url.Builder as Builder
import Url.Parser exposing ((</>), (<?>), Parser, map, oneOf, parse, s, string, top)
import Url.Parser.Query as Query


type Route
    = Connexion (Maybe String)
    | Etablissements (Maybe String)
    | Etablissement ( Maybe String, String )
    | Createurs (Maybe String)
    | Createur ( Maybe String, EntityId EtablissementCreationId )
    | CreateurEdit (EntityId EtablissementCreationId)
    | CreateurNew
    | Suivis (Maybe String)
    | ParamsProfil
    | ParamsSIG
    | ParamsEtiquettes
    | ParamsImport
    | ParamsCollegues
    | ParamsNotifications
    | Dashboard (Maybe String)
    | Activite (Maybe String)
    | Stats (Maybe String)
    | AdminUtilisateurs (Maybe String)
    | AdminEquipes
    | AdminEquipeDemandes
    | AyantDroit String
    | AdminEtiquettes
    | Deconnexion
    | Locaux (Maybe String)
    | Local ( Maybe String, String )
      -- | LocalEdit (EntityId LocalId)
      -- | LocalNew
    | AuthJwtUuid String (Maybe String)
    | AuthCheck (Maybe String)
    | NotFound String
    | Analysis
    | Accessibilite
    | Annuaire (Maybe String)
    | Evenements Siret


route : Url -> Parser (Route -> a) a
route url =
    oneOf
        [ map (Dashboard url.query) <| top
        , map (Dashboard url.query) <| s "accueil"
        , map Connexion <| s "connexion" <?> Query.string "url"
        , map (Etablissements url.query) <| s etablissementsPrefix
        , map (Etablissement << Tuple.pair url.query) <| s etablissementsPrefix </> string
        , map Evenements <| s "evenements" </> s "etablissement" </> string
        , map CreateurNew <| s createursPrefix </> s "nouveau"
        , map CreateurNew <| s createursPrefix </> s "creer"
        , map (Createurs url.query) <| s createursPrefix
        , map CreateurEdit <| s createursPrefix </> Api.EntityId.entityIdParser </> s "modifier"
        , map (Createur << Tuple.pair url.query) <| s createursPrefix </> Api.EntityId.entityIdParser
        , map (Createur << Tuple.pair url.query) <| s "etablissement-creations" </> Api.EntityId.entityIdParser
        , map (Suivis url.query) <| s "suivis"
        , map (Suivis url.query) <| s "rappels"
        , map (Suivis url.query) <| s "actions"
        , map ParamsProfil <| s "profil"
        , map ParamsCollegues <| s "parametres" </> s "collaborateurs"
        , map ParamsCollegues <| s "parametres" </> s "collegues"
        , map ParamsProfil <| s "parametres" </> s "profil"
        , map ParamsSIG <| s "parametres" </> s "sig"
        , map ParamsEtiquettes <| s "parametres" </> s "territoire"
        , map ParamsImport <| s "parametres" </> s "import"
        , map ParamsNotifications <| s "parametres" </> s "notifications"
        , map (AdminUtilisateurs url.query) <| s "admin" </> s "utilisateurs"
        , map AdminEquipes <| s "admin" </> s "territoires" -- rétrocompatibilité
        , map AdminEquipeDemandes <| s "admin" </> s "equipes" </> s "demandes"
        , map AyantDroit <| s "autorisation" </> string
        , map AdminEquipes <| s "admin" </> s "equipes"
        , map AdminEtiquettes <| s "admin" </> s "tags"
        , map (Locaux url.query) <| s locauxPrefix

        -- , map LocalNew <| s locauxPrefix </> s "creer"
        -- , map LocalEdit <| s locauxPrefix </> Api.EntityId.entityIdParser </> s "modifier"
        , map (Local << Tuple.pair url.query) <| s locauxPrefix </> string
        , map Deconnexion <| s "deconnexion"
        , map AuthJwtUuid <| s "auth" </> s "jwt" </> string <?> Query.string "url"
        , map AuthCheck <| s "auth" </> s "check" <?> Query.string "url"
        , map (Stats url.query) <| s "stats"
        , map Analysis <| s "stats" </> s "analyse"
        , map (Annuaire url.query) <| s "annuaire"
        , map (Activite url.query) <| s "stats" </> s "activite"
        , map Accessibilite <| s "accessibilite"
        ]


etablissementsPrefix : String
etablissementsPrefix =
    "etablissements"


createursPrefix : String
createursPrefix =
    "createurs"


locauxPrefix : String
locauxPrefix =
    "locaux"


toRoute : Url -> Route
toRoute url =
    url
        |> parse (route url)
        |> Maybe.withDefault (NotFound <| Url.toString url)


toUrl : Route -> String
toUrl r =
    case r of
        Dashboard query ->
            Builder.absolute [ "accueil" ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        Connexion redirect ->
            Builder.absolute [ "connexion" ] <|
                List.filterMap identity <|
                    [ redirect
                        |> Maybe.map (Builder.string "url")
                    ]

        Createurs query ->
            Builder.absolute [ createursPrefix ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        CreateurNew ->
            Builder.absolute [ createursPrefix, "nouveau" ] []

        CreateurEdit id ->
            Builder.absolute [ createursPrefix, Api.EntityId.entityIdToString id, "modifier" ] []

        Createur ( query, id ) ->
            Builder.absolute [ createursPrefix, Api.EntityId.entityIdToString id ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        Suivis query ->
            Builder.absolute [ "suivis" ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        ParamsCollegues ->
            Builder.absolute [ "parametres", "collegues" ] []

        ParamsProfil ->
            Builder.absolute [ "parametres", "profil" ] []

        ParamsSIG ->
            Builder.absolute [ "parametres", "sig" ] []

        ParamsEtiquettes ->
            Builder.absolute [ "parametres", "territoire" ] []

        ParamsImport ->
            Builder.absolute [ "parametres", "import" ] []

        ParamsNotifications ->
            Builder.absolute [ "parametres", "notifications" ] []

        AdminEquipes ->
            Builder.absolute [ "admin", "equipes" ] []

        AdminEquipeDemandes ->
            Builder.absolute [ "admin", "equipes", "demandes" ] []

        AyantDroit token ->
            Builder.absolute [ "autorisation", token ] []

        AdminUtilisateurs query ->
            Builder.absolute [ "admin", "utilisateurs" ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        AdminEtiquettes ->
            Builder.absolute [ "admin", "tags" ] []

        Deconnexion ->
            Builder.absolute [ "deconnexion" ] []

        Locaux query ->
            Builder.absolute [ locauxPrefix ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        Local ( query, id ) ->
            Builder.absolute [ locauxPrefix, id ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        AuthJwtUuid token redirect ->
            Builder.absolute [ "auth", "jwt", token ] <|
                List.filterMap identity <|
                    [ redirect
                        |> Maybe.map (Builder.string "url")
                    ]

        AuthCheck redirect ->
            Builder.absolute [ "auth", "check" ]
                (redirect
                    |> Maybe.map (Builder.string "url" >> List.singleton)
                    |> Maybe.withDefault []
                )

        Stats query ->
            Builder.absolute [ "stats" ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        Analysis ->
            Builder.absolute [ "stats", "analyse" ] []

        Annuaire query ->
            Builder.absolute [ "annuaire" ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        Activite query ->
            Builder.absolute [ "stats", "activite" ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        Etablissements query ->
            Builder.absolute [ etablissementsPrefix ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        Etablissement ( query, siret ) ->
            Builder.absolute [ etablissementsPrefix, siret ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        Evenements siret ->
            Builder.absolute [ "evenements", "etablissement", siret ] []

        NotFound url ->
            url

        Accessibilite ->
            Builder.absolute [ "accessibilite" ] []


defaultPageForRole : Maybe String -> Data.Role.Role -> Route
defaultPageForRole query role =
    if Data.Role.isDeveco role then
        Dashboard query

    else
        AdminUtilisateurs Nothing


matchAny : Route -> Route -> Maybe ()
matchAny any r =
    if any == r then
        Just ()

    else
        Nothing


matchConnexion : Route -> Maybe (Maybe String)
matchConnexion r =
    case r of
        Connexion redirect ->
            Just redirect

        _ ->
            Nothing


matchEtablissements : Route -> Maybe (Maybe String)
matchEtablissements r =
    case r of
        Etablissements search ->
            Just search

        _ ->
            Nothing


regexAuMoinsUnCaractereNonChiffreOuEspace : Regex.Regex
regexAuMoinsUnCaractereNonChiffreOuEspace =
    "[^\\d^\\s]"
        |> Regex.fromString
        |> Maybe.withDefault Regex.never


regexCaractereEspace : Regex.Regex
regexCaractereEspace =
    "[\\s]"
        |> Regex.fromString
        |> Maybe.withDefault Regex.never


matchEtablissementSiret : Route -> Maybe ( Maybe String, String )
matchEtablissementSiret r =
    case r of
        Etablissement ( search, siret ) ->
            let
                siretDecode =
                    siret
                        |> Url.percentDecode
                        |> Maybe.withDefault ""

                auMoinsUneLettre =
                    siretDecode
                        |> Regex.find regexAuMoinsUnCaractereNonChiffreOuEspace
                        |> List.length
                        |> (/=) 0
            in
            if auMoinsUneLettre then
                Nothing

            else
                let
                    siretNettoye =
                        siretDecode
                            |> Regex.replace regexCaractereEspace (\_ -> "")

                    siretEstValide =
                        siretNettoye
                            |> String.length
                            |> (==) 14
                in
                if not siretEstValide then
                    Nothing

                else
                    Just ( search, siretNettoye )

        _ ->
            Nothing


matchEvenementSiret : Route -> Maybe String
matchEvenementSiret r =
    case r of
        Evenements siret ->
            Just siret

        _ ->
            Nothing


matchRappels : Route -> Maybe (Maybe String)
matchRappels r =
    case r of
        Suivis query ->
            Just query

        _ ->
            Nothing


matchCreateurs : Route -> Maybe (Maybe String)
matchCreateurs r =
    case r of
        Createurs search ->
            Just search

        _ ->
            Nothing


matchCreateurNew : Route -> Maybe ()
matchCreateurNew =
    matchAny CreateurNew


matchCreateurId : Route -> Maybe ( Maybe String, EntityId EtablissementCreationId )
matchCreateurId r =
    case r of
        Createur ( query, id ) ->
            Just ( query, id )

        _ ->
            Nothing


matchCreateurEditId : Route -> Maybe (EntityId EtablissementCreationId)
matchCreateurEditId r =
    case r of
        CreateurEdit id ->
            Just id

        _ ->
            Nothing


matchNotFound : Route -> Maybe String
matchNotFound r =
    case r of
        NotFound url ->
            Just url

        _ ->
            Just <| toUrl r


matchAuthJwtUuid : Route -> Maybe ( String, Maybe String )
matchAuthJwtUuid r =
    case r of
        AuthJwtUuid uuid url ->
            Just ( uuid, url )

        _ ->
            Nothing


matchCollegue : Route -> Maybe ()
matchCollegue =
    matchAny ParamsCollegues


matchProfil : Route -> Maybe ()
matchProfil =
    matchAny ParamsProfil


matchSIG : Route -> Maybe ()
matchSIG =
    matchAny ParamsSIG


matchEtiquettes : Route -> Maybe ()
matchEtiquettes =
    matchAny ParamsEtiquettes


matchImport : Route -> Maybe ()
matchImport =
    matchAny ParamsImport


matchNotifications : Route -> Maybe ()
matchNotifications =
    matchAny ParamsNotifications


matchDashboard : Route -> Maybe (Maybe String)
matchDashboard r =
    case r of
        Dashboard query ->
            Just query

        _ ->
            Nothing


matchAdminEquipes : Route -> Maybe ()
matchAdminEquipes =
    matchAny AdminEquipes


matchAdminEquipeDemandes : Route -> Maybe ()
matchAdminEquipeDemandes =
    matchAny AdminEquipeDemandes


matchAyantDroit : Route -> Maybe String
matchAyantDroit r =
    case r of
        AyantDroit siret ->
            Just siret

        _ ->
            Nothing


matchAdminUtilisateurs : Route -> Maybe (Maybe String)
matchAdminUtilisateurs r =
    case r of
        AdminUtilisateurs search ->
            Just search

        _ ->
            Nothing


matchAdminEtiquettes : Route -> Maybe ()
matchAdminEtiquettes =
    matchAny AdminEtiquettes


matchDeconnexion : Route -> Maybe ()
matchDeconnexion =
    matchAny Deconnexion



--
--


matchLocaux : Route -> Maybe (Maybe String)
matchLocaux r =
    case r of
        Locaux search ->
            Just search

        _ ->
            Nothing



--
--
-- matchLocalNew : Route -> Maybe ()
-- matchLocalNew =
--     matchAny LocalNew
--
--


matchLocalId : Route -> Maybe ( Maybe String, String )
matchLocalId r =
    case r of
        Local ( search, id ) ->
            Just ( search, id )

        _ ->
            Nothing



--
--
-- matchLocalEditId : Route -> Maybe (EntityId LocalId)
-- matchLocalEditId r =
--     case r of
--         LocalEdit id ->
--             Just id
--
--         _ ->
--             Nothing


matchAuthCheck : Route -> Maybe (Maybe String)
matchAuthCheck r =
    case r of
        AuthCheck redirect ->
            Just redirect

        _ ->
            Nothing


matchStats : Route -> Maybe (Maybe String)
matchStats r =
    case r of
        Stats search ->
            Just search

        _ ->
            Nothing


matchAnalysis : Route -> Maybe ()
matchAnalysis =
    matchAny Analysis


matchAnnuaire : Route -> Maybe (Maybe String)
matchAnnuaire r =
    case r of
        Annuaire search ->
            Just search

        _ ->
            Nothing


matchActivite : Route -> Maybe (Maybe String)
matchActivite r =
    case r of
        Activite onglet ->
            Just onglet

        _ ->
            Nothing


matchAccessibilite : Route -> Maybe ()
matchAccessibilite =
    matchAny Accessibilite
