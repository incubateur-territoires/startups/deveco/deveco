module Api exposing (addLocalEtablissementOccupant, authStatus, clotureDemandeEtablissement, clotureDemandeEtablissementCreation, clotureDemandeLocal, clotureDemandeTierce, clotureRappelEtablissement, clotureRappelEtablissementCreation, clotureRappelLocal, confirmCgu, connexion, createCompteAdmin, createContact, createEtablissementContact, createEtablissementCreation, createEtablissementCreationContact, createLocalContact, createRessourceEtablissement, createRessourceEtablissementCreation, createRessourceLocal, deconnexion, deleteLocalEtablissementOccupant, downloadZonage, editContact, editRessourceEtablissement, editRessourceEtablissementCreation, editRessourceLocal, geoTokens, getActiviteStatsEtablissementCreations, getActiviteStatsEtablissements, getActiviteStatsLocaux, getApiEntrepriseData, getContacts, getCreateurSources, getDevecosAdmin, getDevecosCsvAdmin, getDevecosEquipe, getEntrepriseParticipations, getEquipeDemandeAutorisation, getEquipeDemandesAdmin, getEquipeEtablissementSuivis, getEquipeEtiquette, getEquipeEtiquettes, getEquipeMetaFiltres, getEquipeTypes, getEquipesAdmin, getEquipesEtiquettesAdmin, getEtabCreaParId, getEtablissement, getEtablissementContact, getEtablissementCreation, getEtablissementCreationContact, getEtablissementCreations, getEtablissementParSiret, getEtablissementZonages, getEtablissements, getEtablissementsFilters, getEtablissementsRechercheSauvegardeeLien, getEvenements, getLocal, getLocalAdresseNumeros, getLocalAdresseVoies, getLocalCadastreParcelles, getLocalCadastreSections, getLocalContact, getLocalParId, getLocalSources, getLocalZonages, getLocaux, getLocauxRechercheSauvegardeeLien, getNotifications, getStats, getSuivisEquipe, getZonages, jwt, localEtablissementsSuggestions, partageEtablissement, partageEtablissementCreation, partageLocal, rechercheActivite, rechercheAdresse, rechercheCategorieJuridique, rechercheCodeNaf, rechercheEntreprise, rechercheLocalisation, rechercheMot, rechercheQpvGeo, rechercheTerritoireGeo, reouvertureDemandeEtablissement, reouvertureDemandeEtablissementCreation, reouvertureDemandeLocal, reouvertureRappelEtablissement, reouvertureRappelEtablissementCreation, reouvertureRappelLocal, revertTransformEtablissementCreation, saveZonage, territoireSearch, toggleCompteActifAdmin, toggleEtabCreaCreationAbandonnee, toggleFavoriteEtablissement, toggleFavoriteLocal, toggleSignaleFermeEtablissement, transformEtablissementCreation, updateAccount, updateContributionDonnees, updateContributionNom, updateContributionVacance, updateEtablissementCommentaire, updateEtablissementCreationEtiquettes, updateEtablissementEnseigneContribution, updateEtablissementEtiquettes, updateEtablissementGeolocalisationContribution, updateEtablissementsEtiquettes, updateLocalEtiquettes, updateLocalGeolocalisationContribution, updateMotDePasseAdmin, updatePassword, uploadImport, uploadSiretisation, upsertBrouillon, upsertDemandeEtablissement, upsertDemandeEtablissementCreation, upsertDemandeLocal, upsertEchangeEtablissement, upsertEchangeEtablissementCreation, upsertEchangeLocal, upsertRappelEtablissement, upsertRappelEtablissementCreation, upsertRappelLocal)

import Api.EntityId exposing (EntityId, entityIdToString)
import Data.Brouillon exposing (BrouillonId)
import Data.Demande exposing (DemandeId)
import Data.Echange exposing (EchangeId)
import Data.Equipe exposing (EquipeId)
import Data.Etablissement exposing (Siret)
import Data.EtablissementCreationId exposing (EtablissementCreationId)
import Data.Etiquette exposing (EtiquetteId)
import Data.Personne exposing (PersonneId)
import Data.Rappel exposing (RappelId)
import Data.Zonage exposing (ZonageId)
import Url.Builder as Builder exposing (QueryParameter)
import User exposing (UserId)


apiRoot : String
apiRoot =
    "api"


territoiresRoute : String
territoiresRoute =
    "territoires"


localRoute : String
localRoute =
    "locaux"


apiUrl : List String -> List QueryParameter -> String
apiUrl segments =
    Builder.absolute (apiRoot :: segments)


rechercheAdresse : Maybe String -> Maybe String -> Maybe String -> String -> String
rechercheAdresse type_ cityCode postCode search =
    let
        sanitizedSearch =
            String.replace " " "+" search

        typeQuery =
            type_
                |> Maybe.map (Builder.string "type" >> List.singleton)
                |> Maybe.withDefault []

        cityQuery =
            cityCode
                |> Maybe.map (Builder.string "citycode" >> List.singleton)
                |> Maybe.withDefault []

        postQuery =
            postCode
                |> Maybe.map (Builder.string "postcode" >> List.singleton)
                |> Maybe.withDefault []
    in
    Builder.crossOrigin
        "https://api-adresse.data.gouv.fr"
        [ "search" ]
    <|
        Builder.string "q" sanitizedSearch
            :: (cityQuery ++ postQuery ++ typeQuery)


rechercheActivite : String -> String
rechercheActivite =
    rechercheEtiquette { etiquetteType = "activite" }


rechercheLocalisation : String -> String
rechercheLocalisation =
    rechercheEtiquette { etiquetteType = "localisation" }


rechercheMot : String -> String
rechercheMot =
    rechercheEtiquette { etiquetteType = "mot_cle" }


{-| API Meta
-}
metaRoute : String
metaRoute =
    "meta"


rechercheCategorieJuridique : String -> String
rechercheCategorieJuridique recherche =
    apiUrl [ metaRoute, "categories-juridiques" ]
        [ Builder.string "recherche" recherche
        ]


rechercheCodeNaf : String -> String
rechercheCodeNaf recherche =
    apiUrl [ metaRoute, "naf" ]
        [ Builder.string "recherche" recherche
        ]


getEquipeTypes : String
getEquipeTypes =
    apiUrl [ metaRoute, "equipe-types" ] []


getCreateurSources : String
getCreateurSources =
    apiUrl [ metaRoute, "createurs" ] []


{-| API Equipe Meta
-}
equipeMetaRoute : String
equipeMetaRoute =
    "equipe-meta"


getEquipeMetaFiltres : String -> String
getEquipeMetaFiltres context =
    apiUrl [ equipeMetaRoute, "filtres", context ] []


getLocalSources : String
getLocalSources =
    apiUrl [ equipeMetaRoute, "local-types" ] []


getLocalAdresseVoies : String -> String
getLocalAdresseVoies communeId =
    apiUrl [ equipeMetaRoute, "local-adresse", "communes", communeId, "voies-noms" ] []


getLocalAdresseNumeros : String -> String -> String
getLocalAdresseNumeros communeId voieNom =
    apiUrl [ equipeMetaRoute, "local-adresse", "communes", communeId, "voies-noms", voieNom, "numeros" ] []


getLocalCadastreSections : String -> String
getLocalCadastreSections communeId =
    apiUrl [ equipeMetaRoute, "local-adresse", "communes", communeId, "sections" ] []


getLocalCadastreParcelles : String -> String -> String
getLocalCadastreParcelles communeId cadastreSection =
    apiUrl [ equipeMetaRoute, "local-adresse", "communes", communeId, "sections", cadastreSection, "parcelles" ] []


{-| API Compte
-}
accountRoute : String
accountRoute =
    "compte"


updateAccount : String
updateAccount =
    apiUrl [ accountRoute, "info" ] []


updatePassword : String
updatePassword =
    apiUrl [ accountRoute, "mot-de-passe" ] []


getStats : String
getStats =
    apiUrl [ accountRoute, "stats" ] []


confirmCgu : String
confirmCgu =
    apiUrl [ accountRoute, "cgu" ] []


getEtablissementsFilters : String
getEtablissementsFilters =
    apiUrl [ accountRoute, "filtres", "etablissements" ] []


getEtablissementZonages : Siret -> String
getEtablissementZonages siret =
    apiUrl [ etablissementRoute, siret, "zonages" ] []


{-| API Entreprises
-}
entrepriseRoute : String
entrepriseRoute =
    "entreprises"


rechercheEntreprise : String -> String
rechercheEntreprise recherche =
    apiUrl [ entrepriseRoute ]
        [ Builder.string "recherche" recherche
        ]


getEntrepriseParticipations : String -> String
getEntrepriseParticipations siren =
    apiUrl [ entrepriseRoute, siren, "participation" ] []


saveZonage : Maybe (EntityId ZonageId) -> String
saveZonage maybeZonageId =
    maybeZonageId
        |> Maybe.map getZonage
        |> Maybe.withDefault getZonages


getLocaux : String -> String
getLocaux query =
    apiUrl [ localRoute ] [] ++ "?" ++ query


getLocal : String -> String
getLocal localId =
    apiUrl [ localRoute, localId ] []


createLocalContact : String -> String
createLocalContact localId =
    apiUrl [ localRoute, localId, "contacts" ] []


getLocalContact : String -> EntityId PersonneId -> String
getLocalContact localId id =
    apiUrl [ localRoute, localId, "contacts", entityIdToString id ] []


addLocalEtablissementOccupant : String -> String
addLocalEtablissementOccupant localId =
    apiUrl [ localRoute, localId, "occupants" ] []


deleteLocalEtablissementOccupant : String -> String -> String
deleteLocalEtablissementOccupant localId occupantId =
    apiUrl [ localRoute, localId, "occupants", occupantId ] []


localEtablissementsSuggestions : String -> String
localEtablissementsSuggestions localId =
    apiUrl [ localRoute, localId, "etablissement-suggestions" ] []


createRessourceLocal : String -> String
createRessourceLocal localId =
    apiUrl [ localRoute, localId, "ressource" ] []


editRessourceLocal : String -> Int -> String
editRessourceLocal localId ressourceId =
    apiUrl [ localRoute, localId, "ressource", String.fromInt ressourceId ] []


updateContributionNom : String -> String
updateContributionNom localId =
    apiUrl [ localRoute, localId, "nom" ] []


updateContributionVacance : String -> String
updateContributionVacance localId =
    apiUrl [ localRoute, localId, "vacance" ] []


updateContributionDonnees : String -> String
updateContributionDonnees localId =
    apiUrl [ localRoute, localId, "donnees" ] []


geoTokens : String
geoTokens =
    apiUrl [ accountRoute, "geotokens" ] []


rechercheTerritoireGeo : String -> String -> String -> String
rechercheTerritoireGeo geo typeTerritoire recherche =
    apiUrl [ equipeMetaRoute, "territoires", typeTerritoire ]
        [ Builder.string "nom" recherche
        , Builder.string "geo" geo
        ]


territoireSearch : String -> String -> String
territoireSearch typeTerritoire recherche =
    apiUrl [ territoiresRoute, "territoireType", typeTerritoire ]
        [ Builder.string "nom" recherche
        ]


rechercheQpvGeo : String -> String -> String
rechercheQpvGeo geo search =
    apiUrl [ equipeMetaRoute, "qpvs" ]
        [ Builder.string "recherche" search
        , Builder.string "geo" geo
        ]


{-| API Auth
-}
authRoute : String
authRoute =
    "auth"


authStatus : String
authStatus =
    apiUrl [ authRoute ] []


jwt : String -> String
jwt token =
    apiUrl [ authRoute, "jwt", token ] []


deconnexion : String
deconnexion =
    apiUrl [ authRoute, "deconnexion" ] []


connexion : String
connexion =
    apiUrl [ authRoute, "connexion" ] []


{-| API Établissements
-}
etablissementRoute : String
etablissementRoute =
    "etablissements"


getEtablissements : String -> String
getEtablissements query =
    apiUrl [ etablissementRoute ] [] ++ "?" ++ query


getEtablissementsRechercheSauvegardeeLien : String -> String
getEtablissementsRechercheSauvegardeeLien query =
    apiUrl [ etablissementRoute, "recherche-sauvegardee" ] [] ++ "?" ++ query


getEtablissement : String -> String
getEtablissement siret =
    apiUrl [ etablissementRoute, siret ] []


getApiEntrepriseData : String -> String
getApiEntrepriseData siren =
    apiUrl [ entrepriseRoute, siren, "api-entreprise" ] []


getEtablissementParSiret : String -> String
getEtablissementParSiret recherche =
    apiUrl [ etablissementRoute, "par-siret" ]
        [ Builder.string "recherche" recherche
        ]


updateEtablissementCommentaire : String -> String
updateEtablissementCommentaire siret =
    apiUrl [ etablissementRoute, siret, "commentaire" ] []


updateEtablissementEtiquettes : String -> String
updateEtablissementEtiquettes siret =
    apiUrl [ etablissementRoute, siret, "etiquettes" ] []


toggleFavoriteEtablissement : String -> String
toggleFavoriteEtablissement siret =
    apiUrl [ etablissementRoute, siret, "favori" ] []


toggleEtabCreaCreationAbandonnee : EntityId EtablissementCreationId -> String
toggleEtabCreaCreationAbandonnee id =
    apiUrl [ etabCreaRoute, entityIdToString id, "creationAbandonnee" ] []


toggleSignaleFermeEtablissement : String -> String
toggleSignaleFermeEtablissement siret =
    apiUrl
        [ etablissementRoute
        , siret
        , "cloture-date"
        ]
        []


updateEtablissementEnseigneContribution : String -> String
updateEtablissementEnseigneContribution siret =
    apiUrl
        [ etablissementRoute
        , siret
        , "enseigne"
        ]
        []


updateEtablissementGeolocalisationContribution : String -> String
updateEtablissementGeolocalisationContribution siret =
    apiUrl
        [ etablissementRoute
        , siret
        , "geolocalisation"
        ]
        []


updateEtablissementsEtiquettes : String -> String
updateEtablissementsEtiquettes query =
    apiUrl [ etablissementRoute, "etiquettes" ] [] ++ "?" ++ query


createEtablissementContact : String -> String
createEtablissementContact siret =
    apiUrl [ etablissementRoute, siret, "contacts" ] []


getEtablissementContact : String -> EntityId PersonneId -> String
getEtablissementContact siret id =
    apiUrl [ etablissementRoute, siret, "contacts", entityIdToString id ] []


upsertEchangeEtablissement : Bool -> Siret -> Maybe (EntityId EchangeId) -> String
upsertEchangeEtablissement global etablissementId echangeId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ etablissementRoute
               , etablissementId
               , "echanges"
               , echangeId
                    |> Maybe.map entityIdToString
                    |> Maybe.withDefault ""
               ]
        )
        []


upsertDemandeEtablissement : Bool -> Siret -> Maybe (EntityId DemandeId) -> String
upsertDemandeEtablissement global etablissementId demandeId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ etablissementRoute
               , etablissementId
               , "demandes"
               , demandeId
                    |> Maybe.map entityIdToString
                    |> Maybe.withDefault ""
               ]
        )
        []


upsertRappelEtablissement : Bool -> Siret -> Maybe (EntityId RappelId) -> String
upsertRappelEtablissement global siret rappelId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ etablissementRoute
               , siret
               , "rappels"
               , rappelId
                    |> Maybe.map entityIdToString
                    |> Maybe.withDefault ""
               ]
        )
        []


clotureRappelEtablissement : Bool -> Siret -> EntityId RappelId -> String
clotureRappelEtablissement global siret rappelId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ etablissementRoute
               , siret
               , "rappels"
               , rappelId
                    |> entityIdToString
               , "cloture"
               ]
        )
        []


reouvertureRappelEtablissement : Bool -> Siret -> EntityId RappelId -> String
reouvertureRappelEtablissement global siret rappelId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ etablissementRoute
               , siret
               , "rappels"
               , rappelId
                    |> entityIdToString
               , "reouverture"
               ]
        )
        []


clotureDemandeEtablissement : Bool -> String -> EntityId DemandeId -> String
clotureDemandeEtablissement global siret demandeId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ etablissementRoute
               , siret
               , "demandes"
               , entityIdToString demandeId
               , "cloture"
               ]
        )
        []


reouvertureDemandeEtablissement : Bool -> String -> EntityId DemandeId -> String
reouvertureDemandeEtablissement global siret demandeId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ etablissementRoute
               , siret
               , "demandes"
               , entityIdToString demandeId
               , "reouverture"
               ]
        )
        []


upsertBrouillon : Maybe (EntityId BrouillonId) -> String
upsertBrouillon brouillonId =
    apiUrl
        [ equipeRoute
        , "brouillons"
        , brouillonId
            |> Maybe.map entityIdToString
            |> Maybe.withDefault ""
        ]
        []


createRessourceEtablissement : String -> String
createRessourceEtablissement siret =
    apiUrl [ etablissementRoute, siret, "ressource" ] []


editRessourceEtablissement : String -> Int -> String
editRessourceEtablissement siret ressourceId =
    apiUrl [ etablissementRoute, siret, "ressource", String.fromInt ressourceId ] []


{-| API Établissement Création
-}
etabCreaRoute : String
etabCreaRoute =
    "etablissement-creations"


getEtablissementCreations : String -> String
getEtablissementCreations query =
    apiUrl [ etabCreaRoute ] [] ++ "?" ++ query


getEtablissementCreation : EntityId EtablissementCreationId -> String
getEtablissementCreation id =
    apiUrl [ etabCreaRoute, entityIdToString id ] []


createEtablissementCreation : String
createEtablissementCreation =
    apiUrl [ etabCreaRoute ] []


updateEtablissementCreationEtiquettes : String -> String
updateEtablissementCreationEtiquettes query =
    apiUrl [ etabCreaRoute, "etiquettes" ] [] ++ "?" ++ query


transformEtablissementCreation : EntityId EtablissementCreationId -> String -> String
transformEtablissementCreation id siret =
    apiUrl [ etabCreaRoute, entityIdToString id, "creer-transformation", siret ] []


revertTransformEtablissementCreation : EntityId EtablissementCreationId -> String
revertTransformEtablissementCreation id =
    apiUrl [ etabCreaRoute, entityIdToString id, "supprimer-transformation" ] []


upsertEchangeEtablissementCreation : Bool -> EntityId EtablissementCreationId -> Maybe (EntityId EchangeId) -> String
upsertEchangeEtablissementCreation global etablissementCreationId echangeId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ etabCreaRoute
               , entityIdToString etablissementCreationId
               , "echanges"
               , echangeId |> Maybe.map entityIdToString |> Maybe.withDefault ""
               ]
        )
        []


upsertRappelEtablissementCreation : Bool -> EntityId EtablissementCreationId -> Maybe (EntityId RappelId) -> String
upsertRappelEtablissementCreation global etablissementCreationId rappelId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ etabCreaRoute
               , entityIdToString etablissementCreationId
               , "rappels"
               , rappelId |> Maybe.map entityIdToString |> Maybe.withDefault ""
               ]
        )
        []


upsertDemandeEtablissementCreation : Bool -> EntityId EtablissementCreationId -> Maybe (EntityId DemandeId) -> String
upsertDemandeEtablissementCreation global etablissementCreationId demandeId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ etabCreaRoute
               , entityIdToString etablissementCreationId
               , "demandes"
               , demandeId
                    |> Maybe.map entityIdToString
                    |> Maybe.withDefault ""
               ]
        )
        []


clotureRappelEtablissementCreation : Bool -> EntityId EtablissementCreationId -> EntityId RappelId -> String
clotureRappelEtablissementCreation global etablissementCreationId rappelId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ etabCreaRoute
               , entityIdToString etablissementCreationId
               , "rappels"
               , rappelId |> entityIdToString
               , "cloture"
               ]
        )
        []


reouvertureRappelEtablissementCreation : Bool -> EntityId EtablissementCreationId -> EntityId RappelId -> String
reouvertureRappelEtablissementCreation global etablissementCreationId rappelId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ etabCreaRoute
               , entityIdToString etablissementCreationId
               , "rappels"
               , rappelId |> entityIdToString
               , "reouverture"
               ]
        )
        []


clotureDemandeEtablissementCreation : Bool -> EntityId EtablissementCreationId -> EntityId DemandeId -> String
clotureDemandeEtablissementCreation global etablissementCreationId demandeId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ etabCreaRoute
               , entityIdToString etablissementCreationId
               , "demandes"
               , entityIdToString demandeId
               , "cloture"
               ]
        )
        []


reouvertureDemandeEtablissementCreation : Bool -> EntityId EtablissementCreationId -> EntityId DemandeId -> String
reouvertureDemandeEtablissementCreation global etablissementCreationId demandeId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ etabCreaRoute
               , entityIdToString etablissementCreationId
               , "demandes"
               , entityIdToString demandeId
               , "reouverture"
               ]
        )
        []


createEtablissementCreationContact : EntityId EtablissementCreationId -> String
createEtablissementCreationContact etablissementCreationId =
    apiUrl [ etabCreaRoute, entityIdToString etablissementCreationId, "createurs" ] []


getEtablissementCreationContact : EntityId EtablissementCreationId -> EntityId PersonneId -> String
getEtablissementCreationContact etablissementCreationId id =
    apiUrl [ etabCreaRoute, entityIdToString etablissementCreationId, "createurs", entityIdToString id ] []


createRessourceEtablissementCreation : EntityId EtablissementCreationId -> String
createRessourceEtablissementCreation id =
    apiUrl [ etabCreaRoute, entityIdToString id, "ressource" ] []


editRessourceEtablissementCreation : EntityId EtablissementCreationId -> Int -> String
editRessourceEtablissementCreation id ressourceId =
    apiUrl [ etabCreaRoute, entityIdToString id, "ressource", String.fromInt ressourceId ] []


getEtabCreaParId : String -> String
getEtabCreaParId recherche =
    apiUrl [ etabCreaRoute, "par-id" ]
        [ Builder.string "recherche" recherche
        ]


{-| API Admin
-}
adminRoute : String
adminRoute =
    "admin"


getDevecosAdmin : String -> String
getDevecosAdmin query =
    apiUrl [ adminRoute, "deveco" ] [] ++ "?" ++ query


getDevecosCsvAdmin : String
getDevecosCsvAdmin =
    apiUrl [ adminRoute, "deveco", "csv" ] []


getEquipesAdmin : String
getEquipesAdmin =
    apiUrl [ adminRoute, "equipe" ] []


getEquipeDemandesAdmin : String
getEquipeDemandesAdmin =
    apiUrl [ adminRoute, "equipe", "demandes" ] []


getEquipeDemandeAutorisation : String -> String
getEquipeDemandeAutorisation token =
    apiUrl [ "autorisation", token ] []


getEquipesEtiquettesAdmin : EntityId EquipeId -> String
getEquipesEtiquettesAdmin equipeId =
    apiUrl [ adminRoute, "equipe", entityIdToString equipeId, "etiquettes" ] []


createCompteAdmin : String
createCompteAdmin =
    apiUrl [ adminRoute, "compte" ] []


updateMotDePasseAdmin : String -> String
updateMotDePasseAdmin compteId =
    apiUrl [ adminRoute, "compte", compteId, "mot-de-passe" ] []


toggleCompteActifAdmin : String
toggleCompteActifAdmin =
    apiUrl [ adminRoute, "compte", "actif" ] []


uploadImport : EntityId UserId -> String
uploadImport id =
    apiUrl [ adminRoute, "import", entityIdToString id ] []


uploadSiretisation : String
uploadSiretisation =
    apiUrl [ adminRoute, "siretisation" ] []


{-| API Équipe
-}
equipeRoute : String
equipeRoute =
    "equipe"


getSuivisEquipe : String
getSuivisEquipe =
    apiUrl [ equipeRoute, "suivis" ] []


getZonages : String
getZonages =
    apiUrl [ equipeRoute, "zonages" ] []


getZonage : EntityId ZonageId -> String
getZonage id =
    apiUrl [ equipeRoute, "zonages", entityIdToString id ] []


downloadZonage : EntityId ZonageId -> String
downloadZonage id =
    apiUrl [ equipeRoute, "zonages", entityIdToString id, "fichier" ] []


getEquipeEtiquettes : String
getEquipeEtiquettes =
    apiUrl [ equipeRoute, "etiquettes" ] []


getEquipeEtiquette : EntityId EtiquetteId -> String
getEquipeEtiquette etiquetteId =
    apiUrl [ equipeRoute, "etiquettes", entityIdToString etiquetteId ] []


getContacts : String -> String
getContacts query =
    apiUrl [ contactRoute ] [] ++ "?" ++ query


rechercheEtiquette : { etiquetteType : String } -> String -> String
rechercheEtiquette { etiquetteType } search =
    apiUrl [ equipeRoute, "etiquettes", etiquetteType ]
        [ Builder.string "recherche" search
        ]


getDevecosEquipe : String
getDevecosEquipe =
    apiUrl [ equipeRoute, "devecos" ] []


getActiviteStatsEtablissements : String -> String -> String
getActiviteStatsEtablissements debut fin =
    apiUrl [ equipeRoute, "stats", "etablissements" ]
        [ Builder.string "debut" debut
        , Builder.string "fin" fin
        ]


getActiviteStatsEtablissementCreations : String -> String -> String
getActiviteStatsEtablissementCreations debut fin =
    apiUrl [ equipeRoute, "stats", "etablissement-creations" ]
        [ Builder.string "debut" debut
        , Builder.string "fin" fin
        ]


getActiviteStatsLocaux : String -> String -> String
getActiviteStatsLocaux debut fin =
    apiUrl [ equipeRoute, "stats", "locaux" ]
        [ Builder.string "debut" debut
        , Builder.string "fin" fin
        ]


getEvenements : Siret -> String
getEvenements siret =
    apiUrl [ etablissementRoute, siret, "evenements" ] []


getEquipeEtablissementSuivis : Siret -> String
getEquipeEtablissementSuivis siret =
    apiUrl [ etablissementRoute, siret, "suivi-par" ] []


getNotifications : String
getNotifications =
    apiUrl [ accountRoute, "notifications" ] []


partageEtablissement : Siret -> String
partageEtablissement siret =
    apiUrl [ etablissementRoute, siret, "partage" ] []


partageEtablissementCreation : EntityId EtablissementCreationId -> String
partageEtablissementCreation id =
    apiUrl [ etabCreaRoute, entityIdToString id, "partage" ] []


partageLocal : String -> String
partageLocal localId =
    apiUrl [ localRoute, localId, "partage" ] []


upsertEchangeLocal : Bool -> String -> Maybe (EntityId EchangeId) -> String
upsertEchangeLocal global localId echangeId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ localRoute
               , localId
               , "echanges"
               , echangeId
                    |> Maybe.map entityIdToString
                    |> Maybe.withDefault ""
               ]
        )
        []


upsertDemandeLocal : Bool -> String -> Maybe (EntityId DemandeId) -> String
upsertDemandeLocal global localId demandeId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ localRoute
               , localId
               , "demandes"
               , demandeId
                    |> Maybe.map entityIdToString
                    |> Maybe.withDefault ""
               ]
        )
        []


upsertRappelLocal : Bool -> String -> Maybe (EntityId RappelId) -> String
upsertRappelLocal global localId rappelId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ localRoute
               , localId
               , "rappels"
               , rappelId
                    |> Maybe.map entityIdToString
                    |> Maybe.withDefault ""
               ]
        )
        []


clotureRappelLocal : Bool -> String -> EntityId RappelId -> String
clotureRappelLocal global localId rappelId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ localRoute
               , localId
               , "rappels"
               , rappelId
                    |> entityIdToString
               , "cloture"
               ]
        )
        []


reouvertureRappelLocal : Bool -> String -> EntityId RappelId -> String
reouvertureRappelLocal global localId rappelId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ localRoute
               , localId
               , "rappels"
               , rappelId
                    |> entityIdToString
               , "reouverture"
               ]
        )
        []


clotureDemandeLocal : Bool -> String -> EntityId DemandeId -> String
clotureDemandeLocal global localId demandeId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ localRoute
               , localId
               , "demandes"
               , entityIdToString demandeId
               , "cloture"
               ]
        )
        []


reouvertureDemandeLocal : Bool -> String -> EntityId DemandeId -> String
reouvertureDemandeLocal global localId demandeId =
    apiUrl
        ((if global then
            [ equipeRoute ]

          else
            []
         )
            ++ [ localRoute
               , localId
               , "demandes"
               , entityIdToString demandeId
               , "reouverture"
               ]
        )
        []


updateLocalEtiquettes : String -> String
updateLocalEtiquettes localId =
    apiUrl [ localRoute, localId, "etiquettes" ] []


updateLocalGeolocalisationContribution : String -> String
updateLocalGeolocalisationContribution siret =
    apiUrl
        [ localRoute
        , siret
        , "geolocalisation"
        ]
        []


getLocalZonages : Siret -> String
getLocalZonages localId =
    apiUrl [ localRoute, localId, "zonages" ] []


getLocauxRechercheSauvegardeeLien : String -> String
getLocauxRechercheSauvegardeeLien query =
    apiUrl [ localRoute, "recherche-sauvegardee" ] [] ++ "?" ++ query


toggleFavoriteLocal : String -> String
toggleFavoriteLocal id =
    apiUrl [ localRoute, id, "favori" ] []


getLocalParId : String -> String
getLocalParId recherche =
    apiUrl [ localRoute, "par-id" ]
        [ Builder.string "recherche" recherche
        ]


{-| API Contacts
-}
contactRoute : String
contactRoute =
    "contacts"


createContact : String -> String
createContact query =
    apiUrl [ contactRoute ] [] ++ "?" ++ query


editContact : EntityId PersonneId -> String -> String
editContact id query =
    apiUrl [ contactRoute, entityIdToString id ] [] ++ "?" ++ query


clotureDemandeTierce : Int -> String
clotureDemandeTierce id =
    apiUrl [ adminRoute, "equipe", "demandes", String.fromInt id, "cloture" ] []
