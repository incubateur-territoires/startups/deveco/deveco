module Main exposing (main)

import Browser
import Pages.Accessibilite
import Pages.Activite
import Pages.Admin.EquipeDemandes
import Pages.Admin.Equipes
import Pages.Admin.Etiquettes
import Pages.Admin.Utilisateurs
import Pages.AnalyseTerritoriale
import Pages.Annuaire
import Pages.Auth.Check
import Pages.Auth.Jwt.Uuid_
import Pages.AyantDroit
import Pages.Connexion
import Pages.Dashboard
import Pages.Deconnexion
import Pages.Etablissement.Siret_
import Pages.EtablissementCreation.EditId_
import Pages.EtablissementCreation.Id_
import Pages.EtablissementCreation.New
import Pages.EtablissementCreations
import Pages.Etablissements
import Pages.Evenements
import Pages.Local.Id_
import Pages.Locaux
import Pages.NotFound
import Pages.Parametres.Collegues
import Pages.Parametres.Etiquettes
import Pages.Parametres.Import
import Pages.Parametres.Notifications
import Pages.Parametres.Profil
import Pages.Parametres.SIG
import Pages.Stats
import Pages.Suivis
import Route
import Shared
import Spa
import View


main =
    Spa.init
        { defaultView = View.defaultView
        , extractIdentity = Shared.identity
        }
        |> addPages
        |> Spa.application View.map
            { toRoute = Route.toRoute
            , init = Shared.init
            , update = Shared.update
            , subscriptions = Shared.subscriptions
            , toDocument = View.toDocument
            , protectPage = Route.toUrl >> Just >> Route.AuthCheck >> Route.toUrl
            }
        |> Browser.application


addPages spa =
    spa
        |> Spa.addPublicPage View.mappers Route.matchNotFound Pages.NotFound.page
        |> Spa.addProtectedPage View.mappers Route.matchCreateurEditId Pages.EtablissementCreation.EditId_.page
        |> Spa.addProtectedPage View.mappers Route.matchCreateurId Pages.EtablissementCreation.Id_.page
        |> Spa.addProtectedPage View.mappers Route.matchCreateurNew Pages.EtablissementCreation.New.page
        |> Spa.addProtectedPage View.mappers Route.matchCreateurs Pages.EtablissementCreations.page
        |> Spa.addProtectedPage View.mappers Route.matchEtablissements Pages.Etablissements.page
        |> Spa.addProtectedPage View.mappers Route.matchEtablissementSiret Pages.Etablissement.Siret_.page
        |> Spa.addProtectedPage View.mappers Route.matchRappels Pages.Suivis.page
        |> Spa.addProtectedPage View.mappers Route.matchCollegue Pages.Parametres.Collegues.page
        |> Spa.addProtectedPage View.mappers Route.matchProfil Pages.Parametres.Profil.page
        |> Spa.addProtectedPage View.mappers Route.matchSIG Pages.Parametres.SIG.page
        |> Spa.addProtectedPage View.mappers Route.matchNotifications Pages.Parametres.Notifications.page
        |> Spa.addProtectedPage View.mappers Route.matchEtiquettes Pages.Parametres.Etiquettes.page
        |> Spa.addProtectedPage View.mappers Route.matchImport Pages.Parametres.Import.page
        |> Spa.addProtectedPage View.mappers Route.matchAdminUtilisateurs Pages.Admin.Utilisateurs.page
        |> Spa.addProtectedPage View.mappers Route.matchAdminEquipes Pages.Admin.Equipes.page
        |> Spa.addProtectedPage View.mappers Route.matchAdminEquipeDemandes Pages.Admin.EquipeDemandes.page
        |> Spa.addProtectedPage View.mappers Route.matchAdminEtiquettes Pages.Admin.Etiquettes.page
        |> Spa.addProtectedPage View.mappers Route.matchLocaux Pages.Locaux.page
        |> Spa.addProtectedPage View.mappers Route.matchLocalId Pages.Local.Id_.page
        |> Spa.addProtectedPage View.mappers Route.matchAnalysis Pages.AnalyseTerritoriale.page
        |> Spa.addProtectedPage View.mappers Route.matchAnnuaire Pages.Annuaire.page
        |> Spa.addProtectedPage View.mappers Route.matchActivite Pages.Activite.page
        |> Spa.addProtectedPage View.mappers Route.matchEvenementSiret Pages.Evenements.page
        |> Spa.addPublicPage View.mappers Route.matchAccessibilite Pages.Accessibilite.page
        |> Spa.addPublicPage View.mappers Route.matchDashboard Pages.Dashboard.page
        |> Spa.addPublicPage View.mappers Route.matchConnexion Pages.Connexion.page
        |> Spa.addPublicPage View.mappers Route.matchDeconnexion Pages.Deconnexion.page
        |> Spa.addPublicPage View.mappers Route.matchAuthJwtUuid Pages.Auth.Jwt.Uuid_.page
        |> Spa.addPublicPage View.mappers Route.matchAuthCheck Pages.Auth.Check.page
        |> Spa.addPublicPage View.mappers Route.matchStats Pages.Stats.page
        |> Spa.addPublicPage View.mappers Route.matchAyantDroit Pages.AyantDroit.page
