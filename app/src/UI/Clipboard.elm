module UI.Clipboard exposing (clipboard)

import DSFR.Button
import DSFR.Icons.Document
import Html
import Html.Attributes as Attr


clipboard : { content : String, label : String, copiedLabel : String, timeoutMilliseconds : Int } -> Html.Html msg
clipboard { content, label, copiedLabel, timeoutMilliseconds } =
    Html.node "clipboard-copy"
        [ Attr.value <| content ]
        [ DSFR.Button.new
            { label = label
            , onClick = Nothing
            }
            |> DSFR.Button.leftIcon DSFR.Icons.Document.clipboardLine
            |> DSFR.Button.tertiaryNoOutline
            |> DSFR.Button.withAttrs
                [ Attr.title label
                , Attr.attribute "data-clipboard-copied" copiedLabel
                , Attr.attribute "data-clipboard-timeout" <| String.fromInt timeoutMilliseconds
                ]
            |> DSFR.Button.view
        ]
