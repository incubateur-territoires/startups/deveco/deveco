module UI.Demande exposing (DemandeInput, Field(..), clotureDemandeForEntite, default, demandeNomToTag, demandeToDemandeInput, demandeTypeToTag, encodeDemandeInput, form, reouvertureDemandeForEntite, update, updateDemandeForEntite, view, viewDemandeEchanges)

import Accessibility exposing (Html, div, h4, p, span, text)
import Api
import Api.EntityId exposing (EntityId, encodeEntityId)
import DSFR.Button
import DSFR.Icons
import DSFR.Icons.Business
import DSFR.Icons.Communication
import DSFR.Icons.Design
import DSFR.Icons.System
import DSFR.Input
import DSFR.Tag
import DSFR.Tooltip
import DSFR.Typography as Typo
import Data.Brouillon exposing (BrouillonId)
import Data.Demande exposing (Demande, DemandeId, TypeDemande, stringToTypeDemande, typeDemandeToDisplay, typeDemandeToString)
import Data.Entite exposing (Entite(..), viewLienEntite)
import Date exposing (Date)
import Html
import Html.Attributes exposing (class)
import Html.Attributes.Extra exposing (empty)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Json.Encode as Encode exposing (Value)
import Lib.Date exposing (formatDateShort)
import Lib.UI exposing (plural, withEmptyAs)


type Field
    = NomDemande
    | DescriptionDemande
    | TypeDemande


type alias DemandeInput =
    { id : EntityId DemandeId
    , nom : String
    , description : String
    , typeDemande : TypeDemande
    , cloture : Bool
    , motif : String
    , clotureDate : Maybe Date
    , entite : Maybe Entite
    , brouillonId : Maybe (EntityId BrouillonId)
    , brouillonSuppression : Bool
    }


default : Maybe (EntityId BrouillonId) -> DemandeInput
default brouillonId =
    { id = Api.EntityId.newId
    , nom = ""
    , description = ""
    , typeDemande = Data.Demande.Accompagnement
    , cloture = False
    , motif = ""
    , clotureDate = Nothing
    , entite = Nothing
    , brouillonId = brouillonId
    , brouillonSuppression = False
    }


update : Field -> String -> DemandeInput -> DemandeInput
update field value demande =
    case field of
        NomDemande ->
            { demande | nom = value }

        DescriptionDemande ->
            { demande | description = value }

        TypeDemande ->
            { demande | typeDemande = value |> stringToTypeDemande |> Maybe.withDefault demande.typeDemande }


form : List TypeDemande -> (Field -> String -> msg) -> DemandeInput -> Html msg
form options updateField demande =
    div [ class "flex flex-col gap-4" ]
        [ div [ class "fr-form-group" ]
            [ DSFR.Input.new
                { value = demande.typeDemande |> typeDemandeToString
                , onInput = updateField TypeDemande
                , label = text "Type de demande"
                , name = "demande-type"
                }
                |> DSFR.Input.select
                    { options =
                        options
                            |> List.map Just
                            |> (::) Nothing
                    , toId = Maybe.map typeDemandeToString >> Maybe.withDefault "aucun"
                    , toLabel =
                        Maybe.map typeDemandeToDisplay
                            >> Maybe.withDefault "Sélectionner une option"
                            >> text
                    , toDisabled = Nothing
                    }
                |> DSFR.Input.view
            ]
        , div [ class "fr-form-group" ]
            [ DSFR.Input.new
                { value = demande.nom
                , onInput = updateField NomDemande
                , label = text "Titre de la demande"
                , name = "nom-demande"
                }
                |> DSFR.Input.withRequired True
                |> DSFR.Input.withHint [ text <| String.fromInt <| String.length demande.nom, text "/30 caractères" ]
                |> DSFR.Input.withTextInputAttrs (Just { minlength = Nothing, maxlength = Just 30 })
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            ]
        , div [ class "fr-form-group !mb-0" ]
            [ DSFR.Input.new
                { value = demande.description
                , onInput = updateField DescriptionDemande
                , label = text "Description de la demande"
                , name = "description-demande"
                }
                |> DSFR.Input.textArea (Just 8)
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            ]
        ]


demandeToDemandeInput : Maybe (EntityId BrouillonId) -> Demande -> DemandeInput
demandeToDemandeInput brouillonId demande =
    { id = Data.Demande.id demande
    , nom = Data.Demande.nom demande
    , description = Data.Demande.description demande
    , typeDemande = Data.Demande.type_ demande
    , cloture = Data.Demande.cloture demande
    , motif = Data.Demande.clotureMotif demande
    , clotureDate = Data.Demande.clotureDate demande
    , entite = Data.Demande.entite demande
    , brouillonId = brouillonId
    , brouillonSuppression = False
    }


encodeDemandeInput : DemandeInput -> Value
encodeDemandeInput demandeInput =
    [ ( "demande"
      , [ ( "id", encodeEntityId demandeInput.id )
        , ( "nom", Encode.string demandeInput.nom )
        , ( "description", Encode.string demandeInput.description )
        , ( "typeId"
          , Encode.string <|
                typeDemandeToString <|
                    demandeInput.typeDemande
          )
        , ( "brouillonId"
          , Maybe.withDefault Encode.null <|
                Maybe.map encodeEntityId <|
                    demandeInput.brouillonId
          )
        , ( "brouillonSuppression", Encode.bool demandeInput.brouillonSuppression )
        ]
            |> Encode.object
      )
    ]
        |> Encode.object


demandeNomToTag : Demande -> DSFR.Tag.TagConfig msg String
demandeNomToTag demande =
    let
        label =
            if Data.Demande.cloture demande then
                "Demande clôturée : " ++ Data.Demande.nom demande

            else
                Data.Demande.nom demande
    in
    { data = label, toString = identity }
        |> DSFR.Tag.unclickable


demandeTypeToTag : Demande -> DSFR.Tag.TagConfig msg String
demandeTypeToTag demande =
    let
        label =
            if Data.Demande.cloture demande then
                "Demande clôturée : " ++ Data.Demande.label demande

            else
                Data.Demande.label demande
    in
    { data = label, toString = identity }
        |> DSFR.Tag.unclickable


viewDemandeEchanges : Maybe (String -> msg) -> Demande -> Html msg
viewDemandeEchanges viewEchanges demande =
    let
        nombreEchanges =
            Data.Demande.nombreEchanges demande
    in
    Html.div
        [ Typo.textSm
        , Typo.textLight
        , class "!mb-0 grey-text"
        , viewEchanges
            |> Maybe.map (\msg -> Events.onClick <| msg <| Data.Demande.nom <| demande)
            |> Maybe.withDefault empty
        , viewEchanges
            |> Maybe.map (\_ -> class "cursor-pointer")
            |> Maybe.withDefault empty
        ]
        [ DSFR.Icons.iconSM DSFR.Icons.Communication.discussFill
        , text " "
        , text <|
            if nombreEchanges > 0 then
                String.fromInt <| nombreEchanges

            else
                "Aucun"
        , text " échange"
        , text <| plural <| nombreEchanges
        ]


view : { edit : Demande -> msg, closeReopen : Demande -> msg, delete : Demande -> msg, viewEchanges : Maybe (String -> msg), forPrint : Bool } -> Demande -> Html msg
view { edit, closeReopen, delete, viewEchanges, forPrint } demande =
    div [ class "flex flex-col gap-2" ] <|
        [ div [ class "flex flex-row gap-2 items-center" ]
            [ [ demandeTypeToTag demande ]
                |> DSFR.Tag.medium
            , if forPrint then
                nothing

              else
                demande
                    |> Data.Demande.clotureDate
                    |> Maybe.map
                        (\d ->
                            span [ class "blue-text" ] [ DSFR.Icons.icon DSFR.Icons.System.questionLine ]
                                |> (DSFR.Tooltip.survol
                                        { label =
                                            text <|
                                                (withEmptyAs "Aucun motif" <|
                                                    Data.Demande.clotureMotif demande
                                                )
                                                    ++ " (le "
                                                    ++ formatDateShort d
                                                    ++ ")"
                                        , id = "tooltip-demande-cloture-motif"
                                        }
                                        |> DSFR.Tooltip.wrap
                                   )
                                |> List.singleton
                                |> div [ class "pb-[0.75rem]" ]
                        )
                    |> Maybe.withDefault nothing
            ]
        , viewDemandeEchanges viewEchanges demande
        , h4 [ class "whitespace-pre-wrap !mb-0" ]
            [ span [ class "font-bold" ]
                [ text <|
                    withEmptyAs "-" <|
                        Data.Demande.nom demande
                ]
            ]
        , p [ class "whitespace-pre-wrap !mb-0" ]
            [ text <|
                withEmptyAs "-" <|
                    Data.Demande.description demande
            ]
        , viewLienEntite "demandes" <| Data.Demande.entite demande
        , [ DSFR.Button.new
                { onClick = Just <| edit <| demande
                , label = "Modifier"
                }
                |> DSFR.Button.leftIcon DSFR.Icons.Design.editFill
                |> DSFR.Button.withDisabled (Data.Demande.clotureDate demande /= Nothing)
                |> DSFR.Button.withAttrs [ class "!mb-0" ]
                |> DSFR.Button.secondary
          , DSFR.Button.new
                { onClick = Just <| closeReopen <| demande
                , label =
                    if Data.Demande.cloture demande then
                        "Réouvrir"

                    else
                        "Clôturer"
                }
                |> DSFR.Button.leftIcon DSFR.Icons.Business.archiveLine
                |> DSFR.Button.withAttrs [ class "!mb-0" ]
                |> DSFR.Button.tertiary
          , DSFR.Button.new
                { onClick = Just <| delete <| demande
                , label = "Supprimer"
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.deleteLine
                |> DSFR.Button.withAttrs [ class "!mb-0" ]
                |> DSFR.Button.tertiaryNoOutline
          ]
            |> DSFR.Button.group
            |> DSFR.Button.inline
            |> DSFR.Button.iconsLeft
            |> DSFR.Button.viewGroup
        ]


updateDemandeForEntite : Bool -> Maybe Entite -> Maybe (EntityId DemandeId) -> String
updateDemandeForEntite global entite =
    case entite of
        Nothing ->
            \_ -> ""

        Just e ->
            case e of
                Etab siret _ ->
                    Api.upsertDemandeEtablissement global siret

                EtabCrea etablissementCreationId _ ->
                    Api.upsertDemandeEtablissementCreation global etablissementCreationId

                Loc localId _ ->
                    Api.upsertDemandeLocal global localId


clotureDemandeForEntite : Bool -> Maybe Entite -> EntityId DemandeId -> String
clotureDemandeForEntite global entite =
    case entite of
        Nothing ->
            \_ -> ""

        Just e ->
            case e of
                Etab siret _ ->
                    Api.clotureDemandeEtablissement global siret

                EtabCrea etablissementCreationId _ ->
                    Api.clotureDemandeEtablissementCreation global etablissementCreationId

                Loc localId _ ->
                    Api.clotureDemandeLocal global localId


reouvertureDemandeForEntite : Bool -> Maybe Entite -> EntityId DemandeId -> String
reouvertureDemandeForEntite global entite =
    case entite of
        Nothing ->
            \_ -> ""

        Just e ->
            case e of
                Etab siret _ ->
                    Api.reouvertureDemandeEtablissement global siret

                EtabCrea etablissementCreationId _ ->
                    Api.reouvertureDemandeEtablissementCreation global etablissementCreationId

                Loc localId _ ->
                    Api.reouvertureDemandeLocal global localId
