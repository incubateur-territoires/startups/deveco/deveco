module UI.Echange exposing (EchangeInput, Field(..), default, echangeInputToEchange, echangeToDisplay, encodeEchangeInput, form, getDemandeInDemandes, isValid, toggleDemande, update, updateEchangeForEntite, view)

import Accessibility exposing (Html, div, h4, p, span, text)
import Api
import Api.EntityId exposing (EntityId)
import DSFR.Button
import DSFR.Icons
import DSFR.Icons.Business
import DSFR.Icons.Design
import DSFR.Icons.Device
import DSFR.Icons.System
import DSFR.Icons.User
import DSFR.Input
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Brouillon exposing (BrouillonId)
import Data.Demande exposing (Demande, DemandeId)
import Data.Echange exposing (Echange, EchangeId, TypeEchange, stringToTypeEchange, typeEchangeToString)
import Data.Entite exposing (Entite(..), viewLienEntite)
import Date exposing (Date)
import Dict exposing (Dict)
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing, viewIf)
import Json.Encode as Encode exposing (Value)
import Lib.Date
import Lib.UI exposing (withEmptyAs)
import List.Extra
import Time exposing (Zone)
import UI.Demande exposing (demandeNomToTag)


type Field
    = DateEchange
    | TitreEchange
    | DescriptionEchange
    | TypeEchange


type alias EchangeInput =
    { id : EntityId EchangeId
    , typeEchange : TypeEchange
    , dateEchange : String
    , nom : String
    , description : String
    , demandes : List (EntityId DemandeId)
    , entite : Maybe Entite
    , brouillonId : Maybe (EntityId BrouillonId)
    , brouillonSuppression : Bool
    }


default : Maybe (EntityId BrouillonId) -> String -> Date -> EchangeInput
default brouillonId title today =
    { id = Api.EntityId.newId
    , typeEchange = Data.Echange.Telephone
    , dateEchange = today |> Date.toIsoString
    , nom = title
    , description = ""
    , demandes = []
    , entite = Nothing
    , brouillonId = brouillonId
    , brouillonSuppression = False
    }


update : Field -> String -> EchangeInput -> EchangeInput
update field value echange =
    case field of
        DateEchange ->
            { echange | dateEchange = value }

        TitreEchange ->
            { echange | nom = value }

        DescriptionEchange ->
            { echange | description = value }

        TypeEchange ->
            { echange | typeEchange = value |> stringToTypeEchange |> Maybe.withDefault echange.typeEchange }


toggleDemande : Bool -> EntityId DemandeId -> EchangeInput -> EchangeInput
toggleDemande add demandeId echange =
    { echange
        | demandes =
            if add then
                if List.member demandeId echange.demandes then
                    echange.demandes

                else
                    demandeId :: echange.demandes

            else
                echange.demandes
                    |> List.filter (\id -> id /= demandeId)
    }


form : (Field -> String -> msg) -> EchangeInput -> Html msg
form updateField echange =
    div [ class "flex flex-col gap-4" ]
        [ div [ class "fr-form-group flex flex-row gap-4 !mb-0" ]
            [ DSFR.Input.new
                { value = echange.typeEchange |> typeEchangeToString
                , onInput = updateField TypeEchange
                , label = text "Canal de l'échange"
                , name = "type-echange"
                }
                |> DSFR.Input.select
                    { options = typeEchanges
                    , toId = typeEchangeToString
                    , toLabel = echangeToDisplay >> text
                    , toDisabled = Nothing
                    }
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            , DSFR.Input.new
                { value = echange.dateEchange
                , onInput = updateField DateEchange
                , label = text "Date de l'échange"
                , name = "date-echange"
                }
                |> DSFR.Input.date { min = Nothing, max = Nothing }
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            ]
        , div [ class "fr-form-group !mb-0" ]
            [ DSFR.Input.new
                { value = echange.nom
                , onInput = updateField TitreEchange
                , label = text "Titre de l'échange"
                , name = "titre-echange"
                }
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            ]
        , div [ class "fr-form-group !mb-0" ]
            [ DSFR.Input.new
                { value = echange.description
                , onInput = updateField DescriptionEchange
                , label = text "Compte-rendu de l'échange"
                , name = "description-echange"
                }
                |> DSFR.Input.textArea (Just 8)
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            ]
        ]


isValid : EchangeInput -> Bool
isValid =
    echangeInputToEchange >> Result.map (\_ -> True) >> Result.withDefault False


echangeInputToEchange : EchangeInput -> Result (Dict String (List String)) Echange
echangeInputToEchange input =
    let
        validateDate =
            ( .dateEchange
            , "date"
            , \date ->
                date |> Date.fromIsoString |> Result.mapError (\_ -> "Date invalide")
            )
    in
    Ok (\id type_ date titre compteRendu demandes entite -> Data.Echange.createEchange <| { id = id, type_ = type_, titre = titre, compteRendu = compteRendu, date = date, demandes = demandes, entite = entite })
        |> keepErrors (Ok <| .id <| input)
        |> keepErrors (Ok <| .typeEchange <| input)
        |> keepErrors (validate validateDate input)
        |> keepErrors (Ok <| .nom <| input)
        |> keepErrors (Ok <| .description <| input)
        |> keepErrors (Ok <| .demandes <| input)
        |> keepErrors (Ok <| .entite <| input)


echangeToEchangeInput : Echange -> EchangeInput
echangeToEchangeInput echange =
    { id = Data.Echange.id echange
    , typeEchange = Data.Echange.type_ echange
    , dateEchange = Data.Echange.date echange |> Date.toIsoString
    , nom = Data.Echange.titre echange
    , description = Data.Echange.description echange
    , demandes = Data.Echange.demandes echange
    , entite = Data.Echange.entite echange
    , brouillonId = Nothing
    , brouillonSuppression = False
    }


validate : ( input -> field, String, field -> Result error value ) -> input -> Result (Dict String (List error)) value
validate ( accessor, name, validator ) object =
    case validator <| accessor object of
        Ok v ->
            Ok v

        Err err ->
            Err <| Dict.insert name [ err ] Dict.empty


keepErrors : Result (Dict String (List error)) value -> Result (Dict String (List error)) (value -> nextStep) -> Result (Dict String (List error)) nextStep
keepErrors value function =
    case ( value, function ) of
        ( Ok v, Ok f ) ->
            Ok (f v)

        ( Err errs, Ok _ ) ->
            Err errs

        ( Ok _, Err errs ) ->
            Err errs

        ( Err errV, Err errF ) ->
            Err <| Dict.merge (\k v d -> Dict.insert k v d) (\k v1 v2 d -> Dict.insert k (v1 ++ v2) d) (\k v d -> Dict.insert k v d) errV errF Dict.empty


typeEchanges : List TypeEchange
typeEchanges =
    [ Data.Echange.Telephone
    , Data.Echange.Email
    , Data.Echange.Rencontre
    , Data.Echange.Courrier
    ]


echangeToDisplay : TypeEchange -> String
echangeToDisplay echange =
    case echange of
        Data.Echange.Telephone ->
            "Téléphone"

        Data.Echange.Email ->
            "Email"

        Data.Echange.Rencontre ->
            "Rencontre"

        Data.Echange.Courrier ->
            "Courrier"


viewAuthor : Echange -> Html msg
viewAuthor echange =
    echange
        |> Data.Echange.auteur
        |> text
        |> List.singleton
        |> div [ class "grey-text uppercase" ]


getDemandeInDemandes : List Demande -> EntityId DemandeId -> Maybe Demande
getDemandeInDemandes demandes demandeId =
    demandes
        |> List.filter (Data.Demande.id >> (==) demandeId)
        |> List.head


viewEchangeDemandes : List Demande -> EchangeInput -> Html msg
viewEchangeDemandes demandes echange =
    case echange.demandes of
        [] ->
            nothing

        ds ->
            ds
                |> List.filterMap (getDemandeInDemandes demandes)
                |> List.Extra.uniqueBy Data.Demande.nom
                |> List.map demandeNomToTag
                |> DSFR.Tag.medium


echangeTypeToIcon : TypeEchange -> DSFR.Icons.IconName
echangeTypeToIcon echangeType =
    case echangeType of
        Data.Echange.Telephone ->
            DSFR.Icons.Device.phoneLine

        Data.Echange.Email ->
            DSFR.Icons.custom "ri-at-line"

        Data.Echange.Rencontre ->
            DSFR.Icons.User.userLine

        Data.Echange.Courrier ->
            DSFR.Icons.Business.mailLine


viewEchangeMetadata : Zone -> Echange -> Html msg
viewEchangeMetadata timezone echange =
    div [ Typo.textSm, class "flex flex-row gap-2 !mb-0" ]
        [ div [ class "blue-text" ] <|
            List.singleton <|
                DSFR.Icons.icon <|
                    echangeTypeToIcon <|
                        Data.Echange.type_ <|
                            echange
        , viewAuthor echange
        , div [] [ text <| Lib.Date.formatDateShort <| Data.Echange.date echange ]
        , viewIf (Data.Echange.modification echange /= Nothing) <| text " - "
        , case Data.Echange.modification echange of
            Nothing ->
                nothing

            Just ( auteur, date ) ->
                div [ class "flex flex-col text-right" ]
                    [ div
                        [ Html.Attributes.title <|
                            "Dernière modification de l'échange le "
                                ++ Lib.Date.formatShort timezone date
                                ++ " par "
                                ++ auteur
                        , Typo.textSm
                        , class "!mb-0"
                        ]
                        [ text "Modifié "
                        , DSFR.Icons.iconSM DSFR.Icons.System.informationLine
                        ]
                    ]
        ]


view : { edit : EchangeInput -> msg, delete : EchangeInput -> msg } -> Zone -> List Demande -> Echange -> Html msg
view { edit, delete } timezone demandes echange =
    div [ class "flex flex-col gap-4" ] <|
        [ viewEchangeDemandes demandes <| echangeToEchangeInput <| echange
        , viewEchangeMetadata timezone echange
        , h4 [ class "whitespace-pre-wrap !mb-0" ]
            [ span [ class "font-bold" ]
                [ text <| withEmptyAs "-" <| Data.Echange.titre echange ]
            ]
        , p [ class "whitespace-pre-wrap !mb-0" ]
            [ text <|
                withEmptyAs "-" <|
                    Data.Echange.description echange
            ]
        , viewLienEntite "echanges" <| Data.Echange.entite echange
        , [ DSFR.Button.new
                { onClick = Just <| edit <| echangeToEchangeInput <| echange
                , label = "Modifier"
                }
                |> DSFR.Button.leftIcon DSFR.Icons.Design.editFill
                |> DSFR.Button.withAttrs [ class "!mb-0" ]
                |> DSFR.Button.secondary
          , DSFR.Button.new
                { onClick = Just <| delete <| echangeToEchangeInput <| echange
                , label = "Supprimer"
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.deleteLine
                |> DSFR.Button.withAttrs [ class "!mb-0" ]
                |> DSFR.Button.tertiary
          ]
            |> DSFR.Button.group
            |> DSFR.Button.inline
            |> DSFR.Button.iconsLeft
            |> DSFR.Button.viewGroup
        ]


updateEchangeForEntite : Bool -> Maybe Entite -> Maybe (EntityId EchangeId) -> String
updateEchangeForEntite global entite =
    case entite of
        Nothing ->
            \_ -> ""

        Just e ->
            case e of
                Etab siret _ ->
                    Api.upsertEchangeEtablissement global siret

                EtabCrea etablissementCreationId _ ->
                    Api.upsertEchangeEtablissementCreation global etablissementCreationId

                Loc localId _ ->
                    Api.upsertEchangeLocal global localId


encodeEchangeInput : EchangeInput -> Value
encodeEchangeInput echangeInput =
    [ ( "echange"
      , Encode.object
            [ ( "typeId", Data.Echange.encodeTypeEchange echangeInput.typeEchange )
            , ( "date", Encode.string <| echangeInput.dateEchange )
            , ( "nom", Encode.string echangeInput.nom )
            , ( "description", Encode.string echangeInput.description )
            , ( "demandeIds", Encode.list Api.EntityId.encodeEntityId echangeInput.demandes )
            , ( "brouillonId"
              , Maybe.withDefault Encode.null <|
                    Maybe.map Api.EntityId.encodeEntityId <|
                        echangeInput.brouillonId
              )
            , ( "brouillonSuppression", Encode.bool echangeInput.brouillonSuppression )
            ]
      )
    ]
        |> Encode.object
