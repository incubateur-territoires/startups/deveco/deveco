port module UI.Scroll exposing (click, scrollIntoView)


port scrollIntoView : String -> Cmd msg


port click : String -> Cmd msg
