module UI.Entite exposing (badgeAncienCreateur, badgeFrance, badgeInactif, badgeProcedure, badgeSiege, displayIconeCreateur, iconeCreateur, iconeEtablissement, iconeLocal, iconesPortefeuilleInfos)

import Accessibility exposing (Html, div, span, text)
import DSFR.Badge
import DSFR.Color
import DSFR.Icons
import DSFR.Icons.Buildings
import DSFR.Icons.Business
import DSFR.Icons.Communication
import DSFR.Icons.System
import Data.Etablissement exposing (EtatAdministratif)
import Data.PortefeuilleInfos
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing, viewIf)


iconeEtablissement : DSFR.Icons.IconName
iconeEtablissement =
    DSFR.Icons.Buildings.hotelLine


iconeCreateur : DSFR.Icons.IconName
iconeCreateur =
    DSFR.Icons.Business.briefcaseLine


iconeLocal : DSFR.Icons.IconName
iconeLocal =
    DSFR.Icons.Buildings.storeFill


displayIconeCreateur : Html msg
displayIconeCreateur =
    iconeCreateur
        |> DSFR.Icons.iconLG
        |> List.singleton
        |> span [ Html.Attributes.title "Créateur d'entreprise" ]


badgeInactif : Bool -> Maybe EtatAdministratif -> Html msg
badgeInactif signaleFerme maybeEtatAdministratif =
    (if signaleFerme then
        Just "Signalé fermé"

     else
        maybeEtatAdministratif
            |> Maybe.andThen
                (\etatAdministratif ->
                    case etatAdministratif of
                        Data.Etablissement.Inactif ->
                            Just "Fermé"

                        _ ->
                            Nothing
                )
    )
        |> Maybe.map (text >> DSFR.Badge.system { context = DSFR.Badge.Error, withIcon = False } >> DSFR.Badge.badgeMD)
        |> Maybe.withDefault nothing


badgeFrance : Bool -> Html msg
badgeFrance france =
    if france then
        text "France"
            |> DSFR.Badge.system { context = DSFR.Badge.New, withIcon = False }
            |> DSFR.Badge.badgeMD

    else
        nothing


badgeSiege : Bool -> Html msg
badgeSiege siege =
    if siege then
        text "Siège"
            |> DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = False }
            |> DSFR.Badge.badgeMD

    else
        nothing


badgeAncienCreateur : List etablissement -> Html msg
badgeAncienCreateur etablissementsCrees =
    case etablissementsCrees of
        [] ->
            nothing

        _ ->
            text "Ancien créateur"
                |> DSFR.Badge.default
                |> DSFR.Badge.withColor DSFR.Color.brownCaramel
                |> DSFR.Badge.badgeMD


badgeProcedure : Bool -> Bool -> Html msg
badgeProcedure full procedure =
    if procedure then
        (if full then
            "Procédure collective"

         else
            "PCL"
        )
            |> text
            |> DSFR.Badge.system { context = DSFR.Badge.Warning, withIcon = True }
            |> DSFR.Badge.withAttrs [ Html.Attributes.title "Procédure collective en cours" ]
            |> DSFR.Badge.badgeMD

    else
        nothing


iconesPortefeuilleInfos : Data.PortefeuilleInfos.PortefeuilleInfos -> Html msg
iconesPortefeuilleInfos infos =
    if infos.contacts || infos.echanges || infos.demandes || infos.rappels then
        div [ class "w-full flex flex-row gap-2" ]
            [ viewIf infos.contacts <|
                span [ Html.Attributes.title "Contacts qualifiés", class "inline-flex" ] <|
                    List.singleton <|
                        DSFR.Icons.iconMD <|
                            DSFR.Icons.custom <|
                                "ri-contacts-book-2-line"
            , viewIf infos.echanges <|
                span [ Html.Attributes.title "Échanges consignés", class "inline-flex" ] <|
                    List.singleton <|
                        DSFR.Icons.iconMD DSFR.Icons.Communication.discussLine
            , viewIf infos.demandes <|
                span [ Html.Attributes.title "Demandes exprimées", class "inline-flex" ] <|
                    List.singleton <|
                        DSFR.Icons.iconMD DSFR.Icons.Communication.questionnaireLine
            , viewIf infos.rappels <|
                span [ Html.Attributes.title "Rappels en cours", class "inline-flex" ] <|
                    List.singleton <|
                        DSFR.Icons.iconMD DSFR.Icons.System.timerLine
            , text "\u{00A0}"
            ]

    else
        nothing
