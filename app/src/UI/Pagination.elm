module UI.Pagination exposing (Pagination, defaultPagination, paginationToQuery, queryToPagination)

import QS


type alias Pagination =
    { page : Int
    , pages : Int
    , results : Int
    }


defaultPagination : Pagination
defaultPagination =
    { page = 1
    , pages = 1
    , results = 0
    }


queryToPagination : String -> Pagination
queryToPagination rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    p =
                        query
                            |> QS.getAsStringList "page"
                            |> List.head
                            |> Maybe.andThen String.toInt
                            |> Maybe.withDefault 1

                    pages =
                        query
                            |> QS.getAsStringList "pages"
                            |> List.head
                            |> Maybe.andThen String.toInt
                            |> Maybe.withDefault 1
                in
                { defaultPagination
                    | page = p
                    , pages = pages
                }
           )


paginationToQuery : Pagination -> QS.Query
paginationToQuery pagination =
    let
        setPage =
            if pagination.page > 1 then
                QS.setStr "page" <|
                    String.fromInt <|
                        pagination.page

            else
                identity
    in
    QS.empty
        |> setPage
