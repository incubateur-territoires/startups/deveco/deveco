module UI.Contact exposing (ContactAction(..), ContactField, NewContactForm, columnWrapper, encodeNewContactForm, existantVide, newContactFormPourExistant, updateContact, updateContactForm, viewContacts, viewContactsHelper, viewCreateurs, viewDeleteContactBody, viewDeleteContactFormBody, viewEditContact, viewFonctions, viewNewContactFormFields)

import Accessibility exposing (Html, br, div, formWithListeners, h3, h4, hr, label, p, span, text)
import Api.EntityId exposing (EntityId, entityIdToString)
import DSFR.Alert
import DSFR.Button
import DSFR.Checkbox
import DSFR.Grid
import DSFR.Icons
import DSFR.Icons.Business
import DSFR.Icons.Design
import DSFR.Icons.System
import DSFR.Input
import DSFR.Radio
import DSFR.SearchBar
import DSFR.Typography as Typo
import Data.Contact exposing (Contact, ContactInputType(..), EntiteLien(..), FonctionContact(..), encodeContact)
import Data.Personne exposing (Personne, PersonneId, displayPrenomNomPersonne, emptyPersonne)
import Date exposing (Date)
import Html
import Html.Attributes exposing (class, classList, id)
import Html.Events as Events
import Html.Extra exposing (nothing, viewIf)
import Json.Encode as Encode
import Lib.Date
import Lib.UI exposing (displayPhone, withEmptyAs)
import RemoteData as RD exposing (WebData)
import Route


type ContactAction
    = None
    | ViewContactFonctions (List FonctionContact)
    | NewContact NewContactForm
    | DeleteContact Contact (List FonctionContact)
    | DeleteContactForm NewContactForm
    | EditContact Contact Contact
    | EditContactForm NewContactForm NewContactForm


type alias NewContactForm =
    { fonction : Maybe EntiteLien
    , personneNouvelle : Personne
    , personneExistante : PersonneExistanteData
    , modeCreation : Bool
    , id : Int
    }


{-| Personne
-}
type ContactField
    = Fonction
    | NiveauDiplome
    | SituationProfessionnelle
    | Prenom
    | Nom
    | Email
    | Telephone
    | Telephone2
    | DateDeNaissance
    | DemarchageAccepte


type alias PersonneExistanteData =
    { recherche : String
    , resultats : WebData (List Personne)
    , selectionne : Maybe Personne
    }


existantVide : PersonneExistanteData
existantVide =
    { recherche = ""
    , resultats = RD.NotAsked
    , selectionne = Nothing
    }


newContactFormPourExistant : Int -> { fonction : Maybe EntiteLien, prenom : String, nom : String, dateDeNaissance : Maybe Date } -> NewContactForm
newContactFormPourExistant prochainId { fonction, prenom, nom, dateDeNaissance } =
    { fonction = fonction
    , personneNouvelle =
        { emptyPersonne
            | prenom = prenom
            , nom = nom
            , naissanceDate = dateDeNaissance
        }
    , personneExistante =
        { existantVide
            | recherche =
                [ nom, prenom ]
                    |> List.filter (\s -> s /= "")
                    |> String.join " "
        }
    , modeCreation = False
    , id = prochainId
    }


contactFormFields : (ContactField -> String -> msg) -> { contact | personne : Personne } -> List (Html msg)
contactFormFields updateMsg { personne } =
    [ div [ DSFR.Grid.col6 ]
        [ DSFR.Input.new { value = personne.nom, onInput = updateMsg Nom, label = text "Nom", name = "nouveau-contact-nom" }
            |> DSFR.Input.withHint [ text "Format attendu\u{00A0}: Hautbois-Dorman" ]
            |> DSFR.Input.view
        ]
    , div [ DSFR.Grid.col6 ]
        [ DSFR.Input.new { value = personne.prenom, onInput = updateMsg Prenom, label = text "Prénom", name = "nouveau-contact-prenom" }
            |> DSFR.Input.withHint [ text "Format attendu\u{00A0}: Abel" ]
            |> DSFR.Input.view
        ]
    , div [ DSFR.Grid.col12 ]
        [ DSFR.Input.new { value = personne.email, onInput = updateMsg Email, label = text "Email", name = "nouveau-contact-email" }
            |> DSFR.Input.view
        ]
    , div [ DSFR.Grid.col6 ]
        [ DSFR.Input.new { value = personne.telephone, onInput = updateMsg Telephone, label = text "Téléphone", name = "nouveau-contact-telephone" }
            |> DSFR.Input.withHint [ text "Format attendu\u{00A0}: 06 11 22 33 44" ]
            |> DSFR.Input.view
        ]
    , div [ DSFR.Grid.col6 ]
        [ DSFR.Input.new { value = personne.telephone2, onInput = updateMsg Telephone2, label = text "Téléphone 2", name = "nouveau-contact-telephone-2" }
            |> DSFR.Input.withHint [ text "Format attendu\u{00A0}: 06 11 22 33 44" ]
            |> DSFR.Input.view
        ]
    , div [ DSFR.Grid.col6 ]
        [ DSFR.Input.new
            { value =
                personne.naissanceDate
                    |> Maybe.map Date.toIsoString
                    |> Maybe.withDefault ""
            , onInput = updateMsg DateDeNaissance
            , label = text "Date de naissance"
            , name = "nouveau-contact-date-de-naissance"
            }
            |> DSFR.Input.date { min = Nothing, max = Nothing }
            |> DSFR.Input.view
        ]
    , div [ DSFR.Grid.col12 ]
        [ DSFR.Checkbox.single
            { value = "demarchage-accepte"
            , checked = Just personne.demarchageAccepte
            , valueAsString = identity
            , id = "nouveau-contact-demarchage-accepte"
            , label = text "La personne accepte d'être contactée par mon équipe"
            , onChecked =
                \_ bool ->
                    updateMsg DemarchageAccepte <|
                        if bool then
                            "oui"

                        else
                            "non"
            }
            |> DSFR.Checkbox.viewSingle
        ]
    ]


contactFormButtons : msg -> Bool -> WebData entity -> Bool -> EntityId PersonneId -> List (Html msg)
contactFormButtons cancelMsg new request disabled personneId =
    let
        ( label, action ) =
            if new then
                ( "Ajouter", "nouveau" )

            else
                ( "Modifier", "modifier" )
    in
    [ div [ DSFR.Grid.col12, class "flex flex-col !pt-4" ]
        [ [ DSFR.Button.new { onClick = Nothing, label = label }
                |> DSFR.Button.withDisabled (disabled || request == RD.Loading)
                |> DSFR.Button.submit
                |> DSFR.Button.withAttrs
                    [ id <| "confirmer-" ++ action ++ "-contact-" ++ entityIdToString personneId
                    , Html.Attributes.title label
                    ]
          , DSFR.Button.new { onClick = Just <| cancelMsg, label = "Annuler" }
                |> DSFR.Button.secondary
                |> DSFR.Button.withAttrs
                    [ id <| "annuler-" ++ action ++ "-contact-" ++ entityIdToString personneId
                    , Html.Attributes.title "Annuler"
                    ]
          ]
            |> DSFR.Button.group
            |> DSFR.Button.inline
            |> DSFR.Button.alignedRightInverted
            |> DSFR.Button.viewGroup
        ]
    , div [ class "flex flex-row justify-end w-full" ]
        [ case request of
            RD.Failure _ ->
                DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite" }
                    |> DSFR.Alert.alert Nothing DSFR.Alert.error

            _ ->
                nothing
        ]
    ]


{-| @TODO fusionner avec la fonction du dessous
-}
updateContact : ContactField -> String -> Contact -> Contact
updateContact field value contact =
    case field of
        Fonction ->
            case contact.fonction of
                Nothing ->
                    contact

                Just (FonctionEtablissement fe) ->
                    { contact
                        | fonction =
                            fe
                                |> (\f -> { f | fonction = value })
                                |> FonctionEtablissement
                                |> Just
                    }

                Just (FonctionEtablissementCreation fe) ->
                    { contact
                        | fonction =
                            fe
                                |> (\f -> { f | fonction = value })
                                |> FonctionEtablissementCreation
                                |> Just
                    }

                Just (FonctionLocal fe) ->
                    { contact
                        | fonction =
                            fe
                                |> (\f -> { f | fonction = value })
                                |> FonctionLocal
                                |> Just
                    }

                Just (FonctionCreateur fe) ->
                    { contact
                        | fonction =
                            fe
                                |> (\f -> { f | fonction = value })
                                |> FonctionCreateur
                                |> Just
                    }

        NiveauDiplome ->
            case contact.fonction of
                Nothing ->
                    contact

                Just (FonctionEtablissement _) ->
                    contact

                Just (FonctionEtablissementCreation _) ->
                    contact

                Just (FonctionLocal _) ->
                    contact

                Just (FonctionCreateur fe) ->
                    { contact
                        | fonction =
                            fe
                                |> (\f -> { f | niveauDiplome = value })
                                |> FonctionCreateur
                                |> Just
                    }

        SituationProfessionnelle ->
            case contact.fonction of
                Nothing ->
                    contact

                Just (FonctionEtablissement _) ->
                    contact

                Just (FonctionEtablissementCreation _) ->
                    contact

                Just (FonctionLocal _) ->
                    contact

                Just (FonctionCreateur fe) ->
                    { contact
                        | fonction =
                            fe
                                |> (\f -> { f | situationProfessionnelle = value })
                                |> FonctionCreateur
                                |> Just
                    }

        Prenom ->
            { contact | personne = contact.personne |> (\p -> { p | prenom = value }) }

        Nom ->
            { contact | personne = contact.personne |> (\p -> { p | nom = value }) }

        Email ->
            { contact | personne = contact.personne |> (\p -> { p | email = value }) }

        Telephone ->
            { contact | personne = contact.personne |> (\p -> { p | telephone = value }) }

        Telephone2 ->
            { contact | personne = contact.personne |> (\p -> { p | telephone2 = value }) }

        DateDeNaissance ->
            { contact
                | personne =
                    contact.personne
                        |> (\p ->
                                { p
                                    | naissanceDate = value |> Date.fromIsoString |> Result.toMaybe
                                }
                           )
            }

        DemarchageAccepte ->
            { contact
                | personne =
                    contact.personne
                        |> (\p ->
                                { p
                                    | demarchageAccepte = value == "oui"
                                }
                           )
            }


{-| @TODO fusionner avec la fonction du dessus
-}
updateContactForm : ContactField -> String -> NewContactForm -> NewContactForm
updateContactForm field value contact =
    case field of
        Fonction ->
            case contact.fonction of
                Nothing ->
                    contact

                Just (FonctionEtablissement fe) ->
                    { contact
                        | fonction =
                            fe
                                |> (\f -> { f | fonction = value })
                                |> FonctionEtablissement
                                |> Just
                    }

                Just (FonctionEtablissementCreation fe) ->
                    { contact
                        | fonction =
                            fe
                                |> (\f -> { f | fonction = value })
                                |> FonctionEtablissementCreation
                                |> Just
                    }

                Just (FonctionLocal fe) ->
                    { contact
                        | fonction =
                            fe
                                |> (\f -> { f | fonction = value })
                                |> FonctionLocal
                                |> Just
                    }

                Just (FonctionCreateur fe) ->
                    { contact
                        | fonction =
                            fe
                                |> (\f -> { f | fonction = value })
                                |> FonctionCreateur
                                |> Just
                    }

        NiveauDiplome ->
            case contact.fonction of
                Nothing ->
                    contact

                Just (FonctionEtablissement _) ->
                    contact

                Just (FonctionEtablissementCreation _) ->
                    contact

                Just (FonctionLocal _) ->
                    contact

                Just (FonctionCreateur fe) ->
                    { contact
                        | fonction =
                            fe
                                |> (\f -> { f | niveauDiplome = value })
                                |> FonctionCreateur
                                |> Just
                    }

        SituationProfessionnelle ->
            case contact.fonction of
                Nothing ->
                    contact

                Just (FonctionEtablissement _) ->
                    contact

                Just (FonctionEtablissementCreation _) ->
                    contact

                Just (FonctionLocal _) ->
                    contact

                Just (FonctionCreateur fe) ->
                    { contact
                        | fonction =
                            fe
                                |> (\f -> { f | situationProfessionnelle = value })
                                |> FonctionCreateur
                                |> Just
                    }

        Prenom ->
            { contact | personneNouvelle = contact.personneNouvelle |> (\p -> { p | prenom = value }) }

        Nom ->
            { contact | personneNouvelle = contact.personneNouvelle |> (\p -> { p | nom = value }) }

        Email ->
            { contact | personneNouvelle = contact.personneNouvelle |> (\p -> { p | email = value }) }

        Telephone ->
            { contact | personneNouvelle = contact.personneNouvelle |> (\p -> { p | telephone = value }) }

        Telephone2 ->
            { contact | personneNouvelle = contact.personneNouvelle |> (\p -> { p | telephone2 = value }) }

        DateDeNaissance ->
            { contact
                | personneNouvelle =
                    contact.personneNouvelle
                        |> (\p ->
                                { p
                                    | naissanceDate = value |> Date.fromIsoString |> Result.toMaybe
                                }
                           )
            }

        DemarchageAccepte ->
            { contact
                | personneNouvelle =
                    contact.personneNouvelle
                        |> (\p ->
                                { p
                                    | demarchageAccepte = value == "oui"
                                }
                           )
            }


encodeNewContactForm : NewContactForm -> Encode.Value
encodeNewContactForm { fonction, personneNouvelle, personneExistante, modeCreation } =
    if not modeCreation && personneExistante.selectionne /= Nothing then
        [ ( "contactId"
          , personneExistante.selectionne
                |> Maybe.map .id
                |> Maybe.map Api.EntityId.encodeEntityId
                |> Maybe.withDefault Encode.null
          )
        , ( "type", Encode.string "existant" )
        ]
            ++ (fonction
                    |> Maybe.map Data.Contact.encodeEntiteLien
                    |> Maybe.withDefault []
               )
            |> Encode.object

    else
        { fonction = fonction
        , personne = personneNouvelle
        }
            |> encodeContact ContactAddInput


contactIsEmpty : Maybe EntiteLien -> Personne -> Bool
contactIsEmpty fonction personne =
    let
        emptyFonction =
            case fonction of
                Nothing ->
                    False

                Just (FonctionEtablissement fe) ->
                    fe.fonction == ""

                Just (FonctionEtablissementCreation fe) ->
                    fe.fonction == ""

                Just (FonctionLocal fe) ->
                    fe.fonction == ""

                Just (FonctionCreateur fe) ->
                    (fe.fonction == "")
                        && (fe.niveauDiplome == "")
                        && (fe.situationProfessionnelle == "")

        emptyPersonne =
            (personne.prenom == "")
                && (personne.nom == "")
                && (personne.email == "")
                && (personne.telephone == "")
                && (personne.telephone2 == "")
    in
    emptyFonction && emptyPersonne


hasPersonneChanged : Personne -> Personne -> Bool
hasPersonneChanged personneExistante personne =
    (personneExistante.nom /= personne.nom)
        || (personneExistante.prenom /= personne.prenom)
        || (personneExistante.email /= personne.email)
        || (personneExistante.telephone /= personne.telephone)
        || (personneExistante.telephone2 /= personne.telephone2)
        || (personneExistante.naissanceDate /= personne.naissanceDate)
        || (personneExistante.demarchageAccepte /= personne.demarchageAccepte)


modificationWarning : Bool -> Html msg
modificationWarning hasChanged =
    viewIf hasChanged
        (DSFR.Alert.small { title = Nothing, description = text "Vous avez modifié les informations d'un contact existant. Celles-ci seront mises à jour sur toutes les fiches où se trouve le contact." }
            |> DSFR.Alert.alert Nothing DSFR.Alert.warning
        )


viewEditContact :
    { confirm : msg
    , update : ContactField -> String -> msg
    , cancel : msg
    , niveauxDiplome : Maybe (List { id : String, nom : String })
    , situationsProfessionnelles : Maybe (List { id : String, nom : String })
    }
    -> WebData entity
    -> Contact
    -> Contact
    -> Html msg
viewEditContact { confirm, update, cancel, niveauxDiplome, situationsProfessionnelles } request contactExistant ({ fonction, personne } as contact) =
    let
        personneHasChanged =
            hasPersonneChanged contactExistant.personne personne

        fonctionHasChanged =
            contactExistant.fonction /= fonction

        uselessEdit =
            not (personneHasChanged || fonctionHasChanged)
    in
    formWithListeners [ Events.onSubmit <| confirm, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ] <|
        (div [ DSFR.Grid.col12 ] <|
            List.singleton <|
                case fonction of
                    Nothing ->
                        nothing

                    Just (FonctionEtablissement fe) ->
                        etablissementContactForm update fe.fonction

                    Just (FonctionEtablissementCreation fe) ->
                        etablissementContactForm update fe.fonction

                    Just (FonctionLocal fe) ->
                        localContactForm update fe.fonction

                    Just (FonctionCreateur fe) ->
                        createurContactForm
                            (niveauxDiplome |> Maybe.withDefault [])
                            (situationsProfessionnelles |> Maybe.withDefault [])
                            update
                            fe
        )
            :: contactFormFields update contact
            ++ (modificationWarning personneHasChanged
                    :: contactFormButtons cancel False request (uselessEdit || contactIsEmpty fonction personne) personne.id
               )


etablissementContactForm : (ContactField -> String -> msg) -> String -> Html msg
etablissementContactForm update fonction =
    DSFR.Input.new
        { value = fonction
        , onInput = update Fonction
        , label = text "Fonction"
        , name = "nouveau-contact-fonction"
        }
        |> DSFR.Input.view


createurContactForm : List { id : String, nom : String } -> List { id : String, nom : String } -> (ContactField -> String -> msg) -> { fonction : String, niveauDiplome : String, situationProfessionnelle : String } -> Html msg
createurContactForm niveauxDiplome situationsProfessionnelles update { fonction, niveauDiplome, situationProfessionnelle } =
    div [ class "flex flex-col" ]
        [ DSFR.Input.new
            { value = fonction
            , onInput = update Fonction
            , label = text "Fonction"
            , name = "nouveau-createur-fonction"
            }
            |> DSFR.Input.view
        , DSFR.Input.new
            { value =
                case niveauDiplome of
                    "" ->
                        " "

                    _ ->
                        niveauDiplome
            , onInput =
                \optionValue ->
                    niveauxDiplome
                        |> List.filter (\nd -> nd.id == optionValue)
                        |> List.head
                        |> Maybe.map .id
                        |> Maybe.withDefault " "
                        |> update NiveauDiplome
            , label = text "Niveau de diplôme"
            , name = "niveau-diplome-select"
            }
            |> DSFR.Input.select
                { options = niveauxDiplome |> List.map Just |> (::) Nothing
                , toId = Maybe.map .id >> Maybe.withDefault " "
                , toLabel = Maybe.map .nom >> Maybe.withDefault "Non renseigné" >> text
                , toDisabled = Nothing
                }
            |> DSFR.Input.view
        , DSFR.Input.new
            { value =
                case situationProfessionnelle of
                    "" ->
                        " "

                    _ ->
                        situationProfessionnelle
            , onInput =
                \optionValue ->
                    situationsProfessionnelles
                        |> List.filter (\sp -> sp.id == optionValue)
                        |> List.head
                        |> Maybe.map .id
                        |> Maybe.withDefault " "
                        |> update SituationProfessionnelle
            , label = text "Situation professionnelle"
            , name = "situation-professionnelle-select"
            }
            |> DSFR.Input.select
                { options = situationsProfessionnelles |> List.map Just |> (::) Nothing
                , toId = Maybe.map .id >> Maybe.withDefault " "
                , toLabel = Maybe.map .nom >> Maybe.withDefault "Non renseigné" >> text
                , toDisabled = Nothing
                }
            |> DSFR.Input.view
        ]


localContactForm : (ContactField -> String -> msg) -> String -> Html msg
localContactForm update fonction =
    DSFR.Input.new
        { value = fonction
        , onInput = update Fonction
        , label = text "Fonction"
        , name = "nouveau-contact-fonction"
        }
        |> DSFR.Input.select
            { options =
                [ ""
                , "Propriétaire"
                , "Établissement occupant"
                , "Gestionnaire locatif"
                , "Gestionnaire commercial"
                , "Gestionnaire syndic"
                ]
            , toId = identity
            , toLabel = text
            , toDisabled = Nothing
            }
        |> DSFR.Input.view


viewFonctions : (ContactAction -> msg) -> List FonctionContact -> Html msg
viewFonctions setContactAction fonctions =
    div [ class "flex flex-col gap-4" ] <|
        [ if List.length fonctions > 0 then
            div [ class "flex flex-col gap-2" ] <|
                List.map carte <|
                    fonctions

          else
            text "Aucune fonction"
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col12, class "flex flex-col !pt-4" ]
                [ [ DSFR.Button.new { onClick = Just <| setContactAction <| None, label = "Fermer" }
                        |> DSFR.Button.secondary
                        |> DSFR.Button.withId "fermer-voir-fonctions-contact"
                        |> DSFR.Button.withTooltip (text "Annuler")
                  ]
                    |> DSFR.Button.group
                    |> DSFR.Button.inline
                    |> DSFR.Button.alignedRightInverted
                    |> DSFR.Button.viewGroup
                ]
            ]
        ]


viewNewContactFormFields :
    { confirm : msg
    , update : ContactField -> String -> msg
    , cancel : msg
    , toggle : Bool -> msg
    , confirmSearch : msg
    , updateSearch : String -> msg
    , selectedPersonne : Maybe Personne -> msg
    , niveauxDiplome : Maybe (List { id : String, nom : String })
    , situationsProfessionnelles : Maybe (List { id : String, nom : String })
    }
    -> WebData entity
    -> NewContactForm
    -> Html msg
viewNewContactFormFields { confirm, update, cancel, toggle, confirmSearch, updateSearch, selectedPersonne, niveauxDiplome, situationsProfessionnelles } request { fonction, personneNouvelle, personneExistante, modeCreation } =
    formWithListeners [ Events.onSubmit <| confirm, class "flex flex-col gap-4" ]
        [ div [] <|
            List.singleton <|
                case fonction of
                    Nothing ->
                        nothing

                    Just (FonctionEtablissement fe) ->
                        etablissementContactForm update fe.fonction

                    Just (FonctionEtablissementCreation fe) ->
                        etablissementContactForm update fe.fonction

                    Just (FonctionLocal fe) ->
                        localContactForm update fe.fonction

                    Just (FonctionCreateur fe) ->
                        createurContactForm
                            (niveauxDiplome |> Maybe.withDefault [])
                            (situationsProfessionnelles |> Maybe.withDefault [])
                            update
                            fe
        , case fonction of
            Nothing ->
                nothing

            Just _ ->
                DSFR.Radio.group
                    { id = "mode-creation-radio"
                    , options = [ True, False ]
                    , current = Just <| not modeCreation
                    , toLabel =
                        text
                            << (\opt ->
                                    if opt then
                                        "Oui"

                                    else
                                        "Non"
                               )
                    , toId =
                        \opt ->
                            if opt then
                                "mode-creation-oui"

                            else
                                "mode-creation-non"
                    , msg = not >> toggle
                    , legend = Just <| text "Le contact existe déjà\u{00A0}?"
                    }
                    |> DSFR.Radio.inline
                    |> DSFR.Radio.view
        , let
            explicationNouveauContact =
                case fonction of
                    Nothing ->
                        nothing

                    Just _ ->
                        div [ DSFR.Grid.col12 ]
                            [ span [ Typo.textBold ] [ text "Nouveau contact" ]
                            , br []
                            , span [ class "fr-hint-text" ] [ text "Le contact n'existe pas encore" ]
                            ]
          in
          if modeCreation then
            div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ] <|
                explicationNouveauContact
                    :: contactFormFields update { personne = personneNouvelle }

          else
            div [ class "flex flex-col gap-4" ]
                [ existingContact { confirm = confirmSearch, update = updateSearch, selected = selectedPersonne } personneExistante
                ]
        , let
            disabled =
                if modeCreation then
                    contactIsEmpty fonction personneNouvelle

                else
                    personneExistante.selectionne == Nothing
          in
          div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col12 ] <|
                contactFormButtons cancel True request disabled personneNouvelle.id
            ]
        ]


viewDeleteContactBody :
    List FonctionContact
    ->
        { confirm : msg
        , cancel : msg
        }
    -> WebData entity
    -> EntityId PersonneId
    -> Html msg
viewDeleteContactBody autresFonctions { confirm, cancel } request personneId =
    let
        auMoinsUnCreateur =
            autresFonctions
                |> List.any
                    (\f ->
                        case f of
                            CCreateur _ ->
                                True

                            _ ->
                                False
                    )

        boutonAccepter =
            DSFR.Button.new { onClick = Just <| cancel, label = "OK" }
                |> DSFR.Button.secondary
                |> DSFR.Button.withAttrs
                    [ id <| "accepter-suppression-contact-" ++ entityIdToString personneId
                    , Html.Attributes.title "OK"
                    ]

        boutonValider =
            DSFR.Button.new { onClick = Nothing, label = "Valider la suppression" }
                |> DSFR.Button.submit
                |> DSFR.Button.withAttrs
                    [ id <| "confirmer-suppression-contact-" ++ entityIdToString personneId
                    , Html.Attributes.title "Valider la suppression"
                    ]

        boutonAnnuler =
            DSFR.Button.new { onClick = Just <| cancel, label = "Annuler" }
                |> DSFR.Button.secondary
                |> DSFR.Button.withAttrs
                    [ id <| "annuler-suppression-contact-" ++ entityIdToString personneId
                    , Html.Attributes.title "Annuler"
                    ]

        ( notice, boutons ) =
            case List.length autresFonctions of
                0 ->
                    ( [ text "Ce contact n'est présent sur aucune fiche."
                      , text " "
                      , text "Il sera supprimé définitivement."
                      ]
                    , [ boutonValider
                      , boutonAnnuler
                      ]
                    )

                1 ->
                    if auMoinsUnCreateur then
                        ( [ text "Ce contact est un Créateur d'établissement."
                          , text " "
                          , text "Pour pouvoir le supprimer, vous devez d'abord supprimer la fiche Créateur d'établissement."
                          ]
                        , [ boutonAccepter
                          ]
                        )

                    else
                        ( [ text "Ce contact est présent sur une seule fiche."
                          , text " "
                          , text "Il sera supprimé définitivement."
                          ]
                        , [ boutonValider
                          , boutonAnnuler
                          ]
                        )

                nombre ->
                    if auMoinsUnCreateur then
                        ( [ text "Ce contact est présent sur"
                          , text " "
                          , text <| String.fromInt nombre
                          , text " "
                          , text "fiches."
                          , text " "
                          , text "Il sera supprimé de toutes les fiches à l'exception des Créateurs d'établissement, que vous devrez supprimer manuellement."
                          ]
                        , [ boutonValider
                          , boutonAnnuler
                          ]
                        )

                    else
                        ( [ text "Ce contact est présent sur"
                          , text " "
                          , text <| String.fromInt nombre
                          , text " "
                          , text "fiches."
                          , text " "
                          , text "Il sera supprimé de toutes les fiches."
                          ]
                        , [ boutonValider
                          , boutonAnnuler
                          ]
                        )
    in
    formWithListeners [ Events.onSubmit <| confirm, class "flex flex-col" ] <|
        [ div [] <|
            case autresFonctions of
                [] ->
                    [ p [] <| List.singleton <| text "Souhaitez-vous supprimer le contact\u{00A0}?"
                    , p [] <| List.singleton <| text "Le contact sera supprimé uniquement sur cette fiche."
                    ]

                _ ->
                    [ p [] <|
                        List.singleton <|
                            text "Souhaitez-vous supprimer définitivement le contact\u{00A0}?"
                    , p [ class "grey-text" ] <| notice
                    ]
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col12, class "flex flex-col !pt-4" ]
                [ boutons
                    |> DSFR.Button.group
                    |> DSFR.Button.inline
                    |> DSFR.Button.alignedRightInverted
                    |> DSFR.Button.viewGroup
                ]
            , div [ class "flex flex-row justify-end w-full" ]
                [ case request of
                    RD.Failure _ ->
                        DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite" }
                            |> DSFR.Alert.alert Nothing DSFR.Alert.error

                    _ ->
                        nothing
                ]
            ]
        ]


viewDeleteContactFormBody :
    { confirm : msg
    , cancel : msg
    }
    -> Int
    -> Html msg
viewDeleteContactFormBody { confirm, cancel } contactFormId =
    let
        boutonAccepter =
            DSFR.Button.new { onClick = Just <| confirm, label = "OK" }
                |> DSFR.Button.secondary
                |> DSFR.Button.withAttrs
                    [ id <| "accepter-suppression-contact-" ++ String.fromInt contactFormId
                    , Html.Attributes.title "OK"
                    ]

        boutonAnnuler =
            DSFR.Button.new { onClick = Just <| cancel, label = "Annuler" }
                |> DSFR.Button.secondary
                |> DSFR.Button.withAttrs
                    [ id <| "annuler-suppression-contact-" ++ String.fromInt contactFormId
                    , Html.Attributes.title "Annuler"
                    ]
    in
    formWithListeners [ Events.onSubmit <| confirm, class "flex flex-col" ] <|
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col12, class "flex flex-col !pt-4" ]
                [ [ boutonAccepter, boutonAnnuler ]
                    |> DSFR.Button.group
                    |> DSFR.Button.inline
                    |> DSFR.Button.alignedRightInverted
                    |> DSFR.Button.viewGroup
                ]
            ]
        ]


existingContact : { confirm : msg, update : String -> msg, selected : Maybe Personne -> msg } -> PersonneExistanteData -> Html msg
existingContact { confirm, update, selected } { recherche, resultats, selectionne } =
    div [ class "flex flex-col gap-2" ]
        [ DSFR.SearchBar.searchBar
            { errors = []
            , extraAttrs = []
            , disabled = recherche == ""
            }
            { submitMsg = confirm
            , buttonLabel = "Rechercher"
            , inputMsg = update
            , inputLabel = "Contact existant"
            , inputPlaceholder = Nothing
            , inputId = "contact-existant-recherche"
            , inputValue = recherche
            , hints = [ text "Recherche par nom, prénom, téléphone, email" ]
            , fullLabel = Just <| span [ Typo.textBold ] <| List.singleton <| text <| "Contact existant"
            }
        , case resultats of
            RD.Failure _ ->
                div [ class "flex flex-col p-4 gap-4" ]
                    [ div [] [ text "Une erreur s'est produite. Si le problème persiste, veuillez nous contacter." ]
                    ]

            RD.Loading ->
                div [ class "flex flex-col p-4 gap-4" ]
                    [ div [] [ text "Récupération en cours..." ]
                    ]

            RD.Success [] ->
                div [ class "flex flex-col p-4 gap-4" ]
                    [ div [] [ text "Aucun résultat." ]
                    ]

            RD.Success personnes ->
                div [ class "flex flex-col gap-4 !mt-4" ]
                    [ div [] <|
                        List.singleton <|
                            text "Résultats\u{00A0}:"
                    , div [ class "flex flex-col gap-2" ] <| List.map (viewPersonneExistante selected selectionne) <| personnes
                    ]

            _ ->
                nothing
        ]


viewPersonneExistante : (Maybe Personne -> msg) -> Maybe Personne -> Personne -> Html msg
viewPersonneExistante selected selectionnee personne =
    let
        choisi =
            selectionnee
                |> Maybe.map .id
                |> Maybe.map ((==) personne.id)
                |> Maybe.withDefault False
    in
    Html.div
        [ Events.onClick <|
            selected <|
                if choisi then
                    Nothing

                else
                    Just personne
        , class "flex flex-col gap-4 p-4 border-2 cursor-pointer"
        , classList
            [ ( "dark-grey-border", not choisi )
            , ( "dark-blue-border", choisi )
            ]
        ]
        [ div [ class "flex flex-row justify-between" ]
            [ h4 [ Typo.fr_h5, class "!mb-0" ]
                [ text <|
                    withEmptyAs "-" <|
                        displayPrenomNomPersonne <|
                            { nom = personne.nom, prenom = personne.prenom }
                ]
            , span
                [ classList
                    [ ( "disabled-text", not choisi )
                    , ( "blue-text", choisi )
                    ]
                ]
                [ DSFR.Icons.iconMD DSFR.Icons.System.checkboxCircleLine
                ]
            ]
        , div []
            [ div []
                [ text "Prénom(s)\u{00A0}: "
                , span [ Typo.textBold ]
                    [ text <|
                        withEmptyAs "-" <|
                            personne.prenom
                    ]
                ]
            , div []
                [ text "Nom\u{00A0}: "
                , span [ Typo.textBold ]
                    [ text <|
                        withEmptyAs "-" <|
                            personne.nom
                    ]
                ]
            , div []
                [ text "Date de naissance\u{00A0}: "
                , span [ Typo.textBold ]
                    [ text <|
                        withEmptyAs "-" <|
                            Maybe.withDefault "" <|
                                Maybe.map Lib.Date.formatDateShort <|
                                    personne.naissanceDate
                    ]
                ]
            , div []
                [ text "Numéro de téléphone\u{00A0}: "
                , span [ Typo.textBold ]
                    [ text <|
                        withEmptyAs "-" <|
                            case ( personne.telephone, personne.telephone2 ) of
                                ( "", "" ) ->
                                    ""

                                ( t, "" ) ->
                                    t

                                ( "", t2 ) ->
                                    t2

                                ( t, t2 ) ->
                                    [ t, t2 ] |> String.join ", "
                    ]
                ]
            , div []
                [ text "Adresse e-mail\u{00A0}: "
                , span [ Typo.textBold ]
                    [ text <|
                        withEmptyAs "-" <|
                            personne.email
                    ]
                ]
            ]
        ]


viewContacts :
    Maybe EntiteLien
    -> (ContactAction -> msg)
    -> ContactAction
    -> List Contact
    -> Html msg
viewContacts =
    viewContactsHelper False


viewContactsHelper :
    Bool
    -> Maybe EntiteLien
    -> (ContactAction -> msg)
    -> ContactAction
    -> List Contact
    -> Html msg
viewContactsHelper disabled fonction setContactAction contactAction contacts =
    let
        ( titre, labelAjout, supprimable ) =
            case fonction of
                Just (FonctionCreateur _) ->
                    ( "Créateurs", "Ajouter un créateur", List.length contacts > 1 )

                _ ->
                    ( "Contacts", "Ajouter un contact", True )
    in
    div [ class "flex flex-col gap-4" ]
        (div [ class "flex flex-row justify-between", id contactsTagsId ]
            [ h3 [ Typo.fr_h4, class "flex flex-row items-center gap-2" ]
                [ text <| titre
                , viewIf (List.length contacts > 0) <|
                    span [ Typo.textMd, class "circled !mb-0" ]
                        [ text <| Lib.UI.formatIntWithThousandSpacing <| List.length contacts
                        ]
                ]
            , DSFR.Button.new
                { label = labelAjout
                , onClick =
                    Just <|
                        setContactAction <|
                            NewContact
                                { fonction = fonction
                                , personneNouvelle = emptyPersonne
                                , personneExistante = existantVide
                                , modeCreation = False
                                , id = -1
                                }
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withDisabled (disabled || contactAction /= None)
                |> DSFR.Button.view
            ]
            :: div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col ] [ columnWrapper <| label [ class "font-bold" ] [ text "Fonction" ] ]
                , div [ DSFR.Grid.col ] [ columnWrapper <| label [ class "font-bold" ] [ text "Nom" ] ]
                , div [ DSFR.Grid.col ] [ columnWrapper <| label [ class "font-bold" ] [ text "Prénom" ] ]
                , div [ DSFR.Grid.col ] [ columnWrapper <| label [ class "font-bold" ] [ text "Téléphone" ] ]
                , div [ DSFR.Grid.col ] [ columnWrapper <| label [ class "font-bold" ] [ text "Email" ] ]
                , div [ DSFR.Grid.col ] [ nothing ]
                ]
            :: hr [ class "!pb-1" ] []
            :: (if List.length contacts == 0 then
                    span [ class "text-center italic" ] [ text "Aucun contact pour l'instant" ]

                else
                    nothing
               )
            :: (List.intersperse (hr [ class "!pb-1" ] []) <|
                    List.map (viewContact disabled supprimable setContactAction) <|
                        contacts
               )
        )


viewCreateurs :
    Int
    -> Maybe EntiteLien
    -> (ContactAction -> msg)
    -> ContactAction
    -> List NewContactForm
    -> Html msg
viewCreateurs prochainId fonction setContactAction contactAction createurs =
    div [ class "flex flex-col gap-4" ]
        (div [ class "flex flex-row justify-between", id contactsTagsId ]
            [ h3 [ Typo.fr_h4, class "flex flex-row items-center gap-2" ]
                [ text "Créateurs"
                , viewIf (List.length createurs > 0) <|
                    span [ Typo.textMd, class "circled !mb-0" ]
                        [ text <| Lib.UI.formatIntWithThousandSpacing <| List.length createurs
                        ]
                ]
            , div []
                [ DSFR.Button.new
                    { label = "Ajouter un créateur"
                    , onClick =
                        Just <|
                            setContactAction <|
                                NewContact
                                    { fonction = fonction
                                    , personneNouvelle = emptyPersonne
                                    , personneExistante = existantVide
                                    , modeCreation = False
                                    , id = prochainId
                                    }
                    }
                    |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                    |> DSFR.Button.tertiaryNoOutline
                    |> DSFR.Button.withDisabled (contactAction /= None)
                    |> DSFR.Button.view
                ]
            ]
            :: div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col3 ] [ columnWrapper <| label [ class "font-bold" ] [ text "Fonction" ] ]
                , div [ DSFR.Grid.col3 ] [ columnWrapper <| label [ class "font-bold" ] [ text "Nom" ] ]
                , div [ DSFR.Grid.col3 ] [ columnWrapper <| label [ class "font-bold" ] [ text "Prénom" ] ]
                , div [ DSFR.Grid.col3 ] [ nothing ]
                ]
            :: hr [ class "!pb-1" ] []
            :: (if List.length createurs == 0 then
                    span [ class "text-center italic" ] [ text "Aucun créateur pour l'instant" ]

                else
                    nothing
               )
            :: (List.intersperse (hr [ class "!pb-1" ] []) <|
                    List.map (viewCreateur setContactAction) <|
                        createurs
               )
        )


viewContact : Bool -> Bool -> (ContactAction -> msg) -> Contact -> Html msg
viewContact disabled supprimable setContactAction contact =
    let
        buttons =
            [ [ DSFR.Button.new
                    { onClick =
                        Just <|
                            setContactAction <|
                                ViewContactFonctions contact.liens
                    , label = "Voir les fonctions du contact"
                    }
                    |> DSFR.Button.withDisabled disabled
                    |> DSFR.Button.onlyIcon DSFR.Icons.System.informationLine
                    |> DSFR.Button.withAttrs [ id <| "voir-contact-" ++ entityIdToString contact.personne.id, class "!m-[0.25rem" ]
              , DSFR.Button.new
                    { onClick =
                        Just <|
                            setContactAction <|
                                EditContact contact contact
                    , label = "Modifier le contact"
                    }
                    |> DSFR.Button.withDisabled disabled
                    |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
                    |> DSFR.Button.secondary
                    |> DSFR.Button.withAttrs [ id <| "modifier-contact-" ++ entityIdToString contact.personne.id, class "!m-[0.25rem" ]
              , DSFR.Button.new
                    { onClick =
                        Just <|
                            setContactAction <|
                                DeleteContact contact []
                    , label = "Supprimer le contact"
                    }
                    |> DSFR.Button.withDisabled (disabled || not supprimable)
                    |> (if not supprimable then
                            DSFR.Button.withTooltip (text "Impossible de supprimer le dernier créateur")

                        else
                            identity
                       )
                    |> DSFR.Button.onlyIcon DSFR.Icons.System.deleteLine
                    |> DSFR.Button.tertiary
                    |> DSFR.Button.withAttrs [ id <| "supprimer-contact-" ++ entityIdToString contact.personne.id, class "!m-[0.25rem" ]
              ]
                |> DSFR.Button.group
                |> DSFR.Button.iconsOnly
                |> DSFR.Button.inline
                |> DSFR.Button.alignedRight
                |> DSFR.Button.viewGroup
            ]
    in
    viewStaticContact buttons contact


viewCreateur : (ContactAction -> msg) -> NewContactForm -> Html msg
viewCreateur setContactAction contactForm =
    let
        buttons =
            [ [ DSFR.Button.new
                    { onClick =
                        Just <|
                            setContactAction <|
                                EditContactForm contactForm contactForm
                    , label = "Modifier le contact"
                    }
                    |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
                    |> DSFR.Button.tertiary
              , DSFR.Button.new
                    { onClick =
                        Just <|
                            setContactAction <|
                                DeleteContactForm <|
                                    contactForm
                    , label = "Supprimer le contact"
                    }
                    |> DSFR.Button.onlyIcon DSFR.Icons.System.deleteFill
                    |> DSFR.Button.tertiaryNoOutline
              ]
                |> DSFR.Button.group
                |> DSFR.Button.iconsOnly
                |> DSFR.Button.inline
                |> DSFR.Button.alignedRight
                |> DSFR.Button.viewGroup
            ]
    in
    viewStaticCreateur buttons <|
        { fonction = contactForm.fonction
        , personne =
            if contactForm.modeCreation then
                contactForm.personneNouvelle

            else
                contactForm.personneExistante.selectionne
                    |> Maybe.withDefault contactForm.personneNouvelle
        , liens = []
        }


viewStaticContact : List (Html msg) -> { contact | fonction : Maybe EntiteLien, personne : { personne | prenom : String, nom : String, email : String, telephone : String, telephone2 : String, naissanceDate : Maybe Date, demarchageAccepte : Bool } } -> Html msg
viewStaticContact buttons contact =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ columnWrapper <|
                text <|
                    withEmptyAs "-" <|
                        case contact.fonction of
                            Nothing ->
                                ""

                            Just (FonctionEtablissement fe) ->
                                fe.fonction

                            Just (FonctionEtablissementCreation fe) ->
                                fe.fonction

                            Just (FonctionLocal fe) ->
                                fe.fonction

                            Just (FonctionCreateur fe) ->
                                fe.fonction
            ]
        , div [ DSFR.Grid.col, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ columnWrapper <|
                text <|
                    withEmptyAs "-" <|
                        contact.personne.nom
            ]
        , div [ DSFR.Grid.col, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ columnWrapper <|
                text <|
                    withEmptyAs "-" <|
                        contact.personne.prenom
            ]
        , div [ DSFR.Grid.col, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ columnWrapper <|
                text <|
                    displayPhone <|
                        withEmptyAs "-" <|
                            contact.personne.telephone
            , columnWrapper <|
                text <|
                    displayPhone <|
                        withEmptyAs "-" <|
                            contact.personne.telephone2
            ]
        , div [ DSFR.Grid.col, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ columnWrapper <|
                text <|
                    withEmptyAs "-" <|
                        contact.personne.email
            ]
        , div [ DSFR.Grid.col2, class "flex flex-col !mt-[0.5rem]" ] buttons
        ]


viewStaticCreateur : List (Html msg) -> { contact | fonction : Maybe EntiteLien, personne : { personne | prenom : String, nom : String, email : String, telephone : String, telephone2 : String, naissanceDate : Maybe Date, demarchageAccepte : Bool } } -> Html msg
viewStaticCreateur buttons contact =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col3, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ columnWrapper <|
                text <|
                    withEmptyAs "-" <|
                        case contact.fonction of
                            Nothing ->
                                ""

                            Just (FonctionEtablissement fe) ->
                                fe.fonction

                            Just (FonctionEtablissementCreation fe) ->
                                fe.fonction

                            Just (FonctionLocal fe) ->
                                fe.fonction

                            Just (FonctionCreateur fe) ->
                                fe.fonction
            ]
        , div [ DSFR.Grid.col3, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ columnWrapper <|
                text <|
                    withEmptyAs "-" <|
                        contact.personne.nom
            ]
        , div [ DSFR.Grid.col3, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ columnWrapper <|
                text <|
                    withEmptyAs "-" <|
                        contact.personne.prenom
            ]
        , div [ DSFR.Grid.col3, class "flex flex-col !mt-[0.5rem]" ] buttons
        ]


contactsTagsId : String
contactsTagsId =
    "contacts-list"


columnWrapper : Html msg -> Html msg
columnWrapper =
    List.singleton
        >> div [ class "!px-[1rem]", Html.Attributes.style "overflow-wrap" "break-word" ]


carte : FonctionContact -> Html msg
carte fonction =
    let
        { entiteLabel, entiteRoute, entiteType, entiteNom } =
            case fonction of
                CEtablissement ed ->
                    { entiteLabel = "Voir la fiche établissement"
                    , entiteRoute = Route.Etablissement ( Nothing, ed.etablissementId )
                    , entiteType = ed.fonction |> withEmptyAs "-"
                    , entiteNom = ed.etablissementNom
                    }

                CCreateur cd ->
                    { entiteLabel = "Voir la fiche créateur"
                    , entiteRoute = Route.Createur ( Nothing, cd.etablissementCreationId )
                    , entiteType = "Créateur"
                    , entiteNom = cd.etablissementCreationNom
                    }

                CLocal ld ->
                    { entiteLabel = "Voir la fiche local"
                    , entiteRoute = Route.Local ( Nothing, ld.localId )
                    , entiteType = ld.fonction |> withEmptyAs "-"
                    , entiteNom = ld.localNom |> Maybe.withDefault ld.localId
                    }
    in
    div [ Typo.textSm, class "!mb-0" ]
        [ div
            [ class "flex flex-col overflow-y-auto fr-card--white p-2 border-2"
            , Html.Attributes.style "background-image" "none"
            ]
            [ div [ class "flex flex-col gap-2" ]
                [ div [ class "flex flex-row !mb-0" ]
                    [ div [ Typo.textXs, class "!mb-0 grey-text" ]
                        [ DSFR.Icons.iconSM DSFR.Icons.Business.briefcaseFill
                        , text " "
                        , text "Fonction"
                        ]
                    ]
                , div []
                    [ h4 [ Typo.fr_h5, class "!mb-0" ]
                        [ text <| entiteType
                        ]
                    , text entiteNom
                    ]
                , DSFR.Button.new
                    { label = entiteLabel
                    , onClick = Nothing
                    }
                    |> (entiteRoute
                            |> Route.toUrl
                            |> DSFR.Button.linkButton
                       )
                    |> DSFR.Button.leftIcon DSFR.Icons.System.eyeLine
                    |> DSFR.Button.secondary
                    |> DSFR.Button.view
                ]
            ]
        ]
