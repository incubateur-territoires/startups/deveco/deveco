port module UI.Theme exposing
    ( Theme(..)
    , stringToTheme
    , switchTheme
    , themeToString
    )


type Theme
    = System
    | Light
    | Dark


themeToString : Theme -> String
themeToString theme =
    case theme of
        System ->
            "system"

        Light ->
            "light"

        Dark ->
            "dark"


stringToTheme : String -> Maybe Theme
stringToTheme theme =
    case theme of
        "system" ->
            Just System

        "light" ->
            Just Light

        "dark" ->
            Just Dark

        _ ->
            Nothing


port switchTheme : String -> Cmd msg
