module UI.Etablissement exposing (carte, iconeFavoris, iconeVariationCa, iconeVariationEffectif)

import Accessibility exposing (a, div, span, text)
import DSFR.Button
import DSFR.Grid as Grid
import DSFR.Icons
import DSFR.Icons.Finance
import DSFR.Icons.Map
import DSFR.Icons.System
import DSFR.Icons.User
import DSFR.Typography as Typo
import Data.Etablissement exposing (EtablissementSimple, EtatAdministratif(..), Siret)
import Data.PortefeuilleInfos exposing (PortefeuilleInfos)
import Html exposing (Html)
import Html.Attributes as Attr exposing (class)
import Html.Attributes.Extra exposing (empty)
import Html.Extra exposing (nothing)
import Lib.Date
import Lib.UI exposing (withEmptyAs)
import Route
import UI.Entite


type alias CardMessages msg =
    { toggleFavorite : Maybe (Siret -> Bool -> msg)
    }


carte : CardMessages msg -> EtablissementSimple -> PortefeuilleInfos -> Bool -> Html msg
carte cardMessages ({ siret, adresse, dateCreationEtablissement, dateFermetureEtablissement, activiteNaf, etatAdministratif, clotureDateContribution, france, siege, procedure, inpiCaVariation, emaVariation } as etablissement) infos full =
    div [ Grid.col12, Html.Attributes.Extra.attributeIf (not full) Grid.colSm4, class "p-2" ]
        [ div [ Typo.textSm, class "!mb-0" ]
            [ div
                [ class "flex flex-col overflow-y-auto fr-card--white p-2 border-2"
                , Attr.style "background-image" "none"
                ]
                [ div [ class "flex flex-row justify-between relative p-2" ]
                    [ div [ class "flex flex-row gap-2" ]
                        [ UI.Entite.badgeInactif (not <| Nothing == clotureDateContribution) <|
                            Just <|
                                etatAdministratif
                        , UI.Entite.badgeFrance france
                        , UI.Entite.badgeSiege <|
                            siege
                        , UI.Entite.badgeProcedure False <|
                            procedure
                        , text "\u{00A0}"
                        ]
                    ]
                , div [ class "flex flex-row justify-between items-center px-2" ]
                    [ div [ class "flex flex-row gap-2" ]
                        [ UI.Entite.iconesPortefeuilleInfos infos
                        , iconeGeolocalisation etablissement
                        , iconeVariationCa inpiCaVariation
                        , iconeVariationEffectif emaVariation
                        ]
                    , iconeFavoris cardMessages etablissement <| .favori <| infos
                    ]
                , a
                    [ class "fr-card--white flex flex-col p-4 custom-hover overflow-y-auto gap-2"
                    , Attr.style "background-image" "none"
                    , Attr.href <|
                        Route.toUrl <|
                            Route.Etablissement <|
                                ( Nothing, siret )
                    , if full then
                        Attr.target "_self"

                      else
                        empty
                    ]
                    [ div []
                        [ div
                            [ Typo.textBold
                            , class "!mb-0"
                            , class "line-clamp-1"
                            , Attr.title <|
                                .nomAffichage <|
                                    etablissement
                            ]
                          <|
                            List.singleton <|
                                text <|
                                    .nomAffichage <|
                                        etablissement
                        , div
                            [ class "!mb-0"
                            , class "line-clamp-1"
                            , Attr.title <|
                                adresse
                            ]
                          <|
                            List.singleton <|
                                text <|
                                    withEmptyAs "-" <|
                                        adresse
                        ]
                    , siret
                        |> Lib.UI.infoLine (Just 1) "SIRET"
                        |> List.singleton
                        |> div [ class "!mb-0" ]
                    , activiteNaf
                        |> Maybe.withDefault "-"
                        |> Lib.UI.infoLine (Just 1) "Activité NAF"
                        |> List.singleton
                        |> div [ class "!mb-0" ]
                    , div [ class "!mb-0" ] <|
                        List.singleton <|
                            case clotureDateContribution of
                                Just clotureDate ->
                                    clotureDate
                                        |> Lib.Date.formatDateShort
                                        |> Lib.UI.infoLine (Just 1) "Date de fermeture signalée"

                                Nothing ->
                                    if etatAdministratif == Inactif then
                                        dateFermetureEtablissement
                                            |> Maybe.map Lib.Date.formatDateShort
                                            |> Maybe.withDefault "-"
                                            |> Lib.UI.infoLine (Just 1) "Date de fermeture"

                                    else
                                        dateCreationEtablissement
                                            |> Maybe.map Lib.Date.formatDateShort
                                            |> Maybe.withDefault "-"
                                            |> Lib.UI.infoLine (Just 1) "Date de création"
                    ]
                ]
            ]
        ]


iconeFavoris : CardMessages msg -> EtablissementSimple -> Bool -> Html msg
iconeFavoris { toggleFavorite } { siret } favori =
    let
        ( title, icon ) =
            if favori then
                ( "Retirer des favoris"
                , DSFR.Icons.System.starFill
                )

            else
                ( "Ajouter aux favoris"
                , DSFR.Icons.System.starLine
                )

        content =
            case toggleFavorite of
                Nothing ->
                    DSFR.Icons.iconMD icon

                Just msg ->
                    DSFR.Button.new
                        { label = title
                        , onClick =
                            Just <|
                                msg siret <|
                                    not <|
                                        favori
                        }
                        |> DSFR.Button.onlyIcon icon
                        |> DSFR.Button.withId ("ajouter-aux-favoris-" ++ siret)
                        |> DSFR.Button.withTooltip (text title)
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.view
    in
    content


iconeGeolocalisation : EtablissementSimple -> Html msg
iconeGeolocalisation { geolocalisation, geolocalisationContribution, geolocalisationEquipe } =
    let
        ( icon, txt, color ) =
            if geolocalisationContribution /= Nothing then
                ( DSFR.Icons.Map.mapPin2Fill, "géolocalisé (modifié par " ++ (geolocalisationEquipe |> Maybe.withDefault "") ++ ")", "blue-text" )

            else if geolocalisation /= Nothing then
                ( DSFR.Icons.Map.mapPin2Line, "géolocalisé", "blue-text" )

            else
                ( DSFR.Icons.Map.mapPin2Line, "non géolocalisé", "red-text" )
    in
    div
        [ class "!mb-0"
        , Attr.title <| "Établissement " ++ txt
        ]
        [ span [ class color ] [ DSFR.Icons.iconMD icon ]
        ]


iconeVariationEffectif : Maybe Float -> Html msg
iconeVariationEffectif =
    iconeVariation "Effectif" DSFR.Icons.User.teamFill


iconeVariationCa : Maybe Float -> Html msg
iconeVariationCa =
    iconeVariation "Chiffre d'affaire" DSFR.Icons.Finance.moneyEuroCircleFill


iconeVariation : String -> DSFR.Icons.IconName -> Maybe Float -> Html msg
iconeVariation typeTexte typeIcon variation =
    case variation of
        Nothing ->
            nothing

        Just var ->
            let
                params =
                    if var < -10 then
                        Just { tendanceIcon = "↘", tendanceTexte = "en baisse", variationTexte = Lib.UI.formatFloatWithThousandSpacing var, color = "red-text" }

                    else if var > 10 then
                        Just { tendanceIcon = "↗", tendanceTexte = "en hausse", variationTexte = Lib.UI.formatFloatWithThousandSpacing var, color = "blue-text" }

                    else
                        Nothing
            in
            case params of
                Nothing ->
                    nothing

                Just { tendanceIcon, tendanceTexte, variationTexte, color } ->
                    div
                        [ class "!mb-0"
                        , Attr.title <| typeTexte ++ " " ++ tendanceTexte ++ "\u{00A0}: " ++ variationTexte ++ "\u{00A0}%"
                        ]
                        [ span [ class color, class "flex flex-row" ]
                            [ DSFR.Icons.iconMD typeIcon
                            , span [ Typo.textXl, Typo.textBold, class "!mb-0 relative top-[-0.3rem]" ] [ text tendanceIcon ]
                            ]
                        ]
