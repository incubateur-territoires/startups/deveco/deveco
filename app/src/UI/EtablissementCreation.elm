module UI.EtablissementCreation exposing (DataEtablissementCreation, FormErrors, ProjetField(..), Sources, badgeCreationAbandonnee, createursEtablissementBloc, decodeSources, defaultDataEtablissementCreation, encodeDataEtablissementCreation, informationsProjetBloc, premierContactBloc, updateProjet, updateProjetField)

import Accessibility exposing (Html, div, h2, p, span, sup, text)
import DSFR.Alert
import DSFR.Badge
import DSFR.Button
import DSFR.Grid
import DSFR.Icons
import DSFR.Icons.System
import DSFR.Input
import DSFR.Radio
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Commune exposing (Commune)
import Data.Contact exposing (EntiteLien(..))
import Data.GenericSource exposing (GenericSource, decodeGenericSource)
import Data.Personne exposing (emptyPersonne)
import Dict exposing (Dict)
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing)
import Html.Lazy as Lazy
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode exposing (Value)
import Lib.UI exposing (encodeSpaceToEmptyString)
import Lib.Variables exposing (hintActivites, hintMotsCles, hintZoneGeographique)
import MultiSelectRemote
import RemoteData as RD exposing (WebData)
import UI.Contact exposing (ContactAction(..), NewContactForm, encodeNewContactForm, existantVide)


type alias DataEtablissementCreation =
    { contactOrigine : Maybe GenericSource
    , contactDate : Maybe String
    , createurs : List NewContactForm
    , projet : Projet
    }


defaultDataEtablissementCreation : DataEtablissementCreation
defaultDataEtablissementCreation =
    { contactOrigine = Nothing
    , contactDate = Nothing
    , createurs = []
    , projet = defaultProjet
    }


type alias Projet =
    { projetType : String
    , futureEnseigne : String
    , description : String
    , secteurActivite : String
    , formeJuridique : String
    , sourceFinancement : String
    , contratAccompagnement : Bool
    , activites : List String
    , mots : List String
    , localisations : List String
    , communes : List Commune
    , rechercheLocal : Bool
    , commentaires : String
    }


defaultProjet : Projet
defaultProjet =
    { projetType = ""
    , futureEnseigne = ""
    , description = ""
    , secteurActivite = ""
    , formeJuridique = ""
    , sourceFinancement = ""
    , contratAccompagnement = False
    , activites = []
    , mots = []
    , localisations = []
    , communes = []
    , rechercheLocal = False
    , commentaires = ""
    }


type alias Sources =
    { projetTypes : List GenericSource
    , contactOrigines : List GenericSource
    , secteursActivite : List GenericSource
    , sourcesFinancement : List GenericSource
    , formesJuridiques : List GenericSource
    , niveauxDiplome : List GenericSource
    , situationsProfessionnelles : List GenericSource
    }


type ProjetField
    = ProjetType
    | ProjetFutureEnseigne
    | ProjetDescription
    | ProjetSecteurActivite
    | ProjetFormeJuridique
    | ProjetSourceFinancement
    | ProjetCommentaires


type alias FormErrors =
    { global : Maybe String
    , fields : Dict String (List String)
    }


formErrorsFor : String -> FormErrors -> Maybe (List String)
formErrorsFor key { fields } =
    fields
        |> Dict.get key


premierContactBloc :
    { updateContactOrigine : Maybe GenericSource -> msg
    , updateContactDate : Maybe String -> msg
    }
    -> WebData Sources
    -> DataEtablissementCreation
    -> Html msg
premierContactBloc { updateContactOrigine, updateContactDate } sources dataEtablissementCreation =
    let
        contactOrigines =
            sources |> RD.map .contactOrigines |> RD.withDefault []
    in
    div [ class "flex flex-col gap-4" ]
        [ div [ class "flex font-bold" ]
            [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue !mb-0" ]
                [ text "Premier contact" ]
            ]
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col6 ]
                [ DSFR.Input.new
                    { value =
                        dataEtablissementCreation.contactOrigine
                            |> Maybe.map .id
                            |> Maybe.withDefault " "
                    , onInput =
                        \optionValue ->
                            contactOrigines
                                |> List.filter (\et -> et.id == optionValue)
                                |> List.head
                                |> updateContactOrigine
                    , label = text "Origine du contact"
                    , name = "contact-origine-select"
                    }
                    |> DSFR.Input.select
                        { options = contactOrigines |> List.map Just |> (::) Nothing
                        , toId = Maybe.map .id >> Maybe.withDefault " "
                        , toLabel = Maybe.map .nom >> Maybe.withDefault "Non renseigné" >> text
                        , toDisabled = Nothing
                        }
                    |> DSFR.Input.view
                ]
            , div [ DSFR.Grid.col6 ]
                [ DSFR.Input.new
                    { value = dataEtablissementCreation.contactDate |> Maybe.withDefault ""
                    , onInput = Just >> updateContactDate
                    , label = text "Date du contact"
                    , name = "contact-date-select"
                    }
                    |> DSFR.Input.date { min = Nothing, max = Nothing }
                    |> DSFR.Input.view
                ]
            ]
        , DSFR.Alert.small
            { title = Nothing
            , description =
                div [ class "flex flex-col gap-4" ]
                    [ p []
                        [ text "Le contenu du premier échange avec le porteur sera à ajouter dans l'onglet \"Suivi de la relation\" après avoir complété et créé cette fiche"
                        ]
                    ]
            }
            |> DSFR.Alert.alert Nothing DSFR.Alert.info
        ]


informationsProjetBloc :
    { updatedProjet : ProjetField -> String -> msg
    , updateContratAccompagnement : Bool -> msg
    , unselectActivite : String -> msg
    , unselectLocalisation : String -> msg
    , unselectMot : String -> msg
    , unselectCommune : Commune -> msg
    , updateRechercheLocal : Bool -> msg
    }
    -> RD.RemoteData FormErrors ()
    -> MultiSelectRemote.SmartSelect msg String
    -> MultiSelectRemote.SmartSelect msg String
    -> MultiSelectRemote.SmartSelect msg String
    -> MultiSelectRemote.SmartSelect msg Commune
    -> WebData Sources
    -> DataEtablissementCreation
    -> Html msg
informationsProjetBloc { updatedProjet, updateContratAccompagnement, unselectActivite, unselectLocalisation, unselectMot, unselectCommune, updateRechercheLocal } validationFiche selectActivites selectLocalisations selectMots selectCommunes sources dataEtablissementCreation =
    let
        formErrors =
            case validationFiche of
                RD.Failure errors ->
                    Just errors

                _ ->
                    Nothing
    in
    div [ class "flex flex-col gap-4" ]
        [ div [ class "flex font-bold" ]
            [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue !mb-0" ]
                [ text "Informations sur le projet" ]
            ]
        , div [ class "flex flex-col gap-4" ]
            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col6 ]
                    [ DSFR.Input.new
                        { value =
                            if dataEtablissementCreation.projet.projetType == "" then
                                " "

                            else
                                dataEtablissementCreation.projet.projetType
                        , onInput = updatedProjet ProjetType
                        , label = text "Type de projet"
                        , name = "type-projet"
                        }
                        |> DSFR.Input.select
                            { options =
                                sources
                                    |> RD.map .projetTypes
                                    |> RD.withDefault []
                                    |> List.map Just
                                    |> (::) Nothing
                            , toId = Maybe.map .id >> Maybe.withDefault " "
                            , toLabel = Maybe.map .nom >> Maybe.withDefault "Non renseigné" >> text
                            , toDisabled = Nothing
                            }
                        |> DSFR.Input.view
                    ]
                , div [ DSFR.Grid.col6 ]
                    [ DSFR.Input.new
                        { value = dataEtablissementCreation.projet.futureEnseigne
                        , onInput = updatedProjet ProjetFutureEnseigne
                        , label = text "Nom d'enseigne pressenti"
                        , name = "nom-enseigne"
                        }
                        |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                        |> DSFR.Input.view
                    ]
                ]
            , div [ class "flex flex-col" ]
                [ DSFR.Input.new
                    { value = dataEtablissementCreation.projet.description
                    , onInput = updatedProjet ProjetDescription
                    , label = text "Description du projet (activité, produit)"
                    , name = "description-projet"
                    }
                    |> DSFR.Input.withError (formErrors |> Maybe.andThen (formErrorsFor "activite") |> Maybe.map (List.map text))
                    |> DSFR.Input.textArea (Just 2)
                    |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                    |> DSFR.Input.view
                ]
            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col6 ]
                    [ DSFR.Input.new
                        { value =
                            if dataEtablissementCreation.projet.secteurActivite == "" then
                                " "

                            else
                                dataEtablissementCreation.projet.secteurActivite
                        , onInput = updatedProjet ProjetSecteurActivite
                        , label = text "Secteur d'activité"
                        , name = "secteur-activite"
                        }
                        |> DSFR.Input.select
                            { options =
                                sources
                                    |> RD.map .secteursActivite
                                    |> RD.withDefault []
                                    |> List.map Just
                                    |> (::) Nothing
                            , toId = Maybe.map .id >> Maybe.withDefault " "
                            , toLabel = Maybe.map .nom >> Maybe.withDefault "Non renseigné" >> text
                            , toDisabled = Nothing
                            }
                        |> DSFR.Input.view
                    ]
                , div [ DSFR.Grid.col6 ]
                    [ DSFR.Input.new
                        { value =
                            if dataEtablissementCreation.projet.formeJuridique == "" then
                                " "

                            else
                                dataEtablissementCreation.projet.formeJuridique
                        , onInput = updatedProjet ProjetFormeJuridique
                        , label = text "Forme juridique envisagée"
                        , name = "forme-juridique"
                        }
                        |> DSFR.Input.select
                            { options =
                                sources
                                    |> RD.map .formesJuridiques
                                    |> RD.withDefault []
                                    |> List.map Just
                                    |> (::) Nothing
                            , toId = Maybe.map .id >> Maybe.withDefault " "
                            , toLabel = Maybe.map .nom >> Maybe.withDefault "Non renseigné" >> text
                            , toDisabled = Nothing
                            }
                        |> DSFR.Input.view
                    ]
                ]
            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col6 ]
                    [ DSFR.Input.new
                        { value =
                            if dataEtablissementCreation.projet.sourceFinancement == "" then
                                " "

                            else
                                dataEtablissementCreation.projet.sourceFinancement
                        , onInput = updatedProjet ProjetSourceFinancement
                        , label = text "Source de financement"
                        , name = "source-financement"
                        }
                        |> DSFR.Input.select
                            { options =
                                sources
                                    |> RD.map .sourcesFinancement
                                    |> RD.withDefault []
                                    |> List.map Just
                                    |> (::) Nothing
                            , toId = Maybe.map .id >> Maybe.withDefault " "
                            , toLabel = Maybe.map .nom >> Maybe.withDefault "Non renseigné" >> text
                            , toDisabled = Nothing
                            }
                        |> DSFR.Input.view
                    ]
                , div [ DSFR.Grid.col6 ]
                    [ DSFR.Radio.group
                        { id = "contrat-accompagnement-radio"
                        , options = [ True, False ]
                        , current = Just <| dataEtablissementCreation.projet.contratAccompagnement
                        , toLabel =
                            text
                                << (\opt ->
                                        if opt then
                                            "Oui"

                                        else
                                            "Non"
                                   )
                        , toId =
                            \opt ->
                                if opt then
                                    "contrat-accompagnement-oui"

                                else
                                    "contrat-accompagnement-non"
                        , msg = updateContratAccompagnement
                        , legend = Just <| text "Contrat d'accompagnement"
                        }
                        |> DSFR.Radio.inline
                        |> DSFR.Radio.view
                    ]
                ]
            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col6, class "flex flex-col gap-4" ]
                    [ selectActivites
                        |> MultiSelectRemote.viewCustom
                            { isDisabled = False
                            , selected = dataEtablissementCreation.projet.activites
                            , optionLabelFn = identity
                            , optionDescriptionFn = \_ -> ""
                            , optionsContainerMaxHeight = 300
                            , selectTitle = span [] [ text "Activités réelles et filières", sup [ Html.Attributes.title hintActivites ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                            , viewSelectedOptionFn = text
                            , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                            , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                            , noResultsForMsg =
                                \searchText ->
                                    "Aucune autre activité n'a été trouvée"
                                        ++ (if searchText == "" then
                                                ""

                                            else
                                                " pour " ++ searchText
                                           )
                            , noOptionsMsg = "Aucune activité n'a été trouvée"
                            , newOption = Just identity
                            , searchPrompt = "Rechercher une activité"
                            }
                    , dataEtablissementCreation.projet.activites
                        |> List.map (\activite -> DSFR.Tag.deletable unselectActivite { data = activite, toString = identity })
                        |> DSFR.Tag.medium
                    ]
                , div [ DSFR.Grid.col6, class "flex flex-col gap-4" ]
                    [ selectLocalisations
                        |> MultiSelectRemote.viewCustom
                            { isDisabled = False
                            , selected = dataEtablissementCreation.projet.localisations
                            , optionLabelFn = identity
                            , optionDescriptionFn = \_ -> ""
                            , optionsContainerMaxHeight = 300
                            , selectTitle = span [] [ text "Zone géographique", sup [ Html.Attributes.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                            , viewSelectedOptionFn = text
                            , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                            , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                            , noResultsForMsg =
                                \searchText ->
                                    "Aucune autre zone géographique n'a été trouvée"
                                        ++ (if searchText == "" then
                                                ""

                                            else
                                                " pour " ++ searchText
                                           )
                            , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                            , newOption = Just identity
                            , searchPrompt = "Rechercher une zone géographique"
                            }
                    , dataEtablissementCreation.projet.localisations
                        |> List.map (\localisation -> DSFR.Tag.deletable unselectLocalisation { data = localisation, toString = identity })
                        |> DSFR.Tag.medium
                    ]
                , div [ DSFR.Grid.col6, class "flex flex-col gap-4" ]
                    [ selectMots
                        |> MultiSelectRemote.viewCustom
                            { isDisabled = False
                            , selected = dataEtablissementCreation.projet.mots
                            , optionLabelFn = identity
                            , optionDescriptionFn = \_ -> ""
                            , optionsContainerMaxHeight = 300
                            , selectTitle = span [] [ text "Mots-clés", sup [ Html.Attributes.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                            , viewSelectedOptionFn = text
                            , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                            , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                            , noResultsForMsg =
                                \searchText ->
                                    "Aucun autre mot-clé n'a été trouvé"
                                        ++ (if searchText == "" then
                                                ""

                                            else
                                                " pour " ++ searchText
                                           )
                            , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                            , newOption = Just identity
                            , searchPrompt = "Rechercher un mot-clé"
                            }
                    , dataEtablissementCreation.projet.mots
                        |> List.map (\mot -> DSFR.Tag.deletable unselectMot { data = mot, toString = identity })
                        |> DSFR.Tag.medium
                    ]
                ]
            , div [ class "flex flex-col gap-4" ]
                [ selectCommunes
                    |> MultiSelectRemote.viewCustom
                        { selected = dataEtablissementCreation.projet.communes
                        , optionLabelFn = .nom
                        , isDisabled = False
                        , optionDescriptionFn = \_ -> ""
                        , optionsContainerMaxHeight = 300
                        , selectTitle = text "Communes d'implantation"
                        , searchPrompt = "Rechercher une commune"
                        , noResultsForMsg =
                            \searchText ->
                                "Aucune autre commune n'a été trouvée"
                                    ++ (if searchText == "" then
                                            ""

                                        else
                                            " pour " ++ searchText
                                       )
                        , noOptionsMsg = "Aucune commune n'a été trouvée"
                        , viewSelectedOptionFn = .id >> text
                        , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                        , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                        , newOption = Nothing
                        }
                , dataEtablissementCreation.projet.communes
                    |> List.sortBy .nom
                    |> List.map (\commune -> DSFR.Tag.deletable unselectCommune { data = commune, toString = .nom })
                    |> DSFR.Tag.medium
                ]
            , DSFR.Radio.group
                { id = "recherche-local-radio"
                , options = [ True, False ]
                , current = Just <| dataEtablissementCreation.projet.rechercheLocal
                , toLabel =
                    text
                        << (\opt ->
                                if opt then
                                    "Oui"

                                else
                                    "Non"
                           )
                , toId =
                    \opt ->
                        if opt then
                            "recherche-local-oui"

                        else
                            "recherche-local-non"
                , msg = updateRechercheLocal
                , legend = Just <| text "Recherche d'un local"
                }
                |> DSFR.Radio.inline
                |> DSFR.Radio.view
            , DSFR.Input.new { value = dataEtablissementCreation.projet.commentaires, label = span [] [ text "Commentaires" ], onInput = updatedProjet ProjetCommentaires, name = "commentaires" }
                |> DSFR.Input.textArea (Just 2)
                |> DSFR.Input.view
                |> List.singleton
                |> div []
            ]
        ]


createursEtablissementBloc : (ContactAction -> msg) -> Int -> RD.RemoteData FormErrors () -> ContactAction -> DataEtablissementCreation -> Html msg
createursEtablissementBloc setContactAction prochainId validationFiche contactAction dataEtablissementCreation =
    let
        formErrors =
            case validationFiche of
                RD.Failure errors ->
                    Just errors

                _ ->
                    Nothing
    in
    div [ class "flex flex-col gap-4" ]
        [ div [ class "flex font-bold" ]
            [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue !mb-0" ]
                [ text "Créateur(s) d'établissement" ]
            ]
        , if 0 == List.length dataEtablissementCreation.createurs then
            div [ class "flex flex-col gap-4" ]
                [ div [ class "flex flex-row justify-between items-center" ]
                    [ text "Aucun créateur pour l'instant"
                    , DSFR.Button.new
                        { label = "Ajouter"
                        , onClick =
                            Just <|
                                setContactAction <|
                                    NewContact <|
                                        { fonction =
                                            Just <|
                                                FonctionCreateur
                                                    { fonction = "Créateur"
                                                    , niveauDiplome = ""
                                                    , situationProfessionnelle = ""
                                                    }
                                        , personneNouvelle = emptyPersonne
                                        , personneExistante = existantVide
                                        , modeCreation = False
                                        , id = prochainId
                                        }
                        }
                        |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.view
                    ]
                , formErrors
                    |> Maybe.andThen (formErrorsFor "createurs")
                    |> Maybe.map (String.join ", ")
                    |> Maybe.map
                        (\errors ->
                            DSFR.Alert.small { title = Nothing, description = text errors }
                                |> DSFR.Alert.alert Nothing DSFR.Alert.error
                        )
                    |> Maybe.withDefault nothing
                ]

          else
            Lazy.lazy5 UI.Contact.viewCreateurs
                prochainId
                (Just <|
                    FonctionCreateur
                        { fonction = "Créateur"
                        , niveauDiplome = ""
                        , situationProfessionnelle = ""
                        }
                )
                setContactAction
                contactAction
                (.createurs <|
                    dataEtablissementCreation
                )
        ]


updateProjet : (Projet -> Projet) -> DataEtablissementCreation -> DataEtablissementCreation
updateProjet updater dataEtablissementCreation =
    { dataEtablissementCreation | projet = dataEtablissementCreation.projet |> updater }


updateProjetField : ProjetField -> String -> DataEtablissementCreation -> DataEtablissementCreation
updateProjetField field value dataEtablissementCreation =
    let
        updater : Projet -> Projet
        updater =
            case field of
                ProjetType ->
                    \proj -> { proj | projetType = value }

                ProjetFutureEnseigne ->
                    \proj -> { proj | futureEnseigne = value }

                ProjetDescription ->
                    \proj -> { proj | description = value }

                ProjetSecteurActivite ->
                    \proj -> { proj | secteurActivite = value }

                ProjetFormeJuridique ->
                    \proj -> { proj | formeJuridique = value }

                ProjetSourceFinancement ->
                    \proj -> { proj | sourceFinancement = value }

                ProjetCommentaires ->
                    \proj -> { proj | commentaires = value }
    in
    { dataEtablissementCreation | projet = dataEtablissementCreation.projet |> updater }


decodeSources : Decoder Sources
decodeSources =
    Decode.succeed Sources
        |> andMap (Decode.field "typesProjet" <| Decode.list <| decodeGenericSource)
        |> andMap (Decode.field "contactOrigines" <| Decode.list <| decodeGenericSource)
        |> andMap (Decode.field "secteursActivite" <| Decode.list <| decodeGenericSource)
        |> andMap (Decode.field "sourcesFinancement" <| Decode.list <| decodeGenericSource)
        |> andMap (Decode.field "categoriesJuridiques" <| Decode.list <| decodeGenericSource)
        |> andMap (Decode.field "niveauxDiplome" <| Decode.list <| decodeGenericSource)
        |> andMap (Decode.field "situationsProfessionnelles" <| Decode.list <| decodeGenericSource)


encodeDataEtablissementCreation : DataEtablissementCreation -> Value
encodeDataEtablissementCreation dataEtablissementCreation =
    [ ( "contactOrigine"
      , Encode.string <|
            Maybe.withDefault "" <|
                Maybe.map .id <|
                    dataEtablissementCreation.contactOrigine
      )
    , ( "contactDate"
      , Encode.string <|
            Maybe.withDefault "" <|
                dataEtablissementCreation.contactDate
      )
    , ( "createurs"
      , Encode.list encodeNewContactForm dataEtablissementCreation.createurs
      )
    , ( "projet"
      , Encode.object
            [ ( "projetType", Encode.string <| String.trim <| dataEtablissementCreation.projet.projetType )
            , ( "futureEnseigne", Encode.string dataEtablissementCreation.projet.futureEnseigne )
            , ( "description", Encode.string dataEtablissementCreation.projet.description )
            , ( "secteurActivite"
              , Encode.string <|
                    encodeSpaceToEmptyString <|
                        dataEtablissementCreation.projet.secteurActivite
              )
            , ( "formeJuridique"
              , Encode.string <|
                    encodeSpaceToEmptyString <|
                        dataEtablissementCreation.projet.formeJuridique
              )
            , ( "sourceFinancement"
              , Encode.string <|
                    encodeSpaceToEmptyString <|
                        dataEtablissementCreation.projet.sourceFinancement
              )
            , ( "contratAccompagnement", Encode.bool dataEtablissementCreation.projet.contratAccompagnement )
            , ( "activites", Encode.list Encode.string dataEtablissementCreation.projet.activites )
            , ( "mots", Encode.list Encode.string dataEtablissementCreation.projet.mots )
            , ( "localisations", Encode.list Encode.string dataEtablissementCreation.projet.localisations )
            , ( "communes"
              , Encode.list Encode.string <|
                    List.map .id <|
                        dataEtablissementCreation.projet.communes
              )
            , ( "rechercheLocal", Encode.bool dataEtablissementCreation.projet.rechercheLocal )
            , ( "commentaires", Encode.string dataEtablissementCreation.projet.commentaires )
            ]
      )
    ]
        |> Encode.object



-- displayEtabCrea : EtablissementCreation -> String
-- displayEtabCrea etablissementCreation =
--     let
--         ( { personne }, contacts ) =
--             etablissementCreation.createurs
--         createurs =
--             personne
--                 :: (contacts
--                         |> List.map .personne
--                    )
--         identifiant =
--             String.padLeft 6 '0' <|
--                 entityIdToString <|
--                     .id <|
--                         etablissementCreation
--         noms =
--             case ( personne.nom, personne.prenom ) of
--                 ( "", "" ) ->
--                     ""
--                 _ ->
--                     String.concat <|
--                         [ " "
--                         , "("
--                         , String.join ", " <|
--                             List.map displayPrenomNomPersonne <|
--                                 createurs
--                         , ")"
--                         ]
--     in
--     identifiant ++ noms


badgeCreationAbandonnee : Bool -> Html msg
badgeCreationAbandonnee creationAbandonnee =
    if creationAbandonnee then
        text "Création abandonnée"
            |> DSFR.Badge.system { context = DSFR.Badge.New, withIcon = False }
            |> DSFR.Badge.badgeMD

    else
        nothing
