module UI.Brouillon exposing (BrouillonInput, Field(..), default, encodeBrouillonInput, form, update, view)

import Accessibility exposing (Html, div, h4, p, span, text)
import Api.EntityId exposing (EntityId)
import DSFR.Button
import DSFR.Icons
import DSFR.Icons.Business
import DSFR.Icons.Design
import DSFR.Icons.System
import DSFR.Input
import DSFR.Typography as Typo
import Data.Brouillon exposing (Brouillon, BrouillonId)
import Date exposing (Date)
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing, viewIf)
import Json.Encode as Encode exposing (Value)
import Lib.Date
import Lib.UI exposing (withEmptyAs)
import Time


type Field
    = DateBrouillon
    | TitreBrouillon
    | DescriptionBrouillon


type alias BrouillonInput =
    { id : EntityId BrouillonId
    , date : String
    , nom : String
    , description : String
    }


default : String -> Maybe Date -> BrouillonInput
default title today =
    { id = Api.EntityId.newId
    , date = today |> Maybe.map Date.toIsoString |> Maybe.withDefault ""
    , nom = title
    , description = ""
    }


update : Field -> String -> BrouillonInput -> BrouillonInput
update field value brouillon =
    case field of
        DateBrouillon ->
            { brouillon | date = value }

        TitreBrouillon ->
            { brouillon | nom = value }

        DescriptionBrouillon ->
            { brouillon | description = value }


form : (Field -> String -> msg) -> BrouillonInput -> Html msg
form updateField brouillon =
    div [ class "flex flex-col gap-4" ]
        [ div [ class "fr-form-group flex flex-row gap-4 !mb-0" ]
            [ DSFR.Input.new
                { value = brouillon.date
                , onInput = updateField DateBrouillon
                , label = text "Date du brouillon"
                , name = "date-brouillon"
                }
                |> DSFR.Input.date { min = Nothing, max = Nothing }
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            ]
        , div [ class "fr-form-group !mb-0" ]
            [ DSFR.Input.new
                { value = brouillon.nom
                , onInput = updateField TitreBrouillon
                , label = text "Titre du brouillon"
                , name = "titre-brouillon"
                }
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            ]
        , div [ class "fr-form-group !mb-0" ]
            [ DSFR.Input.new
                { value = brouillon.description
                , onInput = updateField DescriptionBrouillon
                , label = text "Contenu du brouillon"
                , name = "description-brouillon"
                }
                |> DSFR.Input.textArea (Just 8)
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            ]
        ]


brouillonToBrouillonInput : Brouillon -> BrouillonInput
brouillonToBrouillonInput brouillon =
    { id = Data.Brouillon.id brouillon
    , date = Data.Brouillon.date brouillon |> Date.toIsoString
    , nom = Data.Brouillon.titre brouillon
    , description = Data.Brouillon.description brouillon
    }


viewAuthor : Brouillon -> Html msg
viewAuthor brouillon =
    brouillon
        |> Data.Brouillon.auteur
        |> text
        |> List.singleton
        |> div [ class "grey-text uppercase" ]


viewBrouillonMetadata : Time.Zone -> Brouillon -> Html msg
viewBrouillonMetadata timezone brouillon =
    div [ Typo.textSm, class "flex flex-row gap-2 !mb-0" ]
        [ viewAuthor brouillon
        , div [] [ text <| Lib.Date.formatDateShort <| Data.Brouillon.date brouillon ]
        , viewIf (Data.Brouillon.modification brouillon /= Nothing) <| text " - "
        , case Data.Brouillon.modification brouillon of
            Nothing ->
                nothing

            Just ( auteur, date ) ->
                div [ class "flex flex-col text-right" ]
                    [ div
                        [ Html.Attributes.title <|
                            "Dernière modification du brouillon le "
                                ++ Lib.Date.formatShort timezone date
                                ++ " par "
                                ++ auteur
                        , Typo.textSm
                        , class "!mb-0"
                        ]
                        [ text "Modifié "
                        , DSFR.Icons.iconSM DSFR.Icons.System.informationLine
                        ]
                    ]
        ]


view : Time.Zone -> { edit : BrouillonInput -> msg, delete : BrouillonInput -> msg, transform : Brouillon -> msg } -> Brouillon -> Html msg
view timezone { edit, delete, transform } brouillon =
    div [ class "flex flex-col gap-4" ] <|
        [ viewBrouillonMetadata timezone brouillon
        , h4 [ class "whitespace-pre-wrap !mb-0" ]
            [ span [ class "font-bold" ]
                [ text <| withEmptyAs "-" <| Data.Brouillon.titre brouillon ]
            ]
        , p [ class "whitespace-pre-wrap !mb-0" ]
            [ text <|
                withEmptyAs "-" <|
                    Data.Brouillon.description brouillon
            ]
        , [ DSFR.Button.new
                { onClick = Just <| transform <| brouillon
                , label = "Transformer"
                }
                |> DSFR.Button.leftIcon DSFR.Icons.Business.linksLine
                |> DSFR.Button.withAttrs [ class "!mb-0" ]
                |> DSFR.Button.secondary
          , DSFR.Button.new
                { onClick = Just <| edit <| brouillonToBrouillonInput <| brouillon
                , label = "Modifier"
                }
                |> DSFR.Button.leftIcon DSFR.Icons.Design.editFill
                |> DSFR.Button.withAttrs [ class "!mb-0" ]
                |> DSFR.Button.secondary
          , DSFR.Button.new
                { onClick = Just <| delete <| brouillonToBrouillonInput <| brouillon
                , label = "Supprimer"
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.deleteLine
                |> DSFR.Button.withAttrs [ class "!mb-0" ]
                |> DSFR.Button.tertiary
          ]
            |> DSFR.Button.group
            |> DSFR.Button.inline
            |> DSFR.Button.iconsLeft
            |> DSFR.Button.viewGroup
        ]


encodeBrouillonInput : BrouillonInput -> Value
encodeBrouillonInput brouillonInput =
    [ ( "brouillon"
      , [ ( "id", Api.EntityId.encodeEntityId brouillonInput.id )
        , ( "date", Encode.string brouillonInput.date )
        , ( "nom", Encode.string brouillonInput.nom )
        , ( "description", Encode.string brouillonInput.description )
        ]
            |> Encode.object
      )
    ]
        |> Encode.object
