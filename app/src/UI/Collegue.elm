module UI.Collegue exposing (Partage, getCollegues, viewPartageModal)

import Accessibility exposing (Html, div, formWithListeners, text)
import Api
import Api.Auth exposing (get)
import Api.EntityId exposing (EntityId, entityIdToString)
import DSFR.Alert
import DSFR.Button
import DSFR.Input
import DSFR.Modal
import Data.Collegues exposing (Collegue, decodeCollegue)
import Html.Attributes exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Json.Decode as Decode
import RemoteData as RD exposing (WebData)
import Shared
import User exposing (UserId)


type alias Partage =
    { collegueId : Maybe (EntityId UserId)
    , commentaire : String
    , request : WebData ()
    }


partageModalBody :
    { confirm : msg
    , cancel : msg
    , updateCollegue : String -> msg
    , updateCommentaire : String -> msg
    }
    -> List Collegue
    -> Partage
    -> Html msg
partageModalBody { confirm, cancel, updateCollegue, updateCommentaire } collegues { collegueId, commentaire, request } =
    div [] <|
        List.singleton <|
            formWithListeners [ Events.onSubmit <| confirm, class "flex flex-col gap-4" ]
                [ div []
                    [ DSFR.Input.new
                        { value =
                            collegueId
                                |> Maybe.map entityIdToString
                                |> Maybe.withDefault "aucun"
                        , onInput = updateCollegue
                        , label = text "Destinataire"
                        , name = "partage-collegue"
                        }
                        |> DSFR.Input.select
                            { options = collegues
                            , toId = .id >> entityIdToString
                            , toLabel =
                                \{ prenom, nom, email } ->
                                    prenom
                                        ++ " "
                                        ++ nom
                                        ++ " ("
                                        ++ email
                                        ++ ")"
                                        |> text
                            , toDisabled = Nothing
                            }
                        |> DSFR.Input.view
                    ]
                , div []
                    [ DSFR.Input.new
                        { value = commentaire
                        , onInput = updateCommentaire
                        , label = text "Commentaire"
                        , name = "partage-commentaire"
                        }
                        |> DSFR.Input.textArea (Just 8)
                        |> DSFR.Input.view
                    ]
                , case request of
                    RD.Success () ->
                        DSFR.Alert.small { title = Nothing, description = text "La fiche a été envoyée avec succès" }
                            |> DSFR.Alert.alert Nothing DSFR.Alert.success

                    RD.Failure _ ->
                        DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite" }
                            |> DSFR.Alert.alert Nothing DSFR.Alert.error

                    _ ->
                        nothing
                , case request of
                    RD.Success () ->
                        div []
                            [ DSFR.Button.group
                                [ DSFR.Button.new { onClick = Just <| cancel, label = "Fermer" }
                                    |> DSFR.Button.submit
                                ]
                                |> DSFR.Button.inline
                                |> DSFR.Button.alignedRightInverted
                                |> DSFR.Button.viewGroup
                            ]

                    _ ->
                        div []
                            [ DSFR.Button.group
                                [ DSFR.Button.new { onClick = Nothing, label = "Envoyer" }
                                    |> DSFR.Button.submit
                                    |> DSFR.Button.withDisabled (request == RD.Loading)
                                , DSFR.Button.new { onClick = Just <| cancel, label = "Annuler" }
                                    |> DSFR.Button.secondary
                                ]
                                |> DSFR.Button.inline
                                |> DSFR.Button.alignedRightInverted
                                |> DSFR.Button.viewGroup
                            ]
                ]


viewPartageModal :
    { noOp : msg
    , confirm : msg
    , cancel : msg
    , updateCollegue : String -> msg
    , updateCommentaire : String -> msg
    }
    -> List Collegue
    -> Maybe Partage
    -> Html msg
viewPartageModal { noOp, confirm, cancel, updateCollegue, updateCommentaire } collegues partage =
    let
        ( opened, content, title ) =
            case partage of
                Nothing ->
                    ( False, nothing, nothing )

                Just partageModal ->
                    ( True
                    , partageModalBody
                        { confirm = confirm
                        , cancel = cancel
                        , updateCollegue = updateCollegue
                        , updateCommentaire = updateCommentaire
                        }
                        collegues
                        partageModal
                    , text "Partager la fiche"
                    )
    in
    DSFR.Modal.view
        { id = "partage-etablissement"
        , label = "partage-etablissement"
        , openMsg = noOp
        , closeMsg = Just cancel
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.md
        }
        content
        Nothing
        |> Tuple.first


getCollegues : (Shared.Msg -> msg) -> Maybe (msg -> Shared.ErrorReport -> msg) -> (WebData (List Collegue) -> msg) -> Cmd msg
getCollegues sharedMsg maybeLogger msg =
    get
        { url = Api.getDevecosEquipe }
        { toShared = sharedMsg
        , logger = maybeLogger
        , handler = msg
        }
        (Decode.field "devecos" <|
            Decode.list <|
                decodeCollegue
        )
