module UI.Rappel exposing
    ( Field(..)
    , RappelInput
    , RappelsMode(..)
    , clotureRappelForEntite
    , default
    , encodeRappelInput
    , form
    , isValid
    , rappelInputValidate
    , rappelsModeToDisplay
    , rappelsModeToString
    , rappelsModes
    , rappelsSelonMode
    , reouvertureRappelForEntite
    , update
    , updateRappelForEntite
    , viewOngletRappels
    )

import Accessibility exposing (Html, div, h4, p, span, text)
import Api
import Api.EntityId exposing (EntityId)
import DSFR.Badge
import DSFR.Button
import DSFR.Icons
import DSFR.Icons.Business
import DSFR.Icons.Design
import DSFR.Icons.Media
import DSFR.Icons.System
import DSFR.Input
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Brouillon exposing (BrouillonId)
import Data.Collegues exposing (Collegue)
import Data.Entite exposing (Entite(..), viewLienEntite)
import Data.Rappel exposing (Rappel, RappelId)
import Date exposing (Date)
import Dict exposing (Dict)
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing)
import Html.Keyed as Keyed
import Json.Encode as Encode exposing (Value)
import Lib.Date
import Lib.UI exposing (withEmptyAs)
import Lib.Variables exposing (contactEmail)
import Time exposing (Posix, Zone)
import Url.Builder as Builder
import User exposing (UserId)


type Field
    = DateRappel
    | TitreRappel
    | DescriptionRappel
    | AffectationRappel


type alias RappelInput =
    { id : EntityId RappelId
    , date : String
    , titre : String
    , description : String
    , affectation : Maybe (EntityId UserId)
    , clotureDate : Maybe String
    , entite : Maybe Entite
    , brouillonId : Maybe (EntityId BrouillonId)
    , brouillonSuppression : Bool
    }


default : Maybe (EntityId BrouillonId) -> EntityId UserId -> String -> String -> Date -> RappelInput
default brouillonId id title description today =
    { id = Api.EntityId.newId
    , date = today |> Date.toIsoString
    , titre = title
    , description = description
    , affectation = Just id
    , clotureDate = Nothing
    , entite = Nothing
    , brouillonId = brouillonId
    , brouillonSuppression = False
    }


type RappelsMode
    = RappelsTous
    | RappelsEchus
    | RappelsAVenir
    | RappelsClotures


rappelsModes : List RappelsMode
rappelsModes =
    [ RappelsTous
    , RappelsEchus
    , RappelsAVenir
    , RappelsClotures
    ]


rappelsModeToString : RappelsMode -> String
rappelsModeToString rappelsMode =
    case rappelsMode of
        RappelsTous ->
            "rappels-mode-tous"

        RappelsEchus ->
            "rappels-mode-echus"

        RappelsAVenir ->
            "rappels-mode-a-venir"

        RappelsClotures ->
            "rappels-mode-clotures"


rappelsModeToDisplay : RappelsMode -> String
rappelsModeToDisplay rappelsMode =
    case rappelsMode of
        RappelsTous ->
            "Tous"

        RappelsEchus ->
            "Arrivés à échéance"

        RappelsAVenir ->
            "À venir"

        RappelsClotures ->
            "Clôturés"


rappelsModeToAdjectif : RappelsMode -> String
rappelsModeToAdjectif rappelsMode =
    case rappelsMode of
        RappelsTous ->
            ""

        RappelsEchus ->
            "arrivé à échéance"

        RappelsAVenir ->
            "à venir"

        RappelsClotures ->
            "clôturé"


update : Field -> String -> RappelInput -> RappelInput
update field value echange =
    case field of
        DateRappel ->
            { echange | date = value }

        TitreRappel ->
            { echange | titre = value }

        DescriptionRappel ->
            { echange | description = value }

        AffectationRappel ->
            { echange
                | affectation =
                    case value of
                        "equipe" ->
                            Nothing

                        _ ->
                            Api.EntityId.parseEntityId value
            }


form : String -> List Collegue -> (Field -> String -> msg) -> RappelInput -> Html msg
form equipe options updateField rappel =
    div [ class "flex flex-col gap-4" ]
        [ div [ class "flex flex-row gap-4" ]
            [ div [ class "fr-form-group w-[180px] !mb-0" ]
                [ DSFR.Input.new
                    { value = rappel.date
                    , onInput = updateField DateRappel
                    , label = text "Date du rappel"
                    , name = "date-rappel"
                    }
                    |> DSFR.Input.date { min = Nothing, max = Nothing }
                    |> DSFR.Input.view
                ]
            , div [ class "fr-form-group !mb-0" ]
                [ DSFR.Input.new
                    { value = rappel.affectation |> Maybe.map Api.EntityId.entityIdToString |> Maybe.withDefault "equipe"
                    , onInput = updateField AffectationRappel
                    , label = text "Affecter le rappel à"
                    , name = "affectation-rappel"
                    }
                    |> DSFR.Input.select
                        { options =
                            options
                                |> List.map Just
                                |> (::) Nothing
                        , toId = Maybe.map .id >> Maybe.map Api.EntityId.entityIdToString >> Maybe.withDefault "equipe"
                        , toLabel =
                            \option ->
                                option
                                    |> Maybe.map
                                        (\{ prenom, nom, email } ->
                                            prenom
                                                ++ " "
                                                ++ nom
                                                ++ " ("
                                                ++ email
                                                ++ ")"
                                                |> text
                                        )
                                    |> Maybe.withDefault ("Toute l'équipe" |> text |> List.singleton |> span [ Typo.textBold ])
                        , toDisabled = Nothing
                        }
                    |> DSFR.Input.view
                , div [ class "mb-4" ]
                    [ text "Votre collègue n'apparaît pas dans la liste\u{00A0}?"
                    , text " "
                    , let
                        emails =
                            [ contactEmail ]

                        subject =
                            Builder.string "subject" <|
                                "Demande d'ajout d'un collegue"

                        bod =
                            Builder.string "body" <|
                                "Territoire\u{00A0}: "
                                    ++ equipe
                                    ++ "\nEmail du collegue\u{00A0}: \nNom du collegue\u{00A0}: \nPrénom du collegue\u{00A0}: \nFonction du collegue\u{00A0}: \n"

                        mailto =
                            "mailto:"
                                ++ String.join "," emails
                                ++ Builder.toQuery [ subject, bod ]
                      in
                      span []
                        [ Typo.externalLink mailto [] [ text "Demander l'ajout d'un collegue" ] ]
                    , text "."
                    ]
                , div [ class "fr-text-default--info" ]
                    [ DSFR.Icons.System.infoFill |> DSFR.Icons.iconSM
                    , text " "
                    , text "Cette personne recevra un e-mail 2 jours avant la date du rappel"
                    ]
                ]
            ]
        , div [ class "fr-form-group" ]
            [ DSFR.Input.new
                { value = rappel.titre
                , onInput = updateField TitreRappel
                , label = text "Titre du rappel"
                , name = "titre-rappel"
                }
                |> DSFR.Input.withRequired True
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            ]
        , div [ class "fr-form-group" ]
            [ DSFR.Input.new
                { value = rappel.description
                , onInput = updateField DescriptionRappel
                , label = text "Description du rappel"
                , name = "description-rappel"
                }
                |> DSFR.Input.textArea (Just 4)
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            ]
        ]


isValid : RappelInput -> Bool
isValid =
    rappelInputValidate >> Result.map (\_ -> True) >> Result.withDefault False


rappelInputValidate : RappelInput -> Result (Dict String (List String)) ()
rappelInputValidate input =
    let
        validateDate =
            ( .date
            , "date"
            , \date ->
                date |> Date.fromIsoString |> Result.mapError (\_ -> "Date invalide")
            )

        validateTitre =
            ( .titre
            , "titre"
            , \titre ->
                if "" /= String.trim titre then
                    Ok titre

                else
                    Err "Titre vide"
            )
    in
    Ok (\_ _ _ _ _ -> ())
        |> keepErrors (Ok <| .id <| input)
        |> keepErrors (validate validateDate input)
        |> keepErrors (validate validateTitre input)
        |> keepErrors (Ok <| .affectation <| input)
        |> keepErrors (Ok <| Maybe.andThen (Date.fromIsoString >> Result.toMaybe) <| .clotureDate <| input)


rappelToRappelInput : Rappel -> RappelInput
rappelToRappelInput rappel =
    { id = Data.Rappel.id rappel
    , date = Data.Rappel.date rappel |> Date.toIsoString
    , titre = Data.Rappel.titre rappel
    , description = Data.Rappel.description rappel
    , affectation = Data.Rappel.affectation rappel |> Maybe.map .id
    , clotureDate = Data.Rappel.clotureDate rappel |> Maybe.map Date.toIsoString
    , entite = Data.Rappel.entite rappel
    , brouillonId = Nothing
    , brouillonSuppression = False
    }


validate : ( input -> field, String, field -> Result error value ) -> input -> Result (Dict String (List error)) value
validate ( accessor, name, validator ) object =
    case validator <| accessor object of
        Ok v ->
            Ok v

        Err err ->
            Err <| Dict.insert name [ err ] Dict.empty


keepErrors : Result (Dict String (List error)) value -> Result (Dict String (List error)) (value -> nextStep) -> Result (Dict String (List error)) nextStep
keepErrors value function =
    case ( value, function ) of
        ( Ok v, Ok f ) ->
            Ok (f v)

        ( Err errs, Ok _ ) ->
            Err errs

        ( Ok _, Err errs ) ->
            Err errs

        ( Err errV, Err errF ) ->
            Err <| Dict.merge (\k v d -> Dict.insert k v d) (\k v1 v2 d -> Dict.insert k (v1 ++ v2) d) (\k v d -> Dict.insert k v d) errV errF Dict.empty


encodeRappelInput : RappelInput -> Value
encodeRappelInput rappelInput =
    [ ( "rappel"
      , [ ( "date", Encode.string rappelInput.date )
        , ( "nom", Encode.string rappelInput.titre )
        , ( "description", Encode.string <| String.trim <| rappelInput.description )
        , ( "affecteId", Maybe.withDefault Encode.null <| Maybe.map Api.EntityId.encodeEntityId <| rappelInput.affectation )
        , ( "clotureDate", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| rappelInput.clotureDate )
        , ( "brouillonId"
          , Maybe.withDefault Encode.null <|
                Maybe.map Api.EntityId.encodeEntityId <|
                    rappelInput.brouillonId
          )
        , ( "brouillonSuppression", Encode.bool rappelInput.brouillonSuppression )
        ]
            |> Encode.object
      )
    ]
        |> Encode.object


rappelIsLate : Zone -> Posix -> Rappel -> Bool
rappelIsLate timezone now rappel =
    rappel |> Data.Rappel.date |> Lib.Date.firstDateIsAfterSecondDate (Date.fromPosix timezone now)


rappelAVenir : Zone -> Posix -> Rappel -> Bool
rappelAVenir timezone now rappel =
    not (rappelCloture rappel) && not (rappelIsLate timezone now rappel)


rappelEchu : Zone -> Posix -> Rappel -> Bool
rappelEchu timezone now rappel =
    not (rappelCloture rappel) && rappelIsLate timezone now rappel


rappelCloture : Rappel -> Bool
rappelCloture rappel =
    Data.Rappel.clotureDate rappel /= Nothing


rappelsSelonMode : Zone -> Posix -> RappelsMode -> List Rappel -> List Rappel
rappelsSelonMode timezone now rappelsMode rappels =
    let
        rappelsTous =
            identity

        rappelsEchus =
            List.filter (rappelEchu timezone now)

        rappelsAVenir =
            List.filter (rappelAVenir timezone now)

        rappelsClotures =
            List.filter rappelCloture
    in
    rappels
        |> List.sortWith (Lib.Date.sortOlderDateFirst Data.Rappel.date)
        |> (case rappelsMode of
                RappelsTous ->
                    rappelsTous

                RappelsEchus ->
                    rappelsEchus

                RappelsAVenir ->
                    rappelsAVenir

                RappelsClotures ->
                    rappelsClotures
           )


viewRappelStatut : Zone -> Posix -> Rappel -> Html msg
viewRappelStatut timezone now rappel =
    if rappelEchu timezone now rappel then
        text "À traiter"
            |> DSFR.Badge.system { context = DSFR.Badge.Warning, withIcon = True }
            |> DSFR.Badge.badgeMD

    else if rappelCloture rappel then
        [ { data = "Rappel clôturé", toString = identity }
            |> DSFR.Tag.unclickable
        ]
            |> DSFR.Tag.medium

    else
        nothing


viewRappelMetadata : Rappel -> Html msg
viewRappelMetadata rappel =
    div [ Typo.textSm, class "flex flex-row gap-2 !mb-0" ]
        [ div [ class "blue-text" ] <|
            List.singleton <|
                DSFR.Icons.iconSM <|
                    DSFR.Icons.Media.notification3Line
        , viewAuthor rappel
        , div [] [ text <| Lib.Date.formatDateShort <| Data.Rappel.date rappel ]
        ]


viewAuthor : Rappel -> Html msg
viewAuthor rappel =
    rappel
        |> Data.Rappel.affectation
        |> Maybe.map Lib.UI.displayPrenomNomPoint
        |> Maybe.withDefault "Toute l'équipe"
        |> text
        |> List.singleton
        |> div [ class "grey-text uppercase" ]


view :
    { edit : RappelInput -> msg
    , closeReopen : RappelInput -> msg
    , delete : RappelInput -> msg
    }
    -> Zone
    -> Posix
    -> Rappel
    -> Html msg
view { edit, closeReopen, delete } timezone now rappel =
    div [ class "flex flex-col gap-4" ] <|
        [ viewRappelStatut timezone now rappel
        , viewRappelMetadata rappel
        , h4 [ class "whitespace-pre-wrap !mb-0" ]
            [ span [ class "font-bold" ]
                [ text <| withEmptyAs "-" <| Data.Rappel.titre rappel ]
            ]
        , p [ class "whitespace-pre-wrap !mb-0" ]
            [ text <|
                withEmptyAs "-" <|
                    Data.Rappel.description rappel
            ]
        , viewLienEntite "rappels" <| Data.Rappel.entite rappel
        , [ DSFR.Button.new { onClick = Just <| edit <| rappelToRappelInput <| rappel, label = "Modifier" }
                |> DSFR.Button.leftIcon DSFR.Icons.Design.editFill
                |> DSFR.Button.withDisabled (rappelCloture rappel)
                |> DSFR.Button.withAttrs [ class "!mb-0" ]
                |> DSFR.Button.secondary
          , DSFR.Button.new
                { onClick =
                    Just <|
                        closeReopen <|
                            (\r ->
                                { r
                                    | clotureDate =
                                        if r.clotureDate == Nothing then
                                            Date.fromPosix timezone now
                                                |> Date.toIsoString
                                                |> Just

                                        else
                                            Nothing
                                }
                            )
                            <|
                                rappelToRappelInput <|
                                    rappel
                , label =
                    if Nothing == Data.Rappel.clotureDate rappel then
                        "Clôturer"

                    else
                        "Réouvrir"
                }
                |> DSFR.Button.leftIcon
                    (if Nothing /= Data.Rappel.clotureDate rappel then
                        DSFR.Icons.System.arrowGoBackLine

                     else
                        DSFR.Icons.Business.archiveLine
                    )
                |> DSFR.Button.withAttrs [ class "!mb-0" ]
                |> DSFR.Button.tertiary
          , DSFR.Button.new { onClick = Just <| delete <| rappelToRappelInput <| rappel, label = "Supprimer" }
                |> DSFR.Button.leftIcon DSFR.Icons.System.deleteFill
                |> DSFR.Button.withAttrs [ class "!mb-0" ]
                |> DSFR.Button.tertiaryNoOutline
          ]
            |> DSFR.Button.group
            |> DSFR.Button.iconsLeft
            |> DSFR.Button.inline
            |> DSFR.Button.viewGroup
        ]


viewOngletRappels :
    { edit : RappelInput -> msg
    , closeReopen : RappelInput -> msg
    , delete : RappelInput -> msg
    }
    -> Zone
    -> Posix
    -> RappelsMode
    -> List Rappel
    -> Html msg
viewOngletRappels msgs timezone now rappelsMode rappels =
    case rappelsSelonMode timezone now rappelsMode rappels of
        [] ->
            div [ class "p-4 sm:p-8 fr-card--white" ] <|
                List.singleton <|
                    span [ class "text-center italic" ]
                        [ text "Aucun rappel"
                        , text " "
                        , text <| rappelsModeToAdjectif <| rappelsMode
                        ]

        rs ->
            div [ class "flex flex-col gap-6" ]
                (List.map
                    (\rappel ->
                        Keyed.node "div" [ class "fr-card--white p-4" ] <|
                            [ ( "rappel-" ++ (Api.EntityId.entityIdToString <| Data.Rappel.id <| rappel)
                              , view msgs timezone now rappel
                              )
                            ]
                    )
                    rs
                )


updateRappelForEntite : Bool -> Maybe Entite -> Maybe (EntityId RappelId) -> String
updateRappelForEntite global entite =
    case entite of
        Nothing ->
            \_ -> ""

        Just e ->
            case e of
                Etab siret _ ->
                    Api.upsertRappelEtablissement global siret

                EtabCrea etablissementCreationId _ ->
                    Api.upsertRappelEtablissementCreation global etablissementCreationId

                Loc localId _ ->
                    Api.upsertRappelLocal global localId


clotureRappelForEntite : Bool -> Maybe Entite -> EntityId RappelId -> String
clotureRappelForEntite global entite =
    case entite of
        Nothing ->
            \_ -> ""

        Just e ->
            case e of
                Etab siret _ ->
                    Api.clotureRappelEtablissement global siret

                EtabCrea etablissementCreationId _ ->
                    Api.clotureRappelEtablissementCreation global etablissementCreationId

                Loc localId _ ->
                    Api.clotureRappelLocal global localId


reouvertureRappelForEntite : Bool -> Maybe Entite -> EntityId RappelId -> String
reouvertureRappelForEntite global entite =
    case entite of
        Nothing ->
            \_ -> ""

        Just e ->
            case e of
                Etab siret _ ->
                    Api.reouvertureRappelEtablissement global siret

                EtabCrea etablissementCreationId _ ->
                    Api.reouvertureRappelEtablissementCreation global etablissementCreationId

                Loc localId _ ->
                    Api.reouvertureRappelLocal global localId
