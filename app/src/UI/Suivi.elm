module UI.Suivi exposing (Model, Msg, Onglet(..), SuiviConfig, SuiviResult(..), changeOnglet, getOngletActif, init, ongletToString, stringToOnglet, suiviConfigEtab, suiviConfigEtabCrea, suiviConfigLocal, suiviResultToMaybe, update, viewOngletAjout, viewOngletSuivis, viewSuivisModal)

import Accessibility exposing (Html, div, h3, h4, li, nav, span, text, ul)
import Accessibility.Aria as Aria
import Accessibility.Landmark exposing (navigation)
import Accessibility.Role as Role
import Api
import Api.Auth exposing (delete, get, post)
import Api.EntityId exposing (EntityId, entityIdToString)
import DSFR.Alert
import DSFR.Button
import DSFR.Checkbox
import DSFR.Grid
import DSFR.Icons
import DSFR.Icons.Communication
import DSFR.Icons.Document
import DSFR.Icons.System
import DSFR.Input
import DSFR.Modal
import DSFR.Radio
import DSFR.SearchBar
import DSFR.Stepper
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Adresse exposing (adresseToString)
import Data.Brouillon exposing (Brouillon, BrouillonId)
import Data.Collegues exposing (Collegue)
import Data.Demande exposing (Demande, DemandeId)
import Data.Echange exposing (Echange, EchangeId)
import Data.Entite exposing (Entite(..))
import Data.Equipe
import Data.Etablissement exposing (EtablissementApercu, Siret)
import Data.EtablissementCreation exposing (EtablissementCreationApercu)
import Data.EtablissementCreationId exposing (EtablissementCreationId)
import Data.Local exposing (LocalApercu)
import Data.Rappel exposing (Rappel, RappelId)
import Data.Role
import Date exposing (Date)
import Effect
import Html
import Html.Attributes exposing (class, classList)
import Html.Attributes.Extra
import Html.Events as Events
import Html.Extra exposing (nothing, viewIf, viewMaybe)
import Html.Keyed as Keyed
import Html.Lazy
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode exposing (Value)
import Lib.Date
import Lib.UI exposing (plural, withEmptyAs)
import Lib.Variables
import List.Extra
import Process
import RemoteData as RD exposing (WebData)
import Route
import Shared
import Task
import Time exposing (Posix, Zone)
import UI.Brouillon exposing (BrouillonInput)
import UI.Clipboard
import UI.Demande exposing (DemandeInput, demandeTypeToTag)
import UI.Echange exposing (EchangeInput, getDemandeInDemandes)
import UI.Entite exposing (iconeCreateur, iconeEtablissement, iconeLocal)
import UI.Rappel exposing (RappelInput, RappelsMode(..))
import User


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | UpdatedBrouillonForm UI.Brouillon.Field String
    | UpdatedEchangeForm UI.Echange.Field String
    | UpdatedEchangeDemande (EntityId DemandeId) Bool
    | UpdatedRappelForm UI.Rappel.Field String
    | UpdatedDemandeForm UI.Demande.Field String
    | UpdatedClotureMotif String
    | UpdatedEntiteId String
    | RequestEntite
    | ReceivedEntite (WebData EntiteResultat)
    | ReceivedSaveSuivi (WebData Value)
    | SetSuiviAction SuiviAction
    | ConfirmSuiviAction
    | CancelSuiviAction
    | SetRappelsMode RappelsMode
    | SelectedOnglet (Maybe Onglet)
    | SetTitreDemandeFilter Bool (Maybe String)
    | SetTypeDemandeFilter Bool Data.Demande.TypeDemande
    | ViewEchanges String


type alias Model =
    { suiviAction : SuiviAction
    , saveSuiviRequest : WebData Value
    , ongletActif : Onglet
    , echangesModel : EchangesModel
    , demandesModel : DemandesModel
    , rappelsModel : RappelsModel
    , brouillonsModel : BrouillonsModel
    }


type Onglet
    = OngletEchanges
    | OngletDemandes
    | OngletRappels
    | OngletBrouillons


ongletToDisplay : Onglet -> String
ongletToDisplay onglet =
    case onglet of
        OngletEchanges ->
            "Échanges"

        OngletDemandes ->
            "Demandes"

        OngletRappels ->
            "Rappels"

        OngletBrouillons ->
            "Brouillons"


ongletToString : Onglet -> String
ongletToString onglet =
    case onglet of
        OngletEchanges ->
            "echanges"

        OngletDemandes ->
            "demandes"

        OngletRappels ->
            "rappels"

        OngletBrouillons ->
            "brouillons"


ongletToIcone : Onglet -> DSFR.Icons.IconName
ongletToIcone onglet =
    case onglet of
        OngletEchanges ->
            DSFR.Icons.Communication.discussLine

        OngletDemandes ->
            DSFR.Icons.Communication.questionnaireLine

        OngletRappels ->
            DSFR.Icons.System.timerLine

        OngletBrouillons ->
            DSFR.Icons.Document.draftLine


stringToOnglet : String -> Maybe Onglet
stringToOnglet s =
    case s of
        "echanges" ->
            Just OngletEchanges

        "demandes" ->
            Just OngletDemandes

        "rappels" ->
            Just OngletRappels

        "brouillons" ->
            Just OngletBrouillons

        _ ->
            Nothing


type alias EchangesModel =
    { triEchanges : TriEchanges
    , titresDemandes : List (Maybe String)
    }


type TriEchanges
    = EchangesRecents


type alias DemandesModel =
    { triDemandes : TriDemandes
    , typesDemandes : List Data.Demande.TypeDemande
    }


type TriDemandes
    = DemandesRecentes


type alias RappelsModel =
    { rappelsMode : RappelsMode
    }


type alias BrouillonsModel =
    {}


init : Onglet -> Maybe Onglet -> Model
init ongletParDefaut onglet =
    { suiviAction = NoSuivi
    , saveSuiviRequest = RD.NotAsked
    , ongletActif = onglet |> Maybe.withDefault ongletParDefaut
    , echangesModel =
        { triEchanges = EchangesRecents
        , titresDemandes = []
        }
    , demandesModel =
        { triDemandes = DemandesRecentes
        , typesDemandes = []
        }
    , rappelsModel =
        { rappelsMode = RappelsTous
        }
    , brouillonsModel =
        {}
    }


withEntity : SuiviResult entity -> ( model, effect ) -> ( model, effect, SuiviResult entity )
withEntity entity ( model, effect ) =
    ( model, effect, entity )


type SuiviResult entity
    = NoResult
    | Result entity
    | Refetch


type alias SuiviConfig entity =
    { decoder : Decoder entity
    , upsertRappelUrl : Maybe Entite -> Maybe (EntityId RappelId) -> String
    , upsertEchangeUrl : Maybe Entite -> Maybe (EntityId EchangeId) -> String
    , upsertDemandeUrl : Maybe Entite -> Maybe (EntityId DemandeId) -> String
    , upsertBrouillonUrl : Maybe (EntityId BrouillonId) -> String
    , clotureRappelUrl : Maybe Entite -> EntityId RappelId -> String
    , reouvertureRappelUrl : Maybe Entite -> EntityId RappelId -> String
    , clotureDemandeUrl : Maybe Entite -> EntityId DemandeId -> String
    , reouvertureDemandeUrl : Maybe Entite -> EntityId DemandeId -> String
    }


update : SuiviConfig entity -> Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg, SuiviResult entity )
update { decoder, upsertRappelUrl, upsertEchangeUrl, upsertDemandeUrl, upsertBrouillonUrl, clotureRappelUrl, clotureDemandeUrl, reouvertureRappelUrl, reouvertureDemandeUrl } msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone
                |> withEntity NoResult

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg
                |> withEntity NoResult

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error
                |> withEntity NoResult

        SetRappelsMode rappelsMode ->
            { model | rappelsModel = model.rappelsModel |> (\rm -> { rm | rappelsMode = rappelsMode }) }
                |> Effect.withNone
                |> withEntity NoResult

        UpdatedEchangeForm field value ->
            case model.suiviAction of
                NewSuivi (SuiviEchange echange) ->
                    { model | suiviAction = NewSuivi <| SuiviEchange <| UI.Echange.update field value <| echange }
                        |> Effect.withNone
                        |> withEntity NoResult

                EditSuivi (SuiviEchange echange) ->
                    { model | suiviAction = EditSuivi <| SuiviEchange <| UI.Echange.update field value <| echange }
                        |> Effect.withNone
                        |> withEntity NoResult

                TransformationBrouillon transformationBrouillonData ->
                    case transformationBrouillonData.suiviType of
                        SuiviEchange echange ->
                            { model
                                | suiviAction =
                                    TransformationBrouillon
                                        { transformationBrouillonData
                                            | suiviType =
                                                SuiviEchange <|
                                                    UI.Echange.update field value <|
                                                        echange
                                        }
                            }
                                |> Effect.withNone
                                |> withEntity NoResult

                        _ ->
                            model
                                |> Effect.withNone
                                |> withEntity NoResult

                _ ->
                    model
                        |> Effect.withNone
                        |> withEntity NoResult

        UpdatedEchangeDemande demandeId add ->
            case model.suiviAction of
                NewSuivi (SuiviEchange echange) ->
                    { model | suiviAction = NewSuivi <| SuiviEchange <| UI.Echange.toggleDemande add demandeId <| echange }
                        |> Effect.withNone
                        |> withEntity NoResult

                EditSuivi (SuiviEchange echange) ->
                    { model | suiviAction = EditSuivi <| SuiviEchange <| UI.Echange.toggleDemande add demandeId <| echange }
                        |> Effect.withNone
                        |> withEntity NoResult

                _ ->
                    model
                        |> Effect.withNone
                        |> withEntity NoResult

        UpdatedRappelForm field value ->
            case model.suiviAction of
                NewSuivi (SuiviRappel rappel) ->
                    { model | suiviAction = NewSuivi <| SuiviRappel <| UI.Rappel.update field value <| rappel }
                        |> Effect.withNone
                        |> withEntity NoResult

                EditSuivi (SuiviRappel rappel) ->
                    { model | suiviAction = EditSuivi <| SuiviRappel <| UI.Rappel.update field value <| rappel }
                        |> Effect.withNone
                        |> withEntity NoResult

                TransformationBrouillon transformationBrouillonData ->
                    case transformationBrouillonData.suiviType of
                        SuiviRappel rappel ->
                            { model
                                | suiviAction =
                                    TransformationBrouillon
                                        { transformationBrouillonData
                                            | suiviType =
                                                SuiviRappel <|
                                                    UI.Rappel.update field value <|
                                                        rappel
                                        }
                            }
                                |> Effect.withNone
                                |> withEntity NoResult

                        _ ->
                            model
                                |> Effect.withNone
                                |> withEntity NoResult

                _ ->
                    model
                        |> Effect.withNone
                        |> withEntity NoResult

        UpdatedDemandeForm field value ->
            case model.suiviAction of
                NewSuivi (SuiviDemande demande) ->
                    { model | suiviAction = NewSuivi <| SuiviDemande <| UI.Demande.update field value <| demande }
                        |> Effect.withNone
                        |> withEntity NoResult

                EditSuivi (SuiviDemande demande) ->
                    { model | suiviAction = EditSuivi <| SuiviDemande <| UI.Demande.update field value <| demande }
                        |> Effect.withNone
                        |> withEntity NoResult

                TransformationBrouillon transformationBrouillonData ->
                    case transformationBrouillonData.suiviType of
                        SuiviDemande demande ->
                            { model
                                | suiviAction =
                                    TransformationBrouillon
                                        { transformationBrouillonData
                                            | suiviType =
                                                SuiviDemande <|
                                                    UI.Demande.update field value <|
                                                        demande
                                        }
                            }
                                |> Effect.withNone
                                |> withEntity NoResult

                        _ ->
                            model
                                |> Effect.withNone
                                |> withEntity NoResult

                _ ->
                    model
                        |> Effect.withNone
                        |> withEntity NoResult

        UpdatedBrouillonForm field value ->
            case model.suiviAction of
                NewSuivi (SuiviBrouillon brouillon) ->
                    { model | suiviAction = NewSuivi <| SuiviBrouillon <| UI.Brouillon.update field value <| brouillon }
                        |> Effect.withNone
                        |> withEntity NoResult

                EditSuivi (SuiviBrouillon brouillon) ->
                    { model | suiviAction = EditSuivi <| SuiviBrouillon <| UI.Brouillon.update field value <| brouillon }
                        |> Effect.withNone
                        |> withEntity NoResult

                _ ->
                    model
                        |> Effect.withNone
                        |> withEntity NoResult

        UpdatedClotureMotif motif ->
            case model.suiviAction of
                ClotureSuivi (SuiviDemande demande) ->
                    { model | suiviAction = ClotureSuivi <| SuiviDemande <| { demande | motif = motif } }
                        |> Effect.withNone
                        |> withEntity NoResult

                _ ->
                    model
                        |> Effect.withNone
                        |> withEntity NoResult

        UpdatedEntiteId id ->
            case model.suiviAction of
                TransformationBrouillon transformationBrouillonData ->
                    { model
                        | suiviAction =
                            TransformationBrouillon <|
                                { transformationBrouillonData
                                    | transformationEntite =
                                        transformationBrouillonData
                                            |> .transformationEntite
                                            |> (\te ->
                                                    { te
                                                        | recherche = id
                                                    }
                                               )
                                }
                    }
                        |> Effect.withNone
                        |> withEntity NoResult

                _ ->
                    model
                        |> Effect.withNone
                        |> withEntity NoResult

        RequestEntite ->
            case model.suiviAction of
                TransformationBrouillon transformationBrouillonData ->
                    { model
                        | suiviAction =
                            TransformationBrouillon <|
                                { transformationBrouillonData
                                    | transformationEntite =
                                        transformationBrouillonData
                                            |> .transformationEntite
                                            |> (\te ->
                                                    { te
                                                        | resultat = RD.Loading
                                                    }
                                               )
                                }
                    }
                        |> Effect.withCmd (rechercheEntite transformationBrouillonData)
                        |> withEntity NoResult

                _ ->
                    model
                        |> Effect.withNone
                        |> withEntity NoResult

        ReceivedEntite resp ->
            case model.suiviAction of
                TransformationBrouillon transformationBrouillonData ->
                    { model
                        | suiviAction =
                            TransformationBrouillon <|
                                { transformationBrouillonData
                                    | transformationEntite =
                                        transformationBrouillonData
                                            |> .transformationEntite
                                            |> (\te ->
                                                    { te
                                                        | resultat = resp
                                                    }
                                               )
                                }
                    }
                        |> Effect.withNone
                        |> withEntity NoResult

                _ ->
                    model
                        |> Effect.withNone
                        |> withEntity NoResult

        ReceivedSaveSuivi response ->
            case model.saveSuiviRequest of
                RD.Loading ->
                    case model.suiviAction of
                        NoSuivi ->
                            model
                                |> Effect.withNone
                                |> withEntity NoResult

                        TransformationBrouillon _ ->
                            case response of
                                RD.Success _ ->
                                    { model
                                        | saveSuiviRequest = RD.NotAsked
                                        , suiviAction = NoSuivi
                                    }
                                        |> Effect.withNone
                                        |> withEntity Refetch

                                _ ->
                                    { model | saveSuiviRequest = response }
                                        |> Effect.withNone
                                        |> withEntity NoResult

                        _ ->
                            case response of
                                RD.Success value ->
                                    case Decode.decodeValue decoder value of
                                        Ok entity ->
                                            { model | saveSuiviRequest = response, suiviAction = NoSuivi }
                                                |> Effect.withNone
                                                |> withEntity (Result entity)

                                        Err _ ->
                                            { model | saveSuiviRequest = response }
                                                |> Effect.withNone
                                                |> withEntity NoResult

                                _ ->
                                    { model | saveSuiviRequest = response }
                                        |> Effect.withNone
                                        |> withEntity NoResult

                _ ->
                    model
                        |> Effect.withNone
                        |> withEntity NoResult

        SetSuiviAction suiviAction ->
            { model | suiviAction = suiviAction }
                |> Effect.withNone
                |> withEntity NoResult

        ConfirmSuiviAction ->
            case model.saveSuiviRequest of
                RD.Loading ->
                    model
                        |> Effect.withNone
                        |> withEntity NoResult

                _ ->
                    case model.suiviAction of
                        NoSuivi ->
                            model
                                |> Effect.withNone
                                |> withEntity NoResult

                        NewSuivi (SuiviEchange echange) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (createEchange upsertEchangeUrl echange)
                                |> withEntity NoResult

                        NewSuivi (SuiviRappel rappel) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (createRappel upsertRappelUrl rappel)
                                |> withEntity NoResult

                        NewSuivi (SuiviDemande demande) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (createDemande upsertDemandeUrl demande)
                                |> withEntity NoResult

                        NewSuivi (SuiviBrouillon brouillon) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (createBrouillon upsertBrouillonUrl brouillon)
                                |> withEntity NoResult

                        EditSuivi (SuiviEchange echange) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (updateEchange upsertEchangeUrl echange)
                                |> withEntity NoResult

                        EditSuivi (SuiviRappel rappel) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (updateRappel upsertRappelUrl rappel)
                                |> withEntity NoResult

                        EditSuivi (SuiviDemande demande) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (updateDemande upsertDemandeUrl demande)
                                |> withEntity NoResult

                        EditSuivi (SuiviBrouillon brouillon) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (updateBrouillon upsertBrouillonUrl brouillon)
                                |> withEntity NoResult

                        DeleteSuivi (SuiviEchange echange) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (deleteEchange upsertEchangeUrl echange)
                                |> withEntity NoResult

                        DeleteSuivi (SuiviRappel rappel) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (deleteRappel upsertRappelUrl rappel)
                                |> withEntity NoResult

                        DeleteSuivi (SuiviDemande demande) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (deleteDemande upsertDemandeUrl demande)
                                |> withEntity NoResult

                        DeleteSuivi (SuiviBrouillon brouillon) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (deleteBrouillon upsertBrouillonUrl brouillon)
                                |> withEntity NoResult

                        ClotureSuivi (SuiviRappel rappel) ->
                            let
                                url =
                                    if rappel.clotureDate == Nothing then
                                        reouvertureRappelUrl

                                    else
                                        clotureRappelUrl
                            in
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (clotureRappel url rappel)
                                |> withEntity NoResult

                        ClotureSuivi (SuiviDemande demande) ->
                            let
                                url =
                                    if demande.clotureDate /= Nothing then
                                        reouvertureDemandeUrl

                                    else
                                        clotureDemandeUrl
                            in
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (clotureDemande url demande)
                                |> withEntity NoResult

                        ClotureSuivi _ ->
                            model
                                |> Effect.withNone
                                |> withEntity NoResult

                        TransformationBrouillon transformationBrouillonData ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (transformBrouillon <| transformationBrouillonData)
                                |> withEntity NoResult

        CancelSuiviAction ->
            { model | saveSuiviRequest = RD.NotAsked, suiviAction = NoSuivi }
                |> Effect.withNone
                |> withEntity NoResult

        SelectedOnglet onglet ->
            { model | ongletActif = onglet |> Maybe.withDefault model.ongletActif }
                |> Effect.withNone
                |> withEntity NoResult

        SetTitreDemandeFilter add titreDemande ->
            { model
                | echangesModel =
                    model.echangesModel
                        |> (\em ->
                                { em
                                    | titresDemandes =
                                        setTitres em.titresDemandes add titreDemande
                                }
                           )
            }
                |> Effect.withNone
                |> withEntity NoResult

        SetTypeDemandeFilter add typeDemande ->
            { model
                | demandesModel =
                    model.demandesModel
                        |> (\dm ->
                                { dm
                                    | typesDemandes =
                                        setTypes dm.typesDemandes add typeDemande
                                }
                           )
            }
                |> Effect.withNone
                |> withEntity NoResult

        ViewEchanges titre ->
            { model
                | ongletActif = OngletEchanges
                , echangesModel =
                    model.echangesModel
                        |> (\em -> { em | titresDemandes = [ Just titre ] })
            }
                |> Effect.withNone
                |> withEntity NoResult


changeOnglet : SuiviConfig entity -> Model -> Onglet -> ( Model, Effect.Effect Shared.Msg Msg, SuiviResult entity )
changeOnglet config model onglet =
    update config (SelectedOnglet <| Just <| onglet) model


type SuiviAction
    = NoSuivi
    | NewSuivi SuiviType
    | EditSuivi SuiviType
    | DeleteSuivi SuiviType
    | ClotureSuivi SuiviType
    | TransformationBrouillon TransformationBrouillonData


type alias TransformationBrouillonData =
    { brouillon : Brouillon
    , suiviType : SuiviType
    , transformationEntite : TransformationEntite
    , etape : TransformationEtape
    , suppression : Bool
    }


type TransformationEtape
    = ChoixSuivi
    | ChoixEntite


type alias TransformationEntite =
    { entiteType : EntiteResultat
    , recherche : String
    , resultat : WebData EntiteResultat
    }


type EntiteResultat
    = EntiteEtab (Maybe EtablissementApercu)
    | EntiteEtabCrea (Maybe EtablissementCreationApercu)
    | EntiteLocal (Maybe LocalApercu)


defaultTransformationEntite : TransformationEntite
defaultTransformationEntite =
    { entiteType = EntiteEtab Nothing
    , recherche = ""
    , resultat = RD.NotAsked
    }


type SuiviType
    = SuiviEchange EchangeInput
    | SuiviRappel RappelInput
    | SuiviDemande DemandeInput
    | SuiviBrouillon BrouillonInput


suiviTypeToElementNom : SuiviType -> String
suiviTypeToElementNom suiviType =
    case suiviType of
        SuiviEchange _ ->
            "un échange"

        SuiviRappel _ ->
            "un rappel"

        SuiviDemande _ ->
            "une demande"

        SuiviBrouillon _ ->
            "un brouillon"


columnsLayout : ( Accessibility.Attribute msg, Accessibility.Attribute msg )
columnsLayout =
    ( DSFR.Grid.col3, DSFR.Grid.col9 )


viewSuiviBody : Shared.User -> Maybe Bool -> List Collegue -> Bool -> WebData entity -> Bool -> Date -> Maybe (List Demande) -> SuiviType -> Html Msg
viewSuiviBody user pourLocal devecos global saveSuiviRequest new today demandes suiviType =
    div []
        [ if new then
            div [ DSFR.Grid.gridRow ]
                [ div [ DSFR.Grid.col ]
                    [ if global then
                        nothing

                      else
                        DSFR.Radio.group
                            { id = "suivi-type-radio"
                            , options =
                                [ "Échange"
                                , "Demande"
                                , "Rappel"
                                ]
                            , current =
                                case suiviType of
                                    SuiviEchange _ ->
                                        Just "Échange"

                                    SuiviDemande _ ->
                                        Just "Demande"

                                    SuiviRappel _ ->
                                        Just "Rappel"

                                    SuiviBrouillon _ ->
                                        Just "Brouillon"
                            , toLabel = text
                            , toId = String.toLower
                            , msg =
                                \s ->
                                    let
                                        id =
                                            User.id user
                                    in
                                    case s of
                                        "Échange" ->
                                            SetSuiviAction <| NewSuivi <| SuiviEchange <| UI.Echange.default Nothing "" today

                                        "Rappel" ->
                                            SetSuiviAction <| NewSuivi <| SuiviRappel <| UI.Rappel.default Nothing id "" "" today

                                        "Demande" ->
                                            SetSuiviAction <| NewSuivi <| SuiviDemande <| UI.Demande.default Nothing

                                        "Brouillon" ->
                                            SetSuiviAction <| NewSuivi <| SuiviBrouillon <| UI.Brouillon.default "" <| Just today

                                        _ ->
                                            SetSuiviAction NoSuivi
                            , legend = Nothing
                            }
                            |> DSFR.Radio.inline
                            |> DSFR.Radio.view
                    ]
                ]

          else
            nothing
        , div [ DSFR.Grid.gridRow ]
            [ viewSuiviTypeBody demandes user devecos pourLocal suiviType
            ]
        , div [ class "py-4" ] [ viewSuiviFooter saveSuiviRequest suiviType ]
        ]


viewSuiviTypeBody : Maybe (List Demande) -> Shared.User -> List Collegue -> Maybe Bool -> SuiviType -> Html Msg
viewSuiviTypeBody demandes user devecos pourLocal suiviType =
    div [ DSFR.Grid.col, class "flex flex-col gap-4" ] <|
        case suiviType of
            SuiviEchange echange ->
                [ UI.Echange.form UpdatedEchangeForm echange
                , case demandes of
                    Nothing ->
                        nothing

                    Just dems ->
                        div [ class "flex flex-col gap-2" ] <|
                            (::) (text "Affecter l'échange à une ou plusieurs demandes\u{00A0}:") <|
                                case dems of
                                    [] ->
                                        [ span [ class "italic" ] [ text "Aucune demande créée pour l'instant" ] ]

                                    _ ->
                                        List.map (viewDemande echange) <|
                                            dems
                ]

            SuiviRappel rappel ->
                let
                    equipe =
                        user
                            |> User.role
                            |> Data.Role.equipe
                            |> Maybe.map Data.Equipe.nom
                            |> Maybe.withDefault "?"
                in
                [ UI.Rappel.form equipe devecos UpdatedRappelForm rappel ]

            SuiviDemande demande ->
                let
                    demandesListe =
                        case pourLocal of
                            Just True ->
                                Data.Demande.listeLocal

                            _ ->
                                Data.Demande.listeEtablissement
                in
                [ UI.Demande.form demandesListe UpdatedDemandeForm demande ]

            SuiviBrouillon brouillon ->
                [ div [ class "fr-text-default--info" ]
                    [ DSFR.Icons.System.infoFill |> DSFR.Icons.iconSM
                    , text " "
                    , text "Les brouillons sont visibles par tous les utilisateurs. Assurez-vous de ne pas y inclure d’informations sensibles ou confidentielles."
                    ]
                , let
                    link =
                        "mailto:" ++ Lib.Variables.brouillonEmail
                  in
                  div [ class "fr-text-default--info" ]
                    [ DSFR.Icons.System.infoFill |> DSFR.Icons.iconSM
                    , text " "
                    , text "Vous pouvez créer un brouillon simplement en envoyant ou transférant un e-mail à "
                    , Typo.link link [] [ text Lib.Variables.brouillonEmail ]
                    , text "."
                    , UI.Clipboard.clipboard
                        { content = Lib.Variables.brouillonEmail
                        , label = "Copier"
                        , copiedLabel = "Copié\u{00A0}!"
                        , timeoutMilliseconds = 2000
                        }
                    ]
                , UI.Brouillon.form UpdatedBrouillonForm brouillon
                ]


viewDemande : EchangeInput -> Demande -> Html Msg
viewDemande echangeInput demande =
    let
        selectionne =
            echangeInput
                |> .demandes
                |> List.member (Data.Demande.id demande)
    in
    Html.div
        [ Events.onClick <|
            UpdatedEchangeDemande (Data.Demande.id demande) <|
                not selectionne
        , Html.Attributes.id <| (++) "echange-demandes-demande-" <| entityIdToString <| Data.Demande.id <| demande
        , class "flex flex-col gap-4 p-4 border-2 cursor-pointer"
        , classList
            [ ( "dark-grey-border", not selectionne )
            , ( "dark-blue-border", selectionne )
            ]
        ]
        [ div [ class "flex flex-row justify-between" ]
            [ [ demandeTypeToTag demande ]
                |> DSFR.Tag.medium
            , span
                [ classList
                    [ ( "disabled-text", not selectionne )
                    , ( "blue-text", selectionne )
                    ]
                ]
                [ DSFR.Icons.iconMD DSFR.Icons.System.checkboxCircleLine
                ]
            ]
        , UI.Demande.viewDemandeEchanges (Just ViewEchanges) demande
        , h4 [ class "whitespace-pre-wrap !mb-0" ]
            [ span [ class "font-bold" ]
                [ text <| withEmptyAs "-" <| Data.Demande.nom demande
                ]
            ]
        ]


viewSuiviFooter : WebData entity -> SuiviType -> Html Msg
viewSuiviFooter saveSuiviRequest suiviType =
    let
        disabled =
            saveSuiviRequest
                == RD.Loading
                || (case suiviType of
                        SuiviEchange echange ->
                            not <| UI.Echange.isValid echange

                        SuiviRappel rappel ->
                            not <| UI.Rappel.isValid rappel

                        SuiviDemande _ ->
                            False

                        SuiviBrouillon _ ->
                            False
                   )
    in
    DSFR.Button.group
        [ DSFR.Button.new { onClick = Just <| ConfirmSuiviAction, label = "Enregistrer" }
            |> DSFR.Button.submit
            |> DSFR.Button.withDisabled disabled
        , DSFR.Button.new { onClick = Just <| CancelSuiviAction, label = "Annuler" }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


viewDeleteSuiviBody : WebData entity -> Html Msg
viewDeleteSuiviBody saveSuiviRequest =
    div [ DSFR.Grid.col, class "flex flex-col gap-4" ]
        [ let
            disabled =
                saveSuiviRequest == RD.Loading
          in
          DSFR.Button.group
            [ DSFR.Button.new { onClick = Just <| ConfirmSuiviAction, label = "Confirmer" }
                |> DSFR.Button.submit
                |> DSFR.Button.withDisabled disabled
            , DSFR.Button.new { onClick = Just <| CancelSuiviAction, label = "Annuler" }
                |> DSFR.Button.secondary
            ]
            |> DSFR.Button.inline
            |> DSFR.Button.alignedRightInverted
            |> DSFR.Button.viewGroup
        ]


viewClotureSuiviBody : WebData entity -> SuiviType -> Html Msg
viewClotureSuiviBody saveSuiviRequest suiviType =
    div [ DSFR.Grid.col, class "flex flex-col gap-4" ]
        [ case suiviType of
            SuiviRappel rappel ->
                let
                    action =
                        if rappel.clotureDate /= Nothing then
                            "clôturer"

                        else
                            "réouvrir"
                in
                text <| "Êtes-vous sûr(e) de vouloir " ++ action ++ " ce rappel\u{00A0}?"

            SuiviDemande demande ->
                let
                    action =
                        if demande.cloture then
                            "réouvrir"

                        else
                            "clôturer"
                in
                div []
                    [ text <| "Êtes-vous sûr(e) de vouloir " ++ action ++ " cette demande\u{00A0}?"
                    , viewIf (not demande.cloture) <|
                        div [ class "flex flex-col gap-4" ]
                            [ div [ class "fr-form-group" ]
                                [ DSFR.Input.new
                                    { value = demande.motif
                                    , onInput = UpdatedClotureMotif
                                    , label = text "Motif"
                                    , name = "nouvelle-fiche-particulier-motif-cloture"
                                    }
                                    |> DSFR.Input.textArea (Just 4)
                                    |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                                    |> DSFR.Input.view
                                ]
                            ]
                    ]

            _ ->
                nothing
        , let
            disabled =
                saveSuiviRequest == RD.Loading
          in
          DSFR.Button.group
            [ DSFR.Button.new { onClick = Just <| ConfirmSuiviAction, label = "Confirmer" }
                |> DSFR.Button.submit
                |> DSFR.Button.withDisabled disabled
            , DSFR.Button.new { onClick = Just <| CancelSuiviAction, label = "Annuler" }
                |> DSFR.Button.secondary
            ]
            |> DSFR.Button.inline
            |> DSFR.Button.alignedRightInverted
            |> DSFR.Button.viewGroup
        ]


viewTransformationBrouillonBody : Shared.User -> Maybe Bool -> List Collegue -> TransformationBrouillonData -> Html Msg
viewTransformationBrouillonBody user pourLocal devecos ({ brouillon, etape, suiviType, transformationEntite } as transformationBrouillonData) =
    case etape of
        ChoixSuivi ->
            div []
                [ div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col ]
                        [ DSFR.Stepper.raw
                            { titreCurrent = "Transformer le brouillon du " ++ Lib.Date.formatDateShort (Data.Brouillon.date brouillon)
                            , titreNext = Just "Lier le brouillon à un établissement, créateur d'entreprise ou à un local"
                            , stepCurrent = 1
                            , stepTotal = 2
                            }
                        , DSFR.Radio.group
                            { id = "suivi-type-radio"
                            , options =
                                [ "Échange"
                                , "Demande"
                                , "Rappel"
                                ]
                            , current =
                                case suiviType of
                                    SuiviEchange _ ->
                                        Just "Échange"

                                    SuiviDemande _ ->
                                        Just "Demande"

                                    SuiviRappel _ ->
                                        Just "Rappel"

                                    SuiviBrouillon _ ->
                                        Just "Brouillon"
                            , toLabel = text
                            , toId = String.toLower
                            , msg =
                                \s ->
                                    let
                                        id =
                                            User.id user
                                    in
                                    case s of
                                        "Échange" ->
                                            SetSuiviAction <|
                                                TransformationBrouillon <|
                                                    { transformationBrouillonData
                                                        | suiviType =
                                                            SuiviEchange <|
                                                                brouillonToEchangeInput <|
                                                                    brouillon
                                                    }

                                        "Rappel" ->
                                            SetSuiviAction <|
                                                TransformationBrouillon <|
                                                    { transformationBrouillonData
                                                        | suiviType =
                                                            SuiviRappel <|
                                                                brouillonToRappelInput id <|
                                                                    brouillon
                                                    }

                                        "Demande" ->
                                            SetSuiviAction <|
                                                TransformationBrouillon <|
                                                    { transformationBrouillonData
                                                        | suiviType =
                                                            SuiviDemande <|
                                                                brouillonToDemandeInput <|
                                                                    brouillon
                                                    }

                                        _ ->
                                            SetSuiviAction NoSuivi
                            , legend = Just <| text "Type d'élément"
                            }
                            |> DSFR.Radio.inline
                            |> DSFR.Radio.view
                        ]
                    ]
                , div [ DSFR.Grid.gridRow ]
                    [ viewSuiviTypeBody Nothing user devecos pourLocal transformationBrouillonData.suiviType
                    ]
                , div [ class "py-4" ]
                    [ DSFR.Button.group
                        [ DSFR.Button.new
                            { onClick =
                                Just <|
                                    SetSuiviAction <|
                                        TransformationBrouillon <|
                                            { transformationBrouillonData
                                                | etape =
                                                    ChoixEntite
                                            }
                            , label = "Étape suivante"
                            }
                            |> DSFR.Button.submit
                        , DSFR.Button.new { onClick = Just <| CancelSuiviAction, label = "Annuler" }
                            |> DSFR.Button.secondary
                        ]
                        |> DSFR.Button.inline
                        |> DSFR.Button.alignedRightInverted
                        |> DSFR.Button.viewGroup
                    ]
                ]

        ChoixEntite ->
            let
                suiviTexte =
                    case transformationBrouillonData.suiviType of
                        SuiviEchange _ ->
                            "l'échange"

                        SuiviDemande _ ->
                            "la demande"

                        SuiviRappel _ ->
                            "le rappel"

                        SuiviBrouillon _ ->
                            "le brouillon"
            in
            div [ class "flex flex-col gap-4" ]
                [ div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col ]
                        [ DSFR.Stepper.raw
                            { titreCurrent = "Lier " ++ suiviTexte ++ " du " ++ Lib.Date.formatDateShort (Data.Brouillon.date brouillon)
                            , titreNext = Nothing
                            , stepCurrent = 2
                            , stepTotal = 2
                            }
                        , DSFR.Radio.group
                            { id = "entite-type-radio"
                            , options =
                                [ "Établissement"
                                , "Créateur d'entreprise"
                                , "Local"
                                ]
                            , current =
                                case transformationEntite.entiteType of
                                    EntiteEtab _ ->
                                        Just "Établissement"

                                    EntiteEtabCrea _ ->
                                        Just "Créateur d'entreprise"

                                    EntiteLocal _ ->
                                        Just "Local"
                            , toLabel = text
                            , toId = String.toLower
                            , msg =
                                \s ->
                                    case s of
                                        "Établissement" ->
                                            SetSuiviAction <|
                                                TransformationBrouillon <|
                                                    { transformationBrouillonData
                                                        | transformationEntite =
                                                            transformationBrouillonData.transformationEntite
                                                                |> (\te ->
                                                                        { te
                                                                            | entiteType = EntiteEtab Nothing
                                                                            , recherche = ""
                                                                            , resultat = RD.NotAsked
                                                                        }
                                                                   )
                                                    }

                                        "Créateur d'entreprise" ->
                                            SetSuiviAction <|
                                                TransformationBrouillon <|
                                                    { transformationBrouillonData
                                                        | transformationEntite =
                                                            transformationBrouillonData.transformationEntite
                                                                |> (\te ->
                                                                        { te
                                                                            | entiteType = EntiteEtabCrea Nothing
                                                                            , recherche = ""
                                                                            , resultat = RD.NotAsked
                                                                        }
                                                                   )
                                                    }

                                        "Local" ->
                                            SetSuiviAction <|
                                                TransformationBrouillon <|
                                                    { transformationBrouillonData
                                                        | transformationEntite =
                                                            transformationBrouillonData.transformationEntite
                                                                |> (\te ->
                                                                        { te
                                                                            | entiteType = EntiteLocal Nothing
                                                                            , recherche = ""
                                                                            , resultat = RD.NotAsked
                                                                        }
                                                                   )
                                                    }

                                        _ ->
                                            NoOp
                            , legend = Nothing
                            }
                            |> DSFR.Radio.inline
                            |> DSFR.Radio.view
                        ]
                    ]
                , case transformationBrouillonData.transformationEntite.entiteType of
                    EntiteEtab _ ->
                        viewSiretForm transformationBrouillonData.transformationEntite.recherche <|
                            RD.map
                                (\entite ->
                                    case entite of
                                        EntiteEtab (Just etab) ->
                                            Just etab

                                        _ ->
                                            Nothing
                                )
                                transformationBrouillonData.transformationEntite.resultat

                    EntiteEtabCrea _ ->
                        viewEtabCreaIdForm transformationBrouillonData.transformationEntite.recherche <|
                            RD.map
                                (\entite ->
                                    case entite of
                                        EntiteEtabCrea (Just etabCrea) ->
                                            Just etabCrea

                                        _ ->
                                            Nothing
                                )
                                transformationBrouillonData.transformationEntite.resultat

                    EntiteLocal _ ->
                        viewLocalIdForm transformationBrouillonData.transformationEntite.recherche <|
                            RD.map
                                (\entite ->
                                    case entite of
                                        EntiteLocal (Just local) ->
                                            Just local

                                        _ ->
                                            Nothing
                                )
                                transformationBrouillonData.transformationEntite.resultat
                , DSFR.Checkbox.single
                    { value = "suppression-brouillon"
                    , checked = Just transformationBrouillonData.suppression
                    , valueAsString = identity
                    , id = "suppression-brouillon"
                    , label = text "Une fois lié, je souhaite que le brouillon soit supprimé"
                    , onChecked = \_ bool -> SetSuiviAction <| TransformationBrouillon { transformationBrouillonData | suppression = bool }
                    }
                    |> DSFR.Checkbox.viewSingle
                , div [ class "py-4" ]
                    [ DSFR.Button.group
                        [ DSFR.Button.new
                            { onClick = Just <| ConfirmSuiviAction
                            , label = "Valider"
                            }
                            |> DSFR.Button.withDisabled
                                (transformationBrouillonData.transformationEntite.resultat
                                    |> RD.map
                                        (\e ->
                                            case e of
                                                EntiteEtab (Just _) ->
                                                    False

                                                EntiteEtabCrea (Just _) ->
                                                    False

                                                EntiteLocal (Just _) ->
                                                    False

                                                _ ->
                                                    True
                                        )
                                    |> RD.withDefault True
                                )
                            |> DSFR.Button.submit
                        , DSFR.Button.new
                            { onClick =
                                Just <|
                                    SetSuiviAction <|
                                        TransformationBrouillon <|
                                            { transformationBrouillonData
                                                | etape =
                                                    ChoixSuivi
                                            }
                            , label = "Retour"
                            }
                            |> DSFR.Button.secondary
                        ]
                        |> DSFR.Button.inline
                        |> DSFR.Button.alignedRightInverted
                        |> DSFR.Button.viewGroup
                    ]
                ]


viewSiretForm : String -> WebData (Maybe EtablissementApercu) -> Html Msg
viewSiretForm siretRecherche siretResultat =
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col gap-2" ]
            [ DSFR.SearchBar.searchBar
                { errors = []
                , extraAttrs =
                    [ Html.Attributes.pattern "(\\s*[0-9]\\s*){14}"
                    , Html.Attributes.title "Un SIRET (14 chiffres)"
                    ]
                , disabled = String.trim siretRecherche == ""
                }
                { submitMsg = RequestEntite
                , buttonLabel = "Rechercher"
                , inputMsg = UpdatedEntiteId
                , inputLabel = "SIRET de l'établissement"
                , inputPlaceholder = Nothing
                , inputId = "etablissement-recherche-siret"
                , inputValue = siretRecherche
                , hints = [ text "Le numéro SIRET comprend 14 chiffres" ]
                , fullLabel = Just <| text <| "SIRET de l'établissement"
                }
            , div [ Typo.textSm, class "fr-text-default--info !mb-0" ]
                [ DSFR.Icons.System.infoFill |> DSFR.Icons.iconSM
                , text " "
                , text "Effectuer une recherche sur Deveco pour trouver le SIRET en "
                , Typo.externalLink (Route.toUrl <| Route.Etablissements Nothing) [] [ text "cliquant ici" ]
                ]
            , case siretResultat of
                RD.Failure _ ->
                    div [ class "flex flex-col p-4 gap-4" ]
                        [ div [] [ text "Une erreur s'est produite. Si le problème persiste, veuillez nous contacter." ]
                        ]

                RD.Loading ->
                    div [ class "flex flex-col p-4 gap-4" ]
                        [ div [] [ text "Récupération en cours..." ]
                        ]

                RD.Success e ->
                    div [ class "flex flex-col gap-4" ]
                        [ div [] <|
                            List.singleton <|
                                text "Résultat\u{00A0}:"
                        , viewEtablissementResultat e
                        ]

                _ ->
                    nothing
            ]
        ]


viewEtablissementResultat : Maybe EtablissementApercu -> Html Msg
viewEtablissementResultat etablissementApercu =
    case etablissementApercu of
        Nothing ->
            div [ class "flex flex-col p-4 gap-4" ]
                [ div []
                    [ text "Aucun établissement trouvé avec ce SIRET"
                    ]
                ]

        Just etablissement ->
            div
                [ class "flex flex-col gap-4 p-4 border-2"
                ]
                [ div [ class "flex flex-row justify-between" ]
                    [ div [ Typo.textXs, class "!mb-0" ]
                        [ DSFR.Icons.iconSM iconeEtablissement
                        , text " "
                        , text "Établissement"
                        ]
                    ]
                , div []
                    [ div [ Typo.textBold, class "flex flex-row gap-4 items-center" ]
                        [ h4 [ Typo.fr_h5, class "!mb-0" ]
                            [ text <|
                                etablissement.nomAffichage
                            ]
                        ]
                    , div []
                        [ text "Adresse\u{00A0}: "
                        , span [ Typo.textBold ]
                            [ text <|
                                withEmptyAs "-" <|
                                    etablissement.adresse
                            ]
                        ]
                    , div []
                        [ text "SIRET\u{00A0}: "
                        , span [ Typo.textBold ]
                            [ text <|
                                withEmptyAs "-" <|
                                    etablissement.siret
                            ]
                        ]
                    ]
                ]


viewEtabCreaIdForm : String -> WebData (Maybe EtablissementCreationApercu) -> Html Msg
viewEtabCreaIdForm idRecherche etabCreaResultat =
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col gap-2" ]
            [ DSFR.SearchBar.searchBar
                { errors = []
                , extraAttrs =
                    [ Html.Attributes.pattern "(\\s*[0-9]\\s*){6}"
                    , Html.Attributes.title "Un identifiant de Créateur d'entreprise (6 chiffres)"
                    ]
                , disabled = String.trim idRecherche == ""
                }
                { submitMsg = RequestEntite
                , buttonLabel = "Rechercher"
                , inputMsg = UpdatedEntiteId
                , inputLabel = "Identifiant du créateur d'entreprise"
                , inputPlaceholder = Nothing
                , inputId = "etab-crea-recherche-id"
                , inputValue = idRecherche
                , hints = [ text "L'identifiant comprend 6 chiffres" ]
                , fullLabel = Just <| text <| "Identifiant du créateur d'entreprise"
                }
            , div [ Typo.textSm, class "fr-text-default--info !mb-0" ]
                [ DSFR.Icons.System.infoFill |> DSFR.Icons.iconSM
                , text " "
                , text "Effectuer une recherche sur Deveco pour trouver l'identifiant en "
                , Typo.externalLink (Route.toUrl <| Route.Createurs Nothing) [] [ text "cliquant ici" ]
                ]
            , case etabCreaResultat of
                RD.Failure _ ->
                    div [ class "flex flex-col p-4 gap-4" ]
                        [ div [] [ text "Une erreur s'est produite. Si le problème persiste, veuillez nous contacter." ]
                        ]

                RD.Loading ->
                    div [ class "flex flex-col p-4 gap-4" ]
                        [ div [] [ text "Récupération en cours..." ]
                        ]

                RD.Success e ->
                    div [ class "flex flex-col gap-4 !mt-4" ]
                        [ div [] <|
                            List.singleton <|
                                text "Résultat\u{00A0}:"
                        , viewEtabCreaResultat e
                        ]

                _ ->
                    nothing
            ]
        ]


viewEtabCreaResultat : Maybe EtablissementCreationApercu -> Html Msg
viewEtabCreaResultat etabCrea =
    case etabCrea of
        Nothing ->
            div [ class "flex flex-col p-4 gap-4" ]
                [ div []
                    [ text "Aucun créateur d'entreprise trouvé avec cet identifiant"
                    ]
                ]

        Just ec ->
            div
                [ class "flex flex-col gap-4 p-4 border-2"
                ]
                [ div [ class "flex flex-row justify-between" ]
                    [ div [ Typo.textXs, class "!mb-0" ]
                        [ DSFR.Icons.iconSM iconeCreateur
                        , text " "
                        , text "Création d'entreprise"
                        ]
                    ]
                , div []
                    [ div [ Typo.textBold, class "flex flex-row gap-4 items-center" ]
                        [ h4 [ Typo.fr_h5, class "!mb-0" ]
                            [ text <|
                                ec.nom
                            ]
                        ]
                    ]
                ]


viewLocalIdForm : String -> WebData (Maybe LocalApercu) -> Html Msg
viewLocalIdForm localIdRecherche localResultat =
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col gap-2" ]
            [ DSFR.SearchBar.searchBar
                { errors = []
                , extraAttrs =
                    [ Html.Attributes.pattern "(\\s*[0-9]\\s*){12}"
                    , Html.Attributes.title "Un numéro d'identifiant (12 chiffres)"
                    ]
                , disabled = String.trim localIdRecherche == ""
                }
                { submitMsg = RequestEntite
                , buttonLabel = "Rechercher"
                , inputMsg = UpdatedEntiteId
                , inputLabel = "Identifiant du local"
                , inputPlaceholder = Nothing
                , inputId = "local-recherche-identifiant"
                , inputValue = localIdRecherche
                , hints = [ text "Le numéro d'identifiant comprend 12 chiffres" ]
                , fullLabel = Just <| text <| "Identifiant du local"
                }
            , div [ Typo.textSm, class "fr-text-default--info !mb-0" ]
                [ DSFR.Icons.System.infoFill |> DSFR.Icons.iconSM
                , text " "
                , text "Effectuer une recherche sur Deveco pour trouver l'identifiant en "
                , Typo.externalLink (Route.toUrl <| Route.Locaux Nothing) [] [ text "cliquant ici" ]
                ]
            , case localResultat of
                RD.Failure _ ->
                    div [ class "flex flex-col p-4 gap-4" ]
                        [ div [] [ text "Une erreur s'est produite. Si le problème persiste, veuillez nous contacter." ]
                        ]

                RD.Loading ->
                    div [ class "flex flex-col p-4 gap-4" ]
                        [ div [] [ text "Récupération en cours..." ]
                        ]

                RD.Success e ->
                    div [ class "flex flex-col gap-4 !mt-4" ]
                        [ div [] <|
                            List.singleton <|
                                text "Résultat\u{00A0}:"
                        , viewLocalResultat e
                        ]

                _ ->
                    nothing
            ]
        ]


viewLocalResultat : Maybe LocalApercu -> Html Msg
viewLocalResultat local =
    case local of
        Nothing ->
            div [ class "flex flex-col p-4 gap-4" ]
                [ div []
                    [ text "Aucun local trouvé avec cet identifiant"
                    ]
                ]

        Just loc ->
            div
                [ class "flex flex-col gap-4 p-4 border-2"
                ]
                [ div [ class "flex flex-row justify-between" ]
                    [ div [ Typo.textXs, class "!mb-0" ]
                        [ DSFR.Icons.iconSM iconeLocal
                        , text " "
                        , text "Local"
                        ]
                    ]
                , div []
                    [ div [ Typo.textBold, class "flex flex-row gap-4 items-center" ]
                        [ h4 [ Typo.fr_h5, class "!mb-0" ]
                            [ text <|
                                loc.nom
                            ]
                        ]
                    , div []
                        [ text "Adresse\u{00A0}: "
                        , span [ Typo.textBold ]
                            [ text <|
                                withEmptyAs "-" <|
                                    adresseToString <|
                                        loc.adresse
                            ]
                        ]
                    ]
                ]


viewSuivisModal : Zone -> Posix -> Shared.User -> Maybe Bool -> WebData (List Collegue) -> Bool -> List Demande -> Model -> Html Msg
viewSuivisModal timezone now user pourLocal rdDevecos avecBrouillons demandes { saveSuiviRequest, suiviAction } =
    let
        today =
            Date.fromPosix timezone now

        devecos =
            rdDevecos |> RD.withDefault [] |> List.filter .actif

        ( opened, content, title ) =
            case suiviAction of
                NoSuivi ->
                    ( False, nothing, nothing )

                NewSuivi suiviType ->
                    ( True, viewSuiviBody user pourLocal devecos avecBrouillons saveSuiviRequest True today (Just demandes) suiviType, text <| "Ajouter " ++ suiviTypeToElementNom suiviType )

                EditSuivi suiviType ->
                    ( True, viewSuiviBody user pourLocal devecos avecBrouillons saveSuiviRequest False today (Just demandes) suiviType, text <| "Modifier " ++ suiviTypeToElementNom suiviType )

                DeleteSuivi suiviType ->
                    ( True
                    , viewDeleteSuiviBody saveSuiviRequest
                    , text <|
                        case suiviType of
                            SuiviEchange _ ->
                                "Supprimer un échange"

                            SuiviRappel _ ->
                                "Supprimer un rappel"

                            SuiviDemande _ ->
                                "Supprimer une demande"

                            SuiviBrouillon _ ->
                                "Supprimer un brouillon"
                    )

                ClotureSuivi suiviType ->
                    ( True
                    , viewClotureSuiviBody saveSuiviRequest suiviType
                    , text <|
                        case suiviType of
                            SuiviEchange _ ->
                                "Clôturer un échange"

                            SuiviRappel rappel ->
                                if rappel.clotureDate /= Nothing then
                                    "Clôturer un rappel"

                                else
                                    "Réouvrir un rappel"

                            SuiviDemande demande ->
                                if demande.cloture then
                                    "Réouvrir une demande"

                                else
                                    "Clôturer une demande"

                            SuiviBrouillon _ ->
                                "Clôturer un brouillon"
                    )

                TransformationBrouillon transformationBrouillonData ->
                    ( True
                    , viewTransformationBrouillonBody
                        user
                        pourLocal
                        devecos
                        transformationBrouillonData
                    , nothing
                    )
    in
    DSFR.Modal.view
        { id = "suivi"
        , label = "suivi"
        , openMsg = NoOp
        , closeMsg = Just CancelSuiviAction
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


viewOngletSuivis :
    Zone
    -> Posix
    -> Bool
    -> Bool
    -> Shared.User
    -> Maybe (Html Msg)
    -> List Echange
    -> List Demande
    -> List Rappel
    -> Maybe (List Brouillon)
    -> Model
    -> Html Msg
viewOngletSuivis timezone now forPrint global user extraBlock echanges demandes rappels brouillons { suiviAction, ongletActif, echangesModel, demandesModel, rappelsModel, brouillonsModel } =
    div [ class "flex flex-col gap-8" ]
        [ viewSuiviAction timezone now user global ongletActif suiviAction
        , if global then
            div []
                [ DSFR.Alert.small
                    { title = Nothing
                    , description =
                        span [ Typo.textSm ]
                            [ text "Actuellement, seuls les 20 derniers échanges et demandes sont visibles."
                            ]
                    }
                    |> DSFR.Alert.alert Nothing DSFR.Alert.info
                ]

          else
            nothing
        , viewMaybe identity extraBlock
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ viewMenuLateral timezone now echangesModel demandesModel rappelsModel brouillonsModel echanges demandes rappels brouillons ongletActif
            , viewSuiviListe timezone now forPrint echangesModel demandesModel rappelsModel brouillonsModel echanges demandes rappels (Maybe.withDefault [] brouillons) ongletActif
            ]
        ]


viewSuiviAction : Zone -> Posix -> User.User -> Bool -> Onglet -> SuiviAction -> Html Msg
viewSuiviAction timezone now user global ongletActif suiviAction =
    let
        today =
            Date.fromPosix timezone now
    in
    div [ class "p-4 sm:p-8 fr-card--white" ]
        [ div [ class "flex flex-row justify-between items-center" ]
            [ h3 [ Typo.fr_h4, class "!mb-0" ] [ text "Suivi de la relation" ]
            , DSFR.Button.new
                { label =
                    if global then
                        "Ajouter un brouillon"

                    else
                        "Ajouter un élément de suivi"
                , onClick =
                    if global then
                        Just <|
                            SetSuiviAction <|
                                NewSuivi <|
                                    SuiviBrouillon <|
                                        UI.Brouillon.default "" <|
                                            Just today

                    else
                        let
                            id =
                                User.id user
                        in
                        Just <|
                            SetSuiviAction <|
                                NewSuivi <|
                                    case ongletActif of
                                        OngletEchanges ->
                                            SuiviEchange <|
                                                UI.Echange.default Nothing "" today

                                        OngletDemandes ->
                                            SuiviDemande <|
                                                UI.Demande.default Nothing

                                        OngletRappels ->
                                            SuiviRappel <|
                                                UI.Rappel.default Nothing id "" "" today

                                        OngletBrouillons ->
                                            SuiviBrouillon <|
                                                UI.Brouillon.default "" <|
                                                    Just today
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                |> DSFR.Button.withDisabled (suiviAction /= NoSuivi)
                |> DSFR.Button.view
            ]
        ]


viewMenuLateral : Zone -> Posix -> EchangesModel -> DemandesModel -> RappelsModel -> BrouillonsModel -> List Echange -> List Demande -> List Rappel -> Maybe (List Brouillon) -> Onglet -> Html Msg
viewMenuLateral timezone now echangesModel demandesModel rappelsModel brouillonsModel echanges demandes rappels brouillons ongletActif =
    div [ Tuple.first columnsLayout, class "flex flex-col gap-6" ]
        [ div [ class "fr-card--white" ]
            [ Html.Lazy.lazy5 menuLateral echanges demandes rappels brouillons ongletActif
            ]
        , div [ class "fr-card--white" ]
            [ menuLateralFiltres timezone now echangesModel demandesModel rappelsModel brouillonsModel echanges demandes rappels (Maybe.withDefault [] brouillons) ongletActif
            ]
        ]


viewSuiviListe : Zone -> Posix -> Bool -> EchangesModel -> DemandesModel -> RappelsModel -> BrouillonsModel -> List Echange -> List Demande -> List Rappel -> List Brouillon -> Onglet -> Html Msg
viewSuiviListe timezone now forPrint echangesModel demandesModel rappelsModel brouillonsModel echanges demandes rappels brouillons ongletActif =
    div [ Tuple.second columnsLayout ]
        [ div []
            [ div [ classList <| [ ( "hidden", ongletActif /= OngletEchanges ) ] ] <|
                List.singleton <|
                    Html.Lazy.lazy4 viewOngletEchanges timezone echangesModel demandes echanges
            , div [ classList <| [ ( "hidden", ongletActif /= OngletDemandes ) ] ] <|
                List.singleton <|
                    Html.Lazy.lazy3 viewOngletDemandes forPrint demandesModel demandes
            , div [ classList <| [ ( "hidden", ongletActif /= OngletRappels ) ] ] <|
                List.singleton <|
                    UI.Rappel.viewOngletRappels
                        { edit = SetSuiviAction << EditSuivi << SuiviRappel
                        , delete = SetSuiviAction << DeleteSuivi << SuiviRappel
                        , closeReopen = SetSuiviAction << ClotureSuivi << SuiviRappel
                        }
                        timezone
                        now
                        rappelsModel.rappelsMode
                        rappels
            , div [ classList <| [ ( "hidden", ongletActif /= OngletBrouillons ) ] ] <|
                List.singleton <|
                    Html.Lazy.lazy3 viewOngletBrouillons timezone brouillonsModel brouillons
            ]
        ]


viewOngletEchanges : Zone -> EchangesModel -> List Demande -> List Echange -> Html Msg
viewOngletEchanges timezone { titresDemandes } demandes echanges =
    case echanges of
        [] ->
            div [ class "p-4 sm:p-8 fr-card--white" ] <|
                List.singleton <|
                    span [ class "text-center italic" ] [ text "Aucun échange pour l'instant" ]

        _ ->
            let
                filtreEchange e =
                    case titresDemandes of
                        [] ->
                            True

                        _ ->
                            titresDemandes
                                |> List.any
                                    (\td ->
                                        case td of
                                            Nothing ->
                                                e
                                                    |> Data.Echange.demandes
                                                    |> List.length
                                                    |> (==) 0

                                            Just titre ->
                                                e
                                                    |> Data.Echange.demandes
                                                    |> List.filterMap (getDemandeInDemandes demandes)
                                                    |> List.map Data.Demande.nom
                                                    |> List.member titre
                                    )

                filteredEchanges =
                    List.filter filtreEchange <|
                        echanges
            in
            case filteredEchanges of
                [] ->
                    div [ class "p-4 sm:p-8 fr-card--white" ] <|
                        List.singleton <|
                            span [ class "text-center italic" ] [ text "Aucun échange pour ce filtre" ]

                _ ->
                    div [ class "flex flex-col gap-6" ]
                        (List.map
                            (\echange ->
                                Keyed.node "div" [ class "fr-card--white p-4" ] <|
                                    [ ( "echange-" ++ (entityIdToString <| Data.Echange.id <| echange)
                                      , UI.Echange.view
                                            { edit = SetSuiviAction << EditSuivi << SuiviEchange
                                            , delete = SetSuiviAction << DeleteSuivi << SuiviEchange
                                            }
                                            timezone
                                            demandes
                                            echange
                                      )
                                    ]
                            )
                         <|
                            List.sortWith sortEchanges <|
                                filteredEchanges
                        )


sortEchanges : Echange -> Echange -> Order
sortEchanges e1 e2 =
    let
        comparison =
            Lib.Date.sortOlderDateLast
                Data.Echange.date
                e1
                e2
    in
    case comparison of
        EQ ->
            Api.EntityId.compare (Data.Echange.id e2) (Data.Echange.id e1)

        _ ->
            comparison


viewOngletDemandes : Bool -> DemandesModel -> List Demande -> Html Msg
viewOngletDemandes forPrint { typesDemandes } demandes =
    case demandes of
        [] ->
            div [ class "p-4 sm:p-8 fr-card--white" ] <|
                List.singleton <|
                    span [ class "text-center italic" ] [ text "Aucune demande pour l'instant" ]

        _ ->
            let
                filtreDemande d =
                    case typesDemandes of
                        [] ->
                            True

                        _ ->
                            d
                                |> Data.Demande.type_
                                |> (\t -> List.member t typesDemandes)

                filteredDemandes =
                    List.filter filtreDemande <|
                        demandes
            in
            case filteredDemandes of
                [] ->
                    div [ class "p-4 sm:p-8 fr-card--white" ] <|
                        List.singleton <|
                            span [ class "text-center italic" ] [ text "Aucune demande pour ce filtre" ]

                _ ->
                    div [ class "flex flex-col gap-6" ]
                        (List.map
                            (\demande ->
                                Keyed.node "div" [ class "fr-card--white p-4" ] <|
                                    [ ( "demande-" ++ (entityIdToString <| Data.Demande.id <| demande)
                                      , UI.Demande.view
                                            { edit = SetSuiviAction << EditSuivi << SuiviDemande << UI.Demande.demandeToDemandeInput Nothing
                                            , closeReopen = SetSuiviAction << ClotureSuivi << SuiviDemande << UI.Demande.demandeToDemandeInput Nothing
                                            , delete = SetSuiviAction << DeleteSuivi << SuiviDemande << UI.Demande.demandeToDemandeInput Nothing
                                            , viewEchanges = Just ViewEchanges
                                            , forPrint = forPrint
                                            }
                                            demande
                                      )
                                    ]
                            )
                         <|
                            List.sortWith sortDemandes <|
                                filteredDemandes
                        )


viewOngletBrouillons : Zone -> BrouillonsModel -> List Brouillon -> Html Msg
viewOngletBrouillons timezone _ brouillons =
    case brouillons of
        [] ->
            div [ class "p-4 sm:p-8 fr-card--white" ] <|
                List.singleton <|
                    span [ class "text-center italic" ] [ text "Aucun brouillon pour l'instant" ]

        _ ->
            div [ class "flex flex-col gap-6" ]
                (List.map
                    (\brouillon ->
                        Keyed.node "div" [ class "fr-card--white p-4" ] <|
                            [ ( "brouillon-" ++ (entityIdToString <| Data.Brouillon.id <| brouillon)
                              , UI.Brouillon.view timezone
                                    { edit = SetSuiviAction << EditSuivi << SuiviBrouillon
                                    , delete = SetSuiviAction << DeleteSuivi << SuiviBrouillon
                                    , transform =
                                        \b ->
                                            SetSuiviAction <|
                                                TransformationBrouillon <|
                                                    TransformationBrouillonData
                                                        b
                                                        (SuiviEchange <| brouillonToEchangeInput <| b)
                                                        defaultTransformationEntite
                                                        ChoixSuivi
                                                        False
                                    }
                                    brouillon
                              )
                            ]
                    )
                 <|
                    List.sortWith sortBrouillons <|
                        brouillons
                )


brouillonToEchangeInput : Brouillon -> EchangeInput
brouillonToEchangeInput brouillon =
    UI.Echange.default (Just <| Data.Brouillon.id brouillon) (Data.Brouillon.titre brouillon) (Data.Brouillon.date brouillon)
        |> (\ei -> { ei | description = Data.Brouillon.description brouillon })


brouillonToDemandeInput : Brouillon -> DemandeInput
brouillonToDemandeInput brouillon =
    UI.Demande.default (Just <| Data.Brouillon.id brouillon)
        |> (\ei ->
                { ei
                    | nom = Data.Brouillon.titre brouillon
                    , description = Data.Brouillon.description brouillon
                }
           )


brouillonToRappelInput : EntityId User.UserId -> Brouillon -> RappelInput
brouillonToRappelInput userId brouillon =
    UI.Rappel.default (Just <| Data.Brouillon.id brouillon) userId (Data.Brouillon.titre brouillon) (Data.Brouillon.description brouillon) (Data.Brouillon.date brouillon)


sortDemandes : Demande -> Demande -> Order
sortDemandes d1 d2 =
    let
        comparison =
            Lib.Date.sortOlderDateLast
                Data.Demande.date
                d1
                d2
    in
    case comparison of
        EQ ->
            Api.EntityId.compare (Data.Demande.id d2) (Data.Demande.id d1)

        _ ->
            comparison


sortBrouillons : Brouillon -> Brouillon -> Order
sortBrouillons b1 b2 =
    let
        comparison =
            Lib.Date.sortOlderDateLast
                Data.Brouillon.date
                b1
                b2
    in
    case comparison of
        EQ ->
            Api.EntityId.compare (Data.Brouillon.id b2) (Data.Brouillon.id b1)

        _ ->
            comparison


createEchange : (Maybe Entite -> Maybe (EntityId EchangeId) -> String) -> EchangeInput -> Cmd Msg
createEchange url echangeInput =
    case UI.Echange.echangeInputToEchange echangeInput of
        Err _ ->
            Cmd.none

        Ok echange ->
            post
                { url = url (Data.Echange.entite echange) Nothing
                , body =
                    echangeInput
                        |> UI.Echange.encodeEchangeInput
                        |> Http.jsonBody
                }
                { toShared = SharedMsg
                , logger = Just LogError
                , handler = ReceivedSaveSuivi
                }
                Decode.value


createRappel : (Maybe Entite -> Maybe (EntityId RappelId) -> String) -> RappelInput -> Cmd Msg
createRappel url rappelInput =
    case UI.Rappel.rappelInputValidate rappelInput of
        Err _ ->
            Cmd.none

        Ok _ ->
            post
                { url = url rappelInput.entite Nothing
                , body = rappelInput |> UI.Rappel.encodeRappelInput |> Http.jsonBody
                }
                { toShared = SharedMsg
                , logger = Just LogError
                , handler = ReceivedSaveSuivi
                }
                Decode.value


createDemande : (Maybe Entite -> Maybe (EntityId DemandeId) -> String) -> DemandeInput -> Cmd Msg
createDemande url demandeInput =
    post
        { url = url demandeInput.entite Nothing
        , body = demandeInput |> UI.Demande.encodeDemandeInput |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveSuivi
        }
        Decode.value


createBrouillon : (Maybe (EntityId BrouillonId) -> String) -> BrouillonInput -> Cmd Msg
createBrouillon url brouillonInput =
    post
        { url = url Nothing
        , body = brouillonInput |> UI.Brouillon.encodeBrouillonInput |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveSuivi
        }
        Decode.value


updateEchange : (Maybe Entite -> Maybe (EntityId EchangeId) -> String) -> EchangeInput -> Cmd Msg
updateEchange url echangeInput =
    case UI.Echange.echangeInputToEchange echangeInput of
        Err _ ->
            Cmd.none

        Ok echange ->
            let
                jsonBody =
                    echangeInput
                        |> UI.Echange.encodeEchangeInput
                        |> Http.jsonBody
            in
            post
                { url = url (Data.Echange.entite echange) <| Just <| Data.Echange.id echange
                , body = jsonBody
                }
                { toShared = SharedMsg
                , logger = Just LogError
                , handler = ReceivedSaveSuivi
                }
                Decode.value


deleteEchange : (Maybe Entite -> Maybe (EntityId EchangeId) -> String) -> EchangeInput -> Cmd Msg
deleteEchange url echangeInput =
    delete
        { url = url echangeInput.entite <| Just <| .id echangeInput
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveSuivi
        }
        Decode.value


updateRappel : (Maybe Entite -> Maybe (EntityId RappelId) -> String) -> RappelInput -> Cmd Msg
updateRappel url rappelInput =
    case UI.Rappel.rappelInputValidate rappelInput of
        Err _ ->
            Cmd.none

        Ok _ ->
            let
                jsonBody =
                    UI.Rappel.encodeRappelInput rappelInput
                        |> Http.jsonBody
            in
            post
                { url = url rappelInput.entite <| Just <| rappelInput.id
                , body = jsonBody
                }
                { toShared = SharedMsg
                , logger = Just LogError
                , handler = ReceivedSaveSuivi
                }
                Decode.value


deleteRappel : (Maybe Entite -> Maybe (EntityId RappelId) -> String) -> RappelInput -> Cmd Msg
deleteRappel url rappelInput =
    delete
        { url = url rappelInput.entite <| Just <| rappelInput.id
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveSuivi
        }
        Decode.value


updateDemande : (Maybe Entite -> Maybe (EntityId DemandeId) -> String) -> DemandeInput -> Cmd Msg
updateDemande url demandeInput =
    let
        jsonBody =
            UI.Demande.encodeDemandeInput demandeInput
                |> Http.jsonBody
    in
    post
        { url = url demandeInput.entite <| Just <| demandeInput.id
        , body = jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveSuivi
        }
        Decode.value


updateBrouillon : (Maybe (EntityId BrouillonId) -> String) -> BrouillonInput -> Cmd Msg
updateBrouillon url brouillonInput =
    let
        jsonBody =
            UI.Brouillon.encodeBrouillonInput brouillonInput
                |> Http.jsonBody
    in
    post
        { url = url <| Just <| brouillonInput.id
        , body = jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveSuivi
        }
        Decode.value


deleteDemande : (Maybe Entite -> Maybe (EntityId DemandeId) -> String) -> DemandeInput -> Cmd Msg
deleteDemande url demandeInput =
    delete
        { url = url demandeInput.entite <| Just <| demandeInput.id
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveSuivi
        }
        Decode.value


deleteBrouillon : (Maybe (EntityId BrouillonId) -> String) -> BrouillonInput -> Cmd Msg
deleteBrouillon url brouillonInput =
    delete
        { url = url <| Just brouillonInput.id
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveSuivi
        }
        Decode.value


clotureRappel : (Maybe Entite -> EntityId RappelId -> String) -> RappelInput -> Cmd Msg
clotureRappel url { id, clotureDate, entite } =
    post
        { url = url entite id
        , body =
            [ ( "cloture", Encode.bool <| clotureDate /= Nothing ) ]
                |> Encode.object
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveSuivi
        }
        Decode.value


clotureDemande : (Maybe Entite -> EntityId DemandeId -> String) -> DemandeInput -> Cmd Msg
clotureDemande url demande =
    post
        { url = url demande.entite <| .id <| demande
        , body =
            demande
                |> Data.Demande.encodeDemandeCloture
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveSuivi
        }
        Decode.value


menuLateral : List Echange -> List Demande -> List Rappel -> Maybe (List Brouillon) -> Onglet -> Html Msg
menuLateral echanges demandes rappels brouillons ongletActif =
    nav
        [ class "fr-sidemenu"
        , Html.Attributes.id "sidemenu-navigation"
        , navigation
        , Aria.label "Menu principal"
        , class "fr-card--white p-4"
        ]
        [ ul [ class "fr-sidemenu__list" ] <|
            List.map (menuLateralOption ongletActif) <|
                List.filterMap identity <|
                    [ brouillons |> Maybe.map (\bs -> ( OngletBrouillons, List.length bs ))
                    , Just ( OngletEchanges, List.length echanges )
                    , Just ( OngletDemandes, List.length demandes )
                    , Just ( OngletRappels, List.length rappels )
                    ]
        ]


menuLateralOption : Onglet -> ( Onglet, Int ) -> Html Msg
menuLateralOption ongletActif ( onglet, quantite ) =
    li
        [ class "fr-sidemenu__item"
        , Html.Attributes.id <| (++) "menu-lateral-" <| ongletToString <| onglet
        ]
        [ DSFR.Button.new { label = ongletToDisplay onglet, onClick = Just <| SelectedOnglet <| Just onglet }
            |> DSFR.Button.withAttrs
                [ class "fr-sidemenu__link"
                , Typo.textSm
                , Html.Attributes.attribute "role" "tab"
                , Role.tab
                , Html.Attributes.Extra.attributeIf (onglet == ongletActif) <| class "blue-text"
                , Html.Attributes.Extra.attributeIf (onglet == ongletActif) Aria.currentPage
                , Html.Attributes.rel ""
                ]
            |> DSFR.Button.withPrefix (DSFR.Icons.iconMD <| ongletToIcone onglet)
            |> DSFR.Button.withSuffix
                (span [ class "circled" ]
                    [ text <| Lib.UI.formatIntWithThousandSpacing <| quantite
                    ]
                )
            |> DSFR.Button.tertiaryNoOutline
            |> DSFR.Button.view
        ]


menuLateralFiltres : Zone -> Posix -> EchangesModel -> DemandesModel -> RappelsModel -> BrouillonsModel -> List Echange -> List Demande -> List Rappel -> List Brouillon -> Onglet -> Html Msg
menuLateralFiltres timezone now echangesModel demandesModel rappelsModel _ echanges demandes rappels _ ongletActif =
    if ongletActif == OngletBrouillons then
        nothing

    else
        div [ class "p-4 sm:p-8 fr-card--white" ]
            [ div [ classList <| [ ( "hidden", ongletActif /= OngletEchanges ) ] ] <|
                List.singleton <|
                    menuLateralFiltresEchanges echangesModel demandes echanges
            , div [ classList <| [ ( "hidden", ongletActif /= OngletDemandes ) ] ] <|
                List.singleton <|
                    menuLateralFiltresDemandes demandesModel demandes
            , div [ classList <| [ ( "hidden", ongletActif /= OngletRappels ) ] ] <|
                List.singleton <|
                    menuLateralFiltresRappels timezone now rappelsModel rappels
            ]


menuLateralFiltresEchanges : EchangesModel -> List Demande -> List Echange -> Html Msg
menuLateralFiltresEchanges echangesModel demandes echanges =
    let
        nomsDemandes =
            echanges
                |> List.concatMap Data.Echange.demandes
                |> List.filterMap (getDemandeInDemandes demandes)
                |> List.map Data.Demande.nom
                |> List.Extra.unique

        nomsDemandesAvecNombreEchanges =
            ( Nothing
            , echanges
                |> List.filter
                    (\echange ->
                        (==) 0 <| List.length <| Data.Echange.demandes echange
                    )
                |> List.length
            )
                :: (nomsDemandes
                        |> List.map
                            (\nomDemande ->
                                ( Just nomDemande
                                , echanges
                                    |> List.filter
                                        (\echange ->
                                            echange
                                                |> Data.Echange.demandes
                                                |> List.filterMap (getDemandeInDemandes demandes)
                                                |> List.map Data.Demande.nom
                                                |> List.member nomDemande
                                        )
                                    |> List.length
                                )
                            )
                   )
    in
    div [ class "flex flex-col gap-4" ]
        [ h4 [ Typo.fr_h6, class "!mb-0" ] [ text "Filtres" ]
        , if List.length nomsDemandesAvecNombreEchanges > 0 then
            DSFR.Checkbox.group
                { id = "filtres-titres-demandes"
                , values = nomsDemandesAvecNombreEchanges |> List.map Tuple.first
                , toLabel = Maybe.withDefault "Aucune demande" >> text
                , toId = Maybe.withDefault "aucun-echange"
                , checked = echangesModel.titresDemandes
                , valueAsString = Maybe.withDefault "aucun-echange"
                , onChecked = \data bool -> SetTitreDemandeFilter bool data
                , label = text "Par titre de demande\u{00A0}:"
                }
                |> DSFR.Checkbox.groupWithToHint
                    (\titre ->
                        nomsDemandesAvecNombreEchanges
                            |> List.filter (\( td, _ ) -> td == titre)
                            |> List.head
                            |> Maybe.map Tuple.second
                            |> Maybe.map (\es -> [ String.fromInt es, " ", "échange", plural es ] |> String.concat)
                    )
                |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
                |> DSFR.Checkbox.viewGroup

          else
            span [ class "italic" ] [ text "Aucun filtre disponible" ]
        , div [ Typo.textXs, class "fr-text-default--info" ]
            [ DSFR.Icons.System.infoFill |> DSFR.Icons.iconSM
            , text " "
            , text "Pour modifier, clôturer ou supprimer une demande, rendez-vous dans la partie \"Demandes\""
            ]
        ]


menuLateralFiltresDemandes : DemandesModel -> List Demande -> Html Msg
menuLateralFiltresDemandes demandesModel demandes =
    let
        typesDemandes =
            demandes
                |> List.map Data.Demande.type_
                |> List.Extra.unique

        typesDemandesAvecNombreEchanges =
            typesDemandes
                |> List.map
                    (\typeDemande ->
                        ( typeDemande
                        , demandes
                            |> List.filter (Data.Demande.type_ >> (==) typeDemande)
                            |> List.length
                        )
                    )
    in
    div [ class "flex flex-col gap-4" ]
        [ h4 [ Typo.fr_h6, class "!mb-0" ] [ text "Filtres" ]
        , if List.length typesDemandesAvecNombreEchanges > 0 then
            DSFR.Checkbox.group
                { id = "filtres-types-demandes"
                , values = typesDemandesAvecNombreEchanges |> List.map Tuple.first
                , toLabel = Data.Demande.typeDemandeToDisplay >> text
                , toId = Data.Demande.typeDemandeToString
                , checked = demandesModel.typesDemandes
                , valueAsString = Data.Demande.typeDemandeToString
                , onChecked = \data bool -> SetTypeDemandeFilter bool data
                , label = text "Par type de demande\u{00A0}:"
                }
                |> DSFR.Checkbox.groupWithToHint
                    (\type_ ->
                        typesDemandesAvecNombreEchanges
                            |> List.filter (\( td, _ ) -> td == type_)
                            |> List.head
                            |> Maybe.map Tuple.second
                            |> Maybe.map (\ds -> [ String.fromInt ds, " ", "demande", plural ds ] |> String.concat)
                    )
                |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
                |> DSFR.Checkbox.viewGroup

          else
            span [ class "italic" ] [ text "Aucun filtre disponible" ]
        , div [ Typo.textXs, class "fr-text-default--info" ]
            [ DSFR.Icons.System.infoFill |> DSFR.Icons.iconSM
            , text " "
            , text "Pour visualiser les échanges d'une demande, sélectionnez \"Échanges\""
            ]
        ]


menuLateralFiltresRappels : Zone -> Posix -> RappelsModel -> List Rappel -> Html Msg
menuLateralFiltresRappels timezone now rappelsModel rappels =
    DSFR.Radio.group
        { id = "rappels-mode-radio"
        , options = UI.Rappel.rappelsModes
        , current = Just rappelsModel.rappelsMode
        , toLabel =
            \rappelsMode ->
                let
                    nombreDeRappels =
                        List.length <| UI.Rappel.rappelsSelonMode timezone now rappelsMode rappels
                in
                span []
                    [ text <| UI.Rappel.rappelsModeToDisplay rappelsMode
                    , text " "
                    , viewIf (nombreDeRappels > 0) <|
                        span [ class "circled" ]
                            [ text <| Lib.UI.formatIntWithThousandSpacing <| nombreDeRappels
                            ]
                    ]
        , toId = UI.Rappel.rappelsModeToString
        , msg = SetRappelsMode
        , legend = Nothing
        }
        |> DSFR.Radio.view


setTitres : List (Maybe String) -> Bool -> Maybe String -> List (Maybe String)
setTitres titres add titre =
    let
        isMember =
            List.member titre titres
    in
    case ( add, isMember ) of
        ( True, True ) ->
            titres

        ( True, False ) ->
            titres ++ [ titre ]

        ( False, True ) ->
            titres
                |> List.filter (\va -> va /= titre)

        ( False, False ) ->
            titres


setTypes : List Data.Demande.TypeDemande -> Bool -> Data.Demande.TypeDemande -> List Data.Demande.TypeDemande
setTypes types add type_ =
    let
        isMember =
            List.member type_ types
    in
    case ( add, isMember ) of
        ( True, True ) ->
            types

        ( True, False ) ->
            types ++ [ type_ ]

        ( False, True ) ->
            types
                |> List.filter (\va -> va /= type_)

        ( False, False ) ->
            types


getOngletActif : Model -> Onglet
getOngletActif model =
    model.ongletActif


viewOngletAjout : Maybe Onglet -> Effect.Effect Shared.Msg Msg
viewOngletAjout ongletAjout =
    Effect.fromCmd <|
        Task.perform identity <|
            Task.map
                (\_ ->
                    case ongletAjout of
                        Just OngletBrouillons ->
                            SetSuiviAction <|
                                NewSuivi <|
                                    SuiviBrouillon <|
                                        UI.Brouillon.default "" Nothing

                        _ ->
                            NoOp
                )
            <|
                Process.sleep 1000


rechercheEntite : TransformationBrouillonData -> Cmd Msg
rechercheEntite { transformationEntite } =
    let
        { entiteType, recherche } =
            transformationEntite

        ( url, decoder ) =
            case entiteType of
                EntiteEtab _ ->
                    ( Api.getEtablissementParSiret
                    , Decode.map EntiteEtab <|
                        Decode.oneOf
                            [ Decode.map Just <|
                                Decode.field "etablissement" <|
                                    Data.Etablissement.decodeEtablissementApercu
                            , Decode.map (\_ -> Nothing) <|
                                Decode.field "error" <|
                                    Decode.string
                            ]
                    )

                EntiteEtabCrea _ ->
                    ( Api.getEtabCreaParId
                    , Decode.map EntiteEtabCrea <|
                        Decode.oneOf
                            [ Decode.map Just <|
                                Decode.field "etabCrea" <|
                                    Data.EtablissementCreation.decodeEtablissementCreationApercu
                            , Decode.map (\_ -> Nothing) <|
                                Decode.field "error" <|
                                    Decode.string
                            ]
                    )

                EntiteLocal _ ->
                    ( Api.getLocalParId
                    , Decode.map EntiteLocal <|
                        Decode.oneOf
                            [ Decode.map Just <|
                                Decode.field "local" <|
                                    Data.Local.decodeLocalApercu
                            , Decode.map (\_ -> Nothing) <|
                                Decode.field "error" <|
                                    Decode.string
                            ]
                    )
    in
    get
        { url = url recherche
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedEntite
        }
        decoder


transformBrouillon : TransformationBrouillonData -> Cmd Msg
transformBrouillon transformationBrouillonData =
    let
        avecBrouillonData e =
            { e
                | brouillonId = Just <| Data.Brouillon.id transformationBrouillonData.brouillon
                , brouillonSuppression = transformationBrouillonData.suppression
            }
    in
    case transformationBrouillonData.transformationEntite.resultat of
        RD.Success entite ->
            let
                { upsertEchangeUrl, upsertDemandeUrl, upsertRappelUrl } =
                    case entite of
                        EntiteEtab etab ->
                            suiviConfigEtab (Decode.succeed ()) <|
                                Maybe.withDefault "" <|
                                    Maybe.map .siret <|
                                        etab

                        EntiteEtabCrea etabCrea ->
                            suiviConfigEtabCrea (Decode.succeed ()) <|
                                Maybe.withDefault Api.EntityId.newId <|
                                    Maybe.map .id <|
                                        etabCrea

                        EntiteLocal local ->
                            suiviConfigLocal (Decode.succeed ()) <|
                                Maybe.withDefault "" <|
                                    Maybe.map .id <|
                                        local
            in
            case transformationBrouillonData.suiviType of
                SuiviEchange echange ->
                    createEchange upsertEchangeUrl <|
                        avecBrouillonData <|
                            echange

                SuiviDemande demande ->
                    createDemande upsertDemandeUrl <|
                        avecBrouillonData <|
                            demande

                SuiviRappel rappel ->
                    createRappel upsertRappelUrl <|
                        avecBrouillonData <|
                            rappel

                _ ->
                    Cmd.none

        _ ->
            Cmd.none


suiviConfigEtab : Decoder etab -> Siret -> SuiviConfig etab
suiviConfigEtab decoder siret =
    { decoder = decoder
    , upsertRappelUrl = \_ -> UI.Rappel.updateRappelForEntite False <| Just <| Etab siret ""
    , upsertEchangeUrl = \_ -> UI.Echange.updateEchangeForEntite False <| Just <| Etab siret ""
    , upsertDemandeUrl = \_ -> UI.Demande.updateDemandeForEntite False <| Just <| Etab siret ""
    , upsertBrouillonUrl = \_ -> ""
    , clotureRappelUrl = \_ -> UI.Rappel.clotureRappelForEntite False <| Just <| Etab siret ""
    , clotureDemandeUrl = \_ -> UI.Demande.clotureDemandeForEntite False <| Just <| Etab siret ""
    , reouvertureRappelUrl = \_ -> UI.Rappel.reouvertureRappelForEntite False <| Just <| Etab siret ""
    , reouvertureDemandeUrl = \_ -> UI.Demande.reouvertureDemandeForEntite False <| Just <| Etab siret ""
    }


suiviConfigEtabCrea : Decoder etabCrea -> EntityId EtablissementCreationId -> SuiviConfig etabCrea
suiviConfigEtabCrea decoder etablissementCreationId =
    { decoder = decoder
    , upsertRappelUrl = \_ -> UI.Rappel.updateRappelForEntite False <| Just <| EtabCrea etablissementCreationId ""
    , upsertEchangeUrl = \_ -> UI.Echange.updateEchangeForEntite False <| Just <| EtabCrea etablissementCreationId ""
    , upsertDemandeUrl = \_ -> UI.Demande.updateDemandeForEntite False <| Just <| EtabCrea etablissementCreationId ""
    , upsertBrouillonUrl = \_ -> ""
    , clotureRappelUrl = \_ -> UI.Rappel.clotureRappelForEntite False <| Just <| EtabCrea etablissementCreationId ""
    , clotureDemandeUrl = \_ -> UI.Demande.clotureDemandeForEntite False <| Just <| EtabCrea etablissementCreationId ""
    , reouvertureRappelUrl = \_ -> UI.Rappel.reouvertureRappelForEntite False <| Just <| EtabCrea etablissementCreationId ""
    , reouvertureDemandeUrl = \_ -> UI.Demande.reouvertureDemandeForEntite False <| Just <| EtabCrea etablissementCreationId ""
    }


suiviConfigLocal : Decoder local -> String -> SuiviConfig local
suiviConfigLocal decoder localId =
    { decoder = decoder
    , upsertRappelUrl = \_ -> UI.Rappel.updateRappelForEntite False <| Just <| Loc localId ""
    , upsertEchangeUrl = \_ -> UI.Echange.updateEchangeForEntite False <| Just <| Loc localId ""
    , upsertDemandeUrl = \_ -> UI.Demande.updateDemandeForEntite False <| Just <| Loc localId ""
    , upsertBrouillonUrl = \_ -> ""
    , clotureRappelUrl = \_ -> UI.Rappel.clotureRappelForEntite False <| Just <| Loc localId ""
    , clotureDemandeUrl = \_ -> UI.Demande.clotureDemandeForEntite False <| Just <| Loc localId ""
    , reouvertureRappelUrl = \_ -> UI.Rappel.reouvertureRappelForEntite False <| Just <| Loc localId ""
    , reouvertureDemandeUrl = \_ -> UI.Demande.reouvertureDemandeForEntite False <| Just <| Loc localId ""
    }


suiviResultToMaybe : SuiviResult entity -> Maybe entity
suiviResultToMaybe suiviResult =
    case suiviResult of
        NoResult ->
            Nothing

        Refetch ->
            Nothing

        Result entity ->
            Just entity
