port module UI.Cgu exposing (Model, Msg, displayModal, init, open, update, viewModal)

import Accessibility exposing (Html, div, p, text)
import Api
import Api.Auth exposing (post)
import DSFR.Button
import DSFR.Checkbox
import DSFR.Icons
import DSFR.Icons.System
import DSFR.Modal
import DSFR.Typography as Typo
import Effect
import Html.Attributes exposing (class)
import Http
import Json.Decode as Decode
import RemoteData as RD
import Shared


type Model
    = Model ModelData


type alias ModelData =
    { isOpen : Bool
    , checked : Bool
    , request : RD.WebData ()
    }


init : Model
init =
    Model
        { isOpen = False
        , checked = False
        , request = RD.NotAsked
        }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | Open
    | ToggleCgu Bool
    | Confirm
    | ReceivedConfirm (RD.WebData ())


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg (Model model) =
    Tuple.mapFirst Model <|
        case msg of
            NoOp ->
                ( model, Effect.none )

            SharedMsg sharedMsg ->
                model
                    |> Effect.withShared sharedMsg

            LogError normalMsg error ->
                model
                    |> Shared.logErrorHelper normalMsg error

            Open ->
                ( { model | isOpen = True }, Effect.none )

            ToggleCgu checked ->
                ( { model | checked = checked }, Effect.none )

            Confirm ->
                ( model, sendConfirm )

            ReceivedConfirm (RD.Success ()) ->
                ( { model | request = RD.Success () }, Effect.fromShared Shared.reload )

            ReceivedConfirm response ->
                ( { model | request = response }, Effect.none )


sendConfirm : Effect.Effect Shared.Msg Msg
sendConfirm =
    Effect.fromCmd <|
        post
            { url = Api.confirmCgu
            , body = Http.emptyBody
            }
            { toShared = SharedMsg
            , logger = Just LogError
            , handler = ReceivedConfirm
            }
            (Decode.succeed ())


view : Bool -> Html Msg
view checked =
    div [ class "flex flex-col" ]
        [ p [] [ text "Les conditions générales d'utilisation de Deveco ont été mises à jour." ]
        , p []
            [ text "Pour accéder à Deveco, vous devez accepter les"
            , text " "
            , Typo.externalLink "https://deveco.incubateur.anct.gouv.fr/cgu"
                []
                [ text "nouvelles conditions générales d'utilisation de l'outil" ]
            , text "."
            ]
        , DSFR.Checkbox.single
            { value = "cgu-acceptees"
            , checked = Just checked
            , valueAsString = identity
            , id = "cgu-acceptees"
            , label = text "J'accepte les conditions générales d'utilisation de Deveco"
            , onChecked = \_ bool -> ToggleCgu bool
            }
            |> DSFR.Checkbox.viewSingle
        ]


header : Html msg
header =
    Accessibility.h1 [ Typo.fr_h4, class "!m-0" ]
        [ DSFR.Icons.iconLG DSFR.Icons.System.arrowRightLine
        , text " "
        , text "Conditions générales d'utilisation de Deveco"
        ]


viewModal : Model -> Html Msg
viewModal (Model { isOpen, checked, request }) =
    DSFR.Modal.view
        { id = modalId
        , label = modalId
        , openMsg = NoOp
        , closeMsg = Nothing
        , title = header
        , opened = isOpen
        , size = Nothing
        }
        (div [ class "flex flex-col gap-4" ] [ view checked, footer checked request ])
        Nothing
        |> Tuple.first


footer : Bool -> RD.WebData () -> Html Msg
footer checked request =
    DSFR.Button.group
        [ DSFR.Button.new
            { onClick = Just Confirm
            , label = "Confirmer"
            }
            |> DSFR.Button.withDisabled (not checked || request == RD.Loading)
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


modalId : String
modalId =
    "cgu-modal"


open : Msg
open =
    Open


displayModal : Cmd msg
displayModal =
    forceDisplayModal <| "modal-" ++ modalId


port forceDisplayModal : String -> Cmd msg
