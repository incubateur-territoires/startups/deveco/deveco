module UI.Local exposing (badgeInactif, cardPlaceholder, carte, iconeFavoris, occupation)

import Accessibility exposing (Html, a, div, h4, span, text)
import DSFR.Badge
import DSFR.Button
import DSFR.Grid as Grid
import DSFR.Icons
import DSFR.Icons.Map
import DSFR.Icons.System
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Adresse exposing (adresseToString)
import Data.Local exposing (LocalSimple, badgeVacance)
import Data.PortefeuilleInfos exposing (PortefeuilleInfos)
import Html.Attributes as Attr exposing (class, style)
import Html.Extra exposing (nothing)
import Lib.UI
import Route
import UI.Entite


type alias CardMessages msg =
    { toggleFavorite : Maybe (String -> Bool -> msg)
    }


carte : CardMessages msg -> LocalSimple -> PortefeuilleInfos -> Html msg
carte cardMessages local portefeuilleInfos =
    div [ Grid.col12, Grid.colSm4, class "p-2" ]
        [ div [ Typo.textSm, class "!mb-0" ]
            [ div
                [ class "flex flex-col overflow-y-auto fr-card--white p-2 border-2 border-black local-card"
                , style "background-image" "none"
                ]
                [ div [ class "flex flex-row justify-between items-center relative p-2" ]
                    [ div [ Typo.textXs, class "!mb-0" ]
                        [ DSFR.Icons.iconSM UI.Entite.iconeLocal
                        , text " "
                        , text "Local"
                        ]
                    , div [ class "flex flex-row gap-2" ] <|
                        [ badgeVacance <| local.occupe
                        , badgeInactif <| local.actif
                        ]
                    ]
                , div [ class "flex flex-row justify-between items-center px-2" ]
                    [ div [ class "flex flex-row gap-2" ]
                        [ UI.Entite.iconesPortefeuilleInfos portefeuilleInfos
                        , iconeGeolocalisation local
                        ]
                    , iconeFavoris cardMessages local portefeuilleInfos
                    ]
                , a
                    [ class "fr-card--white flex flex-col p-2 custom-hover overflow-y-auto gap-2"
                    , style "background-image" "none"
                    , Attr.href <|
                        Route.toUrl <|
                            Route.Local <|
                                ( Nothing, local.id )
                    , Attr.target "_self"
                    ]
                    [ div [ class "flex flex-col" ]
                        [ div
                            [ Typo.textBold
                            , class "!mb-0"
                            , class "line-clamp-1"
                            ]
                            [ text <|
                                case local.nom of
                                    "" ->
                                        local.id

                                    nom ->
                                        nom ++ " (" ++ local.id ++ ")"
                            ]
                        , div
                            [ class "!mb-0"
                            , class "line-clamp-1"
                            , Attr.title <|
                                adresseToString <|
                                    .adresse <|
                                        local
                            ]
                          <|
                            List.singleton <|
                                text <|
                                    Lib.UI.withEmptyAs "-" <|
                                        adresseToString <|
                                            .adresse <|
                                                local
                        , div [ class "!mb-0" ]
                            [ text ""
                            , div [ style "min-height" "1.5rem" ]
                                [ span [] [ text <| "Section\u{00A0}: " ]
                                , span
                                    [ Typo.textBold
                                    ]
                                    [ text local.section ]
                                , text " - "
                                , span [] [ text <| "Parcelle\u{00A0}: " ]
                                , span
                                    [ Typo.textBold
                                    ]
                                    [ text local.parcelle ]
                                ]
                            ]
                        , local.categorie
                            |> Maybe.map (\c -> c.nom ++ " (" ++ c.id ++ ")")
                            |> Maybe.withDefault ""
                            |> Lib.UI.withEmptyAs "-"
                            |> Lib.UI.infoLine (Just 1) "Catégorie"
                            |> List.singleton
                            |> div [ class "!mb-0" ]
                        , local.niveau
                            |> Lib.UI.withEmptyAs "-"
                            |> Lib.UI.infoLine Nothing "Étage"
                            |> List.singleton
                            |> div [ class "!mb-0" ]
                        , local.surfaceTotale
                            |> (\s ->
                                    if s == "" then
                                        "-"

                                    else
                                        s ++ " m²"
                               )
                            |> Lib.UI.infoLine Nothing "Surface totale"
                            |> List.singleton
                            |> div [ class "!mb-0" ]
                        ]
                    ]
                ]
            ]
        ]


occupation : LocalSimple -> Html msg
occupation local =
    div [ Grid.col6 ]
        [ div [ class "flex flex-col p-4 border-2 dark-grey-border gap-4" ]
            [ div [ class "flex flex-row gap-2 !mb-0 justify-between" ]
                [ div [ Typo.textXs, class "!mb-0" ]
                    [ DSFR.Icons.iconSM UI.Entite.iconeLocal
                    , text " "
                    , text "Local"
                    ]
                , div []
                    [ badgeVacance <| local.occupe
                    ]
                ]
            , div []
                [ h4 [ Typo.fr_h5, class "!mb-0" ]
                    [ text <|
                        case local.nom of
                            "" ->
                                local.id

                            nom ->
                                nom ++ " (" ++ local.id ++ ")"
                    ]
                , local.adresse
                    |> adresseToString
                    |> Lib.UI.withEmptyAs "-"
                    |> Lib.UI.infoLine Nothing "Adresse"
                    |> List.singleton
                    |> div [ class "!mb-0" ]
                , local.categorie
                    |> Maybe.map (\c -> c.nom ++ " (" ++ c.id ++ ")")
                    |> Maybe.withDefault ""
                    |> Lib.UI.withEmptyAs "-"
                    |> Lib.UI.infoLine (Just 2) "Catégorie"
                    |> List.singleton
                    |> div [ class "!mb-0" ]
                , local.niveau
                    |> Lib.UI.withEmptyAs "-"
                    |> Lib.UI.infoLine Nothing "Étage"
                    |> List.singleton
                    |> div [ class "!mb-0" ]
                , local.surfaceTotale
                    |> (\s ->
                            if s == "" then
                                "-"

                            else
                                s ++ " m²"
                       )
                    |> Lib.UI.infoLine (Just 1) "Surface totale"
                    |> List.singleton
                    |> div [ class "!mb-0" ]
                ]
            , DSFR.Button.new
                { label = "Voir la fiche local"
                , onClick = Nothing
                }
                |> (local.id
                        |> Tuple.pair Nothing
                        |> Route.Local
                        |> Route.toUrl
                        |> DSFR.Button.linkButton
                   )
                |> DSFR.Button.leftIcon DSFR.Icons.System.eyeLine
                |> DSFR.Button.secondary
                |> DSFR.Button.view
            ]
        ]


cardPlaceholder : Html msg
cardPlaceholder =
    div [ Grid.col12, Grid.colSm6 ]
        [ div [ class "p-2" ]
            [ div
                [ class "fr-card--white flex flex-col h-[270px] p-4 custom-hover overflow-y-auto gap-2"
                , style "background-image" "none"
                ]
                [ div [ class "flex flex-row justify-between" ]
                    [ greyBadge 30
                    ]
                , div [ Typo.textSm, class "!mb-0 min-h-[2rem]" ]
                    [ greyPlaceholder 14
                    , greyPlaceholder 20
                    ]
                , div [ Typo.textSm, class "!mb-0 min-h-[2rem]" ]
                    [ greyPlaceholder 15
                    , greyPlaceholder 8
                    ]
                , div [ Typo.textSm, class "!mb-0 min-h-[2rem]" ]
                    [ greyPlaceholder 15
                    , greyPlaceholder 8
                    ]
                ]
            ]
        ]


greyPlaceholder : Int -> Html msg
greyPlaceholder length =
    "\u{00A0}"
        |> List.repeat length
        |> List.map (text >> List.singleton >> span [ class "w-[1rem] inline-block" ])
        |> span [ class "grey-background m-1" ]
        |> List.singleton
        |> div [ class "pulse-black" ]


greyBadge : Int -> Html msg
greyBadge length =
    "\u{00A0}"
        |> String.repeat length
        |> (\t -> { data = t, toString = identity })
        |> DSFR.Tag.unclickable
        |> List.singleton
        |> DSFR.Tag.medium
        |> List.singleton
        |> div [ class "pulse-black" ]


badgeInactif : Bool -> Html msg
badgeInactif actif =
    if actif then
        nothing

    else
        text "Inactif"
            |> DSFR.Badge.system { context = DSFR.Badge.Warning, withIcon = False }
            |> DSFR.Badge.badgeMD


iconeFavoris : CardMessages msg -> { local | id : String } -> PortefeuilleInfos -> Html msg
iconeFavoris { toggleFavorite } { id } infos =
    let
        ( title, icon ) =
            if infos.favori then
                ( "Retirer des favoris"
                , DSFR.Icons.System.starFill
                )

            else
                ( "Ajouter aux favoris"
                , DSFR.Icons.System.starLine
                )

        content =
            case toggleFavorite of
                Nothing ->
                    DSFR.Icons.iconMD icon

                Just msg ->
                    DSFR.Button.new
                        { label = title
                        , onClick =
                            Just <|
                                msg id <|
                                    not <|
                                        infos.favori
                        }
                        |> DSFR.Button.onlyIcon icon
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.withId ("ajouter-aux-favoris-" ++ id)
                        |> DSFR.Button.withTooltip (text title)
                        |> DSFR.Button.view
    in
    content


iconeGeolocalisation :
    { local
        | geolocalisation : Maybe ( Float, Float )
        , geolocalisationContribution : Maybe ( Float, Float )
        , geolocalisationEquipe : Maybe String
    }
    -> Html msg
iconeGeolocalisation { geolocalisation, geolocalisationContribution, geolocalisationEquipe } =
    let
        ( icon, txt, color ) =
            if geolocalisationContribution /= Nothing then
                ( DSFR.Icons.Map.mapPin2Fill, "géolocalisé (modifié par " ++ (geolocalisationEquipe |> Maybe.withDefault "") ++ ")", "blue-text" )

            else if geolocalisation /= Nothing then
                ( DSFR.Icons.Map.mapPin2Line, "géolocalisé", "blue-text" )

            else
                ( DSFR.Icons.Map.mapPin2Line, "non géolocalisé", "red-text" )
    in
    div
        [ class "!mb-0"
        , Attr.title <| "Local " ++ txt
        ]
        [ span [ class color ] [ DSFR.Icons.iconMD icon ]
        ]
