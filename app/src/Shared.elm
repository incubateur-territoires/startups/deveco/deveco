module Shared exposing
    ( ErrorReport
    , LogErrorConfig
    , Msg(..)
    , Redirect
    , Shared
    , TypeError(..)
    , User
    , autoriseAccesDeveco
    , goBack
    , identity
    , init
    , logErrorHelper
    , pageChangeEffects
    , reload
    , subscriptions
    , update
    )

import Api
import Api.EntityId exposing (EntityId)
import Api.Logging
import Browser.Navigation as Nav
import Data.EtablissementCreationId exposing (EtablissementCreationId)
import Data.Role
import Dict
import Effect
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Lib.Pdf exposing (exportToPdf)
import Lib.UI
import RemoteData as RD exposing (WebData)
import Route
import Task
import Time exposing (Posix, Zone)
import TimeZone
import UI.Scroll exposing (click, scrollIntoView)
import UI.Theme exposing (Theme(..), stringToTheme, switchTheme)
import UI.Tutoriel
import User


type alias Shared =
    { key : Nav.Key
    , identity : Maybe User
    , headerMenuOpen : Bool
    , now : Posix
    , timezone : Zone
    , theme : Theme
    , etablissementCreationIds : List (EntityId EtablissementCreationId)
    , appUrl : String
    , tutoriel : UI.Tutoriel.Model
    }


type alias Flags =
    { now : Int
    , timezone : String
    , currentTheme : String
    , appUrl : String
    }


identity : Shared -> Maybe User
identity =
    .identity


type alias User =
    User.User


type alias Redirect =
    Maybe String


type alias ErrorReport =
    { statut : Int
    , type_ : TypeError
    , message : String
    }


type TypeError
    = CodeFront
    | Serveur
    | Decodeur


typeErrorToString : TypeError -> String
typeErrorToString type_ =
    case type_ of
        CodeFront ->
            "CodeFront"

        Serveur ->
            "Serveur"

        Decodeur ->
            "Decodeur"


type alias LogErrorConfig data msg =
    { toShared : Msg -> msg
    , logger : Maybe (msg -> ErrorReport -> msg)
    , handler : WebData data -> msg
    }


type Msg
    = LoggedIn User.User Redirect
    | LoggedOut Redirect
    | ConfirmedLoggedOut (WebData ())
    | ToggleHeaderMenu
    | CloseHeaderMenu
    | Navigate Route.Route
    | ReplaceUrl Route.Route
    | Reload
    | GotTime Posix
    | SetTheme Theme
    | ScrollUp
    | GoBack
    | NoOp
    | BackUpEntitesSelection (List (EntityId EtablissementCreationId))
    | Tutoriel UI.Tutoriel.Msg
    | ScrollIntoView String
    | LogError ErrorReport
    | UpdateUser
    | PrintToPdf String


init : Flags -> Nav.Key -> ( Shared, Cmd Msg )
init { now, timezone, currentTheme, appUrl } key =
    ( { identity = Nothing
      , key = key
      , headerMenuOpen = False
      , now = Time.millisToPosix now
      , timezone = Dict.get timezone TimeZone.zones |> Maybe.withDefault TimeZone.europe__paris |> (\z -> z ())
      , theme = currentTheme |> stringToTheme |> Maybe.withDefault System
      , etablissementCreationIds = []
      , appUrl = appUrl
      , tutoriel = UI.Tutoriel.init
      }
    , Cmd.none
    )


update : Msg -> Shared -> ( Shared, Cmd Msg )
update msg shared =
    case msg of
        LogError errorReport ->
            ( shared
            , shared.identity
                |> Maybe.map (User.id >> Api.EntityId.entityIdToInt >> logError errorReport)
                |> Maybe.withDefault Cmd.none
            )

        GotTime now ->
            ( { shared | now = now }, Cmd.none )

        ToggleHeaderMenu ->
            ( { shared | headerMenuOpen = not shared.headerMenuOpen }, Cmd.none )

        CloseHeaderMenu ->
            ( { shared | headerMenuOpen = False }, Cmd.none )

        LoggedIn id redirect ->
            ( { shared | identity = Just id, headerMenuOpen = False }
            , Cmd.batch
                [ if not <| User.cgu id then
                    Nav.replaceUrl shared.key <|
                        Route.toUrl <|
                            Route.Dashboard <|
                                (\r ->
                                    if Maybe.withDefault "" r == "/accueil" then
                                        Nothing

                                    else
                                        r
                                )
                                <|
                                    redirect

                  else
                    redirect
                        |> Maybe.map (Nav.replaceUrl shared.key)
                        |> Maybe.withDefault Cmd.none
                , if User.cgu id && User.montrerTutoriel id then
                    click <| "modal-" ++ UI.Tutoriel.modalId ++ "-button"

                  else
                    Cmd.none
                ]
            )

        LoggedOut redirect ->
            ( shared
            , if shared.identity == Nothing then
                redirect
                    |> Maybe.map
                        (Nav.replaceUrl shared.key
                            << Route.toUrl
                            << Route.Connexion
                            << Just
                        )
                    |> Maybe.withDefault Cmd.none

              else
                logout
            )

        ConfirmedLoggedOut result ->
            case result of
                RD.Success () ->
                    ( { shared | identity = Nothing }
                    , Nav.replaceUrl shared.key <|
                        Route.toUrl <|
                            Route.Dashboard Nothing
                    )

                _ ->
                    ( shared, Cmd.none )

        Navigate route ->
            ( { shared | headerMenuOpen = False }
            , Nav.pushUrl shared.key <|
                Route.toUrl <|
                    route
            )

        ReplaceUrl route ->
            ( { shared | headerMenuOpen = False }
            , Nav.replaceUrl shared.key <|
                Route.toUrl <|
                    route
            )

        Reload ->
            ( { shared | headerMenuOpen = False }
            , Nav.reload
            )

        SetTheme theme ->
            ( { shared | theme = theme }, switchTheme <| UI.Theme.themeToString theme )

        ScrollUp ->
            ( shared
            , Lib.UI.scrollElementTo (\_ -> NoOp) Nothing ( 0, 0 )
            )

        ScrollIntoView id ->
            ( shared
            , scrollIntoView id
            )

        GoBack ->
            ( shared
            , Nav.back shared.key 1
            )

        NoOp ->
            ( shared, Cmd.none )

        BackUpEntitesSelection etablissementCreationIds ->
            ( { shared | etablissementCreationIds = etablissementCreationIds }, Cmd.none )

        Tutoriel subMsg ->
            let
                ( tutoriel, cmd ) =
                    UI.Tutoriel.update subMsg shared.tutoriel
            in
            ( { shared | tutoriel = tutoriel }, Cmd.map Tutoriel cmd )

        UpdateUser ->
            case shared.identity of
                Nothing ->
                    ( shared, Cmd.none )

                Just user ->
                    if User.cgu user then
                        ( shared, updateUser True )

                    else
                        ( shared, Cmd.none )

        PrintToPdf id ->
            ( shared
            , [ ( "elementId", Encode.string id )
              ]
                |> Encode.object
                |> exportToPdf
            )


logout : Cmd Msg
logout =
    Http.post
        { url = Api.deconnexion
        , body = Http.emptyBody
        , expect = Http.expectWhatever (RD.fromResult >> ConfirmedLoggedOut)
        }


pageChangeEffects : ( model, Effect.Effect Msg msg ) -> ( model, Effect.Effect Msg msg )
pageChangeEffects =
    Effect.addBatch
        [ closeHeaderMenu
        , scrollUp
        , Effect.fromShared UpdateUser
        ]


closeHeaderMenu : Effect.Effect Msg msg
closeHeaderMenu =
    Effect.fromShared CloseHeaderMenu


scrollUp : Effect.Effect Msg msg
scrollUp =
    Effect.fromShared <| ScrollUp


goBack : Msg
goBack =
    GoBack


reload : Msg
reload =
    Reload


subscriptions : Shared -> Sub Msg
subscriptions _ =
    Time.every 30000 GotTime


autoriseAccesDeveco : User.User -> Maybe String -> Effect.Effect Msg msg -> Effect.Effect Msg msg
autoriseAccesDeveco user query effect =
    if Data.Role.isDeveco <| User.role user then
        effect

    else
        Effect.fromShared <|
            ReplaceUrl <|
                Route.defaultPageForRole query <|
                    User.role user


logError : ErrorReport -> Int -> Cmd msg
logError { message, statut, type_ } compteId =
    [ ( "message", Encode.string message )
    , ( "statut", Encode.int statut )
    , ( "type", Encode.string <| typeErrorToString type_ )
    , ( "compteId", Encode.int compteId )
    ]
        |> Encode.object
        |> Api.Logging.log


logErrorHelper : normalMsg -> ErrorReport -> model -> ( model, Effect.Effect Msg normalMsg )
logErrorHelper normalMsg error =
    Effect.with
        (Effect.batch
            [ Effect.fromCmd <| Task.perform Basics.identity (Task.succeed normalMsg)
            , Effect.fromShared <| LogError error
            ]
        )


updateUser : Bool -> Cmd Msg
updateUser stayOnPage =
    let
        resultToMsg res =
            case res of
                Ok id ->
                    LoggedIn id Nothing

                Err _ ->
                    if stayOnPage then
                        NoOp

                    else
                        LoggedOut Nothing
    in
    Http.get
        { url = Api.authStatus
        , expect =
            Http.expectJson resultToMsg <|
                Decode.field "user" User.decodeUser
        }
