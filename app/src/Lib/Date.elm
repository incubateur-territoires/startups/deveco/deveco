module Lib.Date exposing (dateFromISOString, dateTimeToFrenchString, dateTimeToShortFrenchString, dateWithTimeFromISOString, decodeCalendarDate, decodeDateWithTimeFromISOString, firstDateIsAfterSecondDate, firstDateIsBeforeSecondDate, firstDateIsExactlySecondDate, firstPosixIsAfterSecondPosix, firstPosixIsAlmostSecondPosix, firstPosixIsBeforeSecondPosix, firstPosixIsExactlySecondPosix, formatDateLong, formatDateMonthNameYear, formatDateShort, formatDateShortSansDay, formatDateToYYYYMMDD, formatRelative, formatShort, frenchMonthAbbreviation, intToMonth, sortOlderDateFirst, sortOlderDateLast, sortOlderPosixFirst, sortOlderPosixLast)

import Date exposing (Date)
import DateFormat as DF
import DateFormat.Language as DFL exposing (english)
import DateFormat.Relative as DFR
import Iso8601
import Json.Decode as Decode exposing (Decoder)
import Lib.UI exposing (plural)
import Time exposing (Month(..), Posix, Weekday(..), Zone)


decodeDateWithTimeFromISOString : Decoder Posix
decodeDateWithTimeFromISOString =
    Iso8601.decoder


dateWithTimeFromISOString : String -> Maybe Posix
dateWithTimeFromISOString =
    Iso8601.toTime >> Result.toMaybe


formatRelative : Posix -> Posix -> String
formatRelative =
    DFR.relativeTimeWithOptions frenchRelativeTimeOptions


formatShort : Zone -> Posix -> String
formatShort =
    format
        [ DF.dayOfMonthFixed
        , DF.text "/"
        , DF.monthFixed
        , DF.text "/"
        , DF.yearNumber
        ]


dateTimeToFrenchString : Zone -> Posix -> String
dateTimeToFrenchString zone date =
    format
        [ DF.dayOfWeekNameFull
        , DF.text " "
        , DF.dayOfMonthNumber
        , DF.text " "
        , DF.monthNameFull
        , DF.text " "
        , DF.yearNumber
        ]
        zone
        date
        ++ " à "
        ++ format
            [ DF.hourMilitaryNumber
            , DF.text "h"
            , DF.minuteFixed
            ]
            zone
            date


dateTimeToShortFrenchString : Zone -> Posix -> String
dateTimeToShortFrenchString zone date =
    format
        [ DF.dayOfMonthNumber
        , DF.text " "
        , DF.monthNameAbbreviated
        , DF.text " "
        , DF.yearNumberLastTwo
        ]
        zone
        date
        ++ " - "
        ++ format
            [ DF.hourMilitaryFixed
            , DF.text ":"
            , DF.minuteFixed
            ]
            zone
            date


format : List DF.Token -> Zone -> Posix -> String
format =
    DF.formatWithLanguage frenchDateFormat


displayRelativePastTime : String -> Int -> String
displayRelativePastTime unit t =
    "Il y a " ++ String.fromInt t ++ " " ++ unit ++ plural t


displayRelativeFutureTime : String -> Int -> String
displayRelativeFutureTime unit t =
    "Dans " ++ String.fromInt t ++ " " ++ unit ++ plural t


frenchRelativeTimeOptions : DFR.RelativeTimeOptions
frenchRelativeTimeOptions =
    { someSecondsAgo = \_ -> "À l'instant"
    , someMinutesAgo = displayRelativePastTime "minute"
    , someHoursAgo = displayRelativePastTime "heure"
    , someDaysAgo = displayRelativePastTime "jour"
    , someMonthsAgo = \t -> "Il y a " ++ String.fromInt t ++ " mois"
    , someYearsAgo = displayRelativePastTime "an"
    , rightNow = "À l'instant"
    , inSomeSeconds = always "À l'instant" -- since "now" is always a bit behind, we need to lie and say stuff is "right now" even when it's in the future
    , inSomeMinutes = displayRelativeFutureTime "minute"
    , inSomeHours = displayRelativeFutureTime "heure"
    , inSomeDays = displayRelativeFutureTime "jour"
    , inSomeMonths = \t -> "Dans " ++ String.fromInt t ++ " mois"
    , inSomeYears = displayRelativeFutureTime "an"
    }


frenchDateFormat : DFL.Language
frenchDateFormat =
    { english
        | toMonthName = frenchMonthName
        , toWeekdayName = frenchDayName
        , toMonthAbbreviation = frenchMonthAbbreviation
    }


frenchDayName : Weekday -> String
frenchDayName d =
    case d of
        Mon ->
            "lundi"

        Tue ->
            "mardi"

        Wed ->
            "mercredi"

        Thu ->
            "jeudi"

        Fri ->
            "vendredi"

        Sat ->
            "samedi"

        Sun ->
            "dimanche"


frenchMonthName : Month -> String
frenchMonthName m =
    case m of
        Jan ->
            "janvier"

        Feb ->
            "février"

        Mar ->
            "mars"

        Apr ->
            "avril"

        May ->
            "mai"

        Jun ->
            "juin"

        Jul ->
            "juillet"

        Aug ->
            "août"

        Sep ->
            "septembre"

        Oct ->
            "octobre"

        Nov ->
            "novembre"

        Dec ->
            "décembre"


frenchMonthAbbreviation : Month -> String
frenchMonthAbbreviation m =
    case m of
        Jan ->
            "janv."

        Feb ->
            "févr."

        Mar ->
            "mars"

        Apr ->
            "avr."

        May ->
            "mai"

        Jun ->
            "juin"

        Jul ->
            "juil."

        Aug ->
            "août"

        Sep ->
            "sept."

        Oct ->
            "oct."

        Nov ->
            "nov."

        Dec ->
            "déc."


firstPosixIsBeforeSecondPosix : Posix -> Posix -> Bool
firstPosixIsBeforeSecondPosix first second =
    Time.posixToMillis first < Time.posixToMillis second


firstPosixIsExactlySecondPosix : Posix -> Posix -> Bool
firstPosixIsExactlySecondPosix first second =
    Time.posixToMillis first > Time.posixToMillis second


firstPosixIsAfterSecondPosix : Posix -> Posix -> Bool
firstPosixIsAfterSecondPosix first second =
    Time.posixToMillis first > Time.posixToMillis second


firstPosixIsAlmostSecondPosix : Int -> Posix -> Posix -> Bool
firstPosixIsAlmostSecondPosix margin first second =
    Time.posixToMillis first
        - Time.posixToMillis second
        |> abs
        |> (>=) margin


firstDateIsBeforeSecondDate : Date -> Date -> Bool
firstDateIsBeforeSecondDate first second =
    Date.compare first second == LT


firstDateIsExactlySecondDate : Date -> Date -> Bool
firstDateIsExactlySecondDate first second =
    Date.compare first second == EQ


firstDateIsAfterSecondDate : Date -> Date -> Bool
firstDateIsAfterSecondDate first second =
    Date.compare first second == GT


sortOlderPosixFirst : (data -> Posix) -> data -> data -> Order
sortOlderPosixFirst accessor first second =
    compare (Time.posixToMillis <| accessor <| first) (Time.posixToMillis <| accessor <| second)


sortOlderPosixLast : (data -> Posix) -> data -> data -> Order
sortOlderPosixLast accessor first second =
    compare (Time.posixToMillis <| accessor <| second) (Time.posixToMillis <| accessor <| first)


sortOlderDateFirst : (data -> Date) -> data -> data -> Order
sortOlderDateFirst accessor first second =
    Date.compare (accessor <| first) (accessor <| second)


sortOlderDateLast : (data -> Date) -> data -> data -> Order
sortOlderDateLast accessor first second =
    Date.compare (accessor <| second) (accessor <| first)


decodeCalendarDate : Decoder Date
decodeCalendarDate =
    Decode.string
        |> Decode.andThen
            (\s ->
                case Date.fromIsoString <| String.slice 0 10 <| s of
                    Ok date ->
                        Decode.succeed date

                    Err err ->
                        Decode.fail err
            )


dateFromISOString : String -> Maybe Date
dateFromISOString =
    String.slice 0 10 >> Date.fromIsoString >> Result.toMaybe


formatDateLong : Date -> String
formatDateLong =
    Date.formatWithLanguage frenchDateLanguage "EEEE, ddd MMMM y"


formatDateShort : Date -> String
formatDateShort =
    Date.formatWithLanguage frenchDateLanguage "dd/MM/y"


formatDateShortSansDay : Date -> String
formatDateShortSansDay =
    Date.formatWithLanguage frenchDateLanguage "MM/y"


formatDateMonthNameYear : Date -> String
formatDateMonthNameYear =
    Date.formatWithLanguage frenchDateLanguage "MMM yyyy"


formatDateToYYYYMMDD : Date -> String
formatDateToYYYYMMDD =
    Date.toIsoString


frenchDateLanguage : Date.Language
frenchDateLanguage =
    let
        frenchMonthShort m =
            case m of
                Jan ->
                    "janv"

                Feb ->
                    "fév"

                Mar ->
                    "mars"

                Apr ->
                    "avr"

                May ->
                    "mai"

                Jun ->
                    "juin"

                Jul ->
                    "juil"

                Aug ->
                    "août"

                Sep ->
                    "sept"

                Oct ->
                    "oct"

                Nov ->
                    "nov"

                Dec ->
                    "déc"

        frenchWeekdayShort d =
            case d of
                Mon ->
                    "lun"

                Tue ->
                    "mar"

                Wed ->
                    "mer"

                Thu ->
                    "jeu"

                Fri ->
                    "ven"

                Sat ->
                    "sam"

                Sun ->
                    "dim"
    in
    { monthName = frenchMonthName
    , monthNameShort = frenchMonthShort
    , weekdayName = frenchDayName
    , weekdayNameShort = frenchWeekdayShort
    , dayWithSuffix =
        \day ->
            case day of
                1 ->
                    "1er"

                other ->
                    String.fromInt other
    }


intToMonth : Int -> Month
intToMonth month =
    case month of
        1 ->
            Jan

        2 ->
            Feb

        3 ->
            Mar

        4 ->
            Apr

        5 ->
            May

        6 ->
            Jun

        7 ->
            Jul

        8 ->
            Aug

        9 ->
            Sep

        10 ->
            Oct

        11 ->
            Nov

        _ ->
            Dec
