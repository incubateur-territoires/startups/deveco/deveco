module Lib.Variables exposing (brouillonEmail, contactEmail, hintActivites, hintBandeauSuivis, hintBeneficiairesEffectifsTotalParts, hintDonneesEtablissementsPubliques, hintDonneesLocauxPubliques, hintESS, hintFormesJuridiques, hintMotsCles, hintResultatsEtablissements, hintResultatsTerritoires, hintZoneGeographique, hrefFaq, hrefNouveautes, hrefTutoriel, lienWebinaireDecouverte, lienWebinaireFormation)

import Url.Builder as Builder


contactEmail : String
contactEmail =
    "contact@deveco.incubateur.anct.gouv.fr"


brouillonEmail : String
brouillonEmail =
    "brouillon@deveco.incubateur.anct.gouv.fr"


prodUrl : String
prodUrl =
    "https://deveco.incubateur.anct.gouv.fr"


hrefTutoriel : String
hrefTutoriel =
    Builder.crossOrigin prodUrl [ "documentation" ] []


hrefNouveautes : String
hrefNouveautes =
    Builder.crossOrigin prodUrl [ "nouveautes" ] []


hrefFaq : String
hrefFaq =
    Builder.crossOrigin prodUrl [ "faq" ] []


hintActivites : String
hintActivites =
    "La qualification de ce champ vous permet de filtrer les établissements et créateurs d'entreprise d'une même activité ou filière dans les onglets Établissements et Créateurs d'entreprise\nVotre collectivité peut créer ses propres étiquettes d'activité\n\nExemples\u{00A0}:\nNumérique, ESS, Commerces de proximité, Automobile, Médical, Service aux particuliers, Association, etc."


hintZoneGeographique : String
hintZoneGeographique =
    "La qualification de ce champ vous permet de filtrer les établissements et créateurs d'entreprise d'une même zone géographique dans les onglets Établissements et Créateurs d'entreprise\nVotre collectivité peut créer ses propres étiquettes de zone géographique\n\nExemples\u{00A0}:\nCentre-ville, ZAC Les Peupliers, Rive droite, QPV, Ancien marché, etc."


hintMotsCles : String
hintMotsCles =
    "La qualification de ce champ vous permet de filtrer les établissements et créateurs d'entreprise ayant le même mot-clé dans les onglets Établissements et Créateurs d'entreprise\nVotre collectivité peut créer ses propres étiquettes de mots-clés en fonction de ses spécificités\n\nExemples\u{00A0}:\nFête du pain, Jury de l'industrie, Redevable taxe Y, Charte signée, etc."


hintDonneesEtablissementsPubliques : String
hintDonneesEtablissementsPubliques =
    "Les données publiques disponibles sont celles de la base SIRENE"


hintDonneesLocauxPubliques : String
hintDonneesLocauxPubliques =
    "Les données publiques disponibles sont celles de la DGFIP"


hintESS : String
hintESS =
    "L'Économie Sociale et Solidaire regroupe les associations, les fondations, les coopératives et les mutuelles, ainsi que les établissements qui déclarent adhérer aux principes de l'ESS (loi n°2014-856 du 31 juillet 2014)"


hintFormesJuridiques : String
hintFormesJuridiques =
    String.join "\n" <|
        [ "Les formes juridiques les plus courantes sont\u{00A0}:\n"
        , "Entrepreneur individuel"
        , "Société en participation"
        , "Fiducie"
        , "Assujetti unique à la TVA"
        , "Autre groupement de droit privé non doté de la personnalité morale"
        , "Personne morale de droit étranger, immatriculée au RCS (registre du commerce et des sociétés)"
        , "Personne morale de droit étranger, non immatriculée au RCS"
        , "Etablissement public ou régie à caractère industriel ou commercial"
        , "Société coopérative commerciale particulière"
        , "Société en nom collectif"
        , "Société en commandite"
        , "Société à responsabilité limitée (SARL)"
        , "Société anonyme à conseil d'administration"
        , "Société anonyme à directoire"
        , "Société par actions simplifiée"
        , "Société européenne"
        , "Caisse d'épargne et de prévoyance"
        , "Groupement d'intérêt économique"
        , "Société coopérative agricole"
        , "Société d'assurance mutuelle"
        , "Autre personne morale de droit privé inscrite au registre du commerce et des sociétés"
        , "Organisme mutualiste"
        , "Syndicat de propriétaires"
        ]


hintResultatsEtablissements : String
hintResultatsEtablissements =
    "Seuls les 10 000 premiers résultats sont affichés"


hintResultatsTerritoires : String
hintResultatsTerritoires =
    "Seuls les 20 premiers résultats sont affichés"


hintBandeauSuivis : String
hintBandeauSuivis =
    "Un établissement est suivi par une équipe dès lors qu’un contact, un échange, une demande ou un rappel est saisi sur la fiche de l'établissement."


hintBeneficiairesEffectifsTotalParts : String
hintBeneficiairesEffectifsTotalParts =
    "Pourcentage total du capital détenu et déclaré par le bénéficiaire qui correspond à la somme des parts directes et indirectes. Ce total est soit égal à 0%, soit supérieur ou égal à 25%. En effet, en dessous de ce ratio, la personne physique ne déclare pas son capital."


lienWebinaireDecouverte : String
lienWebinaireDecouverte =
    "https://tally.so/r/nW8LRv"


lienWebinaireFormation : String
lienWebinaireFormation =
    "https://tally.so/r/w4KZ2Y"
