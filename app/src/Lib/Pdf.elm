port module Lib.Pdf exposing (exportToPdf)

import Json.Encode exposing (Value)


port exportToPdf : Value -> Cmd msg
