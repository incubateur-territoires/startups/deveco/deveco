module Lib.UI exposing (Couleur(..), capitalizeName, displayPhone, displayPrenomNom, displayPrenomNomPoint, encodeSpaceToEmptyString, filterBy, formatFloatWithThousandSpacing, formatIntWithThousandSpacing, formatNumberAsStringWithThousandSpacing, infoLine, infoLineHtml, loggingDecoder, numberCard, numberTile, plural, scrollElementTo, withEmptyAs)

import Accessibility exposing (Attribute, Html, div, span, text)
import Browser.Dom as Dom
import DSFR.Card
import DSFR.Tile
import DSFR.Typography as Typo
import Html.Attributes exposing (class)
import Html.Attributes.Extra exposing (empty)
import Json.Decode as Decode exposing (Decoder)
import List.Extra
import String.Extra
import Task


displayPhone : String -> String
displayPhone phone =
    phone
        |> String.replace " " ""
        |> String.toList
        |> List.reverse
        |> List.Extra.greedyGroupsOf 2
        |> List.reverse
        |> List.map (List.reverse >> String.fromList)
        |> String.join "\u{2009}"


displayPrenomNom : { data | prenom : String, nom : String } -> String
displayPrenomNom { prenom, nom } =
    prenom ++ " " ++ nom


displayPrenomNomPoint : { data | prenom : String, nom : String } -> String
displayPrenomNomPoint { prenom, nom } =
    prenom ++ " " ++ abbreviateNom nom



-- TODO
-- const capitalize = (str: string) =>
--     [...str].reduce((result, char, i, arr) => {
--         if (i === 0) return `${char.toUpperCase()}.`;
--
--         if (separators.includes(arr[i - 1])) {
--             return `${result}${arr[i - 1]}${char.toUpperCase()}.`;
--         }
--
--         return result;
--     }, '');


abbreviateNom : String -> String
abbreviateNom nom =
    if String.contains " " nom then
        nom
            |> String.split " "
            |> List.map capitalizeWord
            |> List.map (\s -> String.left 1 s ++ ".")
            |> String.join " "

    else if String.contains "-" nom then
        nom
            |> String.split "-"
            |> List.map capitalizeWord
            |> List.map (\s -> String.left 1 s ++ ".")
            |> String.join "-"

    else if String.contains "'" nom then
        nom
            |> String.split "'"
            |> List.map capitalizeWord
            |> List.map (\s -> String.left 1 s ++ ".")
            |> String.join "'"

    else if String.contains "\u{00A0}" nom then
        nom
            |> String.split "\u{00A0}"
            |> List.map capitalizeWord
            |> List.map (\s -> String.left 1 s ++ ".")
            |> String.join "\u{00A0}"

    else
        nom
            |> capitalizeWord
            |> (\s -> String.left 1 s ++ ".")


capitalizeName : String -> String
capitalizeName name =
    let
        separators =
            [ " ", "-", "'", "\u{00A0}" ]
    in
    separators
        |> List.foldl
            (\separator result ->
                result
                    |> String.split separator
                    |> List.map capitalizeWord
                    |> String.join separator
            )
            (String.toLower name)


capitalizeWord : String -> String
capitalizeWord word =
    case String.toList word of
        [] ->
            ""

        first :: rest ->
            if first == '(' then
                rest
                    |> String.fromList
                    |> capitalizeWord
                    |> String.toList
                    |> (\l -> first :: l)
                    |> String.fromList

            else
                Char.toLocaleUpper first
                    :: rest
                    |> String.fromList


withEmptyAs : String -> String -> String
withEmptyAs default txt =
    if txt == "" then
        default

    else
        txt


infoLine : Maybe Int -> String -> String -> Html msg
infoLine maxLine lab content =
    infoLineWrapper maxLine lab content (text content)


infoLineHtml : Maybe Int -> String -> String -> Html msg -> Html msg
infoLineHtml maxLine lab title content =
    infoLineWrapper maxLine lab title content


infoLineWrapper : Maybe Int -> String -> String -> Html msg -> Html msg
infoLineWrapper maxLine lab title content =
    let
        separator =
            "\u{00A0}: "

        ( clamp, height ) =
            case maxLine of
                Just 1 ->
                    ( Html.Attributes.class "line-clamp-1", Html.Attributes.style "min-height" "1.5rem" )

                Just 2 ->
                    ( Html.Attributes.class "line-clamp-2", Html.Attributes.style "min-height" "3rem" )

                _ ->
                    ( empty, empty )
    in
    div [ clamp, height, Html.Attributes.title <| lab ++ separator ++ title ]
        [ span [] [ text <| lab ++ separator ]
        , span
            [ Typo.textBold
            ]
            [ content ]
        ]


plural : Int -> String
plural t =
    case String.fromInt t of
        "0" ->
            ""

        "-1" ->
            ""

        "1" ->
            ""

        _ ->
            "s"


formatIntWithThousandSpacing : Int -> String
formatIntWithThousandSpacing num =
    num
        |> String.fromInt
        |> formatNumberAsStringWithThousandSpacing


formatFloatWithThousandSpacing : Float -> String
formatFloatWithThousandSpacing num =
    num
        |> String.fromFloat
        |> String.split "."
        |> (\list ->
                case list of
                    [ val ] ->
                        formatNumberAsStringWithThousandSpacing val

                    [ val, decimal ] ->
                        [ formatNumberAsStringWithThousandSpacing val, String.padRight 2 '0' decimal ] |> String.join ","

                    _ ->
                        ""
           )


formatNumberAsStringWithThousandSpacing : String -> String
formatNumberAsStringWithThousandSpacing str =
    str
        |> String.toList
        |> List.reverse
        |> List.Extra.greedyGroupsOf 3
        |> List.reverse
        |> List.map (List.reverse >> String.fromList)
        |> String.join "\u{2009}"


scrollElementTo : (Result Dom.Error () -> msg) -> Maybe String -> ( Float, Float ) -> Cmd msg
scrollElementTo toMsg elementId ( x, y ) =
    case elementId of
        Nothing ->
            Task.attempt toMsg <| Dom.setViewport x y

        Just id ->
            Task.attempt toMsg <| Dom.setViewportOf id x y


numberCard : List (Attribute Never) -> String -> List String -> Int -> Html msg
numberCard extraAttrs prefix lines amount =
    DSFR.Card.card
        (div [ Html.Attributes.class "!m-0", Typo.textBold, Typo.fr_h1, Html.Attributes.class "text-center" ]
            [ text prefix
            , text <| formatIntWithThousandSpacing <| amount
            ]
        )
        DSFR.Card.vertical
        |> DSFR.Card.withArrow False
        |> DSFR.Card.withNoTitle
        |> DSFR.Card.withExtraAttrs extraAttrs
        |> DSFR.Card.withDescription
            (Just <|
                div [ Html.Attributes.class "text-center" ] <|
                    [ text <| String.concat <| lines
                    ]
            )
        |> DSFR.Card.view


type Couleur
    = Bleu
    | Orange
    | Rouge


couleurToClass : Couleur -> String
couleurToClass couleur =
    case couleur of
        Bleu ->
            "blue-text"

        Orange ->
            "orange-text"

        Rouge ->
            "red-text"


numberTile : Maybe String -> List String -> String -> Couleur -> Int -> Html msg
numberTile route badges description couleur quantite =
    DSFR.Tile.vertical
        { title =
            span [ Typo.textBold, class <| couleurToClass couleur, class "text-[4rem] leading-[5rem]" ]
                [ text <| formatIntWithThousandSpacing <| quantite
                ]
        , description = Just <| text description
        , link = route
        }
        |> DSFR.Tile.withBadges badges
        |> DSFR.Tile.view


filterBy : (data -> String) -> String -> List data -> List data
filterBy accessor searchText allOptions =
    let
        lowerSearch =
            searchText
                |> String.Extra.removeDiacritics
                |> String.toLower
    in
    List.filter (String.contains lowerSearch << String.toLower << String.Extra.removeDiacritics << accessor) <|
        allOptions


encodeSpaceToEmptyString : String -> String
encodeSpaceToEmptyString s =
    if s == " " then
        ""

    else
        s


loggingDecoder : Decoder a -> Decoder a
loggingDecoder realDecoder =
    Decode.value
        |> Decode.andThen
            (\event ->
                case Decode.decodeValue realDecoder event of
                    Ok decoded ->
                        Decode.succeed decoded

                    Err error ->
                        error
                            |> Decode.errorToString
                            |> Decode.fail
            )
