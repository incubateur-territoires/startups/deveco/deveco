module Lib.Umap exposing (umapLink, umapLinkExport, umapLinkFeature)

import Json.Encode as Encode
import Url.Builder


umapLink : List Url.Builder.QueryParameter -> String
umapLink =
    Url.Builder.crossOrigin "https://umap.incubateur.anct.gouv.fr" [ "fr", "map" ]


umapLinkExport : String -> String -> String
umapLinkExport root lien =
    umapLink
        [ Url.Builder.string "dataUrl" <| root ++ lien
        , Url.Builder.string "popupTemplate" "Table"
        , Url.Builder.string "labelKey" "nom"
        ]


umapLinkFeature : { etablissement | nom : String, siret : String, latitude : Float, longitude : Float } -> String
umapLinkFeature etablissement =
    let
        feature =
            etablissement
                |> toFeature
                |> Encode.encode 0
    in
    umapLink
        [ Url.Builder.int "zoomTo" 16
        , Url.Builder.string "labelKey" "Label"
        , Url.Builder.string "showLabel" "false"
        , Url.Builder.string "data" feature
        ]


toFeature : { etablissement | nom : String, siret : String, latitude : Float, longitude : Float } -> Encode.Value
toFeature { nom, siret, latitude, longitude } =
    [ ( "type", Encode.string "Feature" )
    , ( "properties"
      , [ ( "Nom", Encode.string nom )
        , ( "SIRET", Encode.string siret )
        , ( "Label", Encode.string <| nom ++ " - " ++ siret )
        ]
            |> Encode.object
      )
    , ( "geometry"
      , [ ( "type", Encode.string "Point" )
        , ( "coordinates"
          , [ latitude, longitude ]
                |> Encode.list Encode.float
          )
        ]
            |> Encode.object
      )
    ]
        |> Encode.object
