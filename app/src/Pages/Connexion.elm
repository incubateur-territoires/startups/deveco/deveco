module Pages.Connexion exposing (Model, Msg, page)

import Accessibility exposing (Html, decorativeImg, div, h1, text)
import Api
import Api.Auth
import Browser.Dom
import DSFR.Alert
import DSFR.Button
import DSFR.Grid
import DSFR.Icons
import DSFR.Icons.System
import DSFR.Input
import DSFR.Typography as Typo exposing (externalLink)
import Effect
import Html exposing (br)
import Html.Attributes as Attr exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing, viewIf)
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Lib.Variables exposing (lienWebinaireDecouverte)
import RemoteData as RD
import Route
import Shared
import Spa.Page exposing (Page)
import Task
import UI.Layout
import View exposing (View)


type alias Model =
    { identifiant : String
    , password : String
    , passwordMode : Bool
    , loginRequest : RD.RemoteData AuthType AuthType
    , redirectUrl : Maybe String
    }


type Msg
    = ChangedUsername String
    | ChangedPassword String
    | RequestedAuth AuthType
    | ReceivedAuth (RD.RemoteData AuthType AuthType)
    | ToggledPasswordMode
    | NoOp


type AuthType
    = MagicLink String
    | Password


page : Shared.Shared -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page _ =
    Spa.Page.element
        { view = view
        , init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        }


init : Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init redirect =
    ( { identifiant = ""
      , password = ""
      , passwordMode = False
      , loginRequest = RD.NotAsked
      , redirectUrl = redirect
      }
    , Effect.fromSharedCmd <|
        Api.Auth.checkAuth True <|
            Just <|
                Maybe.withDefault (Route.toUrl <| Route.Dashboard redirect) <|
                    redirect
    )
        |> Shared.pageChangeEffects


decodeAuth : Decode.Decoder AuthType
decodeAuth =
    Decode.oneOf
        [ Decode.map MagicLink decodeMagicLink
        , Decode.map (\_ -> Password) decodePasswordLogin
        ]


decodeMagicLink : Decode.Decoder String
decodeMagicLink =
    Decode.field "url" <|
        Decode.string


decodePasswordLogin : Decode.Decoder String
decodePasswordLogin =
    Decode.map (\_ -> "") <|
        Decode.field "password" <|
            Decode.bool


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        ChangedUsername username ->
            ( { model
                | identifiant = username
                , loginRequest = RD.NotAsked
              }
            , Effect.none
            )

        ToggledPasswordMode ->
            { model
                | passwordMode = not model.passwordMode
                , loginRequest = RD.NotAsked
            }
                |> Effect.withNone

        ChangedPassword password ->
            ( { model
                | password = password
                , loginRequest = RD.NotAsked
              }
            , Effect.none
            )

        RequestedAuth authType ->
            case authType of
                MagicLink _ ->
                    if model.identifiant == "" then
                        { model | loginRequest = RD.NotAsked }
                            |> Effect.withNone

                    else
                        ( { model | loginRequest = RD.Loading }
                        , Effect.fromCmd <|
                            Http.post
                                { url = Api.connexion
                                , body =
                                    Http.jsonBody <|
                                        Encode.object
                                            [ ( "identifiant", Encode.string <| String.trim <| model.identifiant )
                                            , ( "redirectUrl", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| model.redirectUrl )
                                            ]
                                , expect =
                                    Http.expectJson
                                        (RD.fromResult >> RD.mapError (\_ -> authType) >> ReceivedAuth)
                                        decodeAuth
                                }
                        )

                Password ->
                    if model.identifiant == "" || model.password == "" then
                        { model | loginRequest = RD.NotAsked }
                            |> Effect.withNone

                    else
                        ( { model | loginRequest = RD.Loading }
                        , Effect.fromCmd <|
                            Http.post
                                { url = Api.connexion
                                , body =
                                    Http.jsonBody <|
                                        Encode.object
                                            [ ( "identifiant", Encode.string <| String.trim <| model.identifiant )
                                            , ( "motDePasse", Encode.string <| String.trim <| model.password )
                                            , ( "redirectUrl", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| model.redirectUrl )
                                            ]
                                , expect =
                                    Http.expectJson
                                        (RD.fromResult >> RD.map (\_ -> Password) >> RD.mapError (\_ -> authType) >> ReceivedAuth)
                                        (Decode.succeed ())
                                }
                        )

        ReceivedAuth result ->
            let
                effect =
                    case result of
                        RD.Success (MagicLink _) ->
                            Task.attempt (\_ -> NoOp) (Browser.Dom.focus "accessUrl")
                                |> Effect.fromCmd

                        RD.Success Password ->
                            Effect.fromShared (Shared.Navigate <| Route.Dashboard model.redirectUrl)

                        _ ->
                            Effect.none
            in
            ( { model | loginRequest = result }
            , effect
            )

        NoOp ->
            ( model, Effect.none )


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle "Connexion"
    , body = UI.Layout.lazyBody body model
    , route = Route.Connexion Nothing
    }


body : Model -> Html Msg
body { identifiant, password, passwordMode, loginRequest, redirectUrl } =
    let
        disabled =
            loginRequest == RD.Loading
    in
    div [ class "fr-card--white pt-8" ]
        [ div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col2, DSFR.Grid.colOffset2, class "border-r-2 border-france-blue flex items-start justify-end pr-2" ]
                [ decorativeImg [ Attr.src "/assets/deveco-cadenas.svg" ] ]
            , div [ DSFR.Grid.col6, DSFR.Grid.colMd4, class "pl-8 flex" ]
                [ div [ class "w-full self-center" ]
                    [ h1 [ Typo.fr_h4, class "w-full" ] [ text "Se connecter à Dévéco" ]
                    , case loginRequest of
                        RD.Success (MagicLink magicLink) ->
                            div [ class "flex flex-col gap-4" ]
                                [ text "Un e-mail vous a été envoyé\u{00A0}!"
                                , if magicLink == "" then
                                    nothing

                                  else
                                    div [ class "flex" ]
                                        [ Typo.linkStandalone Typo.linkNoIcon
                                            magicLink
                                            [ Attr.id "accessUrl"
                                            , class "focus:outline-none focus:ring focus:ring-france-blue"
                                            ]
                                            [ text "Se connecter" ]
                                        ]
                                ]

                        RD.Success _ ->
                            div [ class "flex flex-col gap-4" ]
                                [ text "Si vous n'êtes pas redirigé(e), "
                                , Typo.link (Route.toUrl <| Route.Dashboard redirectUrl) [] [ text "cliquez pour vous rendre sur votre page d'accueil." ]
                                ]

                        _ ->
                            div [ class "flex flex-col w-full gap-4" ]
                                [ Html.form [ Events.onSubmit <| RequestedAuth <| MagicLink "" ]
                                    [ { value = identifiant
                                      , onInput = ChangedUsername
                                      , label = text "Identifiant *"
                                      , name = "username"
                                      }
                                        |> DSFR.Input.new
                                        |> DSFR.Input.withHint [ text "ex\u{00A0}: nour@anct.gouv.fr" ]
                                        |> DSFR.Input.withDisabled disabled
                                        |> DSFR.Input.withError Nothing
                                        |> DSFR.Input.view
                                    , DSFR.Button.new { label = "Recevoir un lien de connexion", onClick = Nothing }
                                        |> DSFR.Button.withDisabled (disabled || identifiant == "" || passwordMode)
                                        |> DSFR.Button.withAttrs [ class "!flex !w-full !justify-center" ]
                                        |> DSFR.Button.submit
                                        |> DSFR.Button.view
                                    ]
                                , viewLoginRequestErrorFor (MagicLink "") loginRequest
                                , div [ class "flex items-center my-4" ]
                                    [ div [ class "flex-grow bg bg-gray-300 h-0.5" ] []
                                    , div [ class "flex-grow-0 mx-5 text" ] [ text "ou" ]
                                    , div [ class "flex-grow bg bg-gray-300 h-0.5" ] []
                                    ]
                                , viewIf (not passwordMode) <|
                                    div [ class "mb-2" ]
                                        [ DSFR.Button.new { label = "Saisir mon mot de passe", onClick = Just ToggledPasswordMode }
                                            |> DSFR.Button.secondary
                                            |> DSFR.Button.withAttrs [ class "!flex !w-full !justify-center" ]
                                            |> DSFR.Button.view
                                        ]
                                , viewIf passwordMode <|
                                    div [ class "mb-2" ]
                                        [ DSFR.Button.new { label = "Masquer le mot de passe", onClick = Just ToggledPasswordMode }
                                            |> DSFR.Button.secondary
                                            |> DSFR.Button.withAttrs [ class "!flex !w-full !justify-center" ]
                                            |> DSFR.Button.view
                                        ]
                                , viewIf passwordMode <|
                                    Html.form [ class "flex flex-col", Events.onSubmit <| RequestedAuth Password ]
                                        [ { value = password
                                          , onInput = ChangedPassword
                                          , label = text "Mot de passe *"
                                          , name = "current-password"
                                          }
                                            |> DSFR.Input.new
                                            |> DSFR.Input.password
                                            |> DSFR.Input.withDisabled disabled
                                            |> DSFR.Input.withError Nothing
                                            |> DSFR.Input.withExtraAttrs [ class "!mb-0" ]
                                            |> DSFR.Input.view
                                        , div [ class "fr-text-default--info !mb-4", Typo.textSm ]
                                            [ DSFR.Icons.System.infoFill |> DSFR.Icons.iconSM
                                            , text " "
                                            , text "Vous pouvez gérer votre mot de passe dans les paramètres de votre compte ("
                                            , externalLink "/documentation/connexion" [] [ text "voir tutoriel" ]
                                            , text ")"
                                            ]
                                        , DSFR.Button.new { onClick = Nothing, label = "Se connecter" }
                                            |> DSFR.Button.withDisabled (disabled || identifiant == "" || password == "" || not passwordMode)
                                            |> DSFR.Button.withAttrs [ Attr.name "submit-connexion", class "!flex !w-full !justify-center" ]
                                            |> DSFR.Button.submit
                                            |> DSFR.Button.view
                                        ]
                                , viewIf passwordMode <|
                                    viewLoginRequestErrorFor Password loginRequest
                                , div [ Typo.textXs, class "mt-4 text-center" ]
                                    [ text "Vous n'avez pas encore de compte\u{00A0}?"
                                    , br [] []
                                    , externalLink
                                        lienWebinaireDecouverte
                                        []
                                        [ text "S'inscrire à un webinaire de découverte" ]
                                    ]
                                ]
                    ]
                ]
            ]
        , div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col12 ]
                [ text "\u{00A0}"
                ]
            ]
        ]


viewLoginRequestErrorFor : AuthType -> RD.RemoteData AuthType a -> Html msg
viewLoginRequestErrorFor authType loginRequest =
    case ( authType, loginRequest ) of
        ( Password, RD.Failure Password ) ->
            DSFR.Alert.small { title = Just "Connexion échouée", description = text "Nous n'avons pas pu vous identifier. Veuillez vérifier que votre identifiant et votre mot de passe sont corrects et nous contacter si le problème persiste." }
                |> DSFR.Alert.alert Nothing DSFR.Alert.error
                |> List.singleton
                |> div []

        ( MagicLink _, RD.Failure (MagicLink _) ) ->
            DSFR.Alert.small { title = Just "Connexion échouée", description = text "Veuillez vérifier votre identifiant et nous contacter si le problème persiste." }
                |> DSFR.Alert.alert Nothing DSFR.Alert.error
                |> List.singleton
                |> div []

        _ ->
            nothing
