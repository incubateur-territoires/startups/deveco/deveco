module Pages.Etablissement.Siret_ exposing (Onglet(..), ongletsToQuery, page)

import Accessibility exposing (Html, br, div, formWithListeners, h1, h3, h4, hr, label, p, span, sup, text)
import Api
import Api.Auth exposing (delete, get, getWithError, post)
import Api.EntityId exposing (parseEntityId)
import Browser.Dom as Dom
import DSFR.Alert
import DSFR.Badge
import DSFR.Button
import DSFR.Grid
import DSFR.Icons
import DSFR.Icons.Buildings
import DSFR.Icons.Business
import DSFR.Icons.Communication
import DSFR.Icons.Design
import DSFR.Icons.Document
import DSFR.Icons.Map
import DSFR.Icons.System
import DSFR.Icons.User
import DSFR.Input
import DSFR.Modal
import DSFR.Table
import DSFR.Tabs
import DSFR.Tag
import DSFR.Tile
import DSFR.Typography as Typo
import Data.Adresse exposing (ApiAdresse, decodeApiFeatures)
import Data.Collegues exposing (Collegue)
import Data.Contact exposing (Contact, ContactInputType(..), EntiteLien(..), FonctionContact(..))
import Data.Demande exposing (Demande)
import Data.Equipe
import Data.Etablissement exposing (EtablissementSimple, EtablissementSimpleAvecPortefeuille, EtatAdministratif(..), Siret, entrepriseTypeToDisplay)
import Data.Etiquette exposing (Etiquette, Etiquettes, decodeEtiquetteList)
import Data.Local exposing (LocalSimple)
import Data.Personne exposing (Personne, decodePersonne)
import Data.Ressource exposing (Ressource, RessourceAction(..), RessourceField(..), encodeRessource, ressourceTypeToDisplay, ressourceTypeToString, ressourceTypes, updateRessource, viewRessources)
import Data.Role
import Data.Zonage exposing (Zonage, decodeZonage)
import Date exposing (Date)
import Dict exposing (Dict)
import Effect
import Html
import Html.Attributes exposing (class, classList, id, style)
import Html.Events as Events
import Html.Extra exposing (nothing, viewIf, viewMaybe)
import Html.Lazy as Lazy
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode
import Lib.Date
import Lib.UI exposing (loggingDecoder, plural, withEmptyAs)
import Lib.Umap
import Lib.Variables exposing (hintActivites, hintBeneficiairesEffectifsTotalParts, hintDonneesEtablissementsPubliques, hintMotsCles, hintZoneGeographique)
import List.Extra exposing (isPermutationOf)
import MultiSelectRemote exposing (SelectConfig)
import Pages.Annuaire exposing (defaultFilters)
import Pages.Etablissement.Data as Data exposing (BeneficiaireEffectif, Detention, Effectif, EntrepriseData, EtablissementCreationLie, EtablissementHistorique, EtablissementWithExtraInfo, Exercice, ExerciceInpi, Filiale, MandatairePersonneMorale, MandatairePersonnePhysique, ProcedureCollective, decodeParticipations)
import Pages.Etablissements.Filters as Filters exposing (emptyFilters, filtersAndPaginationToQuery)
import Platform.Cmd as Cmd
import Process
import QS
import RemoteData as RD exposing (WebData)
import Route
import Shared
import SingleSelectRemote
import Spa.Page exposing (Page)
import Task
import Time exposing (Posix, Zone)
import UI.Clipboard
import UI.Collegue exposing (Partage, getCollegues, viewPartageModal)
import UI.Contact exposing (ContactAction(..), ContactField, NewContactForm, encodeNewContactForm, newContactFormPourExistant, updateContact, updateContactForm, viewContacts, viewDeleteContactBody, viewEditContact, viewNewContactFormFields)
import UI.Entite
import UI.Etablissement exposing (carte)
import UI.Layout
import UI.Local
import UI.Pagination exposing (defaultPagination)
import UI.Suivi
import Url.Builder as Builder
import User
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page ( Maybe String, String ) Shared.Msg (View Msg) Model Msg
page shared user =
    Spa.Page.onNewFlags (Tuple.first >> queryToOnglets >> UpdateOnglets) <|
        Spa.Page.element
            { view = view user shared
            , init = init
            , update = update user
            , subscriptions = subscriptions
            }


type alias Model =
    { siret : String
    , etablissementWithExtraInfo : WebData RecuperationEtablissement
    , entrepriseExercices : WebData (Maybe (List Exercice))
    , etablissementMandataires : WebData (Maybe ( List MandatairePersonnePhysique, List MandatairePersonneMorale ))
    , entrepriseBeneficiaires : WebData (Maybe (List BeneficiaireEffectif))
    , entrepriseLiasseFiscale : WebData (Maybe Data.LiasseFiscale)
    , entrepriseParticipations : WebData Data.Participations
    , entrepriseParticipationsVoir : Bool
    , editEtablissementEtiquettes : Maybe EtablissementEtiquettes
    , editEtablissementCommentaire : Maybe EtablissementCommentaire
    , saveEtablissementEtiquettesRequest : WebData EtablissementWithExtraInfo
    , saveEtablissementCommentaireRequest : WebData EtablissementWithExtraInfo
    , selectActivites : MultiSelectRemote.SmartSelect Msg String
    , selectLocalisations : MultiSelectRemote.SmartSelect Msg String
    , selectMots : MultiSelectRemote.SmartSelect Msg String
    , contactAction : ContactAction
    , saveContactRequest : WebData EtablissementWithExtraInfo
    , saveRessourceRequest : WebData EtablissementWithExtraInfo
    , suiviModel : UI.Suivi.Model
    , ressourceAction : RessourceAction
    , zonages : WebData (List Zonage)
    , signaleFermeRequest : WebData (Maybe Date)
    , enseigneContribution : Maybe String
    , modifierEnseigneRequest : WebData ()
    , geolocalisationContribution : Maybe GeolocalisationContribution
    , modifierGeolocalisationRequest : WebData ()
    , devecos : WebData (List Collegue)
    , contactCreationMandataire : WebData EtablissementWithExtraInfo
    , effectifsPeriode : EffectifsPeriode
    , onglet : Onglet
    , suivisParAutres : WebData (List SuiviEquipe)
    , suivisParAutresModal : Bool
    , partageModal : Maybe Partage
    , currentUrl : String
    , prochainId : Int
    , voirToutesFiliales : Bool
    , voirToutesDetentions : Bool
    , voirTousBeneficiaires : Bool
    , voirTousMandatairesPPhysiques : Bool
    , voirTousMandatairesPMorales : Bool
    }


type alias RecuperationEtablissement =
    Result String EtablissementWithExtraInfo


type alias GeolocalisationContribution =
    { centreCarte : ( Float, Float )
    , geolocalisationActuelle : Maybe ( Float, Float )
    , geolocalisationContribution : Maybe ( Float, Float )
    , nouvelleGeolocalisation : Maybe ( Float, Float )
    , nouvelleGeolocalisationChamp : String
    , zoom : Maybe Int
    , selectAdresse : SingleSelectRemote.SmartSelect Msg ApiAdresse
    }


type EffectifsPeriode
    = DouzeDerniersMois
    | Annee String


effectifsPeriodeToString : EffectifsPeriode -> String
effectifsPeriodeToString ep =
    case ep of
        DouzeDerniersMois ->
            "douzeDerniersMois"

        Annee annee ->
            "annee-" ++ annee


effectifsPeriodeToDisplay : EffectifsPeriode -> String
effectifsPeriodeToDisplay ep =
    case ep of
        DouzeDerniersMois ->
            "12 derniers mois"

        Annee annee ->
            "Année " ++ annee


type Onglet
    = OngletEtablissement
    | OngletEntreprise
    | OngletSuivi


ongletToString : Onglet -> String
ongletToString onglet =
    case onglet of
        OngletEtablissement ->
            "etablissement"

        OngletEntreprise ->
            "entreprise"

        OngletSuivi ->
            "suivi"


ongletToDisplay : Onglet -> String
ongletToDisplay onglet =
    case onglet of
        OngletEtablissement ->
            "Établissement"

        OngletEntreprise ->
            "Entreprise"

        OngletSuivi ->
            "Suivi de la relation"


stringToOnglet : String -> Maybe Onglet
stringToOnglet s =
    case s of
        "etablissement" ->
            Just OngletEtablissement

        "entreprise" ->
            Just OngletEntreprise

        "suivi" ->
            Just OngletSuivi

        _ ->
            Nothing


type alias EtablissementEtiquettes =
    { activites : List Etiquette
    , localisations : List Etiquette
    , mots : List Etiquette
    }


type alias EtablissementCommentaire =
    { description : String
    }


type alias SuiviEquipe =
    { nom : String
    , contacts : Maybe Int
    , echanges : Maybe Int
    , demandes : Maybe Int
    , rappels : Maybe Int
    }


selectActivitesId : String
selectActivitesId =
    "champ-selection-activites"


selectLocalisationsId : String
selectLocalisationsId =
    "champ-selection-zones"


selectMotsId : String
selectMotsId =
    "champ-selection-mots"


selectCharacterThreshold : Int
selectCharacterThreshold =
    0


selectDebounceDuration : Float
selectDebounceDuration =
    400


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | ReceivedCollegues (WebData (List Collegue))
    | ReceivedEtablissement (WebData (Result String EtablissementWithExtraInfo))
    | ReceivedApiEntrepriseData (WebData ApiEntrepriseData)
    | EditEtablissementCommentaire
    | CancelEditEtablissementCommentaire
    | UpdatedDescription String
    | RequestedSaveEtablissementCommentaire
    | ReceivedSaveEtablissementCommentaire (WebData EtablissementWithExtraInfo)
    | EditEtablissementEtiquettes
    | CancelEditEtablissementEtiquettes
    | RequestedSaveEtablissementEtiquettes
    | ReceivedSaveEtablissementEtiquettes (WebData EtablissementWithExtraInfo)
    | SelectedActivites ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectActivites (MultiSelectRemote.Msg String)
    | UnselectActivite String
    | SelectedLocalisations ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectLocalisations (MultiSelectRemote.Msg String)
    | UnselectLocalisation String
    | SelectedMots ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectMots (MultiSelectRemote.Msg String)
    | UnselectMot String
    | SetContactAction ContactAction
    | ConfirmContactAction
    | CancelContactAction
    | UpdatedContact ContactField String
    | ReceivedSaveContact (WebData EtablissementWithExtraInfo)
    | ToggleFavorite Bool
    | ReceivedToggleFavoriteResult Bool
    | ToggleSignaleFerme Bool
    | ToggleEnseigneContribution
    | UpdateEnseigneContribution String
    | RequestedEnseigneContributionSave
    | ReceivedEnseigneContributionSave (WebData EtablissementWithExtraInfo)
    | ToggleGeolocalisationContribution
    | UpdateGeolocalisationContributionInput String
    | UpdateGeolocalisationContributionByMap ( Float, Float )
    | RequestedGeolocalisationContributionSave
    | ReceivedGeolocalisationContributionSave (WebData EtablissementWithExtraInfo)
    | ReceivedToggleSignaleFermeResult (WebData (Maybe Date))
    | ReceivedZonages (WebData (List Zonage))
    | SetEffectifsPeriode EffectifsPeriode
    | ClickedChangementOnglet Onglet
    | SetRessourceAction RessourceAction
    | UpdatedRessource RessourceField String
    | ConfirmRessourceAction
    | CancelRessourceAction
    | ReceivedSaveRessource (WebData EtablissementWithExtraInfo)
    | ReceivedSuivis (WebData (List SuiviEquipe))
    | ToggleSuivisEquipesModal Bool
    | ClickedPrintToPdf
    | PrintToPdf
    | ClickedPartage
    | CancelPartage
    | UpdatedPartageDeveco String
    | UpdatedPartageCommentaire String
    | ConfirmPartage
    | ReceivedPartage (WebData ())
    | UpdatedRechercheContactExistant String
    | RechercheContactExistant
    | ReceivedContactsExistants (WebData (List Personne))
    | SelectedContactExistant (Maybe Personne)
    | ToggleNouveauContactModeCreation Bool
    | SuiviMsg UI.Suivi.Msg
    | UpdateOnglets ( Onglet, UI.Suivi.Onglet )
    | SelectedAdresse ( ApiAdresse, SingleSelectRemote.Msg ApiAdresse )
    | UpdatedSelectAdresse (SingleSelectRemote.Msg ApiAdresse)
    | ToggleVoirFiliales
    | ToggleVoirDetentions
    | ToggleVoirBeneficiaires
    | ToggleVoirMandatairesPPhysiques
    | ToggleVoirMandatairesPMorales
    | ClickedVoirParticipations
    | ReceivedParticipations (WebData Data.Participations)
    | ClickedFermerParticipations


type alias ApiEntrepriseData =
    { exercices : Maybe (List Exercice)
    , mandataires : Maybe ( List MandatairePersonnePhysique, List MandatairePersonneMorale )
    , beneficiaires : Maybe (List BeneficiaireEffectif)
    , liasseFiscale : Maybe Data.LiasseFiscale
    }


adresseFilter : SingleSelectRemote.SmartSelect Msg ApiAdresse -> Html Msg
adresseFilter selectAdresse =
    div [ class "flex flex-col gap-2 relative z-20" ]
        [ div [ class "absolute top-[1em] left-[1em] w-[50%]" ]
            [ selectAdresse
                |> SingleSelectRemote.viewCustom
                    { isDisabled = False
                    , selected = Nothing
                    , optionLabelFn = .label
                    , optionDescriptionFn = \_ -> ""
                    , optionsContainerMaxHeight = 300
                    , selectTitle = nothing
                    , searchPrompt = "Rechercher une ville, une adresse"
                    , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                    , noResultsForMsg =
                        \searchText ->
                            "Aucune autre adresse n'a été trouvée"
                                ++ (if searchText == "" then
                                        ""

                                    else
                                        " pour " ++ searchText
                                   )
                    , noOptionsMsg = "Aucune adresse n'a été trouvée"
                    , error = Nothing
                    }
            ]
        ]


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ model |> .selectActivites |> MultiSelectRemote.subscriptions
        , model |> .selectLocalisations |> MultiSelectRemote.subscriptions
        , model |> .selectMots |> MultiSelectRemote.subscriptions
        , model |> .geolocalisationContribution |> Maybe.map (.selectAdresse >> SingleSelectRemote.subscriptions) |> Maybe.withDefault Sub.none
        ]


init : ( Maybe String, String ) -> ( Model, Effect.Effect Shared.Msg Msg )
init ( query, siret ) =
    let
        ( onglet, ongletSuivi ) =
            queryToOnglets query

        nextUrl =
            ongletsToQuery ( onglet, ongletSuivi )
    in
    { siret = siret
    , etablissementWithExtraInfo = RD.Loading
    , entrepriseExercices = RD.NotAsked
    , etablissementMandataires = RD.NotAsked
    , entrepriseBeneficiaires = RD.NotAsked
    , entrepriseLiasseFiscale = RD.NotAsked
    , entrepriseParticipations = RD.NotAsked
    , entrepriseParticipationsVoir = False
    , editEtablissementCommentaire = Nothing
    , editEtablissementEtiquettes = Nothing
    , saveEtablissementEtiquettesRequest = RD.NotAsked
    , saveEtablissementCommentaireRequest = RD.NotAsked
    , selectActivites =
        MultiSelectRemote.init selectActivitesId
            { selectionMsg = SelectedActivites
            , internalMsg = UpdatedSelectActivites
            , characterSearchThreshold = selectCharacterThreshold
            , debounceDuration = selectDebounceDuration
            }
    , selectLocalisations =
        MultiSelectRemote.init selectLocalisationsId
            { selectionMsg = SelectedLocalisations
            , internalMsg = UpdatedSelectLocalisations
            , characterSearchThreshold = selectCharacterThreshold
            , debounceDuration = selectDebounceDuration
            }
    , selectMots =
        MultiSelectRemote.init selectMotsId
            { selectionMsg = SelectedMots
            , internalMsg = UpdatedSelectMots
            , characterSearchThreshold = selectCharacterThreshold
            , debounceDuration = selectDebounceDuration
            }
    , contactAction = None
    , ressourceAction = NoRessource
    , saveContactRequest = RD.NotAsked
    , saveRessourceRequest = RD.NotAsked
    , suiviModel =
        UI.Suivi.init UI.Suivi.OngletEchanges <|
            if onglet == OngletSuivi then
                Just ongletSuivi

            else
                Nothing
    , zonages = RD.Loading
    , signaleFermeRequest = RD.NotAsked
    , enseigneContribution = Nothing
    , modifierEnseigneRequest = RD.NotAsked
    , geolocalisationContribution = Nothing
    , modifierGeolocalisationRequest = RD.NotAsked
    , devecos = RD.NotAsked
    , contactCreationMandataire = RD.NotAsked
    , effectifsPeriode = DouzeDerniersMois
    , onglet = onglet
    , suivisParAutres = RD.Loading
    , suivisParAutresModal = False
    , partageModal = Nothing
    , currentUrl = nextUrl
    , prochainId = 0
    , voirToutesFiliales = False
    , voirToutesDetentions = False
    , voirTousBeneficiaires = False
    , voirTousMandatairesPPhysiques = False
    , voirTousMandatairesPMorales = False
    }
        |> Effect.with
            (Effect.batch
                [ Effect.fromCmd <| fetchEtablissement siret
                , Effect.fromCmd <| getEquipeEtablissementSuivis siret
                , Effect.fromCmd <| getCollegues SharedMsg (Just LogError) ReceivedCollegues
                , Effect.fromShared <|
                    Shared.ReplaceUrl <|
                        Route.Etablissement <|
                            ( nextUrl
                                |> (\q ->
                                        if q == "" then
                                            Nothing

                                        else
                                            Just q
                                   )
                            , siret
                            )
                ]
            )
        |> Shared.pageChangeEffects


update : User.User -> Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update user msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        SuiviMsg suiviMsg ->
            let
                ( suiviModel, suiviEffect, entity ) =
                    UI.Suivi.update (UI.Suivi.suiviConfigEtab (Decode.field "equipeEtablissement" Data.decodeEtablissementWithExtraInfo) model.siret) suiviMsg model.suiviModel

                ongletSuivi =
                    UI.Suivi.getOngletActif suiviModel

                ( newModel, effect ) =
                    { model
                        | suiviModel = suiviModel
                        , etablissementWithExtraInfo =
                            entity
                                |> UI.Suivi.suiviResultToMaybe
                                |> Maybe.map (Ok >> RD.Success)
                                |> Maybe.withDefault model.etablissementWithExtraInfo
                    }
                        |> updateUrl ( model.onglet, ongletSuivi )
            in
            ( newModel
            , Effect.batch [ effect, Effect.map SuiviMsg <| suiviEffect ]
            )

        ReceivedCollegues response ->
            { model | devecos = response }
                |> Effect.withNone

        ReceivedEtablissement resp ->
            case resp of
                RD.Success (Ok etablissement) ->
                    let
                        exercices =
                            case etablissement.entreprise |> Maybe.andThen .exercices of
                                Just ex ->
                                    RD.Success (Just ex)

                                Nothing ->
                                    RD.Loading

                        mandataires =
                            case
                                ( etablissement.entreprise |> Maybe.andThen .mandatairesPersonnesPhysiques
                                , etablissement.entreprise |> Maybe.andThen .mandatairesPersonnesMorales
                                )
                            of
                                ( Just mpp, Just mpm ) ->
                                    RD.Success (Just ( mpp, mpm ))

                                _ ->
                                    RD.Loading

                        beneficiaires =
                            case etablissement.entreprise |> Maybe.andThen .beneficiairesEffectifs of
                                Just ex ->
                                    RD.Success (Just ex)

                                Nothing ->
                                    RD.Loading

                        liasseFiscale =
                            case etablissement.entreprise |> Maybe.andThen .liasseFiscale of
                                Just ex ->
                                    RD.Success (Just ex)

                                Nothing ->
                                    RD.Loading

                        siren =
                            etablissement
                                |> .entreprise
                                |> Maybe.map .siren

                        request =
                            if
                                List.member RD.Loading
                                    [ exercices |> RD.map (always ())
                                    , mandataires |> RD.map (always ())
                                    , beneficiaires |> RD.map (always ())
                                    , liasseFiscale |> RD.map (always ())
                                    ]
                            then
                                case siren of
                                    Just s ->
                                        fetchApiEntrepriseData s

                                    Nothing ->
                                        Cmd.none

                            else
                                Cmd.none
                    in
                    ( { model
                        | etablissementWithExtraInfo = RD.Success <| Ok etablissement
                        , entrepriseExercices = exercices
                        , etablissementMandataires = mandataires
                        , entrepriseBeneficiaires = beneficiaires
                        , entrepriseLiasseFiscale = liasseFiscale
                      }
                    , Effect.batch
                        [ Effect.fromCmd request
                        , getZonages model.siret
                        ]
                    )

                RD.Success err ->
                    { model | etablissementWithExtraInfo = RD.Success err }
                        |> Effect.withNone

                RD.Failure err ->
                    { model | etablissementWithExtraInfo = RD.Failure err }
                        |> Effect.withNone

                RD.Loading ->
                    { model | etablissementWithExtraInfo = RD.Loading }
                        |> Effect.withNone

                RD.NotAsked ->
                    { model | etablissementWithExtraInfo = RD.NotAsked }
                        |> Effect.withNone

        ReceivedApiEntrepriseData resp ->
            { model
                | entrepriseExercices = resp |> RD.map .exercices
                , etablissementMandataires = resp |> RD.map .mandataires
                , entrepriseBeneficiaires = resp |> RD.map .beneficiaires
                , entrepriseLiasseFiscale = resp |> RD.map .liasseFiscale
            }
                |> Effect.withNone

        EditEtablissementCommentaire ->
            case model.etablissementWithExtraInfo of
                RD.Success (Ok { equipeEtablissement }) ->
                    { model
                        | editEtablissementCommentaire =
                            Just
                                { description = equipeEtablissement.description
                                }
                        , saveEtablissementCommentaireRequest = RD.NotAsked
                    }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        CancelEditEtablissementCommentaire ->
            ( { model | editEtablissementCommentaire = Nothing }, Effect.none )

        RequestedSaveEtablissementCommentaire ->
            case model.etablissementWithExtraInfo of
                RD.Success _ ->
                    case model.editEtablissementCommentaire of
                        Just etablissementInfo ->
                            ( { model
                                | saveEtablissementCommentaireRequest = RD.Loading
                              }
                            , updateEtablissementCommentaire model.siret etablissementInfo.description
                            )

                        _ ->
                            ( model, Effect.none )

                _ ->
                    ( model, Effect.none )

        ReceivedSaveEtablissementCommentaire response ->
            case response of
                RD.Success _ ->
                    ( { model | etablissementWithExtraInfo = response |> RD.map Ok, saveEtablissementCommentaireRequest = response, editEtablissementCommentaire = Nothing }, Effect.none )

                _ ->
                    ( { model | saveEtablissementCommentaireRequest = response }, Effect.none )

        UpdatedDescription description ->
            ( { model
                | editEtablissementCommentaire = model.editEtablissementCommentaire |> Maybe.map (\ei -> { ei | description = description })
                , saveEtablissementEtiquettesRequest = RD.NotAsked
              }
            , Effect.none
            )

        EditEtablissementEtiquettes ->
            case model.etablissementWithExtraInfo of
                RD.Success (Ok { equipeEtablissement }) ->
                    { model
                        | editEtablissementEtiquettes =
                            Just
                                { activites = equipeEtablissement.etiquettes.activites
                                , localisations = equipeEtablissement.etiquettes.localisations
                                , mots = equipeEtablissement.etiquettes.motsCles
                                }
                        , saveEtablissementEtiquettesRequest = RD.NotAsked
                    }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        CancelEditEtablissementEtiquettes ->
            ( { model | editEtablissementEtiquettes = Nothing }, Effect.none )

        RequestedSaveEtablissementEtiquettes ->
            case model.etablissementWithExtraInfo of
                RD.Success _ ->
                    case model.editEtablissementEtiquettes of
                        Just etablissementInfo ->
                            ( { model
                                | saveEtablissementEtiquettesRequest = RD.Loading
                              }
                            , updateEtablissementEtiquettes model.siret etablissementInfo
                            )

                        _ ->
                            ( model, Effect.none )

                _ ->
                    ( model, Effect.none )

        ReceivedSaveEtablissementEtiquettes response ->
            case response of
                RD.Success _ ->
                    ( { model | etablissementWithExtraInfo = response |> RD.map Ok, saveEtablissementEtiquettesRequest = response, editEtablissementEtiquettes = Nothing }, Effect.none )

                _ ->
                    ( { model | saveEtablissementEtiquettesRequest = response }, Effect.none )

        SelectedActivites ( activites, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectActivitesConfig model.selectActivites

                activitesUniques =
                    case activites of
                        [] ->
                            []

                        act :: rest ->
                            if List.member act rest then
                                rest

                            else
                                activites
            in
            ( { model
                | selectActivites = updatedSelect
                , editEtablissementEtiquettes = model.editEtablissementEtiquettes |> Maybe.map (\ei -> { ei | activites = activitesUniques })
                , saveEtablissementEtiquettesRequest = RD.NotAsked
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectActivites sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectActivitesConfig model.selectActivites
            in
            ( { model
                | selectActivites = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectActivite activite ->
            if model.saveEtablissementEtiquettesRequest == RD.Loading then
                ( model, Effect.none )

            else
                ( { model
                    | editEtablissementEtiquettes =
                        model.editEtablissementEtiquettes
                            |> Maybe.map
                                (\ei ->
                                    { ei
                                        | activites =
                                            ei.activites
                                                |> List.filter ((/=) activite)
                                    }
                                )
                  }
                , Effect.none
                )

        SelectedLocalisations ( localisations, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations

                localisationsUniques =
                    case localisations of
                        [] ->
                            []

                        act :: rest ->
                            if List.member act rest then
                                rest

                            else
                                localisations
            in
            ( { model
                | selectLocalisations = updatedSelect
                , editEtablissementEtiquettes =
                    model.editEtablissementEtiquettes
                        |> Maybe.map (\ei -> { ei | localisations = localisationsUniques })
                , saveEtablissementEtiquettesRequest = RD.NotAsked
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectLocalisations sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations
            in
            ( { model
                | selectLocalisations = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectLocalisation localisation ->
            if model.saveEtablissementEtiquettesRequest == RD.Loading then
                ( model, Effect.none )

            else
                ( { model
                    | editEtablissementEtiquettes =
                        model.editEtablissementEtiquettes
                            |> Maybe.map
                                (\ei ->
                                    { ei
                                        | localisations =
                                            ei.localisations
                                                |> List.filter ((/=) localisation)
                                    }
                                )
                  }
                , Effect.none
                )

        SelectedMots ( mots, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectMotsConfig model.selectMots

                motsUniques =
                    case mots of
                        [] ->
                            []

                        act :: rest ->
                            if List.member act rest then
                                rest

                            else
                                mots
            in
            ( { model
                | selectMots = updatedSelect
                , editEtablissementEtiquettes =
                    model.editEtablissementEtiquettes
                        |> Maybe.map (\ei -> { ei | mots = motsUniques })
                , saveEtablissementEtiquettesRequest = RD.NotAsked
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectMots sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectMotsConfig model.selectMots
            in
            ( { model
                | selectMots = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectMot mot ->
            if model.saveEtablissementEtiquettesRequest == RD.Loading then
                ( model, Effect.none )

            else
                ( { model
                    | editEtablissementEtiquettes =
                        model.editEtablissementEtiquettes
                            |> Maybe.map
                                (\ei ->
                                    { ei
                                        | mots =
                                            ei.mots
                                                |> List.filter ((/=) mot)
                                    }
                                )
                  }
                , Effect.none
                )

        SetContactAction action ->
            let
                cmd =
                    case action of
                        EditContact _ _ ->
                            Task.attempt (\_ -> NoOp) (Dom.focus <| "nouveau-contact-fonction")

                        NewContact { personneExistante } ->
                            Cmd.batch
                                [ Task.attempt (\_ -> NoOp) (Dom.focus <| "nouveau-contact-fonction")
                                , if personneExistante.recherche /= "" then
                                    Task.perform identity (Task.succeed RechercheContactExistant)

                                  else
                                    Cmd.none
                                ]

                        _ ->
                            Cmd.none
            in
            ( { model | contactAction = action }, Effect.fromCmd cmd )

        CancelContactAction ->
            ( { model | saveContactRequest = RD.NotAsked, contactAction = None }, Effect.none )

        UpdatedContact field value ->
            case model.saveContactRequest of
                RD.Loading ->
                    ( model, Effect.none )

                _ ->
                    Effect.withNone <|
                        (\m -> { m | saveContactRequest = RD.NotAsked }) <|
                            case model.contactAction of
                                None ->
                                    model

                                ViewContactFonctions _ ->
                                    model

                                DeleteContact _ _ ->
                                    model

                                DeleteContactForm _ ->
                                    model

                                NewContact contactForm ->
                                    { model
                                        | contactAction =
                                            NewContact <|
                                                updateContactForm field value <|
                                                    contactForm
                                    }

                                EditContact contactExistant contact ->
                                    { model | contactAction = EditContact contactExistant <| updateContact field value contact }

                                EditContactForm _ _ ->
                                    model

        ConfirmContactAction ->
            case model.saveContactRequest of
                RD.Loading ->
                    ( model, Effect.none )

                _ ->
                    case model.contactAction of
                        None ->
                            ( model, Effect.none )

                        ViewContactFonctions _ ->
                            ( model, Effect.none )

                        NewContact contactForm ->
                            ( { model | saveContactRequest = RD.Loading }, Effect.fromCmd <| createContact model.siret contactForm )

                        DeleteContact contact _ ->
                            ( { model | saveContactRequest = RD.Loading }, Effect.fromCmd <| deleteContact model.siret contact )

                        DeleteContactForm _ ->
                            ( model, Effect.none )

                        EditContact _ contact ->
                            ( { model | saveContactRequest = RD.Loading }, Effect.fromCmd <| editContact model.siret contact )

                        EditContactForm _ _ ->
                            ( model, Effect.none )

        ReceivedSaveContact response ->
            let
                ( contactAction, etablissement ) =
                    case response of
                        RD.Success _ ->
                            ( None, response |> RD.map Ok )

                        _ ->
                            ( model.contactAction, model.etablissementWithExtraInfo )
            in
            ( { model
                | etablissementWithExtraInfo = etablissement
                , contactAction = contactAction
                , saveContactRequest = response
              }
            , Effect.none
            )

        ToggleFavorite add ->
            { model
                | etablissementWithExtraInfo =
                    model.etablissementWithExtraInfo
                        |> RD.map
                            (Result.map
                                (\ewei ->
                                    { ewei
                                        | portefeuilleInfos =
                                            ewei.portefeuilleInfos
                                                |> (\infos ->
                                                        { infos
                                                            | favori = add
                                                        }
                                                   )
                                    }
                                )
                            )
            }
                |> Effect.withCmd
                    (model.etablissementWithExtraInfo
                        |> RD.toMaybe
                        |> Maybe.andThen Result.toMaybe
                        |> Maybe.map (\ewei -> requestToggleFavorite ewei.etablissement.siret add)
                        |> Maybe.withDefault Cmd.none
                    )

        ReceivedToggleFavoriteResult add ->
            { model
                | etablissementWithExtraInfo =
                    model.etablissementWithExtraInfo
                        |> RD.map
                            (Result.map
                                (\etablissementWithExtraInfos ->
                                    { etablissementWithExtraInfos
                                        | portefeuilleInfos =
                                            etablissementWithExtraInfos.portefeuilleInfos
                                                |> (\infos ->
                                                        { infos
                                                            | favori = add
                                                        }
                                                   )
                                    }
                                )
                            )
            }
                |> Effect.withNone

        ToggleSignaleFerme signaleFerme ->
            { model | signaleFermeRequest = RD.Loading }
                |> Effect.withCmd
                    (model.etablissementWithExtraInfo
                        |> RD.toMaybe
                        |> Maybe.andThen Result.toMaybe
                        |> Maybe.map (\ewei -> requestToggleSignaleFerme signaleFerme ewei.etablissement.siret)
                        |> Maybe.withDefault Cmd.none
                    )

        ToggleEnseigneContribution ->
            { model
                | enseigneContribution =
                    case model.enseigneContribution of
                        Just _ ->
                            Nothing

                        Nothing ->
                            model.etablissementWithExtraInfo
                                |> RD.toMaybe
                                |> Maybe.andThen Result.toMaybe
                                |> Maybe.map .etablissement
                                |> Maybe.andThen .enseigneContribution
                                |> Maybe.withDefault ""
                                |> Just
                , modifierEnseigneRequest = RD.NotAsked
            }
                |> Effect.withNone

        UpdateEnseigneContribution enseigneContribution ->
            { model
                | enseigneContribution =
                    model.enseigneContribution |> Maybe.map (\_ -> enseigneContribution)
            }
                |> Effect.withNone

        ToggleGeolocalisationContribution ->
            { model
                | geolocalisationContribution =
                    case model.geolocalisationContribution of
                        Just _ ->
                            Nothing

                        Nothing ->
                            let
                                selectAdresse =
                                    SingleSelectRemote.init "champ-selection-adresse-maplibre"
                                        { selectionMsg = SelectedAdresse
                                        , internalMsg = UpdatedSelectAdresse
                                        , characterSearchThreshold = 3
                                        , debounceDuration = selectDebounceDuration
                                        }
                            in
                            model.etablissementWithExtraInfo
                                |> RD.toMaybe
                                |> Maybe.andThen Result.toMaybe
                                |> Maybe.map .etablissement
                                |> Maybe.map
                                    (\{ geolocalisation, geolocalisationContribution } ->
                                        { centreCarte =
                                            geolocalisationContribution
                                                |> Maybe.map Just
                                                |> Maybe.withDefault geolocalisation
                                                |> Maybe.withDefault centreDeLaFrance
                                        , geolocalisationActuelle = geolocalisation
                                        , geolocalisationContribution = geolocalisationContribution
                                        , nouvelleGeolocalisation = geolocalisationContribution
                                        , nouvelleGeolocalisationChamp =
                                            geolocalisationContribution
                                                |> Maybe.map pointToString
                                                |> Maybe.withDefault ""
                                        , zoom = Nothing
                                        , selectAdresse = selectAdresse
                                        }
                                    )
                , modifierGeolocalisationRequest = RD.NotAsked
            }
                |> Effect.withNone

        UpdateGeolocalisationContributionInput geolocalisationString ->
            { model
                | geolocalisationContribution =
                    model.geolocalisationContribution
                        |> Maybe.map
                            (\geolocalisationContribution ->
                                { geolocalisationContribution
                                    | nouvelleGeolocalisation = geolocalisationString |> stringtoPoint
                                    , nouvelleGeolocalisationChamp = geolocalisationString
                                    , zoom = Nothing
                                }
                            )
            }
                |> Effect.withNone

        UpdateGeolocalisationContributionByMap ( lon, lat ) ->
            { model
                | geolocalisationContribution =
                    model.geolocalisationContribution
                        |> Maybe.map
                            (\geolocalisationContribution ->
                                { geolocalisationContribution
                                    | centreCarte = ( lon, lat )
                                    , nouvelleGeolocalisation = Just ( lon, lat )
                                    , nouvelleGeolocalisationChamp =
                                        ( lon, lat )
                                            |> pointToString
                                    , zoom = Nothing
                                }
                            )
            }
                |> Effect.withNone

        ReceivedToggleSignaleFermeResult resp ->
            { model
                | signaleFermeRequest = resp
                , etablissementWithExtraInfo =
                    model.etablissementWithExtraInfo
                        |> RD.map
                            (Result.map
                                (\etablissementWithExtraInfo ->
                                    { etablissementWithExtraInfo
                                        | etablissement =
                                            etablissementWithExtraInfo.etablissement
                                                |> (\et ->
                                                        { et
                                                            | clotureDateContribution = resp |> RD.withDefault Nothing
                                                        }
                                                   )
                                    }
                                )
                            )
            }
                |> Effect.withNone

        ReceivedZonages resp ->
            { model | zonages = resp }
                |> Effect.withNone

        SetEffectifsPeriode effectifsPeriode ->
            { model | effectifsPeriode = effectifsPeriode }
                |> Effect.withNone

        RequestedEnseigneContributionSave ->
            { model | modifierEnseigneRequest = RD.Loading }
                |> Effect.withCmd (requestEnseigneContributionSave model.etablissementWithExtraInfo model.enseigneContribution)

        ReceivedEnseigneContributionSave resp ->
            let
                ( enseigneContribution, modifierEnseigneRequest ) =
                    case resp of
                        RD.Success _ ->
                            ( Nothing, RD.NotAsked )

                        _ ->
                            ( model.enseigneContribution, resp |> RD.map (\_ -> ()) )
            in
            { model
                | modifierEnseigneRequest = modifierEnseigneRequest
                , enseigneContribution = enseigneContribution
                , etablissementWithExtraInfo =
                    case resp of
                        RD.Success _ ->
                            resp |> RD.map Ok

                        _ ->
                            model.etablissementWithExtraInfo
            }
                |> Effect.withNone

        RequestedGeolocalisationContributionSave ->
            { model | modifierGeolocalisationRequest = RD.Loading }
                |> Effect.withCmd (requestGeolocalisationContributionSave model.etablissementWithExtraInfo model.geolocalisationContribution)

        ReceivedGeolocalisationContributionSave resp ->
            let
                ( geolocalisationContribution, modifierGeolocalisationRequest ) =
                    case resp of
                        RD.Success _ ->
                            ( Nothing, RD.NotAsked )

                        _ ->
                            ( model.geolocalisationContribution, resp |> RD.map (\_ -> ()) )
            in
            { model
                | modifierGeolocalisationRequest = modifierGeolocalisationRequest
                , geolocalisationContribution = geolocalisationContribution
                , etablissementWithExtraInfo =
                    case resp of
                        RD.Success _ ->
                            resp |> RD.map Ok

                        _ ->
                            model.etablissementWithExtraInfo
            }
                |> Effect.withNone

        ClickedChangementOnglet onglet ->
            let
                suiviOnglet =
                    UI.Suivi.getOngletActif model.suiviModel
            in
            updateUrl ( onglet, suiviOnglet ) model

        SetRessourceAction ressourceAction ->
            { model | ressourceAction = ressourceAction }
                |> Effect.withNone

        UpdatedRessource ressourceField ressourceValue ->
            case model.ressourceAction of
                NewRessource ressource ->
                    { model | ressourceAction = NewRessource <| updateRessource ressourceField ressourceValue <| ressource }
                        |> Effect.withNone

                EditRessource ressource ->
                    { model | ressourceAction = EditRessource <| updateRessource ressourceField ressourceValue <| ressource }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        CancelRessourceAction ->
            { model | ressourceAction = NoRessource }
                |> Effect.withNone

        ConfirmRessourceAction ->
            case model.saveRessourceRequest of
                RD.Loading ->
                    ( model, Effect.none )

                _ ->
                    case model.ressourceAction of
                        NoRessource ->
                            ( model, Effect.none )

                        NewRessource ressource ->
                            ( { model | saveRessourceRequest = RD.Loading }, Effect.fromCmd <| createRessource model.siret ressource )

                        DeleteRessource ressource ->
                            ( { model | saveRessourceRequest = RD.Loading }, Effect.fromCmd <| deleteRessource model.siret ressource )

                        EditRessource ressource ->
                            ( { model | saveRessourceRequest = RD.Loading }, Effect.fromCmd <| editRessource model.siret ressource )

        ReceivedSaveRessource response ->
            case model.saveRessourceRequest of
                RD.Loading ->
                    case model.ressourceAction of
                        NoRessource ->
                            model
                                |> Effect.withNone

                        _ ->
                            case response of
                                RD.Success _ ->
                                    { model | etablissementWithExtraInfo = response |> RD.map Ok, saveRessourceRequest = response, ressourceAction = NoRessource }
                                        |> Effect.withNone

                                _ ->
                                    { model | saveRessourceRequest = response }
                                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        ReceivedSuivis resp ->
            { model | suivisParAutres = resp }
                |> Effect.withNone

        ToggleSuivisEquipesModal show ->
            { model | suivisParAutresModal = show }
                |> Effect.withNone

        ClickedPrintToPdf ->
            ( model
            , Effect.fromCmd <|
                (Process.sleep 300
                    |> Task.map (\_ -> PrintToPdf)
                    |> Task.perform Basics.identity
                )
            )

        PrintToPdf ->
            model
                |> Effect.withShared
                    (Shared.PrintToPdf etablissementPageId)

        ClickedPartage ->
            { model
                | partageModal =
                    Just
                        { collegueId =
                            model.devecos
                                |> RD.map (List.filter (\collab -> collab.id /= User.id user))
                                |> RD.withDefault []
                                |> List.head
                                |> Maybe.map .id
                        , commentaire = ""
                        , request =
                            RD.NotAsked
                        }
            }
                |> Effect.withNone

        CancelPartage ->
            { model
                | partageModal =
                    Nothing
            }
                |> Effect.withNone

        UpdatedPartageDeveco collegueId ->
            { model
                | partageModal =
                    model.partageModal
                        |> Maybe.map
                            (\p ->
                                { p
                                    | collegueId = collegueId |> parseEntityId
                                    , request = RD.NotAsked
                                }
                            )
            }
                |> Effect.withNone

        UpdatedPartageCommentaire commentaire ->
            { model
                | partageModal =
                    model.partageModal
                        |> Maybe.map (\p -> { p | commentaire = commentaire })
            }
                |> Effect.withNone

        ConfirmPartage ->
            ( { model
                | partageModal =
                    model.partageModal
                        |> Maybe.map (\p -> { p | request = RD.Loading })
              }
            , model.partageModal
                |> Maybe.map (savePartage model.siret)
                |> Maybe.map Effect.fromCmd
                |> Maybe.withDefault Effect.none
            )

        ReceivedPartage response ->
            { model
                | partageModal =
                    model.partageModal
                        |> Maybe.map (\p -> { p | request = response })
            }
                |> Effect.withNone

        UpdatedRechercheContactExistant recherche ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | recherche = recherche
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm _ _ ->
                    ( model, Effect.none )

        RechercheContactExistant ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | resultats = RD.Loading
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.fromCmd <|
                        getPersonnes <|
                            Pages.Annuaire.filtersAndPaginationToQuery
                                ( { defaultFilters
                                    | recherche = contactForm.personneExistante.recherche
                                    , etablissementId = Just model.siret
                                    , elementsParPage = Just 5
                                  }
                                , defaultPagination
                                )
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm _ _ ->
                    ( model, Effect.none )

        ReceivedContactsExistants response ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | resultats = response
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm _ _ ->
                    ( model, Effect.none )

        SelectedContactExistant maybeContact ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | selectionne = maybeContact
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm _ _ ->
                    ( model, Effect.none )

        ToggleNouveauContactModeCreation modeCreation ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | modeCreation = modeCreation
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm _ _ ->
                    ( model, Effect.none )

        UpdateOnglets ( onglet, ongletSuivi ) ->
            let
                ( newModel, effect ) =
                    updateUrl ( onglet, ongletSuivi ) model
            in
            ( newModel
            , effect
            )

        SelectedAdresse ( apiAdresse, sMsg ) ->
            case model.geolocalisationContribution of
                Nothing ->
                    ( model, Effect.none )

                Just geolocalisationContribution ->
                    let
                        ( updatedSelect, selectCmd ) =
                            SingleSelectRemote.update sMsg selectAdresseConfig geolocalisationContribution.selectAdresse

                        ( emptySelect, emptySelectCmd ) =
                            SingleSelectRemote.setText "" selectAdresseConfig updatedSelect

                        coordonneesAdresse =
                            apiAdresse
                                |> .coordinates

                        newGeolocalisationContribution =
                            { geolocalisationContribution
                                | selectAdresse = emptySelect
                                , nouvelleGeolocalisation =
                                    coordonneesAdresse
                                        |> Maybe.map Just
                                        |> Maybe.withDefault geolocalisationContribution.nouvelleGeolocalisation
                                , nouvelleGeolocalisationChamp =
                                    coordonneesAdresse
                                        |> Maybe.map pointToString
                                        |> Maybe.withDefault geolocalisationContribution.nouvelleGeolocalisationChamp
                                , zoom = Just 16
                            }
                    in
                    ( { model | geolocalisationContribution = Just newGeolocalisationContribution }
                    , Effect.batch [ Effect.fromCmd selectCmd, Effect.fromCmd emptySelectCmd ]
                    )

        UpdatedSelectAdresse sMsg ->
            case model.geolocalisationContribution of
                Nothing ->
                    ( model, Effect.none )

                Just geolocalisationContribution ->
                    let
                        ( updatedSelect, selectCmd ) =
                            SingleSelectRemote.update sMsg selectAdresseConfig geolocalisationContribution.selectAdresse

                        newGeolocalisationContribution =
                            { geolocalisationContribution | selectAdresse = updatedSelect }
                    in
                    ( { model | geolocalisationContribution = Just newGeolocalisationContribution }
                    , Effect.fromCmd selectCmd
                    )

        ToggleVoirFiliales ->
            ( { model | voirToutesFiliales = not model.voirToutesFiliales }
            , Effect.none
            )

        ToggleVoirDetentions ->
            ( { model | voirToutesDetentions = not model.voirToutesDetentions }
            , Effect.none
            )

        ToggleVoirBeneficiaires ->
            ( { model | voirTousBeneficiaires = not model.voirTousBeneficiaires }
            , Effect.none
            )

        ToggleVoirMandatairesPPhysiques ->
            ( { model | voirTousMandatairesPPhysiques = not model.voirTousMandatairesPPhysiques }
            , Effect.none
            )

        ToggleVoirMandatairesPMorales ->
            ( { model | voirTousMandatairesPMorales = not model.voirTousMandatairesPMorales }
            , Effect.none
            )

        ClickedVoirParticipations ->
            let
                cmd =
                    case model.entrepriseParticipations of
                        RD.NotAsked ->
                            getEntrepriseParticipations model.siret

                        RD.Failure _ ->
                            getEntrepriseParticipations model.siret

                        _ ->
                            Cmd.none
            in
            ( { model | entrepriseParticipationsVoir = True }
            , Effect.fromCmd cmd
            )

        ReceivedParticipations entrepriseParticipations ->
            ( { model | entrepriseParticipations = entrepriseParticipations }
            , Effect.none
            )

        ClickedFermerParticipations ->
            ( { model | entrepriseParticipationsVoir = False }
            , Effect.none
            )


etablissementPageId : String
etablissementPageId =
    "etablissement-print"


view : Shared.User -> Shared.Shared -> Model -> View Msg
view user { now, timezone } model =
    let
        title =
            case model.etablissementWithExtraInfo of
                RD.Success (Ok { etablissement }) ->
                    "Fiche de " ++ etablissement.nomAffichage

                _ ->
                    "Fiche de " ++ model.siret
    in
    { title = UI.Layout.pageTitle <| title
    , body = body timezone now user model
    , route = Route.Etablissement ( Nothing, model.siret )
    }


body : Zone -> Posix -> Shared.User -> Model -> List (Html Msg)
body timezone now user model =
    [ div [ class "flex flex-col p-2" ]
        [ RD.withDefault nothing <| RD.map bandeauSuivisAutres <| model.suivisParAutres
        , div [ class "flex flex-row justify-between" ]
            [ DSFR.Button.new
                { label = "Retour"
                , onClick = Just <| SharedMsg <| Shared.goBack
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.arrowLeftLine
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withAttrs [ class "text-underline" ]
                |> DSFR.Button.view
            , case model.etablissementWithExtraInfo of
                RD.Success (Ok { equipeEtablissement }) ->
                    case equipeEtablissement.modificationAuteurDate of
                        Nothing ->
                            nothing

                        Just ( auteurModification, dateModification ) ->
                            let
                                auteur =
                                    if auteurModification == "" then
                                        ""

                                    else
                                        " par " ++ auteurModification
                            in
                            div [ Typo.textSm, class "flex flex-col !mb-0 p-2 text-right" ]
                                [ div [] [ text <| "Dernière modification par l'équipe le " ++ Lib.Date.formatShort timezone dateModification ++ auteur ]
                                ]

                _ ->
                    nothing
            ]
        , case model.etablissementWithExtraInfo of
            RD.Loading ->
                div [ class "p-6 sm:p-8 fr-card--white" ]
                    [ DSFR.Icons.System.refreshFill |> DSFR.Icons.iconLG
                    , "Chargement en cours..."
                        |> text
                    ]

            RD.Success (Ok etablissementWithExtraInfo) ->
                Lazy.lazy5 viewEtablissementBlocks timezone now user model etablissementWithExtraInfo

            RD.Success (Err err) ->
                div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ "Une erreur s'est produite\u{00A0}: "
                        ++ err
                        ++ "."
                        |> text
                    ]

            RD.Failure _ ->
                div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ "Une erreur s'est produite, veuillez recharger la page."
                        |> text
                    ]

            _ ->
                div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ "Une erreur s'est produite, veuillez recharger la page."
                        |> text
                    ]
        ]
    ]


bandeauSuivisAutres : List SuiviEquipe -> Html Msg
bandeauSuivisAutres suivis =
    case suivis of
        [] ->
            nothing

        _ ->
            div
                [ class "fr-notice fr-notice--info"
                ]
                [ div
                    [ class "fr-container"
                    ]
                    [ div
                        [ class "fr-notice__body"
                        ]
                        [ p
                            [ class "fr-notice__title"
                            ]
                            [ p []
                                [ text "Cet établissement est également accompagné"
                                , sup [ Html.Attributes.title Lib.Variables.hintBandeauSuivis ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ]
                                , text " par "
                                , text <| String.fromInt <| List.length suivis
                                , text " "
                                , text "autre"
                                , text <| plural <| List.length suivis
                                , text " "
                                , text "territoire"
                                , text <| plural <| List.length suivis
                                , text "."
                                , text " "
                                , DSFR.Button.new
                                    { onClick = Just <| ToggleSuivisEquipesModal True
                                    , label = "Voir la liste des territoires "
                                    }
                                    |> DSFR.Button.tertiaryNoOutline
                                    |> DSFR.Button.view
                                ]
                            ]
                        ]
                    ]
                ]


badgeGeolocalisation : EtablissementSimple -> Html msg
badgeGeolocalisation { geolocalisation, geolocalisationContribution, geolocalisationEquipe } =
    let
        ( icon, txt, color ) =
            if geolocalisationContribution /= Nothing then
                ( DSFR.Icons.Map.mapPin2Fill, "géolocalisé (modifié par " ++ (geolocalisationEquipe |> Maybe.withDefault "") ++ ")", "blue-text" )

            else if geolocalisation /= Nothing then
                ( DSFR.Icons.Map.mapPin2Line, "géolocalisé", "blue-text" )

            else
                ( DSFR.Icons.Map.mapPin2Line, "non géolocalisé", "red-text" )
    in
    div [ Typo.textSm, class "!mb-0" ]
        [ span [ class color ] [ DSFR.Icons.iconSM icon ]
        , text " "
        , text <| "Établissement " ++ txt
        ]


viewEtablissementHeader : Bool -> WebData (Maybe Date) -> WebData () -> WebData () -> EtablissementWithExtraInfo -> Html Msg
viewEtablissementHeader forPrint signaleFermeRequest modifierEnseigneRequest modifierGeolocalisationRequest { etablissement, equipeEtablissement, portefeuilleInfos } =
    let
        { createursLies } =
            equipeEtablissement

        { adresse, etatAdministratif, clotureDateContribution, siege, france, procedure } =
            etablissement
    in
    div [ class "flex flex-row justify-between items-start fr-card--white p-8" ]
        [ div [ class "flex flex-col" ]
            [ h1 [ Typo.textBold, Typo.fr_h3, class "!mb-0" ] [ DSFR.Icons.iconLG UI.Entite.iconeEtablissement, text "\u{00A0}", text <| .nomAffichage <| etablissement ]
            , div [ Typo.textBold, Typo.textSm, class "!mb-0" ] [ text <| adresse ]
            , badgeGeolocalisation etablissement
            ]
        , div [ class "flex flex-col gap-2" ]
            [ div [ class "flex flex-row gap-4 justify-end items-center" ]
                [ etatAdministratif
                    |> Just
                    |> UI.Entite.badgeInactif (not <| Nothing == clotureDateContribution)
                , UI.Entite.badgeFrance france
                , UI.Entite.badgeSiege <|
                    siege
                , UI.Entite.badgeProcedure True <|
                    procedure
                , UI.Entite.badgeAncienCreateur createursLies
                , UI.Entite.iconesPortefeuilleInfos portefeuilleInfos
                , viewIf (not forPrint) <|
                    UI.Etablissement.iconeFavoris { toggleFavorite = Just <| \_ -> ToggleFavorite }
                        etablissement
                        portefeuilleInfos.favori
                , viewIf (not forPrint) <|
                    DSFR.Button.dropdownSelector
                        { label = text "Actions"
                        , hint = Just "Actions supplémentaires"
                        , id = "etablissement-actions"
                        }
                    <|
                        [ DSFR.Button.new
                            { onClick = Just <| ToggleSignaleFerme <| Nothing == clotureDateContribution
                            , label =
                                if Nothing == clotureDateContribution then
                                    "Signaler l'établissement fermé"

                                else
                                    "Signaler l'établissement actif"
                            }
                            |> DSFR.Button.withDisabled (signaleFermeRequest == RD.Loading || etatAdministratif == Inactif)
                            |> DSFR.Button.leftIcon DSFR.Icons.Buildings.hotelLine
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.view
                        , DSFR.Button.new
                            { onClick = Just <| ToggleEnseigneContribution
                            , label = "Modifier le nom d'établissement"
                            }
                            |> DSFR.Button.withDisabled (modifierEnseigneRequest == RD.Loading)
                            |> DSFR.Button.leftIcon DSFR.Icons.Design.editLine
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.view
                        , DSFR.Button.new
                            { onClick = Just <| ToggleGeolocalisationContribution
                            , label = "Modifier la géolocalisation"
                            }
                            |> DSFR.Button.withDisabled (modifierGeolocalisationRequest == RD.Loading)
                            |> DSFR.Button.leftIcon DSFR.Icons.Map.mapPin2Line
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.view
                        , DSFR.Button.new
                            { onClick = Just <| ClickedPrintToPdf
                            , label = "Exporter la fiche (PDF)"
                            }
                            |> DSFR.Button.leftIcon DSFR.Icons.Document.filePdfLine
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.view
                        , DSFR.Button.new
                            { onClick = Just <| ClickedPartage
                            , label = "Partager la fiche par e-mail"
                            }
                            |> DSFR.Button.leftIcon DSFR.Icons.Business.mailLine
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.view
                        ]
                ]
            ]
        ]


viewEtablissementBlocks : Zone -> Posix -> Shared.User -> Model -> EtablissementWithExtraInfo -> Html Msg
viewEtablissementBlocks timezone now user model etablissementWithExtraInfo =
    div [ class "flex flex-col gap-4 p-2" ]
        [ div [ DSFR.Grid.gridRow, id etablissementPageId, class "print-only" ]
            [ div [ DSFR.Grid.col12 ]
                [ viewPdf timezone now user model etablissementWithExtraInfo ]
            ]
        , Accessibility.map SuiviMsg <| Lazy.lazy8 UI.Suivi.viewSuivisModal timezone now user (Just False) model.devecos False etablissementWithExtraInfo.equipeEtablissement.demandes model.suiviModel
        , Lazy.lazy3 viewContactsModal model.siret model.saveContactRequest model.contactAction
        , Lazy.lazy2 viewRessourcesModal model.saveRessourceRequest model.ressourceAction
        , Lazy.lazy3 viewModifierEnseigneModal model.modifierEnseigneRequest model.etablissementWithExtraInfo model.enseigneContribution
        , Lazy.lazy5 viewModifierGeolocalisationModal user model.siret model.modifierGeolocalisationRequest model.etablissementWithExtraInfo model.geolocalisationContribution
        , Lazy.lazy2 viewSuivisModal model.suivisParAutresModal <| RD.withDefault [] <| model.suivisParAutres
        , Lazy.lazy3 viewPartageModal
            { noOp = NoOp
            , confirm = ConfirmPartage
            , cancel = CancelPartage
            , updateCollegue = UpdatedPartageDeveco
            , updateCommentaire = UpdatedPartageCommentaire
            }
            (model.devecos |> RD.withDefault [] |> List.filter .actif)
            model.partageModal
        , Lazy.lazy3 viewParticipationsModal (String.left 9 model.siret) model.entrepriseParticipationsVoir model.entrepriseParticipations
        , Lazy.lazy5 viewEtablissementHeader False model.signaleFermeRequest model.modifierEnseigneRequest model.modifierGeolocalisationRequest etablissementWithExtraInfo
        , DSFR.Tabs.new
            { changeTabMsg = stringToOnglet >> Maybe.withDefault OngletEtablissement >> ClickedChangementOnglet
            , tabs =
                [ { id = OngletEtablissement |> ongletToString
                  , title = OngletEtablissement |> ongletToDisplay
                  , icon = DSFR.Icons.Buildings.hotelLine
                  , content = Lazy.lazy3 viewOngletEtablissement False model etablissementWithExtraInfo
                  }
                , { id = OngletEntreprise |> ongletToString
                  , title = OngletEntreprise |> ongletToDisplay
                  , icon = DSFR.Icons.Buildings.communityLine
                  , content =
                        viewOngletEntreprise
                            False
                            model.prochainId
                            timezone
                            now
                            model.entrepriseExercices
                            model.entrepriseBeneficiaires
                            model.voirToutesFiliales
                            model.voirToutesDetentions
                            model.voirTousBeneficiaires
                            model.voirTousMandatairesPPhysiques
                            model.voirTousMandatairesPMorales
                            model.entrepriseLiasseFiscale
                            model.etablissementMandataires
                            model.contactCreationMandataire
                            etablissementWithExtraInfo
                  }
                , { id = OngletSuivi |> ongletToString
                  , title = OngletSuivi |> ongletToDisplay
                  , icon = DSFR.Icons.Communication.discussLine
                  , content =
                        Accessibility.map SuiviMsg <|
                            UI.Suivi.viewOngletSuivis timezone
                                now
                                False
                                False
                                user
                                (transformationAlerte <|
                                    .createursLies <|
                                        .equipeEtablissement <|
                                            etablissementWithExtraInfo
                                )
                                etablissementWithExtraInfo.equipeEtablissement.echanges
                                etablissementWithExtraInfo.equipeEtablissement.demandes
                                etablissementWithExtraInfo.equipeEtablissement.rappels
                                Nothing
                                model.suiviModel
                  }
                ]
            }
            |> DSFR.Tabs.view (ongletToString model.onglet)
        ]


transformationAlerte : List EtablissementCreationLie -> Maybe (Html msg)
transformationAlerte etablissementCreationsLies =
    case etablissementCreationsLies of
        [] ->
            Nothing

        _ ->
            Just <|
                div [] <|
                    List.map
                        (\etablissementCreationLie ->
                            let
                                createurLien =
                                    Route.toUrl <|
                                        Route.Createur <|
                                            ( (\q ->
                                                if q == "" then
                                                    Nothing

                                                else
                                                    Just q
                                              )
                                              <|
                                                ongletsToQuery ( OngletSuivi, UI.Suivi.OngletEchanges )
                                            , etablissementCreationLie.id
                                            )
                            in
                            { title = Nothing
                            , description =
                                div []
                                    [ text "Créateur transformé en établissement\u{00A0}:"
                                    , text " "
                                    , Typo.link createurLien
                                        []
                                        [ text etablissementCreationLie.nom
                                        ]
                                    ]
                            }
                                |> DSFR.Alert.small
                                |> DSFR.Alert.alert Nothing DSFR.Alert.info
                        )
                    <|
                        etablissementCreationsLies


viewOngletEtablissement : Bool -> Model -> EtablissementWithExtraInfo -> Html Msg
viewOngletEtablissement forPrint model ({ etablissement, etablissementExtra, equipeEtablissement } as etablissementWithExtraInfo) =
    let
        { emaVariation } =
            etablissement

        { emms, ema, subventions } =
            etablissementExtra
    in
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col gap-4 p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy8 viewEtablissement forPrint model.editEtablissementEtiquettes model.zonages model.selectActivites model.selectLocalisations model.selectMots model.saveEtablissementEtiquettesRequest etablissementWithExtraInfo
            ]
        , div [ class "flex flex-col gap-4 p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy3 viewCommentaire model.saveEtablissementCommentaireRequest model.editEtablissementCommentaire equipeEtablissement.description
            ]
        , div [ class "flex flex-col gap-4 p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy3 viewRessources SetRessourceAction model.ressourceAction <|
                .ressources <|
                    equipeEtablissement
            ]
        , div [ class "p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy4 viewContacts (Just <| FonctionEtablissement { fonction = "" }) SetContactAction model.contactAction <|
                .contacts <|
                    equipeEtablissement
            ]
        , div [ class "p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy5 viewEffectifs forPrint model.effectifsPeriode ema emaVariation emms
            ]
        , div [ class "p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy viewLocaux equipeEtablissement.occupations
            ]
        , div [ class "p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy viewSubventions subventions
            ]
        , div [ class "p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy viewHistorique equipeEtablissement.historique
            ]
        ]


viewOngletEntreprise :
    Bool
    -> Int
    -> Zone
    -> Posix
    -> WebData (Maybe (List Exercice))
    -> WebData (Maybe (List BeneficiaireEffectif))
    -> Bool
    -> Bool
    -> Bool
    -> Bool
    -> Bool
    -> WebData (Maybe Data.LiasseFiscale)
    -> WebData (Maybe ( List MandatairePersonnePhysique, List MandatairePersonneMorale ))
    -> WebData EtablissementWithExtraInfo
    -> EtablissementWithExtraInfo
    -> Html Msg
viewOngletEntreprise forPrint prochainId timezone now entrepriseExercices entrepriseBeneficiaires voirToutesFiliales voirToutesDetentions voirTousBeneficiaires voirTousMandatairesPPhysiques voirTousMandatairesPMorales entrepriseLiasseFiscale etablissementMandataires contactCreationMandataire etablissementWithExtraInfo =
    let
        { equipeEtablissement, entreprise } =
            etablissementWithExtraInfo
    in
    case entreprise of
        Nothing ->
            div [ class "p-4 sm:p-8 fr-card--white" ]
                [ text "Impossible de récupérer les données de l'entreprise"
                ]

        Just entrepriseData ->
            div [ class "flex flex-col gap-8" ]
                [ div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ Lazy.lazy2 viewEntreprise entrepriseLiasseFiscale entrepriseData
                    ]
                , div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ Lazy.lazy5 viewInformationsFinancieres forPrint timezone now entrepriseExercices entrepriseData
                    ]
                , div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ Lazy.lazy5 viewParticipations forPrint voirToutesFiliales voirToutesDetentions entrepriseData.siren entrepriseLiasseFiscale
                    ]
                , div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ Lazy.lazy viewProceduresCollectives entrepriseData
                    ]
                , div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ Lazy.lazy5 viewBeneficiaires forPrint voirTousBeneficiaires prochainId contactCreationMandataire entrepriseBeneficiaires
                    ]
                , div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ Lazy.lazy6 viewMandataires forPrint voirTousMandatairesPPhysiques voirTousMandatairesPMorales prochainId contactCreationMandataire etablissementMandataires
                    ]
                , div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ Lazy.lazy viewEgapro entrepriseData
                    ]
                , div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ Lazy.lazy2 viewEtablissementsFreres entrepriseData.siren equipeEtablissement.freres
                    ]
                ]


viewPdf : Zone -> Posix -> Shared.User -> Model -> EtablissementWithExtraInfo -> Html Msg
viewPdf timezone now user model etablissementWithExtraInfo =
    let
        { createursLies, echanges, demandes, rappels } =
            etablissementWithExtraInfo
                |> .equipeEtablissement
                |> (\eqEtab ->
                        { createursLies = eqEtab.createursLies
                        , echanges = eqEtab.echanges
                        , demandes = eqEtab.demandes
                        , rappels = eqEtab.rappels
                        }
                   )
    in
    div []
        [ Lazy.lazy5 viewEtablissementHeader True model.signaleFermeRequest model.modifierEnseigneRequest model.modifierGeolocalisationRequest etablissementWithExtraInfo
        , Lazy.lazy3 viewOngletEtablissement True model etablissementWithExtraInfo
        , viewOngletEntreprise
            True
            model.prochainId
            timezone
            now
            model.entrepriseExercices
            model.entrepriseBeneficiaires
            True
            True
            True
            True
            True
            model.entrepriseLiasseFiscale
            model.etablissementMandataires
            model.contactCreationMandataire
            etablissementWithExtraInfo
        , Accessibility.map SuiviMsg <|
            UI.Suivi.viewOngletSuivis timezone
                now
                True
                False
                user
                (transformationAlerte <|
                    createursLies
                )
                echanges
                demandes
                rappels
                Nothing
                model.suiviModel
        ]


viewEntreprise : WebData (Maybe Data.LiasseFiscale) -> EntrepriseData -> Html Msg
viewEntreprise liasseFiscale { siren, entrepriseNom, siegeAdresse, active, categorieJuridique, formeJuridique, activiteNaf, categorieNaf, categorieEntreprise, effectifInsee, dateCreationEntreprise, etablissementsActifs, effectifTotalTerritoire } =
    let
        liasse =
            liasseFiscale
                |> RD.withDefault Nothing
    in
    div [ class "flex flex-col gap-4" ]
        [ div [ class "flex flex-col" ]
            [ h3 [ Typo.fr_h4 ] [ text "Informations générales" ]
            , Lib.UI.infoLine Nothing "Nom" <| entrepriseNom
            , Lib.UI.infoLine Nothing "SIREN" <| siren
            , Lib.UI.infoLine Nothing "Adresse du siège" <| siegeAdresse
            , Lib.UI.infoLine Nothing "En activité" <|
                if active then
                    "Oui"

                else
                    "Non"
            , Lib.UI.infoLineHtml Nothing
                "Catégorie"
                (Maybe.withDefault "-" categorieNaf)
                (span []
                    [ text (Maybe.withDefault "-" categorieNaf)
                    , viewMaybe (\_ -> text " ") categorieNaf
                    ]
                )
            , Lib.UI.infoLineHtml Nothing
                "Activité NAF"
                (Maybe.withDefault "Inconnu" activiteNaf)
                (span []
                    [ text (Maybe.withDefault "-" activiteNaf)
                    , viewMaybe (\_ -> text " ") activiteNaf
                    ]
                )
            , Lib.UI.infoLine Nothing "Date de création" <|
                Maybe.withDefault "-" <|
                    Maybe.map Lib.Date.formatDateShort <|
                        dateCreationEntreprise
            , Lib.UI.infoLine Nothing "Catégorie juridique" <|
                Maybe.withDefault "Inconnu" <|
                    categorieJuridique
            , Lib.UI.infoLine Nothing "Forme juridique" <|
                Maybe.withDefault "Inconnu" <|
                    formeJuridique
            , Lib.UI.infoLine Nothing "Taille d'entreprise" <|
                entrepriseTypeToDisplay <|
                    categorieEntreprise
            , Lib.UI.infoLine Nothing "Effectif (API SIRENE - INSEE)" <| effectifInsee
            , Lib.UI.infoLine Nothing "Effectif moyen annuel sur le territoire (GIP MDS)" <|
                String.replace "." "," <|
                    Maybe.withDefault "Inconnu" <|
                        Maybe.map String.fromFloat <|
                            effectifTotalTerritoire
            , Lib.UI.infoLine Nothing "Établissements actifs" <|
                Maybe.withDefault "Aucun" <|
                    Maybe.map
                        (\{ france, territoire } ->
                            String.fromInt france ++ " (dont " ++ String.fromInt territoire ++ " sur le territoire)"
                        )
                    <|
                        etablissementsActifs
            , Lib.UI.infoLine Nothing "Détient des filiales" <|
                Maybe.withDefault "?" <|
                    Maybe.map
                        (\{ aFiliales } ->
                            case aFiliales of
                                Just True ->
                                    "Oui"

                                Just False ->
                                    "Non"

                                Nothing ->
                                    "?"
                        )
                    <|
                        liasse
            , Lib.UI.infoLineHtml Nothing
                "Est une filiale"
                (Maybe.withDefault "?" <|
                    Maybe.map
                        (\{ estDetenue, sirensMere } ->
                            case estDetenue of
                                Just True ->
                                    "Oui"
                                        ++ (case sirensMere of
                                                ( Nothing, Nothing ) ->
                                                    ""

                                                ( Just s1, Nothing ) ->
                                                    " (" ++ s1 ++ ")"

                                                ( Nothing, Just s2 ) ->
                                                    " (" ++ s2 ++ ")"

                                                ( Just s1, Just s2 ) ->
                                                    " (" ++ s1 ++ ", " ++ s2 ++ ")"
                                           )

                                Just False ->
                                    "Non"

                                Nothing ->
                                    "?"
                        )
                    <|
                        liasse
                )
                (Maybe.withDefault (text "?") <|
                    Maybe.map
                        (\{ estDetenue, sirensMere } ->
                            case estDetenue of
                                Just True ->
                                    span [] <|
                                        (::) (text "Oui") <|
                                            case sirensMere of
                                                ( Nothing, Nothing ) ->
                                                    []

                                                ( Just s1, Nothing ) ->
                                                    [ text " ("
                                                    , Typo.externalLink (lienVersRechercheSiren s1) [] [ text s1 ]
                                                    , text ")"
                                                    ]

                                                ( Nothing, Just s2 ) ->
                                                    [ text " ("
                                                    , Typo.externalLink (lienVersRechercheSiren s2) [] [ text s2 ]
                                                    , text ")"
                                                    ]

                                                ( Just s1, Just s2 ) ->
                                                    [ text " ("
                                                    , Typo.externalLink (lienVersRechercheSiren s1) [] [ text s1 ]
                                                    , text ", "
                                                    , Typo.externalLink (lienVersRechercheSiren s2) [] [ text s2 ]
                                                    , text ")"
                                                    ]

                                Just False ->
                                    text "Non"

                                Nothing ->
                                    text "?"
                        )
                    <|
                        liasse
                )
            ]
        ]



-- (Maybe.withDefault ("Aucune filiale" |> text |> List.singleton) <|
--             Maybe.map (List.map (\filiale -> filiale.siren |> Maybe.withDefault "SIREN inconnu" |> text)) <|
--                 filiales
--         )


indexeParAnnee : List { a | annee : comparable } -> Dict comparable { a | annee : comparable }
indexeParAnnee list =
    list
        |> List.foldl
            (\({ annee } as exercice) result ->
                Dict.insert annee exercice result
            )
            Dict.empty


viewInformationsFinancieres : Bool -> Zone -> Posix -> WebData (Maybe (List Exercice)) -> EntrepriseData -> Html Msg
viewInformationsFinancieres forPrint timezone now exercices { inpi, inpiCaVariation } =
    let
        dates =
            Time.toYear timezone now
                |> List.repeat 3
                |> List.indexedMap (\i y -> y - i - 1)
                |> List.map String.fromInt
    in
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col justify-between gap-2" ]
            [ h3 [ Typo.fr_h4, class "!mb-0" ] [ text "Informations financières" ]
            , div [ class "flex flex-col gap-8" ]
                [ div [ class "flex flex-col gap-2" ]
                    [ DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = True } (text "SOURCE\u{00A0}: API Entreprise - DGFIP")
                        |> DSFR.Badge.badgeMD
                    , viewExercices forPrint dates exercices
                    ]
                , div [ class "flex flex-col gap-2" ]
                    [ DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = True } (text "SOURCE\u{00A0}: BCE - INPI")
                        |> DSFR.Badge.badgeMD
                    , inpiCaVariation
                        |> Maybe.map
                            (\var ->
                                div [ class "flex flex-row gap-2" ]
                                    [ div []
                                        [ text "Variation\u{00A0}: "
                                        ]
                                    , UI.Etablissement.iconeVariationCa <| Just var
                                    , div []
                                        [ span [ Typo.textBold ] [ text <| Lib.UI.formatFloatWithThousandSpacing <| var ]
                                        , text "\u{00A0}%"
                                        ]
                                    ]
                            )
                        |> Maybe.withDefault (div [] [ text "Variation\u{00A0}: -" ])
                    , viewExercicesInpi dates inpi
                    ]
                ]
            ]
        ]


viewExercices : Bool -> List String -> WebData (Maybe (List Exercice)) -> Html msg
viewExercices forPrint dates data =
    case data of
        RD.Success exercices ->
            case exercices of
                Nothing ->
                    div [ class "italic" ] [ text "Impossible de récupérer les données." ]

                Just [] ->
                    div [ class "italic" ] [ text "Aucune donnée disponible." ]

                Just list ->
                    let
                        exercicesIndexes =
                            indexeParAnnee list

                        headers : List ( String, String )
                        headers =
                            dates
                                |> List.map (\key -> ( key, key ))
                                |> List.reverse
                                |> (::) ( "", "Date fin d'exercice" )

                        rows : List ( String, Dict String String )
                        rows =
                            [ ( "Date de clôture", Dict.map (\_ exercice -> exercice.date) exercicesIndexes )
                            , ( "Chiffre d'affaires (€)", Dict.map (\_ exercice -> exercice.ca) exercicesIndexes )
                            ]
                    in
                    viewExerciceTable
                        "tableau-exercices"
                        (text "Source\u{00A0}: DGFIP")
                        headers
                        rows

        RD.Failure _ ->
            div [ class "italic" ] [ text "Impossible de récupérer les données." ]

        _ ->
            if forPrint then
                div [ class "italic" ] [ text "Aucune donnée disponible." ]

            else
                div [ class "italic" ] [ text "Récupération des informations en cours." ]


viewExercicesInpi : List String -> List ExerciceInpi -> Html msg
viewExercicesInpi dates inpis =
    case inpis of
        [] ->
            div [ class "italic" ] [ text "Aucune donnée disponible." ]

        _ ->
            let
                inpiIndexes =
                    indexeParAnnee inpis

                headers : List ( String, String )
                headers =
                    dates
                        |> List.map (\key -> ( key, key ))
                        |> List.reverse
                        |> (::) ( "", "Date fin d'exercice" )

                rows : List ( String, Dict String String )
                rows =
                    [ ( "Chiffre d'affaires (€)", Dict.map (\_ inpi -> inpi.ca) inpiIndexes )
                    , ( "Marge brute (€)", Dict.map (\_ inpi -> inpi.marge) inpiIndexes )
                    , ( "EBE (€)", Dict.map (\_ inpi -> inpi.ebe) inpiIndexes )
                    , ( "Résultat net (€)", Dict.map (\_ inpi -> inpi.resultatNet) inpiIndexes )
                    ]
            in
            viewExerciceTable
                "tableau-exercices-inpi"
                (text "Source\u{00A0}: INPI")
                headers
                rows


viewExerciceTable : String -> Html msg -> List ( String, String ) -> List ( String, Dict String String ) -> Html msg
viewExerciceTable id caption headers rows =
    DSFR.Table.table
        { id = id
        , caption = caption
        , headers = headers
        , rows = rows
        , toHeader =
            \( key, label ) ->
                Html.span []
                    [ div
                        [ class <|
                            if key == "" then
                                "text-left"

                            else
                                "text-right"
                        ]
                        [ text label ]
                    ]
        , toRowId = Tuple.first
        , toCell =
            \( key, _ ) ( label, dict ) ->
                if key == "" then
                    text label

                else if label == "Date de clôture" then
                    dict
                        |> Dict.get key
                        |> Maybe.map text
                        |> Maybe.withDefault (text "-")
                        |> (\t -> div [ class "text-right" ] [ t ])

                else
                    dict
                        |> Dict.get key
                        |> Maybe.map
                            (\valeur ->
                                Html.node "currency-display"
                                    [ Html.Attributes.attribute "culture-code" "fr-FR"
                                    , Html.Attributes.attribute "currency" "EUR"
                                    , Html.Attributes.attribute "amount" valeur
                                    ]
                                    []
                            )
                        |> Maybe.withDefault (text "-")
                        |> (\t -> div [ class "text-right" ] [ t ])
        }
        |> DSFR.Table.withContainerAttrs [ class "!mb-0" ]
        -- la classe a pas trop d'effet, à creuser à l'occasion
        |> DSFR.Table.withColAttrs [ ( Just 1, [ class "w-[10em]" ] ), ( Just <| List.length headers - 1, [ class "w-[6em]" ] ) ]
        |> DSFR.Table.noBorders
        |> DSFR.Table.captionHidden
        |> DSFR.Table.fixed
        |> DSFR.Table.view


viewEgapro : EntrepriseData -> Html Msg
viewEgapro { egapro } =
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-row justify-between items-end gap-2" ]
            [ h3 [ Typo.fr_h4, class "!mb-0" ]
                [ text "Index de l'égalité professionnelle (Egapro)"
                ]
            , Typo.externalLink "https://travail-emploi.gouv.fr/index-de-legalite-professionnelle-calcul-et-questionsreponses" [] [ text "Comprendre le calcul de ces chiffres" ]
            ]
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col12, DSFR.Grid.colLg4 ]
                [ DSFR.Tile.vertical
                    { title =
                        span [ class "text-[4rem] leading-[5rem]" ] <|
                            List.singleton <|
                                text <|
                                    Maybe.withDefault "NC" <|
                                        Maybe.map String.fromInt <|
                                            Maybe.andThen .note <|
                                                egapro
                    , description =
                        Just <|
                            span [ Typo.textBold ]
                                [ text <|
                                    Maybe.withDefault "NC" <|
                                        Maybe.map String.fromInt <|
                                            Maybe.map ((+) 1) <|
                                                Maybe.map .annee <|
                                                    egapro
                                ]
                    , link = Nothing
                    }
                    |> DSFR.Tile.withDetails
                        (Maybe.withDefault "Année inconnue" <|
                            Maybe.map (\annee -> "Données " ++ String.fromInt annee) <|
                                Maybe.map .annee <|
                                    egapro
                        )
                    |> DSFR.Tile.withBadges [ "Note" ]
                    |> DSFR.Tile.withNoArrow
                    |> DSFR.Tile.withAttrs [ class "compact-tile" ]
                    |> DSFR.Tile.view
                ]
            , div [ DSFR.Grid.col12, DSFR.Grid.colLg8 ]
                [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ div [ DSFR.Grid.col12, DSFR.Grid.colLg6 ]
                        [ DSFR.Tile.vertical
                            { title =
                                span [ class "text-[2rem] leading-[3rem]" ] <|
                                    List.singleton <|
                                        text <|
                                            Maybe.withDefault "NC" <|
                                                Maybe.map String.fromInt <|
                                                    Maybe.andThen .ecartRemunerations <|
                                                        egapro
                            , description = Nothing
                            , link = Nothing
                            }
                            |> DSFR.Tile.withBadges [ "Écart rémunérations" ]
                            |> DSFR.Tile.withNoArrow
                            |> DSFR.Tile.withAttrs [ class "compact-tile" ]
                            |> DSFR.Tile.view
                        ]
                    , div [ DSFR.Grid.col12, DSFR.Grid.colLg6 ]
                        [ DSFR.Tile.vertical
                            { title =
                                span [ class "text-[2rem] leading-[3rem]" ] <|
                                    List.singleton <|
                                        text <|
                                            Maybe.withDefault "NC" <|
                                                Maybe.map String.fromInt <|
                                                    Maybe.andThen .ecartAugmentations <|
                                                        egapro
                            , description = Nothing
                            , link = Nothing
                            }
                            |> DSFR.Tile.withBadges [ "Écart taux d'augmentations" ]
                            |> DSFR.Tile.withNoArrow
                            |> DSFR.Tile.withAttrs [ class "compact-tile" ]
                            |> DSFR.Tile.view
                        ]
                    ]
                , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ div [ DSFR.Grid.col12, DSFR.Grid.colLg6 ]
                        [ DSFR.Tile.vertical
                            { title =
                                span [ class "text-[2rem] leading-[3rem]" ] <|
                                    List.singleton <|
                                        text <|
                                            Maybe.withDefault "NC" <|
                                                Maybe.map String.fromInt <|
                                                    Maybe.andThen .retourMaternite <|
                                                        egapro
                            , description = Nothing
                            , link = Nothing
                            }
                            |> DSFR.Tile.withBadges [ "Retour congé maternité" ]
                            |> DSFR.Tile.withNoArrow
                            |> DSFR.Tile.withAttrs [ class "compact-tile" ]
                            |> DSFR.Tile.view
                        ]
                    , div [ DSFR.Grid.col12, DSFR.Grid.colLg6 ]
                        [ DSFR.Tile.vertical
                            { title =
                                span [ class "text-[2rem] leading-[3rem]" ] <|
                                    List.singleton <|
                                        text <|
                                            Maybe.withDefault "NC" <|
                                                Maybe.map String.fromInt <|
                                                    Maybe.andThen .hautesRemunerations <|
                                                        egapro
                            , description = Nothing
                            , link = Nothing
                            }
                            |> DSFR.Tile.withBadges [ "Hautes rémunérations" ]
                            |> DSFR.Tile.withNoArrow
                            |> DSFR.Tile.withAttrs [ class "compact-tile" ]
                            |> DSFR.Tile.view
                        ]
                    ]
                ]
            ]
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col12, DSFR.Grid.colLg6 ]
                [ DSFR.Tile.vertical
                    { title =
                        span [ class "text-[2rem] leading-[3rem]" ] <|
                            List.singleton <|
                                text <|
                                    Maybe.withDefault "NC" <|
                                        Maybe.map (\s -> String.replace "." "," s ++ "%") <|
                                            Maybe.map String.fromFloat <|
                                                Maybe.andThen .cadres <|
                                                    egapro
                    , description = Just <| text "Femmes"
                    , link = Nothing
                    }
                    |> DSFR.Tile.withBadges [ "Cadres dirigeantes" ]
                    |> DSFR.Tile.withNoArrow
                    |> DSFR.Tile.withAttrs [ class "compact-tile" ]
                    |> DSFR.Tile.view
                ]
            , div [ DSFR.Grid.col12, DSFR.Grid.colLg6 ]
                [ DSFR.Tile.vertical
                    { title =
                        span [ class "text-[2rem] leading-[3rem]" ] <|
                            List.singleton <|
                                text <|
                                    Maybe.withDefault "NC" <|
                                        Maybe.map (\s -> String.replace "." "," s ++ "%") <|
                                            Maybe.map String.fromFloat <|
                                                Maybe.andThen .instances <|
                                                    egapro
                    , description = Just <| text "Femmes"
                    , link = Nothing
                    }
                    |> DSFR.Tile.withBadges [ "Membres instances dirigeantes" ]
                    |> DSFR.Tile.withNoArrow
                    |> DSFR.Tile.withAttrs [ class "compact-tile" ]
                    |> DSFR.Tile.view
                ]
            ]
        ]


viewProceduresCollectives : EntrepriseData -> Html Msg
viewProceduresCollectives { procedures } =
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col justify-between gap-2" ]
            [ h3 [ Typo.fr_h4, class "!mb-0" ]
                [ text "Procédures collectives"
                ]
            , div [ class "flex flex-col gap-8" ]
                [ DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = True } (text "SOURCE\u{00A0}: BODACC")
                    |> DSFR.Badge.badgeMD
                ]
            ]
        , div [ class "flex flex-col gap-2" ]
            (div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col2 ] [ label [ class "font-bold" ] [ text "Date" ] ]
                , div [ DSFR.Grid.col4 ] [ label [ class "font-bold" ] [ text "Famille" ] ]
                , div [ DSFR.Grid.col6 ] [ label [ class "font-bold" ] [ text "Nature" ] ]
                ]
                :: hr [ class "!pb-1" ] []
                :: (if List.isEmpty procedures then
                        span [ class "text-center italic" ] [ text "Aucune procédure collective" ]

                    else
                        nothing
                   )
                :: (List.intersperse (hr [ class "!pb-1" ] []) <|
                        List.map viewStaticProcedure procedures
                   )
            )
        ]


viewStaticProcedure : ProcedureCollective -> Html msg
viewStaticProcedure procedure =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ text <|
                withEmptyAs "-" <|
                    procedure.date
            ]
        , div [ DSFR.Grid.col4, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ text <|
                withEmptyAs "-" <|
                    procedure.famille
            ]
        , div [ DSFR.Grid.col6, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ text <|
                withEmptyAs "-" <|
                    procedure.nature
            ]
        ]


viewBeneficiaires : Bool -> Bool -> Int -> WebData EtablissementWithExtraInfo -> WebData (Maybe (List BeneficiaireEffectif)) -> Html Msg
viewBeneficiaires forPrint voirTousBeneficiaires prochainId contactRequest data =
    div [ class "flex flex-col gap-4" ]
        (case data of
            RD.Success beneficiaires ->
                case beneficiaires of
                    Nothing ->
                        [ div [ class "flex flex-col gap-2" ]
                            [ h3 [ Typo.fr_h4, class "!mb-0" ] [ text "Bénéficiaires" ]
                            , DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = True } (text "SOURCE\u{00A0}: API Entreprise - INPI/RNE")
                                |> DSFR.Badge.badgeMD
                            , div [ class "italic" ] [ text "Impossible de récupérer les données." ]
                            ]
                        ]

                    Just beneficiairesEffectifs ->
                        [ div [ class "flex flex-col" ] <|
                            h3 [ Typo.fr_h4, class "!mb-2" ] [ text "Bénéficiaires", text " (", text <| String.fromInt <| List.length beneficiairesEffectifs, text ")" ]
                                :: (DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = True } (text "SOURCE\u{00A0}: API Entreprise - INPI/RNE")
                                        |> DSFR.Badge.badgeMD
                                   )
                                :: div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "!mt-2" ]
                                    [ div [ DSFR.Grid.col2 ] [ UI.Contact.columnWrapper <| label [ class "font-bold" ] [ text "Nom" ] ]
                                    , div [ DSFR.Grid.col2 ] [ UI.Contact.columnWrapper <| label [ class "font-bold" ] [ text "Nom d'usage" ] ]
                                    , div [ DSFR.Grid.col2 ] [ UI.Contact.columnWrapper <| label [ class "font-bold" ] [ text "Prénom(s)" ] ]
                                    , div [ DSFR.Grid.col2 ] [ UI.Contact.columnWrapper <| label [ class "font-bold" ] [ text "Date de naissance" ] ]
                                    , div [ DSFR.Grid.col2 ]
                                        [ div [ class "!px-[1rem] text-right", style "overflow-wrap" "break-word" ] <|
                                            List.singleton <|
                                                label [ class "font-bold" ]
                                                    [ text "Total parts"
                                                    , sup [ Html.Attributes.title hintBeneficiairesEffectifsTotalParts ]
                                                        [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ]
                                                    ]
                                        ]
                                    , div [ DSFR.Grid.col2 ] [ nothing ]
                                    ]
                                :: hr [ class "!pb-1" ] []
                                :: (if List.length beneficiairesEffectifs == 0 then
                                        span [ class "text-center italic" ] [ text "Aucun bénéficiaire effectif connu" ]

                                    else
                                        nothing
                                   )
                                :: (List.intersperse (hr [ class "!pb-1" ] []) <|
                                        avecVoirPlus voirTousBeneficiaires ToggleVoirBeneficiaires (viewBeneficiaire prochainId contactRequest) <|
                                            Just beneficiairesEffectifs
                                   )
                        ]

            RD.Failure _ ->
                [ div [ class "flex flex-col" ]
                    [ h3 [ Typo.fr_h4 ] [ text "Bénéficiaires" ]
                    , viewIf (not forPrint) <| div [ class "italic" ] [ text "Impossible de récupérer les données." ]
                    , viewIf forPrint <| span [ class "text-center italic" ] [ text "Aucun bénéficiaire effectif connu" ]
                    ]
                ]

            _ ->
                [ div [ class "flex flex-col" ]
                    [ h3 [ Typo.fr_h4 ] [ text "Bénéficiaires" ]
                    , viewIf (not forPrint) <| div [ class "italic" ] [ text "Récupération des données en cours..." ]
                    , viewIf forPrint <| span [ class "text-center italic" ] [ text "Aucun bénéficiaire effectif connu" ]
                    ]
                ]
        )


viewMandataires : Bool -> Bool -> Bool -> Int -> WebData EtablissementWithExtraInfo -> WebData (Maybe ( List MandatairePersonnePhysique, List MandatairePersonneMorale )) -> Html Msg
viewMandataires forPrint voirTousMandatairesPPhysiques voirTousMandatairesPMorales prochainId contactRequest data =
    div [ class "flex flex-col gap-4" ]
        (case data of
            RD.Success mandataires ->
                case mandataires of
                    Nothing ->
                        [ div [ class "flex flex-col gap-2" ]
                            [ h3 [ Typo.fr_h4, class "!mb-0" ] [ text "Mandataires" ]
                            , DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = True } (text "SOURCE\u{00A0}: API Entreprise - Infogreffe RCS")
                                |> DSFR.Badge.badgeMD
                            , div [ class "italic" ] [ text "Impossible de récupérer les données." ]
                            ]
                        ]

                    Just ( personnesPhysiques, personnesMorales ) ->
                        [ div [ class "flex flex-col gap-2" ] <|
                            h3 [ Typo.fr_h4, class "!mb-0" ] [ text "Mandataires", text " (", text <| String.fromInt <| List.length personnesPhysiques + List.length personnesMorales, text ")" ]
                                :: (DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = True } (text "SOURCE\u{00A0}: API Entreprise - Infogreffe RCS")
                                        |> DSFR.Badge.badgeMD
                                   )
                                :: div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                                    [ div [ DSFR.Grid.col ] [ nothing ]
                                    , div [ DSFR.Grid.col3 ] [ UI.Contact.columnWrapper <| label [ class "font-bold" ] [ text "Fonction" ] ]
                                    , div [ DSFR.Grid.col2 ] [ UI.Contact.columnWrapper <| label [ class "font-bold" ] [ text "Prénom" ] ]
                                    , div [ DSFR.Grid.col2 ] [ UI.Contact.columnWrapper <| label [ class "font-bold" ] [ text "Nom" ] ]
                                    , div [ DSFR.Grid.col2 ] [ UI.Contact.columnWrapper <| label [ class "font-bold" ] [ text "Date de naissance" ] ]
                                    , div [ DSFR.Grid.col2 ] [ nothing ]
                                    ]
                                :: hr [ class "!pb-1" ] []
                                :: (if List.length personnesPhysiques == 0 then
                                        span [ class "text-center italic" ] [ text "Aucun mandataire personne physique connu" ]

                                    else
                                        nothing
                                   )
                                :: (List.intersperse (hr [ class "!pb-1" ] []) <|
                                        avecVoirPlus voirTousMandatairesPPhysiques ToggleVoirMandatairesPPhysiques (viewPersonnePhysique prochainId contactRequest) <|
                                            Just personnesPhysiques
                                   )
                                ++ (div [ class "!pb-1" ] []
                                        :: div [ class "!pb-1" ] []
                                        :: div [ class "!pb-1" ] []
                                        :: div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                                            [ div [ DSFR.Grid.col ] [ UI.Contact.columnWrapper <| label [ class "font-bold" ] [ text "" ] ]
                                            , div [ DSFR.Grid.col3 ] [ UI.Contact.columnWrapper <| label [ class "font-bold" ] [ text "Fonction" ] ]
                                            , div [ DSFR.Grid.col4 ] [ UI.Contact.columnWrapper <| label [ class "font-bold" ] [ text "Nom" ] ]
                                            , div [ DSFR.Grid.col2 ] [ UI.Contact.columnWrapper <| label [ class "font-bold" ] [ text "SIREN" ] ]
                                            , div [ DSFR.Grid.col2 ] [ nothing ]
                                            ]
                                        :: hr [ class "!pb-1" ] []
                                        :: (if List.length personnesMorales == 0 then
                                                span [ class "text-center italic" ] [ text "Aucun mandataire personne morale connu" ]

                                            else
                                                nothing
                                           )
                                        :: (List.intersperse (hr [ class "!pb-1" ] []) <|
                                                avecVoirPlus voirTousMandatairesPMorales ToggleVoirMandatairesPMorales viewPersonneMorale <|
                                                    Just personnesMorales
                                           )
                                   )
                        ]

            RD.Failure _ ->
                [ div [ class "flex flex-col" ]
                    [ h3 [ Typo.fr_h4 ] [ text "Mandataires" ]
                    , viewIf (not forPrint) <| div [ class "italic" ] [ text "Impossible de récupérer les données." ]
                    , viewIf forPrint <| span [ class "text-center italic" ] [ text "Aucun mandataire personne physique connu" ]
                    ]
                ]

            _ ->
                [ div [ class "flex flex-col" ]
                    [ h3 [ Typo.fr_h4 ] [ text "Mandataires" ]
                    , viewIf (not forPrint) <| div [ class "italic" ] [ text "Récupération des données en cours..." ]
                    , viewIf forPrint <| span [ class "text-center italic" ] [ text "Aucun mandataire personne physique connu" ]
                    ]
                ]
        )


viewParticipations :
    Bool
    -> Bool
    -> Bool
    -> String
    -> WebData (Maybe Data.LiasseFiscale)
    -> Html Msg
viewParticipations forPrint voirToutesFiliales voirToutesDetentions siren entrepriseLiasseFiscale =
    div [ class "flex flex-col gap-8" ]
        (case entrepriseLiasseFiscale of
            RD.Success Nothing ->
                [ div [ class "flex flex-col gap-4" ]
                    [ div [ class "flex flex-row justify-between" ]
                        [ h3 [ Typo.fr_h4, class "!mb-2" ] [ text "Participations" ]
                        , DSFR.Button.new { onClick = Just ClickedVoirParticipations, label = "Voir les participations" }
                            |> DSFR.Button.leftIcon DSFR.Icons.System.eyeLine
                            |> DSFR.Button.secondary
                            |> DSFR.Button.view
                        ]
                    , DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = True } (text "SOURCE\u{00A0}: API Entreprise - DGFIP")
                        |> DSFR.Badge.badgeMD
                    , div [ class "italic" ] [ text "Impossible de récupérer les données." ]
                    ]
                ]

            RD.Success (Just { detentions, filiales }) ->
                [ div []
                    [ div [ class "flex flex-row justify-between" ]
                        [ h3 [ Typo.fr_h4, class "!mb-2" ] [ text "Participations" ]
                        , DSFR.Button.new { onClick = Just ClickedVoirParticipations, label = "Voir les participations" }
                            |> DSFR.Button.leftIcon DSFR.Icons.System.eyeLine
                            |> DSFR.Button.secondary
                            |> DSFR.Button.view
                        ]
                    , DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = True } (text "SOURCE\u{00A0}: API Entreprise - DGFIP")
                        |> DSFR.Badge.badgeMD
                    ]
                , div [ class "flex flex-col" ] <|
                    div [ class "flex flex-col" ]
                        [ div [ class "flex flex-row justify-between items-start" ]
                            [ h4 [ Typo.fr_h6, class "!mb-2" ]
                                [ text "Détentions"
                                , text " ("
                                , text <| Maybe.withDefault "?" <| Maybe.map String.fromInt <| Maybe.map List.length <| detentions
                                , text ")"
                                ]
                            , let
                                rechercheUrl =
                                    lienVersRechercheDetentions siren
                              in
                              Typo.externalLink rechercheUrl [] [ text "Voir les établissements sur mon territoire" ]
                            ]
                        , div [ Typo.textXs, class "!mb-2" ]
                            [ text "Cette entreprise est détenue par les entreprises suivantes"
                            ]
                        ]
                        :: div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "!mt-2" ]
                            [ div [ DSFR.Grid.col6 ] [ UI.Contact.columnWrapper <| label [ class "font-bold" ] [ text "Nom" ] ]
                            , div [ DSFR.Grid.col4 ]
                                [ div [ class "!px-[1rem] text-right", style "overflow-wrap" "break-word" ] <|
                                    List.singleton <|
                                        label [ class "font-bold" ] [ text "Parts" ]
                                ]
                            , div [ DSFR.Grid.col2 ]
                                [ div [ class "!px-[1rem] text-center", style "overflow-wrap" "break-word" ] <|
                                    List.singleton <|
                                        label [ class "font-bold" ] [ text "SIREN" ]
                                ]
                            ]
                        :: hr [ class "!pb-1" ] []
                        :: (if (detentions |> Maybe.withDefault [] |> List.length) == 0 then
                                span [ class "text-center italic" ] [ text "Aucune détention connue" ]

                            else
                                nothing
                           )
                        :: (List.intersperse (hr [ class "!pb-1" ] []) <|
                                avecVoirPlus voirToutesDetentions ToggleVoirDetentions viewDetention <|
                                    detentions
                           )
                , div [ class "flex flex-col" ] <|
                    div [ class "flex flex-col" ]
                        [ div [ class "flex flex-row justify-between items-start" ]
                            [ h4 [ Typo.fr_h6, class "!mb-2" ]
                                [ text "Filiales"
                                , text " ("
                                , text <| Maybe.withDefault "?" <| Maybe.map String.fromInt <| Maybe.map List.length <| filiales
                                , text ")"
                                ]
                            , let
                                rechercheUrl =
                                    lienVersRechercheFiliales siren
                              in
                              Typo.externalLink rechercheUrl [] [ text "Voir les établissements sur mon territoire" ]
                            ]
                        , div [ Typo.textXs, class "!mb-2" ]
                            [ text "Cette entreprise dispose de participations dans le capital des entreprises suivantes"
                            ]
                        ]
                        :: div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "!mt-2" ]
                            [ div [ DSFR.Grid.col8 ] [ UI.Contact.columnWrapper <| label [ class "font-bold" ] [ text "Nom" ] ]
                            , div [ DSFR.Grid.col2 ]
                                [ div [ class "!px-[1rem] text-right", style "overflow-wrap" "break-word" ] <|
                                    List.singleton <|
                                        label [ class "font-bold" ] [ text "Participation (%)" ]
                                ]
                            , div [ DSFR.Grid.col2 ]
                                [ div [ class "!px-[1rem] text-center", style "overflow-wrap" "break-word" ] <|
                                    List.singleton <|
                                        label [ class "font-bold" ] [ text "SIREN" ]
                                ]
                            ]
                        :: hr [ class "!pb-1" ] []
                        :: (if (filiales |> Maybe.withDefault [] |> List.length) == 0 then
                                span [ class "text-center italic" ] [ text "Aucune filiale connue" ]

                            else
                                nothing
                           )
                        :: (List.intersperse (hr [ class "!pb-1" ] []) <|
                                avecVoirPlus voirToutesFiliales ToggleVoirFiliales viewFiliale <|
                                    filiales
                           )
                ]

            RD.Failure _ ->
                [ div [ class "flex flex-col gap-4" ]
                    [ div [ class "flex flex-row justify-between" ]
                        [ h3 [ Typo.fr_h4, class "!mb-0" ] [ text "Participations" ]
                        , DSFR.Button.new { onClick = Just ClickedVoirParticipations, label = "Voir les participations" }
                            |> DSFR.Button.leftIcon DSFR.Icons.System.eyeLine
                            |> DSFR.Button.secondary
                            |> DSFR.Button.view
                        ]
                    , DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = True } (text "SOURCE\u{00A0}: API Entreprise - DGFIP")
                        |> DSFR.Badge.badgeMD
                    , viewIf (not forPrint) <| div [ class "italic" ] [ text "Impossible de récupérer les données." ]
                    , viewIf forPrint <| span [ class "text-center italic" ] [ text "Aucune participation connue" ]
                    ]
                ]

            _ ->
                [ div [ class "flex flex-col" ]
                    [ div [ class "flex flex-row justify-between" ]
                        [ h3 [ Typo.fr_h4, class "!mb-2" ] [ text "Participations" ]
                        , DSFR.Button.new { onClick = Just ClickedVoirParticipations, label = "Voir les participations" }
                            |> DSFR.Button.leftIcon DSFR.Icons.System.eyeLine
                            |> DSFR.Button.secondary
                            |> DSFR.Button.view
                        ]
                    , DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = True } (text "SOURCE\u{00A0}: API Entreprise - DGFIP")
                        |> DSFR.Badge.badgeMD
                    , viewIf (not forPrint) <| div [ class "italic" ] [ text "Récupération des données en cours..." ]
                    , viewIf forPrint <| span [ class "text-center italic" ] [ text "Aucune participation connue" ]
                    ]
                ]
        )


viewEffectifs : Bool -> EffectifsPeriode -> Maybe Effectif -> Maybe Float -> List Effectif -> Html Msg
viewEffectifs forPrint effectifsPeriode ema emaVariation emms =
    let
        effectifsPeriodes =
            (::) DouzeDerniersMois <|
                List.map Annee <|
                    List.map String.fromInt <|
                        List.reverse <|
                            List.sort <|
                                List.foldl
                                    (\{ annee } result ->
                                        if List.member annee result then
                                            result

                                        else
                                            annee :: result
                                    )
                                    []
                                <|
                                    emms

        indexeParAnneeMois list =
            list
                |> List.foldl
                    (\({ mois, annee } as effectif) result ->
                        let
                            key =
                                String.fromInt annee ++ "-" ++ String.padLeft 2 '0' (String.fromInt mois)
                        in
                        Dict.insert key effectif result
                    )
                    Dict.empty

        -- rangeons les effectifs par année-mois
        emmsIndexes =
            indexeParAnneeMois emms

        -- on fusionne les listes
        tousIndexes =
            emmsIndexes

        -- on garde les 12 dernières dates
        toutesDates =
            tousIndexes
                -- on récupère les dates utilisées
                |> Dict.keys
                |> List.Extra.unique
                |> List.sort

        dates =
            case effectifsPeriode of
                DouzeDerniersMois ->
                    toutesDates
                        -- on garde les 12 derniers mois
                        |> List.reverse
                        |> List.take 12
                        |> List.reverse

                Annee annee ->
                    toutesDates
                        |> List.filter (String.startsWith (annee ++ "-"))

        headers : List ( String, String )
        headers =
            (case effectifsPeriode of
                DouzeDerniersMois ->
                    dates
                        |> List.indexedMap
                            (\index key ->
                                tousIndexes
                                    |> Dict.get key
                                    |> Maybe.map
                                        (\{ mois, annee } ->
                                            let
                                                m =
                                                    Lib.Date.frenchMonthAbbreviation (Lib.Date.intToMonth mois)
                                            in
                                            if index == 0 || mois == 1 then
                                                m ++ " " ++ String.right 2 (String.fromInt annee)

                                            else
                                                m
                                        )
                                    |> Maybe.map (Tuple.pair key)
                            )
                        |> List.filterMap identity

                Annee a ->
                    List.range 1 12
                        |> List.map (\m -> { mois = m, annee = a })
                        |> List.indexedMap
                            (\index { mois, annee } ->
                                let
                                    key =
                                        annee ++ "-" ++ String.padLeft 2 '0' (String.fromInt mois)

                                    m =
                                        Lib.Date.frenchMonthAbbreviation (Lib.Date.intToMonth mois)

                                    label =
                                        if index == 0 || mois == 1 then
                                            m ++ " " ++ String.right 2 annee

                                        else
                                            m
                                in
                                ( key, label )
                            )
            )
                |> (::) ( "", "Date" )

        rows : List ( String, Dict String Effectif )
        rows =
            [ ( "EMM", emmsIndexes )
            ]
    in
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col gap-2" ]
            [ div [ class "flex flex-row justify-between" ]
                [ h3 [ Typo.fr_h4 ] [ text "Effectifs de l'établissement" ]
                , viewIf (not forPrint) <|
                    div [ class "flex flex-row justify-end" ]
                        [ span []
                            [ DSFR.Input.new
                                { value = effectifsPeriode |> effectifsPeriodeToString
                                , onInput =
                                    \optionValue ->
                                        effectifsPeriodes
                                            |> List.filter (\ep -> effectifsPeriodeToString ep == optionValue)
                                            |> List.head
                                            |> Maybe.withDefault effectifsPeriode
                                            |> SetEffectifsPeriode
                                , label = nothing
                                , name = "select-effectifs-periode"
                                }
                                |> DSFR.Input.select
                                    { options = effectifsPeriodes
                                    , toId = effectifsPeriodeToString
                                    , toLabel = effectifsPeriodeToDisplay >> text
                                    , toDisabled = Nothing
                                    }
                                |> DSFR.Input.view
                            ]
                        ]
                ]
            , DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = True } (text "SOURCE\u{00A0}: GIP MDS")
                |> DSFR.Badge.badgeMD
            , viewMaybe
                (\{ valeur, annee, mois } ->
                    let
                        moisNom =
                            mois
                                |> Maybe.map Lib.Date.intToMonth
                                |> Maybe.map Lib.Date.frenchMonthAbbreviation

                        moisLabel =
                            moisNom
                                |> Maybe.map (\n -> n ++ " " ++ annee)
                                |> Maybe.withDefault annee
                                |> (\s -> " (" ++ s ++ ")")

                        effectifsLabel =
                            "Effectif moyen annuel" ++ moisLabel ++ "\u{00A0}:"

                        val =
                            String.replace "." "," <|
                                String.fromFloat <|
                                    valeur
                    in
                    div []
                        [ text <| effectifsLabel
                        , text " "
                        , span [ Typo.textBold ] [ text val ]
                        ]
                )
                (case effectifsPeriode of
                    DouzeDerniersMois ->
                        ema
                            |> Maybe.map
                                (\e ->
                                    { valeur = e.valeur, annee = String.fromInt e.annee, mois = Just e.mois }
                                )

                    Annee a ->
                        let
                            v =
                                emmsIndexes
                                    |> Dict.foldl
                                        (\_ { annee, valeur } ({ total, nombre } as current) ->
                                            if String.fromInt annee == a then
                                                { total = total + valeur
                                                , nombre = nombre + 1
                                                }

                                            else
                                                current
                                        )
                                        { total = 0, nombre = 0 }
                                    |> (\{ total, nombre } ->
                                            if nombre == 0 then
                                                0

                                            else
                                                (total * 100 / nombre)
                                                    |> floor
                                                    |> (\i -> toFloat i / 100)
                                       )
                        in
                        Just { valeur = v, annee = a, mois = Nothing }
                )
            , emaVariation
                |> Maybe.map
                    (\var ->
                        div [ class "flex flex-row gap-2" ]
                            [ div []
                                [ text "Variation\u{00A0}: "
                                ]
                            , UI.Etablissement.iconeVariationEffectif <| Just var
                            , div []
                                [ span [ Typo.textBold ] [ text <| Lib.UI.formatFloatWithThousandSpacing <| var, text "\u{00A0}%" ]
                                ]
                            ]
                    )
                |> Maybe.withDefault (div [] [ text "Variation\u{00A0}: -" ])
            , if List.length emms == 0 then
                div [ class "italic" ] <| List.singleton <| text "Aucune donnée d'effectifs."

              else
                div []
                    [ DSFR.Table.table
                        { id = "tableau-effectifs"
                        , caption = text "Effectifs"
                        , headers = headers
                        , rows = rows
                        , toHeader =
                            \( _, label ) ->
                                Html.span []
                                    [ label |> text
                                    ]
                        , toRowId = Tuple.first
                        , toCell =
                            \( key, _ ) ( label, dict ) ->
                                if key == "" then
                                    text label

                                else
                                    dict
                                        |> Dict.get key
                                        |> Maybe.map (\{ valeur } -> String.replace "." "," <| String.fromFloat valeur)
                                        |> Maybe.withDefault "-"
                                        |> text
                        }
                        |> DSFR.Table.withContainerAttrs [ class "!mb-0" ]
                        |> DSFR.Table.noBorders
                        |> DSFR.Table.captionHidden
                        |> DSFR.Table.fixed
                        |> DSFR.Table.view
                    , div [ class "w-full italic text-right" ] [ p [ Typo.textSm ] [ text "EMM\u{00A0}: Effectif Moyen Mensuel, voir ", Typo.externalLink "https://entreprendre.service-public.fr/vosdroits/F24332" [] [ text "https://entreprendre.service-public.fr/vosdroits/F24332" ] ] ]
                    ]
            ]
        ]


viewLocaux : List LocalSimple -> Html Msg
viewLocaux locaux =
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col gap-2" ]
            [ h3 [ Typo.fr_h4, class "flex flex-row items-center gap-2" ]
                [ text <|
                    if List.length locaux > 1 then
                        "Locaux occupés"

                    else
                        "Local occupé"
                , viewIf (List.length locaux > 0) <|
                    span [ Typo.textMd, class "circled !mb-0" ]
                        [ text <| Lib.UI.formatIntWithThousandSpacing <| List.length locaux
                        ]
                ]
            , viewIf (List.length locaux == 0) <|
                div [ class "italic" ] <|
                    List.singleton <|
                        text "Aucun local occupé."
            , viewIf (List.length locaux > 0) <|
                div [ DSFR.Grid.gridRowGutters, DSFR.Grid.gridRow ] <|
                    List.map UI.Local.occupation <|
                        locaux
            ]
        ]


viewSubventions : List Data.Subvention -> Html msg
viewSubventions subventions =
    let
        headers =
            [ "type"
            , "annee"
            , "objet"
            , "montant"
            ]

        headerLabels =
            [ ( "type", "Type de subvention" )
            , ( "annee", "Année de versement" )
            , ( "objet", "Objet de la subvention" )
            , ( "montant", "Montant total" )
            ]
                |> Dict.fromList

        rows =
            subventions
                |> List.map
                    (\{ type_, montant, objet, annee } ->
                        [ ( "type", type_ )
                        , ( "annee", annee )
                        , ( "objet", objet )
                        , ( "montant", montant )
                        ]
                            |> Dict.fromList
                    )
    in
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col gap-2" ]
            [ h3 [ Typo.fr_h4, class "flex flex-row items-center gap-2 !mb-0" ]
                [ text "Subventions"
                , viewIf (List.length subventions > 0) <|
                    span [ Typo.textMd, class "circled !mb-0" ]
                        [ text <| Lib.UI.formatIntWithThousandSpacing <| List.length subventions
                        ]
                ]
            , DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = True } (text "SOURCE\u{00A0}: ANCT - DGDPV")
                |> DSFR.Badge.badgeMD
            , if List.length subventions == 0 then
                div [ class "italic" ] <| List.singleton <| text "Aucune donnée de subvention."

              else
                DSFR.Table.table
                    { id = "tableau-subventions"
                    , caption = text "Subventions"
                    , headers = headers
                    , rows = rows
                    , toHeader =
                        \key ->
                            Html.span []
                                [ text <|
                                    Maybe.withDefault "-" <|
                                        Dict.get key headerLabels
                                ]
                    , toRowId = Dict.get "annee" >> Maybe.withDefault "-"
                    , toCell =
                        \key dict ->
                            dict
                                |> Dict.get key
                                |> Maybe.withDefault "-"
                                |> text
                                |> List.singleton
                                |> div
                                    [ class <|
                                        if key == "objet" then
                                            ""

                                        else if key == "montant" then
                                            "text-right"

                                        else
                                            "text-center"
                                    ]
                    }
                    |> DSFR.Table.withThAttrs
                        (\key ->
                            List.singleton <|
                                class <|
                                    if key == "objet" then
                                        ""

                                    else if key == "montant" then
                                        "!text-right"

                                    else
                                        "!text-center"
                        )
                    |> DSFR.Table.withContainerAttrs [ class "!mb-0" ]
                    |> DSFR.Table.withColAttrs [ ( Just 1, [ class "w-[17%]" ] ), ( Just 1, [ class "w-[17%]" ] ), ( Just 1, [ class "w-[50%]" ] ), ( Just 1, [ class "w-[16%]" ] ) ]
                    |> DSFR.Table.noBorders
                    |> DSFR.Table.captionHidden
                    |> DSFR.Table.fixed
                    |> DSFR.Table.view
            ]
        ]


viewHistorique : EtablissementHistorique -> Html msg
viewHistorique { predecesseurs, successeurs } =
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col justify-between gap-2" ]
            [ h3 [ Typo.fr_h4, class "!mb-0" ] [ text "Historique de l'établissement" ]
            , DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = True } (text "SOURCE\u{00A0}: API SIRENE - INSEE")
                |> DSFR.Badge.badgeMD
            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col6 ]
                    [ h4 [ Typo.fr_h6 ] [ text "Prédécesseur" ]
                    , case predecesseurs of
                        [] ->
                            div [ class "italic" ] [ text "Cet établissement n'a pas de prédécesseur." ]

                        _ ->
                            predecesseurs
                                |> List.map (\( etab, inf ) -> carte { toggleFavorite = Nothing } etab inf True)
                                |> div [ DSFR.Grid.gridRow ]
                                |> List.singleton
                                |> div [ class "overflow-scroll max-h-[33em]" ]
                    ]
                , div [ DSFR.Grid.col6 ]
                    [ h4 [ Typo.fr_h6 ] [ text "Successeur" ]
                    , case successeurs of
                        [] ->
                            div [ class "italic" ] [ text "Cet établissement n'a pas de successeur." ]

                        _ ->
                            successeurs
                                |> List.map (\( etab, inf ) -> carte { toggleFavorite = Nothing } etab inf True)
                                |> div [ DSFR.Grid.gridRow ]
                                |> List.singleton
                                |> div [ class "overflow-scroll max-h-[33em]" ]
                    ]
                ]
            ]
        ]


viewEtablissementsFreres : String -> List EtablissementSimpleAvecPortefeuille -> Html msg
viewEtablissementsFreres siren freres =
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col justify-between gap-2" ]
            [ h3 [ Typo.fr_h4, class "!mb-0" ] [ text "Autres établissements de l'entreprise" ]
            , DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = True } (text "SOURCE\u{00A0}: API SIRENE - INSEE")
                |> DSFR.Badge.badgeMD
            , p [] <|
                case freres of
                    [] ->
                        [ span [ class "italic" ] [ text "Aucun autre établissement pour cette entreprise sur le territoire." ] ]

                    _ ->
                        [ text "Il existe "
                        , span [ Typo.textBold ] <|
                            List.singleton <|
                                if List.length freres > 5 then
                                    text "plus de 5"

                                else
                                    text <|
                                        String.fromInt <|
                                            List.length <|
                                                freres
                        , text " autre"
                        , text <| plural <| List.length freres
                        , text " établissement"
                        , text <| plural <| List.length freres
                        , text " sur votre territoire pour cette entreprise\u{00A0}:"
                        ]
            , case freres of
                [] ->
                    nothing

                _ ->
                    freres
                        |> List.take 5
                        |> List.map (\( etab, inf ) -> carte { toggleFavorite = Nothing } etab inf False)
                        |> appendPlaceholderIfNeeded siren freres
                        |> div [ DSFR.Grid.gridRow ]
                        |> List.singleton
                        |> div [ class "overflow-scroll max-h-[34em]" ]
            ]
        ]


appendPlaceholderIfNeeded : String -> List a -> List (Html msg) -> List (Html msg)
appendPlaceholderIfNeeded siren original list =
    if List.length original > 5 then
        list
            |> List.reverse
            |> (::) (carteLienVersRechercheSiren siren)
            |> List.reverse

    else
        list


lienVersRechercheSiren : String -> String
lienVersRechercheSiren siren =
    Route.toUrl <|
        Route.Etablissements <|
            (\q ->
                if q == "" then
                    Nothing

                else
                    Just q
            )
            <|
                filtersAndPaginationToQuery <|
                    ( { emptyFilters | recherche = siren, situationGeographique = Filters.TouteLaFrance }, defaultPagination )


lienVersRechercheFiliales : String -> String
lienVersRechercheFiliales siren =
    Route.toUrl <|
        Route.Etablissements <|
            (\q ->
                if q == "" then
                    Nothing

                else
                    Just q
            )
            <|
                filtersAndPaginationToQuery <|
                    ( { emptyFilters
                        | filiales =
                            [ { id = siren
                              , nom = ""
                              }
                            ]
                      }
                    , defaultPagination
                    )


lienVersRechercheDetentions : String -> String
lienVersRechercheDetentions siren =
    Route.toUrl <|
        Route.Etablissements <|
            (\q ->
                if q == "" then
                    Nothing

                else
                    Just q
            )
            <|
                filtersAndPaginationToQuery <|
                    ( { emptyFilters
                        | detentions =
                            [ { id = siren
                              , nom = ""
                              }
                            ]
                      }
                    , defaultPagination
                    )


voirLienExterneVersRehercheSiren : String -> Html msg
voirLienExterneVersRehercheSiren siren =
    Typo.externalLink (lienVersRechercheSiren siren) [] [ text siren ]


carteLienVersRechercheSiren : String -> Html msg
carteLienVersRechercheSiren siren =
    div [ DSFR.Grid.col4, class "p-2" ]
        [ div [ Typo.textSm, class "!mb-0 h-full" ]
            [ div
                [ class "flex flex-col overflow-y-auto fr-card--white p-2 border-2 h-full"
                , style "background-image" "none"
                ]
                [ Typo.link (lienVersRechercheSiren siren)
                    [ class "fr-card--white flex flex-row p-4 custom-hover overflow-y-auto gap-2 items-center h-full"
                    , style "background-image" "none"
                    ]
                    [ span [ Typo.textBold ] [ text "Voir tous les établissements de l'entreprise" ] ]
                ]
            ]
        ]


viewSuivisModal : Bool -> List SuiviEquipe -> Html Msg
viewSuivisModal modal suivis =
    let
        ( opened, content, title ) =
            if modal then
                ( True
                , div [ class "flex flex-col gap-4" ]
                    [ div []
                        [ div [] [ text "Liste des équipes qui suivent cet établissement" ]
                        , div [ class "grey-text", Typo.textSm, class "!mb-0" ] [ text "Un établissement est suivi par une équipe dès lors qu'un contact, un échange, une demande ou un rappel est saisi sur la fiche." ]
                        ]
                    , suivis
                        |> List.map viewSuiviBlock
                        |> div []
                    , div [] [ text "\u{00A0}" ]
                    ]
                , text "Suivi de l'établissement"
                )

            else
                ( False, nothing, nothing )
    in
    DSFR.Modal.view
        { id = "suivis-equipes"
        , label = "suivis-equipes"
        , openMsg = NoOp
        , closeMsg = Just <| ToggleSuivisEquipesModal False
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.md
        }
        content
        Nothing
        |> Tuple.first


viewSuiviBlock : SuiviEquipe -> Html Msg
viewSuiviBlock { nom, contacts, echanges, demandes, rappels } =
    div [ class "!p-4 border-2 dark-grey-border" ]
        [ [ ( "contact", contacts )
          , ( "echange", echanges )
          , ( "demande", demandes )
          , ( "rappel", rappels )
          ]
            |> List.filterMap (\( label, num ) -> num |> Maybe.map (\n -> String.fromInt n ++ " " ++ label ++ plural n))
            |> List.map (\label -> DSFR.Tag.unclickable { data = label, toString = identity })
            |> DSFR.Tag.medium
        , div [ Typo.textBold, Typo.textLg, class "!mb-0" ] [ text nom ]
        ]


viewModifierEnseigneModal : WebData () -> WebData (Result String EtablissementWithExtraInfo) -> Maybe String -> Html Msg
viewModifierEnseigneModal modifierEnseigneRequest etablissementWithExtraInfo modificationEnseigne =
    let
        ( opened, content, title ) =
            case ( etablissementWithExtraInfo, modificationEnseigne ) of
                ( _, Nothing ) ->
                    ( False, nothing, nothing )

                ( RD.Success (Ok { etablissement }), Just enseigneContribution ) ->
                    ( True
                    , viewModifierEnseigneModalBody modifierEnseigneRequest etablissement enseigneContribution
                    , text "Modifier le nom d'établissement"
                    )

                _ ->
                    ( False, nothing, nothing )
    in
    DSFR.Modal.view
        { id = "modifier-enseigne"
        , label = "modifier-enseigne"
        , openMsg = NoOp
        , closeMsg = Just ToggleEnseigneContribution
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.md
        }
        content
        Nothing
        |> Tuple.first


viewModifierEnseigneModalBody : WebData () -> EtablissementSimple -> String -> Html Msg
viewModifierEnseigneModalBody modifierEnseigneRequest etablissement enseigneContribution =
    Html.form [ class "flex flex-col gap-4", Events.onSubmit RequestedEnseigneContributionSave ]
        [ div [ class "flex flex-col" ]
            [ DSFR.Input.new
                { value = etablissement.nomEntreprise |> Maybe.withDefault ""
                , onInput = \_ -> NoOp
                , label = text "Nom de l'entreprise"
                , name = "enseigne-contribution-entreprise"
                }
                |> DSFR.Input.withExtraAttrs [ Html.Attributes.title <| Maybe.withDefault "" <| etablissement.nomEntreprise ]
                |> DSFR.Input.withDisabled True
                |> DSFR.Input.view
            , DSFR.Input.new
                { value = etablissement.nom |> Maybe.withDefault ""
                , onInput = \_ -> NoOp
                , label = text "Nom de l'établissement"
                , name = "enseigne-contribution-etablissement"
                }
                |> DSFR.Input.withExtraAttrs [ Html.Attributes.title <| Maybe.withDefault "" <| etablissement.nom ]
                |> DSFR.Input.withDisabled True
                |> DSFR.Input.view
            , DSFR.Input.new
                { value = enseigneContribution
                , onInput = UpdateEnseigneContribution
                , label = text "Nouveau nom de l'établissement"
                , name = "enseigne-contribution-enseigne"
                }
                |> DSFR.Input.view
            , div [ class "fr-text-default--info", Typo.textSm ]
                [ DSFR.Icons.System.infoFill |> DSFR.Icons.iconSM
                , text " "
                , text "Laissez le champ vide si vous ne souhaitez pas modifier le nom"
                ]
            ]
        , div [ class "flex flex-row justify-end" ]
            [ div []
                [ let
                    ( isButtonDisabled, title_ ) =
                        case modifierEnseigneRequest of
                            RD.Loading ->
                                ( True, "Enregistrement en cours" )

                            _ ->
                                ( False, "Enregistrer" )

                    buttons =
                        case modifierEnseigneRequest of
                            RD.Success _ ->
                                [ DSFR.Button.new { onClick = Just ToggleEnseigneContribution, label = "OK" }
                                ]

                            _ ->
                                [ DSFR.Button.new { onClick = Nothing, label = "Enregistrer" }
                                    |> DSFR.Button.submit
                                    |> DSFR.Button.withAttrs [ Html.Attributes.title title_, Html.Attributes.name "submit-enseigne-contribution" ]
                                    |> DSFR.Button.withDisabled isButtonDisabled
                                , DSFR.Button.new { onClick = Just ToggleEnseigneContribution, label = "Annuler" }
                                    |> DSFR.Button.withDisabled isButtonDisabled
                                    |> DSFR.Button.secondary
                                ]
                  in
                  DSFR.Button.group buttons
                    |> DSFR.Button.inline
                    |> DSFR.Button.alignedRightInverted
                    |> DSFR.Button.viewGroup
                ]
            ]
        ]


viewModifierGeolocalisationModal : Shared.User -> String -> WebData () -> WebData (Result String EtablissementWithExtraInfo) -> Maybe GeolocalisationContribution -> Html Msg
viewModifierGeolocalisationModal user etablissementId modifierGeolocalisationRequest etablissementWithExtraInfo modificationGeolocalisation =
    let
        ( opened, content, title ) =
            case ( etablissementWithExtraInfo, modificationGeolocalisation ) of
                ( _, Nothing ) ->
                    ( False, nothing, nothing )

                ( RD.Success _, Just geolocalisationContribution ) ->
                    ( True
                    , viewModifierGeolocalisationModalBody user
                        etablissementId
                        (etablissementWithExtraInfo
                            |> RD.withDefault (Err "")
                            |> Result.map Just
                            |> Result.withDefault Nothing
                            |> Maybe.map .etablissement
                            |> Maybe.andThen .geolocalisationEquipe
                        )
                        modifierGeolocalisationRequest
                        geolocalisationContribution
                    , text "Modifier la géolocalisation de l'établissement"
                    )

                _ ->
                    ( False, nothing, nothing )
    in
    DSFR.Modal.view
        { id = "modifier-geolocalisation"
        , label = "modifier-geolocalisation"
        , openMsg = NoOp
        , closeMsg = Just ToggleGeolocalisationContribution
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.lg
        }
        content
        Nothing
        |> Tuple.first


viewModifierGeolocalisationModalBody : Shared.User -> String -> Maybe String -> WebData () -> GeolocalisationContribution -> Html Msg
viewModifierGeolocalisationModalBody user etablissementId geolocalisationEquipe modifierGeolocalisationRequest geolocalisationContribution =
    let
        userEquipe =
            user
                |> User.role
                |> Data.Role.equipe
                |> Maybe.map Data.Equipe.nom

        modifieParUneAutreEquipe =
            case geolocalisationEquipe of
                Nothing ->
                    False

                Just equipe ->
                    equipe
                        |> Just
                        |> (/=) userEquipe
    in
    Html.form [ class "flex flex-col gap-4", Events.onSubmit RequestedGeolocalisationContributionSave ]
        [ div [ class "flex flex-col gap-2" ]
            [ div []
                [ text <| "Géolocalisation de l'établissement"
                , text "\u{00A0}: "
                , text <|
                    Maybe.withDefault "-" <|
                        Maybe.map pointToString <|
                            .geolocalisationActuelle <|
                                geolocalisationContribution
                ]
            , div []
                [ text <| "Géolocalisation personnalisée"
                , text "\u{00A0}: "
                , text <|
                    Maybe.withDefault "-" <|
                        Maybe.map pointToString <|
                            .geolocalisationContribution <|
                                geolocalisationContribution
                ]
            ]
        , if modifieParUneAutreEquipe then
            let
                emails =
                    [ Lib.Variables.contactEmail ]

                subject =
                    Builder.string "subject" <|
                        "Signalement de géolocalisation erronée pour l'établissement "
                            ++ etablissementId

                mailto =
                    "mailto:"
                        ++ String.join "," emails
                        ++ Builder.toQuery [ subject ]
            in
            div [ class "flex flex-col", Typo.textBold ]
                [ p [ class "!mb-0" ]
                    [ text "Cette géolocalisation a déjà été modifiée par l'équipe "
                    , text <|
                        Maybe.withDefault "?" <|
                            geolocalisationEquipe
                    , text ", et vous ne pouvez donc pas la modifier."
                    ]
                , p [ class "!mb-0" ]
                    [ text "Si vous pensez que la géolocalisation est erronée, contactez-nous à l'adresse suivante\u{00A0}: "
                    , Typo.externalLink mailto
                        []
                        [ text Lib.Variables.contactEmail
                        ]
                    ]
                ]

          else
            div [ class "flex flex-col gap-2" ]
                [ let
                    error =
                        if geolocalisationContribution.nouvelleGeolocalisationChamp /= "" && (geolocalisationContribution.nouvelleGeolocalisation == Nothing) then
                            Just [ text "La géolocalisation saisie est invalide" ]

                        else
                            Nothing
                  in
                  div [ class "flex flex-col gap-2" ]
                    [ DSFR.Input.new
                        { value =
                            geolocalisationContribution
                                |> .nouvelleGeolocalisationChamp
                        , onInput = UpdateGeolocalisationContributionInput
                        , label = text "Nouvelle géolocalisation personnalisée"
                        , name = "geolocalisation-contribution-nouvelle"
                        }
                        |> DSFR.Input.withError error
                        |> DSFR.Input.withExtraAttrs [ class "!mb-0" ]
                        |> DSFR.Input.withHint [ text "Format attendu\u{00A0}: longitude, latitude" ]
                        |> DSFR.Input.view
                    , DSFR.Alert.small
                        { title = Nothing
                        , description =
                            span [ Typo.textSm ]
                                [ text "Déplacez la carte pour modifier la géolocalisation."
                                , br []
                                , text "Videz le champ pour supprimer."
                                ]
                        }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.info
                    ]
                ]
        , adresseFilter geolocalisationContribution.selectAdresse
        , div [ class "w-full h-[200px] z-10" ]
            [ Html.node "maplibre-geolocalisation"
                [ Html.Attributes.attribute "data" <|
                    Encode.encode 0 <|
                        Encode.object
                            [ ( "centre"
                              , geolocalisationContribution
                                    |> .centreCarte
                                    |> encodePoint
                              )
                            , ( "geolocalisationActuelle"
                              , geolocalisationContribution
                                    |> .geolocalisationActuelle
                                    |> Maybe.map encodePoint
                                    |> Maybe.withDefault Encode.null
                              )
                            , ( "geolocalisationContribution"
                              , geolocalisationContribution
                                    |> .geolocalisationContribution
                                    |> Maybe.map encodePoint
                                    |> Maybe.withDefault Encode.null
                              )
                            , ( "nouvelleGeolocalisation"
                              , geolocalisationContribution
                                    |> .nouvelleGeolocalisation
                                    |> Maybe.map encodePoint
                                    |> Maybe.withDefault Encode.null
                              )
                            , ( "zoom"
                              , geolocalisationContribution
                                    |> .zoom
                                    |> Maybe.map Encode.int
                                    |> Maybe.withDefault Encode.null
                              )
                            ]
                , Events.on "movemap" <| loggingDecoder decodeMoveMap
                ]
                []
            ]
        , div [ class "flex flex-row justify-end" ]
            [ div []
                [ let
                    ( isButtonDisabled, label ) =
                        if modifieParUneAutreEquipe then
                            ( True, "Enregistrer" )

                        else
                            case modifierGeolocalisationRequest of
                                RD.Loading ->
                                    ( True, "Enregistrement en cours" )

                                _ ->
                                    if geolocalisationContribution.nouvelleGeolocalisation == geolocalisationContribution.geolocalisationContribution then
                                        ( True
                                        , "Enregistrer"
                                        )

                                    else if geolocalisationContribution.nouvelleGeolocalisationChamp == "" then
                                        ( False
                                        , "Supprimer"
                                        )

                                    else if geolocalisationContribution.nouvelleGeolocalisation == Nothing then
                                        ( True
                                        , "Enregistrer"
                                        )

                                    else
                                        ( False
                                        , "Enregistrer"
                                        )

                    buttons =
                        case modifierGeolocalisationRequest of
                            RD.Success _ ->
                                [ DSFR.Button.new
                                    { onClick = Just ToggleGeolocalisationContribution
                                    , label = "OK"
                                    }
                                ]

                            _ ->
                                [ DSFR.Button.new
                                    { onClick = Nothing
                                    , label = label
                                    }
                                    |> DSFR.Button.withDisabled isButtonDisabled
                                    |> DSFR.Button.submit
                                , DSFR.Button.new
                                    { onClick = Just ToggleGeolocalisationContribution
                                    , label = "Annuler"
                                    }
                                    |> DSFR.Button.secondary
                                ]
                  in
                  DSFR.Button.group buttons
                    |> DSFR.Button.inline
                    |> DSFR.Button.alignedRightInverted
                    |> DSFR.Button.viewGroup
                ]
            ]
        ]


decodeMoveMap : Decoder Msg
decodeMoveMap =
    Decode.field "detail" <|
        Decode.map2 (\lon lat -> UpdateGeolocalisationContributionByMap ( lon, lat ))
            (Decode.field "longitude" Decode.float)
            (Decode.field "latitude" Decode.float)


viewContactsModal : String -> WebData entity -> ContactAction -> Html Msg
viewContactsModal etablissementId saveContactRequest contactAction =
    let
        ( opened, content, title ) =
            case contactAction of
                None ->
                    ( False, nothing, nothing )

                ViewContactFonctions fonctions ->
                    let
                        autresFonctions =
                            fonctions
                                |> List.filter
                                    (\f ->
                                        case f of
                                            CEtablissement ed ->
                                                ed.etablissementId /= etablissementId

                                            _ ->
                                                True
                                    )
                    in
                    ( True, UI.Contact.viewFonctions SetContactAction autresFonctions, text "Autres fonctions du contact" )

                NewContact contactForm ->
                    ( True
                    , viewNewContactFormFields
                        { confirm = ConfirmContactAction
                        , update = UpdatedContact
                        , cancel = CancelContactAction
                        , toggle = ToggleNouveauContactModeCreation
                        , confirmSearch = RechercheContactExistant
                        , updateSearch = UpdatedRechercheContactExistant
                        , selectedPersonne = SelectedContactExistant
                        , niveauxDiplome = Nothing
                        , situationsProfessionnelles = Nothing
                        }
                        saveContactRequest
                        contactForm
                    , text "Ajouter un contact"
                    )

                EditContact contactExistant contact ->
                    ( True
                    , viewEditContact
                        { confirm = ConfirmContactAction
                        , update = UpdatedContact
                        , cancel = CancelContactAction
                        , niveauxDiplome = Nothing
                        , situationsProfessionnelles = Nothing
                        }
                        saveContactRequest
                        contactExistant
                        contact
                    , text "Modifier un contact"
                    )

                DeleteContact contact _ ->
                    ( True
                    , viewDeleteContactBody
                        []
                        { confirm = ConfirmContactAction
                        , cancel = CancelContactAction
                        }
                        saveContactRequest
                        contact.personne.id
                    , text "Supprimer un contact"
                    )

                DeleteContactForm _ ->
                    ( False, nothing, nothing )

                EditContactForm _ _ ->
                    ( False, nothing, nothing )
    in
    DSFR.Modal.view
        { id = "contact"
        , label = "contact"
        , openMsg = NoOp
        , closeMsg = Just CancelContactAction
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.md
        }
        content
        Nothing
        |> Tuple.first


viewRessourcesModal : WebData EtablissementWithExtraInfo -> RessourceAction -> Html Msg
viewRessourcesModal saveRessourceRequest ressourceAction =
    let
        ( opened, content, title ) =
            case ressourceAction of
                NoRessource ->
                    ( False, nothing, nothing )

                NewRessource ressource ->
                    ( True
                    , viewRessourceFormFields True saveRessourceRequest ressource
                    , text "Ajouter une ressource"
                    )

                EditRessource ressource ->
                    ( True
                    , viewRessourceFormFields False saveRessourceRequest ressource
                    , text "Modifier une ressource"
                    )

                DeleteRessource ressource ->
                    ( True
                    , div []
                        [ text "Êtes-vous sûr(e) de vouloir supprimer cette ressource\u{00A0}?"
                        , div [ DSFR.Grid.col12, class "flex flex-col !pt-4" ]
                            [ [ DSFR.Button.new { onClick = Just <| ConfirmRessourceAction, label = "Supprimer" }
                                    |> DSFR.Button.withDisabled (saveRessourceRequest == RD.Loading)
                                    |> DSFR.Button.withAttrs
                                        [ id <| "confirmer-modifier-ressource-" ++ String.fromInt ressource.id
                                        , Html.Attributes.title "Supprimer"
                                        ]
                              , DSFR.Button.new { onClick = Just <| CancelRessourceAction, label = "Annuler" }
                                    |> DSFR.Button.withAttrs
                                        [ id <| "annuler-modifier-ressource-" ++ String.fromInt ressource.id
                                        , Html.Attributes.title "Annuler"
                                        ]
                                    |> DSFR.Button.secondary
                              ]
                                |> DSFR.Button.group
                                |> DSFR.Button.inline
                                |> DSFR.Button.alignedRightInverted
                                |> DSFR.Button.viewGroup
                            ]
                        , div [ class "flex flex-row justify-end w-full" ]
                            [ case saveRessourceRequest of
                                RD.Failure _ ->
                                    DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite" }
                                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                                _ ->
                                    nothing
                            ]
                        ]
                    , text "Supprimer une ressource"
                    )
    in
    DSFR.Modal.view
        { id = "ressource"
        , label = "ressource"
        , openMsg = NoOp
        , closeMsg = Just CancelRessourceAction
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.md
        }
        content
        Nothing
        |> Tuple.first


viewEtablissement : Bool -> Maybe EtablissementEtiquettes -> WebData (List Zonage) -> MultiSelectRemote.SmartSelect Msg String -> MultiSelectRemote.SmartSelect Msg String -> MultiSelectRemote.SmartSelect Msg String -> WebData EtablissementWithExtraInfo -> EtablissementWithExtraInfo -> Html Msg
viewEtablissement forPrint editEtablissementEtiquettes zonages selectActivites selectLocalisations selectMots saveEtablissementEtiquettesRequest ({ equipeEtablissement, etablissement, etablissementExtra } as etablissementWithExtraInfo) =
    let
        { etiquettes, demandes } =
            equipeEtablissement

        { sireneUpdate } =
            etablissementExtra

        { siret, geolocalisation, geolocalisationContribution, nomAffichage } =
            etablissement
    in
    div []
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col5 ]
                [ Lazy.lazy2 viewDonneesPubliques sireneUpdate etablissementWithExtraInfo
                ]
            , div
                [ DSFR.Grid.col4
                , classList
                    [ ( "fr-card--grey", editEtablissementEtiquettes == Nothing )
                    ]
                ]
                [ Lazy.lazy8 viewEtablissementData forPrint editEtablissementEtiquettes (zonages |> RD.withDefault []) selectActivites selectLocalisations selectMots saveEtablissementEtiquettesRequest etiquettes
                ]
            , div [ DSFR.Grid.col3, class "flex flex-col gap-4 justify-between" ]
                [ Lazy.lazy viewFicheDemandes demandes
                , viewIf (not forPrint) <| Lazy.lazy3 viewLinks (geolocalisationContribution |> Maybe.map Just |> Maybe.withDefault geolocalisation) nomAffichage siret
                ]
            ]
        ]


viewRessourceFormFields : Bool -> WebData EtablissementWithExtraInfo -> Ressource -> Html Msg
viewRessourceFormFields new request ressource =
    let
        ressourceIsEmpty =
            ressource.lien == ""
    in
    formWithListeners [ Events.onSubmit <| ConfirmRessourceAction, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12 ]
            [ DSFR.Input.new { value = ressourceTypeToString ressource.type_, onInput = UpdatedRessource RType, label = text "Type", name = "ressource-type" }
                |> DSFR.Input.select
                    { options = ressourceTypes
                    , toId = ressourceTypeToString
                    , toLabel = ressourceTypeToDisplay >> text
                    , toDisabled = Nothing
                    }
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col6 ]
            [ DSFR.Input.new { value = ressource.nom, onInput = UpdatedRessource RNom, label = text "Nom", name = "ressource-nom" }
                |> DSFR.Input.withRequired True
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col6 ]
            [ DSFR.Input.new { value = ressource.lien, onInput = UpdatedRessource RLien, label = text "Lien", name = "ressource-lien" }
                |> DSFR.Input.withRequired True
                |> DSFR.Input.view
            ]
        , let
            label =
                if new then
                    "Ajouter"

                else
                    "Modifier"
          in
          div [ DSFR.Grid.col12, class "flex flex-col !pt-4" ]
            [ [ DSFR.Button.new { onClick = Nothing, label = label }
                    |> DSFR.Button.withDisabled (ressourceIsEmpty || request == RD.Loading)
                    |> DSFR.Button.submit
                    |> DSFR.Button.withAttrs
                        [ id <| "confirmer-modifier-ressource-" ++ String.fromInt ressource.id
                        , Html.Attributes.title label
                        ]
              , DSFR.Button.new { onClick = Just <| CancelRessourceAction, label = "Annuler" }
                    |> DSFR.Button.secondary
                    |> DSFR.Button.withAttrs
                        [ id <| "annuler-modifier-ressource-" ++ String.fromInt ressource.id
                        , Html.Attributes.title "Annuler"
                        ]
              ]
                |> DSFR.Button.group
                |> DSFR.Button.inline
                |> DSFR.Button.alignedRightInverted
                |> DSFR.Button.viewGroup
            ]
        , div [ class "flex flex-row justify-end w-full" ]
            [ case request of
                RD.Failure _ ->
                    DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite" }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                _ ->
                    nothing
            ]
        ]


viewCommentaire : WebData EtablissementWithExtraInfo -> Maybe EtablissementCommentaire -> String -> Html Msg
viewCommentaire saveEtablissementCommentaireRequest editEtablissementCommentaire commentaire =
    div [ class "flex flex-col gap-4" ]
        [ div [ class "flex flex-row justify-between" ]
            [ h3 [ Typo.fr_h4 ] [ text "Commentaire" ]
            , DSFR.Button.new { label = "Modifier le commentaire", onClick = Just EditEtablissementCommentaire }
                |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
                |> DSFR.Button.withAttrs [ id "modifier-etablissement-commentaire" ]
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.view
                |> viewIf (editEtablissementCommentaire == Nothing)
            ]
        , case editEtablissementCommentaire of
            Nothing ->
                div []
                    [ case commentaire of
                        "" ->
                            span [ class "italic" ] [ text "Aucune description pour l'instant" ]

                        _ ->
                            div [ class "whitespace-pre-wrap" ]
                                [ text commentaire
                                ]
                    ]

            Just { description } ->
                let
                    noDifference =
                        description == commentaire
                in
                formWithListeners [ Events.onSubmit <| RequestedSaveEtablissementCommentaire ]
                    [ DSFR.Input.new
                        { value = description
                        , label = nothing
                        , onInput = UpdatedDescription
                        , name = "description"
                        }
                        |> DSFR.Input.textArea (Just 8)
                        |> DSFR.Input.view
                    , case saveEtablissementCommentaireRequest of
                        RD.NotAsked ->
                            nothing

                        RD.Loading ->
                            nothing

                        RD.Failure _ ->
                            DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite" }
                                |> DSFR.Alert.alert Nothing DSFR.Alert.error

                        RD.Success _ ->
                            DSFR.Alert.small { title = Nothing, description = text "Modifications enregistrées\u{00A0}!" }
                                |> DSFR.Alert.alert Nothing DSFR.Alert.success
                    , [ DSFR.Button.new { onClick = Nothing, label = "Valider" }
                            |> DSFR.Button.withDisabled (noDifference || saveEtablissementCommentaireRequest == RD.Loading)
                            |> DSFR.Button.submit
                      , DSFR.Button.new { onClick = Just CancelEditEtablissementCommentaire, label = "Annuler" }
                            |> DSFR.Button.secondary
                            |> DSFR.Button.withDisabled (saveEtablissementCommentaireRequest == RD.Loading)
                      ]
                        |> DSFR.Button.group
                        |> DSFR.Button.inline
                        |> DSFR.Button.alignedRightInverted
                        |> DSFR.Button.viewGroup
                    ]
        ]


viewPersonnePhysique :
    Int
    -> WebData EtablissementWithExtraInfo
    ->
        { personnePhysique
            | fonction : String
            , personne : Personne
        }
    -> Html Msg
viewPersonnePhysique prochainId contactCreationMandataire contact =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col1, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ UI.Contact.columnWrapper <| DSFR.Icons.iconXL DSFR.Icons.User.userLine ]
        , div [ DSFR.Grid.col3, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ UI.Contact.columnWrapper <| text <| contact.fonction ]
        , div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ UI.Contact.columnWrapper <| text <| contact.personne.prenom ]
        , div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ UI.Contact.columnWrapper <| text <| contact.personne.nom ]
        , div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ UI.Contact.columnWrapper <| text <| Maybe.withDefault "" <| Maybe.map Lib.Date.formatDateShort <| contact.personne.naissanceDate ]
        , div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ DSFR.Button.new
                { label = "Ajouter en contact"
                , onClick =
                    Just <|
                        SetContactAction <|
                            NewContact <|
                                newContactFormPourExistant
                                    prochainId
                                    { fonction = Just <| FonctionEtablissement { fonction = contact.fonction }
                                    , prenom = contact.personne.prenom
                                    , nom = contact.personne.nom
                                    , dateDeNaissance = contact.personne.naissanceDate
                                    }
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withDisabled (contactCreationMandataire == RD.Loading)
                |> DSFR.Button.view
            ]
        ]


viewPersonneMorale : { personneMorale | fonction : String, nom : String, siren : String } -> Html Msg
viewPersonneMorale personneMorale =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col1, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ UI.Contact.columnWrapper <| DSFR.Icons.iconXL DSFR.Icons.Buildings.hotelLine ]
        , div [ DSFR.Grid.col3, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ UI.Contact.columnWrapper <| text <| withEmptyAs "-" <| personneMorale.fonction ]
        , div [ DSFR.Grid.col4, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ UI.Contact.columnWrapper <| text <| withEmptyAs "-" <| personneMorale.nom ]
        , div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ UI.Contact.columnWrapper <|
                case personneMorale.siren of
                    "" ->
                        text "-"

                    s ->
                        voirLienExterneVersRehercheSiren s
            ]
        , div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ nothing ]
        ]


viewBeneficiaire :
    Int
    -> WebData EtablissementWithExtraInfo
    -> BeneficiaireEffectif
    -> Html Msg
viewBeneficiaire prochainId contactCreation beneficiaire =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ UI.Contact.columnWrapper <| text <| beneficiaire.nom ]
        , div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ UI.Contact.columnWrapper <| text <| beneficiaire.nomUsage ]
        , div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ UI.Contact.columnWrapper <| text <| beneficiaire.prenoms ]
        , div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ UI.Contact.columnWrapper <| text <| Maybe.withDefault "" <| Maybe.map Lib.Date.formatDateShortSansDay <| beneficiaire.naissanceDate ]
        , div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ div [ class "!px-[1rem] text-right", style "overflow-wrap" "break-word" ] <|
                List.singleton <|
                    text <|
                        Lib.UI.formatFloatWithThousandSpacing <|
                            beneficiaire.totalParts
            ]
        , div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ DSFR.Button.new
                { label = "Ajouter en contact"
                , onClick =
                    Just <|
                        SetContactAction <|
                            NewContact <|
                                newContactFormPourExistant
                                    prochainId
                                    { fonction = Just <| FonctionEtablissement { fonction = "Bénéficiaire" }
                                    , prenom =
                                        beneficiaire.prenoms
                                            |> String.split ","
                                            |> List.head
                                            |> Maybe.withDefault ""
                                    , nom = beneficiaire.nom
                                    , dateDeNaissance = beneficiaire.naissanceDate
                                    }
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withDisabled (contactCreation == RD.Loading)
                |> DSFR.Button.view
            ]
        ]


viewFiliale :
    Filiale
    -> Html Msg
viewFiliale filiale =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col8, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ UI.Contact.columnWrapper <|
                text <|
                    Maybe.withDefault "-" <|
                        filiale.nom
            ]
        , div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ div [ class "!px-[1rem] text-right", style "overflow-wrap" "break-word" ] <|
                List.singleton <|
                    text <|
                        Lib.UI.formatFloatWithThousandSpacing <|
                            Maybe.withDefault 0 <|
                                filiale.participation
            ]
        , div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ div [ class "!px-[1rem] text-center", style "overflow-wrap" "break-word" ] <|
                List.singleton <|
                    Maybe.withDefault (text "-") <|
                        Maybe.map
                            (\siren ->
                                voirLienExterneVersRehercheSiren siren
                            )
                        <|
                            filiale.siren
            ]
        ]


viewDetention :
    Detention
    -> Html Msg
viewDetention detention =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col6, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ UI.Contact.columnWrapper <|
                text <|
                    Maybe.withDefault "-" <|
                        detention.nom
            ]
        , div [ DSFR.Grid.col4, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ div [ class "!px-[1rem] text-right", style "overflow-wrap" "break-word" ] <|
                List.singleton <|
                    text <|
                        case ( detention.detention, detention.parts ) of
                            ( Just quantite, Just pourcentage ) ->
                                Lib.UI.formatFloatWithThousandSpacing quantite ++ " (" ++ Lib.UI.formatFloatWithThousandSpacing pourcentage ++ " %)"

                            ( Just quantite, Nothing ) ->
                                Lib.UI.formatFloatWithThousandSpacing quantite

                            ( Nothing, Just pourcentage ) ->
                                Lib.UI.formatFloatWithThousandSpacing pourcentage ++ " %"

                            _ ->
                                "Inconnu"
            ]
        , div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ]
            [ div [ class "!px-[1rem] text-center", style "overflow-wrap" "break-word" ] <|
                List.singleton <|
                    Maybe.withDefault (text "-") <|
                        Maybe.map
                            (\siren ->
                                Typo.externalLink (lienVersRechercheSiren siren) [] [ text siren ]
                            )
                        <|
                            detention.siren
            ]
        ]


viewFicheDemandes : List Demande -> Html msg
viewFicheDemandes demandes =
    let
        openDemandes =
            demandes
                |> List.filter (Data.Demande.cloture >> not)
    in
    div [ class "p-2" ]
        [ div [ Typo.textBold, class "!mb-2" ]
            [ text "Demande"
            , text <|
                if List.length openDemandes > 1 then
                    "s"

                else
                    ""
            , text " en cours"
            ]
        , hr [ class "fr-hr" ] []
        , case openDemandes of
            [] ->
                span [ class "italic" ] [ text "Aucune demande en cours." ]

            ds ->
                ds
                    |> List.map
                        (Data.Demande.label
                            >> (\t -> { data = t, toString = identity })
                            >> DSFR.Tag.unclickable
                        )
                    |> DSFR.Tag.medium
        ]


viewDonneesPubliques : Maybe Date -> EtablissementWithExtraInfo -> Html msg
viewDonneesPubliques sireneUpdate { etablissement, etablissementExtra, entreprise } =
    let
        { siret, activiteNaf, activiteNafId, dateCreationEtablissement, dateFermetureEtablissement, diffusible } =
            etablissement

        { categorieNaf, categorieNafId, zonagesPrioritaires, ema, enseigne } =
            etablissementExtra

        { siren, categorieJuridique, categorieJuridiqueId, formeJuridique, formeJuridiqueId, ess, entrepriseNom, micro } =
            entreprise
                |> Maybe.map
                    (\ed ->
                        { siren = ed.siren
                        , categorieJuridique = ed.categorieJuridique
                        , categorieJuridiqueId = ed.categorieJuridiqueId
                        , formeJuridique = ed.formeJuridique
                        , formeJuridiqueId = ed.formeJuridiqueId
                        , ess = Just ed.ess
                        , entrepriseNom = ed.entrepriseNom
                        , micro = Just ed.micro
                        }
                    )
                |> Maybe.withDefault
                    { siren = String.dropRight 5 siret
                    , categorieJuridique = Nothing
                    , categorieJuridiqueId = Nothing
                    , formeJuridique = Nothing
                    , formeJuridiqueId = Nothing
                    , ess = Nothing
                    , entrepriseNom = "Inconnu"
                    , micro = Nothing
                    }
    in
    div [ class "p-2" ]
        [ div [ class "pb-4" ]
            [ div [ Typo.textBold ] [ text "Données publiques", sup [ Html.Attributes.title hintDonneesEtablissementsPubliques ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
            , sireneUpdate
                |> viewMaybe
                    (\date ->
                        div [ Typo.textXs, class "!mb-2" ]
                            [ text "(dernière modification dans la base SIRENE le "
                            , text <| Lib.Date.formatDateShort date
                            , text ")"
                            ]
                    )
            , hr [ class "!pb-4" ] []
            , div []
                [ Lib.UI.infoLineHtml Nothing "SIRET" siret <|
                    span [ class "inline-flex flex-row items-center gap-4" ]
                        [ text siret
                        , UI.Clipboard.clipboard
                            { content = siret
                            , label = "Copier"
                            , copiedLabel = "Copié\u{00A0}!"
                            , timeoutMilliseconds = 2000
                            }
                        ]
                , Lib.UI.infoLine Nothing "Nom établissement" <|
                    withEmptyAs "-" <|
                        Maybe.withDefault "-" <|
                            etablissement.nom
                , Lib.UI.infoLine Nothing "Enseigne" <|
                    withEmptyAs "-" <|
                        Maybe.withDefault "-" <|
                            enseigne
                , Lib.UI.infoLineHtml Nothing "SIREN" siren <|
                    span [ class "inline-flex flex-row items-center gap-4" ]
                        [ text siren
                        , UI.Clipboard.clipboard
                            { content = siren
                            , label = "Copier"
                            , copiedLabel = "Copié\u{00A0}!"
                            , timeoutMilliseconds = 2000
                            }
                        ]
                , Lib.UI.infoLine Nothing "Nom entreprise" <|
                    withEmptyAs "-" <|
                        entrepriseNom
                , Lib.UI.infoLine Nothing "Statut de diffusion" <|
                    if diffusible then
                        "Diffusible"

                    else
                        "Non-diffusible"
                , Lib.UI.infoLineHtml Nothing
                    "Catégorie"
                    (Maybe.withDefault "-" categorieNaf)
                    (span []
                        [ text (Maybe.withDefault "-" categorieNaf)
                        , viewMaybe (\_ -> text " ") categorieNaf
                        , Maybe.map2
                            (\catNaf catNafId ->
                                Typo.link
                                    (Route.toUrl <|
                                        Route.Etablissements <|
                                            (\q ->
                                                if q == "" then
                                                    Nothing

                                                else
                                                    Just q
                                            )
                                            <|
                                                filtersAndPaginationToQuery
                                                    ( { emptyFilters
                                                        | codesNaf =
                                                            [ { id = catNafId
                                                              , nom = catNaf
                                                              }
                                                            ]
                                                      }
                                                    , defaultPagination
                                                    )
                                    )
                                    [ Html.Attributes.title "Rechercher les établissements avec la même catégorie NAF" ]
                                    [ DSFR.Icons.iconSM DSFR.Icons.System.searchLine
                                    ]
                            )
                            categorieNaf
                            categorieNafId
                            |> Maybe.withDefault nothing
                        ]
                    )
                , Lib.UI.infoLineHtml Nothing
                    "Activité NAF"
                    (Maybe.withDefault "-" activiteNaf)
                    (span []
                        [ text (Maybe.withDefault "-" activiteNaf)
                        , viewMaybe (\_ -> text " ") activiteNaf
                        , Maybe.map2
                            (\actNaf actNafId ->
                                Typo.link
                                    (Route.toUrl <|
                                        Route.Etablissements <|
                                            (\q ->
                                                if q == "" then
                                                    Nothing

                                                else
                                                    Just q
                                            )
                                            <|
                                                filtersAndPaginationToQuery
                                                    ( { emptyFilters
                                                        | codesNaf =
                                                            [ { id = actNafId
                                                              , nom = actNaf
                                                              }
                                                            ]
                                                      }
                                                    , defaultPagination
                                                    )
                                    )
                                    [ Html.Attributes.title "Rechercher les établissements avec la même activité NAF" ]
                                    [ DSFR.Icons.iconSM DSFR.Icons.System.searchLine
                                    ]
                            )
                            activiteNaf
                            activiteNafId
                            |> Maybe.withDefault nothing
                        ]
                    )
                , case ema of
                    Nothing ->
                        Lib.UI.infoLine Nothing "Effectif" <| "Indisponible"

                    Just { valeur, annee, mois } ->
                        let
                            moisNom =
                                mois
                                    |> Lib.Date.intToMonth
                                    |> Lib.Date.frenchMonthAbbreviation

                            effectifsLabel =
                                "Effectif moyen annuel (" ++ moisNom ++ " " ++ String.fromInt annee ++ ")"

                            valeurTexte =
                                String.replace "." "," <|
                                    String.fromFloat <|
                                        valeur
                        in
                        Lib.UI.infoLineHtml Nothing
                            effectifsLabel
                            valeurTexte
                            (span []
                                [ text valeurTexte
                                , text " "
                                , Typo.link
                                    (Route.toUrl <|
                                        Route.Etablissements <|
                                            (\q ->
                                                if q == "" then
                                                    Nothing

                                                else
                                                    Just q
                                            )
                                            <|
                                                filtersAndPaginationToQuery
                                                    ( { emptyFilters
                                                        | effectifMax = Just <| String.fromFloat <| valeur + 0.1
                                                        , effectifMin = Just <| String.fromFloat <| valeur
                                                      }
                                                    , defaultPagination
                                                    )
                                    )
                                    [ Html.Attributes.title "Rechercher les établissements avec les mêmes effectifs" ]
                                    [ DSFR.Icons.iconSM DSFR.Icons.System.searchLine
                                    ]
                                ]
                            )
                , Lib.UI.infoLine Nothing "ESS" <|
                    case ess of
                        Just True ->
                            "Oui"

                        Just False ->
                            "Non"

                        Nothing ->
                            "Inconnu"
                , Lib.UI.infoLineHtml Nothing
                    "Catégorie juridique"
                    (Maybe.withDefault "-" categorieJuridique)
                    (span []
                        [ text (Maybe.withDefault "-" categorieJuridique)
                        , viewMaybe (\_ -> text " ") categorieJuridique
                        , Maybe.map2
                            (\catJur catJurId ->
                                Typo.link
                                    (Route.toUrl <|
                                        Route.Etablissements <|
                                            (\q ->
                                                if q == "" then
                                                    Nothing

                                                else
                                                    Just q
                                            )
                                            <|
                                                filtersAndPaginationToQuery
                                                    ( { emptyFilters
                                                        | categoriesJuridiques =
                                                            [ { id = catJurId
                                                              , nom = catJur
                                                              , niveau = 2
                                                              , parentId = ""
                                                              }
                                                            ]
                                                      }
                                                    , defaultPagination
                                                    )
                                    )
                                    [ Html.Attributes.title "Rechercher les établissements avec la même catégorie juridique" ]
                                    [ DSFR.Icons.iconSM DSFR.Icons.System.searchLine
                                    ]
                            )
                            categorieJuridique
                            categorieJuridiqueId
                            |> Maybe.withDefault nothing
                        ]
                    )
                , Lib.UI.infoLineHtml Nothing
                    "Forme juridique"
                    (Maybe.withDefault "-" formeJuridique)
                    (span []
                        [ text (Maybe.withDefault "-" formeJuridique)
                        , viewMaybe (\_ -> text " ") formeJuridique
                        , Maybe.map2
                            (\catJur catJurId ->
                                Typo.link
                                    (Route.toUrl <|
                                        Route.Etablissements <|
                                            (\q ->
                                                if q == "" then
                                                    Nothing

                                                else
                                                    Just q
                                            )
                                            <|
                                                filtersAndPaginationToQuery
                                                    ( { emptyFilters
                                                        | categoriesJuridiques =
                                                            [ { id = catJurId
                                                              , nom = catJur
                                                              , niveau = 3
                                                              , parentId = ""
                                                              }
                                                            ]
                                                      }
                                                    , defaultPagination
                                                    )
                                    )
                                    [ Html.Attributes.title "Rechercher les établissements avec la même forme juridique" ]
                                    [ DSFR.Icons.iconSM DSFR.Icons.System.searchLine
                                    ]
                            )
                            formeJuridique
                            formeJuridiqueId
                            |> Maybe.withDefault nothing
                        ]
                    )
                , Lib.UI.infoLine Nothing "Micro-entreprise" <|
                    case micro of
                        Just True ->
                            "Oui"

                        Just False ->
                            "Non"

                        Nothing ->
                            "Inconnu"
                , Lib.UI.infoLine Nothing ("Zonage" ++ plural (List.length zonagesPrioritaires)) <|
                    case zonagesPrioritaires of
                        [] ->
                            "-"

                        _ ->
                            String.join "\u{00A0}— " <|
                                zonagesPrioritaires
                , Lib.UI.infoLine Nothing "Date de création" <| Maybe.withDefault "-" <| Maybe.map Lib.Date.formatDateShort <| dateCreationEtablissement
                , Lib.UI.infoLine Nothing "Date de fermeture" <| Maybe.withDefault "-" <| Maybe.map Lib.Date.formatDateShort <| dateFermetureEtablissement
                ]
            ]
        ]


viewLinks : Maybe ( Float, Float ) -> String -> String -> Html msg
viewLinks geolocalisation nom siret =
    div [ class "p-2" ]
        [ div [ Typo.textBold ] [ text "Liens" ]
        , hr [ class "!pb-4" ] []
        , div []
            [ div []
                [ span [ Typo.textXs ] [ text "Géolocalisation de l'établissement\u{00A0}: " ]
                , case geolocalisation of
                    Nothing ->
                        span
                            [ Typo.textXs ]
                            [ text "Indisponible" ]

                    Just ( latitude, longitude ) ->
                        let
                            lien =
                                Lib.Umap.umapLinkFeature
                                    { nom = nom
                                    , siret = siret
                                    , latitude = latitude
                                    , longitude = longitude
                                    }
                        in
                        Typo.externalLink lien
                            [ Typo.textXs ]
                            [ text "voir sur umap" ]
                ]
            ]
        , div []
            [ div []
                [ span [ Typo.textXs ] [ text "Annuaire des entreprises\u{00A0}: " ]
                , Typo.externalLink ("https://annuaire-entreprises.data.gouv.fr/etablissement/" ++ siret)
                    [ Typo.textXs ]
                    [ text "https://annuaire-entreprises.data.gouv.fr" ]
                ]
            ]
        , div []
            [ span [ Typo.textXs ] [ text "Données Bodacc\u{00A0}: " ]
            , Typo.externalLink ("https://annuaire-entreprises.data.gouv.fr/annonces/" ++ String.left 9 siret)
                [ Typo.textXs ]
                [ text ("https://annuaire-entreprises.data.gouv.fr/annonces/" ++ String.left 9 siret) ]
            ]
        ]


viewEtablissementData : Bool -> Maybe EtablissementEtiquettes -> List Zonage -> MultiSelectRemote.SmartSelect Msg String -> MultiSelectRemote.SmartSelect Msg String -> MultiSelectRemote.SmartSelect Msg String -> WebData EtablissementWithExtraInfo -> Etiquettes -> Html Msg
viewEtablissementData forPrint editEtablissementEtiquettes zonages selectActivites selectLocalisations selectMots saveEtablissementEtiquettesRequest etiquettes =
    div [] <|
        [ Lazy.lazy viewEtablissementDataHeader editEtablissementEtiquettes
        , hr [ class "fr-hr" ] []
        , div [] <|
            List.singleton <|
                viewEtiquettesSelects forPrint editEtablissementEtiquettes zonages selectActivites selectLocalisations selectMots saveEtablissementEtiquettesRequest etiquettes
        ]


viewEtablissementDataHeader : Maybe EtablissementEtiquettes -> Html Msg
viewEtablissementDataHeader editEtablissementEtiquettes =
    div [ Typo.textBold, class "flex flex-row justify-between items-center" ]
        [ span [ Typo.textBold, class "p-2" ] [ text "Étiquettes" ]
        , DSFR.Button.new { label = "Modifier l'établissement", onClick = Just EditEtablissementEtiquettes }
            |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
            |> DSFR.Button.withDisabled (editEtablissementEtiquettes /= Nothing)
            |> DSFR.Button.withAttrs [ id "modifier-etablissement", classList [ ( "invisible", editEtablissementEtiquettes /= Nothing ) ] ]
            |> DSFR.Button.tertiaryNoOutline
            |> DSFR.Button.view
        ]


viewEtiquettesSelects : Bool -> Maybe EtablissementEtiquettes -> List Zonage -> MultiSelectRemote.SmartSelect Msg String -> MultiSelectRemote.SmartSelect Msg String -> MultiSelectRemote.SmartSelect Msg String -> WebData EtablissementWithExtraInfo -> Etiquettes -> Html Msg
viewEtiquettesSelects forPrint editEtablissementEtiquettes zonages selectActivites selectLocalisations selectMots saveEtablissementEtiquettesRequest { activites, localisations, motsCles } =
    let
        classAttrs =
            class "flex flex-col px-4 h-full gap-4"

        affichageEtiquettes =
            div [ classAttrs ]
                [ div [ class "flex flex-col gap-2" ]
                    [ div [ Typo.textBold ]
                        [ span [] [ text "Activités réelles et filières", sup [ Html.Attributes.title hintActivites ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                        ]
                    , case activites of
                        [] ->
                            text "-"

                        _ ->
                            activites
                                |> List.map
                                    (\t ->
                                        { data = t, toString = identity }
                                            |> DSFR.Tag.clickable
                                                (Route.toUrl <|
                                                    Route.Etablissements <|
                                                        (\q ->
                                                            if q == "" then
                                                                Nothing

                                                            else
                                                                Just q
                                                        )
                                                        <|
                                                            filtersAndPaginationToQuery
                                                                ( { emptyFilters
                                                                    | activites = [ t ]
                                                                  }
                                                                , defaultPagination
                                                                )
                                                )
                                    )
                                |> DSFR.Tag.medium
                    ]
                , div [ class "flex flex-col gap-2" ]
                    [ div [ Typo.textBold ] [ span [] [ text "Zone géographique", sup [ Html.Attributes.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ] ]
                    , let
                        locs =
                            localisations
                                |> List.map
                                    (\t ->
                                        { data = t, toString = identity }
                                            |> DSFR.Tag.clickable
                                                (Route.toUrl <|
                                                    Route.Etablissements <|
                                                        (\q ->
                                                            if q == "" then
                                                                Nothing

                                                            else
                                                                Just q
                                                        )
                                                        <|
                                                            filtersAndPaginationToQuery
                                                                ( { emptyFilters
                                                                    | zones = [ t ]
                                                                  }
                                                                , defaultPagination
                                                                )
                                                )
                                    )

                        zons =
                            zonages
                                |> List.map .nom
                                |> List.map
                                    (\t ->
                                        { data = t, toString = identity }
                                            |> DSFR.Tag.clickable ""
                                    )
                      in
                      case locs ++ zons of
                        [] ->
                            text "-"

                        _ ->
                            (locs ++ zons)
                                |> DSFR.Tag.medium
                    ]
                , div [ class "flex flex-col gap-2" ]
                    [ div [ Typo.textBold ] [ span [] [ text "Mots-clés", sup [ Html.Attributes.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ] ]
                    , case motsCles of
                        [] ->
                            text "-"

                        _ ->
                            motsCles
                                |> List.map
                                    (\t ->
                                        { data = t, toString = identity }
                                            |> DSFR.Tag.clickable
                                                (Route.toUrl <|
                                                    Route.Etablissements <|
                                                        (\q ->
                                                            if q == "" then
                                                                Nothing

                                                            else
                                                                Just q
                                                        )
                                                        <|
                                                            filtersAndPaginationToQuery
                                                                ( { emptyFilters
                                                                    | mots = [ t ]
                                                                  }
                                                                , defaultPagination
                                                                )
                                                )
                                    )
                                |> DSFR.Tag.medium
                    ]
                ]
    in
    case ( forPrint, editEtablissementEtiquettes ) of
        ( True, _ ) ->
            affichageEtiquettes

        ( False, Nothing ) ->
            affichageEtiquettes

        ( False, Just infos ) ->
            let
                noDifference =
                    isPermutationOf infos.activites activites
                        && isPermutationOf infos.localisations localisations
                        && isPermutationOf infos.mots motsCles
            in
            formWithListeners [ Events.onSubmit <| RequestedSaveEtablissementEtiquettes, classAttrs, class "border-[0.1rem] border-france-blue py-[0.4rem]" ]
                [ div []
                    [ div [ class "flex flex-col gap-4" ]
                        [ selectActivites
                            |> MultiSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = infos.activites
                                , optionLabelFn = identity
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle = span [ Typo.textBold ] [ text "Activités réelles et filières", sup [ Html.Attributes.title hintActivites ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                , viewSelectedOptionFn = text
                                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg =
                                    \searchText ->
                                        "Aucune autre activité n'a été trouvée"
                                            ++ (if searchText == "" then
                                                    ""

                                                else
                                                    " pour " ++ searchText
                                               )
                                , noOptionsMsg = "Aucune activité n'a été trouvée"
                                , newOption = Just identity
                                , searchPrompt = "Rechercher une activité"
                                }
                        , infos.activites
                            |> List.map (\activite -> DSFR.Tag.deletable UnselectActivite { data = activite, toString = identity })
                            |> DSFR.Tag.medium
                        ]
                    ]
                , div []
                    [ div [ class "flex flex-col gap-4" ]
                        [ selectLocalisations
                            |> MultiSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = infos.localisations
                                , optionLabelFn = identity
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle = span [ Typo.textBold ] [ text "Zone géographique", sup [ Html.Attributes.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                , viewSelectedOptionFn = text
                                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg =
                                    \searchText ->
                                        "Aucune autre zone géographique n'a été trouvée"
                                            ++ (if searchText == "" then
                                                    ""

                                                else
                                                    " pour " ++ searchText
                                               )
                                , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                                , newOption = Just identity
                                , searchPrompt = "Rechercher une zone géographique"
                                }
                        , let
                            locs =
                                infos.localisations
                                    |> List.map
                                        (\localisation ->
                                            DSFR.Tag.deletable UnselectLocalisation { data = localisation, toString = identity }
                                        )

                            zons =
                                zonages
                                    |> List.map .nom
                                    |> List.map
                                        (\zonage ->
                                            DSFR.Tag.clickable "" { data = zonage, toString = identity }
                                                |> DSFR.Tag.withExtraAttrs [ Html.Attributes.title "Cette étiquette a été ajoutée automatiquement via un zonage personnalisée et ne peut être supprimée ici" ]
                                        )
                          in
                          (locs ++ zons)
                            |> DSFR.Tag.medium
                        ]
                    ]
                , div []
                    [ div [ class "flex flex-col gap-4" ]
                        [ selectMots
                            |> MultiSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = infos.mots
                                , optionLabelFn = identity
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle = span [ Typo.textBold ] [ text "Mots-clés", sup [ Html.Attributes.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                , viewSelectedOptionFn = text
                                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg =
                                    \searchText ->
                                        "Aucun autre mot-clé n'a été trouvé"
                                            ++ (if searchText == "" then
                                                    ""

                                                else
                                                    " pour " ++ searchText
                                               )
                                , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                                , newOption = Just identity
                                , searchPrompt = "Rechercher un mot-clé"
                                }
                        , infos.mots
                            |> List.map (\mot -> DSFR.Tag.deletable UnselectMot { data = mot, toString = identity })
                            |> DSFR.Tag.medium
                        ]
                    ]
                , case saveEtablissementEtiquettesRequest of
                    RD.NotAsked ->
                        nothing

                    RD.Loading ->
                        nothing

                    RD.Failure _ ->
                        DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite" }
                            |> DSFR.Alert.alert Nothing DSFR.Alert.error

                    RD.Success _ ->
                        DSFR.Alert.small { title = Nothing, description = text "Modifications enregistrées\u{00A0}!" }
                            |> DSFR.Alert.alert Nothing DSFR.Alert.success
                , [ DSFR.Button.new { onClick = Nothing, label = "Valider" }
                        |> DSFR.Button.withDisabled (noDifference || saveEtablissementEtiquettesRequest == RD.Loading)
                        |> DSFR.Button.submit
                  , DSFR.Button.new { onClick = Just CancelEditEtablissementEtiquettes, label = "Annuler" }
                        |> DSFR.Button.secondary
                        |> DSFR.Button.withDisabled (saveEtablissementEtiquettesRequest == RD.Loading)
                  ]
                    |> DSFR.Button.group
                    |> DSFR.Button.inline
                    |> DSFR.Button.alignedRightInverted
                    |> DSFR.Button.viewGroup
                ]


requestToggleFavorite : String -> Bool -> Cmd Msg
requestToggleFavorite siret favorite =
    post
        { url = Api.toggleFavoriteEtablissement siret
        , body =
            [ ( "favori", Encode.bool favorite )
            ]
                |> Encode.object
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler =
            \webdata ->
                ReceivedToggleFavoriteResult <|
                    case webdata of
                        RD.Success _ ->
                            favorite

                        _ ->
                            not favorite
        }
        (Decode.succeed ())


requestToggleSignaleFerme : Bool -> String -> Cmd Msg
requestToggleSignaleFerme signaleFerme siret =
    post
        { url = Api.toggleSignaleFermeEtablissement siret
        , body =
            [ ( "cloture", Encode.bool signaleFerme )
            ]
                |> Encode.object
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedToggleSignaleFermeResult
        }
        (Decode.field "clotureDateContribution" <| Decode.maybe <| Lib.Date.decodeCalendarDate)


fetchEtablissement : String -> Cmd Msg
fetchEtablissement siret =
    getWithError
        { url = Api.getEtablissement siret }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedEtablissement
        }
        (Decode.oneOf
            [ Decode.field "equipeEtablissement" Data.decodeEtablissementWithExtraInfo |> Decode.map Ok
            , Decode.at [ "error", "message" ] Decode.string |> Decode.map Err
            ]
        )


selectActivitesConfig : SelectConfig String
selectActivitesConfig =
    { headers = []
    , url = Api.rechercheActivite
    , optionDecoder = decodeEtiquetteList
    }


selectLocalisationsConfig : SelectConfig String
selectLocalisationsConfig =
    { headers = []
    , url = Api.rechercheLocalisation
    , optionDecoder = decodeEtiquetteList
    }


selectMotsConfig : SelectConfig String
selectMotsConfig =
    { headers = []
    , url = Api.rechercheMot
    , optionDecoder = decodeEtiquetteList
    }


updateEtablissementEtiquettes : String -> EtablissementEtiquettes -> Effect.Effect Shared.Msg Msg
updateEtablissementEtiquettes siret { activites, localisations, mots } =
    let
        jsonBody =
            [ ( "activites", Encode.list Encode.string activites )
            , ( "localisations", Encode.list Encode.string localisations )
            , ( "motsCles", Encode.list Encode.string mots )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    post
        { url = Api.updateEtablissementEtiquettes siret
        , body = jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveEtablissementEtiquettes
        }
        (Decode.field "equipeEtablissement" Data.decodeEtablissementWithExtraInfo)
        |> Effect.fromCmd


updateEtablissementCommentaire : String -> String -> Effect.Effect Shared.Msg Msg
updateEtablissementCommentaire siret description =
    let
        jsonBody =
            [ ( "description", Encode.string description )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    post
        { url = Api.updateEtablissementCommentaire siret
        , body = jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveEtablissementCommentaire
        }
        (Decode.field "equipeEtablissement" Data.decodeEtablissementWithExtraInfo)
        |> Effect.fromCmd


createContact : String -> NewContactForm -> Cmd Msg
createContact siret newContactForm =
    let
        httpBody =
            encodeNewContactForm newContactForm
    in
    post
        { url = Api.createEtablissementContact siret
        , body = httpBody |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveContact
        }
        (Decode.field "equipeEtablissement" Data.decodeEtablissementWithExtraInfo)


editContact : String -> Contact -> Cmd Msg
editContact siret contact =
    post
        { url = Api.getEtablissementContact siret contact.personne.id
        , body =
            contact
                |> Data.Contact.encodeContact ContactUpdateInput
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveContact
        }
        (Decode.field "equipeEtablissement" Data.decodeEtablissementWithExtraInfo)


deleteContact : String -> Contact -> Cmd Msg
deleteContact siret { personne } =
    delete
        { url = Api.getEtablissementContact siret personne.id
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveContact
        }
        (Decode.field "equipeEtablissement" Data.decodeEtablissementWithExtraInfo)


getZonages : Siret -> Effect.Effect Shared.Msg Msg
getZonages siret =
    Effect.fromCmd <|
        get
            { url = Api.getEtablissementZonages siret
            }
            { toShared = SharedMsg
            , logger = Just LogError
            , handler = ReceivedZonages
            }
            (Decode.list <|
                decodeZonage
            )


fetchApiEntrepriseData : String -> Cmd Msg
fetchApiEntrepriseData siren =
    get
        { url = Api.getApiEntrepriseData siren }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedApiEntrepriseData
        }
    <|
        decodeApiEntrepriseData


decodeApiEntrepriseData : Decoder ApiEntrepriseData
decodeApiEntrepriseData =
    Decode.succeed ApiEntrepriseData
        |> andMap
            (Decode.field "exercices" <|
                Decode.nullable <|
                    Decode.map (List.filterMap identity) <|
                        Decode.list Data.decodeCa
            )
        |> andMap
            (Decode.map2 (Maybe.map2 Tuple.pair)
                (Decode.field "mandatairesPersonnesPhysiques" <|
                    Decode.nullable <|
                        Decode.list Data.decodeMandatairePersonnePhysique
                )
                (Decode.field "mandatairesPersonnesMorales" <|
                    Decode.nullable <|
                        Decode.list Data.decodeMandatairePersonneMorale
                )
            )
        |> andMap
            (Decode.field "beneficiairesEffectifs" <|
                Decode.nullable <|
                    Decode.list Data.decodeBeneficiaireEffectif
            )
        |> andMap
            (Decode.field "liasseFiscale" <|
                Decode.nullable <|
                    Data.decodeLiasseFiscale
            )


requestEnseigneContributionSave : WebData (Result String EtablissementWithExtraInfo) -> Maybe String -> Cmd Msg
requestEnseigneContributionSave etablissementWithExtraInfo enseigneContribution =
    case ( etablissementWithExtraInfo, enseigneContribution ) of
        ( RD.Success (Ok { etablissement }), Just enseigne ) ->
            post
                { url = Api.updateEtablissementEnseigneContribution etablissement.siret
                , body =
                    Encode.object [ ( "enseigne", Encode.string enseigne ) ]
                        |> Http.jsonBody
                }
                { toShared = SharedMsg
                , logger = Just LogError
                , handler = ReceivedEnseigneContributionSave
                }
                (Decode.field "equipeEtablissement" Data.decodeEtablissementWithExtraInfo)

        _ ->
            Cmd.none


requestGeolocalisationContributionSave : WebData (Result String EtablissementWithExtraInfo) -> Maybe GeolocalisationContribution -> Cmd Msg
requestGeolocalisationContributionSave etablissementWithExtraInfo geolocalisationContribution =
    case ( etablissementWithExtraInfo, geolocalisationContribution ) of
        ( RD.Success (Ok { etablissement }), Just { nouvelleGeolocalisation } ) ->
            post
                { url = Api.updateEtablissementGeolocalisationContribution etablissement.siret
                , body =
                    Encode.object
                        [ ( "geolocalisation"
                          , nouvelleGeolocalisation
                                |> Maybe.map (\( lon, lat ) -> [ lon, lat ])
                                |> Maybe.map (Encode.list Encode.float)
                                |> Maybe.withDefault Encode.null
                          )
                        ]
                        |> Http.jsonBody
                }
                { toShared = SharedMsg
                , logger = Just LogError
                , handler = ReceivedGeolocalisationContributionSave
                }
                (Decode.field "equipeEtablissement" Data.decodeEtablissementWithExtraInfo)

        _ ->
            Cmd.none


createRessource : String -> Ressource -> Cmd Msg
createRessource siret ressource =
    post
        { url = Api.createRessourceEtablissement siret
        , body =
            ressource
                |> encodeRessource
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveRessource
        }
        (Decode.field "equipeEtablissement" Data.decodeEtablissementWithExtraInfo)


deleteRessource : String -> Ressource -> Cmd Msg
deleteRessource siret ressource =
    delete
        { url = Api.editRessourceEtablissement siret ressource.id
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveRessource
        }
        (Decode.field "equipeEtablissement" Data.decodeEtablissementWithExtraInfo)


editRessource : String -> Ressource -> Cmd Msg
editRessource siret ressource =
    post
        { url = Api.editRessourceEtablissement siret ressource.id
        , body =
            ressource
                |> encodeRessource
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveRessource
        }
        (Decode.field "equipeEtablissement" Data.decodeEtablissementWithExtraInfo)


getEquipeEtablissementSuivis : String -> Cmd Msg
getEquipeEtablissementSuivis siret =
    get
        { url = Api.getEquipeEtablissementSuivis siret }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSuivis
        }
    <|
        (Decode.field "suivis" <|
            Decode.list <|
                decodeSuiviEquipe
        )


decodeSuiviEquipe : Decoder SuiviEquipe
decodeSuiviEquipe =
    Decode.succeed SuiviEquipe
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (optionalNullableField "contacts" Decode.int)
        |> andMap (optionalNullableField "echanges" Decode.int)
        |> andMap (optionalNullableField "demandes" Decode.int)
        |> andMap (optionalNullableField "rappels" Decode.int)


getPersonnes : String -> Cmd Msg
getPersonnes query =
    get
        { url = Api.getContacts query }
        { toShared = SharedMsg
        , logger = Nothing
        , handler = ReceivedContactsExistants
        }
        (Decode.field "elements" <| Decode.list <| decodePersonne)


savePartage : Siret -> Partage -> Cmd Msg
savePartage siret { collegueId, commentaire } =
    post
        { url = Api.partageEtablissement siret
        , body =
            [ ( "compteId", Maybe.withDefault Encode.null <| Maybe.map Api.EntityId.encodeEntityId <| collegueId )
            , ( "commentaire", Encode.string commentaire )
            ]
                |> Encode.object
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedPartage
        }
        (Decode.succeed ())


queryKeys :
    { onglet : String
    , suivi : String
    }
queryKeys =
    { onglet = "onglet"
    , suivi = "suivi"
    }


queryToOnglets : Maybe String -> ( Onglet, UI.Suivi.Onglet )
queryToOnglets rawQuery =
    rawQuery
        |> Maybe.withDefault ""
        |> QS.parse QS.config
        |> (\query ->
                let
                    onglet =
                        query
                            |> QS.getAsStringList queryKeys.onglet
                            |> List.head
                            |> Maybe.andThen stringToOnglet
                            |> Maybe.withDefault OngletEtablissement

                    ongletSuivi =
                        query
                            |> QS.getAsStringList queryKeys.suivi
                            |> List.head
                            |> Maybe.andThen UI.Suivi.stringToOnglet
                            |> Maybe.withDefault UI.Suivi.OngletEchanges
                in
                ( onglet, ongletSuivi )
           )


ongletsToQuery : ( Onglet, UI.Suivi.Onglet ) -> String
ongletsToQuery ( onglet, ongletSuivi ) =
    let
        setOnglet =
            case onglet of
                OngletEtablissement ->
                    identity

                _ ->
                    onglet
                        |> ongletToString
                        |> QS.setStr queryKeys.onglet

        setOngletSuivi =
            case onglet of
                OngletSuivi ->
                    ongletSuivi
                        |> UI.Suivi.ongletToString
                        |> QS.setStr queryKeys.suivi

                _ ->
                    identity
    in
    QS.empty
        |> setOnglet
        |> setOngletSuivi
        |> QS.serialize (QS.config |> QS.addQuestionMark False)


updateUrl : ( Onglet, UI.Suivi.Onglet ) -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
updateUrl ( onglet, ongletSuivi ) model =
    let
        ( newSuiviModel, suiviCmd, _ ) =
            UI.Suivi.changeOnglet (UI.Suivi.suiviConfigEtab (Decode.field "equipeEtablissement" Data.decodeEtablissementWithExtraInfo) model.siret) model.suiviModel ongletSuivi

        newOngletSuivi =
            UI.Suivi.getOngletActif newSuiviModel

        nextUrl =
            ongletsToQuery ( onglet, newOngletSuivi )

        requestChanged =
            nextUrl /= model.currentUrl

        ongletCmd =
            Effect.fromShared <|
                Shared.Navigate <|
                    Route.Etablissement <|
                        ( nextUrl
                            |> (\q ->
                                    if q == "" then
                                        Nothing

                                    else
                                        Just q
                               )
                        , model.siret
                        )
    in
    if requestChanged then
        ( { model
            | onglet = onglet
            , suiviModel = newSuiviModel
            , currentUrl = nextUrl
          }
        , Effect.batch
            [ ongletCmd
            , Effect.map SuiviMsg suiviCmd
            ]
        )

    else
        model
            |> Effect.withNone


stringtoPoint : String -> Maybe ( Float, Float )
stringtoPoint string =
    string
        |> String.split ","
        |> List.map String.trim
        |> List.map String.toFloat
        |> (\list ->
                case list of
                    [ Just lon, Just lat ] ->
                        let
                            isLonValid =
                                lon >= -180 && lon <= 180

                            isLatValid =
                                lat >= -90 && lat <= 90
                        in
                        if isLonValid && isLatValid then
                            Just ( lon, lat )

                        else
                            Nothing

                    _ ->
                        Nothing
           )


pointToString : ( Float, Float ) -> String
pointToString ( lon, lat ) =
    [ String.fromFloat lon
    , String.fromFloat lat
    ]
        |> String.join ", "


centreDeLaFrance : ( Float, Float )
centreDeLaFrance =
    ( 2.8906249999922693, 46.81809179606458 )


encodePoint : ( Float, Float ) -> Encode.Value
encodePoint ( lon, lat ) =
    [ lon, lat ]
        |> Encode.list Encode.float


selectAdresseConfig : SingleSelectRemote.SelectConfig ApiAdresse
selectAdresseConfig =
    { headers = []
    , url = Api.rechercheAdresse Nothing Nothing Nothing
    , optionDecoder = decodeApiFeatures
    }


viewEntrepriseGraphs : String -> WebData Data.Participations -> Html msg
viewEntrepriseGraphs siren participations =
    div [ class "flex flex-row gap-4 !mb-8" ]
        [ div [ class "!border-2 !dark-grey-border h-[500px] w-[80%]" ]
            [ case participations of
                RD.Success [] ->
                    div [ class "flex flex-row items-center justify-center h-full" ] [ text "Aucune participation connue" ]

                RD.Success parts ->
                    Html.node "graph-network"
                        [ Encode.object
                            [ ( "values"
                              , parts |> Encode.list (\( a, b ) -> Encode.list Encode.string [ a, b ])
                              )
                            , ( "siren"
                              , siren |> Encode.string
                              )
                            ]
                            |> Encode.encode 0
                            |> Html.Attributes.attribute "data"
                        , class "w-full"
                        ]
                        []

                RD.Loading ->
                    div [ class "flex flex-row items-center justify-center h-full" ] [ text "Chargement des participations en cours..." ]

                RD.NotAsked ->
                    div [ class "flex flex-row items-center justify-center h-full" ] [ text "Chargement des participations en cours..." ]

                RD.Failure _ ->
                    div [ class "flex flex-row items-center justify-center h-full" ] [ text "Une erreur s'est produite, veuillez réessayer." ]
            ]
        , div [ class "flex flex-col gap-4" ]
            [ div [ Typo.textBold ] [ text "Légende" ]
            , div [ class "flex flex-col gap-2" ]
                [ div [ class "flex flex-row items-center gap-2" ]
                    [ div [ class "w-[4em] flex flex-row justify-center" ] [ span [ class "inline-block h-[1em] w-[1em]", style "background-color" "#929292", style "border-radius" "50%" ] [] ]
                    , div [ class "w-full" ] [ text "Une entreprise" ]
                    ]
                , div [ class "flex flex-row items-center gap-2" ]
                    [ div [ class "w-[4em] flex flex-row justify-center", style "color" "#929292" ]
                        [ text "A"
                        , DSFR.Icons.icon DSFR.Icons.System.arrowRightLine
                        , text "B"
                        ]
                    , div [ class "w-full" ] [ text "A détient B" ]
                    ]
                , div [ class "flex flex-row items-center gap-2" ]
                    [ div [ class "w-[4em] flex flex-row justify-center" ] [ span [ class "inline-block h-[1em] w-[2em]", style "background-color" "#000091" ] [] ]
                    , div [ class "w-full" ] [ text "Entreprise actuelle" ]
                    ]
                , div [ class "flex flex-row items-center gap-2" ]
                    [ div [ class "w-[4em] flex flex-row justify-center" ] [ span [ class "inline-block h-[1em] w-[2em]", style "background-color" "#C9191E" ] [] ]
                    , div [ class "w-full" ] [ text "Autres entreprises" ]
                    ]
                ]
            ]
        ]


getEntrepriseParticipations : String -> Cmd Msg
getEntrepriseParticipations siret =
    let
        siren =
            String.left 9 siret
    in
    get
        { url = Api.getEntrepriseParticipations siren }
        { toShared = SharedMsg
        , logger = Nothing
        , handler = ReceivedParticipations
        }
        (Decode.field "entrepriseParticipation" <| decodeParticipations)


viewParticipationsModal : String -> Bool -> WebData Data.Participations -> Html Msg
viewParticipationsModal siren voir participations =
    let
        ( opened, content, title ) =
            if voir then
                ( True
                , viewEntrepriseGraphs siren participations
                , text "Graphe des participations"
                )

            else
                ( False, nothing, nothing )
    in
    DSFR.Modal.view
        { id = "participations-entreprise"
        , label = "participations-entreprise"
        , openMsg = NoOp
        , closeMsg = Just ClickedFermerParticipations
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.xl
        }
        content
        Nothing
        |> Tuple.first


avecVoirPlus : Bool -> Msg -> (data -> Html Msg) -> Maybe (List data) -> List (Html Msg)
avecVoirPlus voirTout voirToutMsg viewer list =
    if (list |> Maybe.withDefault [] |> List.length) <= 10 then
        List.map viewer <|
            Maybe.withDefault [] <|
                list

    else
        List.reverse <|
            (if voirTout then
                (::)
                    (div [ class "pt-2 text-center" ]
                        [ DSFR.Button.new { onClick = Just voirToutMsg, label = "Voir moins" }
                            |> DSFR.Button.secondary
                            |> DSFR.Button.view
                        ]
                    )

             else
                (::)
                    (div [ class "pt-2 text-center" ]
                        [ DSFR.Button.new { onClick = Just ToggleVoirDetentions, label = "Voir plus" }
                            |> DSFR.Button.secondary
                            |> DSFR.Button.view
                        ]
                    )
            )
            <|
                List.reverse <|
                    List.map viewer <|
                        (if voirTout then
                            identity

                         else
                            List.take 9
                        )
                        <|
                            Maybe.withDefault [] <|
                                list
