module Pages.Etablissement.Data exposing (BeneficiaireEffectif, Detention, Effectif, EntrepriseData, EquipeEtablissementData, EtablissementCreationLie, EtablissementExtra, EtablissementHistorique, EtablissementWithExtraInfo, Exercice, ExerciceInpi, Filiale, LiasseFiscale, MandatairePersonneMorale, MandatairePersonnePhysique, Participations, ProcedureCollective, Subvention, decodeBeneficiaireEffectif, decodeCa, decodeEtablissementWithExtraInfo, decodeLiasseFiscale, decodeMandatairePersonneMorale, decodeMandatairePersonnePhysique, decodeParticipations)

import Api.EntityId exposing (EntityId, decodeEntityId)
import Data.Contact exposing (Contact)
import Data.Demande exposing (Demande, decodeDemande)
import Data.Echange exposing (Echange, decodeEchange)
import Data.Etablissement exposing (EntrepriseType, EtablissementSimple, EtablissementSimpleAvecPortefeuille, decodeEntrepriseType, decodeEtablissementSimpleAvecPortefeuille, decodeNaf, decodeNafId)
import Data.EtablissementCreationId exposing (EtablissementCreationId)
import Data.Etiquette exposing (Etiquettes, decodeEtiquettes)
import Data.Local exposing (LocalSimple, decodeLocalSimple)
import Data.Personne exposing (Personne, decodePersonne)
import Data.PortefeuilleInfos exposing (PortefeuilleInfos, decodePortefeuilleInfos)
import Data.Rappel exposing (Rappel, decodeRappel)
import Data.Ressource exposing (Ressource, decodeRessource)
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Lib.Date
import Time exposing (Posix)


type alias EtablissementHistorique =
    { predecesseurs : List EtablissementSimpleAvecPortefeuille
    , successeurs : List EtablissementSimpleAvecPortefeuille
    }


type alias EtablissementWithExtraInfo =
    { etablissement : EtablissementSimple
    , portefeuilleInfos : PortefeuilleInfos
    , etablissementExtra : EtablissementExtra
    , entreprise : Maybe EntrepriseData
    , equipeEtablissement : EquipeEtablissementData
    }


type alias EquipeEtablissementData =
    { description : String
    , etiquettes : Etiquettes
    , contacts : List Contact
    , echanges : List Echange
    , demandes : List Demande
    , rappels : List Rappel
    , createursLies : List EtablissementCreationLie
    , modificationAuteurDate : Maybe ( String, Posix )
    , freres : List EtablissementSimpleAvecPortefeuille
    , historique : EtablissementHistorique
    , ressources : List Ressource
    , occupations : List LocalSimple
    }


type alias EtablissementCreationLie =
    { id : EntityId EtablissementCreationId
    , nom : String
    }


type alias EtablissementExtra =
    { categorieNaf : Maybe String
    , categorieNafId : Maybe String
    , emms : List Effectif
    , ema : Maybe Effectif
    , zonagesPrioritaires : List String
    , acv : Bool
    , pvd : Bool
    , va : Bool
    , subventions : List Subvention
    , sireneUpdate : Maybe Date
    , enseigne : Maybe String
    }


type alias EntrepriseData =
    { siren : String
    , entrepriseNom : String
    , siegeAdresse : String
    , categorieJuridique : Maybe String
    , categorieJuridiqueId : Maybe String
    , formeJuridique : Maybe String
    , formeJuridiqueId : Maybe String
    , exercices : Maybe (List Exercice)
    , inpi : List ExerciceInpi
    , inpiCaVariation : Maybe Float
    , ess : Bool
    , micro : Bool
    , mandatairesPersonnesPhysiques : Maybe (List MandatairePersonnePhysique)
    , mandatairesPersonnesMorales : Maybe (List MandatairePersonneMorale)
    , beneficiairesEffectifs : Maybe (List BeneficiaireEffectif)
    , liasseFiscale : Maybe LiasseFiscale
    , dateCreationEntreprise : Maybe Date
    , dateFermetureEntreprise : Maybe Date
    , effectifInsee : String
    , active : Bool
    , procedure : Bool
    , procedures : List ProcedureCollective
    , activiteNaf : Maybe String
    , activiteNafId : Maybe String
    , categorieNaf : Maybe String
    , categorieNafId : Maybe String
    , categorieEntreprise : EntrepriseType
    , etablissementsActifs : Maybe { territoire : Int, france : Int }
    , effectifTotalTerritoire : Maybe Float
    , egapro : Maybe Egapro
    }


type alias Egapro =
    { note : Maybe Int
    , ecartRemunerations : Maybe Int
    , ecartAugmentations : Maybe Int
    , retourMaternite : Maybe Int
    , hautesRemunerations : Maybe Int
    , cadres : Maybe Float
    , instances : Maybe Float
    , annee : Int
    }


type alias Participations =
    List ( String, String )


type alias LiasseFiscale =
    { aFiliales : Maybe Bool
    , estDetenue : Maybe Bool
    , sirensMere : ( Maybe String, Maybe String )
    , filiales : Maybe (List Filiale)
    , detentions : Maybe (List Detention)
    }


type alias Subvention =
    { type_ : String
    , montant : String
    , objet : String
    , annee : String
    }


type alias Exercice =
    { ca : String
    , annee : String
    , date : String
    }


type alias ExerciceInpi =
    { ca : String
    , marge : String
    , ebe : String
    , resultatNet : String
    , annee : String
    }


type alias ProcedureCollective =
    { date : String
    , famille : String
    , nature : String
    , annee : String
    }


type alias MandatairePersonnePhysique =
    { fonction : String
    , personne : Personne
    }


type alias MandatairePersonneMorale =
    { fonction : String
    , nom : String
    , siren : String
    }


type alias BeneficiaireEffectif =
    { nom : String
    , nomUsage : String
    , prenoms : String
    , naissanceDate : Maybe Date
    , totalParts : Float
    }


type alias Filiale =
    { nom : Maybe String
    , siren : Maybe String
    , participation : Maybe Float
    }


type alias Detention =
    { nom : Maybe String
    , siren : Maybe String
    , detention : Maybe Float
    , parts : Maybe Float
    }


decodeEtablissementWithExtraInfo : Decoder EtablissementWithExtraInfo
decodeEtablissementWithExtraInfo =
    Decode.succeed EtablissementWithExtraInfo
        |> andMap (Decode.field "etablissement" Data.Etablissement.decodeEtablissementSimple)
        |> andMap (Decode.field "portefeuilleInfos" decodePortefeuilleInfos)
        |> andMap (Decode.field "etablissementExtra" decodeEtablissementExtra)
        |> andMap
            (Decode.field "entreprise" <|
                Decode.nullable <|
                    decodeEntreprise
            )
        |> andMap (Decode.field "equipeEtablissement" <| decodeEquipeEtablissement)


decodeEquipeEtablissement : Decoder EquipeEtablissementData
decodeEquipeEtablissement =
    Decode.succeed EquipeEtablissementData
        |> andMap (Decode.field "description" <| Decode.string)
        |> andMap (Decode.field "etiquettes" <| decodeEtiquettes)
        |> andMap (Decode.field "contacts" <| Decode.list Data.Contact.decodeContact)
        |> andMap (Decode.field "echanges" <| Decode.list decodeEchange)
        |> andMap (Decode.field "demandes" <| Decode.list decodeDemande)
        |> andMap (Decode.field "rappels" <| Decode.list decodeRappel)
        |> andMap (Decode.field "etabCreaTransformes" <| Decode.list <| decodeEtablissementCreationLie)
        |> andMap
            (Decode.field "modification" <|
                Decode.nullable <|
                    Decode.map2 Tuple.pair
                        (Decode.field "compte" <|
                            Decode.map2 (\prenom nom -> prenom ++ " " ++ nom)
                                (Decode.field "prenom" Decode.string)
                                (Decode.field "nom" Decode.string)
                        )
                        (Decode.field "date" Lib.Date.decodeDateWithTimeFromISOString)
            )
        |> andMap
            (Decode.map (Maybe.withDefault []) <|
                optionalNullableField "freres" <|
                    Decode.list <|
                        decodeEtablissementSimpleAvecPortefeuille
            )
        |> andMap
            (Decode.map2 EtablissementHistorique
                (Decode.field "predecesseurs" <|
                    Decode.list <|
                        decodeEtablissementSimpleAvecPortefeuille
                )
                (Decode.field "successeurs" <|
                    Decode.list <|
                        decodeEtablissementSimpleAvecPortefeuille
                )
            )
        |> andMap (Decode.field "ressources" <| Decode.list decodeRessource)
        |> andMap (Decode.field "occupations" <| Decode.list decodeLocalSimple)


decodeEtablissementExtra : Decoder EtablissementExtra
decodeEtablissementExtra =
    Decode.succeed EtablissementExtra
        |> andMap (optionalNullableField "categorieNaf" Decode.string)
        |> andMap (optionalNullableField "categorieNafId" Decode.string)
        |> andMap (Decode.field "emms" decodeEffectifs)
        |> andMap (Decode.field "ema" decodeEffectif)
        |> andMap
            (Decode.field "zonages" <|
                Decode.list <|
                    Decode.string
            )
        |> andMap (Decode.field "acv" Decode.bool)
        |> andMap (Decode.field "pvd" Decode.bool)
        |> andMap (Decode.field "va" Decode.bool)
        |> andMap (Decode.field "subventions" <| Decode.list <| decodeSubvention)
        |> andMap (optionalNullableField "sireneTraitementDate" Lib.Date.decodeCalendarDate)
        |> andMap (optionalNullableField "enseigne" Decode.string)


decodeEntreprise : Decoder EntrepriseData
decodeEntreprise =
    Decode.succeed EntrepriseData
        |> andMap (Decode.field "entrepriseId" Decode.string)
        |> andMap (Decode.field "entrepriseNom" Decode.string)
        |> andMap
            (Decode.map (Maybe.withDefault "") <|
                optionalNullableField "siegeAdresse" <|
                    Data.Etablissement.decodeAdresseToString
            )
        |> andMap (optionalNullableField "categorieJuridique" Decode.string)
        |> andMap (optionalNullableField "categorieJuridiqueId" Decode.string)
        |> andMap (optionalNullableField "formeJuridique" Decode.string)
        |> andMap (optionalNullableField "formeJuridiqueId" Decode.string)
        |> andMap
            (Decode.field "exercices" <|
                Decode.nullable <|
                    Decode.map (List.filterMap identity) <|
                        Decode.list decodeCa
            )
        |> andMap
            (Decode.field "inpi" <|
                Decode.list decodeExerciceInpi
            )
        |> andMap (Decode.field "inpiCaVariation" <| Decode.nullable <| Decode.float)
        |> andMap
            (Decode.map (Maybe.withDefault False) <|
                optionalNullableField "ess" Decode.bool
            )
        |> andMap
            (Decode.map (Maybe.withDefault False) <|
                optionalNullableField "micro" Decode.bool
            )
        |> andMap
            (Decode.field "mandatairesPersonnesPhysiques" <|
                Decode.nullable <|
                    Decode.list decodeMandatairePersonnePhysique
            )
        |> andMap
            (Decode.field "mandatairesPersonnesMorales" <|
                Decode.nullable <|
                    Decode.list decodeMandatairePersonneMorale
            )
        |> andMap
            (Decode.field "beneficiairesEffectifs" <|
                Decode.nullable <|
                    Decode.list decodeBeneficiaireEffectif
            )
        |> andMap (Decode.field "liasseFiscale" <| Decode.nullable decodeLiasseFiscale)
        |> andMap (Decode.field "creationDate" <| Decode.maybe <| Lib.Date.decodeCalendarDate)
        |> andMap (Decode.field "fermetureDate" <| Decode.maybe <| Lib.Date.decodeCalendarDate)
        |> andMap (Decode.field "effectif" Decode.string)
        |> andMap (Decode.field "active" Decode.bool)
        |> andMap (Decode.field "procedure" Decode.bool)
        |> andMap
            (Decode.field "procedures" <|
                Decode.list decodeProcedureCollective
            )
        |> andMap decodeNaf
        |> andMap decodeNafId
        |> andMap (optionalNullableField "categorieNaf" Decode.string)
        |> andMap (optionalNullableField "categorieNafId" Decode.string)
        |> andMap (Decode.field "categorieEntreprise" decodeEntrepriseType)
        |> andMap
            (Decode.field "etablissementsActifs" <|
                Decode.nullable <|
                    Decode.map2
                        (\t f ->
                            { territoire = t
                            , france = f
                            }
                        )
                        (Decode.field "territoire" Decode.int)
                        (Decode.field "france" Decode.int)
            )
        |> andMap (Decode.field "effectifTotalTerritoire" <| Decode.nullable <| Decode.float)
        |> andMap (Decode.field "egapro" <| Decode.nullable <| decodeEgapro)


decodeEgapro : Decoder Egapro
decodeEgapro =
    Decode.succeed Egapro
        |> andMap (Decode.field "note" <| Decode.nullable Decode.int)
        |> andMap (Decode.field "ecartRemunerations" <| Decode.nullable Decode.int)
        |> andMap (Decode.field "ecartAugmentations" <| Decode.nullable Decode.int)
        |> andMap (Decode.field "retourMaternite" <| Decode.nullable Decode.int)
        |> andMap (Decode.field "hautesRemunerations" <| Decode.nullable Decode.int)
        |> andMap (Decode.field "cadres" <| Decode.nullable Decode.float)
        |> andMap (Decode.field "instances" <| Decode.nullable Decode.float)
        |> andMap (Decode.field "annee" <| Decode.int)


decodeParticipations : Decoder (List ( String, String ))
decodeParticipations =
    Decode.list Decode.string
        |> Decode.andThen
            (\participations ->
                case participations of
                    [ a, b ] ->
                        Decode.succeed ( a, b )

                    _ ->
                        Decode.fail <| "Pas le bon nombre de relations : " ++ String.join ", " participations
            )
        |> Decode.list


decodeLiasseFiscale : Decoder LiasseFiscale
decodeLiasseFiscale =
    Decode.succeed LiasseFiscale
        |> andMap
            (Decode.field "aFiliales" <|
                Decode.nullable <|
                    Decode.bool
            )
        |> andMap
            (Decode.field "estDetenue" <|
                Decode.nullable <|
                    Decode.bool
            )
        |> andMap
            (Decode.field "sirensMere" <|
                Decode.map
                    (\sirens ->
                        case sirens of
                            [] ->
                                ( Nothing, Nothing )

                            [ s ] ->
                                ( Just s, Nothing )

                            s1 :: s2 :: _ ->
                                ( Just s1, Just s2 )
                    )
                <|
                    Decode.list Decode.string
            )
        |> andMap
            (Decode.field "filiales" <|
                Decode.nullable <|
                    Decode.list decodeFiliale
            )
        |> andMap
            (Decode.field "detentions" <|
                Decode.nullable <|
                    Decode.list decodeDetention
            )


decodeSubvention : Decoder Subvention
decodeSubvention =
    Decode.succeed Subvention
        |> andMap (Decode.field "type" Decode.string)
        |> andMap (Decode.field "montant" Decode.string)
        |> andMap (Decode.field "objet" Decode.string)
        |> andMap (Decode.field "annee" Decode.string)


decodeProcedureCollective : Decoder ProcedureCollective
decodeProcedureCollective =
    Decode.succeed ProcedureCollective
        |> andMap (Decode.field "date" Decode.string)
        |> andMap (Decode.field "famille" Decode.string)
        |> andMap (Decode.field "nature" Decode.string)
        |> andMap (Decode.field "annee" Decode.string)


decodeMandatairePersonnePhysique : Decoder MandatairePersonnePhysique
decodeMandatairePersonnePhysique =
    Decode.succeed MandatairePersonnePhysique
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "fonction" Decode.string)
        |> andMap (Decode.field "contact" decodePersonne)


decodeMandatairePersonneMorale : Decoder MandatairePersonneMorale
decodeMandatairePersonneMorale =
    Decode.succeed MandatairePersonneMorale
        |> andMap (Decode.field "fonction" Decode.string)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "siren" Decode.string)


decodeBeneficiaireEffectif : Decoder BeneficiaireEffectif
decodeBeneficiaireEffectif =
    Decode.succeed BeneficiaireEffectif
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "nom" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "nomUsage" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "prenoms" Decode.string)
        |> andMap (optionalNullableField "naissanceDate" Lib.Date.decodeCalendarDate)
        |> andMap (Decode.map (Maybe.withDefault 0) <| optionalNullableField "totalParts" Decode.float)


decodeFiliale : Decoder Filiale
decodeFiliale =
    Decode.succeed Filiale
        |> andMap (optionalNullableField "nom" Decode.string)
        |> andMap (optionalNullableField "siren" Decode.string)
        |> andMap (optionalNullableField "pourcentage" Decode.float)


decodeDetention : Decoder Detention
decodeDetention =
    Decode.succeed Detention
        |> andMap (optionalNullableField "nom" Decode.string)
        |> andMap (optionalNullableField "siren" Decode.string)
        |> andMap (optionalNullableField "parts" Decode.float)
        |> andMap (optionalNullableField "pourcentage" Decode.float)


decodeCa : Decoder (Maybe Exercice)
decodeCa =
    Decode.map3 (Maybe.map3 Exercice)
        (Decode.field "ca" <| Decode.nullable Decode.string)
        (Decode.field "annee" <| Decode.nullable Decode.string)
        (Decode.field "date" <| Decode.nullable Decode.string)


type alias Effectif =
    { mois : Int
    , annee : Int
    , valeur : Float
    }


decodeEffectifs : Decoder (List Effectif)
decodeEffectifs =
    Decode.map (List.filterMap identity) <|
        Decode.list <|
            decodeEffectif


decodeEffectif : Decoder (Maybe Effectif)
decodeEffectif =
    Decode.map3 (Maybe.map3 Effectif)
        (optionalNullableField "mois" <| Decode.int)
        (optionalNullableField "annee" <| Decode.int)
        (optionalNullableField "valeur" <| Decode.float)


decodeEtablissementCreationLie : Decoder EtablissementCreationLie
decodeEtablissementCreationLie =
    Decode.succeed EtablissementCreationLie
        |> andMap (Decode.field "etabCreaId" decodeEntityId)
        |> andMap (Decode.field "nom" <| Decode.string)


decodeExerciceInpi : Decoder ExerciceInpi
decodeExerciceInpi =
    Decode.succeed ExerciceInpi
        |> andMap (Decode.field "ca" Decode.string)
        |> andMap (Decode.field "marge" Decode.string)
        |> andMap (Decode.field "ebe" Decode.string)
        |> andMap (Decode.field "resultatNet" Decode.string)
        |> andMap (Decode.field "annee" Decode.string)
