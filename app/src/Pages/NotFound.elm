module Pages.NotFound exposing (page)

import Accessibility as Html
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Layout
import View


page : Shared.Shared -> Page String Shared.Msg (View.View ()) String ()
page _ =
    Spa.Page.sandbox
        { init = init
        , view = view
        , update = always identity
        }


init : a -> a
init url =
    url


view : String -> View.View ()
view url =
    { title = UI.Layout.pageTitle <| "Page non trouvée - " ++ url
    , body =
        [ Html.h1 [] [ Html.text "Page non trouvée\u{00A0}!" ]
        , Html.p [] [ Html.text url ]
        ]
    , route = Route.NotFound url
    }
