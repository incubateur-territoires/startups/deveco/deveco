module Pages.Evenements exposing (page)

import Accessibility exposing (Html, br, div, li, text, ul)
import Api
import Api.Auth exposing (get)
import DSFR.Button
import DSFR.Icons.System
import Effect
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode exposing (Value)
import Lib.Date exposing (dateTimeToFrenchString)
import Lib.UI exposing (capitalizeName, displayPrenomNom)
import RemoteData as RD exposing (WebData)
import Route
import Shared
import Spa.Page exposing (Page)
import Time exposing (Posix, Zone)
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page String Shared.Msg (View Msg) Model Msg
page shared _ =
    Spa.Page.element
        { view = view shared
        , init = init shared
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    { siret : String
    , evenements : WebData (List Evenement)
    }


type alias Evenement =
    { id : Int
    , typeId : String
    , date : Posix
    , diff : Maybe Diff
    , compteId : Int
    , compte : Compte
    }


type alias Compte =
    { nom : String
    , prenom : String
    , email : String
    }


type alias Diff =
    { message : String
    , changes : Maybe Changes
    }


type alias Changes =
    { after : Value
    , before : Value
    }


type Msg
    = SharedMsg Shared.Msg
    | ReceivedEvenements (WebData (List Evenement))


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


init : Shared.Shared -> String -> ( Model, Effect.Effect Shared.Msg Msg )
init _ siret =
    { siret = siret
    , evenements = RD.Loading
    }
        |> Effect.withCmd (getEvenements siret)
        |> Shared.pageChangeEffects


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ReceivedEvenements response ->
            { model | evenements = response }
                |> Effect.withNone


view : Shared.Shared -> Model -> View Msg
view { now, timezone } model =
    { title = UI.Layout.pageTitle <| "Evenements"
    , body = body timezone now model
    , route = Route.Evenements <| model.siret
    }


body : Zone -> Posix -> Model -> List (Html Msg)
body zone _ model =
    [ div [ class "flex flex-col p-2" ]
        [ div [ class "flex flex-row justify-between" ]
            [ DSFR.Button.new
                { label = "Retour"
                , onClick = Just <| SharedMsg <| Shared.goBack
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.arrowLeftLine
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withAttrs [ class "text-underline" ]
                |> DSFR.Button.view
            ]
        , model.evenements
            |> RD.withDefault []
            |> List.sortBy (.date >> Time.posixToMillis)
            |> List.reverse
            |> List.map (viewEvenement zone)
            |> ul []
        ]
    ]


viewEvenement : Zone -> Evenement -> Html Msg
viewEvenement zone evenement =
    li []
        [ text <| capitalizeName evenement.typeId
        , br []
        , text <| dateTimeToFrenchString zone evenement.date
        , text <| " par " ++ displayPrenomNom evenement.compte
        , br []
        , Maybe.withDefault nothing <|
            Maybe.map text <|
                Maybe.map .message <|
                    evenement.diff
        ]


getEvenements : String -> Cmd Msg
getEvenements siret =
    get
        { url = Api.getEvenements <| siret
        }
        { toShared = SharedMsg
        , logger = Nothing
        , handler = ReceivedEvenements
        }
        (Decode.field "evenements" (Decode.list <| decodeEvenement))


decodeEvenement : Decoder Evenement
decodeEvenement =
    Decode.succeed Evenement
        |> andMap (Decode.field "id" Decode.int)
        |> andMap (Decode.field "typeId" Decode.string)
        |> andMap (Decode.map Time.millisToPosix <| Decode.field "date" Decode.int)
        |> andMap (Decode.maybe <| Decode.field "diff" decodeDiff)
        |> andMap (Decode.field "compteId" Decode.int)
        |> andMap (Decode.field "compte" decodeCompte)


decodeDiff : Decoder Diff
decodeDiff =
    Decode.succeed Diff
        |> andMap (Decode.field "message" Decode.string)
        |> andMap (Decode.field "changes" <| decodeChanges)


decodeChanges : Decoder (Maybe Changes)
decodeChanges =
    Decode.succeed Nothing


decodeCompte : Decoder Compte
decodeCompte =
    Decode.succeed Compte
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "prenom" Decode.string)
        |> andMap (Decode.field "email" Decode.string)
