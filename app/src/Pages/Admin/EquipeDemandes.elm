module Pages.Admin.EquipeDemandes exposing (Model, Msg, page)

import Accessibility exposing (Html, br, div, formWithListeners, h1, h2, h3, h4, text)
import Api
import Api.Auth exposing (get, post)
import DSFR.Accordion
import DSFR.Button
import DSFR.Grid
import DSFR.Icons.System exposing (addLine)
import DSFR.Input
import DSFR.Modal
import DSFR.Table
import DSFR.Typography as Typo
import Data.EquipeDemande exposing (Autorisation, CompteDemande, EquipeDemande, Territoire(..), decodeEquipeDemande, encodeEquipeDemande, stringToTerritoire, territoireToDisplay, territoireToString, territoires)
import Effect
import Html
import Html.Attributes exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Html.Keyed as Keyed
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Lib.Date
import Lib.UI exposing (capitalizeName, plural, withEmptyAs)
import List.Extra
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import SingleSelectRemote
import Spa.Page exposing (Page)
import Time
import UI.Layout
import View exposing (View)


type alias Model =
    { equipeDemandes : WebData (List EquipeDemande)
    , newEquipeDemande : Maybe NewEquipeDemande
    , saveEquipeDemande : WebData (List EquipeDemande)
    , clotureDemandeRequest : WebData (List EquipeDemande)
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | ReceivedEquipeDemandes (WebData (List EquipeDemande))
    | AddEquipeDemande
    | CancelAddEquipeDemande
    | UpdateEquipeDemande EquipeDemandeField String
    | AddAutorisation
    | RemoveAutorisation AutorisationId
    | UpdateAutorisation AutorisationId AutorisationField String
    | SelectedTypeTerritoire Territoire
    | SelectedNewTerritoire AutorisationId ( NewTerritoire, SingleSelectRemote.Msg NewTerritoire )
    | UpdatedSelectNewTerritoire AutorisationId (SingleSelectRemote.Msg NewTerritoire)
    | SaveEquipeDemande
    | ReceivedSaveEquipeDemande (WebData (List EquipeDemande))
    | ClickedClotureDemande EquipeDemande
    | ReceivedClotureDemandeResponse (WebData (List EquipeDemande))


initNewEquipeDemande : NewEquipeDemande
initNewEquipeDemande =
    { nom = ""
    , comptePrenom = ""
    , compteNom = ""
    , compteEmail = ""
    , compteFonction = ""
    , territoireType = Commune
    , autorisations = [ initNewAutorisation 0 ]
    }


type alias NewEquipeDemande =
    { nom : String
    , comptePrenom : String
    , compteNom : String
    , compteEmail : String
    , compteFonction : String
    , territoireType : Territoire
    , autorisations : List NewAutorisation
    }


type alias NewAutorisation =
    { ayantDroitPrenom : String
    , ayantDroitNom : String
    , ayantDroitEmail : String
    , ayantDroitFonction : String
    , selectNewTerritoire : SingleSelectRemote.SmartSelect Msg NewTerritoire
    , newTerritoire : Maybe NewTerritoire
    }


initNewAutorisation : Int -> NewAutorisation
initNewAutorisation index =
    { ayantDroitPrenom = ""
    , ayantDroitNom = ""
    , ayantDroitEmail = ""
    , ayantDroitFonction = ""
    , selectNewTerritoire =
        SingleSelectRemote.init ("select-new-territoire-" ++ String.fromInt index)
            { selectionMsg = SelectedNewTerritoire <| index
            , internalMsg = UpdatedSelectNewTerritoire <| index
            , characterSearchThreshold = selectCharacterThreshold
            , debounceDuration = selectDebounceDuration
            }
    , newTerritoire = Nothing
    }


type alias AutorisationId =
    Int


type EquipeDemandeField
    = Nom
    | CPrenom
    | CNom
    | CEmail
    | CFonction


type AutorisationField
    = ADPrenom
    | ADNom
    | ADEmail
    | ADFonction


type alias NewTerritoire =
    { id : String
    , name : String
    }


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page { appUrl, timezone, now } _ =
    Spa.Page.element
        { view = view appUrl timezone now
        , init = init
        , update = update
        , subscriptions = subscriptions
        }


subscriptions : Model -> Sub Msg
subscriptions model =
    model
        |> .newEquipeDemande
        |> Maybe.map .autorisations
        |> Maybe.map List.reverse
        |> Maybe.andThen List.head
        |> Maybe.map .selectNewTerritoire
        |> Maybe.map SingleSelectRemote.subscriptions
        |> Maybe.withDefault Sub.none


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init () =
    ( { equipeDemandes = RD.NotAsked
      , newEquipeDemande = Nothing
      , saveEquipeDemande = RD.NotAsked
      , clotureDemandeRequest = RD.NotAsked
      }
    , Effect.batch [ getEquipeDemandes ]
    )
        |> Shared.pageChangeEffects


getEquipeDemandes : Effect.Effect Shared.Msg Msg
getEquipeDemandes =
    get
        { url = Api.getEquipeDemandesAdmin
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedEquipeDemandes
        }
        (Decode.list decodeEquipeDemande)
        |> Effect.fromCmd


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        ReceivedEquipeDemandes response ->
            { model | equipeDemandes = response }
                |> Effect.withNone

        AddEquipeDemande ->
            { model | newEquipeDemande = Just initNewEquipeDemande }
                |> Effect.withNone

        CancelAddEquipeDemande ->
            { model | newEquipeDemande = Nothing }
                |> Effect.withNone

        UpdateEquipeDemande field value ->
            case model.newEquipeDemande of
                Nothing ->
                    model
                        |> Effect.withNone

                Just newEquipeDemande ->
                    let
                        updated =
                            Just <|
                                case field of
                                    Nom ->
                                        { newEquipeDemande | nom = value }

                                    CNom ->
                                        { newEquipeDemande | compteNom = value }

                                    CPrenom ->
                                        { newEquipeDemande | comptePrenom = value }

                                    CEmail ->
                                        { newEquipeDemande | compteEmail = value }

                                    CFonction ->
                                        { newEquipeDemande | compteFonction = value }
                    in
                    { model | newEquipeDemande = updated }
                        |> Effect.withNone

        SaveEquipeDemande ->
            case model.newEquipeDemande of
                Nothing ->
                    model
                        |> Effect.withNone

                Just newEquipeDemande ->
                    model
                        |> Effect.with (saveEquipeDemande newEquipeDemande)

        ReceivedSaveEquipeDemande ((RD.Success _) as resp) ->
            { model
                | saveEquipeDemande = RD.NotAsked
                , newEquipeDemande = Nothing
                , equipeDemandes = resp
            }
                |> Effect.withNone

        ReceivedSaveEquipeDemande resp ->
            { model | saveEquipeDemande = resp }
                |> Effect.withNone

        AddAutorisation ->
            case model.newEquipeDemande of
                Nothing ->
                    model
                        |> Effect.withNone

                Just equipeDemande ->
                    let
                        newEquipeDemande =
                            { equipeDemande
                                | autorisations = List.reverse <| (initNewAutorisation <| List.length equipeDemande.autorisations) :: List.reverse equipeDemande.autorisations
                            }
                    in
                    { model | newEquipeDemande = Just newEquipeDemande }
                        |> Effect.withNone

        RemoveAutorisation index ->
            case model.newEquipeDemande of
                Nothing ->
                    model
                        |> Effect.withNone

                Just equipeDemande ->
                    ( { model
                        | newEquipeDemande =
                            Just <|
                                { equipeDemande
                                    | autorisations =
                                        equipeDemande.autorisations
                                            |> List.Extra.removeAt index
                                }
                      }
                    , Effect.none
                    )

        UpdateAutorisation index field value ->
            case model.newEquipeDemande of
                Nothing ->
                    model
                        |> Effect.withNone

                Just equipeDemande ->
                    case equipeDemande.autorisations |> List.Extra.getAt index of
                        Nothing ->
                            model
                                |> Effect.withNone

                        Just _ ->
                            let
                                updater a =
                                    case field of
                                        ADNom ->
                                            { a | ayantDroitNom = value }

                                        ADPrenom ->
                                            { a | ayantDroitPrenom = value }

                                        ADEmail ->
                                            { a | ayantDroitEmail = value }

                                        ADFonction ->
                                            { a | ayantDroitFonction = value }

                                newEquipeDemande =
                                    Just <| updateAutorisationInNewEquipeDemande index updater equipeDemande
                            in
                            ( { model | newEquipeDemande = newEquipeDemande }
                            , Effect.none
                            )

        SelectedTypeTerritoire typeTerritoire ->
            case model.newEquipeDemande of
                Nothing ->
                    model
                        |> Effect.withNone

                Just equipeDemande ->
                    let
                        newEquipeDemande =
                            { equipeDemande
                                | territoireType = typeTerritoire
                                , autorisations = [ initNewAutorisation 0 ]
                            }
                    in
                    ( { model | newEquipeDemande = Just newEquipeDemande }
                    , Effect.none
                    )

        SelectedNewTerritoire index ( newTerritoire, sMsg ) ->
            case model.newEquipeDemande of
                Nothing ->
                    model
                        |> Effect.withNone

                Just equipeDemande ->
                    case equipeDemande.autorisations |> List.Extra.getAt index of
                        Nothing ->
                            model
                                |> Effect.withNone

                        Just autorisation ->
                            let
                                ( updatedSelect, selectCmd ) =
                                    SingleSelectRemote.update sMsg (selectConfig equipeDemande.territoireType) autorisation.selectNewTerritoire

                                updater a =
                                    { a
                                        | selectNewTerritoire = updatedSelect
                                        , newTerritoire = Just newTerritoire
                                    }

                                newEquipeDemande =
                                    Just <| updateAutorisationInNewEquipeDemande index updater equipeDemande
                            in
                            ( { model | newEquipeDemande = newEquipeDemande }
                            , Effect.fromCmd selectCmd
                            )

        UpdatedSelectNewTerritoire index sMsg ->
            case model.newEquipeDemande of
                Nothing ->
                    model
                        |> Effect.withNone

                Just equipeDemande ->
                    case equipeDemande.autorisations |> List.Extra.getAt index of
                        Nothing ->
                            model
                                |> Effect.withNone

                        Just autorisation ->
                            let
                                ( updatedSelect, selectCmd ) =
                                    SingleSelectRemote.update sMsg (selectConfig equipeDemande.territoireType) autorisation.selectNewTerritoire

                                updater a =
                                    { a
                                        | selectNewTerritoire = updatedSelect
                                    }

                                newEquipeDemande =
                                    Just <| updateAutorisationInNewEquipeDemande index updater equipeDemande
                            in
                            ( { model
                                | newEquipeDemande = newEquipeDemande
                              }
                            , Effect.fromCmd selectCmd
                            )

        ClickedClotureDemande { id } ->
            ( { model | clotureDemandeRequest = RD.Loading }, requestClotureDemande id )

        ReceivedClotureDemandeResponse request ->
            ( { model
                | clotureDemandeRequest = request
                , equipeDemandes =
                    request
                        |> RD.map RD.Success
                        |> RD.withDefault model.equipeDemandes
              }
            , Effect.none
            )


view : String -> Time.Zone -> Time.Posix -> Model -> View Msg
view appUrl zone now model =
    { title = UI.Layout.pageTitle "Demandes d'équipe - Superadmin"
    , body = [ body appUrl zone now model ]
    , route = Route.AdminEquipeDemandes
    }


body : String -> Time.Zone -> Time.Posix -> Model -> Html Msg
body appUrl zone now model =
    div [ DSFR.Grid.gridRow ]
        [ viewModal model
        , div [ DSFR.Grid.col12, class "flex flex-row justify-between items-center mb-4" ]
            [ h1 [ class "!mb-0" ] [ text "Demandes de délégation d'équipe" ]
            , div []
                [ DSFR.Button.new { label = "Ajouter", onClick = Just AddEquipeDemande }
                    |> DSFR.Button.leftIcon addLine
                    |> DSFR.Button.view
                ]
            ]
        , div [ DSFR.Grid.col12, class "p-4" ]
            [ div [ DSFR.Grid.gridRow ]
                [ viewEquipeDemandes appUrl zone now model.clotureDemandeRequest <|
                    model.equipeDemandes
                ]
            ]
        ]


viewModal : Model -> Html Msg
viewModal model =
    let
        ( opened, content, title ) =
            case model.newEquipeDemande of
                Nothing ->
                    ( False, nothing, nothing )

                Just newEquipeDemande ->
                    ( True, viewEquipeDemandeForm model.saveEquipeDemande newEquipeDemande, text "Ajouter une demande de délégation" )
    in
    DSFR.Modal.view
        { id = "demande"
        , label = "demande"
        , openMsg = NoOp
        , closeMsg = Just CancelAddEquipeDemande
        , title = title
        , opened = opened
        , size = Just <| DSFR.Modal.lg
        }
        content
        Nothing
        |> Tuple.first


viewEquipeDemandeForm : WebData (List EquipeDemande) -> NewEquipeDemande -> Html Msg
viewEquipeDemandeForm request input =
    formWithListeners [ Events.onSubmit <| SaveEquipeDemande, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12 ]
            [ h3 [ Typo.fr_h5 ] [ text "Équipe" ]
            , div [ class "flex flex-row gap-4" ]
                [ DSFR.Input.new
                    { value = input.nom
                    , label = text "Nom de l'équipe"
                    , onInput = UpdateEquipeDemande Nom
                    , name = "nouvelle-demande-equipe-nom"
                    }
                    |> DSFR.Input.withRequired True
                    |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
                    |> DSFR.Input.view
                , DSFR.Input.new
                    { value = input.territoireType |> territoireToString
                    , onInput =
                        stringToTerritoire
                            >> Maybe.map SelectedTypeTerritoire
                            >> Maybe.withDefault NoOp
                    , label = text "Type du territoire"
                    , name = "select-territoire-type"
                    }
                    |> DSFR.Input.select
                        { options = territoires
                        , toId = territoireToString
                        , toLabel = text << territoireToDisplay
                        , toDisabled = Nothing
                        }
                    |> DSFR.Input.withRequired True
                    |> DSFR.Input.view
                ]
            ]
        , div [ DSFR.Grid.col12 ]
            [ h3 [ Typo.fr_h5 ] [ text "Requérant(e)" ]
            , div [ class "flex flex-col gap-4" ]
                [ div [ class "flex flex-row gap-4" ]
                    [ DSFR.Input.new
                        { value = input.compteNom
                        , label = text "Nom"
                        , onInput = UpdateEquipeDemande CNom
                        , name = "nouvelle-demande-compte-nom"
                        }
                        |> DSFR.Input.withRequired True
                        |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
                        |> DSFR.Input.view
                    , DSFR.Input.new
                        { value = input.comptePrenom
                        , label = text "Prénom"
                        , onInput = UpdateEquipeDemande CPrenom
                        , name = "nouvelle-demande-compte-prenom"
                        }
                        |> DSFR.Input.withRequired True
                        |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
                        |> DSFR.Input.view
                    ]
                , div [ class "flex flex-row gap-4" ]
                    [ DSFR.Input.new
                        { value = input.compteEmail
                        , label = text "Email"
                        , onInput = UpdateEquipeDemande CEmail
                        , name = "nouvelle-demande-compte-email"
                        }
                        |> DSFR.Input.withRequired True
                        |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
                        |> DSFR.Input.view
                    , DSFR.Input.new
                        { value = input.compteFonction
                        , label = text "Fonction"
                        , onInput = UpdateEquipeDemande CFonction
                        , name = "nouvelle-demande-compte-fonction"
                        }
                        |> DSFR.Input.withRequired True
                        |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
                        |> DSFR.Input.view
                    ]
                ]
            ]
        , div [ DSFR.Grid.col12 ]
            [ h3 [ Typo.fr_h5 ]
                [ text "Autorisations"
                , DSFR.Button.new { label = "ajouter", onClick = Just <| AddAutorisation }
                    |> DSFR.Button.tertiaryNoOutline
                    |> DSFR.Button.view
                ]
            , div [ class "flex flex-col gap-4" ] <| List.indexedMap (viewNewAutorisation input.territoireType) <| input.autorisations
            ]
        , div [ DSFR.Grid.col12 ]
            [ footer request input
            ]
        ]


viewNewAutorisation : Territoire -> AutorisationId -> NewAutorisation -> Html Msg
viewNewAutorisation territoireType index newAutorisation =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12 ]
            [ div []
                [ text <| "Autorisation # " ++ (String.fromInt <| 1 + index)
                , DSFR.Button.new { label = "supprimer", onClick = Just <| RemoveAutorisation index }
                    |> DSFR.Button.tertiaryNoOutline
                    |> DSFR.Button.view
                ]
            , div []
                [ h4 [ Typo.fr_h6 ] [ text "Territoire" ]
                , div [ class "flex flex-col gap-4" ]
                    [ div []
                        [ newAutorisation.selectNewTerritoire
                            |> SingleSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = newAutorisation.newTerritoire
                                , optionLabelFn =
                                    \{ id, name } ->
                                        name ++ " (" ++ id ++ ")"
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle =
                                    text <|
                                        (\type_ ->
                                            case type_ of
                                                Commune ->
                                                    "Commune (code INSEE)"

                                                _ ->
                                                    territoireToDisplay type_
                                        )
                                        <|
                                            territoireType
                                , searchPrompt =
                                    case territoireType of
                                        Commune ->
                                            "Par nom, code INSEE, code postal"

                                        _ ->
                                            ""
                                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg =
                                    \searchText ->
                                        "Aucun autre territoire n'a été trouvé pour"
                                            ++ (if searchText == "" then
                                                    ""

                                                else
                                                    " pour " ++ searchText
                                               )
                                , noOptionsMsg = "Aucune entité n'a été trouvée"
                                , error = Nothing
                                }
                        ]
                    , div [ class "flex flex-row gap-4" ]
                        [ DSFR.Input.new
                            { value = newAutorisation.ayantDroitNom
                            , label = text "Nom"
                            , onInput = UpdateAutorisation index ADNom
                            , name = "nouvelle-demande-ayant-droit-nom"
                            }
                            |> DSFR.Input.withRequired True
                            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
                            |> DSFR.Input.view
                        , DSFR.Input.new
                            { value = newAutorisation.ayantDroitPrenom
                            , label = text "Prénom"
                            , onInput = UpdateAutorisation index ADPrenom
                            , name = "nouvelle-demande-ayant-droit-prenom"
                            }
                            |> DSFR.Input.withRequired True
                            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
                            |> DSFR.Input.view
                        ]
                    , div [ class "flex flex-row gap-4" ]
                        [ DSFR.Input.new
                            { value = newAutorisation.ayantDroitEmail
                            , label = text "Email"
                            , onInput = UpdateAutorisation index ADEmail
                            , name = "nouvelle-demande-ayant-droit-email"
                            }
                            |> DSFR.Input.withRequired True
                            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
                            |> DSFR.Input.view
                        , DSFR.Input.new
                            { value = newAutorisation.ayantDroitFonction
                            , label = text "Fonction"
                            , onInput = UpdateAutorisation index ADFonction
                            , name = "nouvelle-demande-ayant-droit-fonction"
                            }
                            |> DSFR.Input.withRequired True
                            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
                            |> DSFR.Input.view
                        ]
                    ]
                ]
            ]
        ]


footer : WebData (List EquipeDemande) -> NewEquipeDemande -> Html Msg
footer request input =
    DSFR.Button.group
        [ DSFR.Button.new { onClick = Nothing, label = "Enregistrer" }
            |> DSFR.Button.withDisabled (input.nom == "" || request == RD.Loading)
            |> DSFR.Button.submit
        , DSFR.Button.new { onClick = Just CancelAddEquipeDemande, label = "Annuler" }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


viewEquipeDemandes : String -> Time.Zone -> Time.Posix -> WebData (List EquipeDemande) -> WebData (List EquipeDemande) -> Html Msg
viewEquipeDemandes appUrl zone now clotureDemandeRequest data =
    case data of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Chargement en cours"
                    ]
                ]

        RD.Failure _ ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text <| "Une erreur s'est produite, veuillez recharger la page."
                    ]
                ]

        RD.Success [] ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Aucune demande d'équipe."
                    ]
                ]

        RD.Success equipeDemandes ->
            let
                sorter : EquipeDemande -> ( List EquipeDemande, List EquipeDemande, List EquipeDemande ) -> ( List EquipeDemande, List EquipeDemande, List EquipeDemande )
                sorter ({ autorisations, transformedAt } as item) ( ec, t, p ) =
                    if transformedAt /= Nothing then
                        ( ec
                        , t |> List.reverse |> (::) item |> List.reverse
                        , p
                        )

                    else if autorisations |> List.all (.acceptedAt >> (/=) Nothing) then
                        ( ec
                        , t
                        , p |> List.reverse |> (::) item |> List.reverse
                        )

                    else
                        ( ec |> List.reverse |> (::) item |> List.reverse
                        , t
                        , p
                        )

                ( enCours, terminees, pretes ) =
                    equipeDemandes
                        |> List.foldl sorter ( [], [], [] )

                viewer titre liste =
                    div [ class "flex flex-col gap-4" ]
                        [ h2 [ class "!mb-0" ] [ text titre ]
                        , case liste of
                            [] ->
                                div [ class "italic", DSFR.Grid.col ]
                                    [ div [ class "fr-card--white p-4" ]
                                        [ text "Aucune"
                                        ]
                                    ]

                            _ ->
                                div [ class "flex flex-col gap-4 w-full" ] <|
                                    List.map (viewEquipeDemande appUrl zone now clotureDemandeRequest) <|
                                        liste
                        ]
            in
            div [ DSFR.Grid.col12, class "flex flex-col gap-8" ]
                [ viewer "Prêtes" pretes
                , viewer "En cours" enCours
                , viewer "Terminées" terminees
                ]


viewEquipeDemande : String -> Time.Zone -> Time.Posix -> WebData (List EquipeDemande) -> EquipeDemande -> Html Msg
viewEquipeDemande appUrl zone now clotureDemandeRequest equipeDemande =
    let
        autorisations =
            List.length <|
                equipeDemande.autorisations

        autorisationsAcceptees =
            List.length <|
                List.filter (.acceptedAt >> (/=) Nothing) <|
                    equipeDemande.autorisations

        id =
            "equipe-demande-" ++ String.fromInt equipeDemande.id
    in
    Keyed.node "div"
        [ class "fr-card--white p-4 w-full flex flex-col gap-4" ]
        [ ( id
          , DSFR.Accordion.raw
                { id = id
                , title =
                    [ h3 [ Typo.fr_h5, class "!mb-0" ]
                        [ text "Équipe\u{00A0}: "
                        , text equipeDemande.nom
                        , text " ("
                        , text <|
                            String.fromInt <|
                                autorisationsAcceptees
                        , text " "
                        , text "demande"
                        , text <| plural autorisationsAcceptees
                        , text " "
                        , text "acceptée"
                        , text <| plural autorisationsAcceptees
                        , text " "
                        , text "sur"
                        , text " "
                        , text <|
                            String.fromInt <|
                                autorisations
                        , text ")"
                        ]
                    ]
                , borderless = False
                , content =
                    [ div [ class "w-full flex flex-col gap-4" ]
                        [ div []
                            [ div []
                                [ text <| "Demande saisie par " ++ equipeDemande.admin.prenom ++ " " ++ equipeDemande.admin.nom ++ " le " ++ Lib.Date.dateTimeToShortFrenchString zone equipeDemande.createdAt
                                ]
                            , div [ class "flex flex-row justify-between" ]
                                [ equipeDemande.transformedAt
                                    |> Maybe.map (Lib.Date.dateTimeToShortFrenchString zone >> (++) "Transformée en équipe le ")
                                    |> Maybe.withDefault "Pas encore transformée en équipe"
                                    |> text
                                , equipeDemande.transformedAt
                                    |> Maybe.map (\_ -> nothing)
                                    |> Maybe.withDefault
                                        (if autorisations == autorisationsAcceptees then
                                            DSFR.Button.new
                                                { label = "Clôturer la demande"
                                                , onClick = Just <| ClickedClotureDemande equipeDemande
                                                }
                                                |> DSFR.Button.withDisabled (RD.NotAsked /= clotureDemandeRequest)
                                                |> DSFR.Button.view

                                         else
                                            nothing
                                        )
                                ]
                            ]
                        , viewCompteDemande equipeDemande.compteDemande
                        , viewAutorisationsTable appUrl now (String.fromInt equipeDemande.id) equipeDemande.autorisations
                        ]
                    ]
                }
          )
        ]


viewCompteDemande : CompteDemande -> Html msg
viewCompteDemande compteDemande =
    div [ class "flex flex-col gap-2" ]
        [ h4 [ Typo.fr_h6, class "!mb-0" ] [ text "Compte demandé" ]
        , text <| "Nom\u{00A0}: " ++ compteDemande.nom
        , br []
        , text <| "Prénom\u{00A0}: " ++ compteDemande.prenom
        , br []
        , text <| "Email\u{00A0}: " ++ compteDemande.email
        , br []
        , text <| "Fonction\u{00A0}: " ++ compteDemande.fonction
        ]


viewAutorisationsTable : String -> Time.Posix -> String -> List Autorisation -> Html Msg
viewAutorisationsTable appUrl now equipeId autorisations =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12 ]
            [ DSFR.Table.table
                { id = "tableau-equipe-autorisations-" ++ equipeId
                , caption = text "Autorisations nécessaires"
                , headers = headers
                , rows = autorisations
                , toHeader =
                    \header ->
                        Html.span []
                            [ headerToString header |> text
                            ]
                , toRowId = \a -> territoireToString a.territoireType ++ "-" ++ a.territoireId ++ "-" ++ equipeId
                , toCell = toCell appUrl now
                }
                |> DSFR.Table.withContainerAttrs [ class "!mb-0" ]
                |> DSFR.Table.noBorders
                |> DSFR.Table.captionHidden
                |> DSFR.Table.fixed
                |> DSFR.Table.view
            ]
        ]


type Header
    = HTerritoireNom
    | HTerritoireType
    | HTerritoireId
    | HAyantDroit
    | HAcceptedAt
    | HToken


headers : List Header
headers =
    [ HTerritoireNom
    , HTerritoireType
    , HTerritoireId
    , HAyantDroit
    , HToken
    , HAcceptedAt
    ]


headerToString : Header -> String
headerToString header =
    case header of
        HTerritoireNom ->
            "Territoire"

        HTerritoireType ->
            "Type"

        HTerritoireId ->
            "Code"

        HAyantDroit ->
            "Ayant-droit"

        HToken ->
            "Formulaire"

        HAcceptedAt ->
            "Statut"


toCell : String -> Time.Posix -> Header -> Autorisation -> Html Msg
toCell appUrl now header autorisation =
    case header of
        HTerritoireId ->
            autorisation
                |> .territoireId
                |> capitalizeName
                |> withEmptyAs "-"
                |> text

        HTerritoireNom ->
            autorisation
                |> .territoireNom
                |> text

        HTerritoireType ->
            autorisation
                |> .territoireType
                |> territoireToDisplay
                |> capitalizeName
                |> text

        HAyantDroit ->
            autorisation
                |> .ayantDroit
                |> (\{ prenom, nom, fonction, email } -> prenom ++ " " ++ nom ++ " - " ++ fonction ++ " - " ++ email)
                |> text

        HToken ->
            autorisation
                |> .token
                |> (++) "/autorisation/"
                |> (++) appUrl
                |> (\url -> Typo.externalLink url [] [ text url ])

        HAcceptedAt ->
            autorisation
                |> .acceptedAt
                |> Maybe.map (Lib.Date.formatRelative now >> String.toLower >> (++) "Accepté ")
                |> Maybe.withDefault "En attente de validation"
                |> text


saveEquipeDemande : NewEquipeDemande -> Effect.Effect Shared.Msg Msg
saveEquipeDemande newEquipeDemande =
    let
        toEncode =
            { nom = newEquipeDemande.nom
            , territoireType = newEquipeDemande.territoireType
            , compte =
                { nom = newEquipeDemande.compteNom
                , prenom = newEquipeDemande.comptePrenom
                , email = newEquipeDemande.compteEmail
                , fonction = newEquipeDemande.compteFonction
                }
            , autorisations =
                newEquipeDemande.autorisations
                    |> List.filterMap
                        (\autorisation ->
                            autorisation.newTerritoire
                                |> Maybe.map
                                    (\{ id } ->
                                        { territoireId = id
                                        , ayantDroit =
                                            { nom = autorisation.ayantDroitNom
                                            , prenom = autorisation.ayantDroitPrenom
                                            , email = autorisation.ayantDroitEmail
                                            , fonction = autorisation.ayantDroitFonction
                                            }
                                        }
                                    )
                        )
            }

        jsonBody =
            toEncode
                |> encodeEquipeDemande
                |> Http.jsonBody
    in
    post
        { url = Api.getEquipeDemandesAdmin
        , body = jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveEquipeDemande
        }
        (Decode.list decodeEquipeDemande)
        |> Effect.fromCmd


selectConfig : Territoire -> { headers : List a, url : String -> String, optionDecoder : Decoder (List NewTerritoire) }
selectConfig typeTerritoire =
    { headers = []
    , url = Api.territoireSearch (territoireToString typeTerritoire)
    , optionDecoder = Decode.field "territoires" <| Decode.list <| decodeNewTerritoire
    }


decodeNewTerritoire : Decoder NewTerritoire
decodeNewTerritoire =
    Decode.succeed NewTerritoire
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "nom" Decode.string)


updateAutorisationInNewEquipeDemande : Int -> (NewAutorisation -> NewAutorisation) -> NewEquipeDemande -> NewEquipeDemande
updateAutorisationInNewEquipeDemande index updater equipeDemande =
    let
        newEquipeDemande =
            { equipeDemande
                | autorisations = newAutorisations
            }

        autorisations =
            equipeDemande.autorisations

        newAutorisations =
            autorisations
                |> List.Extra.updateAt index updater
    in
    newEquipeDemande


selectCharacterThreshold : Int
selectCharacterThreshold =
    2


selectDebounceDuration : Float
selectDebounceDuration =
    400


requestClotureDemande : Int -> Effect.Effect Shared.Msg Msg
requestClotureDemande id =
    post
        { url = Api.clotureDemandeTierce id
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedClotureDemandeResponse
        }
        (Decode.list decodeEquipeDemande)
        |> Effect.fromCmd
