module Pages.Admin.Equipes exposing (Model, Msg, page)

import Accessibility exposing (Html, div, label, text)
import Api
import Api.Auth exposing (get)
import Api.EntityId
import DSFR.Button
import DSFR.Grid
import DSFR.Icons exposing (iconMD)
import DSFR.Icons.System exposing (addLine, arrowDownLine, arrowUpLine)
import DSFR.SearchBar
import DSFR.Table
import Data.Equipe exposing (Equipe)
import Effect
import Html
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Html.Extra exposing (nothing)
import Json.Decode as Decode
import Lib.UI exposing (capitalizeName, withEmptyAs)
import List.Extra
import Pages.Admin.Equipes.AddModal as AddModal exposing (EquipeType)
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


type alias Model =
    { recherche : String
    , territoires : WebData (List Equipe)
    , sorting : Sorting
    , modalOpened : Bool
    , addModalModel : AddModal.Model
    , equipeTypes : WebData (List EquipeType)
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | AddModalMsg AddModal.Msg
    | ReceivedEquipes (WebData (List Equipe))
    | ReceivedEquipeTypes (WebData (List EquipeType))
    | UpdatedRecherche String
    | OpenAddEquipeModal
    | SetSorting Header


type alias Sorting =
    ( Header, Direction )


type Direction
    = Ascending
    | Descending


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.element
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }


subscriptions : Model -> Sub Msg
subscriptions model =
    model.addModalModel
        |> AddModal.subscriptions
        |> Sub.map AddModalMsg


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init () =
    let
        ( addModalModel, addModalEffect ) =
            AddModal.init
    in
    ( { recherche = ""
      , territoires = RD.NotAsked
      , modalOpened = False
      , sorting = ( HEquipeNom, Ascending )
      , addModalModel = addModalModel
      , equipeTypes = RD.NotAsked
      }
    , Effect.batch
        [ getEquipes
        , Effect.map AddModalMsg addModalEffect
        , getEquipeTypes
        ]
    )
        |> Shared.pageChangeEffects


getEquipes : Effect.Effect Shared.Msg Msg
getEquipes =
    get
        { url = Api.getEquipesAdmin
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedEquipes
        }
        (Decode.list Data.Equipe.decodeEquipe)
        |> Effect.fromCmd


getEquipeTypes : Effect.Effect Shared.Msg Msg
getEquipeTypes =
    get
        { url = Api.getEquipeTypes
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedEquipeTypes
        }
        (Decode.list AddModal.decodeEquipeType)
        |> Effect.fromCmd


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        AddModalMsg addModalMsg ->
            let
                ( newAddModalModel, addModalEffect ) =
                    AddModal.update addModalMsg model.addModalModel

                isDone =
                    AddModal.isDone newAddModalModel

                territoryCreated =
                    AddModal.territoryCreated newAddModalModel
            in
            ( { model
                | addModalModel = newAddModalModel
                , modalOpened = not isDone
              }
            , Effect.batch
                [ Effect.map AddModalMsg addModalEffect
                , if territoryCreated then
                    getEquipes

                  else
                    Effect.none
                ]
            )

        ReceivedEquipes response ->
            { model | territoires = response }
                |> Effect.withNone

        UpdatedRecherche recherche ->
            { model | recherche = recherche }
                |> Effect.withNone

        OpenAddEquipeModal ->
            let
                ( addModalModel, addModalCmd ) =
                    AddModal.init
            in
            { model
                | modalOpened = True
                , addModalModel = addModalModel
            }
                |> Effect.with (Effect.map AddModalMsg addModalCmd)

        SetSorting header ->
            let
                sorting =
                    case model.sorting of
                        ( h, Ascending ) ->
                            if h == header then
                                ( h, Descending )

                            else
                                ( header, Ascending )

                        ( h, Descending ) ->
                            if h == header then
                                ( h, Ascending )

                            else
                                ( header, Ascending )
            in
            { model | sorting = sorting }
                |> Effect.withNone

        ReceivedEquipeTypes resp ->
            { model | equipeTypes = resp }
                |> Effect.withNone


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle "Équipes - Superadmin"
    , body = [ body model ]
    , route = Route.AdminEquipes
    }


rechercherEquipesInputName : String
rechercherEquipesInputName =
    "recherches-territoires"


body : Model -> Html Msg
body model =
    div [ DSFR.Grid.gridRow ]
        [ Html.map AddModalMsg <|
            AddModal.view model.modalOpened (RD.withDefault [] model.equipeTypes) (RD.withDefault [] model.territoires) model.addModalModel
        , div [ DSFR.Grid.col12, class "p-4 fr-card--white" ]
            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col, class "flex flex-row gap-4 justify-end" ]
                    [ DSFR.Button.new
                        { label = "Ajouter une équipe"
                        , onClick = Just OpenAddEquipeModal
                        }
                        |> DSFR.Button.leftIcon addLine
                        |> DSFR.Button.view
                    ]
                ]
            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "!mb-4" ]
                [ div [ DSFR.Grid.col6, class "flex flex-col gap-2" ] <|
                    [ label
                        [ class "fr-label"
                        , Html.Attributes.for rechercherEquipesInputName
                        ]
                        [ text "Rechercher une équipe" ]
                    , DSFR.SearchBar.searchBar { errors = [], extraAttrs = [], disabled = False }
                        { submitMsg = NoOp
                        , buttonLabel = "Rechercher"
                        , inputMsg = UpdatedRecherche
                        , inputLabel = "Rechercher une équipe"
                        , inputPlaceholder = Nothing
                        , inputId = rechercherEquipesInputName
                        , inputValue = model.recherche
                        , hints = []
                        , fullLabel = Nothing
                        }
                    ]
                ]
            , div [ DSFR.Grid.gridRow ]
                [ div [ DSFR.Grid.col ] <|
                    List.singleton <|
                        viewEquipesTable model.sorting model.recherche model.territoires
                ]
            ]
        ]


viewEquipesTable : Sorting -> String -> WebData (List Equipe) -> Html Msg
viewEquipesTable sorting recherche data =
    case data of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Chargement en cours"
                    ]
                ]

        RD.Failure _ ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Une erreur s'est produite, veuillez recharger la page."
                    ]
                ]

        RD.Success [] ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Aucune équipe pour l'instant."
                    ]
                ]

        RD.Success territoires ->
            case territoires |> List.filter (filterEquipes recherche) of
                [] ->
                    div [ class "text-center", DSFR.Grid.col ]
                        [ div [ class "fr-card--white p-20" ]
                            [ text "Aucune équipe correspondant."
                            ]
                        ]

                us ->
                    div [ DSFR.Grid.col12 ]
                        [ DSFR.Table.table
                            { id = "tableau-equipes"
                            , caption = text "Équipes"
                            , headers = headers
                            , rows = us |> sortWithSorting sorting
                            , toHeader =
                                \header ->
                                    Html.span [ onClick <| SetSorting <| header, class "cursor-pointer" ]
                                        [ headerToString header |> text
                                        , sortingIcon sorting header
                                        ]
                            , toRowId = Data.Equipe.id >> Api.EntityId.entityIdToString
                            , toCell = toCell
                            }
                            |> DSFR.Table.withContainerAttrs [ class "!mb-0" ]
                            |> DSFR.Table.noBorders
                            |> DSFR.Table.captionHidden
                            |> DSFR.Table.fixed
                            |> DSFR.Table.view
                        ]


sortingIcon : ( Header, Direction ) -> Header -> Html msg
sortingIcon ( sortingHeader, direction ) header =
    if header == sortingHeader then
        case direction of
            Ascending ->
                iconMD arrowDownLine

            Descending ->
                iconMD arrowUpLine

    else
        nothing


sortWithSorting : ( Header, Direction ) -> List Equipe -> List Equipe
sortWithSorting ( header, direction ) =
    let
        sortWith =
            case header of
                HEquipeNom ->
                    Data.Equipe.nom >> String.toLower >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HEquipeType ->
                    Data.Equipe.equipeType >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HTerritoireNom ->
                    Data.Equipe.territoireNom >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HTerritoireType ->
                    Data.Equipe.territoireType >> String.toLower >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HTerritoireCategorie ->
                    Data.Equipe.territoireCategorie >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HSiret ->
                    Data.Equipe.siret >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"
    in
    List.Extra.stableSortWith
        (\a b ->
            case direction of
                Ascending ->
                    compare (sortWith a) (sortWith b)

                Descending ->
                    compare (sortWith b) (sortWith a)
        )


filterEquipes : String -> Equipe -> Bool
filterEquipes recherche equipe =
    let
        lowerRecherche =
            String.toLower recherche

        matches =
            String.toLower
                >> String.contains lowerRecherche

        matchRecherche =
            (lowerRecherche == "")
                || (matches <| Data.Equipe.nom equipe)
                || (matches <| Data.Equipe.equipeType equipe)
    in
    matchRecherche


type Header
    = HEquipeNom
    | HEquipeType
    | HTerritoireNom
    | HTerritoireType
    | HTerritoireCategorie
    | HSiret


headers : List Header
headers =
    [ HEquipeNom
    , HEquipeType
    , HTerritoireNom
    , HTerritoireType
    , HTerritoireCategorie
    , HSiret
    ]


headerToString : Header -> String
headerToString header =
    case header of
        HEquipeNom ->
            "Équipe"

        HEquipeType ->
            "Type d'équipe"

        HTerritoireNom ->
            "Nom du territoire"

        HTerritoireType ->
            "Type du territoire"

        HTerritoireCategorie ->
            "Catégorie du territoire"

        HSiret ->
            "Siret"


toCell : Header -> Equipe -> Html Msg
toCell header equipe =
    case header of
        HEquipeNom ->
            equipe
                |> Data.Equipe.nom
                |> withEmptyAs "-"
                |> text

        HEquipeType ->
            equipe
                |> Data.Equipe.equipeType
                |> capitalizeName
                |> withEmptyAs "-"
                |> text

        HTerritoireNom ->
            equipe
                |> Data.Equipe.territoireNom
                |> capitalizeName
                |> text

        HTerritoireType ->
            equipe
                |> Data.Equipe.territoireType
                |> String.toLower
                |> capitalizeName
                |> text

        HTerritoireCategorie ->
            equipe
                |> Data.Equipe.territoireCategorie
                |> capitalizeName
                |> text

        HSiret ->
            equipe
                |> Data.Equipe.siret
                |> text
