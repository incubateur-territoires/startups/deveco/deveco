module Pages.Admin.Equipes.AddModal exposing (EquipeType, Model, Msg, decodeEquipeType, init, isDone, subscriptions, territoryCreated, update, view)

import Accessibility exposing (Html, div, formWithListeners, p, text)
import Api
import Api.Auth exposing (post)
import DSFR.Alert
import DSFR.Button
import DSFR.Checkbox
import DSFR.Grid
import DSFR.Input
import DSFR.Modal
import DSFR.Tag
import Data.Equipe exposing (Equipe)
import Effect
import Html.Attributes exposing (class, title)
import Html.Attributes.Extra exposing (attributeIf)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import Lib.UI exposing (capitalizeName)
import MultiSelectRemote
import RemoteData as RD exposing (WebData)
import Shared
import SingleSelectRemote


type alias Model =
    { nomEquipe : String
    , groupement : Bool
    , territoireType : Type
    , equipeType : Maybe EquipeType
    , siret : String
    , selectNewTerritoire : SingleSelectRemote.SmartSelect Msg NewTerritoire
    , selectNewTerritoires : MultiSelectRemote.SmartSelect Msg NewTerritoire
    , newTerritoires : List NewTerritoire
    , saveEquipeRequest : WebData ()
    , isDone : Bool
    , territoryCreated : Bool
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
      --
    | UpdatedNomEquipe String
    | UpdatedSiret String
    | ToggledGroupement Bool
    | SelectedTypeTerritoire Type
    | SelectedEquipeType (Maybe EquipeType)
      --
    | SelectedNewTerritoire ( NewTerritoire, SingleSelectRemote.Msg NewTerritoire )
    | UpdatedSelectNewTerritoire (SingleSelectRemote.Msg NewTerritoire)
      --
    | SelectedNewTerritoires ( List NewTerritoire, MultiSelectRemote.Msg NewTerritoire )
    | UpdatedSelectNewTerritoires (MultiSelectRemote.Msg NewTerritoire)
      --
    | UnselectNewTerritoire NewTerritoire
      --
    | RequestedSaveEquipe
    | ReceivedSaveEquipe (WebData ())
      --
    | CloseModal


init : ( Model, Effect.Effect Shared.Msg Msg )
init =
    { nomEquipe = ""
    , groupement = False
    , territoireType = Commune
    , equipeType = Nothing
    , newTerritoires = []
    , siret = ""
    , selectNewTerritoire =
        SingleSelectRemote.init "select-new-territoire"
            { selectionMsg = SelectedNewTerritoire
            , internalMsg = UpdatedSelectNewTerritoire
            , characterSearchThreshold = selectCharacterThreshold
            , debounceDuration = selectDebounceDuration
            }
    , selectNewTerritoires =
        MultiSelectRemote.init "select-new-territoires"
            { selectionMsg = SelectedNewTerritoires
            , internalMsg = UpdatedSelectNewTerritoires
            , characterSearchThreshold = selectCharacterThreshold
            , debounceDuration = selectDebounceDuration
            }
    , saveEquipeRequest = RD.NotAsked
    , isDone = False
    , territoryCreated = False
    }
        |> Effect.withNone


selectCharacterThreshold : Int
selectCharacterThreshold =
    2


selectDebounceDuration : Float
selectDebounceDuration =
    400


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ SingleSelectRemote.subscriptions model.selectNewTerritoire
        , MultiSelectRemote.subscriptions model.selectNewTerritoires
        ]


type alias NewTerritoire =
    { id : String
    , name : String
    }


type Type
    = Commune
    | EPCI
    | Metropole
    | Petr
    | TerritoireIndustrie
    | Departement
    | Region
    | France


types : List Type
types =
    [ Commune
    , EPCI
    , Metropole
    , Petr
    , TerritoireIndustrie
    , Departement
    , Region
    , France
    ]


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        UpdatedNomEquipe nom ->
            ( { model | nomEquipe = nom }
            , Effect.none
            )

        UpdatedSiret str ->
            ( { model | siret = str }
            , Effect.none
            )

        RequestedSaveEquipe ->
            { model | saveEquipeRequest = RD.Loading }
                |> Effect.withCmd (saveEquipe model)

        ReceivedSaveEquipe response ->
            { model | saveEquipeRequest = response, territoryCreated = RD.isSuccess response }
                |> Effect.withNone

        ToggledGroupement groupement ->
            { model | groupement = groupement, newTerritoires = [] } |> Effect.withNone

        SelectedTypeTerritoire typeTerritoire ->
            { model | territoireType = typeTerritoire, newTerritoires = [] } |> Effect.withNone

        SelectedEquipeType equipeType ->
            { model | equipeType = equipeType } |> Effect.withNone

        SelectedNewTerritoire ( newTerritoire, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg (selectConfig model.territoireType) model.selectNewTerritoire
            in
            ( { model
                | selectNewTerritoire = updatedSelect
                , newTerritoires = [ newTerritoire ]
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectNewTerritoire sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg (selectConfig model.territoireType) model.selectNewTerritoire
            in
            ( { model
                | selectNewTerritoire = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        SelectedNewTerritoires ( newTerritoires, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectConfig model.territoireType) model.selectNewTerritoires
            in
            ( { model
                | selectNewTerritoires = updatedSelect
                , newTerritoires = newTerritoires
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectNewTerritoires sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectConfig model.territoireType) model.selectNewTerritoires
            in
            ( { model
                | selectNewTerritoires = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectNewTerritoire { id } ->
            { model | newTerritoires = model.newTerritoires |> List.filter (\t -> t.id /= id) }
                |> Effect.withNone

        CloseModal ->
            { model | isDone = True }
                |> Effect.withNone


typeTerritoireToString : Type -> String
typeTerritoireToString t =
    case t of
        Commune ->
            "commune"

        EPCI ->
            "epci"

        Metropole ->
            "metropole"

        Petr ->
            "petr"

        TerritoireIndustrie ->
            "territoire_industrie"

        Departement ->
            "departement"

        Region ->
            "region"

        France ->
            "france"


typeTerritoireToDisplay : Type -> String
typeTerritoireToDisplay t =
    case t of
        Commune ->
            "Commune"

        EPCI ->
            "EPCI"

        Metropole ->
            "Ville à arrondissements"

        Petr ->
            "PETR"

        TerritoireIndustrie ->
            "Territoire d'industrie"

        Departement ->
            "Département"

        Region ->
            "Région"

        France ->
            "France"


stringToTypeTerritoire : String -> Maybe Type
stringToTypeTerritoire string =
    case string of
        "commune" ->
            Just Commune

        "epci" ->
            Just EPCI

        "metropole" ->
            Just Metropole

        "petr" ->
            Just Petr

        "territoire_industrie" ->
            Just TerritoireIndustrie

        "departement" ->
            Just Departement

        "region" ->
            Just Region

        "france" ->
            Just France

        _ ->
            Nothing


view : Bool -> List EquipeType -> List Equipe -> Model -> Html Msg
view opened equipeTypes equipes model =
    DSFR.Modal.view
        { id = "territoire"
        , label = "territoire"
        , openMsg = NoOp
        , closeMsg = Nothing
        , title = text "Ajouter un territoire"
        , opened = opened
        , size = Nothing
        }
        (viewEquipeForm equipeTypes equipes model)
        Nothing
        |> Tuple.first


viewEquipeForm : List EquipeType -> List Equipe -> Model -> Html Msg
viewEquipeForm equipeTypes equipesExistantes model =
    let
        nomExistant =
            List.member model.nomEquipe <| List.map Data.Equipe.nom <| equipesExistantes
    in
    case model.saveEquipeRequest of
        RD.Success _ ->
            div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col12 ]
                    [ DSFR.Alert.small { title = Just "Création réussie", description = text "L'ajout des établissements de ce territoire est désormais en cours." }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.success
                        |> List.singleton
                        |> div [ class "flex justify-center items-center p-4 w-full" ]
                    ]
                , div [ DSFR.Grid.col12 ]
                    [ footerUtilisateur False model
                    ]
                ]

        _ ->
            formWithListeners [ Events.onSubmit <| RequestedSaveEquipe, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col6 ]
                    [ DSFR.Input.new
                        { value = model.nomEquipe
                        , label = text "Nom"
                        , onInput = UpdatedNomEquipe
                        , name = "equipe"
                        }
                        |> DSFR.Input.withExtraAttrs
                            [ attributeIf nomExistant <| title "Ce nom est déjà pris"
                            , attributeIf nomExistant <| class "fr-input-group--error"
                            ]
                        |> DSFR.Input.view
                    ]
                , div [ DSFR.Grid.col6 ]
                    [ DSFR.Input.new
                        { value = model.territoireType |> typeTerritoireToString
                        , onInput =
                            stringToTypeTerritoire
                                >> Maybe.map SelectedTypeTerritoire
                                >> Maybe.withDefault NoOp
                        , label = text "Type du territoire"
                        , name = "select-territoire-type"
                        }
                        |> DSFR.Input.select
                            { options =
                                if model.groupement then
                                    [ Commune, EPCI, Departement ]

                                else
                                    types
                            , toId = typeTerritoireToString
                            , toLabel = text << typeTerritoireToDisplay
                            , toDisabled = Nothing
                            }
                        |> DSFR.Input.view
                    ]
                , div [ DSFR.Grid.col6 ]
                    [ DSFR.Input.new
                        { value = model.equipeType |> Maybe.map .id |> Maybe.withDefault "rien"
                        , onInput =
                            \optionValue ->
                                equipeTypes
                                    |> List.filter (\et -> et.id == optionValue)
                                    |> List.head
                                    |> SelectedEquipeType
                        , label = text "Type de l'équipe"
                        , name = "select-equipe-type"
                        }
                        |> DSFR.Input.select
                            { options = { id = "rien", nom = "Aucun type" } :: equipeTypes
                            , toId = .id
                            , toLabel = text << capitalizeName << .nom
                            , toDisabled = Nothing
                            }
                        |> DSFR.Input.view
                    ]
                , div [ DSFR.Grid.col6 ]
                    [ DSFR.Checkbox.group
                        { id = "geojson-props"
                        , label = text "Groupement"
                        , onChecked = \_ -> ToggledGroupement
                        , values = [ True ]
                        , checked = [ model.groupement ]
                        , valueAsString = \_ -> "groupement"
                        , toId = \_ -> "groupement"
                        , toLabel = \_ -> text "Ce territoire est un groupement"
                        }
                        |> DSFR.Checkbox.viewGroup
                    ]
                , div [ DSFR.Grid.col6 ]
                    [ if model.territoireType == France then
                        nothing

                      else if not model.groupement then
                        model.selectNewTerritoire
                            |> SingleSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = model.newTerritoires |> List.head
                                , optionLabelFn =
                                    \{ id, name } ->
                                        name ++ " (" ++ id ++ ")"
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle =
                                    text <|
                                        (\type_ ->
                                            case type_ of
                                                Commune ->
                                                    "Commune (code INSEE)"

                                                _ ->
                                                    typeTerritoireToDisplay type_
                                        )
                                        <|
                                            model.territoireType
                                , searchPrompt =
                                    case model.territoireType of
                                        Commune ->
                                            "Par nom, code INSEE, code postal"

                                        _ ->
                                            ""
                                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg =
                                    \searchText ->
                                        "Aucun autre territoire n'a été trouvé"
                                            ++ (if searchText == "" then
                                                    ""

                                                else
                                                    " pour " ++ searchText
                                               )
                                , noOptionsMsg = "Aucune entité n'a été trouvée"
                                , error = Nothing
                                }

                      else
                        div [ class "flex flex-col gap-4" ]
                            [ model.selectNewTerritoires
                                |> MultiSelectRemote.viewCustom
                                    { isDisabled = False
                                    , selected = model.newTerritoires
                                    , optionLabelFn =
                                        \{ id, name } ->
                                            name ++ " (" ++ id ++ ")"
                                    , optionDescriptionFn = \_ -> ""
                                    , optionsContainerMaxHeight = 300
                                    , selectTitle =
                                        text <|
                                            (\type_ ->
                                                case type_ of
                                                    Commune ->
                                                        "Communes (code INSEE)"

                                                    EPCI ->
                                                        "EPCI"

                                                    _ ->
                                                        typeTerritoireToDisplay type_ ++ "s"
                                            )
                                            <|
                                                model.territoireType
                                    , viewSelectedOptionFn = \_ -> text ""
                                    , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                    , noResultsForMsg =
                                        \searchText ->
                                            "Aucun autre territoire n'a été trouvé"
                                                ++ (if searchText == "" then
                                                        ""

                                                    else
                                                        " pour " ++ searchText
                                                   )
                                    , noOptionsMsg = "Aucune entité n'a été trouvée"
                                    , newOption = Nothing
                                    , searchPrompt = "Rechercher un territoire"
                                    }
                            , model.newTerritoires
                                |> List.map (\territoire -> DSFR.Tag.deletable UnselectNewTerritoire { data = territoire, toString = .name })
                                |> DSFR.Tag.medium
                            ]
                    ]
                , div [ DSFR.Grid.col6 ]
                    [ DSFR.Input.new
                        { value = model.siret
                        , label = text "SIRET (14 chiffres)"
                        , onInput = UpdatedSiret
                        , name = "equipe"
                        }
                        |> DSFR.Input.view
                    ]
                , div [ DSFR.Grid.col6 ]
                    [ p [ class "italic" ] [ text "" ]
                    ]
                , case model.saveEquipeRequest of
                    RD.Loading ->
                        DSFR.Alert.small { title = Nothing, description = text "Création en cours" }
                            |> DSFR.Alert.alert Nothing DSFR.Alert.info
                            |> List.singleton
                            |> div [ class "flex justify-center items-center p-4 w-full" ]

                    RD.Failure _ ->
                        DSFR.Alert.small { title = Just "Création échouée", description = text "Assurez-vous qu'un territoire avec ce nom n'existe pas déjà." }
                            |> DSFR.Alert.alert Nothing DSFR.Alert.error
                            |> List.singleton
                            |> div [ class "flex justify-center items-center p-4 w-full" ]

                    _ ->
                        nothing
                , Html.Extra.viewIf nomExistant <|
                    div [ DSFR.Grid.col12 ]
                        [ p []
                            [ text "Il existe déjà "
                            , text "un"
                            , text " territoire "
                            , text "avec ce nom"
                            ]
                        ]
                , div [ DSFR.Grid.col12 ]
                    [ footerUtilisateur nomExistant model
                    ]
                ]


footerUtilisateur : Bool -> Model -> Html Msg
footerUtilisateur nomExistant model =
    let
        valid =
            (model.nomEquipe /= "")
                && not nomExistant
                && (List.length model.newTerritoires
                        |> (if model.territoireType == France then
                                (==) 0

                            else if model.groupement then
                                (<) 1

                            else
                                (<) 0
                           )
                   )
                && (model.siret
                        == ""
                        || (model.siret
                                |> String.replace " " ""
                                |> String.length
                                |> (==) 14
                           )
                   )

        ( isButtonDisabled, title_ ) =
            case ( model.saveEquipeRequest, valid ) of
                ( RD.Loading, _ ) ->
                    ( True, "Enregistrer" )

                ( _, False ) ->
                    ( True, "Enregistrer" )

                _ ->
                    ( False, "Enregistrer" )

        buttons =
            case model.saveEquipeRequest of
                RD.Success _ ->
                    [ DSFR.Button.new { onClick = Just CloseModal, label = "OK" }
                    ]

                _ ->
                    [ DSFR.Button.new { onClick = Nothing, label = "Enregistrer" }
                        |> DSFR.Button.submit
                        |> DSFR.Button.withAttrs [ title title_, Html.Attributes.name "submit-new-territoire" ]
                        |> DSFR.Button.withDisabled isButtonDisabled
                    , DSFR.Button.new { onClick = Just CloseModal, label = "Annuler" }
                        |> DSFR.Button.secondary
                    ]
    in
    DSFR.Button.group buttons
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


saveEquipe : Model -> Cmd Msg
saveEquipe { newTerritoires, territoireType, equipeType, nomEquipe, siret } =
    let
        jsonBody =
            [ ( "nom", Encode.string <| nomEquipe )
            , ( "territoireTypeId", Encode.string <| typeTerritoireToString <| territoireType )
            , ( "territoireIds", Encode.list Encode.string <| List.map .id newTerritoires )
            , ( "equipeTypeId", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| Maybe.map .id <| equipeType )
            , ( "siret", Encode.string <| siret )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    post
        { url = Api.getEquipesAdmin
        , body = jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveEquipe
        }
        (Decode.succeed ())


isDone : Model -> Bool
isDone model =
    model.isDone


territoryCreated : Model -> Bool
territoryCreated model =
    model.territoryCreated


decodeNewTerritoire : Decoder NewTerritoire
decodeNewTerritoire =
    Decode.succeed NewTerritoire
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "nom" Decode.string)


selectConfig : Type -> { headers : List a, url : String -> String, optionDecoder : Decoder (List NewTerritoire) }
selectConfig typeTerritoire =
    { headers = []
    , url = Api.territoireSearch (typeTerritoireToString typeTerritoire)
    , optionDecoder = Decode.field "territoires" <| Decode.list <| decodeNewTerritoire
    }


type alias EquipeType =
    { id : String
    , nom : String
    }


decodeEquipeType : Decoder EquipeType
decodeEquipeType =
    Decode.map2 EquipeType
        (Decode.field "id" Decode.string)
        (Decode.field "nom" Decode.string)
