module Pages.Admin.Etiquettes exposing (Model, Msg, page)

import Accessibility exposing (Html, div, h2, p, span, text)
import Api
import Api.Auth exposing (delete, get)
import Api.EntityId exposing (EntityId, decodeEntityId, entityIdToString)
import DSFR.Accordion
import DSFR.Button
import DSFR.Grid
import DSFR.Icons.System exposing (deleteFill)
import DSFR.Modal
import DSFR.Typography as Typo
import Data.Equipe exposing (EquipeId)
import Data.Etiquette exposing (EtiquetteAvecUsages, EtiquettesAvecUsages, decodeEtiquettesAvecUsages)
import Effect
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import Lib.UI exposing (filterBy, plural)
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import SingleSelect
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


type alias Model =
    { equipes : WebData (List EquipeSimple)
    , selectEquipe : SingleSelect.SmartSelect Msg EquipeSimple
    , selectedEquipe : Maybe EquipeSimple
    , etiquettes : WebData EtiquettesAvecUsages
    , deleteRequest : Maybe DeleteRequest
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | SelectedEquipe ( EquipeSimple, SingleSelect.Msg EquipeSimple )
    | UpdatedSelectEquipe (SingleSelect.Msg EquipeSimple)
    | ReceivedEquipes (WebData (List EquipeSimple))
    | ReceivedCurrentEtiquettes (WebData EtiquettesAvecUsages)
    | ClickedDelete (EntityId EquipeId) EtiquetteType EtiquetteAvecUsages
    | CanceledDelete
    | ConfirmedDelete
    | ReceivedDelete (WebData EtiquettesAvecUsages)


type alias DeleteRequest =
    { request : WebData EtiquettesAvecUsages
    , equipeId : EntityId EquipeId
    , tagType : EtiquetteType
    , tag : EtiquetteAvecUsages
    }


type EtiquetteType
    = Activites
    | Zones
    | Mots


tagTypeToString : EtiquetteType -> String
tagTypeToString tagType =
    case tagType of
        Activites ->
            "activites_reelles"

        Zones ->
            "entreprise_localisations"

        Mots ->
            "mots_cles"


tagTypeToDisplay : EtiquetteType -> String
tagTypeToDisplay tagType =
    case tagType of
        Activites ->
            "Activités réelles et filières"

        Zones ->
            "Zones géographiques"

        Mots ->
            "Mots-clés"


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.element
        { view = view
        , init = init
        , update = update
        , subscriptions = .selectEquipe >> SingleSelect.subscriptions
        }


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init () =
    ( { equipes = RD.Loading
      , selectEquipe =
            SingleSelect.init "equipe-select"
                { selectionMsg = SelectedEquipe
                , internalMsg = UpdatedSelectEquipe
                }
      , selectedEquipe = Nothing
      , etiquettes = RD.NotAsked
      , deleteRequest = Nothing
      }
    , getEquipes
    )
        |> Shared.pageChangeEffects


getEquipes : Effect.Effect sharedMsg Msg
getEquipes =
    Effect.fromCmd <|
        get
            { url = Api.getEquipesAdmin
            }
            { toShared = SharedMsg
            , logger = Just LogError
            , handler = ReceivedEquipes
            }
            (Decode.list <| decodeZone)


decodeZone : Decoder EquipeSimple
decodeZone =
    Decode.succeed EquipeSimple
        |> andMap (Decode.field "id" <| decodeEntityId)
        |> andMap (Decode.field "nom" Decode.string)


getCurrentEtiquettes : EquipeSimple -> Effect.Effect sharedMsg Msg
getCurrentEtiquettes equipe =
    Effect.fromCmd <|
        get
            { url = Api.getEquipesEtiquettesAdmin <| equipe.id
            }
            { toShared = SharedMsg
            , logger = Just LogError
            , handler = ReceivedCurrentEtiquettes
            }
            (Decode.field "etiquettes" decodeEtiquettesAvecUsages)


deleteEtiquette : EntityId EquipeId -> EtiquetteAvecUsages -> Effect.Effect sharedMsg Msg
deleteEtiquette equipeId tag =
    Effect.fromCmd <|
        delete
            { url = Api.getEquipesEtiquettesAdmin equipeId
            , body =
                [ ( "etiquetteIds"
                  , Encode.list Encode.string <|
                        List.singleton <|
                            entityIdToString tag.id
                  )
                ]
                    |> Encode.object
                    |> Http.jsonBody
            }
            { toShared = SharedMsg
            , logger = Just LogError
            , handler = ReceivedDelete
            }
            (Decode.field "etiquettes" decodeEtiquettesAvecUsages)


type alias EquipeSimple =
    { id : EntityId EquipeId
    , nom : String
    }


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        SelectedEquipe ( equipe, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectEquipe
            in
            ( { model
                | selectedEquipe = Just equipe
                , selectEquipe = updatedSelect
                , etiquettes = RD.Loading
              }
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , getCurrentEtiquettes equipe
                ]
            )

        UpdatedSelectEquipe sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectEquipe
            in
            ( { model | selectEquipe = updatedSelect }, Effect.fromCmd selectCmd )

        ReceivedEquipes response ->
            { model | equipes = response }
                |> Effect.withNone

        ReceivedCurrentEtiquettes tags ->
            { model | etiquettes = tags }
                |> Effect.withNone

        ClickedDelete equipeId tagType tag ->
            ( { model
                | deleteRequest =
                    Just <|
                        DeleteRequest RD.NotAsked equipeId tagType tag
              }
            , Effect.none
            )

        CanceledDelete ->
            ( { model | deleteRequest = Nothing }
            , Effect.none
            )

        ConfirmedDelete ->
            case model.deleteRequest of
                Nothing ->
                    model |> Effect.withNone

                Just dr ->
                    ( { model
                        | deleteRequest =
                            Just <|
                                { dr | request = RD.Loading }
                      }
                    , deleteEtiquette dr.equipeId dr.tag
                    )

        ReceivedDelete response ->
            case response of
                RD.Success _ ->
                    { model
                        | deleteRequest = Nothing
                        , etiquettes = response
                    }
                        |> Effect.withNone

                _ ->
                    { model
                        | deleteRequest =
                            model.deleteRequest
                                |> Maybe.map (\dr -> { dr | request = response })
                    }
                        |> Effect.withNone


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle "Étiquettes - Superadmin"
    , body = UI.Layout.lazyBody body model
    , route = Route.AdminEtiquettes
    }


body : Model -> Html Msg
body model =
    let
        equipes =
            model.equipes
                |> RD.withDefault []
    in
    div [ DSFR.Grid.gridRow ]
        [ div [ DSFR.Grid.col12, class "p-4 fr-card--white" ]
            [ viewDeleteModal model.deleteRequest
            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "!mb-4" ]
                [ div [ DSFR.Grid.col6 ] <|
                    List.singleton <|
                        div [ class "p-4" ] <|
                            List.singleton <|
                                SingleSelect.viewCustom
                                    { selected = model.selectedEquipe
                                    , options = equipes |> List.sortBy (.nom >> String.toLower)
                                    , optionLabelFn = .nom
                                    , isDisabled = False
                                    , optionDescriptionFn = \_ -> ""
                                    , optionsContainerMaxHeight = 300
                                    , selectTitle = h2 [] [ text "Équipe" ]
                                    , searchPrompt = "Rechercher une équipe"
                                    , noResultsForMsg =
                                        \searchText ->
                                            "Aucune autre équipe n'a été trouvée"
                                                ++ (if searchText == "" then
                                                        ""

                                                    else
                                                        " pour " ++ searchText
                                                   )
                                    , noOptionsMsg = "Aucune équipe n'a été trouvée"
                                    , searchFn = filterBy .nom
                                    }
                                    model.selectEquipe
                ]
            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "!mb-4" ] <|
                case ( model.selectedEquipe, model.etiquettes ) of
                    ( Nothing, _ ) ->
                        [ div [ DSFR.Grid.col6 ] <|
                            List.singleton <|
                                div [ class "p-4" ]
                                    [ text "Veuillez sélectionner une équipe." ]
                        ]

                    ( Just { id }, RD.Success { activites, localisations, motsCles } ) ->
                        [ div [ DSFR.Grid.col6 ] <|
                            viewTags id Activites <|
                                activites
                        , div [ DSFR.Grid.col6 ] <|
                            viewTags id Zones <|
                                localisations
                        , div [ DSFR.Grid.col6 ] <|
                            viewTags id Mots <|
                                motsCles
                        ]

                    ( _, RD.Loading ) ->
                        [ text "Chargement en cours..." ]

                    ( _, RD.Failure _ ) ->
                        [ text "Erreur, veuillez contacter l'équipe de développement." ]

                    _ ->
                        [ text "Quelque chose aurait dû se passer..." ]
            ]
        ]


viewTag : EntityId EquipeId -> EtiquetteType -> EtiquetteAvecUsages -> Html Msg
viewTag equipeId tagType ({ label, usages } as tag) =
    div [ class "flex flex-row gap-4 items-center" ]
        [ div []
            [ span [ Typo.textBold ] [ text label ]
            , text " "
            , text "(utilisé "
            , text <| String.fromInt <| usages
            , text " fois)"
            ]
        , DSFR.Button.new
            { label = "Supprimer"
            , onClick =
                Just <|
                    ClickedDelete equipeId tagType tag
            }
            |> DSFR.Button.onlyIcon deleteFill
            |> DSFR.Button.view
        ]


viewTags : EntityId EquipeId -> EtiquetteType -> List EtiquetteAvecUsages -> List (Html Msg)
viewTags equipeId tagType tags =
    List.singleton <|
        div [ class "p-2" ] <|
            [ h2 [] [ text <| tagTypeToDisplay tagType ]
            , case tags of
                [] ->
                    span [ class "italic" ] [ text "Aucune étiquette." ]

                _ ->
                    DSFR.Accordion.raw
                        { id = "accordion-" ++ tagTypeToString tagType
                        , title =
                            text "Voir "
                                :: (if List.length tags > 1 then
                                        [ text "les ", text <| String.fromInt <| List.length tags, text " étiquettes" ]

                                    else
                                        [ text "l'étiquette"
                                        ]
                                   )
                        , content =
                            List.singleton <|
                                div [ class "flex flex-col gap-2" ] <|
                                    List.map (viewTag equipeId tagType) <|
                                        List.sortBy .label <|
                                            tags
                        , borderless = True
                        }
            ]


viewDeleteModal : Maybe DeleteRequest -> Html Msg
viewDeleteModal deleteRequest =
    let
        ( opened, title, content ) =
            case deleteRequest of
                Nothing ->
                    ( False, nothing, nothing )

                Just dr ->
                    ( True
                    , div [] [ h2 [] [ text "Suppression de «\u{00A0}", text dr.tag.label, text "\u{00A0}»" ] ]
                    , div [ class "flex flex-col gap-4" ]
                        [ if dr.tag.usages > 0 then
                            div []
                                [ p []
                                    [ text "Cette qualification est utilisée par "
                                    , text <| String.fromInt <| dr.tag.usages
                                    , text " entreprise"
                                    , text <| plural dr.tag.usages
                                    , text " ou créateur"
                                    , text <| plural dr.tag.usages
                                    , text " d'entreprises."
                                    ]
                                , p [] [ text "Confirmez-vous la suppression\u{00A0}?" ]
                                ]

                          else
                            text "Cette qualification n'est pas utilisée."
                        , DSFR.Button.group
                            [ DSFR.Button.new
                                { onClick = Just ConfirmedDelete
                                , label = "Confirmer la suppression"
                                }
                                |> DSFR.Button.withDisabled (dr.request == RD.Loading)
                            , DSFR.Button.new { onClick = Just CanceledDelete, label = "Annuler" }
                                |> DSFR.Button.secondary
                            ]
                            |> DSFR.Button.inline
                            |> DSFR.Button.alignedRightInverted
                            |> DSFR.Button.viewGroup
                        ]
                    )
    in
    DSFR.Modal.view
        { id = "delete-tag-modal"
        , label = "delete-tag-modal"
        , openMsg = NoOp
        , closeMsg = Just CanceledDelete
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first
