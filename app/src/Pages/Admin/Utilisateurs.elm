module Pages.Admin.Utilisateurs exposing (Model, Msg, page)

import Accessibility exposing (Html, div, formWithListeners, label, p, span, text)
import Api
import Api.Auth exposing (get, post)
import Api.EntityId exposing (EntityId, decodeEntityId, entityIdToString)
import Bytes exposing (Bytes)
import DSFR.Alert
import DSFR.Button
import DSFR.Grid
import DSFR.Icons exposing (iconMD)
import DSFR.Icons.Business
import DSFR.Icons.Design exposing (editFill)
import DSFR.Icons.System exposing (addLine, arrowDownLine, arrowUpLine, downloadLine, lockLine, lockUnlockLine)
import DSFR.Input
import DSFR.Modal
import DSFR.Pagination
import DSFR.SearchBar
import DSFR.Table
import DSFR.Typography as Typo
import Data.Equipe exposing (Equipe, decodeEquipe)
import Effect
import File
import File.Download
import File.Select
import Html
import Html.Attributes exposing (class)
import Html.Events as Events exposing (onClick)
import Html.Extra exposing (nothing, viewIf)
import Html.Lazy
import Http exposing (Response(..))
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode
import Lib.Date
import Lib.UI exposing (filterBy, withEmptyAs)
import QS
import RemoteData as RD exposing (WebData)
import Route
import Shared
import SingleSelect
import Spa.Page exposing (Page)
import Task
import Time exposing (Posix)
import TimeZone
import UI.Layout
import UI.Pagination exposing (Pagination, defaultPagination, paginationToQuery, queryToPagination)
import User exposing (UserId)
import View exposing (View)


type alias Model =
    { equipes : WebData (List Equipe)
    , utilisateurs : WebData (List UserWithAdminInfo)
    , userInput : Maybe UserInput
    , saveUserRequest : WebData SaveUserResponse
    , passwordInput : Maybe PasswordInput
    , savePasswordRequest : WebData ()
    , selectEquipe : SingleSelect.SmartSelect Msg (Maybe Equipe)
    , actifRequest : WebData ()
    , statsGeneration : WebData ()
    , filters : Filters
    , pagination : Pagination
    , import_ : Maybe Import
    , siretisation : Maybe Siretisation
    }


type alias Import =
    { compte : User
    , nom : String
    , contenu : String
    , taille : Int
    , derniereModification : Posix
    , requete : WebData ()
    }


defaultImport : User -> Import
defaultImport user =
    { compte = user
    , nom = ""
    , contenu = ""
    , taille = 0
    , derniereModification = Time.millisToPosix 0
    , requete = RD.NotAsked
    }


type alias Siretisation =
    { nom : String
    , contenu : String
    , taille : Int
    , derniereModification : Posix
    , requete : WebData Bytes

    -- , requete : WebData ()
    }


defaultSiretisation : Siretisation
defaultSiretisation =
    { nom = ""
    , contenu = ""
    , taille = 0
    , derniereModification = Time.millisToPosix 0
    , requete = RD.NotAsked
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | ReceivedEquipes (WebData (List Equipe))
    | ReceivedUsers (WebData ( List UserWithAdminInfo, Pagination ))
    | UpdatedRecherche String
    | ConfirmedRecherche
    | SelectedEquipe ( Maybe Equipe, SingleSelect.Msg (Maybe Equipe) )
    | UpdatedSelectEquipe (SingleSelect.Msg (Maybe Equipe))
    | ClickedAddUser
    | CanceledAddUser
    | UpdatedUserInput UserField String
    | ClickedEditPassword (EntityId UserId)
    | CanceledEditPassword
    | UpdatedPasswordInput PasswordField String
    | SelectedNewEquipe ( Equipe, SingleSelect.Msg Equipe )
    | UpdatedSelectNewEquipe (SingleSelect.Msg Equipe)
    | RequestedSaveUser
    | ReceivedSaveUser (WebData SaveUserResponse)
    | RequestedSavePassword
    | ReceivedSavePassword (WebData ())
    | SetSorting Header
    | ClickedToggleActif (EntityId UserId) Bool
    | ReceivedToggleActif (WebData ( List UserWithAdminInfo, Pagination ))
    | DoSearch ( Filters, Pagination )
    | ClickedAddImport User
    | ClickedAddSiretisation
    | ClickedCancelImport
    | ClickedCancelSiretisation
    | ClickedUploadImport
    | ClickedUploadSiretisation
    | UploadedImport File.File
    | UploadedSiretisation File.File
    | ReadImport String
    | ReadSiretisation String
    | ClickedRemovedImport
    | ClickedRemovedSiretisation
    | ConfirmedAddImport
    | ConfirmedAddSiretisation
    | ReceivedAddImport (WebData ())
    | ReceivedAddSiretisation (WebData Bytes)


type SaveUserResponse
    = SaveSuccess ( List UserWithAdminInfo, Pagination )
    | SaveError String



-- | ReceivedAddSiretisation (WebData ())


type UserField
    = UserEmail
    | UserIdentifiant
    | UserNom
    | UserPrenom


type PasswordField
    = Password
    | Repeat


type alias Filters =
    { recherche : String
    , equipe : Maybe Equipe
    , tri : Header
    , direction : Direction
    }


type alias UserWithAdminInfo =
    { user : User
    , equipe : String
    , lastLogin : Maybe Posix
    , lastAuth : Maybe Posix
    , accessUrl : Maybe String
    , enabled : Bool
    }


type alias User =
    { id : EntityId UserId
    , nom : String
    , prenom : String
    , email : String
    , identifiant : String
    , bienvenueEmail : Bool
    }


type Direction
    = Ascending
    | Descending


directionToString : Direction -> String
directionToString direction =
    case direction of
        Ascending ->
            "asc"

        Descending ->
            "desc"


stringToDirection : String -> Maybe Direction
stringToDirection string =
    case string of
        "asc" ->
            Just Ascending

        "desc" ->
            Just Descending

        _ ->
            Nothing


type alias UserInput =
    { email : String
    , identifiant : String
    , nom : String
    , prenom : String
    , nomEquipe : String
    , selectNewEquipe : SingleSelect.SmartSelect Msg Equipe
    , selectedNewEquipe : Maybe Equipe
    }


defaultUserInput : UserInput
defaultUserInput =
    { email = ""
    , identifiant = ""
    , nom = ""
    , prenom = ""
    , nomEquipe = ""
    , selectNewEquipe =
        SingleSelect.init "champ-selection-equipe"
            { selectionMsg = SelectedNewEquipe
            , internalMsg = UpdatedSelectNewEquipe
            }
    , selectedNewEquipe = Nothing
    }


type alias PasswordInput =
    { password : String
    , repeat : String
    , id : EntityId UserId
    }


defaultPasswordInput : EntityId UserId -> PasswordInput
defaultPasswordInput id =
    { password = ""
    , repeat = ""
    , id = id
    }


page : Shared.Shared -> Shared.User -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.onNewFlags (queryToFiltersAndPagination >> DoSearch) <|
        Spa.Page.element
            { view = view
            , init = init
            , update = update
            , subscriptions = .selectEquipe >> SingleSelect.subscriptions
            }


init : Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init rawQuery =
    let
        ( filters, pagination ) =
            queryToFiltersAndPagination rawQuery
    in
    ( { equipes = RD.Loading
      , utilisateurs = RD.Loading
      , userInput = Nothing
      , saveUserRequest = RD.NotAsked
      , passwordInput = Nothing
      , savePasswordRequest = RD.NotAsked
      , selectEquipe =
            SingleSelect.init "equipe-select"
                { selectionMsg = SelectedEquipe
                , internalMsg = UpdatedSelectEquipe
                }
      , actifRequest = RD.NotAsked
      , statsGeneration = RD.NotAsked
      , filters = filters
      , pagination = pagination
      , import_ = Nothing
      , siretisation = Nothing
      }
    , Effect.batch
        [ updateUrl
            { filters = filters
            , pagination = pagination
            }
        , getEquipes
        ]
    )
        |> Shared.pageChangeEffects


getUtilisateurs : String -> Effect.Effect Shared.Msg Msg
getUtilisateurs query =
    get
        { url = Api.getDevecosAdmin query
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedUsers
        }
        decodeBatchResponse
        |> Effect.fromCmd


decodeBatchResponse : Decoder ( List UserWithAdminInfo, Pagination )
decodeBatchResponse =
    Decode.succeed (\p pages results data -> ( data, Pagination p pages results ))
        |> andMap (Decode.field "page" Decode.int)
        |> andMap (Decode.field "pages" Decode.int)
        |> andMap (Decode.field "total" Decode.int)
        |> andMap (Decode.field "elements" <| Decode.list <| decodeAdminUtilisateur)


getEquipes : Effect.Effect Shared.Msg Msg
getEquipes =
    get
        { url = Api.getEquipesAdmin
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedEquipes
        }
        (Decode.list decodeEquipe)
        |> Effect.fromCmd


decodeAdminUtilisateur : Decoder UserWithAdminInfo
decodeAdminUtilisateur =
    Decode.succeed UserWithAdminInfo
        |> andMap decodeUser
        |> andMap (Decode.field "equipe" Decode.string)
        |> andMap (optionalNullableField "derniereConnexion" <| Lib.Date.decodeDateWithTimeFromISOString)
        |> andMap (optionalNullableField "derniereActionAuthentifiee" <| Lib.Date.decodeDateWithTimeFromISOString)
        |> andMap (optionalNullableField "urlAcces" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault False) (optionalNullableField "actif" Decode.bool))


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        ReceivedEquipes response ->
            let
                equipesSources =
                    response
                        |> RD.withDefault []

                newEquipe =
                    equipesSources
                        |> List.filter (\e -> (model.filters.equipe |> Maybe.map .id) == Just e.id)
                        |> List.head
            in
            { model
                | equipes = response
                , filters =
                    model.filters |> (\f -> { f | equipe = newEquipe })
            }
                |> Effect.withNone

        ReceivedUsers response ->
            case response of
                RD.Success ( fs, pagination ) ->
                    ( { model | utilisateurs = RD.Success fs, pagination = pagination }
                    , Effect.none
                    )

                _ ->
                    ( { model | utilisateurs = response |> RD.map Tuple.first }
                    , Effect.none
                    )

        UpdatedRecherche recherche ->
            { model
                | filters =
                    model.filters
                        |> (\f -> { f | recherche = recherche })
            }
                |> Effect.withNone

        ConfirmedRecherche ->
            let
                newModel =
                    { model | pagination = defaultPagination }
            in
            newModel
                |> Effect.with (updateUrl newModel)

        SelectedEquipe ( equipe, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectEquipe

                newModel =
                    { model
                        | selectEquipe = updatedSelect
                        , pagination = defaultPagination
                        , filters =
                            model.filters
                                |> (\f -> { f | equipe = equipe })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , updateUrl newModel
                ]
            )

        UpdatedSelectEquipe sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectEquipe
            in
            ( { model | selectEquipe = updatedSelect }, Effect.fromCmd selectCmd )

        UpdatedUserInput field value ->
            { model | userInput = model.userInput |> Maybe.map (updateUserInput field value) }
                |> Effect.withNone

        ClickedAddUser ->
            { model | userInput = Just defaultUserInput, saveUserRequest = RD.NotAsked }
                |> Effect.withNone

        CanceledAddUser ->
            { model | userInput = Nothing }
                |> Effect.withNone

        UpdatedPasswordInput field value ->
            { model | passwordInput = model.passwordInput |> Maybe.map (updatePasswordInput field value) }
                |> Effect.withNone

        ClickedEditPassword id ->
            { model | passwordInput = Just <| defaultPasswordInput id }
                |> Effect.withNone

        CanceledEditPassword ->
            { model | passwordInput = Nothing }
                |> Effect.withNone

        RequestedSaveUser ->
            case ( model.saveUserRequest, model.userInput ) of
                ( RD.Loading, _ ) ->
                    model
                        |> Effect.withNone

                ( _, Just userInput ) ->
                    { model | saveUserRequest = RD.Loading }
                        |> Effect.withCmd (saveUser userInput)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedSaveUser response ->
            case model.saveUserRequest of
                RD.Loading ->
                    case response of
                        RD.Success (SaveSuccess ( users, pagination )) ->
                            { model
                                | saveUserRequest = response
                                , userInput = Nothing
                                , utilisateurs = RD.Success users
                                , pagination = pagination
                            }
                                |> Effect.withNone

                        _ ->
                            ( { model | saveUserRequest = response }
                            , Effect.none
                            )

                _ ->
                    model
                        |> Effect.withNone

        RequestedSavePassword ->
            case ( model.savePasswordRequest, model.passwordInput ) of
                ( RD.Loading, _ ) ->
                    model
                        |> Effect.withNone

                ( _, Just passwordInput ) ->
                    { model | savePasswordRequest = RD.Loading }
                        |> Effect.withCmd (savePassword passwordInput)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedSavePassword response ->
            case model.savePasswordRequest of
                RD.Loading ->
                    case response of
                        RD.Success _ ->
                            { model
                                | savePasswordRequest = response
                                , passwordInput = Nothing
                            }
                                |> Effect.withNone

                        _ ->
                            { model | savePasswordRequest = response }
                                |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        SelectedNewEquipe ( newEquipe, sMsg ) ->
            case model.userInput of
                Nothing ->
                    model
                        |> Effect.withNone

                Just input ->
                    let
                        ( updatedSelect, selectCmd ) =
                            SingleSelect.update sMsg input.selectNewEquipe
                    in
                    ( { model | userInput = Just { input | selectNewEquipe = updatedSelect, selectedNewEquipe = Just newEquipe } }, Effect.fromCmd selectCmd )

        UpdatedSelectNewEquipe sMsg ->
            case model.userInput of
                Nothing ->
                    model
                        |> Effect.withNone

                Just input ->
                    let
                        ( updatedSelect, selectCmd ) =
                            SingleSelect.update sMsg input.selectNewEquipe
                    in
                    ( { model | userInput = Just { input | selectNewEquipe = updatedSelect } }, Effect.fromCmd selectCmd )

        SetSorting header ->
            let
                ( tri, direction ) =
                    case ( model.filters.tri, model.filters.direction ) of
                        ( h, Ascending ) ->
                            if h == header then
                                ( h, Descending )

                            else
                                ( header, Ascending )

                        ( h, Descending ) ->
                            if h == header then
                                ( h, Ascending )

                            else
                                ( header, Ascending )

                filters =
                    model.filters

                newModel =
                    { model | filters = { filters | tri = tri, direction = direction } }
            in
            ( newModel
            , updateUrl newModel
            )

        ClickedToggleActif userId on ->
            ( { model
                | actifRequest = RD.Loading
              }
            , Effect.fromCmd <| requestToggleActif userId on
            )

        ReceivedToggleActif (RD.Success r) ->
            ( { model | utilisateurs = RD.Success <| Tuple.first r, actifRequest = RD.NotAsked }, Effect.none )

        ReceivedToggleActif response ->
            ( { model | actifRequest = response |> RD.map (\_ -> ()) }, Effect.none )

        DoSearch ( filters, pagination ) ->
            let
                equipesSources =
                    model.equipes
                        |> RD.withDefault []

                newEquipe =
                    equipesSources
                        |> List.filter (\e -> (model.filters.equipe |> Maybe.map .id) == Just e.id)
                        |> List.head
            in
            { model
                | utilisateurs = RD.Loading
                , filters =
                    model.filters |> (\f -> { f | equipe = newEquipe })
                , pagination = pagination
            }
                |> Effect.with
                    (getUtilisateurs <|
                        filtersAndPaginationToQuery ( filters, pagination )
                    )

        ClickedAddImport user ->
            { model | import_ = Just <| defaultImport user }
                |> Effect.withNone

        ClickedAddSiretisation ->
            { model | siretisation = Just <| defaultSiretisation }
                |> Effect.withNone

        ClickedCancelImport ->
            { model | import_ = Nothing }
                |> Effect.withNone

        ClickedCancelSiretisation ->
            { model | siretisation = Nothing }
                |> Effect.withNone

        ClickedUploadImport ->
            model
                |> Effect.withCmd (File.Select.file [] UploadedImport)

        ClickedUploadSiretisation ->
            model
                |> Effect.withCmd (File.Select.file [] UploadedSiretisation)

        UploadedImport fichier ->
            { model
                | import_ =
                    model.import_
                        |> Maybe.map
                            (\i ->
                                { i
                                    | nom = File.name fichier
                                    , taille = File.size fichier
                                    , derniereModification = File.lastModified fichier
                                }
                            )
            }
                |> Effect.withCmd (Task.perform ReadImport <| File.toUrl fichier)

        UploadedSiretisation fichier ->
            { model
                | siretisation =
                    model.siretisation
                        |> Maybe.map
                            (\i ->
                                { i
                                    | nom = File.name fichier
                                    , taille = File.size fichier
                                    , derniereModification = File.lastModified fichier
                                }
                            )
            }
                |> Effect.withCmd (Task.perform ReadSiretisation <| File.toUrl fichier)

        ReadImport contenu ->
            { model
                | import_ =
                    model.import_
                        |> Maybe.map
                            (\i ->
                                { i
                                    | contenu = contenu
                                }
                            )
            }
                |> Effect.withNone

        ReadSiretisation contenu ->
            { model
                | siretisation =
                    model.siretisation
                        |> Maybe.map
                            (\i ->
                                { i
                                    | contenu = contenu
                                }
                            )
            }
                |> Effect.withNone

        ClickedRemovedImport ->
            { model
                | import_ =
                    model.import_
                        |> Maybe.map
                            (\i ->
                                { i
                                    | nom = ""
                                    , contenu = ""
                                    , requete = RD.NotAsked
                                }
                            )
            }
                |> Effect.withNone

        ClickedRemovedSiretisation ->
            { model
                | siretisation =
                    model.siretisation
                        |> Maybe.map
                            (\i ->
                                { i
                                    | nom = ""
                                    , contenu = ""
                                    , requete = RD.NotAsked
                                }
                            )
            }
                |> Effect.withNone

        ConfirmedAddImport ->
            { model
                | import_ =
                    model.import_
                        |> Maybe.map (\i -> { i | requete = RD.Loading })
            }
                |> Effect.with (uploadImport model.import_)

        ConfirmedAddSiretisation ->
            { model
                | siretisation =
                    model.siretisation
                        |> Maybe.map (\i -> { i | requete = RD.Loading })
            }
                |> Effect.with (uploadSiretisation model.siretisation)

        ReceivedAddImport resp ->
            { model
                | import_ =
                    model.import_
                        |> Maybe.map (\i -> { i | requete = resp })
            }
                |> Effect.withNone

        ReceivedAddSiretisation resp ->
            let
                cmd =
                    resp
                        |> RD.map (File.Download.bytes "siretisation.xlsx" "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                        |> RD.withDefault Cmd.none
            in
            { model
                | siretisation =
                    model.siretisation
                        |> Maybe.map (\i -> { i | requete = resp })
            }
                |> Effect.withCmd cmd


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle "Utilisateurs - Superadmin"
    , body = UI.Layout.lazyBody body model
    , route = Route.AdminUtilisateurs Nothing
    }


rechercherUtilisateursInputName : String
rechercherUtilisateursInputName =
    "recherches-utilisateurs"


body : Model -> Html Msg
body model =
    div [ DSFR.Grid.gridRow ]
        [ Html.Lazy.lazy3 addUserModal model.equipes model.saveUserRequest model.userInput
        , Html.Lazy.lazy2 addPasswordModal model.savePasswordRequest model.passwordInput
        , Html.Lazy.lazy viewImportModal model.import_
        , Html.Lazy.lazy viewSiretisationModal model.siretisation
        , Html.Lazy.lazy6 usersTable model.equipes model.actifRequest model.selectEquipe model.filters model.pagination model.utilisateurs
        ]


usersTable : WebData (List Equipe) -> WebData () -> SingleSelect.SmartSelect Msg (Maybe Equipe) -> Filters -> Pagination -> WebData (List UserWithAdminInfo) -> Html Msg
usersTable equipesRD actifRequest selectEquipe filters pagination users =
    let
        equipes =
            equipesRD
                |> RD.withDefault []
    in
    div [ DSFR.Grid.col12, class "p-4 fr-card--white" ]
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col, class "flex flex-row gap-4 justify-end" ]
                [ DSFR.Button.new { label = "Exporter les utilisateurs", onClick = Nothing }
                    |> DSFR.Button.linkButton Api.getDevecosCsvAdmin
                    |> DSFR.Button.leftIcon downloadLine
                    |> DSFR.Button.withAttrs [ Html.Attributes.target "_self" ]
                    |> DSFR.Button.secondary
                    |> DSFR.Button.view
                , DSFR.Button.new { label = "Ajouter un utilisateur", onClick = Just ClickedAddUser }
                    |> DSFR.Button.leftIcon addLine
                    |> DSFR.Button.view
                , DSFR.Button.new { label = "Siretiser", onClick = Just <| ClickedAddSiretisation }
                    |> DSFR.Button.view
                ]
            ]
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "!mb-4" ]
            [ div [ DSFR.Grid.col6, class "flex flex-col gap-2" ] <|
                [ label
                    [ class "fr-label"
                    , Html.Attributes.for rechercherUtilisateursInputName
                    ]
                    [ text "Rechercher un utilisateur" ]
                , DSFR.SearchBar.searchBar { errors = [], extraAttrs = [], disabled = False }
                    { submitMsg = ConfirmedRecherche
                    , buttonLabel = "Rechercher"
                    , inputMsg = UpdatedRecherche
                    , inputLabel = "Rechercher un utilisateur"
                    , inputPlaceholder = Just "Recherche par identifiant, email, nom, prénom"
                    , inputId = rechercherUtilisateursInputName
                    , inputValue = filters.recherche
                    , hints = []
                    , fullLabel = Nothing
                    }
                ]
            , div [ DSFR.Grid.col6 ] <|
                List.singleton <|
                    SingleSelect.viewCustom
                        { selected = Just filters.equipe
                        , options =
                            equipes
                                |> List.sortBy (.nom >> String.toLower)
                                |> List.map Just
                                |> (::) Nothing
                        , optionLabelFn = Maybe.map Data.Equipe.nom >> Maybe.withDefault "Tous"
                        , isDisabled = False
                        , optionDescriptionFn = \_ -> ""
                        , optionsContainerMaxHeight = 300
                        , selectTitle = span [ Typo.textBold ] [ text "Équipe" ]
                        , searchPrompt = "Rechercher une équipe"
                        , noResultsForMsg =
                            \searchText ->
                                "Aucune autre équipe n'a été trouvée"
                                    ++ (if searchText == "" then
                                            ""

                                        else
                                            " pour " ++ searchText
                                       )
                        , noOptionsMsg = "Aucune équipe n'a été trouvée"
                        , searchFn = filterBy (Maybe.map Data.Equipe.nom >> Maybe.withDefault "")
                        }
                        selectEquipe
            ]
        , div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col ] <|
                List.singleton <|
                    viewUsersTable actifRequest filters pagination users
            ]
        ]


viewUsersTable : WebData () -> Filters -> Pagination -> WebData (List UserWithAdminInfo) -> Html Msg
viewUsersTable actifRequest filters pagination data =
    case data of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Chargement en cours"
                    ]
                ]

        RD.Failure _ ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Une erreur s'est produite, veuillez recharger la page."
                    ]
                ]

        RD.Success [] ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Aucune utilisateur correspondant."
                    ]
                ]

        RD.Success users ->
            div [ DSFR.Grid.col12 ]
                [ DSFR.Table.table
                    { id = "tableau-utilisateurs"
                    , caption = text "Utilisateurs"
                    , headers = headers
                    , rows = users
                    , toHeader =
                        \header ->
                            let
                                sortOnClick =
                                    if isSortable header then
                                        [ onClick <| SetSorting <| header, class "cursor-pointer" ]

                                    else
                                        []

                                sortIcon =
                                    if isSortable header then
                                        sortingIcon ( filters.tri, filters.direction ) header

                                    else
                                        nothing
                            in
                            Html.span sortOnClick
                                [ headerToDisplay header |> text
                                , sortIcon
                                ]
                    , toRowId = .user >> .email
                    , toCell = toCell actifRequest
                    }
                    |> DSFR.Table.withContainerAttrs [ class "!mb-0" ]
                    |> DSFR.Table.withToRowAttrs
                        (\{ enabled } ->
                            if enabled then
                                []

                            else
                                [ class "opacity-[0.5]"
                                , Html.Attributes.title "Compte désactivé"
                                ]
                        )
                    |> DSFR.Table.noBorders
                    |> DSFR.Table.captionHidden
                    |> DSFR.Table.fixed
                    |> DSFR.Table.view
                , if pagination.pages > 1 then
                    div [ class "!mt-4" ]
                        [ DSFR.Pagination.view pagination.page pagination.pages <|
                            toHref ( filters, pagination )
                        ]

                  else
                    nothing
                ]


sortingIcon : ( Header, Direction ) -> Header -> Html msg
sortingIcon ( sortingHeader, direction ) header =
    if header == sortingHeader then
        case direction of
            Ascending ->
                iconMD arrowDownLine

            Descending ->
                iconMD arrowUpLine

    else
        nothing


type Header
    = HIdentifiant
    | HEmail
    | HPrenom
    | HNom
    | HEquipeNom
    | HLogin
    | HAuth
    | HToken
    | HPassword
    | HActif
    | HWelcome
    | HUpload


stringToHeader : String -> Maybe Header
stringToHeader header =
    case header of
        "nom" ->
            Just HNom

        "prenom" ->
            Just HPrenom

        "email" ->
            Just HEmail

        "connexion" ->
            Just HLogin

        "action" ->
            Just HAuth

        _ ->
            Nothing


headers : List Header
headers =
    [ HIdentifiant
    , HEmail
    , HPrenom
    , HNom
    , HEquipeNom
    , HLogin
    , HAuth
    , HToken
    , HPassword
    , HActif
    , HWelcome
    , HUpload
    ]


isSortable : Header -> Bool
isSortable header =
    not <|
        List.member header [ HEquipeNom, HToken, HPassword ]


headerToDisplay : Header -> String
headerToDisplay header =
    case header of
        HIdentifiant ->
            "Identifiant"

        HEmail ->
            "Email"

        HNom ->
            "Nom"

        HPrenom ->
            "Prénom"

        HEquipeNom ->
            "Équipe"

        HLogin ->
            "Dernier login"

        HAuth ->
            "Dernière action"

        HToken ->
            "Lien"

        HPassword ->
            ""

        HActif ->
            ""

        HWelcome ->
            ""

        HUpload ->
            ""


headerToString : Header -> String
headerToString header =
    case header of
        HIdentifiant ->
            "identifiant"

        HEmail ->
            "email"

        HNom ->
            "nom"

        HPrenom ->
            "prenom"

        HEquipeNom ->
            "equipe"

        HLogin ->
            "connexion"

        HAuth ->
            "action"

        HToken ->
            "lien"

        _ ->
            ""


toCell : WebData () -> Header -> UserWithAdminInfo -> Html Msg
toCell actifRequest header { user, equipe, accessUrl, lastLogin, lastAuth, enabled } =
    let
        pendingActif =
            actifRequest == RD.Loading
    in
    case header of
        HIdentifiant ->
            span [ Html.Attributes.style "overflow-wrap" "break-word" ] <|
                List.singleton <|
                    text <|
                        withEmptyAs "-" <|
                            .identifiant user

        HEmail ->
            span [ Html.Attributes.style "overflow-wrap" "break-word" ] <|
                List.singleton <|
                    text <|
                        withEmptyAs "-" <|
                            .email user

        HPrenom ->
            text <|
                withEmptyAs "-" <|
                    .prenom user

        HNom ->
            text <|
                withEmptyAs "-" <|
                    .nom user

        HEquipeNom ->
            equipe
                |> withEmptyAs "-"
                |> text

        HLogin ->
            lastLogin
                |> Maybe.map (Lib.Date.dateTimeToShortFrenchString (TimeZone.europe__paris ()))
                |> Maybe.withDefault "-"
                |> text

        HAuth ->
            lastAuth
                |> Maybe.map (Lib.Date.dateTimeToShortFrenchString (TimeZone.europe__paris ()))
                |> Maybe.withDefault "-"
                |> text

        HToken ->
            accessUrl
                |> Maybe.withDefault "-"
                |> withEmptyAs "-"
                |> text
                |> List.singleton
                |> span [ class "break-all" ]

        HPassword ->
            DSFR.Button.new { label = "", onClick = Just <| ClickedEditPassword <| .id <| user }
                |> DSFR.Button.withAttrs [ Html.Attributes.name <| "edit-password-" ++ (entityIdToString <| .id user) ]
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.onlyIcon editFill
                |> DSFR.Button.view

        HActif ->
            DSFR.Button.new { label = "", onClick = Just <| ClickedToggleActif (.id <| user) <| not enabled }
                |> DSFR.Button.withAttrs
                    [ Html.Attributes.name <| "edit-actif-" ++ (entityIdToString <| .id user)
                    , Html.Attributes.title <|
                        if enabled then
                            "Désactiver"

                        else
                            "Activer"
                    ]
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withDisabled pendingActif
                |> DSFR.Button.onlyIcon
                    (if enabled then
                        lockLine

                     else
                        lockUnlockLine
                    )
                |> DSFR.Button.view

        HWelcome ->
            let
                ( title, colorClass ) =
                    if user |> .bienvenueEmail then
                        ( "Email de bienvenue envoyé", "blue-text" )

                    else
                        ( "Email de bienvenue pas encore envoyé", "red-text" )
            in
            span [ class colorClass, Html.Attributes.title title ] [ DSFR.Icons.icon <| DSFR.Icons.Business.sendPlaneFill ]

        HUpload ->
            DSFR.Button.new { label = "", onClick = Just <| ClickedAddImport user }
                |> DSFR.Button.withAttrs [ Html.Attributes.title "Importer des données" ]
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.onlyIcon DSFR.Icons.System.uploadLine
                |> DSFR.Button.view


addUserModal : WebData (List Equipe) -> WebData SaveUserResponse -> Maybe UserInput -> Html Msg
addUserModal equipes request userInput =
    let
        ( opened, content, title ) =
            case userInput of
                Nothing ->
                    ( False, nothing, nothing )

                Just input ->
                    ( True, viewUserForm equipes request input, text "Ajouter un utilisateur" )
    in
    DSFR.Modal.view
        { id = "utilisateur"
        , label = "utilisateur"
        , openMsg = NoOp
        , closeMsg = Just CanceledAddUser
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


addPasswordModal : WebData () -> Maybe PasswordInput -> Html Msg
addPasswordModal request passwordInput =
    let
        ( opened, content, title ) =
            case passwordInput of
                Nothing ->
                    ( False, nothing, nothing )

                Just input ->
                    ( True, viewPasswordForm request input, text "Remplacer le mot de passe de l'utilisateur" )
    in
    DSFR.Modal.view
        { id = "password"
        , label = "password"
        , openMsg = NoOp
        , closeMsg = Just CanceledEditPassword
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


updateUserInput : UserField -> String -> UserInput -> UserInput
updateUserInput field value userInput =
    case field of
        UserIdentifiant ->
            { userInput | identifiant = value }

        UserEmail ->
            { userInput
                | email = value
                , identifiant =
                    if userInput.identifiant == userInput.email then
                        value

                    else
                        userInput.identifiant
            }

        UserNom ->
            { userInput | nom = value }

        UserPrenom ->
            { userInput | prenom = value }


updatePasswordInput : PasswordField -> String -> PasswordInput -> PasswordInput
updatePasswordInput field value passwordInput =
    case field of
        Password ->
            { passwordInput | password = value }

        Repeat ->
            { passwordInput | repeat = value }


viewUserForm : WebData (List Equipe) -> WebData SaveUserResponse -> UserInput -> Html Msg
viewUserForm equipes request input =
    formWithListeners [ Events.onSubmit <| RequestedSaveUser, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ DSFR.Input.new { value = input.prenom, label = text "Prénom", onInput = UpdatedUserInput UserPrenom, name = "prenom" }
            |> DSFR.Input.withRequired True
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , DSFR.Input.new { value = input.nom, label = text "Nom", onInput = UpdatedUserInput UserNom, name = "nom" }
            |> DSFR.Input.withRequired True
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , DSFR.Input.new { value = input.email, label = text "Email", onInput = UpdatedUserInput UserEmail, name = "email" }
            |> DSFR.Input.withRequired True
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , DSFR.Input.new { value = input.identifiant, label = text "Identifiant", onInput = UpdatedUserInput UserIdentifiant, name = "identifiant" }
            |> DSFR.Input.withRequired True
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , div [ DSFR.Grid.col6 ]
            [ input.selectNewEquipe
                |> SingleSelect.viewCustom
                    { isDisabled = False
                    , selected = input.selectedNewEquipe
                    , options = equipes |> RD.withDefault [] |> List.sortBy (.nom >> String.toLower)
                    , optionLabelFn = Data.Equipe.nom
                    , optionDescriptionFn = \_ -> ""
                    , optionsContainerMaxHeight = 300
                    , selectTitle = span [ Typo.textBold ] [ text "Équipe" ]
                    , searchPrompt = "Rechercher une équipe"
                    , noResultsForMsg =
                        \searchText ->
                            "Aucune autre équipe n'a été trouvée"
                                ++ (if searchText == "" then
                                        ""

                                    else
                                        " pour " ++ searchText
                                   )
                    , noOptionsMsg = "Aucune équipe n'a été trouvée"
                    , searchFn = filterBy Data.Equipe.nom
                    }
            ]
        , case request of
            RD.Success (SaveError error) ->
                DSFR.Alert.small { title = Just "Création échouée", description = text <| "Erreur lors de la création : " ++ error }
                    |> DSFR.Alert.alert Nothing DSFR.Alert.error
                    |> List.singleton
                    |> div [ class "flex justify-center items-center p-4 w-full" ]

            RD.Failure _ ->
                DSFR.Alert.small { title = Just "Création échouée", description = text "Une erreur s'est produite, veuillez réessayer." }
                    |> DSFR.Alert.alert Nothing DSFR.Alert.error
                    |> List.singleton
                    |> div [ class "flex justify-center items-center p-4 w-full" ]

            _ ->
                nothing
        , div [ DSFR.Grid.col12 ]
            [ footerUtilisateur request input
            ]
        ]


viewPasswordForm : WebData () -> PasswordInput -> Html Msg
viewPasswordForm request input =
    formWithListeners [ Events.onSubmit <| RequestedSavePassword, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ DSFR.Input.new
            { value = input.password
            , label = text "Mot de passe"
            , onInput = UpdatedPasswordInput Password
            , name = "password"
            }
            |> DSFR.Input.withRequired True
            |> DSFR.Input.newPassword
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , DSFR.Input.new
            { value = input.repeat
            , label = text "Confirmez le mot de passe"
            , onInput = UpdatedPasswordInput Repeat
            , name = "repeat"
            }
            |> DSFR.Input.withRequired True
            |> DSFR.Input.confirmPassword
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , case request of
            RD.Failure _ ->
                DSFR.Alert.small { title = Just "Modification du mot de passe échouée", description = text "Assurez-vous que les deux mots de passe sont identiques et que le mot de passe fait au moins 10 caractères." }
                    |> DSFR.Alert.alert Nothing DSFR.Alert.error
                    |> List.singleton
                    |> div [ class "flex justify-center items-center p-4 w-full" ]

            _ ->
                nothing
        , div [ DSFR.Grid.col12 ]
            [ footerPassword request input
            ]
        ]


footerUtilisateur : WebData SaveUserResponse -> UserInput -> Html Msg
footerUtilisateur request userInput =
    let
        valid =
            (userInput.prenom /= "")
                && (userInput.nom /= "")
                && (userInput.email /= "")
                && (userInput.selectedNewEquipe /= Nothing)

        ( isButtonDisabled, title ) =
            case ( request, valid ) of
                ( RD.Loading, _ ) ->
                    ( True, "Enregistrer" )

                ( _, False ) ->
                    ( True, "Enregistrer" )

                _ ->
                    ( False, "Enregistrer" )
    in
    DSFR.Button.group
        [ DSFR.Button.new { onClick = Nothing, label = "Enregistrer" }
            |> DSFR.Button.submit
            |> DSFR.Button.withAttrs [ Html.Attributes.title title, Html.Attributes.name "submit-new-user" ]
            |> DSFR.Button.withDisabled isButtonDisabled
        , DSFR.Button.new { onClick = Just CanceledAddUser, label = "Annuler" }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


footerPassword : WebData () -> PasswordInput -> Html Msg
footerPassword request passwordInput =
    let
        valid =
            passwordInput.password /= "" && passwordInput.repeat /= "" && passwordInput.password == passwordInput.repeat

        ( isButtonDisabled, title ) =
            case ( request, valid ) of
                ( RD.Loading, _ ) ->
                    ( True, "Enregistrer" )

                ( _, False ) ->
                    ( True, "Enregistrer" )

                _ ->
                    ( False, "Enregistrer" )
    in
    DSFR.Button.group
        [ DSFR.Button.new { onClick = Nothing, label = "Enregistrer" }
            |> DSFR.Button.submit
            |> DSFR.Button.withAttrs [ Html.Attributes.title title, Html.Attributes.name "new-password" ]
            |> DSFR.Button.withDisabled isButtonDisabled
        , DSFR.Button.new { onClick = Just CanceledEditPassword, label = "Annuler" }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


saveUser : UserInput -> Cmd Msg
saveUser { nom, prenom, email, identifiant, selectedNewEquipe } =
    case selectedNewEquipe of
        Nothing ->
            Cmd.none

        Just equipe ->
            let
                jsonBody =
                    [ ( "nom", Encode.string nom )
                    , ( "prenom", Encode.string prenom )
                    , ( "identifiant", Encode.string identifiant )
                    , ( "email", Encode.string email )
                    , ( "compteTypeId", Encode.string "deveco" )
                    , ( "equipeId", Api.EntityId.encodeEntityId <| Data.Equipe.id <| equipe )
                    ]
                        |> Encode.object
                        |> Http.jsonBody
            in
            post
                { url = Api.createCompteAdmin
                , body = jsonBody
                }
                { toShared = SharedMsg
                , logger = Just LogError
                , handler = ReceivedSaveUser
                }
                (Decode.oneOf
                    [ Decode.map SaveError <|
                        Decode.field "error" <|
                            Decode.string
                    , Decode.map SaveSuccess <|
                        decodeBatchResponse
                    ]
                )


savePassword : PasswordInput -> Cmd Msg
savePassword { password, repeat, id } =
    let
        jsonBody =
            [ ( "motDePasse", Encode.string password )
            , ( "repetition", Encode.string repeat )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    post
        { url = Api.updateMotDePasseAdmin <| entityIdToString id
        , body = jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSavePassword
        }
        (Decode.succeed ())


requestToggleActif : EntityId UserId -> Bool -> Cmd Msg
requestToggleActif id on =
    let
        jsonBody =
            [ ( "actif", Encode.bool on )
            , ( "compteId", Api.EntityId.encodeEntityId id )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    post
        { url = Api.toggleCompteActifAdmin
        , body = jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedToggleActif
        }
        decodeBatchResponse


queryToFiltersAndPagination : Maybe String -> ( Filters, Pagination )
queryToFiltersAndPagination rawQuery =
    rawQuery
        |> Maybe.withDefault ""
        |> (\query -> ( queryToFilters query, queryToPagination query ))


queryToFilters : String -> Filters
queryToFilters rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    recherche =
                        query
                            |> QS.getAsStringList "recherche"
                            |> List.head
                            |> Maybe.withDefault ""

                    equipe =
                        query
                            |> QS.getAsStringList "equipeId"
                            |> List.head
                            |> Maybe.andThen Api.EntityId.parseEntityId
                            |> Maybe.map (\id -> Equipe id "" False "" "" "" "" "")

                    tri =
                        query
                            |> QS.getAsStringList "tri"
                            |> List.head
                            |> Maybe.andThen stringToHeader
                            |> Maybe.withDefault HNom

                    direction =
                        query
                            |> QS.getAsStringList "direction"
                            |> List.head
                            |> Maybe.andThen stringToDirection
                            |> Maybe.withDefault Ascending
                in
                { recherche = recherche
                , equipe = equipe
                , tri = tri
                , direction = direction
                }
           )


filtersAndPaginationToQuery : ( Filters, Pagination ) -> String
filtersAndPaginationToQuery ( recherche, pagination ) =
    QS.merge (filtersToQuery recherche) (paginationToQuery pagination)
        |> QS.serialize (QS.config |> QS.addQuestionMark False)


filtersToQuery : Filters -> QS.Query
filtersToQuery { recherche, equipe, tri, direction } =
    let
        setRecherche =
            case recherche of
                "" ->
                    identity

                _ ->
                    QS.setStr "recherche" recherche

        setEquipe =
            case equipe of
                Nothing ->
                    identity

                Just e ->
                    QS.setStr "equipeId" <| entityIdToString e.id

        setTri =
            case tri of
                HNom ->
                    identity

                _ ->
                    QS.setStr "tri" <| headerToString tri

        setDirection =
            case direction of
                Ascending ->
                    identity

                Descending ->
                    QS.setStr "direction" <| directionToString direction
    in
    QS.empty
        |> setRecherche
        |> setEquipe
        |> setTri
        |> setDirection


toHref : ( Filters, Pagination ) -> Int -> String
toHref ( filters, pagination ) newPage =
    Route.toUrl <|
        Route.AdminUtilisateurs <|
            Just <|
                filtersAndPaginationToQuery ( filters, { pagination | page = newPage } )


updateUrl : { data | filters : Filters, pagination : Pagination } -> Effect.Effect Shared.Msg Msg
updateUrl { filters, pagination } =
    Effect.fromShared <|
        Shared.Navigate <|
            Route.AdminUtilisateurs <|
                Just <|
                    filtersAndPaginationToQuery ( filters, pagination )


decodeUser : Decoder User
decodeUser =
    Decode.succeed User
        |> andMap (Decode.field "id" <| decodeEntityId)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "prenom" Decode.string)
        |> andMap (Decode.field "email" Decode.string)
        |> andMap (Decode.field "identifiant" Decode.string)
        |> andMap (Decode.field "bienvenueEmail" Decode.bool)


uploadImport : Maybe Import -> Effect.Effect Shared.Msg Msg
uploadImport import_ =
    case import_ of
        Nothing ->
            Effect.none

        Just { nom, contenu, taille, derniereModification, compte } ->
            Effect.fromCmd <|
                post
                    { url = Api.uploadImport compte.id
                    , body =
                        [ ( "nom", Encode.string nom )
                        , ( "contenu", Encode.string contenu )
                        , ( "taille", Encode.int taille )
                        , ( "derniereModification", Encode.int <| Time.posixToMillis derniereModification )
                        ]
                            |> Encode.object
                            |> Http.jsonBody
                    }
                    { toShared = SharedMsg
                    , logger = Just LogError
                    , handler = ReceivedAddImport
                    }
                    (Decode.succeed ())


viewImportModal : Maybe Import -> Html Msg
viewImportModal import_ =
    let
        ( opened, content, title ) =
            case import_ of
                Nothing ->
                    ( False, nothing, nothing )

                Just i ->
                    ( True
                    , viewImportBody i
                    , text "Importer un fichier"
                    )
    in
    DSFR.Modal.view
        { id = "nouveau-import"
        , label = "nouveau-import"
        , openMsg = NoOp
        , closeMsg = Just ClickedCancelImport
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


viewImportBody : Import -> Html Msg
viewImportBody import_ =
    div [ class "p-4" ]
        [ div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col12 ]
                [ case import_.requete of
                    RD.Loading ->
                        div []
                            [ text <| "Le téléversement est en cours..."
                            ]

                    RD.Success _ ->
                        div []
                            [ p []
                                [ text <|
                                    "Le fichier "
                                        ++ import_.nom
                                        ++ " a été téléversé avec succès\u{00A0}!"
                                ]
                            , p []
                                [ text <|
                                    "L'import est en cours et peut prendre plusieurs minutes. Un email sera envoyé à la fin de l'import."
                                ]
                            ]

                    _ ->
                        div []
                            [ div
                                [ class "fr-upload-group"
                                ]
                                [ label
                                    [ class "fr-label"
                                    , Html.Attributes.for "file-upload"
                                    ]
                                    [ text "Fichier d'import"
                                    , span
                                        [ class "fr-hint-text"
                                        ]
                                        [ text "Fichier conforme au modèle d'import" ]
                                    , case import_.nom of
                                        "" ->
                                            DSFR.Button.new { label = "Choisir...", onClick = Just ClickedUploadImport }
                                                |> DSFR.Button.tertiaryNoOutline
                                                |> DSFR.Button.view

                                        nom ->
                                            div [ class "flex flex-row gap-2 items-center" ]
                                                [ text nom
                                                , DSFR.Button.new { label = "", onClick = Just ClickedRemovedImport }
                                                    |> DSFR.Button.onlyIcon DSFR.Icons.System.closeLine
                                                    |> DSFR.Button.tertiaryNoOutline
                                                    |> DSFR.Button.view
                                                ]
                                    ]
                                ]
                            , viewIf (RD.isFailure import_.requete) (text "Une erreur s'est produite, veuillez réessayer.")
                            ]
                ]
            , div [ DSFR.Grid.col12 ]
                [ let
                    valid =
                        (import_.nom /= "")
                            && (import_.contenu /= "")

                    ( isButtonDisabled, title_ ) =
                        case ( import_.requete, valid ) of
                            ( RD.Loading, _ ) ->
                                ( True, "Enregistrer" )

                            ( _, False ) ->
                                ( True, "Enregistrer" )

                            _ ->
                                ( False, "Enregistrer" )

                    buttons =
                        case import_.requete of
                            RD.Success _ ->
                                [ DSFR.Button.new { onClick = Just ClickedCancelImport, label = "OK" }
                                ]

                            _ ->
                                [ DSFR.Button.new { onClick = Just ConfirmedAddImport, label = "Enregistrer" }
                                    |> DSFR.Button.withAttrs [ Html.Attributes.title title_, Html.Attributes.name "ajout-nouveau-zonage" ]
                                    |> DSFR.Button.withDisabled isButtonDisabled
                                , DSFR.Button.new { onClick = Just ClickedCancelImport, label = "Annuler" }
                                    |> DSFR.Button.secondary
                                ]
                  in
                  DSFR.Button.group buttons
                    |> DSFR.Button.inline
                    |> DSFR.Button.alignedRightInverted
                    |> DSFR.Button.viewGroup
                ]
            ]
        ]


uploadSiretisation : Maybe Siretisation -> Effect.Effect Shared.Msg Msg
uploadSiretisation siretisation =
    case siretisation of
        Nothing ->
            Effect.none

        Just { nom, contenu, taille, derniereModification } ->
            Effect.fromCmd <|
                Http.post
                    { url = Api.uploadSiretisation
                    , body =
                        [ ( "nom", Encode.string nom )
                        , ( "contenu", Encode.string contenu )
                        , ( "taille", Encode.int taille )
                        , ( "derniereModification", Encode.int <| Time.posixToMillis derniereModification )
                        ]
                            |> Encode.object
                            |> Http.jsonBody
                    , expect = Http.expectBytesResponse (bytesResultToWebData >> ReceivedAddSiretisation) getBytes
                    }



-- post
-- { url = Api.uploadSiretisation
-- , body =
--     [ ( "nom", Encode.string nom )
--     , ( "contenu", Encode.string contenu )
--     , ( "taille", Encode.int taille )
--     , ( "derniereModification", Encode.int <| Time.posixToMillis derniereModification )
--     ]
--         |> Encode.object
--         |> Http.jsonBody
-- }
-- { toShared = SharedMsg
-- , logger = Just LogError
-- , handler = ReceivedAddSiretisation
-- }
-- (Decode.succeed ())


getBytes : Response Bytes -> Result () Bytes
getBytes response =
    case response of
        GoodStatus_ _ bytes ->
            Ok bytes

        _ ->
            Err ()


bytesResultToWebData : Result () Bytes -> WebData Bytes
bytesResultToWebData result =
    case result of
        Ok bytes ->
            RD.Success bytes

        _ ->
            RD.Failure (Http.BadBody "")


viewSiretisationModal : Maybe Siretisation -> Html Msg
viewSiretisationModal siretisation =
    let
        ( opened, content, title ) =
            case siretisation of
                Nothing ->
                    ( False, nothing, nothing )

                Just i ->
                    ( True
                    , viewSiretisationBody i
                    , text "Siretiser un fichier"
                    )
    in
    DSFR.Modal.view
        { id = "nouveau-siretisation"
        , label = "nouveau-siretisation"
        , openMsg = NoOp
        , closeMsg = Just ClickedCancelSiretisation
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


viewSiretisationBody : Siretisation -> Html Msg
viewSiretisationBody siretisation =
    div [ class "p-4" ]
        [ div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col12 ]
                [ case siretisation.requete of
                    RD.Loading ->
                        div []
                            [ text <| "Le téléversement est en cours..."
                            ]

                    RD.Success _ ->
                        div []
                            [ p []
                                [ text <|
                                    "Le fichier "
                                        ++ siretisation.nom
                                        ++ " a été téléversé avec succès\u{00A0}!"
                                ]
                            , p []
                                [ text <|
                                    "La siretisation est en cours et peut prendre plusieurs minutes."
                                ]
                            ]

                    _ ->
                        div []
                            [ div
                                [ class "fr-upload-group"
                                ]
                                [ label
                                    [ class "fr-label"
                                    , Html.Attributes.for "file-upload"
                                    ]
                                    [ text "Fichier de siretisation"
                                    , span
                                        [ class "fr-hint-text"
                                        ]
                                        [ text "Fichier conforme au modèle de siretisation" ]
                                    , case siretisation.nom of
                                        "" ->
                                            DSFR.Button.new { label = "Choisir...", onClick = Just ClickedUploadSiretisation }
                                                |> DSFR.Button.tertiaryNoOutline
                                                |> DSFR.Button.view

                                        nom ->
                                            div [ class "flex flex-row gap-2 items-center" ]
                                                [ text nom
                                                , DSFR.Button.new { label = "", onClick = Just ClickedRemovedSiretisation }
                                                    |> DSFR.Button.onlyIcon DSFR.Icons.System.closeLine
                                                    |> DSFR.Button.tertiaryNoOutline
                                                    |> DSFR.Button.view
                                                ]
                                    ]
                                ]
                            , viewIf (RD.isFailure siretisation.requete) (text "Une erreur s'est produite, veuillez réessayer.")
                            ]
                ]
            , div [ DSFR.Grid.col12 ]
                [ let
                    valid =
                        (siretisation.nom /= "")
                            && (siretisation.contenu /= "")

                    ( isButtonDisabled, title_ ) =
                        case ( siretisation.requete, valid ) of
                            ( RD.Loading, _ ) ->
                                ( True, "Enregistrer" )

                            ( _, False ) ->
                                ( True, "Enregistrer" )

                            _ ->
                                ( False, "Enregistrer" )

                    buttons =
                        case siretisation.requete of
                            RD.Success _ ->
                                [ DSFR.Button.new { onClick = Just ClickedCancelSiretisation, label = "OK" }
                                ]

                            _ ->
                                [ DSFR.Button.new { onClick = Just ConfirmedAddSiretisation, label = "Enregistrer" }
                                    |> DSFR.Button.withAttrs [ Html.Attributes.title title_, Html.Attributes.name "ajout-nouveau-zonage" ]
                                    |> DSFR.Button.withDisabled isButtonDisabled
                                , DSFR.Button.new { onClick = Just ClickedCancelSiretisation, label = "Annuler" }
                                    |> DSFR.Button.secondary
                                ]
                  in
                  DSFR.Button.group buttons
                    |> DSFR.Button.inline
                    |> DSFR.Button.alignedRightInverted
                    |> DSFR.Button.viewGroup
                ]
            ]
        ]
