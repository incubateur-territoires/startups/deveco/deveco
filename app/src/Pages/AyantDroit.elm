module Pages.AyantDroit exposing (Model, page)

import Accessibility exposing (Html, br, div, h1, h2, h3, p, span, text)
import Api
import Api.Auth exposing (get, post)
import DSFR.Button
import DSFR.Checkbox
import DSFR.Grid
import DSFR.Typography as Typo
import Data.EquipeDemande exposing (EquipeDemande, decodeEquipeDemande)
import Effect exposing (Effect)
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing)
import Http
import Lib.Date
import RemoteData as RD exposing (WebData)
import Route
import Shared
import Spa.Page exposing (Page)
import Time
import UI.Layout
import View exposing (View)


type alias Model =
    { token : String
    , accord : Bool
    , equipeDemande : WebData EquipeDemande
    , confirmation : WebData EquipeDemande
    }


type Msg
    = ToggleAccord Bool
    | ReceivedEquipeDemande (WebData EquipeDemande)
    | Confirm
    | ReceivedConfirmation (WebData EquipeDemande)
    | SharedMsg Shared.Msg


page : Shared.Shared -> Page String Shared.Msg (View Msg) Model Msg
page { timezone } =
    Spa.Page.element
        { view = view timezone
        , init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        }


view : Time.Zone -> Model -> View Msg
view zone model =
    { title =
        UI.Layout.pageTitle <|
            "Autorisation d'accès à Deveco pour un tiers - Deveco"
    , body = [ body zone model ]
    , route = Route.AyantDroit model.token
    }


init : String -> ( Model, Effect Shared.Msg Msg )
init token =
    ( { token = token
      , accord = False
      , equipeDemande = RD.Loading
      , confirmation = RD.NotAsked
      }
    , getEquipeDemande token
    )


update : Msg -> Model -> ( Model, Effect Shared.Msg Msg )
update msg model =
    case msg of
        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ToggleAccord bool ->
            ( { model | accord = bool }, Effect.none )

        ReceivedEquipeDemande resp ->
            ( { model | equipeDemande = resp }, Effect.none )

        Confirm ->
            ( { model | confirmation = RD.Loading }, confirmAutorisation model.token )

        ReceivedConfirmation (RD.Success resp) ->
            ( { model | confirmation = RD.NotAsked, equipeDemande = RD.Success resp }, Effect.none )

        ReceivedConfirmation resp ->
            ( { model | confirmation = resp }, Effect.none )


body : Time.Zone -> Model -> Html Msg
body zone model =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12, class "flex flex-col gap-4 fr-card--white" ]
            [ h1 [ Typo.fr_h3 ] [ text "Autorisation de délégation d'usage de Deveco" ]
            , case model.equipeDemande of
                RD.NotAsked ->
                    nothing

                RD.Loading ->
                    text "Chargement en cours"

                RD.Failure _ ->
                    text "Impossible de trouver la demande d'autorisation. Veuillez nous contacter si le problème persiste."

                RD.Success equipeDemande ->
                    case equipeDemande.autorisations of
                        [] ->
                            text "Une erreur s'est produite, veuillez recharger la page."

                        autorisation :: _ ->
                            viewEquipeDemande zone model.accord model.confirmation equipeDemande equipeDemande.compteDemande autorisation
            ]
        ]


viewEquipeDemande : Time.Zone -> Bool -> WebData EquipeDemande -> EquipeDemande -> Data.EquipeDemande.CompteDemande -> Data.EquipeDemande.Autorisation -> Html Msg
viewEquipeDemande zone accord confirmation equipeDemande compteDemande autorisation =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12 ] [ h2 [ Typo.fr_h4 ] [ text "Récapitulatif de la demande " ] ]
        , div [ DSFR.Grid.col6 ]
            [ h3 [ Typo.fr_h6 ] [ text "Vos informations" ]
            , text <| "Nom\u{00A0}: "
            , span [ Typo.textBold ] [ text autorisation.ayantDroit.nom ]
            , br []
            , text <| "Prénom\u{00A0}: "
            , span [ Typo.textBold ] [ text autorisation.ayantDroit.prenom ]
            , br []
            , text <| "Email\u{00A0}: "
            , span [ Typo.textBold ] [ text autorisation.ayantDroit.email ]
            , br []
            , text <| "Fonction\u{00A0}: "
            , span [ Typo.textBold ] [ text autorisation.ayantDroit.fonction ]
            , br []
            , text <| "Collectivité\u{00A0}: "
            , span [ Typo.textBold ] [ text autorisation.territoireNom ]
            ]
        , div [ DSFR.Grid.col6 ]
            [ h3 [ Typo.fr_h6 ] [ text "La personne faisant la demande" ]
            , text <| "Nom\u{00A0}: "
            , span [ Typo.textBold ] [ text compteDemande.nom ]
            , br []
            , text <| "Prénom\u{00A0}: "
            , span [ Typo.textBold ] [ text compteDemande.prenom ]
            , br []
            , text <| "Email\u{00A0}: "
            , span [ Typo.textBold ] [ text compteDemande.email ]
            , br []
            , text <| "Fonction\u{00A0}: "
            , span [ Typo.textBold ] [ text compteDemande.fonction ]
            , br []
            , text <| "Structure\u{00A0}: "
            , span [ Typo.textBold ] [ text equipeDemande.nom ]
            ]
        , div [ DSFR.Grid.col12 ] <|
            case autorisation.acceptedAt of
                Nothing ->
                    [ p []
                        [ text <| "Je soussigné(e) "
                        , span [ Typo.textBold ] [ text autorisation.ayantDroit.nom, text " ", text autorisation.ayantDroit.prenom ]
                        , text ", représentant(e) de "
                        , span [ Typo.textBold ] [ text autorisation.territoireNom ]
                        , text ", "
                        , text "autorise la création d’un compte sur la plateforme Deveco à tout personnel de "
                        , span [ Typo.textBold ] [ text equipeDemande.nom ]
                        , text "."
                        ]
                    , p []
                        [ text <| "Je déclare avoir confié à "
                        , span [ Typo.textBold ] [ text equipeDemande.nom ]
                        , text " une mission de développement économique sur le territoire de ma collectivité et l’autorise à utiliser la plateforme Deveco pour le compte de ma collectivité."
                        ]
                    , p []
                        [ text "Cette autorisation est accordée sous ma responsabilité et en aucune circonstance la plateforme Deveco ne peut être tenue responsable des éventuels préjudices qui peuvent en résulter."
                        ]
                    , p []
                        [ text "Pour le compte de ma collectivité et par délégation,"
                        ]
                    , p []
                        [ span [ Typo.textBold ] [ text autorisation.ayantDroit.nom ]
                        , text " "
                        , span [ Typo.textBold ] [ text autorisation.ayantDroit.prenom ]
                        ]
                    , div [ class "flex flex-col gap-4" ]
                        [ DSFR.Checkbox.single
                            { value = "autorisation-delegation"
                            , checked = Just accord
                            , valueAsString = identity
                            , id = "autorisation-delegation"
                            , label = text "J'accepte de donner l'accès aux données de mon territoire dans l'outil Deveco"
                            , onChecked = \_ bool -> ToggleAccord bool
                            }
                            |> DSFR.Checkbox.viewSingle
                        , DSFR.Button.new { label = "Confirmer", onClick = Just Confirm }
                            |> DSFR.Button.withDisabled (not accord || confirmation == RD.Loading)
                            |> DSFR.Button.view
                        ]
                    ]

                Just date ->
                    [ p []
                        [ text "Vous avez accepté cette demande le "
                        , span [ Typo.textBold ] [ text <| Lib.Date.dateTimeToFrenchString zone date ]
                        , text "."
                        ]
                    ]
        ]


getEquipeDemande : String -> Effect Shared.Msg Msg
getEquipeDemande token =
    Effect.fromCmd <|
        get
            { url = Api.getEquipeDemandeAutorisation token }
            { toShared = SharedMsg
            , logger = Nothing
            , handler = ReceivedEquipeDemande
            }
            decodeEquipeDemande


confirmAutorisation : String -> Effect Shared.Msg Msg
confirmAutorisation token =
    Effect.fromCmd <|
        post
            { url = Api.getEquipeDemandeAutorisation token
            , body = Http.emptyBody
            }
            { toShared = SharedMsg
            , logger = Nothing
            , handler = ReceivedConfirmation
            }
            decodeEquipeDemande
