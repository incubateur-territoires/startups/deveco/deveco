module Pages.Etablissements exposing (Model, getTDBStats, page)

import Accessibility exposing (Html, br, div, h1, span, sup, text)
import Api
import Api.Auth exposing (get, post, request)
import DSFR.Alert
import DSFR.Button
import DSFR.Grid as Grid
import DSFR.Icons
import DSFR.Icons.Design
import DSFR.Icons.System
import DSFR.Modal
import DSFR.Pagination
import DSFR.Range
import DSFR.SegmentedControl
import DSFR.Tag
import DSFR.Tooltip
import DSFR.Typography as Typo
import Data.Adresse exposing (ApiAdresse, decodeApiFeatures)
import Data.Etablissement exposing (EtablissementSimpleAvecPortefeuille, Siret, decodeEtablissementSimpleAvecPortefeuille)
import Data.Etiquette exposing (decodeEtiquetteList)
import Data.Role
import Date
import Effect
import Html exposing (label, p)
import Html.Attributes as Attr exposing (class, for)
import Html.Attributes.Extra exposing (empty)
import Html.Events as Events
import Html.Extra exposing (nothing, viewIf, viewMaybe)
import Html.Keyed as Keyed
import Html.Lazy
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import Lib.UI exposing (formatIntWithThousandSpacing, loggingDecoder)
import Lib.Umap
import Lib.Variables exposing (hintActivites, hintMotsCles, hintResultatsEtablissements, hintZoneGeographique)
import MultiSelectRemote
import Pages.Etablissements.Filters as Filters exposing (Filters, FiltresRequete, SituationGeographique(..), countActivePersoFilters, decodeFiltresRequete, defaultFiltresRequete, emptyFilters)
import Pages.Etablissements.Stats exposing (TDBStats, decodeTDBStats, emptyTDBStats, viewStats)
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import SingleSelectRemote
import Spa.Page exposing (Page)
import Time
import UI.Entite
import UI.Etablissement
import UI.Layout
import UI.Pagination exposing (Pagination, defaultPagination)
import Url.Builder
import User
import View exposing (View)


page : Shared.Shared -> User -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page { appUrl, now, timezone } user =
    Spa.Page.onNewFlags (Filters.queryToFiltersAndPagination >> DoSearch) <|
        Spa.Page.element <|
            { init = init now timezone
            , update = update
            , view = view user appUrl
            , subscriptions = subscriptions
            }


type alias Model =
    { etablissements : WebData (List EtablissementSimpleAvecPortefeuille)
    , tdbStats : WebData TDBStats
    , cartoFeatures : WebData CartoFeatures
    , mapInfo : Maybe MapInfo
    , etablissementsRequests : Int
    , batchRequest : Maybe BatchRequest
    , geoJsonExport : Maybe GeoJsonFormat
    , excelExport : Maybe ()
    , excelExportRequested : Bool
    , filters : Filters.Model
    , rechercheSauvegardeeRequest : WebData String
    , viewMode : ViewMode
    , selectAdresse : SingleSelectRemote.SmartSelect Msg ApiAdresse
    , anneeCourante : Int
    }


type alias MapInfo =
    { center : ( Float, Float )
    , south : Float
    , west : Float
    , north : Float
    , east : Float
    , zoom : Float
    }


type ViewMode
    = Liste
    | Stats
    | Carte


viewModes : List ViewMode
viewModes =
    [ Liste
    , Stats
    , Carte
    ]


viewModeToId : ViewMode -> String
viewModeToId viewMode =
    case viewMode of
        Liste ->
            "liste"

        Stats ->
            "stats"

        Carte ->
            "carte"


viewModeToDisplay : ViewMode -> String
viewModeToDisplay viewMode =
    case viewMode of
        Liste ->
            "Liste"

        Stats ->
            "Statistiques"

        Carte ->
            "Carte"


type GeoJsonFormat
    = Regular


geoJsonFormatToString : GeoJsonFormat -> String
geoJsonFormatToString geoJsonFormat =
    case geoJsonFormat of
        Regular ->
            "geojson"


type alias BatchRequest =
    { request : WebData ( List EtablissementSimpleAvecPortefeuille, Pagination, FiltresRequete )
    , selectActivites : MultiSelectRemote.SmartSelect Msg String
    , selectLocalisations : MultiSelectRemote.SmartSelect Msg String
    , selectMots : MultiSelectRemote.SmartSelect Msg String
    , activites : List String
    , localisations : List String
    , mots : List String
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | DoSearch ( Filters, Pagination )
    | ReceivedEtablissements (WebData ( List EtablissementSimpleAvecPortefeuille, Pagination, FiltresRequete ))
    | ReceivedTDBStats (WebData TDBStats)
    | ReceivedCartoFeatures (WebData CartoFeatures)
    | FiltersMsg Filters.Msg
    | ToggleFavorite Siret Bool
    | GotFavoriteResult Siret Bool
    | ClickedQualifierFiches
    | CanceledQualifierFiches
    | ConfirmedQualifierFiches
    | ReceivedQualifierFiches (WebData ( List EtablissementSimpleAvecPortefeuille, Pagination, FiltresRequete ))
    | SelectedBatchActivites ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectBatchActivites (MultiSelectRemote.Msg String)
    | UnselectBatchActivite String
    | SelectedBatchLocalisations ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectBatchLocalisations (MultiSelectRemote.Msg String)
    | UnselectBatchLocalisation String
    | SelectedBatchMots ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectBatchMots (MultiSelectRemote.Msg String)
    | UnselectBatchMot String
    | ClickedExportExcel
    | ClickedCancelExportExcel
    | ClickedConfirmExportExcel
    | ClickedExportGeoJson
    | ClickedCanceledGeoJson
    | RequestedRechercheSauvegardee
    | ReceivedRechercheSauvegardee (WebData String)
    | ChangeViewMode ViewMode
    | UpdatedMapInfo MapInfo
    | SelectedAdresse ( ApiAdresse, SingleSelectRemote.Msg ApiAdresse )
    | UpdatedSelectAdresse (SingleSelectRemote.Msg ApiAdresse)
    | AnneeChangee Int


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ model.batchRequest
            |> Maybe.map .selectActivites
            |> Maybe.map MultiSelectRemote.subscriptions
            |> Maybe.withDefault Sub.none
        , model.batchRequest
            |> Maybe.map .selectLocalisations
            |> Maybe.map MultiSelectRemote.subscriptions
            |> Maybe.withDefault Sub.none
        , model.batchRequest
            |> Maybe.map .selectMots
            |> Maybe.map MultiSelectRemote.subscriptions
            |> Maybe.withDefault Sub.none
        , model.filters
            |> Filters.subscriptions
            |> Sub.map FiltersMsg
        , model.selectAdresse |> SingleSelectRemote.subscriptions
        ]


init : Time.Posix -> Time.Zone -> Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init now timezone rawQuery =
    let
        etablissementsRequests =
            0

        ( initFilters, effectFilters ) =
            Filters.init defaultFiltresRequete rawQuery

        selectAdresse =
            SingleSelectRemote.init "champ-selection-adresse-maplibre"
                { selectionMsg = SelectedAdresse
                , internalMsg = UpdatedSelectAdresse
                , characterSearchThreshold = 3
                , debounceDuration = selectDebounceDuration
                }

        anneeCourante =
            Date.fromPosix timezone now |> Date.year
    in
    ( { etablissements = RD.Loading
      , tdbStats = RD.NotAsked
      , cartoFeatures = RD.NotAsked
      , mapInfo = Nothing
      , etablissementsRequests = etablissementsRequests
      , filters = initFilters
      , batchRequest = Nothing
      , geoJsonExport = Nothing
      , excelExport = Nothing
      , excelExportRequested = False
      , rechercheSauvegardeeRequest = RD.NotAsked
      , viewMode = Liste
      , selectAdresse = selectAdresse
      , anneeCourante = anneeCourante
      }
    , Effect.map FiltersMsg effectFilters
    )
        |> Shared.pageChangeEffects


getEtablissementsTracker : Int -> String
getEtablissementsTracker count =
    "get-etablissements-tracker" ++ "-" ++ String.fromInt count


getEtablissementsStatsTracker : Int -> String
getEtablissementsStatsTracker count =
    "get-etablissements-stats-tracker" ++ "-" ++ String.fromInt count


getEtablissementsCartoTracker : Int -> String
getEtablissementsCartoTracker count =
    "get-etablissements-carto-tracker" ++ "-" ++ String.fromInt count


getEtablissements : Int -> String -> Cmd Msg
getEtablissements trackerCount nextUrl =
    request
        { method = "GET"
        , headers = []
        , url = Api.getEtablissements <| nextUrl
        , body = Http.emptyBody
        , timeout = Nothing
        , tracker = Just <| getEtablissementsTracker trackerCount
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedEtablissements
        }
        decodeBatchResponse


getTDBStats :
    { sharedMsg : Shared.Msg -> msg
    , logError : Maybe (msg -> Shared.ErrorReport -> msg)
    , msg : WebData TDBStats -> msg
    }
    -> Int
    -> String
    -> Cmd msg
getTDBStats { sharedMsg, logError, msg } trackerCount nextUrl =
    request
        { method = "GET"
        , headers = []
        , url = Api.getEtablissements <| nextUrl ++ "&affichage=stats"
        , body = Http.emptyBody
        , timeout = Nothing
        , tracker = Just <| getEtablissementsStatsTracker trackerCount
        }
        { toShared = sharedMsg
        , logger = logError
        , handler = msg
        }
        (Decode.oneOf
            [ Decode.field "stats" decodeTDBStats
            , Decode.field "error" Decode.string
                |> Decode.andThen
                    (\s ->
                        if s == "Impossible d'exporter avec l'option \"Toute la France\"" then
                            Decode.succeed emptyTDBStats

                        else
                            Decode.fail <| "Erreur inconnue : " ++ s
                    )
            ]
        )


getCartoFeatures : Int -> Int -> String -> Maybe MapInfo -> Cmd Msg
getCartoFeatures anneeCourante trackerCount nextUrl mapInfo =
    let
        zoom =
            mapInfo
                |> Maybe.map (\m -> "&z=" ++ String.fromFloat m.zoom)
                |> Maybe.withDefault ""

        bounds =
            mapInfo
                |> Maybe.map
                    (\{ north, west, south, east } ->
                        "&nwse="
                            ++ ([ north, west, south, east ]
                                    |> List.map String.fromFloat
                                    |> String.join ","
                               )
                    )
                |> Maybe.withDefault ""
    in
    request
        { method = "GET"
        , headers = []
        , url = Api.getEtablissements <| nextUrl ++ "&affichage=geo" ++ zoom ++ bounds
        , body = Http.emptyBody
        , timeout = Nothing
        , tracker = Just <| getEtablissementsCartoTracker trackerCount
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedCartoFeatures
        }
        (decodeCartoFeatures anneeCourante trackerCount)


type alias CartoFeatures =
    { total : Int
    , features : String
    , anneeSelectionnee : Int
    , territoire : Maybe Contour
    , qpvs : List Contour
    , zonages : List Contour
    , requete : Int
    , adresse : Maybe ApiAdresse
    }


type alias Contour =
    { id : String
    , nom : String
    , geometrie : String
    , boundingBox : String
    }


decodeCartoFeatures : Int -> Int -> Decoder CartoFeatures
decodeCartoFeatures anneeCourante tracker =
    Decode.succeed CartoFeatures
        |> andMap (Decode.field "total" Decode.int)
        |> andMap (Decode.field "features" (Decode.nullable Decode.string) |> Decode.map (Maybe.withDefault ""))
        |> andMap (Decode.succeed anneeCourante)
        |> andMap (Decode.field "territoire" <| Decode.nullable decodeContour)
        |> andMap (Decode.field "qpvs" <| Decode.list decodeContour)
        |> andMap (Decode.field "zonages" <| Decode.list decodeContour)
        |> andMap (Decode.succeed tracker)
        |> andMap (Decode.succeed Nothing)


decodeContour : Decoder Contour
decodeContour =
    Decode.succeed Contour
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "geometrie" Decode.string)
        |> andMap (Decode.field "box" Decode.string)


sendBatchRequest : Filters -> Pagination -> BatchRequest -> Cmd Msg
sendBatchRequest filters pagination { activites, localisations, mots } =
    let
        jsonBody =
            [ ( "activites", Encode.list Encode.string activites )
            , ( "localisations", Encode.list Encode.string localisations )
            , ( "motsCles", Encode.list Encode.string mots )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    post
        { url =
            Api.updateEtablissementsEtiquettes <|
                Filters.filtersAndPaginationToQuery ( filters, pagination )
        , body = jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedQualifierFiches
        }
        decodeBatchResponse


decodeBatchResponse : Decoder ( List EtablissementSimpleAvecPortefeuille, Pagination, FiltresRequete )
decodeBatchResponse =
    Decode.succeed (\p pages results data filters -> ( data, Pagination p pages results, filters ))
        |> andMap (Decode.field "page" Decode.int)
        |> andMap (Decode.field "pages" Decode.int)
        |> andMap (Decode.field "total" Decode.int)
        |> andMap (Decode.field "elements" <| Decode.list decodeEtablissementSimpleAvecPortefeuille)
        |> andMap (Decode.field "filtres" decodeFiltresRequete)


requestToggleFavorite : String -> Bool -> Cmd Msg
requestToggleFavorite siret favorite =
    post
        { url = Api.toggleFavoriteEtablissement siret
        , body =
            [ ( "favori", Encode.bool favorite )
            ]
                |> Encode.object
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler =
            \webdata ->
                GotFavoriteResult siret <|
                    case webdata of
                        RD.Success _ ->
                            favorite

                        _ ->
                            not favorite
        }
        (Decode.succeed ())


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Effect.none )

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        FiltersMsg filtersMsg ->
            let
                ( filters, effectFilters ) =
                    Filters.update filtersMsg model.filters
            in
            { model | filters = filters }
                |> Effect.with (Effect.map FiltersMsg effectFilters)

        ReceivedEtablissements etablissements ->
            case etablissements of
                RD.Success ( fs, pagination, filtresRequete ) ->
                    let
                        newFilters =
                            model.filters
                                |> Filters.setFiltersRequete filtresRequete
                                |> Filters.forcePagination pagination
                    in
                    ( { model
                        | etablissements = RD.Success fs
                        , tdbStats = RD.NotAsked
                        , filters = newFilters
                      }
                    , if model.viewMode == Carte then
                        Effect.batch
                            [ Effect.fromCmd <|
                                getCartoFeatures
                                    model.anneeCourante
                                    model.etablissementsRequests
                                    newFilters.currentUrl
                                    Nothing
                            , Effect.fromCmd <|
                                Http.cancel <|
                                    getEtablissementsCartoTracker (model.etablissementsRequests - 1)
                            , Effect.fromShared <| Shared.ScrollIntoView "carto"
                            ]

                      else
                        Effect.none
                    )

                _ ->
                    ( { model
                        | etablissements = etablissements |> RD.map (\( e, _, _ ) -> e)
                      }
                    , Effect.none
                    )

        ReceivedTDBStats tdbStats ->
            ( { model
                | tdbStats = tdbStats
              }
            , Effect.none
            )

        ReceivedCartoFeatures cartoFeatures ->
            ( { model
                | cartoFeatures = cartoFeatures
              }
            , Effect.none
            )

        DoSearch ( filters, pagination ) ->
            let
                ( filtersModel, effectFilters ) =
                    Filters.doSearch ( filters, pagination ) model.filters

                nextRequestNumber =
                    model.etablissementsRequests + 1
            in
            ( { model
                | filters = filtersModel
                , etablissements = RD.Loading
                , tdbStats = RD.NotAsked
                , viewMode =
                    if model.viewMode == Stats then
                        Liste

                    else
                        model.viewMode
                , etablissementsRequests = nextRequestNumber
                , excelExportRequested = False
                , rechercheSauvegardeeRequest = RD.NotAsked
              }
                |> resetModals
            , Effect.batch
                [ Effect.fromCmd (getEtablissements nextRequestNumber <| Filters.filtersAndPaginationToQuery ( Filters.getFilters filtersModel, Filters.getPagination filtersModel ))
                , Effect.fromCmd (Http.cancel <| getEtablissementsTracker model.etablissementsRequests)
                , Effect.fromShared <| Shared.ScrollIntoView Filters.listFiltersTagsId
                , Effect.map FiltersMsg effectFilters
                ]
            )

        ToggleFavorite siret add ->
            { model
                | etablissements =
                    model.etablissements
                        |> RD.map (List.map (updateEtablissementInList siret add))
            }
                |> Effect.withCmd (requestToggleFavorite siret add)

        GotFavoriteResult siret add ->
            { model
                | etablissements =
                    model.etablissements
                        |> RD.map (List.map (updateEtablissementInList siret add))
            }
                |> Effect.withNone

        ClickedQualifierFiches ->
            { model
                | batchRequest = initBatchRequest
            }
                |> Effect.withNone

        CanceledQualifierFiches ->
            { model | batchRequest = Nothing }
                |> Effect.withNone

        ConfirmedQualifierFiches ->
            case model.batchRequest of
                Nothing ->
                    model
                        |> Effect.withNone

                Just br ->
                    { model
                        | batchRequest =
                            Just { br | request = RD.Loading }
                    }
                        |> Effect.withCmd (sendBatchRequest (Filters.getFilters model.filters) (Filters.getPagination model.filters) br)

        ReceivedQualifierFiches response ->
            let
                { etablissements, filters } =
                    case response of
                        RD.Success ( e, p, filtresRequete ) ->
                            { etablissements = RD.Success e
                            , filters =
                                model.filters
                                    |> Filters.setFiltersRequete filtresRequete
                                    |> Filters.forcePagination p
                            }

                        _ ->
                            { etablissements = model.etablissements
                            , filters = model.filters
                            }
            in
            { model
                | batchRequest = model.batchRequest |> Maybe.map (\br -> { br | request = response })
                , filters = filters
                , etablissements = etablissements
            }
                |> Effect.withNone

        SelectedBatchActivites ( activites, sMsg ) ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectActivitesConfig br.selectActivites

                        activitesUniques =
                            case activites of
                                [] ->
                                    []

                                a :: rest ->
                                    if List.member a rest then
                                        rest

                                    else
                                        activites
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br | selectActivites = updatedSelect, activites = activitesUniques }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UpdatedSelectBatchActivites sMsg ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectActivitesConfig br.selectActivites
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br | selectActivites = updatedSelect }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UnselectBatchActivite activite ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | activites =
                                        br.activites
                                            |> List.filter ((/=) activite)
                                }
                      }
                    , Effect.none
                    )

        SelectedBatchLocalisations ( localisations, sMsg ) ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectLocalisationsConfig br.selectLocalisations

                        localisationsUniques =
                            case localisations of
                                [] ->
                                    []

                                a :: rest ->
                                    if List.member a rest then
                                        rest

                                    else
                                        localisations
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | localisations = localisationsUniques
                                    , selectLocalisations = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UpdatedSelectBatchLocalisations sMsg ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectLocalisationsConfig br.selectLocalisations
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | selectLocalisations = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UnselectBatchLocalisation localisation ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | localisations =
                                        br.localisations
                                            |> List.filter ((/=) localisation)
                                }
                      }
                    , Effect.none
                    )

        SelectedBatchMots ( mots, sMsg ) ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectMotsConfig br.selectMots

                        motsUniques =
                            case mots of
                                [] ->
                                    []

                                a :: rest ->
                                    if List.member a rest then
                                        rest

                                    else
                                        mots
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | mots = motsUniques
                                    , selectMots = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UpdatedSelectBatchMots sMsg ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectMotsConfig br.selectMots
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | selectMots = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UnselectBatchMot mot ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | mots =
                                        br.mots
                                            |> List.filter ((/=) mot)
                                }
                      }
                    , Effect.none
                    )

        ClickedExportExcel ->
            { model | excelExport = Just () }
                |> Effect.withNone

        ClickedCancelExportExcel ->
            { model | excelExport = Nothing }
                |> Effect.withNone

        ClickedConfirmExportExcel ->
            { model
                | excelExport = Nothing
                , excelExportRequested = True
            }
                |> Effect.withNone

        ClickedExportGeoJson ->
            { model | geoJsonExport = Just Regular }
                |> Effect.withNone

        ClickedCanceledGeoJson ->
            { model | geoJsonExport = Nothing }
                |> Effect.withNone

        RequestedRechercheSauvegardee ->
            { model | rechercheSauvegardeeRequest = RD.Loading }
                |> Effect.withCmd (getRechercheSauvegardeeRequest (Filters.getCurrentUrl model.filters))

        ReceivedRechercheSauvegardee response ->
            case model.rechercheSauvegardeeRequest of
                RD.Loading ->
                    { model | rechercheSauvegardeeRequest = response }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        ChangeViewMode viewMode ->
            let
                currentEtablissementRequestNumber =
                    model.etablissementsRequests + 1

                previousEtablissementRequestNumber =
                    model.etablissementsRequests

                ( tdbStats, cartoFeatures, effect ) =
                    case ( viewMode, model.tdbStats, model.cartoFeatures ) of
                        ( Stats, RD.NotAsked, _ ) ->
                            ( RD.Loading
                            , model.cartoFeatures
                            , Effect.batch
                                [ Effect.fromCmd <|
                                    getTDBStats
                                        { sharedMsg = SharedMsg
                                        , logError = Just LogError
                                        , msg = ReceivedTDBStats
                                        }
                                        currentEtablissementRequestNumber
                                        model.filters.currentUrl
                                , Effect.fromCmd <|
                                    Http.cancel <|
                                        getEtablissementsStatsTracker previousEtablissementRequestNumber
                                ]
                            )

                        ( Carte, _, RD.NotAsked ) ->
                            ( model.tdbStats
                            , RD.Loading
                            , Effect.batch
                                [ Effect.fromCmd <|
                                    getCartoFeatures
                                        model.anneeCourante
                                        currentEtablissementRequestNumber
                                        model.filters.currentUrl
                                        Nothing
                                , Effect.fromCmd <|
                                    Http.cancel <|
                                        getEtablissementsCartoTracker previousEtablissementRequestNumber
                                , Effect.fromShared <| Shared.ScrollIntoView "carto"
                                ]
                            )

                        _ ->
                            ( model.tdbStats
                            , model.cartoFeatures
                            , Effect.none
                            )
            in
            { model
                | viewMode = viewMode
                , tdbStats = tdbStats
                , cartoFeatures = cartoFeatures
            }
                |> Effect.with effect

        UpdatedMapInfo mapInfo ->
            let
                currentEtablissementRequestNumber =
                    model.etablissementsRequests + 1

                previousEtablissementRequestNumber =
                    model.etablissementsRequests
            in
            ( { model
                | mapInfo = Just mapInfo
                , etablissementsRequests = currentEtablissementRequestNumber
              }
            , Effect.batch
                [ Effect.fromCmd
                    (getCartoFeatures
                        model.anneeCourante
                        currentEtablissementRequestNumber
                        model.filters.currentUrl
                        (Just mapInfo)
                    )
                , Effect.fromCmd (Http.cancel <| getEtablissementsCartoTracker previousEtablissementRequestNumber)
                ]
            )

        SelectedAdresse ( apiAdresse, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectAdresseConfig model.selectAdresse

                ( emptySelect, emptySelectCmd ) =
                    SingleSelectRemote.setText "" selectAdresseConfig updatedSelect

                newCartoFeatures =
                    model.cartoFeatures
                        |> RD.map (\cartoFeatures -> { cartoFeatures | adresse = Just apiAdresse })
            in
            ( { model
                | cartoFeatures = newCartoFeatures
                , selectAdresse = emptySelect
              }
            , Effect.batch [ Effect.fromCmd selectCmd, Effect.fromCmd emptySelectCmd ]
            )

        UpdatedSelectAdresse sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectAdresseConfig model.selectAdresse
            in
            ( { model
                | selectAdresse = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        AnneeChangee annee ->
            { model
                | cartoFeatures =
                    model.cartoFeatures
                        |> RD.map
                            (\cf -> { cf | anneeSelectionnee = annee })
            }
                |> Effect.withNone


getRechercheSauvegardeeRequest : String -> Cmd Msg
getRechercheSauvegardeeRequest nextUrl =
    get
        { url = Api.getEtablissementsRechercheSauvegardeeLien <| nextUrl
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedRechercheSauvegardee
        }
        (Decode.field "url" Decode.string)


initBatchRequest : Maybe BatchRequest
initBatchRequest =
    Just
        { request = RD.NotAsked
        , selectActivites = initSelectActivites
        , selectLocalisations = initSelectLocalisations
        , selectMots = initSelectMots
        , activites = []
        , localisations = []
        , mots = []
        }


updateEtablissementInList : String -> Bool -> EtablissementSimpleAvecPortefeuille -> EtablissementSimpleAvecPortefeuille
updateEtablissementInList siret add (( etablissement, portefeuilleInfos ) as etablissementSimpleAvecPortefeuille) =
    if etablissement.siret == siret then
        ( etablissement
        , { portefeuilleInfos | favori = add }
        )

    else
        etablissementSimpleAvecPortefeuille


view : User -> String -> Model -> View Msg
view user appUrl model =
    { title = UI.Layout.pageTitle <| "Établissements"
    , body = UI.Layout.lazyBody (body user appUrl) model
    , route =
        Route.Etablissements <|
            (\q ->
                if q == "" then
                    Nothing

                else
                    Just q
            )
            <|
                Filters.filtersAndPaginationToQuery ( Filters.getFilters model.filters, Filters.getPagination model.filters )
    }


body : User -> String -> Model -> Html Msg
body user appUrl model =
    let
        results =
            model.filters |> Filters.getPagination |> .results
    in
    div [ class "flex flex-col gap-4 py-4" ]
        [ viewBatchRequestModal results model.batchRequest
        , viewGeoJsonModal appUrl (Filters.getCurrentUrl model.filters) results model.rechercheSauvegardeeRequest model.geoJsonExport
        , viewExcelModal (Filters.getCurrentUrl model.filters) model.excelExport
        , Html.map FiltersMsg <| Filters.viewSaveRechercheEnregistree appUrl model.filters
        , Html.map FiltersMsg <| Filters.viewDeleteRechercheEnregistree model.filters
        , h1 [ class "fr-h6 !mb-0" ]
            [ DSFR.Icons.iconLG UI.Entite.iconeEtablissement
            , text "\u{00A0}"
            , text "Établissements"
            ]
        , Html.map FiltersMsg <| Filters.filterPanel user model.filters
        , Html.Lazy.lazy8 viewEtablissementsList user model.viewMode model.excelExportRequested (Filters.getPagination model.filters) (Filters.getFilters model.filters) model.etablissements model.tdbStats (Filters.getCurrentUrl model.filters)
        , Keyed.node "div"
            [ if model.viewMode /= Carte then
                class "hidden"

              else
                empty
            , class "w-full"
            ]
            [ ( "carto", Html.Lazy.lazy3 viewEtablissementsCarte model.anneeCourante model.selectAdresse model.cartoFeatures ) ]
        ]


viewExportBoutons : User -> ViewMode -> Bool -> WebData (List EtablissementSimpleAvecPortefeuille) -> Filters -> Pagination -> String -> Html Msg
viewExportBoutons user viewMode excelExportRequested request filters pagination currentUrl =
    let
        ( currentFilters, _ ) =
            currentUrl
                |> Just
                |> Filters.queryToFiltersAndPagination

        touteLaFrance =
            currentFilters.situationGeographique == Filters.TouteLaFrance

        filtresPerso =
            countActivePersoFilters currentFilters > 0

        tooltipPourTouteLaFrance id =
            if etablissementsTropNombreux && touteLaFrance && not filtresPerso then
                DSFR.Tooltip.survol { label = text "Impossible d'exporter pour l'option \"Toute la France\" sans ajouter des filtres personnalisés", id = "tooltip-export-" ++ id }
                    |> DSFR.Tooltip.wrap

            else
                identity

        nextUrl =
            Filters.filtersAndPaginationToQuery ( filters, pagination )

        filtresPasEncoreValides =
            nextUrl /= currentUrl

        etablissementsTropNombreux =
            results > etablissementsMax

        tooltipPourFiltresPasEncoreValides id =
            if filtresPasEncoreValides then
                DSFR.Tooltip.survol { label = text "Vous avez changé des filtres sans lancer votre recherche", id = "tooltip-filtres-pas-valides-exports-" ++ id }
                    |> DSFR.Tooltip.wrap

            else
                identity

        results =
            pagination.results
    in
    div [ class "flex flex-col gap-4 mb-4" ]
        [ div [ class "flex flex-col gap-4 lg:flex-row lg:items-center" ]
            [ div [ class "font-bold" ] <|
                case request of
                    RD.Loading ->
                        [ greyPlaceholder 20 ]

                    _ ->
                        let
                            hint =
                                sup [] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ]

                            tooltip =
                                hint
                                    |> (DSFR.Tooltip.survol
                                            { label = text hintResultatsEtablissements
                                            , id = "resultats-limites"
                                            }
                                            |> DSFR.Tooltip.wrap
                                       )
                        in
                        [ text (formatIntWithThousandSpacing results)
                        , text " résultat"
                        , viewIf (results > 1) (text "s")
                        , viewIf (results > 100) tooltip
                        ]
            , div [ class "flex flex-col gap-2 items-start lg:flex-row lg:ml-auto" ]
                [ DSFR.Button.new
                    { label = "Qualifier"
                    , onClick = Just ClickedQualifierFiches
                    }
                    |> DSFR.Button.secondary
                    |> DSFR.Button.leftIcon DSFR.Icons.Design.editLine
                    |> DSFR.Button.withDisabled (results == 0 || filtresPasEncoreValides)
                    |> DSFR.Button.view
                    |> tooltipPourFiltresPasEncoreValides "qualifier"
                , DSFR.Button.new { onClick = Just ClickedExportExcel, label = "Exporter (Excel)" }
                    |> DSFR.Button.secondary
                    |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                    |> DSFR.Button.withDisabled (results == 0 || excelExportRequested || (etablissementsTropNombreux && touteLaFrance && not filtresPerso) || filtresPasEncoreValides)
                    |> DSFR.Button.view
                    |> tooltipPourTouteLaFrance "excel"
                    |> tooltipPourFiltresPasEncoreValides "export-excel"
                , DSFR.Button.new
                    { label = "Exporter (GeoJSON)"
                    , onClick = Just <| ClickedExportGeoJson
                    }
                    |> DSFR.Button.secondary
                    |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                    |> DSFR.Button.withDisabled (results == 0 || filtresPasEncoreValides)
                    |> DSFR.Button.view
                    |> tooltipPourFiltresPasEncoreValides "export-geojson"
                ]
            ]
        , div [ class "flex flex-row items-baseline gap-4 justify-between" ] <|
            case request of
                RD.Loading ->
                    [ greyPlaceholder 20 ]

                _ ->
                    let
                        selectLabel =
                            "select-sort"

                        hasCarto =
                            Data.Role.hasCarto <|
                                User.role <|
                                    user

                        seulementTouteLaFrance =
                            Filters.filtersAndPaginationToQuery ( currentFilters, defaultPagination )
                                == Filters.filtersAndPaginationToQuery
                                    ( { emptyFilters
                                        | situationGeographique = Filters.TouteLaFrance
                                      }
                                    , defaultPagination
                                    )
                    in
                    [ div [ class "fr-card--white" ]
                        [ DSFR.SegmentedControl.raw
                            { legend = "Choix de l'affichage"
                            , selected = viewMode
                            , toId = viewModeToId
                            , toDisplay = viewModeToDisplay
                            , toDisabled =
                                \option ->
                                    case option of
                                        Stats ->
                                            if seulementTouteLaFrance then
                                                Just "La consultation des statistiques n'est pas possible avec uniquement le filtre \"Toute la France\""

                                            else
                                                Nothing

                                        Carte ->
                                            if not hasCarto then
                                                Just "Bientôt disponible"

                                            else
                                                Nothing

                                        _ ->
                                            Nothing
                            , options = viewModes
                            , selectMsg = ChangeViewMode
                            }
                        ]
                    , div [ class "flex flex-row items-baseline gap-4" ]
                        [ label [ class "fr-label", for selectLabel ] [ text "Tri" ]
                        , div
                            [ class "fr-select-group"
                            ]
                            [ Html.map FiltersMsg <| Filters.selectOrder selectLabel filters
                            ]
                        ]
                    ]
        ]


viewEtablissementsList : User -> ViewMode -> Bool -> Pagination -> Filters -> WebData (List EtablissementSimpleAvecPortefeuille) -> WebData TDBStats -> String -> Html Msg
viewEtablissementsList user viewMode excelExportRequested pagination filters etablissements stats currentUrl =
    case etablissements of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            case viewMode of
                Carte ->
                    nothing

                _ ->
                    div [ Grid.col12 ]
                        [ viewExportBoutons user viewMode excelExportRequested etablissements filters pagination currentUrl
                        , div [ Grid.gridRow ] <|
                            List.repeat 3 <|
                                cardPlaceholder
                        ]

        RD.Failure _ ->
            div [ class "text-center" ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Une erreur s'est produite, veuillez recharger la page."
                    ]
                ]

        RD.Success [] ->
            div [ class "fr-card--white h-[250px] text-center p-20" ]
                [ p []
                    [ text "Aucun établissement ne correspond à cette recherche."
                    , text " "
                    , viewMaybe (\_ -> text "Moteur de recherche par adresse en cours de perfectionnement\u{00A0}: certaines adresses peuvent ne pas être trouvées.") filters.apiStreet
                    ]
                , viewIf (filters.situationGeographique == Filters.SurMonTerritoire)
                    (p []
                        [ text "Rechercher sur "
                        , Typo.link
                            (Route.toUrl <|
                                Route.Etablissements <|
                                    Just
                                        (filters
                                            |> Filters.changeSituationGeographique TouteLaFrance
                                            |> (\f ->
                                                    Filters.filtersAndPaginationToQuery ( f, defaultPagination )
                                               )
                                        )
                            )
                            []
                            [ text "toute la France"
                            ]
                        ]
                    )
                ]

        RD.Success liste ->
            div []
                [ viewExportBoutons user viewMode excelExportRequested etablissements filters pagination currentUrl
                , case viewMode of
                    Liste ->
                        div []
                            [ liste
                                |> List.map (\( etablissement, portefeuilleInfos ) -> UI.Etablissement.carte { toggleFavorite = Just ToggleFavorite } etablissement portefeuilleInfos False)
                                |> div [ Grid.gridRow ]
                            , if pagination.pages > 1 then
                                div [ class "!mt-4" ]
                                    [ DSFR.Pagination.view pagination.page (min 500 pagination.pages) <|
                                        Filters.toHref ( filters, pagination )
                                    ]

                              else
                                nothing
                            ]

                    Stats ->
                        viewStats False stats

                    Carte ->
                        nothing
                ]


viewEtablissementsCarte : Int -> SingleSelectRemote.SmartSelect Msg ApiAdresse -> WebData CartoFeatures -> Html Msg
viewEtablissementsCarte anneeCourante selectAdresse cartoFeatures =
    let
        total =
            cartoFeatures
                |> RD.toMaybe
                |> Maybe.map (.total >> Encode.int)
                |> Maybe.withDefault Encode.null

        features =
            cartoFeatures
                |> RD.toMaybe
                |> Maybe.map (.features >> Encode.string)
                |> Maybe.withDefault Encode.null

        encodeContour { id, nom, boundingBox, geometrie } =
            [ ( "boundingBox", boundingBox |> Encode.string )
            , ( "geometrie", geometrie |> Encode.string )
            , ( "nom", nom |> Encode.string )
            , ( "id", id |> Encode.string )
            ]
                |> Encode.object

        territoireData =
            Maybe.withDefault Encode.null <|
                Maybe.map encodeContour <|
                    Maybe.andThen .territoire <|
                        RD.toMaybe <|
                            cartoFeatures

        qpvsData =
            RD.withDefault Encode.null <|
                RD.map (Encode.list encodeContour) <|
                    RD.map .qpvs <|
                        cartoFeatures

        zonages =
            RD.withDefault Encode.null <|
                RD.map (Encode.list encodeContour) <|
                    RD.map .zonages <|
                        cartoFeatures

        requete =
            RD.withDefault Encode.null <|
                RD.map Encode.int <|
                    RD.map .requete <|
                        cartoFeatures

        adresse =
            Maybe.withDefault Encode.null <|
                Maybe.map
                    (\{ coordinates, type_ } ->
                        case coordinates of
                            Just ( longitude, latitude ) ->
                                Encode.object
                                    [ ( "center"
                                      , Encode.object
                                            [ ( "lon", Encode.float longitude )
                                            , ( "lat", Encode.float latitude )
                                            ]
                                      )
                                    , ( "type", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| type_ )
                                    ]

                            Nothing ->
                                Encode.null
                    )
                <|
                    Maybe.andThen .adresse <|
                        RD.toMaybe <|
                            cartoFeatures

        data =
            [ ( "features", features )
            , ( "total", total )
            , ( "territoire", territoireData )
            , ( "qpvs", qpvsData )
            , ( "zonages", zonages )
            , ( "requete", requete )
            ]

        annee =
            cartoFeatures
                |> RD.toMaybe
                |> Maybe.map (.anneeSelectionnee >> Encode.int)
                |> Maybe.withDefault Encode.null
    in
    div [ class "w-full h-[50rem]" ]
        [ adresseFilter selectAdresse
        , Html.node "maplibre-etablissements"
            [ data
                |> Encode.object
                |> Encode.encode 0
                |> Attr.attribute "data"
            , [ ( "requete", requete )
              ]
                |> Encode.object
                |> Encode.encode 0
                |> Attr.attribute "requete"
            , annee
                |> Encode.encode 0
                |> Attr.attribute "annee"
            , adresse
                |> Encode.encode 0
                |> Attr.attribute "adresse"
            , Events.on "updatedmap" <| loggingDecoder decodeMapInfo
            , Attr.id "carto"
            , class "block h-[700px]"
            ]
            []
        , div [ class "block mt-[2rem] h-[6rem] w-full" ] <|
            List.singleton <|
                (DSFR.Range.new
                    { id = "carte-annee"
                    , label = text "Évolution sur 10 ans"
                    , min = anneeCourante - 10 |> toFloat
                    , max = anneeCourante |> toFloat
                    , step = 1
                    }
                    |> DSFR.Range.simple
                        { value =
                            cartoFeatures
                                |> RD.map .anneeSelectionnee
                                |> RD.withDefault anneeCourante
                                |> toFloat
                        , onChange = ceiling >> AnneeChangee
                        }
                    |> DSFR.Range.withShowSteps
                    |> DSFR.Range.view
                )
        ]


viewBatchRequestModal : Int -> Maybe BatchRequest -> Html Msg
viewBatchRequestModal foundFiches batchRequest =
    DSFR.Modal.view
        { id = "batch-tag"
        , label = "batch-tag"
        , openMsg = NoOp
        , closeMsg = Just <| CanceledQualifierFiches
        , title =
            text <|
                "Qualifier "
                    ++ formatIntWithThousandSpacing foundFiches
                    ++ " fiche"
                    ++ (if foundFiches > 1 then
                            "s"

                        else
                            ""
                       )
        , opened = batchRequest /= Nothing
        , size = Nothing
        }
        (case batchRequest of
            Nothing ->
                nothing

            Just { request, selectLocalisations, selectActivites, selectMots, activites, localisations, mots } ->
                let
                    maxFiches =
                        1000

                    tropDeFiches =
                        foundFiches > maxFiches

                    noTags =
                        (List.length activites == 0)
                            && (List.length mots == 0)
                            && (List.length localisations == 0)

                    disabled =
                        request == RD.Loading
                in
                div []
                    [ div [ Grid.gridRow, Grid.gridRowGutters ]
                        [ div [ Grid.col12 ]
                            [ case request of
                                RD.Success _ ->
                                    div [ class "flex flex-col gap-2" ]
                                        [ viewIf (List.length activites > 0) <|
                                            div []
                                                [ div [ Typo.textBold ]
                                                    [ text "Activité(s) ajoutée(s)\u{00A0}:"
                                                    ]
                                                , div []
                                                    [ text <| String.join ", " activites
                                                    ]
                                                ]
                                        , viewIf (List.length localisations > 0) <|
                                            div []
                                                [ div [ Typo.textBold ]
                                                    [ text "Localisation(s) ajoutée(s)\u{00A0}:"
                                                    ]
                                                , div []
                                                    [ text <| String.join ", " localisations
                                                    ]
                                                ]
                                        , viewIf (List.length mots > 0) <|
                                            div []
                                                [ div [ Typo.textBold ]
                                                    [ text "Mot(s)-clé(s) ajouté(s)\u{00A0}:"
                                                    ]
                                                , div []
                                                    [ text <| String.join ", " mots
                                                    ]
                                                ]
                                        , div [ class "flex flex-col gap-4" ]
                                            [ div [ Typo.textBold ]
                                                [ text "Résultat de la mise à jour\u{00A0}:"
                                                ]
                                            , div [] <|
                                                [ div [ class "fr-text-default--success" ] [ text "Les fiches ont été qualifiées avec succès\u{00A0}!" ]
                                                ]
                                            ]
                                        ]

                                _ ->
                                    div [ class "flex flex-col gap-4" ]
                                        [ viewIf (RD.isFailure request) <|
                                            DSFR.Alert.alert Nothing DSFR.Alert.error <|
                                                DSFR.Alert.small
                                                    { title = Nothing
                                                    , description = text "Une erreur s'est produite, veuillez réessayer"
                                                    }
                                        , if tropDeFiches then
                                            div [] <|
                                                List.singleton <|
                                                    DSFR.Alert.alert Nothing DSFR.Alert.info <|
                                                        DSFR.Alert.medium
                                                            { title = "Trop de résultats"
                                                            , description = Just <| text "Impossible de qualifier plus de 1000 fiches à la fois, veuillez affiner votre sélection."
                                                            }

                                          else
                                            div [ Grid.gridRow, Grid.gridRowGutters ]
                                                [ div [ Grid.col6 ]
                                                    [ div [ class "flex flex-col gap-4" ]
                                                        [ selectActivites
                                                            |> MultiSelectRemote.viewCustom
                                                                { isDisabled = False
                                                                , selected = activites
                                                                , optionLabelFn = identity
                                                                , optionDescriptionFn = \_ -> ""
                                                                , optionsContainerMaxHeight = 300
                                                                , selectTitle = span [ Typo.textBold ] [ text "Activités réelles et filières", sup [ Attr.title hintActivites ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                                                , viewSelectedOptionFn = text
                                                                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                                                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                                                , noResultsForMsg =
                                                                    \searchText ->
                                                                        "Aucune autre activité n'a été trouvée"
                                                                            ++ (if searchText == "" then
                                                                                    ""

                                                                                else
                                                                                    " pour " ++ searchText
                                                                               )
                                                                , noOptionsMsg = "Aucune activité n'a été trouvée"
                                                                , newOption = Just identity
                                                                , searchPrompt = "Rechercher une activité"
                                                                }
                                                        , case activites of
                                                            [] ->
                                                                nothing

                                                            _ ->
                                                                activites
                                                                    |> List.map (\activite -> DSFR.Tag.deletable UnselectBatchActivite { data = activite, toString = identity })
                                                                    |> DSFR.Tag.medium
                                                        ]
                                                    ]
                                                , div [ Grid.col6 ]
                                                    [ div [ class "flex flex-col gap-4" ]
                                                        [ selectLocalisations
                                                            |> MultiSelectRemote.viewCustom
                                                                { isDisabled = False
                                                                , selected = localisations
                                                                , optionLabelFn = identity
                                                                , optionDescriptionFn = \_ -> ""
                                                                , optionsContainerMaxHeight = 300
                                                                , selectTitle = span [ Typo.textBold ] [ text "Zone géographique", sup [ Attr.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                                                , viewSelectedOptionFn = text
                                                                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                                                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                                                , noResultsForMsg =
                                                                    \searchText ->
                                                                        "Aucune autre zone géographique n'a été trouvée"
                                                                            ++ (if searchText == "" then
                                                                                    ""

                                                                                else
                                                                                    " pour " ++ searchText
                                                                               )
                                                                , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                                                                , newOption = Just identity
                                                                , searchPrompt = "Rechercher une zone géographique"
                                                                }
                                                        , case localisations of
                                                            [] ->
                                                                nothing

                                                            _ ->
                                                                localisations
                                                                    |> List.map (\localisation -> DSFR.Tag.deletable UnselectBatchLocalisation { data = localisation, toString = identity })
                                                                    |> DSFR.Tag.medium
                                                        , span [] [ Typo.link (Route.toUrl <| Route.ParamsEtiquettes) [ Typo.textSm ] [ text "Ajouter une zone géographique via un fichier GeoJSON" ] ]
                                                        ]
                                                    ]
                                                , div [ Grid.col6 ]
                                                    [ div [ class "flex flex-col gap-4" ]
                                                        [ selectMots
                                                            |> MultiSelectRemote.viewCustom
                                                                { isDisabled = False
                                                                , selected = mots
                                                                , optionLabelFn = identity
                                                                , optionDescriptionFn = \_ -> ""
                                                                , optionsContainerMaxHeight = 300
                                                                , selectTitle = span [ Typo.textBold ] [ text "Mots-clés", sup [ Attr.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                                                , viewSelectedOptionFn = text
                                                                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                                                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                                                , noResultsForMsg =
                                                                    \searchText ->
                                                                        "Aucun autre mot-clé n'a été trouvé"
                                                                            ++ (if searchText == "" then
                                                                                    ""

                                                                                else
                                                                                    " pour " ++ searchText
                                                                               )
                                                                , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                                                                , newOption = Just identity
                                                                , searchPrompt = "Rechercher un mot-clé"
                                                                }
                                                        , case mots of
                                                            [] ->
                                                                nothing

                                                            _ ->
                                                                mots
                                                                    |> List.map (\mot -> DSFR.Tag.deletable UnselectBatchMot { data = mot, toString = identity })
                                                                    |> DSFR.Tag.medium
                                                        ]
                                                    ]
                                                ]
                                        ]
                            ]
                        ]
                    , div [ Grid.gridRow, Grid.gridRowGutters ]
                        [ div [ Grid.col12 ]
                            [ (case request of
                                RD.Success _ ->
                                    [ DSFR.Button.new
                                        { onClick = Just <| CanceledQualifierFiches
                                        , label = "OK"
                                        }
                                    ]

                                _ ->
                                    [ DSFR.Button.new
                                        { onClick = Just <| ConfirmedQualifierFiches
                                        , label =
                                            if request == RD.Loading then
                                                "Qualification en cours..."

                                            else
                                                "Confirmer"
                                        }
                                        |> DSFR.Button.withAttrs
                                            [ Attr.title <|
                                                if tropDeFiches then
                                                    "Impossible de qualifier plus de " ++ String.fromInt maxFiches ++ " fiches à la fois"

                                                else if noTags then
                                                    "Veuillez ajouter au moins une étiquette"

                                                else
                                                    ""
                                            ]
                                        |> DSFR.Button.withDisabled (disabled || tropDeFiches || noTags)
                                    , DSFR.Button.new { onClick = Just <| CanceledQualifierFiches, label = "Annuler" }
                                        |> DSFR.Button.withDisabled disabled
                                        |> DSFR.Button.secondary
                                    ]
                              )
                                |> DSFR.Button.group
                                |> DSFR.Button.inline
                                |> DSFR.Button.alignedRightInverted
                                |> DSFR.Button.viewGroup
                            ]
                        ]
                    ]
        )
        Nothing
        |> Tuple.first


viewExcelModal : String -> Maybe () -> Html Msg
viewExcelModal currentUrl excelExport =
    let
        ( opened, content, title ) =
            case excelExport of
                Nothing ->
                    ( False
                    , nothing
                    , text "Exporter les résultats"
                    )

                Just () ->
                    ( True
                    , viewExcelModalBody currentUrl
                    , text "Exporter les résultats"
                    )
    in
    DSFR.Modal.view
        { id = "excel-export"
        , label = "excel-export"
        , openMsg = NoOp
        , closeMsg = Just <| ClickedCancelExportExcel
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


viewGeoJsonModal : String -> String -> Int -> WebData String -> Maybe GeoJsonFormat -> Html Msg
viewGeoJsonModal appUrl currentUrl results rechercheSauvegardeeRequest gje =
    let
        ( opened, content, title ) =
            case gje of
                Nothing ->
                    ( False
                    , nothing
                    , text "Exporter les résultats"
                    )

                Just geoJsonExport ->
                    ( True
                    , viewGeoJsonModalBody appUrl currentUrl results rechercheSauvegardeeRequest geoJsonExport
                    , text "Exporter les résultats"
                    )
    in
    DSFR.Modal.view
        { id = "geojson-export"
        , label = "geojson-export"
        , openMsg = NoOp
        , closeMsg = Just <| ClickedCanceledGeoJson
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


etablissementsMax : Int
etablissementsMax =
    1000


viewExcelModalBody : String -> Html Msg
viewExcelModalBody currentUrl =
    div []
        [ div [ Grid.gridRow, Grid.gridRowGutters ]
            [ div [ Grid.col12, class "flex flex-col gap-8" ]
                [ DSFR.Alert.alert Nothing DSFR.Alert.warning <|
                    DSFR.Alert.small
                        { title = Nothing
                        , description =
                            p [ class "!mb-0" ]
                                [ text "Les données listées ci-dessous sont des données restreintes. Pour des raisons de confidentialité, celles-ci n’apparaîtront pas dans l’export :"
                                , br []
                                , text "- Chiffres d’affaires\u{00A0};"
                                , br []
                                , text "- Effectifs moyens annuels\u{00A0};"
                                , br []
                                , text "- Effectifs moyens mensuels."
                                ]
                        }
                ]
            ]
        , div [ Grid.gridRow, Grid.gridRowGutters ]
            [ div [ Grid.col12 ]
                [ div [ class "flex flex-row justify-end p-4 gap-4" ]
                    [ [ let
                            exportUrl =
                                (Api.getEtablissements <|
                                    currentUrl
                                )
                                    ++ "&format=xlsx"
                        in
                        DSFR.Button.new { onClick = Just ClickedConfirmExportExcel, label = "Exporter" }
                            |> DSFR.Button.linkButtonExternal exportUrl
                            |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                      , DSFR.Button.new { onClick = Just <| ClickedCancelExportExcel, label = "Annuler" }
                            |> DSFR.Button.secondary
                      ]
                        |> DSFR.Button.group
                        |> DSFR.Button.iconsLeft
                        |> DSFR.Button.inline
                        |> DSFR.Button.alignedRightInverted
                        |> DSFR.Button.viewGroup
                    ]
                ]
            ]
        ]


viewGeoJsonModalBody : String -> String -> Int -> WebData String -> GeoJsonFormat -> Html Msg
viewGeoJsonModalBody appUrl currentUrl results rechercheSauvegardeeRequest geoJsonExport =
    let
        etablissementsTropNombreux =
            results > etablissementsMax
    in
    div []
        [ div [ Grid.gridRow, Grid.gridRowGutters ]
            [ div [ Grid.col12, class "flex flex-col gap-8" ]
                [ p [ class "!mb-0" ]
                    [ text "Vous pouvez utiliser le fichier téléchargé pour visualiser les établissements sur une carte grâce à des services en ligne comme "
                    , let
                        link =
                            Url.Builder.crossOrigin "https://umap.incubateur.anct.gouv.fr"
                                [ "fr", "map" ]
                                []
                      in
                      Typo.externalLink link [] [ text link ]
                    , text "."
                    ]
                ]
            ]
        , div [ Grid.gridRow, Grid.gridRowGutters ]
            [ div [ Grid.col12 ]
                [ div [ class "flex flex-row justify-end p-4 gap-4" ]
                    [ DSFR.Button.view <|
                        DSFR.Button.leftIcon DSFR.Icons.System.eyeLine <|
                            DSFR.Button.secondary <|
                                case rechercheSauvegardeeRequest of
                                    RD.Success lien ->
                                        let
                                            root =
                                                if String.contains "http" lien then
                                                    ""

                                                else
                                                    appUrl
                                        in
                                        DSFR.Button.new { onClick = Nothing, label = "Visualiser sur umap" }
                                            |> DSFR.Button.linkButtonExternal (Lib.Umap.umapLinkExport root lien)

                                    _ ->
                                        DSFR.Button.new { onClick = Just RequestedRechercheSauvegardee, label = "Créer un lien de visualisation" }
                                            |> DSFR.Button.withDisabled etablissementsTropNombreux
                                            |> DSFR.Button.withAttrs
                                                [ if etablissementsTropNombreux then
                                                    Attr.title <| "Impossible de visualiser plus de " ++ String.fromInt etablissementsMax ++ " éléments"

                                                  else
                                                    empty
                                                ]
                    , let
                        exportUrl =
                            (Api.getEtablissements <|
                                currentUrl
                            )
                                ++ "&format="
                                ++ geoJsonFormatToString geoJsonExport

                        ( currentFilters, _ ) =
                            currentUrl
                                |> Just
                                |> Filters.queryToFiltersAndPagination

                        touteLaFrance =
                            currentFilters.situationGeographique == Filters.TouteLaFrance

                        filtresPerso =
                            countActivePersoFilters currentFilters > 0

                        titleAttr =
                            if touteLaFrance && not filtresPerso then
                                [ Attr.title "Impossible d'exporter pour l'option \"Toute la France\"" ]

                            else
                                []

                        disabled =
                            etablissementsTropNombreux && (touteLaFrance && not filtresPerso)
                      in
                      DSFR.Button.new { onClick = Nothing, label = "Exporter" }
                        |> DSFR.Button.withAttrs titleAttr
                        |> DSFR.Button.linkButtonExternal exportUrl
                        |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                        |> DSFR.Button.withDisabled disabled
                        |> DSFR.Button.view
                    ]
                ]
            ]
        ]


cardPlaceholder : Html Msg
cardPlaceholder =
    div [ Grid.col12, Grid.colSm6 ]
        [ div [ class "p-2", Typo.textSm ]
            [ div
                [ class "fr-card--white flex flex-col p-4 custom-hover overflow-y-auto gap-2"
                , Attr.style "background-image" "none"
                ]
                [ div [ class "flex flex-row justify-between" ]
                    [ greyBadge 30
                    ]
                , div [ class "!mb-0" ]
                    [ greyPlaceholder 14
                    , greyPlaceholder 20
                    ]
                , div [ class "!mb-0" ]
                    [ greyPlaceholder 15
                    , greyPlaceholder 8
                    ]
                , div [ class "!mb-0" ]
                    [ greyPlaceholder 15
                    , greyPlaceholder 8
                    ]
                ]
            ]
        ]


greyPlaceholder : Int -> Html msg
greyPlaceholder length =
    "\u{00A0}"
        |> List.repeat length
        |> List.map (text >> List.singleton >> span [ class "w-[1rem] inline-block" ])
        |> span [ class "grey-background m-1" ]
        |> List.singleton
        |> div [ class "pulse-black" ]


greyBadge : Int -> Html msg
greyBadge length =
    "\u{00A0}"
        |> String.repeat length
        |> (\t -> { data = t, toString = identity })
        |> DSFR.Tag.unclickable
        |> List.singleton
        |> DSFR.Tag.medium
        |> List.singleton
        |> div [ class "pulse-black" ]


initSelectActivites : MultiSelectRemote.SmartSelect Msg String
initSelectActivites =
    MultiSelectRemote.init "select-activites"
        { selectionMsg = SelectedBatchActivites
        , internalMsg = UpdatedSelectBatchActivites
        , characterSearchThreshold = selectCharacterThreshold
        , debounceDuration = selectDebounceDuration
        }


initSelectLocalisations : MultiSelectRemote.SmartSelect Msg String
initSelectLocalisations =
    MultiSelectRemote.init "select-zones"
        { selectionMsg = SelectedBatchLocalisations
        , internalMsg = UpdatedSelectBatchLocalisations
        , characterSearchThreshold = selectCharacterThreshold
        , debounceDuration = selectDebounceDuration
        }


initSelectMots : MultiSelectRemote.SmartSelect Msg String
initSelectMots =
    MultiSelectRemote.init "select-mots"
        { selectionMsg = SelectedBatchMots
        , internalMsg = UpdatedSelectBatchMots
        , characterSearchThreshold = selectCharacterThreshold
        , debounceDuration = selectDebounceDuration
        }


selectActivitesConfig : MultiSelectRemote.SelectConfig String
selectActivitesConfig =
    { headers = []
    , url = Api.rechercheActivite
    , optionDecoder = decodeEtiquetteList
    }


selectLocalisationsConfig : MultiSelectRemote.SelectConfig String
selectLocalisationsConfig =
    { headers = []
    , url = Api.rechercheLocalisation
    , optionDecoder = decodeEtiquetteList
    }


selectMotsConfig : MultiSelectRemote.SelectConfig String
selectMotsConfig =
    { headers = []
    , url = Api.rechercheMot
    , optionDecoder = decodeEtiquetteList
    }


selectCharacterThreshold : Int
selectCharacterThreshold =
    0


selectDebounceDuration : Float
selectDebounceDuration =
    400


decodeMapInfo : Decoder Msg
decodeMapInfo =
    Decode.map UpdatedMapInfo <|
        Decode.field "detail" <|
            (Decode.succeed MapInfo
                |> andMap (Decode.field "center" decodePoint)
                |> andMap (Decode.field "south" Decode.float)
                |> andMap (Decode.field "west" Decode.float)
                |> andMap (Decode.field "north" Decode.float)
                |> andMap (Decode.field "east" Decode.float)
                |> andMap (Decode.field "zoom" Decode.float)
            )


decodePoint : Decoder ( Float, Float )
decodePoint =
    Decode.succeed Tuple.pair
        |> andMap (Decode.field "lng" Decode.float)
        |> andMap (Decode.field "lat" Decode.float)


adresseFilter : SingleSelectRemote.SmartSelect Msg ApiAdresse -> Html Msg
adresseFilter selectAdresse =
    div [ class "flex flex-col gap-2 relative z-20" ]
        [ div [ class "absolute top-[1em] left-[1em] w-[50%]" ]
            [ selectAdresse
                |> SingleSelectRemote.viewCustom
                    { isDisabled = False
                    , selected = Nothing
                    , optionLabelFn = .label
                    , optionDescriptionFn = \_ -> ""
                    , optionsContainerMaxHeight = 300
                    , selectTitle = nothing
                    , searchPrompt = "Rechercher une ville, une adresse"
                    , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                    , noResultsForMsg =
                        \searchText ->
                            "Aucune autre adresse n'a été trouvée"
                                ++ (if searchText == "" then
                                        ""

                                    else
                                        " pour " ++ searchText
                                   )
                    , noOptionsMsg = "Aucune adresse n'a été trouvée"
                    , error = Nothing
                    }
            ]
        ]


selectAdresseConfig : SingleSelectRemote.SelectConfig ApiAdresse
selectAdresseConfig =
    { headers = []
    , url = Api.rechercheAdresse Nothing Nothing Nothing
    , optionDecoder = decodeApiFeatures
    }


resetModals : Model -> Model
resetModals model =
    { model
        | rechercheSauvegardeeRequest = RD.NotAsked
        , batchRequest = Nothing
        , excelExport = Nothing
        , excelExportRequested = False
        , geoJsonExport = Nothing
        , filters = Filters.resetModals model.filters
    }
