module Pages.Deconnexion exposing (page)

import Accessibility exposing (Html, text)
import Effect
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Page () Shared.Msg (View ()) () ()
page _ =
    Spa.Page.element
        { view = view
        , init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        }


init : () -> ( (), Effect.Effect Shared.Msg () )
init () =
    ( ()
    , Effect.fromShared <| Shared.LoggedOut Nothing
    )
        |> Shared.pageChangeEffects


update : () -> () -> ( (), Effect.Effect Shared.Msg () )
update () model =
    model
        |> Effect.withNone


view : () -> View ()
view model =
    { title = UI.Layout.pageTitle "Déconnexion"
    , body = UI.Layout.lazyBody body model
    , route = Route.Deconnexion
    }


body : () -> Html ()
body () =
    text "Déconnexion en cours..."
