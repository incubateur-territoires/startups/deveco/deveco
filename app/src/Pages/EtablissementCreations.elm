module Pages.EtablissementCreations exposing (Model, Msg, page)

import Accessibility exposing (a, decorativeImg, div, h1, h2, label, p, select, span, text)
import Accessibility.Aria as Aria
import Api
import Api.Auth exposing (get, post, request)
import Api.EntityId exposing (EntityId, unsafeConvert)
import DSFR.Accordion
import DSFR.Button
import DSFR.Checkbox
import DSFR.Grid as Grid
import DSFR.Icons
import DSFR.Icons.Design
import DSFR.Icons.System
import DSFR.Modal
import DSFR.Pagination
import DSFR.Radio
import DSFR.SearchBar
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Adresse exposing (ApiStreet, decodeApiStreet)
import Data.Demande exposing (TypeDemande)
import Data.EtablissementCreation exposing (EtablissementCreation, decodeEtablissementCreation)
import Data.EtablissementCreationId exposing (EtablissementCreationId)
import Data.Etiquette exposing (decodeEtiquetteList)
import Effect
import Html exposing (Html, hr, option, sup)
import Html.Attributes as Attr exposing (class, classList)
import Html.Events as Events
import Html.Extra exposing (nothing, viewIf, viewMaybe)
import Html.Lazy
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import Lib.UI exposing (filterBy, formatIntWithThousandSpacing, plural, withEmptyAs)
import Lib.Variables exposing (hintActivites, hintMotsCles, hintZoneGeographique)
import MultiSelect
import MultiSelectRemote exposing (SelectConfig)
import Pages.Etablissements.Filters as Filters
import QS
import RemoteData as RD
import Route
import Shared
import SingleSelectRemote
import Spa.Page exposing (Page)
import UI.Entite
import UI.EtablissementCreation
import UI.Layout
import UI.Pagination exposing (Pagination, defaultPagination, paginationToQuery, queryToPagination)
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page { etablissementCreationIds } user =
    Spa.Page.onNewFlags (queryToFiltersAndPagination >> DoSearch) <|
        Spa.Page.element <|
            { init = init (List.map unsafeConvert etablissementCreationIds) user
            , update = update
            , view = view
            , subscriptions = subscriptions
            }


type alias Model =
    { etablissementCreations : RD.WebData (List EtablissementCreation)
    , etablissementCreationsRequests : Int
    , filters : Filters
    , filtersSource : RD.WebData FiltersSource
    , pagination : Pagination
    , selectActivites : MultiSelect.SmartSelect Msg String
    , selectDemandes : MultiSelect.SmartSelect Msg TypeDemande
    , selectZones : MultiSelect.SmartSelect Msg String
    , selectMots : MultiSelect.SmartSelect Msg String
    , lastUrlFetched : String
    , selectedEntites : List (EntityId EtablissementCreationId)
    , batchRequest : Maybe BatchRequest
    , selectRue : SingleSelectRemote.SmartSelect Msg ApiStreet
    , selectQpvs : MultiSelectRemote.SmartSelect Msg Qpv
    }


type alias Qpv =
    { id : String
    , nom : String
    }


type alias BatchRequest =
    { request : RD.WebData ( List EtablissementCreation, Pagination )
    , selectActivites : MultiSelectRemote.SmartSelect Msg String
    , selectLocalisations : MultiSelectRemote.SmartSelect Msg String
    , selectMots : MultiSelectRemote.SmartSelect Msg String
    , activites : List String
    , localisations : List String
    , mots : List String
    }


type alias Filters =
    { recherche : String
    , activites : List String
    , demandes : List TypeDemande
    , zones : List String
    , mots : List String
    , rue : Maybe ApiStreet
    , qpvs : List Qpv
    , suivi : Suivi
    , orderBy : OrderBy
    , orderDirection : OrderDirection
    }


type Suivi
    = Tout
    | EnCours
    | Cree
    | Abandonnee


type OrderBy
    = EtablissementCreationConsultation
    | EtablissementCreationCreation
    | EtablissementCreationAlphabetique


orderByToString : OrderBy -> String
orderByToString orderBy =
    case orderBy of
        EtablissementCreationConsultation ->
            "consultation"

        EtablissementCreationCreation ->
            "creation"

        EtablissementCreationAlphabetique ->
            "alphabetique"


stringToOrderBy : String -> Maybe OrderBy
stringToOrderBy orderBy =
    case orderBy of
        "alphabetique" ->
            Just EtablissementCreationAlphabetique

        "consultation" ->
            Just EtablissementCreationConsultation

        "creation" ->
            Just EtablissementCreationCreation

        _ ->
            Nothing


type OrderDirection
    = Desc
    | Asc


orderDirectionToString : OrderDirection -> String
orderDirectionToString orderDirection =
    case orderDirection of
        Asc ->
            "asc"

        Desc ->
            "desc"


stringToOrderDirection : String -> Maybe OrderDirection
stringToOrderDirection orderDirection =
    case orderDirection of
        "asc" ->
            Just Asc

        "desc" ->
            Just Desc

        _ ->
            Nothing


defaultFilters : Filters
defaultFilters =
    { recherche = ""
    , activites = []
    , demandes = []
    , zones = []
    , mots = []
    , rue = Nothing
    , qpvs = []
    , suivi = Tout
    , orderBy = EtablissementCreationConsultation
    , orderDirection = Desc
    }


type alias FiltersSource =
    { activites : List String
    , zones : List String
    , mots : List String
    , demandes : List TypeDemande
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | ReceivedFiltersSource (RD.WebData FiltersSource)
    | FetchEtablissementCreations
    | DoSearch ( Filters, Pagination )
    | UpdatedSearch String
    | ReceivedEtablissementCreations (RD.WebData ( List EtablissementCreation, Pagination ))
    | ClearAllFilters
    | SetSuiviFilter Suivi
    | SelectedActivites ( List String, MultiSelect.Msg String )
    | UpdatedSelectActivites (MultiSelect.Msg String)
    | UnselectActivite String
    | SelectedDemandes ( List TypeDemande, MultiSelect.Msg TypeDemande )
    | UpdatedSelectDemandes (MultiSelect.Msg TypeDemande)
    | UnselectDemande TypeDemande
    | SelectedZones ( List String, MultiSelect.Msg String )
    | UpdatedSelectZones (MultiSelect.Msg String)
    | UnselectZone String
    | SelectedMots ( List String, MultiSelect.Msg String )
    | UpdatedSelectMots (MultiSelect.Msg String)
    | UnselectMot String
    | ToggleSelection (List (EntityId EtablissementCreationId)) Bool
    | EmptySelection
    | ClickedQualifierEtablissementCreations
    | CanceledQualifierEtablissementCreations
    | ConfirmedQualifierEtablissementCreations
    | ReceivedQualifierEtablissementCreations (RD.WebData ( List EtablissementCreation, Pagination ))
    | SelectedBatchActivites ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectBatchActivites (MultiSelectRemote.Msg String)
    | UnselectBatchActivite String
    | SelectedBatchLocalisations ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectBatchLocalisations (MultiSelectRemote.Msg String)
    | UnselectBatchLocalisation String
    | SelectedBatchMots ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectBatchMots (MultiSelectRemote.Msg String)
    | UnselectBatchMot String
    | SelectedRue ( ApiStreet, SingleSelectRemote.Msg ApiStreet )
    | UpdatedSelectRue (SingleSelectRemote.Msg ApiStreet)
    | ClearRue
    | SelectedQpvs ( List Qpv, MultiSelectRemote.Msg Qpv )
    | UpdatedSelectQpvs (MultiSelectRemote.Msg Qpv)
    | UnselectQpv Qpv
    | ClickedOrderBy OrderBy OrderDirection
    | ClickedSearch


queryToFilters : String -> Filters
queryToFilters rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    recherche =
                        query
                            |> QS.getAsStringList queryKeys.recherche
                            |> List.head
                            |> Maybe.withDefault ""

                    activites =
                        query
                            |> QS.getAsStringList queryKeys.activites

                    demandes =
                        query
                            |> QS.getAsStringList queryKeys.demandes
                            |> List.filterMap Data.Demande.stringToTypeDemande

                    zones =
                        query
                            |> QS.getAsStringList queryKeys.localisations

                    orderBy =
                        query
                            |> QS.getAsStringList queryKeys.tri
                            |> List.head
                            |> Maybe.andThen stringToOrderBy
                            |> Maybe.withDefault EtablissementCreationConsultation

                    orderDirection =
                        query
                            |> QS.getAsStringList queryKeys.direction
                            |> List.head
                            |> Maybe.andThen stringToOrderDirection
                            |> Maybe.withDefault Desc

                    mots =
                        query
                            |> QS.getAsStringList queryKeys.motsCles

                    numero =
                        query
                            |> QS.getAsStringList queryKeys.numero
                            |> List.head

                    rue =
                        query
                            |> QS.getAsStringList queryKeys.rue
                            |> List.head

                    cp =
                        query
                            |> QS.getAsStringList queryKeys.cp
                            |> List.head

                    qpvs =
                        query
                            |> QS.getAsStringList queryKeys.qpvs
                            |> List.map (\id -> { id = id, nom = "" })

                    suivi =
                        query
                            |> QS.getAsStringList queryKeys.suivi
                            |> List.head
                            |> Maybe.map
                                (\s ->
                                    if s == "createurs" then
                                        EnCours

                                    else if s == "etablissements" then
                                        Cree

                                    else if s == "abandonnes" then
                                        Abandonnee

                                    else
                                        Tout
                                )
                            |> Maybe.withDefault Tout
                in
                { recherche = recherche
                , activites = activites
                , demandes = demandes
                , zones = zones
                , mots = mots
                , rue =
                    Maybe.map
                        (\c ->
                            let
                                n =
                                    numero
                                        |> Maybe.map (\num -> num ++ " ")
                                        |> Maybe.withDefault ""

                                r =
                                    rue
                                        |> Maybe.map (\ru -> ru ++ " - ")
                                        |> Maybe.withDefault ""
                            in
                            ApiStreet (n ++ r ++ c) (numero |> Maybe.withDefault "") r c Nothing ""
                        )
                        cp
                , qpvs = qpvs
                , suivi = suivi
                , orderBy = orderBy
                , orderDirection = orderDirection
                }
           )


filtersToQuery : Filters -> QS.Query
filtersToQuery filters =
    let
        setActivites =
            case filters.activites of
                [] ->
                    identity

                activites ->
                    activites
                        |> QS.setListStr queryKeys.activites

        setDemandes =
            case filters.demandes of
                [] ->
                    identity

                demandes ->
                    demandes
                        |> List.map Data.Demande.typeDemandeToString
                        |> QS.setListStr queryKeys.demandes

        setZones =
            case filters.zones of
                [] ->
                    identity

                zones ->
                    zones
                        |> QS.setListStr queryKeys.localisations

        setRecherche =
            case filters.recherche of
                "" ->
                    identity

                recherche ->
                    recherche
                        |> QS.setStr queryKeys.recherche

        setMots =
            case filters.mots of
                [] ->
                    identity

                mots ->
                    mots
                        |> QS.setListStr queryKeys.motsCles

        setNumero =
            filters.rue
                |> Maybe.map .street
                |> Maybe.map (QS.setStr queryKeys.rue)
                |> Maybe.withDefault identity

        setRue =
            filters.rue
                |> Maybe.map .housenumber
                |> Maybe.map (QS.setStr queryKeys.numero)
                |> Maybe.withDefault identity

        setCp =
            filters.rue
                |> Maybe.map .postcode
                |> Maybe.map (QS.setStr queryKeys.cp)
                |> Maybe.withDefault identity

        setQpvs =
            case filters.qpvs of
                [] ->
                    identity

                demandes ->
                    demandes
                        |> List.map .id
                        |> QS.setListStr queryKeys.qpvs

        setSuivi =
            case filters.suivi of
                EnCours ->
                    "createurs"
                        |> QS.setStr queryKeys.suivi

                Cree ->
                    "etablissements"
                        |> QS.setStr queryKeys.suivi

                Abandonnee ->
                    "abandonnes"
                        |> QS.setStr queryKeys.suivi

                _ ->
                    identity

        setOrder =
            case filters.orderBy of
                EtablissementCreationAlphabetique ->
                    QS.setStr queryKeys.tri <|
                        "alphabetique"

                EtablissementCreationCreation ->
                    QS.setStr queryKeys.tri <|
                        "creation"

                EtablissementCreationConsultation ->
                    identity

        setDirection =
            case filters.orderDirection of
                Asc ->
                    QS.setStr queryKeys.direction <|
                        "asc"

                Desc ->
                    identity
    in
    QS.empty
        |> setActivites
        |> setDemandes
        |> setZones
        |> setRecherche
        |> setMots
        |> setNumero
        |> setRue
        |> setCp
        |> setQpvs
        |> setSuivi
        |> setOrder
        |> setDirection


queryKeys : { activites : String, demandes : String, localisations : String, recherche : String, page : String, pages : String, motsCles : String, numero : String, rue : String, cp : String, tri : String, direction : String, qpvs : String, suivi : String }
queryKeys =
    { activites = "activites"
    , demandes = "demandes"
    , localisations = "localisations"
    , recherche = "recherche"
    , page = "page"
    , pages = "pages"
    , motsCles = "motsCles"
    , numero = "numero"
    , rue = "rue"
    , cp = "cp"
    , tri = "tri"
    , direction = "direction"
    , qpvs = "qpvs"
    , suivi = "suivi"
    }


serializeModel : ( Filters, Pagination ) -> String
serializeModel ( filters, pagination ) =
    QS.merge (paginationToQuery pagination) (filtersToQuery filters)
        |> QS.serialize (QS.config |> QS.addQuestionMark False)


toHref : ( Filters, Pagination ) -> Int -> String
toHref ( filters, pagination ) newPage =
    Route.toUrl <|
        Route.Createurs <|
            (\q ->
                if q == "" then
                    Nothing

                else
                    Just q
            )
            <|
                serializeModel ( filters, { pagination | page = newPage } )


queryToFiltersAndPagination : Maybe String -> ( Filters, Pagination )
queryToFiltersAndPagination rawQuery =
    case rawQuery of
        Nothing ->
            ( defaultFilters, defaultPagination )

        Just query ->
            ( queryToFilters query, queryToPagination query )


init : List (EntityId EtablissementCreationId) -> Shared.User -> Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init etablissementCreationSelection _ rawQuery =
    let
        ( filters, pagination ) =
            queryToFiltersAndPagination rawQuery

        apiStreet =
            filters.rue |> Maybe.map .label |> Maybe.withDefault ""

        ( selectRue, selectRueCmd ) =
            SingleSelectRemote.init selectRueId
                { selectionMsg = SelectedRue
                , internalMsg = UpdatedSelectRue
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
                |> SingleSelectRemote.setText apiStreet selectRueConfig
    in
    ( { etablissementCreations = RD.Loading
      , etablissementCreationsRequests = 0
      , filters = filters
      , filtersSource = RD.Loading
      , pagination = pagination
      , selectActivites =
            MultiSelect.init selectActivitesId
                { selectionMsg = SelectedActivites
                , internalMsg = UpdatedSelectActivites
                }
      , selectDemandes =
            MultiSelect.init selectDemandesId
                { selectionMsg = SelectedDemandes
                , internalMsg = UpdatedSelectDemandes
                }
      , selectZones =
            MultiSelect.init selectZonesId
                { selectionMsg = SelectedZones
                , internalMsg = UpdatedSelectZones
                }
      , selectMots =
            MultiSelect.init selectMotsId
                { selectionMsg = SelectedMots
                , internalMsg = UpdatedSelectMots
                }
      , lastUrlFetched = serializeModel ( filters, pagination )
      , selectedEntites = etablissementCreationSelection
      , batchRequest = Nothing
      , selectRue = selectRue
      , selectQpvs =
            MultiSelectRemote.init selectQpvsId
                { selectionMsg = SelectedQpvs
                , internalMsg = UpdatedSelectQpvs
                , characterSearchThreshold = selectCharacterThresholdEtiquettes
                , debounceDuration = selectDebounceDuration
                }
      }
    , Effect.batch
        [ Effect.fromCmd <| getEtablissementCreations 0 ( filters, pagination )
        , Effect.fromCmd <| getFiltersSource
        , Effect.fromCmd selectRueCmd
        ]
    )
        |> Shared.pageChangeEffects


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ model.selectActivites |> MultiSelect.subscriptions
        , model.selectDemandes |> MultiSelect.subscriptions
        , model.selectZones |> MultiSelect.subscriptions
        , model.selectMots |> MultiSelect.subscriptions
        , model.selectQpvs |> MultiSelectRemote.subscriptions
        , model.batchRequest
            |> Maybe.map .selectActivites
            |> Maybe.map MultiSelectRemote.subscriptions
            |> Maybe.withDefault Sub.none
        , model.batchRequest
            |> Maybe.map .selectLocalisations
            |> Maybe.map MultiSelectRemote.subscriptions
            |> Maybe.withDefault Sub.none
        , model.batchRequest
            |> Maybe.map .selectMots
            |> Maybe.map MultiSelectRemote.subscriptions
            |> Maybe.withDefault Sub.none
        , model.selectRue |> SingleSelectRemote.subscriptions
        ]


selectActivitesId : String
selectActivitesId =
    "champ-selection-activites"


selectDemandesId : String
selectDemandesId =
    "champ-selection-demandes"


selectZonesId : String
selectZonesId =
    "champ-selection-zones"


selectMotsId : String
selectMotsId =
    "champ-selection-mots"


selectQpvsId : String
selectQpvsId =
    "champ-selection-qpvs"


getEtablissementCreationsTracker : String
getEtablissementCreationsTracker =
    "get-etablissementCreations-tracker"


getEtablissementCreations : Int -> ( Filters, Pagination ) -> Cmd Msg
getEtablissementCreations trackerCount ( filters, pagination ) =
    request
        { method = "GET"
        , headers = []
        , url = Api.getEtablissementCreations <| serializeModel ( filters, pagination )
        , body = Http.emptyBody
        , timeout = Nothing
        , tracker = Just <| getEtablissementCreationsTracker ++ "-" ++ String.fromInt trackerCount
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedEtablissementCreations
        }
        decodePayload


decodePayload : Decoder ( List EtablissementCreation, Pagination )
decodePayload =
    Decode.succeed (\p pages results data -> ( data, Pagination p pages results ))
        |> andMap (Decode.field "page" Decode.int)
        |> andMap (Decode.field "pages" Decode.int)
        |> andMap (Decode.field "total" Decode.int)
        |> andMap (Decode.field "elements" decodeEtablissementCreations)


decodeEtablissementCreations : Decoder (List EtablissementCreation)
decodeEtablissementCreations =
    Decode.list decodeEtablissementCreation


getFiltersSource : Cmd Msg
getFiltersSource =
    get
        { url = Api.getEquipeMetaFiltres "createurs" }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedFiltersSource
        }
        decodeFiltersSource


decodeFiltersSource : Decoder FiltersSource
decodeFiltersSource =
    Decode.succeed FiltersSource
        |> andMap (Decode.field "activites" <| Decode.list <| Decode.field "nom" Decode.string)
        |> andMap (Decode.field "localisations" <| Decode.list <| Decode.field "nom" Decode.string)
        |> andMap (Decode.field "motsCles" <| Decode.list <| Decode.field "nom" Decode.string)
        |> andMap (Decode.field "demandes" <| Decode.list Data.Demande.decodeTypeDemande)


sendBatchRequest : Filters -> Pagination -> List (EntityId EtablissementCreationId) -> BatchRequest -> Cmd Msg
sendBatchRequest filters pagination selectedEntites { activites, localisations, mots } =
    let
        jsonBody =
            [ ( "activites", Encode.list Encode.string activites )
            , ( "localisations", Encode.list Encode.string localisations )
            , ( "motsCles", Encode.list Encode.string mots )
            , ( "selection", Encode.list Api.EntityId.encodeEntityId selectedEntites )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    post
        { url = Api.updateEtablissementCreationEtiquettes <| serializeModel ( filters, pagination )
        , body = jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedQualifierEtablissementCreations
        }
        decodePayload


rechercherEtablissementCreationInputName : String
rechercherEtablissementCreationInputName =
    "rechercher-etablissementCreation"


selectCharacterThresholdEtiquettes : Int
selectCharacterThresholdEtiquettes =
    0


selectCharacterThreshold : Int
selectCharacterThreshold =
    2


selectDebounceDuration : Float
selectDebounceDuration =
    400


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        ReceivedFiltersSource response ->
            { model | filtersSource = response }
                |> Effect.withNone

        ReceivedEtablissementCreations etablissementCreations ->
            case etablissementCreations of
                RD.Success ( fs, pagination ) ->
                    ( { model | etablissementCreations = RD.Success fs, pagination = pagination }
                    , Effect.none
                    )

                _ ->
                    ( { model | etablissementCreations = etablissementCreations |> RD.map Tuple.first }
                    , Effect.none
                    )

        FetchEtablissementCreations ->
            let
                nextUrl =
                    serializeModel ( model.filters, model.pagination )
            in
            if nextUrl /= model.lastUrlFetched then
                ( { model | etablissementCreations = RD.Loading }
                , updateUrl model
                )

            else
                ( model, Effect.none )

        DoSearch ( filters, pagination ) ->
            let
                nextUrl =
                    serializeModel ( filters, pagination )

                requestChanged =
                    nextUrl /= model.lastUrlFetched
            in
            if requestChanged then
                ( { model
                    | pagination = pagination
                    , etablissementCreations = RD.Loading
                    , lastUrlFetched = nextUrl
                    , etablissementCreationsRequests = model.etablissementCreationsRequests + 1
                  }
                , Effect.batch
                    [ Effect.fromCmd (Http.cancel <| getEtablissementCreationsTracker ++ "-" ++ String.fromInt model.etablissementCreationsRequests)
                    , Effect.fromCmd (getEtablissementCreations (model.etablissementCreationsRequests + 1) ( filters, pagination ))
                    ]
                )

            else
                ( model, Effect.none )

        ClearAllFilters ->
            let
                ( updatedSelectRue, selectCmd ) =
                    SingleSelectRemote.setText "" selectRueConfig model.selectRue

                newModel =
                    { model | filters = { defaultFilters | recherche = model.filters.recherche }, selectRue = updatedSelectRue }
            in
            ( newModel
            , Effect.fromCmd selectCmd
            )

        UpdatedSearch recherche ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | recherche = recherche }) }
            in
            ( newModel
            , Effect.none
            )

        SelectedActivites ( activites, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectActivites

                activitesUniques =
                    case activites of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                activites

                newModel =
                    { model
                        | selectActivites = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | activites = activitesUniques })
                    }
            in
            ( newModel
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectActivites sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectActivites
            in
            ( { model
                | selectActivites = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectActivite activite ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | activites =
                                                filters.activites
                                                    |> List.filter ((/=) activite)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedDemandes ( demandes, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectDemandes

                demandesUniques =
                    case demandes of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                demandes

                newModel =
                    { model
                        | selectDemandes = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | demandes = demandesUniques })
                    }
            in
            ( newModel
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectDemandes sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectDemandes
            in
            ( { model
                | selectDemandes = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectDemande demande ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | demandes =
                                                filters.demandes
                                                    |> List.filter ((/=) demande)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedZones ( zones, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZones

                zonesUniques =
                    case zones of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                zones

                newModel =
                    { model
                        | selectZones = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | zones = zonesUniques })
                    }
            in
            ( newModel
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectZones sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZones
            in
            ( { model
                | selectZones = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectZone zone ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | zones =
                                                filters.zones
                                                    |> List.filter ((/=) zone)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedMots ( mots, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectMots

                motsUniques =
                    case mots of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                mots

                newModel =
                    { model
                        | selectMots = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | mots = motsUniques })
                    }
            in
            ( newModel
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectMots sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectMots
            in
            ( { model
                | selectMots = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectMot mot ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | mots =
                                                filters.mots
                                                    |> List.filter ((/=) mot)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ToggleSelection etablissementCreationsToToggle add ->
            let
                entiteInSelection : EntityId EtablissementCreationId -> Bool
                entiteInSelection selected =
                    List.member selected etablissementCreationsToToggle

                action selected =
                    selected
                        |> List.filter (entiteInSelection >> not)
                        -- remove any etablissementCreationsToToggle from the current list if present
                        |> (if add then
                                -- add all of them to the selection
                                (++) etablissementCreationsToToggle

                            else
                                -- do nothing, they're already gone
                                identity
                           )

                selectedEntites =
                    model.selectedEntites
                        |> action
            in
            { model | selectedEntites = selectedEntites }
                |> Effect.withShared
                    (Shared.BackUpEntitesSelection <|
                        List.map unsafeConvert <|
                            selectedEntites
                    )

        EmptySelection ->
            { model | selectedEntites = [] }
                |> Effect.withShared (Shared.BackUpEntitesSelection [])

        ClickedQualifierEtablissementCreations ->
            { model
                | batchRequest = initBatchRequest
            }
                |> Effect.withNone

        CanceledQualifierEtablissementCreations ->
            { model | batchRequest = Nothing }
                |> Effect.withNone

        ConfirmedQualifierEtablissementCreations ->
            case model.batchRequest of
                Nothing ->
                    model
                        |> Effect.withNone

                Just br ->
                    { model
                        | batchRequest =
                            Just { br | request = RD.Loading }
                    }
                        |> Effect.withCmd (sendBatchRequest model.filters model.pagination model.selectedEntites br)

        ReceivedQualifierEtablissementCreations response ->
            let
                ( etablissementCreations, pagination, cmd ) =
                    case response of
                        RD.Success ( f, p ) ->
                            ( RD.Success f, p, getFiltersSource )

                        _ ->
                            ( model.etablissementCreations, model.pagination, Cmd.none )
            in
            { model
                | batchRequest =
                    model.batchRequest
                        |> Maybe.map (\br -> { br | request = response })
                , pagination = pagination
                , etablissementCreations = etablissementCreations
            }
                |> Effect.withCmd cmd

        SelectedBatchActivites ( activites, sMsg ) ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectActivitesConfig br.selectActivites

                        activitesUniques =
                            case activites of
                                [] ->
                                    []

                                a :: rest ->
                                    if List.member a rest then
                                        rest

                                    else
                                        activites
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br | selectActivites = updatedSelect, activites = activitesUniques }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UpdatedSelectBatchActivites sMsg ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectActivitesConfig br.selectActivites
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br | selectActivites = updatedSelect }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UnselectBatchActivite activite ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | activites =
                                        br.activites
                                            |> List.filter ((/=) activite)
                                }
                      }
                    , Effect.none
                    )

        SelectedBatchLocalisations ( localisations, sMsg ) ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectLocalisationsConfig br.selectLocalisations

                        localisationsUniques =
                            case localisations of
                                [] ->
                                    []

                                a :: rest ->
                                    if List.member a rest then
                                        rest

                                    else
                                        localisations
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | localisations = localisationsUniques
                                    , selectLocalisations = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UpdatedSelectBatchLocalisations sMsg ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectLocalisationsConfig br.selectLocalisations
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | selectLocalisations = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UnselectBatchLocalisation localisation ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | localisations =
                                        br.localisations
                                            |> List.filter ((/=) localisation)
                                }
                      }
                    , Effect.none
                    )

        SelectedBatchMots ( mots, sMsg ) ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectMotsConfig br.selectMots

                        motsUniques =
                            case mots of
                                [] ->
                                    []

                                a :: rest ->
                                    if List.member a rest then
                                        rest

                                    else
                                        mots
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | mots = motsUniques
                                    , selectMots = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UpdatedSelectBatchMots sMsg ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectMotsConfig br.selectMots
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | selectMots = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UnselectBatchMot mot ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | mots =
                                        br.mots
                                            |> List.filter ((/=) mot)
                                }
                      }
                    , Effect.none
                    )

        SelectedRue ( apiRue, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectRueConfig model.selectRue

                newModel =
                    { model
                        | selectRue = updatedSelect
                        , filters =
                            model.filters
                                |> (\f -> { f | rue = Just apiRue })
                    }
            in
            ( newModel
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectRue sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectRueConfig model.selectRue

                newModel =
                    { model | selectRue = updatedSelect }
            in
            ( newModel, Effect.fromCmd selectCmd )

        ClearRue ->
            let
                ( updatedSelectRue, selectCmd ) =
                    SingleSelectRemote.setText "" selectRueConfig model.selectRue

                newModel =
                    { model | filters = model.filters |> (\f -> { f | rue = Nothing }), selectRue = updatedSelectRue }
            in
            ( newModel, Effect.fromCmd selectCmd )

        SelectedQpvs ( qpvs, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectQpvsConfig model.selectQpvs

                qpvsUniques =
                    case qpvs of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a.id <| List.map .id rest then
                                rest

                            else
                                qpvs

                newModel =
                    { model
                        | selectQpvs = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | qpvs = qpvsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectQpvs sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectQpvsConfig model.selectQpvs
            in
            ( { model
                | selectQpvs = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectQpv qpv ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | qpvs =
                                                filters.qpvs
                                                    |> List.filter (\{ id } -> id /= qpv.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ClickedOrderBy orderBy orderDirection ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | orderBy = orderBy, orderDirection = orderDirection }) }
            in
            ( newModel
            , updateUrl newModel
            )

        SetSuiviFilter suivi ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | suivi = suivi }) }
            in
            ( newModel
            , Effect.none
            )

        ClickedSearch ->
            ( model, updateUrl model )


initBatchRequest : Maybe BatchRequest
initBatchRequest =
    Just
        { request = RD.NotAsked
        , selectActivites = initSelectActivites
        , selectLocalisations = initSelectLocalisations
        , selectMots = initSelectMots
        , activites = []
        , localisations = []
        , mots = []
        }


initSelectActivites : MultiSelectRemote.SmartSelect Msg String
initSelectActivites =
    MultiSelectRemote.init "select-activites"
        { selectionMsg = SelectedBatchActivites
        , internalMsg = UpdatedSelectBatchActivites
        , characterSearchThreshold = selectCharacterThresholdEtiquettes
        , debounceDuration = selectDebounceDuration
        }


initSelectLocalisations : MultiSelectRemote.SmartSelect Msg String
initSelectLocalisations =
    MultiSelectRemote.init "select-zones"
        { selectionMsg = SelectedBatchLocalisations
        , internalMsg = UpdatedSelectBatchLocalisations
        , characterSearchThreshold = selectCharacterThresholdEtiquettes
        , debounceDuration = selectDebounceDuration
        }


initSelectMots : MultiSelectRemote.SmartSelect Msg String
initSelectMots =
    MultiSelectRemote.init "select-mots"
        { selectionMsg = SelectedBatchMots
        , internalMsg = UpdatedSelectBatchMots
        , characterSearchThreshold = selectCharacterThresholdEtiquettes
        , debounceDuration = selectDebounceDuration
        }


selectActivitesConfig : SelectConfig String
selectActivitesConfig =
    { headers = []
    , url = Api.rechercheActivite
    , optionDecoder = decodeEtiquetteList
    }


selectLocalisationsConfig : SelectConfig String
selectLocalisationsConfig =
    { headers = []
    , url = Api.rechercheLocalisation
    , optionDecoder = decodeEtiquetteList
    }


selectMotsConfig : SelectConfig String
selectMotsConfig =
    { headers = []
    , url = Api.rechercheMot
    , optionDecoder = decodeEtiquetteList
    }


selectRueId : String
selectRueId =
    "champ-selection-rue"


selectRueConfig : SingleSelectRemote.SelectConfig ApiStreet
selectRueConfig =
    { headers = []
    , url = Api.rechercheAdresse Nothing Nothing Nothing
    , optionDecoder = decodeApiStreet
    }


updateUrl : Model -> Effect.Effect Shared.Msg Msg
updateUrl model =
    Effect.batch
        [ Effect.fromShared <|
            Shared.Navigate <|
                Route.Createurs <|
                    (\q ->
                        if q == "" then
                            Nothing

                        else
                            Just q
                    )
                    <|
                        serializeModel ( model.filters, model.pagination )
        , Effect.fromCmd <| Lib.UI.scrollElementTo (\_ -> NoOp) Nothing ( 0, 0 )
        ]


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle <| "Créateurs d'entreprise"
    , body = UI.Layout.lazyBody body model
    , route =
        Route.Createurs <|
            (\q ->
                if q == "" then
                    Nothing

                else
                    Just q
            )
            <|
                serializeModel ( model.filters, model.pagination )
    }


body : Model -> Html Msg
body model =
    case ( showingUnfilteredResults model.lastUrlFetched, model.etablissementCreations ) of
        ( True, RD.Success [] ) ->
            div [ class "fr-card--white text-center p-20" ]
                [ div [ Grid.gridRow ]
                    [ div [ Grid.col3, class "flex justify-center" ]
                        [ decorativeImg [ Attr.src "/assets/deveco-carte-identite.svg" ] ]
                    , div [ Grid.col9, class "flex flex-col fr-card--white text-center justify-center" ]
                        [ p []
                            [ text "Créez votre première fiche "
                            , span [ Typo.textBold, class "blue-text" ] [ text "Créateur d'entreprise" ]
                            , text " (porteur de projet)"
                            ]
                        , div [ class "flex justify-center" ]
                            [ DSFR.Button.new { onClick = Nothing, label = "Créer une fiche" }
                                |> DSFR.Button.linkButton (Route.toUrl <| Route.CreateurNew)
                                |> DSFR.Button.view
                            ]
                        ]
                    ]
                ]

        _ ->
            let
                { activites, demandes, zones, mots } =
                    case model.filtersSource of
                        RD.Success filtersSource ->
                            filtersSource

                        _ ->
                            { activites = [], demandes = [], zones = [], mots = [] }
            in
            div []
                [ viewModal model.pagination.results model.selectedEntites model.batchRequest
                , div [ Grid.gridRow, Grid.gridRowGutters, Grid.gridRowMiddle ] <|
                    [ h1 [ class "flex flex-row gap-2 fr-h6 !my-0", Grid.colOffsetSm4, Grid.colSm4, Grid.col12 ]
                        [ DSFR.Icons.iconLG UI.Entite.iconeCreateur
                        , text <|
                            (if showingUnfilteredResults model.lastUrlFetched then
                                "Créateurs d'entreprise"

                             else
                                "Résultats de la recherche"
                            )
                                ++ (model.pagination
                                        |> .results
                                        |> formatIntWithThousandSpacing
                                        |> (\t -> " (" ++ t ++ ")")
                                   )
                        ]
                    , let
                        selectedLength =
                            model.selectedEntites |> List.length

                        results =
                            model.pagination.results
                      in
                      div [ Grid.colSm4, Grid.col12, class "flex flex-row items-baseline justify-end" ]
                        [ div []
                            [ DSFR.Button.dropdownSelector { label = text "Actions", hint = Just "Actions du portefeuille", id = "portefeuille-actions" } <|
                                [ DSFR.Button.new { onClick = Nothing, label = "Créer une fiche" }
                                    |> DSFR.Button.linkButton (Route.toUrl <| Route.CreateurNew)
                                    |> DSFR.Button.tertiaryNoOutline
                                    |> DSFR.Button.withAttrs [ class "!w-full" ]
                                    |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                                    |> DSFR.Button.view
                                , DSFR.Button.new
                                    { label =
                                        if selectedLength > 0 then
                                            "Qualifier la sélection"

                                        else if showingUnfilteredResults model.lastUrlFetched then
                                            "Qualifier les Créateurs ("
                                                ++ String.fromInt results
                                                ++ " fiche"
                                                ++ plural results
                                                ++ ")"

                                        else
                                            "Qualifier les résultats ("
                                                ++ String.fromInt results
                                                ++ " fiche"
                                                ++ plural results
                                                ++ ")"
                                    , onClick = Just <| ClickedQualifierEtablissementCreations
                                    }
                                    |> DSFR.Button.tertiaryNoOutline
                                    |> DSFR.Button.leftIcon DSFR.Icons.Design.editLine
                                    |> DSFR.Button.withDisabled (selectedLength == 0 && results == 0)
                                    |> DSFR.Button.withAttrs [ class "!w-full", Aria.controls [ "modal-" ++ modalId ] ]
                                    |> DSFR.Button.view
                                , let
                                    exportUrl =
                                        (Api.getEtablissementCreations <|
                                            model.lastUrlFetched
                                        )
                                            ++ "&format=xlsx"
                                  in
                                  DSFR.Button.new { onClick = Nothing, label = "Exporter (Excel)" }
                                    |> DSFR.Button.linkButtonExternal exportUrl
                                    |> DSFR.Button.tertiaryNoOutline
                                    |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                                    |> DSFR.Button.withDisabled (results == 0)
                                    |> DSFR.Button.withAttrs [ class "!w-full" ]
                                    |> DSFR.Button.view
                                ]
                            ]
                        ]
                    ]
                , div [ Grid.gridRow ] <|
                    [ div [ Grid.col12, Grid.colSm4 ]
                        [ filterPanel model.selectActivites model.selectDemandes model.selectZones model.selectMots model.selectRue model.selectQpvs activites demandes zones mots model.filters model.pagination model.lastUrlFetched ]
                    , div [ Grid.col12, Grid.colSm8 ]
                        [ div [ Grid.gridRow ] <|
                            List.singleton <|
                                Html.Lazy.lazy4 viewEtablissementCreationList model.selectedEntites model.pagination model.filters model.etablissementCreations
                        ]
                    ]
                ]


modalId : String
modalId =
    "batch-tag"


viewModal : Int -> List (EntityId EtablissementCreationId) -> Maybe BatchRequest -> Html Msg
viewModal foundEtablissementCreations selectedEntites batchRequest =
    let
        ( opened, content, title ) =
            case batchRequest of
                Nothing ->
                    ( False, nothing, nothing )

                Just { request, selectLocalisations, selectActivites, selectMots, activites, localisations, mots } ->
                    ( True
                    , let
                        noTags =
                            (List.length activites == 0)
                                && (List.length mots == 0)
                                && (List.length localisations == 0)

                        disabled =
                            request == RD.Loading
                      in
                      div []
                        [ div [ Grid.gridRow, Grid.gridRowGutters ]
                            [ div [ Grid.col12 ]
                                [ case request of
                                    RD.Success _ ->
                                        div [ class "flex flex-col gap-2" ]
                                            [ viewIf (List.length activites > 0) <|
                                                div []
                                                    [ div [ Typo.textBold ]
                                                        [ text "Activité(s) ajoutée(s)\u{00A0}:"
                                                        ]
                                                    , div []
                                                        [ text <| String.join ", " activites
                                                        ]
                                                    ]
                                            , viewIf (List.length localisations > 0) <|
                                                div []
                                                    [ div [ Typo.textBold ]
                                                        [ text "Localisation(s) ajoutée(s)\u{00A0}:"
                                                        ]
                                                    , div []
                                                        [ text <| String.join ", " localisations
                                                        ]
                                                    ]
                                            , viewIf (List.length mots > 0) <|
                                                div []
                                                    [ div [ Typo.textBold ]
                                                        [ text "Mot(s)-clé(s) ajouté(s)\u{00A0}:"
                                                        ]
                                                    , div []
                                                        [ text <| String.join ", " mots
                                                        ]
                                                    ]
                                            , div [ class "flex flex-col gap-4" ]
                                                [ div [ Typo.textBold ]
                                                    [ text "Résultat de la mise à jour\u{00A0}:"
                                                    ]
                                                , div [] <|
                                                    [ div [ class "fr-text-default--success" ] [ text "Les fiches ont été qualifiées avec succès\u{00A0}!" ]
                                                    ]
                                                ]
                                            ]

                                    _ ->
                                        div []
                                            [ div [ Grid.gridRow, Grid.gridRowGutters ]
                                                [ div [ Grid.col6 ]
                                                    [ div [ class "flex flex-col gap-4" ]
                                                        [ selectActivites
                                                            |> MultiSelectRemote.viewCustom
                                                                { isDisabled = False
                                                                , selected = activites
                                                                , optionLabelFn = identity
                                                                , optionDescriptionFn = \_ -> ""
                                                                , optionsContainerMaxHeight = 300
                                                                , selectTitle = span [ Typo.textBold ] [ text "Activités réelles et filières", sup [ Attr.title hintActivites ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                                                , viewSelectedOptionFn = text
                                                                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                                                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                                                , noResultsForMsg =
                                                                    \searchText ->
                                                                        "Aucune autre activité n'a été trouvée"
                                                                            ++ (if searchText == "" then
                                                                                    ""

                                                                                else
                                                                                    " pour " ++ searchText
                                                                               )
                                                                , noOptionsMsg = "Aucune activité n'a été trouvée"
                                                                , newOption = Just identity
                                                                , searchPrompt = "Rechercher une activité"
                                                                }
                                                        , activites
                                                            |> List.map (\activite -> DSFR.Tag.deletable UnselectBatchActivite { data = activite, toString = identity })
                                                            |> DSFR.Tag.medium
                                                        ]
                                                    ]
                                                , div [ Grid.col6 ]
                                                    [ div [ class "flex flex-col gap-4" ]
                                                        [ selectLocalisations
                                                            |> MultiSelectRemote.viewCustom
                                                                { isDisabled = False
                                                                , selected = localisations
                                                                , optionLabelFn = identity
                                                                , optionDescriptionFn = \_ -> ""
                                                                , optionsContainerMaxHeight = 300
                                                                , selectTitle = span [ Typo.textBold ] [ text "Zone géographique", sup [ Attr.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                                                , viewSelectedOptionFn = text
                                                                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                                                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                                                , noResultsForMsg =
                                                                    \searchText ->
                                                                        "Aucune autre zone géographique n'a été trouvée"
                                                                            ++ (if searchText == "" then
                                                                                    ""

                                                                                else
                                                                                    " pour " ++ searchText
                                                                               )
                                                                , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                                                                , newOption = Just identity
                                                                , searchPrompt = "Rechercher une zone géographique"
                                                                }
                                                        , localisations
                                                            |> List.map (\localisation -> DSFR.Tag.deletable UnselectBatchLocalisation { data = localisation, toString = identity })
                                                            |> DSFR.Tag.medium
                                                        ]
                                                    ]
                                                ]
                                            , div [ Grid.gridRow, Grid.gridRowGutters ]
                                                [ div [ Grid.col6 ]
                                                    [ div [ class "flex flex-col gap-4" ]
                                                        [ selectMots
                                                            |> MultiSelectRemote.viewCustom
                                                                { isDisabled = False
                                                                , selected = mots
                                                                , optionLabelFn = identity
                                                                , optionDescriptionFn = \_ -> ""
                                                                , optionsContainerMaxHeight = 300
                                                                , selectTitle = span [ Typo.textBold ] [ text "Mots-clés", sup [ Attr.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                                                , viewSelectedOptionFn = text
                                                                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                                                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                                                , noResultsForMsg =
                                                                    \searchText ->
                                                                        "Aucun autre mot-clé n'a été trouvé"
                                                                            ++ (if searchText == "" then
                                                                                    ""

                                                                                else
                                                                                    " pour " ++ searchText
                                                                               )
                                                                , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                                                                , newOption = Just identity
                                                                , searchPrompt = "Rechercher un mot-clé"
                                                                }
                                                        , mots
                                                            |> List.map (\mot -> DSFR.Tag.deletable UnselectBatchMot { data = mot, toString = identity })
                                                            |> DSFR.Tag.medium
                                                        ]
                                                    ]
                                                ]
                                            ]
                                ]
                            ]
                        , div [ Grid.gridRow, Grid.gridRowGutters ]
                            [ div [ Grid.col12 ]
                                [ (case request of
                                    RD.Success _ ->
                                        [ DSFR.Button.new
                                            { onClick = Just <| CanceledQualifierEtablissementCreations
                                            , label = "OK"
                                            }
                                        ]

                                    _ ->
                                        [ DSFR.Button.new
                                            { onClick = Just <| ConfirmedQualifierEtablissementCreations
                                            , label =
                                                if request == RD.Loading then
                                                    "Qualification en cours..."

                                                else
                                                    "Confirmer"
                                            }
                                            |> DSFR.Button.withDisabled (disabled || noTags)
                                        , DSFR.Button.new { onClick = Just <| CanceledQualifierEtablissementCreations, label = "Annuler" }
                                            |> DSFR.Button.withDisabled disabled
                                            |> DSFR.Button.secondary
                                        ]
                                  )
                                    |> DSFR.Button.group
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]
                            ]
                        ]
                    , text <|
                        case selectedEntites of
                            [] ->
                                "Qualifier "
                                    ++ String.fromInt foundEtablissementCreations
                                    ++ " fiche"
                                    ++ (if foundEtablissementCreations > 1 then
                                            "s"

                                        else
                                            ""
                                       )

                            _ ->
                                "Qualifier "
                                    ++ (String.fromInt <| List.length <| selectedEntites)
                                    ++ " fiche"
                                    ++ (if List.length selectedEntites > 1 then
                                            "s"

                                        else
                                            ""
                                       )
                    )
    in
    DSFR.Modal.view
        { id = modalId
        , label = modalId
        , openMsg = NoOp
        , closeMsg = Just <| CanceledQualifierEtablissementCreations
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


titleWithBadge : String -> Int -> List (Html msg)
titleWithBadge title selected =
    [ div [ class "flex flex-row gap-4" ]
        [ span [] [ text title ]
        , if selected > 0 then
            span [ class "circled" ]
                [ text <| String.fromInt <| selected
                ]

          else
            nothing
        ]
    ]


filterPanel : MultiSelect.SmartSelect Msg String -> MultiSelect.SmartSelect Msg TypeDemande -> MultiSelect.SmartSelect Msg String -> MultiSelect.SmartSelect Msg String -> SingleSelectRemote.SmartSelect Msg ApiStreet -> MultiSelectRemote.SmartSelect Msg Qpv -> List String -> List TypeDemande -> List String -> List String -> Filters -> Pagination -> String -> Html Msg
filterPanel selectActivites selectDemandes selectZones selectMots selectRue selectQpvs optionsActivites optionsDemandes optionsZones optionsMots ({ activites, demandes, zones, mots, rue } as filters) pagination lastUrlFetched =
    div [ class "p-2" ]
        [ div [ class "p-4 fr-card--white" ]
            [ DSFR.SearchBar.searchBar { errors = [], extraAttrs = [], disabled = False }
                { submitMsg = FetchEtablissementCreations
                , buttonLabel = "Rechercher"
                , inputMsg = UpdatedSearch
                , inputLabel = "Rechercher dans les créateurs d'entreprise"
                , inputPlaceholder = Nothing
                , inputId = rechercherEtablissementCreationInputName
                , inputValue = filters.recherche
                , hints =
                    "Recherche par nom, nom d'enseigne pressenti"
                        |> text
                        |> List.singleton
                , fullLabel =
                    Just <|
                        span
                            [ class "!mb-0"
                            , Typo.textBold
                            , Typo.textLg
                            ]
                            [ text "Rechercher dans les créateurs d'entreprise" ]
                }
            , text "\u{00A0}"
            , hr [ class "fr-hr" ] []
            , div [ class "flex flex-col" ]
                [ div [ class "flex flex-row items-center justify-between mb-4" ]
                    [ h2 [ Typo.fr_h6, class "!mb-0" ] [ text "Filtrer" ]
                    , Html.Lazy.lazy clearAllFiltersButton filters
                    ]
                , Html.Lazy.lazy suiviFilter filters.suivi
                , let
                    selected =
                        List.length activites + List.length zones + List.length mots
                  in
                  DSFR.Accordion.raw
                    { id = "filter-group-qualification"
                    , title = titleWithBadge "Étiquettes" selected
                    , content =
                        [ div [ class "flex flex-col gap-8" ]
                            [ Html.Lazy.lazy3 activitesFilter selectActivites optionsActivites activites
                            , Html.Lazy.lazy3 zonesFilter selectZones optionsZones zones
                            , Html.Lazy.lazy3 motsFilter selectMots optionsMots mots
                            ]
                        ]
                    , borderless = False
                    }
                , let
                    selected =
                        List.length demandes
                  in
                  DSFR.Accordion.raw
                    { id = "filter-group-demandes"
                    , title = titleWithBadge "Demandes en cours" selected
                    , content =
                        [ div [ class "flex flex-col gap-8" ]
                            [ Html.Lazy.lazy3 demandesFilter selectDemandes optionsDemandes demandes
                            ]
                        ]
                    , borderless = False
                    }
                , let
                    selected =
                        (rue |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                            + List.length filters.qpvs
                  in
                  DSFR.Accordion.raw
                    { id = "filter-group-localisation"
                    , title = titleWithBadge "Localisation" selected
                    , content =
                        [ div [ class "flex flex-col gap-8" ]
                            [ Html.Lazy.lazy2 rueFilter selectRue rue
                            , Html.Lazy.lazy2 qpvsFilter selectQpvs filters.qpvs
                            ]
                        ]
                    , borderless = False
                    }
                , DSFR.Button.new { label = "Appliquer les filtres", onClick = Just ClickedSearch }
                    |> DSFR.Button.withDisabled (lastUrlFetched == serializeModel ( filters, pagination ))
                    |> DSFR.Button.view
                    |> List.singleton
                    |> div [ class "flex flex-row justify-center mt-4" ]
                ]
            ]
        ]


rueFilter : SingleSelectRemote.SmartSelect Msg ApiStreet -> Maybe ApiStreet -> Html Msg
rueFilter selectRue selectedRue =
    div [ class "flex flex-col gap-2" ]
        [ selectRue
            |> SingleSelectRemote.viewCustom
                { isDisabled = False
                , selected = selectedRue
                , optionLabelFn = .label
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "Adresse" ]
                , searchPrompt = "Rechercher une adresse"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre adresse n'a été trouvée"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucune adresse n'a été trouvée"
                , error = Nothing
                }
        , selectedRue
            |> viewMaybe (\rue -> DSFR.Tag.deletable (\_ -> ClearRue) { data = rue.label, toString = identity } |> List.singleton |> DSFR.Tag.medium)
        ]


qpvsFilter : MultiSelectRemote.SmartSelect Msg Qpv -> List Qpv -> Html Msg
qpvsFilter selectQpvs qpvs =
    div [ class "flex flex-col gap-2" ]
        [ selectQpvs
            |> MultiSelectRemote.viewCustom
                { isDisabled = False
                , selected = qpvs
                , optionLabelFn = .nom
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "QPV - Quartier Prioritaire de la Politique de la Ville" ]
                , viewSelectedOptionFn = .nom >> text
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , noResultsForMsg =
                    \searchText ->
                        "Aucun autre QPV n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun QPV n'a été trouvé"
                , newOption = Nothing
                , searchPrompt = "Filtrer par QPV"
                }
        , qpvs
            |> List.map (\qpv -> DSFR.Tag.deletable UnselectQpv { data = qpv, toString = .nom })
            |> DSFR.Tag.medium
        ]


activitesFilter : MultiSelect.SmartSelect Msg String -> List String -> List String -> Html Msg
activitesFilter selectActivites optionsActivites activites =
    div [ class "flex flex-col gap-4" ]
        [ selectActivites
            |> MultiSelect.viewCustom
                { isDisabled = optionsActivites |> List.length |> (==) 0
                , selected = activites
                , options = optionsActivites |> List.sortBy String.toLower
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Activité réelle et filière"
                , viewSelectedOptionFn = text
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre activité n'a été trouvée"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucune activité n'a été trouvée"
                , searchFn = filterBy identity
                , searchPrompt = "Filtrer par activité"
                }
        , case activites of
            [] ->
                nothing

            _ ->
                activites
                    |> List.map (\activite -> DSFR.Tag.deletable UnselectActivite { data = activite, toString = identity })
                    |> DSFR.Tag.medium
        ]


zonesFilter : MultiSelect.SmartSelect Msg String -> List String -> List String -> Html Msg
zonesFilter selectZones optionsZones zones =
    div [ class "flex flex-col gap-4" ]
        [ selectZones
            |> MultiSelect.viewCustom
                { isDisabled = optionsZones |> List.length |> (==) 0
                , selected = zones
                , options = optionsZones |> List.sortBy String.toLower
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Zone géographique"
                , viewSelectedOptionFn = text
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre zone géographique n'a été trouvée"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                , searchFn = filterBy identity
                , searchPrompt = "Filtrer par zone géographique"
                }
        , case zones of
            [] ->
                nothing

            _ ->
                zones
                    |> List.map (\zone -> DSFR.Tag.deletable UnselectZone { data = zone, toString = identity })
                    |> DSFR.Tag.medium
        ]


motsFilter : MultiSelect.SmartSelect Msg String -> List String -> List String -> Html Msg
motsFilter selectMots optionsMots mots =
    div [ class "flex flex-col gap-4" ]
        [ selectMots
            |> MultiSelect.viewCustom
                { isDisabled = optionsMots |> List.length |> (==) 0
                , selected = mots
                , options = optionsMots |> List.sortBy String.toLower
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Mots-clés"
                , viewSelectedOptionFn = text
                , noResultsForMsg =
                    \searchText ->
                        "Aucun autre mot-clé n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                , searchFn = filterBy identity
                , searchPrompt = "Filtrer par mot-clé"
                }
        , case mots of
            [] ->
                nothing

            _ ->
                mots
                    |> List.map (\mot -> DSFR.Tag.deletable UnselectMot { data = mot, toString = identity })
                    |> DSFR.Tag.medium
        ]


demandesFilter : MultiSelect.SmartSelect Msg TypeDemande -> List TypeDemande -> List TypeDemande -> Html Msg
demandesFilter selectDemandes optionsDemandes demandes =
    div [ class "flex flex-col gap-4" ]
        [ selectDemandes
            |> MultiSelect.viewCustom
                { isDisabled = optionsDemandes |> List.length |> (==) 0
                , selected = demandes
                , options = optionsDemandes
                , optionLabelFn = Data.Demande.typeDemandeToDisplay
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Type de demandes en cours"
                , viewSelectedOptionFn = Data.Demande.typeDemandeToDisplay >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucun autre type de demande n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun type de demande n'a été trouvé"
                , searchFn = filterBy Data.Demande.typeDemandeToDisplay
                , searchPrompt = "Filtrer par type de demande"
                }
        , case demandes of
            [] ->
                nothing

            _ ->
                demandes
                    |> List.map (\demande -> DSFR.Tag.deletable UnselectDemande { data = demande, toString = Data.Demande.typeDemandeToDisplay })
                    |> DSFR.Tag.medium
        ]


suiviToLabel : Suivi -> Html msg
suiviToLabel suivi =
    case suivi of
        Tout ->
            span [ Typo.textBold ]
                [ text "Tous"
                ]

        EnCours ->
            span [ Typo.textBold ]
                [ text "Création en cours"
                ]

        Cree ->
            span [ Typo.textBold ]
                [ text "Anciens Créateurs accompagnés"
                ]

        Abandonnee ->
            span [ Typo.textBold ]
                [ text "Création abandonnée"
                ]


suiviToId : Suivi -> String
suiviToId suivi =
    case suivi of
        Tout ->
            "suivi-tous"

        EnCours ->
            "suivi-createurs"

        Cree ->
            "suivi-etablissements"

        Abandonnee ->
            "suivi-abandonnes"


suiviFilter : Suivi -> Html Msg
suiviFilter suivi =
    DSFR.Radio.group
        { id = "filtres-suivi"
        , options =
            [ Tout
            , EnCours
            , Cree
            , Abandonnee
            ]
        , current = Just suivi
        , toLabel = suiviToLabel
        , toId = suiviToId
        , msg = SetSuiviFilter
        , legend = Just <| div [ Typo.textBold ] [ text "Périmètre de recherche" ]
        }
        |> DSFR.Radio.view


clearAllFiltersButton : Filters -> Html Msg
clearAllFiltersButton filters =
    DSFR.Button.new
        { label = "Tout effacer"
        , onClick = Just <| ClearAllFilters
        }
        |> DSFR.Button.withAttrs [ class "!p-0" ]
        |> DSFR.Button.withDisabled ({ filters | recherche = "" } == { defaultFilters | recherche = "" })
        |> DSFR.Button.tertiaryNoOutline
        |> DSFR.Button.view


viewEtablissementCreationList : List (EntityId EtablissementCreationId) -> Pagination -> Filters -> RD.WebData (List EtablissementCreation) -> Html Msg
viewEtablissementCreationList selectedEntites pagination filters etablissementCreations =
    case etablissementCreations of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            div [ Grid.col12 ]
                [ []
                    |> viewSelection filters selectedEntites
                , div [ Grid.gridRow ] <|
                    List.repeat 3 <|
                        cardPlaceholder
                ]

        RD.Failure _ ->
            div [ class "text-center", Grid.col, class "p-2" ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Une erreur s'est produite, veuillez recharger la page."
                    ]
                ]

        RD.Success [] ->
            div [ Grid.col12 ]
                [ []
                    |> viewSelection filters selectedEntites
                , div [ class "p-2" ]
                    [ div [ class "fr-card--white h-[250px] text-center p-20" ]
                        [ text "Aucun Créateur d'entreprise ne correspond à cette recherche."
                        , text " "
                        , viewMaybe (\_ -> text "Moteur de recherche par adresse en cours de perfectionnement\u{00A0}: certaines adresses peuvent ne pas être trouvées.") filters.rue
                        ]
                    ]
                ]

        RD.Success fs ->
            div [ Grid.col12 ]
                [ fs
                    |> viewSelection filters selectedEntites
                , fs
                    |> List.map (etablissementCreationCard selectedEntites)
                    |> div [ Grid.gridRow ]
                , if pagination.pages > 1 then
                    div [ class "!mt-4" ]
                        [ DSFR.Pagination.view pagination.page pagination.pages <|
                            toHref ( filters, pagination )
                        ]

                  else
                    nothing
                ]


{-| This tells us that, even if the url is not "clean",
we are showing unfiltered results, though they may be ordered.
-}
showingUnfilteredResults : String -> Bool
showingUnfilteredResults currentUrl =
    List.member currentUrl
        [ ""
        , queryKeys.tri ++ "=" ++ orderDirectionToString Asc
        , queryKeys.direction ++ "=" ++ orderByToString EtablissementCreationConsultation
        , queryKeys.direction ++ "=" ++ orderByToString EtablissementCreationCreation
        , queryKeys.direction ++ "=" ++ orderByToString EtablissementCreationAlphabetique
        , queryKeys.tri ++ "=" ++ orderDirectionToString Asc ++ "&" ++ queryKeys.direction ++ "=" ++ orderByToString EtablissementCreationConsultation
        , queryKeys.tri ++ "=" ++ orderDirectionToString Asc ++ "&" ++ queryKeys.direction ++ "=" ++ orderByToString EtablissementCreationCreation
        , queryKeys.tri ++ "=" ++ orderDirectionToString Asc ++ "&" ++ queryKeys.direction ++ "=" ++ orderByToString EtablissementCreationAlphabetique
        ]


codeToOrderBy : String -> Msg
codeToOrderBy code =
    let
        match ob od () =
            if code == (orderByToString ob ++ orderDirectionToString od) then
                Just ( ob, od )

            else
                Nothing

        or : (() -> Maybe data) -> Maybe data -> Maybe data
        or orElse maybe =
            case maybe of
                Just m ->
                    Just m

                Nothing ->
                    orElse ()
    in
    match EtablissementCreationConsultation Desc ()
        |> or (match EtablissementCreationConsultation Asc)
        |> or (match EtablissementCreationCreation Desc)
        |> or (match EtablissementCreationCreation Asc)
        |> or (match EtablissementCreationAlphabetique Desc)
        |> or (match EtablissementCreationAlphabetique Asc)
        |> Maybe.withDefault ( EtablissementCreationConsultation, Desc )
        |> (\( ob, od ) -> ClickedOrderBy ob od)


viewSelection : Filters -> List (EntityId EtablissementCreationId) -> List EtablissementCreation -> Html Msg
viewSelection { orderBy, orderDirection } selectedEntites etablissementCreationsOnPage =
    let
        entiteIdsOfEtablissementCreationsOnPage =
            etablissementCreationsOnPage
                |> List.map .id

        inSelected id =
            selectedEntites
                |> List.member id
    in
    div [ class "flex flex-col p-2 gap-2" ]
        [ DSFR.Checkbox.single
            { value = "toggle-page"
            , checked =
                if List.length entiteIdsOfEtablissementCreationsOnPage == 0 then
                    Just False

                else if List.all inSelected entiteIdsOfEtablissementCreationsOnPage then
                    Just True

                else if List.all (inSelected >> not) entiteIdsOfEtablissementCreationsOnPage then
                    Just False

                else
                    Nothing
            , valueAsString = identity
            , id = "etablissementCreation-page-selection"
            , label = text "Sélectionner les fiches de cette page"
            , onChecked = \_ bool -> ToggleSelection entiteIdsOfEtablissementCreationsOnPage bool
            }
            |> DSFR.Checkbox.singleWithDisabled (List.length entiteIdsOfEtablissementCreationsOnPage == 0)
            |> DSFR.Checkbox.viewSingle
            |> List.singleton
            |> div [ class "flex shrink" ]
        , case selectedEntites of
            [] ->
                nothing

            _ ->
                let
                    length =
                        List.length selectedEntites
                in
                div [ class "fr-notice--info flex flex-row justify-between items-center gap-2 p-2" ] <|
                    [ p [ class "!mb-0" ]
                        [ text "Il y a "
                        , span [ Typo.textBold, class "blue-text" ]
                            [ text <| String.fromInt <| length
                            , text " fiche"
                            , viewIf (length > 1) (text "s")
                            ]
                        , text " dans la sélection"
                        ]
                    , DSFR.Button.new
                        { label = "Vider la sélection"
                        , onClick = Just <| EmptySelection
                        }
                        |> DSFR.Button.withDisabled (List.length selectedEntites == 0)
                        |> DSFR.Button.secondary
                        |> DSFR.Button.withAttrs [ class "!mb-0" ]
                        |> DSFR.Button.view
                    ]
        , let
            selectId =
                "select-tri"
          in
          div [ class "flex flex-row justify-end items-baseline gap-4" ]
            [ label
                [ class "fr-label"
                , Attr.for selectId
                ]
                [ text "Tri" ]
            , div
                [ class "fr-select-group"
                ]
                [ let
                    toOption ob od lab =
                        option
                            [ Attr.value <| orderByToString ob ++ orderDirectionToString od
                            , Attr.selected <| ( orderBy, orderDirection ) == ( ob, od )
                            ]
                            [ text lab ]
                  in
                  select
                    [ class "fr-select"
                    , Attr.id selectId
                    , Attr.name "select"
                    , Events.onInput codeToOrderBy
                    ]
                    [ toOption EtablissementCreationConsultation Desc "Créateurs récemment consultés"
                    , toOption EtablissementCreationCreation Desc "Créateurs récemment créés"
                    , toOption EtablissementCreationAlphabetique Asc "Noms classés par ordre alphabétique"
                    , toOption EtablissementCreationAlphabetique Desc "Noms classés par ordre alphabétique inversé"
                    ]
                ]
            ]
        ]


etablissementCreationCard : List (EntityId EtablissementCreationId) -> EtablissementCreation -> Html Msg
etablissementCreationCard selectedEntites etablissementCreation =
    let
        selected =
            List.member (.id etablissementCreation) selectedEntites

        ( { personne }, _ ) =
            etablissementCreation.createurs
    in
    div [ Grid.col12, Grid.colSm6, class "p-2" ]
        [ div [ Typo.textSm, class "!mb-0" ]
            [ div
                [ class "flex flex-col overflow-y-auto fr-card--white p-2 gap-2 min-h-[18rem] border-2 createur-card"
                , Attr.style "background-image" "none"
                , classList
                    [ ( "dark-blue-border", selected )
                    , ( "border-transparent", not selected )
                    ]
                ]
                [ div [ class "flex flex-row justify-between relative p-4" ]
                    [ div [ class "absolute top-[12px] right-0 createur-card-checkbox" ]
                        [ DSFR.Checkbox.single
                            { value = etablissementCreation
                            , checked = Just <| selected
                            , valueAsString = .id >> Api.EntityId.entityIdToString
                            , id = "etablissementCreation-selection-" ++ (etablissementCreation |> .id |> Api.EntityId.entityIdToString)
                            , label = text "Ajouter à la sélection"
                            , onChecked = .id >> List.singleton >> ToggleSelection
                            }
                            |> DSFR.Checkbox.singleWithHiddenLabel
                            |> DSFR.Checkbox.viewSingle
                        ]
                    ]
                , a
                    [ class "flex flex-col grow gap-2 custom-hover p-2"
                    , Attr.style "background-image" "none"
                    , Attr.href <|
                        Route.toUrl <|
                            Route.Createur <|
                                ( Nothing, unsafeConvert <| .id etablissementCreation )
                    ]
                    [ UI.EtablissementCreation.badgeCreationAbandonnee etablissementCreation.creationAbandonnee
                    , div
                        [ Typo.textBold
                        , class "!mb-0"
                        , class "line-clamp-1"
                        ]
                      <|
                        List.singleton <|
                            text <|
                                .nom <|
                                    etablissementCreation
                    , personne
                        |> .email
                        |> withEmptyAs "-"
                        |> Lib.UI.infoLine (Just 1) "Email"
                        |> List.singleton
                        |> div [ class "!mb-0" ]
                    , etablissementCreation
                        |> .futureEnseigne
                        |> withEmptyAs "-"
                        |> Lib.UI.infoLine (Just 1) "Nom d'enseigne pressenti"
                        |> List.singleton
                        |> div [ class "!mb-0" ]
                    , personne
                        |> .telephone
                        |> withEmptyAs "-"
                        |> Lib.UI.infoLine (Just 1) "Téléphone"
                        |> List.singleton
                        |> div [ class "!mb-0" ]
                    , let
                        activites =
                            .activitesReelles <|
                                etablissementCreation
                      in
                      div [ class "!mb-0" ] <|
                        [ Lib.UI.infoLine (Just 1)
                            ("Activité"
                                ++ (if List.length activites > 1 then
                                        "s"

                                    else
                                        ""
                                   )
                                ++ " réelle"
                                ++ (if List.length activites > 1 then
                                        "s"

                                    else
                                        ""
                                   )
                            )
                          <|
                            case activites of
                                [] ->
                                    "-"

                                act ->
                                    String.join "\u{00A0}— " <|
                                        act
                        ]
                    , let
                        demandes =
                            etablissementCreation.demandes |> List.filter (Data.Demande.cloture >> not)
                      in
                      div [ class "!mb-0" ] <|
                        [ Lib.UI.infoLine (Just 1)
                            ("Demande"
                                ++ (if List.length demandes > 1 then
                                        "s"

                                    else
                                        ""
                                   )
                                ++ " en cours"
                            )
                          <|
                            case demandes of
                                [] ->
                                    "-"

                                ds ->
                                    String.join "\u{00A0}— " <|
                                        List.map Data.Demande.label <|
                                            ds
                        ]
                    ]
                ]
            ]
        ]


cardPlaceholder : Html Msg
cardPlaceholder =
    div [ Grid.col12, Grid.colSm6 ]
        [ div [ class "p-2", Typo.textSm ]
            [ div
                [ class "fr-card--white flex flex-col p-4 custom-hover overflow-y-auto gap-2"
                , Attr.style "background-image" "none"
                ]
                [ div [ class "flex flex-row justify-between" ]
                    [ greyBadge 30
                    ]
                , div [ class "!mb-0" ]
                    [ greyPlaceholder 14
                    , greyPlaceholder 20
                    ]
                , div [ class "!mb-0" ]
                    [ greyPlaceholder 15
                    , greyPlaceholder 8
                    ]
                , div [ class "!mb-0" ]
                    [ greyPlaceholder 15
                    , greyPlaceholder 8
                    ]
                ]
            ]
        ]


greyPlaceholder : Int -> Html msg
greyPlaceholder length =
    "\u{00A0}"
        |> List.repeat length
        |> List.map (text >> List.singleton >> span [ class "w-[1rem] inline-block" ])
        |> span [ class "grey-background m-1" ]
        |> List.singleton
        |> div [ class "pulse-black" ]


greyBadge : Int -> Html msg
greyBadge length =
    "\u{00A0}"
        |> String.repeat length
        |> (\t -> { data = t, toString = identity })
        |> DSFR.Tag.unclickable
        |> List.singleton
        |> DSFR.Tag.medium
        |> List.singleton
        |> div [ class "pulse-black" ]


selectQpvsConfig : SelectConfig Qpv
selectQpvsConfig =
    { headers = []
    , url = Api.rechercheQpvGeo (Filters.situationGeographiqueToCode Filters.SurMonTerritoire)
    , optionDecoder =
        Decode.field "qpvs" <|
            Decode.list <|
                Decode.map2 Qpv
                    (Decode.field "id" Decode.string)
                    (Decode.field "nom" Decode.string)
    }
