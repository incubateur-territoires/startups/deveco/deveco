module Pages.AnalyseTerritoriale exposing (Model, Msg, page)

import Accessibility exposing (Html, div, h1, h2, span, text)
import DSFR.Alert
import DSFR.Grid
import DSFR.Typography as Typo
import Effect
import Html.Attributes exposing (class)
import Html.Lazy
import Lib.UI exposing (filterBy)
import Pages.Etablissements exposing (getTDBStats)
import Pages.Etablissements.Filters exposing (RechercheEnregistree)
import Pages.Etablissements.Stats exposing (TDBStats, viewStats)
import RemoteData as RD exposing (WebData)
import Route
import Shared
import SingleSelect
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


type alias Model =
    { recherchesEnregistrees : WebData (List RechercheEnregistree)
    , left : StatsRequest
    , right : StatsRequest
    }


type alias StatsRequest =
    { selectRechercheEnregistree : SingleSelect.SmartSelect Msg (Maybe RechercheEnregistree)
    , rechercheEnregistree : Maybe RechercheEnregistree
    , stats : WebData TDBStats
    }


type Column
    = Left
    | Right


type Msg
    = SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | ReceivedStats Column (WebData TDBStats)
    | ReceivedRecherchesEnregistrees (WebData (List RechercheEnregistree))
    | SelectedRechercheEnregistree Column ( Maybe RechercheEnregistree, SingleSelect.Msg (Maybe RechercheEnregistree) )
    | UpdatedSelectRechercheEnregistre Column (SingleSelect.Msg (Maybe RechercheEnregistree))


page : Shared.Shared -> Shared.User -> Page () Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.element
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init _ =
    let
        left =
            { rechercheEnregistree = Nothing
            , selectRechercheEnregistree =
                SingleSelect.init "select-recherche-left"
                    { selectionMsg = SelectedRechercheEnregistree Left
                    , internalMsg = UpdatedSelectRechercheEnregistre Left
                    }
            , stats = RD.NotAsked
            }

        right =
            { rechercheEnregistree = Nothing
            , selectRechercheEnregistree =
                SingleSelect.init "select-recherche-right"
                    { selectionMsg = SelectedRechercheEnregistree Right
                    , internalMsg = UpdatedSelectRechercheEnregistre Right
                    }
            , stats = RD.NotAsked
            }
    in
    ( { recherchesEnregistrees = RD.Loading
      , left = left
      , right = right
      }
    , Effect.fromCmd <|
        Pages.Etablissements.Filters.getRecherchesEnregistrees
            { sharedMsg = SharedMsg
            , logError = Just LogError
            , msg = ReceivedRecherchesEnregistrees
            }
    )
        |> Shared.pageChangeEffects


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        ReceivedStats column response ->
            updateStats column response model
                |> Effect.withNone

        ReceivedRecherchesEnregistrees resp ->
            { model | recherchesEnregistrees = resp }
                |> Effect.withNone

        SelectedRechercheEnregistree column ( rechercheEnregistree, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg <| getSelect column model

                statsCmd =
                    Maybe.withDefault Cmd.none <|
                        Maybe.map
                            (getTDBStats
                                { sharedMsg = SharedMsg
                                , logError = Just LogError
                                , msg = ReceivedStats column
                                }
                                0
                            )
                        <|
                            Maybe.map .url <|
                                rechercheEnregistree

                statsRequest =
                    getStatsRequest column model

                newStatsRequest =
                    { statsRequest
                        | selectRechercheEnregistree = updatedSelect
                        , rechercheEnregistree = rechercheEnregistree
                        , stats =
                            rechercheEnregistree
                                |> Maybe.map (\_ -> RD.Loading)
                                |> Maybe.withDefault RD.NotAsked
                    }
            in
            ( updateStatsRequest column model newStatsRequest
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.fromCmd statsCmd
                ]
            )

        UpdatedSelectRechercheEnregistre column sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg <| getSelect column model

                statsRequest =
                    getStatsRequest column model

                newStatsRequest =
                    { statsRequest
                        | selectRechercheEnregistree = updatedSelect
                    }
            in
            ( updateStatsRequest column model newStatsRequest
            , Effect.fromCmd selectCmd
            )


view : Model -> View Msg
view model =
    { title =
        UI.Layout.pageTitle <|
            "Statistiques d'utilisation - Deveco"
    , body = UI.Layout.lazyBody body model
    , route = Route.Analysis
    }


body : Model -> Html Msg
body { recherchesEnregistrees, left, right } =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12, class "flex flex-col gap-4" ]
            [ h1 [ Typo.fr_h4, class "!mb-0" ]
                [ text "Analyse territoriale" ]
            ]
        , div [ DSFR.Grid.col12 ]
            [ DSFR.Alert.small
                { title = Nothing
                , description =
                    span [ Typo.textSm ]
                        [ text "Enregistrez vos recherches dans l'onglet Établissements afin de les comparer ci-dessous"
                        ]
                }
                |> DSFR.Alert.alert Nothing DSFR.Alert.info
            ]
        , div [ DSFR.Grid.col12 ]
            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col6 ]
                    [ Html.Lazy.lazy3
                        viewFilters
                        (RD.withDefault [] recherchesEnregistrees)
                        Left
                        left
                    ]
                , div [ DSFR.Grid.col6 ]
                    [ Html.Lazy.lazy3
                        viewFilters
                        (RD.withDefault [] recherchesEnregistrees)
                        Right
                        right
                    ]
                ]
            ]
        , div [ DSFR.Grid.col6 ]
            [ Html.Lazy.lazy2 viewStats True left.stats
            ]
        , div [ DSFR.Grid.col6 ]
            [ Html.Lazy.lazy2 viewStats True right.stats
            ]
        ]


viewFilters : List RechercheEnregistree -> Column -> StatsRequest -> Html Msg
viewFilters recherchesEnregistrees column statsRequest =
    div [ class "flex flex-row fr-card--white p-2 sm:p-4 w-full" ]
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "w-full" ]
            [ div [ DSFR.Grid.col12 ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ]
                    [ text <|
                        (++) "Sélection " <|
                            case column of
                                Left ->
                                    "1"

                                Right ->
                                    "2"
                    ]
                ]
            , div [ DSFR.Grid.col12 ]
                [ div [ class "flex flex-col gap-2" ]
                    [ statsRequest.selectRechercheEnregistree
                        |> SingleSelect.viewCustom
                            { isDisabled = False
                            , selected = Just statsRequest.rechercheEnregistree
                            , options =
                                recherchesEnregistrees
                                    |> List.sortBy (.nom >> String.toLower)
                                    |> List.map Just
                                    |> (::) Nothing
                            , optionLabelFn =
                                Maybe.map .nom
                                    >> Maybe.withDefault "Aucune"
                            , optionDescriptionFn = \_ -> ""
                            , optionsContainerMaxHeight = 300
                            , selectTitle = text "Recherches enregistrées"
                            , noResultsForMsg =
                                \searchText ->
                                    "Aucune autre valeur n'a été trouvée"
                                        ++ (if searchText == "" then
                                                ""

                                            else
                                                " pour " ++ searchText
                                           )
                            , noOptionsMsg = "Aucune valeur n'a été trouvée"
                            , searchFn = filterBy <| (Maybe.map .nom >> Maybe.withDefault "")
                            , searchPrompt = "Choisir une recherche enregistrée"
                            }
                    ]
                ]
            ]
        ]


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ model
            |> .left
            |> .selectRechercheEnregistree
            |> SingleSelect.subscriptions
        , model
            |> .right
            |> .selectRechercheEnregistree
            |> SingleSelect.subscriptions
        ]


updateStatsRequest : Column -> Model -> StatsRequest -> Model
updateStatsRequest column model newStatsRequest =
    case column of
        Left ->
            { model | left = newStatsRequest }

        Right ->
            { model | right = newStatsRequest }


getStatsRequest : Column -> Model -> StatsRequest
getStatsRequest column model =
    case column of
        Left ->
            model.left

        Right ->
            model.right


getSelect : Column -> Model -> SingleSelect.SmartSelect Msg (Maybe RechercheEnregistree)
getSelect column model =
    getStatsRequest column model
        |> .selectRechercheEnregistree


updateStats : Column -> WebData TDBStats -> Model -> Model
updateStats column response model =
    updateStatsRequest column model <|
        (\statsRequest -> { statsRequest | stats = response }) <|
            getStatsRequest column model
