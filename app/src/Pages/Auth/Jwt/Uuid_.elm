module Pages.Auth.Jwt.Uuid_ exposing (Model, Msg, page)

import Accessibility exposing (Html, div, p, text)
import Api
import DSFR.Icons
import DSFR.Icons.System
import DSFR.Typography as Typo
import Effect
import Html.Attributes exposing (class)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode
import Process
import RemoteData as RD exposing (WebData)
import Route
import Shared
import Spa.Page exposing (Page, onNewFlags)
import Task
import UI.Layout
import User exposing (User)
import View exposing (View)


page : Shared.Shared -> Page ( String, Maybe String ) Shared.Msg (View Msg) Model Msg
page shared =
    Spa.Page.element
        { init = init
        , update = update
        , view = view shared
        , subscriptions = subscriptions
        }
        |> onNewFlags (\_ -> NoOp)



-- INIT


type alias Model =
    { authenticationRequest : WebData AuthenticatedUser
    , uuid : String
    , redirectUrl : Maybe String
    }


init : ( String, Maybe String ) -> ( Model, Effect.Effect Shared.Msg Msg )
init ( uuid, redirectUrl ) =
    ( { authenticationRequest = RD.Loading, uuid = uuid, redirectUrl = redirectUrl }
    , Effect.fromCmd <|
        Cmd.batch <|
            [ authenticateJwt uuid redirectUrl
            , timeout
            ]
    )
        |> Shared.pageChangeEffects


timeout : Cmd Msg
timeout =
    Process.sleep 5000
        |> Task.perform
            (\_ ->
                RD.Failure Http.Timeout
                    |> ReceivedAuthentication
            )


authenticateJwt : String -> Maybe String -> Cmd Msg
authenticateJwt uuid redirectUrl =
    Http.post
        { url = Api.jwt uuid
        , body =
            redirectUrl
                |> Maybe.map
                    (\url ->
                        [ ( "redirectUrl", Encode.string url )
                        ]
                            |> Encode.object
                            |> Http.jsonBody
                    )
                |> Maybe.withDefault Http.emptyBody
        , expect =
            Http.expectJson (RD.fromResult >> ReceivedAuthentication) <|
                decodeAuthenticatedUser
        }


decodeAuthenticatedUser : Decoder AuthenticatedUser
decodeAuthenticatedUser =
    Decode.succeed AuthenticatedUser
        |> andMap User.decodeUser
        |> andMap (optionalNullableField "redirectUrl" Decode.string)



-- UPDATE


type Msg
    = ReceivedAuthentication (WebData AuthenticatedUser)
    | NoOp


type alias AuthenticatedUser =
    { user : User
    , redirectUrl : Maybe String
    }


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Effect.none )

        ReceivedAuthentication (RD.Success { user, redirectUrl }) ->
            ( model
            , Effect.fromShared <|
                Shared.LoggedIn user
                    (Just <|
                        (redirectUrl
                            |> Maybe.withDefault
                                (Route.toUrl <|
                                    Route.defaultPageForRole model.redirectUrl <|
                                        User.role user
                                )
                        )
                    )
            )

        ReceivedAuthentication response ->
            ( { model | authenticationRequest = response }, Effect.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Shared.Shared -> Model -> View Msg
view _ model =
    { title = UI.Layout.pageTitle <| "Validation de l'authentification"
    , body = UI.Layout.lazyBody body model
    , route = Route.AuthJwtUuid model.uuid model.redirectUrl
    }


body : Model -> Html Msg
body { authenticationRequest } =
    div [ class "flex flex-col p-2" ]
        [ div [ class "p-6 sm:p-8 fr-card--white" ] <|
            case authenticationRequest of
                RD.Failure _ ->
                    [ p [] [ text "Le jeton utilisé n'a pas pu être validé. Il a peut-être déjà été utilisé." ]
                    , p []
                        [ text "Veuillez vous rendre sur "
                        , Typo.link (Route.toUrl <| Route.Connexion Nothing) [] [ text "la page de connexion" ]
                        , text " pour demander un nouveau jeton."
                        ]
                    ]

                _ ->
                    [ DSFR.Icons.System.refreshFill |> DSFR.Icons.iconLG
                    , text "Validation du jeton en cours..."
                    ]
        ]
