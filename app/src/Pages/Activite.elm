module Pages.Activite exposing (Model, Msg, page)

import Accessibility exposing (Html, div, h1, h3, span, text)
import Api
import Api.Auth exposing (get)
import DSFR.Button
import DSFR.Grid
import DSFR.Input
import DSFR.Tabs
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Demande
import Data.Echange
import Date exposing (Date)
import Effect
import Html
import Html.Attributes exposing (class)
import Html.Events as Events
import Html.Extra exposing (viewIf)
import Html.Keyed
import Html.Lazy
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import Lib.Date exposing (formatDateToYYYYMMDD)
import Lib.UI exposing (numberCard, plural)
import List.Extra
import QS
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import Time
import UI.Echange
import UI.Entite
import UI.Layout
import View exposing (View)


type alias Model =
    { etablissementsStats : WebData EtablissementsStatsData
    , createursStats : WebData CreateursStatsData
    , locauxStats : WebData LocauxStatsData
    , currentTab : StatsTab
    , debut : Date
    , fin : Date
    , compteur : Int
    , plage : Maybe Plage
    , dernieresDatesEtablissements : Maybe ( Date, Date )
    , dernieresDatesCreateurs : Maybe ( Date, Date )
    , dernieresDatesLocaux : Maybe ( Date, Date )
    , derniereUrl : String
    }


type StatsTab
    = StatsEtablissements
    | StatsCreateurs
    | StatsLocaux


statsTabToString : StatsTab -> String
statsTabToString statsTab =
    case statsTab of
        StatsEtablissements ->
            "etablissements"

        StatsCreateurs ->
            "createurs"

        StatsLocaux ->
            "locaux"


statsTabToDisplay : StatsTab -> String
statsTabToDisplay statsTab =
    case statsTab of
        StatsEtablissements ->
            "Établissements"

        StatsCreateurs ->
            "Créateurs d'entreprise"

        StatsLocaux ->
            "Locaux"


stringToStatsTab : String -> Maybe StatsTab
stringToStatsTab s =
    case s of
        "etablissements" ->
            Just StatsEtablissements

        "createurs" ->
            Just StatsCreateurs

        "locaux" ->
            Just StatsLocaux

        _ ->
            Nothing


type alias EtablissementsStatsData =
    { etablissements : EtablissementsStats
    , rappels : RappelsStats
    , echanges : List EchangesStats
    , demandes : DemandesStats
    , actions : ActionsStats
    }


type alias CreateursStatsData =
    { createurs : CreateursStats
    , rappels : RappelsStats
    , echanges : List EchangesStats
    , demandes : DemandesStats
    , actions : ActionsStats
    }


type alias LocauxStatsData =
    { locaux : LocauxStats
    , rappels : RappelsStats
    , echanges : List EchangesStats
    , demandes : DemandesStats
    , actions : ActionsStats
    }


type alias EtablissementsStats =
    { nouveaux : Int
    , qpvs : Int
    , horsTerritoire : Int
    }


type alias CreateursStats =
    { nouveaux : Int
    , qpvs : Int
    , transformes : Int
    }


type alias LocauxStats =
    { occupes : Int
    , vacants : Int
    , contacts : Int
    }


type alias RappelsStats =
    { nouveaux : Int
    , expires : Int
    , clotures : Int
    }


type alias EchangesStats =
    { auteur : String
    , type_ : String
    }


type alias DemandesStats =
    { nouvelles : Int
    , ouvertes : List String
    , cloturees : Int
    }


type alias ActionsStats =
    { vues : Int
    , qualifications : Int
    , etiquetages : Int
    }


type Msg
    = SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | ReceivedEtablissementsStats (WebData EtablissementsStatsData)
    | ReceivedCreateursStats (WebData CreateursStatsData)
    | ReceivedLocauxStats (WebData LocauxStatsData)
    | ClickedChangeTab StatsTab
    | ChangeDate Borne Date
    | ChangeDatePreset Plage
    | GetStats ( StatsTab, Maybe Plage, ( Date, Date ) )
    | ClickedSearch


type Borne
    = Debut
    | Fin


type Plage
    = Semaine
    | Mois
    | Annee


page : Shared.Shared -> User -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page { now, timezone } user =
    Spa.Page.onNewFlags (queryToTabPlageDebutFin now timezone >> GetStats) <|
        Spa.Page.element
            { view = view now timezone user
            , init = init now timezone user
            , update = update now timezone
            , subscriptions = \_ -> Sub.none
            }


parseQuery : String -> ( StatsTab, Maybe Plage, Maybe ( Date, Date ) )
parseQuery query =
    let
        parsed =
            QS.parse QS.config query

        fin =
            case QS.getAsStringList "fin" parsed of
                f :: _ ->
                    f
                        |> Date.fromIsoString
                        |> Result.toMaybe

                _ ->
                    Nothing

        debut =
            case QS.getAsStringList "debut" parsed of
                d :: _ ->
                    d
                        |> Date.fromIsoString
                        |> Result.toMaybe

                _ ->
                    Nothing

        dates =
            Maybe.map2 Tuple.pair debut fin

        plage =
            case QS.getAsStringList "plage" parsed of
                p :: _ ->
                    stringToPlage p

                _ ->
                    Nothing

        onglet =
            case QS.getAsStringList "onglet" parsed of
                "createurs" :: _ ->
                    StatsCreateurs

                "locaux" :: _ ->
                    StatsLocaux

                _ ->
                    StatsEtablissements
    in
    ( onglet, plage, dates )


serializeQuery : { model | currentTab : StatsTab, debut : Date, fin : Date, plage : Maybe Plage } -> String
serializeQuery { currentTab, debut, fin, plage } =
    let
        setTab =
            case currentTab of
                StatsEtablissements ->
                    identity

                StatsCreateurs ->
                    QS.setStr "onglet" "createurs"

                StatsLocaux ->
                    QS.setStr "onglet" "locaux"

        setDates =
            case plage of
                Nothing ->
                    QS.setStr "debut" (Date.toIsoString debut)
                        >> QS.setStr "fin" (Date.toIsoString fin)

                Just Semaine ->
                    identity

                Just p ->
                    QS.setStr "plage" <|
                        plageToString <|
                            p
    in
    QS.empty
        |> setTab
        |> setDates
        |> QS.serialize (QS.config |> QS.addQuestionMark False)


datesFromPlage : Time.Posix -> Time.Zone -> Plage -> ( Date, Date )
datesFromPlage now timezone plage =
    let
        today =
            Date.fromPosix timezone now
    in
    case plage of
        Semaine ->
            let
                d =
                    Date.add Date.Days -7 today
            in
            ( d, today )

        Mois ->
            let
                d =
                    Date.add Date.Days -30 today
            in
            ( d, today )

        Annee ->
            let
                d =
                    Date.floor Date.Year today
            in
            ( d, today )


queryToTabPlageDebutFin : Time.Posix -> Time.Zone -> Maybe String -> ( StatsTab, Maybe Plage, ( Date, Date ) )
queryToTabPlageDebutFin now timezone query =
    let
        ( currentTab, maybePlage, maybeDates ) =
            parseQuery <| Maybe.withDefault "" query

        ( plage, ( debut, fin ) ) =
            case ( maybePlage, maybeDates ) of
                ( Nothing, Nothing ) ->
                    ( Just Semaine, datesFromPlage now timezone Semaine )

                ( Just p, _ ) ->
                    ( Just p, datesFromPlage now timezone p )

                ( Nothing, Just ( d, f ) ) ->
                    ( Nothing, ( d, f ) )
    in
    ( currentTab, plage, ( debut, fin ) )


init : Time.Posix -> Time.Zone -> User -> Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init now timezone user query =
    let
        ( currentTab, plage, ( debut, fin ) ) =
            queryToTabPlageDebutFin now timezone query

        nextUrl =
            serializeQuery
                { currentTab = currentTab
                , debut = debut
                , fin = fin
                , plage = plage
                }
    in
    ( { etablissementsStats = RD.NotAsked
      , createursStats = RD.NotAsked
      , locauxStats = RD.NotAsked
      , currentTab = currentTab
      , debut = debut
      , fin = fin
      , compteur = 0
      , plage = plage
      , dernieresDatesEtablissements = Nothing
      , dernieresDatesCreateurs = Nothing
      , dernieresDatesLocaux = Nothing
      , derniereUrl = nextUrl
      }
    , Shared.autoriseAccesDeveco user Nothing <|
        Effect.fromShared <|
            Shared.ReplaceUrl <|
                Route.Activite <|
                    (\q ->
                        if q == "" then
                            Nothing

                        else
                            Just q
                    )
                    <|
                        nextUrl
    )
        |> Shared.pageChangeEffects


update : Time.Posix -> Time.Zone -> Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update now timezone msg model =
    (case msg of
        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        ReceivedEtablissementsStats response ->
            { model | etablissementsStats = response }
                |> Effect.withNone

        ReceivedCreateursStats response ->
            { model | createursStats = response }
                |> Effect.withNone

        ReceivedLocauxStats response ->
            { model | locauxStats = response }
                |> Effect.withNone

        ClickedChangeTab statsTab ->
            let
                newModel =
                    { model | currentTab = statsTab }

                nextUrl =
                    serializeQuery { currentTab = newModel.currentTab, debut = newModel.debut, fin = newModel.fin, plage = newModel.plage }
            in
            { newModel | derniereUrl = nextUrl }
                |> Effect.with (updateUrl newModel)

        ClickedSearch ->
            let
                nextUrl =
                    serializeQuery { currentTab = model.currentTab, debut = model.debut, fin = model.fin, plage = model.plage }
            in
            { model | derniereUrl = nextUrl }
                |> Effect.with (updateUrl model)

        GetStats ( currentTab, plage, ( debut, fin ) ) ->
            let
                doNothing =
                    ( Cmd.none, ( ( model.etablissementsStats, model.dernieresDatesEtablissements ), ( model.createursStats, model.dernieresDatesCreateurs ), ( model.locauxStats, model.dernieresDatesLocaux ) ) )

                ( cmd, ( ( etablissementsStats, dernieresDatesEtablissements ), ( createursStats, dernieresDatesCreateurs ), ( locauxStats, dernieresDatesLocaux ) ) ) =
                    case currentTab of
                        StatsEtablissements ->
                            if model.dernieresDatesEtablissements /= Just ( debut, fin ) then
                                ( getEtablissementsStats debut fin, ( ( RD.Loading, Just ( debut, fin ) ), ( model.createursStats, model.dernieresDatesCreateurs ), ( model.locauxStats, model.dernieresDatesLocaux ) ) )

                            else
                                doNothing

                        StatsCreateurs ->
                            if model.dernieresDatesCreateurs /= Just ( debut, fin ) then
                                ( getCreateursStats debut fin, ( ( model.etablissementsStats, model.dernieresDatesEtablissements ), ( RD.Loading, Just ( debut, fin ) ), ( model.locauxStats, model.dernieresDatesLocaux ) ) )

                            else
                                doNothing

                        StatsLocaux ->
                            if model.dernieresDatesLocaux /= Just ( debut, fin ) then
                                ( getLocauxStats debut fin, ( ( model.etablissementsStats, model.dernieresDatesEtablissements ), ( model.createursStats, model.dernieresDatesCreateurs ), ( RD.Loading, Just ( debut, fin ) ) ) )

                            else
                                doNothing
            in
            { model
                | etablissementsStats = etablissementsStats
                , createursStats = createursStats
                , locauxStats = locauxStats
                , currentTab = currentTab
                , plage = plage
                , debut = debut
                , fin = fin
                , dernieresDatesEtablissements = dernieresDatesEtablissements
                , dernieresDatesCreateurs = dernieresDatesCreateurs
                , dernieresDatesLocaux = dernieresDatesLocaux
            }
                |> Effect.withCmd cmd

        ChangeDate borne date ->
            let
                newModel =
                    case borne of
                        Debut ->
                            { model | debut = date, plage = Nothing }

                        Fin ->
                            { model | fin = date, plage = Nothing }
            in
            newModel
                |> Effect.withNone

        ChangeDatePreset plage ->
            let
                ( debut, fin ) =
                    datesFromPlage now timezone plage
            in
            { model | debut = debut, fin = fin, plage = Just plage }
                |> Effect.withNone
    )
        |> (\( m, c ) -> ( { m | compteur = m.compteur + 1 }, c ))


updateUrl : Model -> Effect.Effect Shared.Msg Msg
updateUrl model =
    Effect.fromShared <|
        Shared.Navigate <|
            Route.Activite <|
                (\q ->
                    if q == "" then
                        Nothing

                    else
                        Just q
                )
                <|
                    serializeQuery model


view : Time.Posix -> Time.Zone -> User -> Model -> View Msg
view now timezone identity model =
    { title =
        UI.Layout.pageTitle <| "Activité du service"
    , body = [ body now timezone identity model ]
    , route = Route.Activite Nothing
    }


body : Time.Posix -> Time.Zone -> User -> Model -> Html Msg
body now zone _ model =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12, class "flex" ]
            [ h1 [ Typo.fr_h4, class "!mb-0" ]
                [ text "Activité du service - Tableau de bord" ]
            ]
        , div [ DSFR.Grid.col12 ]
            [ DSFR.Tabs.new
                { changeTabMsg = stringToStatsTab >> Maybe.withDefault StatsEtablissements >> ClickedChangeTab
                , tabs =
                    [ { id = StatsEtablissements |> statsTabToString
                      , title = StatsEtablissements |> statsTabToDisplay
                      , icon = UI.Entite.iconeEtablissement
                      , content =
                            div [ class "fr-card--white p-8" ]
                                [ div [ class "flex flex-col gap-8" ]
                                    [ statsControls now zone model.compteur model.derniereUrl model.currentTab model.debut model.fin model.plage <| RD.map (\_ -> ()) <| model.etablissementsStats
                                    , Html.Lazy.lazy etablissementsTab model.etablissementsStats
                                    ]
                                ]
                      }
                    , { id = StatsCreateurs |> statsTabToString
                      , title = StatsCreateurs |> statsTabToDisplay
                      , icon = UI.Entite.iconeCreateur
                      , content =
                            div [ class "fr-card--white p-8" ]
                                [ div [ class "flex flex-col gap-8" ]
                                    [ statsControls now zone model.compteur model.derniereUrl model.currentTab model.debut model.fin model.plage <| RD.map (\_ -> ()) <| model.createursStats
                                    , Html.Lazy.lazy createursTab model.createursStats
                                    ]
                                ]
                      }
                    , { id = StatsLocaux |> statsTabToString
                      , title = StatsLocaux |> statsTabToDisplay
                      , icon = UI.Entite.iconeLocal
                      , content =
                            div [ class "fr-card--white p-8" ]
                                [ div [ class "flex flex-col gap-8" ]
                                    [ statsControls now zone model.compteur model.derniereUrl model.currentTab model.debut model.fin model.plage <| RD.map (\_ -> ()) <| model.locauxStats
                                    , Html.Lazy.lazy locauxTab model.locauxStats
                                    ]
                                ]
                      }
                    ]
                }
                |> DSFR.Tabs.view (statsTabToString model.currentTab)
            ]
        ]


selecteurDates : Time.Posix -> Time.Zone -> Int -> String -> StatsTab -> Date -> Date -> Maybe Plage -> WebData () -> Html Msg
selecteurDates now zone compteur derniereUrl currentTab debut fin maybePlage request =
    let
        today =
            Date.fromPosix zone now

        nextUrl =
            serializeQuery { currentTab = currentTab, debut = debut, fin = fin, plage = maybePlage }
    in
    div [ class "flex flex-col gap-4" ]
        [ Html.form
            [ Events.onSubmit <| ClickedSearch
            , class "flex flex-row items-end gap-4"
            ]
            [ DSFR.Input.new
                { value = debut |> formatDateToYYYYMMDD
                , onInput =
                    Date.fromIsoString
                        >> Result.withDefault debut
                        >> ChangeDate Debut
                , label = text "Date de début\u{00A0}:"
                , name = "debut-date-input"
                }
                |> DSFR.Input.withExtraAttrs [ class "!mb-0" ]
                |> DSFR.Input.date
                    { min = Just <| Date.toIsoString <| Date.add Date.Years -2 <| today
                    , max = Just <| Date.toIsoString <| today
                    }
                |> DSFR.Input.view
            , DSFR.Input.new
                { value = fin |> formatDateToYYYYMMDD
                , onInput =
                    Date.fromIsoString
                        >> Result.withDefault fin
                        >> ChangeDate Fin
                , label = text "Date de fin\u{00A0}:"
                , name = "fin-date-input"
                }
                |> DSFR.Input.withExtraAttrs [ class "!mb-0" ]
                |> DSFR.Input.date
                    { min = Just <| Date.toIsoString <| debut
                    , max = Just <| Date.toIsoString <| today
                    }
                |> DSFR.Input.view
            , DSFR.Button.new { onClick = Nothing, label = "OK" }
                |> DSFR.Button.withDisabled (request == RD.Loading || derniereUrl == nextUrl)
                |> DSFR.Button.submit
                |> DSFR.Button.view
            ]
        , Html.Keyed.node "div"
            [ class "flex flex-row gap-4" ]
            [ ( String.fromInt compteur
              , plages
                    |> List.map (\plage -> DSFR.Tag.selectable ChangeDatePreset (Just plage == maybePlage) { data = plage, toString = plageToDisplay })
                    |> DSFR.Tag.medium
              )
            ]
        ]


etablissementsTab : WebData EtablissementsStatsData -> Html Msg
etablissementsTab wdStats =
    div [] <|
        case wdStats of
            RD.Success { etablissements, echanges, rappels, demandes, actions } ->
                [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ div [ DSFR.Grid.col6 ]
                        [ etablissementsBlocs etablissements
                        ]
                    , div [ DSFR.Grid.col6 ]
                        [ rappelsBlocs rappels
                        ]
                    ]
                , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ echangesSection echanges
                    ]
                , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ demandesBlocs demandes
                    , demandesGraphiques demandes
                    , actionsBlocs True actions
                    ]
                ]

            RD.Loading ->
                [ text "Chargement des statistiques en cours..." ]

            RD.NotAsked ->
                [ text "huh" ]

            _ ->
                [ text "Une erreur s'est produite, veuillez recharger la page et nous contacter si le problème persiste." ]


statsControls : Time.Posix -> Time.Zone -> Int -> String -> StatsTab -> Date -> Date -> Maybe Plage -> WebData () -> Html Msg
statsControls now zone compteur derniereUrl currentTab debut fin plage wdStats =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12, class "flex flex-col gap-4" ]
            [ div [ class "flex flex-row" ]
                [ selecteurDates now zone compteur derniereUrl currentTab debut fin plage <| RD.map (\_ -> ()) <| wdStats
                ]
            ]
        ]


createursTab : WebData CreateursStatsData -> Html Msg
createursTab wdStats =
    div [] <|
        case wdStats of
            RD.Success { createurs, echanges, rappels, demandes, actions } ->
                [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ div [ DSFR.Grid.col6 ]
                        [ createursBlocs createurs
                        ]
                    , div [ DSFR.Grid.col6 ]
                        [ rappelsBlocs rappels
                        ]
                    ]
                , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ echangesSection echanges
                    ]
                , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ demandesBlocs demandes
                    , demandesGraphiques demandes
                    , actionsBlocs False actions
                    ]
                ]

            RD.Loading ->
                [ text "Chargement des statistiques en cours..." ]

            _ ->
                [ text "Une erreur s'est produite, veuillez recharger la page et nous contacter si le problème persiste." ]


locauxTab : WebData LocauxStatsData -> Html Msg
locauxTab wdStats =
    div [] <|
        case wdStats of
            RD.Success { locaux, echanges, rappels, demandes, actions } ->
                [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ div [ DSFR.Grid.col6 ]
                        [ locauxBlocs locaux
                        ]
                    , div [ DSFR.Grid.col6 ]
                        [ rappelsBlocs rappels
                        ]
                    ]
                , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ echangesSection echanges
                    ]
                , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ demandesBlocs demandes
                    , demandesGraphiques demandes
                    , actionsBlocs False actions
                    ]
                ]

            RD.Loading ->
                [ text "Chargement des statistiques en cours..." ]

            _ ->
                [ text "Une erreur s'est produite, veuillez recharger la page et nous contacter si le problème persiste." ]


etablissementsBlocs : { nouveaux : Int, qpvs : Int, horsTerritoire : Int } -> Html msg
etablissementsBlocs etablissements =
    div [ class "flex flex-col gap-4 fr-card--grey p-4" ]
        [ div [ class "flex flex-row justify-between items-center" ]
            [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Portefeuille du service" ]
            ]
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col4 ]
                [ numberCard []
                    "+"
                    [ "établiss."
                    ]
                    etablissements.nouveaux
                ]
            , div [ DSFR.Grid.col4 ]
                [ numberCard []
                    ""
                    [ "dont "
                    , String.fromInt etablissements.qpvs
                    , " issu"
                    , plural etablissements.qpvs
                    , " de QPV"
                    ]
                    etablissements.qpvs
                ]
            , div [ DSFR.Grid.col4 ]
                [ numberCard []
                    ""
                    [ "dont "
                    , String.fromInt etablissements.horsTerritoire
                    , " hors territoire"
                    ]
                    etablissements.horsTerritoire
                ]
            ]
        ]


createursBlocs : { nouveaux : Int, qpvs : Int, transformes : Int } -> Html msg
createursBlocs createurs =
    div [ class "flex flex-col gap-4 fr-card--grey p-4" ]
        [ div [ class "flex flex-row justify-between items-center" ]
            [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Créateurs d'entreprise" ]
            , DSFR.Button.new { label = "Voir tous les créateurs", onClick = Nothing }
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Createurs Nothing)
                |> DSFR.Button.secondary
                |> DSFR.Button.view
            ]
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col4 ]
                [ numberCard []
                    "+"
                    [ "créateur"
                    , plural createurs.nouveaux
                    , " créé"
                    , plural createurs.nouveaux
                    ]
                    createurs.nouveaux
                ]
            , div [ DSFR.Grid.col4 ]
                [ numberCard []
                    ""
                    [ "dont "
                    , String.fromInt createurs.qpvs
                    , " issu"
                    , plural createurs.qpvs
                    , " de QPV"
                    ]
                    createurs.qpvs
                ]
            , div [ DSFR.Grid.col4 ]
                [ numberCard []
                    ""
                    [ "transformé"
                    , plural createurs.transformes
                    , " en établiss."
                    ]
                    createurs.transformes
                ]
            ]
        ]


localPluriel : Int -> String
localPluriel quantite =
    if quantite > 1 then
        "locaux"

    else
        "local"


locauxBlocs : LocauxStats -> Html msg
locauxBlocs locaux =
    div [ class "flex flex-col gap-4 fr-card--grey p-4" ]
        [ div [ class "flex flex-row justify-between items-center" ]
            [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Locaux" ]
            , DSFR.Button.new { label = "Voir tous les locaux", onClick = Nothing }
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Locaux Nothing)
                |> DSFR.Button.secondary
                |> DSFR.Button.view
            ]
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col4 ]
                [ numberCard []
                    "+"
                    [ localPluriel locaux.occupes
                    , " occupé"
                    , plural locaux.occupes
                    ]
                    locaux.occupes
                ]
            , div [ DSFR.Grid.col4 ]
                [ numberCard []
                    "+"
                    [ localPluriel locaux.vacants
                    , " vacant"
                    , plural locaux.vacants
                    ]
                    locaux.vacants
                ]
            , div [ DSFR.Grid.col4 ]
                [ numberCard []
                    "+"
                    [ " contact"
                    , plural locaux.contacts
                    , " créé"
                    , plural locaux.contacts
                    ]
                    locaux.contacts
                ]
            ]
        ]


rappelsBlocs : RappelsStats -> Html msg
rappelsBlocs rappels =
    div [ class "flex flex-col gap-4 fr-card--grey p-4" ]
        [ div [ class "flex flex-row justify-between items-center" ]
            [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Rappels" ]
            , DSFR.Button.new { label = "Voir tous les rappels", onClick = Nothing }
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Suivis (Just "suivi=rappels"))
                |> DSFR.Button.secondary
                |> DSFR.Button.view
            ]
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col4 ]
                [ numberCard []
                    ""
                    [ "rappel"
                    , plural rappels.nouveaux
                    , " créé"
                    , plural rappels.nouveaux
                    ]
                    rappels.nouveaux
                ]
            , div [ DSFR.Grid.col4 ]
                [ numberCard []
                    ""
                    [ "arrivant à échéance"
                    ]
                    rappels.expires
                ]
            , div [ DSFR.Grid.col4 ]
                [ numberCard []
                    ""
                    [ "rappel"
                    , plural rappels.clotures
                    , " clôturé"
                    , plural rappels.clotures
                    ]
                    rappels.clotures
                ]
            ]
        ]


echangesSection : List EchangesStats -> Html msg
echangesSection echanges =
    div [ DSFR.Grid.col12 ]
        [ h3 [ Typo.fr_h6 ] [ text "Échanges" ]
        , div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col2, class "" ]
                [ numberCard []
                    "+"
                    [ "échange"
                    , plural <|
                        List.length echanges
                    ]
                    (List.length echanges)
                ]
            , div [ DSFR.Grid.col5, class "p-4 box-shadow" ]
                [ div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col4 ]
                        [ span [ Typo.textBold ] [ text "Répartition des échanges par type de contact" ] ]
                    , div [ DSFR.Grid.col8 ]
                        [ Html.node "chart-pie"
                            [ Encode.object
                                [ ( "values"
                                  , echanges
                                        |> List.Extra.gatherEqualsBy .type_
                                        |> List.map (\( e, rest ) -> ( e.type_, 1 + List.length rest ))
                                        |> List.sortBy (\( _, length ) -> length)
                                        |> List.reverse
                                        |> Encode.list
                                            (\( type_, count ) ->
                                                Encode.object
                                                    [ ( "label", Encode.string <| type_ )
                                                    , ( "count", Encode.float <| toFloat count )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "title", Encode.string "répartition" )
                                        , ( "height", Encode.int 200 )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Html.Attributes.attribute "data"
                            ]
                            []
                        ]
                    ]
                ]
            , div [ DSFR.Grid.col5, class "p-4 box-shadow" ]
                [ div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col4 ]
                        [ span [ Typo.textBold ] [ text "Alimentation de l'outil par des échanges" ] ]
                    , div [ DSFR.Grid.col8 ]
                        [ Html.node "chart-pie"
                            [ Encode.object
                                [ ( "values"
                                  , echanges
                                        |> List.Extra.gatherEqualsBy .auteur
                                        |> List.map (\( e, rest ) -> ( e.auteur, 1 + List.length rest ))
                                        |> List.sortBy (\( _, length ) -> length)
                                        |> List.reverse
                                        |> Encode.list
                                            (\( type_, count ) ->
                                                Encode.object
                                                    [ ( "label", Encode.string <| type_ )
                                                    , ( "count", Encode.float <| toFloat count )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "title", Encode.string "répartition" )
                                        , ( "height", Encode.int 200 )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Html.Attributes.attribute "data"
                            ]
                            []
                        ]
                    ]
                ]
            ]
        ]


demandesBlocs : DemandesStats -> Html msg
demandesBlocs demandes =
    div [ DSFR.Grid.col2, class "!pt-4 !pr-4 !pb-0" ]
        [ h3 [ Typo.fr_h6 ] [ text "Demandes" ]
        , div [ class "flex flex-col" ]
            [ numberCard []
                "+"
                [ "nouvelle"
                , plural demandes.nouvelles
                , " demande"
                , plural demandes.nouvelles
                ]
                demandes.nouvelles
            , numberCard []
                ""
                [ "demande"
                , plural <| List.length demandes.ouvertes
                , " en cours (au total)"
                ]
                (List.length demandes.ouvertes)
            , numberCard []
                ""
                [ "demande"
                , plural demandes.cloturees
                , " clôturée"
                , plural demandes.cloturees
                ]
                demandes.cloturees
            ]
        ]


demandesGraphiques : DemandesStats -> Html msg
demandesGraphiques demandes =
    div [ DSFR.Grid.col8, class "fr-card--grey !p-4 !pb-0" ]
        [ h3 [ Typo.fr_h6 ] [ text "Types de demandes en cours" ]
        , Html.node "chart-bar"
            [ Encode.object
                [ ( "values"
                  , demandes.ouvertes
                        |> List.Extra.gatherEquals
                        |> List.map (\( type_, rest ) -> ( type_, 1 + List.length rest ))
                        |> List.sortBy (\( _, length ) -> length)
                        |> List.reverse
                        |> Encode.list
                            (\( type_, count ) ->
                                Encode.object
                                    [ ( "label", Encode.string <| type_ )
                                    , ( "count", Encode.float <| toFloat count )
                                    ]
                            )
                  )
                ]
                |> Encode.encode 0
                |> Html.Attributes.attribute "data"
            ]
            []
        ]


actionsBlocs : Bool -> ActionsStats -> Html msg
actionsBlocs withQualifications actions =
    div [ DSFR.Grid.col2, class "!pt-4 !pl-4 !pb-0" ]
        [ h3 [ Typo.fr_h6 ] [ text "Actions" ]
        , div [ class "flex flex-col" ]
            [ numberCard []
                ""
                [ "consultation"
                , plural actions.vues
                , " de fiches"
                ]
                actions.vues
            , viewIf withQualifications <|
                numberCard []
                    ""
                    [ "qualification"
                    , plural actions.qualifications
                    , " de la base de données"
                    ]
                    actions.qualifications
            , numberCard []
                ""
                [ "action"
                , plural actions.etiquetages
                , " sur étiquettes"
                ]
                actions.etiquetages
            ]
        ]


getEtablissementsStats : Date -> Date -> Cmd Msg
getEtablissementsStats debut fin =
    get
        { url = Api.getActiviteStatsEtablissements (formatDateToYYYYMMDD debut) (formatDateToYYYYMMDD fin) }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedEtablissementsStats
        }
        decodeEtablissementsStats


getCreateursStats : Date -> Date -> Cmd Msg
getCreateursStats debut fin =
    get
        { url = Api.getActiviteStatsEtablissementCreations (formatDateToYYYYMMDD debut) (formatDateToYYYYMMDD fin) }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedCreateursStats
        }
        decodeCreateursStats


getLocauxStats : Date -> Date -> Cmd Msg
getLocauxStats debut fin =
    get
        { url = Api.getActiviteStatsLocaux (formatDateToYYYYMMDD debut) (formatDateToYYYYMMDD fin) }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedLocauxStats
        }
        decodeLocauxStats


decodeEtablissementsStats : Decoder EtablissementsStatsData
decodeEtablissementsStats =
    Decode.succeed EtablissementsStatsData
        |> andMap decodeEtablissements
        |> andMap decodeRappels
        |> andMap decodeEchanges
        |> andMap decodeDemandes
        |> andMap decodeActions


decodeCreateursStats : Decoder CreateursStatsData
decodeCreateursStats =
    Decode.succeed CreateursStatsData
        |> andMap decodeCreateurs
        |> andMap decodeRappels
        |> andMap decodeEchanges
        |> andMap decodeDemandes
        |> andMap decodeActions


decodeLocauxStats : Decoder LocauxStatsData
decodeLocauxStats =
    Decode.succeed LocauxStatsData
        |> andMap decodeLocaux
        |> andMap decodeRappels
        |> andMap decodeEchanges
        |> andMap decodeDemandes
        |> andMap decodeActions


decodeEtablissements : Decoder EtablissementsStats
decodeEtablissements =
    Decode.field "etablissements" <|
        (Decode.succeed EtablissementsStats
            |> andMap (Decode.field "nouveaux" <| Decode.int)
            |> andMap (Decode.field "qpvs" <| Decode.int)
            |> andMap (Decode.field "horsTerritoire" <| Decode.int)
        )


decodeCreateurs : Decoder CreateursStats
decodeCreateurs =
    Decode.field "createurs" <|
        (Decode.succeed CreateursStats
            |> andMap (Decode.field "nouveaux" <| Decode.int)
            |> andMap (Decode.field "qpvs" <| Decode.int)
            |> andMap (Decode.field "transformes" <| Decode.int)
        )


decodeLocaux : Decoder LocauxStats
decodeLocaux =
    Decode.field "locaux" <|
        (Decode.succeed LocauxStats
            |> andMap (Decode.field "occupes" <| Decode.int)
            |> andMap (Decode.field "vacants" <| Decode.int)
            |> andMap (Decode.field "contacts" <| Decode.int)
        )


decodeRappels : Decoder RappelsStats
decodeRappels =
    Decode.field "rappels" <|
        (Decode.succeed RappelsStats
            |> andMap (Decode.field "nouveaux" <| Decode.int)
            |> andMap (Decode.field "expires" <| Decode.int)
            |> andMap (Decode.field "clotures" <| Decode.int)
        )


decodeEchanges : Decoder (List EchangesStats)
decodeEchanges =
    Decode.field "echanges" <|
        Decode.list <|
            (Decode.succeed EchangesStats
                |> andMap (Decode.field "auteur" <| Decode.string)
                |> andMap (Decode.field "type" <| Decode.map UI.Echange.echangeToDisplay <| Data.Echange.decodeTypeEchange)
            )


decodeDemandes : Decoder DemandesStats
decodeDemandes =
    Decode.field "demandes" <|
        (Decode.succeed DemandesStats
            |> andMap (Decode.field "nouvelles" <| Decode.int)
            |> andMap (Decode.field "ouvertes" <| Decode.list <| Decode.field "type" <| Decode.map Data.Demande.typeDemandeToDisplay <| Data.Demande.decodeTypeDemande)
            |> andMap (Decode.field "cloturees" <| Decode.int)
        )


decodeActions : Decoder ActionsStats
decodeActions =
    Decode.field "actions" <|
        (Decode.succeed ActionsStats
            |> andMap (Decode.field "vues" <| Decode.int)
            |> andMap (Decode.field "qualifications" <| Decode.int)
            |> andMap (Decode.field "etiquetages" <| Decode.int)
        )


plages : List Plage
plages =
    [ Semaine, Mois, Annee ]


plageToString : Plage -> String
plageToString plage =
    case plage of
        Semaine ->
            "semaine"

        Mois ->
            "mois"

        Annee ->
            "annee"


stringToPlage : String -> Maybe Plage
stringToPlage plage =
    case plage of
        "semaine" ->
            Just Semaine

        "mois" ->
            Just Mois

        "annee" ->
            Just Annee

        _ ->
            Nothing


plageToDisplay : Plage -> String
plageToDisplay plage =
    case plage of
        Semaine ->
            "7 derniers jours"

        Mois ->
            "30 derniers jours"

        Annee ->
            "Année en cours"
