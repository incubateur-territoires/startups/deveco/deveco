module Pages.Annuaire exposing (Model, Msg, defaultFilters, filtersAndPaginationToQuery, page)

import Accessibility exposing (Html, br, div, h1, span, text)
import Api
import Api.Auth
import Api.EntityId exposing (EntityId, entityIdToString, parseEntityId)
import Browser.Dom as Dom
import DSFR.Button
import DSFR.Grid
import DSFR.Icons exposing (iconMD)
import DSFR.Icons.Design
import DSFR.Icons.System exposing (arrowDownLine, arrowUpLine)
import DSFR.Modal
import DSFR.Pagination
import DSFR.SearchBar
import DSFR.Table
import DSFR.Typography as Typo
import Data.Contact exposing (ContactInputType(..))
import Data.Etablissement exposing (Siret)
import Data.EtablissementCreationId exposing (EtablissementCreationId)
import Data.Personne exposing (Personne, decodePersonne)
import Effect
import Html
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Html.Extra exposing (nothing, viewIf)
import Html.Lazy
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Lib.UI exposing (plural, withEmptyAs)
import QS
import RemoteData as RD exposing (WebData)
import Route
import Shared
import Spa.Page exposing (Page)
import Task
import UI.Contact exposing (ContactAction(..), ContactField, NewContactForm, encodeNewContactForm, updateContact, updateContactForm)
import UI.Layout
import UI.Pagination exposing (Pagination, defaultPagination, paginationToQuery, queryToPagination)
import View exposing (View)


type alias Model =
    { personnes : WebData (List PersonneEtFonctions)
    , filters : Filters
    , pagination : Pagination
    , currentUrl : String
    , excelExportRequested : Bool
    , contactAction : ContactAction
    , saveContactRequest : WebData (List PersonneEtFonctions)
    , prochainId : Int
    }


type Msg
    = ReceivedPersonnesEtFonctions (WebData ( List PersonneEtFonctions, Pagination ))
    | DoSearch ( Filters, Pagination )
    | UpdatedRecherche String
    | ConfirmedRecherche
    | SetSorting Header
    | ClickedExportExcel
    | NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | SetContactAction ContactAction
    | ConfirmContactAction
    | CancelContactAction
    | UpdatedContact ContactField String
    | ReceivedSaveContact (WebData ( List PersonneEtFonctions, Pagination ))
    | UpdatedRechercheContactExistant String
    | RechercheContactExistant
    | ReceivedContactsExistants (WebData (List Personne))
    | SelectedContactExistant (Maybe Personne)
    | ToggleNouveauContactModeCreation Bool


type alias PersonneEtFonctions =
    ( Personne, List Data.Contact.FonctionContact )


type Header
    = HNom
    | HPrenom
    | HTelephone
    | HEmail
    | HDemarchageAccepte
    | HBoutons


stringToHeader : String -> Maybe Header
stringToHeader header =
    case header of
        "nom" ->
            Just HNom

        "prenom" ->
            Just HPrenom

        "email" ->
            Just HEmail

        _ ->
            Nothing


headers : List Header
headers =
    [ HNom
    , HPrenom
    , HTelephone
    , HEmail
    , HDemarchageAccepte
    , HBoutons
    ]


headerToDisplay : Header -> String
headerToDisplay header =
    case header of
        HNom ->
            "Nom"

        HPrenom ->
            "Prénom"

        HTelephone ->
            "Téléphone"

        HEmail ->
            "Email"

        HDemarchageAccepte ->
            "Accord contact"

        HBoutons ->
            ""


headerToString : Header -> String
headerToString header =
    case header of
        HNom ->
            "nom"

        HPrenom ->
            "prenom"

        HTelephone ->
            "telephone"

        HEmail ->
            "email"

        HDemarchageAccepte ->
            "demarchage"

        HBoutons ->
            "boutons"


type alias Filters =
    { recherche : String
    , etablissementId : Maybe Siret
    , etablissementCreationId : Maybe (EntityId EtablissementCreationId)
    , localId : Maybe String
    , elementsParPage : Maybe Int
    , tri : Header
    , direction : Direction
    }


defaultFilters : Filters
defaultFilters =
    { recherche = ""
    , etablissementId = Nothing
    , etablissementCreationId = Nothing
    , localId = Nothing
    , elementsParPage = Nothing
    , tri = HNom
    , direction = Ascending
    }


queryKeys :
    { recherche : String
    , etablissementId : String
    , etablissementCreationId : String
    , localId : String
    , elementsParPage : String
    , tri : String
    , direction : String
    }
queryKeys =
    { recherche = "recherche"
    , etablissementId = "etablissementId"
    , etablissementCreationId = "etablissementCreationId"
    , localId = "localId"
    , elementsParPage = "elementsParPage"
    , tri = "tri"
    , direction = "direction"
    }


type Direction
    = Ascending
    | Descending


directionToString : Direction -> String
directionToString direction =
    case direction of
        Ascending ->
            "asc"

        Descending ->
            "desc"


stringToDirection : String -> Maybe Direction
stringToDirection string =
    case string of
        "asc" ->
            Just Ascending

        "desc" ->
            Just Descending

        _ ->
            Nothing


decodePersonneEtFonctions : Decoder PersonneEtFonctions
decodePersonneEtFonctions =
    Decode.succeed Tuple.pair
        |> andMap decodePersonne
        |> andMap (Decode.field "liens" <| Decode.list Data.Contact.decodeFonctionContact)


page : Shared.Shared -> Shared.User -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.onNewFlags (queryToFiltersAndPagination >> DoSearch) <|
        Spa.Page.element
            { view = view
            , init = init
            , update = update
            , subscriptions = \_ -> Sub.none
            }


init : Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init rawQuery =
    let
        ( filters, pagination ) =
            queryToFiltersAndPagination rawQuery

        nextUrl =
            filtersAndPaginationToQuery ( filters, pagination )
    in
    ( { personnes = RD.Loading
      , filters = filters
      , pagination = pagination
      , currentUrl = nextUrl
      , excelExportRequested = False
      , contactAction = None
      , saveContactRequest = RD.NotAsked
      , prochainId = 0
      }
    , updateUrl { filters = filters, pagination = pagination }
    )
        |> Shared.pageChangeEffects


getPersonnesEtFonctions : String -> Cmd Msg
getPersonnesEtFonctions query =
    Api.Auth.get
        { url = Api.getContacts query }
        { toShared = SharedMsg
        , logger = Nothing
        , handler = ReceivedPersonnesEtFonctions
        }
        decodeBatchResponse


getPersonnes : String -> Cmd Msg
getPersonnes query =
    Api.Auth.get
        { url = Api.getContacts query }
        { toShared = SharedMsg
        , logger = Nothing
        , handler = ReceivedContactsExistants
        }
        (Decode.field "elements" <| Decode.list <| decodePersonne)


decodeBatchResponse : Decoder ( List PersonneEtFonctions, Pagination )
decodeBatchResponse =
    Decode.succeed (\p pages results data -> ( data, Pagination p pages results ))
        |> andMap (Decode.field "page" Decode.int)
        |> andMap (Decode.field "pages" Decode.int)
        |> andMap (Decode.field "total" Decode.int)
        |> andMap (Decode.field "elements" <| Decode.list <| decodePersonneEtFonctions)


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        ReceivedPersonnesEtFonctions response ->
            case response of
                RD.Success ( fs, pagination ) ->
                    ( { model | personnes = RD.Success fs, pagination = pagination }
                    , Effect.none
                    )

                _ ->
                    ( { model | personnes = response |> RD.map Tuple.first }
                    , Effect.none
                    )

        UpdatedRecherche recherche ->
            { model | filters = model.filters |> (\f -> { f | recherche = recherche }) }
                |> Effect.withNone

        ConfirmedRecherche ->
            let
                newModel =
                    { model | pagination = defaultPagination }
            in
            newModel
                |> Effect.with (updateUrl newModel)

        SetSorting header ->
            let
                ( tri, direction ) =
                    case ( model.filters.tri, model.filters.direction ) of
                        ( h, Ascending ) ->
                            if h == header then
                                ( h, Descending )

                            else
                                ( header, Ascending )

                        ( h, Descending ) ->
                            if h == header then
                                ( h, Ascending )

                            else
                                ( header, Ascending )

                filters =
                    model.filters

                newModel =
                    { model | filters = { filters | tri = tri, direction = direction } }
            in
            ( newModel
            , updateUrl newModel
            )

        ClickedExportExcel ->
            { model | excelExportRequested = True }
                |> Effect.withNone

        DoSearch ( filters, pagination ) ->
            let
                nextUrl =
                    filtersAndPaginationToQuery ( filters, pagination )
            in
            { model
                | personnes = RD.Loading
                , filters = filters
                , pagination = pagination
                , currentUrl = nextUrl
                , excelExportRequested = False
            }
                |> Effect.withCmd
                    (getPersonnesEtFonctions <|
                        filtersAndPaginationToQuery ( filters, pagination )
                    )

        SetContactAction action ->
            let
                cmd =
                    case action of
                        EditContact _ _ ->
                            Task.attempt (\_ -> NoOp) (Dom.focus <| "modifier-contact-fonction")

                        NewContact _ ->
                            Task.attempt (\_ -> NoOp) (Dom.focus <| "nouveau-contact-fonction")

                        _ ->
                            Cmd.none
            in
            ( { model | contactAction = action }, Effect.fromCmd cmd )

        CancelContactAction ->
            ( { model | saveContactRequest = RD.NotAsked, contactAction = None }, Effect.none )

        UpdatedContact field value ->
            case model.saveContactRequest of
                RD.Loading ->
                    ( model, Effect.none )

                _ ->
                    Effect.withNone <|
                        (\m -> { m | saveContactRequest = RD.NotAsked }) <|
                            case model.contactAction of
                                None ->
                                    model

                                ViewContactFonctions _ ->
                                    model

                                DeleteContact _ _ ->
                                    model

                                DeleteContactForm _ ->
                                    model

                                NewContact contactForm ->
                                    { model | contactAction = NewContact <| updateContactForm field value contactForm }

                                EditContact personne contact ->
                                    { model | contactAction = EditContact personne <| updateContact field value contact }

                                EditContactForm contactFormOriginal contactForm ->
                                    { model | contactAction = EditContactForm contactFormOriginal contactForm }

        ConfirmContactAction ->
            case model.saveContactRequest of
                RD.Loading ->
                    ( model, Effect.none )

                _ ->
                    case model.contactAction of
                        None ->
                            ( model, Effect.none )

                        ViewContactFonctions _ ->
                            ( model, Effect.none )

                        NewContact contact ->
                            ( { model | saveContactRequest = RD.Loading }
                            , Effect.fromCmd <|
                                createContact contact <|
                                    filtersAndPaginationToQuery ( model.filters, model.pagination )
                            )

                        DeleteContact contact _ ->
                            ( { model | saveContactRequest = RD.Loading }
                            , Effect.fromCmd <|
                                deleteContact contact <|
                                    filtersAndPaginationToQuery ( model.filters, model.pagination )
                            )

                        DeleteContactForm _ ->
                            ( model, Effect.none )

                        EditContact _ contact ->
                            ( { model | saveContactRequest = RD.Loading }
                            , Effect.fromCmd <|
                                editContact contact <|
                                    filtersAndPaginationToQuery ( model.filters, model.pagination )
                            )

                        EditContactForm _ _ ->
                            ( model, Effect.none )

        ReceivedSaveContact response ->
            let
                ( contactAction, personnes, pagination ) =
                    case response of
                        RD.Success ( ps, pag ) ->
                            ( None, RD.Success ps, pag )

                        _ ->
                            ( model.contactAction, response |> RD.map Tuple.first, model.pagination )
            in
            ( { model
                | personnes = personnes
                , pagination = pagination
                , contactAction = contactAction
                , saveContactRequest = response |> RD.map Tuple.first
              }
            , Effect.none
            )

        UpdatedRechercheContactExistant recherche ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | recherche = recherche
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm _ _ ->
                    ( model, Effect.none )

        RechercheContactExistant ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | resultats = RD.Loading
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.fromCmd <|
                        getPersonnes <|
                            filtersAndPaginationToQuery
                                ( { defaultFilters
                                    | recherche = contactForm.personneExistante.recherche
                                    , elementsParPage = Just 5
                                  }
                                , defaultPagination
                                )
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm _ _ ->
                    ( model, Effect.none )

        ReceivedContactsExistants response ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | resultats = response
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm _ _ ->
                    ( model, Effect.none )

        SelectedContactExistant maybeContact ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | selectionne = maybeContact
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm _ _ ->
                    ( model, Effect.none )

        ToggleNouveauContactModeCreation modeCreation ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | modeCreation = modeCreation
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm _ _ ->
                    ( model, Effect.none )


updateUrl : { data | filters : Filters, pagination : Pagination } -> Effect.Effect Shared.Msg Msg
updateUrl { filters, pagination } =
    Effect.fromShared <|
        Shared.Navigate <|
            Route.Annuaire <|
                (\q ->
                    if q == "" then
                        Nothing

                    else
                        Just q
                )
                <|
                    filtersAndPaginationToQuery ( filters, pagination )


view : Model -> View Msg
view model =
    { title =
        UI.Layout.pageTitle <|
            "Annuaire - Deveco"
    , body = UI.Layout.lazyBody body model
    , route =
        Route.Annuaire <|
            (\q ->
                if q == "" then
                    Nothing

                else
                    Just q
            )
            <|
                filtersAndPaginationToQuery ( model.filters, model.pagination )
    }


body : Model -> Html Msg
body { filters, excelExportRequested, pagination, personnes, currentUrl, saveContactRequest, contactAction, prochainId } =
    div [ class "p-4" ]
        [ Html.Lazy.lazy2 viewContactsModal saveContactRequest contactAction
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col12, class "fr-card--white" ]
                [ div [ class "flex flex-row items-baseline justify-between" ]
                    [ h1 [ Typo.fr_h4, class "!m-0" ]
                        [ text "Annuaire" ]
                    , DSFR.Button.dropdownSelector { label = text "Actions", hint = Just "Actions de l'annuaire", id = "annuaire-actions" } <|
                        [ DSFR.Button.new
                            { onClick =
                                Just <|
                                    SetContactAction <|
                                        NewContact
                                            { fonction = Nothing
                                            , personneNouvelle = Data.Personne.emptyPersonne
                                            , personneExistante = UI.Contact.existantVide
                                            , modeCreation = True
                                            , id = prochainId
                                            }
                            , label = "Créer un contact"
                            }
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.withAttrs [ class "!w-full" ]
                            |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                            |> DSFR.Button.view
                        , let
                            exportUrl =
                                (Api.getContacts <|
                                    currentUrl
                                )
                                    ++ "&format=xlsx"
                          in
                          DSFR.Button.new { onClick = Just ClickedExportExcel, label = "Exporter (Excel)" }
                            |> DSFR.Button.withDisabled (pagination.results == 0 || excelExportRequested)
                            |> DSFR.Button.linkButtonExternal exportUrl
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                            |> DSFR.Button.view
                        ]
                    ]
                ]
            , Html.Lazy.lazy3 contactsTable filters pagination personnes
            ]
        ]


contactsTable : Filters -> Pagination -> WebData (List PersonneEtFonctions) -> Html Msg
contactsTable filters pagination personnes =
    div [ DSFR.Grid.col12, class "p-4 fr-card--white" ] <|
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "!mb-4" ]
            [ div [ DSFR.Grid.col, class "flex flex-row justify-between items-end" ] <|
                [ DSFR.SearchBar.searchBar { errors = [], extraAttrs = [], disabled = False }
                    { submitMsg = ConfirmedRecherche
                    , buttonLabel = "Rechercher"
                    , inputMsg = UpdatedRecherche
                    , inputLabel = "Recherche par nom, prénom, téléphone, ou email"
                    , inputPlaceholder = Nothing
                    , inputId = "recherche-annuaire"
                    , inputValue = filters.recherche
                    , hints = []
                    , fullLabel = Just <| text "Recherche par nom, prénom, téléphone, ou email"
                    }
                ]
            ]
        , viewIf (pagination.results > 0) <|
            div [ class "mb-4" ]
                [ text <| String.fromInt pagination.results
                , text " "
                , text "résultat"
                , text <| plural pagination.results
                ]
        , div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col ] <|
                List.singleton <|
                    viewContactsTable filters pagination personnes
            ]
        ]


toCell : Header -> PersonneEtFonctions -> Html Msg
toCell header ( { id, prenom, nom, email, telephone, telephone2, demarchageAccepte } as personne, fonctions ) =
    case header of
        HEmail ->
            span [ Html.Attributes.style "overflow-wrap" "break-word" ] <|
                List.singleton <|
                    text <|
                        withEmptyAs "-" <|
                            email

        HPrenom ->
            text <|
                withEmptyAs "-" <|
                    prenom

        HNom ->
            text <|
                withEmptyAs "-" <|
                    nom

        HTelephone ->
            div []
                [ div [] <|
                    List.singleton <|
                        text <|
                            withEmptyAs "-" <|
                                telephone
                , div [] <|
                    List.singleton <|
                        text <|
                            withEmptyAs "-" <|
                                telephone2
                ]

        HDemarchageAccepte ->
            div [ class "flex flex-col gap-2" ] <|
                List.singleton <|
                    text <|
                        if demarchageAccepte then
                            "Oui"

                        else
                            "Non"

        HBoutons ->
            [ DSFR.Button.new
                { onClick =
                    Just <|
                        SetContactAction <|
                            ViewContactFonctions fonctions
                , label = "Voir le contact"
                }
                |> DSFR.Button.onlyIcon DSFR.Icons.System.informationLine
                |> DSFR.Button.withAttrs [ Html.Attributes.id <| "voir-contact-" ++ entityIdToString personne.id, class "!m-[0.25rem]" ]
            , DSFR.Button.new
                { onClick =
                    Just <|
                        SetContactAction <|
                            EditContact
                                { fonction = Nothing
                                , personne = personne
                                , liens = fonctions
                                }
                                { fonction = Nothing
                                , personne = personne
                                , liens = fonctions
                                }
                , label = "Modifier le contact"
                }
                |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
                |> DSFR.Button.secondary
                |> DSFR.Button.withAttrs [ Html.Attributes.id <| "modifier-contact-" ++ entityIdToString id, class "!m-[0.25rem]" ]
            , DSFR.Button.new
                { onClick =
                    Just <|
                        SetContactAction <|
                            DeleteContact
                                { fonction = Nothing
                                , personne = personne
                                , liens = fonctions
                                }
                                fonctions
                , label = "Supprimer le contact"
                }
                |> DSFR.Button.onlyIcon DSFR.Icons.System.deleteLine
                |> DSFR.Button.tertiary
                |> DSFR.Button.withAttrs [ Html.Attributes.id <| "supprimer-contact-" ++ entityIdToString id, class "!m-[0.25rem]" ]
            ]
                |> DSFR.Button.group
                |> DSFR.Button.inline
                |> DSFR.Button.alignedRight
                |> DSFR.Button.viewGroup


viewContactsTable : Filters -> Pagination -> WebData (List PersonneEtFonctions) -> Html Msg
viewContactsTable filters pagination personnes =
    case personnes of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            div [ class "text-center" ]
                [ text "Chargement en cours..." ]

        RD.Failure _ ->
            div [ class "text-center" ]
                [ text "Une erreur s'est produite, veuillez recharger la page." ]

        RD.Success liste ->
            case liste of
                [] ->
                    div [ class "text-center", DSFR.Grid.col ]
                        [ text "Aucune personne trouvée."
                        ]

                _ ->
                    div [ DSFR.Grid.col12 ]
                        [ DSFR.Table.table
                            { id = "tableau-utilisateurs"
                            , caption = text "Utilisateurs"
                            , headers = headers
                            , rows = liste
                            , toHeader =
                                \header ->
                                    let
                                        sortOnClick =
                                            if isSortable header then
                                                [ onClick <| SetSorting <| header, class "cursor-pointer" ]

                                            else
                                                []

                                        sortIcon =
                                            if isSortable header then
                                                sortingIcon ( filters.tri, filters.direction ) header

                                            else
                                                nothing
                                    in
                                    Html.span sortOnClick
                                        [ case header of
                                            HDemarchageAccepte ->
                                                span [] [ text "Accord", br [], text "contact" ]

                                            _ ->
                                                headerToDisplay header |> text
                                        , sortIcon
                                        ]
                            , toRowId = Tuple.first >> .id >> entityIdToString
                            , toCell = toCell
                            }
                            |> DSFR.Table.withContainerAttrs [ class "!mb-0" ]
                            |> DSFR.Table.noBorders
                            |> DSFR.Table.captionHidden
                            |> DSFR.Table.fixed
                            |> DSFR.Table.view
                        , if pagination.pages > 1 then
                            div [ class "!mt-4" ]
                                [ DSFR.Pagination.view pagination.page pagination.pages <|
                                    toHref ( filters, pagination )
                                ]

                          else
                            nothing
                        ]


sortingIcon : ( Header, Direction ) -> Header -> Html msg
sortingIcon ( sortingHeader, direction ) header =
    if header == sortingHeader then
        case direction of
            Ascending ->
                iconMD arrowDownLine

            Descending ->
                iconMD arrowUpLine

    else
        nothing


isSortable : Header -> Bool
isSortable header =
    List.member header [ HPrenom, HNom, HEmail ]


queryToFiltersAndPagination : Maybe String -> ( Filters, Pagination )
queryToFiltersAndPagination rawQuery =
    rawQuery
        |> Maybe.withDefault ""
        |> (\query -> ( queryToFilters query, queryToPagination query ))


queryToFilters : String -> Filters
queryToFilters rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    recherche =
                        query
                            |> QS.getAsStringList queryKeys.recherche
                            |> List.head
                            |> Maybe.withDefault ""

                    etablissementId =
                        query
                            |> QS.getAsStringList queryKeys.etablissementId
                            |> List.head

                    etablissementCreationId =
                        query
                            |> QS.getAsStringList queryKeys.etablissementCreationId
                            |> List.head
                            |> Maybe.andThen parseEntityId

                    localId =
                        query
                            |> QS.getAsStringList queryKeys.localId
                            |> List.head

                    tri =
                        query
                            |> QS.getAsStringList queryKeys.tri
                            |> List.head
                            |> Maybe.andThen stringToHeader
                            |> Maybe.withDefault HNom

                    direction =
                        query
                            |> QS.getAsStringList queryKeys.direction
                            |> List.head
                            |> Maybe.andThen stringToDirection
                            |> Maybe.withDefault Ascending

                    elementsParPage =
                        query
                            |> QS.getAsStringList queryKeys.elementsParPage
                            |> List.head
                            |> Maybe.andThen String.toInt
                in
                { recherche = recherche
                , etablissementId = etablissementId
                , etablissementCreationId = etablissementCreationId
                , localId = localId
                , elementsParPage = elementsParPage
                , tri = tri
                , direction = direction
                }
           )


filtersAndPaginationToQuery : ( Filters, Pagination ) -> String
filtersAndPaginationToQuery ( recherche, pagination ) =
    QS.merge (filtersToQuery recherche) (paginationToQuery pagination)
        |> QS.serialize (QS.config |> QS.addQuestionMark False)


filtersToQuery : Filters -> QS.Query
filtersToQuery { recherche, etablissementId, localId, elementsParPage, tri, direction } =
    let
        setRecherche =
            case recherche of
                "" ->
                    identity

                _ ->
                    QS.setStr queryKeys.recherche recherche

        setTri =
            case tri of
                HNom ->
                    identity

                _ ->
                    QS.setStr queryKeys.tri <| headerToString tri

        setDirection =
            case direction of
                Ascending ->
                    identity

                Descending ->
                    QS.setStr queryKeys.direction <| directionToString direction

        setEtablissementId =
            case etablissementId of
                Nothing ->
                    identity

                Just id ->
                    QS.setStr queryKeys.etablissementId <| id

        setLocalId =
            case localId of
                Nothing ->
                    identity

                Just id ->
                    QS.setStr queryKeys.localId <| id

        setElementsParPage =
            case elementsParPage of
                Nothing ->
                    identity

                Just e ->
                    QS.setStr queryKeys.elementsParPage <| String.fromInt <| e
    in
    QS.empty
        |> setRecherche
        |> setEtablissementId
        |> setLocalId
        |> setElementsParPage
        |> setTri
        |> setDirection


toHref : ( Filters, Pagination ) -> Int -> String
toHref ( filters, pagination ) newPage =
    Route.toUrl <|
        Route.Annuaire <|
            (\q ->
                if q == "" then
                    Nothing

                else
                    Just q
            )
            <|
                filtersAndPaginationToQuery ( filters, { pagination | page = newPage } )


createContact : NewContactForm -> String -> Cmd Msg
createContact newContactForm query =
    let
        httpBody =
            encodeNewContactForm newContactForm
    in
    Api.Auth.post
        { url = Api.createContact query
        , body = httpBody |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveContact
        }
        decodeBatchResponse


editContact : Data.Contact.Contact -> String -> Cmd Msg
editContact contact query =
    Api.Auth.post
        { url = Api.editContact contact.personne.id query
        , body =
            contact
                |> Data.Contact.encodeContact ContactUpdateInput
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveContact
        }
        decodeBatchResponse


deleteContact : Data.Contact.Contact -> String -> Cmd Msg
deleteContact { personne } query =
    Api.Auth.delete
        { url = Api.editContact personne.id query
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveContact
        }
        decodeBatchResponse


viewContactsModal : WebData (List PersonneEtFonctions) -> ContactAction -> Html Msg
viewContactsModal saveContactRequest contactAction =
    let
        ( opened, content, title ) =
            case contactAction of
                None ->
                    ( False, nothing, nothing )

                ViewContactFonctions fonctions ->
                    ( True, UI.Contact.viewFonctions SetContactAction fonctions, text "Voir les fonctions du contact" )

                NewContact contactForm ->
                    ( True
                    , UI.Contact.viewNewContactFormFields
                        { confirm = ConfirmContactAction
                        , update = UpdatedContact
                        , cancel = CancelContactAction
                        , toggle = ToggleNouveauContactModeCreation
                        , confirmSearch = RechercheContactExistant
                        , updateSearch = UpdatedRechercheContactExistant
                        , selectedPersonne = SelectedContactExistant
                        , niveauxDiplome = Nothing
                        , situationsProfessionnelles = Nothing
                        }
                        saveContactRequest
                        contactForm
                    , text "Ajouter un contact"
                    )

                EditContact contactExistant contact ->
                    ( True
                    , UI.Contact.viewEditContact
                        { confirm = ConfirmContactAction
                        , update = UpdatedContact
                        , cancel = CancelContactAction
                        , niveauxDiplome = Nothing
                        , situationsProfessionnelles = Nothing
                        }
                        saveContactRequest
                        contactExistant
                        contact
                    , text "Modifier un contact"
                    )

                EditContactForm _ _ ->
                    ( False, nothing, nothing )

                DeleteContact contact autresFonctions ->
                    ( True
                    , UI.Contact.viewDeleteContactBody
                        autresFonctions
                        { confirm = ConfirmContactAction
                        , cancel = CancelContactAction
                        }
                        saveContactRequest
                        contact.personne.id
                    , text "Supprimer un contact"
                    )

                DeleteContactForm _ ->
                    ( False, nothing, nothing )
    in
    DSFR.Modal.view
        { id = "contact"
        , label = "contact"
        , openMsg = NoOp
        , closeMsg = Just CancelContactAction
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.md
        }
        content
        Nothing
        |> Tuple.first
