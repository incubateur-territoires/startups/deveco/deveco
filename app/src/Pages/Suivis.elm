module Pages.Suivis exposing (Model, Msg, page)

import Accessibility exposing (Html, div, text)
import Api
import Api.Auth exposing (get)
import Data.Brouillon exposing (Brouillon)
import Data.Collegues exposing (Collegue)
import Data.Demande exposing (Demande)
import Data.Echange exposing (Echange)
import Data.Rappel exposing (Rappel)
import Data.Role
import Effect
import Html.Attributes exposing (class)
import Html.Lazy as Lazy
import Json.Decode as Decode
import Json.Decode.Extra exposing (andMap)
import QS
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (Shared)
import Spa.Page exposing (Page)
import UI.Collegue exposing (getCollegues)
import UI.Demande
import UI.Echange
import UI.Layout
import UI.Rappel
import UI.Suivi exposing (SuiviResult(..))
import User
import View exposing (View)


type alias Model =
    { suivis : WebData Suivis
    , devecos : WebData (List Collegue)
    , suiviModel : UI.Suivi.Model
    , currentUrl : String
    }


type alias Suivis =
    { echanges : List Echange
    , demandes : List Demande
    , rappels : List Rappel
    , brouillons : List Brouillon
    }


type Msg
    = SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | ReceivedGetSuivisResponse (WebData Suivis)
    | ReceivedCollegues (WebData (List Collegue))
    | SuiviMsg UI.Suivi.Msg
    | UpdateOnglet ( UI.Suivi.Onglet, Maybe UI.Suivi.Onglet )


page : Shared -> Shared.User -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page shared user =
    Spa.Page.onNewFlags (queryToOngletAndAjout >> UpdateOnglet) <|
        Spa.Page.element
            { view = view user shared
            , init = init user
            , update = update
            , subscriptions = \_ -> Sub.none
            }


init : Shared.User -> Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init user query =
    let
        ( ongletSuivi, ongletAjout ) =
            queryToOngletAndAjout query

        nextUrl =
            ongletToQuery ongletSuivi

        ongletAjoutEffect =
            UI.Suivi.viewOngletAjout ongletAjout
    in
    Shared.pageChangeEffects <|
        if Data.Role.isDeveco <| User.role user then
            ( { suivis = RD.Loading
              , devecos = RD.Loading
              , suiviModel = UI.Suivi.init UI.Suivi.OngletBrouillons (Just ongletSuivi)
              , currentUrl = nextUrl
              }
            , Effect.batch
                [ Effect.fromCmd getSuivis
                , Effect.fromCmd <| getCollegues SharedMsg (Just LogError) ReceivedCollegues
                , Effect.map SuiviMsg ongletAjoutEffect
                ]
            )

        else
            ( { suivis = RD.NotAsked
              , devecos = RD.NotAsked
              , suiviModel = UI.Suivi.init UI.Suivi.OngletBrouillons Nothing
              , currentUrl = nextUrl
              }
            , Effect.fromShared <| Shared.ReplaceUrl <| Route.Dashboard Nothing
            )


getSuivis : Cmd Msg
getSuivis =
    get
        { url = Api.getSuivisEquipe }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedGetSuivisResponse
        }
        decodeSuivis


suiviConfig : UI.Suivi.SuiviConfig Suivis
suiviConfig =
    { decoder = decodeSuivis
    , upsertRappelUrl = UI.Rappel.updateRappelForEntite True
    , upsertEchangeUrl = UI.Echange.updateEchangeForEntite True
    , upsertDemandeUrl = UI.Demande.updateDemandeForEntite True
    , upsertBrouillonUrl = Api.upsertBrouillon
    , clotureRappelUrl = UI.Rappel.clotureRappelForEntite True
    , clotureDemandeUrl = UI.Demande.clotureDemandeForEntite True
    , reouvertureRappelUrl = UI.Rappel.reouvertureRappelForEntite True
    , reouvertureDemandeUrl = UI.Demande.reouvertureDemandeForEntite False
    }


decodeSuivis : Decode.Decoder Suivis
decodeSuivis =
    Decode.succeed Suivis
        |> andMap (Decode.field "echanges" <| Decode.list Data.Echange.decodeEchange)
        |> andMap (Decode.field "demandes" <| Decode.list Data.Demande.decodeDemande)
        |> andMap (Decode.field "rappels" <| Decode.list Data.Rappel.decodeRappel)
        |> andMap (Decode.field "brouillons" <| Decode.list Data.Brouillon.decodeBrouillon)


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        SuiviMsg suiviMsg ->
            let
                ( suiviModel, suiviEffect, suivis ) =
                    UI.Suivi.update suiviConfig suiviMsg model.suiviModel

                ongletSuivi =
                    UI.Suivi.getOngletActif suiviModel

                ( newSuivis, suivisEffect ) =
                    case suivis of
                        NoResult ->
                            ( model.suivis, Effect.none )

                        Result s ->
                            ( RD.Success s, Effect.none )

                        Refetch ->
                            ( RD.Loading, Effect.fromCmd getSuivis )

                ( newModel, effect ) =
                    { model
                        | suiviModel = suiviModel
                        , suivis = newSuivis
                    }
                        |> updateUrl ongletSuivi
            in
            ( newModel
            , Effect.batch [ effect, Effect.map SuiviMsg <| suiviEffect, suivisEffect ]
            )

        ReceivedGetSuivisResponse suivis ->
            { model | suivis = suivis }
                |> Effect.withNone

        ReceivedCollegues response ->
            { model | devecos = response }
                |> Effect.withNone

        UpdateOnglet ( ongletSuivi, _ ) ->
            let
                ( newModel, effect ) =
                    updateUrl ongletSuivi model
            in
            ( newModel
            , effect
            )


view : Shared.User -> Shared -> Model -> View Msg
view user shared model =
    { title = UI.Layout.pageTitle "Suivis"
    , body = [ body user shared model ]
    , route =
        Route.Suivis <|
            case model.currentUrl of
                "" ->
                    Nothing

                _ ->
                    Just model.currentUrl
    }


body : Shared.User -> Shared -> Model -> Html Msg
body user { timezone, now } model =
    div [ class "flex flex-col gap-8" ]
        [ Accessibility.map SuiviMsg <| Lazy.lazy8 UI.Suivi.viewSuivisModal timezone now user Nothing model.devecos True [] model.suiviModel
        , case model.suivis of
            RD.Success { echanges, demandes, rappels, brouillons } ->
                Accessibility.map SuiviMsg <| UI.Suivi.viewOngletSuivis timezone now False True user Nothing echanges demandes rappels (Just brouillons) model.suiviModel

            RD.Loading ->
                div [ class "p-4 sm:p-8 fr-card--white italic" ]
                    [ text "Chargement des suivis en cours..." ]

            _ ->
                div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ text "Une erreur s'est produite, veuillez réessayer." ]
        ]


queryKeys :
    { suivi : String
    , ajout : String
    }
queryKeys =
    { suivi = "suivi"
    , ajout = "ajout"
    }


queryToOngletAndAjout : Maybe String -> ( UI.Suivi.Onglet, Maybe UI.Suivi.Onglet )
queryToOngletAndAjout rawQuery =
    rawQuery
        |> Maybe.withDefault ""
        |> QS.parse QS.config
        |> (\query ->
                let
                    ongletSuivi =
                        query
                            |> QS.getAsStringList queryKeys.suivi
                            |> List.head
                            |> Maybe.andThen UI.Suivi.stringToOnglet
                            |> Maybe.withDefault UI.Suivi.OngletBrouillons

                    ongletAjout =
                        query
                            |> QS.getAsStringList queryKeys.ajout
                            |> List.head
                            |> Maybe.map ((==) "oui")
                            |> Maybe.withDefault False
                            |> (\ajout ->
                                    if ajout then
                                        if ongletSuivi == UI.Suivi.OngletBrouillons then
                                            Just UI.Suivi.OngletBrouillons

                                        else
                                            Nothing

                                    else
                                        Nothing
                               )
                in
                ( ongletSuivi, ongletAjout )
           )


ongletToQuery : UI.Suivi.Onglet -> String
ongletToQuery ongletSuivi =
    let
        setOngletSuivi =
            ongletSuivi
                |> UI.Suivi.ongletToString
                |> QS.setStr queryKeys.suivi
    in
    QS.empty
        |> setOngletSuivi
        |> QS.serialize (QS.config |> QS.addQuestionMark False)


updateUrl : UI.Suivi.Onglet -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
updateUrl ongletSuivi model =
    let
        ( newSuiviModel, suiviCmd, _ ) =
            UI.Suivi.changeOnglet suiviConfig model.suiviModel ongletSuivi

        newOngletSuivi =
            UI.Suivi.getOngletActif newSuiviModel

        nextUrl =
            ongletToQuery newOngletSuivi

        requestChanged =
            nextUrl /= model.currentUrl

        ongletCmd =
            Effect.fromShared <|
                Shared.Navigate <|
                    Route.Suivis <|
                        ((\q ->
                            if q == "" then
                                Nothing

                            else
                                Just q
                         )
                         <|
                            nextUrl
                        )
    in
    if requestChanged then
        ( { model
            | suiviModel = newSuiviModel
            , currentUrl = nextUrl
          }
        , Effect.batch
            [ ongletCmd
            , Effect.map SuiviMsg suiviCmd
            ]
        )

    else
        model
            |> Effect.withNone
