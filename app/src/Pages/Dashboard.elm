module Pages.Dashboard exposing (Model, Msg, page)

import Accessibility exposing (Html, decorativeImg, div, h1, p, span, text)
import Api
import Api.Auth exposing (get)
import DSFR.Button
import DSFR.Card
import DSFR.Grid
import DSFR.Tile
import DSFR.Typography as Typo
import Data.Equipe
import Data.Role
import Date exposing (Date)
import Effect
import Html
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Lib.UI exposing (numberTile, plural)
import Lib.Variables exposing (contactEmail)
import Pages.Etablissements.Filters exposing (Procedure(..), emptyFilters, filtersAndPaginationToQuery)
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import Time
import UI.Cgu
import UI.Layout
import UI.Pagination exposing (defaultPagination)
import Url.Builder as Builder
import User
import View exposing (View)


type alias Model =
    { stats : WebData Stats
    , viewActive : Bool
    , redirect : Maybe String
    , cgu : UI.Cgu.Model
    }


type alias Stats =
    { etablissementsOuverts : Int
    , etablissementsFermes : Int
    , etablissementsProcedure : Int
    , echanges : Int
    , demandes : Int
    , rappels : Int
    , mesRappelsAVenir : Int
    , mesRappelsEnRetard : Int
    }


type Msg
    = ReceivedStats (WebData Stats)
    | CguMsg UI.Cgu.Msg
    | SharedMsg Shared.Msg


page : Shared.Shared -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page { identity, now, timezone } =
    Spa.Page.element
        { view = view now timezone identity
        , init = init identity
        , update = update
        , subscriptions = \_ -> Sub.none
        }


init : Maybe User -> Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init identity query =
    let
        ( cguModel, cguEffect ) =
            case identity of
                Nothing ->
                    ( UI.Cgu.init, Effect.none )

                Just user ->
                    if User.cgu user then
                        ( UI.Cgu.init, Effect.none )

                    else
                        UI.Cgu.update UI.Cgu.open UI.Cgu.init
                            |> (\( cM, cE ) ->
                                    ( cM
                                    , Effect.batch
                                        [ cE
                                        , Effect.fromCmd UI.Cgu.displayModal
                                        ]
                                    )
                               )
    in
    ( { stats = RD.Loading
      , viewActive = True
      , redirect = query
      , cgu = cguModel
      }
    , Effect.batch
        [ case identity of
            Nothing ->
                Effect.fromSharedCmd <|
                    Api.Auth.checkAuth True query

            Just user ->
                if User.cgu user then
                    Shared.autoriseAccesDeveco user query <|
                        Effect.fromCmd getStats

                else
                    Effect.none
        , Effect.map CguMsg cguEffect
        ]
    )
        |> Shared.pageChangeEffects


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        ReceivedStats response ->
            { model | stats = response }
                |> Effect.withNone

        CguMsg cguMsg ->
            let
                ( cguModel, cguEffect ) =
                    UI.Cgu.update cguMsg model.cgu
            in
            ( { model | cgu = cguModel }, Effect.map CguMsg cguEffect )

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg


view : Time.Posix -> Time.Zone -> Maybe User -> Model -> View Msg
view now timezone identity model =
    { title =
        UI.Layout.pageTitle <|
            Maybe.withDefault "Outil pour développeur économique" <|
                Maybe.map (\_ -> "Tableau de bord") <|
                    identity
    , body = [ body now timezone identity model ]
    , route = Route.Dashboard model.redirect
    }


body : Time.Posix -> Time.Zone -> Maybe User -> Model -> Html Msg
body now timezone identity model =
    div []
        [ Html.map CguMsg <| UI.Cgu.viewModal model.cgu
        , identity
            |> Maybe.map (viewUserDashboard now timezone model)
            |> Maybe.withDefault viewLanding
        ]


viewLanding : Html msg
viewLanding =
    div [ class "flex flex-col fr-card--white" ]
        [ div [ DSFR.Grid.gridRow, class "p-4 sm:p-8" ]
            [ div [ DSFR.Grid.colOffset1, DSFR.Grid.col3, class "" ]
                [ decorativeImg [ Html.Attributes.src "/assets/deveco-accueil.svg" ] ]
            , div [ DSFR.Grid.col7, class "flex" ]
                [ div [ class "self-center" ]
                    [ h1 [ Typo.fr_h4 ] [ text "Bienvenue sur Deveco\u{00A0}!" ]
                    , p [] [ text "Ce service facilite l'accès et la gestion des données des entreprises pour les développeurs économiques au sein des collectivités locales." ]
                    , DSFR.Button.new { label = "Accéder à mon compte", onClick = Nothing }
                        |> DSFR.Button.linkButton (Route.toUrl <| Route.Connexion Nothing)
                        |> DSFR.Button.primary
                        |> DSFR.Button.view
                    ]
                ]
            ]
        , div [ DSFR.Grid.gridRow, class "fr-background-contrast--blue-ecume p-8 sm:p-16" ]
            [ div [ DSFR.Grid.colOffset1, DSFR.Grid.col6 ]
                [ text "Gagnez en efficacité pour mieux accompagner le développement économique de votre territoire." ]
            , div [ DSFR.Grid.colOffset1, DSFR.Grid.col3 ]
                [ let
                    emails =
                        [ contactEmail ]

                    subject =
                        Builder.string "subject" <|
                            "Demande d'information sur Dévéco"

                    mailto =
                        "mailto:"
                            ++ String.join "," emails
                            ++ Builder.toQuery [ subject ]
                  in
                  DSFR.Button.new { label = "Contactez-nous", onClick = Nothing }
                    |> DSFR.Button.linkButton mailto
                    |> DSFR.Button.view
                ]
            ]
        , div [ class "p-4 sm:p-8" ]
            [ div [ DSFR.Grid.gridRow, class "p-2 sm:p-4" ]
                [ div [ DSFR.Grid.colOffset1, DSFR.Grid.col10, class "fr-card--white" ]
                    [ div [ DSFR.Grid.gridRow, class "flex items-end justify-end" ]
                        [ div [ DSFR.Grid.col4, class "px-8" ]
                            [ decorativeImg [ Html.Attributes.src "/assets/deveco-information.svg" ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ decorativeImg [ Html.Attributes.src "/assets/deveco-connaissance.svg" ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ decorativeImg [ Html.Attributes.src "/assets/deveco-pilotage.svg" ]
                            ]
                        ]
                    ]
                ]
            , div [ DSFR.Grid.gridRow, class "p-2 sm:p-4" ]
                [ div [ DSFR.Grid.colOffset1, DSFR.Grid.col10, class "fr-card--white" ]
                    [ div [ DSFR.Grid.gridRow ]
                        [ div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [ Typo.textBold ] [ text "Partage d’infos entre agents d’une même équipe" ] ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [ Typo.textBold ] [ text "Connaissance du tissu économique du territoire" ] ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [ Typo.textBold ] [ text "Pilotage de l’activité" ] ]
                            ]
                        ]
                    ]
                ]
            , div [ DSFR.Grid.gridRow, class "p-2 sm:p-4" ]
                [ div [ DSFR.Grid.colOffset1, DSFR.Grid.col10, class "fr-card--white" ]
                    [ div [ DSFR.Grid.gridRow ]
                        [ div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [] [ text "Centralisez le suivi de la relation aux établissements et créateurs d'entreprises du territoire. Gérez les locaux d’activité vacants." ] ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [] [ text "Explorez le tissu économique local à partir des données fiables et actualisées des institutions publiques." ] ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [] [ text "Suivez et analysez l’activité des services de développement économique." ] ]
                            ]
                        ]
                    ]
                ]
            ]
        ]


viewUserDashboard : Time.Posix -> Time.Zone -> Model -> User -> Html Msg
viewUserDashboard now timezone model user =
    let
        today =
            Date.fromPosix timezone now
    in
    div [ class "p-2 flex flex-col gap-8" ] <|
        [ viewRoleAndTerritory user
        , viewBlocsLiens
        , viewActualitesTerritoire today model.stats
        , div [ class "flex flex-row" ]
            [ div [ class "w-[60%] pr-4" ] [ viewActualitesService model.stats ]
            , div [ class "w-[40%] pl-4" ] [ viewActualitesPerso model.stats ]
            ]
        ]


viewActualitesPerso : WebData Stats -> Html Msg
viewActualitesPerso stats =
    div [ class "px-4" ]
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "flex flex-col gap-4" ]
            [ div [ DSFR.Grid.col12 ]
                [ h1 [ Typo.fr_h4, class "!mb-0" ] [ text "Mon activité" ]
                , text "Mes rappels"
                ]
            , div []
                [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ div [ DSFR.Grid.col12, DSFR.Grid.colLg6 ]
                        [ numberCardWithPlaceholderDefault
                            stats
                            .mesRappelsEnRetard
                            (\rappels ->
                                numberTile
                                    (Just <| Route.toUrl <| Route.Suivis <| Just "suivi=rappels")
                                    [ "En retard" ]
                                    "Rappels"
                                    Lib.UI.Rouge
                                    rappels
                            )
                        ]
                    , div [ DSFR.Grid.col12, DSFR.Grid.colLg6 ]
                        [ numberCardWithPlaceholderDefault
                            stats
                            .mesRappelsAVenir
                            (\rappels ->
                                numberTile
                                    (Just <| Route.toUrl <| Route.Suivis <| Just "suivi=rappels")
                                    [ "À venir" ]
                                    "Rappels"
                                    Lib.UI.Bleu
                                    rappels
                            )
                        ]
                    ]
                ]
            ]
        ]


viewBlocsLiens : Html msg
viewBlocsLiens =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12, DSFR.Grid.colLg3 ]
            [ DSFR.Tile.vertical
                { title = text "Rechercher un établissement"
                , description = Nothing
                , link = Just <| Route.toUrl <| Route.Etablissements Nothing
                }
                |> DSFR.Tile.withPicto "personnalise/compass.svg"
                |> DSFR.Tile.view
            ]
        , div [ DSFR.Grid.col12, DSFR.Grid.colLg3 ]
            [ DSFR.Tile.vertical
                { title = text "Ajouter un créateur"
                , description = Nothing
                , link = Just <| Route.toUrl <| Route.CreateurNew
                }
                |> DSFR.Tile.withPicto "personnalise/avatar.svg"
                |> DSFR.Tile.view
            ]
        , div [ DSFR.Grid.col12, DSFR.Grid.colLg3 ]
            [ DSFR.Tile.vertical
                { title = text "Rechercher un local"
                , description = Nothing
                , link = Just <| Route.toUrl <| Route.Locaux Nothing
                }
                |> DSFR.Tile.withPicto "personnalise/compagnie.svg"
                |> DSFR.Tile.view
            ]
        , div [ DSFR.Grid.col12, DSFR.Grid.colLg3 ]
            [ DSFR.Tile.vertical
                { title = text "Saisir un brouillon"
                , description = Nothing
                , link =
                    Just <|
                        Route.toUrl <|
                            Route.Suivis <|
                                Just "suivi=brouillons&ajout=oui"
                }
                |> DSFR.Tile.withPicto "personnalise/conclusion.svg"
                |> DSFR.Tile.view
            ]
        ]


viewRoleAndTerritory : User -> Html msg
viewRoleAndTerritory user =
    div [ class "fr-card--white p-4 flex flex-row justify-between items-center" ]
        [ div [ class "flex flex-col" ]
            [ h1 [ Typo.fr_h4, class "!mb-0" ]
                [ text <|
                    "Bonjour "
                        ++ User.prenom user
                ]
            , user
                |> User.role
                |> Data.Role.equipe
                |> Maybe.map
                    (\equipe ->
                        span []
                            [ span [] [ text <| "Votre équipe de référence est\u{00A0}:" ]
                            , text " "
                            , span [ Typo.textBold ] [ text <| Data.Equipe.nom equipe ]
                            ]
                    )
                |> Maybe.withDefault (text "Vous êtes Superadmin")
            ]
        , [ DSFR.Button.new
                { label = "Voir les nouveautés"
                , onClick = Nothing
                }
                |> DSFR.Button.withAttrs [ class "!mb-0" ]
                |> DSFR.Button.linkButtonExternal Lib.Variables.hrefNouveautes
                |> DSFR.Button.secondary
          , DSFR.Button.new
                { label = "Découvrir Deveco"
                , onClick = Nothing
                }
                |> DSFR.Button.withAttrs [ class "!mb-0" ]
                |> DSFR.Button.linkButtonExternal Lib.Variables.lienWebinaireFormation
                |> DSFR.Button.tertiary
          ]
            |> DSFR.Button.group
            |> DSFR.Button.inline
            |> DSFR.Button.alignedRight
            |> DSFR.Button.viewGroup
            |> List.singleton
            |> div []
        ]


numberCardWithPlaceholderDefault : WebData data -> (data -> prop) -> (prop -> Html Msg) -> Html Msg
numberCardWithPlaceholderDefault webdata accessor viewer =
    case RD.map accessor webdata of
        RD.Success data ->
            viewer data

        _ ->
            placeholderCard


viewActualitesService : WebData Stats -> Html Msg
viewActualitesService stats =
    div [ class "px-4" ]
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "flex flex-col gap-4" ]
            [ div [ DSFR.Grid.col12 ]
                [ h1 [ Typo.fr_h4, class "!mb-0" ] [ text "Actualités du service" ]
                , text "sur les 5 derniers jours"
                ]
            , div []
                [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ div [ DSFR.Grid.col12, DSFR.Grid.colLg6, DSFR.Grid.colXl4 ]
                        [ numberCardWithPlaceholderDefault
                            stats
                            .echanges
                            (\echanges ->
                                numberTile
                                    (Just <| Route.toUrl <| Route.Suivis <| Just "suivi=echanges")
                                    [ "Création" ++ plural echanges ]
                                    ("Échange" ++ plural echanges)
                                    Lib.UI.Bleu
                                    echanges
                            )
                        ]
                    , div [ DSFR.Grid.col12, DSFR.Grid.colLg6, DSFR.Grid.colXl4 ]
                        [ numberCardWithPlaceholderDefault
                            stats
                            .demandes
                            (\demandes ->
                                numberTile
                                    (Just <| Route.toUrl <| Route.Suivis <| Just "suivi=demandes")
                                    [ "Création" ++ plural demandes ]
                                    ("Demande" ++ plural demandes)
                                    Lib.UI.Bleu
                                    demandes
                            )
                        ]
                    , div [ DSFR.Grid.col12, DSFR.Grid.colLg6, DSFR.Grid.colXl4 ]
                        [ numberCardWithPlaceholderDefault
                            stats
                            .rappels
                            (\rappels ->
                                numberTile
                                    (Just <| Route.toUrl <| Route.Suivis <| Just "suivi=rappels")
                                    [ "Création" ++ plural rappels ]
                                    ("Rappel" ++ plural rappels)
                                    Lib.UI.Bleu
                                    rappels
                            )
                        ]
                    ]
                ]
            ]
        ]


viewActualitesTerritoire : Date -> WebData Stats -> Html Msg
viewActualitesTerritoire now stats =
    let
        ilYA5Jours =
            Date.add Date.Days -5 now
    in
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "flex flex-col" ]
        [ div [ DSFR.Grid.col12 ]
            [ h1 [ Typo.fr_h4, class "!mb-0" ] [ text "Actualités du territoire" ]
            , text "sur les 5 derniers jours"
            ]
        , div [ DSFR.Grid.col12 ]
            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col12, DSFR.Grid.colLg4 ]
                    [ numberCardWithPlaceholderDefault
                        stats
                        .etablissementsOuverts
                        (\count ->
                            numberTile
                                (Just <|
                                    Route.toUrl <|
                                        Route.Etablissements <|
                                            (\q ->
                                                if q == "" then
                                                    Nothing

                                                else
                                                    Just q
                                            )
                                            <|
                                                filtersAndPaginationToQuery <|
                                                    ( { emptyFilters | creeApres = Just ilYA5Jours, creeAvant = Just now }, defaultPagination )
                                )
                                [ "Création" ++ plural count ]
                                ("Établissement" ++ plural count)
                                Lib.UI.Bleu
                                count
                        )
                    ]
                , div [ DSFR.Grid.col12, DSFR.Grid.colLg4 ]
                    [ numberCardWithPlaceholderDefault
                        stats
                        .etablissementsFermes
                        (\count ->
                            numberTile
                                (Just <|
                                    Route.toUrl <|
                                        Route.Etablissements <|
                                            (\q ->
                                                if q == "" then
                                                    Nothing

                                                else
                                                    Just q
                                            )
                                            <|
                                                filtersAndPaginationToQuery <|
                                                    ( { emptyFilters | fermeApres = Just ilYA5Jours, fermeAvant = Just now }, defaultPagination )
                                )
                                [ "Fermeture" ++ plural count ]
                                ("Établissement" ++ plural count)
                                Lib.UI.Rouge
                                count
                        )
                    ]
                , div
                    [ DSFR.Grid.col12
                    , DSFR.Grid.colLg4
                    ]
                    [ numberCardWithPlaceholderDefault
                        stats
                        .etablissementsProcedure
                        (\count ->
                            numberTile
                                (Just <|
                                    Route.toUrl <|
                                        Route.Etablissements <|
                                            (\q ->
                                                if q == "" then
                                                    Nothing

                                                else
                                                    Just q
                                            )
                                            <|
                                                filtersAndPaginationToQuery <|
                                                    ( { emptyFilters | procedures = [ ProcedureOui ], proceduresApres = Just ilYA5Jours, proceduresAvant = Just now }, defaultPagination )
                                )
                                [ "Procédure" ++ plural count ++ " collective" ++ plural count ]
                                ("Établissement" ++ plural count ++ " siège")
                                Lib.UI.Orange
                                count
                        )
                    ]
                ]
            ]
        ]


getStats : Cmd Msg
getStats =
    get
        { url = Api.getStats }
        { toShared = SharedMsg
        , logger = Nothing
        , handler = ReceivedStats
        }
        decodeStats


decodeStats : Decoder Stats
decodeStats =
    Decode.succeed Stats
        |> andMap (Decode.field "etablissementsOuverts" <| Decode.int)
        |> andMap (Decode.field "etablissementsFermes" <| Decode.int)
        |> andMap (Decode.field "etablissementsProcedure" <| Decode.int)
        |> andMap (Decode.field "echanges" <| Decode.int)
        |> andMap (Decode.field "demandes" <| Decode.int)
        |> andMap (Decode.field "rappels" <| Decode.int)
        |> andMap (Decode.field "rappelsAVenir" <| Decode.int)
        |> andMap (Decode.field "rappelsEnRetard" <| Decode.int)


placeholderCard : Html msg
placeholderCard =
    DSFR.Card.card nothing DSFR.Card.vertical
        |> DSFR.Card.withArrow False
        |> DSFR.Card.withNoTitle
        |> DSFR.Card.withDescription
            (Just <|
                div [ class "text-center" ] <|
                    [ div [ Typo.textBold, Typo.fr_h1 ] <|
                        [ greyPlaceholder 1
                        ]
                    , greyPlaceholder 4
                    , greyPlaceholder 4
                    ]
            )
        |> DSFR.Card.view


greyPlaceholder : Int -> Html msg
greyPlaceholder length =
    "\u{00A0}"
        |> List.repeat length
        |> List.map (text >> List.singleton >> span [ class "w-[1rem] inline-block" ])
        |> span [ class "grey-background m-1" ]
        |> List.singleton
        |> div [ class "pulse-black" ]
