module Pages.Locaux exposing (Model, Msg, emptyFilters, filtersAndPaginationToQuery, page)

import Accessibility exposing (div, h2, label, option, p, select, span, sup, text)
import Api
import Api.Auth exposing (get, post, request)
import DSFR.Accordion
import DSFR.Button
import DSFR.Checkbox
import DSFR.Grid as Grid
import DSFR.Icons
import DSFR.Icons.Buildings
import DSFR.Icons.System
import DSFR.Input
import DSFR.Modal
import DSFR.Pagination
import DSFR.Radio
import DSFR.SegmentedControl
import DSFR.Tag
import DSFR.Tooltip
import DSFR.Typography as Typo
import Data.Adresse exposing (ApiAdresse, decodeApiFeatures)
import Data.Commune exposing (Commune)
import Data.Local as Local exposing (LocalSimple)
import Data.PortefeuilleInfos exposing (PortefeuilleInfos, decodePortefeuilleInfos)
import Date exposing (Date)
import Effect
import Html exposing (Html, h1)
import Html.Attributes exposing (class)
import Html.Attributes.Extra exposing (empty)
import Html.Events as Events
import Html.Extra exposing (nothing, viewIf, viewMaybe)
import Html.Keyed as Keyed
import Html.Lazy
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import Lib.Date
import Lib.UI exposing (filterBy, formatIntWithThousandSpacing, loggingDecoder, plural)
import Lib.Umap
import Lib.Variables exposing (hintResultatsTerritoires)
import MultiSelect
import MultiSelectRemote
import NaturalOrdering
import Pages.Etablissements.Filters as Filters
import QS
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import SingleSelect
import SingleSelectRemote
import Spa.Page exposing (Page)
import UI.Layout
import UI.Local
import UI.Pagination exposing (Pagination, defaultPagination, paginationToQuery, queryToPagination)
import Url.Builder
import View exposing (View)


page : Shared.Shared -> User -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page { appUrl } _ =
    Spa.Page.onNewFlags (queryToFiltersAndPagination >> DoSearch) <|
        Spa.Page.element <|
            { init = init
            , update = update
            , view = view appUrl
            , subscriptions = subscriptions
            }


type alias Model =
    { locaux : WebData (List ( LocalSimple, PortefeuilleInfos ))
    , locauxRequests : Int
    , filters : Filters
    , filtersSource : WebData FiltersSource
    , cartoFeatures : WebData CartoFeatures
    , mapInfo : Maybe MapInfo
    , viewMode : ViewMode
    , selectAdresse : SingleSelectRemote.SmartSelect Msg ApiAdresse
    , pagination : Pagination
    , currentUrl : String
    , rechercheSauvegardeeRequest : WebData String
    , selectAdresseCommune : SingleSelectRemote.SmartSelect Msg Commune
    , selectAdresseVoie : SingleSelect.SmartSelect Msg (Maybe String)
    , selectAdresseNumero : SingleSelect.SmartSelect Msg (Maybe String)
    , selectCadastreSection : SingleSelect.SmartSelect Msg (Maybe String)
    , selectCadastreParcelle : SingleSelect.SmartSelect Msg (Maybe String)
    , selectEtages : MultiSelect.SmartSelect Msg Etage
    , selectNatures : MultiSelect.SmartSelect Msg LocalNature
    , selectCategories : MultiSelect.SmartSelect Msg LocalCategorie
    , selectCommunes : MultiSelectRemote.SmartSelect Msg Commune
    , selectEpcis : MultiSelectRemote.SmartSelect Msg Epci
    , selectDepartements : MultiSelectRemote.SmartSelect Msg Departement
    , selectRegions : MultiSelectRemote.SmartSelect Msg Region
    , selectQpvs : MultiSelectRemote.SmartSelect Msg Qpv
    , selectTis : MultiSelectRemote.SmartSelect Msg TerritoireIndustrie
    , natures : WebData (List LocalNature)
    , categories : WebData (List LocalCategorie)
    , voies : WebData (List String)
    , numeros : WebData (List String)
    , sections : WebData (List String)
    , parcelles : WebData (List String)
    , selectZones : MultiSelect.SmartSelect Msg String
    , selectMots : MultiSelect.SmartSelect Msg String
    , geoJsonExport : Maybe GeoJsonFormat
    , excelExportRequested : Bool
    , filtresRequete : FiltresRequete
    }


type alias MapInfo =
    { center : ( Float, Float )
    , south : Float
    , west : Float
    , north : Float
    , east : Float
    , zoom : Float
    }


type ViewMode
    = Liste
    | Carte


viewModes : List ViewMode
viewModes =
    [ Liste
    , Carte
    ]


viewModeToId : ViewMode -> String
viewModeToId viewMode =
    case viewMode of
        Liste ->
            "liste"

        Carte ->
            "carte"


viewModeToDisplay : ViewMode -> String
viewModeToDisplay viewMode =
    case viewMode of
        Liste ->
            "Liste"

        Carte ->
            "Carte"


stringToAccompagnes : String -> Maybe Accompagnes
stringToAccompagnes s =
    case s of
        "oui" ->
            Just AccompagnesOui

        "non" ->
            Just AccompagnesNon

        _ ->
            Nothing


accompagnesToString : Accompagnes -> String
accompagnesToString s =
    case s of
        AccompagnesOui ->
            "oui"

        AccompagnesNon ->
            "non"


type alias Etage =
    { id : String, nom : String }


type alias LocalNature =
    { id : String, nom : String }


type alias LocalCategorie =
    { id : String, nom : String }


type alias LocalSources =
    { natures : List LocalNature
    , categories : List LocalCategorie
    }


type alias Filters =
    { nom : String
    , proprietaire : String
    , invariant : String
    , etages : List Etage
    , natures : List LocalNature
    , categories : List LocalCategorie
    , surfaces : Surfaces
    , vacances : List Vacances
    , communes : List Commune
    , epcis : List Epci
    , departements : List Departement
    , regions : List Region
    , qpvs : List Qpv
    , tousQpvs : Bool
    , tis : List TerritoireIndustrie
    , tousTis : Bool
    , adresseType : AdresseType
    , adresseCommune : Maybe Commune
    , adresseVoie : Maybe String
    , adresseNumero : Maybe String
    , cadastreSection : Maybe String
    , cadastreParcelle : Maybe String
    , orderBy : OrderBy
    , orderDirection : OrderDirection
    , accompagnes : List Accompagnes
    , accompagnesApres : Maybe Date
    , accompagnesAvant : Maybe Date
    , zones : List String
    , mots : List String
    , favoris : List Bool
    }


type Accompagnes
    = AccompagnesOui
    | AccompagnesNon


type AdresseType
    = AdresseVoie
    | AdresseParcelle


adresseTypeToDisplay : AdresseType -> String
adresseTypeToDisplay adresseType =
    case adresseType of
        AdresseVoie ->
            "Par adresse"

        AdresseParcelle ->
            "Par parcelle"


adresseTypeToString : AdresseType -> String
adresseTypeToString adresseType =
    case adresseType of
        AdresseVoie ->
            "adresse-voie"

        AdresseParcelle ->
            "adresse-parcelle"


type Vacances
    = VOccupe
    | VVacant
    | VNonRenseigne


vacancesToDisplay : Vacances -> String
vacancesToDisplay vacances =
    case vacances of
        VOccupe ->
            "Occupé"

        VVacant ->
            "Vacant"

        VNonRenseigne ->
            "Non-renseigné"


vacancesToString : Vacances -> String
vacancesToString vacances =
    case vacances of
        VOccupe ->
            "occupe"

        VVacant ->
            "vacant"

        VNonRenseigne ->
            "nr"


stringToVacances : String -> Maybe Vacances
stringToVacances string =
    case string of
        "occupe" ->
            Just VOccupe

        "vacant" ->
            Just VVacant

        "nr" ->
            Just VNonRenseigne

        _ ->
            Nothing


emptyFilters : Filters
emptyFilters =
    { nom = ""
    , proprietaire = ""
    , invariant = ""
    , etages = []
    , natures = []
    , categories = []
    , surfaces = defaultSurfaces
    , vacances = []
    , communes = []
    , epcis = []
    , departements = []
    , regions = []
    , qpvs = []
    , tousQpvs = False
    , tis = []
    , tousTis = False
    , adresseType = AdresseVoie
    , adresseCommune = Nothing
    , adresseVoie = Nothing
    , adresseNumero = Nothing
    , cadastreSection = Nothing
    , cadastreParcelle = Nothing
    , orderBy = LocalConsultation
    , orderDirection = Desc
    , accompagnes = []
    , accompagnesApres = Nothing
    , accompagnesAvant = Nothing
    , zones = []
    , mots = []
    , favoris = []
    }


type alias FiltersSource =
    { zones : List String
    , mots : List String
    }


defaultFiltersSource : FiltersSource
defaultFiltersSource =
    { zones = []
    , mots = []
    }


type alias Surfaces =
    { totale : MinMax
    , vente : MinMax
    , reserve : MinMax
    , exterieure : MinMax
    , stationnementCouverte : MinMax
    , stationnementNonCouverte : MinMax
    }


type SurfaceType
    = STotale
    | SVente
    | SReserve
    | SExterieure
    | SStationnementCouverte
    | SStationnementNonCouverte


surfaceTypes : List SurfaceType
surfaceTypes =
    [ STotale
    , SVente
    , SReserve
    , SExterieure
    , SStationnementCouverte
    , SStationnementNonCouverte
    ]


defaultSurfaces : Surfaces
defaultSurfaces =
    { totale = defaultMinMax
    , vente = defaultMinMax
    , reserve = defaultMinMax
    , exterieure = defaultMinMax
    , stationnementCouverte = defaultMinMax
    , stationnementNonCouverte = defaultMinMax
    }


surfaceTypeToDisplay : SurfaceType -> String
surfaceTypeToDisplay surfaceType =
    case surfaceType of
        STotale ->
            "Surface totale (P1 + P2 + P3)"

        SVente ->
            "Surface de vente (P1)"

        SReserve ->
            "Surface de réserve (P2)"

        SExterieure ->
            "Surface extérieure non couverte (P3)"

        SStationnementCouverte ->
            "Surface de stationnement couverte"

        SStationnementNonCouverte ->
            "Surface de stationnement non couverte"


surfaceTypeToString : SurfaceType -> String
surfaceTypeToString surfaceType =
    case surfaceType of
        STotale ->
            "totale"

        SVente ->
            "vente"

        SReserve ->
            "reserve"

        SExterieure ->
            "exterieure"

        SStationnementCouverte ->
            "stationnement-couvert"

        SStationnementNonCouverte ->
            "stationnement-non-couvert"


type alias MinMax =
    { min : Maybe String
    , max : Maybe String
    }


type MinMaxType
    = Min
    | Max


defaultMinMax : MinMax
defaultMinMax =
    { min = Nothing
    , max = Nothing
    }


minMaxTypeToDisplay : MinMaxType -> String
minMaxTypeToDisplay minMaxType =
    case minMaxType of
        Min ->
            "Minimum"

        Max ->
            "Maximum"


minMaxTypeToString : MinMaxType -> String
minMaxTypeToString minMaxType =
    case minMaxType of
        Min ->
            "min"

        Max ->
            "max"


type OrderBy
    = LocalConsultation
    | Alphabetique


orderByToString : OrderBy -> String
orderByToString orderBy =
    case orderBy of
        LocalConsultation ->
            "consultation"

        Alphabetique ->
            "alphabetique"


stringToOrderBy : String -> Maybe OrderBy
stringToOrderBy orderBy =
    case orderBy of
        "consultation" ->
            Just LocalConsultation

        "alphabetique" ->
            Just Alphabetique

        _ ->
            Nothing


type OrderDirection
    = Desc
    | Asc


orderDirectionToString : OrderDirection -> String
orderDirectionToString orderDirection =
    case orderDirection of
        Asc ->
            "asc"

        Desc ->
            "desc"


stringToOrderDirection : String -> Maybe OrderDirection
stringToOrderDirection orderDirection =
    case orderDirection of
        "asc" ->
            Just Asc

        "desc" ->
            Just Desc

        _ ->
            Nothing


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | ReceivedFiltersSource (WebData FiltersSource)
    | UpdatedNom String
    | UpdatedProprietaire String
    | UpdatedInvariant String
    | ReceivedLocaux (WebData ( List ( LocalSimple, PortefeuilleInfos ), Pagination, FiltresRequete ))
    | ReceivedVoies (WebData (List String))
    | ReceivedNumeros (WebData (List String))
    | ReceivedCadastreSections (WebData (List String))
    | ReceivedCadastreParcelles (WebData (List String))
    | ClearAllFilters
    | DoSearch ( Filters, Pagination )
    | ClickedSearch
    | UpdateUrl ( Filters, Pagination )
      -- Selects
    | UpdatedAdresseType AdresseType
    | SelectedAdresseCommune ( Commune, SingleSelectRemote.Msg Commune )
    | UpdatedSelectAdresseCommune (SingleSelectRemote.Msg Commune)
    | SelectedAdresseVoie ( Maybe String, SingleSelect.Msg (Maybe String) )
    | ClearSelectAdresseCommune
    | UpdatedSelectAdresseVoie (SingleSelect.Msg (Maybe String))
    | SelectedAdresseNumero ( Maybe String, SingleSelect.Msg (Maybe String) )
    | UpdatedSelectAdresseNumero (SingleSelect.Msg (Maybe String))
    | SelectedCadastreSection ( Maybe String, SingleSelect.Msg (Maybe String) )
    | UpdatedSelectCadastreSection (SingleSelect.Msg (Maybe String))
    | SelectedCadastreParcelle ( Maybe String, SingleSelect.Msg (Maybe String) )
    | UpdatedSelectCadastreParcelle (SingleSelect.Msg (Maybe String))
    | SelectedEtages ( List Etage, MultiSelect.Msg Etage )
    | UpdatedSelectEtages (MultiSelect.Msg Etage)
    | UnselectEtage Etage
    | SelectedZones ( List String, MultiSelect.Msg String )
    | UpdatedSelectZones (MultiSelect.Msg String)
    | UnselectZone String
    | SelectedMots ( List String, MultiSelect.Msg String )
    | UpdatedSelectMots (MultiSelect.Msg String)
    | UnselectMot String
    | ReceivedSources (WebData LocalSources)
    | SelectedNatures ( List LocalNature, MultiSelect.Msg LocalNature )
    | UpdatedSelectNatures (MultiSelect.Msg LocalNature)
    | UnselectNature LocalNature
    | SelectedCategories ( List LocalCategorie, MultiSelect.Msg LocalCategorie )
    | UpdatedSelectCategories (MultiSelect.Msg LocalCategorie)
    | UnselectCategorie LocalCategorie
    | UpdatedSurface SurfaceType MinMaxType String
    | SetVacanceFilter Bool Vacances
    | SetFavoriFilter Bool Bool
    | SelectedCommunes ( List Commune, MultiSelectRemote.Msg Commune )
    | UpdatedSelectCommunes (MultiSelectRemote.Msg Commune)
    | UnselectCommune Commune
    | SelectedEpcis ( List Epci, MultiSelectRemote.Msg Epci )
    | UpdatedSelectEpcis (MultiSelectRemote.Msg Epci)
    | UnselectEpci Epci
    | SelectedDepartements ( List Departement, MultiSelectRemote.Msg Departement )
    | UpdatedSelectDepartements (MultiSelectRemote.Msg Departement)
    | UnselectDepartement Departement
    | SelectedRegions ( List Region, MultiSelectRemote.Msg Region )
    | UpdatedSelectRegions (MultiSelectRemote.Msg Region)
    | UnselectRegion Region
    | SelectedQpvs ( List Qpv, MultiSelectRemote.Msg Qpv )
    | UpdatedSelectQpvs (MultiSelectRemote.Msg Qpv)
    | UnselectQpv Qpv
    | ToggleAllQpvs Bool
    | SelectedTis ( List TerritoireIndustrie, MultiSelectRemote.Msg TerritoireIndustrie )
    | UpdatedSelectTis (MultiSelectRemote.Msg TerritoireIndustrie)
    | UnselectTi TerritoireIndustrie
    | ToggleAllTis Bool
    | ClickedOrderBy OrderBy OrderDirection
    | ClickedExportExcel
    | ClickedExportGeoJson
    | ClickedCanceledGeoJson
    | RequestedRechercheSauvegardee
    | ReceivedRechercheSauvegardee (WebData String)
    | ToggleFavorite String Bool
    | GotFavoriteResult String Bool
    | SetAccompagnesFilter Bool Accompagnes
    | UpdateAccompagnesDate AccompagnesDate (Maybe Date)
      -- carto
    | ChangeViewMode ViewMode
    | UpdatedMapInfo MapInfo
    | SelectedAdresse ( ApiAdresse, SingleSelectRemote.Msg ApiAdresse )
    | ReceivedCartoFeatures (WebData CartoFeatures)
    | UpdatedSelectAdresse (SingleSelectRemote.Msg ApiAdresse)


type AccompagnesDate
    = AccompagnesApres
    | AccompagnesAvant


init : Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init rawQuery =
    let
        locauxRequests =
            1

        ( filters, pagination ) =
            queryToFiltersAndPagination rawQuery

        selectAdresseCommune =
            SingleSelectRemote.init selectAdresseCommuneId
                { selectionMsg = SelectedAdresseCommune
                , internalMsg = UpdatedSelectAdresseCommune
                , characterSearchThreshold = 0
                , debounceDuration = 400
                }

        selectAdresseVoie =
            SingleSelect.init selectAdresseVoieId
                { selectionMsg = SelectedAdresseVoie
                , internalMsg = UpdatedSelectAdresseVoie
                }

        selectAdresseNumero =
            SingleSelect.init selectAdresseNumeroId
                { selectionMsg = SelectedAdresseNumero
                , internalMsg = UpdatedSelectAdresseNumero
                }

        selectCadastreSection =
            SingleSelect.init selectCadastreSectionId
                { selectionMsg = SelectedCadastreSection
                , internalMsg = UpdatedSelectCadastreSection
                }

        selectCadastreParcelle =
            SingleSelect.init selectCadastreParcelleId
                { selectionMsg = SelectedCadastreParcelle
                , internalMsg = UpdatedSelectCadastreParcelle
                }

        selectEtages =
            MultiSelect.init "select-etage"
                { selectionMsg = SelectedEtages
                , internalMsg = UpdatedSelectEtages
                }

        selectNatures =
            MultiSelect.init "select-nature"
                { selectionMsg = SelectedNatures
                , internalMsg = UpdatedSelectNatures
                }

        selectCategories =
            MultiSelect.init "select-categorie"
                { selectionMsg = SelectedCategories
                , internalMsg = UpdatedSelectCategories
                }

        selectAdresse =
            SingleSelectRemote.init "champ-selection-adresse-maplibre"
                { selectionMsg = SelectedAdresse
                , internalMsg = UpdatedSelectAdresse
                , characterSearchThreshold = 3
                , debounceDuration = selectDebounceDuration
                }

        nextUrl =
            filtersAndPaginationToQuery ( filters, pagination )
    in
    ( { locaux = RD.Loading
      , cartoFeatures = RD.NotAsked
      , mapInfo = Nothing
      , viewMode = Liste
      , selectAdresse = selectAdresse
      , locauxRequests = locauxRequests
      , filters = filters
      , filtersSource = RD.Loading
      , pagination = pagination
      , currentUrl = nextUrl
      , rechercheSauvegardeeRequest = RD.NotAsked
      , selectAdresseCommune = selectAdresseCommune
      , selectAdresseVoie = selectAdresseVoie
      , selectAdresseNumero = selectAdresseNumero
      , selectCadastreParcelle = selectCadastreParcelle
      , selectCadastreSection = selectCadastreSection
      , selectEtages = selectEtages
      , selectNatures = selectNatures
      , selectCategories = selectCategories
      , selectCommunes =
            MultiSelectRemote.init selectCommunesId
                { selectionMsg = SelectedCommunes
                , internalMsg = UpdatedSelectCommunes
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectEpcis =
            MultiSelectRemote.init selectEpcisId
                { selectionMsg = SelectedEpcis
                , internalMsg = UpdatedSelectEpcis
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectDepartements =
            MultiSelectRemote.init selectDepartementsId
                { selectionMsg = SelectedDepartements
                , internalMsg = UpdatedSelectDepartements
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectRegions =
            MultiSelectRemote.init selectRegionsId
                { selectionMsg = SelectedRegions
                , internalMsg = UpdatedSelectRegions
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectQpvs =
            MultiSelectRemote.init selectQpvsId
                { selectionMsg = SelectedQpvs
                , internalMsg = UpdatedSelectQpvs
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectTis =
            MultiSelectRemote.init selectTisId
                { selectionMsg = SelectedTis
                , internalMsg = UpdatedSelectTis
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , natures = RD.Loading
      , categories = RD.Loading
      , voies = RD.NotAsked
      , numeros = RD.NotAsked
      , sections = RD.NotAsked
      , parcelles = RD.NotAsked
      , selectZones =
            MultiSelect.init selectZonesId
                { selectionMsg = SelectedZones
                , internalMsg = UpdatedSelectZones
                }
      , selectMots =
            MultiSelect.init selectMotsId
                { selectionMsg = SelectedMots
                , internalMsg = UpdatedSelectMots
                }
      , geoJsonExport = Nothing
      , excelExportRequested = False
      , filtresRequete = defaultFiltresRequete
      }
    , Effect.batch
        [ Effect.fromCmd <| getLocaux locauxRequests <| nextUrl
        , Effect.fromCmd <| getSources
        , Effect.fromCmd <| getFiltersSource
        , filters.adresseCommune
            |> Maybe.map .id
            |> Maybe.map getAdresseVoies
            |> Maybe.withDefault Effect.none
        , Maybe.map2 getAdresseNumeros (filters.adresseCommune |> Maybe.map .id) filters.adresseVoie
            |> Maybe.withDefault Effect.none
        ]
    )
        |> Shared.pageChangeEffects


getLocauxTracker : Int -> String
getLocauxTracker count =
    "get-locaux-tracker" ++ "-" ++ String.fromInt count


getLocauxCartoTracker : Int -> String
getLocauxCartoTracker count =
    "get-locaux-carto-tracker" ++ "-" ++ String.fromInt count


getLocaux : Int -> String -> Cmd Msg
getLocaux trackerCount nextUrl =
    request
        { method = "GET"
        , headers = []
        , url = Api.getLocaux <| nextUrl
        , body = Http.emptyBody
        , timeout = Nothing
        , tracker = Just <| getLocauxTracker trackerCount
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedLocaux
        }
        decodePayload


getCartoFeatures : Int -> String -> Maybe MapInfo -> Cmd Msg
getCartoFeatures trackerCount nextUrl mapInfo =
    let
        zoom =
            mapInfo
                |> Maybe.map (\m -> "&z=" ++ String.fromFloat m.zoom)
                |> Maybe.withDefault ""

        bounds =
            mapInfo
                |> Maybe.map
                    (\{ north, west, south, east } ->
                        "&nwse="
                            ++ ([ north, west, south, east ]
                                    |> List.map String.fromFloat
                                    |> String.join ","
                               )
                    )
                |> Maybe.withDefault ""
    in
    request
        { method = "GET"
        , headers = []
        , url = Api.getLocaux <| nextUrl ++ "&affichage=geo" ++ zoom ++ bounds
        , body = Http.emptyBody
        , timeout = Nothing
        , tracker = Just <| getLocauxCartoTracker trackerCount
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedCartoFeatures
        }
        (decodeCartoFeatures trackerCount)


type alias CartoFeatures =
    { total : Int
    , features : String
    , territoire : Maybe Contour
    , qpvs : List Contour
    , zonages : List Contour
    , requete : Int
    , adresse : Maybe ApiAdresse
    }


type alias Contour =
    { id : String
    , nom : String
    , geometrie : String
    , boundingBox : String
    }


decodeCartoFeatures : Int -> Decoder CartoFeatures
decodeCartoFeatures tracker =
    Decode.succeed CartoFeatures
        |> andMap (Decode.field "total" Decode.int)
        |> andMap (Decode.field "features" (Decode.nullable Decode.string) |> Decode.map (Maybe.withDefault ""))
        |> andMap (Decode.field "territoire" <| Decode.nullable decodeContour)
        |> andMap (Decode.field "qpvs" <| Decode.list decodeContour)
        |> andMap (Decode.field "zonages" <| Decode.list decodeContour)
        |> andMap (Decode.succeed tracker)
        |> andMap (Decode.succeed Nothing)


decodeContour : Decoder Contour
decodeContour =
    Decode.succeed Contour
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "geometrie" Decode.string)
        |> andMap (Decode.field "box" Decode.string)


decodePayload : Decoder ( List ( LocalSimple, PortefeuilleInfos ), Pagination, FiltresRequete )
decodePayload =
    Decode.succeed (\p pages results data fr -> ( data, Pagination p pages results, fr ))
        |> andMap (Decode.field "page" Decode.int)
        |> andMap (Decode.field "pages" Decode.int)
        |> andMap (Decode.field "total" Decode.int)
        |> andMap (Decode.field "elements" <| Decode.list <| decodeLocalAvecPortefeuille)
        |> andMap (Decode.field "filtres" decodeFiltresRequete)


decodeLocalAvecPortefeuille : Decoder ( LocalSimple, PortefeuilleInfos )
decodeLocalAvecPortefeuille =
    Decode.succeed Tuple.pair
        |> andMap Local.decodeLocalSimple
        |> andMap (Decode.field "portefeuilleInfos" decodePortefeuilleInfos)


getSources : Cmd Msg
getSources =
    get
        { url = Api.getLocalSources
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSources
        }
        (Decode.succeed LocalSources
            |> andMap (Decode.field "natures" <| Decode.list <| decodeIdLabel)
            |> andMap (Decode.field "categories" <| Decode.list <| decodeIdLabel)
        )


decodeIdLabel : Decoder { id : String, nom : String }
decodeIdLabel =
    Decode.succeed (\id nom -> { id = id, nom = nom })
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "nom" Decode.string)


rechercherLocalNomInputName : String
rechercherLocalNomInputName =
    "rechercher-local-nom"


rechercherLocalProprietaireInputName : String
rechercherLocalProprietaireInputName =
    "rechercher-local-proprietaire"


rechercherLocalInvariantInputName : String
rechercherLocalInvariantInputName =
    "rechercher-local-invariant"


view : String -> Model -> View Msg
view appUrl model =
    { title = UI.Layout.pageTitle <| "Mes locaux"
    , body = UI.Layout.lazyBody (body appUrl) model
    , route = Route.Locaux Nothing
    }


body : String -> Model -> Html Msg
body appUrl model =
    case ( showingUnfilteredResults model.currentUrl, model.locaux ) of
        ( True, RD.Success [] ) ->
            div [ class "fr-card--white text-center p-20" ]
                [ div [ Grid.gridRow ]
                    [ text "Aucun local trouvé. C'est sans doute une erreur de notre côté, veuillez nous contacter\u{00A0}!"
                    ]
                ]

        _ ->
            let
                results =
                    model.pagination |> .results
            in
            div [ class "flex flex-col gap-4 py-4" ]
                [ viewGeoJsonModal appUrl model.currentUrl results model.rechercheSauvegardeeRequest model.geoJsonExport
                , div [ class "flex flex-col gap-4 lg:flex-row" ]
                    [ h1 [ class "fr-h6 !mb-0" ]
                        [ DSFR.Icons.iconLG DSFR.Icons.Buildings.storeLine
                        , text "\u{00A0}"
                        , text "Locaux"
                        ]
                    ]
                , filtersPanel model
                , Html.Lazy.lazy8 viewLocalList model.viewMode model.selectAdresse model.cartoFeatures model.excelExportRequested model.pagination model.filters model.locaux model.currentUrl
                ]


viewLocalList : ViewMode -> SingleSelectRemote.SmartSelect Msg ApiAdresse -> WebData CartoFeatures -> Bool -> Pagination -> Filters -> WebData (List ( LocalSimple, PortefeuilleInfos )) -> String -> Html Msg
viewLocalList viewMode selectAdresse cartoFeatures excelExportRequested pagination filters locaux currentUrl =
    case locaux of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            case viewMode of
                Carte ->
                    nothing

                _ ->
                    div []
                        [ Html.Lazy.lazy6 viewExportBoutons viewMode excelExportRequested locaux filters pagination currentUrl
                        , div [ Grid.gridRow ] <|
                            List.repeat 3 <|
                                UI.Local.cardPlaceholder
                        ]

        RD.Failure _ ->
            div [ class "text-center", Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Une erreur s'est produite, veuillez recharger la page."
                    ]
                ]

        RD.Success [] ->
            div [ class "fr-card--white h-[250px] text-center p-20" ]
                [ text "Aucun local ne correspond à cette recherche."
                ]

        RD.Success liste ->
            div []
                [ Html.Lazy.lazy6 viewExportBoutons viewMode excelExportRequested locaux filters pagination currentUrl
                , case viewMode of
                    Liste ->
                        div []
                            [ liste
                                |> List.map (\( local, portefeuilleInfos ) -> UI.Local.carte { toggleFavorite = Just ToggleFavorite } local portefeuilleInfos)
                                |> div [ Grid.gridRow ]
                            , if pagination.pages > 1 then
                                div [ class "!mt-4" ]
                                    [ DSFR.Pagination.view pagination.page (min 500 pagination.pages) <|
                                        toHref ( filters, pagination )
                                    ]

                              else
                                nothing
                            ]

                    Carte ->
                        nothing
                , Keyed.node "div"
                    [ if viewMode /= Carte then
                        class "hidden"

                      else
                        empty
                    , class "w-full"
                    ]
                    [ ( "carto", Html.Lazy.lazy2 viewLocauxCarte selectAdresse cartoFeatures ) ]
                ]


viewLocauxCarte : SingleSelectRemote.SmartSelect Msg ApiAdresse -> WebData CartoFeatures -> Html Msg
viewLocauxCarte selectAdresse cartoFeatures =
    let
        total =
            cartoFeatures
                |> RD.toMaybe
                |> Maybe.map (.total >> Encode.int)
                |> Maybe.withDefault Encode.null

        features =
            cartoFeatures
                |> RD.toMaybe
                |> Maybe.map (.features >> Encode.string)
                |> Maybe.withDefault Encode.null

        encodeContour { id, nom, boundingBox, geometrie } =
            [ ( "boundingBox", boundingBox |> Encode.string )
            , ( "geometrie", geometrie |> Encode.string )
            , ( "nom", nom |> Encode.string )
            , ( "id", id |> Encode.string )
            ]
                |> Encode.object

        territoireData =
            Maybe.withDefault Encode.null <|
                Maybe.map encodeContour <|
                    Maybe.andThen .territoire <|
                        RD.toMaybe <|
                            cartoFeatures

        qpvsData =
            RD.withDefault Encode.null <|
                RD.map (Encode.list encodeContour) <|
                    RD.map .qpvs <|
                        cartoFeatures

        zonages =
            RD.withDefault Encode.null <|
                RD.map (Encode.list encodeContour) <|
                    RD.map .zonages <|
                        cartoFeatures

        requete =
            RD.withDefault Encode.null <|
                RD.map Encode.int <|
                    RD.map .requete <|
                        cartoFeatures

        adresse =
            Maybe.withDefault Encode.null <|
                Maybe.map
                    (\{ coordinates, type_ } ->
                        case coordinates of
                            Just ( longitude, latitude ) ->
                                Encode.object
                                    [ ( "center"
                                      , Encode.object
                                            [ ( "lon", Encode.float longitude )
                                            , ( "lat", Encode.float latitude )
                                            ]
                                      )
                                    , ( "type", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| type_ )
                                    ]

                            Nothing ->
                                Encode.null
                    )
                <|
                    Maybe.andThen .adresse <|
                        RD.toMaybe <|
                            cartoFeatures

        data =
            [ ( "features", features )
            , ( "total", total )
            , ( "territoire", territoireData )
            , ( "qpvs", qpvsData )
            , ( "zonages", zonages )
            , ( "requete", requete )
            ]
    in
    div [ class "w-full h-[50rem]" ]
        [ adresseCartoFilter selectAdresse
        , Html.node "maplibre-locaux"
            [ data
                |> Encode.object
                |> Encode.encode 0
                |> Html.Attributes.attribute "data"
            , [ ( "requete", requete )
              ]
                |> Encode.object
                |> Encode.encode 0
                |> Html.Attributes.attribute "requete"
            , adresse
                |> Encode.encode 0
                |> Html.Attributes.attribute "adresse"
            , Events.on "updatedmap" <| loggingDecoder decodeMapInfo
            , Html.Attributes.id "carto"
            , class "block h-[700px]"
            ]
            []
        ]


adresseCartoFilter : SingleSelectRemote.SmartSelect Msg ApiAdresse -> Html Msg
adresseCartoFilter selectAdresse =
    div [ class "flex flex-col gap-2 relative z-20" ]
        [ div [ class "absolute top-[1em] left-[1em] w-[50%]" ]
            [ selectAdresse
                |> SingleSelectRemote.viewCustom
                    { isDisabled = False
                    , selected = Nothing
                    , optionLabelFn = .label
                    , optionDescriptionFn = \_ -> ""
                    , optionsContainerMaxHeight = 300
                    , selectTitle = nothing
                    , searchPrompt = "Rechercher une ville, une adresse"
                    , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                    , noResultsForMsg =
                        \searchText ->
                            "Aucune autre adresse n'a été trouvée"
                                ++ (if searchText == "" then
                                        ""

                                    else
                                        " pour " ++ searchText
                                   )
                    , noOptionsMsg = "Aucune adresse n'a été trouvée"
                    , error = Nothing
                    }
            ]
        ]


viewExportBoutons : ViewMode -> Bool -> WebData (List ( LocalSimple, PortefeuilleInfos )) -> Filters -> Pagination -> String -> Html Msg
viewExportBoutons viewMode excelExportRequested request filters pagination currentUrl =
    let
        results =
            pagination.results
    in
    div [ class "flex flex-col gap-4 mb-4" ]
        [ div [ class "flex flex-col gap-4 lg:flex-row lg:items-center" ]
            [ div [ class "font-bold" ] <|
                case request of
                    RD.Loading ->
                        [ greyPlaceholder 20 ]

                    _ ->
                        [ text (formatIntWithThousandSpacing results)
                        , text " résultat"
                        , viewIf (results > 1) (text "s")
                        ]
            , div [ class "flex flex-col gap-2 items-start lg:flex-row lg:ml-auto" ]
                [ let
                    exportUrl =
                        (Api.getLocaux <|
                            currentUrl
                        )
                            ++ "&format=xlsx"
                  in
                  DSFR.Button.new
                    { onClick = Just ClickedExportExcel
                    , label = "Exporter (Excel)"
                    }
                    |> DSFR.Button.linkButtonExternal exportUrl
                    |> DSFR.Button.secondary
                    |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                    |> DSFR.Button.withDisabled (results == 0 || excelExportRequested)
                    |> DSFR.Button.view
                , DSFR.Button.new
                    { label = "Exporter (GeoJSON)"
                    , onClick = Just <| ClickedExportGeoJson
                    }
                    |> DSFR.Button.secondary
                    |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                    |> DSFR.Button.withDisabled (results == 0)
                    |> DSFR.Button.view
                ]
            ]
        , div [ class "flex flex-row justify-between items-baseline gap-4" ] <|
            [ div [ class "fr-card--white" ]
                [ DSFR.SegmentedControl.raw
                    { legend = "Choix de l'affichage"
                    , selected = viewMode
                    , toId = viewModeToId
                    , toDisplay = viewModeToDisplay
                    , toDisabled = \_ -> Nothing
                    , options = viewModes
                    , selectMsg = ChangeViewMode
                    }
                ]
            , div [ class "flex flex-row items-baseline gap-4" ] <|
                case request of
                    RD.Loading ->
                        [ greyPlaceholder 20 ]

                    _ ->
                        let
                            selectLabel =
                                "select-sort"
                        in
                        [ label [ class "fr-label", Html.Attributes.for selectLabel ] [ text "Tri" ]
                        , div
                            [ class "fr-select-group"
                            ]
                            [ selectOrder selectLabel filters
                            ]
                        ]
            ]
        ]


nomFilter : String -> Html Msg
nomFilter nom =
    div []
        [ DSFR.Input.new
            { value = nom
            , onInput = UpdatedNom
            , label =
                span
                    [ Typo.textBold
                    ]
                    [ text "Recherche par nom" ]
            , name = rechercherLocalNomInputName
            }
            |> DSFR.Input.view
        ]


proprietaireFilter : String -> Html Msg
proprietaireFilter proprietaire =
    div []
        [ DSFR.Input.new
            { value = proprietaire
            , onInput = UpdatedProprietaire
            , label =
                span
                    [ Typo.textBold
                    ]
                    [ text "Recherche par propriétaire" ]
            , name = rechercherLocalProprietaireInputName
            }
            |> DSFR.Input.withHint [ text "SIREN, nom de la personne, nom de l'entreprise" ]
            |> DSFR.Input.view
        ]


invariantFilter : String -> Html Msg
invariantFilter invariant =
    div []
        [ DSFR.Input.new
            { value = invariant
            , onInput = UpdatedInvariant
            , label =
                span
                    [ Typo.textBold
                    ]
                    [ text "Recherche par numéro d'invariant (10 chiffres)" ]
            , name = rechercherLocalInvariantInputName
            }
            |> DSFR.Input.withTextInputAttrs (Just { minlength = Just 10, maxlength = Just 10 })
            |> DSFR.Input.view
        ]


filtersPanel : Model -> Html Msg
filtersPanel ({ selectAdresseCommune, selectAdresseVoie, selectAdresseNumero, selectCadastreSection, selectCadastreParcelle, filters, pagination, currentUrl, voies, numeros, sections, parcelles } as model) =
    div [ class "fr-card--white p-4 flex flex-col gap-4" ]
        [ Html.form [ class "flex flex-col gap-4", Events.onSubmit ClickedSearch ]
            [ div [ class "mb-4" ]
                [ adresseFilter voies numeros sections parcelles selectAdresseCommune selectAdresseVoie selectAdresseNumero selectCadastreSection selectCadastreParcelle filters.adresseType filters.adresseCommune filters.adresseVoie filters.adresseNumero filters.cadastreSection filters.cadastreParcelle
                ]
            , div []
                [ Html.Lazy.lazy filtresPublics model
                , Html.Lazy.lazy filtresPerso model
                ]
            , DSFR.Button.new { label = "Rechercher", onClick = Nothing }
                |> DSFR.Button.withDisabled (currentUrl == filtersAndPaginationToQuery ( filters, pagination ))
                |> DSFR.Button.withAttrs [ class "!w-full justify-center" ]
                |> DSFR.Button.submit
                |> DSFR.Button.large
                |> DSFR.Button.view
                |> List.singleton
                |> div [ class "flex flex-row w-full mt-4" ]
            ]
        , if hasFilterTags currentUrl then
            div [ Html.Attributes.id listFiltersTagsId ]
                [ div [ class "flex flex-row !mt-2" ]
                    [ showFilterTags model
                    , div [ class "ml-auto shrink-0" ] [ clearAllFiltersButton currentUrl ]
                    ]
                ]

          else
            text ""
        ]


filtresPublics : Model -> Html Msg
filtresPublics { selectEtages, selectNatures, selectCategories, selectCommunes, selectEpcis, selectDepartements, selectRegions, selectQpvs, selectTis, natures, categories, filters } =
    let
        selectedPublicFilters =
            (if filters.invariant /= "" then
                1

             else
                0
            )
                + (if filters.proprietaire /= "" then
                    1

                   else
                    0
                  )
                + List.length filters.natures
                + List.length filters.categories
                + List.length filters.etages
                + List.length filters.communes
                + List.length filters.epcis
                + List.length filters.departements
                + List.length filters.regions
                + List.length filters.qpvs
                + List.length filters.tis
                + (if filters.tousQpvs then
                    1

                   else
                    0
                  )
                + (if filters.tousTis then
                    1

                   else
                    0
                  )
                + (filters.surfaces.totale.min |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.surfaces.totale.max |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.surfaces.vente.min |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.surfaces.vente.max |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.surfaces.reserve.min |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.surfaces.reserve.max |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.surfaces.exterieure.min |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.surfaces.exterieure.max |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.surfaces.stationnementCouverte.min |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.surfaces.stationnementCouverte.max |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.surfaces.stationnementNonCouverte.min |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.surfaces.stationnementNonCouverte.max |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
    in
    DSFR.Accordion.raw
        { id = "filtres-donnees-publiques"
        , title =
            [ h2 [ Typo.fr_h6, class "flex flex-row gap-4" ]
                [ span [] [ text "Filtres données publiques" ]
                , if selectedPublicFilters > 0 then
                    span [ class "circled" ]
                        [ text <| formatIntWithThousandSpacing <| selectedPublicFilters
                        ]

                  else
                    nothing
                ]
            ]
        , borderless = False
        , content =
            [ div [ class "flex flex-col lg:flex-row gap-9" ]
                [ div [ class "lg:basis-1/2 flex flex-col" ]
                    [ div [ class "flex flex-col gap-9" ]
                        [ Html.Lazy.lazy proprietaireFilter filters.proprietaire
                        , Html.Lazy.lazy invariantFilter filters.invariant
                        , Html.Lazy.lazy3 categorieFilter (categories |> RD.withDefault []) selectCategories filters.categories
                        , Html.Lazy.lazy3 natureFilter (natures |> RD.withDefault []) selectNatures filters.natures
                        , Html.Lazy.lazy2 etageFilter selectEtages filters.etages
                        , Html.Lazy.lazy3 communesFilter filters.adresseCommune selectCommunes filters.communes
                        , Html.Lazy.lazy2 epcisFilter selectEpcis filters.epcis
                        , Html.Lazy.lazy2 departementsFilter selectDepartements filters.departements
                        , Html.Lazy.lazy2 regionsFilter selectRegions filters.regions
                        , Html.Lazy.lazy3 qpvsFilter selectQpvs filters.tousQpvs filters.qpvs
                        , Html.Lazy.lazy3 tisFilter selectTis filters.tousTis filters.tis
                        ]
                    ]
                , div [ class "lg:basis-1/2 flex flex-col" ]
                    [ div [ class "flex flex-col gap-9" ] <|
                        List.map (Html.Lazy.lazy2 bornesSurfaces filters.surfaces) <|
                            surfaceTypes
                    ]
                ]
            ]
        }


filtresPerso : Model -> Html Msg
filtresPerso { filtersSource, selectZones, selectMots, filters } =
    let
        { zones, mots } =
            case filtersSource of
                RD.Success sources ->
                    sources

                _ ->
                    defaultFiltersSource

        selectedPersoFilters =
            (if filters.nom /= "" then
                1

             else
                0
            )
                + List.length filters.vacances
                + List.length filters.zones
                + List.length filters.mots
                + List.length filters.favoris
    in
    DSFR.Accordion.raw
        { id = "filtres-donnees-perso"
        , title =
            [ h2 [ Typo.fr_h6, class "flex flex-row gap-4" ]
                [ span [] [ text "Filtres données personnalisées" ]
                , if selectedPersoFilters > 0 then
                    span [ class "circled" ]
                        [ text <| formatIntWithThousandSpacing <| selectedPersoFilters
                        ]

                  else
                    nothing
                ]
            ]
        , borderless = False
        , content =
            [ div [ class "flex flex-col lg:flex-row w-full gap-9" ]
                [ div [ class "flex flex-col w-full gap-9" ]
                    [ Html.Lazy.lazy nomFilter filters.nom
                    , Html.Lazy.lazy vacanceFilter filters.vacances
                    ]
                , div [ class "flex flex-col w-full gap-9" ]
                    [ Html.Lazy.lazy3 accompagnesFilter filters.accompagnes filters.accompagnesApres filters.accompagnesAvant
                    ]
                , div [ class "flex flex-col w-full gap-9" ]
                    [ Html.Lazy.lazy3 zonesFilter selectZones zones filters.zones
                    , Html.Lazy.lazy3 motsFilter selectMots mots filters.mots
                    , Html.Lazy.lazy favorisFilter filters.favoris
                    ]
                ]
            ]
        }


vacanceFilter : List Vacances -> Html Msg
vacanceFilter vacances =
    DSFR.Checkbox.group
        { id = "filtres-vacance"
        , values =
            [ VOccupe
            , VVacant
            , VNonRenseigne
            ]
        , toLabel = text << vacancesToDisplay
        , toId = vacancesToString
        , checked = vacances
        , valueAsString = vacancesToString
        , onChecked = \data bool -> SetVacanceFilter bool data
        , label =
            div [ Typo.textBold ]
                [ text "Vacance du local"
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


optionsEtages : List Etage
optionsEtages =
    [ { id = "00", nom = "00" }
    , { id = "01", nom = "01" }
    , { id = "02", nom = "02" }
    , { id = "03", nom = "03" }
    , { id = "04", nom = "04" }
    , { id = "05", nom = "05" }
    , { id = "06", nom = "06" }
    , { id = "07", nom = "07" }
    , { id = "08", nom = "08" }
    , { id = "09", nom = "09" }
    , { id = "10", nom = "10" }
    , { id = "11", nom = "11" }
    , { id = "12", nom = "12" }
    , { id = "13", nom = "13" }
    , { id = "14", nom = "14" }
    , { id = "15", nom = "15" }
    , { id = "16", nom = "16" }
    , { id = "17", nom = "17" }
    , { id = "18", nom = "18" }
    , { id = "19", nom = "19" }
    , { id = "20", nom = "20" }
    , { id = "21", nom = "21" }
    , { id = "22", nom = "22" }
    , { id = "23", nom = "23" }
    , { id = "24", nom = "24" }
    , { id = "25", nom = "25" }
    , { id = "26", nom = "26" }
    , { id = "27", nom = "27" }
    , { id = "28", nom = "28" }
    , { id = "29", nom = "29" }
    , { id = "30", nom = "30" }
    , { id = "31", nom = "31" }
    , { id = "32", nom = "32" }
    , { id = "33", nom = "33" }
    , { id = "34", nom = "34" }
    , { id = "35", nom = "35" }
    , { id = "36", nom = "36" }
    , { id = "37", nom = "37" }
    , { id = "38", nom = "38" }
    , { id = "39", nom = "39" }
    , { id = "44", nom = "44" }
    , { id = "51", nom = "51" }
    , { id = "67", nom = "67" }
    , { id = "80", nom = "80" }
    , { id = "81", nom = "81" }
    , { id = "82", nom = "82" }
    , { id = "83", nom = "83" }
    , { id = "84", nom = "84" }
    , { id = "85", nom = "85" }
    , { id = "86", nom = "86" }
    , { id = "88", nom = "88" }
    , { id = "91", nom = "91" }
    ]


etageFilter : MultiSelect.SmartSelect Msg Etage -> List Etage -> Html Msg
etageFilter selectEtages etages =
    div [ class "flex flex-col gap-2" ]
        [ selectEtages
            |> MultiSelect.viewCustom
                { isDisabled = optionsEtages |> List.length |> (==) 0
                , selected = etages
                , options = optionsEtages |> List.sortBy (.nom >> String.toLower)
                , optionLabelFn = .nom
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Niveau étage du local"
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucun autre niveau n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun niveau n'a été trouvé"
                , searchFn = filterBy .nom
                , searchPrompt = "Filtrer par niveau"
                }
        , case etages of
            [] ->
                nothing

            _ ->
                etages
                    |> List.map (\etage -> DSFR.Tag.deletable UnselectEtage { data = etage, toString = .nom })
                    |> DSFR.Tag.medium
        ]


bornesSurfaces : Surfaces -> SurfaceType -> Html Msg
bornesSurfaces surfaces surfaceType =
    let
        maybeMin =
            getSurfaceTypeValue surfaces Min surfaceType
                |> Maybe.andThen String.toInt

        maybeMax =
            getSurfaceTypeValue surfaces Max surfaceType
                |> Maybe.andThen String.toInt

        invalidRange =
            case ( maybeMin, maybeMax ) of
                ( Nothing, Nothing ) ->
                    False

                ( Just _, Nothing ) ->
                    False

                ( Nothing, Just _ ) ->
                    False

                ( Just min, Just max ) ->
                    min > max
    in
    div []
        [ div [ Typo.textBold ] [ text <| surfaceTypeToDisplay <| surfaceType, text " (en m²)" ]
        , div [ class "flex flex-row justify-between" ]
            [ borneSurfaceInput invalidRange surfaceType Min surfaces
            , borneSurfaceInput invalidRange surfaceType Max surfaces
            ]
        ]


borneSurfaceInput : Bool -> SurfaceType -> MinMaxType -> Surfaces -> Html Msg
borneSurfaceInput invalidRange surfaceType minMaxType surfaces =
    let
        value =
            Maybe.withDefault "" <|
                getSurfaceTypeValue surfaces minMaxType surfaceType
    in
    DSFR.Input.new
        { value = value
        , label = text <| minMaxTypeToDisplay <| minMaxType
        , name = "surface" ++ "-" ++ surfaceTypeToString surfaceType ++ "-" ++ minMaxTypeToString minMaxType
        , onInput = UpdatedSurface surfaceType minMaxType
        }
        |> DSFR.Input.withError
            (if invalidRange then
                Just [ text "Intervalle incohérent" ]

             else
                Nothing
            )
        |> DSFR.Input.withExtraAttrs [ class "!mb-0" ]
        |> DSFR.Input.number (Just { step = Nothing, min = Nothing, max = Nothing })
        |> DSFR.Input.view


categorieFilter : List LocalCategorie -> MultiSelect.SmartSelect Msg LocalCategorie -> List LocalCategorie -> Html Msg
categorieFilter optionsCategories selectCategories categories =
    div [ class "flex flex-col gap-2" ]
        [ selectCategories
            |> MultiSelect.viewCustom
                { isDisabled = optionsCategories |> List.length |> (==) 0
                , selected = categories
                , options = optionsCategories |> List.sortBy (.nom >> String.toLower)
                , optionLabelFn = \{ id, nom } -> nom ++ " (" ++ id ++ ")"
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Catégories"
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre catégorie n'a été trouvée"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucune catégorie n'a été trouvée"
                , searchFn = filterBy .nom
                , searchPrompt = "Filtrer par catégorie de local"
                }
        , case categories of
            [] ->
                nothing

            _ ->
                categories
                    |> List.map (\categorie -> DSFR.Tag.deletable UnselectCategorie { data = categorie, toString = .nom })
                    |> DSFR.Tag.medium
        ]


natureFilter : List LocalNature -> MultiSelect.SmartSelect Msg LocalNature -> List LocalNature -> Html Msg
natureFilter optionsNatures selectNatures natures =
    div [ class "flex flex-col gap-2" ]
        [ selectNatures
            |> MultiSelect.viewCustom
                { isDisabled = optionsNatures |> List.length |> (==) 0
                , selected = natures
                , options = optionsNatures |> List.sortBy (.nom >> String.toLower)
                , optionLabelFn = .nom
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Natures"
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre nature n'a été trouvée"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucune nature n'a été trouvée"
                , searchFn = filterBy .nom
                , searchPrompt = "Filtrer par nature de local"
                }
        , case natures of
            [] ->
                nothing

            _ ->
                natures
                    |> List.map (\nature -> DSFR.Tag.deletable UnselectNature { data = nature, toString = .nom })
                    |> DSFR.Tag.medium
        ]


adresseFilter : WebData (List String) -> WebData (List String) -> WebData (List String) -> WebData (List String) -> SingleSelectRemote.SmartSelect Msg Commune -> SingleSelect.SmartSelect Msg (Maybe String) -> SingleSelect.SmartSelect Msg (Maybe String) -> SingleSelect.SmartSelect Msg (Maybe String) -> SingleSelect.SmartSelect Msg (Maybe String) -> AdresseType -> Maybe Commune -> Maybe String -> Maybe String -> Maybe String -> Maybe String -> Html Msg
adresseFilter voies numeros sections parcelles selectAdresseCommune selectAdresseVoie selectAdresseNumero selectCadastreSection selectCadastreParcelle adresseType selectedAdresseCommune selectedAdresseVoie selectedAdresseNumero selectedCadastreSection selectedCadastreParcelle =
    div [ class "flex flex-col" ]
        [ span [ Typo.textBold, Typo.textLg, class "!mb-0" ] [ text "Adresse du local" ]
        , DSFR.Radio.group
            { id = "adresse-type-radio"
            , options = [ AdresseVoie, AdresseParcelle ]
            , current = Just adresseType
            , toLabel = adresseTypeToDisplay >> text
            , toId = adresseTypeToString
            , msg = UpdatedAdresseType
            , legend = Nothing
            }
            |> DSFR.Radio.withExtraAttrs [ class "!mb-0 !mt-4" ]
            |> DSFR.Radio.inline
            |> DSFR.Radio.view
        , div [ Grid.gridRowGutters, Grid.gridRow ]
            [ div [ Grid.col4, class "w-full flex flex-col gap-2 !mb-0" ]
                [ SingleSelectRemote.viewCustom
                    { selected = selectedAdresseCommune
                    , optionLabelFn = .nom
                    , isDisabled = False
                    , optionDescriptionFn = \_ -> ""
                    , optionsContainerMaxHeight = 300
                    , selectTitle = nothing
                    , searchPrompt = "Rechercher une commune"
                    , noResultsForMsg =
                        \searchText ->
                            "Aucune autre commune n'a été trouvée"
                                ++ (if searchText == "" then
                                        ""

                                    else
                                        " pour " ++ searchText
                                   )
                    , noOptionsMsg = "Aucune commune n'a été trouvée"
                    , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                    , error = Nothing
                    }
                    selectAdresseCommune
                , span [ Typo.textSm, class "fr-hint-text" ] [ text "Seules les 20 premières communes sont affichées" ]
                , selectedAdresseCommune
                    |> viewMaybe
                        (\adresseCommune ->
                            DSFR.Tag.deletable (\_ -> ClearSelectAdresseCommune)
                                { data = adresseCommune.nom
                                , toString = identity
                                }
                                |> List.singleton
                                |> DSFR.Tag.medium
                        )
                ]
            , div [ Grid.col4, class "w-full !mt-0 mb-4" ]
                [ case adresseType of
                    AdresseVoie ->
                        SingleSelect.viewCustom
                            { selected = Maybe.map Just <| selectedAdresseVoie
                            , options =
                                voies
                                    |> RD.withDefault []
                                    |> NaturalOrdering.sortBy String.toLower
                                    |> List.map Just
                                    |> (::) Nothing
                            , optionLabelFn = Maybe.withDefault "Toutes les voies"
                            , isDisabled = selectedAdresseCommune == Nothing || selectedCadastreSection /= Nothing
                            , optionDescriptionFn = \_ -> ""
                            , optionsContainerMaxHeight = 300
                            , selectTitle = nothing
                            , searchPrompt = "Choisir un libellé de voie"
                            , noResultsForMsg =
                                \searchText ->
                                    "Aucun local ne se trouve sur cette voie"
                                        ++ (if searchText == "" then
                                                ""

                                            else
                                                " pour " ++ searchText
                                           )
                            , noOptionsMsg = "Aucun local ne se trouve sur cette voie"
                            , searchFn = filterBy (Maybe.withDefault "")
                            }
                            selectAdresseVoie

                    AdresseParcelle ->
                        SingleSelect.viewCustom
                            { selected = Maybe.map Just <| selectedCadastreSection
                            , options =
                                sections
                                    |> RD.withDefault []
                                    |> NaturalOrdering.sortBy String.toLower
                                    |> List.map Just
                                    |> (::) Nothing
                            , optionLabelFn = Maybe.withDefault "Toutes"
                            , isDisabled = selectedAdresseCommune == Nothing || selectedAdresseVoie /= Nothing
                            , optionDescriptionFn = \_ -> ""
                            , optionsContainerMaxHeight = 300
                            , selectTitle = nothing
                            , searchPrompt = "Choisir une section cadastrale"
                            , noResultsForMsg =
                                \searchText ->
                                    "Aucun local ne se trouve sur la section cadastrale"
                                        ++ (if searchText == "" then
                                                ""

                                            else
                                                " " ++ searchText
                                           )
                            , noOptionsMsg = "Aucun local ne se trouve sur cette section cadastrale"
                            , searchFn = filterBy (Maybe.withDefault "")
                            }
                            selectCadastreSection
                ]
            , div [ Grid.col4, class "w-full !mt-0 mb-4" ]
                [ case adresseType of
                    AdresseVoie ->
                        SingleSelect.viewCustom
                            { selected = Maybe.map Just <| selectedAdresseNumero
                            , options =
                                numeros
                                    |> RD.withDefault []
                                    |> NaturalOrdering.sortBy String.toLower
                                    |> List.map Just
                                    |> (::) Nothing
                            , optionLabelFn = Maybe.withDefault "Tous"
                            , isDisabled = selectedAdresseVoie == Nothing
                            , optionDescriptionFn = \_ -> ""
                            , optionsContainerMaxHeight = 300
                            , selectTitle = nothing
                            , searchPrompt = "Choisir un numéro de voie"
                            , noResultsForMsg =
                                \searchText ->
                                    "Aucun local ne se trouve au numéro"
                                        ++ (if searchText == "" then
                                                ""

                                            else
                                                " " ++ searchText
                                           )
                            , noOptionsMsg = "Aucun local ne se trouve à ce numéro"
                            , searchFn = filterBy (Maybe.withDefault "")
                            }
                            selectAdresseNumero

                    AdresseParcelle ->
                        SingleSelect.viewCustom
                            { selected = Maybe.map Just <| selectedCadastreParcelle
                            , options =
                                parcelles
                                    |> RD.withDefault []
                                    |> NaturalOrdering.sortBy String.toLower
                                    |> List.map Just
                                    |> (::) Nothing
                            , optionLabelFn = Maybe.withDefault "Toutes"
                            , isDisabled = selectedCadastreSection == Nothing
                            , optionDescriptionFn = \_ -> ""
                            , optionsContainerMaxHeight = 300
                            , selectTitle = nothing
                            , searchPrompt = "Choisir un numéro de parcelle"
                            , noResultsForMsg =
                                \searchText ->
                                    "Aucun local ne se trouve sur la parcelle"
                                        ++ (if searchText == "" then
                                                ""

                                            else
                                                " " ++ searchText
                                           )
                            , noOptionsMsg = "Aucun local ne se trouve sur cette parcelle"
                            , searchFn = filterBy (Maybe.withDefault "")
                            }
                            selectCadastreParcelle
                ]
            ]
        ]


communesFilter : Maybe Commune -> MultiSelectRemote.SmartSelect Msg Commune -> List Commune -> Html Msg
communesFilter adresseCommune selectCommunes communes =
    let
        adresseCommuneSelected =
            adresseCommune /= Nothing

        communeToLabel c =
            case ( c.nom, c.departementId ) of
                ( "", "" ) ->
                    c.id

                _ ->
                    c.nom ++ " (" ++ c.departementId ++ ")"

        ( isDisabled, title ) =
            if adresseCommuneSelected then
                ( True, Just "Une commune a déjà été choisie pour l'adresse du local" )

            else
                ( False, Nothing )
    in
    div [ class "flex flex-col gap-2", Maybe.withDefault empty <| Maybe.map Html.Attributes.title <| title ]
        [ selectCommunes
            |> MultiSelectRemote.viewCustom
                { isDisabled = isDisabled
                , selected = communes
                , optionLabelFn = communeToLabel
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = div [ Typo.textBold ] [ text "Communes" ]
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre commune n'a été trouvée"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucune commune n'a été trouvée"
                , searchPrompt = "Filtrer par commune"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , newOption = Nothing
                }
        , case communes of
            [] ->
                nothing

            _ ->
                communes
                    |> List.map (\commune -> DSFR.Tag.deletable UnselectCommune { data = commune, toString = communeToLabel })
                    |> DSFR.Tag.medium
        ]


epcisFilter : MultiSelectRemote.SmartSelect Msg Epci -> List Epci -> Html Msg
epcisFilter selectEpcis epcis =
    div [ class "flex flex-col gap-2" ]
        [ selectEpcis
            |> MultiSelectRemote.viewCustom
                { isDisabled = False
                , selected = epcis
                , optionLabelFn = \epci -> epci.id ++ " - " ++ epci.nom
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle =
                    div [ class "flex flex-col" ]
                        [ span [ Typo.textBold ]
                            [ text "EPCIs"
                            , sup [] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ]
                                |> (DSFR.Tooltip.survol
                                        { label = text hintResultatsTerritoires
                                        , id = "resultats-limites-epcis"
                                        }
                                        |> DSFR.Tooltip.wrap
                                   )
                            ]
                        ]
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucun autre EPCI n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun EPCI n'a été trouvé"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                , newOption = Nothing
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , searchPrompt = "Rechercher par nom ou numéro de SIREN"
                }
        , case epcis of
            [] ->
                nothing

            _ ->
                let
                    ( toLabel, toTitle ) =
                        dataToLabelAndToTitle
                in
                epcis
                    |> List.map
                        (\epci ->
                            { data = epci, toString = toLabel }
                                |> DSFR.Tag.deletable UnselectEpci
                                |> DSFR.Tag.withAttrs
                                    [ Html.Attributes.title <| toTitle <| epci
                                    ]
                        )
                    |> DSFR.Tag.medium
        ]


departementsFilter : MultiSelectRemote.SmartSelect Msg Departement -> List Departement -> Html Msg
departementsFilter selectDepartements departements =
    div [ class "flex flex-col gap-2" ]
        [ selectDepartements
            |> MultiSelectRemote.viewCustom
                { isDisabled = False
                , selected = departements
                , optionLabelFn = \d -> d.id ++ " - " ++ d.nom
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle =
                    div [ class "flex flex-col" ]
                        [ span [ Typo.textBold ]
                            [ text "Départements"
                            , sup [] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ]
                                |> (DSFR.Tooltip.survol
                                        { label = text hintResultatsTerritoires
                                        , id = "resultats-limites-departements"
                                        }
                                        |> DSFR.Tooltip.wrap
                                   )
                            ]
                        ]
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucun autre département n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun département n'a été trouvé"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                , newOption = Nothing
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , searchPrompt = "Rechercher par nom ou numéro"
                }
        , case departements of
            [] ->
                nothing

            _ ->
                let
                    ( toLabel, toTitle ) =
                        dataToLabelAndToTitle
                in
                departements
                    |> List.map
                        (\departement ->
                            { data = departement, toString = toLabel }
                                |> DSFR.Tag.deletable UnselectDepartement
                                |> DSFR.Tag.withAttrs
                                    [ Html.Attributes.title <| toTitle <| departement
                                    ]
                        )
                    |> DSFR.Tag.medium
        ]


regionsFilter : MultiSelectRemote.SmartSelect Msg Region -> List Region -> Html Msg
regionsFilter selectRegion regions =
    div [ class "flex flex-col gap-2" ]
        [ selectRegion
            |> MultiSelectRemote.viewCustom
                { isDisabled = False
                , selected = regions
                , optionLabelFn = \region -> region.id ++ " - " ++ region.nom
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle =
                    div [ class "flex flex-col" ]
                        [ span [ Typo.textBold ]
                            [ text "Régions"
                            , sup [] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ]
                                |> (DSFR.Tooltip.survol
                                        { label = text hintResultatsTerritoires
                                        , id = "resultats-limites-regions"
                                        }
                                        |> DSFR.Tooltip.wrap
                                   )
                            ]
                        ]
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre région n'a été trouvée"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucune région n'a été trouvée"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                , newOption = Nothing
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , searchPrompt = "Rechercher par nom ou code INSEE"
                }
        , case regions of
            [] ->
                nothing

            _ ->
                let
                    ( toLabel, toTitle ) =
                        dataToLabelAndToTitle
                in
                regions
                    |> List.map
                        (\region ->
                            { data = region, toString = toLabel }
                                |> DSFR.Tag.deletable UnselectRegion
                                |> DSFR.Tag.withAttrs
                                    [ Html.Attributes.title <| toTitle <| region
                                    ]
                        )
                    |> DSFR.Tag.medium
        ]


qpvsFilter : MultiSelectRemote.SmartSelect Msg Qpv -> Bool -> List Qpv -> Html Msg
qpvsFilter selectQpvs tousQpvs qpvs =
    div [ class "flex flex-col gap-2" ]
        [ selectQpvs
            |> MultiSelectRemote.viewCustom
                { isDisabled = tousQpvs
                , selected = qpvs
                , optionLabelFn = \qpv -> qpv.id ++ " - " ++ qpv.nom
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle =
                    div [ class "flex flex-col" ]
                        [ span [ Typo.textBold ]
                            [ text "QPV - Quartier Prioritaire de la Politique de la Ville"
                            , sup [] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ]
                                |> (DSFR.Tooltip.survol
                                        { label = text hintResultatsTerritoires
                                        , id = "resultats-limites-qpvs"
                                        }
                                        |> DSFR.Tooltip.wrap
                                   )
                            ]
                        ]
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucun autre QPV n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun QPV n'a été trouvé"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                , newOption = Nothing
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , searchPrompt = "Rechercher par nom ou par code"
                }
        , case qpvs of
            [] ->
                nothing

            _ ->
                let
                    ( toLabel, toTitle ) =
                        dataToLabelAndToTitle
                in
                qpvs
                    |> List.map
                        (\qpv ->
                            { data = qpv, toString = toLabel }
                                |> DSFR.Tag.deletable UnselectQpv
                                |> DSFR.Tag.withAttrs
                                    [ Html.Attributes.title <| toTitle <| qpv
                                    ]
                        )
                    |> DSFR.Tag.medium
        , DSFR.Checkbox.single
            { value = "qpvs-tous-selection"
            , checked = Just tousQpvs
            , valueAsString = identity
            , id = "qpvs-tous-selection"
            , label = text "Tous les QPV"
            , onChecked = \_ bool -> ToggleAllQpvs bool
            }
            |> DSFR.Checkbox.viewSingle
        ]


tisFilter : MultiSelectRemote.SmartSelect Msg TerritoireIndustrie -> Bool -> List TerritoireIndustrie -> Html Msg
tisFilter selectTis tousTis tis =
    div [ class "flex flex-col gap-2" ]
        [ selectTis
            |> MultiSelectRemote.viewCustom
                { isDisabled = tousTis
                , selected = tis
                , optionLabelFn = .nom
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle =
                    div [ class "flex flex-col" ]
                        [ span [ Typo.textBold ]
                            [ text "Territoire d'industrie"
                            ]
                        ]
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucun autre Territoire d'industrie n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun Territoire d'industrie n'a été trouvé"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                , newOption = Nothing
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , searchPrompt = "Rechercher un Territoire d'industrie"
                }
        , case tis of
            [] ->
                nothing

            _ ->
                tis
                    |> List.map (\ti -> DSFR.Tag.deletable UnselectTi { data = ti, toString = .nom })
                    |> DSFR.Tag.medium
        , DSFR.Checkbox.single
            { value = "tis-tous-selection"
            , checked = Just tousTis
            , valueAsString = identity
            , id = "tis-tous-selection"
            , label = text "Tous les Territoires d'industrie"
            , onChecked = \_ bool -> ToggleAllTis bool
            }
            |> DSFR.Checkbox.viewSingle
        ]


zonesFilter : MultiSelect.SmartSelect Msg String -> List String -> List String -> Html Msg
zonesFilter selectZones optionsZones zones =
    div [ class "flex flex-col gap-4" ]
        [ selectZones
            |> MultiSelect.viewCustom
                { isDisabled = optionsZones |> List.length |> (==) 0
                , selected = zones
                , options = optionsZones |> List.sortBy String.toLower
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Zone géographique"
                , viewSelectedOptionFn = text
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre zone géographique n'a été trouvée"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                , searchFn = filterBy identity
                , searchPrompt = "Filtrer par zone géographique"
                }
        , case zones of
            [] ->
                nothing

            _ ->
                zones
                    |> List.map (\zone -> DSFR.Tag.deletable UnselectZone { data = zone, toString = identity })
                    |> DSFR.Tag.medium
        ]


motsFilter : MultiSelect.SmartSelect Msg String -> List String -> List String -> Html Msg
motsFilter selectMots optionsMots mots =
    div [ class "flex flex-col gap-4" ]
        [ selectMots
            |> MultiSelect.viewCustom
                { isDisabled = optionsMots |> List.length |> (==) 0
                , selected = mots
                , options = optionsMots |> List.sortBy String.toLower
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Mots-clés"
                , viewSelectedOptionFn = text
                , noResultsForMsg =
                    \searchText ->
                        "Aucun autre mot-clé n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                , searchFn = filterBy identity
                , searchPrompt = "Filtrer par mot-clé"
                }
        , case mots of
            [] ->
                nothing

            _ ->
                mots
                    |> List.map (\mot -> DSFR.Tag.deletable UnselectMot { data = mot, toString = identity })
                    |> DSFR.Tag.medium
        ]


hasFilterTags : String -> Bool
hasFilterTags url =
    let
        filters =
            url |> queryToFilters

        hasFilters =
            [ filters.nom /= ""
            , filters.proprietaire /= ""
            , filters.invariant /= ""
            , List.length filters.etages > 0
            , List.length filters.natures > 0
            , List.length filters.categories > 0
            , filters.surfaces.totale.min /= Nothing
            , filters.surfaces.totale.max /= Nothing
            , filters.surfaces.vente.min /= Nothing
            , filters.surfaces.vente.max /= Nothing
            , filters.surfaces.reserve.min /= Nothing
            , filters.surfaces.reserve.max /= Nothing
            , filters.surfaces.exterieure.min /= Nothing
            , filters.surfaces.exterieure.max /= Nothing
            , filters.surfaces.stationnementCouverte.min /= Nothing
            , filters.surfaces.stationnementCouverte.max /= Nothing
            , filters.surfaces.stationnementNonCouverte.min /= Nothing
            , filters.surfaces.stationnementNonCouverte.max /= Nothing
            , List.length filters.vacances > 0
            , List.length filters.communes > 0
            , List.length filters.epcis > 0
            , List.length filters.departements > 0
            , List.length filters.regions > 0
            , List.length filters.qpvs > 0
            , List.length filters.tis > 0
            , filters.tousQpvs
            , filters.tousTis
            , filters.adresseCommune /= Nothing
            , filters.adresseVoie /= Nothing
            , filters.adresseNumero /= Nothing
            , filters.cadastreSection /= Nothing
            , filters.cadastreParcelle /= Nothing
            , List.length filters.accompagnes > 0
            , filters.accompagnesApres /= Nothing
            , filters.accompagnesAvant /= Nothing
            , List.length filters.zones > 0
            , List.length filters.mots > 0
            , List.length filters.favoris > 0
            ]
    in
    List.any (\a -> a) hasFilters


favorisFilter : List Bool -> Html Msg
favorisFilter favoris =
    DSFR.Checkbox.group
        { id = "filtres-favoris"
        , values =
            [ True
            , False
            ]
        , toLabel =
            text
                << (\v ->
                        if v then
                            "Oui"

                        else
                            "Non"
                   )
        , toId =
            \v ->
                if v then
                    "favori-oui"

                else
                    "favori-non"
        , checked = favoris
        , valueAsString =
            \v ->
                if v then
                    "oui"

                else
                    "non"
        , onChecked = \data bool -> SetFavoriFilter bool data
        , label =
            div [ Typo.textBold ]
                [ text "Mes favoris"
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


accompagnesFilter : List Accompagnes -> Maybe Date -> Maybe Date -> Html Msg
accompagnesFilter accompagnes accompagnesApres accompagnesAvant =
    div [ class "flex flex-col gap-4" ]
        [ span [ Typo.textBold ] [ text "Local accompagné" ]
        , span
            [ class "fr-hint-text"
            ]
            [ text "Au moins un échange/demande/rappel saisi" ]
        , DSFR.Checkbox.group
            { id = "filtres-accompagnes"
            , values =
                [ AccompagnesOui
                , AccompagnesNon
                ]
            , toLabel =
                text
                    << (\e ->
                            case e of
                                AccompagnesOui ->
                                    "Oui"

                                AccompagnesNon ->
                                    "Non"
                       )
            , toId =
                \e ->
                    case e of
                        AccompagnesOui ->
                            "oui"

                        AccompagnesNon ->
                            "non"
            , checked = accompagnes
            , valueAsString =
                \e ->
                    case e of
                        AccompagnesOui ->
                            "oui"

                        AccompagnesNon ->
                            "non"
            , onChecked =
                \data bool -> SetAccompagnesFilter bool data
            , label = nothing
            }
            |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
            |> DSFR.Checkbox.viewGroup
        , div [ class "flex flex-col gap-4" ]
            [ selecteurDate
                { msg = UpdateAccompagnesDate AccompagnesApres
                , label = text "À partir du"
                , name = "filtre-accompagnes-apres"
                , value = accompagnesApres
                , min = Nothing
                , max = Nothing
                , disabled = not <| List.member AccompagnesOui accompagnes
                }
            , selecteurDate
                { msg = UpdateAccompagnesDate AccompagnesAvant
                , label = text "Jusqu'au (inclus)"
                , name = "filtre-accompagnes-avant"
                , value = accompagnesAvant
                , min = accompagnesApres
                , max = Nothing
                , disabled = not <| List.member AccompagnesOui accompagnes
                }
            ]
        ]


clearAllFiltersButton : String -> Html Msg
clearAllFiltersButton currentUrl =
    DSFR.Button.new
        { label = "Tout effacer"
        , onClick = Just <| ClearAllFilters
        }
        |> DSFR.Button.withDisabled (queryToFilters currentUrl == emptyFilters)
        |> DSFR.Button.tertiaryNoOutline
        |> DSFR.Button.view


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Effect.none )

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        ReceivedFiltersSource response ->
            { model | filtersSource = response }
                |> Effect.withNone

        ReceivedLocaux response ->
            case response of
                RD.Success ( locaux, pagination, filtresRequete ) ->
                    let
                        newFilters =
                            model.filters
                                |> hydrateFilters filtresRequete
                    in
                    ( { model
                        | locaux = RD.Success locaux
                        , pagination = pagination
                        , filters = newFilters
                        , filtresRequete = filtresRequete
                      }
                    , if model.viewMode == Carte then
                        Effect.batch
                            [ Effect.fromCmd <|
                                getCartoFeatures
                                    model.locauxRequests
                                    model.currentUrl
                                    Nothing
                            , Effect.fromCmd <|
                                Http.cancel <|
                                    getLocauxCartoTracker (model.locauxRequests - 1)
                            , Effect.fromShared <| Shared.ScrollIntoView "carto"
                            ]

                      else
                        Effect.none
                    )

                _ ->
                    ( { model | locaux = response |> RD.map (\( l, _, _ ) -> l) }
                    , Effect.none
                    )

        ReceivedCartoFeatures cartoFeatures ->
            ( { model
                | cartoFeatures = cartoFeatures
              }
            , Effect.none
            )

        DoSearch ( filters, pagination ) ->
            doSearch ( filters, pagination ) model

        ToggleFavorite id add ->
            { model
                | locaux =
                    model.locaux
                        |> RD.map (List.map (updateLocalInList id add))
            }
                |> Effect.withCmd (requestToggleFavorite id add)

        GotFavoriteResult id add ->
            { model
                | locaux =
                    model.locaux
                        |> RD.map (List.map (updateLocalInList id add))
            }
                |> Effect.withNone

        ClearAllFilters ->
            let
                newModel =
                    { model | filters = emptyFilters }
            in
            ( newModel
            , updateUrl newModel
            )

        UpdatedNom recherche ->
            ( { model
                | filters =
                    model.filters
                        |> (\f -> { f | nom = recherche })
              }
            , Effect.none
            )

        UpdatedProprietaire recherche ->
            ( { model
                | filters =
                    model.filters
                        |> (\f -> { f | proprietaire = recherche })
              }
            , Effect.none
            )

        UpdatedInvariant recherche ->
            ( { model
                | filters =
                    model.filters
                        |> (\f -> { f | invariant = recherche })
              }
            , Effect.none
            )

        ClickedOrderBy orderBy orderDirection ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | orderBy = orderBy, orderDirection = orderDirection }) }
            in
            ( newModel
            , updateUrl newModel
            )

        UpdateUrl ( filters, pagination ) ->
            ( model, updateUrl { filters = filters, pagination = pagination } )

        ClickedSearch ->
            let
                forcedPagination =
                    model.pagination |> (\p -> { p | page = 1 })
            in
            ( model, updateUrl { model | pagination = forcedPagination } )

        UpdatedAdresseType adresseType ->
            { model
                | filters =
                    model.filters
                        |> (\ft ->
                                { ft
                                    | adresseType = adresseType
                                }
                           )
            }
                |> Effect.withNone

        SelectedAdresseCommune ( adresseCommune, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectAdresseCommuneConfig model.selectAdresseCommune

                newModel =
                    { model
                        | selectAdresseCommune = updatedSelect
                        , filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | adresseCommune = Just adresseCommune
                                            , adresseVoie = Nothing
                                            , adresseNumero = Nothing
                                            , cadastreSection = Nothing
                                            , cadastreParcelle = Nothing
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , if model.filters.adresseCommune /= Just adresseCommune then
                    Effect.batch
                        [ adresseCommune
                            |> .id
                            |> getAdresseVoies
                        , adresseCommune
                            |> .id
                            |> getCadastreSections
                        ]

                  else
                    Effect.none
                ]
            )

        UpdatedSelectAdresseCommune sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectAdresseCommuneConfig model.selectAdresseCommune
            in
            ( { model | selectAdresseCommune = updatedSelect }, Effect.fromCmd selectCmd )

        ClearSelectAdresseCommune ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.setText "" selectAdresseCommuneConfig model.selectAdresseCommune

                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | adresseCommune = Nothing
                                            , adresseVoie = Nothing
                                            , adresseNumero = Nothing
                                            , cadastreSection = Nothing
                                            , cadastreParcelle = Nothing
                                        }
                                   )
                        , selectAdresseCommune = updatedSelect
                    }
            in
            ( newModel, Effect.fromCmd selectCmd )

        SelectedAdresseVoie ( adresseVoie, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectAdresseVoie

                newModel =
                    { model
                        | selectAdresseVoie = updatedSelect
                        , filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | adresseVoie = adresseVoie
                                            , adresseNumero = Nothing
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , if model.filters.adresseVoie /= adresseVoie then
                    Maybe.map2 getAdresseNumeros (model.filters.adresseCommune |> Maybe.map .id) adresseVoie
                        |> Maybe.withDefault Effect.none

                  else
                    Effect.none
                ]
            )

        UpdatedSelectAdresseVoie sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectAdresseVoie
            in
            ( { model | selectAdresseVoie = updatedSelect }, Effect.fromCmd selectCmd )

        SelectedAdresseNumero ( adresseNumero, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectAdresseNumero

                newModel =
                    { model
                        | selectAdresseNumero = updatedSelect
                        , filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | adresseNumero = adresseNumero
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectAdresseNumero sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectAdresseNumero
            in
            ( { model | selectAdresseNumero = updatedSelect }, Effect.fromCmd selectCmd )

        SelectedCadastreSection ( cadastreSection, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectCadastreSection

                newModel =
                    { model
                        | selectCadastreSection = updatedSelect
                        , filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | cadastreSection = cadastreSection
                                            , cadastreParcelle = Nothing
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , if model.filters.cadastreSection /= cadastreSection then
                    Maybe.map2 getCadastreParcelles (model.filters.adresseCommune |> Maybe.map .id) cadastreSection
                        |> Maybe.withDefault Effect.none

                  else
                    Effect.none
                ]
            )

        UpdatedSelectCadastreSection sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectCadastreSection
            in
            ( { model | selectCadastreSection = updatedSelect }, Effect.fromCmd selectCmd )

        SelectedCadastreParcelle ( cadastreParcelle, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectCadastreParcelle

                newModel =
                    { model
                        | selectCadastreParcelle = updatedSelect
                        , filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | cadastreParcelle = cadastreParcelle
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectCadastreParcelle sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectCadastreParcelle
            in
            ( { model | selectCadastreParcelle = updatedSelect }, Effect.fromCmd selectCmd )

        SelectedEtages ( etages, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectEtages

                etagesUniques =
                    case etages of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                etages

                newModel =
                    { model
                        | selectEtages = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | etages = etagesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectEtages sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectEtages
            in
            ( { model | selectEtages = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectEtage etage ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | etages =
                                                filters.etages
                                                    |> List.filter (\e -> e /= etage)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedZones ( zones, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZones

                zonesUniques =
                    case zones of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                zones

                newModel =
                    { model
                        | selectZones = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | zones = zonesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectZones sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZones
            in
            ( { model
                | selectZones = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectZone zone ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | zones =
                                                filters.zones
                                                    |> List.filter ((/=) zone)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedMots ( mots, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectMots

                motsUniques =
                    case mots of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                mots

                newModel =
                    { model
                        | selectMots = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | mots = motsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectMots sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectMots
            in
            ( { model
                | selectMots = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectMot mot ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | mots =
                                                filters.mots
                                                    |> List.filter ((/=) mot)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ReceivedSources resp ->
            let
                naturesSources =
                    resp
                        |> RD.map .natures
                        |> RD.withDefault []

                natures =
                    model.filters.natures

                newNatures =
                    hydrateData .id naturesSources natures

                categoriesSources =
                    resp
                        |> RD.map .categories
                        |> RD.withDefault []

                categories =
                    model.filters.categories

                newCategories =
                    hydrateData .id categoriesSources categories

                communes =
                    model.filters.communes

                newCommunes =
                    communes

                newCommune =
                    model.filters.adresseCommune
            in
            { model
                | categories =
                    resp
                        |> RD.map .categories
                , natures =
                    resp
                        |> RD.map .natures
                , filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | categories = newCategories
                                    , natures = newNatures
                                    , communes = newCommunes
                                    , adresseCommune = newCommune
                                }
                           )
            }
                |> Effect.withNone

        SelectedNatures ( natures, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectNatures

                naturesUniques =
                    case natures of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                natures

                newModel =
                    { model
                        | selectNatures = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | natures = naturesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectNatures sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectNatures
            in
            ( { model | selectNatures = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectNature nature ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | natures =
                                                filters.natures
                                                    |> List.filter (\n -> n.id /= nature.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedCategories ( categories, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectCategories

                categoriesUniques =
                    case categories of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                categories

                newModel =
                    { model
                        | selectCategories = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | categories = categoriesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectCategories sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectCategories
            in
            ( { model | selectCategories = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectCategorie categorie ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | categories =
                                                filters.categories
                                                    |> List.filter (\c -> c.id /= categorie.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        UpdatedSurface surfaceType minMaxType valeur ->
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | surfaces =
                                        f.surfaces
                                            |> updateSurfaces surfaceType minMaxType (Just valeur)
                                }
                           )
              }
            , Effect.none
            )

        SetVacanceFilter add vacance ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | vacances = setVacances model.filters add vacance
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedCommunes ( communes, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectCommunesConfig model.selectCommunes

                communesUniques =
                    case communes of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                communes

                newModel =
                    { model
                        | selectCommunes = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | communes = communesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectCommunes sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectCommunesConfig model.selectCommunes
            in
            ( { model | selectCommunes = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectCommune commune ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | communes =
                                                filters.communes
                                                    |> List.filter (\c -> c.id /= commune.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ReceivedVoies response ->
            { model | voies = response }
                |> Effect.withNone

        ReceivedNumeros response ->
            { model | numeros = response }
                |> Effect.withNone

        ReceivedCadastreSections response ->
            { model | sections = response }
                |> Effect.withNone

        ReceivedCadastreParcelles response ->
            { model | parcelles = response }
                |> Effect.withNone

        ClickedExportExcel ->
            { model | excelExportRequested = True }
                |> Effect.withNone

        ClickedExportGeoJson ->
            { model | geoJsonExport = Just Regular }
                |> Effect.withNone

        ClickedCanceledGeoJson ->
            { model | geoJsonExport = Nothing }
                |> Effect.withNone

        RequestedRechercheSauvegardee ->
            { model | rechercheSauvegardeeRequest = RD.Loading }
                |> Effect.withCmd (getRechercheSauvegardeeRequest model.currentUrl)

        ReceivedRechercheSauvegardee response ->
            case model.rechercheSauvegardeeRequest of
                RD.Loading ->
                    { model | rechercheSauvegardeeRequest = response }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        SetAccompagnesFilter add accompagne ->
            let
                currentAccompagnes =
                    model.filters.accompagnes

                isMember =
                    List.member accompagne currentAccompagnes

                newAccompagne =
                    case ( add, isMember ) of
                        ( True, True ) ->
                            currentAccompagnes

                        ( True, False ) ->
                            currentAccompagnes ++ [ accompagne ]

                        ( False, True ) ->
                            currentAccompagnes
                                |> List.filter (\t -> t /= accompagne)

                        ( False, False ) ->
                            currentAccompagnes

                newModel =
                    { model | filters = model.filters |> (\f -> { f | accompagnes = newAccompagne }) }
            in
            ( newModel
            , Effect.none
            )

        UpdateAccompagnesDate dateType date ->
            let
                updateFilters : Filters -> Filters
                updateFilters f =
                    case dateType of
                        AccompagnesApres ->
                            { f | accompagnesApres = date }

                        AccompagnesAvant ->
                            { f | accompagnesAvant = date }
            in
            ( { model | filters = updateFilters model.filters }
            , Effect.none
            )

        SetFavoriFilter add favori ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> setFavorisInFilters add favori
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedEpcis ( epcis, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectEpcisConfig model.selectEpcis

                epcisUniques =
                    case epcis of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                epcis

                newModel =
                    { model
                        | selectEpcis = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | epcis = epcisUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectEpcis sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectEpcisConfig model.selectEpcis
            in
            ( { model | selectEpcis = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectEpci epci ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | epcis =
                                                filters.epcis
                                                    |> List.filter (\c -> c.id /= epci.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedDepartements ( departements, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectDepartementsConfig model.selectDepartements

                departementsUniques =
                    case departements of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                departements

                newModel =
                    { model
                        | selectDepartements = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | departements = departementsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectDepartements sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectDepartementsConfig model.selectDepartements
            in
            ( { model | selectDepartements = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectDepartement departement ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | departements =
                                                filters.departements
                                                    |> List.filter (\c -> c.id /= departement.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedRegions ( regions, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectRegionsConfig model.selectRegions

                regionsUniques =
                    case regions of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                regions

                newModel =
                    { model
                        | selectRegions = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | regions = regionsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectRegions sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectRegionsConfig model.selectRegions
            in
            ( { model | selectRegions = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectRegion region ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | regions =
                                                filters.regions
                                                    |> List.filter (\c -> c.id /= region.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedQpvs ( qpvs, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectQpvsConfig model.selectQpvs

                qpvsUniques =
                    case qpvs of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a.id <| List.map .id rest then
                                rest

                            else
                                qpvs

                newModel =
                    { model
                        | selectQpvs = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | qpvs = qpvsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectQpvs sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectQpvsConfig model.selectQpvs
            in
            ( { model
                | selectQpvs = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectQpv qpv ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | qpvs =
                                                filters.qpvs
                                                    |> List.filter (\{ id } -> id /= qpv.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ToggleAllQpvs on ->
            ( { model
                | filters =
                    model.filters
                        |> (\filters ->
                                { filters
                                    | qpvs = []
                                    , tousQpvs = on
                                }
                           )
              }
            , Effect.none
            )

        SelectedTis ( tis, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectTisConfig model.selectTis

                tisUniques =
                    case tis of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a.id <| List.map .id rest then
                                rest

                            else
                                tis

                newModel =
                    { model
                        | selectTis = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | tis = tisUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectTis sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectTisConfig model.selectTis
            in
            ( { model
                | selectTis = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectTi ti ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | tis =
                                                filters.tis
                                                    |> List.filter (\{ id } -> id /= ti.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ToggleAllTis on ->
            ( { model
                | filters =
                    model.filters
                        |> (\filters ->
                                { filters
                                    | tis = []
                                    , tousTis = on
                                }
                           )
              }
            , Effect.none
            )

        ChangeViewMode viewMode ->
            let
                currentEtablissementRequestNumber =
                    model.locauxRequests + 1

                previousEtablissementRequestNumber =
                    model.locauxRequests

                ( cartoFeatures, effect ) =
                    case ( viewMode, model.cartoFeatures ) of
                        ( Carte, RD.NotAsked ) ->
                            ( RD.Loading
                            , Effect.batch
                                [ Effect.fromCmd <|
                                    getCartoFeatures
                                        currentEtablissementRequestNumber
                                        model.currentUrl
                                        Nothing
                                , Effect.fromCmd <|
                                    Http.cancel <|
                                        getLocauxCartoTracker previousEtablissementRequestNumber
                                , Effect.fromShared <| Shared.ScrollIntoView "carto"
                                ]
                            )

                        _ ->
                            ( model.cartoFeatures
                            , Effect.none
                            )
            in
            { model
                | viewMode = viewMode
                , cartoFeatures = cartoFeatures
            }
                |> Effect.with effect

        UpdatedMapInfo mapInfo ->
            let
                currentEtablissementRequestNumber =
                    model.locauxRequests + 1

                previousEtablissementRequestNumber =
                    model.locauxRequests
            in
            ( { model
                | mapInfo = Just mapInfo
                , locauxRequests = currentEtablissementRequestNumber
              }
            , Effect.batch
                [ Effect.fromCmd
                    (getCartoFeatures
                        currentEtablissementRequestNumber
                        model.currentUrl
                        (Just mapInfo)
                    )
                , Effect.fromCmd (Http.cancel <| getLocauxCartoTracker previousEtablissementRequestNumber)
                ]
            )

        SelectedAdresse ( apiAdresse, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectAdresseConfig model.selectAdresse

                ( emptySelect, emptySelectCmd ) =
                    SingleSelectRemote.setText "" selectAdresseConfig updatedSelect

                newCartoFeatures =
                    model.cartoFeatures
                        |> RD.map (\cartoFeatures -> { cartoFeatures | adresse = Just apiAdresse })
            in
            ( { model
                | cartoFeatures = newCartoFeatures
                , selectAdresse = emptySelect
              }
            , Effect.batch [ Effect.fromCmd selectCmd, Effect.fromCmd emptySelectCmd ]
            )

        UpdatedSelectAdresse sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectAdresseConfig model.selectAdresse
            in
            ( { model
                | selectAdresse = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )


setFavorisInFilters : Bool -> Bool -> Filters -> Filters
setFavorisInFilters add favori filters =
    { filters
        | favoris =
            case filters.favoris of
                [] ->
                    case ( add, favori ) of
                        ( True, True ) ->
                            [ True ]

                        ( True, False ) ->
                            [ False ]

                        ( False, True ) ->
                            []

                        ( False, False ) ->
                            []

                [ True ] ->
                    case ( add, favori ) of
                        ( True, True ) ->
                            [ True ]

                        ( True, False ) ->
                            [ True, False ]

                        ( False, True ) ->
                            []

                        ( False, False ) ->
                            [ True ]

                [ False ] ->
                    case ( add, favori ) of
                        ( True, True ) ->
                            [ False, True ]

                        ( True, False ) ->
                            [ False ]

                        ( False, True ) ->
                            [ False ]

                        ( False, False ) ->
                            []

                [ True, False ] ->
                    case ( add, favori ) of
                        ( True, True ) ->
                            [ False, True ]

                        ( True, False ) ->
                            [ False, True ]

                        ( False, True ) ->
                            [ False ]

                        ( False, False ) ->
                            [ True ]

                [ False, True ] ->
                    case ( add, favori ) of
                        ( True, True ) ->
                            [ False, True ]

                        ( True, False ) ->
                            [ False, True ]

                        ( False, True ) ->
                            [ False ]

                        ( False, False ) ->
                            [ True ]

                _ ->
                    filters.favoris
    }


setVacances : Filters -> Bool -> Vacances -> List Vacances
setVacances filters add vacance =
    let
        vacances =
            filters.vacances

        isMember =
            List.member vacance vacances
    in
    case ( add, isMember ) of
        ( True, True ) ->
            vacances

        ( True, False ) ->
            vacances ++ [ vacance ]

        ( False, True ) ->
            vacances
                |> List.filter (\va -> va /= vacance)

        ( False, False ) ->
            vacances


updateSurfaces : SurfaceType -> MinMaxType -> Maybe String -> Surfaces -> Surfaces
updateSurfaces surfaceType minMaxType valeur surfaces =
    let
        newValeur =
            case valeur of
                Just "" ->
                    Nothing

                _ ->
                    valeur
    in
    case ( surfaceType, minMaxType ) of
        ( STotale, Min ) ->
            { surfaces
                | totale =
                    surfaces.totale
                        |> (\s -> { s | min = newValeur })
            }

        ( STotale, Max ) ->
            { surfaces
                | totale =
                    surfaces.totale
                        |> (\s -> { s | max = newValeur })
            }

        ( SVente, Min ) ->
            { surfaces
                | vente =
                    surfaces.vente
                        |> (\s -> { s | min = newValeur })
            }

        ( SVente, Max ) ->
            { surfaces
                | vente =
                    surfaces.vente
                        |> (\s -> { s | max = newValeur })
            }

        ( SReserve, Min ) ->
            { surfaces
                | reserve =
                    surfaces.reserve
                        |> (\s -> { s | min = newValeur })
            }

        ( SReserve, Max ) ->
            { surfaces
                | reserve =
                    surfaces.reserve
                        |> (\s -> { s | max = newValeur })
            }

        ( SExterieure, Min ) ->
            { surfaces
                | exterieure =
                    surfaces.exterieure
                        |> (\s -> { s | min = newValeur })
            }

        ( SExterieure, Max ) ->
            { surfaces
                | exterieure =
                    surfaces.exterieure
                        |> (\s -> { s | max = newValeur })
            }

        ( SStationnementCouverte, Min ) ->
            { surfaces
                | stationnementCouverte =
                    surfaces.stationnementCouverte
                        |> (\s -> { s | min = newValeur })
            }

        ( SStationnementCouverte, Max ) ->
            { surfaces
                | stationnementCouverte =
                    surfaces.stationnementCouverte
                        |> (\s -> { s | max = newValeur })
            }

        ( SStationnementNonCouverte, Min ) ->
            { surfaces
                | stationnementNonCouverte =
                    surfaces.stationnementNonCouverte
                        |> (\s -> { s | min = newValeur })
            }

        ( SStationnementNonCouverte, Max ) ->
            { surfaces
                | stationnementNonCouverte =
                    surfaces.stationnementNonCouverte
                        |> (\s -> { s | max = newValeur })
            }


doSearch : ( Filters, Pagination ) -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
doSearch ( filters, pagination ) model =
    let
        nextUrl =
            filtersAndPaginationToQuery ( filters, pagination )

        requestChanged =
            nextUrl /= model.currentUrl
    in
    if requestChanged then
        let
            naturesSources =
                model.natures
                    |> RD.withDefault []

            natures =
                filters.natures

            newNatures =
                hydrateData .id naturesSources natures

            categoriesSources =
                model.categories
                    |> RD.withDefault []

            categories =
                filters.categories

            newCategories =
                hydrateData .id categoriesSources categories

            communes =
                filters.communes

            newCommunes =
                communes

            newCommune =
                filters.adresseCommune

            etages =
                filters.etages

            newEtages =
                hydrateData .id optionsEtages etages

            currentRequestNumber =
                model.locauxRequests

            nextRequestNumber =
                model.locauxRequests + 1

            newModel =
                { model
                    | pagination = pagination
                    , filters =
                        { filters
                            | natures = newNatures
                            , categories = newCategories
                            , communes = newCommunes
                            , adresseCommune = newCommune
                            , etages = newEtages
                        }
                    , currentUrl = nextUrl
                    , locaux = RD.Loading
                    , locauxRequests = nextRequestNumber
                    , rechercheSauvegardeeRequest = RD.NotAsked
                }
                    |> resetModals
        in
        ( newModel
        , Effect.batch
            [ Effect.fromCmd <| getLocaux nextRequestNumber <| newModel.currentUrl
            , Effect.fromCmd (Http.cancel <| getLocauxTracker currentRequestNumber)
            , Effect.fromShared <| Shared.ScrollIntoView listFiltersTagsId
            , filters.adresseCommune
                |> Maybe.map .id
                |> Maybe.map getAdresseVoies
                |> Maybe.withDefault Effect.none
            , Maybe.map2 getAdresseNumeros (filters.adresseCommune |> Maybe.map .id) filters.adresseVoie
                |> Maybe.withDefault Effect.none
            ]
        )

    else
        ( model, Effect.none )


hydrateData : (a -> b) -> List a -> List a -> List a
hydrateData accessor sources data =
    data
        |> List.map
            (\datum ->
                sources
                    |> List.filter (accessor >> (==) (accessor datum))
                    |> List.head
                    |> Maybe.withDefault datum
            )


updateUrl : { data | filters : Filters, pagination : Pagination } -> Effect.Effect Shared.Msg Msg
updateUrl { filters, pagination } =
    Effect.fromShared <|
        Shared.Navigate <|
            Route.Locaux <|
                (\q ->
                    if q == "" then
                        Nothing

                    else
                        Just q
                )
                <|
                    filtersAndPaginationToQuery ( filters, pagination )


toHref : ( Filters, Pagination ) -> Int -> String
toHref ( filters, pagination ) newPage =
    Route.toUrl <|
        Route.Locaux <|
            (\q ->
                if q == "" then
                    Nothing

                else
                    Just q
            )
            <|
                filtersAndPaginationToQuery ( filters, { pagination | page = newPage } )


queryToFiltersAndPagination : Maybe String -> ( Filters, Pagination )
queryToFiltersAndPagination rawQuery =
    case rawQuery of
        Nothing ->
            ( emptyFilters, defaultPagination )

        Just query ->
            ( queryToFilters query, queryToPagination query )


filtersAndPaginationToQuery : ( Filters, Pagination ) -> String
filtersAndPaginationToQuery ( filters, pagination ) =
    QS.merge (filtersToQuery filters) (paginationToQuery pagination)
        |> QS.serialize (QS.config |> QS.addQuestionMark False)


queryKeys :
    { nom : String
    , proprietaire : String
    , invariant : String
    , numero : String
    , rue : String
    , etages : String
    , natures : String
    , categories : String
    , types : String
    , statut : String
    , page : String
    , pages : String
    , surfaceTotaleMax : String
    , surfaceTotaleMin : String
    , surfaceVenteMax : String
    , surfaceVenteMin : String
    , surfaceReserveMax : String
    , surfaceReserveMin : String
    , surfaceExterieureMax : String
    , surfaceExterieureMin : String
    , surfaceStationnementCouverteMax : String
    , surfaceStationnementCouverteMin : String
    , surfaceStationnementNonCouverteMax : String
    , surfaceStationnementNonCouverteMin : String
    , vacance : String
    , communes : String
    , epcis : String
    , departements : String
    , regions : String
    , qpvs : String
    , tis : String
    , adresseCommune : String
    , adresseVoie : String
    , adresseNumero : String
    , cadastreSection : String
    , cadastreParcelle : String
    , tri : String
    , direction : String
    , localisations : String
    , motsCles : String
    , accompagnes : String
    , accompagnesApres : String
    , accompagnesAvant : String
    , favoris : String
    }
queryKeys =
    { nom = "nom"
    , proprietaire = "proprio"
    , invariant = "invariant"
    , numero = "numero"
    , rue = "rue"
    , etages = "etages"
    , natures = "natures"
    , categories = "categories"
    , types = "types"
    , statut = "statut"
    , page = "page"
    , pages = "pages"
    , surfaceTotaleMax = "stmax"
    , surfaceTotaleMin = "stmin"
    , surfaceVenteMax = "svmax"
    , surfaceVenteMin = "svmin"
    , surfaceReserveMax = "srmax"
    , surfaceReserveMin = "srmin"
    , surfaceExterieureMax = "semax"
    , surfaceExterieureMin = "semin"
    , surfaceStationnementCouverteMax = "sscmax"
    , surfaceStationnementCouverteMin = "sscmin"
    , surfaceStationnementNonCouverteMax = "ssncmax"
    , surfaceStationnementNonCouverteMin = "ssncmin"
    , vacance = "vacance"
    , communes = "communes"
    , epcis = "epcis"
    , departements = "departements"
    , regions = "regions"
    , qpvs = "qpvs"
    , tis = "territoireIndustries"
    , adresseCommune = "aCommune"
    , adresseVoie = "aVoie"
    , adresseNumero = "aNumero"
    , cadastreSection = "aSection"
    , cadastreParcelle = "aParcelle"
    , tri = "tri"
    , direction = "direction"
    , localisations = "localisations"
    , motsCles = "motsCles"
    , accompagnes = "accompagnes"
    , accompagnesApres = "accompagnesApres"
    , accompagnesAvant = "accompagnesAvant"
    , favoris = "favoris"
    }


filtersToQuery : Filters -> QS.Query
filtersToQuery filters =
    let
        setNom =
            case filters.nom of
                "" ->
                    identity

                nom ->
                    nom
                        |> QS.setStr queryKeys.nom

        setProprietaire =
            case filters.proprietaire of
                "" ->
                    identity

                proprietaire ->
                    proprietaire
                        |> QS.setStr queryKeys.proprietaire

        setInvariant =
            case filters.invariant of
                "" ->
                    identity

                invariant ->
                    invariant
                        |> QS.setStr queryKeys.invariant

        setAdresseCommune =
            filters.adresseCommune
                |> Maybe.map .id
                |> Maybe.map (QS.setStr queryKeys.adresseCommune)
                |> Maybe.withDefault identity

        setAdresseVoie =
            filters.adresseVoie
                |> Maybe.map (QS.setStr queryKeys.adresseVoie)
                |> Maybe.withDefault identity

        setAdresseNumero =
            filters.adresseNumero
                |> Maybe.map (QS.setStr queryKeys.adresseNumero)
                |> Maybe.withDefault identity

        setCadastreSection =
            filters.cadastreSection
                |> Maybe.map (QS.setStr queryKeys.cadastreSection)
                |> Maybe.withDefault identity

        setCadastreParcelle =
            filters.cadastreParcelle
                |> Maybe.map (QS.setStr queryKeys.cadastreParcelle)
                |> Maybe.withDefault identity

        setEtage =
            filters.etages
                |> List.map .id
                |> QS.setListStr queryKeys.etages

        setNature =
            filters.natures
                |> List.map .id
                |> QS.setListStr queryKeys.natures

        setCategorie =
            filters.categories
                |> List.map .id
                |> QS.setListStr queryKeys.categories

        setSurfaceTotaleMin =
            filters.surfaces
                |> .totale
                |> .min
                |> Maybe.map (QS.setStr queryKeys.surfaceTotaleMin)
                |> Maybe.withDefault identity

        setSurfaceTotaleMax =
            filters.surfaces
                |> .totale
                |> .max
                |> Maybe.map (QS.setStr queryKeys.surfaceTotaleMax)
                |> Maybe.withDefault identity

        setSurfaceVenteMin =
            filters.surfaces
                |> .vente
                |> .min
                |> Maybe.map (QS.setStr queryKeys.surfaceVenteMin)
                |> Maybe.withDefault identity

        setSurfaceVenteMax =
            filters.surfaces
                |> .vente
                |> .max
                |> Maybe.map (QS.setStr queryKeys.surfaceVenteMax)
                |> Maybe.withDefault identity

        setSurfaceReserveMin =
            filters.surfaces
                |> .reserve
                |> .min
                |> Maybe.map (QS.setStr queryKeys.surfaceReserveMin)
                |> Maybe.withDefault identity

        setSurfaceReserveMax =
            filters.surfaces
                |> .reserve
                |> .max
                |> Maybe.map (QS.setStr queryKeys.surfaceReserveMax)
                |> Maybe.withDefault identity

        setSurfaceExterieureMin =
            filters.surfaces
                |> .exterieure
                |> .min
                |> Maybe.map (QS.setStr queryKeys.surfaceExterieureMin)
                |> Maybe.withDefault identity

        setSurfaceExterieureMax =
            filters.surfaces
                |> .exterieure
                |> .max
                |> Maybe.map (QS.setStr queryKeys.surfaceExterieureMax)
                |> Maybe.withDefault identity

        setSurfaceStationnementCouverteMin =
            filters.surfaces
                |> .stationnementCouverte
                |> .min
                |> Maybe.map (QS.setStr queryKeys.surfaceStationnementCouverteMin)
                |> Maybe.withDefault identity

        setSurfaceStationnementCouverteMax =
            filters.surfaces
                |> .stationnementCouverte
                |> .max
                |> Maybe.map (QS.setStr queryKeys.surfaceStationnementCouverteMax)
                |> Maybe.withDefault identity

        setSurfaceStationnementNonCouverteMin =
            filters.surfaces
                |> .stationnementNonCouverte
                |> .min
                |> Maybe.map (QS.setStr queryKeys.surfaceStationnementNonCouverteMin)
                |> Maybe.withDefault identity

        setSurfaceStationnementNonCouverteMax =
            filters.surfaces
                |> .stationnementNonCouverte
                |> .max
                |> Maybe.map (QS.setStr queryKeys.surfaceStationnementNonCouverteMax)
                |> Maybe.withDefault identity

        setVac =
            if List.all (\v -> List.member v filters.vacances) [ VOccupe, VVacant, VNonRenseigne ] then
                identity

            else
                filters.vacances
                    |> List.map vacancesToString
                    |> QS.setListStr queryKeys.vacance

        setCommune =
            filters.communes
                |> List.map .id
                |> QS.setListStr queryKeys.communes

        setOrder =
            case filters.orderBy of
                Alphabetique ->
                    QS.setStr queryKeys.tri <|
                        orderByToString Alphabetique

                LocalConsultation ->
                    identity

        setDirection =
            case filters.orderDirection of
                Asc ->
                    QS.setStr queryKeys.direction <|
                        orderDirectionToString Asc

                Desc ->
                    identity

        setZones =
            case filters.zones of
                [] ->
                    identity

                zones ->
                    zones
                        |> QS.setListStr queryKeys.localisations

        setMots =
            case filters.mots of
                [] ->
                    identity

                mots ->
                    mots
                        |> QS.setListStr queryKeys.motsCles

        setAccompagnes =
            filters.accompagnes
                |> List.map accompagnesToString
                |> QS.setListStr queryKeys.accompagnes

        setAccompagnesApres =
            filters.accompagnesApres
                |> Maybe.map Lib.Date.formatDateToYYYYMMDD
                |> Maybe.map (QS.setStr queryKeys.accompagnesApres)
                |> Maybe.withDefault identity

        setAccompagnesAvant =
            filters.accompagnesAvant
                |> Maybe.map Lib.Date.formatDateToYYYYMMDD
                |> Maybe.map (QS.setStr queryKeys.accompagnesAvant)
                |> Maybe.withDefault identity

        setFavoris =
            case filters.favoris of
                [] ->
                    identity

                [ True ] ->
                    "oui"
                        |> QS.setStr queryKeys.favoris

                [ False ] ->
                    "non"
                        |> QS.setStr queryKeys.favoris

                [ True, False ] ->
                    [ "oui", "non" ]
                        |> QS.setListStr queryKeys.favoris

                [ False, True ] ->
                    [ "oui", "non" ]
                        |> QS.setListStr queryKeys.favoris

                _ ->
                    identity

        setEpci =
            filters.epcis
                |> List.map .id
                |> QS.setListStr queryKeys.epcis

        setDepartement =
            filters.departements
                |> List.map .id
                |> QS.setListStr queryKeys.departements

        setRegion =
            filters.regions
                |> List.map .id
                |> QS.setListStr queryKeys.regions

        setQpvs =
            if filters.tousQpvs then
                QS.setListStr queryKeys.qpvs [ "tous" ]

            else
                case filters.qpvs of
                    [] ->
                        identity

                    qpvs ->
                        qpvs
                            |> List.map .id
                            |> QS.setListStr queryKeys.qpvs

        setTis =
            if filters.tousTis then
                QS.setListStr queryKeys.tis [ "tous" ]

            else
                case filters.tis of
                    [] ->
                        identity

                    tis ->
                        tis
                            |> List.map .id
                            |> QS.setListStr queryKeys.tis
    in
    QS.empty
        |> setNom
        |> setProprietaire
        |> setInvariant
        |> setAdresseCommune
        |> setAdresseVoie
        |> setAdresseNumero
        |> setCadastreSection
        |> setCadastreParcelle
        |> setEtage
        |> setNature
        |> setCategorie
        |> setCommune
        |> setEpci
        |> setDepartement
        |> setRegion
        |> setQpvs
        |> setTis
        |> setSurfaceTotaleMin
        |> setSurfaceTotaleMax
        |> setSurfaceVenteMin
        |> setSurfaceVenteMax
        |> setSurfaceReserveMin
        |> setSurfaceReserveMax
        |> setSurfaceExterieureMin
        |> setSurfaceExterieureMax
        |> setSurfaceStationnementCouverteMin
        |> setSurfaceStationnementCouverteMax
        |> setSurfaceStationnementNonCouverteMin
        |> setSurfaceStationnementNonCouverteMax
        |> setVac
        |> setOrder
        |> setDirection
        |> setZones
        |> setMots
        |> setFavoris
        |> setAccompagnes
        |> setAccompagnesApres
        |> setAccompagnesAvant


queryToFilters : String -> Filters
queryToFilters rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    nom =
                        query
                            |> QS.getAsStringList queryKeys.nom
                            |> List.head
                            |> Maybe.withDefault ""

                    proprietaire =
                        query
                            |> QS.getAsStringList queryKeys.proprietaire
                            |> List.head
                            |> Maybe.withDefault ""

                    invariant =
                        query
                            |> QS.getAsStringList queryKeys.invariant
                            |> List.head
                            |> Maybe.withDefault ""

                    etages =
                        query
                            |> QS.getAsStringList queryKeys.etages
                            |> List.map (\id -> { id = id, nom = "" })

                    newEtages =
                        hydrateData .id optionsEtages etages

                    vacances =
                        query
                            |> QS.getAsStringList queryKeys.vacance
                            |> List.filterMap stringToVacances

                    natures =
                        query
                            |> QS.getAsStringList queryKeys.natures
                            |> List.map (\id -> { id = id, nom = "" })

                    categories =
                        query
                            |> QS.getAsStringList queryKeys.categories
                            |> List.map (\id -> { id = id, nom = "" })

                    surfaceTotaleMax =
                        query
                            |> QS.getAsStringList queryKeys.surfaceTotaleMax
                            |> List.head

                    surfaceTotaleMin =
                        query
                            |> QS.getAsStringList queryKeys.surfaceTotaleMin
                            |> List.head

                    surfaceVenteMax =
                        query
                            |> QS.getAsStringList queryKeys.surfaceVenteMax
                            |> List.head

                    surfaceVenteMin =
                        query
                            |> QS.getAsStringList queryKeys.surfaceVenteMin
                            |> List.head

                    surfaceReserveMax =
                        query
                            |> QS.getAsStringList queryKeys.surfaceReserveMax
                            |> List.head

                    surfaceReserveMin =
                        query
                            |> QS.getAsStringList queryKeys.surfaceReserveMin
                            |> List.head

                    surfaceExterieureMax =
                        query
                            |> QS.getAsStringList queryKeys.surfaceExterieureMax
                            |> List.head

                    surfaceExterieureMin =
                        query
                            |> QS.getAsStringList queryKeys.surfaceExterieureMin
                            |> List.head

                    surfaceStationnementCouverteMax =
                        query
                            |> QS.getAsStringList queryKeys.surfaceStationnementCouverteMax
                            |> List.head

                    surfaceStationnementCouverteMin =
                        query
                            |> QS.getAsStringList queryKeys.surfaceStationnementCouverteMin
                            |> List.head

                    surfaceStationnementNonCouverteMax =
                        query
                            |> QS.getAsStringList queryKeys.surfaceStationnementNonCouverteMax
                            |> List.head

                    surfaceStationnementNonCouverteMin =
                        query
                            |> QS.getAsStringList queryKeys.surfaceStationnementNonCouverteMin
                            |> List.head

                    communes =
                        query
                            |> QS.getAsStringList queryKeys.communes
                            |> List.map (\id -> { id = id, nom = "", departementId = "" })

                    epcis =
                        query
                            |> QS.getAsStringList queryKeys.epcis
                            |> List.map (\id -> { id = id, nom = "" })

                    departements =
                        query
                            |> QS.getAsStringList queryKeys.departements
                            |> List.map (\id -> { id = id, nom = "" })

                    regions =
                        query
                            |> QS.getAsStringList queryKeys.regions
                            |> List.map (\id -> { id = id, nom = "" })

                    ( tousQpvs, qpvs ) =
                        query
                            |> QS.getAsStringList queryKeys.qpvs
                            |> (\qpvIds ->
                                    if List.member "tous" qpvIds then
                                        ( True, [] )

                                    else
                                        ( False, qpvIds |> List.map (\id -> { id = id, nom = "" }) )
                               )

                    ( tousTis, tis ) =
                        query
                            |> QS.getAsStringList queryKeys.tis
                            |> (\tiIds ->
                                    if List.member "tous" tiIds then
                                        ( True, [] )

                                    else
                                        ( False, tiIds |> List.map (\id -> { id = id, nom = "" }) )
                               )

                    adresseCommune =
                        query
                            |> QS.getAsStringList queryKeys.adresseCommune
                            |> List.head
                            |> Maybe.map (\id -> { id = id, nom = "", departementId = "" })

                    adresseVoie =
                        query
                            |> QS.getAsStringList queryKeys.adresseVoie
                            |> List.head

                    adresseNumero =
                        query
                            |> QS.getAsStringList queryKeys.adresseNumero
                            |> List.head

                    cadastreSection =
                        query
                            |> QS.getAsStringList queryKeys.cadastreSection
                            |> List.head

                    cadastreParcelle =
                        query
                            |> QS.getAsStringList queryKeys.cadastreParcelle
                            |> List.head

                    adresseType =
                        adresseVoie
                            |> Maybe.map (\_ -> Just AdresseVoie)
                            |> Maybe.withDefault (cadastreSection |> Maybe.map (\_ -> AdresseParcelle))
                            |> Maybe.withDefault AdresseVoie

                    orderBy =
                        query
                            |> QS.getAsStringList queryKeys.tri
                            |> List.head
                            |> Maybe.andThen stringToOrderBy
                            |> Maybe.withDefault LocalConsultation

                    orderDirection =
                        query
                            |> QS.getAsStringList queryKeys.direction
                            |> List.head
                            |> Maybe.andThen stringToOrderDirection
                            |> Maybe.withDefault Desc

                    accompagnes =
                        query
                            |> QS.getAsStringList queryKeys.accompagnes
                            |> List.filterMap stringToAccompagnes

                    accompagnesApres =
                        if not <| List.member AccompagnesOui accompagnes then
                            Nothing

                        else
                            query
                                |> QS.getAsStringList queryKeys.accompagnesApres
                                |> List.head
                                |> Maybe.andThen Lib.Date.dateFromISOString

                    accompagnesAvant =
                        if not <| List.member AccompagnesOui accompagnes then
                            Nothing

                        else
                            query
                                |> QS.getAsStringList queryKeys.accompagnesAvant
                                |> List.head
                                |> Maybe.andThen Lib.Date.dateFromISOString

                    zones =
                        query
                            |> QS.getAsStringList queryKeys.localisations

                    mots =
                        query
                            |> QS.getAsStringList queryKeys.motsCles

                    favoris =
                        query
                            |> QS.getAsStringList queryKeys.favoris
                            |> List.filterMap
                                (\fav ->
                                    if fav == "oui" then
                                        Just True

                                    else if fav == "non" then
                                        Just False

                                    else
                                        Nothing
                                )
                in
                { nom = nom
                , proprietaire = proprietaire
                , invariant = invariant
                , etages = newEtages
                , natures = natures
                , categories = categories
                , surfaces =
                    { totale =
                        { min = surfaceTotaleMin
                        , max = surfaceTotaleMax
                        }
                    , vente =
                        { min = surfaceVenteMin
                        , max = surfaceVenteMax
                        }
                    , reserve =
                        { min = surfaceReserveMin
                        , max = surfaceReserveMax
                        }
                    , exterieure =
                        { min = surfaceExterieureMin
                        , max = surfaceExterieureMax
                        }
                    , stationnementCouverte =
                        { min = surfaceStationnementCouverteMin
                        , max = surfaceStationnementCouverteMax
                        }
                    , stationnementNonCouverte =
                        { min = surfaceStationnementNonCouverteMin
                        , max = surfaceStationnementNonCouverteMax
                        }
                    }
                , vacances = vacances
                , communes = communes
                , epcis = epcis
                , departements = departements
                , regions = regions
                , qpvs = qpvs
                , tousQpvs = tousQpvs
                , tis = tis
                , tousTis = tousTis
                , adresseType = adresseType
                , adresseCommune = adresseCommune
                , adresseVoie = adresseVoie
                , adresseNumero = adresseNumero
                , cadastreSection = cadastreSection
                , cadastreParcelle = cadastreParcelle
                , orderBy = orderBy
                , orderDirection = orderDirection
                , zones = zones
                , mots = mots
                , favoris = favoris
                , accompagnes = accompagnes
                , accompagnesApres = accompagnesApres
                , accompagnesAvant = accompagnesAvant
                }
           )


selectAdresseCommuneId : String
selectAdresseCommuneId =
    "champ-selection-adresse-commune"


selectAdresseVoieId : String
selectAdresseVoieId =
    "champ-selection-adresse-voie"


selectAdresseNumeroId : String
selectAdresseNumeroId =
    "champ-selection-adresse-numero"


selectCadastreSectionId : String
selectCadastreSectionId =
    "champ-selection-cadastre-section"


selectCadastreParcelleId : String
selectCadastreParcelleId =
    "champ-selection-cadastre-parcelle"


selectZonesId : String
selectZonesId =
    "champ-selection-zones"


selectMotsId : String
selectMotsId =
    "champ-selection-mots"


listFiltersTagsId : String
listFiltersTagsId =
    "liste-filters-tags"


greyPlaceholder : Int -> Html msg
greyPlaceholder length =
    "\u{00A0}"
        |> List.repeat length
        |> List.map (text >> List.singleton >> span [ class "w-[1rem] inline-block" ])
        |> span [ class "grey-background m-1" ]
        |> List.singleton
        |> div [ class "pulse-black" ]


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ model.selectAdresseCommune |> SingleSelectRemote.subscriptions
        , model.selectAdresseVoie |> SingleSelect.subscriptions
        , model.selectAdresseNumero |> SingleSelect.subscriptions
        , model.selectCadastreSection |> SingleSelect.subscriptions
        , model.selectCadastreParcelle |> SingleSelect.subscriptions
        , model.selectEtages |> MultiSelect.subscriptions
        , model.selectNatures |> MultiSelect.subscriptions
        , model.selectCategories |> MultiSelect.subscriptions
        , model.selectCommunes |> MultiSelectRemote.subscriptions
        , model.selectEpcis |> MultiSelectRemote.subscriptions
        , model.selectDepartements |> MultiSelectRemote.subscriptions
        , model.selectRegions |> MultiSelectRemote.subscriptions
        , model.selectQpvs |> MultiSelectRemote.subscriptions
        , model.selectTis |> MultiSelectRemote.subscriptions
        , model.selectZones |> MultiSelect.subscriptions
        , model.selectMots |> MultiSelect.subscriptions
        ]


showFilterTags : Model -> Html Msg
showFilterTags model =
    let
        currentUrl =
            model.currentUrl

        naturesSources =
            model.natures
                |> RD.withDefault []

        categoriesSources =
            model.categories
                |> RD.withDefault []

        pagination =
            let
                p =
                    queryToPagination currentUrl
            in
            { p | page = 1 }

        filters =
            currentUrl
                |> queryToFilters
                |> hydrateFilters model.filtresRequete

        { nom, proprietaire, invariant, natures, categories, communes, epcis, departements, regions, qpvs, tousQpvs, tis, tousTis, etages, surfaces, vacances, zones, mots, favoris } =
            filters

        newEtages =
            hydrateData .id optionsEtages etages

        newNatures =
            hydrateData .id naturesSources natures

        newCategories =
            hydrateData .id categoriesSources categories

        newCommunes =
            communes

        newCommune =
            filters.adresseCommune

        adresseVoie =
            filters.adresseVoie

        adresseNumero =
            filters.adresseNumero

        cadastreSection =
            filters.cadastreSection

        cadastreParcelle =
            filters.cadastreParcelle
    in
    div
        [ class "flex flex-row gap-2 mt-1" ]
        [ DSFR.Tag.groupWrapper <|
            toFilterDeletableTags "Nom"
                (if nom /= "" then
                    [ nom ]

                 else
                    []
                )
                (\_ -> UpdateUrl <| ( { filters | nom = "" }, pagination ))
                identity
                ++ toFilterDeletableTags "Zone"
                    zones
                    (\zone ->
                        UpdateUrl <|
                            ( { filters
                                | zones =
                                    filters.zones
                                        |> List.filter
                                            (\z -> z /= zone)
                              }
                            , pagination
                            )
                    )
                    identity
                ++ toFilterDeletableTags "Mot-clé"
                    mots
                    (\m ->
                        UpdateUrl <|
                            ( { filters
                                | mots =
                                    filters.mots
                                        |> List.filter
                                            (\z -> z /= m)
                              }
                            , pagination
                            )
                    )
                    identity
                ++ toFilterDeletableTags "Propriétaire"
                    (if proprietaire /= "" then
                        [ proprietaire ]

                     else
                        []
                    )
                    (\_ -> UpdateUrl <| ( { filters | proprietaire = "" }, pagination ))
                    identity
                ++ toFilterDeletableTags "Invariant"
                    (if invariant /= "" then
                        [ invariant ]

                     else
                        []
                    )
                    (\_ -> UpdateUrl <| ( { filters | invariant = "" }, pagination ))
                    identity
                ++ toFilterDeletableTags "Étage"
                    newEtages
                    (\c ->
                        UpdateUrl <|
                            ( { filters
                                | etages =
                                    filters.etages
                                        |> List.filter
                                            (\t -> t.id /= c.id)
                              }
                            , pagination
                            )
                    )
                    (\c -> c.id)
                ++ toFilterDeletableTags "Catégorie"
                    newCategories
                    (\c ->
                        UpdateUrl <|
                            ( { filters
                                | categories =
                                    filters.categories
                                        |> List.filter
                                            (\t -> t.id /= c.id)
                              }
                            , pagination
                            )
                    )
                    (\c -> c.id)
                ++ toFilterDeletableTags "Nature"
                    newNatures
                    (\c ->
                        UpdateUrl <|
                            ( { filters
                                | natures =
                                    filters.natures
                                        |> List.filter
                                            (\t -> t.id /= c.id)
                              }
                            , pagination
                            )
                    )
                    (\c -> c.id)
                ++ toFilterDeletableTags "Adresse (commune)"
                    (newCommune
                        |> Maybe.map List.singleton
                        |> Maybe.withDefault []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | adresseCommune = Nothing
                                , adresseVoie = Nothing
                                , adresseNumero = Nothing
                              }
                            , pagination
                            )
                    )
                    (\e -> e.nom)
                ++ toFilterDeletableTags "Adresse (voie)"
                    (adresseVoie
                        |> Maybe.map List.singleton
                        |> Maybe.withDefault []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | adresseVoie = Nothing
                                , adresseNumero = Nothing
                              }
                            , pagination
                            )
                    )
                    identity
                ++ toFilterDeletableTags "Adresse (numéro)"
                    (adresseNumero
                        |> Maybe.map List.singleton
                        |> Maybe.withDefault []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | adresseNumero = Nothing
                              }
                            , pagination
                            )
                    )
                    identity
                ++ toFilterDeletableTags "Cadastre (section)"
                    (cadastreSection
                        |> Maybe.map List.singleton
                        |> Maybe.withDefault []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | cadastreSection = Nothing
                                , cadastreParcelle = Nothing
                              }
                            , pagination
                            )
                    )
                    identity
                ++ toFilterDeletableTags "Cadastre (parcelle)"
                    (cadastreParcelle
                        |> Maybe.map List.singleton
                        |> Maybe.withDefault []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | cadastreParcelle = Nothing
                              }
                            , pagination
                            )
                    )
                    identity
                ++ toFilterDeletableTags "Commune"
                    newCommunes
                    (\co ->
                        UpdateUrl <|
                            ( { filters
                                | communes =
                                    filters.communes
                                        |> List.filter
                                            (\t -> t.id /= co.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.nom)
                ++ toFilterDeletableTags "Epci"
                    epcis
                    (\co ->
                        UpdateUrl <|
                            ( { filters
                                | epcis =
                                    filters.epcis
                                        |> List.filter
                                            (\t -> t.id /= co.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.nom)
                ++ toFilterDeletableTags "Département"
                    departements
                    (\co ->
                        UpdateUrl <|
                            ( { filters
                                | departements =
                                    filters.departements
                                        |> List.filter
                                            (\t -> t.id /= co.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.nom)
                ++ toFilterDeletableTags "Région"
                    regions
                    (\co ->
                        UpdateUrl <|
                            ( { filters
                                | regions =
                                    filters.regions
                                        |> List.filter
                                            (\t -> t.id /= co.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.nom)
                ++ toFilterDeletableTags "QPV"
                    qpvs
                    (\qpv ->
                        UpdateUrl <|
                            ( { filters
                                | qpvs =
                                    filters.qpvs
                                        |> List.filter
                                            (\t -> t.id /= qpv.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.nom)
                ++ toFilterDeletableTags "QPV"
                    (if tousQpvs then
                        [ tousQpvs ]

                     else
                        []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | tousQpvs = False
                              }
                            , pagination
                            )
                    )
                    (\_ -> "Tous")
                ++ toFilterDeletableTags "TI"
                    tis
                    (\ti ->
                        UpdateUrl <|
                            ( { filters
                                | tis =
                                    filters.tis
                                        |> List.filter
                                            (\t -> t.id /= ti.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.nom)
                ++ toFilterDeletableTags "TI"
                    (if tousTis then
                        [ tousTis ]

                     else
                        []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | tousTis = False
                              }
                            , pagination
                            )
                    )
                    (\_ -> "Tous")
                ++ toFilterDeletableTags "Vacance"
                    vacances
                    (\v ->
                        UpdateUrl <|
                            ( { filters
                                | vacances =
                                    filters.vacances
                                        |> List.filter (\t -> t /= v)
                              }
                            , pagination
                            )
                    )
                    vacancesToDisplay
                ++ (surfaceTypes
                        |> List.concatMap
                            (\surfaceType ->
                                [ Min
                                , Max
                                ]
                                    |> List.concatMap
                                        (\minMaxType ->
                                            toFilterDeletableTags (surfaceTypeToDisplay surfaceType ++ " (" ++ minMaxTypeToDisplay minMaxType ++ ")")
                                                ([ getSurfaceTypeValue surfaces minMaxType surfaceType ] |> List.filterMap identity)
                                                (\_ ->
                                                    UpdateUrl <|
                                                        ( { filters
                                                            | surfaces = updateSurfaces surfaceType minMaxType Nothing surfaces
                                                          }
                                                        , pagination
                                                        )
                                                )
                                                (\v -> v ++ " m²")
                                        )
                            )
                   )
                ++ toFilterDeletableTags "Local accompagné"
                    filters.accompagnes
                    (\s ->
                        UpdateUrl <|
                            ( { filters
                                | accompagnes =
                                    filters.accompagnes
                                        |> List.filter
                                            (\t -> t /= s)
                              }
                            , pagination
                            )
                    )
                    (\s ->
                        case s of
                            AccompagnesOui ->
                                "Oui"

                            AccompagnesNon ->
                                "Non"
                    )
                ++ toFilterDeletableTags "Accompagné"
                    ([ filters.accompagnesApres ] |> List.filterMap identity)
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | accompagnesApres = Nothing
                              }
                            , pagination
                            )
                    )
                    (Lib.Date.formatDateShort >> (++) "à partir du ")
                ++ toFilterDeletableTags "Accompagné"
                    ([ filters.accompagnesAvant ] |> List.filterMap identity)
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | accompagnesAvant = Nothing
                              }
                            , pagination
                            )
                    )
                    (Lib.Date.formatDateShort >> (\d -> "jusqu'au " ++ d ++ " (inclus)"))
                ++ toFilterDeletableTags "Favoris"
                    favoris
                    (\e ->
                        UpdateUrl <|
                            ( setFavorisInFilters False e filters
                            , pagination
                            )
                    )
                    (\f ->
                        if f then
                            "Oui"

                        else
                            "Non"
                    )
        ]


getSurfaceTypeValue : Surfaces -> MinMaxType -> SurfaceType -> Maybe String
getSurfaceTypeValue surfaces minMaxType surfaceType =
    case ( surfaceType, minMaxType ) of
        ( STotale, Max ) ->
            surfaces.totale.max

        ( STotale, Min ) ->
            surfaces.totale.min

        ( SVente, Max ) ->
            surfaces.vente.max

        ( SVente, Min ) ->
            surfaces.vente.min

        ( SReserve, Max ) ->
            surfaces.reserve.max

        ( SReserve, Min ) ->
            surfaces.reserve.min

        ( SExterieure, Max ) ->
            surfaces.exterieure.max

        ( SExterieure, Min ) ->
            surfaces.exterieure.min

        ( SStationnementCouverte, Max ) ->
            surfaces.stationnementCouverte.max

        ( SStationnementCouverte, Min ) ->
            surfaces.stationnementCouverte.min

        ( SStationnementNonCouverte, Max ) ->
            surfaces.stationnementNonCouverte.max

        ( SStationnementNonCouverte, Min ) ->
            surfaces.stationnementNonCouverte.min


toFilterDeletableTags : String -> List data -> (data -> msg) -> (data -> String) -> List (Html msg)
toFilterDeletableTags prefix list toMsg toString =
    list
        |> List.sortBy toString
        |> List.map
            (\data ->
                DSFR.Tag.deletable toMsg { data = data, toString = \d -> prefix ++ "\u{00A0}: " ++ toString d }
            )
        |> List.map DSFR.Tag.oneMedium


{-| On détermine s'il existe des filtres qui ne sont pas des critères d'ordre et de direction
-}
showingUnfilteredResults : String -> Bool
showingUnfilteredResults currentUrl =
    List.member currentUrl
        [ ""
        , queryKeys.tri ++ "=" ++ orderDirectionToString Asc
        , queryKeys.direction ++ "=" ++ orderByToString LocalConsultation
        , queryKeys.tri ++ "=" ++ orderDirectionToString Asc ++ "&" ++ queryKeys.direction ++ "=" ++ orderByToString LocalConsultation
        , queryKeys.direction ++ "=" ++ orderByToString Alphabetique
        , queryKeys.tri ++ "=" ++ orderDirectionToString Asc ++ "&" ++ queryKeys.direction ++ "=" ++ orderByToString Alphabetique
        ]


codeToOrderBy : String -> Msg
codeToOrderBy code =
    let
        match ob od () =
            if code == (orderByToString ob ++ orderDirectionToString od) then
                Just ( ob, od )

            else
                Nothing

        or : (() -> Maybe data) -> Maybe data -> Maybe data
        or orElse maybe =
            case maybe of
                Just m ->
                    Just m

                Nothing ->
                    orElse ()
    in
    match LocalConsultation Desc ()
        |> or (match LocalConsultation Asc)
        |> or (match Alphabetique Desc)
        |> or (match Alphabetique Asc)
        |> Maybe.withDefault ( LocalConsultation, Desc )
        |> (\( ob, od ) -> ClickedOrderBy ob od)


selectOrder : String -> { a | orderBy : OrderBy, orderDirection : OrderDirection } -> Html Msg
selectOrder selectId { orderBy, orderDirection } =
    let
        toOption ob od lab =
            option
                [ Html.Attributes.value <| orderByToString ob ++ orderDirectionToString od
                , Html.Attributes.selected <| ( orderBy, orderDirection ) == ( ob, od )
                ]
                [ text lab ]
    in
    select
        [ class "fr-select"
        , Html.Attributes.id selectId
        , Html.Attributes.name "select-sortBy"
        , Events.onInput codeToOrderBy
        ]
        [ toOption LocalConsultation Desc "Derniers consultés"
        , toOption Alphabetique Asc "Noms classés par ordre alphabétique"

        -- , toOption Alphabetique Desc "Noms classés par ordre alphabétique inversé"
        ]


type GeoJsonFormat
    = Regular


geoJsonFormatToString : GeoJsonFormat -> String
geoJsonFormatToString geoJsonFormat =
    case geoJsonFormat of
        Regular ->
            "geojson"


viewGeoJsonModal : String -> String -> Int -> WebData String -> Maybe GeoJsonFormat -> Html Msg
viewGeoJsonModal appUrl currentUrl results rechercheSauvegardeeRequest gje =
    let
        ( opened, content, title ) =
            case gje of
                Nothing ->
                    ( False
                    , nothing
                    , text "Exporter les résultats"
                    )

                Just geoJsonExport ->
                    ( True
                    , viewGeoJsonModalBody appUrl currentUrl results rechercheSauvegardeeRequest geoJsonExport
                    , text "Exporter les résultats"
                    )
    in
    DSFR.Modal.view
        { id = "geojson-export"
        , label = "geojson-export"
        , openMsg = NoOp
        , closeMsg = Just <| ClickedCanceledGeoJson
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


locauxMax : Int
locauxMax =
    1000


viewGeoJsonModalBody : String -> String -> Int -> RD.RemoteData e String -> GeoJsonFormat -> Html Msg
viewGeoJsonModalBody appUrl currentUrl results rechercheSauvegardeeRequest geoJsonExport =
    let
        locauxTropNombreux =
            results > locauxMax
    in
    div []
        [ div [ Grid.gridRow, Grid.gridRowGutters ]
            [ div [ Grid.col12, class "flex flex-col gap-8" ]
                [ p [ class "!mb-0" ]
                    [ text "Vous pouvez utiliser le fichier téléchargé pour visualiser les établissements sur une carte grâce à des services en ligne comme "
                    , let
                        link =
                            Url.Builder.crossOrigin "https://umap.incubateur.anct.gouv.fr"
                                [ "fr", "map" ]
                                []
                      in
                      Typo.externalLink link [] [ text link ]
                    , text "."
                    ]
                ]
            ]
        , div [ Grid.gridRow, Grid.gridRowGutters ]
            [ div [ Grid.col12 ]
                [ div [ class "flex flex-row justify-end p-4 gap-4" ]
                    [ DSFR.Button.view <|
                        DSFR.Button.leftIcon DSFR.Icons.System.eyeLine <|
                            DSFR.Button.secondary <|
                                case rechercheSauvegardeeRequest of
                                    RD.Success lien ->
                                        let
                                            root =
                                                if String.contains "http" lien then
                                                    ""

                                                else
                                                    appUrl
                                        in
                                        DSFR.Button.new { onClick = Nothing, label = "Visualiser sur umap" }
                                            |> DSFR.Button.linkButtonExternal (Lib.Umap.umapLinkExport root lien)

                                    _ ->
                                        DSFR.Button.new { onClick = Just RequestedRechercheSauvegardee, label = "Créer un lien de visualisation" }
                                            |> DSFR.Button.withDisabled locauxTropNombreux
                                            |> DSFR.Button.withAttrs
                                                [ if locauxTropNombreux then
                                                    Html.Attributes.title <| "Impossible de visualiser plus de " ++ String.fromInt locauxMax ++ " éléments"

                                                  else
                                                    empty
                                                ]
                    , let
                        exportUrl =
                            (Api.getLocaux <|
                                currentUrl
                            )
                                ++ "&format="
                                ++ geoJsonFormatToString geoJsonExport
                      in
                      DSFR.Button.new { onClick = Nothing, label = "Exporter" }
                        |> DSFR.Button.linkButtonExternal exportUrl
                        |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                        |> DSFR.Button.view
                    ]
                ]
            ]
        ]


getAdresseVoies : String -> Effect.Effect Shared.Msg Msg
getAdresseVoies communeId =
    Effect.fromCmd <|
        get
            { url = Api.getLocalAdresseVoies communeId
            }
            { toShared = SharedMsg
            , logger = Just LogError
            , handler = ReceivedVoies
            }
            (Decode.field "voieNoms" <| Decode.list Decode.string)


getAdresseNumeros : String -> String -> Effect.Effect Shared.Msg Msg
getAdresseNumeros communeId voieNom =
    Effect.fromCmd <|
        get
            { url = Api.getLocalAdresseNumeros communeId voieNom
            }
            { toShared = SharedMsg
            , logger = Just LogError
            , handler = ReceivedNumeros
            }
            (Decode.field "numeros" <| Decode.list Decode.string)


getCadastreSections : String -> Effect.Effect Shared.Msg Msg
getCadastreSections communeId =
    Effect.fromCmd <|
        get
            { url = Api.getLocalCadastreSections communeId
            }
            { toShared = SharedMsg
            , logger = Just LogError
            , handler = ReceivedCadastreSections
            }
            (Decode.field "sections" <| Decode.list Decode.string)


getCadastreParcelles : String -> String -> Effect.Effect Shared.Msg Msg
getCadastreParcelles communeId cadastreSection =
    Effect.fromCmd <|
        get
            { url = Api.getLocalCadastreParcelles communeId cadastreSection
            }
            { toShared = SharedMsg
            , logger = Just LogError
            , handler = ReceivedCadastreParcelles
            }
            (Decode.field "parcelles" <| Decode.list Decode.string)


getFiltersSource : Cmd Msg
getFiltersSource =
    get
        { url = Api.getEquipeMetaFiltres "local" }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedFiltersSource
        }
        decodeFiltersSource


decodeFiltersSource : Decoder FiltersSource
decodeFiltersSource =
    Decode.succeed FiltersSource
        |> andMap (Decode.field "localisations" <| Decode.list <| Decode.field "nom" Decode.string)
        |> andMap (Decode.field "motsCles" <| Decode.list <| Decode.field "nom" Decode.string)


getRechercheSauvegardeeRequest : String -> Cmd Msg
getRechercheSauvegardeeRequest nextUrl =
    get
        { url = Api.getLocauxRechercheSauvegardeeLien <| nextUrl
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedRechercheSauvegardee
        }
        (Decode.field "url" Decode.string)


updateLocalInList : String -> Bool -> ( LocalSimple, PortefeuilleInfos ) -> ( LocalSimple, PortefeuilleInfos )
updateLocalInList id add (( local, portefeuilleInfos ) as localSimpleAvecPortefeuille) =
    if local.id == id then
        ( local
        , { portefeuilleInfos | favori = add }
        )

    else
        localSimpleAvecPortefeuille


requestToggleFavorite : String -> Bool -> Cmd Msg
requestToggleFavorite siret favorite =
    post
        { url = Api.toggleFavoriteLocal siret
        , body =
            [ ( "favori", Encode.bool favorite )
            ]
                |> Encode.object
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler =
            \webdata ->
                GotFavoriteResult siret <|
                    case webdata of
                        RD.Success _ ->
                            favorite

                        _ ->
                            not favorite
        }
        (Decode.succeed ())


selectAdresseCommuneConfig : SingleSelectRemote.SelectConfig Commune
selectAdresseCommuneConfig =
    { headers = []
    , url = Api.rechercheTerritoireGeo (Filters.situationGeographiqueToCode Filters.SurMonTerritoire) "commune"
    , optionDecoder = Decode.field "territoires" <| Decode.list <| Data.Commune.decodeCommune
    }


selectCommunesId : String
selectCommunesId =
    "champ-selection-communes"


selectCommunesConfig : MultiSelectRemote.SelectConfig Commune
selectCommunesConfig =
    { headers = []
    , url = Api.rechercheTerritoireGeo (Filters.situationGeographiqueToCode Filters.SurMonTerritoire) "commune"
    , optionDecoder = Decode.field "territoires" <| Decode.list <| Data.Commune.decodeCommune
    }


selectEpcisId : String
selectEpcisId =
    "champ-selection-epcis"


selectEpcisConfig : MultiSelectRemote.SelectConfig Epci
selectEpcisConfig =
    { headers = []
    , url = Api.rechercheTerritoireGeo (Filters.situationGeographiqueToCode Filters.SurMonTerritoire) "epci"
    , optionDecoder =
        Decode.field "territoires" <|
            Decode.list <|
                Decode.map2 Epci
                    (Decode.field "id" Decode.string)
                    (Decode.field "nom" Decode.string)
    }


selectDepartementsId : String
selectDepartementsId =
    "champ-selection-departements"


selectDepartementsConfig : MultiSelectRemote.SelectConfig Departement
selectDepartementsConfig =
    { headers = []
    , url = Api.rechercheTerritoireGeo (Filters.situationGeographiqueToCode Filters.SurMonTerritoire) "departement"
    , optionDecoder =
        Decode.field "territoires" <|
            Decode.list <|
                Decode.map2 Departement
                    (Decode.field "id" Decode.string)
                    (Decode.field "nom" Decode.string)
    }


selectRegionsId : String
selectRegionsId =
    "champ-selection-regions"


selectRegionsConfig : MultiSelectRemote.SelectConfig Region
selectRegionsConfig =
    { headers = []
    , url = Api.rechercheTerritoireGeo (Filters.situationGeographiqueToCode Filters.SurMonTerritoire) "region"
    , optionDecoder =
        Decode.field "territoires" <|
            Decode.list <|
                Decode.map2 Region
                    (Decode.field "id" Decode.string)
                    (Decode.field "nom" Decode.string)
    }


selectQpvsId : String
selectQpvsId =
    "champ-selection-qpvs"


selectQpvsConfig : MultiSelectRemote.SelectConfig Qpv
selectQpvsConfig =
    { headers = []
    , url = Api.rechercheQpvGeo (Filters.situationGeographiqueToCode Filters.SurMonTerritoire)
    , optionDecoder =
        Decode.field "qpvs" <|
            Decode.list <|
                Decode.map2 Qpv
                    (Decode.field "id" Decode.string)
                    (Decode.field "nom" Decode.string)
    }


selectTisId : String
selectTisId =
    "champ-selection-tis"


selectTisConfig : MultiSelectRemote.SelectConfig TerritoireIndustrie
selectTisConfig =
    { headers = []
    , url = Api.rechercheTerritoireGeo (Filters.situationGeographiqueToCode Filters.SurMonTerritoire) "territoire_industrie"
    , optionDecoder =
        Decode.field "territoires" <|
            Decode.list <|
                Decode.map2 TerritoireIndustrie
                    (Decode.field "id" Decode.string)
                    (Decode.field "nom" Decode.string)
    }


selecteurDate : { msg : Maybe Date -> msg, label : Html Never, name : String, value : Maybe Date, min : Maybe Date, max : Maybe Date, disabled : Bool } -> Html msg
selecteurDate { msg, label, name, value, min, max, disabled } =
    DSFR.Input.new
        { value = value |> Maybe.map Lib.Date.formatDateToYYYYMMDD |> Maybe.withDefault ""
        , onInput =
            Date.fromIsoString
                >> Result.toMaybe
                >> msg
        , label = label
        , name = name
        }
        |> DSFR.Input.withExtraAttrs [ class "!mb-0" ]
        |> DSFR.Input.withDisabled disabled
        |> DSFR.Input.date { min = Maybe.map Lib.Date.formatDateToYYYYMMDD min, max = Maybe.map Lib.Date.formatDateToYYYYMMDD max }
        |> DSFR.Input.view


hydrateFilters : FiltresRequete -> Filters -> Filters
hydrateFilters filtresRequete filters =
    let
        newACommune =
            filters.adresseCommune
                |> Maybe.andThen
                    (\c ->
                        filtresRequete.communes
                            |> List.filter (\frc -> frc.id == c.id)
                            |> List.head
                    )

        ( aCommune, communes ) =
            case newACommune of
                Nothing ->
                    ( Nothing, filtresRequete.communes )

                Just ac ->
                    ( Just ac, [] )
    in
    { filters
        | communes = communes
        , epcis = filtresRequete.epcis
        , departements = filtresRequete.departements
        , regions = filtresRequete.regions
        , tis = filtresRequete.territoireIndustries
        , qpvs = filtresRequete.qpvs
        , adresseCommune = aCommune
    }


type alias FiltresRequete =
    { qpvs : List Qpv
    , communes : List Commune
    , epcis : List Epci
    , departements : List Departement
    , regions : List Region
    , territoireIndustries : List TerritoireIndustrie
    }


type alias Epci =
    { id : String
    , nom : String
    }


type alias Departement =
    { id : String
    , nom : String
    }


type alias Region =
    { id : String
    , nom : String
    }


type alias Qpv =
    { id : String
    , nom : String
    }


type alias TerritoireIndustrie =
    { id : String
    , nom : String
    }


defaultFiltresRequete : FiltresRequete
defaultFiltresRequete =
    { qpvs = []
    , communes = []
    , epcis = []
    , departements = []
    , regions = []
    , territoireIndustries = []
    }


decodeFiltresRequete : Decoder FiltresRequete
decodeFiltresRequete =
    Decode.succeed FiltresRequete
        |> andMap
            (Decode.field "qpvs" <|
                Decode.list <|
                    Decode.map2 Qpv
                        (Decode.field "id" Decode.string)
                        (Decode.field "nom" Decode.string)
            )
        |> andMap
            (Decode.field "communes" <|
                Decode.list <|
                    Data.Commune.decodeCommune
            )
        |> andMap
            (Decode.field "epcis" <|
                Decode.list <|
                    Decode.map2 Epci
                        (Decode.field "id" Decode.string)
                        (Decode.field "nom" Decode.string)
            )
        |> andMap
            (Decode.field "departements" <|
                Decode.list <|
                    Decode.map2 Departement
                        (Decode.field "id" Decode.string)
                        (Decode.field "nom" Decode.string)
            )
        |> andMap
            (Decode.field "regions" <|
                Decode.list <|
                    Decode.map2 Region
                        (Decode.field "id" Decode.string)
                        (Decode.field "nom" Decode.string)
            )
        |> andMap
            (Decode.field "territoireIndustries" <|
                Decode.list <|
                    Decode.map2 TerritoireIndustrie
                        (Decode.field "id" Decode.string)
                        (Decode.field "nom" Decode.string)
            )


dataToLabelAndToTitle :
    ( { data
        | id : String
        , nom : String
      }
      -> String
    , { data
        | id : String
        , nom : String
      }
      -> String
    )
dataToLabelAndToTitle =
    let
        toLabel data =
            if data.nom == "" then
                data.id

            else if String.length data.nom < 20 then
                data.id ++ " - " ++ data.nom

            else
                data.id ++ " - " ++ String.left 17 data.nom ++ "..."

        toTitle data =
            if data.nom /= "" then
                data.id ++ " - " ++ data.nom

            else
                data.id
    in
    ( toLabel, toTitle )


selectCharacterThreshold : Int
selectCharacterThreshold =
    0


selectDebounceDuration : Float
selectDebounceDuration =
    400


selectAdresseConfig : SingleSelectRemote.SelectConfig ApiAdresse
selectAdresseConfig =
    { headers = []
    , url = Api.rechercheAdresse Nothing Nothing Nothing
    , optionDecoder = decodeApiFeatures
    }


decodeMapInfo : Decoder Msg
decodeMapInfo =
    Decode.map UpdatedMapInfo <|
        Decode.field "detail" <|
            (Decode.succeed MapInfo
                |> andMap (Decode.field "center" decodePoint)
                |> andMap (Decode.field "south" Decode.float)
                |> andMap (Decode.field "west" Decode.float)
                |> andMap (Decode.field "north" Decode.float)
                |> andMap (Decode.field "east" Decode.float)
                |> andMap (Decode.field "zoom" Decode.float)
            )


decodePoint : Decoder ( Float, Float )
decodePoint =
    Decode.succeed Tuple.pair
        |> andMap (Decode.field "lng" Decode.float)
        |> andMap (Decode.field "lat" Decode.float)


resetModals : Model -> Model
resetModals model =
    { model
        | geoJsonExport = Nothing
        , excelExportRequested = False
        , rechercheSauvegardeeRequest = RD.NotAsked
    }
