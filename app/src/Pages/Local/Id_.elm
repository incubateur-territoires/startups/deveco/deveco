module Pages.Local.Id_ exposing (Model, Msg(..), Onglet(..), page)

import Accessibility exposing (Html, br, div, formWithListeners, h1, h3, h4, hr, p, span, sup, text)
import Api
import Api.Auth exposing (delete, get, post)
import Api.EntityId exposing (parseEntityId)
import Browser.Dom as Dom
import DSFR.Alert
import DSFR.Button
import DSFR.Grid
import DSFR.Icons
import DSFR.Icons.Buildings
import DSFR.Icons.Business
import DSFR.Icons.Communication
import DSFR.Icons.Design
import DSFR.Icons.Editor
import DSFR.Icons.Map
import DSFR.Icons.System
import DSFR.Icons.User
import DSFR.Input
import DSFR.Modal
import DSFR.Radio
import DSFR.SearchBar
import DSFR.Table
import DSFR.Tabs
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Adresse exposing (ApiAdresse, adresseToString, decodeApiFeatures)
import Data.Collegues exposing (Collegue)
import Data.Contact exposing (Contact, ContactInputType(..), EntiteLien(..), FonctionContact(..), decodeContact)
import Data.Demande exposing (Demande, decodeDemande)
import Data.Echange exposing (Echange, decodeEchange)
import Data.Equipe
import Data.Etablissement exposing (EtablissementApercu, decodeEtablissementApercu)
import Data.Etiquette exposing (Etiquette, Etiquettes, decodeEtiquetteList, decodeEtiquettes)
import Data.Local exposing (Contribution, Local, Mutation, ProprietairePersonneMorale, ProprietairePersonnePhysique, badgeVacance, bailTypeList, bailTypeToDisplay, bailTypeToString, decodeContribution, decodeLocal, vacanceMotifList, vacanceMotifToDisplay, vacanceMotifToString, vacanceToDisplay, vacanceToString, vacanceTypeList, vacanceTypeToDisplay, vacanceTypeToString)
import Data.Personne exposing (Personne, decodePersonne)
import Data.PortefeuilleInfos exposing (PortefeuilleInfos, decodePortefeuilleInfos)
import Data.Rappel exposing (Rappel, decodeRappel)
import Data.Ressource exposing (Ressource, RessourceAction(..), RessourceField(..), decodeRessource, encodeRessource, ressourceTypeToDisplay, ressourceTypeToString, ressourceTypes, updateRessource, viewRessources)
import Data.Role
import Data.Zonage exposing (Zonage, decodeZonage)
import Date exposing (Date)
import Effect
import Html
import Html.Attributes exposing (class, id)
import Html.Events as Events
import Html.Extra exposing (nothing, viewIf)
import Html.Lazy as Lazy
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode
import Lib.Date
import Lib.UI exposing (capitalizeName, formatFloatWithThousandSpacing, loggingDecoder, plural, withEmptyAs)
import Lib.Variables exposing (hintDonneesLocauxPubliques, hintMotsCles, hintZoneGeographique)
import List.Extra exposing (isPermutationOf)
import MultiSelectRemote
import Pages.Annuaire exposing (defaultFilters)
import Pages.Locaux exposing (emptyFilters, filtersAndPaginationToQuery)
import QS
import Regex
import RemoteData as RD exposing (WebData)
import Route
import Shared
import SingleSelectRemote
import Spa.Page exposing (Page)
import Task
import Time exposing (Posix, Zone)
import UI.Collegue exposing (Partage, getCollegues, viewPartageModal)
import UI.Contact exposing (ContactAction(..), ContactField, NewContactForm, encodeNewContactForm, newContactFormPourExistant, updateContact, updateContactForm, viewContacts, viewDeleteContactBody, viewEditContact, viewNewContactFormFields)
import UI.Entite
import UI.Layout
import UI.Local exposing (badgeInactif)
import UI.Pagination exposing (Pagination, defaultPagination)
import UI.Suivi
import Url.Builder as Builder
import User
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page ( Maybe String, String ) Shared.Msg (View Msg) Model Msg
page shared user =
    Spa.Page.onNewFlags (Tuple.first >> queryToOnglets >> UpdateOnglets) <|
        Spa.Page.element
            { view = view user shared
            , init = init shared
            , update = update user
            , subscriptions = subscriptions
            }


type alias Model =
    { localWithExtraInfo : WebData LocalWithExtraInfo
    , localId : String
    , suiviModel : UI.Suivi.Model
    , modifierNom : Maybe ModifierNom
    , modifierVacance : Maybe ModifierVacance
    , modifierContribution : Maybe ModifierContribution
    , geolocalisationContribution : Maybe GeolocalisationContribution
    , modifierGeolocalisationRequest : WebData ()
    , contactAction : ContactAction
    , saveContactRequest : WebData LocalWithExtraInfo
    , newEtablissementOccupant : Maybe NewEtablissementOccupant
    , deleteEtablissementOccupant :
        Maybe
            { etablissementOccupant : EtablissementApercu
            , request : WebData LocalWithExtraInfo
            }
    , saveRessourceRequest : WebData LocalWithExtraInfo
    , ressourceAction : RessourceAction
    , contactCreation : WebData LocalWithExtraInfo
    , partageModal : Maybe Partage
    , devecos : WebData (List Collegue)
    , today : Date
    , onglet : Onglet
    , currentUrl : String
    , editLocalEtiquettes : Maybe LocalEtiquettes
    , saveLocalEtiquettesRequest : WebData LocalWithExtraInfo
    , selectLocalisations : MultiSelectRemote.SmartSelect Msg String
    , selectMots : MultiSelectRemote.SmartSelect Msg String
    , zonages : WebData (List Zonage)
    , prochainId : Int
    }


type alias GeolocalisationContribution =
    { centreCarte : ( Float, Float )
    , geolocalisationActuelle : Maybe ( Float, Float )
    , geolocalisationContribution : Maybe ( Float, Float )
    , nouvelleGeolocalisation : Maybe ( Float, Float )
    , nouvelleGeolocalisationChamp : String
    , zoom : Maybe Int
    , selectAdresse : SingleSelectRemote.SmartSelect Msg ApiAdresse
    }


type Onglet
    = OngletLocal
    | OngletSuivi


ongletToString : Onglet -> String
ongletToString onglet =
    case onglet of
        OngletLocal ->
            "local"

        OngletSuivi ->
            "suivi"


ongletToDisplay : Onglet -> String
ongletToDisplay onglet =
    case onglet of
        OngletLocal ->
            "Local"

        OngletSuivi ->
            "Suivi de la relation"


stringToOnglet : String -> Maybe Onglet
stringToOnglet s =
    case s of
        "etablissement" ->
            Just OngletLocal

        "suivi" ->
            Just OngletSuivi

        _ ->
            Nothing


type alias LocalEtiquettes =
    { localisations : List Etiquette
    , mots : List Etiquette
    }


type alias ModifierContribution =
    { contribution : Contribution
    , request : WebData LocalWithExtraInfo
    }


type alias ModifierNom =
    { nom : String
    , request : WebData LocalWithExtraInfo
    }


type alias ModifierVacance =
    { occupe : Maybe Bool
    , request : WebData LocalWithExtraInfo
    }


type alias LocalWithExtraInfo =
    { local : Local
    , contribution : Contribution
    , contacts : List Contact
    , echanges : List Echange
    , rappels : List Rappel
    , demandes : List Demande
    , portefeuilleInfos : PortefeuilleInfos
    , ressources : List Ressource
    , modificationAuteurDate : Maybe ( String, Posix )
    , etiquettes : Etiquettes
    , geolocalisationContribution : Maybe ( Float, Float )
    , geolocalisationEquipe : Maybe String
    }


init : Shared.Shared -> ( Maybe String, String ) -> ( Model, Effect.Effect Shared.Msg Msg )
init shared ( query, id ) =
    let
        ( onglet, ongletSuivi ) =
            queryToOnglets query

        nextUrl =
            ongletsToQuery ( onglet, ongletSuivi )
    in
    ( { localWithExtraInfo = RD.Loading
      , localId = id
      , suiviModel = UI.Suivi.init UI.Suivi.OngletEchanges <| Just ongletSuivi
      , modifierNom = Nothing
      , modifierVacance = Nothing
      , modifierContribution = Nothing
      , contactAction = None
      , saveContactRequest = RD.NotAsked
      , newEtablissementOccupant = Nothing
      , deleteEtablissementOccupant = Nothing
      , saveRessourceRequest = RD.NotAsked
      , ressourceAction = NoRessource
      , contactCreation = RD.NotAsked
      , partageModal = Nothing
      , devecos = RD.NotAsked
      , today = Date.fromPosix shared.timezone shared.now
      , onglet = onglet
      , currentUrl = nextUrl
      , editLocalEtiquettes = Nothing
      , saveLocalEtiquettesRequest = RD.NotAsked
      , selectLocalisations =
            MultiSelectRemote.init selectLocalisationsId
                { selectionMsg = SelectedLocalisations
                , internalMsg = UpdatedSelectLocalisations
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectMots =
            MultiSelectRemote.init selectMotsId
                { selectionMsg = SelectedMots
                , internalMsg = UpdatedSelectMots
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , zonages = RD.Loading
      , geolocalisationContribution = Nothing
      , modifierGeolocalisationRequest = RD.NotAsked
      , prochainId = 0
      }
    , Effect.batch
        [ Effect.fromCmd <| fetchLocal id
        , Effect.fromCmd <| getCollegues SharedMsg (Just LogError) ReceivedCollegues
        ]
    )
        |> Shared.pageChangeEffects


fetchLocal : String -> Cmd Msg
fetchLocal id =
    get
        { url = Api.getLocal id
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedLocal
        }
        (Decode.field "local" decodeLocalWithExtraInfo)


decodeLocalWithExtraInfo : Decoder LocalWithExtraInfo
decodeLocalWithExtraInfo =
    Decode.succeed LocalWithExtraInfo
        |> andMap decodeLocal
        |> andMap decodeContribution
        |> andMap (Decode.field "contacts" <| Decode.list decodeContact)
        |> andMap (Decode.field "echanges" <| Decode.list decodeEchange)
        |> andMap (Decode.field "rappels" <| Decode.list decodeRappel)
        |> andMap (Decode.field "demandes" <| Decode.list decodeDemande)
        |> andMap (Decode.field "portefeuilleInfos" decodePortefeuilleInfos)
        |> andMap (Decode.field "ressources" <| Decode.list decodeRessource)
        |> andMap
            (Decode.field "modification" <|
                Decode.nullable <|
                    Decode.map2 Tuple.pair
                        (Decode.field "compte" <|
                            Decode.map2 (\prenom nom -> prenom ++ " " ++ nom)
                                (Decode.field "prenom" Decode.string)
                                (Decode.field "nom" Decode.string)
                        )
                        (Decode.field "date" Lib.Date.decodeDateWithTimeFromISOString)
            )
        |> andMap (Decode.field "etiquettes" <| decodeEtiquettes)
        |> andMap (optionalNullableField "geolocalisationContribution" <| Data.Adresse.decodeCoordinates)
        |> andMap (optionalNullableField "geolocalisationEquipe" <| Decode.string)


selectLocalisationsId : String
selectLocalisationsId =
    "champ-selection-zones"


selectMotsId : String
selectMotsId =
    "champ-selection-mots"


selectCharacterThreshold : Int
selectCharacterThreshold =
    0


selectDebounceDuration : Float
selectDebounceDuration =
    400


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | SuiviMsg UI.Suivi.Msg
    | ReceivedLocal (WebData LocalWithExtraInfo)
    | AfficherModifierVacance Bool
    | UpdateVacance (Maybe Bool)
    | ConfirmVacanceRequest
    | ReceivedVacanceResponse (WebData LocalWithExtraInfo)
    | AfficherModifierNom Bool
    | UpdateNom String
    | ConfirmNomRequest
    | ReceivedNomResponse (WebData LocalWithExtraInfo)
    | AfficherModifierContribution Bool
    | UpdateContribution ContributionField String
    | ConfirmContributionRequest
    | ReceivedContributionResponse (WebData LocalWithExtraInfo)
    | ToggleGeolocalisationContribution
    | UpdateGeolocalisationContributionInput String
    | UpdateGeolocalisationContributionByMap ( Float, Float )
    | RequestedGeolocalisationContributionSave
    | ReceivedGeolocalisationContributionSave (WebData LocalWithExtraInfo)
    | SelectedAdresse ( ApiAdresse, SingleSelectRemote.Msg ApiAdresse )
    | UpdatedSelectAdresse (SingleSelectRemote.Msg ApiAdresse)
    | SetContactAction ContactAction
    | ConfirmContactAction
    | CancelContactAction
    | UpdatedContact ContactField String
    | ReceivedSaveContact (WebData LocalWithExtraInfo)
    | AddEtablissementOccupant
    | SelectedEtablissementOccupant (Maybe EtablissementApercu)
    | ReceivedEtablissementSuggestions (WebData ( List EtablissementApercu, Pagination ))
    | CanceledAddEtablissementOccupant
    | UpdatedSiret String
    | ConfirmedSiret
    | ReceivedEtablissementResults (WebData RechercheParSiretResultat)
    | ConfirmedAddEtablissementOccupant
    | ReceivedAddEtablissementOccupant (WebData LocalWithExtraInfo)
    | ClickedDeleteEtablissementOccupant EtablissementApercu
    | CanceledDeleteEtablissementOccupant
    | ConfirmedDeleteEtablissementOccupant
    | ReceivedDeleteEtablissementOccupantResponse (WebData LocalWithExtraInfo)
    | SetRessourceAction RessourceAction
    | UpdatedRessource RessourceField String
    | ConfirmRessourceAction
    | CancelRessourceAction
    | ReceivedSaveRessource (WebData LocalWithExtraInfo)
    | UpdatedRechercheContactExistant String
    | RechercheContactExistant
    | ReceivedContactsExistants (WebData (List Personne))
    | SelectedContactExistant (Maybe Personne)
    | ToggleNouveauContactModeCreation Bool
    | ClickedPartage
    | CancelPartage
    | UpdatedPartageDeveco String
    | UpdatedPartageCommentaire String
    | ConfirmPartage
    | ReceivedPartage (WebData ())
    | ReceivedCollegues (WebData (List Collegue))
    | ClickedChangementOnglet Onglet
    | UpdateOnglets ( Onglet, UI.Suivi.Onglet )
    | EditLocalEtiquettes
    | CancelEditLocalEtiquettes
    | RequestedSaveLocalEtiquettes
    | ReceivedSaveLocalEtiquettes (WebData LocalWithExtraInfo)
    | SelectedLocalisations ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectLocalisations (MultiSelectRemote.Msg String)
    | UnselectLocalisation String
    | SelectedMots ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectMots (MultiSelectRemote.Msg String)
    | UnselectMot String
    | ReceivedZonages (WebData (List Zonage))
    | ToggleFavorite Bool
    | GotFavoriteResult Bool


createContact : String -> NewContactForm -> Cmd Msg
createContact localId newContactForm =
    let
        httpBody =
            encodeNewContactForm newContactForm
    in
    post
        { url = Api.createLocalContact localId
        , body = httpBody |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveContact
        }
        (Decode.field "local" decodeLocalWithExtraInfo)


editContact : String -> Contact -> Cmd Msg
editContact localId contact =
    post
        { url = Api.getLocalContact localId contact.personne.id
        , body =
            contact
                |> Data.Contact.encodeContact ContactUpdateInput
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveContact
        }
        (Decode.field "local" decodeLocalWithExtraInfo)


deleteContact : String -> Contact -> Cmd Msg
deleteContact localId { personne } =
    delete
        { url = Api.getLocalContact localId personne.id
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveContact
        }
        (Decode.field "local" decodeLocalWithExtraInfo)


type ContributionField
    = CLoyer
    | CBailType
    | CVacanceType
    | CVacanceMotif
    | CRemembre
    | CFonds
    | CVente
    | CCommentaires


type alias NewEtablissementOccupant =
    { siretRecherche : String
    , siretResultat : WebData RechercheParSiretResultat
    , siretChoix : Maybe EtablissementApercu
    , request : WebData LocalWithExtraInfo
    , suggestions : WebData ( List EtablissementApercu, Pagination )
    }


type RechercheParSiretResultat
    = Resultat EtablissementApercu
    | Erreur String


emptyNewEtablissementOccupant : NewEtablissementOccupant
emptyNewEtablissementOccupant =
    { siretRecherche = ""
    , siretResultat = RD.NotAsked
    , siretChoix = Nothing
    , request = RD.NotAsked
    , suggestions = RD.Loading
    }


decodePaginatedResponse : Decoder ( List EtablissementApercu, Pagination )
decodePaginatedResponse =
    Decode.succeed (\p pages results data -> ( data, Pagination p pages results ))
        |> andMap (Decode.field "page" Decode.int)
        |> andMap (Decode.field "pages" Decode.int)
        |> andMap (Decode.field "total" Decode.int)
        |> andMap
            (Decode.field "elements" <|
                Decode.list <|
                    Decode.field "etablissement" decodeEtablissementApercu
            )


update : User.User -> Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update user msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        SuiviMsg suiviMsg ->
            let
                ( suiviModel, suiviEffect, entity ) =
                    UI.Suivi.update (UI.Suivi.suiviConfigLocal (Decode.field "local" decodeLocalWithExtraInfo) model.localId) suiviMsg model.suiviModel

                ongletSuivi =
                    UI.Suivi.getOngletActif suiviModel

                ( newModel, effect ) =
                    { model
                        | suiviModel = suiviModel
                        , localWithExtraInfo =
                            entity
                                |> UI.Suivi.suiviResultToMaybe
                                |> Maybe.map RD.Success
                                |> Maybe.withDefault model.localWithExtraInfo
                    }
                        |> updateUrl ( model.onglet, ongletSuivi )
            in
            ( newModel
            , Effect.batch [ effect, Effect.map SuiviMsg <| suiviEffect ]
            )

        ReceivedLocal response ->
            ( { model | localWithExtraInfo = response }, getZonages model.localId )

        AfficherModifierVacance show ->
            case model.localWithExtraInfo of
                RD.Success { local } ->
                    { model
                        | modifierVacance =
                            if show then
                                Just
                                    { occupe = local.occupe
                                    , request = RD.NotAsked
                                    }

                            else
                                Nothing
                    }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        UpdateVacance occupe ->
            case model.localWithExtraInfo of
                RD.Success _ ->
                    { model
                        | modifierVacance =
                            model.modifierVacance
                                |> Maybe.map
                                    (\v ->
                                        { v
                                            | occupe = occupe
                                        }
                                    )
                    }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        ConfirmVacanceRequest ->
            case model.localWithExtraInfo of
                RD.Success { local } ->
                    case model.modifierVacance of
                        Nothing ->
                            model
                                |> Effect.withNone

                        Just modifierVacanceRequest ->
                            { model
                                | modifierVacance =
                                    Just
                                        { modifierVacanceRequest
                                            | request = RD.Loading
                                        }
                            }
                                |> Effect.withCmd (confirmVacanceRequest local.id modifierVacanceRequest.occupe)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedVacanceResponse (RD.Success localWithExtraInfo) ->
            case model.localWithExtraInfo of
                RD.Success _ ->
                    { model
                        | modifierVacance = Nothing
                        , localWithExtraInfo = RD.Success localWithExtraInfo
                    }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        ReceivedVacanceResponse response ->
            case model.localWithExtraInfo of
                RD.Success _ ->
                    { model
                        | modifierVacance =
                            model.modifierVacance
                                |> Maybe.map
                                    (\v ->
                                        { v
                                            | request = response
                                        }
                                    )
                    }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        AfficherModifierNom show ->
            case model.localWithExtraInfo of
                RD.Success { local } ->
                    { model
                        | modifierNom =
                            if show then
                                Just
                                    { nom = local.nom
                                    , request = RD.NotAsked
                                    }

                            else
                                Nothing
                    }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        UpdateNom nom ->
            case model.localWithExtraInfo of
                RD.Success _ ->
                    { model
                        | modifierNom =
                            model.modifierNom
                                |> Maybe.map
                                    (\n ->
                                        { n
                                            | nom = nom
                                        }
                                    )
                    }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        ConfirmNomRequest ->
            case model.localWithExtraInfo of
                RD.Success { local } ->
                    case model.modifierNom of
                        Nothing ->
                            model
                                |> Effect.withNone

                        Just modifierNomRequest ->
                            { model
                                | modifierNom =
                                    Just
                                        { modifierNomRequest
                                            | request = RD.Loading
                                        }
                            }
                                |> Effect.withCmd (confirmNomRequest local.id modifierNomRequest.nom)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedNomResponse (RD.Success localWithExtraInfo) ->
            case model.localWithExtraInfo of
                RD.Success _ ->
                    { model
                        | modifierNom = Nothing
                        , localWithExtraInfo = RD.Success localWithExtraInfo
                    }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        ReceivedNomResponse response ->
            case model.localWithExtraInfo of
                RD.Success _ ->
                    { model
                        | modifierNom =
                            model.modifierNom
                                |> Maybe.map
                                    (\v ->
                                        { v
                                            | request = response
                                        }
                                    )
                    }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        AfficherModifierContribution show ->
            case model.localWithExtraInfo of
                RD.Success { contribution } ->
                    { model
                        | modifierContribution =
                            if show then
                                Just
                                    { contribution = contribution
                                    , request = RD.NotAsked
                                    }

                            else
                                Nothing
                    }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        UpdateContribution field value ->
            case model.localWithExtraInfo of
                RD.Success _ ->
                    { model
                        | modifierContribution =
                            model.modifierContribution
                                |> Maybe.map
                                    (\m ->
                                        { m
                                            | contribution = updateContribution field value m.contribution
                                        }
                                    )
                    }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        ConfirmContributionRequest ->
            case model.localWithExtraInfo of
                RD.Success { local } ->
                    case model.modifierContribution of
                        Nothing ->
                            model
                                |> Effect.withNone

                        Just modifierContribution ->
                            { model
                                | modifierContribution =
                                    Just
                                        { modifierContribution
                                            | request = RD.Loading
                                        }
                            }
                                |> Effect.withCmd (confirmContributionRequest local.id modifierContribution.contribution)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedContributionResponse (RD.Success localWithExtraInfo) ->
            case model.localWithExtraInfo of
                RD.Success _ ->
                    { model
                        | modifierContribution = Nothing
                        , localWithExtraInfo = RD.Success localWithExtraInfo
                    }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        ReceivedContributionResponse response ->
            case model.localWithExtraInfo of
                RD.Success _ ->
                    { model
                        | modifierContribution =
                            model.modifierContribution
                                |> Maybe.map
                                    (\v ->
                                        { v
                                            | request = response
                                        }
                                    )
                    }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        SelectedAdresse ( apiAdresse, sMsg ) ->
            case model.geolocalisationContribution of
                Nothing ->
                    ( model, Effect.none )

                Just geolocalisationContribution ->
                    let
                        ( updatedSelect, selectCmd ) =
                            SingleSelectRemote.update sMsg selectAdresseConfig geolocalisationContribution.selectAdresse

                        ( emptySelect, emptySelectCmd ) =
                            SingleSelectRemote.setText "" selectAdresseConfig updatedSelect

                        coordonneesAdresse =
                            apiAdresse
                                |> .coordinates

                        newGeolocalisationContribution =
                            { geolocalisationContribution
                                | selectAdresse = emptySelect
                                , nouvelleGeolocalisation =
                                    coordonneesAdresse
                                        |> Maybe.map Just
                                        |> Maybe.withDefault geolocalisationContribution.nouvelleGeolocalisation
                                , nouvelleGeolocalisationChamp =
                                    coordonneesAdresse
                                        |> Maybe.map pointToString
                                        |> Maybe.withDefault geolocalisationContribution.nouvelleGeolocalisationChamp
                                , zoom = Just 16
                            }
                    in
                    ( { model | geolocalisationContribution = Just newGeolocalisationContribution }
                    , Effect.batch [ Effect.fromCmd selectCmd, Effect.fromCmd emptySelectCmd ]
                    )

        UpdatedSelectAdresse sMsg ->
            case model.geolocalisationContribution of
                Nothing ->
                    ( model, Effect.none )

                Just geolocalisationContribution ->
                    let
                        ( updatedSelect, selectCmd ) =
                            SingleSelectRemote.update sMsg selectAdresseConfig geolocalisationContribution.selectAdresse

                        newGeolocalisationContribution =
                            { geolocalisationContribution | selectAdresse = updatedSelect }
                    in
                    ( { model | geolocalisationContribution = Just newGeolocalisationContribution }
                    , Effect.fromCmd selectCmd
                    )

        ToggleGeolocalisationContribution ->
            { model
                | geolocalisationContribution =
                    case model.geolocalisationContribution of
                        Just _ ->
                            Nothing

                        Nothing ->
                            let
                                selectAdresse =
                                    SingleSelectRemote.init "champ-selection-adresse-maplibre"
                                        { selectionMsg = SelectedAdresse
                                        , internalMsg = UpdatedSelectAdresse
                                        , characterSearchThreshold = 3
                                        , debounceDuration = selectDebounceDuration
                                        }
                            in
                            model.localWithExtraInfo
                                |> RD.toMaybe
                                -- |> Maybe.andThen Result.toMaybe
                                |> Maybe.map
                                    (\localWithExtraInfo ->
                                        { centreCarte =
                                            localWithExtraInfo.geolocalisationContribution
                                                |> Maybe.map Just
                                                |> Maybe.withDefault localWithExtraInfo.local.geolocalisation
                                                |> Maybe.withDefault centreDeLaFrance
                                        , geolocalisationActuelle = localWithExtraInfo.local.geolocalisation
                                        , geolocalisationContribution = localWithExtraInfo.geolocalisationContribution
                                        , nouvelleGeolocalisation = localWithExtraInfo.geolocalisationContribution
                                        , nouvelleGeolocalisationChamp =
                                            localWithExtraInfo.geolocalisationContribution
                                                |> Maybe.map pointToString
                                                |> Maybe.withDefault ""
                                        , zoom = Nothing
                                        , selectAdresse = selectAdresse
                                        }
                                    )
                , modifierGeolocalisationRequest = RD.NotAsked
            }
                |> Effect.withNone

        UpdateGeolocalisationContributionInput geolocalisationString ->
            { model
                | geolocalisationContribution =
                    model.geolocalisationContribution
                        |> Maybe.map
                            (\geolocalisationContribution ->
                                { geolocalisationContribution
                                    | nouvelleGeolocalisation = geolocalisationString |> stringtoPoint
                                    , nouvelleGeolocalisationChamp = geolocalisationString
                                    , zoom = Nothing
                                }
                            )
            }
                |> Effect.withNone

        UpdateGeolocalisationContributionByMap ( lon, lat ) ->
            { model
                | geolocalisationContribution =
                    model.geolocalisationContribution
                        |> Maybe.map
                            (\geolocalisationContribution ->
                                { geolocalisationContribution
                                    | centreCarte = ( lon, lat )
                                    , nouvelleGeolocalisation = Just ( lon, lat )
                                    , nouvelleGeolocalisationChamp =
                                        ( lon, lat )
                                            |> pointToString
                                    , zoom = Nothing
                                }
                            )
            }
                |> Effect.withNone

        SetContactAction action ->
            let
                cmd =
                    case action of
                        EditContact _ _ ->
                            Task.attempt (\_ -> NoOp) (Dom.focus <| "modifier-contact-fonction")

                        NewContact _ ->
                            Task.attempt (\_ -> NoOp) (Dom.focus <| "nouveau-contact-fonction")

                        _ ->
                            Cmd.none
            in
            ( { model | contactAction = action }, Effect.fromCmd cmd )

        CancelContactAction ->
            ( { model | saveContactRequest = RD.NotAsked, contactAction = None }, Effect.none )

        UpdatedContact field value ->
            case model.saveContactRequest of
                RD.Loading ->
                    ( model, Effect.none )

                _ ->
                    Effect.withNone <|
                        (\m -> { m | saveContactRequest = RD.NotAsked }) <|
                            case model.contactAction of
                                None ->
                                    model

                                ViewContactFonctions _ ->
                                    model

                                DeleteContact _ _ ->
                                    model

                                DeleteContactForm _ ->
                                    model

                                NewContact contact ->
                                    { model | contactAction = NewContact <| updateContactForm field value contact }

                                EditContact personne contact ->
                                    { model | contactAction = EditContact personne <| updateContact field value contact }

                                EditContactForm _ _ ->
                                    model

        ConfirmContactAction ->
            case model.saveContactRequest of
                RD.Loading ->
                    ( model, Effect.none )

                _ ->
                    case model.contactAction of
                        None ->
                            ( model, Effect.none )

                        ViewContactFonctions _ ->
                            ( model, Effect.none )

                        NewContact contact ->
                            ( { model | saveContactRequest = RD.Loading }, Effect.fromCmd <| createContact model.localId contact )

                        DeleteContact contact _ ->
                            ( { model | saveContactRequest = RD.Loading }, Effect.fromCmd <| deleteContact model.localId contact )

                        DeleteContactForm _ ->
                            ( model, Effect.none )

                        EditContact _ contact ->
                            ( { model | saveContactRequest = RD.Loading }, Effect.fromCmd <| editContact model.localId contact )

                        EditContactForm _ _ ->
                            ( model, Effect.none )

        ReceivedSaveContact response ->
            let
                ( contactAction, localWithExtraInfo ) =
                    case response of
                        RD.Success _ ->
                            ( None, response )

                        _ ->
                            ( model.contactAction, model.localWithExtraInfo )
            in
            ( { model
                | localWithExtraInfo = localWithExtraInfo
                , contactAction = contactAction
                , saveContactRequest = response
              }
            , Effect.none
            )

        AddEtablissementOccupant ->
            { model | newEtablissementOccupant = Just emptyNewEtablissementOccupant }
                |> Effect.withCmd (getEtablissementOccupantSuggestions model.localWithExtraInfo)

        ReceivedEtablissementSuggestions suggestions ->
            { model | newEtablissementOccupant = model.newEtablissementOccupant |> Maybe.map (\eo -> { eo | suggestions = suggestions }) }
                |> Effect.withNone

        SelectedEtablissementOccupant etablissementOccupant ->
            { model | newEtablissementOccupant = model.newEtablissementOccupant |> Maybe.map (\eo -> { eo | siretChoix = etablissementOccupant }) }
                |> Effect.withNone

        CanceledAddEtablissementOccupant ->
            { model | newEtablissementOccupant = Nothing }
                |> Effect.withNone

        UpdatedSiret siret ->
            { model
                | newEtablissementOccupant =
                    model.newEtablissementOccupant
                        |> Maybe.map
                            (\e ->
                                { e
                                    | siretRecherche = siret
                                }
                            )
            }
                |> Effect.withNone

        ConfirmedSiret ->
            let
                ( newResultats, cmd ) =
                    ( RD.Loading
                    , searchEtablissement model.localWithExtraInfo model.newEtablissementOccupant
                    )

                newModel =
                    { model
                        | newEtablissementOccupant =
                            model.newEtablissementOccupant
                                |> Maybe.map (\p -> { p | siretResultat = newResultats })
                    }
            in
            newModel
                |> Effect.withCmd cmd

        ReceivedEtablissementResults results ->
            { model
                | newEtablissementOccupant =
                    model.newEtablissementOccupant
                        |> Maybe.map (\p -> { p | siretResultat = results })
            }
                |> Effect.withNone

        ConfirmedAddEtablissementOccupant ->
            case ( model.localWithExtraInfo, model.newEtablissementOccupant ) of
                ( RD.Success localWithExtraInfo, Just prop ) ->
                    case prop.request of
                        RD.Loading ->
                            model
                                |> Effect.withNone

                        _ ->
                            { model
                                | newEtablissementOccupant =
                                    model.newEtablissementOccupant
                                        |> Maybe.map
                                            (\m ->
                                                { m
                                                    | request = RD.Loading
                                                }
                                            )
                            }
                                |> Effect.withCmd (saveEtablissementOccupant localWithExtraInfo prop)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedAddEtablissementOccupant (RD.Success localWithExtraInfo) ->
            { model
                | newEtablissementOccupant = Nothing
                , localWithExtraInfo = RD.Success localWithExtraInfo
            }
                |> Effect.withNone

        ReceivedAddEtablissementOccupant result ->
            { model
                | newEtablissementOccupant =
                    model.newEtablissementOccupant
                        |> Maybe.map
                            (\m ->
                                { m
                                    | request = result
                                }
                            )
            }
                |> Effect.withNone

        ClickedDeleteEtablissementOccupant etablissementOccupant ->
            { model
                | deleteEtablissementOccupant =
                    Just
                        { etablissementOccupant = etablissementOccupant
                        , request = RD.NotAsked
                        }
            }
                |> Effect.withNone

        CanceledDeleteEtablissementOccupant ->
            { model
                | deleteEtablissementOccupant = Nothing
            }
                |> Effect.withNone

        ConfirmedDeleteEtablissementOccupant ->
            case ( model.localWithExtraInfo, model.deleteEtablissementOccupant ) of
                ( RD.Success localWithExtraInfo, Just del ) ->
                    { model | deleteEtablissementOccupant = Just { del | request = RD.Loading } }
                        |> Effect.withCmd (deleteProprietaire del.etablissementOccupant localWithExtraInfo)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedDeleteEtablissementOccupantResponse response ->
            case response of
                RD.Success _ ->
                    { model | deleteEtablissementOccupant = Nothing, localWithExtraInfo = response }
                        |> Effect.withNone

                _ ->
                    { model
                        | deleteEtablissementOccupant =
                            model.deleteEtablissementOccupant
                                |> Maybe.map (\d -> { d | request = response })
                    }
                        |> Effect.withNone

        SetRessourceAction ressourceAction ->
            { model | ressourceAction = ressourceAction }
                |> Effect.withNone

        UpdatedRessource ressourceField ressourceValue ->
            case model.ressourceAction of
                NewRessource ressource ->
                    { model | ressourceAction = NewRessource <| updateRessource ressourceField ressourceValue <| ressource }
                        |> Effect.withNone

                EditRessource ressource ->
                    { model | ressourceAction = EditRessource <| updateRessource ressourceField ressourceValue <| ressource }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        CancelRessourceAction ->
            { model | ressourceAction = NoRessource }
                |> Effect.withNone

        ConfirmRessourceAction ->
            case model.saveRessourceRequest of
                RD.Loading ->
                    ( model, Effect.none )

                _ ->
                    case model.ressourceAction of
                        NoRessource ->
                            ( model, Effect.none )

                        NewRessource ressource ->
                            ( { model | saveRessourceRequest = RD.Loading }, Effect.fromCmd <| createRessource model.localId ressource )

                        DeleteRessource ressource ->
                            ( { model | saveRessourceRequest = RD.Loading }, Effect.fromCmd <| deleteRessource model.localId ressource )

                        EditRessource ressource ->
                            ( { model | saveRessourceRequest = RD.Loading }, Effect.fromCmd <| editRessource model.localId ressource )

        ReceivedSaveRessource response ->
            case model.saveRessourceRequest of
                RD.Loading ->
                    case model.ressourceAction of
                        NoRessource ->
                            model
                                |> Effect.withNone

                        _ ->
                            case response of
                                RD.Success _ ->
                                    { model | localWithExtraInfo = response, saveRessourceRequest = response, ressourceAction = NoRessource }
                                        |> Effect.withNone

                                _ ->
                                    { model | saveRessourceRequest = response }
                                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        UpdatedRechercheContactExistant recherche ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | recherche = recherche
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm _ _ ->
                    ( model, Effect.none )

        RechercheContactExistant ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | resultats = RD.Loading
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.fromCmd <|
                        getPersonnes <|
                            Pages.Annuaire.filtersAndPaginationToQuery
                                ( { defaultFilters
                                    | recherche = contactForm.personneExistante.recherche
                                    , localId = Just model.localId
                                    , elementsParPage = Just 5
                                  }
                                , defaultPagination
                                )
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm _ _ ->
                    ( model, Effect.none )

        ReceivedContactsExistants response ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | resultats = response
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm _ _ ->
                    ( model, Effect.none )

        SelectedContactExistant maybeContact ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | selectionne = maybeContact
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm _ _ ->
                    ( model, Effect.none )

        ToggleNouveauContactModeCreation modeCreation ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | modeCreation = modeCreation
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm _ _ ->
                    ( model, Effect.none )

        ClickedPartage ->
            { model
                | partageModal =
                    Just
                        { collegueId =
                            model.devecos
                                |> RD.map (List.filter (\collab -> collab.id /= User.id user))
                                |> RD.withDefault []
                                |> List.head
                                |> Maybe.map .id
                        , commentaire = ""
                        , request =
                            RD.NotAsked
                        }
            }
                |> Effect.withNone

        CancelPartage ->
            { model
                | partageModal =
                    Nothing
            }
                |> Effect.withNone

        UpdatedPartageDeveco collegueId ->
            { model
                | partageModal =
                    model.partageModal
                        |> Maybe.map
                            (\p ->
                                { p
                                    | collegueId = collegueId |> parseEntityId
                                    , request = RD.NotAsked
                                }
                            )
            }
                |> Effect.withNone

        UpdatedPartageCommentaire commentaire ->
            { model
                | partageModal =
                    model.partageModal
                        |> Maybe.map (\p -> { p | commentaire = commentaire })
            }
                |> Effect.withNone

        ConfirmPartage ->
            ( { model
                | partageModal =
                    model.partageModal
                        |> Maybe.map (\p -> { p | request = RD.Loading })
              }
            , model.partageModal
                |> Maybe.map (savePartage model.localId)
                |> Maybe.map Effect.fromCmd
                |> Maybe.withDefault Effect.none
            )

        ReceivedPartage response ->
            { model
                | partageModal =
                    model.partageModal
                        |> Maybe.map (\p -> { p | request = response })
            }
                |> Effect.withNone

        ReceivedCollegues response ->
            { model | devecos = response }
                |> Effect.withNone

        ClickedChangementOnglet onglet ->
            let
                suiviOnglet =
                    UI.Suivi.getOngletActif model.suiviModel
            in
            updateUrl ( onglet, suiviOnglet ) model

        UpdateOnglets ( onglet, ongletSuivi ) ->
            let
                ( newModel, effect ) =
                    updateUrl ( onglet, ongletSuivi ) model
            in
            ( newModel
            , effect
            )

        EditLocalEtiquettes ->
            case model.localWithExtraInfo of
                RD.Success { etiquettes } ->
                    { model
                        | editLocalEtiquettes =
                            Just
                                { localisations = etiquettes.localisations
                                , mots = etiquettes.motsCles
                                }
                        , saveLocalEtiquettesRequest = RD.NotAsked
                    }
                        |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        CancelEditLocalEtiquettes ->
            ( { model | editLocalEtiquettes = Nothing }, Effect.none )

        RequestedSaveLocalEtiquettes ->
            case model.localWithExtraInfo of
                RD.Success _ ->
                    case model.editLocalEtiquettes of
                        Just localInfo ->
                            ( { model
                                | saveLocalEtiquettesRequest = RD.Loading
                              }
                            , updateLocalEtiquettes model.localId localInfo
                            )

                        _ ->
                            ( model, Effect.none )

                _ ->
                    ( model, Effect.none )

        ReceivedSaveLocalEtiquettes response ->
            case response of
                RD.Success _ ->
                    ( { model | localWithExtraInfo = response, saveLocalEtiquettesRequest = response, editLocalEtiquettes = Nothing }, Effect.none )

                _ ->
                    ( { model | saveLocalEtiquettesRequest = response }, Effect.none )

        RequestedGeolocalisationContributionSave ->
            { model | modifierGeolocalisationRequest = RD.Loading }
                |> Effect.withCmd (requestGeolocalisationContributionSave model.geolocalisationContribution model.localWithExtraInfo)

        ReceivedGeolocalisationContributionSave resp ->
            let
                ( geolocalisationContribution, modifierGeolocalisationRequest ) =
                    case resp of
                        RD.Success _ ->
                            ( Nothing, RD.NotAsked )

                        _ ->
                            ( model.geolocalisationContribution, resp |> RD.map (\_ -> ()) )
            in
            { model
                | modifierGeolocalisationRequest = modifierGeolocalisationRequest
                , geolocalisationContribution = geolocalisationContribution
                , localWithExtraInfo =
                    case resp of
                        RD.Success _ ->
                            resp

                        _ ->
                            model.localWithExtraInfo
            }
                |> Effect.withNone

        SelectedLocalisations ( localisations, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations

                localisationsUniques =
                    case localisations of
                        [] ->
                            []

                        act :: rest ->
                            if List.member act rest then
                                rest

                            else
                                localisations
            in
            ( { model
                | selectLocalisations = updatedSelect
                , editLocalEtiquettes =
                    model.editLocalEtiquettes
                        |> Maybe.map (\ei -> { ei | localisations = localisationsUniques })
                , saveLocalEtiquettesRequest = RD.NotAsked
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectLocalisations sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations
            in
            ( { model
                | selectLocalisations = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectLocalisation localisation ->
            if model.saveLocalEtiquettesRequest == RD.Loading then
                ( model, Effect.none )

            else
                ( { model
                    | editLocalEtiquettes =
                        model.editLocalEtiquettes
                            |> Maybe.map
                                (\ei ->
                                    { ei
                                        | localisations =
                                            ei.localisations
                                                |> List.filter ((/=) localisation)
                                    }
                                )
                  }
                , Effect.none
                )

        SelectedMots ( mots, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectMotsConfig model.selectMots

                motsUniques =
                    case mots of
                        [] ->
                            []

                        act :: rest ->
                            if List.member act rest then
                                rest

                            else
                                mots
            in
            ( { model
                | selectMots = updatedSelect
                , editLocalEtiquettes =
                    model.editLocalEtiquettes
                        |> Maybe.map (\ei -> { ei | mots = motsUniques })
                , saveLocalEtiquettesRequest = RD.NotAsked
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectMots sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectMotsConfig model.selectMots
            in
            ( { model
                | selectMots = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectMot mot ->
            if model.saveLocalEtiquettesRequest == RD.Loading then
                ( model, Effect.none )

            else
                ( { model
                    | editLocalEtiquettes =
                        model.editLocalEtiquettes
                            |> Maybe.map
                                (\ei ->
                                    { ei
                                        | mots =
                                            ei.mots
                                                |> List.filter ((/=) mot)
                                    }
                                )
                  }
                , Effect.none
                )

        ReceivedZonages resp ->
            { model | zonages = resp }
                |> Effect.withNone

        ToggleFavorite add ->
            { model
                | localWithExtraInfo =
                    model.localWithExtraInfo
                        |> RD.map
                            (\localWithExtraInfo ->
                                { localWithExtraInfo
                                    | portefeuilleInfos =
                                        localWithExtraInfo.portefeuilleInfos
                                            |> (\infos ->
                                                    { infos
                                                        | favori = add
                                                    }
                                               )
                                }
                            )
            }
                |> Effect.withCmd
                    (model.localWithExtraInfo
                        |> RD.map (\lwei -> requestToggleFavorite lwei.local.id add)
                        |> RD.withDefault Cmd.none
                    )

        GotFavoriteResult add ->
            { model
                | localWithExtraInfo =
                    model.localWithExtraInfo
                        |> RD.map
                            (\localWithExtraInfos ->
                                { localWithExtraInfos
                                    | portefeuilleInfos =
                                        localWithExtraInfos.portefeuilleInfos
                                            |> (\infos ->
                                                    { infos
                                                        | favori = add
                                                    }
                                               )
                                }
                            )
            }
                |> Effect.withNone


getEtablissementOccupantSuggestions : WebData LocalWithExtraInfo -> Cmd Msg
getEtablissementOccupantSuggestions localWithExtraInfo =
    case localWithExtraInfo of
        RD.Success { local } ->
            get
                { url =
                    Api.localEtablissementsSuggestions local.id
                }
                { toShared = SharedMsg
                , logger = Just LogError
                , handler = ReceivedEtablissementSuggestions
                }
                decodePaginatedResponse

        _ ->
            Cmd.none


deleteProprietaire : EtablissementApercu -> LocalWithExtraInfo -> Cmd Msg
deleteProprietaire etablissementOccupant localWithExtraInfo =
    delete
        { url = Api.deleteLocalEtablissementOccupant localWithExtraInfo.local.id etablissementOccupant.siret
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedDeleteEtablissementOccupantResponse
        }
        (Decode.field "local" decodeLocalWithExtraInfo)


adresseFilter : SingleSelectRemote.SmartSelect Msg ApiAdresse -> Html Msg
adresseFilter selectAdresse =
    div [ class "flex flex-col gap-2 relative z-20" ]
        [ div [ class "absolute top-[1em] left-[1em] w-[50%]" ]
            [ selectAdresse
                |> SingleSelectRemote.viewCustom
                    { isDisabled = False
                    , selected = Nothing
                    , optionLabelFn = .label
                    , optionDescriptionFn = \_ -> ""
                    , optionsContainerMaxHeight = 300
                    , selectTitle = nothing
                    , searchPrompt = "Rechercher une ville, une adresse"
                    , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                    , noResultsForMsg =
                        \searchText ->
                            "Aucune autre adresse n'a été trouvée"
                                ++ (if searchText == "" then
                                        ""

                                    else
                                        " pour " ++ searchText
                                   )
                    , noOptionsMsg = "Aucune adresse n'a été trouvée"
                    , error = Nothing
                    }
            ]
        ]


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ model |> .selectLocalisations |> MultiSelectRemote.subscriptions
        , model |> .selectMots |> MultiSelectRemote.subscriptions
        , model |> .geolocalisationContribution |> Maybe.map (.selectAdresse >> SingleSelectRemote.subscriptions) |> Maybe.withDefault Sub.none
        ]


view : Shared.User -> Shared.Shared -> Model -> View Msg
view user { now, timezone } model =
    let
        title =
            case model.localWithExtraInfo of
                RD.Success _ ->
                    "Local"

                _ ->
                    "Local"
    in
    { title = UI.Layout.pageTitle <| title
    , body = body timezone now user model
    , route = Route.Local ( Nothing, model.localId )
    }


body : Zone -> Posix -> Shared.User -> Model -> List (Html Msg)
body timezone now user model =
    [ div [ class "flex flex-col p-2" ]
        [ div [ class "flex flex-row justify-between" ]
            [ DSFR.Button.new
                { label = "Retour"
                , onClick = Just <| SharedMsg <| Shared.goBack
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.arrowLeftLine
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withAttrs [ class "text-underline" ]
                |> DSFR.Button.view
            , case model.localWithExtraInfo of
                RD.Success { modificationAuteurDate } ->
                    case modificationAuteurDate of
                        Nothing ->
                            nothing

                        Just ( auteurModification, dateModification ) ->
                            let
                                auteur =
                                    if auteurModification == "" then
                                        ""

                                    else
                                        " par " ++ auteurModification
                            in
                            div [ Typo.textSm, class "flex flex-col !mb-0 p-2 text-right" ]
                                [ div [] [ text <| "Dernière modification de la fiche le " ++ Lib.Date.formatShort timezone dateModification ++ auteur ]
                                ]

                _ ->
                    nothing
            ]
        , case model.localWithExtraInfo of
            RD.Loading ->
                div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ DSFR.Icons.System.refreshFill |> DSFR.Icons.iconLG
                    , "Chargement en cours..."
                        |> text
                    ]

            RD.Success localWithExtraInfo ->
                viewLocalBlocks timezone now False user model localWithExtraInfo

            RD.Failure _ ->
                div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ "Une erreur s'est produite, veuillez recharger la page."
                        |> text
                    ]

            _ ->
                div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ "Une erreur s'est produite, veuillez recharger la page."
                        |> text
                    ]
        ]
    ]


viewLocalBlocks : Zone -> Posix -> Bool -> Shared.User -> Model -> LocalWithExtraInfo -> Html Msg
viewLocalBlocks timezone now forPrint user model localWithExtraInfo =
    div [ class "flex flex-col gap-4 p-2" ]
        [ Lazy.lazy2 viewNomModal localWithExtraInfo.local.nom model.modifierNom
        , Lazy.lazy2 viewVacanceModal localWithExtraInfo.local.occupe model.modifierVacance
        , Lazy.lazy2 viewContributionModal localWithExtraInfo.contribution model.modifierContribution
        , Lazy.lazy5 viewModifierGeolocalisationModal user model.localId model.modifierGeolocalisationRequest model.geolocalisationContribution model.localWithExtraInfo
        , Lazy.lazy2 viewAddEtablissementOccupantModal localWithExtraInfo model.newEtablissementOccupant
        , Lazy.lazy2 viewDeleteEtablissementOccupantModal localWithExtraInfo model.deleteEtablissementOccupant
        , Lazy.lazy3 viewContactsModal model.localId model.saveContactRequest model.contactAction
        , Lazy.lazy2 viewRessourcesModal model.saveRessourceRequest model.ressourceAction
        , Lazy.lazy3 viewPartageModal
            { noOp = NoOp
            , confirm = ConfirmPartage
            , cancel = CancelPartage
            , updateCollegue = UpdatedPartageDeveco
            , updateCommentaire = UpdatedPartageCommentaire
            }
            (model.devecos |> RD.withDefault [] |> List.filter .actif)
            model.partageModal
        , Accessibility.map SuiviMsg <| Lazy.lazy8 UI.Suivi.viewSuivisModal timezone now user (Just True) model.devecos False localWithExtraInfo.demandes model.suiviModel
        , Lazy.lazy2 viewLocalHeader model.modifierGeolocalisationRequest localWithExtraInfo
        , DSFR.Tabs.new
            { changeTabMsg = stringToOnglet >> Maybe.withDefault OngletLocal >> ClickedChangementOnglet
            , tabs =
                [ { id = OngletLocal |> ongletToString
                  , title = OngletLocal |> ongletToDisplay
                  , icon = DSFR.Icons.Buildings.storeLine
                  , content = viewTabLocal model localWithExtraInfo
                  }
                , { id = OngletSuivi |> ongletToString
                  , title = OngletSuivi |> ongletToDisplay
                  , icon = DSFR.Icons.Communication.discussLine
                  , content = Accessibility.map SuiviMsg <| UI.Suivi.viewOngletSuivis timezone now forPrint False user Nothing localWithExtraInfo.echanges localWithExtraInfo.demandes localWithExtraInfo.rappels Nothing model.suiviModel
                  }
                ]
            }
            |> DSFR.Tabs.view (ongletToString model.onglet)
        ]


viewTabLocal : Model -> LocalWithExtraInfo -> Html Msg
viewTabLocal model localWithExtraInfo =
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col gap-4 p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy6 viewLocal model.editLocalEtiquettes model.zonages model.selectLocalisations model.selectMots model.saveLocalEtiquettesRequest localWithExtraInfo
            ]
        , div [ class "flex flex-col gap-4 p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy3 viewRessources SetRessourceAction model.ressourceAction localWithExtraInfo.ressources
            ]
        , div [ class "flex flex-col gap-4 p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy4 viewContacts (Just <| Data.Contact.FonctionLocal { fonction = "" }) SetContactAction model.contactAction localWithExtraInfo.contacts
            ]
        , div [ class "flex flex-col gap-4 p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy3 viewProprietaires model.prochainId model.contactCreation localWithExtraInfo.local
            ]
        , div [ class "flex flex-col gap-4 p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy viewOccupants localWithExtraInfo.local
            ]
        , div [ class "flex flex-col gap-4 p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy viewMutations localWithExtraInfo.local
            ]
        ]


viewNomModal : String -> Maybe ModifierNom -> Html Msg
viewNomModal currentNom modifierNomRequest =
    let
        ( opened, content, title ) =
            case modifierNomRequest of
                Nothing ->
                    ( False, nothing, nothing )

                Just { nom, request } ->
                    ( True
                    , viewNomForm currentNom request nom
                    , text "Modifier le nom du local"
                    )
    in
    DSFR.Modal.view
        { id = "local-nom-modale"
        , label = "local-nom-modale"
        , openMsg = NoOp
        , closeMsg = Just <| AfficherModifierNom False
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.md
        }
        content
        Nothing
        |> Tuple.first


viewNomForm : String -> WebData LocalWithExtraInfo -> String -> Html Msg
viewNomForm currentNom request nom =
    formWithListeners [ Events.onSubmit <| ConfirmNomRequest, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12 ]
            [ DSFR.Input.new
                { value = nom
                , label = text "Nom du local"
                , onInput = UpdateNom
                , name = "nom-local"
                }
                |> DSFR.Input.view
            , div [ class "fr-text-default--info", Typo.textSm ]
                [ DSFR.Icons.System.infoFill |> DSFR.Icons.iconSM
                , text " "
                , text "Laissez le champ vide si vous ne souhaitez pas modifier le nom"
                ]
            ]
        , div [ DSFR.Grid.col12 ]
            [ footerNom currentNom request nom
            ]
        ]


footerNom : String -> WebData LocalWithExtraInfo -> String -> Html Msg
footerNom currentNom request nom =
    DSFR.Button.group
        [ DSFR.Button.new { onClick = Nothing, label = "Valider" }
            |> DSFR.Button.withDisabled (currentNom == nom || request == RD.Loading)
            |> DSFR.Button.submit
        , DSFR.Button.new { onClick = Just <| AfficherModifierNom False, label = "Annuler" }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


viewVacanceModal : Maybe Bool -> Maybe ModifierVacance -> Html Msg
viewVacanceModal currentVacance modifierVacanceRequest =
    let
        ( opened, content, title ) =
            case modifierVacanceRequest of
                Nothing ->
                    ( False, nothing, nothing )

                Just { occupe, request } ->
                    ( True
                    , viewVacanceForm currentVacance request occupe
                    , text "Modifier la vacance du local"
                    )
    in
    DSFR.Modal.view
        { id = "local-vacance-modale"
        , label = "local-vacance-modale"
        , openMsg = NoOp
        , closeMsg = Just <| AfficherModifierVacance False
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.md
        }
        content
        Nothing
        |> Tuple.first


viewVacanceForm : Maybe Bool -> WebData LocalWithExtraInfo -> Maybe Bool -> Html Msg
viewVacanceForm currentVacance request vacance =
    formWithListeners [ Events.onSubmit <| ConfirmVacanceRequest, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12 ]
            [ DSFR.Radio.group
                { id = "radio-vacance-local"
                , options = [ Just False, Just True, Nothing ]
                , current = Just vacance
                , toLabel = Maybe.map vacanceToDisplay >> Maybe.withDefault "Non renseigné" >> text
                , toId = Maybe.map vacanceToString >> Maybe.withDefault "non-renseigne"
                , msg = UpdateVacance
                , legend = Just <| text "Vacance du local"
                }
                |> DSFR.Radio.inline
                |> DSFR.Radio.view
            ]
        , div [ DSFR.Grid.col12 ]
            [ footerVacance currentVacance request vacance
            ]
        ]


footerVacance : Maybe Bool -> WebData LocalWithExtraInfo -> Maybe Bool -> Html Msg
footerVacance currentVacance request vacance =
    DSFR.Button.group
        [ DSFR.Button.new
            { onClick = Nothing
            , label = "Valider"
            }
            |> DSFR.Button.withDisabled (currentVacance == vacance || request == RD.Loading)
            |> DSFR.Button.submit
        , DSFR.Button.new
            { onClick =
                Just <|
                    AfficherModifierVacance False
            , label = "Annuler"
            }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


viewRessourcesModal : WebData LocalWithExtraInfo -> RessourceAction -> Html Msg
viewRessourcesModal saveRessourceRequest ressourceAction =
    let
        ( opened, content, title ) =
            case ressourceAction of
                NoRessource ->
                    ( False, nothing, nothing )

                NewRessource ressource ->
                    ( True
                    , viewRessourceFormFields True saveRessourceRequest ressource
                    , text "Ajouter une ressource"
                    )

                EditRessource ressource ->
                    ( True
                    , viewRessourceFormFields False saveRessourceRequest ressource
                    , text "Modifier une ressource"
                    )

                DeleteRessource ressource ->
                    ( True
                    , div []
                        [ text "Êtes-vous sûr(e) de vouloir supprimer cette ressource\u{00A0}?"
                        , div [ DSFR.Grid.col12, class "flex flex-col !pt-4" ]
                            [ [ DSFR.Button.new { onClick = Just <| ConfirmRessourceAction, label = "Supprimer" }
                                    |> DSFR.Button.withDisabled (saveRessourceRequest == RD.Loading)
                                    |> DSFR.Button.withAttrs
                                        [ id <| "confirmer-modifier-ressource-" ++ String.fromInt ressource.id
                                        , Html.Attributes.title "Supprimer"
                                        ]
                              , DSFR.Button.new { onClick = Just <| CancelRessourceAction, label = "Annuler" }
                                    |> DSFR.Button.withAttrs
                                        [ id <| "annuler-modifier-ressource-" ++ String.fromInt ressource.id
                                        , Html.Attributes.title "Annuler"
                                        ]
                                    |> DSFR.Button.secondary
                              ]
                                |> DSFR.Button.group
                                |> DSFR.Button.inline
                                |> DSFR.Button.alignedRightInverted
                                |> DSFR.Button.viewGroup
                            ]
                        , div [ class "flex flex-row justify-end w-full" ]
                            [ case saveRessourceRequest of
                                RD.Failure _ ->
                                    DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite" }
                                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                                _ ->
                                    nothing
                            ]
                        ]
                    , text "Supprimer une ressource"
                    )
    in
    DSFR.Modal.view
        { id = "ressource"
        , label = "ressource"
        , openMsg = NoOp
        , closeMsg = Just CancelRessourceAction
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.md
        }
        content
        Nothing
        |> Tuple.first


type MutationHeader
    = MHDate
    | MHNature
    | MHPrix


mutationHeaderToDisplay : MutationHeader -> String
mutationHeaderToDisplay header =
    case header of
        MHDate ->
            "Date"

        MHNature ->
            "Nature"

        MHPrix ->
            "Prix"


mutationToCell : MutationHeader -> Mutation -> Html Msg
mutationToCell header { date, valeur, type_ } =
    case header of
        MHDate ->
            text <|
                withEmptyAs "-" <|
                    Lib.Date.formatDateShort <|
                        date

        MHNature ->
            text <|
                withEmptyAs "-" <|
                    type_

        MHPrix ->
            valeur
                |> Maybe.map formatFloatWithThousandSpacing
                |> Maybe.map (\prix -> prix ++ "\u{00A0}€")
                |> Maybe.withDefault "-"
                |> text
                |> List.singleton
                |> div [ class "flex flex-row justify-end w-full" ]


viewRessourceFormFields : Bool -> WebData LocalWithExtraInfo -> Ressource -> Html Msg
viewRessourceFormFields new request ressource =
    let
        ressourceIsEmpty =
            ressource.lien == ""
    in
    formWithListeners [ Events.onSubmit <| ConfirmRessourceAction, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12 ]
            [ DSFR.Input.new { value = ressourceTypeToString ressource.type_, onInput = UpdatedRessource RType, label = text "Type", name = "ressource-type" }
                |> DSFR.Input.select
                    { options = ressourceTypes
                    , toId = ressourceTypeToString
                    , toLabel = ressourceTypeToDisplay >> text
                    , toDisabled = Nothing
                    }
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col6 ]
            [ DSFR.Input.new { value = ressource.nom, onInput = UpdatedRessource RNom, label = text "Nom", name = "ressource-nom" }
                |> DSFR.Input.withRequired True
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col6 ]
            [ DSFR.Input.new { value = ressource.lien, onInput = UpdatedRessource RLien, label = text "Lien", name = "ressource-lien" }
                |> DSFR.Input.withRequired True
                |> DSFR.Input.view
            ]
        , let
            label =
                if new then
                    "Ajouter"

                else
                    "Modifier"
          in
          div [ DSFR.Grid.col12, class "flex flex-col !pt-4" ]
            [ [ DSFR.Button.new { onClick = Nothing, label = label }
                    |> DSFR.Button.withDisabled (ressourceIsEmpty || request == RD.Loading)
                    |> DSFR.Button.submit
                    |> DSFR.Button.withAttrs
                        [ id <| "confirmer-modifier-ressource-" ++ String.fromInt ressource.id
                        , Html.Attributes.title label
                        ]
              , DSFR.Button.new { onClick = Just <| CancelRessourceAction, label = "Annuler" }
                    |> DSFR.Button.secondary
                    |> DSFR.Button.withAttrs
                        [ id <| "annuler-modifier-ressource-" ++ String.fromInt ressource.id
                        , Html.Attributes.title "Annuler"
                        ]
              ]
                |> DSFR.Button.group
                |> DSFR.Button.inline
                |> DSFR.Button.alignedRightInverted
                |> DSFR.Button.viewGroup
            ]
        , div [ class "flex flex-row justify-end w-full" ]
            [ case request of
                RD.Failure _ ->
                    DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite" }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                _ ->
                    nothing
            ]
        ]


badgeGeolocalisation : LocalWithExtraInfo -> Html msg
badgeGeolocalisation localWithExtraInfo =
    let
        { geolocalisation } =
            localWithExtraInfo.local

        { geolocalisationContribution, geolocalisationEquipe } =
            localWithExtraInfo

        ( icon, txt, color ) =
            if geolocalisationContribution /= Nothing then
                ( DSFR.Icons.Map.mapPin2Fill, "géolocalisé (modifié par " ++ (geolocalisationEquipe |> Maybe.withDefault "") ++ ")", "blue-text" )

            else if geolocalisation /= Nothing then
                ( DSFR.Icons.Map.mapPin2Line, "géolocalisé", "blue-text" )

            else
                ( DSFR.Icons.Map.mapPin2Line, "non géolocalisé", "red-text" )
    in
    div [ Typo.textSm, class "!mb-0" ]
        [ span [ class color ] [ DSFR.Icons.iconSM icon ]
        , text " "
        , text <| "Local " ++ txt
        ]


viewLocalHeader : WebData () -> LocalWithExtraInfo -> Html Msg
viewLocalHeader modifierGeolocalisationRequest ({ local, portefeuilleInfos } as localWithExtraInfo) =
    let
        ( aDesOccupants, attributs ) =
            if List.length local.occupants > 0 then
                ( True, [ Html.Attributes.title "Ce local a un occupant" ] )

            else
                ( False, [] )
    in
    div [ class "flex flex-row justify-between items-start fr-card--white p-8" ]
        [ div [ class "flex flex-col" ]
            [ h1 [ Typo.textBold, Typo.fr_h3, class "!mb-0" ]
                [ DSFR.Icons.iconLG DSFR.Icons.Buildings.storeLine
                , text "\u{00A0}"
                , case local.nom of
                    "" ->
                        text <| local.id

                    nom ->
                        text <| nom ++ " (" ++ local.id ++ ")"
                ]
            , div [ Typo.textBold, Typo.textSm, class "!mb-0" ]
                [ text <|
                    adresseToString <|
                        .adresse <|
                            local
                ]
            , badgeGeolocalisation localWithExtraInfo
            ]
        , div [ class "flex flex-col gap-2" ]
            [ div [ class "flex flex-row gap-4 justify-end items-center" ]
                [ local
                    |> .occupe
                    |> badgeVacance
                , local
                    |> .actif
                    |> badgeInactif
                , UI.Entite.iconesPortefeuilleInfos portefeuilleInfos
                , UI.Local.iconeFavoris { toggleFavorite = Just <| \_ -> ToggleFavorite } local portefeuilleInfos
                , DSFR.Button.dropdownSelector { label = text "Actions", hint = Just "Actions supplémentaires", id = "local-actions" } <|
                    [ DSFR.Button.new
                        { onClick = Just <| AfficherModifierVacance True
                        , label = "Modifier la vacance du local"
                        }
                        |> DSFR.Button.withDisabled aDesOccupants
                        |> DSFR.Button.withAttrs attributs
                        |> DSFR.Button.leftIcon DSFR.Icons.Buildings.storeLine
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.view
                    , DSFR.Button.new
                        { onClick = Just <| AfficherModifierNom True
                        , label = "Modifier le nom du local"
                        }
                        |> DSFR.Button.leftIcon DSFR.Icons.Design.editLine
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.view
                    , DSFR.Button.new
                        { onClick = Just <| ToggleGeolocalisationContribution
                        , label = "Modifier la géolocalisation"
                        }
                        |> DSFR.Button.withDisabled (modifierGeolocalisationRequest == RD.Loading)
                        |> DSFR.Button.leftIcon DSFR.Icons.Map.mapPin2Line
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.view
                    , DSFR.Button.new
                        { onClick = Just <| ClickedPartage
                        , label = "Partager la fiche par e-mail"
                        }
                        |> DSFR.Button.leftIcon DSFR.Icons.Business.mailLine
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.view
                    ]
                ]
            ]
        ]


viewLocal : Maybe LocalEtiquettes -> WebData (List Zonage) -> MultiSelectRemote.SmartSelect Msg String -> MultiSelectRemote.SmartSelect Msg String -> WebData LocalWithExtraInfo -> LocalWithExtraInfo -> Html Msg
viewLocal editLocalEtiquettes zonages selectLocalisations selectMots saveLocalEtiquettesRequest { local, etiquettes, contribution } =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div
            [ DSFR.Grid.col4
            , class "p-2"
            ]
            [ Lazy.lazy viewLocalResume local
            ]
        , div
            [ DSFR.Grid.col4
            , Html.Attributes.classList
                [ ( "fr-card--grey", editLocalEtiquettes == Nothing )
                ]
            ]
            [ div [] <| List.singleton <| viewLocalData editLocalEtiquettes (zonages |> RD.withDefault []) selectLocalisations selectMots saveLocalEtiquettesRequest etiquettes
            ]
        , div
            [ DSFR.Grid.col4
            ]
            [ Lazy.lazy viewLocalContribution contribution
            ]
        ]


viewLocalData : Maybe LocalEtiquettes -> List Zonage -> MultiSelectRemote.SmartSelect Msg String -> MultiSelectRemote.SmartSelect Msg String -> WebData LocalWithExtraInfo -> Etiquettes -> Html Msg
viewLocalData editLocalEtiquettes zonages selectLocalisations selectMots saveLocalEtiquettesRequest etiquettes =
    div [] <|
        [ Lazy.lazy viewLocalDataHeader editLocalEtiquettes
        , hr [ class "fr-hr" ] []
        , div [] <|
            List.singleton <|
                viewEtiquettesSelects editLocalEtiquettes zonages selectLocalisations selectMots saveLocalEtiquettesRequest etiquettes
        ]


viewLocalDataHeader : Maybe LocalEtiquettes -> Html Msg
viewLocalDataHeader editLocalEtiquettes =
    div [ Typo.textBold, class "flex flex-row justify-between items-center" ]
        [ span [ Typo.textBold, class "p-2" ] [ text "Étiquettes" ]
        , DSFR.Button.new { label = "Modifier le local", onClick = Just EditLocalEtiquettes }
            |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
            |> DSFR.Button.withDisabled (editLocalEtiquettes /= Nothing)
            |> DSFR.Button.withAttrs [ id "modifier-local", Html.Attributes.classList [ ( "invisible", editLocalEtiquettes /= Nothing ) ] ]
            |> DSFR.Button.tertiaryNoOutline
            |> DSFR.Button.view
        ]


viewEtiquettesSelects : Maybe LocalEtiquettes -> List Zonage -> MultiSelectRemote.SmartSelect Msg String -> MultiSelectRemote.SmartSelect Msg String -> WebData LocalWithExtraInfo -> Etiquettes -> Html Msg
viewEtiquettesSelects editLocalEtiquettes zonages selectLocalisations selectMots saveLocalEtiquettesRequest { localisations, motsCles } =
    let
        classAttrs =
            class "flex flex-col px-4 h-full gap-4"

        affichageEtiquettes =
            div [ classAttrs ]
                [ div []
                    [ div [ Typo.textBold ] [ span [] [ text "Zone géographique", sup [ Html.Attributes.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ] ]
                    , let
                        locs =
                            localisations
                                |> List.map
                                    (\t ->
                                        { data = t, toString = identity }
                                            |> DSFR.Tag.clickable
                                                (Route.toUrl <|
                                                    Route.Locaux <|
                                                        (\q ->
                                                            if q == "" then
                                                                Nothing

                                                            else
                                                                Just q
                                                        )
                                                        <|
                                                            filtersAndPaginationToQuery
                                                                ( { emptyFilters
                                                                    | zones = [ t ]
                                                                  }
                                                                , defaultPagination
                                                                )
                                                )
                                    )

                        zons =
                            zonages
                                |> List.map .nom
                                |> List.map
                                    (\t ->
                                        { data = t, toString = identity }
                                            |> DSFR.Tag.clickable ""
                                    )
                      in
                      case locs ++ zons of
                        [] ->
                            text "-"

                        _ ->
                            (locs ++ zons)
                                |> DSFR.Tag.medium
                    ]
                , div []
                    [ div [ Typo.textBold ] [ span [] [ text "Mots-clés", sup [ Html.Attributes.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ] ]
                    , case motsCles of
                        [] ->
                            text "-"

                        _ ->
                            motsCles
                                |> List.sort
                                |> List.map
                                    (\t ->
                                        { data = t, toString = identity }
                                            |> DSFR.Tag.clickable
                                                (Route.toUrl <|
                                                    Route.Locaux <|
                                                        (\q ->
                                                            if q == "" then
                                                                Nothing

                                                            else
                                                                Just q
                                                        )
                                                        <|
                                                            filtersAndPaginationToQuery
                                                                ( { emptyFilters
                                                                    | mots = [ t ]
                                                                  }
                                                                , defaultPagination
                                                                )
                                                )
                                    )
                                |> DSFR.Tag.medium
                    ]
                ]
    in
    case editLocalEtiquettes of
        Nothing ->
            affichageEtiquettes

        Just infos ->
            let
                noDifference =
                    isPermutationOf infos.localisations localisations
                        && isPermutationOf infos.mots motsCles
            in
            formWithListeners [ Events.onSubmit <| RequestedSaveLocalEtiquettes, classAttrs, class "border-[0.1rem] border-france-blue py-[0.4rem]" ]
                [ div []
                    [ div [ class "flex flex-col gap-4" ]
                        [ selectLocalisations
                            |> MultiSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = infos.localisations
                                , optionLabelFn = identity
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle = span [ Typo.textBold ] [ text "Zone géographique", sup [ Html.Attributes.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                , viewSelectedOptionFn = text
                                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg =
                                    \searchText ->
                                        "Aucune autre zone géographique n'a été trouvée"
                                            ++ (if searchText == "" then
                                                    ""

                                                else
                                                    " pour " ++ searchText
                                               )
                                , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                                , newOption = Just identity
                                , searchPrompt = "Rechercher une zone géographique"
                                }
                        , let
                            locs =
                                infos.localisations
                                    |> List.map
                                        (\localisation ->
                                            DSFR.Tag.deletable UnselectLocalisation { data = localisation, toString = identity }
                                        )

                            zons =
                                zonages
                                    |> List.map .nom
                                    |> List.map
                                        (\zonage ->
                                            DSFR.Tag.clickable "" { data = zonage, toString = identity }
                                                |> DSFR.Tag.withExtraAttrs [ Html.Attributes.title "Cette étiquette a été ajoutée automatiquement via un zonage personnalisée et ne peut être supprimée ici" ]
                                        )
                          in
                          (locs ++ zons)
                            |> DSFR.Tag.medium
                        ]
                    ]
                , div []
                    [ div [ class "flex flex-col gap-4" ]
                        [ selectMots
                            |> MultiSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = infos.mots
                                , optionLabelFn = identity
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle = span [ Typo.textBold ] [ text "Mots-clés", sup [ Html.Attributes.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                , viewSelectedOptionFn = text
                                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ plural diff ++ " pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg =
                                    \searchText ->
                                        "Aucun autre mot-clé n'a été trouvé"
                                            ++ (if searchText == "" then
                                                    ""

                                                else
                                                    " pour " ++ searchText
                                               )
                                , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                                , newOption = Just identity
                                , searchPrompt = "Rechercher un mot-clé"
                                }
                        , infos.mots
                            |> List.sort
                            |> List.map (\mot -> DSFR.Tag.deletable UnselectMot { data = mot, toString = identity })
                            |> DSFR.Tag.medium
                        ]
                    ]
                , case saveLocalEtiquettesRequest of
                    RD.NotAsked ->
                        nothing

                    RD.Loading ->
                        nothing

                    RD.Failure _ ->
                        DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite" }
                            |> DSFR.Alert.alert Nothing DSFR.Alert.error

                    RD.Success _ ->
                        DSFR.Alert.small { title = Nothing, description = text "Modifications enregistrées\u{00A0}!" }
                            |> DSFR.Alert.alert Nothing DSFR.Alert.success
                , [ DSFR.Button.new { onClick = Nothing, label = "Valider" }
                        |> DSFR.Button.withDisabled (noDifference || saveLocalEtiquettesRequest == RD.Loading)
                        |> DSFR.Button.submit
                  , DSFR.Button.new { onClick = Just CancelEditLocalEtiquettes, label = "Annuler" }
                        |> DSFR.Button.secondary
                        |> DSFR.Button.withDisabled (saveLocalEtiquettesRequest == RD.Loading)
                  ]
                    |> DSFR.Button.group
                    |> DSFR.Button.inline
                    |> DSFR.Button.alignedRightInverted
                    |> DSFR.Button.viewGroup
                ]


viewLocalContribution : Contribution -> Html Msg
viewLocalContribution contribution =
    div [ class "pb-4" ]
        [ div [ Typo.textBold, class "flex flex-row justify-between items-center" ]
            [ text "Données personnalisées"
            , DSFR.Button.new { label = "Modifier", onClick = Just <| AfficherModifierContribution True }
                |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.view
            ]
        , hr [ class "fr-hr" ] []
        , div []
            [ Lib.UI.infoLine Nothing "Loyer annuel" <|
                (\e ->
                    if e == "" then
                        "-"

                    else
                        e ++ "\u{2009}€"
                )
                <|
                    Lib.UI.formatNumberAsStringWithThousandSpacing <|
                        .loyer <|
                            contribution
            ]
        , div []
            [ Lib.UI.infoLine Nothing "Type de bail" <|
                withEmptyAs "-" <|
                    Maybe.withDefault "" <|
                        Maybe.map bailTypeToDisplay <|
                            .bailType <|
                                contribution
            ]
        , div []
            [ Lib.UI.infoLine Nothing "Type de vacance" <|
                withEmptyAs "-" <|
                    Maybe.withDefault "" <|
                        Maybe.map vacanceTypeToDisplay <|
                            .vacanceType <|
                                contribution
            ]
        , div []
            [ Lib.UI.infoLine Nothing "Motif de vacance" <|
                withEmptyAs "-" <|
                    Maybe.withDefault "" <|
                        Maybe.map vacanceMotifToDisplay <|
                            .vacanceMotif <|
                                contribution
            ]
        , div []
            [ Lib.UI.infoLine Nothing "Local remembré" <|
                withEmptyAs "-" <|
                    (\r ->
                        if r then
                            "Oui"

                        else
                            "Non"
                    )
                    <|
                        .remembre <|
                            contribution
            ]
        , div []
            [ Lib.UI.infoLine Nothing "Valeur fonds de commerce" <|
                (\e ->
                    if e == "" then
                        "-"

                    else
                        e ++ "\u{2009}€"
                )
                <|
                    Lib.UI.formatNumberAsStringWithThousandSpacing <|
                        .fonds <|
                            contribution
            ]
        , div []
            [ Lib.UI.infoLine Nothing "Prix de vente du local" <|
                (\e ->
                    if e == "" then
                        "-"

                    else
                        e ++ "\u{2009}€"
                )
                <|
                    Lib.UI.formatNumberAsStringWithThousandSpacing <|
                        .vente <|
                            contribution
            ]
        , div []
            [ Lib.UI.infoLine Nothing "Commentaire" <|
                withEmptyAs "-" <|
                    .commentaires <|
                        contribution
            ]
        ]


viewContributionModal : Contribution -> Maybe ModifierContribution -> Html Msg
viewContributionModal currentContrib modifierContribution =
    let
        ( opened, content, title ) =
            case modifierContribution of
                Nothing ->
                    ( False, nothing, nothing )

                Just contrib ->
                    ( True
                    , viewContributionForm currentContrib contrib
                    , text "Données personnalisées"
                    )
    in
    DSFR.Modal.view
        { id = "local-contribution-modale"
        , label = "local-contribution-modale"
        , openMsg = NoOp
        , closeMsg = Just <| AfficherModifierContribution False
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.md
        }
        content
        Nothing
        |> Tuple.first


viewContributionForm : Contribution -> ModifierContribution -> Html Msg
viewContributionForm currentContribution { contribution, request } =
    let
        noDifference =
            currentContribution.loyer
                == contribution.loyer
                && currentContribution.bailType
                == contribution.bailType
                && currentContribution.vacanceType
                == contribution.vacanceType
                && currentContribution.vacanceMotif
                == contribution.vacanceMotif
                && currentContribution.remembre
                == contribution.remembre
                && currentContribution.fonds
                == contribution.fonds
                && currentContribution.vente
                == contribution.vente
                && currentContribution.commentaires
                == contribution.commentaires
    in
    formWithListeners [ Events.onSubmit <| ConfirmContributionRequest, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12 ]
            [ DSFR.Input.new
                { value = contribution.loyer
                , label = text "Loyer annuel (en €)"
                , onInput = UpdateContribution CLoyer
                , name = "nom-local"
                }
                |> DSFR.Input.number Nothing
                |> DSFR.Input.withHint [ text "Exemple\u{00A0}: 10500" ]
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col12 ]
            [ DSFR.Input.new
                { value = contribution.bailType |> Maybe.map bailTypeToString |> Maybe.withDefault "non-renseigne"
                , label = text "Type de bail"
                , onInput = UpdateContribution CBailType
                , name = "bail-type-local"
                }
                |> DSFR.Input.select
                    { options = Nothing :: List.map Just bailTypeList
                    , toId = Maybe.map bailTypeToString >> Maybe.withDefault "non-renseigne"
                    , toLabel = Maybe.map bailTypeToDisplay >> Maybe.withDefault "Non renseigné" >> text
                    , toDisabled = Nothing
                    }
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col12 ]
            [ DSFR.Input.new
                { value = contribution.vacanceType |> Maybe.map vacanceTypeToString |> Maybe.withDefault "non-renseigne"
                , label = text "Type de vacance"
                , onInput = UpdateContribution CVacanceType
                , name = "vacance-type-local"
                }
                |> DSFR.Input.select
                    { options = Nothing :: List.map Just vacanceTypeList
                    , toId = Maybe.map vacanceTypeToString >> Maybe.withDefault "non-renseigne"
                    , toLabel = Maybe.map vacanceTypeToDisplay >> Maybe.withDefault "Non renseigné" >> text
                    , toDisabled = Nothing
                    }
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col12 ]
            [ DSFR.Input.new
                { value = contribution.vacanceMotif |> Maybe.map vacanceMotifToString |> Maybe.withDefault "non-renseigne"
                , label = text "Motif de vacance"
                , onInput = UpdateContribution CVacanceMotif
                , name = "vacance-motif-local"
                }
                |> DSFR.Input.select
                    { options = Nothing :: List.map Just vacanceMotifList
                    , toId = Maybe.map vacanceMotifToString >> Maybe.withDefault "non-renseigne"
                    , toLabel = Maybe.map vacanceMotifToDisplay >> Maybe.withDefault "Non renseigné" >> text
                    , toDisabled = Nothing
                    }
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col12 ]
            [ DSFR.Radio.group
                { id = "contribution-remembre-local"
                , options = [ True, False ]
                , current = Just contribution.remembre
                , toLabel =
                    text
                        << (\r ->
                                if r then
                                    "Oui"

                                else
                                    "Non"
                           )
                , toId =
                    \r ->
                        if r then
                            "oui"

                        else
                            "non"
                , msg =
                    \r ->
                        UpdateContribution CRemembre <|
                            if r then
                                "oui"

                            else
                                "non"
                , legend = Just <| text "Local remembré"
                }
                |> DSFR.Radio.withExtraAttrs [ class "!mb-0" ]
                |> DSFR.Radio.inline
                |> DSFR.Radio.view
            ]
        , div [ DSFR.Grid.col6 ]
            [ DSFR.Input.new
                { value = contribution.fonds
                , label = text "Valeur fonds de commerce (en €)"
                , onInput = UpdateContribution CFonds
                , name = "fonds-local"
                }
                |> DSFR.Input.number Nothing
                |> DSFR.Input.withHint [ text "Exemple\u{00A0}: 90000" ]
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col6 ]
            [ DSFR.Input.new
                { value = contribution.vente
                , label = text "Prix de vente du local (en €)"
                , onInput = UpdateContribution CVente
                , name = "vente-local"
                }
                |> DSFR.Input.number Nothing
                |> DSFR.Input.withHint [ text "Exemple\u{00A0}: 100000" ]
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col12 ]
            [ DSFR.Input.new
                { value = contribution.commentaires
                , label = span [] [ text "Commentaires" ]
                , onInput = UpdateContribution CCommentaires
                , name = "description"
                }
                |> DSFR.Input.textArea (Just 4)
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col12 ]
            [ case request of
                RD.NotAsked ->
                    nothing

                RD.Loading ->
                    nothing

                RD.Failure _ ->
                    DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite" }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                RD.Success _ ->
                    DSFR.Alert.small { title = Nothing, description = text "Modifications enregistrées\u{00A0}!" }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.success
            ]
        , div [ DSFR.Grid.col12 ]
            [ [ DSFR.Button.new { onClick = Nothing, label = "Valider" }
                    |> DSFR.Button.withDisabled (noDifference || request == RD.Loading)
                    |> DSFR.Button.submit
              , DSFR.Button.new { onClick = Just <| AfficherModifierContribution False, label = "Annuler" }
                    |> DSFR.Button.secondary
                    |> DSFR.Button.withDisabled (request == RD.Loading)
              ]
                |> DSFR.Button.group
                |> DSFR.Button.inline
                |> DSFR.Button.alignedRightInverted
                |> DSFR.Button.viewGroup
            ]
        ]


viewModifierGeolocalisationModal : Shared.User -> String -> WebData () -> Maybe GeolocalisationContribution -> WebData LocalWithExtraInfo -> Html Msg
viewModifierGeolocalisationModal user localId modifierGeolocalisationRequest modificationGeolocalisation localWithExtraInfo =
    let
        ( opened, content, title ) =
            case ( localWithExtraInfo, modificationGeolocalisation ) of
                ( _, Nothing ) ->
                    ( False, nothing, nothing )

                ( RD.Success _, Just geolocalisationContribution ) ->
                    ( True
                    , viewModifierGeolocalisationModalBody
                        user
                        localId
                        (localWithExtraInfo
                            |> RD.map Just
                            |> RD.withDefault Nothing
                            |> Maybe.andThen .geolocalisationEquipe
                        )
                        modifierGeolocalisationRequest
                        geolocalisationContribution
                    , text "Modifier la géolocalisation du local"
                    )

                _ ->
                    ( False, nothing, nothing )
    in
    DSFR.Modal.view
        { id = "modifier-geolocalisation"
        , label = "modifier-geolocalisation"
        , openMsg = NoOp
        , closeMsg = Just ToggleGeolocalisationContribution
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.lg
        }
        content
        Nothing
        |> Tuple.first


viewModifierGeolocalisationModalBody : Shared.User -> String -> Maybe String -> WebData () -> GeolocalisationContribution -> Html Msg
viewModifierGeolocalisationModalBody user localId geolocalisationEquipe modifierGeolocalisationRequest geolocalisationContribution =
    let
        userEquipe =
            user
                |> User.role
                |> Data.Role.equipe
                |> Maybe.map Data.Equipe.nom

        modifieParUneAutreEquipe =
            case geolocalisationEquipe of
                Nothing ->
                    False

                Just equipe ->
                    equipe
                        |> Just
                        |> (/=) userEquipe
    in
    Html.form [ class "flex flex-col gap-4", Events.onSubmit RequestedGeolocalisationContributionSave ]
        [ div [ class "flex flex-col gap-2" ]
            [ div []
                [ text <| "Géolocalisation du local"
                , text "\u{00A0}: "
                , text <|
                    Maybe.withDefault "-" <|
                        Maybe.map pointToString <|
                            .geolocalisationActuelle <|
                                geolocalisationContribution
                ]
            , div []
                [ text <| "Géolocalisation personnalisée"
                , text "\u{00A0}: "
                , text <|
                    Maybe.withDefault "-" <|
                        Maybe.map pointToString <|
                            .geolocalisationContribution <|
                                geolocalisationContribution
                ]
            ]
        , if modifieParUneAutreEquipe then
            let
                emails =
                    [ Lib.Variables.contactEmail ]

                subject =
                    Builder.string "subject" <|
                        "Signalement de géolocalisation erronée pour le local "
                            ++ localId

                mailto =
                    "mailto:"
                        ++ String.join "," emails
                        ++ Builder.toQuery [ subject ]
            in
            div [ class "flex flex-col", Typo.textBold ]
                [ p [ class "!mb-0" ]
                    [ text "Cette géolocalisation a déjà été modifiée par l'équipe "
                    , text <|
                        Maybe.withDefault "?" <|
                            geolocalisationEquipe
                    , text ", et vous ne pouvez donc pas la modifier."
                    ]
                , p [ class "!mb-0" ]
                    [ text "Si vous pensez que la géolocalisation est erronée, contactez-nous à l'adresse suivante\u{00A0}: "
                    , Typo.externalLink mailto
                        []
                        [ text Lib.Variables.contactEmail
                        ]
                    ]
                ]

          else
            div [ class "flex flex-col gap-2" ]
                [ let
                    error =
                        if geolocalisationContribution.nouvelleGeolocalisationChamp /= "" && (geolocalisationContribution.nouvelleGeolocalisation == Nothing) then
                            Just [ text "La géolocalisation saisie est invalide" ]

                        else
                            Nothing
                  in
                  div [ class "flex flex-col gap-2" ]
                    [ DSFR.Input.new
                        { value =
                            geolocalisationContribution
                                |> .nouvelleGeolocalisationChamp
                        , onInput = UpdateGeolocalisationContributionInput
                        , label = text "Nouvelle géolocalisation personnalisée"
                        , name = "geolocalisation-contribution-nouvelle"
                        }
                        |> DSFR.Input.withError error
                        |> DSFR.Input.withExtraAttrs [ class "!mb-0" ]
                        |> DSFR.Input.withHint [ text "Format attendu\u{00A0}: longitude, latitude" ]
                        |> DSFR.Input.view
                    , DSFR.Alert.small
                        { title = Nothing
                        , description =
                            span [ Typo.textSm ]
                                [ text "Déplacez la carte pour modifier la géolocalisation."
                                , br []
                                , text "Videz le champ pour supprimer."
                                ]
                        }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.info
                    ]
                ]
        , adresseFilter geolocalisationContribution.selectAdresse
        , div [ class "w-full h-[200px] z-10" ]
            [ Html.node "maplibre-geolocalisation"
                [ Html.Attributes.attribute "data" <|
                    Encode.encode 0 <|
                        Encode.object
                            [ ( "centre"
                              , geolocalisationContribution
                                    |> .centreCarte
                                    |> encodePoint
                              )
                            , ( "geolocalisationActuelle"
                              , geolocalisationContribution
                                    |> .geolocalisationActuelle
                                    |> Maybe.map encodePoint
                                    |> Maybe.withDefault Encode.null
                              )
                            , ( "geolocalisationContribution"
                              , geolocalisationContribution
                                    |> .geolocalisationContribution
                                    |> Maybe.map encodePoint
                                    |> Maybe.withDefault Encode.null
                              )
                            , ( "nouvelleGeolocalisation"
                              , geolocalisationContribution
                                    |> .nouvelleGeolocalisation
                                    |> Maybe.map encodePoint
                                    |> Maybe.withDefault Encode.null
                              )
                            , ( "zoom"
                              , geolocalisationContribution
                                    |> .zoom
                                    |> Maybe.map Encode.int
                                    |> Maybe.withDefault Encode.null
                              )
                            ]
                , Events.on "movemap" <| loggingDecoder decodeMoveMap
                ]
                []
            ]
        , div [ class "flex flex-row justify-end" ]
            [ div []
                [ let
                    ( isButtonDisabled, label ) =
                        if modifieParUneAutreEquipe then
                            ( True, "Enregistrer" )

                        else
                            case modifierGeolocalisationRequest of
                                RD.Loading ->
                                    ( True, "Enregistrement en cours" )

                                _ ->
                                    if geolocalisationContribution.nouvelleGeolocalisation == geolocalisationContribution.geolocalisationContribution then
                                        ( True
                                        , "Enregistrer"
                                        )

                                    else if geolocalisationContribution.nouvelleGeolocalisationChamp == "" then
                                        ( False
                                        , "Supprimer"
                                        )

                                    else if geolocalisationContribution.nouvelleGeolocalisation == Nothing then
                                        ( True
                                        , "Enregistrer"
                                        )

                                    else
                                        ( False
                                        , "Enregistrer"
                                        )

                    buttons =
                        case modifierGeolocalisationRequest of
                            RD.Success _ ->
                                [ DSFR.Button.new
                                    { onClick = Just ToggleGeolocalisationContribution
                                    , label = "OK"
                                    }
                                ]

                            _ ->
                                [ DSFR.Button.new
                                    { onClick = Nothing
                                    , label = label
                                    }
                                    |> DSFR.Button.withDisabled isButtonDisabled
                                    |> DSFR.Button.submit
                                , DSFR.Button.new
                                    { onClick = Just ToggleGeolocalisationContribution
                                    , label = "Annuler"
                                    }
                                    |> DSFR.Button.secondary
                                ]
                  in
                  DSFR.Button.group buttons
                    |> DSFR.Button.inline
                    |> DSFR.Button.alignedRightInverted
                    |> DSFR.Button.viewGroup
                ]
            ]
        ]


decodeMoveMap : Decoder Msg
decodeMoveMap =
    Decode.field "detail" <|
        Decode.map2 (\lon lat -> UpdateGeolocalisationContributionByMap ( lon, lat ))
            (Decode.field "longitude" Decode.float)
            (Decode.field "latitude" Decode.float)


etageRegex : Regex.Regex
etageRegex =
    Regex.fromString "^0"
        |> Maybe.withDefault Regex.never


viewLocalResume : Local -> Html Msg
viewLocalResume local =
    div [ class "p-2" ]
        [ div [ Typo.textBold, class "!mb-2" ] [ text "Données publiques", sup [ Html.Attributes.title hintDonneesLocauxPubliques ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
        , hr [ class "fr-hr" ] []
        , div []
            [ Lib.UI.infoLine Nothing "Numéro d'invariant" <|
                withEmptyAs "-" <|
                    .invariant <|
                        local
            , Lib.UI.infoLine Nothing "Section cadastrale" <|
                withEmptyAs "-" <|
                    .section <|
                        local
            , Lib.UI.infoLine Nothing "Numéro de parcelle" <|
                withEmptyAs "-" <|
                    .parcelle <|
                        local
            , Lib.UI.infoLine Nothing "Type" <|
                "Local professionnel"
            , Lib.UI.infoLine Nothing "Nature" <|
                withEmptyAs "-" <|
                    capitalizeName <|
                        .nom <|
                            .nature <|
                                local
            , Lib.UI.infoLine Nothing "Catégorie" <|
                withEmptyAs "-" <|
                    Maybe.withDefault "" <|
                        Maybe.map (\c -> c.nom ++ " (" ++ c.id ++ ")") <|
                            .categorie <|
                                local
            , Lib.UI.infoLine Nothing "Étage" <|
                withEmptyAs "-" <|
                    Regex.replace etageRegex (\_ -> "") <|
                        .niveau <|
                            local
            , Lib.UI.infoLine Nothing "Surface totale (P1 + P2 + P3)" <|
                withEmptyAs "-" <|
                    (\surface -> surface ++ " m²") <|
                        String.fromInt <|
                            (\l -> l.surfaceVente + l.surfaceReserve + l.surfaceExterieureNonCouverte) <|
                                local
            , Lib.UI.infoLine Nothing "Surface de vente (P1)" <|
                withEmptyAs "-" <|
                    (\surface -> surface ++ " m²") <|
                        String.fromInt <|
                            .surfaceVente <|
                                local
            , Lib.UI.infoLine Nothing "Surface de réserve (P2)" <|
                withEmptyAs "-" <|
                    (\surface -> surface ++ " m²") <|
                        String.fromInt <|
                            .surfaceReserve <|
                                local
            , Lib.UI.infoLine Nothing "Surface extérieure non couverte (P3)" <|
                withEmptyAs "-" <|
                    (\surface -> surface ++ " m²") <|
                        String.fromInt <|
                            .surfaceExterieureNonCouverte <|
                                local
            , Lib.UI.infoLine Nothing "Surface des espaces de stationnement couverts (PK1)" <|
                withEmptyAs "-" <|
                    (\surface -> surface ++ " m²") <|
                        String.fromInt <|
                            .surfaceStationnementCouverte <|
                                local
            , Lib.UI.infoLine Nothing "Surface des espaces de stationnement non couverts (PK2)" <|
                withEmptyAs "-" <|
                    (\surface -> surface ++ " m²") <|
                        String.fromInt <|
                            .surfaceStationnementNonCouverte <|
                                local
            ]
        ]


viewProprietaires : Int -> WebData LocalWithExtraInfo -> Local -> Html Msg
viewProprietaires prochainId contactCreation local =
    let
        totalProprietaires =
            List.length local.proprietairesPersonnesMorales + List.length local.proprietairesPersonnesPhysiques
    in
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12, class "flex flex-col gap-4" ]
            [ h3 [ Typo.fr_h4, class "flex flex-row items-center gap-2 !mb-0" ]
                [ span []
                    [ text "Propriétaire"
                    , text <| plural totalProprietaires
                    ]
                , viewIf (totalProprietaires > 0) <|
                    span [ Typo.textMd, class "circled !mb-0" ]
                        [ text <| Lib.UI.formatIntWithThousandSpacing <| totalProprietaires
                        ]
                ]
            , if totalProprietaires == 0 then
                div [ class "flex flex-col gap-4" ] <|
                    [ span [ class "text-center italic" ] [ text "Aucun propriétaire enregistré" ] ]

              else
                div [ class "flex flex-col gap-4" ] <|
                    [ div [ class "flex flex-col gap-4" ] <|
                        List.map (viewProprietairePersonnePhysique prochainId contactCreation) <|
                            .proprietairesPersonnesPhysiques <|
                                local
                    , div [ class "flex flex-col gap-4" ] <|
                        List.map viewProprietairePersonneMorale <|
                            .proprietairesPersonnesMorales <|
                                local
                    ]
            ]
        ]


viewOccupants : Local -> Html Msg
viewOccupants local =
    let
        totalOccupants =
            List.length local.occupants
    in
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12, class "flex flex-col gap-4" ]
            [ div [ class "flex flex-row justify-between" ]
                [ h3 [ Typo.fr_h4, class "flex flex-row items-center gap-2 !mb-0" ]
                    [ span []
                        [ text "Établissement"
                        , text <| plural totalOccupants
                        , text " occupant"
                        ]
                    , text <| plural totalOccupants
                    , viewIf (totalOccupants > 0) <|
                        span [ Typo.textMd, class "circled !mb-0" ]
                            [ text <| Lib.UI.formatIntWithThousandSpacing <| totalOccupants
                            ]
                    ]
                , DSFR.Button.new
                    { label = "Ajouter un établissement occupant"
                    , onClick = Just <| AddEtablissementOccupant
                    }
                    |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                    |> DSFR.Button.tertiaryNoOutline
                    |> DSFR.Button.view
                ]
            , if totalOccupants == 0 then
                div [ class "flex flex-col gap-4" ] <|
                    [ span [ class "text-center italic" ] [ text "Aucun établissement occupant" ] ]

              else
                div [ class "flex flex-col gap-4" ] <|
                    List.map viewEtablissementOccupant <|
                        .occupants <|
                            local
            ]
        ]


viewMutations : Local -> Html Msg
viewMutations local =
    let
        totalMutations =
            List.length local.mutations
    in
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12, class "flex flex-col gap-4" ]
            [ div [ class "flex flex-row justify-between" ]
                [ h3 [ Typo.fr_h4, class "flex flex-row items-center gap-2 !mb-0" ]
                    [ span []
                        [ text "Mutation"
                        , text <| plural totalMutations
                        , text " du local"
                        ]
                    , viewIf (totalMutations > 0) <|
                        span [ Typo.textMd, class "circled !mb-0" ]
                            [ text <| Lib.UI.formatIntWithThousandSpacing <| totalMutations
                            ]
                    ]
                ]
            , div [ class "flex flex-col gap-4" ] <|
                List.singleton <|
                    if totalMutations == 0 then
                        span [ class "text-center italic" ] [ text "Aucune mutation disponible pour ce local" ]

                    else
                        DSFR.Table.table
                            { id = "tableau-mutations"
                            , caption = text "Mutations du local"
                            , headers =
                                [ MHDate
                                , MHNature
                                , MHPrix
                                ]
                            , rows = local.mutations
                            , toHeader = mutationHeaderToDisplay >> text >> List.singleton >> Html.span []
                            , toRowId = .date >> Lib.Date.formatDateShort
                            , toCell = mutationToCell
                            }
                            |> DSFR.Table.withThAttrs
                                (\header ->
                                    case header of
                                        MHPrix ->
                                            [ class "!text-right" ]

                                        _ ->
                                            []
                                )
                            |> DSFR.Table.withContainerAttrs [ class "!mb-0" ]
                            |> DSFR.Table.noBorders
                            |> DSFR.Table.captionHidden
                            |> DSFR.Table.fixed
                            |> DSFR.Table.view
            ]
        ]


viewEtablissementSuggestion : Maybe EtablissementApercu -> EtablissementApercu -> Html Msg
viewEtablissementSuggestion etablissementActuel etablissementOccupant =
    let
        selectionne =
            etablissementActuel
                |> Maybe.map (.siret >> (==) etablissementOccupant.siret)
                |> Maybe.withDefault False
    in
    Html.div
        [ Events.onClick <|
            SelectedEtablissementOccupant <|
                if selectionne then
                    Nothing

                else
                    Just etablissementOccupant
        , class "flex flex-col gap-4 p-4 border-2 cursor-pointer"
        , Html.Attributes.classList
            [ ( "dark-grey-border", not selectionne )
            , ( "dark-blue-border", selectionne )
            ]
        ]
        [ div [ class "flex flex-row justify-between" ]
            [ div [ Typo.textXs, class "!mb-0" ]
                [ DSFR.Icons.iconSM DSFR.Icons.Buildings.communityLine
                , text " "
                , text "Établissement"
                ]
            , span
                [ Html.Attributes.classList
                    [ ( "disabled-text", not selectionne )
                    , ( "blue-text", selectionne )
                    ]
                ]
                [ DSFR.Icons.iconMD DSFR.Icons.System.checkboxCircleLine
                ]
            ]
        , div [ Typo.textBold, class "flex flex-row gap-4 items-center" ]
            [ h4 [ Typo.fr_h5, class "!mb-0" ]
                [ text <|
                    etablissementOccupant.nomAffichage
                ]
            , div [ class "flex flex-row gap-2" ]
                [ UI.Entite.badgeInactif (not <| Nothing == etablissementOccupant.clotureDateContribution) <|
                    Just <|
                        etablissementOccupant.etatAdministratif
                , UI.Entite.badgeSiege <|
                    etablissementOccupant.siege
                , text "\u{00A0}"
                ]
            ]
        , div []
            [ text "Adresse\u{00A0}: "
            , span [ Typo.textBold ]
                [ text <|
                    withEmptyAs "-" <|
                        etablissementOccupant.adresse
                ]
            ]
        , div []
            [ text "SIRET\u{00A0}: "
            , span [ Typo.textBold ]
                [ text <|
                    withEmptyAs "-" <|
                        etablissementOccupant.siret
                ]
            ]
        ]


viewEtablissementOccupant : EtablissementApercu -> Html Msg
viewEtablissementOccupant etablissementOccupant =
    div [ class "flex flex-col gap-4 p-4 border-2 dark-grey-border" ]
        [ div [ Typo.textXs, class "!mb-0" ]
            [ DSFR.Icons.iconSM DSFR.Icons.Buildings.communityLine
            , text " "
            , text "Établissement"
            ]
        , div [ Typo.textBold, class "flex flex-row gap-4 items-center" ]
            [ h4 [ Typo.fr_h5, class "!mb-0" ]
                [ text <|
                    etablissementOccupant.nomAffichage
                ]
            , div [ class "flex flex-row gap-2" ]
                [ UI.Entite.badgeInactif (not <| Nothing == etablissementOccupant.clotureDateContribution) <|
                    Just <|
                        etablissementOccupant.etatAdministratif
                , UI.Entite.badgeSiege <|
                    etablissementOccupant.siege
                , text "\u{00A0}"
                ]
            ]
        , div []
            [ text "SIRET\u{00A0}: "
            , span [ Typo.textBold ]
                [ text <|
                    withEmptyAs "-" <|
                        etablissementOccupant.siret
                ]
            ]
        , [ DSFR.Button.new
                { label = "Voir l'établissement"
                , onClick = Nothing
                }
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Etablissement ( Nothing, etablissementOccupant.siret ))
                |> DSFR.Button.leftIcon DSFR.Icons.Editor.link
                |> DSFR.Button.secondary
          , DSFR.Button.new
                { label = "Supprimer"
                , onClick = Just <| ClickedDeleteEtablissementOccupant etablissementOccupant
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.deleteLine
                |> DSFR.Button.tertiary
          ]
            |> DSFR.Button.group
            |> DSFR.Button.iconsLeft
            |> DSFR.Button.inline
            |> DSFR.Button.viewGroup
            |> List.singleton
            |> div [ class "shrink" ]
        ]


viewProprietairePersonnePhysique : Int -> WebData LocalWithExtraInfo -> ProprietairePersonnePhysique -> Html Msg
viewProprietairePersonnePhysique prochainId contactCreation proprietaire =
    div [ class "flex flex-col gap-4 p-4 border-2 dark-grey-border" ]
        [ div [ Typo.textXs, class "!mb-0" ]
            [ DSFR.Icons.iconSM DSFR.Icons.User.userLine
            , text " "
            , text "Personne physique"
            ]
        , div [ Typo.textBold ]
            [ h4 [ Typo.fr_h5, class "!mb-0" ]
                [ text <|
                    Data.Personne.displayPrenomNomPersonne <|
                        { nom = proprietaire.nom |> Maybe.withDefault "?", prenom = proprietaire.prenom |> Maybe.withDefault "?" }
                ]
            ]
        , div []
            [ div []
                [ text "Prénom(s)\u{00A0}: "
                , span [ Typo.textBold ]
                    [ text <|
                        withEmptyAs "-" <|
                            case ( proprietaire.prenom, proprietaire.prenoms ) of
                                ( Nothing, Nothing ) ->
                                    ""

                                ( Just p, Nothing ) ->
                                    p

                                ( Nothing, Just ps ) ->
                                    ps

                                ( Just "", Just ps ) ->
                                    ps

                                ( Just p, Just "" ) ->
                                    p

                                ( Just p, Just ps ) ->
                                    [ p, ps ] |> String.join ", "
                    ]
                ]
            , div []
                [ text "Nom\u{00A0}: "
                , span [ Typo.textBold ]
                    [ text <|
                        withEmptyAs "-" <|
                            Maybe.withDefault "" <|
                                proprietaire.nom
                    ]
                ]
            , div []
                [ text "Nom de naissance\u{00A0}: "
                , span [ Typo.textBold ]
                    [ text <|
                        withEmptyAs "-" <|
                            Maybe.withDefault "" <|
                                proprietaire.naissanceNom
                    ]
                ]
            , div []
                [ text "Date de naissance\u{00A0}: "
                , span [ Typo.textBold ]
                    [ text <|
                        withEmptyAs "-" <|
                            Maybe.withDefault "" <|
                                Maybe.map Lib.Date.formatDateShort <|
                                    proprietaire.naissanceDate
                    ]
                ]
            ]
        , DSFR.Button.new
            { label = "Ajouter en contact"
            , onClick =
                Just <|
                    SetContactAction <|
                        NewContact <|
                            newContactFormPourExistant
                                prochainId
                                { fonction = Just <| FonctionLocal { fonction = "Propriétaire" }
                                , prenom =
                                    case ( proprietaire.prenom, proprietaire.prenoms ) of
                                        ( Nothing, Nothing ) ->
                                            ""

                                        ( Just p, Nothing ) ->
                                            p

                                        ( Nothing, Just ps ) ->
                                            ps

                                        ( Just p, Just ps ) ->
                                            if String.contains p ps then
                                                ps

                                            else
                                                p ++ "(" ++ ps ++ ")"
                                , nom =
                                    case ( proprietaire.nom, proprietaire.naissanceNom ) of
                                        ( Nothing, Nothing ) ->
                                            ""

                                        ( Nothing, Just n ) ->
                                            n

                                        ( Just n, Nothing ) ->
                                            n

                                        ( Just n, Just nn ) ->
                                            if n == nn then
                                                n

                                            else
                                                n ++ " (" ++ nn ++ ")"
                                , dateDeNaissance = proprietaire.naissanceDate
                                }
            }
            |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
            |> DSFR.Button.secondary
            |> DSFR.Button.withDisabled (contactCreation == RD.Loading)
            |> DSFR.Button.view
        ]


viewProprietairePersonneMorale : ProprietairePersonneMorale -> Html msg
viewProprietairePersonneMorale proprietaire =
    div [ class "flex flex-col gap-4 p-4 border-2 dark-grey-border" ]
        [ div [ Typo.textXs, class "!mb-0" ]
            [ DSFR.Icons.iconSM DSFR.Icons.Buildings.communityLine
            , text " "
            , text "Personne morale"
            ]
        , div [ Typo.textBold ]
            [ h4 [ Typo.fr_h5, class "!mb-0" ]
                [ text <|
                    proprietaire.nom
                ]
            ]
        , div []
            [ div []
                [ text "SIREN\u{00A0}: "
                , span [ Typo.textBold ]
                    [ text <|
                        withEmptyAs "-" <|
                            proprietaire.siren
                    ]
                ]
            ]
        , DSFR.Button.new
            { label = "Voir l'établissement siège"
            , onClick = Nothing
            }
            |> (proprietaire.siretSiege
                    |> Maybe.map
                        (Tuple.pair Nothing
                            >> Route.Etablissement
                            >> Route.toUrl
                            >> DSFR.Button.linkButton
                        )
                    |> Maybe.withDefault
                        (DSFR.Button.withDisabled True
                            >> DSFR.Button.withAttrs [ Html.Attributes.title "Aucun établissement siège trouvé" ]
                        )
               )
            |> DSFR.Button.leftIcon DSFR.Icons.Editor.link
            |> DSFR.Button.secondary
            |> DSFR.Button.view
        ]


confirmVacanceRequest : String -> Maybe Bool -> Cmd Msg
confirmVacanceRequest id vacance =
    post
        { url = Api.updateContributionVacance id
        , body =
            [ ( "contribution"
              , Encode.object <|
                    [ ( "occupe"
                      , Maybe.withDefault Encode.null <|
                            Maybe.map Encode.bool <|
                                vacance
                      )
                    ]
              )
            ]
                |> Encode.object
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedVacanceResponse
        }
        (Decode.field "local" decodeLocalWithExtraInfo)


confirmNomRequest : String -> String -> Cmd Msg
confirmNomRequest id nom =
    post
        { url = Api.updateContributionNom id
        , body =
            [ ( "contribution"
              , Encode.object <|
                    [ ( "nom", Encode.string nom )
                    ]
              )
            ]
                |> Encode.object
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedNomResponse
        }
        (Decode.field "local" decodeLocalWithExtraInfo)


confirmContributionRequest : String -> Contribution -> Cmd Msg
confirmContributionRequest id contribution =
    post
        { url = Api.updateContributionDonnees id
        , body =
            [ ( "contribution"
              , Encode.object <|
                    [ ( "loyerEuros"
                      , Maybe.withDefault Encode.null <|
                            Maybe.map Encode.int <|
                                String.toInt <|
                                    .loyer <|
                                        contribution
                      )
                    , ( "bailTypeId"
                      , Maybe.withDefault Encode.null <|
                            Maybe.map Encode.string <|
                                Maybe.map bailTypeToString <|
                                    .bailType <|
                                        contribution
                      )
                    , ( "vacanceTypeId"
                      , Maybe.withDefault Encode.null <|
                            Maybe.map Encode.string <|
                                Maybe.map vacanceTypeToString <|
                                    .vacanceType <|
                                        contribution
                      )
                    , ( "vacanceMotifTypeId"
                      , Maybe.withDefault Encode.null <|
                            Maybe.map Encode.string <|
                                Maybe.map vacanceMotifToString <|
                                    .vacanceMotif <|
                                        contribution
                      )
                    , ( "remembre"
                      , Encode.bool <|
                            .remembre <|
                                contribution
                      )
                    , ( "fondsEuros"
                      , Maybe.withDefault Encode.null <|
                            Maybe.map Encode.int <|
                                String.toInt <|
                                    .fonds <|
                                        contribution
                      )
                    , ( "venteEuros"
                      , Maybe.withDefault Encode.null <|
                            Maybe.map Encode.int <|
                                String.toInt <|
                                    .vente <|
                                        contribution
                      )
                    , ( "description"
                      , (\c ->
                            if c == "" then
                                Encode.null

                            else
                                Encode.string c
                        )
                        <|
                            .commentaires <|
                                contribution
                      )
                    ]
              )
            ]
                |> Encode.object
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedContributionResponse
        }
        (Decode.field "local" decodeLocalWithExtraInfo)


updateContribution : ContributionField -> String -> Contribution -> Contribution
updateContribution field value contribution =
    case field of
        CLoyer ->
            { contribution | loyer = value }

        CBailType ->
            { contribution
                | bailType =
                    value
                        |> Data.Local.stringToBailType
            }

        CVacanceType ->
            { contribution
                | vacanceType =
                    value
                        |> Data.Local.stringToVacanceType
            }

        CVacanceMotif ->
            { contribution
                | vacanceMotif =
                    value
                        |> Data.Local.stringToVacanceMotif
            }

        CRemembre ->
            { contribution | remembre = value == "oui" }

        CFonds ->
            { contribution | fonds = value }

        CVente ->
            { contribution | vente = value }

        CCommentaires ->
            { contribution | commentaires = value }


viewContactsModal : String -> WebData LocalWithExtraInfo -> ContactAction -> Html Msg
viewContactsModal localId saveContactRequest contactAction =
    let
        ( opened, content, title ) =
            case contactAction of
                None ->
                    ( False, nothing, nothing )

                ViewContactFonctions fonctions ->
                    let
                        autresFonctions =
                            fonctions
                                |> List.filter
                                    (\f ->
                                        case f of
                                            CLocal ld ->
                                                ld.localId /= localId

                                            _ ->
                                                True
                                    )
                    in
                    ( True, UI.Contact.viewFonctions SetContactAction autresFonctions, text "Autres fonctions du contact" )

                NewContact contactForm ->
                    ( True
                    , viewNewContactFormFields
                        { confirm = ConfirmContactAction
                        , update = UpdatedContact
                        , cancel = CancelContactAction
                        , toggle = ToggleNouveauContactModeCreation
                        , confirmSearch = RechercheContactExistant
                        , updateSearch = UpdatedRechercheContactExistant
                        , selectedPersonne = SelectedContactExistant
                        , niveauxDiplome = Nothing
                        , situationsProfessionnelles = Nothing
                        }
                        saveContactRequest
                        contactForm
                    , text "Ajouter un contact"
                    )

                EditContact contactExistant contact ->
                    ( True
                    , viewEditContact
                        { confirm = ConfirmContactAction
                        , update = UpdatedContact
                        , cancel = CancelContactAction
                        , niveauxDiplome = Nothing
                        , situationsProfessionnelles = Nothing
                        }
                        saveContactRequest
                        contactExistant
                        contact
                    , text "Modifier un contact"
                    )

                DeleteContact contact autresFonctions ->
                    ( True
                    , viewDeleteContactBody
                        autresFonctions
                        { confirm = ConfirmContactAction
                        , cancel = CancelContactAction
                        }
                        saveContactRequest
                        contact.personne.id
                    , text "Supprimer un contact"
                    )

                DeleteContactForm _ ->
                    ( False, nothing, nothing )

                EditContactForm _ _ ->
                    ( False, nothing, nothing )
    in
    DSFR.Modal.view
        { id = "contact"
        , label = "contact"
        , openMsg = NoOp
        , closeMsg = Just CancelContactAction
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.md
        }
        content
        Nothing
        |> Tuple.first


viewAddEtablissementOccupantModal : LocalWithExtraInfo -> Maybe NewEtablissementOccupant -> Html Msg
viewAddEtablissementOccupantModal localWithExtraInfo newEtablissementOccupant =
    let
        ( opened, content, title ) =
            case newEtablissementOccupant of
                Nothing ->
                    ( False, nothing, nothing )

                Just prop ->
                    ( True
                    , div [ class "flex flex-col gap-4" ]
                        [ viewSiretForm prop
                        , case prop.request of
                            RD.Failure _ ->
                                DSFR.Alert.small { title = Just "Erreur", description = text "Une erreur s'est produite, veuillez réessayer." }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.error

                            RD.Loading ->
                                DSFR.Alert.small { title = Nothing, description = text "Ajout de l'établissement occupant en cours..." }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.info

                            RD.Success _ ->
                                nothing

                            _ ->
                                nothing
                        , [ DSFR.Button.new { label = "Ajouter", onClick = Just ConfirmedAddEtablissementOccupant }
                                |> DSFR.Button.withDisabled (RD.Loading == prop.request || (not <| isValid <| prop))
                          , DSFR.Button.new { label = "Annuler", onClick = Just CanceledAddEtablissementOccupant }
                                |> DSFR.Button.secondary
                          ]
                            |> DSFR.Button.group
                            |> DSFR.Button.inline
                            |> DSFR.Button.alignedRightInverted
                            |> DSFR.Button.viewGroup
                        ]
                    , text <| "Ajouter un établissement occupant au local " ++ localWithExtraInfo.local.nom
                    )
    in
    DSFR.Modal.view
        { id = "ajout-etablissement-occupant"
        , label = "ajout-etablissement-occupant"
        , openMsg = NoOp
        , closeMsg = Just CanceledAddEtablissementOccupant
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


viewDeleteEtablissementOccupantModal : LocalWithExtraInfo -> Maybe { etablissementOccupant : EtablissementApercu, request : WebData LocalWithExtraInfo } -> Html Msg
viewDeleteEtablissementOccupantModal localWithExtraInfo deleteEtablissementOccupant =
    let
        ( opened, content, title ) =
            case deleteEtablissementOccupant of
                Nothing ->
                    ( False, nothing, nothing )

                Just delProp ->
                    ( True
                    , div []
                        [ text "Êtes-vous sûr(e) de vouloir retirer cet établissement du local\u{00A0}?"
                        , [ DSFR.Button.new { label = "OK", onClick = Just ConfirmedDeleteEtablissementOccupant }
                                |> DSFR.Button.withDisabled (RD.Loading == delProp.request)
                          , DSFR.Button.new { label = "Annuler", onClick = Just CanceledDeleteEtablissementOccupant }
                                |> DSFR.Button.secondary
                          ]
                            |> DSFR.Button.group
                            |> DSFR.Button.inline
                            |> DSFR.Button.alignedRightInverted
                            |> DSFR.Button.viewGroup
                        ]
                    , text <| "Supprimer un établissement occupant du local " ++ localWithExtraInfo.local.nom
                    )
    in
    DSFR.Modal.view
        { id = "suppression-etablissement-occupant"
        , label = "suppression-etablissement-occupant"
        , openMsg = NoOp
        , closeMsg = Just CanceledDeleteEtablissementOccupant
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


viewSiretForm : NewEtablissementOccupant -> Html Msg
viewSiretForm { siretRecherche, siretResultat, suggestions, siretChoix } =
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col gap-2" ]
            [ DSFR.SearchBar.searchBar
                { errors = []
                , extraAttrs =
                    [ Html.Attributes.pattern "(\\s*[0-9]\\s*){14}"
                    , Html.Attributes.title "Un SIRET (14 chiffres)"
                    ]
                , disabled = String.trim siretRecherche == ""
                }
                { submitMsg = ConfirmedSiret
                , buttonLabel = "Rechercher"
                , inputMsg = UpdatedSiret
                , inputLabel = "Numéro SIRET"
                , inputPlaceholder = Nothing
                , inputId = "proprietaire-recherche-siret"
                , inputValue = siretRecherche
                , hints = [ text "Le numéro SIRET comprend 14 chiffres" ]
                , fullLabel = Just <| text <| "Numéro SIRET"
                }
            , case siretResultat of
                RD.Failure _ ->
                    div [ class "flex flex-col p-4 gap-4" ]
                        [ div [] [ text "Une erreur s'est produite. Si le problème persiste, veuillez nous contacter." ]
                        ]

                RD.Loading ->
                    div [ class "flex flex-col p-4 gap-4" ]
                        [ div [] [ text "Récupération en cours..." ]
                        ]

                RD.Success (Resultat e) ->
                    div [ class "flex flex-col gap-4 !mt-4" ]
                        [ div [] <|
                            List.singleton <|
                                text "Résultat\u{00A0}:"
                        , viewEtablissementSuggestion siretChoix e
                        ]

                RD.Success (Erreur erreur) ->
                    div [ class "flex flex-col p-4 gap-4" ]
                        [ div []
                            [ text erreur
                            ]
                        ]

                _ ->
                    nothing
            ]
        , div [ class "flex flex-col gap-4" ]
            [ div []
                [ div [ class "italic" ] [ text "Suggestions d'établissements en fonction de l'adresse" ]
                , viewIf (suggestions |> RD.map (Tuple.second >> .results >> (<=) 5) |> RD.withDefault False)
                    (text "(seuls les 5 premiers résultats sont affichés)")
                ]
            , div [ class "flex flex-col gap-2" ] <|
                case suggestions of
                    RD.Success ( [], _ ) ->
                        [ text "Aucune suggestion disponible." ]

                    RD.Success list ->
                        List.map (viewEtablissementSuggestion siretChoix) <|
                            Tuple.first <|
                                list

                    RD.Loading ->
                        [ text "Récupération des suggestions..." ]

                    RD.Failure _ ->
                        [ text "Une erreur s'est produite, veuillez nous contacter si le problème persiste." ]

                    RD.NotAsked ->
                        [ text "Une erreur s'est produite, veuillez nous contacter si le problème persiste." ]
            ]
        ]


isValid : NewEtablissementOccupant -> Bool
isValid prop =
    prop.siretChoix /= Nothing


searchEtablissement : WebData LocalWithExtraInfo -> Maybe NewEtablissementOccupant -> Cmd Msg
searchEtablissement localWithExtraInfo newProprietaire =
    case ( localWithExtraInfo, newProprietaire ) of
        ( RD.Success _, Just newP ) ->
            get
                { url =
                    Api.getEtablissementParSiret newP.siretRecherche
                }
                { toShared = SharedMsg
                , logger = Just LogError
                , handler = ReceivedEtablissementResults
                }
                (Decode.oneOf
                    [ Decode.map Resultat <|
                        Decode.field "etablissement" <|
                            decodeEtablissementApercu
                    , Decode.map Erreur <|
                        Decode.field "error" <|
                            Decode.string
                    ]
                )

        _ ->
            Cmd.none


saveEtablissementOccupant : LocalWithExtraInfo -> NewEtablissementOccupant -> Cmd Msg
saveEtablissementOccupant localWithExtraInfo etablissementOccupant =
    let
        payload =
            Http.jsonBody <|
                Encode.object <|
                    [ ( "siret"
                      , etablissementOccupant.siretChoix
                            |> Maybe.map .siret
                            |> Maybe.map Encode.string
                            |> Maybe.withDefault Encode.null
                      )
                    ]
    in
    post
        { url = Api.addLocalEtablissementOccupant localWithExtraInfo.local.id
        , body = payload
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedAddEtablissementOccupant
        }
        (Decode.field "local" decodeLocalWithExtraInfo)


createRessource : String -> Ressource -> Cmd Msg
createRessource localId ressource =
    post
        { url = Api.createRessourceLocal localId
        , body =
            ressource
                |> encodeRessource
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveRessource
        }
        (Decode.field "local" decodeLocalWithExtraInfo)


deleteRessource : String -> Ressource -> Cmd Msg
deleteRessource localId ressource =
    delete
        { url = Api.editRessourceLocal localId ressource.id
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveRessource
        }
        (Decode.field "local" decodeLocalWithExtraInfo)


editRessource : String -> Ressource -> Cmd Msg
editRessource localId ressource =
    post
        { url = Api.editRessourceLocal localId ressource.id
        , body =
            ressource
                |> encodeRessource
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveRessource
        }
        (Decode.field "local" decodeLocalWithExtraInfo)


getPersonnes : String -> Cmd Msg
getPersonnes query =
    get
        { url = Api.getContacts query }
        { toShared = SharedMsg
        , logger = Nothing
        , handler = ReceivedContactsExistants
        }
        (Decode.field "elements" <| Decode.list <| decodePersonne)


savePartage : String -> Partage -> Cmd Msg
savePartage localId { collegueId, commentaire } =
    post
        { url = Api.partageLocal localId
        , body =
            [ ( "compteId", Maybe.withDefault Encode.null <| Maybe.map Api.EntityId.encodeEntityId <| collegueId )
            , ( "commentaire", Encode.string commentaire )
            ]
                |> Encode.object
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedPartage
        }
        (Decode.succeed ())


queryKeys :
    { onglet : String
    , suivi : String
    }
queryKeys =
    { onglet = "onglet"
    , suivi = "suivi"
    }


queryToOnglets : Maybe String -> ( Onglet, UI.Suivi.Onglet )
queryToOnglets rawQuery =
    rawQuery
        |> Maybe.withDefault ""
        |> QS.parse QS.config
        |> (\query ->
                let
                    onglet =
                        query
                            |> QS.getAsStringList queryKeys.onglet
                            |> List.head
                            |> Maybe.andThen stringToOnglet
                            |> Maybe.withDefault OngletLocal

                    ongletSuivi =
                        query
                            |> QS.getAsStringList queryKeys.suivi
                            |> List.head
                            |> Maybe.andThen UI.Suivi.stringToOnglet
                            |> Maybe.withDefault UI.Suivi.OngletEchanges
                in
                ( onglet, ongletSuivi )
           )


ongletsToQuery : ( Onglet, UI.Suivi.Onglet ) -> String
ongletsToQuery ( onglet, ongletSuivi ) =
    let
        setOnglet =
            case onglet of
                OngletLocal ->
                    identity

                _ ->
                    onglet
                        |> ongletToString
                        |> QS.setStr queryKeys.onglet

        setOngletSuivi =
            case onglet of
                OngletSuivi ->
                    ongletSuivi
                        |> UI.Suivi.ongletToString
                        |> QS.setStr queryKeys.suivi

                _ ->
                    identity
    in
    QS.empty
        |> setOnglet
        |> setOngletSuivi
        |> QS.serialize (QS.config |> QS.addQuestionMark False)


updateUrl : ( Onglet, UI.Suivi.Onglet ) -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
updateUrl ( onglet, ongletSuivi ) model =
    let
        ( newSuiviModel, suiviCmd, _ ) =
            UI.Suivi.changeOnglet (UI.Suivi.suiviConfigLocal (Decode.field "local" decodeLocalWithExtraInfo) model.localId) model.suiviModel ongletSuivi

        newOngletSuivi =
            UI.Suivi.getOngletActif newSuiviModel

        nextUrl =
            ongletsToQuery ( onglet, newOngletSuivi )

        requestChanged =
            nextUrl /= model.currentUrl

        ongletCmd =
            Effect.fromShared <|
                Shared.Navigate <|
                    Route.Local <|
                        ( (\q ->
                            if q == "" then
                                Nothing

                            else
                                Just q
                          )
                          <|
                            nextUrl
                        , model.localId
                        )
    in
    if requestChanged then
        ( { model
            | onglet = onglet
            , suiviModel = newSuiviModel
            , currentUrl = nextUrl
          }
        , Effect.batch
            [ ongletCmd
            , Effect.map SuiviMsg suiviCmd
            ]
        )

    else
        model
            |> Effect.withNone


selectLocalisationsConfig : MultiSelectRemote.SelectConfig String
selectLocalisationsConfig =
    { headers = []
    , url = Api.rechercheLocalisation
    , optionDecoder = decodeEtiquetteList |> Decode.map (List.sortBy String.toLower)
    }


selectMotsConfig : MultiSelectRemote.SelectConfig String
selectMotsConfig =
    { headers = []
    , url = Api.rechercheMot
    , optionDecoder = decodeEtiquetteList |> Decode.map (List.sortBy String.toLower)
    }


updateLocalEtiquettes : String -> LocalEtiquettes -> Effect.Effect Shared.Msg Msg
updateLocalEtiquettes siret { localisations, mots } =
    let
        jsonBody =
            [ ( "localisations", Encode.list Encode.string localisations )
            , ( "motsCles", Encode.list Encode.string mots )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    post
        { url = Api.updateLocalEtiquettes siret
        , body = jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveLocalEtiquettes
        }
        (Decode.field "local" decodeLocalWithExtraInfo)
        |> Effect.fromCmd


getZonages : String -> Effect.Effect Shared.Msg Msg
getZonages localId =
    Effect.fromCmd <|
        get
            { url = Api.getLocalZonages localId
            }
            { toShared = SharedMsg
            , logger = Just LogError
            , handler = ReceivedZonages
            }
            (Decode.list <|
                decodeZonage
            )


requestToggleFavorite : String -> Bool -> Cmd Msg
requestToggleFavorite siret favorite =
    post
        { url = Api.toggleFavoriteLocal siret
        , body =
            [ ( "favori", Encode.bool favorite )
            ]
                |> Encode.object
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler =
            \webdata ->
                GotFavoriteResult <|
                    case webdata of
                        RD.Success _ ->
                            favorite

                        _ ->
                            not favorite
        }
        (Decode.succeed ())


requestGeolocalisationContributionSave : Maybe GeolocalisationContribution -> WebData LocalWithExtraInfo -> Cmd Msg
requestGeolocalisationContributionSave geolocalisationContribution localWithExtraInfo =
    case ( localWithExtraInfo, geolocalisationContribution ) of
        ( RD.Success { local }, Just { nouvelleGeolocalisation } ) ->
            post
                { url = Api.updateLocalGeolocalisationContribution local.id
                , body =
                    Encode.object
                        [ ( "geolocalisation"
                          , nouvelleGeolocalisation
                                |> Maybe.map (\( lon, lat ) -> [ lon, lat ])
                                |> Maybe.map (Encode.list Encode.float)
                                |> Maybe.withDefault Encode.null
                          )
                        ]
                        |> Http.jsonBody
                }
                { toShared = SharedMsg
                , logger = Just LogError
                , handler = ReceivedGeolocalisationContributionSave
                }
                (Decode.field "local" decodeLocalWithExtraInfo)

        _ ->
            Cmd.none


stringtoPoint : String -> Maybe ( Float, Float )
stringtoPoint string =
    string
        |> String.split ","
        |> List.map String.trim
        |> List.map String.toFloat
        |> (\list ->
                case list of
                    [ Just lon, Just lat ] ->
                        let
                            isLonValid =
                                lon >= -180 && lon <= 180

                            isLatValid =
                                lat >= -90 && lat <= 90
                        in
                        if isLonValid && isLatValid then
                            Just ( lon, lat )

                        else
                            Nothing

                    _ ->
                        Nothing
           )


pointToString : ( Float, Float ) -> String
pointToString ( lon, lat ) =
    [ String.fromFloat lon
    , String.fromFloat lat
    ]
        |> String.join ", "


centreDeLaFrance : ( Float, Float )
centreDeLaFrance =
    ( 2.8906249999922693, 46.81809179606458 )


encodePoint : ( Float, Float ) -> Encode.Value
encodePoint ( lon, lat ) =
    [ lon, lat ]
        |> Encode.list Encode.float


selectAdresseConfig : SingleSelectRemote.SelectConfig ApiAdresse
selectAdresseConfig =
    { headers = []
    , url = Api.rechercheAdresse Nothing Nothing Nothing
    , optionDecoder = decodeApiFeatures
    }
