module Pages.EtablissementCreation.New exposing (Model, Msg, page)

import Accessibility exposing (Html, div, formWithListeners, h1, text)
import Api
import Api.Auth exposing (get, post)
import Api.EntityId exposing (EntityId, decodeEntityId)
import Browser.Dom as Dom
import DSFR.Alert
import DSFR.Button
import DSFR.Modal
import Data.Commune exposing (Commune)
import Data.EtablissementCreationId exposing (EtablissementCreationId)
import Data.Etiquette exposing (decodeEtiquetteList)
import Data.GenericSource exposing (GenericSource)
import Data.Personne exposing (Personne, decodePersonne)
import Dict
import Effect
import Html.Attributes exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Html.Lazy as Lazy
import Http
import Json.Decode as Decode
import MultiSelectRemote
import Pages.Annuaire exposing (defaultFilters)
import Pages.Etablissements.Filters as Filters
import RemoteData as RD exposing (WebData)
import Route
import Shared
import SingleSelectRemote exposing (SelectConfig)
import Spa.Page exposing (Page)
import Task
import UI.Contact exposing (ContactAction(..), ContactField, updateContactForm, viewDeleteContactFormBody, viewNewContactFormFields)
import UI.EtablissementCreation exposing (DataEtablissementCreation, FormErrors, ProjetField, Sources, createursEtablissementBloc, decodeSources, defaultDataEtablissementCreation, encodeDataEtablissementCreation, informationsProjetBloc, premierContactBloc, updateProjet, updateProjetField)
import UI.Layout
import UI.Pagination exposing (defaultPagination)
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page () Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.element <|
        { init = init
        , update = update
        , view = view
        , subscriptions =
            \model ->
                [ model.selectActivites |> MultiSelectRemote.subscriptions
                , model.selectLocalisations |> MultiSelectRemote.subscriptions
                , model.selectMots |> MultiSelectRemote.subscriptions
                , model.selectCommunes |> MultiSelectRemote.subscriptions
                ]
                    |> Sub.batch
        }


type alias Model =
    { dataEtablissementCreation : DataEtablissementCreation
    , selectActivites : MultiSelectRemote.SmartSelect Msg String
    , selectLocalisations : MultiSelectRemote.SmartSelect Msg String
    , selectMots : MultiSelectRemote.SmartSelect Msg String
    , selectCommunes : MultiSelectRemote.SmartSelect Msg Commune
    , validationFiche : RD.RemoteData FormErrors ()
    , enregistrementFiche : WebData (EntityId EtablissementCreationId)
    , sources : WebData Sources
    , contactAction : ContactAction
    , prochainId : Int
    }


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init _ =
    ( { dataEtablissementCreation = defaultDataEtablissementCreation
      , selectActivites =
            MultiSelectRemote.init selectActivitesId
                { selectionMsg = SelectedActivites
                , internalMsg = UpdatedSelectActivites
                , characterSearchThreshold = selectCharacterThresholdEtiquettes
                , debounceDuration = selectDebounceDuration
                }
      , selectLocalisations =
            MultiSelectRemote.init selectLocalisationsId
                { selectionMsg = SelectedLocalisations
                , internalMsg = UpdatedSelectLocalisations
                , characterSearchThreshold = selectCharacterThresholdEtiquettes
                , debounceDuration = selectDebounceDuration
                }
      , selectMots =
            MultiSelectRemote.init selectMotsId
                { selectionMsg = SelectedMots
                , internalMsg = UpdatedSelectMots
                , characterSearchThreshold = selectCharacterThresholdEtiquettes
                , debounceDuration = selectDebounceDuration
                }
      , selectCommunes =
            MultiSelectRemote.init "select-communes"
                { selectionMsg = SelectedCommunes
                , internalMsg = UpdatedSelectCommunes
                , characterSearchThreshold = 0
                , debounceDuration = 400
                }
      , validationFiche = RD.NotAsked
      , enregistrementFiche = RD.NotAsked
      , sources = RD.Loading
      , contactAction = None
      , prochainId = 0
      }
    , Effect.fromCmd getSources
    )
        |> Shared.pageChangeEffects


selectCharacterThresholdEtiquettes : Int
selectCharacterThresholdEtiquettes =
    0


selectDebounceDuration : Float
selectDebounceDuration =
    400


selectActivitesId : String
selectActivitesId =
    "champ-selection-activites"


selectLocalisationsId : String
selectLocalisationsId =
    "champ-selection-localisations"


selectMotsId : String
selectMotsId =
    "champ-selection-mots"


selectActivitesConfig : SelectConfig String
selectActivitesConfig =
    { headers = []
    , url = Api.rechercheActivite
    , optionDecoder = decodeEtiquetteList
    }


selectLocalisationsConfig : SelectConfig String
selectLocalisationsConfig =
    { headers = []
    , url = Api.rechercheLocalisation
    , optionDecoder = decodeEtiquetteList
    }


selectMotsConfig : SelectConfig String
selectMotsConfig =
    { headers = []
    , url = Api.rechercheMot
    , optionDecoder = decodeEtiquetteList
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
      -- meta
    | ReceivedSources (WebData Sources)
      -- premier contact
    | UpdateContactOrigine (Maybe GenericSource)
    | UpdateContactDate (Maybe String)
      -- projet
    | UpdateContratAccompagnement Bool
    | UpdateRechercheLocal Bool
    | UpdatedProjet ProjetField String
    | SelectedActivites ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectActivites (MultiSelectRemote.Msg String)
    | UnselectActivite String
    | SelectedLocalisations ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectLocalisations (MultiSelectRemote.Msg String)
    | UnselectLocalisation String
    | SelectedMots ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectMots (MultiSelectRemote.Msg String)
    | UnselectMot String
    | SelectedCommunes ( List Commune, MultiSelectRemote.Msg Commune )
    | UpdatedSelectCommunes (MultiSelectRemote.Msg Commune)
    | UnselectCommune Commune
      -- enregistrement
    | RequestedCreationFiche
    | ReceivedCreationFiche (WebData (EntityId EtablissementCreationId))
      -- contacts
    | SetContactAction ContactAction
    | ConfirmContactAction
    | CancelContactAction
    | UpdatedContact ContactField String
    | UpdatedRechercheContactExistant String
    | RechercheContactExistant
    | ReceivedContactsExistants (WebData (List Personne))
    | SelectedContactExistant (Maybe Personne)
    | ToggleNouveauContactModeCreation Bool


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        UpdatedProjet field value ->
            ( { model
                | dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> updateProjetField field value
              }
            , Effect.none
            )

        ReceivedCreationFiche ((RD.Success id) as enregistrementFiche) ->
            ( { model | enregistrementFiche = enregistrementFiche }
            , Effect.fromShared <|
                Shared.Navigate <|
                    Route.Createur <|
                        ( Nothing, id )
            )

        ReceivedCreationFiche enregistrementFiche ->
            ( { model | enregistrementFiche = enregistrementFiche }, Effect.none )

        RequestedCreationFiche ->
            let
                parsedFiche =
                    validateFiche model.dataEtablissementCreation

                ( enregistrementFiche, effect ) =
                    case parsedFiche of
                        RD.Success () ->
                            ( RD.Loading, createCreateur model.dataEtablissementCreation )

                        _ ->
                            ( RD.NotAsked, Effect.none )
            in
            ( { model | validationFiche = parsedFiche, enregistrementFiche = enregistrementFiche }, effect )

        SelectedActivites ( activites, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectActivitesConfig model.selectActivites

                activitesUniques =
                    case activites of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                activites
            in
            ( { model
                | selectActivites = updatedSelect
                , dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> updateProjet (\proj -> { proj | activites = activitesUniques })
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectActivites sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectActivitesConfig model.selectActivites
            in
            ( { model
                | selectActivites = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectActivite activite ->
            if model.enregistrementFiche == RD.Loading then
                ( model, Effect.none )

            else
                ( { model
                    | dataEtablissementCreation =
                        model.dataEtablissementCreation
                            |> updateProjet
                                (\part ->
                                    { part
                                        | activites =
                                            part.activites
                                                |> List.filter ((/=) activite)
                                    }
                                )
                  }
                , Effect.none
                )

        SelectedLocalisations ( localisations, sMsg ) ->
            if model.enregistrementFiche == RD.Loading then
                ( model, Effect.none )

            else
                let
                    ( updatedSelect, selectCmd ) =
                        MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations

                    localisationsUniques =
                        case localisations of
                            [] ->
                                []

                            a :: rest ->
                                if List.member a rest then
                                    rest

                                else
                                    localisations
                in
                ( { model
                    | selectLocalisations = updatedSelect
                    , dataEtablissementCreation =
                        model.dataEtablissementCreation
                            |> updateProjet (\p -> { p | localisations = localisationsUniques })
                  }
                , Effect.fromCmd selectCmd
                )

        UpdatedSelectLocalisations sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations
            in
            ( { model
                | selectLocalisations = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectLocalisation localisation ->
            ( { model
                | dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> updateProjet (\p -> { p | localisations = p.localisations |> List.filter ((/=) localisation) })
              }
            , Effect.none
            )

        SelectedMots ( mots, sMsg ) ->
            if model.enregistrementFiche == RD.Loading then
                ( model, Effect.none )

            else
                let
                    ( updatedSelect, selectCmd ) =
                        MultiSelectRemote.update sMsg selectMotsConfig model.selectMots

                    motsUniques =
                        case mots of
                            [] ->
                                []

                            a :: rest ->
                                if List.member a rest then
                                    rest

                                else
                                    mots
                in
                ( { model
                    | selectMots = updatedSelect
                    , dataEtablissementCreation =
                        model.dataEtablissementCreation
                            |> updateProjet (\p -> { p | mots = motsUniques })
                  }
                , Effect.fromCmd selectCmd
                )

        UpdatedSelectMots sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectMotsConfig model.selectMots
            in
            ( { model
                | selectMots = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectMot mot ->
            ( { model
                | dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> updateProjet
                            (\p ->
                                { p
                                    | mots =
                                        p.mots
                                            |> List.filter ((/=) mot)
                                }
                            )
              }
            , Effect.none
            )

        SelectedCommunes ( communes, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectCommunesConfig model.selectCommunes

                communesUniques =
                    case communes of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                communes

                newModel =
                    { model
                        | selectCommunes = updatedSelect
                        , dataEtablissementCreation =
                            model.dataEtablissementCreation
                                |> updateProjet
                                    (\proj ->
                                        { proj
                                            | communes = communesUniques
                                        }
                                    )
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectCommunes sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectCommunesConfig model.selectCommunes
            in
            ( { model | selectCommunes = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectCommune commune ->
            let
                newModel =
                    { model
                        | dataEtablissementCreation =
                            model.dataEtablissementCreation
                                |> updateProjet
                                    (\proj ->
                                        { proj
                                            | communes =
                                                proj.communes
                                                    |> List.filter (\{ id } -> id /= commune.id)
                                        }
                                    )
                    }
            in
            ( newModel
            , Effect.none
            )

        UpdateContactOrigine contactOrigine ->
            { model
                | dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> (\dec -> { dec | contactOrigine = contactOrigine })
            }
                |> Effect.withNone

        UpdateContactDate contactDate ->
            { model
                | dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> (\dec -> { dec | contactDate = contactDate })
            }
                |> Effect.withNone

        SetContactAction action ->
            let
                cmd =
                    case action of
                        EditContact _ _ ->
                            Task.attempt (\_ -> NoOp) (Dom.focus <| "nouveau-contact-fonction")

                        NewContact { personneExistante } ->
                            Cmd.batch
                                [ Task.attempt (\_ -> NoOp) (Dom.focus <| "nouveau-contact-fonction")
                                , if personneExistante.recherche /= "" then
                                    Task.perform identity (Task.succeed RechercheContactExistant)

                                  else
                                    Cmd.none
                                ]

                        _ ->
                            Cmd.none
            in
            ( { model | contactAction = action }, Effect.fromCmd cmd )

        CancelContactAction ->
            ( { model | contactAction = None }, Effect.none )

        UpdatedContact field value ->
            Effect.withNone <|
                case model.contactAction of
                    None ->
                        model

                    ViewContactFonctions _ ->
                        model

                    DeleteContact _ _ ->
                        model

                    DeleteContactForm _ ->
                        model

                    NewContact contactForm ->
                        { model
                            | contactAction =
                                NewContact <|
                                    updateContactForm field value <|
                                        contactForm
                        }

                    EditContact _ _ ->
                        model

                    EditContactForm contactFormOriginal contactForm ->
                        { model
                            | contactAction =
                                EditContactForm contactFormOriginal <|
                                    updateContactForm field value contactForm
                        }

        ConfirmContactAction ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    { model
                        | dataEtablissementCreation =
                            model.dataEtablissementCreation
                                |> (\dec ->
                                        { dec
                                            | createurs =
                                                dec.createurs
                                                    |> List.reverse
                                                    |> (::) contactForm
                                                    |> List.reverse
                                        }
                                   )
                        , contactAction = None
                        , prochainId = model.prochainId + 1
                    }
                        |> Effect.withNone

                DeleteContact _ _ ->
                    model
                        |> Effect.withNone

                DeleteContactForm contactForm ->
                    { model
                        | dataEtablissementCreation =
                            model.dataEtablissementCreation
                                |> (\dec ->
                                        { dec
                                            | createurs =
                                                dec.createurs
                                                    |> List.filter
                                                        (\c ->
                                                            c.id /= contactForm.id
                                                        )
                                        }
                                   )
                        , contactAction = None
                    }
                        |> Effect.withNone

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm contactFormOriginal contactForm ->
                    { model
                        | dataEtablissementCreation =
                            model.dataEtablissementCreation
                                |> (\dec ->
                                        { dec
                                            | createurs =
                                                dec.createurs
                                                    |> List.map
                                                        (\c ->
                                                            if contactFormOriginal.id == c.id then
                                                                contactForm

                                                            else
                                                                c
                                                        )
                                        }
                                   )
                        , contactAction = None
                    }
                        |> Effect.withNone

        ReceivedSources sources ->
            { model | sources = sources }
                |> Effect.withNone

        UpdateContratAccompagnement contratAccompagnement ->
            { model
                | dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> updateProjet (\dec -> { dec | contratAccompagnement = contratAccompagnement })
            }
                |> Effect.withNone

        UpdateRechercheLocal rechercheLocal ->
            { model
                | dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> updateProjet (\dec -> { dec | rechercheLocal = rechercheLocal })
            }
                |> Effect.withNone

        UpdatedRechercheContactExistant recherche ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | recherche = recherche
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm contactFormOriginal contactForm ->
                    ( { model
                        | contactAction =
                            EditContactForm contactFormOriginal <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | recherche = recherche
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

        RechercheContactExistant ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | resultats = RD.Loading
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.fromCmd <|
                        getPersonnes <|
                            Pages.Annuaire.filtersAndPaginationToQuery
                                ( { defaultFilters
                                    | recherche = contactForm.personneExistante.recherche
                                    , elementsParPage = Just 5
                                  }
                                , defaultPagination
                                )
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm contactFormOriginal contactForm ->
                    ( { model
                        | contactAction =
                            EditContactForm contactFormOriginal <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | resultats = RD.Loading
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.fromCmd <|
                        getPersonnes <|
                            Pages.Annuaire.filtersAndPaginationToQuery
                                ( { defaultFilters
                                    | recherche = contactForm.personneExistante.recherche
                                    , elementsParPage = Just 5
                                  }
                                , defaultPagination
                                )
                    )

        ReceivedContactsExistants response ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | resultats = response
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm contactFormOriginal contactForm ->
                    ( { model
                        | contactAction =
                            EditContactForm contactFormOriginal <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | resultats = response
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

        SelectedContactExistant maybeContact ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | selectionne = maybeContact
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm contactFormOriginal contactForm ->
                    ( { model
                        | contactAction =
                            EditContactForm contactFormOriginal <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | selectionne = maybeContact
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

        ToggleNouveauContactModeCreation modeCreation ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | modeCreation = modeCreation
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm contactFormOriginal contactForm ->
                    ( { model
                        | contactAction =
                            EditContactForm contactFormOriginal <|
                                (\cf ->
                                    { cf
                                        | modeCreation = modeCreation
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )


validateFiche : DataEtablissementCreation -> RD.RemoteData FormErrors ()
validateFiche dataEtablissementCreation =
    if List.length dataEtablissementCreation.createurs == 0 then
        let
            errors =
                [ ( "createurs"
                  , [ "Il est nécessaire d'ajouter au moins un créateur" ]
                  )
                ]
                    |> Dict.fromList
        in
        RD.Failure
            { global =
                if Dict.isEmpty errors then
                    Nothing

                else
                    Just "Veuillez corriger les erreurs."
            , fields = errors
            }

    else
        RD.Success ()


createCreateur : DataEtablissementCreation -> Effect.Effect Shared.Msg Msg
createCreateur dataEtablissementCreation =
    post
        { url = Api.createEtablissementCreation
        , body =
            Http.jsonBody <|
                encodeDataEtablissementCreation dataEtablissementCreation
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedCreationFiche
        }
        (Decode.field "etablissementCreation" <| Decode.field "id" <| decodeEntityId)
        |> Effect.fromCmd


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle <| "Nouvelle fiche"
    , body = UI.Layout.lazyBody body model
    , route = Route.CreateurNew
    }


formulaireCreation : Model -> Html Msg
formulaireCreation model =
    let
        niveauxDiplome =
            model
                |> .sources
                |> RD.map .niveauxDiplome
                |> RD.withDefault []

        situationsProfessionnelles =
            model
                |> .sources
                |> RD.map .situationsProfessionnelles
                |> RD.withDefault []
    in
    div [ class "flex flex-col gap-8" ]
        [ Lazy.lazy3 viewCreateursModal niveauxDiplome situationsProfessionnelles model.contactAction
        , premierContactBloc { updateContactOrigine = UpdateContactOrigine, updateContactDate = UpdateContactDate } model.sources model.dataEtablissementCreation
        , Lazy.lazy5 createursEtablissementBloc SetContactAction model.prochainId model.validationFiche model.contactAction model.dataEtablissementCreation
        , Lazy.lazy8 informationsProjetBloc
            { updatedProjet = UpdatedProjet
            , updateContratAccompagnement = UpdateContratAccompagnement
            , unselectActivite = UnselectActivite
            , unselectLocalisation = UnselectLocalisation
            , unselectMot = UnselectMot
            , unselectCommune = UnselectCommune
            , updateRechercheLocal = UpdateRechercheLocal
            }
            model.validationFiche
            model.selectActivites
            model.selectLocalisations
            model.selectMots
            model.selectCommunes
            model.sources
            model.dataEtablissementCreation
        ]


viewCreateursModal : List { id : String, nom : String } -> List { id : String, nom : String } -> ContactAction -> Html Msg
viewCreateursModal niveauxDiplome situationsProfessionnelles contactAction =
    let
        ( opened, content, title ) =
            case contactAction of
                None ->
                    ( False, nothing, nothing )

                ViewContactFonctions fonctions ->
                    ( True, UI.Contact.viewFonctions SetContactAction fonctions, text "Autres fonctions du contact" )

                NewContact contactForm ->
                    ( True
                    , viewNewContactFormFields
                        { confirm = ConfirmContactAction
                        , update = UpdatedContact
                        , cancel = CancelContactAction
                        , toggle = ToggleNouveauContactModeCreation
                        , confirmSearch = RechercheContactExistant
                        , updateSearch = UpdatedRechercheContactExistant
                        , selectedPersonne = SelectedContactExistant
                        , niveauxDiplome = Just niveauxDiplome
                        , situationsProfessionnelles = Just situationsProfessionnelles
                        }
                        RD.NotAsked
                        contactForm
                    , text "Ajouter un créateur"
                    )

                EditContact _ _ ->
                    ( False, nothing, nothing )

                EditContactForm _ contactForm ->
                    ( True
                    , viewNewContactFormFields
                        { confirm = ConfirmContactAction
                        , update = UpdatedContact
                        , cancel = CancelContactAction
                        , toggle = ToggleNouveauContactModeCreation
                        , confirmSearch = RechercheContactExistant
                        , updateSearch = UpdatedRechercheContactExistant
                        , selectedPersonne = SelectedContactExistant
                        , niveauxDiplome = Just niveauxDiplome
                        , situationsProfessionnelles = Just situationsProfessionnelles
                        }
                        RD.NotAsked
                        contactForm
                    , text "Modifier un créateur"
                    )

                DeleteContact _ _ ->
                    ( False, nothing, nothing )

                DeleteContactForm contactForm ->
                    ( True
                    , viewDeleteContactFormBody
                        { confirm = ConfirmContactAction
                        , cancel = CancelContactAction
                        }
                        contactForm.id
                    , text "Supprimer un créateur"
                    )
    in
    DSFR.Modal.view
        { id = "contact"
        , label = "contact"
        , openMsg = NoOp
        , closeMsg = Just CancelContactAction
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.md
        }
        content
        Nothing
        |> Tuple.first


body : Model -> Html Msg
body model =
    div [ class "fr-card--white p-4 sm:p-8 " ]
        [ nothing
        , div [ class "flex flex-col gap-8 sm:max-w-[80%] mx-[10%]" ]
            [ h1 [ class "!mb-0" ] [ text "Création d'une fiche créateur d'entreprise" ]
            , formWithListeners [ Events.onSubmit <| RequestedCreationFiche, class "flex flex-col gap-8" ]
                [ formulaireCreation model
                , footer model
                ]
            ]
        ]


footer : Model -> Html Msg
footer model =
    let
        label =
            case model.enregistrementFiche of
                RD.NotAsked ->
                    "Créer la fiche"

                RD.Loading ->
                    "Création..."

                RD.Failure _ ->
                    "Créer la fiche"

                RD.Success _ ->
                    "Créer la fiche"

        isButtonDisabled =
            model.enregistrementFiche == RD.Loading
    in
    div [ class "flex flex-col gap-2" ]
        [ div [ class "flex flex-row justify-end" ]
            [ case model.validationFiche of
                RD.Failure { global } ->
                    case global of
                        Nothing ->
                            nothing

                        Just error ->
                            DSFR.Alert.small { title = Nothing, description = text error }
                                |> DSFR.Alert.alert Nothing DSFR.Alert.info

                _ ->
                    nothing
            , case model.enregistrementFiche of
                RD.Failure _ ->
                    DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite, veuillez réessayer." }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                _ ->
                    nothing
            ]
        , DSFR.Button.group
            [ DSFR.Button.new { onClick = Nothing, label = label }
                |> DSFR.Button.submit
                |> DSFR.Button.withDisabled isButtonDisabled
            , DSFR.Button.new { onClick = Nothing, label = "Précédent" }
                |> DSFR.Button.secondary
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Createurs Nothing)
            ]
            |> DSFR.Button.inline
            |> DSFR.Button.alignedRightInverted
            |> DSFR.Button.viewGroup
        ]


getSources : Cmd Msg
getSources =
    get
        { url = Api.getCreateurSources }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSources
        }
        decodeSources


getPersonnes : String -> Cmd Msg
getPersonnes query =
    get
        { url = Api.getContacts query }
        { toShared = SharedMsg
        , logger = Nothing
        , handler = ReceivedContactsExistants
        }
        (Decode.field "elements" <| Decode.list <| decodePersonne)


selectCommunesConfig : MultiSelectRemote.SelectConfig Commune
selectCommunesConfig =
    { headers = []
    , url = Api.rechercheTerritoireGeo (Filters.situationGeographiqueToCode Filters.SurMonTerritoire) "commune"
    , optionDecoder = Decode.field "territoires" <| Decode.list <| Data.Commune.decodeCommune
    }
