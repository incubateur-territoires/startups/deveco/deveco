module Pages.EtablissementCreation.EditId_ exposing (Model, Msg(..), page)

import Accessibility exposing (Html, div, formWithListeners, h1, text)
import Api
import Api.Auth exposing (get, post)
import Api.EntityId exposing (EntityId)
import DSFR.Alert
import DSFR.Button
import Data.Commune exposing (Commune)
import Data.EtablissementCreationId exposing (EtablissementCreationId)
import Data.Etiquette exposing (decodeEtiquetteList)
import Data.GenericSource exposing (GenericSource)
import Effect
import Html.Attributes exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Html.Lazy as Lazy
import Http
import Json.Decode as Decode
import Lib.Date exposing (formatDateToYYYYMMDD)
import MultiSelectRemote
import Pages.EtablissementCreation.Data exposing (EtablissementCreationWithExtraInfo, decodeEtablissementCreationWithExtraInfo)
import Pages.Etablissements.Filters as Filters
import RemoteData as RD exposing (WebData)
import Route
import Shared
import SingleSelectRemote exposing (SelectConfig)
import Spa.Page exposing (Page)
import UI.EtablissementCreation exposing (DataEtablissementCreation, ProjetField, Sources, decodeSources, defaultDataEtablissementCreation, encodeDataEtablissementCreation, informationsProjetBloc, premierContactBloc, updateProjet, updateProjetField)
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page (EntityId EtablissementCreationId) Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.element <|
        { init = init
        , update = update
        , view = view
        , subscriptions =
            \model ->
                [ model.selectActivites |> MultiSelectRemote.subscriptions
                , model.selectLocalisations |> MultiSelectRemote.subscriptions
                , model.selectMots |> MultiSelectRemote.subscriptions
                , model.selectCommunes |> MultiSelectRemote.subscriptions
                ]
                    |> Sub.batch
        }


type alias Model =
    { etablissementCreationWithExtraInfo : WebData EtablissementCreationWithExtraInfo
    , ficheId : EntityId EtablissementCreationId
    , dataEtablissementCreation : DataEtablissementCreation
    , selectActivites : MultiSelectRemote.SmartSelect Msg String
    , selectLocalisations : MultiSelectRemote.SmartSelect Msg String
    , selectMots : MultiSelectRemote.SmartSelect Msg String
    , selectCommunes : MultiSelectRemote.SmartSelect Msg Commune
    , enregistrementFiche : WebData (EntityId EtablissementCreationId)
    , sources : WebData Sources
    }


init : EntityId EtablissementCreationId -> ( Model, Effect.Effect Shared.Msg Msg )
init id =
    ( { etablissementCreationWithExtraInfo = RD.Loading
      , ficheId = id
      , dataEtablissementCreation = defaultDataEtablissementCreation
      , selectActivites =
            MultiSelectRemote.init selectActivitesId
                { selectionMsg = SelectedActivites
                , internalMsg = UpdatedSelectActivites
                , characterSearchThreshold = selectCharacterThresholdEtiquettes
                , debounceDuration = selectDebounceDuration
                }
      , selectLocalisations =
            MultiSelectRemote.init selectLocalisationsId
                { selectionMsg = SelectedLocalisations
                , internalMsg = UpdatedSelectLocalisations
                , characterSearchThreshold = selectCharacterThresholdEtiquettes
                , debounceDuration = selectDebounceDuration
                }
      , selectMots =
            MultiSelectRemote.init selectMotsId
                { selectionMsg = SelectedMots
                , internalMsg = UpdatedSelectMots
                , characterSearchThreshold = selectCharacterThresholdEtiquettes
                , debounceDuration = selectDebounceDuration
                }
      , selectCommunes =
            MultiSelectRemote.init "select-communes"
                { selectionMsg = SelectedCommunes
                , internalMsg = UpdatedSelectCommunes
                , characterSearchThreshold = 0
                , debounceDuration = 400
                }
      , sources = RD.Loading
      , enregistrementFiche = RD.NotAsked
      }
    , Effect.fromCmd <|
        Cmd.batch
            [ fetchEtablissementCreation id
            , getSources
            ]
    )


selectActivitesId : String
selectActivitesId =
    "champ-selection-activites"


selectLocalisationsId : String
selectLocalisationsId =
    "champ-selection-zones"


selectMotsId : String
selectMotsId =
    "champ-selection-mots"


selectCharacterThresholdEtiquettes : Int
selectCharacterThresholdEtiquettes =
    0


selectDebounceDuration : Float
selectDebounceDuration =
    400


selectActivitesConfig : SelectConfig String
selectActivitesConfig =
    { headers = []
    , url = Api.rechercheActivite
    , optionDecoder = decodeEtiquetteList
    }


selectLocalisationsConfig : SelectConfig String
selectLocalisationsConfig =
    { headers = []
    , url = Api.rechercheLocalisation
    , optionDecoder = decodeEtiquetteList
    }


selectMotsConfig : SelectConfig String
selectMotsConfig =
    { headers = []
    , url = Api.rechercheMot
    , optionDecoder = decodeEtiquetteList
    }


fetchEtablissementCreation : EntityId EtablissementCreationId -> Cmd Msg
fetchEtablissementCreation id =
    get
        { url = Api.getEtablissementCreation id
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedEtablissementCreationWithExtraInfo
        }
        (Decode.field "etablissementCreation" decodeEtablissementCreationWithExtraInfo)


type Msg
    = SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
      -- meta
    | ReceivedSources (WebData Sources)
      -- création établissement existante
    | ReceivedEtablissementCreationWithExtraInfo (WebData EtablissementCreationWithExtraInfo)
      -- premier contact
    | UpdateContactOrigine (Maybe GenericSource)
    | UpdateContactDate (Maybe String)
      -- projet
    | UpdateContratAccompagnement Bool
    | UpdateRechercheLocal Bool
    | UpdatedProjet ProjetField String
    | SelectedActivites ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectActivites (MultiSelectRemote.Msg String)
    | UnselectActivite String
    | SelectedLocalisations ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectLocalisations (MultiSelectRemote.Msg String)
    | UnselectLocalisation String
    | SelectedMots ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectMots (MultiSelectRemote.Msg String)
    | UnselectMot String
    | SelectedCommunes ( List Commune, MultiSelectRemote.Msg Commune )
    | UpdatedSelectCommunes (MultiSelectRemote.Msg Commune)
    | UnselectCommune Commune
      -- enregistrement
    | RequestedSaveEtablissementCreationWithExtraInfo
    | ReceivedSaveEtablissementCreationWithExtraInfo (WebData (EntityId EtablissementCreationId))


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        ReceivedSaveEtablissementCreationWithExtraInfo response ->
            case response of
                RD.Success _ ->
                    ( { model | enregistrementFiche = response }
                    , Effect.fromShared <|
                        Shared.Navigate <|
                            Route.Createur <|
                                ( Nothing, model.ficheId )
                    )

                _ ->
                    ( { model | enregistrementFiche = response }, Effect.none )

        RequestedSaveEtablissementCreationWithExtraInfo ->
            ( { model
                | enregistrementFiche = RD.Loading
              }
            , updateEtablissementCreationWithExtraInfo model
            )

        ReceivedEtablissementCreationWithExtraInfo response ->
            case response of
                RD.Success etablissementCreationWithExtraInfo ->
                    case etablissementCreationWithExtraInfo.etablissementLie of
                        Just _ ->
                            ( model
                            , Effect.fromShared <|
                                Shared.Navigate <|
                                    Route.Createur <|
                                        ( Nothing, model.ficheId )
                            )

                        Nothing ->
                            let
                                dataEtablissementCreation =
                                    dataEtablissementCreationFromEtablissementCreationWithExtraInfo etablissementCreationWithExtraInfo
                            in
                            ( { model
                                | etablissementCreationWithExtraInfo = response
                                , dataEtablissementCreation = dataEtablissementCreation
                              }
                            , Effect.none
                            )

                _ ->
                    ( model, Effect.none )

        SelectedActivites ( activites, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectActivitesConfig model.selectActivites

                activitesUniques =
                    case activites of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                activites
            in
            ( { model
                | selectActivites = updatedSelect
                , dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> updateProjet (\proj -> { proj | activites = activitesUniques })
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectActivites sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectActivitesConfig model.selectActivites
            in
            ( { model
                | selectActivites = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectActivite activite ->
            if model.enregistrementFiche == RD.Loading then
                ( model, Effect.none )

            else
                ( { model
                    | dataEtablissementCreation =
                        model.dataEtablissementCreation
                            |> updateProjet
                                (\part ->
                                    { part
                                        | activites =
                                            part.activites
                                                |> List.filter ((/=) activite)
                                    }
                                )
                  }
                , Effect.none
                )

        SelectedLocalisations ( localisations, sMsg ) ->
            if model.enregistrementFiche == RD.Loading then
                ( model, Effect.none )

            else
                let
                    ( updatedSelect, selectCmd ) =
                        MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations

                    localisationsUniques =
                        case localisations of
                            [] ->
                                []

                            a :: rest ->
                                if List.member a rest then
                                    rest

                                else
                                    localisations
                in
                ( { model
                    | selectLocalisations = updatedSelect
                    , dataEtablissementCreation =
                        model.dataEtablissementCreation
                            |> updateProjet (\p -> { p | localisations = localisationsUniques })
                  }
                , Effect.fromCmd selectCmd
                )

        UpdatedSelectLocalisations sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations
            in
            ( { model
                | selectLocalisations = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectLocalisation localisation ->
            ( { model
                | dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> updateProjet (\p -> { p | localisations = p.localisations |> List.filter ((/=) localisation) })
              }
            , Effect.none
            )

        SelectedMots ( mots, sMsg ) ->
            if model.enregistrementFiche == RD.Loading then
                ( model, Effect.none )

            else
                let
                    ( updatedSelect, selectCmd ) =
                        MultiSelectRemote.update sMsg selectMotsConfig model.selectMots

                    motsUniques =
                        case mots of
                            [] ->
                                []

                            a :: rest ->
                                if List.member a rest then
                                    rest

                                else
                                    mots
                in
                ( { model
                    | selectMots = updatedSelect
                    , dataEtablissementCreation =
                        model.dataEtablissementCreation
                            |> updateProjet (\p -> { p | mots = motsUniques })
                  }
                , Effect.fromCmd selectCmd
                )

        UpdatedSelectMots sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectMotsConfig model.selectMots
            in
            ( { model
                | selectMots = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectMot mot ->
            ( { model
                | dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> updateProjet
                            (\p ->
                                { p
                                    | mots =
                                        p.mots
                                            |> List.filter ((/=) mot)
                                }
                            )
              }
            , Effect.none
            )

        SelectedCommunes ( communes, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectCommunesConfig model.selectCommunes

                communesUniques =
                    case communes of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                communes

                newModel =
                    { model
                        | selectCommunes = updatedSelect
                        , dataEtablissementCreation =
                            model.dataEtablissementCreation
                                |> updateProjet
                                    (\proj ->
                                        { proj
                                            | communes = communesUniques
                                        }
                                    )
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectCommunes sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectCommunesConfig model.selectCommunes
            in
            ( { model | selectCommunes = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectCommune commune ->
            let
                newModel =
                    { model
                        | dataEtablissementCreation =
                            model.dataEtablissementCreation
                                |> updateProjet
                                    (\proj ->
                                        { proj
                                            | communes =
                                                proj.communes
                                                    |> List.filter (\{ id } -> id /= commune.id)
                                        }
                                    )
                    }
            in
            ( newModel
            , Effect.none
            )

        UpdatedProjet field value ->
            ( { model
                | dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> updateProjetField field value
              }
            , Effect.none
            )

        UpdateContactOrigine contactOrigine ->
            { model
                | dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> (\dec -> { dec | contactOrigine = contactOrigine })
            }
                |> Effect.withNone

        UpdateContactDate contactDate ->
            { model
                | dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> (\dec -> { dec | contactDate = contactDate })
            }
                |> Effect.withNone

        ReceivedSources sources ->
            { model | sources = sources }
                |> Effect.withNone

        UpdateContratAccompagnement contratAccompagnement ->
            { model
                | dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> updateProjet (\dec -> { dec | contratAccompagnement = contratAccompagnement })
            }
                |> Effect.withNone

        UpdateRechercheLocal rechercheLocal ->
            { model
                | dataEtablissementCreation =
                    model.dataEtablissementCreation
                        |> updateProjet (\dec -> { dec | rechercheLocal = rechercheLocal })
            }
                |> Effect.withNone


updateEtablissementCreationWithExtraInfo : Model -> Effect.Effect Shared.Msg Msg
updateEtablissementCreationWithExtraInfo { dataEtablissementCreation, ficheId } =
    let
        jsonBody =
            dataEtablissementCreation
                |> encodeDataEtablissementCreation
                |> Http.jsonBody
    in
    post
        { url = Api.getEtablissementCreation ficheId
        , body = jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveEtablissementCreationWithExtraInfo
        }
        (Decode.succeed ficheId)
        |> Effect.fromCmd


view : Model -> View Msg
view model =
    { title =
        UI.Layout.pageTitle <|
            RD.withDefault "Modification" <|
                RD.map ((++) "Modification de " << .nom << .etablissementCreation) <|
                    model.etablissementCreationWithExtraInfo
    , body = UI.Layout.lazyBody body model
    , route = Route.CreateurNew
    }


body : Model -> Html Msg
body model =
    div [ class "fr-card--white p-4 sm:p-8 flex flex-col gap-8" ]
        [ h1 [ class "!mb-0" ]
            [ text <|
                RD.withDefault "Modification de la fiche" <|
                    RD.map ((++) "Modification de la fiche de " << .nom << .etablissementCreation) <|
                        model.etablissementCreationWithExtraInfo
            ]
        , formWithListeners [ Events.onSubmit <| RequestedSaveEtablissementCreationWithExtraInfo, class "flex flex-col gap-8" ]
            [ Lazy.lazy3 premierContactBloc
                { updateContactOrigine = UpdateContactOrigine
                , updateContactDate = UpdateContactDate
                }
                model.sources
                model.dataEtablissementCreation
            , Lazy.lazy8 informationsProjetBloc
                { updatedProjet = UpdatedProjet
                , updateContratAccompagnement = UpdateContratAccompagnement
                , unselectActivite = UnselectActivite
                , unselectLocalisation = UnselectLocalisation
                , unselectMot = UnselectMot
                , unselectCommune = UnselectCommune
                , updateRechercheLocal = UpdateRechercheLocal
                }
                RD.NotAsked
                model.selectActivites
                model.selectLocalisations
                model.selectMots
                model.selectCommunes
                model.sources
                model.dataEtablissementCreation
            , footer model
            ]
        ]


footer : Model -> Html Msg
footer model =
    div [ class "flex flex-col gap-2" ]
        [ div [ class "flex flex-row justify-end" ]
            [ case model.enregistrementFiche of
                RD.Success _ ->
                    DSFR.Alert.small { title = Nothing, description = text "Modifications enregistrées\u{00A0}!" }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.success

                RD.Failure _ ->
                    DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite, veuillez réessayer." }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                _ ->
                    nothing
            ]
        , DSFR.Button.group
            [ DSFR.Button.new { onClick = Nothing, label = "Enregistrer" }
                |> DSFR.Button.submit
                |> DSFR.Button.withDisabled (model.enregistrementFiche == RD.Loading)
            , DSFR.Button.new { onClick = Nothing, label = "Précédent" }
                |> DSFR.Button.secondary
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Createur ( Nothing, model.ficheId ))
            ]
            |> DSFR.Button.inline
            |> DSFR.Button.alignedRightInverted
            |> DSFR.Button.viewGroup
        ]


getSources : Cmd Msg
getSources =
    get
        { url = Api.getCreateurSources }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSources
        }
        decodeSources


dataEtablissementCreationFromEtablissementCreationWithExtraInfo : EtablissementCreationWithExtraInfo -> DataEtablissementCreation
dataEtablissementCreationFromEtablissementCreationWithExtraInfo ecwei =
    { contactOrigine = ecwei.contactOrigine
    , contactDate =
        ecwei.contactDate
            |> Maybe.map formatDateToYYYYMMDD
    , createurs = []
    , projet =
        { projetType =
            ecwei.projetType
                |> Maybe.map .id
                |> Maybe.withDefault ""
                |> String.trim
        , futureEnseigne = ecwei.etablissementCreation.futureEnseigne
        , description = ecwei.description
        , secteurActivite =
            ecwei.secteurActivite
                |> Maybe.map .id
                |> Maybe.withDefault ""
                |> String.trim
        , formeJuridique = ecwei.formeJuridique
        , sourceFinancement =
            ecwei.sourceFinancement
                |> Maybe.map .id
                |> Maybe.withDefault ""
                |> String.trim
        , contratAccompagnement =
            ecwei.contratAccompagnement
                |> Maybe.withDefault False
        , activites = ecwei.etiquettes.activites
        , mots = ecwei.etiquettes.motsCles
        , localisations = ecwei.etiquettes.localisations
        , communes = ecwei.communes
        , rechercheLocal = ecwei.rechercheLocal |> Maybe.withDefault False
        , commentaires = ecwei.commentaire
        }
    }


selectCommunesConfig : MultiSelectRemote.SelectConfig Commune
selectCommunesConfig =
    { headers = []
    , url = Api.rechercheTerritoireGeo (Filters.situationGeographiqueToCode Filters.SurMonTerritoire) "commune"
    , optionDecoder = Decode.field "territoires" <| Decode.list <| Data.Commune.decodeCommune
    }
