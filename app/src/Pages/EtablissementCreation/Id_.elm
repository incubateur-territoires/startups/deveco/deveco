module Pages.EtablissementCreation.Id_ exposing (Model, Msg(..), Onglet(..), page)

import Accessibility exposing (Html, div, formWithListeners, h1, h3, hr, p, span, sup, text)
import Api
import Api.Auth exposing (delete, get, post)
import Api.EntityId exposing (EntityId, parseEntityId, unsafeConvert)
import Browser.Dom as Dom
import DSFR.Alert
import DSFR.Button
import DSFR.Grid
import DSFR.Icons
import DSFR.Icons.Business
import DSFR.Icons.Communication
import DSFR.Icons.Design
import DSFR.Icons.System
import DSFR.Input
import DSFR.Modal
import DSFR.SearchBar
import DSFR.Tabs
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Collegues exposing (Collegue)
import Data.Commune exposing (Commune)
import Data.Contact exposing (Contact, ContactInputType(..), EntiteLien(..), FonctionContact(..))
import Data.Demande
import Data.Etablissement exposing (EtablissementApercu, decodeEtablissementApercu)
import Data.EtablissementCreationId exposing (EtablissementCreationId)
import Data.Etiquette exposing (Etiquettes)
import Data.GenericSource exposing (GenericSource, decodeGenericSource)
import Data.Personne exposing (Personne, decodePersonne)
import Data.Ressource exposing (Ressource, RessourceAction(..), RessourceField(..), encodeRessource, ressourceTypeToDisplay, ressourceTypeToString, ressourceTypes, updateRessource, viewRessourcesHelper)
import Date exposing (Date)
import Effect
import Html.Attributes exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Html.Lazy as Lazy
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import Lib.Date exposing (formatDateShort)
import Lib.UI exposing (plural, withEmptyAs)
import Lib.Variables exposing (hintActivites, hintMotsCles, hintZoneGeographique)
import Pages.Annuaire exposing (defaultFilters)
import Pages.Etablissement.Siret_
import Pages.EtablissementCreation.Data exposing (EtablissementCreationWithExtraInfo, EtablissementLie, decodeEtablissementCreationWithExtraInfo)
import QS
import RemoteData as RD exposing (WebData)
import Route
import Shared
import Spa.Page exposing (Page)
import Task
import Time exposing (Posix, Zone)
import UI.Collegue exposing (Partage, getCollegues, viewPartageModal)
import UI.Contact exposing (ContactAction(..), ContactField, NewContactForm, encodeNewContactForm, updateContact, updateContactForm, viewDeleteContactBody, viewEditContact, viewNewContactFormFields)
import UI.Entite exposing (displayIconeCreateur, iconeCreateur)
import UI.EtablissementCreation
import UI.Layout
import UI.Pagination exposing (defaultPagination)
import UI.Suivi
import User
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page ( Maybe String, EntityId EtablissementCreationId ) Shared.Msg (View Msg) Model Msg
page shared user =
    Spa.Page.onNewFlags (Tuple.first >> queryToOnglets >> UpdateOnglets) <|
        Spa.Page.element
            { view = view user shared
            , init = init shared
            , update = update user
            , subscriptions = \_ -> Sub.none
            }


type alias Model =
    { etablissementCreationWithExtraInfo : WebData EtablissementCreationWithExtraInfo
    , etablissementCreationId : EntityId EtablissementCreationId
    , suiviModel : UI.Suivi.Model
    , today : Date
    , deleteEtablissementCreation : Maybe (WebData ())
    , lien : Maybe Link
    , suppressionTransformation : Maybe (WebData EtablissementCreationWithExtraInfo)
    , devecos : WebData (List Collegue)
    , partageModal : Maybe Partage
    , onglet : Onglet
    , currentUrl : String
    , contactAction : ContactAction
    , ressourceAction : RessourceAction
    , prochainId : Int
    , saveCreateurRequest : WebData EtablissementCreationWithExtraInfo
    , saveRessourceRequest : WebData EtablissementCreationWithExtraInfo
    , sources : WebData Sources
    }


type Onglet
    = OngletProjet
    | OngletSuivi


ongletToString : Onglet -> String
ongletToString onglet =
    case onglet of
        OngletProjet ->
            "createur"

        OngletSuivi ->
            "suivi"


ongletToDisplay : Onglet -> String
ongletToDisplay onglet =
    case onglet of
        OngletProjet ->
            "Projet"

        OngletSuivi ->
            "Suivi de la relation"


stringToOnglet : String -> Maybe Onglet
stringToOnglet s =
    case s of
        "createur" ->
            Just OngletProjet

        "suivi" ->
            Just OngletSuivi

        _ ->
            Nothing


type alias Link =
    { siret : String
    , requete : WebData EtablissementCreationWithExtraInfo
    , resultEtablissements : WebData RechercheParSiretResultat
    , etablissement : Maybe EtablissementApercu
    }


init : Shared.Shared -> ( Maybe String, EntityId EtablissementCreationId ) -> ( Model, Effect.Effect Shared.Msg Msg )
init shared ( query, id ) =
    let
        ( onglet, ongletSuivi ) =
            queryToOnglets query

        nextUrl =
            ongletsToQuery ( onglet, ongletSuivi )
    in
    ( { etablissementCreationWithExtraInfo = RD.Loading
      , etablissementCreationId = id
      , suiviModel = UI.Suivi.init UI.Suivi.OngletEchanges <| Just ongletSuivi
      , today = Date.fromPosix shared.timezone shared.now
      , deleteEtablissementCreation = Nothing
      , lien = Nothing
      , suppressionTransformation = Nothing
      , devecos = RD.Loading
      , partageModal = Nothing
      , onglet = onglet
      , currentUrl = nextUrl
      , contactAction = None
      , ressourceAction = NoRessource
      , prochainId = 0
      , saveCreateurRequest = RD.NotAsked
      , saveRessourceRequest = RD.NotAsked
      , sources = RD.Loading
      }
    , Effect.batch
        [ Effect.fromCmd <| fetchEtablissementCreation id
        , Effect.fromCmd (getCollegues SharedMsg (Just LogError) ReceivedCollegue)
        , Effect.fromCmd getSources
        ]
    )
        |> Shared.pageChangeEffects


fetchEtablissementCreation : EntityId EtablissementCreationId -> Cmd Msg
fetchEtablissementCreation id =
    get
        { url = Api.getEtablissementCreation id }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedEtablissementCreation
        }
        (Decode.field "etablissementCreation" decodeEtablissementCreationWithExtraInfo)


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | SuiviMsg UI.Suivi.Msg
    | ReceivedEtablissementCreation (WebData EtablissementCreationWithExtraInfo)
    | ClickedDelete
    | CanceledDelete
    | ConfirmedDelete
    | ReceivedDelete (WebData ())
    | LinkCreateur
    | CanceledLinkCreateur
    | ConfirmedLinkCreateur
    | ReceivedLink (WebData EtablissementCreationWithExtraInfo)
    | UpdatedSiret String
    | RequestedRecherche
    | ReceivedRecherche (WebData RechercheParSiretResultat)
    | UnlinkCreateur
    | CanceledUnlinkCreateur
    | ConfirmedUnlinkCreateur
    | ReceivedUnlink (WebData EtablissementCreationWithExtraInfo)
    | ReceivedCollegue (WebData (List Collegue))
    | ClickedPartage
    | CancelPartage
    | UpdatedPartageDeveco String
    | UpdatedPartageCommentaire String
    | ConfirmPartage
    | ReceivedPartage (WebData ())
    | ClickedChangementOnglet Onglet
    | UpdateOnglets ( Onglet, UI.Suivi.Onglet )
    | SetContactAction ContactAction
    | ConfirmContactAction
    | ReceivedSaveContact (WebData EtablissementCreationWithExtraInfo)
    | CancelContactAction
    | UpdatedContact ContactField String
    | UpdatedRechercheContactExistant String
    | RechercheContactExistant
    | ReceivedContactsExistants (WebData (List Personne))
    | SelectedContactExistant (Maybe Personne)
    | ToggleNouveauContactModeCreation Bool
    | ReceivedSources (WebData Sources)
    | SetRessourceAction RessourceAction
    | UpdatedRessource RessourceField String
    | ConfirmRessourceAction
    | CancelRessourceAction
    | ReceivedSaveRessource (WebData EtablissementCreationWithExtraInfo)
    | ToggleCreationAbandonnee
    | ReceivedToggleCreationAbandonneeResult Bool


type RechercheParSiretResultat
    = Resultat EtablissementApercu
    | Erreur String


update : User.User -> Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update user msg model =
    case msg of
        NoOp ->
            ( model, Effect.none )

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        SuiviMsg suiviMsg ->
            let
                ( suiviModel, suiviEffect, entity ) =
                    UI.Suivi.update (UI.Suivi.suiviConfigEtabCrea (Decode.field "etablissementCreation" decodeEtablissementCreationWithExtraInfo) model.etablissementCreationId) suiviMsg model.suiviModel

                ongletSuivi =
                    UI.Suivi.getOngletActif suiviModel

                ( newModel, effect ) =
                    { model
                        | suiviModel = suiviModel
                        , etablissementCreationWithExtraInfo =
                            entity
                                |> UI.Suivi.suiviResultToMaybe
                                |> Maybe.map RD.Success
                                |> Maybe.withDefault model.etablissementCreationWithExtraInfo
                    }
                        |> updateUrl ( model.onglet, ongletSuivi )
            in
            ( newModel
            , Effect.batch [ effect, Effect.map SuiviMsg <| suiviEffect ]
            )

        ReceivedEtablissementCreation response ->
            ( { model | etablissementCreationWithExtraInfo = response }, Effect.none )

        ClickedDelete ->
            { model | deleteEtablissementCreation = Just RD.NotAsked }
                |> Effect.withNone

        CanceledDelete ->
            { model | deleteEtablissementCreation = Nothing }
                |> Effect.withNone

        ConfirmedDelete ->
            case model.etablissementCreationWithExtraInfo of
                RD.Success etablissementCreationWithExtraInfo ->
                    { model | deleteEtablissementCreation = Just RD.Loading }
                        |> Effect.withCmd (deleteEtablissementCreation etablissementCreationWithExtraInfo)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedDelete response ->
            { model | deleteEtablissementCreation = Just response }
                |> Effect.withNone

        LinkCreateur ->
            { model
                | lien =
                    Just
                        { siret = ""
                        , requete = RD.NotAsked
                        , resultEtablissements = RD.NotAsked
                        , etablissement = Nothing
                        }
            }
                |> Effect.withNone

        UpdatedSiret siret ->
            case model.lien of
                Nothing ->
                    model
                        |> Effect.withNone

                Just lien ->
                    ( { model | lien = Just { lien | siret = siret } }, Effect.none )

        RequestedRecherche ->
            case model.lien of
                Nothing ->
                    model
                        |> Effect.withNone

                Just lien ->
                    ( { model
                        | lien =
                            Just
                                { lien
                                    | resultEtablissements = RD.Loading
                                    , etablissement = Nothing
                                }
                      }
                    , searchEtablissement lien.siret
                    )

        ReceivedRecherche resultEtablissements ->
            case model.lien of
                Nothing ->
                    model
                        |> Effect.withNone

                Just lien ->
                    let
                        etablissement =
                            case resultEtablissements of
                                RD.Success (Resultat e) ->
                                    Just e

                                _ ->
                                    Nothing
                    in
                    ( { model
                        | lien =
                            Just
                                { lien
                                    | resultEtablissements = resultEtablissements
                                    , etablissement = etablissement
                                }
                      }
                    , Effect.none
                    )

        CanceledLinkCreateur ->
            { model | lien = Nothing }
                |> Effect.withNone

        ConfirmedLinkCreateur ->
            case ( model.etablissementCreationWithExtraInfo, model.lien ) of
                ( RD.Success etablissementCreationWithExtraInfo, Just lien ) ->
                    case lien.etablissement of
                        Just et ->
                            { model | lien = Just { lien | requete = RD.Loading } }
                                |> Effect.withCmd (lierFiche etablissementCreationWithExtraInfo et.siret)

                        Nothing ->
                            model
                                |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        ReceivedLink result ->
            { model
                | lien =
                    model.lien
                        |> Maybe.map
                            (\lien ->
                                { lien | requete = result }
                            )
                , etablissementCreationWithExtraInfo =
                    case result of
                        RD.Success etablissementCreationWithExtraInfo ->
                            RD.Success etablissementCreationWithExtraInfo

                        _ ->
                            model.etablissementCreationWithExtraInfo
            }
                |> Effect.withNone

        UnlinkCreateur ->
            { model | suppressionTransformation = Just RD.NotAsked }
                |> Effect.withNone

        CanceledUnlinkCreateur ->
            { model | suppressionTransformation = Nothing }
                |> Effect.withNone

        ConfirmedUnlinkCreateur ->
            case model.etablissementCreationWithExtraInfo of
                RD.Success etablissementCreationWithExtraInfo ->
                    { model | suppressionTransformation = Just RD.Loading }
                        |> Effect.withCmd (supprimerLienFiche <| unsafeConvert etablissementCreationWithExtraInfo.etablissementCreation.id)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedUnlink result ->
            { model
                | suppressionTransformation = Just result
                , etablissementCreationWithExtraInfo =
                    case result of
                        RD.Success etablissementCreationWithExtraInfo ->
                            RD.Success etablissementCreationWithExtraInfo

                        _ ->
                            model.etablissementCreationWithExtraInfo
            }
                |> Effect.withNone

        ReceivedCollegue response ->
            { model | devecos = response }
                |> Effect.withNone

        ClickedPartage ->
            { model
                | partageModal =
                    Just
                        { collegueId =
                            model.devecos
                                |> RD.map (List.filter (\collab -> collab.id /= User.id user))
                                |> RD.withDefault []
                                |> List.head
                                |> Maybe.map .id
                        , commentaire = ""
                        , request =
                            RD.NotAsked
                        }
            }
                |> Effect.withNone

        CancelPartage ->
            { model
                | partageModal =
                    Nothing
            }
                |> Effect.withNone

        UpdatedPartageDeveco collegueId ->
            { model
                | partageModal =
                    model.partageModal
                        |> Maybe.map
                            (\p ->
                                { p
                                    | collegueId = collegueId |> parseEntityId
                                    , request = RD.NotAsked
                                }
                            )
            }
                |> Effect.withNone

        UpdatedPartageCommentaire commentaire ->
            { model
                | partageModal =
                    model.partageModal
                        |> Maybe.map (\p -> { p | commentaire = commentaire })
            }
                |> Effect.withNone

        ConfirmPartage ->
            ( { model
                | partageModal =
                    model.partageModal
                        |> Maybe.map (\p -> { p | request = RD.Loading })
              }
            , model.partageModal
                |> Maybe.map (savePartage model.etablissementCreationId)
                |> Maybe.map Effect.fromCmd
                |> Maybe.withDefault Effect.none
            )

        ReceivedPartage response ->
            { model
                | partageModal =
                    model.partageModal
                        |> Maybe.map (\p -> { p | request = response })
            }
                |> Effect.withNone

        ClickedChangementOnglet onglet ->
            let
                suiviOnglet =
                    UI.Suivi.getOngletActif model.suiviModel
            in
            updateUrl ( onglet, suiviOnglet ) model

        UpdateOnglets ( onglet, ongletSuivi ) ->
            let
                ( newModel, effect ) =
                    updateUrl ( onglet, ongletSuivi ) model
            in
            ( newModel
            , effect
            )

        SetContactAction action ->
            let
                cmd =
                    case action of
                        EditContact _ _ ->
                            Task.attempt (\_ -> NoOp) (Dom.focus <| "nouveau-contact-fonction")

                        NewContact { personneExistante } ->
                            Cmd.batch
                                [ Task.attempt (\_ -> NoOp) (Dom.focus <| "nouveau-contact-fonction")
                                , if personneExistante.recherche /= "" then
                                    Task.perform identity (Task.succeed RechercheContactExistant)

                                  else
                                    Cmd.none
                                ]

                        _ ->
                            Cmd.none
            in
            ( { model | contactAction = action }, Effect.fromCmd cmd )

        CancelContactAction ->
            ( { model | contactAction = None }, Effect.none )

        UpdatedContact field value ->
            Effect.withNone <|
                case model.contactAction of
                    None ->
                        model

                    ViewContactFonctions _ ->
                        model

                    DeleteContact _ _ ->
                        model

                    DeleteContactForm _ ->
                        model

                    NewContact contactForm ->
                        { model
                            | contactAction =
                                NewContact <|
                                    updateContactForm field value <|
                                        contactForm
                        }

                    EditContact contactExistant contact ->
                        { model
                            | contactAction =
                                EditContact contactExistant <|
                                    updateContact field value contact
                        }

                    EditContactForm _ _ ->
                        model

        ConfirmContactAction ->
            case model.saveCreateurRequest of
                RD.Loading ->
                    ( model, Effect.none )

                _ ->
                    case model.contactAction of
                        None ->
                            ( model, Effect.none )

                        ViewContactFonctions _ ->
                            ( model, Effect.none )

                        NewContact contactForm ->
                            ( { model | saveCreateurRequest = RD.Loading }, Effect.fromCmd <| createContact model.etablissementCreationId contactForm )

                        DeleteContact contact _ ->
                            ( { model | saveCreateurRequest = RD.Loading }, Effect.fromCmd <| deleteContact model.etablissementCreationId contact )

                        DeleteContactForm _ ->
                            ( model, Effect.none )

                        EditContact _ contact ->
                            ( { model | saveCreateurRequest = RD.Loading }, Effect.fromCmd <| editContact model.etablissementCreationId contact )

                        EditContactForm _ _ ->
                            ( model, Effect.none )

        ReceivedSaveContact response ->
            let
                ( contactAction, etablissementCreation ) =
                    case response of
                        RD.Success _ ->
                            ( None, response )

                        _ ->
                            ( model.contactAction, model.etablissementCreationWithExtraInfo )
            in
            ( { model
                | etablissementCreationWithExtraInfo = etablissementCreation
                , contactAction = contactAction
                , saveCreateurRequest = response
              }
            , Effect.none
            )

        UpdatedRechercheContactExistant recherche ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | recherche = recherche
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm contactFormOriginal contactForm ->
                    ( { model
                        | contactAction =
                            EditContactForm contactFormOriginal <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | recherche = recherche
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

        RechercheContactExistant ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | resultats = RD.Loading
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.fromCmd <|
                        getPersonnes <|
                            Pages.Annuaire.filtersAndPaginationToQuery
                                ( { defaultFilters
                                    | recherche = contactForm.personneExistante.recherche
                                    , elementsParPage = Just 5
                                  }
                                , defaultPagination
                                )
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm contactFormOriginal contactForm ->
                    ( { model
                        | contactAction =
                            EditContactForm contactFormOriginal <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | resultats = RD.Loading
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.fromCmd <|
                        getPersonnes <|
                            Pages.Annuaire.filtersAndPaginationToQuery
                                ( { defaultFilters
                                    | recherche = contactForm.personneExistante.recherche
                                    , elementsParPage = Just 5
                                  }
                                , defaultPagination
                                )
                    )

        ReceivedContactsExistants response ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | resultats = response
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm contactFormOriginal contactForm ->
                    ( { model
                        | contactAction =
                            EditContactForm contactFormOriginal <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | resultats = response
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

        SelectedContactExistant maybeContact ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | selectionne = maybeContact
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm contactFormOriginal contactForm ->
                    ( { model
                        | contactAction =
                            EditContactForm contactFormOriginal <|
                                (\cf ->
                                    { cf
                                        | personneExistante =
                                            cf.personneExistante
                                                |> (\e ->
                                                        { e
                                                            | selectionne = maybeContact
                                                        }
                                                   )
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

        ToggleNouveauContactModeCreation modeCreation ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                ViewContactFonctions _ ->
                    ( model, Effect.none )

                NewContact contactForm ->
                    ( { model
                        | contactAction =
                            NewContact <|
                                (\cf ->
                                    { cf
                                        | modeCreation = modeCreation
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

                DeleteContact _ _ ->
                    ( model, Effect.none )

                DeleteContactForm _ ->
                    ( model, Effect.none )

                EditContact _ _ ->
                    ( model, Effect.none )

                EditContactForm contactFormOriginal contactForm ->
                    ( { model
                        | contactAction =
                            EditContactForm contactFormOriginal <|
                                (\cf ->
                                    { cf
                                        | modeCreation = modeCreation
                                    }
                                )
                                <|
                                    contactForm
                      }
                    , Effect.none
                    )

        ReceivedSources sources ->
            { model | sources = sources }
                |> Effect.withNone

        SetRessourceAction ressourceAction ->
            { model | ressourceAction = ressourceAction }
                |> Effect.withNone

        UpdatedRessource ressourceField ressourceValue ->
            case model.ressourceAction of
                NewRessource ressource ->
                    { model | ressourceAction = NewRessource <| updateRessource ressourceField ressourceValue <| ressource }
                        |> Effect.withNone

                EditRessource ressource ->
                    { model | ressourceAction = EditRessource <| updateRessource ressourceField ressourceValue <| ressource }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        CancelRessourceAction ->
            { model | ressourceAction = NoRessource }
                |> Effect.withNone

        ConfirmRessourceAction ->
            case model.saveRessourceRequest of
                RD.Loading ->
                    ( model, Effect.none )

                _ ->
                    case model.ressourceAction of
                        NoRessource ->
                            ( model, Effect.none )

                        NewRessource ressource ->
                            ( { model | saveRessourceRequest = RD.Loading }, Effect.fromCmd <| createRessource model.etablissementCreationId ressource )

                        DeleteRessource ressource ->
                            ( { model | saveRessourceRequest = RD.Loading }, Effect.fromCmd <| deleteRessource model.etablissementCreationId ressource )

                        EditRessource ressource ->
                            ( { model | saveRessourceRequest = RD.Loading }, Effect.fromCmd <| editRessource model.etablissementCreationId ressource )

        ReceivedSaveRessource response ->
            case model.saveRessourceRequest of
                RD.Loading ->
                    case model.ressourceAction of
                        NoRessource ->
                            model
                                |> Effect.withNone

                        _ ->
                            case response of
                                RD.Success _ ->
                                    { model | etablissementCreationWithExtraInfo = response, saveRessourceRequest = response, ressourceAction = NoRessource }
                                        |> Effect.withNone

                                _ ->
                                    { model | saveRessourceRequest = response }
                                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        ToggleCreationAbandonnee ->
            let
                cmd =
                    case model.etablissementCreationWithExtraInfo of
                        RD.Success { etablissementCreation } ->
                            requestToggleCreationAbandonnee model.etablissementCreationId <|
                                not <|
                                    .creationAbandonnee <|
                                        etablissementCreation

                        _ ->
                            Cmd.none
            in
            ( model, Effect.fromCmd cmd )

        ReceivedToggleCreationAbandonneeResult creationAbandonnee ->
            { model
                | etablissementCreationWithExtraInfo =
                    model.etablissementCreationWithExtraInfo
                        |> RD.map
                            (\ecwei ->
                                { ecwei
                                    | etablissementCreation =
                                        ecwei.etablissementCreation
                                            |> (\ec ->
                                                    { ec
                                                        | creationAbandonnee = creationAbandonnee
                                                    }
                                               )
                                }
                            )
            }
                |> Effect.withNone


lierFiche : EtablissementCreationWithExtraInfo -> String -> Cmd Msg
lierFiche etablissementCreationWithExtraInfo siret =
    post
        { url = Api.transformEtablissementCreation (unsafeConvert etablissementCreationWithExtraInfo.etablissementCreation.id) siret
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedLink
        }
        (Decode.field "etablissementCreation" decodeEtablissementCreationWithExtraInfo)


supprimerLienFiche : EntityId EtablissementCreationId -> Cmd Msg
supprimerLienFiche etablissementCreationId =
    post
        { url = Api.revertTransformEtablissementCreation etablissementCreationId
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedUnlink
        }
        (Decode.field "etablissementCreation" decodeEtablissementCreationWithExtraInfo)


deleteEtablissementCreation : EtablissementCreationWithExtraInfo -> Cmd Msg
deleteEtablissementCreation etablissementCreationWithExtraInfo =
    delete
        { url = Api.getEtablissementCreation <| unsafeConvert <| .id etablissementCreationWithExtraInfo.etablissementCreation
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedDelete
        }
        (Decode.succeed ())


searchEtablissement : String -> Effect.Effect Shared.Msg Msg
searchEtablissement recherche =
    Effect.fromCmd <|
        get
            { url = Api.getEtablissementParSiret recherche
            }
            { toShared = SharedMsg
            , logger = Just LogError
            , handler = ReceivedRecherche
            }
            (Decode.oneOf
                [ Decode.map Resultat <|
                    Decode.field "etablissement" <|
                        decodeEtablissementApercu
                , Decode.map Erreur <|
                    Decode.field "error" <|
                        Decode.string
                ]
            )


view : Shared.User -> Shared.Shared -> Model -> View Msg
view user { now, timezone } model =
    let
        title =
            case model.etablissementCreationWithExtraInfo of
                RD.Success etablissementCreationWithExtraInfo ->
                    "Fiche de " ++ .nom etablissementCreationWithExtraInfo.etablissementCreation

                _ ->
                    "Fiche"
    in
    { title = UI.Layout.pageTitle <| title
    , body = body timezone now user model
    , route = Route.Createur <| ( Nothing, model.etablissementCreationId )
    }


body : Zone -> Posix -> Shared.User -> Model -> List (Html Msg)
body timezone now user model =
    [ div [ class "flex flex-col p-2" ]
        [ div [ class "flex flex-row justify-between" ]
            [ DSFR.Button.new
                { label = "Retour"
                , onClick = Just <| SharedMsg <| Shared.goBack
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.arrowLeftLine
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withAttrs [ class "text-underline" ]
                |> DSFR.Button.view
            , case model.etablissementCreationWithExtraInfo of
                RD.Success { modificationAuteurDate } ->
                    case modificationAuteurDate of
                        Nothing ->
                            nothing

                        Just ( auteurModification, dateModification ) ->
                            let
                                auteur =
                                    if auteurModification == "" then
                                        ""

                                    else
                                        " par " ++ auteurModification
                            in
                            div [ Typo.textSm, class "flex flex-col !mb-0 p-2 text-right" ]
                                [ div [] [ text <| "Dernière modification de la fiche le " ++ Lib.Date.formatShort timezone dateModification ++ auteur ]
                                ]

                _ ->
                    nothing
            ]
        , case model.etablissementCreationWithExtraInfo of
            RD.Loading ->
                div [ class "p-6 sm:p-8 fr-card--white" ]
                    [ DSFR.Icons.System.refreshFill |> DSFR.Icons.iconLG
                    , "Chargement en cours..."
                        |> text
                    ]

            RD.Success etablissementCreationWithExtraInfo ->
                viewEtablissementCreationBlocks timezone now user model etablissementCreationWithExtraInfo

            RD.Failure _ ->
                div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ "Une erreur s'est produite, veuillez recharger la page."
                        |> text
                    ]

            RD.NotAsked ->
                div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ "Une erreur s'est produite, veuillez recharger la page."
                        |> text
                    ]
        ]
    ]


viewEtablissementCreationBlocks : Zone -> Posix -> Shared.User -> Model -> EtablissementCreationWithExtraInfo -> Html Msg
viewEtablissementCreationBlocks timezone now user model ({ etablissementCreation, demandes } as etablissementCreationWithExtraInfo) =
    let
        niveauxDiplome =
            model
                |> .sources
                |> RD.map .niveauxDiplome
                |> RD.withDefault []

        situationsProfessionnelles =
            model
                |> .sources
                |> RD.map .situationsProfessionnelles
                |> RD.withDefault []
    in
    div [ class "flex flex-col gap-4 p-2" ]
        [ Lazy.lazy2 viewDeleteModal etablissementCreationWithExtraInfo model.deleteEtablissementCreation
        , Lazy.lazy viewTransformationModal model.lien
        , Lazy.lazy2 viewRessourcesModal model.saveRessourceRequest model.ressourceAction
        , Lazy.lazy5 viewCreateursModal niveauxDiplome situationsProfessionnelles etablissementCreation.id model.saveCreateurRequest model.contactAction
        , Lazy.lazy2 viewSuppressionTransformationModal etablissementCreationWithExtraInfo model.suppressionTransformation
        , Lazy.lazy3 viewPartageModal
            { noOp = NoOp
            , confirm = ConfirmPartage
            , cancel = CancelPartage
            , updateCollegue = UpdatedPartageDeveco
            , updateCommentaire = UpdatedPartageCommentaire
            }
            (model.devecos |> RD.withDefault [] |> List.filter .actif)
            model.partageModal
        , Accessibility.map SuiviMsg <| Lazy.lazy8 UI.Suivi.viewSuivisModal timezone now user (Just False) model.devecos False demandes model.suiviModel
        , viewEtablissementCreationHeader model.lien model.etablissementCreationId etablissementCreationWithExtraInfo
        , DSFR.Tabs.new
            { changeTabMsg = stringToOnglet >> Maybe.withDefault OngletProjet >> ClickedChangementOnglet
            , tabs =
                [ { id = OngletProjet |> ongletToString
                  , title = OngletProjet |> ongletToDisplay
                  , icon = iconeCreateur
                  , content = viewTabEtablissementCreation model.ressourceAction model.contactAction etablissementCreationWithExtraInfo
                  }
                , { id = OngletSuivi |> ongletToString
                  , title = OngletSuivi |> ongletToDisplay
                  , icon = DSFR.Icons.Communication.discussLine
                  , content = viewSuiviTab timezone now user model etablissementCreationWithExtraInfo
                  }
                ]
            }
            |> DSFR.Tabs.view (ongletToString model.onglet)
        ]


viewEtablissementCreationHeader : Maybe maybeLien -> EntityId EtablissementCreationId -> EtablissementCreationWithExtraInfo -> Html Msg
viewEtablissementCreationHeader maybeLien etablissementCreationId etablissementCreationWithExtraInfo =
    let
        { etablissementCreation, etablissementLie } =
            etablissementCreationWithExtraInfo
    in
    div [ class "flex flex-row justify-between items-start fr-card--white p-8" ]
        [ div [ class "flex flex-col" ]
            [ h1 [ Typo.textBold, Typo.fr_h3, class "!mb-0" ]
                [ displayIconeCreateur
                , text "\u{00A0}"
                , text <|
                    .nom <|
                        etablissementCreation
                ]
            ]
        , div [ class "flex flex-row gap-4 justify-end items-center" ]
            [ UI.Entite.badgeAncienCreateur <|
                List.filterMap identity <|
                    List.singleton etablissementLie
            , UI.EtablissementCreation.badgeCreationAbandonnee etablissementCreationWithExtraInfo.etablissementCreation.creationAbandonnee
            , DSFR.Button.new { label = "Modifier", onClick = Nothing }
                |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
                |> DSFR.Button.withAttrs [ Html.Attributes.id "modifier-createur" ]
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withDisabled (etablissementLie /= Nothing)
                |> (if etablissementLie /= Nothing then
                        identity

                    else
                        DSFR.Button.linkButton <|
                            Route.toUrl <|
                                Route.CreateurEdit etablissementCreationId
                   )
                |> DSFR.Button.view
            , DSFR.Button.dropdownSelector { label = text "Actions", hint = Just "Actions", id = "etablissements-actions" } <|
                [ case etablissementLie of
                    Just _ ->
                        DSFR.Button.new
                            { label = "Supprimer la transformation en Établissement"
                            , onClick = Just UnlinkCreateur
                            }
                            |> DSFR.Button.leftIcon DSFR.Icons.System.closeLine
                            |> DSFR.Button.withDisabled (maybeLien /= Nothing)
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.view

                    Nothing ->
                        DSFR.Button.new
                            { label = "Transformer en Établissement"
                            , onClick = Just LinkCreateur
                            }
                            |> DSFR.Button.leftIcon DSFR.Icons.System.logoutBoxRLine
                            |> DSFR.Button.withDisabled (maybeLien /= Nothing)
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.view
                , DSFR.Button.new
                    { label = "Supprimer le Créateur"
                    , onClick = Just <| ClickedDelete
                    }
                    |> DSFR.Button.leftIcon DSFR.Icons.System.deleteFill
                    |> DSFR.Button.tertiaryNoOutline
                    |> DSFR.Button.withDisabled (etablissementLie /= Nothing)
                    |> DSFR.Button.view
                , DSFR.Button.new
                    { label =
                        if etablissementCreation.creationAbandonnee then
                            "Restaurer la Création"

                        else
                            "Abandonner la Création"
                    , onClick = Just <| ToggleCreationAbandonnee
                    }
                    |> DSFR.Button.leftIcon DSFR.Icons.Business.archiveLine
                    |> DSFR.Button.withDisabled (etablissementLie /= Nothing)
                    |> DSFR.Button.tertiaryNoOutline
                    |> DSFR.Button.view
                , DSFR.Button.new
                    { onClick = Just <| ClickedPartage
                    , label = "Partager la fiche par e-mail"
                    }
                    |> DSFR.Button.leftIcon DSFR.Icons.Business.mailLine
                    |> DSFR.Button.tertiaryNoOutline
                    |> DSFR.Button.view
                ]
            ]
        ]


viewTabEtablissementCreation : RessourceAction -> ContactAction -> EtablissementCreationWithExtraInfo -> Html Msg
viewTabEtablissementCreation ressourceAction contactAction etablissementCreationWithExtraInfo =
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col gap-4 p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy viewEtablissementCreation etablissementCreationWithExtraInfo
            ]
        , div [ class "flex flex-col gap-4 p-4 sm:p-8 fr-card--white" ]
            [ viewCreateursBloc contactAction etablissementCreationWithExtraInfo
            ]
        , div [ class "flex flex-col gap-4 p-4 sm:p-8 fr-card--white" ]
            [ Lazy.lazy4 viewRessourcesHelper (etablissementCreationWithExtraInfo.etablissementLie /= Nothing) SetRessourceAction ressourceAction etablissementCreationWithExtraInfo.ressources
            ]
        ]


viewSuiviTab : Zone -> Posix -> Shared.User -> Model -> EtablissementCreationWithExtraInfo -> Html Msg
viewSuiviTab timezone now user model etablissementCreationWithExtraInfo =
    case etablissementCreationWithExtraInfo.etablissementLie of
        Nothing ->
            Accessibility.map SuiviMsg <| UI.Suivi.viewOngletSuivis timezone now False False user Nothing etablissementCreationWithExtraInfo.echanges etablissementCreationWithExtraInfo.demandes etablissementCreationWithExtraInfo.rappels Nothing model.suiviModel

        Just etablissement ->
            div [ class "flex flex-col gap-8" ]
                [ div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ div [ class "flex flex-row justify-between items-center" ]
                        [ h3 [ Typo.fr_h4, class "!mb-0" ] [ text "Suivi de la relation" ]
                        , DSFR.Button.new
                            { label = "Ajouter un élément de suivi"
                            , onClick = Nothing
                            }
                            |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                            |> DSFR.Button.withDisabled True
                            |> DSFR.Button.view
                        ]
                    ]
                , div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ p []
                        [ text "Les éléments de suivi ont été transférés à l'établissement lié\u{00A0}: "
                        , Typo.link
                            (Route.toUrl <|
                                Route.Etablissement
                                    ( (\q ->
                                        if q == "" then
                                            Nothing

                                        else
                                            Just q
                                      )
                                      <|
                                        Pages.Etablissement.Siret_.ongletsToQuery ( Pages.Etablissement.Siret_.OngletSuivi, UI.Suivi.OngletEchanges )
                                    , etablissement.id
                                    )
                            )
                            []
                            [ text <|
                                .nomAffichage <|
                                    etablissement
                            ]
                        , text "."
                        ]
                    ]
                ]


transformationNotice : Maybe EtablissementLie -> Html msg
transformationNotice maybeEtablissementLie =
    maybeEtablissementLie
        |> Maybe.map
            (\etablissementLie ->
                DSFR.Alert.small
                    { title = Nothing
                    , description =
                        div [ class "flex flex-col gap-4" ]
                            [ p []
                                [ text "Ce créateur a été transformé en établissement\u{00A0}: "
                                , Typo.link (Route.toUrl <| Route.Etablissement ( Nothing, etablissementLie.id ))
                                    []
                                    [ text <|
                                        .nomAffichage <|
                                            etablissementLie
                                    ]
                                , text "."
                                ]
                            , p []
                                [ text "Vous ne pouvez plus modifier cette fiche."
                                ]
                            ]
                    }
                    |> DSFR.Alert.alert Nothing DSFR.Alert.info
            )
        |> Maybe.withDefault nothing


viewTransformationModal : Maybe Link -> Html Msg
viewTransformationModal maybeTransformation =
    let
        ( opened, content, title ) =
            case maybeTransformation of
                Nothing ->
                    ( False, nothing, nothing )

                Just transformation ->
                    ( True
                    , case transformation.requete of
                        RD.Failure _ ->
                            div [ class "flex flex-col gap-4" ]
                                [ DSFR.Alert.medium
                                    { title = "Une erreur s'est produite, veuillez fermer la fenêtre et réessayer."
                                    , description = Nothing
                                    }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.error
                                , [ DSFR.Button.new { label = "OK", onClick = Just CanceledLinkCreateur }
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]

                        RD.Success _ ->
                            div [ class "flex flex-col gap-4" ]
                                [ DSFR.Alert.medium
                                    { title = "Le Créateur a été transformé en Établissement\u{00A0}!"
                                    , description = Nothing
                                    }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.success
                                , [ DSFR.Button.new { label = "Voir l'Établissement", onClick = Nothing }
                                        |> DSFR.Button.linkButton (Route.toUrl <| Route.Etablissement ( Nothing, transformation.siret ))
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]

                        _ ->
                            div [ class "flex flex-col" ]
                                [ p []
                                    [ text
                                        "Toutes les informations de la fiche Créateur vont être intégrées dans la fiche de l'Établissement que vous allez choisir."
                                    ]
                                , p []
                                    [ text
                                        "Cette fiche Créateur ne sera plus éditable, toutes les informations seront désormais consignées dans la fiche Établissement."
                                    ]
                                , div
                                    [ class "flex flex-col gap-8" ]
                                    [ div
                                        [ class "flex flex-col", DSFR.Grid.colSm7, DSFR.Grid.col12 ]
                                        [ DSFR.SearchBar.searchBar
                                            { errors = []
                                            , extraAttrs =
                                                [ Html.Attributes.pattern "(\\s*[0-9]\\s*){14}"
                                                , Html.Attributes.title "Un SIRET (14 chiffres)"
                                                ]
                                            , disabled = String.trim transformation.siret == ""
                                            }
                                            { submitMsg = RequestedRecherche
                                            , buttonLabel = "Rechercher"
                                            , inputMsg = UpdatedSiret
                                            , inputLabel = ""
                                            , inputPlaceholder = Nothing
                                            , inputId = "lien-etablissement-siret-search"
                                            , inputValue = transformation.siret
                                            , hints =
                                                "Un SIRET (14 chiffres)"
                                                    |> text
                                                    |> List.singleton
                                            , fullLabel = Nothing
                                            }
                                        ]
                                    , div [ DSFR.Grid.col7 ]
                                        [ viewEtablissementsList transformation
                                        ]
                                    ]
                                , [ DSFR.Button.new
                                        { label =
                                            if transformation.requete == RD.Loading then
                                                "Transformation en cours..."

                                            else
                                                "Confirmer"
                                        , onClick = Just ConfirmedLinkCreateur
                                        }
                                        |> DSFR.Button.withDisabled (transformation.siret == "" || transformation.etablissement == Nothing || transformation.requete == RD.Loading)
                                  , DSFR.Button.new { label = "Annuler", onClick = Just CanceledLinkCreateur }
                                        |> DSFR.Button.tertiaryNoOutline
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.iconsLeft
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]
                    , text <| "Transférer les informations à une fiche Établissement"
                    )
    in
    DSFR.Modal.view
        { id = "lien"
        , label = "lien"
        , openMsg = NoOp
        , closeMsg = Nothing
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


viewSuppressionTransformationModal : EtablissementCreationWithExtraInfo -> Maybe (WebData EtablissementCreationWithExtraInfo) -> Html Msg
viewSuppressionTransformationModal etablissementCreationWithExtraInfo maybeLien =
    let
        ( opened, content, title ) =
            case maybeLien of
                Nothing ->
                    ( False, nothing, nothing )

                Just requete ->
                    ( True
                    , case requete of
                        RD.Failure _ ->
                            div [ class "flex flex-col gap-4" ]
                                [ DSFR.Alert.medium
                                    { title = "Une erreur s'est produite, veuillez fermer la fenêtre et réessayer."
                                    , description = Nothing
                                    }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.error
                                , [ DSFR.Button.new { label = "OK", onClick = Just CanceledUnlinkCreateur }
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]

                        RD.Success _ ->
                            div [ class "flex flex-col gap-4" ]
                                [ DSFR.Alert.medium
                                    { title = "La transformation en Établissement a été supprimée\u{00A0}!"
                                    , description = Nothing
                                    }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.success
                                , [ DSFR.Button.new { label = "OK", onClick = Just CanceledUnlinkCreateur }
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]

                        _ ->
                            div [ class "flex flex-col" ]
                                [ p []
                                    [ text
                                        "Voulez-vous vraiment supprimer le lien entre le Créateur"
                                    , text <| " "
                                    , text <|
                                        .nom <|
                                            .etablissementCreation <|
                                                etablissementCreationWithExtraInfo
                                    , text " et l'Établissement"
                                    , span [ Typo.textBold ]
                                        [ text <|
                                            Maybe.withDefault "" <|
                                                Maybe.map (\t -> " " ++ t) <|
                                                    Maybe.map .nomAffichage <|
                                                        etablissementCreationWithExtraInfo.etablissementLie
                                        ]
                                    , text "\u{00A0}?"
                                    ]
                                , [ DSFR.Button.new
                                        { label =
                                            if requete == RD.Loading then
                                                "Suppression du lien en cours..."

                                            else
                                                "Confirmer"
                                        , onClick = Just ConfirmedUnlinkCreateur
                                        }
                                        |> DSFR.Button.withDisabled (requete == RD.Loading)
                                  , DSFR.Button.new { label = "Annuler", onClick = Just CanceledUnlinkCreateur }
                                        |> DSFR.Button.tertiaryNoOutline
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.iconsLeft
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]
                    , text <| "Supprimer le lien du Créateur " ++ .nom etablissementCreationWithExtraInfo.etablissementCreation ++ "\u{00A0}?"
                    )
    in
    DSFR.Modal.view
        { id = "suppressionLien"
        , label = "suppressionLien"
        , openMsg = NoOp
        , closeMsg = Nothing
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


viewDeleteModal : EtablissementCreationWithExtraInfo -> Maybe (WebData ()) -> Html Msg
viewDeleteModal etablissementCreationWithExtraInfo deleteRequest =
    let
        ( opened, content, title ) =
            case deleteRequest of
                Nothing ->
                    ( False, nothing, nothing )

                Just request ->
                    ( True
                    , case request of
                        RD.NotAsked ->
                            div [ class "flex flex-col gap-4" ]
                                [ DSFR.Alert.medium
                                    { title = "Êtes-vous sûr(e) de vouloir supprimer la fiche\u{00A0}?"
                                    , description = Just <| text "Cette opération est définitive et irréversible."
                                    }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.warning
                                , [ DSFR.Button.new { label = "Je veux vraiment supprimer la fiche", onClick = Just ConfirmedDelete }
                                        |> DSFR.Button.leftIcon DSFR.Icons.System.deleteFill
                                  , DSFR.Button.new { label = "Annuler", onClick = Just CanceledDelete }
                                        |> DSFR.Button.tertiaryNoOutline
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.iconsLeft
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]

                        RD.Loading ->
                            DSFR.Alert.medium
                                { title = "Suppression en cours..."
                                , description = Nothing
                                }
                                |> DSFR.Alert.alert Nothing DSFR.Alert.info

                        RD.Failure _ ->
                            div [ class "flex flex-col gap-4" ]
                                [ DSFR.Alert.medium
                                    { title = "Une erreur s'est produite, veuillez fermer la fenêtre et réessayer."
                                    , description = Nothing
                                    }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.error
                                , [ DSFR.Button.new { label = "OK", onClick = Just CanceledDelete }
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]

                        RD.Success () ->
                            div [ class "flex flex-col gap-4" ]
                                [ DSFR.Alert.medium
                                    { title = "La fiche a été supprimée avec succès\u{00A0}!"
                                    , description = Just <| text "Cliquez sur OK pour retourner à la liste des fiches."
                                    }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.success
                                , [ DSFR.Button.new { label = "OK", onClick = Nothing }
                                        |> DSFR.Button.linkButton (Route.toUrl <| Route.Createurs Nothing)
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]
                    , text <| "Supprimer la fiche " ++ .nom etablissementCreationWithExtraInfo.etablissementCreation ++ "\u{00A0}?"
                    )
    in
    DSFR.Modal.view
        { id = "suppression"
        , label = "suppression"
        , openMsg = NoOp
        , closeMsg = Nothing
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


viewEtablissementCreation : EtablissementCreationWithExtraInfo -> Html Msg
viewEtablissementCreation { etablissementCreation, projetType, secteurActivite, sourceFinancement, formeJuridique, commentaire, demandes, description, etablissementLie, etiquettes, contactOrigine, contactDate, communes, contratAccompagnement, rechercheLocal } =
    div []
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col ]
                []
            ]
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col ]
                [ transformationNotice etablissementLie
                ]
            ]
        , div [ class "pb-8" ] []
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col9 ]
                [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ div [ DSFR.Grid.col ]
                        [ div [ DSFR.Grid.gridRow, class "fr-card--grey" ]
                            [ div [ DSFR.Grid.col7, class "p-2" ]
                                [ div [ class "p-2 pb-0" ]
                                    [ div [ Typo.textBold ] [ text "Projet" ]
                                    , hr [ class "fr-hr" ] []
                                    ]
                                , div [ class "px-2" ]
                                    [ div [ Typo.textBold ] [ text "Type de projet" ]
                                    , div []
                                        [ text <|
                                            withEmptyAs "-" <|
                                                String.trim <|
                                                    Maybe.withDefault "" <|
                                                        Maybe.map .nom <|
                                                            projetType
                                        ]
                                    ]
                                , div [ class "p-2" ]
                                    [ div [ Typo.textBold ] [ text "Nom d'enseigne pressenti" ]
                                    , div []
                                        [ text <|
                                            withEmptyAs "-" <|
                                                etablissementCreation.futureEnseigne
                                        ]
                                    ]
                                , div [ class "p-2" ]
                                    [ div [ Typo.textBold ] [ text "Description du projet" ]
                                    , div [ class "whitespace-pre-wrap" ]
                                        [ text <|
                                            withEmptyAs "-" <|
                                                description
                                        ]
                                    ]
                                , div [ class "p-2" ]
                                    [ div [ Typo.textBold ] [ text "Secteur d'activité" ]
                                    , div []
                                        [ text <|
                                            withEmptyAs "-" <|
                                                Maybe.withDefault "" <|
                                                    Maybe.map .nom <|
                                                        secteurActivite
                                        ]
                                    ]
                                , div [ class "p-2" ]
                                    [ div [ Typo.textBold ] [ text "Forme juridique envisagée" ]
                                    , div []
                                        [ text <|
                                            withEmptyAs "-" <|
                                                formeJuridique
                                        ]
                                    ]
                                , div [ class "p-2" ]
                                    [ div [ Typo.textBold ] [ text "Source de financement" ]
                                    , div []
                                        [ text <|
                                            withEmptyAs "-" <|
                                                Maybe.withDefault "" <|
                                                    Maybe.map .nom <|
                                                        sourceFinancement
                                        ]
                                    ]
                                , div [ class "p-2" ]
                                    [ div [ Typo.textBold ] [ text "Contrat d'accompagnement" ]
                                    , div []
                                        [ text <|
                                            withEmptyAs "-" <|
                                                Maybe.withDefault "" <|
                                                    Maybe.map
                                                        (\ca ->
                                                            if ca then
                                                                "Oui"

                                                            else
                                                                "Non"
                                                        )
                                                    <|
                                                        contratAccompagnement
                                        ]
                                    ]
                                , viewProjetCommunes communes
                                , div [ class "p-2" ]
                                    [ div [ Typo.textBold ] [ text "Recherche d'un local" ]
                                    , div []
                                        [ text <|
                                            withEmptyAs "-" <|
                                                Maybe.withDefault "" <|
                                                    Maybe.map
                                                        (\rl ->
                                                            if rl then
                                                                "Oui"

                                                            else
                                                                "Non"
                                                        )
                                                    <|
                                                        rechercheLocal
                                        ]
                                    ]

                                -- , viewPersonneContact personne adresse
                                -- , viewPersonneZonage zonagesPrioritaires
                                ]
                            , div [ DSFR.Grid.col5, class "flex flex-col p-4 justify-between" ] <|
                                [ div []
                                    [ div [ Typo.textBold ] [ text "Étiquettes" ]
                                    , hr [ class "fr-hr" ] []
                                    , viewProjetQualifications commentaire etiquettes
                                    ]
                                , div []
                                    [ div [ Typo.textBold ] [ text "Informations complémentaires" ]
                                    , hr [ class "fr-hr" ] []
                                    , viewOrigineContact contactOrigine contactDate
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            , div [ DSFR.Grid.col3 ]
                [ viewFicheDemandes demandes
                ]
            ]
        ]


viewProjetCommunes : List Commune -> Html msg
viewProjetCommunes communes =
    div [ class "p-2" ]
        [ div [ Typo.textBold ]
            [ text "Commune"
            , text <| plural <| List.length communes
            , text " "
            , text "d'implantation"
            ]
        , text <|
            case communes of
                [] ->
                    "-"

                _ ->
                    communes
                        |> List.sortBy .nom
                        |> List.map .nom
                        |> String.join ", "
        ]



-- viewPersonneContact : { a | email : String, telephone : String } -> Maybe Adresse -> Html msg
-- viewPersonneContact personne adresse =
--     let
--         adresseString =
--             Maybe.withDefault "" <|
--                 Maybe.map adresseToString <|
--                     adresse
--     in
--     div [ class "p-2" ]
--         [ div [ Typo.textBold ] [ text "Contact" ]
--         , viewIf (List.all ((==) "") <| List.map String.trim <| [ adresseString, personne.email, personne.telephone ]) <| text "-"
--         , viewIf (adresseString /= "") <| div [] [ text adresseString ]
--         , viewIf (personne.email /= "" || personne.telephone /= "") <|
--             div []
--                 [ div [ class "py-4" ]
--                     [ viewIf (personne.email /= "") <| div [] [ text personne.email ]
--                     , viewIf (personne.telephone /= "") <| div [] [ text <| displayPhone personne.telephone ]
--                     ]
--                 ]
--         ]
-- viewPersonneZonage : List String -> Html msg
-- viewPersonneZonage zonages =
--     div [ class "p-2" ]
--         [ div [ Typo.textBold ] [ text "Zonage prioritaire" ]
--         , div [] [ text <| withEmptyAs "-" <| String.join ", " <| zonages ]
--         ]


viewProjetQualifications : String -> Etiquettes -> Html msg
viewProjetQualifications commentaire etiquettes =
    div [ class "flex flex-col gap-4" ]
        [ div []
            [ div [ Typo.textBold, class "flex flex-row justify-between items-center" ]
                [ span [] [ text "Activités réelles et filières", sup [ Html.Attributes.title hintActivites ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                ]
            , case etiquettes.activites of
                [] ->
                    text "-"

                activitesReelles ->
                    activitesReelles
                        |> List.map
                            ((\t -> { data = t, toString = identity })
                                >> DSFR.Tag.clickable ""
                            )
                        |> DSFR.Tag.medium
            ]
        , div []
            [ div [ Typo.textBold ] [ span [] [ text "Zone géographique", sup [ Html.Attributes.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ] ]
            , case etiquettes.localisations of
                [] ->
                    text "-"

                localisations ->
                    localisations
                        |> List.map
                            ((\t -> { data = t, toString = identity })
                                >> DSFR.Tag.clickable ""
                            )
                        |> DSFR.Tag.medium
            ]
        , div []
            [ div [ Typo.textBold ] [ span [] [ text "Mots-clés", sup [ Html.Attributes.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ] ]
            , case etiquettes.motsCles of
                [] ->
                    text "-"

                motsCles ->
                    motsCles
                        |> List.map
                            ((\t -> { data = t, toString = identity })
                                >> DSFR.Tag.clickable ""
                            )
                        |> DSFR.Tag.medium
            ]
        , div []
            [ div [ Typo.textBold ] [ text "Commentaires" ]
            , div [ class "whitespace-pre-wrap" ]
                [ text <|
                    withEmptyAs "-" <|
                        commentaire
                ]
            ]
        ]


viewOrigineContact : Maybe GenericSource -> Maybe Date -> Html msg
viewOrigineContact contactOrigine contactDate =
    div []
        [ div [ Typo.textBold ] [ text "Origine du contact" ]
        , div [ class "whitespace-pre-wrap" ]
            [ text <|
                Maybe.withDefault "-" <|
                    Maybe.map .nom <|
                        contactOrigine
            ]
        , div [ Typo.textBold ] [ text "Date du contact" ]
        , div [ class "whitespace-pre-wrap" ]
            [ text <|
                withEmptyAs "-" <|
                    Maybe.withDefault "" <|
                        Maybe.map formatDateShort <|
                            contactDate
            ]
        ]


viewFicheDemandes : List Data.Demande.Demande -> Html msg
viewFicheDemandes demandes =
    let
        openDemandes =
            demandes
                |> List.filter (Data.Demande.cloture >> not)
    in
    div [ class "p-2" ]
        [ div [ Typo.textBold ]
            [ text "Demande"
            , text <|
                if List.length openDemandes > 1 then
                    "s"

                else
                    ""
            , text " en cours"
            ]
        , hr [ class "!pb-4" ] []
        , case openDemandes of
            [] ->
                span [ class "italic" ] [ text "Aucune demande en cours." ]

            ds ->
                ds
                    |> List.map
                        (Data.Demande.label
                            >> (\t -> { data = t, toString = identity })
                            >> DSFR.Tag.unclickable
                        )
                    |> DSFR.Tag.medium
        ]


viewEtablissementsList : Link -> Html Msg
viewEtablissementsList { resultEtablissements, etablissement } =
    case resultEtablissements of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            text "Recherche en cours..."

        RD.Failure _ ->
            div [ class "flex flex-col gap-2" ]
                [ div []
                    [ text "Pas de résultats."
                    ]
                , div []
                    [ p []
                        [ text
                            "Il peut y avoir un décalage entre votre connaissance de la création d'un établissement et sa mise à jour de la base SIRENE. Veuillez réessayer ultérieurement."
                        ]
                    ]
                ]

        RD.Success (Resultat _) ->
            div [ class "flex flex-col gap-2" ]
                [ div []
                    [ text "Résultat\u{00A0}:"
                    ]
                , etablissement
                    |> Maybe.map viewEtablissementCard
                    |> Maybe.withDefault (text "Aucun résultat")
                ]

        RD.Success (Erreur erreur) ->
            div [ class "flex flex-col gap-2" ]
                [ div []
                    [ text "Résultat\u{00A0}:"
                    ]
                , text erreur
                ]


viewEtablissementCard : EtablissementApercu -> Html msg
viewEtablissementCard { nomAffichage, siret, adresse, etatAdministratif, clotureDateContribution, siege } =
    div [ class "flex flex-col" ]
        [ div [ class "font-bold flex flex-row gap-4 items-baseline" ]
            [ text nomAffichage
            , etatAdministratif
                |> Just
                |> UI.Entite.badgeInactif (not <| Nothing == clotureDateContribution)
            , UI.Entite.badgeSiege <|
                siege
            ]
        , div []
            [ text "SIREN\u{00A0}: "
            , span [ class "font-bold" ] [ text <| String.dropRight 5 <| siret ]
            ]
        , div []
            [ text "SIRET\u{00A0}: "
            , span [ class "font-bold" ] [ text siret ]
            ]
        , div []
            [ text "Adresse\u{00A0}: "
            , span [ class "font-bold" ] [ text adresse ]
            ]
        ]


savePartage : EntityId EtablissementCreationId -> Partage -> Cmd Msg
savePartage etablissementCreationId { collegueId, commentaire } =
    post
        { url = Api.partageEtablissementCreation etablissementCreationId
        , body =
            [ ( "compteId", Maybe.withDefault Encode.null <| Maybe.map Api.EntityId.encodeEntityId <| collegueId )
            , ( "commentaire", Encode.string commentaire )
            ]
                |> Encode.object
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedPartage
        }
        (Decode.succeed ())


queryKeys :
    { onglet : String
    , suivi : String
    }
queryKeys =
    { onglet = "onglet"
    , suivi = "suivi"
    }


queryToOnglets : Maybe String -> ( Onglet, UI.Suivi.Onglet )
queryToOnglets rawQuery =
    rawQuery
        |> Maybe.withDefault ""
        |> QS.parse QS.config
        |> (\query ->
                let
                    onglet =
                        query
                            |> QS.getAsStringList queryKeys.onglet
                            |> List.head
                            |> Maybe.andThen stringToOnglet
                            |> Maybe.withDefault OngletProjet

                    ongletSuivi =
                        query
                            |> QS.getAsStringList queryKeys.suivi
                            |> List.head
                            |> Maybe.andThen UI.Suivi.stringToOnglet
                            |> Maybe.withDefault UI.Suivi.OngletEchanges
                in
                ( onglet, ongletSuivi )
           )


ongletsToQuery : ( Onglet, UI.Suivi.Onglet ) -> String
ongletsToQuery ( onglet, ongletSuivi ) =
    let
        setOnglet =
            case onglet of
                OngletProjet ->
                    identity

                _ ->
                    onglet
                        |> ongletToString
                        |> QS.setStr queryKeys.onglet

        setOngletSuivi =
            case onglet of
                OngletSuivi ->
                    ongletSuivi
                        |> UI.Suivi.ongletToString
                        |> QS.setStr queryKeys.suivi

                _ ->
                    identity
    in
    QS.empty
        |> setOnglet
        |> setOngletSuivi
        |> QS.serialize (QS.config |> QS.addQuestionMark False)


updateUrl : ( Onglet, UI.Suivi.Onglet ) -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
updateUrl ( onglet, ongletSuivi ) model =
    let
        ( newSuiviModel, suiviCmd, _ ) =
            UI.Suivi.changeOnglet (UI.Suivi.suiviConfigEtabCrea (Decode.field "etablissementCreation" decodeEtablissementCreationWithExtraInfo) model.etablissementCreationId) model.suiviModel ongletSuivi

        newOngletSuivi =
            UI.Suivi.getOngletActif newSuiviModel

        nextUrl =
            ongletsToQuery ( onglet, newOngletSuivi )

        requestChanged =
            nextUrl /= model.currentUrl

        ongletCmd =
            Effect.fromShared <|
                Shared.Navigate <|
                    Route.Createur <|
                        ( (\q ->
                            if q == "" then
                                Nothing

                            else
                                Just q
                          )
                          <|
                            nextUrl
                        , model.etablissementCreationId
                        )
    in
    if requestChanged then
        ( { model
            | onglet = onglet
            , suiviModel = newSuiviModel
            , currentUrl = nextUrl
          }
        , Effect.batch
            [ ongletCmd
            , Effect.map SuiviMsg suiviCmd
            ]
        )

    else
        model
            |> Effect.withNone


viewCreateursBloc : ContactAction -> EtablissementCreationWithExtraInfo -> Html Msg
viewCreateursBloc contactAction etabCrea =
    div [ class "flex flex-col gap-4" ]
        [ Lazy.lazy5 UI.Contact.viewContactsHelper
            (etabCrea.etablissementLie /= Nothing)
            (Just <|
                FonctionCreateur
                    { fonction = "Créateur"
                    , niveauDiplome = ""
                    , situationProfessionnelle = ""
                    }
            )
            SetContactAction
            contactAction
            (etabCrea.etablissementCreation.createurs |> (\( c, l ) -> c :: l))
        ]


getPersonnes : String -> Cmd Msg
getPersonnes query =
    get
        { url = Api.getContacts query }
        { toShared = SharedMsg
        , logger = Nothing
        , handler = ReceivedContactsExistants
        }
        (Decode.field "elements" <| Decode.list <| decodePersonne)


viewCreateursModal : List { id : String, nom : String } -> List { id : String, nom : String } -> EntityId EtablissementCreationId -> WebData entity -> ContactAction -> Html Msg
viewCreateursModal niveauxDiplome situationsProfessionnelles etablissementCreationId saveCreateurRequest contactAction =
    let
        ( opened, content, title ) =
            case contactAction of
                None ->
                    ( False, nothing, nothing )

                ViewContactFonctions fonctions ->
                    let
                        autresFonctions =
                            fonctions
                                |> List.filter
                                    (\f ->
                                        case f of
                                            CCreateur cd ->
                                                cd.etablissementCreationId /= etablissementCreationId

                                            _ ->
                                                True
                                    )
                    in
                    ( True, UI.Contact.viewFonctions SetContactAction autresFonctions, text "Autres fonctions du créateur" )

                NewContact contactForm ->
                    ( True
                    , viewNewContactFormFields
                        { confirm = ConfirmContactAction
                        , update = UpdatedContact
                        , cancel = CancelContactAction
                        , toggle = ToggleNouveauContactModeCreation
                        , confirmSearch = RechercheContactExistant
                        , updateSearch = UpdatedRechercheContactExistant
                        , selectedPersonne = SelectedContactExistant
                        , niveauxDiplome = Just niveauxDiplome
                        , situationsProfessionnelles = Just situationsProfessionnelles
                        }
                        saveCreateurRequest
                        contactForm
                    , text "Ajouter un créateur"
                    )

                EditContact contactExistant contact ->
                    ( True
                    , viewEditContact
                        { confirm = ConfirmContactAction
                        , update = UpdatedContact
                        , cancel = CancelContactAction
                        , niveauxDiplome = Just niveauxDiplome
                        , situationsProfessionnelles = Just situationsProfessionnelles
                        }
                        saveCreateurRequest
                        contactExistant
                        contact
                    , text "Modifier un créateur"
                    )

                DeleteContact contact _ ->
                    ( True
                    , viewDeleteContactBody
                        []
                        { confirm = ConfirmContactAction
                        , cancel = CancelContactAction
                        }
                        saveCreateurRequest
                        contact.personne.id
                    , text "Supprimer un créateur"
                    )

                DeleteContactForm _ ->
                    ( False, nothing, nothing )

                EditContactForm _ _ ->
                    ( False, nothing, nothing )
    in
    DSFR.Modal.view
        { id = "createur"
        , label = "createur"
        , openMsg = NoOp
        , closeMsg = Just CancelContactAction
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.md
        }
        content
        Nothing
        |> Tuple.first


createContact : EntityId EtablissementCreationId -> NewContactForm -> Cmd Msg
createContact etablissementCreationId newContactForm =
    let
        httpBody =
            encodeNewContactForm newContactForm
    in
    post
        { url = Api.createEtablissementCreationContact etablissementCreationId
        , body = httpBody |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveContact
        }
        (Decode.field "etablissementCreation" decodeEtablissementCreationWithExtraInfo)


editContact : EntityId EtablissementCreationId -> Contact -> Cmd Msg
editContact etablissementCreationId contact =
    post
        { url = Api.getEtablissementCreationContact etablissementCreationId contact.personne.id
        , body =
            contact
                |> Data.Contact.encodeContact ContactUpdateInput
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveContact
        }
        (Decode.field "etablissementCreation" decodeEtablissementCreationWithExtraInfo)


deleteContact : EntityId EtablissementCreationId -> Contact -> Cmd Msg
deleteContact etablissementCreationId { personne } =
    delete
        { url = Api.getEtablissementCreationContact etablissementCreationId personne.id
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveContact
        }
        (Decode.field "etablissementCreation" decodeEtablissementCreationWithExtraInfo)


getSources : Cmd Msg
getSources =
    get
        { url = Api.getCreateurSources }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSources
        }
        decodeSources


decodeSources : Decoder Sources
decodeSources =
    Decode.succeed Sources
        |> andMap (Decode.field "typesProjet" <| Decode.list <| decodeGenericSource)
        |> andMap (Decode.field "contactOrigines" <| Decode.list <| decodeGenericSource)
        |> andMap (Decode.field "secteursActivite" <| Decode.list <| decodeGenericSource)
        |> andMap (Decode.field "sourcesFinancement" <| Decode.list <| decodeGenericSource)
        |> andMap (Decode.field "niveauxDiplome" <| Decode.list <| decodeGenericSource)
        |> andMap (Decode.field "situationsProfessionnelles" <| Decode.list <| decodeGenericSource)


type alias Sources =
    { projetTypes : List GenericSource
    , contactOrigines : List GenericSource
    , secteursActivite : List GenericSource
    , sourcesFinancement : List GenericSource
    , niveauxDiplome : List GenericSource
    , situationsProfessionnelles : List GenericSource
    }


createRessource : EntityId EtablissementCreationId -> Ressource -> Cmd Msg
createRessource id ressource =
    post
        { url = Api.createRessourceEtablissementCreation id
        , body =
            ressource
                |> encodeRessource
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveRessource
        }
        (Decode.field "etablissementCreation" decodeEtablissementCreationWithExtraInfo)


deleteRessource : EntityId EtablissementCreationId -> Ressource -> Cmd Msg
deleteRessource id ressource =
    delete
        { url = Api.editRessourceEtablissementCreation id ressource.id
        , body = Http.emptyBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveRessource
        }
        (Decode.field "etablissementCreation" decodeEtablissementCreationWithExtraInfo)


editRessource : EntityId EtablissementCreationId -> Ressource -> Cmd Msg
editRessource id ressource =
    post
        { url = Api.editRessourceEtablissementCreation id ressource.id
        , body =
            ressource
                |> encodeRessource
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveRessource
        }
        (Decode.field "etablissementCreation" decodeEtablissementCreationWithExtraInfo)


viewRessourcesModal : WebData EtablissementCreationWithExtraInfo -> RessourceAction -> Html Msg
viewRessourcesModal saveRessourceRequest ressourceAction =
    let
        ( opened, content, title ) =
            case ressourceAction of
                NoRessource ->
                    ( False, nothing, nothing )

                NewRessource ressource ->
                    ( True
                    , viewRessourceFormFields True saveRessourceRequest ressource
                    , text "Ajouter une ressource"
                    )

                EditRessource ressource ->
                    ( True
                    , viewRessourceFormFields False saveRessourceRequest ressource
                    , text "Modifier une ressource"
                    )

                DeleteRessource ressource ->
                    ( True
                    , div []
                        [ text "Êtes-vous sûr(e) de vouloir supprimer cette ressource\u{00A0}?"
                        , div [ DSFR.Grid.col12, class "flex flex-col !pt-4" ]
                            [ [ DSFR.Button.new { onClick = Just <| ConfirmRessourceAction, label = "Supprimer" }
                                    |> DSFR.Button.withDisabled (saveRessourceRequest == RD.Loading)
                                    |> DSFR.Button.withAttrs
                                        [ Html.Attributes.id <| "confirmer-modifier-ressource-" ++ String.fromInt ressource.id
                                        , Html.Attributes.title "Supprimer"
                                        ]
                              , DSFR.Button.new { onClick = Just <| CancelRessourceAction, label = "Annuler" }
                                    |> DSFR.Button.withAttrs
                                        [ Html.Attributes.id <| "annuler-modifier-ressource-" ++ String.fromInt ressource.id
                                        , Html.Attributes.title "Annuler"
                                        ]
                                    |> DSFR.Button.secondary
                              ]
                                |> DSFR.Button.group
                                |> DSFR.Button.inline
                                |> DSFR.Button.alignedRightInverted
                                |> DSFR.Button.viewGroup
                            ]
                        , div [ class "flex flex-row justify-end w-full" ]
                            [ case saveRessourceRequest of
                                RD.Failure _ ->
                                    DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite" }
                                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                                _ ->
                                    nothing
                            ]
                        ]
                    , text "Supprimer une ressource"
                    )
    in
    DSFR.Modal.view
        { id = "ressource"
        , label = "ressource"
        , openMsg = NoOp
        , closeMsg = Just CancelRessourceAction
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.md
        }
        content
        Nothing
        |> Tuple.first


viewRessourceFormFields : Bool -> WebData EtablissementCreationWithExtraInfo -> Ressource -> Html Msg
viewRessourceFormFields new request ressource =
    let
        ressourceIsEmpty =
            ressource.lien == ""
    in
    formWithListeners [ Events.onSubmit <| ConfirmRessourceAction, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12 ]
            [ DSFR.Input.new { value = ressourceTypeToString ressource.type_, onInput = UpdatedRessource RType, label = text "Type", name = "ressource-type" }
                |> DSFR.Input.select
                    { options = ressourceTypes
                    , toId = ressourceTypeToString
                    , toLabel = ressourceTypeToDisplay >> text
                    , toDisabled = Nothing
                    }
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col6 ]
            [ DSFR.Input.new { value = ressource.nom, onInput = UpdatedRessource RNom, label = text "Nom", name = "ressource-nom" }
                |> DSFR.Input.withRequired True
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col6 ]
            [ DSFR.Input.new { value = ressource.lien, onInput = UpdatedRessource RLien, label = text "Lien", name = "ressource-lien" }
                |> DSFR.Input.withRequired True
                |> DSFR.Input.view
            ]
        , let
            label =
                if new then
                    "Ajouter"

                else
                    "Modifier"
          in
          div [ DSFR.Grid.col12, class "flex flex-col !pt-4" ]
            [ [ DSFR.Button.new { onClick = Nothing, label = label }
                    |> DSFR.Button.withDisabled (ressourceIsEmpty || request == RD.Loading)
                    |> DSFR.Button.submit
                    |> DSFR.Button.withAttrs
                        [ Html.Attributes.id <| "confirmer-modifier-ressource-" ++ String.fromInt ressource.id
                        , Html.Attributes.title label
                        ]
              , DSFR.Button.new { onClick = Just <| CancelRessourceAction, label = "Annuler" }
                    |> DSFR.Button.secondary
                    |> DSFR.Button.withAttrs
                        [ Html.Attributes.id <| "annuler-modifier-ressource-" ++ String.fromInt ressource.id
                        , Html.Attributes.title "Annuler"
                        ]
              ]
                |> DSFR.Button.group
                |> DSFR.Button.inline
                |> DSFR.Button.alignedRightInverted
                |> DSFR.Button.viewGroup
            ]
        , div [ class "flex flex-row justify-end w-full" ]
            [ case request of
                RD.Failure _ ->
                    DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite" }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                _ ->
                    nothing
            ]
        ]


requestToggleCreationAbandonnee : EntityId EtablissementCreationId -> Bool -> Cmd Msg
requestToggleCreationAbandonnee etabCreaId creationAbandonnee =
    post
        { url = Api.toggleEtabCreaCreationAbandonnee etabCreaId
        , body =
            [ ( "creationAbandonnee", Encode.bool creationAbandonnee )
            ]
                |> Encode.object
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler =
            \webdata ->
                ReceivedToggleCreationAbandonneeResult <|
                    case webdata of
                        RD.Success _ ->
                            creationAbandonnee

                        _ ->
                            not creationAbandonnee
        }
        (Decode.succeed ())
