module Pages.EtablissementCreation.Data exposing (EtablissementCreationWithExtraInfo, EtablissementLie, decodeEtablissementCreationWithExtraInfo)

import Data.Commune exposing (Commune, decodeCommune)
import Data.Demande exposing (Demande, decodeDemande)
import Data.Echange exposing (Echange, decodeEchange)
import Data.Etablissement exposing (Siret)
import Data.EtablissementCreation exposing (EtablissementCreation, decodeEtablissementCreation)
import Data.Etiquette exposing (Etiquettes, decodeEtiquettes)
import Data.GenericSource exposing (GenericSource, decodeGenericSource)
import Data.Rappel exposing (Rappel, decodeRappel)
import Data.Ressource exposing (Ressource, decodeRessource)
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Lib.Date exposing (decodeCalendarDate)
import Time exposing (Posix)


type alias EtablissementCreationWithExtraInfo =
    { etablissementCreation : EtablissementCreation
    , description : String
    , commentaire : String
    , etiquettes : Etiquettes
    , echanges : List Echange
    , rappels : List Rappel
    , demandes : List Demande
    , zonagesPrioritaires : List String
    , etablissementLie : Maybe EtablissementLie
    , modificationAuteurDate : Maybe ( String, Posix )
    , communes : List Commune
    , contactOrigine : Maybe GenericSource
    , contactDate : Maybe Date
    , projetType : Maybe GenericSource
    , secteurActivite : Maybe GenericSource
    , sourceFinancement : Maybe GenericSource
    , formeJuridique : String
    , contratAccompagnement : Maybe Bool
    , rechercheLocal : Maybe Bool
    , ressources : List Ressource
    }


type alias EtablissementLie =
    { id : Siret
    , nomAffichage : String
    }


decodeEtablissementCreationWithExtraInfo : Decoder EtablissementCreationWithExtraInfo
decodeEtablissementCreationWithExtraInfo =
    Decode.succeed EtablissementCreationWithExtraInfo
        |> andMap decodeEtablissementCreation
        |> andMap (Decode.field "description" <| Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.string)
        |> andMap (Decode.field "commentaire" <| Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.string)
        |> andMap (Decode.field "etiquettes" <| decodeEtiquettes)
        |> andMap (Decode.field "echanges" <| Decode.list decodeEchange)
        |> andMap (Decode.field "rappels" <| Decode.list decodeRappel)
        |> andMap (Decode.field "demandes" <| Decode.list decodeDemande)
        |> andMap (Decode.field "zonagesPrioritaires" <| Decode.list Decode.string)
        |> andMap
            (Decode.field "etablissement" <|
                Decode.nullable <|
                    decodeEtablissementLie
            )
        |> andMap
            (Decode.field "modification" <|
                Decode.nullable <|
                    Decode.map2 Tuple.pair
                        (Decode.field "compte" <|
                            Decode.map3
                                (\prenom nom email ->
                                    if prenom /= "" && nom /= "" then
                                        prenom ++ " " ++ nom

                                    else if email /= "" then
                                        email

                                    else
                                        "?"
                                )
                                (Decode.map (Maybe.withDefault "") <| optionalNullableField "prenom" Decode.string)
                                (Decode.map (Maybe.withDefault "") <| optionalNullableField "nom" Decode.string)
                                (Decode.map (Maybe.withDefault "") <| optionalNullableField "email" Decode.string)
                        )
                        (Decode.field "date" Lib.Date.decodeDateWithTimeFromISOString)
            )
        |> andMap (Decode.field "communes" <| Decode.list decodeCommune)
        |> andMap (Decode.field "contactOrigine" <| Decode.nullable decodeGenericSource)
        |> andMap (Decode.field "contactDate" <| Decode.nullable decodeCalendarDate)
        |> andMap (Decode.field "projetType" <| Decode.nullable decodeGenericSource)
        |> andMap (Decode.field "secteurActivite" <| Decode.nullable decodeGenericSource)
        |> andMap (Decode.field "sourceFinancement" <| Decode.nullable decodeGenericSource)
        |> andMap (Decode.field "formeJuridique" <| Decode.string)
        |> andMap (Decode.field "contratAccompagnement" <| Decode.nullable Decode.bool)
        |> andMap (Decode.field "rechercheLocal" <| Decode.nullable Decode.bool)
        |> andMap (Decode.field "ressources" <| Decode.list decodeRessource)


decodeEtablissementLie : Decoder EtablissementLie
decodeEtablissementLie =
    Decode.map2 EtablissementLie
        (Decode.field "id" Decode.string)
        (Decode.field "nomAffichage" Decode.string)
