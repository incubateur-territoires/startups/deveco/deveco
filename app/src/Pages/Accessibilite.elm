module Pages.Accessibilite exposing (page)

import Accessibility exposing (Html, a, abbr, br, div, h1, h2, h3, hr, li, p, span, strong, text, ul)
import Api.Auth
import DSFR.Grid
import Effect
import Html.Attributes exposing (class)
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Page () Shared.Msg (View Msg) Model Msg
page { identity } =
    Spa.Page.element
        { init =
            \_ ->
                ( ()
                , case identity of
                    Nothing ->
                        Effect.fromSharedCmd <| Api.Auth.checkAuth True Nothing

                    _ ->
                        Effect.none
                )
                    |> Shared.pageChangeEffects
        , update = \_ _ -> ( (), Effect.none )
        , view = \_ -> view
        , subscriptions = \_ -> Sub.none
        }


type alias Model =
    ()


type alias Msg =
    ()


view : View Model
view =
    { title =
        UI.Layout.pageTitle <|
            "Déclaration d'accessibilité - Deveco"
    , body = UI.Layout.lazyBody body ()
    , route = Route.Accessibilite
    }


body : Model -> Html ()
body () =
    div [ class "p-4" ]
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col12, class "fr-card--white !p-8" ]
                [ h1 []
                    [ text "Déclaration d’accessibilité" ]
                , p []
                    [ text "Établie le "
                    , span []
                        [ text "14 novembre 2023" ]
                    , text "."
                    ]
                , p []
                    [ span []
                        [ text "Notre organisation" ]
                    , text " s’engage à rendre son service accessible, conformément à l’article 47 de la loi n° 2005-102 du 11 février 2005."
                    ]
                , p []
                    [ text "Cette déclaration d’accessibilité s’applique à "
                    , strong []
                        [ text "ce service" ]
                    , text "."
                    ]
                , h2 []
                    [ text "État de conformité" ]
                , text " "
                , p []
                    [ strong []
                        [ text "Notre site" ]
                    , text " "
                    , text "est"
                    , text " "
                    , strong []
                        [ span
                            [ Html.Attributes.attribute "data-printfilter" "lowercase"
                            ]
                            [ text "non conforme" ]
                        ]
                    , text " "
                    , text "avec le"
                    , text " "
                    , abbr
                        [ Html.Attributes.title "Référentiel général d’amélioration de l’accessibilité"
                        ]
                        [ text "RGAA" ]
                    , text "."
                    , text " "
                    , span []
                        [ text "Le site n’a encore pas été audité." ]
                    ]
                , h2 []
                    [ text "Contenus non accessibles" ]
                , h2 []
                    [ text "Établissement de cette déclaration d’accessibilité" ]
                , p []
                    [ text "Cette déclaration a été établie le"
                    , text " "
                    , span []
                        [ text "14 novembre 2023" ]
                    , text "."
                    ]
                , h3 []
                    [ text "Technologies utilisées" ]
                , p []
                    [ text "L’accessibilité de"
                    , text " "
                    , span []
                        [ text "ce service" ]
                    , text " "
                    , text "s’appuie sur les technologies suivantes\u{00A0}:"
                    ]
                , ul
                    [ class "technical-information technologies-used"
                    ]
                    [ li []
                        [ text "HTML" ]
                    , li []
                        [ text "WAI-ARIA" ]
                    , li []
                        [ text "CSS" ]
                    , li []
                        [ text "JavaScript" ]
                    ]
                , h2 []
                    [ text "Amélioration et contact" ]
                , p []
                    [ text "Si vous n’arrivez pas à accéder à un contenu ou à un service, vous pouvez contacter le responsable de"
                    , text " "
                    , span []
                        [ text "ce service" ]
                    , text " "
                    , text "pour être orienté vers une alternative accessible ou obtenir le contenu sous une autre forme."
                    ]
                , ul
                    [ class "basic-information feedback h-card"
                    ]
                    [ li []
                        [ text "E-mail\u{00A0}:"
                        , text " "
                        , a
                            [ Html.Attributes.href "mailto:lea.gislais@anct.gouv.frlea.gislais@anct.gouv.fr"
                            ]
                            [ text "lea.gislais@anct.gouv.fr" ]
                        ]
                    , li []
                        [ text "Adresse\u{00A0}:"
                        , text " "
                        , span []
                            [ text "ANCT, 20 avenue de Ségur, Paris" ]
                        ]
                    ]
                , p []
                    [ text "Nous essayons de répondre dans les"
                    , text " "
                    , span []
                        [ text "2 jours ouvrés" ]
                    , text "."
                    ]
                , h2 []
                    [ text "Voie de recours" ]
                , p []
                    [ text "Cette procédure est à utiliser dans le cas suivant\u{00A0}: vous avez signalé au responsable du site internet un défaut d’accessibilité qui vous empêche d’accéder à un contenu ou à un des services du portail et vous n’avez pas obtenu de réponse satisfaisante." ]
                , p []
                    [ text "Vous pouvez\u{00A0}:" ]
                , ul []
                    [ li []
                        [ text "Écrire un message au"
                        , text " "
                        , a
                            [ Html.Attributes.href "https://formulaire.defenseurdesdroits.fr/"
                            ]
                            [ text "Défenseur des droits" ]
                        ]
                    , li []
                        [ text "Contacter"
                        , text " "
                        , a
                            [ Html.Attributes.href "https://www.defenseurdesdroits.fr/saisir/delegues"
                            ]
                            [ text "le délégué du Défenseur des droits dans votre région" ]
                        ]
                    , li []
                        [ text "Envoyer un courrier par la poste (gratuit, ne pas mettre de timbre)\u{00A0}:"
                        , text " "
                        , br []
                        , text "Défenseur des droits"
                        , br []
                        , text "Libre réponse 71120 75342 Paris CEDEX 07"
                        ]
                    ]
                , hr []
                    []
                , p []
                    [ text "Cette déclaration d’accessibilité a été créée le"
                    , text " "
                    , span []
                        [ text "14 novembre 2023" ]
                    , text " "
                    , text "grâce au"
                    , text " "
                    , a
                        [ Html.Attributes.href "https://betagouv.github.io/a11y-generateur-declaration/#create"
                        ]
                        [ text "Générateur de Déclaration d’Accessibilité de BetaGouv" ]
                    , text "."
                    ]
                ]
            ]
        ]
