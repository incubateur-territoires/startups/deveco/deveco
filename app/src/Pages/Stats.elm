module Pages.Stats exposing (Model, page)

import Accessibility exposing (Html, div, iframe)
import DSFR.Grid
import Html.Attributes exposing (class)
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


type alias Model =
    ()


page : Shared.Shared -> Page (Maybe String) Shared.Msg (View msg) Model ()
page _ =
    Spa.Page.static view


view : View msg
view =
    { title =
        UI.Layout.pageTitle <|
            "Statistiques d'utilisation - Deveco"
    , body = UI.Layout.lazyBody body ()
    , route = Route.Stats Nothing
    }


body : Model -> Html msg
body () =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12, class "flex flex-col gap-4" ]
            [ iframe
                [ Html.Attributes.src "https://metabase.deveco.incubateur.anct.gouv.fr/public/dashboard/b09856ec-1c25-4d49-ac32-6b68b61cd026"
                , Html.Attributes.attribute "frameborder" "0"
                , Html.Attributes.height 400
                , Html.Attributes.attribute "allowtransparency" ""
                , Html.Attributes.id "metabase"
                ]
                []
            ]
        ]
