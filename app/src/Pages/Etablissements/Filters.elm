module Pages.Etablissements.Filters exposing (Filters, FiltresRequete, Model, Msg, Procedure(..), RechercheEnregistree, SituationGeographique(..), changeSituationGeographique, countActivePersoFilters, decodeFiltresRequete, defaultFiltresRequete, doSearch, emptyFilters, filterPanel, filtersAndPaginationToQuery, forcePagination, getCurrentUrl, getFilters, getPagination, getRecherchesEnregistrees, init, listFiltersTagsId, queryKeys, queryToFiltersAndPagination, resetModals, selectOrder, setFiltersRequete, situationGeographiqueToCode, subscriptions, toHref, update, viewDeleteRechercheEnregistree, viewSaveRechercheEnregistree)

import Accessibility exposing (Html, div, h2, hr, option, p, select, span, sup, text)
import Api
import Api.Auth exposing (delete, get, post)
import DSFR.Accordion
import DSFR.Alert
import DSFR.Button
import DSFR.Checkbox
import DSFR.Grid as Grid
import DSFR.Icons
import DSFR.Icons.System
import DSFR.Input
import DSFR.Modal
import DSFR.Range
import DSFR.Tag
import DSFR.Toggle
import DSFR.Tooltip
import DSFR.Typography as Typo
import Data.AFR exposing (AFR)
import Data.Adresse exposing (ApiStreet, decodeApiStreet)
import Data.CategorieJuridique exposing (CategorieJuridique)
import Data.Commune exposing (Commune, decodeCommune)
import Data.Demande exposing (TypeDemande)
import Data.Etablissement exposing (EntrepriseType(..), entrepriseTypeToDisplay, entrepriseTypeToString, stringToEntrepriseType)
import Data.Exercice exposing (Exercice)
import Data.Naf exposing (CodeNaf)
import Data.Role
import Data.ZRR exposing (ZRR)
import Date exposing (Date)
import Effect
import Html
import Html.Attributes as Attr exposing (class)
import Html.Attributes.Extra exposing (empty)
import Html.Events as Events
import Html.Extra exposing (nothing, viewIf, viewMaybe)
import Html.Lazy
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import Lib.Date
import Lib.UI exposing (filterBy, formatIntWithThousandSpacing)
import Lib.Variables exposing (hintESS, hintFormesJuridiques, hintResultatsTerritoires)
import MultiSelect
import MultiSelectRemote
import QS
import RemoteData as RD exposing (WebData)
import Route
import Shared
import SingleSelectRemote
import UI.Clipboard
import UI.Pagination exposing (Pagination, paginationToQuery, queryToPagination)
import User


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | LogError Msg Shared.ErrorReport
    | ReceivedRecherchesEnregistrees (WebData (List RechercheEnregistree))
    | SearchRechercheEnregistree RechercheEnregistree
    | UpdatedSearch String
    | SaveRechercheEnregistree
    | CancelSaveRechercheEnregistree
    | UpdateSaveRechercheEnregistree String
    | ConfirmSaveRechercheEnregistree
    | ReceivedSaveRechercheEnregistree (WebData (List RechercheEnregistree))
    | DeleteRechercheEnregistree RechercheEnregistree
    | CancelDeleteFilter
    | ConfirmDeleteFilter
    | ReceivedDeleteRechercheEnregistree (WebData (List RechercheEnregistree))
    | ClearAllFilters
    | SetEtatFilter Bool EtatAdministratifFiltre
    | SetGeolocalisationFilter Bool GeolocalisationFiltre
    | SetEntrepriseTypeFilter Bool EntrepriseType
    | SetMicroFilter Bool MicroEntreprise
    | SetFavoriFilter Bool Favori
    | SetAvecContactFilter Bool AvecContact
    | SetAncienCreateurFilter Bool AncienCreateur
    | SetSituationGeographiqueFilter SituationGeographique
    | SetFormesJuridiquesFilter Bool
    | SelectedCategoriesJuridiques ( List CategorieJuridique, MultiSelectRemote.Msg CategorieJuridique )
    | UpdatedSelectCategoriesJuridiques (MultiSelectRemote.Msg CategorieJuridique)
    | UnselectCategorieJuridique CategorieJuridique
    | ToggleCategorieJuridiqueSans
    | SelectedCodesNaf ( List CodeNaf, MultiSelectRemote.Msg CodeNaf )
    | UpdatedSelectCodesNaf (MultiSelectRemote.Msg CodeNaf)
    | UnselectCodeNaf CodeNaf
    | ToggleCodeNafSans
    | SelectedCommunes ( List Commune, MultiSelectRemote.Msg Commune )
    | UpdatedSelectCommunes (MultiSelectRemote.Msg Commune)
    | UnselectCommune Commune
    | SelectedEpcis ( List Epci, MultiSelectRemote.Msg Epci )
    | UpdatedSelectEpcis (MultiSelectRemote.Msg Epci)
    | UnselectEpci Epci
    | SelectedDepartements ( List Departement, MultiSelectRemote.Msg Departement )
    | UpdatedSelectDepartements (MultiSelectRemote.Msg Departement)
    | UnselectDepartement Departement
    | SelectedRegions ( List Region, MultiSelectRemote.Msg Region )
    | UpdatedSelectRegions (MultiSelectRemote.Msg Region)
    | UnselectRegion Region
    | UpdatedEffectifMin (Maybe String)
    | UpdatedEffectifMax (Maybe String)
    | ToggleEffectifInconnu Bool
    | SelectedExercices ( List Exercice, MultiSelect.Msg Exercice )
    | UpdatedSelectExercices (MultiSelect.Msg Exercice)
    | UnselectedExercices Exercice
    | SelectedAdresse ( ApiStreet, SingleSelectRemote.Msg ApiStreet )
    | UpdatedSelectAdresse (SingleSelectRemote.Msg ApiStreet)
    | ClearAdresse
    | SelectedActivites ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectActivites (MultiSelectRemote.Msg String)
    | UnselectActivite String
    | ToggleActivitesToutes
    | SelectedDemandes ( List TypeDemande, MultiSelect.Msg TypeDemande )
    | UpdatedSelectDemandes (MultiSelect.Msg TypeDemande)
    | UnselectDemande TypeDemande
    | SelectedZones ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectZones (MultiSelectRemote.Msg String)
    | UnselectZone String
    | ToggleZonesToutes
    | SelectedMots ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectMots (MultiSelectRemote.Msg String)
    | UnselectMot String
    | ToggleMotsTous
    | SelectedQpvs ( List Qpv, MultiSelectRemote.Msg Qpv )
    | UpdatedSelectQpvs (MultiSelectRemote.Msg Qpv)
    | UnselectQpv Qpv
    | ToggleAllQpvs Bool
    | SelectedTis ( List TerritoireIndustrie, MultiSelectRemote.Msg TerritoireIndustrie )
    | UpdatedSelectTis (MultiSelectRemote.Msg TerritoireIndustrie)
    | UnselectTi TerritoireIndustrie
    | ToggleAllTis Bool
    | SelectedZRR ( List ZRR, MultiSelect.Msg ZRR )
    | UpdatedSelectZRR (MultiSelect.Msg ZRR)
    | UnselectedZRR ZRR
    | SelectedAFR ( List AFR, MultiSelect.Msg AFR )
    | UpdatedSelectAFR (MultiSelect.Msg AFR)
    | UnselectedAFR AFR
    | SetESSFilter Bool Ess
    | SetACVFilter Bool Acv
    | SetPVDFilter Bool Pvd
    | SetVAFilter Bool Va
    | SetSiegeFilter Bool Siege
    | SetProceduresFilter Bool Procedure
    | SetSubventionsFilter Bool Subvention
    | SetSubventionsAnneeMin (Maybe String)
    | SetSubventionsAnneeMax (Maybe String)
    | UpdatedSubventionsMontantMin (Maybe String)
    | UpdatedSubventionsMontantMax (Maybe String)
    | UpdateDate DateType (Maybe Date)
    | ClickedSearch
    | UpdateUrl ( Filters, Pagination )
    | ClickedOrderBy OrderBy OrderDirection
    | SetAccompagnesFilter Bool Accompagnes
    | UpdateAccompagnesDate AccompagnesDate (Maybe Date)
    | SetCaVariationFilter Bool VariationType
    | UpdatedCaVariationMin (Maybe String)
    | UpdatedCaVariationMax (Maybe String)
    | SetEffectifVariationFilter Bool VariationType
    | UpdatedEffectifVarMin (Maybe String)
    | UpdatedEffectifVariationMax (Maybe String)
    | SelectedFiliales ( List EntrepriseApercu, MultiSelectRemote.Msg EntrepriseApercu )
    | UpdatedSelectFiliales (MultiSelectRemote.Msg EntrepriseApercu)
    | UnselectFiliale EntrepriseApercu
    | SelectedDetentions ( List EntrepriseApercu, MultiSelectRemote.Msg EntrepriseApercu )
    | UpdatedSelectDetentions (MultiSelectRemote.Msg EntrepriseApercu)
    | UnselectDetention EntrepriseApercu
    | UpdatedEgaproIndiceMin String
    | UpdatedEgaproIndiceMax String
    | UpdatedEgaproCadresMin String
    | UpdatedEgaproCadresMax String
    | UpdatedEgaproInstancesMin String
    | UpdatedEgaproInstancesMax String


type AccompagnesDate
    = AccompagnesApres
    | AccompagnesAvant


type EtatAdministratifFiltre
    = Actif
    | Ferme
    | SignaleFerme


etatAdministratifFiltreToDisplay : EtatAdministratifFiltre -> String
etatAdministratifFiltreToDisplay e =
    case e of
        Actif ->
            "Actif"

        Ferme ->
            "Fermé"

        SignaleFerme ->
            "Signalé fermé"


etatAdministratifFiltreToString : EtatAdministratifFiltre -> String
etatAdministratifFiltreToString e =
    case e of
        Actif ->
            "A"

        Ferme ->
            "F"

        SignaleFerme ->
            "S"


stringToEtatAdministratifFiltre : String -> Maybe EtatAdministratifFiltre
stringToEtatAdministratifFiltre s =
    case s of
        "A" ->
            Just Actif

        "F" ->
            Just Ferme

        "S" ->
            Just SignaleFerme

        _ ->
            Nothing


type GeolocalisationFiltre
    = GeolocalisationNon
    | GeolocalisationOui
    | GeolocalisationPerso


geolocalisationFiltreToDisplay : GeolocalisationFiltre -> String
geolocalisationFiltreToDisplay e =
    case e of
        GeolocalisationNon ->
            "Non géolocalisé"

        GeolocalisationOui ->
            "Géolocalisé"

        GeolocalisationPerso ->
            "Géolocalisé (modifié)"


geolocalisationFiltreToString : GeolocalisationFiltre -> String
geolocalisationFiltreToString e =
    case e of
        GeolocalisationNon ->
            "N"

        GeolocalisationOui ->
            "O"

        GeolocalisationPerso ->
            "P"


stringToGeolocalisationFiltre : String -> Maybe GeolocalisationFiltre
stringToGeolocalisationFiltre s =
    case s of
        "N" ->
            Just GeolocalisationNon

        "O" ->
            Just GeolocalisationOui

        "P" ->
            Just GeolocalisationPerso

        _ ->
            Nothing


type DateType
    = CreeApres
    | CreeAvant
    | FermeApres
    | FermeAvant
    | ProcedureApres
    | ProcedureAvant


init : FiltresRequete -> Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init filtresRequete rawQuery =
    let
        ( filters, pagination ) =
            queryToFiltersAndPagination rawQuery

        exercicesIds =
            filters.exercices
                |> List.map .id

        newExercices =
            Data.Exercice.tranchesExercices
                |> List.filter (\{ id } -> List.member id exercicesIds)

        zrrsIds =
            filters.zrrs
                |> List.map .id

        newZrrs =
            Data.ZRR.zrrs
                |> List.filter (\{ id } -> List.member id zrrsIds)

        afrsIds =
            filters.afrs
                |> List.map .id

        newAfrs =
            Data.AFR.afrs
                |> List.filter (\{ id } -> List.member id afrsIds)

        apiStreet =
            filters.apiStreet |> Maybe.map .label |> Maybe.withDefault ""

        ( selectAdresse, selectAdresseCmd ) =
            SingleSelectRemote.init selectAdresseId
                { selectionMsg = SelectedAdresse
                , internalMsg = UpdatedSelectAdresse
                , characterSearchThreshold = 3
                , debounceDuration = selectDebounceDuration
                }
                |> SingleSelectRemote.setText apiStreet selectAdresseConfig

        nextUrl =
            filtersAndPaginationToQuery ( filters, pagination )
    in
    ( { filters =
            { filters
                | exercices = newExercices
                , zrrs = newZrrs
                , afrs = newAfrs
            }
      , recherchesEnregistrees = RD.Loading
      , saveRechercheEnregistree = Nothing
      , deleteRechercheEnregistree = Nothing
      , selectCategoriesJuridiques =
            MultiSelectRemote.init selectCategoriesJuridiquesId
                { selectionMsg = SelectedCategoriesJuridiques
                , internalMsg = UpdatedSelectCategoriesJuridiques
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectCodesNaf =
            MultiSelectRemote.init selectCodesNafId
                { selectionMsg = SelectedCodesNaf
                , internalMsg = UpdatedSelectCodesNaf
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectCommunes =
            MultiSelectRemote.init selectCommunesId
                { selectionMsg = SelectedCommunes
                , internalMsg = UpdatedSelectCommunes
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectEpcis =
            MultiSelectRemote.init selectEpcisId
                { selectionMsg = SelectedEpcis
                , internalMsg = UpdatedSelectEpcis
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectDepartements =
            MultiSelectRemote.init selectDepartementsId
                { selectionMsg = SelectedDepartements
                , internalMsg = UpdatedSelectDepartements
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectRegions =
            MultiSelectRemote.init selectRegionsId
                { selectionMsg = SelectedRegions
                , internalMsg = UpdatedSelectRegions
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectExercices =
            MultiSelect.init "select-exercices"
                { selectionMsg = SelectedExercices
                , internalMsg = UpdatedSelectExercices
                }
      , selectAdresse = selectAdresse
      , selectActivites =
            MultiSelectRemote.init selectActivitesId
                { selectionMsg = SelectedActivites
                , internalMsg = UpdatedSelectActivites
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectDemandes =
            MultiSelect.init selectDemandesId
                { selectionMsg = SelectedDemandes
                , internalMsg = UpdatedSelectDemandes
                }
      , selectZones =
            MultiSelectRemote.init selectZonesId
                { selectionMsg = SelectedZones
                , internalMsg = UpdatedSelectZones
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectMots =
            MultiSelectRemote.init selectMotsId
                { selectionMsg = SelectedMots
                , internalMsg = UpdatedSelectMots
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectQpvs =
            MultiSelectRemote.init selectQpvsId
                { selectionMsg = SelectedQpvs
                , internalMsg = UpdatedSelectQpvs
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectTis =
            MultiSelectRemote.init selectTisId
                { selectionMsg = SelectedTis
                , internalMsg = UpdatedSelectTis
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectZrrs =
            MultiSelect.init "select-zrr"
                { selectionMsg = SelectedZRR
                , internalMsg = UpdatedSelectZRR
                }
      , selectAfrs =
            MultiSelect.init "select-afr"
                { selectionMsg = SelectedAFR
                , internalMsg = UpdatedSelectAFR
                }
      , pagination = pagination
      , currentUrl = nextUrl
      , filtresRequete = filtresRequete
      , selectFiliales =
            MultiSelectRemote.init selectFilialesId
                { selectionMsg = SelectedFiliales
                , internalMsg = UpdatedSelectFiliales
                , characterSearchThreshold = 3
                , debounceDuration = selectDebounceDuration
                }
      , selectDetentions =
            MultiSelectRemote.init selectDetentionsId
                { selectionMsg = SelectedDetentions
                , internalMsg = UpdatedSelectDetentions
                , characterSearchThreshold = 3
                , debounceDuration = selectDebounceDuration
                }
      }
    , Effect.batch <|
        [ Effect.fromCmd selectAdresseCmd
        , Effect.fromCmd <|
            getRecherchesEnregistrees
                { sharedMsg = SharedMsg
                , logError = Just LogError
                , msg = ReceivedRecherchesEnregistrees
                }
        , Effect.fromShared <|
            Shared.ReplaceUrl <|
                Route.Etablissements <|
                    (\q ->
                        if q == "" then
                            Nothing

                        else
                            Just q
                    )
                    <|
                        nextUrl
        ]
    )


type alias RechercheEnregistree =
    { id : Int
    , nom : String
    , url : String
    , date : Date
    }


type alias Model =
    { filters : Filters
    , recherchesEnregistrees : WebData (List RechercheEnregistree)
    , saveRechercheEnregistree : Maybe SaveRechercheEnregistreeModal
    , deleteRechercheEnregistree : Maybe DeleteRechercheEnregistreeModal
    , selectCategoriesJuridiques : MultiSelectRemote.SmartSelect Msg CategorieJuridique
    , selectCodesNaf : MultiSelectRemote.SmartSelect Msg CodeNaf
    , selectCommunes : MultiSelectRemote.SmartSelect Msg Commune
    , selectEpcis : MultiSelectRemote.SmartSelect Msg Epci
    , selectDepartements : MultiSelectRemote.SmartSelect Msg Departement
    , selectRegions : MultiSelectRemote.SmartSelect Msg Region
    , selectExercices : MultiSelect.SmartSelect Msg Exercice
    , selectAdresse : SingleSelectRemote.SmartSelect Msg ApiStreet
    , selectActivites : MultiSelectRemote.SmartSelect Msg String
    , selectDemandes : MultiSelect.SmartSelect Msg TypeDemande
    , selectZones : MultiSelectRemote.SmartSelect Msg String
    , selectMots : MultiSelectRemote.SmartSelect Msg String
    , selectQpvs : MultiSelectRemote.SmartSelect Msg Qpv
    , selectTis : MultiSelectRemote.SmartSelect Msg TerritoireIndustrie
    , selectZrrs : MultiSelect.SmartSelect Msg ZRR
    , selectAfrs : MultiSelect.SmartSelect Msg AFR
    , selectFiliales : MultiSelectRemote.SmartSelect Msg EntrepriseApercu
    , selectDetentions : MultiSelectRemote.SmartSelect Msg EntrepriseApercu
    , pagination : Pagination
    , currentUrl : String
    , filtresRequete : FiltresRequete
    }


type alias SaveRechercheEnregistreeModal =
    { nom : String
    , url : String
    , saveRequest : WebData (List RechercheEnregistree)
    }


type alias DeleteRechercheEnregistreeModal =
    { rechercheEnregistree : RechercheEnregistree
    , deleteRequest : WebData (List RechercheEnregistree)
    }


type alias Epci =
    { id : String
    , nom : String
    }


type alias Departement =
    { id : String
    , nom : String
    }


type alias Region =
    { id : String
    , nom : String
    }


type alias Filters =
    { recherche : String
    , etat : List EtatAdministratifFiltre
    , categoriesJuridiques : List CategorieJuridique
    , categoriesJuridiquesSans : Bool
    , codesNaf : List CodeNaf
    , codesNafSans : Bool
    , formesJuridiques : FormesJuridiques
    , communes : List Commune
    , epcis : List Epci
    , departements : List Departement
    , regions : List Region
    , effectifInconnu : Bool
    , effectifMin : Maybe String
    , effectifMax : Maybe String
    , exercices : List Exercice
    , apiStreet : Maybe ApiStreet
    , situationGeographique : SituationGeographique
    , entrepriseTypes : List EntrepriseType
    , activites : List String
    , activitesToutes : Bool
    , demandes : List TypeDemande
    , zones : List String
    , zonesToutes : Bool
    , mots : List String
    , motsTous : Bool
    , qpvs : List Qpv
    , tousQpvs : Bool
    , tis : List TerritoireIndustrie
    , tousTis : Bool
    , zrrs : List ZRR
    , ess : List Ess
    , acv : List Acv
    , pvd : List Pvd
    , va : List Va
    , afrs : List AFR
    , siege : List Siege
    , procedures : List Procedure
    , proceduresApres : Maybe Date
    , proceduresAvant : Maybe Date
    , subventions : List Subvention
    , subventionsAnneeMin : Maybe String
    , subventionsAnneeMax : Maybe String
    , subventionsMontantMin : Maybe String
    , subventionsMontantMax : Maybe String
    , creeApres : Maybe Date
    , creeAvant : Maybe Date
    , fermeApres : Maybe Date
    , fermeAvant : Maybe Date
    , orderBy : OrderBy
    , orderDirection : OrderDirection
    , accompagnes : List Accompagnes
    , accompagnesApres : Maybe Date
    , accompagnesAvant : Maybe Date
    , micro : List MicroEntreprise
    , favori : List Favori
    , avecContact : List AvecContact
    , ancienCreateur : List AncienCreateur
    , geolocalisation : List GeolocalisationFiltre
    , longitude : Maybe Float
    , latitude : Maybe Float
    , zoom : Maybe Float

    -- , nwse : Maybe NWSE
    , caVarTypes : List VariationType
    , caVarMin : Maybe String
    , caVarMax : Maybe String
    , effectifVarTypes : List VariationType
    , effectifVarMin : Maybe String
    , effectifVarMax : Maybe String
    , filiales : List EntrepriseApercu
    , detentions : List EntrepriseApercu
    , egaproIndiceMin : String
    , egaproIndiceMax : String
    , egaproCadresMin : String
    , egaproCadresMax : String
    , egaproInstancesMin : String
    , egaproInstancesMax : String
    }


type alias EntrepriseApercu =
    { id : String
    , nom : String
    }


type VariationType
    = VariationHausse
    | VariationBaisse


type Accompagnes
    = AccompagnesOui
    | AccompagnesNon


type alias Qpv =
    { id : String
    , nom : String
    }


type alias TerritoireIndustrie =
    { id : String
    , nom : String
    }


type FormesJuridiques
    = Courantes
    | Toutes


type MicroEntreprise
    = MicroOui
    | MicroNon


type Favori
    = FavoriOui
    | FavoriNon


type AvecContact
    = AvecContactOui
    | AvecContactNon


type AncienCreateur
    = AncienCreateurOui
    | AncienCreateurNon


type SituationGeographique
    = SurMonTerritoire
    | HorsDeMonTerritoire
    | TouteLaFrance


type OrderBy
    = EtablissementConsultation
      -- | Alphabetique
    | EtablissementCreation
    | EtablissementFermeture


orderByToString : OrderBy -> String
orderByToString orderBy =
    case orderBy of
        EtablissementConsultation ->
            "consultation"

        EtablissementCreation ->
            "creation"

        EtablissementFermeture ->
            "fermeture"


stringToOrderBy : String -> Maybe OrderBy
stringToOrderBy orderBy =
    case orderBy of
        "consultation" ->
            Just EtablissementConsultation

        "creation" ->
            Just EtablissementCreation

        "fermeture" ->
            Just EtablissementFermeture

        _ ->
            Nothing


type OrderDirection
    = Desc
    | Asc


orderDirectionToString : OrderDirection -> String
orderDirectionToString orderDirection =
    case orderDirection of
        Asc ->
            "asc"

        Desc ->
            "desc"


stringToOrderDirection : String -> Maybe OrderDirection
stringToOrderDirection orderDirection =
    case orderDirection of
        "asc" ->
            Just Asc

        "desc" ->
            Just Desc

        _ ->
            Nothing


type Ess
    = EssOui
    | EssNon


type Acv
    = AcvOui
    | AcvNon


type Pvd
    = PvdOui
    | PvdNon


type Va
    = VaOui
    | VaNon


type Siege
    = SiegeOui
    | SiegeNon


type Procedure
    = ProcedureOui
    | ProcedureNon


type Subvention
    = SubventionOui
    | SubventionNon


stringToSubventions : String -> Maybe Subvention
stringToSubventions s =
    case s of
        "oui" ->
            Just SubventionOui

        "non" ->
            Just SubventionNon

        _ ->
            Nothing


subventionToString : Subvention -> String
subventionToString s =
    case s of
        SubventionOui ->
            "oui"

        SubventionNon ->
            "non"


subventionToDisplay : Subvention -> String
subventionToDisplay s =
    case s of
        SubventionOui ->
            "Oui"

        SubventionNon ->
            "Non"


stringToAccompagnes : String -> Maybe Accompagnes
stringToAccompagnes s =
    case s of
        "oui" ->
            Just AccompagnesOui

        "non" ->
            Just AccompagnesNon

        _ ->
            Nothing


accompagnesToString : Accompagnes -> String
accompagnesToString s =
    case s of
        AccompagnesOui ->
            "oui"

        AccompagnesNon ->
            "non"


microEntrepriseToString : MicroEntreprise -> String
microEntrepriseToString s =
    case s of
        MicroOui ->
            "oui"

        MicroNon ->
            "non"


microEntrepriseToDisplay : MicroEntreprise -> String
microEntrepriseToDisplay s =
    case s of
        MicroOui ->
            "Oui"

        MicroNon ->
            "Non"


favoriToString : Favori -> String
favoriToString s =
    case s of
        FavoriOui ->
            "oui"

        FavoriNon ->
            "non"


favoriToDisplay : Favori -> String
favoriToDisplay s =
    case s of
        FavoriOui ->
            "Oui"

        FavoriNon ->
            "Non"


avecContactToString : AvecContact -> String
avecContactToString s =
    case s of
        AvecContactOui ->
            "oui"

        AvecContactNon ->
            "non"


avecContactToDisplay : AvecContact -> String
avecContactToDisplay s =
    case s of
        AvecContactOui ->
            "Oui"

        AvecContactNon ->
            "Non"


ancienCreateurToString : AncienCreateur -> String
ancienCreateurToString s =
    case s of
        AncienCreateurOui ->
            "oui"

        AncienCreateurNon ->
            "non"


ancienCreateurToDisplay : AncienCreateur -> String
ancienCreateurToDisplay s =
    case s of
        AncienCreateurOui ->
            "Oui"

        AncienCreateurNon ->
            "Non"


emptyFilters : Filters
emptyFilters =
    { recherche = ""
    , etat = []
    , categoriesJuridiques = []
    , categoriesJuridiquesSans = False
    , codesNaf = []
    , codesNafSans = False
    , formesJuridiques = Toutes
    , communes = []
    , epcis = []
    , departements = []
    , regions = []
    , effectifInconnu = False
    , effectifMin = Nothing
    , effectifMax = Nothing
    , exercices = []
    , apiStreet = Nothing
    , situationGeographique = SurMonTerritoire
    , entrepriseTypes = []
    , activites = []
    , activitesToutes = False
    , demandes = []
    , zones = []
    , zonesToutes = False
    , mots = []
    , motsTous = False
    , orderBy = EtablissementConsultation
    , orderDirection = Desc
    , qpvs = []
    , tousQpvs = False
    , tis = []
    , tousTis = False
    , zrrs = []
    , ess = []
    , acv = []
    , pvd = []
    , va = []
    , afrs = []
    , siege = []
    , procedures = []
    , proceduresApres = Nothing
    , proceduresAvant = Nothing
    , subventions = []
    , subventionsAnneeMin = Nothing
    , subventionsAnneeMax = Nothing
    , subventionsMontantMin = Nothing
    , subventionsMontantMax = Nothing
    , creeApres = Nothing
    , creeAvant = Nothing
    , fermeApres = Nothing
    , fermeAvant = Nothing
    , accompagnes = []
    , accompagnesApres = Nothing
    , accompagnesAvant = Nothing
    , micro = []
    , favori = []
    , avecContact = []
    , ancienCreateur = []
    , geolocalisation = []
    , longitude = Nothing
    , latitude = Nothing
    , zoom = Nothing

    -- , nwse = Nothing
    , caVarTypes = []
    , caVarMin = Nothing
    , caVarMax = Nothing
    , effectifVarTypes = []
    , effectifVarMin = Nothing
    , effectifVarMax = Nothing
    , filiales = []
    , detentions = []
    , egaproIndiceMin = "0"
    , egaproIndiceMax = "100"
    , egaproCadresMin = "0"
    , egaproCadresMax = "100"
    , egaproInstancesMin = "0"
    , egaproInstancesMax = "100"
    }


hasFilterTags : String -> Bool
hasFilterTags url =
    let
        filters =
            url |> queryToFilters

        hasFilters =
            [ filters.recherche /= ""
            , List.length filters.etat > 0
            , List.length filters.geolocalisation > 0
            , List.length filters.micro > 0
            , List.length filters.categoriesJuridiques > 0
            , List.length filters.codesNaf > 0
            , filters.formesJuridiques /= Toutes
            , List.length filters.communes > 0
            , List.length filters.epcis > 0
            , List.length filters.departements > 0
            , List.length filters.regions > 0
            , filters.effectifInconnu
            , filters.effectifMin /= Nothing
            , filters.effectifMax /= Nothing
            , List.length filters.exercices > 0
            , filters.apiStreet /= Nothing
            , filters.situationGeographique /= SurMonTerritoire
            , List.length filters.entrepriseTypes > 0
            , List.length filters.activites > 0
            , List.length filters.demandes > 0
            , List.length filters.zones > 0
            , List.length filters.mots > 0
            , List.length filters.qpvs > 0
            , filters.tousQpvs
            , List.length filters.tis > 0
            , filters.tousTis
            , List.length filters.zrrs > 0
            , List.length filters.ess > 0
            , List.length filters.acv > 0
            , List.length filters.pvd > 0
            , List.length filters.va > 0
            , List.length filters.afrs > 0
            , List.length filters.siege > 0
            , List.length filters.procedures > 0
            , filters.proceduresApres /= Nothing
            , filters.proceduresAvant /= Nothing
            , List.length filters.subventions > 0
            , filters.subventionsAnneeMin /= Nothing
            , filters.subventionsAnneeMax /= Nothing
            , filters.subventionsMontantMin /= Nothing
            , filters.subventionsMontantMax /= Nothing
            , filters.creeApres /= Nothing
            , filters.creeAvant /= Nothing
            , filters.fermeApres /= Nothing
            , filters.fermeAvant /= Nothing
            , List.length filters.accompagnes > 0
            , filters.accompagnesApres /= Nothing
            , filters.accompagnesAvant /= Nothing
            , List.length filters.favori > 0
            , List.length filters.avecContact > 0
            , List.length filters.ancienCreateur > 0
            , List.length filters.caVarTypes > 0
            , filters.caVarMin /= Nothing
            , filters.caVarMax /= Nothing
            , List.length filters.effectifVarTypes > 0
            , filters.effectifVarMin /= Nothing
            , filters.effectifVarMax /= Nothing
            , List.length filters.filiales > 0
            , List.length filters.detentions > 0
            , filters.egaproIndiceMin /= "0"
            , filters.egaproIndiceMax /= "100"
            , filters.egaproCadresMin /= "0"
            , filters.egaproCadresMax /= "100"
            , filters.egaproInstancesMin /= "0"
            , filters.egaproInstancesMax /= "100"
            ]
    in
    List.any (\a -> a) hasFilters


categoriesJuridiquesFilter : MultiSelectRemote.SmartSelect Msg CategorieJuridique -> FormesJuridiques -> Bool -> List CategorieJuridique -> Html Msg
categoriesJuridiquesFilter selectCategoriesJuridiques formesJuridiques categoriesJuridiquesSans categoriesJuridiques =
    div []
        [ div [ class "flex flex-col gap-4" ]
            [ selectCategoriesJuridiques
                |> MultiSelectRemote.viewCustom
                    { isDisabled = formesJuridiques == Courantes
                    , selected = categoriesJuridiques
                    , optionLabelFn = \code -> code.id ++ " - " ++ code.nom
                    , optionDescriptionFn = \_ -> ""
                    , optionsContainerMaxHeight = 300
                    , selectTitle =
                        div [ class "flex flex-col" ]
                            [ span [ Typo.textBold ] [ text "Catégorie juridique" ]
                            , span [ Typo.textSm, class "!mb-0 blue-text font-normal" ]
                                [ Typo.externalLink lienCategoriesJuridiques [] [ text "Accéder au référentiel des catégories juridiques" ] ]
                            ]
                    , viewSelectedOptionFn = text << (\code -> code.id ++ " - " ++ code.nom)
                    , noResultsForMsg =
                        \searchText ->
                            "Aucune autre catégorie juridique n'a été trouvée"
                                ++ (if searchText == "" then
                                        ""

                                    else
                                        " pour " ++ searchText
                                   )
                    , noOptionsMsg = "Aucune catégorie juridique n'a été trouvée"
                    , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                    , newOption = Nothing
                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                    , searchPrompt = "Rechercher une catégorie juridique"
                    }
            , div []
                [ case categoriesJuridiques of
                    [] ->
                        nothing

                    _ ->
                        let
                            ( toLabel, toTitle ) =
                                dataToLabelAndToTitle
                        in
                        categoriesJuridiques
                            |> List.map
                                (\categorieJuridique ->
                                    { data = categorieJuridique, toString = toLabel }
                                        |> DSFR.Tag.deletable UnselectCategorieJuridique
                                        |> DSFR.Tag.withAttrs
                                            [ Attr.title <| toTitle <| categorieJuridique
                                            ]
                                )
                            |> DSFR.Tag.medium
                , div []
                    [ DSFR.Toggle.single
                        { value = True
                        , checked = Just categoriesJuridiquesSans
                        , valueAsString =
                            \v ->
                                if v then
                                    "oui"

                                else
                                    "non"
                        , id = "categories-juridiques-sans"
                        , label = text "Exclure"
                        , onChecked = \_ _ -> ToggleCategorieJuridiqueSans
                        }
                        |> DSFR.Toggle.singleWithLabelLeft
                        |> DSFR.Toggle.singleWithNoState
                        |> DSFR.Toggle.viewSingle
                    ]
                ]
            ]
        , hr [ class "fr-hr !mb-0 !pb-0 h-[1px]" ] []
        , Html.Lazy.lazy formesFilter formesJuridiques
        ]


dataToLabelAndToTitle :
    ( { data
        | id : String
        , nom : String
      }
      -> String
    , { data
        | id : String
        , nom : String
      }
      -> String
    )
dataToLabelAndToTitle =
    let
        toLabel data =
            if data.nom == "" then
                data.id

            else if String.length data.nom < 20 then
                data.id ++ " - " ++ data.nom

            else
                data.id ++ " - " ++ String.left 17 data.nom ++ "..."

        toTitle data =
            if data.nom /= "" then
                data.id ++ " - " ++ data.nom

            else
                data.id
    in
    ( toLabel, toTitle )


lienCategoriesJuridiques : String
lienCategoriesJuridiques =
    "https://www.insee.fr/fr/statistiques/fichier/2028129/cj_septembre_2022.xls"


lienCodesNAF : String
lienCodesNAF =
    "https://www.insee.fr/fr/metadonnees/nafr2/section/A?champRecherche=true"


codesNafFilter : MultiSelectRemote.SmartSelect Msg CodeNaf -> Bool -> List CodeNaf -> Html Msg
codesNafFilter selectCodesNaf codesNafSans codesNaf =
    div []
        [ div [ class "flex flex-col gap-4" ]
            [ selectCodesNaf
                |> MultiSelectRemote.viewCustom
                    { isDisabled = False
                    , selected = codesNaf
                    , optionLabelFn =
                        \code ->
                            if String.length code.id == 1 then
                                " == " ++ code.id ++ " - " ++ code.nom ++ " == "

                            else if String.length code.id == 2 then
                                " = " ++ code.id ++ " - " ++ code.nom ++ " = "

                            else
                                code.id ++ " - " ++ code.nom
                    , optionDescriptionFn = \_ -> ""
                    , optionsContainerMaxHeight = 300
                    , selectTitle =
                        div [ class "flex flex-col" ]
                            [ span [ Typo.textBold ] [ text "Codes NAF" ]
                            , span [ Typo.textSm, class "!mb-0 blue-text font-normal" ]
                                [ Typo.externalLink lienCodesNAF [] [ text "Accéder au référentiel des codes NAF" ] ]
                            ]
                    , viewSelectedOptionFn = text << (\code -> code.id ++ " - " ++ code.nom)
                    , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                    , noResultsForMsg =
                        \searchText ->
                            "Aucun autre code NAF n'a été trouvé"
                                ++ (if searchText == "" then
                                        ""

                                    else
                                        " pour " ++ searchText
                                   )
                    , noOptionsMsg = "Aucun code NAF n'a été trouvé"
                    , newOption = Nothing
                    , searchPrompt = "Rechercher un code NAF"
                    }
            , div []
                [ case codesNaf of
                    [] ->
                        nothing

                    _ ->
                        let
                            ( toLabel, toTitle ) =
                                dataToLabelAndToTitle
                        in
                        codesNaf
                            |> List.map
                                (\codeNaf ->
                                    { data = codeNaf
                                    , toString = toLabel
                                    }
                                        |> DSFR.Tag.deletable UnselectCodeNaf
                                        |> DSFR.Tag.withAttrs
                                            [ Attr.title <|
                                                toTitle <|
                                                    codeNaf
                                            ]
                                )
                            |> DSFR.Tag.medium
                , div []
                    [ DSFR.Toggle.single
                        { value = True
                        , checked = Just codesNafSans
                        , valueAsString =
                            \v ->
                                if v then
                                    "oui"

                                else
                                    "non"
                        , id = "codes-naf-sans"
                        , label = text "Exclure"
                        , onChecked = \_ _ -> ToggleCodeNafSans
                        }
                        |> DSFR.Toggle.singleWithLabelLeft
                        |> DSFR.Toggle.singleWithNoState
                        |> DSFR.Toggle.viewSingle
                    ]
                ]
            ]
        , hr [ class "fr-hr !mb-0 !pb-0 h-[1px]" ] []
        ]


effectifsFilter : Bool -> Maybe String -> Maybe String -> Html Msg
effectifsFilter effectifsInconnus effectifsMinimum effectifsMaximum =
    let
        effectifsHint =
            "Derniers effectifs moyens mensuels connus (source GIP MDS)"

        maybeMin =
            effectifsMinimum
                |> Maybe.andThen String.toFloat

        maybeMax =
            effectifsMaximum
                |> Maybe.andThen String.toFloat

        invalidRange =
            case ( maybeMin, maybeMax ) of
                ( Nothing, Nothing ) ->
                    False

                ( Just _, Nothing ) ->
                    False

                ( Nothing, Just _ ) ->
                    False

                ( Just min, Just max ) ->
                    min > max
    in
    div [ class "flex flex-col gap-4" ]
        [ span [ Typo.textBold ]
            [ text "Effectifs moyens annuels (EMA)"
            , sup []
                [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ]
                    |> (DSFR.Tooltip.survol { label = text effectifsHint, id = "tooltip-filtres-ema" }
                            |> DSFR.Tooltip.wrap
                       )
                ]
            ]
        , div [ class "flex flex-row justify-between gap-4" ]
            [ DSFR.Input.new
                { value = effectifsMinimum |> Maybe.withDefault ""
                , onInput =
                    (\s ->
                        if s == "" then
                            Nothing

                        else
                            Just s
                    )
                        >> UpdatedEffectifMin
                , label = text "Minimum"
                , name = "effectifs-minimum"
                }
                |> DSFR.Input.number (Just { step = Just "0.01", min = Just "0", max = Just "100000" })
                |> DSFR.Input.withHint [ text "Format attendu\u{00A0}: xxxxx,xx" ]
                |> DSFR.Input.withError
                    (if invalidRange then
                        Just [ text "Intervalle incohérent" ]

                     else
                        Nothing
                    )
                |> DSFR.Input.view
            , DSFR.Input.new
                { value = effectifsMaximum |> Maybe.withDefault ""
                , onInput =
                    (\s ->
                        if s == "" then
                            Nothing

                        else
                            Just s
                    )
                        >> UpdatedEffectifMax
                , label = text "Maximum"
                , name = "effectifs-maximum"
                }
                |> DSFR.Input.number (Just { step = Just "0.01", min = Just "0", max = Just "100000" })
                |> DSFR.Input.withHint [ text "Format attendu\u{00A0}: xxxxx,xx" ]
                |> DSFR.Input.withError
                    (if invalidRange then
                        Just [ text "Intervalle incohérent" ]

                     else
                        Nothing
                    )
                |> DSFR.Input.view
            ]
        , DSFR.Checkbox.single
            { value = ()
            , checked = Just effectifsInconnus
            , valueAsString = \_ -> "Effectifs inconnus"
            , id = "effectifs-inconnus"
            , label = text "Inclure les effectifs inconnus"
            , onChecked = \_ -> ToggleEffectifInconnu
            }
            |> DSFR.Checkbox.singleWithDisabled (effectifsMinimum == Nothing && effectifsMaximum == Nothing)
            |> DSFR.Checkbox.viewSingle
        ]


exercicesFilter : List Exercice -> MultiSelect.SmartSelect Msg Exercice -> Html Msg
exercicesFilter exercices selectExercices =
    let
        exercicesHint =
            "Dernier chiffre d'affaires connu sur les trois dernières années (source INPI)"
    in
    div [ class "flex flex-col gap-4" ]
        [ selectExercices
            |> MultiSelect.viewCustom
                { isDisabled = False
                , selected = exercices
                , optionLabelFn = .label
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle =
                    span [ Typo.textBold ]
                        [ text "Chiffre d'affaires (source INPI)"
                        , sup []
                            [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ]
                                |> (DSFR.Tooltip.survol { label = text exercicesHint, id = "tooltip-filtres-exercices-inpi" }
                                        |> DSFR.Tooltip.wrap
                                   )
                            ]
                        ]
                , viewSelectedOptionFn = text << (\code -> code.id ++ " - " ++ code.label)
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre tranche de chiffre d'affaires n'a été trouvée"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucune tranche de chiffre d'affaires n'a été trouvée"
                , options = Data.Exercice.tranchesExercices
                , searchFn = filterBy .label
                , searchPrompt = "Choisir une tranche de chiffre d'affaires"
                }
        , case exercices of
            [] ->
                nothing

            _ ->
                exercices
                    |> List.map
                        (\exercice ->
                            { data = exercice, toString = .label }
                                |> DSFR.Tag.deletable UnselectedExercices
                        )
                    |> DSFR.Tag.medium
        ]


zrrsFilter : List ZRR -> MultiSelect.SmartSelect Msg ZRR -> Html Msg
zrrsFilter zrrs selectZrrs =
    div [ class "flex flex-col gap-2" ]
        [ selectZrrs
            |> MultiSelect.viewCustom
                { isDisabled = False
                , selected = zrrs
                , optionLabelFn = .label
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "ZRR - Zone de Revitalisation Rurale" ]
                , viewSelectedOptionFn = text << .label
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre classement n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun classement n'a été trouvé"
                , options = Data.ZRR.zrrs
                , searchFn = filterBy .label
                , searchPrompt = "Filtrer par ZRR"
                }
        , case zrrs of
            [] ->
                nothing

            _ ->
                zrrs
                    |> List.map
                        (\zrr ->
                            { data = zrr, toString = .label }
                                |> DSFR.Tag.deletable UnselectedZRR
                        )
                    |> DSFR.Tag.medium
        ]


afrsFilter : List AFR -> MultiSelect.SmartSelect Msg AFR -> Html Msg
afrsFilter afrs selectAfrs =
    div [ class "flex flex-col gap-2" ]
        [ selectAfrs
            |> MultiSelect.viewCustom
                { isDisabled = False
                , selected = afrs
                , optionLabelFn = .label
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "AFR - Aide à finalité régionale" ]
                , viewSelectedOptionFn = text << .label
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre classement n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun classement n'a été trouvé"
                , options = Data.AFR.afrs
                , searchFn = filterBy .label
                , searchPrompt = "Filtrer par AFR"
                }
        , case afrs of
            [] ->
                nothing

            _ ->
                afrs
                    |> List.map
                        (\afr ->
                            { data = afr, toString = .label }
                                |> DSFR.Tag.deletable UnselectedAFR
                        )
                    |> DSFR.Tag.medium
        ]


situationGeographiqueToString : SituationGeographique -> String
situationGeographiqueToString situationGeographique =
    case situationGeographique of
        SurMonTerritoire ->
            "Sur mon territoire"

        HorsDeMonTerritoire ->
            "Hors de mon territoire"

        TouteLaFrance ->
            "Toute la France"


situationGeographiqueToLabel : SituationGeographique -> Html msg
situationGeographiqueToLabel =
    situationGeographiqueToString
        >> text
        >> List.singleton
        >> span [ Typo.textBold ]


situationGeographiqueToId : SituationGeographique -> String
situationGeographiqueToId situationGeographique =
    case situationGeographique of
        SurMonTerritoire ->
            "situation-geographique-sur"

        HorsDeMonTerritoire ->
            "situation-geographique-hors"

        TouteLaFrance ->
            "situation-geographique-france"


situationGeographiqueToCode : SituationGeographique -> String
situationGeographiqueToCode situationGeographique =
    case situationGeographique of
        SurMonTerritoire ->
            "territoire"

        HorsDeMonTerritoire ->
            "hors"

        TouteLaFrance ->
            "france"


idToSituationGeographique : String -> Maybe SituationGeographique
idToSituationGeographique s =
    case s of
        "situation-geographique-sur" ->
            Just SurMonTerritoire

        "situation-geographique-hors" ->
            Just HorsDeMonTerritoire

        "situation-geographique-france" ->
            Just TouteLaFrance

        _ ->
            Nothing


situationGeographiqueFilter : SituationGeographique -> Html Msg
situationGeographiqueFilter situationGeographique =
    div [ class "mb-4" ]
        [ DSFR.Input.new
            { value = situationGeographiqueToId situationGeographique
            , onInput =
                idToSituationGeographique
                    >> Maybe.map SetSituationGeographiqueFilter
                    >> Maybe.withDefault NoOp
            , label =
                span
                    [ Typo.textBold
                    , Typo.textLg
                    , class "!mb-0"
                    ]
                    [ text "Situation géographique" ]
            , name = "select-situation-geographique"
            }
            |> DSFR.Input.select
                { options =
                    [ SurMonTerritoire
                    , HorsDeMonTerritoire
                    , TouteLaFrance
                    ]
                , toId = situationGeographiqueToId
                , toLabel = situationGeographiqueToLabel
                , toDisabled = Nothing
                }
            |> DSFR.Input.view
        ]


etatFilter : List EtatAdministratifFiltre -> Html Msg
etatFilter etat =
    DSFR.Checkbox.group
        { id = "filtres-etat-type"
        , values =
            [ Actif
            , Ferme
            , SignaleFerme
            ]
        , toLabel = text << etatAdministratifFiltreToDisplay
        , toId = etatAdministratifFiltreToString
        , checked = etat
        , valueAsString = etatAdministratifFiltreToString
        , onChecked = \data bool -> SetEtatFilter bool data
        , label =
            div [ Typo.textBold ]
                [ text "État administratif"
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


geolocalisationFilter : List GeolocalisationFiltre -> Html Msg
geolocalisationFilter geolocalisation =
    DSFR.Checkbox.group
        { id = "filtres-geolocalisation"
        , values =
            [ GeolocalisationNon
            , GeolocalisationOui
            , GeolocalisationPerso
            ]
        , toLabel = text << geolocalisationFiltreToDisplay
        , toId = geolocalisationFiltreToString
        , checked = geolocalisation
        , valueAsString = geolocalisationFiltreToString
        , onChecked = \data bool -> SetGeolocalisationFilter bool data
        , label =
            div [ Typo.textBold ]
                [ text "Géolocalisation"
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


entrepriseTypeFilter : List EntrepriseType -> Html Msg
entrepriseTypeFilter entrepriseTypes =
    DSFR.Checkbox.group
        { id = "filtres-entreprise-type"
        , values =
            [ PMEEntrepriseType
            , ETIEntrepriseType
            , GEEntrepriseType
            , InconnuEntrepriseType
            ]
        , toLabel = text << entrepriseTypeToDisplay
        , toId = entrepriseTypeToString
        , checked = entrepriseTypes
        , valueAsString = entrepriseTypeToString
        , onChecked = \data bool -> SetEntrepriseTypeFilter bool data
        , label =
            div [ Typo.textBold ]
                [ text "Taille d'entreprise"
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


microFilter : List MicroEntreprise -> Html Msg
microFilter micro =
    DSFR.Checkbox.group
        { id = "filtres-micro-entreprise"
        , values =
            [ MicroOui
            , MicroNon
            ]
        , toLabel = text << microEntrepriseToDisplay
        , toId = microEntrepriseToString
        , checked = micro
        , valueAsString = microEntrepriseToString
        , onChecked = \data bool -> SetMicroFilter bool data
        , label =
            div [ Typo.textBold ]
                [ text "Micro-entreprise"
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


favoriFilter : List Favori -> Html Msg
favoriFilter favori =
    DSFR.Checkbox.group
        { id = "filtres-favori"
        , values =
            [ FavoriOui
            , FavoriNon
            ]
        , toLabel = text << favoriToDisplay
        , toId = favoriToString
        , checked = favori
        , valueAsString = favoriToString
        , onChecked = \data bool -> SetFavoriFilter bool data
        , label =
            div [ Typo.textBold ]
                [ text "Établissements favoris"
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


avecContactFilter : List AvecContact -> Html Msg
avecContactFilter avecContact =
    DSFR.Checkbox.group
        { id = "filtres-avec-contact"
        , values =
            [ AvecContactOui
            , AvecContactNon
            ]
        , toLabel = text << avecContactToDisplay
        , toId = avecContactToString
        , checked = avecContact
        , valueAsString = avecContactToString
        , onChecked = \data bool -> SetAvecContactFilter bool data
        , label =
            div [ Typo.textBold ]
                [ text "Établissements avec contact"
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


ancienCreateurFilter : List AncienCreateur -> Html Msg
ancienCreateurFilter ancienCreateur =
    DSFR.Checkbox.group
        { id = "filtres-ancien-createur"
        , values =
            [ AncienCreateurOui
            , AncienCreateurNon
            ]
        , toLabel = text << ancienCreateurToDisplay
        , toId = ancienCreateurToString
        , checked = ancienCreateur
        , valueAsString = ancienCreateurToString
        , onChecked = \data bool -> SetAncienCreateurFilter bool data
        , label =
            div [ Typo.textBold ]
                [ text "Anciens créateurs"
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


formesToLabel : FormesJuridiques -> String
formesToLabel formes =
    case formes of
        Courantes ->
            "Seulement les plus courantes"

        Toutes ->
            "Toutes"


formesToHtml : FormesJuridiques -> Html msg
formesToHtml formes =
    case formes of
        Courantes ->
            span [] [ text "Seulement les plus courantes", sup [ Attr.title hintFormesJuridiques ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]

        Toutes ->
            text "Toutes"


formesFilter : FormesJuridiques -> Html Msg
formesFilter formesJuridiques =
    DSFR.Checkbox.group
        { id = "filtres-formes-juridiques"
        , values =
            [ Courantes
            ]
        , toLabel = formesToHtml
        , toId =
            \formes ->
                case formes of
                    Courantes ->
                        "courantes"

                    Toutes ->
                        "toutes"
        , checked = [ formesJuridiques ]
        , valueAsString =
            \formes ->
                case formes of
                    Courantes ->
                        "courantes"

                    Toutes ->
                        "toutes"
        , onChecked = \_ -> SetFormesJuridiquesFilter
        , label = nothing
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!m-0", Attr.title <| hintFormesJuridiques ]
        |> DSFR.Checkbox.viewGroup


essFilter : List Ess -> Html Msg
essFilter ess =
    DSFR.Checkbox.group
        { id = "filtres-ess"
        , values =
            [ EssOui
            , EssNon
            ]
        , toLabel =
            text
                << (\e ->
                        case e of
                            EssOui ->
                                "Oui"

                            EssNon ->
                                "Non"
                   )
        , toId =
            \e ->
                case e of
                    EssOui ->
                        "oui"

                    EssNon ->
                        "non"
        , checked = ess
        , valueAsString =
            \e ->
                case e of
                    EssOui ->
                        "oui"

                    EssNon ->
                        "non"
        , onChecked =
            \data bool -> SetESSFilter bool data
        , label =
            div [ Typo.textBold ]
                [ span [ Typo.textBold ] [ text "Économie sociale et solidaire", sup [ Attr.title hintESS ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


acvFilter : List Acv -> Html Msg
acvFilter acv =
    DSFR.Checkbox.group
        { id = "filtres-acv"
        , values =
            [ AcvOui
            , AcvNon
            ]
        , toLabel =
            text
                << (\e ->
                        case e of
                            AcvOui ->
                                "Oui"

                            AcvNon ->
                                "Non"
                   )
        , toId =
            \e ->
                case e of
                    AcvOui ->
                        "oui"

                    AcvNon ->
                        "non"
        , checked = acv
        , valueAsString =
            \e ->
                case e of
                    AcvOui ->
                        "oui"

                    AcvNon ->
                        "non"
        , onChecked =
            \data bool -> SetACVFilter bool data
        , label =
            div [ Typo.textBold ]
                [ span [ Typo.textBold ] [ text "ACV - Action cœur de ville" ]
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


pvdFilter : List Pvd -> Html Msg
pvdFilter pvd =
    DSFR.Checkbox.group
        { id = "filtres-pvd"
        , values =
            [ PvdOui
            , PvdNon
            ]
        , toLabel =
            text
                << (\e ->
                        case e of
                            PvdOui ->
                                "Oui"

                            PvdNon ->
                                "Non"
                   )
        , toId =
            \e ->
                case e of
                    PvdOui ->
                        "oui"

                    PvdNon ->
                        "non"
        , checked = pvd
        , valueAsString =
            \e ->
                case e of
                    PvdOui ->
                        "oui"

                    PvdNon ->
                        "non"
        , onChecked =
            \data bool -> SetPVDFilter bool data
        , label =
            div [ Typo.textBold ]
                [ span [ Typo.textBold ] [ text "PVD - Petite ville de demain" ]
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


vaFilter : List Va -> Html Msg
vaFilter va =
    DSFR.Checkbox.group
        { id = "filtres-va"
        , values =
            [ VaOui
            , VaNon
            ]
        , toLabel =
            text
                << (\e ->
                        case e of
                            VaOui ->
                                "Oui"

                            VaNon ->
                                "Non"
                   )
        , toId =
            \e ->
                case e of
                    VaOui ->
                        "oui"

                    VaNon ->
                        "non"
        , checked = va
        , valueAsString =
            \e ->
                case e of
                    VaOui ->
                        "oui"

                    VaNon ->
                        "non"
        , onChecked =
            \data bool -> SetVAFilter bool data
        , label =
            div [ Typo.textBold ]
                [ span [ Typo.textBold ] [ text "VA - Village d'avenir" ]
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


siegeFilter : List Siege -> Html Msg
siegeFilter siege =
    DSFR.Checkbox.group
        { id = "filtres-siege"
        , values =
            [ SiegeOui
            , SiegeNon
            ]
        , toLabel =
            text
                << (\e ->
                        case e of
                            SiegeOui ->
                                "Oui"

                            SiegeNon ->
                                "Non"
                   )
        , toId =
            \e ->
                case e of
                    SiegeOui ->
                        "oui"

                    SiegeNon ->
                        "non"
        , checked = siege
        , valueAsString =
            \e ->
                case e of
                    SiegeOui ->
                        "oui"

                    SiegeNon ->
                        "non"
        , onChecked =
            \data bool -> SetSiegeFilter bool data
        , label =
            div [ Typo.textBold ]
                [ span [ Typo.textBold ] [ text "Établissement siège" ]
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


procedureFilter : List Procedure -> Maybe Date -> Maybe Date -> Html Msg
procedureFilter procedures proceduresApres proceduresAvant =
    div [ class "flex flex-col gap-4" ]
        [ div []
            [ DSFR.Checkbox.group
                { id = "filtres-procedure"
                , values =
                    [ ProcedureOui
                    , ProcedureNon
                    ]
                , toLabel =
                    text
                        << (\e ->
                                case e of
                                    ProcedureOui ->
                                        "Oui"

                                    ProcedureNon ->
                                        "Non"
                           )
                , toId =
                    \e ->
                        case e of
                            ProcedureOui ->
                                "oui"

                            ProcedureNon ->
                                "non"
                , checked = procedures
                , valueAsString =
                    \e ->
                        case e of
                            ProcedureOui ->
                                "oui"

                            ProcedureNon ->
                                "non"
                , onChecked =
                    \data bool -> SetProceduresFilter bool data
                , label =
                    div [ Typo.textBold ]
                        [ span [ Typo.textBold ] [ text "Procédure collective en cours" ]
                        ]
                }
                |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
                |> DSFR.Checkbox.viewGroup
            , div [ class "flex flex-col gap-2" ]
                [ div [ Typo.textBold, class "!mb-0" ] [ text "Date des procédures" ]
                , div [ class "flex flex-row justify_between gap-2" ]
                    [ selecteurDate
                        { msg = UpdateDate ProcedureApres
                        , label = span [ class "!mb-0" ] [ text "À partir du" ]
                        , name = "filtre-procedure-apres"
                        , value = proceduresApres
                        , min = Nothing
                        , max = Nothing
                        , disabled = not <| List.member ProcedureOui procedures
                        }
                    , selecteurDate
                        { msg = UpdateDate ProcedureAvant
                        , label = text "Jusqu'au (inclus)"
                        , name = "filtre-procedure-avant"
                        , value = proceduresAvant
                        , min = proceduresApres
                        , max = Nothing
                        , disabled = not <| List.member ProcedureOui procedures
                        }
                    ]
                ]
            ]
        ]


filialesFilter : MultiSelectRemote.SmartSelect Msg EntrepriseApercu -> List EntrepriseApercu -> Html Msg
filialesFilter selectFiliales filiales =
    div []
        [ div [ class "flex flex-col gap-4" ]
            [ selectFiliales
                |> MultiSelectRemote.viewCustom
                    { isDisabled = False
                    , selected = filiales
                    , optionLabelFn =
                        \code ->
                            if String.length code.id == 1 then
                                " == " ++ code.id ++ " - " ++ code.nom ++ " == "

                            else if String.length code.id == 2 then
                                " = " ++ code.id ++ " - " ++ code.nom ++ " = "

                            else
                                code.id ++ " - " ++ code.nom
                    , optionDescriptionFn = \_ -> ""
                    , optionsContainerMaxHeight = 300
                    , selectTitle =
                        div []
                            [ span [ Typo.textBold ]
                                [ text "Filiales"
                                , sup [] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ]
                                    |> (DSFR.Tooltip.survol { label = text "Chercher tous les établissements appartenant aux filiales des entreprises suivantes", id = "tooltip-filtres-filiales" }
                                            |> DSFR.Tooltip.wrap
                                       )
                                ]
                            ]
                    , viewSelectedOptionFn = text << (\code -> code.id ++ " - " ++ code.nom)
                    , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                    , noResultsForMsg =
                        \searchText ->
                            "Aucune autre entreprise n'a été trouvée"
                                ++ (if searchText == "" then
                                        ""

                                    else
                                        " pour " ++ searchText
                                   )
                    , noOptionsMsg = "Aucune autre entreprise n'a été trouvée"
                    , newOption = Nothing
                    , searchPrompt = "Rechercher une entreprise"
                    }
            , div []
                [ case filiales of
                    [] ->
                        nothing

                    _ ->
                        filiales
                            |> List.map
                                (\f ->
                                    { data = f
                                    , toString = \{ id, nom } -> id ++ " - " ++ nom
                                    }
                                        |> DSFR.Tag.deletable UnselectFiliale
                                        |> DSFR.Tag.withAttrs [ Attr.title <| f.id ++ " - " ++ f.nom ]
                                )
                            |> DSFR.Tag.medium
                ]
            ]
        ]


detentionsFilter : MultiSelectRemote.SmartSelect Msg EntrepriseApercu -> List EntrepriseApercu -> Html Msg
detentionsFilter selectDetentions detentions =
    div []
        [ div [ class "flex flex-col gap-4" ]
            [ selectDetentions
                |> MultiSelectRemote.viewCustom
                    { isDisabled = False
                    , selected = detentions
                    , optionLabelFn =
                        \code ->
                            if String.length code.id == 1 then
                                " == " ++ code.id ++ " - " ++ code.nom ++ " == "

                            else if String.length code.id == 2 then
                                " = " ++ code.id ++ " - " ++ code.nom ++ " = "

                            else
                                code.id ++ " - " ++ code.nom
                    , optionDescriptionFn = \_ -> ""
                    , optionsContainerMaxHeight = 300
                    , selectTitle =
                        div []
                            [ span [ Typo.textBold ]
                                [ text "Détentions"
                                , sup []
                                    [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ]
                                        |> (DSFR.Tooltip.survol { label = text "Chercher tous les établissements des entreprises possédant les entreprises suivantes", id = "tooltip-filtres-detentions" }
                                                |> DSFR.Tooltip.wrap
                                           )
                                    ]
                                ]
                            ]
                    , viewSelectedOptionFn = text << (\code -> code.id ++ " - " ++ code.nom)
                    , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                    , noResultsForMsg =
                        \searchText ->
                            "Aucune autre entreprise n'a été trouvée"
                                ++ (if searchText == "" then
                                        ""

                                    else
                                        " pour " ++ searchText
                                   )
                    , noOptionsMsg = "Aucune autre entreprise n'a été trouvée"
                    , newOption = Nothing
                    , searchPrompt = "Rechercher une entreprise"
                    }
            , div []
                [ case detentions of
                    [] ->
                        nothing

                    _ ->
                        detentions
                            |> List.map
                                (\f ->
                                    { data = f
                                    , toString = \{ id, nom } -> id ++ " - " ++ nom
                                    }
                                        |> DSFR.Tag.deletable UnselectDetention
                                        |> DSFR.Tag.withAttrs [ Attr.title <| f.id ++ " - " ++ f.nom ]
                                )
                            |> DSFR.Tag.medium
                ]
            ]
        ]


egaproIndiceFilter : String -> String -> Html Msg
egaproIndiceFilter egaproIndiceMin egaproIndiceMax =
    div
        [ class "flex flex-col gap-4" ]
        [ div []
            [ DSFR.Range.new
                { id = "egapro-indice-range"
                , label = span [ Typo.textBold ] [ text "Note Egapro" ]
                , min = 0
                , max = 100
                , step = 1
                }
                |> DSFR.Range.double
                    { valueMin = egaproIndiceMin |> String.toFloat |> Maybe.withDefault 0
                    , valueMax = egaproIndiceMax |> String.toFloat |> Maybe.withDefault 100
                    , onChangeMin = UpdatedEgaproIndiceMin << String.fromFloat
                    , onChangeMax = UpdatedEgaproIndiceMax << String.fromFloat
                    }
                |> DSFR.Range.view
            ]
        ]


egaproCadresFilter : String -> String -> Html Msg
egaproCadresFilter egaproCadresMin egaproCadresMax =
    div
        [ class "flex flex-col gap-4" ]
        [ div []
            [ DSFR.Range.new
                { id = "egapro-cadres-range"
                , label = span [ Typo.textBold ] [ text "Cadres dirigeantes" ]
                , min = 0
                , max = 100
                , step = 0.1
                }
                |> DSFR.Range.double
                    { valueMin = egaproCadresMin |> String.toFloat |> Maybe.withDefault 0
                    , valueMax = egaproCadresMax |> String.toFloat |> Maybe.withDefault 100
                    , onChangeMin = UpdatedEgaproCadresMin << String.fromFloat
                    , onChangeMax = UpdatedEgaproCadresMax << String.fromFloat
                    }
                |> DSFR.Range.withDescription "Représentation des femmes en pourcentage"
                |> DSFR.Range.view
            ]
        ]


egaproInstancesFilter : String -> String -> Html Msg
egaproInstancesFilter egaproInstancesMin egaproInstancesMax =
    div
        [ class "flex flex-col gap-4" ]
        [ div []
            [ DSFR.Range.new
                { id = "egapro-instances-range"
                , label = span [ Typo.textBold ] [ text "Membres instances dirigeantes" ]
                , min = 0
                , max = 100
                , step = 0.1
                }
                |> DSFR.Range.double
                    { valueMin = egaproInstancesMin |> String.toFloat |> Maybe.withDefault 0
                    , valueMax = egaproInstancesMax |> String.toFloat |> Maybe.withDefault 100
                    , onChangeMin = UpdatedEgaproInstancesMin << String.fromFloat
                    , onChangeMax = UpdatedEgaproInstancesMax << String.fromFloat
                    }
                |> DSFR.Range.withDescription "Représentation des femmes en pourcentage"
                |> DSFR.Range.view
            ]
        ]


subventionsFilter : List Subvention -> Maybe String -> Maybe String -> Maybe String -> Maybe String -> Html Msg
subventionsFilter subventions subventionsAnneeMin subventionsAnneeMax subventionsMontantMin subventionsMontantMax =
    div [ class "flex flex-col gap-4" ]
        [ div []
            [ DSFR.Checkbox.group
                { id = "filtres-subvention"
                , values =
                    [ SubventionOui
                    , SubventionNon
                    ]
                , toLabel = text << subventionToDisplay
                , toId = subventionToString
                , checked = subventions
                , valueAsString = subventionToString
                , onChecked =
                    \data bool -> SetSubventionsFilter bool data
                , label =
                    div [ Typo.textBold ]
                        [ span [ Typo.textBold ]
                            [ text "Subvention reçue"
                            , sup [] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ]
                                |> (DSFR.Tooltip.survol { label = text "Subventions de la Politique de la Ville uniquement", id = "tooltip-filtres-subventions" }
                                        |> DSFR.Tooltip.wrap
                                   )
                            ]
                        ]
                }
                |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
                |> DSFR.Checkbox.viewGroup
            ]
        , div [ class "flex flex-col gap-2" ]
            [ div [ Typo.textBold, class "!mb-0" ] [ text "Année des subventions" ]
            , div [ class "flex flex-row justify-between gap-2" ]
                [ div [ class "w-full" ]
                    [ DSFR.Input.new
                        { value = subventionsAnneeMin |> Maybe.withDefault ""
                        , onInput =
                            (\v ->
                                if v == "" then
                                    Nothing

                                else
                                    Just v
                            )
                                >> SetSubventionsAnneeMin
                        , label = text "À partir de"
                        , name = "select-subventions-annee-min"
                        }
                        |> DSFR.Input.select
                            { options =
                                [ "2020"
                                , "2021"
                                , "2022"
                                , "2023"
                                ]
                            , toId = identity
                            , toLabel = text
                            , toDisabled = Nothing
                            }
                        |> DSFR.Input.withDisabled (not <| List.member SubventionOui subventions)
                        |> DSFR.Input.view
                    ]
                , div [ class "w-full" ]
                    [ DSFR.Input.new
                        { value = subventionsAnneeMax |> Maybe.withDefault ""
                        , onInput =
                            (\v ->
                                if v == "" then
                                    Nothing

                                else
                                    Just v
                            )
                                >> SetSubventionsAnneeMax
                        , label = text "Jusqu'à"
                        , name = "select-subventions-annee-max"
                        }
                        |> DSFR.Input.select
                            { options =
                                [ "2020"
                                , "2021"
                                , "2022"
                                , "2023"
                                ]
                            , toId = identity
                            , toLabel = text
                            , toDisabled = Nothing
                            }
                        |> DSFR.Input.withDisabled (not <| List.member SubventionOui subventions)
                        |> DSFR.Input.view
                    ]
                ]
            ]
        , let
            maybeMin =
                subventionsMontantMin
                    |> Maybe.andThen String.toInt

            maybeMax =
                subventionsMontantMax
                    |> Maybe.andThen String.toInt

            invalidRange =
                case ( maybeMin, maybeMax ) of
                    ( Nothing, Nothing ) ->
                        False

                    ( Just _, Nothing ) ->
                        False

                    ( Nothing, Just _ ) ->
                        False

                    ( Just min, Just max ) ->
                        min > max
          in
          div [ class "flex flex-col gap-2" ]
            [ div [ Typo.textBold, class "!mb-0" ] [ text "Montant total des subventions (en €)" ]
            , div [ class "flex flex-row justify-between gap-4" ]
                [ div [ class "w-full" ]
                    [ DSFR.Input.new
                        { value = subventionsMontantMin |> Maybe.withDefault ""
                        , onInput =
                            (\s ->
                                if s == "" then
                                    Nothing

                                else
                                    Just s
                            )
                                >> UpdatedSubventionsMontantMin
                        , label = text "Minimum"
                        , name = "subventions-montant-minimum"
                        }
                        |> DSFR.Input.withDisabled (not <| List.member SubventionOui subventions)
                        |> DSFR.Input.number (Just { step = Just "100", min = Just "0", max = Just "1000000" })
                        |> DSFR.Input.withHint [ text "Format attendu\u{00A0}: xxxxx" ]
                        |> DSFR.Input.withError
                            (if invalidRange then
                                Just [ text "Intervalle incohérent" ]

                             else
                                Nothing
                            )
                        |> DSFR.Input.view
                    ]
                , div [ class "w-full" ]
                    [ DSFR.Input.new
                        { value = subventionsMontantMax |> Maybe.withDefault ""
                        , onInput =
                            (\s ->
                                if s == "" then
                                    Nothing

                                else
                                    Just s
                            )
                                >> UpdatedSubventionsMontantMax
                        , label = text "Maximum"
                        , name = "subventions-montant-maximum"
                        }
                        |> DSFR.Input.withDisabled (not <| List.member SubventionOui subventions)
                        |> DSFR.Input.number (Just { step = Just "100", min = Just "0", max = Just "1000000" })
                        |> DSFR.Input.withHint [ text "Format attendu\u{00A0}: xxxxx" ]
                        |> DSFR.Input.withError
                            (if invalidRange then
                                Just [ text "Intervalle incohérent" ]

                             else
                                Nothing
                            )
                        |> DSFR.Input.view
                    ]
                ]
            ]
        ]


caVariationFilter : List VariationType -> Maybe String -> Maybe String -> Html Msg
caVariationFilter caVariations caVariationMin caVariationMax =
    div [ class "flex flex-col gap-4" ]
        [ div []
            [ DSFR.Checkbox.group
                { id = "filtres-ca-variation"
                , values =
                    [ VariationHausse
                    , VariationBaisse
                    ]
                , toLabel = text << variationTypeToDisplay
                , toId = variationTypeToString
                , checked = caVariations
                , valueAsString = variationTypeToString
                , onChecked =
                    \data bool -> SetCaVariationFilter bool data
                , label =
                    div [ Typo.textBold ]
                        [ span [ Typo.textBold ]
                            [ text "Variation de CA"
                            ]
                        ]
                }
                |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
                |> DSFR.Checkbox.viewGroup
            ]
        , let
            maybeMin =
                caVariationMin
                    |> Maybe.andThen String.toFloat

            maybeMax =
                caVariationMax
                    |> Maybe.andThen String.toFloat

            invalidRange =
                case ( maybeMin, maybeMax ) of
                    ( Nothing, Nothing ) ->
                        False

                    ( Just _, Nothing ) ->
                        False

                    ( Nothing, Just _ ) ->
                        False

                    ( Just min, Just max ) ->
                        min > max
          in
          div [ class "flex flex-col gap-2" ]
            [ div [ Typo.textBold, class "!mb-0" ] [ text "Variation du CA (en %)" ]
            , div [ class "flex flex-row justify-between gap-4" ]
                [ div [ class "w-full" ]
                    [ DSFR.Input.new
                        { value = caVariationMin |> Maybe.withDefault ""
                        , onInput =
                            (\s ->
                                if s == "" then
                                    Nothing

                                else
                                    Just s
                            )
                                >> UpdatedCaVariationMin
                        , label = text "Minimum"
                        , name = "ca-variation-minimum"
                        }
                        |> DSFR.Input.withDisabled (List.length caVariations == 0)
                        |> DSFR.Input.number (Just { step = Just "1", min = Just "0", max = Just "1000" })
                        |> DSFR.Input.withHint [ text "Format attendu\u{00A0}: xxx.xx" ]
                        |> DSFR.Input.withError
                            (if invalidRange then
                                Just [ text "Intervalle incohérent" ]

                             else
                                Nothing
                            )
                        |> DSFR.Input.view
                    ]
                , div [ class "w-full" ]
                    [ DSFR.Input.new
                        { value = caVariationMax |> Maybe.withDefault ""
                        , onInput =
                            (\s ->
                                if s == "" then
                                    Nothing

                                else
                                    Just s
                            )
                                >> UpdatedCaVariationMax
                        , label = text "Maximum"
                        , name = "ca-variation-maximum"
                        }
                        |> DSFR.Input.withDisabled (List.length caVariations == 0)
                        |> DSFR.Input.number (Just { step = Just "1", min = Just "0", max = Just "1000" })
                        |> DSFR.Input.withHint [ text "Format attendu\u{00A0}: xxx.xx" ]
                        |> DSFR.Input.withError
                            (if invalidRange then
                                Just [ text "Intervalle incohérent" ]

                             else
                                Nothing
                            )
                        |> DSFR.Input.view
                    ]
                ]
            ]
        ]


effectifVariationFilter : List VariationType -> Maybe String -> Maybe String -> Html Msg
effectifVariationFilter effectifVariations effectifVariationMin effectifVariationMax =
    div [ class "flex flex-col gap-4" ]
        [ div []
            [ DSFR.Checkbox.group
                { id = "filtres-effectif-variation"
                , values =
                    [ VariationHausse
                    , VariationBaisse
                    ]
                , toLabel = text << variationTypeToDisplay
                , toId = variationTypeToString
                , checked = effectifVariations
                , valueAsString = variationTypeToString
                , onChecked =
                    \data bool -> SetEffectifVariationFilter bool data
                , label =
                    div [ Typo.textBold ]
                        [ span [ Typo.textBold ]
                            [ text "Variation d'effectif"
                            ]
                        ]
                }
                |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
                |> DSFR.Checkbox.viewGroup
            ]
        , let
            maybeMin =
                effectifVariationMin
                    |> Maybe.andThen String.toFloat

            maybeMax =
                effectifVariationMax
                    |> Maybe.andThen String.toFloat

            invalidRange =
                case ( maybeMin, maybeMax ) of
                    ( Nothing, Nothing ) ->
                        False

                    ( Just _, Nothing ) ->
                        False

                    ( Nothing, Just _ ) ->
                        False

                    ( Just min, Just max ) ->
                        min > max
          in
          div [ class "flex flex-col gap-2" ]
            [ div [ Typo.textBold, class "!mb-0" ] [ text "Variation des effectifs (en %)" ]
            , div [ class "flex flex-row justify-between gap-4" ]
                [ div [ class "w-full" ]
                    [ DSFR.Input.new
                        { value = effectifVariationMin |> Maybe.withDefault ""
                        , onInput =
                            (\s ->
                                if s == "" then
                                    Nothing

                                else
                                    Just s
                            )
                                >> UpdatedEffectifVarMin
                        , label = text "Minimum"
                        , name = "effectif-variation-minimum"
                        }
                        |> DSFR.Input.withDisabled (List.length effectifVariations == 0)
                        |> DSFR.Input.number (Just { step = Just "1", min = Just "0", max = Just "1000" })
                        |> DSFR.Input.withHint [ text "Format attendu\u{00A0}: xxx.xx" ]
                        |> DSFR.Input.withError
                            (if invalidRange then
                                Just [ text "Intervalle incohérent" ]

                             else
                                Nothing
                            )
                        |> DSFR.Input.view
                    ]
                , div [ class "w-full" ]
                    [ DSFR.Input.new
                        { value = effectifVariationMax |> Maybe.withDefault ""
                        , onInput =
                            (\s ->
                                if s == "" then
                                    Nothing

                                else
                                    Just s
                            )
                                >> UpdatedEffectifVariationMax
                        , label = text "Maximum"
                        , name = "effectif-variation-maximum"
                        }
                        |> DSFR.Input.withDisabled (List.length effectifVariations == 0)
                        |> DSFR.Input.number (Just { step = Just "1", min = Just "0", max = Just "1000" })
                        |> DSFR.Input.withHint [ text "Format attendu\u{00A0}: xxx.xx" ]
                        |> DSFR.Input.withError
                            (if invalidRange then
                                Just [ text "Intervalle incohérent" ]

                             else
                                Nothing
                            )
                        |> DSFR.Input.view
                    ]
                ]
            ]
        ]


communesFilter : MultiSelectRemote.SmartSelect Msg Commune -> List Commune -> Html Msg
communesFilter selectCommunes communes =
    let
        communeToLabel c =
            case ( c.nom, c.departementId ) of
                ( "", "" ) ->
                    c.id

                _ ->
                    c.nom ++ " (" ++ c.departementId ++ ")"
    in
    div [ class "flex flex-col gap-2" ]
        [ selectCommunes
            |> MultiSelectRemote.viewCustom
                { isDisabled = False
                , selected = communes
                , optionLabelFn = communeToLabel
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle =
                    div [ class "flex flex-col" ]
                        [ span [ Typo.textBold ]
                            [ text "Communes"
                            , sup [] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ]
                                |> (DSFR.Tooltip.survol
                                        { label = text hintResultatsTerritoires
                                        , id = "resultats-limites-communes"
                                        }
                                        |> DSFR.Tooltip.wrap
                                   )
                            ]
                        ]
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre commune n'a été trouvée"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucune commune n'a été trouvée"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                , newOption = Nothing
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , searchPrompt = "Rechercher par nom, code postal, ou code INSEE"
                }
        , case communes of
            [] ->
                nothing

            _ ->
                communes
                    |> List.map (\commune -> DSFR.Tag.deletable UnselectCommune { data = commune, toString = communeToLabel })
                    |> DSFR.Tag.medium
        ]


epcisFilter : MultiSelectRemote.SmartSelect Msg Epci -> List Epci -> Html Msg
epcisFilter selectEpcis epcis =
    div [ class "flex flex-col gap-2" ]
        [ selectEpcis
            |> MultiSelectRemote.viewCustom
                { isDisabled = False
                , selected = epcis
                , optionLabelFn = \epci -> epci.id ++ " - " ++ epci.nom
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle =
                    div [ class "flex flex-col" ]
                        [ span [ Typo.textBold ]
                            [ text "EPCIs"
                            , sup [] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ]
                                |> (DSFR.Tooltip.survol
                                        { label = text hintResultatsTerritoires
                                        , id = "resultats-limites-epcis"
                                        }
                                        |> DSFR.Tooltip.wrap
                                   )
                            ]
                        ]
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucun autre EPCI n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun EPCI n'a été trouvé"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                , newOption = Nothing
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , searchPrompt = "Rechercher par nom ou numéro de SIREN"
                }
        , case epcis of
            [] ->
                nothing

            _ ->
                let
                    ( toLabel, toTitle ) =
                        dataToLabelAndToTitle
                in
                epcis
                    |> List.map
                        (\epci ->
                            { data = epci, toString = toLabel }
                                |> DSFR.Tag.deletable UnselectEpci
                                |> DSFR.Tag.withAttrs
                                    [ Attr.title <| toTitle <| epci
                                    ]
                        )
                    |> DSFR.Tag.medium
        ]


departementsFilter : MultiSelectRemote.SmartSelect Msg Departement -> List Departement -> Html Msg
departementsFilter selectDepartements departements =
    div [ class "flex flex-col gap-2" ]
        [ selectDepartements
            |> MultiSelectRemote.viewCustom
                { isDisabled = False
                , selected = departements
                , optionLabelFn = \d -> d.id ++ " - " ++ d.nom
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle =
                    div [ class "flex flex-col" ]
                        [ span [ Typo.textBold ]
                            [ text "Départements"
                            , sup [] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ]
                                |> (DSFR.Tooltip.survol
                                        { label = text hintResultatsTerritoires
                                        , id = "resultats-limites-departements"
                                        }
                                        |> DSFR.Tooltip.wrap
                                   )
                            ]
                        ]
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucun autre département n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun département n'a été trouvé"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                , newOption = Nothing
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , searchPrompt = "Rechercher par nom ou numéro"
                }
        , case departements of
            [] ->
                nothing

            _ ->
                let
                    ( toLabel, toTitle ) =
                        dataToLabelAndToTitle
                in
                departements
                    |> List.map
                        (\departement ->
                            { data = departement, toString = toLabel }
                                |> DSFR.Tag.deletable UnselectDepartement
                                |> DSFR.Tag.withAttrs
                                    [ Attr.title <| toTitle <| departement
                                    ]
                        )
                    |> DSFR.Tag.medium
        ]


regionsFilter : MultiSelectRemote.SmartSelect Msg Region -> List Region -> Html Msg
regionsFilter selectRegion regions =
    div [ class "flex flex-col gap-2" ]
        [ selectRegion
            |> MultiSelectRemote.viewCustom
                { isDisabled = False
                , selected = regions
                , optionLabelFn = \region -> region.id ++ " - " ++ region.nom
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle =
                    div [ class "flex flex-col" ]
                        [ span [ Typo.textBold ]
                            [ text "Régions"
                            , sup [] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ]
                                |> (DSFR.Tooltip.survol
                                        { label = text hintResultatsTerritoires
                                        , id = "resultats-limites-regions"
                                        }
                                        |> DSFR.Tooltip.wrap
                                   )
                            ]
                        ]
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre région n'a été trouvée"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucune région n'a été trouvée"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                , newOption = Nothing
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , searchPrompt = "Rechercher par nom ou code INSEE"
                }
        , case regions of
            [] ->
                nothing

            _ ->
                let
                    ( toLabel, toTitle ) =
                        dataToLabelAndToTitle
                in
                regions
                    |> List.map
                        (\region ->
                            { data = region, toString = toLabel }
                                |> DSFR.Tag.deletable UnselectRegion
                                |> DSFR.Tag.withAttrs
                                    [ Attr.title <| toTitle <| region
                                    ]
                        )
                    |> DSFR.Tag.medium
        ]


adresseFilter : SingleSelectRemote.SmartSelect Msg ApiStreet -> Maybe ApiStreet -> Html Msg
adresseFilter selectAdresse selectedAdresse =
    div [ class "flex flex-col gap-2" ]
        [ selectAdresse
            |> SingleSelectRemote.viewCustom
                { isDisabled = False
                , selected = selectedAdresse
                , optionLabelFn = .label
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "Adresse" ]
                , searchPrompt = "Rechercher une adresse"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre adresse n'a été trouvée"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucune adresse n'a été trouvée"
                , error = Nothing
                }
        , selectedAdresse
            |> viewMaybe
                (\adresse ->
                    DSFR.Tag.deletable (\_ -> ClearAdresse) { data = adresse.label, toString = identity }
                        |> List.singleton
                        |> DSFR.Tag.medium
                )
        ]


explicationEtiquettesToutes : String
explicationEtiquettesToutes =
    "Avec toutes les étiquettes sélectionnées"


activitesFilter : MultiSelectRemote.SmartSelect Msg String -> Bool -> List String -> Html Msg
activitesFilter selectActivites activitesToutes activites =
    div [ class "flex flex-col gap-2" ]
        [ selectActivites
            |> MultiSelectRemote.viewCustom
                { isDisabled = False
                , selected = activites
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "Activité réelle et filière" ]
                , viewSelectedOptionFn = text
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre activité n'a été trouvée"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucune activité n'a été trouvée"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                , newOption = Nothing
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , searchPrompt = "Rechercher une activité"
                }
        , case activites of
            [] ->
                nothing

            _ ->
                activites
                    |> List.map (\activite -> DSFR.Tag.deletable UnselectActivite { data = activite, toString = identity })
                    |> DSFR.Tag.medium
        , div []
            [ DSFR.Toggle.single
                { value = True
                , checked = Just activitesToutes
                , valueAsString =
                    \v ->
                        if v then
                            "oui"

                        else
                            "non"
                , id = "activites-toutes"
                , label = span [ Typo.textSm, class "!mb-0" ] [ text explicationEtiquettesToutes ]
                , onChecked = \_ _ -> ToggleActivitesToutes
                }
                |> DSFR.Toggle.singleWithLabelLeft
                |> DSFR.Toggle.singleWithNoState
                |> DSFR.Toggle.viewSingle
            , span [ Typo.textSm, class "italic" ] <|
                List.singleton <|
                    text <|
                        if activitesToutes then
                            "TOUTES ces activités"

                        else
                            "AU MOINS une de ces activités"
            ]
        , hr [ class "fr-hr !mb-0 !pb-0 h-[1px]" ] []
        ]


zonesFilter : MultiSelectRemote.SmartSelect Msg String -> Bool -> List String -> Html Msg
zonesFilter selectZones zonesToutes zones =
    div [ class "flex flex-col gap-2" ]
        [ selectZones
            |> MultiSelectRemote.viewCustom
                { isDisabled = False
                , selected = zones
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "Zone géographique" ]
                , viewSelectedOptionFn = text
                , noResultsForMsg =
                    \searchText ->
                        "Aucune autre zone géographique n'a été trouvée"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                , newOption = Nothing
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , searchPrompt = "Rechercher une zone géographique"
                }
        , case zones of
            [] ->
                nothing

            _ ->
                zones
                    |> List.map (\zone -> DSFR.Tag.deletable UnselectZone { data = zone, toString = identity })
                    |> DSFR.Tag.medium
        , div []
            [ DSFR.Toggle.single
                { value = True
                , checked = Just zonesToutes
                , valueAsString =
                    \v ->
                        if v then
                            "oui"

                        else
                            "non"
                , id = "zones-toutes"
                , label = span [ Typo.textSm, class "!mb-0" ] [ text explicationEtiquettesToutes ]
                , onChecked = \_ _ -> ToggleZonesToutes
                }
                |> DSFR.Toggle.singleWithLabelLeft
                |> DSFR.Toggle.singleWithNoState
                |> DSFR.Toggle.viewSingle
            , span [ Typo.textSm, class "italic" ] <|
                List.singleton <|
                    text <|
                        if zonesToutes then
                            "TOUTES ces localisations"

                        else
                            "AU MOINS une de ces localisations"
            ]
        , hr [ class "fr-hr !mb-0 !pb-0 h-[1px]" ] []
        ]


motsFilter : MultiSelectRemote.SmartSelect Msg String -> Bool -> List String -> Html Msg
motsFilter selectMots motsTous mots =
    div [ class "flex flex-col gap-2" ]
        [ selectMots
            |> MultiSelectRemote.viewCustom
                { isDisabled = False
                , selected = mots
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "Mots-clés" ]
                , viewSelectedOptionFn = text
                , noResultsForMsg =
                    \searchText ->
                        "Aucun autre mot-clé n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                , newOption = Nothing
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , searchPrompt = "Rechercher un mot-clé"
                }
        , case mots of
            [] ->
                nothing

            _ ->
                mots
                    |> List.map (\mot -> DSFR.Tag.deletable UnselectMot { data = mot, toString = identity })
                    |> DSFR.Tag.medium
        , div []
            [ DSFR.Toggle.single
                { value = True
                , checked = Just motsTous
                , valueAsString =
                    \v ->
                        if v then
                            "oui"

                        else
                            "non"
                , id = "mots-tous"
                , label = span [ Typo.textSm, class "!mb-0" ] [ text explicationEtiquettesToutes ]
                , onChecked = \_ _ -> ToggleMotsTous
                }
                |> DSFR.Toggle.singleWithLabelLeft
                |> DSFR.Toggle.singleWithNoState
                |> DSFR.Toggle.viewSingle
            , span [ Typo.textSm, class "italic" ] <|
                List.singleton <|
                    text <|
                        if motsTous then
                            "TOUS ces mots-clés"

                        else
                            "AU MOINS un de ces mots-clés"
            ]
        , hr [ class "fr-hr !mb-0 !pb-0 h-[1px]" ] []
        ]


qpvsFilter : MultiSelectRemote.SmartSelect Msg Qpv -> Bool -> List Qpv -> Html Msg
qpvsFilter selectQpvs tousQpvs qpvs =
    div [ class "flex flex-col gap-2" ]
        [ selectQpvs
            |> MultiSelectRemote.viewCustom
                { isDisabled = tousQpvs
                , selected = qpvs
                , optionLabelFn = \qpv -> qpv.id ++ " - " ++ qpv.nom
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle =
                    div [ class "flex flex-col" ]
                        [ span [ Typo.textBold ]
                            [ text "QPV - Quartier Prioritaire de la Politique de la Ville"
                            , sup [] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ]
                                |> (DSFR.Tooltip.survol
                                        { label = text hintResultatsTerritoires
                                        , id = "resultats-limites-qpvs"
                                        }
                                        |> DSFR.Tooltip.wrap
                                   )
                            ]
                        ]
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucun autre QPV n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun QPV n'a été trouvé"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                , newOption = Nothing
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , searchPrompt = "Rechercher par nom ou par code"
                }
        , case qpvs of
            [] ->
                nothing

            _ ->
                let
                    ( toLabel, toTitle ) =
                        dataToLabelAndToTitle
                in
                qpvs
                    |> List.map
                        (\qpv ->
                            { data = qpv, toString = toLabel }
                                |> DSFR.Tag.deletable UnselectQpv
                                |> DSFR.Tag.withAttrs
                                    [ Attr.title <| toTitle <| qpv
                                    ]
                        )
                    |> DSFR.Tag.medium
        , DSFR.Checkbox.single
            { value = "qpvs-tous-selection"
            , checked = Just tousQpvs
            , valueAsString = identity
            , id = "qpvs-tous-selection"
            , label = text "Tous les QPV"
            , onChecked = \_ bool -> ToggleAllQpvs bool
            }
            |> DSFR.Checkbox.viewSingle
        ]


tisFilter : MultiSelectRemote.SmartSelect Msg TerritoireIndustrie -> Bool -> List TerritoireIndustrie -> Html Msg
tisFilter selectTis tousTis tis =
    div [ class "flex flex-col gap-2" ]
        [ selectTis
            |> MultiSelectRemote.viewCustom
                { isDisabled = tousTis
                , selected = tis
                , optionLabelFn = .nom
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle =
                    div [ class "flex flex-col" ]
                        [ span [ Typo.textBold ]
                            [ text "Territoire d'industrie"
                            ]
                        ]
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucun autre Territoire d'industrie n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucun Territoire d'industrie n'a été trouvé"
                , characterThresholdPrompt = \diff -> "Veuillez taper encore " ++ String.fromInt diff ++ " caractère" ++ Lib.UI.plural diff ++ " pour lancer la recherche"
                , newOption = Nothing
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , searchPrompt = "Rechercher un Territoire d'industrie"
                }
        , case tis of
            [] ->
                nothing

            _ ->
                tis
                    |> List.map (\ti -> DSFR.Tag.deletable UnselectTi { data = ti, toString = .nom })
                    |> DSFR.Tag.medium
        , DSFR.Checkbox.single
            { value = "tis-tous-selection"
            , checked = Just tousTis
            , valueAsString = identity
            , id = "tis-tous-selection"
            , label = text "Tous les Territoires d'industrie"
            , onChecked = \_ bool -> ToggleAllTis bool
            }
            |> DSFR.Checkbox.viewSingle
        ]


demandesFilter : MultiSelect.SmartSelect Msg TypeDemande -> List TypeDemande -> Html Msg
demandesFilter selectDemandes demandes =
    div [ class "flex flex-col gap-4" ]
        [ selectDemandes
            |> MultiSelect.viewCustom
                { isDisabled = False
                , selected = demandes
                , options = Data.Demande.listeEtablissement
                , optionLabelFn = Data.Demande.typeDemandeToDisplay
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Demandes en cours"
                , viewSelectedOptionFn = Data.Demande.typeDemandeToDisplay >> text
                , noResultsForMsg =
                    \searchText ->
                        "Aucun autre type de demande n'a été trouvé"
                            ++ (if searchText == "" then
                                    ""

                                else
                                    " pour " ++ searchText
                               )
                , noOptionsMsg = "Aucune demande n'a été trouvée"
                , searchFn = filterBy Data.Demande.typeDemandeToDisplay
                , searchPrompt = "Filtrer par demande en cours"
                }
        , case demandes of
            [] ->
                nothing

            _ ->
                demandes
                    |> List.map (\demande -> DSFR.Tag.deletable UnselectDemande { data = demande, toString = Data.Demande.typeDemandeToDisplay })
                    |> DSFR.Tag.medium
        ]


creeFilter : Maybe Date -> Maybe Date -> Html Msg
creeFilter apres avant =
    div [ class "flex flex-col gap-4" ]
        [ span [ Typo.textBold ] [ text "Date de création" ]
        , div [ class "flex flex-row justify_between gap-2" ]
            [ selecteurDate
                { msg = UpdateDate CreeApres
                , label = span [ class "!mb-0" ] [ text "À partir du" ]
                , name = "filtre-cree-apres"
                , value = apres
                , min = Nothing
                , max = Nothing
                , disabled = False
                }
            , selecteurDate
                { msg = UpdateDate CreeAvant
                , label = text "Jusqu'au (inclus)"
                , name = "filtre-cree-avant"
                , value = avant
                , min = apres
                , max = Nothing
                , disabled = False
                }
            ]
        ]


fermeFilter : Maybe Date -> Maybe Date -> Maybe Date -> Html Msg
fermeFilter creeApres apres avant =
    let
        fermeAvantOuvert =
            Maybe.withDefault False <|
                Maybe.map2 Lib.Date.firstDateIsBeforeSecondDate
                    avant
                    creeApres
    in
    div [ class "flex flex-col gap-4" ]
        [ span [ Typo.textBold ] [ text "Date de fermeture" ]
        , div [ class "flex flex-row justify_between gap-2" ]
            [ selecteurDate
                { msg = UpdateDate FermeApres
                , label = text "À partir du"
                , name = "filtre-ferme-apres"
                , value = apres
                , min = Nothing
                , max = Nothing
                , disabled = False
                }
            , selecteurDate
                { msg = UpdateDate FermeAvant
                , label = text "Jusqu'au (inclus)"
                , name = "filtre-ferme-avant"
                , value = avant
                , min = apres
                , max = Nothing
                , disabled = False
                }
            ]
        , viewIf fermeAvantOuvert
            (DSFR.Alert.small
                { title = Nothing
                , description = text "Les dates de fermeture sont avant les dates d'ouverture."
                }
                |> DSFR.Alert.alert Nothing DSFR.Alert.warning
            )
        ]


accompagnesFilter : List Accompagnes -> Maybe Date -> Maybe Date -> Html Msg
accompagnesFilter accompagnes accompagnesApres accompagnesAvant =
    div [ class "flex flex-col gap-4" ]
        [ span [ Typo.textBold ] [ text "Établissement accompagné" ]
        , span
            [ class "fr-hint-text"
            ]
            [ text "Au moins un échange/demande/rappel saisi" ]
        , DSFR.Checkbox.group
            { id = "filtres-accompagnes"
            , values =
                [ AccompagnesOui
                , AccompagnesNon
                ]
            , toLabel =
                text
                    << (\e ->
                            case e of
                                AccompagnesOui ->
                                    "Oui"

                                AccompagnesNon ->
                                    "Non"
                       )
            , toId =
                \e ->
                    case e of
                        AccompagnesOui ->
                            "oui"

                        AccompagnesNon ->
                            "non"
            , checked = accompagnes
            , valueAsString =
                \e ->
                    case e of
                        AccompagnesOui ->
                            "oui"

                        AccompagnesNon ->
                            "non"
            , onChecked =
                \data bool -> SetAccompagnesFilter bool data
            , label = nothing
            }
            |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
            |> DSFR.Checkbox.viewGroup
        , div [ class "flex flex-col gap-4" ]
            [ selecteurDate
                { msg = UpdateAccompagnesDate AccompagnesApres
                , label = text "À partir du"
                , name = "filtre-accompagnes-apres"
                , value = accompagnesApres
                , min = Nothing
                , max = Nothing
                , disabled = not <| List.member AccompagnesOui accompagnes
                }
            , selecteurDate
                { msg = UpdateAccompagnesDate AccompagnesAvant
                , label = text "Jusqu'au (inclus)"
                , name = "filtre-accompagnes-avant"
                , value = accompagnesAvant
                , min = accompagnesApres
                , max = Nothing
                , disabled = not <| List.member AccompagnesOui accompagnes
                }
            ]
        ]


showFilterTags : Model -> Html Msg
showFilterTags model =
    let
        currentUrl =
            model.currentUrl

        pagination =
            let
                p =
                    queryToPagination currentUrl
            in
            { p | page = 1 }

        filters =
            model.currentUrl
                |> queryToFilters
                |> hydrateFilters model.filtresRequete

        { recherche, etat, categoriesJuridiques, categoriesJuridiquesSans, codesNaf, codesNafSans, formesJuridiques, communes, epcis, departements, regions, tis, tousTis, effectifInconnu, effectifMin, effectifMax, exercices, apiStreet, situationGeographique, entrepriseTypes, activites, activitesToutes, demandes, zones, zonesToutes, mots, motsTous, qpvs, tousQpvs, zrrs, ess, acv, pvd, va, afrs, siege, procedures, subventions, subventionsAnneeMin, subventionsAnneeMax, subventionsMontantMin, subventionsMontantMax, micro, favori, avecContact, ancienCreateur, geolocalisation, longitude, latitude, caVarTypes, caVarMin, caVarMax, effectifVarTypes, effectifVarMin, effectifVarMax, filiales, detentions, egaproIndiceMin, egaproIndiceMax, egaproCadresMin, egaproCadresMax, egaproInstancesMin, egaproInstancesMax } =
            filters

        newZrrs =
            List.map
                (\zrr ->
                    case zrr.id of
                        "P" ->
                            { zrr | label = "Partiellement classé" }

                        "C" ->
                            { zrr | label = "Classé" }

                        "N" ->
                            { zrr | label = "Non classé" }

                        _ ->
                            { zrr | label = "" }
                )
                zrrs

        newAfrs =
            List.map
                (\afr ->
                    case afr.id of
                        "O" ->
                            { afr | label = "Oui" }

                        "P" ->
                            { afr | label = "Partiel" }

                        "N" ->
                            { afr | label = "Non" }

                        _ ->
                            { afr | label = "" }
                )
                afrs

        exercicesIds =
            exercices
                |> List.map .id

        newExercices =
            Data.Exercice.tranchesExercices
                |> List.filter (\{ id } -> List.member id exercicesIds)
    in
    div
        [ class "flex flex-row gap-2 mt-1" ]
        [ DSFR.Tag.groupWrapper <|
            toFilterDeletableTags "Situation géographique"
                (if situationGeographique == SurMonTerritoire then
                    []

                 else
                    [ situationGeographique ]
                )
                (\_ -> UpdateUrl <| ( { filters | situationGeographique = SurMonTerritoire }, pagination ))
                situationGeographiqueToString
                ++ toFilterDeletableTags "Taille d'entreprise"
                    entrepriseTypes
                    (\e ->
                        UpdateUrl <|
                            ( { filters
                                | entrepriseTypes =
                                    entrepriseTypes
                                        |> List.filter
                                            (\t -> t /= e)
                              }
                            , pagination
                            )
                    )
                    entrepriseTypeToDisplay
                ++ toFilterDeletableTags "Recherche"
                    (if recherche /= "" then
                        [ recherche ]

                     else
                        []
                    )
                    (\_ -> UpdateUrl <| ( { filters | recherche = "" }, pagination ))
                    identity
                ++ toFilterDeletableTagsWithTitle
                    ("Catégorie juridique"
                        ++ (if categoriesJuridiquesSans then
                                " (exclue)"

                            else
                                ""
                           )
                    )
                    categoriesJuridiques
                    (\c ->
                        UpdateUrl <|
                            ( { filters
                                | categoriesJuridiques =
                                    filters.categoriesJuridiques
                                        |> List.filter
                                            (\t -> t.id /= c.id)
                              }
                            , pagination
                            )
                    )
                    dataToLabelAndToTitle
                ++ toFilterDeletableTagsWithTitle
                    ("Code NAF"
                        ++ (if codesNafSans then
                                " (exclu)"

                            else
                                ""
                           )
                    )
                    codesNaf
                    (\c ->
                        UpdateUrl <|
                            ( { filters
                                | codesNaf =
                                    filters.codesNaf
                                        |> List.filter
                                            (\t -> t.id /= c.id)
                              }
                            , pagination
                            )
                    )
                    dataToLabelAndToTitle
                ++ toFilterDeletableTags "Effectifs"
                    (case effectifMin of
                        Just e ->
                            [ e |> String.replace "." "," ]

                        Nothing ->
                            []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | effectifMin = Nothing
                              }
                            , pagination
                            )
                    )
                    (\e -> "minimum " ++ e)
                ++ toFilterDeletableTags "Effectifs"
                    (case effectifMax of
                        Just e ->
                            [ e |> String.replace "." "," ]

                        Nothing ->
                            []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | effectifMax = Nothing
                              }
                            , pagination
                            )
                    )
                    (\e -> "maximum " ++ e)
                ++ toFilterDeletableTags "Effectifs"
                    (if effectifInconnu then
                        [ effectifInconnu ]

                     else
                        []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | effectifInconnu = False
                              }
                            , pagination
                            )
                    )
                    (\_ -> "inconnus inclus")
                ++ toFilterDeletableTags "Chiffre d'affaires"
                    newExercices
                    (\e ->
                        UpdateUrl <|
                            ( { filters
                                | exercices =
                                    newExercices
                                        |> List.filter
                                            (\t -> t.id /= e.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.label)
                ++ toFilterDeletableTags "Économie sociale et solidaire"
                    ess
                    (\e ->
                        UpdateUrl <|
                            ( { filters
                                | ess =
                                    filters.ess
                                        |> List.filter
                                            (\t -> t /= e)
                              }
                            , pagination
                            )
                    )
                    (\e ->
                        case e of
                            EssOui ->
                                "Oui"

                            EssNon ->
                                "Non"
                    )
                ++ toFilterDeletableTags "Établissement siège"
                    siege
                    (\s ->
                        UpdateUrl <|
                            ( { filters
                                | siege =
                                    filters.siege
                                        |> List.filter
                                            (\t -> t /= s)
                              }
                            , pagination
                            )
                    )
                    (\s ->
                        case s of
                            SiegeOui ->
                                "Oui"

                            SiegeNon ->
                                "Non"
                    )
                ++ toFilterDeletableTags "Procédure collective"
                    procedures
                    (\s ->
                        UpdateUrl <|
                            ( { filters
                                | procedures =
                                    filters.procedures
                                        |> List.filter
                                            (\t -> t /= s)
                              }
                            , pagination
                            )
                    )
                    (\s ->
                        case s of
                            ProcedureOui ->
                                "Oui"

                            ProcedureNon ->
                                "Non"
                    )
                ++ toFilterDeletableTags "Procédure collective"
                    ([ filters.proceduresApres ] |> List.filterMap identity)
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | proceduresApres = Nothing
                              }
                            , pagination
                            )
                    )
                    (Lib.Date.formatDateShort >> (++) "à partir du ")
                ++ toFilterDeletableTags "Procédure collective"
                    ([ filters.proceduresAvant ] |> List.filterMap identity)
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | proceduresAvant = Nothing
                              }
                            , pagination
                            )
                    )
                    (Lib.Date.formatDateShort >> (\d -> "jusqu'au " ++ d ++ " (inclus)"))
                ++ toFilterDeletableTags "Micro-entreprise"
                    micro
                    (\s ->
                        UpdateUrl <|
                            ( { filters
                                | micro =
                                    filters.micro
                                        |> List.filter
                                            (\t -> t /= s)
                              }
                            , pagination
                            )
                    )
                    (\s ->
                        case s of
                            MicroOui ->
                                "Oui"

                            MicroNon ->
                                "Non"
                    )
                ++ toFilterDeletableTags "Subvention reçue"
                    subventions
                    (\s ->
                        UpdateUrl <|
                            ( { filters
                                | subventions =
                                    filters.subventions
                                        |> List.filter
                                            (\t -> t /= s)
                              }
                            , pagination
                            )
                    )
                    (\s ->
                        case s of
                            SubventionOui ->
                                "Oui"

                            SubventionNon ->
                                "Non"
                    )
                ++ toFilterDeletableTags "Subvention (période)"
                    []
                    (\_ ->
                        UpdateUrl <|
                            ( filters
                            , pagination
                            )
                    )
                    identity
                ++ toFilterDeletableTags "Année des subventions"
                    (case subventionsAnneeMin of
                        Just e ->
                            [ e ]

                        Nothing ->
                            []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | subventionsAnneeMin = Nothing
                              }
                            , pagination
                            )
                    )
                    (\a -> "après " ++ a)
                ++ toFilterDeletableTags "Année des subventions"
                    (case subventionsAnneeMax of
                        Just e ->
                            [ e ]

                        Nothing ->
                            []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | subventionsAnneeMax = Nothing
                              }
                            , pagination
                            )
                    )
                    (\a -> "avant " ++ a)
                ++ toFilterDeletableTags "Montant total des subventions"
                    (case subventionsMontantMin of
                        Just e ->
                            [ e |> String.replace "." "," ]

                        Nothing ->
                            []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | subventionsMontantMin = Nothing
                              }
                            , pagination
                            )
                    )
                    (\e -> "minimum " ++ e ++ " €")
                ++ toFilterDeletableTags "Montant total des subventions"
                    (case subventionsMontantMax of
                        Just e ->
                            [ e |> String.replace "." "," ]

                        Nothing ->
                            []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | subventionsMontantMax = Nothing
                              }
                            , pagination
                            )
                    )
                    (\e -> "maximum " ++ e ++ " €")
                ++ toFilterDeletableTags "Adresse"
                    (case apiStreet of
                        Nothing ->
                            []

                        Just a ->
                            [ a ]
                    )
                    (\_ -> UpdateUrl <| ( { filters | apiStreet = Nothing }, pagination ))
                    (\e -> e.label)
                ++ toFilterDeletableTags "QPV"
                    qpvs
                    (\qpv ->
                        UpdateUrl <|
                            ( { filters
                                | qpvs =
                                    filters.qpvs
                                        |> List.filter
                                            (\t -> t.id /= qpv.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.nom)
                ++ toFilterDeletableTags "QPV"
                    (if tousQpvs then
                        [ tousQpvs ]

                     else
                        []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | tousQpvs = False
                              }
                            , pagination
                            )
                    )
                    (\_ -> "Tous")
                ++ toFilterDeletableTags "TI"
                    tis
                    (\ti ->
                        UpdateUrl <|
                            ( { filters
                                | tis =
                                    filters.tis
                                        |> List.filter
                                            (\t -> t.id /= ti.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.nom)
                ++ toFilterDeletableTags "TI"
                    (if tousTis then
                        [ tousTis ]

                     else
                        []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | tousTis = False
                              }
                            , pagination
                            )
                    )
                    (\_ -> "Tous")
                ++ toFilterDeletableTags "ZRR"
                    newZrrs
                    (\zrr ->
                        UpdateUrl <|
                            ( { filters
                                | zrrs =
                                    filters.zrrs
                                        |> List.filter
                                            (\t -> t.id /= zrr.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.label)
                ++ toFilterDeletableTags "ACV"
                    acv
                    (\a ->
                        UpdateUrl <|
                            ( { filters
                                | acv =
                                    filters.acv
                                        |> List.filter
                                            (\t -> t /= a)
                              }
                            , pagination
                            )
                    )
                    (\a ->
                        case a of
                            AcvOui ->
                                "Oui"

                            AcvNon ->
                                "Non"
                    )
                ++ toFilterDeletableTags "PVD"
                    pvd
                    (\p ->
                        UpdateUrl <|
                            ( { filters
                                | pvd =
                                    filters.pvd
                                        |> List.filter
                                            (\t -> t /= p)
                              }
                            , pagination
                            )
                    )
                    (\p ->
                        case p of
                            PvdOui ->
                                "Oui"

                            PvdNon ->
                                "Non"
                    )
                ++ toFilterDeletableTags "VA"
                    va
                    (\a ->
                        UpdateUrl <|
                            ( { filters
                                | va =
                                    filters.va
                                        |> List.filter
                                            (\t -> t /= a)
                              }
                            , pagination
                            )
                    )
                    (\a ->
                        case a of
                            VaOui ->
                                "Oui"

                            VaNon ->
                                "Non"
                    )
                ++ toFilterDeletableTags "AFR"
                    newAfrs
                    (\afr ->
                        UpdateUrl <|
                            ( { filters
                                | afrs =
                                    filters.afrs
                                        |> List.filter
                                            (\t -> t.id /= afr.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.label)
                ++ toFilterDeletableTagsWithTitle "Commune"
                    communes
                    (\co ->
                        UpdateUrl <|
                            ( { filters
                                | communes =
                                    filters.communes
                                        |> List.filter
                                            (\t -> t.id /= co.id)
                              }
                            , pagination
                            )
                    )
                    dataToLabelAndToTitle
                ++ toFilterDeletableTagsWithTitle "Epci"
                    epcis
                    (\co ->
                        UpdateUrl <|
                            ( { filters
                                | epcis =
                                    filters.epcis
                                        |> List.filter
                                            (\t -> t.id /= co.id)
                              }
                            , pagination
                            )
                    )
                    dataToLabelAndToTitle
                ++ toFilterDeletableTagsWithTitle "Département"
                    departements
                    (\co ->
                        UpdateUrl <|
                            ( { filters
                                | departements =
                                    filters.departements
                                        |> List.filter
                                            (\t -> t.id /= co.id)
                              }
                            , pagination
                            )
                    )
                    dataToLabelAndToTitle
                ++ toFilterDeletableTagsWithTitle "Région"
                    regions
                    (\co ->
                        UpdateUrl <|
                            ( { filters
                                | regions =
                                    filters.regions
                                        |> List.filter
                                            (\t -> t.id /= co.id)
                              }
                            , pagination
                            )
                    )
                    dataToLabelAndToTitle
                ++ toFilterDeletableTags "Formes juridiques"
                    (if formesJuridiques == Courantes then
                        [ formesJuridiques ]

                     else
                        []
                    )
                    (\_ -> UpdateUrl <| ( { filters | formesJuridiques = Toutes }, pagination ))
                    formesToLabel
                ++ toFilterDeletableTags "État administratif"
                    etat
                    (\e ->
                        UpdateUrl <|
                            ( { filters
                                | etat = setEtats filters False e
                                , orderBy =
                                    case ( filters.orderBy, e ) of
                                        ( EtablissementFermeture, Ferme ) ->
                                            EtablissementConsultation

                                        _ ->
                                            filters.orderBy
                              }
                            , pagination
                            )
                    )
                    etatAdministratifFiltreToDisplay
                ++ toFilterDeletableTags "Géolocalisation"
                    geolocalisation
                    (\e ->
                        UpdateUrl <|
                            ( { filters
                                | geolocalisation = setGeolocalisations filters False e
                              }
                            , pagination
                            )
                    )
                    geolocalisationFiltreToDisplay
                ++ toFilterDeletableTags "Coordonnées"
                    (Maybe.map2 Tuple.pair longitude latitude
                        |> Maybe.map List.singleton
                        |> Maybe.withDefault []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | longitude = Nothing
                                , latitude = Nothing
                              }
                            , pagination
                            )
                    )
                    pointToString
                ++ toFilterDeletableTags
                    ("Activité"
                        ++ (if activitesToutes then
                                " (toutes)"

                            else
                                ""
                           )
                    )
                    activites
                    (\a ->
                        UpdateUrl <|
                            ( { filters
                                | activites =
                                    filters.activites
                                        |> List.filter
                                            (\z -> z /= a)
                              }
                            , pagination
                            )
                    )
                    identity
                ++ toFilterDeletableTags
                    ("Zone"
                        ++ (if zonesToutes then
                                " (toutes)"

                            else
                                ""
                           )
                    )
                    zones
                    (\zone ->
                        UpdateUrl <|
                            ( { filters
                                | zones =
                                    filters.zones
                                        |> List.filter
                                            (\z -> z /= zone)
                              }
                            , pagination
                            )
                    )
                    identity
                ++ toFilterDeletableTags
                    ("Mot-clé"
                        ++ (if motsTous then
                                " (toutes)"

                            else
                                ""
                           )
                    )
                    mots
                    (\m ->
                        UpdateUrl <|
                            ( { filters
                                | mots =
                                    filters.mots
                                        |> List.filter
                                            (\z -> z /= m)
                              }
                            , pagination
                            )
                    )
                    identity
                ++ toFilterDeletableTags "Demande en cours"
                    demandes
                    (\m ->
                        UpdateUrl <|
                            ( { filters
                                | demandes =
                                    filters.demandes
                                        |> List.filter
                                            (\z -> z /= m)
                              }
                            , pagination
                            )
                    )
                    Data.Demande.typeDemandeToDisplay
                ++ toFilterDeletableTags "Créé"
                    ([ filters.creeApres ] |> List.filterMap identity)
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | creeApres = Nothing
                              }
                            , pagination
                            )
                    )
                    (Lib.Date.formatDateShort >> (++) "à partir du ")
                ++ toFilterDeletableTags "Créé"
                    ([ filters.creeAvant ] |> List.filterMap identity)
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | creeAvant = Nothing
                              }
                            , pagination
                            )
                    )
                    (Lib.Date.formatDateShort >> (\d -> "jusqu'au " ++ d ++ " (inclus)"))
                ++ toFilterDeletableTags "Fermé"
                    ([ filters.fermeApres ] |> List.filterMap identity)
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | fermeApres = Nothing
                              }
                            , pagination
                            )
                    )
                    (Lib.Date.formatDateShort >> (++) "à partir du ")
                ++ toFilterDeletableTags "Fermé"
                    ([ filters.fermeAvant ] |> List.filterMap identity)
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | fermeAvant = Nothing
                              }
                            , pagination
                            )
                    )
                    (Lib.Date.formatDateShort >> (\d -> "jusqu'au " ++ d ++ " (inclus)"))
                ++ toFilterDeletableTags "Établissement accompagné"
                    filters.accompagnes
                    (\s ->
                        UpdateUrl <|
                            ( { filters
                                | accompagnes =
                                    filters.accompagnes
                                        |> List.filter
                                            (\t -> t /= s)
                              }
                            , pagination
                            )
                    )
                    (\s ->
                        case s of
                            AccompagnesOui ->
                                "Oui"

                            AccompagnesNon ->
                                "Non"
                    )
                ++ toFilterDeletableTags "Accompagné"
                    ([ filters.accompagnesApres ] |> List.filterMap identity)
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | accompagnesApres = Nothing
                              }
                            , pagination
                            )
                    )
                    (Lib.Date.formatDateShort >> (++) "à partir du ")
                ++ toFilterDeletableTags "Accompagné"
                    ([ filters.accompagnesAvant ] |> List.filterMap identity)
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | accompagnesAvant = Nothing
                              }
                            , pagination
                            )
                    )
                    (Lib.Date.formatDateShort >> (\d -> "jusqu'au " ++ d ++ " (inclus)"))
                ++ toFilterDeletableTags "Favoris"
                    favori
                    (\s ->
                        UpdateUrl <|
                            ( { filters
                                | favori =
                                    filters.favori
                                        |> List.filter
                                            (\t -> t /= s)
                              }
                            , pagination
                            )
                    )
                    (\s ->
                        case s of
                            FavoriOui ->
                                "Oui"

                            FavoriNon ->
                                "Non"
                    )
                ++ toFilterDeletableTags "Avec contact"
                    avecContact
                    (\s ->
                        UpdateUrl <|
                            ( { filters
                                | avecContact =
                                    filters.avecContact
                                        |> List.filter
                                            (\t -> t /= s)
                              }
                            , pagination
                            )
                    )
                    (\s ->
                        case s of
                            AvecContactOui ->
                                "Oui"

                            AvecContactNon ->
                                "Non"
                    )
                ++ toFilterDeletableTags "Anciens créateurs"
                    ancienCreateur
                    (\s ->
                        UpdateUrl <|
                            ( { filters
                                | ancienCreateur =
                                    filters.ancienCreateur
                                        |> List.filter
                                            (\t -> t /= s)
                              }
                            , pagination
                            )
                    )
                    (\s ->
                        case s of
                            AncienCreateurOui ->
                                "Oui"

                            AncienCreateurNon ->
                                "Non"
                    )
                ++ toFilterDeletableTags "Variation de CA"
                    caVarTypes
                    (\s ->
                        UpdateUrl <|
                            ( let
                                newCaVarTypes =
                                    filters.caVarTypes
                                        |> List.filter
                                            (\t -> t /= s)
                              in
                              { filters
                                | caVarTypes = newCaVarTypes
                                , caVarMin =
                                    if List.length newCaVarTypes == 0 then
                                        Nothing

                                    else
                                        filters.caVarMin
                                , caVarMax =
                                    if List.length newCaVarTypes == 0 then
                                        Nothing

                                    else
                                        filters.caVarMax
                              }
                            , pagination
                            )
                    )
                    variationTypeToDisplay
                ++ toFilterDeletableTags "Variation de CA minimum"
                    (case caVarMin of
                        Just e ->
                            [ e |> String.replace "." "," ]

                        Nothing ->
                            []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | caVarMin = Nothing
                              }
                            , pagination
                            )
                    )
                    (\e -> e ++ " %")
                ++ toFilterDeletableTags "Variation de CA maximum"
                    (case caVarMax of
                        Just e ->
                            [ e |> String.replace "." "," ]

                        Nothing ->
                            []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | caVarMax = Nothing
                              }
                            , pagination
                            )
                    )
                    (\e -> e ++ " %")
                ++ toFilterDeletableTags "Variation d'effectif"
                    effectifVarTypes
                    (\s ->
                        UpdateUrl <|
                            ( let
                                newEffectifVarTypes =
                                    filters.effectifVarTypes
                                        |> List.filter
                                            (\t -> t /= s)
                              in
                              { filters
                                | effectifVarTypes = newEffectifVarTypes
                                , effectifVarMin =
                                    if List.length newEffectifVarTypes == 0 then
                                        Nothing

                                    else
                                        filters.effectifVarMin
                                , effectifVarMax =
                                    if List.length newEffectifVarTypes == 0 then
                                        Nothing

                                    else
                                        filters.effectifVarMax
                              }
                            , pagination
                            )
                    )
                    variationTypeToDisplay
                ++ toFilterDeletableTags "Variation d'effectif minimum"
                    (case effectifVarMin of
                        Just e ->
                            [ e |> String.replace "." "," ]

                        Nothing ->
                            []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | effectifVarMin = Nothing
                              }
                            , pagination
                            )
                    )
                    (\e -> e ++ " %")
                ++ toFilterDeletableTags "Variation d'effectif maximum"
                    (case effectifVarMax of
                        Just e ->
                            [ e |> String.replace "." "," ]

                        Nothing ->
                            []
                    )
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | effectifVarMax = Nothing
                              }
                            , pagination
                            )
                    )
                    (\e -> e ++ " %")
                ++ toFilterDeletableTags
                    "Filiale"
                    filiales
                    (\filiale ->
                        UpdateUrl <|
                            ( { filters
                                | filiales =
                                    filters.filiales
                                        |> List.filter
                                            (\f -> f /= filiale)
                              }
                            , pagination
                            )
                    )
                    (\{ id, nom } -> id ++ " - " ++ nom)
                ++ toFilterDeletableTags
                    "Détention"
                    detentions
                    (\detention ->
                        UpdateUrl <|
                            ( { filters
                                | detentions =
                                    filters.detentions
                                        |> List.filter
                                            (\f -> f /= detention)
                              }
                            , pagination
                            )
                    )
                    (\{ id, nom } -> id ++ " - " ++ nom)
                ++ toFilterDeletableTags
                    "Note Egapro (min)"
                    ([ egaproIndiceMin ] |> List.filter ((/=) "0"))
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | egaproIndiceMin = "0"
                              }
                            , pagination
                            )
                    )
                    identity
                ++ toFilterDeletableTags
                    "Note Egapro (max)"
                    ([ egaproIndiceMax ] |> List.filter ((/=) "100"))
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | egaproIndiceMax = "100"
                              }
                            , pagination
                            )
                    )
                    identity
                ++ toFilterDeletableTags
                    "Cadres dirigeantes (min)"
                    ([ egaproCadresMin ] |> List.filter ((/=) "0"))
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | egaproCadresMin = "0"
                              }
                            , pagination
                            )
                    )
                    (\s -> String.replace "." "," s ++ " %")
                ++ toFilterDeletableTags
                    "Cadres dirigeantes (max)"
                    ([ egaproCadresMax ] |> List.filter ((/=) "100"))
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | egaproCadresMax = "100"
                              }
                            , pagination
                            )
                    )
                    (\s -> String.replace "." "," s ++ " %")
                ++ toFilterDeletableTags
                    "Membres instances (min)"
                    ([ egaproInstancesMin ] |> List.filter ((/=) "0"))
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | egaproInstancesMin = "0"
                              }
                            , pagination
                            )
                    )
                    (\s -> String.replace "." "," s ++ " %")
                ++ toFilterDeletableTags
                    "Membres instances (max)"
                    ([ egaproInstancesMax ] |> List.filter ((/=) "100"))
                    (\_ ->
                        UpdateUrl <|
                            ( { filters
                                | egaproInstancesMax = "100"
                              }
                            , pagination
                            )
                    )
                    (\s -> String.replace "." "," s ++ " %")
        ]


toFilterDeletableTags : String -> List data -> (data -> msg) -> (data -> String) -> List (Html msg)
toFilterDeletableTags prefix list toMsg toString =
    list
        |> List.sortBy toString
        |> List.map
            (\data ->
                DSFR.Tag.deletable toMsg { data = data, toString = \d -> prefix ++ "\u{00A0}: " ++ toString d }
            )
        |> List.map DSFR.Tag.oneMedium


toFilterDeletableTagsWithTitle : String -> List data -> (data -> msg) -> ( data -> String, data -> String ) -> List (Html msg)
toFilterDeletableTagsWithTitle prefix list toMsg ( toLabel, toTitle ) =
    list
        |> List.sortBy toLabel
        |> List.map
            (\data ->
                DSFR.Tag.deletable toMsg
                    { data = data
                    , toString =
                        \d ->
                            prefix ++ "\u{00A0}: " ++ toLabel d
                    }
                    |> DSFR.Tag.withAttrs
                        [ Attr.title <| toTitle <| data
                        ]
            )
        |> List.map DSFR.Tag.oneMedium


viewSaveRechercheEnregistree : String -> Model -> Html Msg
viewSaveRechercheEnregistree appUrl model =
    let
        ( opened, content, title ) =
            case model.saveRechercheEnregistree of
                Nothing ->
                    ( False, nothing, text <| "Enregistrer une recherche" )

                Just { nom, url, saveRequest } ->
                    let
                        lienRecherche =
                            (++) appUrl <|
                                Route.toUrl <|
                                    Route.Etablissements <|
                                        (\q ->
                                            if q == "" then
                                                Nothing

                                            else
                                                Just q
                                        )
                                        <|
                                            url
                    in
                    ( True
                    , div [ class "flex flex-col gap-4" ]
                        [ DSFR.Input.new
                            { value = nom
                            , onInput = UpdateSaveRechercheEnregistree
                            , label =
                                text "Nom de la recherche"
                            , name = "recherche-enregistree-nom"
                            }
                            |> DSFR.Input.view
                        , p [ class "fr-text-default--info !mb-0" ]
                            [ DSFR.Icons.System.infoFill |> DSFR.Icons.iconSM
                            , text " "
                            , text "La recherche enregistrée sera uniquement visible par vous. Si vous souhaitez la partager, vous pouvez envoyer le lien suivant\u{00A0}: "
                            ]
                        , p [ class "fr-text-default--info flex flex-col gap-4" ]
                            [ span [ Attr.id "recherche-lien" ] [ text <| lienRecherche ]
                            , UI.Clipboard.clipboard
                                { content = lienRecherche
                                , label = "Copier vers le presse-papier"
                                , copiedLabel = "Copié\u{00A0}!"
                                , timeoutMilliseconds = 2000
                                }
                            ]
                        , div [] <|
                            List.singleton <|
                                case saveRequest of
                                    RD.NotAsked ->
                                        nothing

                                    RD.Loading ->
                                        nothing

                                    RD.Failure _ ->
                                        text "Une erreur s'est produite, veuillez réessayer."

                                    RD.Success _ ->
                                        text "Recherche enregistrée avec succès\u{00A0}!"
                        , div [ Grid.gridRow, Grid.gridRowGutters ]
                            [ div [ Grid.col12 ]
                                [ (case saveRequest of
                                    RD.Success _ ->
                                        [ DSFR.Button.new
                                            { onClick = Just <| CancelSaveRechercheEnregistree
                                            , label = "OK"
                                            }
                                        ]

                                    _ ->
                                        [ DSFR.Button.new
                                            { onClick = Just <| ConfirmSaveRechercheEnregistree
                                            , label =
                                                if saveRequest == RD.Loading then
                                                    "Ajout en cours..."

                                                else
                                                    "Ajouter"
                                            }
                                            |> DSFR.Button.withDisabled (saveRequest == RD.Loading || nom == "")
                                        , DSFR.Button.new { onClick = Just <| CancelSaveRechercheEnregistree, label = "Annuler" }
                                            |> DSFR.Button.withDisabled (saveRequest == RD.Loading)
                                            |> DSFR.Button.secondary
                                        ]
                                  )
                                    |> DSFR.Button.group
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]
                            ]
                        ]
                    , text <| "Enregistrer une recherche"
                    )
    in
    DSFR.Modal.view
        { id = "recherche-enregistree-enregistrer"
        , label = "recherche-enregistree-enregistrer"
        , openMsg = NoOp
        , closeMsg = Just <| CancelSaveRechercheEnregistree
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


viewDeleteRechercheEnregistree : Model -> Html Msg
viewDeleteRechercheEnregistree model =
    let
        ( opened, content, title ) =
            case model.deleteRechercheEnregistree of
                Nothing ->
                    ( False, nothing, text <| "Supprimer une recherche" )

                Just { rechercheEnregistree, deleteRequest } ->
                    ( True
                    , div [ class "flex flex-col gap-4" ]
                        [ div [] [ text <| "Vous allez supprimer la recherche «\u{00A0}" ++ rechercheEnregistree.nom ++ "\u{00A0}». Êtes-vous sûr(e)\u{00A0}?" ]
                        , div [ Grid.gridRow, Grid.gridRowGutters ]
                            [ div [ Grid.col12 ]
                                [ (case deleteRequest of
                                    RD.Success _ ->
                                        [ DSFR.Button.new
                                            { onClick = Just <| CancelDeleteFilter
                                            , label = "OK"
                                            }
                                        ]

                                    _ ->
                                        [ DSFR.Button.new
                                            { onClick = Just <| ConfirmDeleteFilter
                                            , label =
                                                if deleteRequest == RD.Loading then
                                                    "Suppression en cours..."

                                                else
                                                    "Confirmer"
                                            }
                                            |> DSFR.Button.withDisabled (deleteRequest == RD.Loading)
                                        , DSFR.Button.new { onClick = Just <| CancelDeleteFilter, label = "Annuler" }
                                            |> DSFR.Button.withDisabled (deleteRequest == RD.Loading)
                                            |> DSFR.Button.secondary
                                        ]
                                  )
                                    |> DSFR.Button.group
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]
                            ]
                        ]
                    , text <| "Supprimer une recherche"
                    )
    in
    DSFR.Modal.view
        { id = "recherche-enregistree-supprimer"
        , label = "recherche-enregistree-supprimer"
        , openMsg = NoOp
        , closeMsg = Just <| CancelDeleteFilter
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


filterPanel : User.User -> Model -> Html Msg
filterPanel user model =
    let
        isFrance =
            Data.Role.isFrance <|
                User.role <|
                    user

        nextUrl =
            filtersAndPaginationToQuery ( model.filters, model.pagination )

        filtresPasEncoreValides =
            nextUrl /= model.currentUrl

        tooltipPourFiltresPasEncoreValides =
            if filtresPasEncoreValides then
                DSFR.Tooltip.survol { label = text "Vous avez changé des filtres sans lancer votre recherche", id = "tooltip-filtres-pas-valides-filters-panel" }
                    |> DSFR.Tooltip.wrap

            else
                identity
    in
    div [ class "fr-card--white p-4 flex flex-col gap-4" ]
        [ Html.form [ class "flex flex-col gap-4", Events.onSubmit ClickedSearch ]
            [ div [ class "flex flex-col lg:flex-row gap-4" ]
                [ div [ class "lg:basis-1/2" ]
                    [ div [ class "mb-4" ]
                        [ DSFR.Input.new
                            { value = model.filters.recherche
                            , onInput = UpdatedSearch
                            , label =
                                span
                                    [ Typo.textBold
                                    , Typo.textLg
                                    ]
                                    [ text "Recherche par nom, SIRET, SIREN" ]
                            , name = rechercherEtablissementInputName
                            }
                            |> DSFR.Input.view
                        ]
                    ]
                , div [ class "lg:basis-1/2" ]
                    [ if isFrance then
                        nothing

                      else
                        Html.Lazy.lazy situationGeographiqueFilter model.filters.situationGeographique
                    ]
                ]
            , div []
                [ publicFiltersPanel model
                , persoFiltersPanel model
                , recherchesEnregistreesPanel model
                ]
            , DSFR.Button.new { label = "Rechercher", onClick = Nothing }
                |> DSFR.Button.withDisabled (model.currentUrl == filtersAndPaginationToQuery ( model.filters, model.pagination ))
                |> DSFR.Button.withAttrs [ class "!w-full justify-center" ]
                |> DSFR.Button.submit
                |> DSFR.Button.large
                |> DSFR.Button.view
                |> List.singleton
                |> div [ class "flex flex-row w-full mt-4" ]
            ]
        , if hasFilterTags model.currentUrl then
            div [ Attr.id listFiltersTagsId ]
                [ div [ class "flex flex-row !mt-2" ]
                    [ showFilterTags model
                    , div [ class "ml-auto" ]
                        [ [ saveFiltersButton filtresPasEncoreValides (RD.withDefault [] model.recherchesEnregistrees) model.currentUrl
                          , clearAllFiltersButton model.currentUrl
                          ]
                            |> DSFR.Button.group
                            |> DSFR.Button.viewGroup
                            |> tooltipPourFiltresPasEncoreValides
                        ]
                    ]
                ]

          else
            text ""
        ]


publicFiltersPanel : Model -> Html Msg
publicFiltersPanel { selectCommunes, selectEpcis, selectDepartements, selectRegions, selectCategoriesJuridiques, selectCodesNaf, selectExercices, selectAdresse, selectQpvs, selectTis, selectZrrs, selectAfrs, selectFiliales, selectDetentions, filters } =
    let
        selectedPublicFilters =
            List.length filters.etat
                + List.length filters.geolocalisation
                + List.length filters.micro
                + List.length filters.entrepriseTypes
                + List.length filters.categoriesJuridiques
                + List.length filters.codesNaf
                + (if filters.formesJuridiques == Toutes then
                    0

                   else
                    1
                  )
                + List.length filters.communes
                + List.length filters.epcis
                + List.length filters.departements
                + List.length filters.regions
                + (filters.effectifInconnu
                    |> (\b ->
                            if b then
                                1

                            else
                                0
                       )
                  )
                + (filters.effectifMin |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.effectifMax |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + List.length filters.exercices
                + (filters.apiStreet |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + List.length filters.qpvs
                + (filters.tousQpvs
                    |> (\b ->
                            if b then
                                1

                            else
                                0
                       )
                  )
                + List.length filters.tis
                + (filters.tousTis
                    |> (\b ->
                            if b then
                                1

                            else
                                0
                       )
                  )
                + List.length filters.zrrs
                + List.length filters.ess
                + List.length filters.acv
                + List.length filters.pvd
                + List.length filters.va
                + List.length filters.afrs
                + List.length filters.siege
                + List.length filters.procedures
                + List.length filters.subventions
                + List.length filters.filiales
                + List.length filters.detentions
                + (filters.creeApres |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.creeAvant |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.fermeApres |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.fermeAvant |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                + (filters.egaproIndiceMin
                    |> (\i ->
                            if i == "0" then
                                0

                            else
                                1
                       )
                  )
                + (filters.egaproIndiceMax
                    |> (\i ->
                            if i == "100" then
                                0

                            else
                                1
                       )
                  )
                + (filters.egaproCadresMin
                    |> (\i ->
                            if i == "0" then
                                0

                            else
                                1
                       )
                  )
                + (filters.egaproCadresMax
                    |> (\i ->
                            if i == "100" then
                                0

                            else
                                1
                       )
                  )
                + (filters.egaproInstancesMin
                    |> (\i ->
                            if i == "0" then
                                0

                            else
                                1
                       )
                  )
                + (filters.egaproInstancesMax
                    |> (\i ->
                            if i == "100" then
                                0

                            else
                                1
                       )
                  )
    in
    DSFR.Accordion.raw
        { id = "filtres-donnees-publiques"
        , title =
            [ h2 [ Typo.fr_h6, class "flex flex-row gap-4" ]
                [ span [] [ text "Filtres données publiques" ]
                , if selectedPublicFilters > 0 then
                    span [ class "circled" ]
                        [ text <| formatIntWithThousandSpacing <| selectedPublicFilters
                        ]

                  else
                    nothing
                ]
            ]
        , borderless = False
        , content =
            [ div [ class "flex flex-col lg:flex-row gap-9" ]
                [ div [ class "flex flex-col w-full gap-9" ]
                    [ Html.Lazy.lazy etatFilter filters.etat
                    , Html.Lazy.lazy siegeFilter filters.siege
                    , Html.Lazy.lazy2 creeFilter filters.creeApres filters.creeAvant
                    , Html.Lazy.lazy3 fermeFilter filters.creeApres filters.fermeApres filters.fermeAvant
                    , Html.Lazy.lazy3 effectifsFilter filters.effectifInconnu filters.effectifMin filters.effectifMax
                    , Html.Lazy.lazy2 exercicesFilter filters.exercices selectExercices
                    , Html.Lazy.lazy3 effectifVariationFilter filters.effectifVarTypes filters.effectifVarMin filters.effectifVarMax
                    , Html.Lazy.lazy3 caVariationFilter filters.caVarTypes filters.caVarMin filters.caVarMax
                    ]
                , div [ class "flex flex-col w-full gap-9" ]
                    [ Html.Lazy.lazy4 categoriesJuridiquesFilter selectCategoriesJuridiques filters.formesJuridiques filters.categoriesJuridiquesSans filters.categoriesJuridiques
                    , Html.Lazy.lazy3 codesNafFilter selectCodesNaf filters.codesNafSans filters.codesNaf
                    , Html.Lazy.lazy entrepriseTypeFilter filters.entrepriseTypes
                    , Html.Lazy.lazy microFilter filters.micro
                    , Html.Lazy.lazy essFilter filters.ess
                    , Html.Lazy.lazy5 subventionsFilter filters.subventions filters.subventionsAnneeMin filters.subventionsAnneeMax filters.subventionsMontantMin filters.subventionsMontantMax
                    , Html.Lazy.lazy3 procedureFilter filters.procedures filters.proceduresApres filters.proceduresAvant
                    , Html.Lazy.lazy2 filialesFilter selectFiliales filters.filiales
                    , Html.Lazy.lazy2 detentionsFilter selectDetentions filters.detentions
                    , Html.Lazy.lazy2 egaproIndiceFilter filters.egaproIndiceMin filters.egaproIndiceMax
                    , Html.Lazy.lazy2 egaproCadresFilter filters.egaproCadresMin filters.egaproCadresMax
                    , Html.Lazy.lazy2 egaproInstancesFilter filters.egaproInstancesMin filters.egaproInstancesMax
                    ]
                , div [ class "flex flex-col w-full gap-9" ]
                    [ Html.Lazy.lazy2 adresseFilter selectAdresse filters.apiStreet
                    , Html.Lazy.lazy3 qpvsFilter selectQpvs filters.tousQpvs filters.qpvs
                    , Html.Lazy.lazy3 tisFilter selectTis filters.tousTis filters.tis
                    , Html.Lazy.lazy2 zrrsFilter filters.zrrs selectZrrs
                    , Html.Lazy.lazy acvFilter filters.acv
                    , Html.Lazy.lazy pvdFilter filters.pvd
                    , Html.Lazy.lazy vaFilter filters.va
                    , Html.Lazy.lazy2 afrsFilter filters.afrs selectAfrs
                    , Html.Lazy.lazy2 communesFilter selectCommunes filters.communes
                    , Html.Lazy.lazy2 epcisFilter selectEpcis filters.epcis
                    , Html.Lazy.lazy2 departementsFilter selectDepartements filters.departements
                    , Html.Lazy.lazy2 regionsFilter selectRegions filters.regions
                    , Html.Lazy.lazy geolocalisationFilter filters.geolocalisation
                    ]
                ]
            ]
        }


countSelectedPersoFilters : Filters -> Int
countSelectedPersoFilters filters =
    List.length filters.activites
        + List.length filters.demandes
        + List.length filters.zones
        + List.length filters.mots
        + List.length filters.accompagnes
        + (filters.accompagnesApres |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
        + (filters.accompagnesAvant |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
        + List.length filters.favori
        + List.length filters.avecContact
        + List.length filters.ancienCreateur


countActivePersoFilters : Filters -> Int
countActivePersoFilters filters =
    List.length filters.activites
        + List.length filters.demandes
        + List.length filters.zones
        + List.length filters.mots
        + List.length (List.filter ((==) AccompagnesOui) filters.accompagnes)
        + (filters.accompagnesApres |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
        + (filters.accompagnesAvant |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
        + List.length (List.filter ((==) FavoriOui) filters.favori)
        + List.length (List.filter ((==) AvecContactOui) filters.avecContact)
        + List.length (List.filter ((==) AncienCreateurOui) filters.ancienCreateur)


persoFiltersPanel : Model -> Html Msg
persoFiltersPanel { selectActivites, selectZones, selectMots, selectDemandes, filters } =
    let
        selectedPersoFilters =
            countSelectedPersoFilters filters
    in
    DSFR.Accordion.raw
        { id = "filtres-donnees-personnalisees"
        , title =
            [ h2 [ Typo.fr_h6, class "flex flex-row gap-4" ]
                [ span [] [ text "Filtres données personnalisées" ]
                , if selectedPersoFilters > 0 then
                    span [ class "circled" ]
                        [ text <| formatIntWithThousandSpacing <| selectedPersoFilters
                        ]

                  else
                    nothing
                ]
            ]
        , borderless = False
        , content =
            [ div [ class "flex flex-col lg:flex-row w-full gap-9" ]
                [ div [ class "flex flex-col w-full gap-9" ]
                    [ Html.Lazy.lazy3 accompagnesFilter filters.accompagnes filters.accompagnesApres filters.accompagnesAvant
                    ]
                , div [ class "flex flex-col w-full gap-9" ]
                    [ Html.Lazy.lazy favoriFilter filters.favori
                    , Html.Lazy.lazy avecContactFilter filters.avecContact
                    , Html.Lazy.lazy ancienCreateurFilter filters.ancienCreateur
                    ]
                , div [ class "flex flex-col w-full gap-9" ]
                    [ Html.Lazy.lazy2 demandesFilter selectDemandes filters.demandes
                    , Html.Lazy.lazy3 activitesFilter selectActivites filters.activitesToutes filters.activites
                    , Html.Lazy.lazy3 zonesFilter selectZones filters.zonesToutes filters.zones
                    , Html.Lazy.lazy3 motsFilter selectMots filters.motsTous filters.mots
                    ]
                ]
            ]
        }


recherchesEnregistreesPanel : Model -> Html Msg
recherchesEnregistreesPanel { recherchesEnregistrees } =
    DSFR.Accordion.raw
        { id = "filtres-enregistres"
        , title =
            [ h2 [ Typo.fr_h6, class "flex flex-row gap-4" ]
                [ span []
                    [ text "Recherche"
                    , text <| Lib.UI.plural <| List.length <| RD.withDefault [] <| recherchesEnregistrees
                    , text " "
                    , text "enregistrée"
                    , text <| Lib.UI.plural <| List.length <| RD.withDefault [] <| recherchesEnregistrees
                    ]
                ]
            ]
        , borderless = False
        , content =
            [ div [ class "flex flex-col" ] <|
                case recherchesEnregistrees of
                    RD.NotAsked ->
                        [ nothing ]

                    RD.Loading ->
                        [ text "Chargement en cours, veuillez patienter."
                        ]

                    RD.Failure _ ->
                        [ text "Une erreur s'est produite, veuillez réessayer."
                        ]

                    RD.Success [] ->
                        [ text "Aucune recherche enregistrée pour l'instant."
                        ]

                    RD.Success list ->
                        list
                            |> List.map viewRechercheEnregistree
            ]
        }


viewRechercheEnregistree : RechercheEnregistree -> Html Msg
viewRechercheEnregistree rechercheEnregistree =
    div [ class "flex flex-row justify-between items-center" ]
        [ div [ class "flex flex-col" ]
            [ span [ Typo.textBold, Typo.textLg, class "!mb-0" ]
                [ text <| rechercheEnregistree.nom ]
            , span [ Typo.textSm, class "!mb-0" ]
                [ text "Enregistré le"
                , text " "
                , text <| Lib.Date.formatDateShort <| rechercheEnregistree.date
                ]
            ]
        , div [ class "flex flex-row" ]
            [ [ DSFR.Button.new
                    { label = "Lancer la recherche"
                    , onClick = Just <| SearchRechercheEnregistree rechercheEnregistree
                    }
                    |> DSFR.Button.leftIcon DSFR.Icons.System.searchLine
              , DSFR.Button.new
                    { label = "Supprimer"
                    , onClick = Just <| DeleteRechercheEnregistree rechercheEnregistree
                    }
                    |> DSFR.Button.secondary
                    |> DSFR.Button.leftIcon DSFR.Icons.System.deleteLine
              ]
                |> DSFR.Button.group
                |> DSFR.Button.iconsLeft
                |> DSFR.Button.inline
                |> DSFR.Button.viewGroup
            ]
        ]


saveFiltersButton : Bool -> List RechercheEnregistree -> String -> DSFR.Button.ButtonConfig Msg
saveFiltersButton filtresPasEncoreValides recherchesEnregistrees currentUrl =
    let
        rechercheEnregistreeExistante =
            recherchesEnregistrees
                |> List.map .url
                |> List.member currentUrl
    in
    DSFR.Button.new
        { label = "Enregistrer"
        , onClick = Just <| SaveRechercheEnregistree
        }
        |> DSFR.Button.withDisabled (queryToFilters currentUrl == emptyFilters || rechercheEnregistreeExistante || filtresPasEncoreValides)
        |> DSFR.Button.secondary


clearAllFiltersButton : String -> DSFR.Button.ButtonConfig Msg
clearAllFiltersButton currentUrl =
    DSFR.Button.new
        { label = "Tout effacer"
        , onClick = Just <| ClearAllFilters
        }
        |> DSFR.Button.withDisabled (queryToFilters currentUrl == emptyFilters)
        |> DSFR.Button.tertiaryNoOutline


queryToFilters : String -> Filters
queryToFilters rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    recherche =
                        query
                            |> QS.getAsStringList queryKeys.recherche
                            |> List.head
                            |> Maybe.withDefault ""

                    etat =
                        query
                            |> QS.getAsStringList queryKeys.etat
                            |> List.filterMap stringToEtatAdministratifFiltre

                    geolocalisation =
                        query
                            |> QS.getAsStringList queryKeys.geolocalisation
                            |> List.filterMap stringToGeolocalisationFiltre

                    longitude =
                        query
                            |> QS.getAsStringList queryKeys.longitude
                            |> List.head
                            |> Maybe.map String.toFloat
                            |> Maybe.withDefault Nothing

                    latitude =
                        query
                            |> QS.getAsStringList queryKeys.latitude
                            |> List.head
                            |> Maybe.map String.toFloat
                            |> Maybe.withDefault Nothing

                    -- nwse =
                    --     query
                    --         |> QS.getAsStringList queryKeys.nwse
                    --         |> List.head
                    --         |> Maybe.andThen
                    --             (String.split ","
                    --                 >> List.map String.toFloat
                    --                 >> (\l ->
                    --                         case l of
                    --                             [ Just n, Just w, Just s, Just e ] ->
                    --                                 Just ( ( w, n ), ( e, s ) )
                    --                             _ ->
                    --                                 Nothing
                    --                    )
                    --             )
                    zoom =
                        query
                            |> QS.getAsStringList queryKeys.zoom
                            |> List.head
                            |> Maybe.andThen String.toFloat

                    categorieJuridiqueCourante =
                        query
                            |> QS.getAsStringList queryKeys.categorieJuridiqueCourante
                            |> List.head
                            |> Maybe.map
                                (\formes ->
                                    if formes == "true" then
                                        Courantes

                                    else
                                        Toutes
                                )
                            |> Maybe.withDefault Toutes

                    categoriesJuridiquesRaw =
                        query
                            |> QS.getAsStringList queryKeys.categoriesJuridiques
                            |> List.map (\id -> { id = id, nom = "", niveau = 0, parentId = "" })

                    categoriesJuridiquesSansRaw =
                        query
                            |> QS.getAsStringList queryKeys.categoriesJuridiquesSans
                            |> List.map (\id -> { id = id, nom = "", niveau = 0, parentId = "" })

                    categoriesJuridiquesSans =
                        if categorieJuridiqueCourante == Courantes then
                            False

                        else
                            (List.length categoriesJuridiquesRaw == 0) && (List.length categoriesJuridiquesSansRaw > 0)

                    categoriesJuridiques =
                        if categorieJuridiqueCourante == Courantes then
                            []

                        else if categoriesJuridiquesSans then
                            categoriesJuridiquesSansRaw

                        else
                            categoriesJuridiquesRaw

                    codesNafRaw =
                        query
                            |> QS.getAsStringList queryKeys.codesNaf
                            |> List.map (\id -> { id = id, nom = "" })

                    codesNafSansRaw =
                        query
                            |> QS.getAsStringList queryKeys.codesNafSans
                            |> List.map (\id -> { id = id, nom = "" })

                    codesNafSans =
                        (List.length codesNafRaw == 0) && (List.length codesNafSansRaw > 0)

                    codesNaf =
                        if codesNafSans then
                            codesNafSansRaw

                        else
                            codesNafRaw

                    effectifsInconnus =
                        query
                            |> QS.has queryKeys.effectifInconnu

                    effectifsMinimum =
                        query
                            |> QS.getAsStringList queryKeys.effectifMin
                            |> List.head

                    effectifsMaximum =
                        query
                            |> QS.getAsStringList queryKeys.effectifMax
                            |> List.head

                    exercices =
                        query
                            |> QS.getAsStringList queryKeys.exercices
                            |> List.map (\id -> { id = id, label = "" })

                    zrrs =
                        query
                            |> QS.getAsStringList queryKeys.zrrs
                            |> List.map (\id -> { id = id, label = "" })

                    communes =
                        query
                            |> QS.getAsStringList queryKeys.communes
                            |> List.map (\id -> { id = id, nom = "", departementId = "" })

                    epcis =
                        query
                            |> QS.getAsStringList queryKeys.epcis
                            |> List.map (\id -> { id = id, nom = "" })

                    departements =
                        query
                            |> QS.getAsStringList queryKeys.departements
                            |> List.map (\id -> { id = id, nom = "" })

                    regions =
                        query
                            |> QS.getAsStringList queryKeys.regions
                            |> List.map (\id -> { id = id, nom = "" })

                    numero =
                        query
                            |> QS.getAsStringList queryKeys.numero
                            |> List.head

                    adresse =
                        query
                            |> QS.getAsStringList queryKeys.adresse
                            |> List.head

                    cp =
                        query
                            |> QS.getAsStringList queryKeys.cp
                            |> List.head

                    ville =
                        query
                            |> QS.getAsStringList queryKeys.ville
                            |> List.head

                    situationGeographique =
                        query
                            |> QS.getAsStringList queryKeys.situationGeographique
                            |> List.head
                            |> Maybe.map
                                (\s ->
                                    if s == "hors" then
                                        HorsDeMonTerritoire

                                    else if s == "france" then
                                        TouteLaFrance

                                    else
                                        SurMonTerritoire
                                )
                            |> Maybe.withDefault SurMonTerritoire

                    entrepriseTypes =
                        query
                            |> QS.getAsStringList queryKeys.entrepriseTypes
                            |> List.filterMap stringToEntrepriseType

                    activites =
                        query
                            |> QS.getAsStringList queryKeys.activites

                    activitesToutes =
                        query
                            |> QS.getAsStringList queryKeys.activitesToutes
                            |> List.head
                            |> Maybe.map ((==) "oui")
                            |> Maybe.withDefault False

                    demandes =
                        query
                            |> QS.getAsStringList queryKeys.demandes
                            |> List.filterMap Data.Demande.stringToTypeDemande

                    zones =
                        query
                            |> QS.getAsStringList queryKeys.localisations

                    zonesToutes =
                        query
                            |> QS.getAsStringList queryKeys.localisationsToutes
                            |> List.head
                            |> Maybe.map ((==) "oui")
                            |> Maybe.withDefault False

                    mots =
                        query
                            |> QS.getAsStringList queryKeys.motsCles

                    motsTous =
                        query
                            |> QS.getAsStringList queryKeys.motsClesTous
                            |> List.head
                            |> Maybe.map ((==) "oui")
                            |> Maybe.withDefault False

                    orderBy =
                        query
                            |> QS.getAsStringList queryKeys.tri
                            |> List.head
                            |> Maybe.andThen stringToOrderBy
                            |> Maybe.andThen
                                (\tri ->
                                    case ( tri, etat ) of
                                        ( EtablissementFermeture, [ Ferme ] ) ->
                                            Just tri

                                        ( EtablissementFermeture, _ ) ->
                                            Nothing

                                        _ ->
                                            Just tri
                                )
                            |> Maybe.withDefault EtablissementConsultation

                    orderDirection =
                        query
                            |> QS.getAsStringList queryKeys.direction
                            |> List.head
                            |> Maybe.andThen stringToOrderDirection
                            |> Maybe.withDefault Desc

                    ( tousQpvs, qpvs ) =
                        query
                            |> QS.getAsStringList queryKeys.qpvs
                            |> (\qpvIds ->
                                    if List.member "tous" qpvIds then
                                        ( True, [] )

                                    else
                                        ( False, qpvIds |> List.map (\id -> { id = id, nom = "" }) )
                               )

                    ( tousTis, tis ) =
                        query
                            |> QS.getAsStringList queryKeys.tis
                            |> (\tiIds ->
                                    if List.member "tous" tiIds then
                                        ( True, [] )

                                    else
                                        ( False, tiIds |> List.map (\id -> { id = id, nom = "" }) )
                               )

                    ess =
                        query
                            |> QS.getAsStringList queryKeys.ess
                            |> List.head
                            |> (\e ->
                                    case e of
                                        Just "true" ->
                                            [ EssOui ]

                                        Just "false" ->
                                            [ EssNon ]

                                        Just _ ->
                                            []

                                        Nothing ->
                                            []
                               )

                    acv =
                        query
                            |> QS.getAsStringList queryKeys.acv
                            |> List.head
                            |> (\e ->
                                    case e of
                                        Just "true" ->
                                            [ AcvOui ]

                                        Just "false" ->
                                            [ AcvNon ]

                                        Just _ ->
                                            []

                                        Nothing ->
                                            []
                               )

                    pvd =
                        query
                            |> QS.getAsStringList queryKeys.pvd
                            |> List.head
                            |> (\e ->
                                    case e of
                                        Just "true" ->
                                            [ PvdOui ]

                                        Just "false" ->
                                            [ PvdNon ]

                                        Just _ ->
                                            []

                                        Nothing ->
                                            []
                               )

                    va =
                        query
                            |> QS.getAsStringList queryKeys.va
                            |> List.head
                            |> (\e ->
                                    case e of
                                        Just "true" ->
                                            [ VaOui ]

                                        Just "false" ->
                                            [ VaNon ]

                                        Just _ ->
                                            []

                                        Nothing ->
                                            []
                               )

                    afrs =
                        query
                            |> QS.getAsStringList queryKeys.afrs
                            |> List.map (\id -> { id = id, label = "" })

                    siege =
                        query
                            |> QS.getAsStringList queryKeys.siege
                            |> List.head
                            |> (\e ->
                                    case e of
                                        Just "true" ->
                                            [ SiegeOui ]

                                        Just "false" ->
                                            [ SiegeNon ]

                                        Just _ ->
                                            []

                                        Nothing ->
                                            []
                               )

                    procedures =
                        query
                            |> QS.getAsStringList queryKeys.procedures
                            |> List.head
                            |> (\e ->
                                    case e of
                                        Just "true" ->
                                            [ ProcedureOui ]

                                        Just "false" ->
                                            [ ProcedureNon ]

                                        Just _ ->
                                            []

                                        Nothing ->
                                            []
                               )

                    proceduresApres =
                        if not <| List.member ProcedureOui procedures then
                            Nothing

                        else
                            query
                                |> QS.getAsStringList queryKeys.proceduresApres
                                |> List.head
                                |> Maybe.andThen Lib.Date.dateFromISOString

                    proceduresAvant =
                        if not <| List.member ProcedureOui procedures then
                            Nothing

                        else
                            query
                                |> QS.getAsStringList queryKeys.proceduresAvant
                                |> List.head
                                |> Maybe.andThen Lib.Date.dateFromISOString

                    subventions =
                        query
                            |> QS.getAsStringList queryKeys.subventions
                            |> List.filterMap stringToSubventions

                    subventionsAnneeMin =
                        if not <| List.member SubventionOui subventions then
                            Nothing

                        else
                            query
                                |> QS.getAsStringList queryKeys.subventionsAnneeMin
                                |> List.head

                    subventionsAnneeMax =
                        if not <| List.member SubventionOui subventions then
                            Nothing

                        else
                            query
                                |> QS.getAsStringList queryKeys.subventionsAnneeMax
                                |> List.head

                    subventionsMontantMin =
                        if not <| List.member SubventionOui subventions then
                            Nothing

                        else
                            query
                                |> QS.getAsStringList queryKeys.subventionsMontantMin
                                |> List.head

                    subventionsMontantMax =
                        if not <| List.member SubventionOui subventions then
                            Nothing

                        else
                            query
                                |> QS.getAsStringList queryKeys.subventionsMontantMax
                                |> List.head

                    creeApres =
                        query
                            |> QS.getAsStringList queryKeys.creeApres
                            |> List.head
                            |> Maybe.andThen Lib.Date.dateFromISOString

                    creeAvant =
                        query
                            |> QS.getAsStringList queryKeys.creeAvant
                            |> List.head
                            |> Maybe.andThen Lib.Date.dateFromISOString

                    fermeApres =
                        query
                            |> QS.getAsStringList queryKeys.fermeApres
                            |> List.head
                            |> Maybe.andThen Lib.Date.dateFromISOString

                    fermeAvant =
                        query
                            |> QS.getAsStringList queryKeys.fermeAvant
                            |> List.head
                            |> Maybe.andThen Lib.Date.dateFromISOString

                    accompagnes =
                        query
                            |> QS.getAsStringList queryKeys.accompagnes
                            |> List.filterMap stringToAccompagnes

                    accompagnesApres =
                        if not <| List.member AccompagnesOui accompagnes then
                            Nothing

                        else
                            query
                                |> QS.getAsStringList queryKeys.accompagnesApres
                                |> List.head
                                |> Maybe.andThen Lib.Date.dateFromISOString

                    accompagnesAvant =
                        if not <| List.member AccompagnesOui accompagnes then
                            Nothing

                        else
                            query
                                |> QS.getAsStringList queryKeys.accompagnesAvant
                                |> List.head
                                |> Maybe.andThen Lib.Date.dateFromISOString

                    micro =
                        query
                            |> QS.getAsStringList queryKeys.micro
                            |> List.head
                            |> (\e ->
                                    case e of
                                        Just "true" ->
                                            [ MicroOui ]

                                        Just "false" ->
                                            [ MicroNon ]

                                        Just _ ->
                                            []

                                        Nothing ->
                                            []
                               )

                    favori =
                        query
                            |> QS.getAsStringList queryKeys.favori
                            |> List.head
                            |> (\e ->
                                    case e of
                                        Just "true" ->
                                            [ FavoriOui ]

                                        Just "false" ->
                                            [ FavoriNon ]

                                        Just _ ->
                                            []

                                        Nothing ->
                                            []
                               )

                    avecContact =
                        query
                            |> QS.getAsStringList queryKeys.avecContact
                            |> List.head
                            |> (\e ->
                                    case e of
                                        Just "true" ->
                                            [ AvecContactOui ]

                                        Just "false" ->
                                            [ AvecContactNon ]

                                        Just _ ->
                                            []

                                        Nothing ->
                                            []
                               )

                    ancienCreateur =
                        query
                            |> QS.getAsStringList queryKeys.ancienCreateur
                            |> List.head
                            |> (\e ->
                                    case e of
                                        Just "true" ->
                                            [ AncienCreateurOui ]

                                        Just "false" ->
                                            [ AncienCreateurNon ]

                                        Just _ ->
                                            []

                                        Nothing ->
                                            []
                               )

                    caVarTypes =
                        query
                            |> QS.getAsStringList queryKeys.caVarTypes
                            |> List.filterMap stringToVariationType

                    caVarMin =
                        query
                            |> QS.getAsStringList queryKeys.caVarMin
                            |> List.head

                    caVarMax =
                        query
                            |> QS.getAsStringList queryKeys.caVarMax
                            |> List.head

                    effectifVarTypes =
                        query
                            |> QS.getAsStringList queryKeys.effectifVarTypes
                            |> List.filterMap stringToVariationType

                    effectifVarMin =
                        query
                            |> QS.getAsStringList queryKeys.effectifVarMin
                            |> List.head

                    effectifVarMax =
                        query
                            |> QS.getAsStringList queryKeys.effectifVarMax
                            |> List.head

                    filiales =
                        query
                            |> QS.getAsStringList queryKeys.filiale
                            |> List.map (\id -> { id = id, nom = "" })

                    detentions =
                        query
                            |> QS.getAsStringList queryKeys.detention
                            |> List.map (\id -> { id = id, nom = "" })

                    egaproIndiceMin =
                        query
                            |> QS.getAsStringList queryKeys.egaproIndiceMin
                            |> List.head
                            |> Maybe.andThen String.toFloat
                            |> Maybe.map (min 100 >> max 0)
                            |> Maybe.map String.fromFloat
                            |> Maybe.withDefault "0"

                    egaproIndiceMax =
                        query
                            |> QS.getAsStringList queryKeys.egaproIndiceMax
                            |> List.head
                            |> Maybe.andThen String.toFloat
                            |> Maybe.map (min 100 >> max 0)
                            |> Maybe.map String.fromFloat
                            |> Maybe.withDefault "100"

                    egaproCadresMin =
                        query
                            |> QS.getAsStringList queryKeys.egaproCadresMin
                            |> List.head
                            |> Maybe.andThen String.toFloat
                            |> Maybe.map (min 100 >> max 0)
                            |> Maybe.map String.fromFloat
                            |> Maybe.withDefault "0"

                    egaproCadresMax =
                        query
                            |> QS.getAsStringList queryKeys.egaproCadresMax
                            |> List.head
                            |> Maybe.andThen String.toFloat
                            |> Maybe.map (min 100 >> max 0)
                            |> Maybe.map String.fromFloat
                            |> Maybe.withDefault "100"

                    egaproInstancesMin =
                        query
                            |> QS.getAsStringList queryKeys.egaproInstancesMin
                            |> List.head
                            |> Maybe.andThen String.toFloat
                            |> Maybe.map (min 100 >> max 0)
                            |> Maybe.map String.fromFloat
                            |> Maybe.withDefault "0"

                    egaproInstancesMax =
                        query
                            |> QS.getAsStringList queryKeys.egaproInstancesMax
                            |> List.head
                            |> Maybe.andThen String.toFloat
                            |> Maybe.map (min 100 >> max 0)
                            |> Maybe.map String.fromFloat
                            |> Maybe.withDefault "100"
                in
                { etat = etat
                , formesJuridiques = categorieJuridiqueCourante
                , communes = communes
                , categoriesJuridiques = categoriesJuridiques
                , categoriesJuridiquesSans = categoriesJuridiquesSans
                , codesNaf = codesNaf
                , codesNafSans = codesNafSans
                , effectifInconnu = effectifsInconnus
                , effectifMin = effectifsMinimum
                , effectifMax = effectifsMaximum
                , exercices = exercices
                , epcis = epcis
                , departements = departements
                , regions = regions
                , ess = ess
                , qpvs = qpvs
                , tousQpvs = tousQpvs
                , tis = tis
                , tousTis = tousTis
                , recherche = recherche
                , zrrs = zrrs
                , apiStreet =
                    Maybe.map
                        (\c ->
                            ApiStreet
                                ([ [ numero, adresse ]
                                    |> List.filterMap identity
                                    |> String.join " "
                                 , [ Just c
                                   , ville
                                   ]
                                    |> List.filterMap identity
                                    |> String.join " "
                                 ]
                                    |> String.join " "
                                )
                                (numero |> Maybe.withDefault "")
                                (adresse |> Maybe.withDefault "")
                                c
                                Nothing
                                (ville |> Maybe.withDefault "")
                        )
                        cp
                , demandes = demandes
                , activites = activites
                , activitesToutes = activitesToutes
                , zones = zones
                , zonesToutes = zonesToutes
                , mots = mots
                , motsTous = motsTous
                , situationGeographique = situationGeographique
                , entrepriseTypes = entrepriseTypes
                , acv = acv
                , pvd = pvd
                , va = va
                , afrs = afrs
                , siege = siege
                , procedures = procedures
                , proceduresApres = proceduresApres
                , proceduresAvant = proceduresAvant
                , subventions = subventions
                , subventionsAnneeMin = subventionsAnneeMin
                , subventionsAnneeMax = subventionsAnneeMax
                , subventionsMontantMin = subventionsMontantMin
                , subventionsMontantMax = subventionsMontantMax
                , creeApres = creeApres
                , creeAvant = creeAvant
                , fermeApres = fermeApres
                , fermeAvant = fermeAvant
                , orderBy = orderBy
                , orderDirection = orderDirection
                , accompagnes = accompagnes
                , accompagnesApres = accompagnesApres
                , accompagnesAvant = accompagnesAvant
                , micro = micro
                , favori = favori
                , avecContact = avecContact
                , ancienCreateur = ancienCreateur
                , geolocalisation = geolocalisation
                , longitude = longitude
                , latitude = latitude
                , zoom = zoom

                -- , nwse = nwse
                , caVarTypes = caVarTypes
                , caVarMin = caVarMin
                , caVarMax = caVarMax
                , effectifVarTypes = effectifVarTypes
                , effectifVarMin = effectifVarMin
                , effectifVarMax = effectifVarMax
                , filiales = filiales
                , detentions = detentions
                , egaproIndiceMin = egaproIndiceMin
                , egaproIndiceMax = egaproIndiceMax
                , egaproCadresMin = egaproCadresMin
                , egaproCadresMax = egaproCadresMax
                , egaproInstancesMin = egaproInstancesMin
                , egaproInstancesMax = egaproInstancesMax
                }
           )


filtersToQuery : Filters -> QS.Query
filtersToQuery filters =
    let
        setEtat =
            filters.etat
                |> List.map etatAdministratifFiltreToString
                |> QS.setListStr queryKeys.etat

        setGeolocalisation =
            filters.geolocalisation
                |> List.map geolocalisationFiltreToString
                |> QS.setListStr queryKeys.geolocalisation

        setLongitude =
            filters.longitude
                |> Maybe.map (\lon -> QS.setStr queryKeys.longitude (String.fromFloat lon))
                |> Maybe.withDefault identity

        setLatitude =
            filters.latitude
                |> Maybe.map (\lon -> QS.setStr queryKeys.latitude (String.fromFloat lon))
                |> Maybe.withDefault identity

        -- setNwse =
        --     filters.nwse
        --         |> Maybe.map
        --             (\( ( w, n ), ( e, s ) ) ->
        --                 [ n, w, s, e ]
        --                     |> List.map String.fromFloat
        --                     |> String.join ","
        --                     |> QS.setStr queryKeys.nwse
        --             )
        --         |> Maybe.withDefault identity
        setZoom =
            filters.zoom
                |> Maybe.map (String.fromFloat >> QS.setStr queryKeys.zoom)
                |> Maybe.withDefault identity

        setRecherche =
            case filters.recherche of
                "" ->
                    identity

                recherche ->
                    recherche
                        |> QS.setStr queryKeys.recherche

        setFormesJuridiques =
            case filters.formesJuridiques of
                Courantes ->
                    "true"
                        |> QS.setStr queryKeys.categorieJuridiqueCourante

                _ ->
                    identity

        setCategoriesJuridiques =
            filters.categoriesJuridiques
                |> List.map .id
                |> QS.setListStr
                    (if filters.categoriesJuridiquesSans then
                        queryKeys.categoriesJuridiquesSans

                     else
                        queryKeys.categoriesJuridiques
                    )

        setCodesNaf =
            filters.codesNaf
                |> List.map .id
                |> QS.setListStr
                    (if filters.codesNafSans then
                        queryKeys.codesNafSans

                     else
                        queryKeys.codesNaf
                    )

        setEffectifsInconnus =
            if filters.effectifInconnu then
                QS.setBool queryKeys.effectifInconnu True

            else
                identity

        setEffectifMin =
            filters.effectifMin
                |> Maybe.map (QS.setStr queryKeys.effectifMin)
                |> Maybe.withDefault identity

        setEffectifMax =
            filters.effectifMax
                |> Maybe.map (QS.setStr queryKeys.effectifMax)
                |> Maybe.withDefault identity

        setExercices =
            filters.exercices
                |> List.map .id
                |> QS.setListStr queryKeys.exercices

        setZrrs =
            filters.zrrs
                |> List.map .id
                |> QS.setListStr queryKeys.zrrs

        setCodeCommune =
            filters.communes
                |> List.map .id
                |> QS.setListStr queryKeys.communes

        setEpci =
            filters.epcis
                |> List.map .id
                |> QS.setListStr queryKeys.epcis

        setDepartement =
            filters.departements
                |> List.map .id
                |> QS.setListStr queryKeys.departements

        setRegion =
            filters.regions
                |> List.map .id
                |> QS.setListStr queryKeys.regions

        setNumero =
            filters.apiStreet
                |> Maybe.map .housenumber
                |> Maybe.map (QS.setStr queryKeys.numero)
                |> Maybe.withDefault identity

        setAdresse =
            filters.apiStreet
                |> Maybe.map .street
                |> Maybe.map (QS.setStr queryKeys.adresse)
                |> Maybe.withDefault identity

        setCp =
            filters.apiStreet
                |> Maybe.map .postcode
                |> Maybe.map (QS.setStr queryKeys.cp)
                |> Maybe.withDefault identity

        setVille =
            filters.apiStreet
                |> Maybe.map .city
                |> Maybe.map (QS.setStr queryKeys.ville)
                |> Maybe.withDefault identity

        setSituationGeographique =
            case filters.situationGeographique of
                HorsDeMonTerritoire ->
                    "hors"
                        |> QS.setStr queryKeys.situationGeographique

                TouteLaFrance ->
                    "france"
                        |> QS.setStr queryKeys.situationGeographique

                _ ->
                    identity

        setEntrepriseType =
            filters.entrepriseTypes
                |> List.map entrepriseTypeToString
                |> QS.setListStr queryKeys.entrepriseTypes

        setActivites =
            case filters.activites of
                [] ->
                    identity

                activites ->
                    activites
                        |> QS.setListStr queryKeys.activites

        setActivitesToutes =
            if filters.activitesToutes then
                QS.setStr queryKeys.activitesToutes "oui"

            else
                identity

        setDemandes =
            case filters.demandes of
                [] ->
                    identity

                demandes ->
                    demandes
                        |> List.map Data.Demande.typeDemandeToString
                        |> QS.setListStr queryKeys.demandes

        setZones =
            case filters.zones of
                [] ->
                    identity

                zones ->
                    zones
                        |> QS.setListStr queryKeys.localisations

        setZonesToutes =
            if filters.zonesToutes then
                QS.setStr queryKeys.localisationsToutes "oui"

            else
                identity

        setMots =
            case filters.mots of
                [] ->
                    identity

                mots ->
                    mots
                        |> QS.setListStr queryKeys.motsCles

        setMotsTous =
            if filters.motsTous then
                QS.setStr queryKeys.motsClesTous "oui"

            else
                identity

        setOrder =
            case filters.orderBy of
                -- Alphabetique ->
                --     QS.setStr queryKeys.tri <|
                --         orderByToString Alphabetique
                --
                EtablissementCreation ->
                    QS.setStr queryKeys.tri <|
                        orderByToString EtablissementCreation

                EtablissementFermeture ->
                    QS.setStr queryKeys.tri <|
                        orderByToString EtablissementFermeture

                EtablissementConsultation ->
                    identity

        setDirection =
            case filters.orderDirection of
                Asc ->
                    QS.setStr queryKeys.direction <|
                        orderDirectionToString Asc

                Desc ->
                    identity

        setQpvs =
            if filters.tousQpvs then
                QS.setListStr queryKeys.qpvs [ "tous" ]

            else
                case filters.qpvs of
                    [] ->
                        identity

                    qpvs ->
                        qpvs
                            |> List.map .id
                            |> QS.setListStr queryKeys.qpvs

        setTis =
            if filters.tousTis then
                QS.setListStr queryKeys.tis [ "tous" ]

            else
                case filters.tis of
                    [] ->
                        identity

                    tis ->
                        tis
                            |> List.map .id
                            |> QS.setListStr queryKeys.tis

        setEss =
            let
                isEss =
                    List.member EssOui filters.ess

                isNotEss =
                    List.member EssNon filters.ess
            in
            case ( isEss, isNotEss ) of
                ( True, False ) ->
                    QS.setStr queryKeys.ess "true"

                ( False, True ) ->
                    QS.setStr queryKeys.ess "false"

                ( True, True ) ->
                    identity

                ( False, False ) ->
                    identity

        setAcv =
            let
                isAcv =
                    List.member AcvOui filters.acv

                isNotAcv =
                    List.member AcvNon filters.acv
            in
            case ( isAcv, isNotAcv ) of
                ( True, False ) ->
                    QS.setStr queryKeys.acv "true"

                ( False, True ) ->
                    QS.setStr queryKeys.acv "false"

                ( True, True ) ->
                    identity

                ( False, False ) ->
                    identity

        setPvd =
            let
                isPvd =
                    List.member PvdOui filters.pvd

                isNotPvd =
                    List.member PvdNon filters.pvd
            in
            case ( isPvd, isNotPvd ) of
                ( True, False ) ->
                    QS.setStr queryKeys.pvd "true"

                ( False, True ) ->
                    QS.setStr queryKeys.pvd "false"

                ( True, True ) ->
                    identity

                ( False, False ) ->
                    identity

        setVa =
            let
                isVa =
                    List.member VaOui filters.va

                isNotVa =
                    List.member VaNon filters.va
            in
            case ( isVa, isNotVa ) of
                ( True, False ) ->
                    QS.setStr queryKeys.va "true"

                ( False, True ) ->
                    QS.setStr queryKeys.va "false"

                ( True, True ) ->
                    identity

                ( False, False ) ->
                    identity

        setAfrs =
            filters.afrs
                |> List.map .id
                |> QS.setListStr queryKeys.afrs

        setSiege =
            let
                isSiege =
                    List.member SiegeOui filters.siege

                isNotSiege =
                    List.member SiegeNon filters.siege
            in
            case ( isSiege, isNotSiege ) of
                ( True, False ) ->
                    QS.setStr queryKeys.siege "true"

                ( False, True ) ->
                    QS.setStr queryKeys.siege "false"

                ( True, True ) ->
                    identity

                ( False, False ) ->
                    identity

        setProcedures =
            let
                isProcedure =
                    List.member ProcedureOui filters.procedures

                isNotProcedure =
                    List.member ProcedureNon filters.procedures
            in
            case ( isProcedure, isNotProcedure ) of
                ( True, False ) ->
                    QS.setStr queryKeys.procedures "true"

                ( False, True ) ->
                    QS.setStr queryKeys.procedures "false"

                ( True, True ) ->
                    identity

                ( False, False ) ->
                    identity

        setProceduresDateMin =
            if List.member ProcedureOui filters.procedures then
                filters.proceduresApres
                    |> Maybe.map Lib.Date.formatDateToYYYYMMDD
                    |> Maybe.map (QS.setStr queryKeys.proceduresApres)
                    |> Maybe.withDefault identity

            else
                identity

        setProceduresDateMax =
            if List.member ProcedureOui filters.procedures then
                filters.proceduresAvant
                    |> Maybe.map Lib.Date.formatDateToYYYYMMDD
                    |> Maybe.map (QS.setStr queryKeys.proceduresAvant)
                    |> Maybe.withDefault identity

            else
                identity

        setSubventions =
            filters.subventions
                |> List.map subventionToString
                |> QS.setListStr queryKeys.subventions

        setSubventionsAnneeMin =
            if List.member SubventionOui filters.subventions then
                filters.subventionsAnneeMin
                    |> Maybe.map (QS.setStr queryKeys.subventionsAnneeMin)
                    |> Maybe.withDefault identity

            else
                identity

        setSubventionsAnneeMax =
            if List.member SubventionOui filters.subventions then
                filters.subventionsAnneeMax
                    |> Maybe.map (QS.setStr queryKeys.subventionsAnneeMax)
                    |> Maybe.withDefault identity

            else
                identity

        setSubventionsMontantMin =
            if List.member SubventionOui filters.subventions then
                filters.subventionsMontantMin
                    |> Maybe.map (QS.setStr queryKeys.subventionsMontantMin)
                    |> Maybe.withDefault identity

            else
                identity

        setSubventionsMontantMax =
            if List.member SubventionOui filters.subventions then
                filters.subventionsMontantMax
                    |> Maybe.map (QS.setStr queryKeys.subventionsMontantMax)
                    |> Maybe.withDefault identity

            else
                identity

        setCreeApres =
            filters.creeApres
                |> Maybe.map Lib.Date.formatDateToYYYYMMDD
                |> Maybe.map (QS.setStr queryKeys.creeApres)
                |> Maybe.withDefault identity

        setCreeAvant =
            filters.creeAvant
                |> Maybe.map Lib.Date.formatDateToYYYYMMDD
                |> Maybe.map (QS.setStr queryKeys.creeAvant)
                |> Maybe.withDefault identity

        setFermeApres =
            filters.fermeApres
                |> Maybe.map Lib.Date.formatDateToYYYYMMDD
                |> Maybe.map (QS.setStr queryKeys.fermeApres)
                |> Maybe.withDefault identity

        setFermeAvant =
            filters.fermeAvant
                |> Maybe.map Lib.Date.formatDateToYYYYMMDD
                |> Maybe.map (QS.setStr queryKeys.fermeAvant)
                |> Maybe.withDefault identity

        setAccompagnes =
            filters.accompagnes
                |> List.map accompagnesToString
                |> QS.setListStr queryKeys.accompagnes

        setAccompagnesApres =
            filters.accompagnesApres
                |> Maybe.map Lib.Date.formatDateToYYYYMMDD
                |> Maybe.map (QS.setStr queryKeys.accompagnesApres)
                |> Maybe.withDefault identity

        setAccompagnesAvant =
            filters.accompagnesAvant
                |> Maybe.map Lib.Date.formatDateToYYYYMMDD
                |> Maybe.map (QS.setStr queryKeys.accompagnesAvant)
                |> Maybe.withDefault identity

        setMicro =
            let
                isMicro =
                    List.member MicroOui filters.micro

                isNotMicro =
                    List.member MicroNon filters.micro
            in
            case ( isMicro, isNotMicro ) of
                ( True, False ) ->
                    QS.setStr queryKeys.micro "true"

                ( False, True ) ->
                    QS.setStr queryKeys.micro "false"

                ( True, True ) ->
                    identity

                ( False, False ) ->
                    identity

        setFavori =
            let
                isFavori =
                    List.member FavoriOui filters.favori

                isNotFavori =
                    List.member FavoriNon filters.favori
            in
            case ( isFavori, isNotFavori ) of
                ( True, False ) ->
                    QS.setStr queryKeys.favori "true"

                ( False, True ) ->
                    QS.setStr queryKeys.favori "false"

                ( True, True ) ->
                    identity

                ( False, False ) ->
                    identity

        setAvecContact =
            let
                isAvecContact =
                    List.member AvecContactOui filters.avecContact

                isNotAvecContact =
                    List.member AvecContactNon filters.avecContact
            in
            case ( isAvecContact, isNotAvecContact ) of
                ( True, False ) ->
                    QS.setStr queryKeys.avecContact "true"

                ( False, True ) ->
                    QS.setStr queryKeys.avecContact "false"

                ( True, True ) ->
                    identity

                ( False, False ) ->
                    identity

        setAncienCreateur =
            let
                isAncienCreateur =
                    List.member AncienCreateurOui filters.ancienCreateur

                isNotAncienCreateur =
                    List.member AncienCreateurNon filters.ancienCreateur
            in
            case ( isAncienCreateur, isNotAncienCreateur ) of
                ( True, False ) ->
                    QS.setStr queryKeys.ancienCreateur "true"

                ( False, True ) ->
                    QS.setStr queryKeys.ancienCreateur "false"

                ( True, True ) ->
                    identity

                ( False, False ) ->
                    identity

        setCaVarTypes =
            filters.caVarTypes
                |> List.map variationTypeToString
                |> QS.setListStr queryKeys.caVarTypes

        setCaVarMin =
            Maybe.withDefault identity <|
                Maybe.map (QS.setStr queryKeys.caVarMin) <|
                    case filters.caVarTypes of
                        [] ->
                            Nothing

                        _ ->
                            filters.caVarMin

        setCaVarMax =
            Maybe.withDefault identity <|
                Maybe.map (QS.setStr queryKeys.caVarMax) <|
                    case filters.caVarTypes of
                        [] ->
                            Nothing

                        _ ->
                            filters.caVarMax

        setEffectifVarTypes =
            filters.effectifVarTypes
                |> List.map variationTypeToString
                |> QS.setListStr queryKeys.effectifVarTypes

        setEffectifVarMin =
            Maybe.withDefault identity <|
                Maybe.map (QS.setStr queryKeys.effectifVarMin) <|
                    case filters.effectifVarTypes of
                        [] ->
                            Nothing

                        _ ->
                            filters.effectifVarMin

        setEffectifVarMax =
            Maybe.withDefault identity <|
                Maybe.map (QS.setStr queryKeys.effectifVarMax) <|
                    case filters.effectifVarTypes of
                        [] ->
                            Nothing

                        _ ->
                            filters.effectifVarMax

        setFiliales =
            filters.filiales
                |> List.map .id
                |> QS.setListStr queryKeys.filiale

        setDetentions =
            filters.detentions
                |> List.map .id
                |> QS.setListStr queryKeys.detention

        setEgaproIndiceMin =
            if filters.egaproIndiceMin == "0" then
                identity

            else
                QS.setStr queryKeys.egaproIndiceMin filters.egaproIndiceMin

        setEgaproIndiceMax =
            if filters.egaproIndiceMax == "100" then
                identity

            else
                QS.setStr queryKeys.egaproIndiceMax filters.egaproIndiceMax

        setEgaproCadresMin =
            if filters.egaproCadresMin == "0" then
                identity

            else
                QS.setStr queryKeys.egaproCadresMin filters.egaproCadresMin

        setEgaproCadresMax =
            if filters.egaproCadresMax == "100" then
                identity

            else
                QS.setStr queryKeys.egaproCadresMax filters.egaproCadresMax

        setEgaproInstancesMin =
            if filters.egaproInstancesMin == "0" then
                identity

            else
                QS.setStr queryKeys.egaproInstancesMin filters.egaproInstancesMin

        setEgaproInstancesMax =
            if filters.egaproInstancesMax == "100" then
                identity

            else
                QS.setStr queryKeys.egaproInstancesMax filters.egaproInstancesMax
    in
    QS.empty
        |> setEtat
        |> setFormesJuridiques
        |> setRecherche
        |> setCategoriesJuridiques
        |> setCodesNaf
        |> setCodeCommune
        |> setEpci
        |> setDepartement
        |> setRegion
        |> setEffectifsInconnus
        |> setEffectifMin
        |> setEffectifMax
        |> setExercices
        |> setNumero
        |> setAdresse
        |> setCp
        |> setVille
        |> setSituationGeographique
        |> setActivites
        |> setActivitesToutes
        |> setZones
        |> setZonesToutes
        |> setMots
        |> setMotsTous
        |> setDemandes
        |> setOrder
        |> setDirection
        |> setQpvs
        |> setTis
        |> setZrrs
        |> setEss
        |> setAcv
        |> setPvd
        |> setVa
        |> setAfrs
        |> setSiege
        |> setProcedures
        |> setProceduresDateMin
        |> setProceduresDateMax
        |> setSubventions
        |> setSubventionsAnneeMin
        |> setSubventionsAnneeMax
        |> setSubventionsMontantMin
        |> setSubventionsMontantMax
        |> setCreeApres
        |> setCreeAvant
        |> setFermeApres
        |> setFermeAvant
        |> setAccompagnes
        |> setAccompagnesApres
        |> setAccompagnesAvant
        |> setMicro
        |> setFavori
        |> setAvecContact
        |> setAncienCreateur
        |> setEntrepriseType
        |> setGeolocalisation
        |> setLongitude
        |> setLatitude
        |> setZoom
        -- |> setNwse
        |> setCaVarTypes
        |> setCaVarMin
        |> setCaVarMax
        |> setEffectifVarTypes
        |> setEffectifVarMin
        |> setEffectifVarMax
        |> setFiliales
        |> setDetentions
        |> setEgaproIndiceMin
        |> setEgaproIndiceMax
        |> setEgaproCadresMin
        |> setEgaproCadresMax
        |> setEgaproInstancesMin
        |> setEgaproInstancesMax


variationTypeToString : VariationType -> String
variationTypeToString variationType =
    case variationType of
        VariationHausse ->
            "hausse"

        VariationBaisse ->
            "baisse"


stringToVariationType : String -> Maybe VariationType
stringToVariationType variationType =
    case variationType of
        "hausse" ->
            Just VariationHausse

        "baisse" ->
            Just VariationBaisse

        _ ->
            Nothing


variationTypeToDisplay : VariationType -> String
variationTypeToDisplay variationType =
    case variationType of
        VariationHausse ->
            "Hausse"

        VariationBaisse ->
            "Baisse"


getPagination : Model -> Pagination
getPagination { pagination } =
    pagination


getCurrentUrl : Model -> String
getCurrentUrl { currentUrl } =
    currentUrl


getFilters : Model -> Filters
getFilters { filters } =
    filters


queryKeys :
    { etat : String
    , recherche : String
    , categoriesJuridiques : String
    , categoriesJuridiquesSans : String
    , codesNaf : String
    , codesNafSans : String
    , categorieJuridiqueCourante : String
    , communes : String
    , epcis : String
    , departements : String
    , regions : String
    , effectifInconnu : String
    , effectifMin : String
    , effectifMax : String
    , exercices : String
    , numero : String
    , adresse : String
    , cp : String
    , ville : String
    , situationGeographique : String
    , entrepriseTypes : String
    , activites : String
    , activitesToutes : String
    , demandes : String
    , localisations : String
    , localisationsToutes : String
    , motsCles : String
    , motsClesTous : String
    , qpvs : String
    , tis : String
    , zrrs : String
    , ess : String
    , acv : String
    , pvd : String
    , va : String
    , afrs : String
    , siege : String
    , procedures : String
    , proceduresApres : String
    , proceduresAvant : String
    , subventions : String
    , subventionsAnneeMin : String
    , subventionsAnneeMax : String
    , subventionsMontantMin : String
    , subventionsMontantMax : String
    , creeApres : String
    , creeAvant : String
    , fermeApres : String
    , fermeAvant : String
    , tri : String
    , direction : String
    , accompagnes : String
    , accompagnesApres : String
    , accompagnesAvant : String
    , micro : String
    , favori : String
    , avecContact : String
    , ancienCreateur : String
    , geolocalisation : String
    , longitude : String
    , latitude : String
    , zoom : String
    , filiale : String
    , detention : String

    -- , nwse : String
    , caVarTypes : String
    , caVarMin : String
    , caVarMax : String
    , effectifVarTypes : String
    , effectifVarMin : String
    , effectifVarMax : String
    , egaproIndiceMin : String
    , egaproIndiceMax : String
    , egaproCadresMin : String
    , egaproCadresMax : String
    , egaproInstancesMin : String
    , egaproInstancesMax : String
    }
queryKeys =
    { etat = "etat"
    , categorieJuridiqueCourante = "categorieJuridiqueCourante"
    , communes = "communes"
    , categoriesJuridiques = "categoriesJuridiques"
    , categoriesJuridiquesSans = "categoriesJuridiquesSans"
    , codesNaf = "codesNaf"
    , codesNafSans = "codesNafSans"
    , effectifInconnu = "effectifInconnu"
    , effectifMin = "effectifMin"
    , effectifMax = "effectifMax"
    , exercices = "cas"
    , epcis = "epcis"
    , departements = "departements"
    , regions = "regions"
    , ess = "ess"
    , qpvs = "qpvs"
    , tis = "territoireIndustries"
    , recherche = "recherche"
    , zrrs = "zrrs"
    , demandes = "demandes"
    , activites = "activites"
    , activitesToutes = "activitesToutes"
    , localisations = "localisations"
    , localisationsToutes = "localisationsToutes"
    , motsCles = "motsCles"
    , motsClesTous = "motsClesTous"
    , situationGeographique = "geo"
    , entrepriseTypes = "types"
    , numero = "numero"
    , adresse = "rue"
    , cp = "cp"
    , ville = "ville"
    , acv = "acv"
    , pvd = "pvd"
    , va = "va"
    , afrs = "afrs"
    , siege = "siege"
    , procedures = "procedure"
    , proceduresApres = "procApres"
    , proceduresAvant = "procAvant"
    , subventions = "subventions"
    , subventionsAnneeMin = "subAnneeMin"
    , subventionsAnneeMax = "subAnneeMax"
    , subventionsMontantMin = "subMontantMin"
    , subventionsMontantMax = "subMontantMax"
    , creeApres = "creeApres"
    , creeAvant = "creeAvant"
    , fermeApres = "fermeApres"
    , fermeAvant = "fermeAvant"
    , tri = "tri"
    , direction = "direction"
    , accompagnes = "accompagnes"
    , accompagnesApres = "accompagnesApres"
    , accompagnesAvant = "accompagnesAvant"
    , micro = "micro"
    , favori = "favori"
    , avecContact = "avecContact"
    , ancienCreateur = "ancienCreateur"
    , geolocalisation = "geoloc"
    , longitude = "longitude"
    , latitude = "latitude"
    , zoom = "z"

    -- , nwse = "nwse"
    , caVarTypes = "caVarTypes"
    , caVarMin = "caVarMin"
    , caVarMax = "caVarMax"
    , effectifVarTypes = "effectifVarTypes"
    , effectifVarMin = "effectifVarMin"
    , effectifVarMax = "effectifVarMax"
    , filiale = "filiales"
    , detention = "detentions"
    , egaproIndiceMin = "egaproIndiceMin"
    , egaproIndiceMax = "egaproIndiceMax"
    , egaproCadresMin = "egaproCadresMin"
    , egaproCadresMax = "egaproCadresMax"
    , egaproInstancesMin = "egaproInstancesMin"
    , egaproInstancesMax = "egaproInstancesMax"
    }


filtersAndPaginationToQuery : ( Filters, Pagination ) -> String
filtersAndPaginationToQuery ( filters, pagination ) =
    QS.merge (filtersToQuery filters) (paginationToQuery pagination)
        |> QS.serialize (QS.config |> QS.addQuestionMark False)


toHref : ( Filters, Pagination ) -> Int -> String
toHref ( filters, pagination ) newPage =
    Route.toUrl <|
        Route.Etablissements <|
            (\q ->
                if q == "" then
                    Nothing

                else
                    Just q
            )
            <|
                filtersAndPaginationToQuery ( filters, { pagination | page = newPage } )


queryToFiltersAndPagination : Maybe String -> ( Filters, Pagination )
queryToFiltersAndPagination rawQuery =
    rawQuery
        |> Maybe.withDefault ""
        |> (\query -> ( queryToFilters query, queryToPagination query ))


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ model.selectCategoriesJuridiques |> MultiSelectRemote.subscriptions
        , model.selectCodesNaf |> MultiSelectRemote.subscriptions
        , model.selectCommunes |> MultiSelectRemote.subscriptions
        , model.selectEpcis |> MultiSelectRemote.subscriptions
        , model.selectDepartements |> MultiSelectRemote.subscriptions
        , model.selectRegions |> MultiSelectRemote.subscriptions
        , model.selectExercices |> MultiSelect.subscriptions
        , model.selectAdresse |> SingleSelectRemote.subscriptions
        , model.selectActivites |> MultiSelectRemote.subscriptions
        , model.selectDemandes |> MultiSelect.subscriptions
        , model.selectZones |> MultiSelectRemote.subscriptions
        , model.selectMots |> MultiSelectRemote.subscriptions
        , model.selectQpvs |> MultiSelectRemote.subscriptions
        , model.selectTis |> MultiSelectRemote.subscriptions
        , model.selectZrrs |> MultiSelect.subscriptions
        , model.selectAfrs |> MultiSelect.subscriptions
        , model.selectFiliales |> MultiSelectRemote.subscriptions
        , model.selectDetentions |> MultiSelectRemote.subscriptions
        ]


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Effect.none )

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        LogError normalMsg error ->
            model
                |> Shared.logErrorHelper normalMsg error

        ReceivedRecherchesEnregistrees resp ->
            { model | recherchesEnregistrees = resp }
                |> Effect.withNone

        SearchRechercheEnregistree rechercheEnregistree ->
            init defaultFiltresRequete <| Just rechercheEnregistree.url

        SaveRechercheEnregistree ->
            { model
                | saveRechercheEnregistree =
                    Just <|
                        { nom = ""
                        , url = model.currentUrl
                        , saveRequest = RD.NotAsked
                        }
            }
                |> Effect.withNone

        CancelSaveRechercheEnregistree ->
            { model | saveRechercheEnregistree = Nothing }
                |> Effect.withNone

        UpdateSaveRechercheEnregistree nom ->
            { model
                | saveRechercheEnregistree =
                    model.saveRechercheEnregistree
                        |> Maybe.map
                            (\saveFilter -> { saveFilter | nom = nom, saveRequest = RD.NotAsked })
            }
                |> Effect.withNone

        ConfirmSaveRechercheEnregistree ->
            case model.saveRechercheEnregistree of
                Nothing ->
                    model
                        |> Effect.withNone

                Just saveRechercheEnregistree ->
                    { model
                        | saveRechercheEnregistree =
                            saveRechercheEnregistree
                                |> (\sf -> { sf | saveRequest = RD.Loading })
                                |> Just
                    }
                        |> Effect.withCmd (doSaveRechercheEnregistree saveRechercheEnregistree.nom model.currentUrl)

        ReceivedSaveRechercheEnregistree resp ->
            case resp of
                RD.Success _ ->
                    { model | saveRechercheEnregistree = Nothing, recherchesEnregistrees = resp }
                        |> Effect.withNone

                _ ->
                    { model
                        | saveRechercheEnregistree =
                            model.saveRechercheEnregistree
                                |> Maybe.map
                                    (\sf ->
                                        { sf | saveRequest = resp }
                                    )
                    }
                        |> Effect.withNone

        DeleteRechercheEnregistree rechercheEnregistree ->
            { model
                | deleteRechercheEnregistree = Just { rechercheEnregistree = rechercheEnregistree, deleteRequest = RD.NotAsked }
            }
                |> Effect.withNone

        CancelDeleteFilter ->
            { model | deleteRechercheEnregistree = Nothing }
                |> Effect.withNone

        ConfirmDeleteFilter ->
            case model.deleteRechercheEnregistree of
                Nothing ->
                    model
                        |> Effect.withNone

                Just deleteRechercheEnregistree ->
                    { model
                        | deleteRechercheEnregistree =
                            deleteRechercheEnregistree
                                |> (\sf -> { sf | deleteRequest = RD.Loading })
                                |> Just
                    }
                        |> Effect.withCmd (doDeleteRechercheEnregistree deleteRechercheEnregistree.rechercheEnregistree)

        ReceivedDeleteRechercheEnregistree resp ->
            case resp of
                RD.Success _ ->
                    { model
                        | deleteRechercheEnregistree = Nothing
                        , recherchesEnregistrees = resp
                    }
                        |> Effect.withNone

                _ ->
                    { model
                        | saveRechercheEnregistree =
                            model.saveRechercheEnregistree
                                |> Maybe.map
                                    (\sf ->
                                        { sf | saveRequest = resp }
                                    )
                    }
                        |> Effect.withNone

        ClearAllFilters ->
            let
                ( updatedSelectAdresse, selectCmd ) =
                    SingleSelectRemote.setText "" selectAdresseConfig model.selectAdresse

                newModel =
                    { model | filters = emptyFilters, selectAdresse = updatedSelectAdresse }
            in
            ( newModel
            , Effect.batch [ updateUrl newModel, Effect.fromCmd selectCmd ]
            )

        UpdatedSearch recherche ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | recherche = recherche }) }
            in
            ( newModel
            , Effect.none
            )

        SetEtatFilter add etat ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | etat = setEtats model.filters add etat
                                            , orderBy =
                                                case ( f.orderBy, add, etat ) of
                                                    ( EtablissementFermeture, False, Ferme ) ->
                                                        EtablissementConsultation

                                                    _ ->
                                                        f.orderBy
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SetGeolocalisationFilter add geolocalisation ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | geolocalisation = setGeolocalisations model.filters add geolocalisation
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SetEntrepriseTypeFilter add entrepriseType ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | entrepriseTypes = setEntrepriseTypes model.filters add entrepriseType
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SetMicroFilter add micro ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | micro = setMicros model.filters add micro
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SetFavoriFilter add favori ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | favori = setFavoris model.filters add favori
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SetAvecContactFilter add avecContact ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | avecContact = setAvecContacts model.filters add avecContact
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SetAncienCreateurFilter add ancienCreateur ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | ancienCreateur = setAncienCreateurs model.filters add ancienCreateur
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SetSituationGeographiqueFilter situationGeographique ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | situationGeographique = situationGeographique }) }
            in
            ( newModel
            , Effect.none
            )

        SetFormesJuridiquesFilter courantesOnly ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | formesJuridiques =
                                                if courantesOnly then
                                                    Courantes

                                                else
                                                    Toutes
                                            , categoriesJuridiques =
                                                if courantesOnly then
                                                    []

                                                else
                                                    f.categoriesJuridiques
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedCategoriesJuridiques ( categoriesJuridiques, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectCategoriesJuridiquesConfig model.selectCategoriesJuridiques

                categoriesJuridiquesUniques =
                    case categoriesJuridiques of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                categoriesJuridiques

                newModel =
                    { model
                        | selectCategoriesJuridiques = updatedSelect
                        , filters =
                            model.filters
                                |> (\l ->
                                        { l | categoriesJuridiques = categoriesJuridiquesUniques }
                                   )
                    }
            in
            ( newModel
            , Effect.batch [ Effect.none, Effect.fromCmd selectCmd ]
            )

        UpdatedSelectCategoriesJuridiques sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectCategoriesJuridiquesConfig model.selectCategoriesJuridiques
            in
            ( { model
                | selectCategoriesJuridiques = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectCategorieJuridique categorieJuridique ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | categoriesJuridiques =
                                                f.categoriesJuridiques
                                                    |> List.filter ((/=) categorieJuridique)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ToggleCategorieJuridiqueSans ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | categoriesJuridiquesSans = not f.categoriesJuridiquesSans
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedCodesNaf ( codesNaf, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectCodesNafConfig model.selectCodesNaf

                codesNafUniques =
                    case codesNaf of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                codesNaf

                newModel =
                    { model
                        | selectCodesNaf = updatedSelect
                        , filters =
                            model.filters
                                |> (\l ->
                                        { l | codesNaf = codesNafUniques }
                                   )
                    }
            in
            ( newModel
            , Effect.batch [ Effect.none, Effect.fromCmd selectCmd ]
            )

        UpdatedSelectCodesNaf sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectCodesNafConfig model.selectCodesNaf
            in
            ( { model
                | selectCodesNaf = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectCodeNaf codeNaf ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | codesNaf =
                                                f.codesNaf
                                                    |> List.filter ((/=) codeNaf)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ToggleCodeNafSans ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | codesNafSans = not f.codesNafSans
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedCommunes ( communes, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectCommunesConfig model.filters.situationGeographique) model.selectCommunes

                communesUniques =
                    case communes of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                communes

                newModel =
                    { model
                        | selectCommunes = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | communes = communesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectCommunes sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectCommunesConfig model.filters.situationGeographique) model.selectCommunes
            in
            ( { model | selectCommunes = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectCommune commune ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | communes =
                                                filters.communes
                                                    |> List.filter (\c -> c.id /= commune.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedEpcis ( epcis, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectEpcisConfig model.filters.situationGeographique) model.selectEpcis

                epcisUniques =
                    case epcis of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                epcis

                newModel =
                    { model
                        | selectEpcis = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | epcis = epcisUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectEpcis sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectEpcisConfig model.filters.situationGeographique) model.selectEpcis
            in
            ( { model | selectEpcis = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectEpci epci ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | epcis =
                                                filters.epcis
                                                    |> List.filter (\c -> c.id /= epci.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedDepartements ( departements, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectDepartementsConfig model.filters.situationGeographique) model.selectDepartements

                departementsUniques =
                    case departements of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                departements

                newModel =
                    { model
                        | selectDepartements = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | departements = departementsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectDepartements sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectDepartementsConfig model.filters.situationGeographique) model.selectDepartements
            in
            ( { model | selectDepartements = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectDepartement departement ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | departements =
                                                filters.departements
                                                    |> List.filter (\c -> c.id /= departement.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedRegions ( regions, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectRegionsConfig model.filters.situationGeographique) model.selectRegions

                regionsUniques =
                    case regions of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                regions

                newModel =
                    { model
                        | selectRegions = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | regions = regionsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectRegions sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectRegionsConfig model.filters.situationGeographique) model.selectRegions
            in
            ( { model | selectRegions = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectRegion region ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | regions =
                                                filters.regions
                                                    |> List.filter (\c -> c.id /= region.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedExercices ( exercices, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectExercices

                exercicesUniques =
                    case exercices of
                        [] ->
                            []

                        e :: rest ->
                            if List.member e rest then
                                rest

                            else
                                exercices

                newModel =
                    { model
                        | selectExercices = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | exercices = exercicesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectExercices sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectExercices
            in
            ( { model
                | selectExercices = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectedExercices exercices ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | exercices =
                                                filters.exercices
                                                    |> List.filter ((/=) exercices)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedAdresse ( apiAdresse, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectAdresseConfig model.selectAdresse

                newModel =
                    { model
                        | selectAdresse = updatedSelect
                        , filters =
                            model.filters
                                |> (\f -> { f | apiStreet = Just apiAdresse })
                    }
            in
            ( newModel
            , Effect.batch [ Effect.fromCmd selectCmd, Effect.none ]
            )

        UpdatedSelectAdresse sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectAdresseConfig model.selectAdresse

                newModel =
                    { model | selectAdresse = updatedSelect }
            in
            ( newModel, Effect.batch [ Effect.fromCmd selectCmd, Effect.none ] )

        ClearAdresse ->
            let
                ( updatedSelectAdresse, selectCmd ) =
                    SingleSelectRemote.setText "" selectAdresseConfig model.selectAdresse

                newModel =
                    { model | filters = model.filters |> (\f -> { f | apiStreet = Nothing }), selectAdresse = updatedSelectAdresse }
            in
            ( newModel, Effect.batch [ Effect.fromCmd selectCmd, Effect.none ] )

        SelectedActivites ( activites, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectActivitesConfig model.selectActivites

                activitesUniques =
                    case activites of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                activites

                newModel =
                    { model
                        | selectActivites = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | activites = activitesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectActivites sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectActivitesConfig model.selectActivites
            in
            ( { model
                | selectActivites = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectActivite activite ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | activites =
                                                filters.activites
                                                    |> List.filter ((/=) activite)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ToggleActivitesToutes ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | activitesToutes = not filters.activitesToutes
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedDemandes ( demandes, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectDemandes

                demandesUniques =
                    case demandes of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                demandes

                newModel =
                    { model
                        | selectDemandes = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | demandes = demandesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectDemandes sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectDemandes
            in
            ( { model
                | selectDemandes = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectDemande demande ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | demandes =
                                                filters.demandes
                                                    |> List.filter ((/=) demande)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedZones ( zones, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectZonesConfig model.selectZones

                zonesUniques =
                    case zones of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                zones

                newModel =
                    { model
                        | selectZones = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | zones = zonesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectZones sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectZonesConfig model.selectZones
            in
            ( { model
                | selectZones = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectZone zone ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | zones =
                                                filters.zones
                                                    |> List.filter ((/=) zone)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ToggleZonesToutes ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | zonesToutes = not filters.zonesToutes
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedMots ( mots, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectMotsConfig model.selectMots

                motsUniques =
                    case mots of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                mots

                newModel =
                    { model
                        | selectMots = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | mots = motsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectMots sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectMotsConfig model.selectMots
            in
            ( { model
                | selectMots = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectMot mot ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | mots =
                                                filters.mots
                                                    |> List.filter ((/=) mot)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ToggleMotsTous ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | motsTous = not filters.motsTous
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedQpvs ( qpvs, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectQpvsConfig model.filters.situationGeographique) model.selectQpvs

                qpvsUniques =
                    case qpvs of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a.id <| List.map .id rest then
                                rest

                            else
                                qpvs

                newModel =
                    { model
                        | selectQpvs = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | qpvs = qpvsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectQpvs sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectQpvsConfig model.filters.situationGeographique) model.selectQpvs
            in
            ( { model
                | selectQpvs = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectQpv qpv ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | qpvs =
                                                filters.qpvs
                                                    |> List.filter (\{ id } -> id /= qpv.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ToggleAllQpvs on ->
            ( { model
                | filters =
                    model.filters
                        |> (\filters ->
                                { filters
                                    | qpvs = []
                                    , tousQpvs = on
                                }
                           )
              }
            , Effect.none
            )

        SelectedTis ( tis, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectTisConfig model.filters.situationGeographique) model.selectTis

                tisUniques =
                    case tis of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a.id <| List.map .id rest then
                                rest

                            else
                                tis

                newModel =
                    { model
                        | selectTis = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | tis = tisUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectTis sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectTisConfig model.filters.situationGeographique) model.selectTis
            in
            ( { model
                | selectTis = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectTi ti ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | tis =
                                                filters.tis
                                                    |> List.filter (\{ id } -> id /= ti.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ToggleAllTis on ->
            ( { model
                | filters =
                    model.filters
                        |> (\filters ->
                                { filters
                                    | tis = []
                                    , tousTis = on
                                }
                           )
              }
            , Effect.none
            )

        SelectedZRR ( zrrs, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZrrs

                zrrsUniques =
                    case zrrs of
                        [] ->
                            []

                        z :: rest ->
                            if List.member z rest then
                                rest

                            else
                                zrrs

                newModel =
                    { model
                        | selectZrrs = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | zrrs = zrrsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectZRR sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZrrs
            in
            ( { model
                | selectZrrs = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectedZRR zrrs ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | zrrs =
                                                filters.zrrs
                                                    |> List.filter ((/=) zrrs)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedAFR ( afrs, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectAfrs

                afrsUniques =
                    case afrs of
                        [] ->
                            []

                        z :: rest ->
                            if List.member z rest then
                                rest

                            else
                                afrs

                newModel =
                    { model
                        | selectAfrs = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | afrs = afrsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectAFR sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectAfrs
            in
            ( { model
                | selectAfrs = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectedAFR afrs ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | afrs =
                                                filters.afrs
                                                    |> List.filter ((/=) afrs)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SetESSFilter add ess ->
            let
                currentEss =
                    model.filters.ess

                isMember =
                    List.member ess currentEss

                newEss =
                    case ( add, isMember ) of
                        ( True, True ) ->
                            currentEss

                        ( True, False ) ->
                            currentEss ++ [ ess ]

                        ( False, True ) ->
                            currentEss
                                |> List.filter (\t -> t /= ess)

                        ( False, False ) ->
                            currentEss

                newModel =
                    { model | filters = model.filters |> (\f -> { f | ess = newEss }) }
            in
            ( newModel
            , Effect.none
            )

        SetACVFilter add acv ->
            let
                currentAcv =
                    model.filters.acv

                isMember =
                    List.member acv currentAcv

                newAcv =
                    case ( add, isMember ) of
                        ( True, True ) ->
                            currentAcv

                        ( True, False ) ->
                            currentAcv ++ [ acv ]

                        ( False, True ) ->
                            currentAcv
                                |> List.filter (\t -> t /= acv)

                        ( False, False ) ->
                            currentAcv

                newModel =
                    { model | filters = model.filters |> (\f -> { f | acv = newAcv }) }
            in
            ( newModel
            , Effect.none
            )

        SetPVDFilter add pvd ->
            let
                currentPvd =
                    model.filters.pvd

                isMember =
                    List.member pvd currentPvd

                newPvd =
                    case ( add, isMember ) of
                        ( True, True ) ->
                            currentPvd

                        ( True, False ) ->
                            currentPvd ++ [ pvd ]

                        ( False, True ) ->
                            currentPvd
                                |> List.filter (\t -> t /= pvd)

                        ( False, False ) ->
                            currentPvd

                newModel =
                    { model | filters = model.filters |> (\f -> { f | pvd = newPvd }) }
            in
            ( newModel
            , Effect.none
            )

        SetVAFilter add va ->
            let
                currentVa =
                    model.filters.va

                isMember =
                    List.member va currentVa

                newVa =
                    case ( add, isMember ) of
                        ( True, True ) ->
                            currentVa

                        ( True, False ) ->
                            currentVa ++ [ va ]

                        ( False, True ) ->
                            currentVa
                                |> List.filter (\t -> t /= va)

                        ( False, False ) ->
                            currentVa

                newModel =
                    { model | filters = model.filters |> (\f -> { f | va = newVa }) }
            in
            ( newModel
            , Effect.none
            )

        SetSiegeFilter add siege ->
            let
                currentSiege =
                    model.filters.siege

                isMember =
                    List.member siege currentSiege

                newSiege =
                    case ( add, isMember ) of
                        ( True, True ) ->
                            currentSiege

                        ( True, False ) ->
                            currentSiege ++ [ siege ]

                        ( False, True ) ->
                            currentSiege
                                |> List.filter (\t -> t /= siege)

                        ( False, False ) ->
                            currentSiege

                newModel =
                    { model | filters = model.filters |> (\f -> { f | siege = newSiege }) }
            in
            ( newModel
            , Effect.none
            )

        SetProceduresFilter add procedure ->
            let
                currentProcedure =
                    model.filters.procedures

                isMember =
                    List.member procedure currentProcedure

                newProcedure =
                    case ( add, isMember ) of
                        ( True, True ) ->
                            currentProcedure

                        ( True, False ) ->
                            currentProcedure ++ [ procedure ]

                        ( False, True ) ->
                            currentProcedure
                                |> List.filter (\t -> t /= procedure)

                        ( False, False ) ->
                            currentProcedure

                newModel =
                    { model | filters = model.filters |> (\f -> { f | procedures = newProcedure }) }
            in
            ( newModel
            , Effect.none
            )

        SetSubventionsFilter add subvention ->
            let
                currentSubvention =
                    model.filters.subventions

                isMember =
                    List.member subvention currentSubvention

                newSubvention =
                    case ( add, isMember ) of
                        ( True, True ) ->
                            currentSubvention

                        ( True, False ) ->
                            currentSubvention ++ [ subvention ]

                        ( False, True ) ->
                            currentSubvention
                                |> List.filter (\t -> t /= subvention)

                        ( False, False ) ->
                            currentSubvention

                newModel =
                    { model | filters = model.filters |> (\f -> { f | subventions = newSubvention }) }
            in
            ( newModel
            , Effect.none
            )

        SetSubventionsAnneeMin subventionsAnneeMin ->
            let
                subventionsAnneeMax =
                    model.filters.subventionsAnneeMax

                nouveauSubventionsAnneeMax =
                    case ( subventionsAnneeMin, subventionsAnneeMax ) of
                        ( Nothing, Nothing ) ->
                            Nothing

                        ( Just _, Nothing ) ->
                            Nothing

                        ( Nothing, Just _ ) ->
                            subventionsAnneeMax

                        ( Just eam, Just eap ) ->
                            Maybe.map2 max (String.toFloat eam) (String.toFloat eap)
                                |> Maybe.map (String.fromFloat >> Just)
                                |> Maybe.withDefault subventionsAnneeMax
            in
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | subventionsAnneeMin = subventionsAnneeMin
                                    , subventionsAnneeMax = nouveauSubventionsAnneeMax
                                }
                           )
              }
            , Effect.none
            )

        SetSubventionsAnneeMax subventionsAnneeMax ->
            let
                subventionsAnneeMin =
                    model.filters.subventionsAnneeMin

                nouveauSubventionsAnneeMin =
                    case ( subventionsAnneeMin, subventionsAnneeMax ) of
                        ( Nothing, Nothing ) ->
                            Nothing

                        ( Just _, Nothing ) ->
                            subventionsAnneeMin

                        ( Nothing, Just _ ) ->
                            Nothing

                        ( Just eam, Just eap ) ->
                            Maybe.map2 min (String.toFloat eam) (String.toFloat eap)
                                |> Maybe.map (String.fromFloat >> Just)
                                |> Maybe.withDefault subventionsAnneeMin
            in
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | subventionsAnneeMax = subventionsAnneeMax
                                    , subventionsAnneeMin = nouveauSubventionsAnneeMin
                                }
                           )
              }
            , Effect.none
            )

        UpdatedSubventionsMontantMin subventionsMontantAuMoins ->
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | subventionsMontantMin = subventionsMontantAuMoins
                                }
                           )
              }
            , Effect.none
            )

        UpdatedSubventionsMontantMax subventionsMontantAuPlus ->
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | subventionsMontantMax = subventionsMontantAuPlus
                                }
                           )
              }
            , Effect.none
            )

        UpdateDate dateType date ->
            let
                updateFilters : Filters -> Filters
                updateFilters f =
                    case dateType of
                        CreeApres ->
                            { f | creeApres = date }

                        CreeAvant ->
                            { f | creeAvant = date }

                        FermeApres ->
                            { f | fermeApres = date }

                        FermeAvant ->
                            { f | fermeAvant = date }

                        ProcedureApres ->
                            { f | proceduresApres = date }

                        ProcedureAvant ->
                            { f | proceduresAvant = date }
            in
            ( { model | filters = updateFilters model.filters }
            , Effect.none
            )

        ClickedOrderBy orderBy orderDirection ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | orderBy = orderBy, orderDirection = orderDirection }) }
            in
            ( newModel
            , updateUrl newModel
            )

        UpdateUrl ( filters, pagination ) ->
            ( model, updateUrl { filters = filters, pagination = pagination } )

        ClickedSearch ->
            let
                forcedPagination =
                    model.pagination |> (\p -> { p | page = 1 })
            in
            ( model, updateUrl { model | pagination = forcedPagination } )

        UpdatedEffectifMin effectifMin ->
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | effectifMin = effectifMin
                                }
                           )
              }
            , Effect.none
            )

        UpdatedEffectifMax effectifMax ->
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | effectifMax = effectifMax
                                }
                           )
              }
            , Effect.none
            )

        ToggleEffectifInconnu value ->
            ( { model | filters = model.filters |> (\f -> { f | effectifInconnu = value }) }
            , Effect.none
            )

        SetAccompagnesFilter add accompagne ->
            let
                currentAccompagnes =
                    model.filters.accompagnes

                isMember =
                    List.member accompagne currentAccompagnes

                newAccompagne =
                    case ( add, isMember ) of
                        ( True, True ) ->
                            currentAccompagnes

                        ( True, False ) ->
                            currentAccompagnes ++ [ accompagne ]

                        ( False, True ) ->
                            currentAccompagnes
                                |> List.filter (\t -> t /= accompagne)

                        ( False, False ) ->
                            currentAccompagnes

                newModel =
                    { model | filters = model.filters |> (\f -> { f | accompagnes = newAccompagne }) }
            in
            ( newModel
            , Effect.none
            )

        UpdateAccompagnesDate dateType date ->
            let
                updateFilters : Filters -> Filters
                updateFilters f =
                    case dateType of
                        AccompagnesApres ->
                            { f | accompagnesApres = date }

                        AccompagnesAvant ->
                            { f | accompagnesAvant = date }
            in
            ( { model | filters = updateFilters model.filters }
            , Effect.none
            )

        SetCaVariationFilter add variation ->
            let
                currentCaVariations =
                    model.filters.caVarTypes

                isMember =
                    List.member variation currentCaVariations

                newCaVariations =
                    case ( add, isMember ) of
                        ( True, True ) ->
                            currentCaVariations

                        ( True, False ) ->
                            currentCaVariations ++ [ variation ]

                        ( False, True ) ->
                            currentCaVariations
                                |> List.filter (\t -> t /= variation)

                        ( False, False ) ->
                            currentCaVariations

                newModel =
                    { model | filters = model.filters |> (\f -> { f | caVarTypes = newCaVariations }) }
            in
            ( newModel
            , Effect.none
            )

        UpdatedCaVariationMin caVariationMin ->
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | caVarMin = caVariationMin
                                }
                           )
              }
            , Effect.none
            )

        UpdatedCaVariationMax caVarMax ->
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | caVarMax = caVarMax
                                }
                           )
              }
            , Effect.none
            )

        SetEffectifVariationFilter add variation ->
            let
                currentEffectifVariations =
                    model.filters.effectifVarTypes

                isMember =
                    List.member variation currentEffectifVariations

                newEffectifVariations =
                    case ( add, isMember ) of
                        ( True, True ) ->
                            currentEffectifVariations

                        ( True, False ) ->
                            currentEffectifVariations ++ [ variation ]

                        ( False, True ) ->
                            currentEffectifVariations
                                |> List.filter (\t -> t /= variation)

                        ( False, False ) ->
                            currentEffectifVariations

                newModel =
                    { model | filters = model.filters |> (\f -> { f | effectifVarTypes = newEffectifVariations }) }
            in
            ( newModel
            , Effect.none
            )

        UpdatedEffectifVarMin effectifVarMin ->
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | effectifVarMin = effectifVarMin
                                }
                           )
              }
            , Effect.none
            )

        UpdatedEffectifVariationMax effectifVariationMax ->
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | effectifVarMax = effectifVariationMax
                                }
                           )
              }
            , Effect.none
            )

        SelectedFiliales ( filiales, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectFilialesConfig model.selectFiliales

                filialesUniques =
                    case filiales of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                filiales

                newModel =
                    { model
                        | selectFiliales = updatedSelect
                        , filters =
                            model.filters
                                |> (\l ->
                                        { l | filiales = filialesUniques }
                                   )
                    }
            in
            ( newModel
            , Effect.batch [ Effect.none, Effect.fromCmd selectCmd ]
            )

        UpdatedSelectFiliales sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectFilialesConfig model.selectFiliales
            in
            ( { model
                | selectFiliales = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectFiliale filiale ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | filiales =
                                                f.filiales
                                                    |> List.filter ((/=) filiale)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedDetentions ( detentions, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectDetentionsConfig model.selectDetentions

                detentionsUniques =
                    case detentions of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                detentions

                newModel =
                    { model
                        | selectDetentions = updatedSelect
                        , filters =
                            model.filters
                                |> (\l ->
                                        { l | detentions = detentionsUniques }
                                   )
                    }
            in
            ( newModel
            , Effect.batch [ Effect.none, Effect.fromCmd selectCmd ]
            )

        UpdatedSelectDetentions sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectDetentionsConfig model.selectDetentions
            in
            ( { model
                | selectDetentions = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectDetention detention ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | detentions =
                                                f.detentions
                                                    |> List.filter ((/=) detention)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        UpdatedEgaproIndiceMin egaproIndiceMin ->
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | egaproIndiceMin = egaproIndiceMin
                                }
                           )
              }
            , Effect.none
            )

        UpdatedEgaproIndiceMax egaproIndiceMax ->
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | egaproIndiceMax = egaproIndiceMax
                                }
                           )
              }
            , Effect.none
            )

        UpdatedEgaproCadresMin egaproCadresMin ->
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | egaproCadresMin = egaproCadresMin
                                }
                           )
              }
            , Effect.none
            )

        UpdatedEgaproCadresMax egaproCadresMax ->
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | egaproCadresMax = egaproCadresMax
                                }
                           )
              }
            , Effect.none
            )

        UpdatedEgaproInstancesMin egaproInstancesMin ->
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | egaproInstancesMin = egaproInstancesMin
                                }
                           )
              }
            , Effect.none
            )

        UpdatedEgaproInstancesMax egaproInstancesMax ->
            ( { model
                | filters =
                    model.filters
                        |> (\f ->
                                { f
                                    | egaproInstancesMax = egaproInstancesMax
                                }
                           )
              }
            , Effect.none
            )


selectCharacterThreshold : Int
selectCharacterThreshold =
    0


selectDebounceDuration : Float
selectDebounceDuration =
    400


selectCategoriesJuridiquesId : String
selectCategoriesJuridiquesId =
    "champ-selection-categories-juridiques"


selectCategoriesJuridiquesConfig : MultiSelectRemote.SelectConfig CategorieJuridique
selectCategoriesJuridiquesConfig =
    { headers = []
    , url = Api.rechercheCategorieJuridique
    , optionDecoder = Decode.list <| Data.CategorieJuridique.decodeCategorieJuridique
    }


selectCodesNafId : String
selectCodesNafId =
    "champ-selection-codes-naf"


selectCodesNafConfig : MultiSelectRemote.SelectConfig CodeNaf
selectCodesNafConfig =
    { headers = []
    , url = Api.rechercheCodeNaf
    , optionDecoder = Decode.list <| Data.Naf.decodeCodeNaf
    }


selectAdresseId : String
selectAdresseId =
    "champ-selection-adresse"


selectAdresseConfig : SingleSelectRemote.SelectConfig ApiStreet
selectAdresseConfig =
    { headers = []
    , url = Api.rechercheAdresse Nothing Nothing Nothing
    , optionDecoder = decodeApiStreet
    }


selectActivitesId : String
selectActivitesId =
    "champ-selection-activites"


selectActivitesConfig : MultiSelectRemote.SelectConfig String
selectActivitesConfig =
    { headers = []
    , url = Api.rechercheActivite
    , optionDecoder =
        Decode.field "elements" <|
            Decode.list <|
                Decode.field "nom" Decode.string
    }


selectDemandesId : String
selectDemandesId =
    "champ-selection-demandes"


selectZonesId : String
selectZonesId =
    "champ-selection-zones"


selectZonesConfig : MultiSelectRemote.SelectConfig String
selectZonesConfig =
    { headers = []
    , url = Api.rechercheLocalisation
    , optionDecoder =
        Decode.field "elements" <|
            Decode.list <|
                Decode.field "nom" Decode.string
    }


selectMotsId : String
selectMotsId =
    "champ-selection-mots"


selectMotsConfig : MultiSelectRemote.SelectConfig String
selectMotsConfig =
    { headers = []
    , url = Api.rechercheMot
    , optionDecoder =
        Decode.field "elements" <|
            Decode.list <|
                Decode.field "nom" Decode.string
    }


selectCommunesId : String
selectCommunesId =
    "champ-selection-communes"


selectCommunesConfig : SituationGeographique -> MultiSelectRemote.SelectConfig Commune
selectCommunesConfig situationGeographique =
    { headers = []
    , url = Api.rechercheTerritoireGeo (situationGeographiqueToCode situationGeographique) "commune"
    , optionDecoder = Decode.field "territoires" <| Decode.list <| decodeCommune
    }


selectEpcisId : String
selectEpcisId =
    "champ-selection-epcis"


selectEpcisConfig : SituationGeographique -> MultiSelectRemote.SelectConfig Epci
selectEpcisConfig situationGeographique =
    { headers = []
    , url = Api.rechercheTerritoireGeo (situationGeographiqueToCode situationGeographique) "epci"
    , optionDecoder =
        Decode.field "territoires" <|
            Decode.list <|
                Decode.map2 Epci
                    (Decode.field "id" Decode.string)
                    (Decode.field "nom" Decode.string)
    }


selectDepartementsId : String
selectDepartementsId =
    "champ-selection-departements"


selectDepartementsConfig : SituationGeographique -> MultiSelectRemote.SelectConfig Departement
selectDepartementsConfig situationGeographique =
    { headers = []
    , url = Api.rechercheTerritoireGeo (situationGeographiqueToCode situationGeographique) "departement"
    , optionDecoder =
        Decode.field "territoires" <|
            Decode.list <|
                Decode.map2 Departement
                    (Decode.field "id" Decode.string)
                    (Decode.field "nom" Decode.string)
    }


selectRegionsId : String
selectRegionsId =
    "champ-selection-regions"


selectRegionsConfig : SituationGeographique -> MultiSelectRemote.SelectConfig Region
selectRegionsConfig situationGeographique =
    { headers = []
    , url = Api.rechercheTerritoireGeo (situationGeographiqueToCode situationGeographique) "region"
    , optionDecoder =
        Decode.field "territoires" <|
            Decode.list <|
                Decode.map2 Region
                    (Decode.field "id" Decode.string)
                    (Decode.field "nom" Decode.string)
    }


selectQpvsId : String
selectQpvsId =
    "champ-selection-qpvs"


selectQpvsConfig : SituationGeographique -> MultiSelectRemote.SelectConfig Qpv
selectQpvsConfig situationGeographique =
    { headers = []
    , url = Api.rechercheQpvGeo (situationGeographiqueToCode situationGeographique)
    , optionDecoder =
        Decode.field "qpvs" <|
            Decode.list <|
                Decode.map2 Qpv
                    (Decode.field "id" Decode.string)
                    (Decode.field "nom" Decode.string)
    }


selectTisId : String
selectTisId =
    "champ-selection-tis"


selectTisConfig : SituationGeographique -> MultiSelectRemote.SelectConfig TerritoireIndustrie
selectTisConfig situationGeographique =
    { headers = []
    , url = Api.rechercheTerritoireGeo (situationGeographiqueToCode situationGeographique) "territoire_industrie"
    , optionDecoder =
        Decode.field "territoires" <|
            Decode.list <|
                Decode.map2 TerritoireIndustrie
                    (Decode.field "id" Decode.string)
                    (Decode.field "nom" Decode.string)
    }


selectFilialesId : String
selectFilialesId =
    "champ-selection-filiales"


selectFilialesConfig : MultiSelectRemote.SelectConfig EntrepriseApercu
selectFilialesConfig =
    { headers = []
    , url = Api.rechercheEntreprise
    , optionDecoder = Decode.field "elements" <| Decode.list <| decodeEntrepriseApercu
    }


selectDetentionsId : String
selectDetentionsId =
    "champ-selection-detentions"


selectDetentionsConfig : MultiSelectRemote.SelectConfig EntrepriseApercu
selectDetentionsConfig =
    { headers = []
    , url = Api.rechercheEntreprise
    , optionDecoder = Decode.field "elements" <| Decode.list <| decodeEntrepriseApercu
    }


decodeEntrepriseApercu : Decoder EntrepriseApercu
decodeEntrepriseApercu =
    Decode.succeed EntrepriseApercu
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "nomAffichage" Decode.string)


getRecherchesEnregistrees :
    { sharedMsg : Shared.Msg -> msg
    , logError : Maybe (msg -> Shared.ErrorReport -> msg)
    , msg : WebData (List RechercheEnregistree) -> msg
    }
    -> Cmd msg
getRecherchesEnregistrees { sharedMsg, logError, msg } =
    get
        { url = Api.getEtablissementsFilters }
        { toShared = sharedMsg
        , logger = logError
        , handler = msg
        }
        (Decode.list decodeRechercheEnregistree)


doSaveRechercheEnregistree : String -> String -> Cmd Msg
doSaveRechercheEnregistree nom url =
    post
        { url = Api.getEtablissementsFilters
        , body =
            [ ( "nom", Encode.string nom )
            , ( "url", Encode.string url )
            ]
                |> Encode.object
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedSaveRechercheEnregistree
        }
        (Decode.list decodeRechercheEnregistree)


doDeleteRechercheEnregistree : RechercheEnregistree -> Cmd Msg
doDeleteRechercheEnregistree rechercheEnregistree =
    delete
        { url = Api.getEtablissementsFilters
        , body =
            [ ( "rechercheEnregistreeId", Encode.int rechercheEnregistree.id )
            ]
                |> Encode.object
                |> Http.jsonBody
        }
        { toShared = SharedMsg
        , logger = Just LogError
        , handler = ReceivedDeleteRechercheEnregistree
        }
        (Decode.list decodeRechercheEnregistree)


decodeRechercheEnregistree : Decoder RechercheEnregistree
decodeRechercheEnregistree =
    Decode.succeed RechercheEnregistree
        |> andMap (Decode.field "id" Decode.int)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "url" Decode.string)
        |> andMap (Decode.field "createdAt" <| Lib.Date.decodeCalendarDate)


rechercherEtablissementInputName : String
rechercherEtablissementInputName =
    "rechercher-etablissement"


setEtats : Filters -> Bool -> EtatAdministratifFiltre -> List EtatAdministratifFiltre
setEtats filters add etat =
    let
        etats =
            filters.etat

        isMember =
            List.member etat etats
    in
    case ( add, isMember ) of
        ( True, True ) ->
            etats

        ( True, False ) ->
            etats ++ [ etat ]

        ( False, True ) ->
            etats
                |> List.filter (\et -> et /= etat)

        ( False, False ) ->
            etats


setGeolocalisations : Filters -> Bool -> GeolocalisationFiltre -> List GeolocalisationFiltre
setGeolocalisations filters add geolocalisation =
    let
        geolocalisations =
            filters.geolocalisation

        isMember =
            List.member geolocalisation geolocalisations
    in
    case ( add, isMember ) of
        ( True, True ) ->
            geolocalisations

        ( True, False ) ->
            geolocalisations ++ [ geolocalisation ]

        ( False, True ) ->
            geolocalisations
                |> List.filter (\et -> et /= geolocalisation)

        ( False, False ) ->
            geolocalisations


setEntrepriseTypes : Filters -> Bool -> EntrepriseType -> List EntrepriseType
setEntrepriseTypes filters add entrepriseType =
    let
        entrepriseTypes =
            filters.entrepriseTypes

        isMember =
            List.member entrepriseType entrepriseTypes
    in
    case ( add, isMember ) of
        ( True, True ) ->
            entrepriseTypes

        ( True, False ) ->
            entrepriseTypes ++ [ entrepriseType ]

        ( False, True ) ->
            entrepriseTypes
                |> List.filter (\et -> et /= entrepriseType)

        ( False, False ) ->
            entrepriseTypes


setMicros : Filters -> Bool -> MicroEntreprise -> List MicroEntreprise
setMicros filters add micro =
    let
        micros =
            filters.micro

        isMember =
            List.member micro micros
    in
    case ( add, isMember ) of
        ( True, True ) ->
            micros

        ( True, False ) ->
            micros ++ [ micro ]

        ( False, True ) ->
            micros
                |> List.filter (\et -> et /= micro)

        ( False, False ) ->
            micros


setFavoris : Filters -> Bool -> Favori -> List Favori
setFavoris filters add favori =
    let
        favoris =
            filters.favori

        isMember =
            List.member favori favoris
    in
    case ( add, isMember ) of
        ( True, True ) ->
            favoris

        ( True, False ) ->
            favoris ++ [ favori ]

        ( False, True ) ->
            favoris
                |> List.filter (\et -> et /= favori)

        ( False, False ) ->
            favoris


setAvecContacts : Filters -> Bool -> AvecContact -> List AvecContact
setAvecContacts filters add avecContact =
    let
        avecContacts =
            filters.avecContact

        isMember =
            List.member avecContact avecContacts
    in
    case ( add, isMember ) of
        ( True, True ) ->
            avecContacts

        ( True, False ) ->
            avecContacts ++ [ avecContact ]

        ( False, True ) ->
            avecContacts
                |> List.filter (\et -> et /= avecContact)

        ( False, False ) ->
            avecContacts


setAncienCreateurs : Filters -> Bool -> AncienCreateur -> List AncienCreateur
setAncienCreateurs filters add ancienCreateur =
    let
        anciensCreateurs =
            filters.ancienCreateur

        isMember =
            List.member ancienCreateur anciensCreateurs
    in
    case ( add, isMember ) of
        ( True, True ) ->
            anciensCreateurs

        ( True, False ) ->
            anciensCreateurs ++ [ ancienCreateur ]

        ( False, True ) ->
            anciensCreateurs
                |> List.filter (\et -> et /= ancienCreateur)

        ( False, False ) ->
            anciensCreateurs


listFiltersTagsId : String
listFiltersTagsId =
    "liste-filters-tags"


updateUrl : { data | filters : Filters, pagination : Pagination } -> Effect.Effect Shared.Msg Msg
updateUrl { filters, pagination } =
    Effect.fromShared <|
        Shared.Navigate <|
            Route.Etablissements <|
                (\q ->
                    if q == "" then
                        Nothing

                    else
                        Just q
                )
                <|
                    filtersAndPaginationToQuery ( filters, pagination )


codeToOrderBy : String -> Msg
codeToOrderBy code =
    let
        match ob od () =
            if code == (orderByToString ob ++ orderDirectionToString od) then
                Just ( ob, od )

            else
                Nothing

        or : (() -> Maybe data) -> Maybe data -> Maybe data
        or orElse maybe =
            case maybe of
                Just m ->
                    Just m

                Nothing ->
                    orElse ()
    in
    match EtablissementConsultation Desc ()
        |> or (match EtablissementConsultation Asc)
        |> or (match EtablissementCreation Desc)
        |> or (match EtablissementCreation Asc)
        |> or (match EtablissementFermeture Desc)
        |> or (match EtablissementFermeture Asc)
        -- |> or (match Alphabetique Desc)
        -- |> or (match Alphabetique Asc)
        |> Maybe.withDefault ( EtablissementConsultation, Desc )
        |> (\( ob, od ) -> ClickedOrderBy ob od)


selectOrder : String -> { a | etat : List EtatAdministratifFiltre, orderBy : OrderBy, orderDirection : OrderDirection } -> Html Msg
selectOrder selectId { etat, orderBy, orderDirection } =
    let
        toOption ob od lab dis title =
            option
                [ Attr.value <| orderByToString ob ++ orderDirectionToString od
                , Attr.selected <| ( orderBy, orderDirection ) == ( ob, od )
                , Attr.disabled dis
                , title |> Maybe.map Attr.title |> Maybe.withDefault empty
                ]
                [ text lab ]

        ( fermeDisabled, fermeTitle ) =
            case etat of
                [ Ferme ] ->
                    ( False, Nothing )

                _ ->
                    ( True, Just "Vous devez appliquer le filtre sur l'état Fermé pour pouvoir faire ce tri" )
    in
    select
        [ class "fr-select"
        , Attr.id selectId
        , Attr.name "select-sortBy"
        , Events.onInput codeToOrderBy
        ]
        [ toOption EtablissementConsultation Desc "Derniers consultés" False Nothing

        -- , toOption EtablissementConsultation Asc "Établissements consultés récemment en dernier"
        -- , toOption EtablissementCreation Asc "Établissements créés récemment en dernier"
        -- , toOption Alphabetique Asc "Noms classés par ordre alphabétique"
        -- , toOption Alphabetique Desc "Noms classés par ordre alphabétique inversé"
        , toOption EtablissementCreation Desc "Derniers créés (base SIRENE)" False Nothing
        , toOption EtablissementFermeture Desc "Derniers fermés (base SIRENE)" fermeDisabled fermeTitle
        ]


forcePagination : a -> { b | pagination : a } -> { b | pagination : a }
forcePagination pagination model =
    { model | pagination = pagination }


doSearch : ( Filters, Pagination ) -> Model -> ( Model, Effect.Effect sharedMsg Msg )
doSearch ( filters, pagination ) model =
    let
        nextUrl =
            filtersAndPaginationToQuery ( filters, pagination )

        requestChanged =
            nextUrl /= model.currentUrl
    in
    if requestChanged then
        let
            communes =
                filters.communes

            epcis =
                filters.epcis

            departements =
                filters.departements

            tis =
                filters.tis

            qpvs =
                filters.qpvs

            exercicesIds =
                filters.exercices
                    |> List.map .id

            newExercices =
                Data.Exercice.tranchesExercices
                    |> List.filter (\{ id } -> List.member id exercicesIds)

            zrrsIds =
                filters.zrrs
                    |> List.map .id

            newZrrs =
                Data.ZRR.zrrs
                    |> List.filter (\{ id } -> List.member id zrrsIds)

            afrsIds =
                filters.afrs
                    |> List.map .id

            newAfrs =
                Data.AFR.afrs
                    |> List.filter (\{ id } -> List.member id afrsIds)

            currentApiStreet =
                model.filters.apiStreet

            apiStreet =
                case currentApiStreet of
                    Nothing ->
                        filters.apiStreet

                    Just current ->
                        filters.apiStreet
                            |> Maybe.andThen
                                (\adresse ->
                                    if adresse.street == current.street && adresse.postcode == current.postcode then
                                        Just current

                                    else
                                        filters.apiStreet
                                )

            ( updatedSelect, selectCmd ) =
                SingleSelectRemote.setText
                    (apiStreet
                        |> Maybe.map .label
                        |> Maybe.withDefault ""
                    )
                    selectAdresseConfig
                    model.selectAdresse
        in
        ( { model
            | pagination = pagination
            , filters =
                { filters
                    | communes = communes
                    , epcis = epcis
                    , departements = departements
                    , tis = tis
                    , qpvs = qpvs
                    , exercices = newExercices
                    , zrrs = newZrrs
                    , afrs = newAfrs
                    , apiStreet = apiStreet
                }
            , currentUrl = nextUrl
            , selectAdresse = updatedSelect
          }
        , Effect.fromCmd selectCmd
        )

    else
        ( model, Effect.none )


selecteurDate : { msg : Maybe Date -> msg, label : Html Never, name : String, value : Maybe Date, min : Maybe Date, max : Maybe Date, disabled : Bool } -> Html msg
selecteurDate { msg, label, name, value, min, max, disabled } =
    DSFR.Input.new
        { value = value |> Maybe.map Lib.Date.formatDateToYYYYMMDD |> Maybe.withDefault ""
        , onInput =
            Date.fromIsoString
                >> Result.toMaybe
                >> msg
        , label = label
        , name = name
        }
        |> DSFR.Input.withExtraAttrs [ class "!mb-0" ]
        |> DSFR.Input.withDisabled disabled
        |> DSFR.Input.date { min = Maybe.map Lib.Date.formatDateToYYYYMMDD min, max = Maybe.map Lib.Date.formatDateToYYYYMMDD max }
        |> DSFR.Input.view


hydrateFilters : FiltresRequete -> Filters -> Filters
hydrateFilters filtresRequete filters =
    { filters
        | communes = filtresRequete.communes
        , epcis = filtresRequete.epcis
        , departements = filtresRequete.departements
        , regions = filtresRequete.regions
        , tis = filtresRequete.territoireIndustries
        , qpvs = filtresRequete.qpvs
        , codesNaf = filtresRequete.nafTypes
        , categoriesJuridiques = filtresRequete.categoriesJuridiques
        , filiales = filtresRequete.filiales
        , detentions = filtresRequete.detentions
    }


setFiltersRequete : FiltresRequete -> Model -> Model
setFiltersRequete filtresRequete model =
    { model
        | filtresRequete = filtresRequete
        , filters = hydrateFilters filtresRequete model.filters
    }


type alias FiltresRequete =
    { qpvs : List Qpv
    , communes : List Commune
    , epcis : List Epci
    , departements : List Departement
    , regions : List Region
    , territoireIndustries : List TerritoireIndustrie
    , nafTypes : List CodeNaf
    , categoriesJuridiques : List CategorieJuridique
    , filiales : List EntrepriseApercu
    , detentions : List EntrepriseApercu
    }


defaultFiltresRequete : FiltresRequete
defaultFiltresRequete =
    { qpvs = []
    , communes = []
    , epcis = []
    , departements = []
    , regions = []
    , territoireIndustries = []
    , nafTypes = []
    , categoriesJuridiques = []
    , filiales = []
    , detentions = []
    }


decodeFiltresRequete : Decoder FiltresRequete
decodeFiltresRequete =
    Decode.succeed FiltresRequete
        |> andMap
            (Decode.field "qpvs" <|
                Decode.list <|
                    Decode.map2 Qpv
                        (Decode.field "id" Decode.string)
                        (Decode.field "nom" Decode.string)
            )
        |> andMap
            (Decode.field "communes" <|
                Decode.list <|
                    decodeCommune
            )
        |> andMap
            (Decode.field "epcis" <|
                Decode.list <|
                    Decode.map2 Epci
                        (Decode.field "id" Decode.string)
                        (Decode.field "nom" Decode.string)
            )
        |> andMap
            (Decode.field "departements" <|
                Decode.list <|
                    Decode.map2 Departement
                        (Decode.field "id" Decode.string)
                        (Decode.field "nom" Decode.string)
            )
        |> andMap
            (Decode.field "regions" <|
                Decode.list <|
                    Decode.map2 Region
                        (Decode.field "id" Decode.string)
                        (Decode.field "nom" Decode.string)
            )
        |> andMap
            (Decode.field "territoireIndustries" <|
                Decode.list <|
                    Decode.map2 TerritoireIndustrie
                        (Decode.field "id" Decode.string)
                        (Decode.field "nom" Decode.string)
            )
        |> andMap
            (Decode.field "nafTypes"
                (Decode.list <|
                    Data.Naf.decodeCodeNaf
                )
            )
        |> andMap
            (Decode.field "catJurTypes"
                (Decode.list <|
                    Data.CategorieJuridique.decodeCategorieJuridique
                )
            )
        |> andMap
            (Decode.field "filiales" <|
                Decode.list <|
                    Decode.map2 EntrepriseApercu
                        (Decode.field "id" Decode.string)
                        (Decode.field "nomAffichage" Decode.string)
            )
        |> andMap
            (Decode.field "detentions" <|
                Decode.list <|
                    Decode.map2 EntrepriseApercu
                        (Decode.field "id" Decode.string)
                        (Decode.field "nomAffichage" Decode.string)
            )


changeSituationGeographique : SituationGeographique -> Filters -> Filters
changeSituationGeographique situationGeographique filters =
    { filters
        | situationGeographique = situationGeographique
    }


pointToString : ( Float, Float ) -> String
pointToString ( lon, lat ) =
    [ String.fromFloat lon
    , String.fromFloat lat
    ]
        |> String.join ", "


resetModals : Model -> Model
resetModals model =
    { model
        | saveRechercheEnregistree = Nothing
    }
