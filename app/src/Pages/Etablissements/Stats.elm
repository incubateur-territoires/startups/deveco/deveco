module Pages.Etablissements.Stats exposing (TDBStats, decodeTDBStats, emptyTDBStats, viewStats)

import Accessibility exposing (Html, div, h2, hr, span, sup, text, ul)
import DSFR.Grid as Grid
import DSFR.Icons
import DSFR.Icons.System
import DSFR.Typography as Typo
import Html
import Html.Attributes as Attr exposing (class)
import Html.Lazy
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode
import Lib.UI exposing (formatFloatWithThousandSpacing, formatIntWithThousandSpacing)
import Pages.Etablissements.Filters exposing (queryKeys)
import RemoteData as RD exposing (WebData)


type alias TDBStats =
    { etablissements : EtablissementsStats
    , nafs : List SiegeStats
    , nafsEffectifs : List SiegeStats
    , nafsCas : List SiegeStats
    , categories : List SiegeStats
    , categoriesEffectifs : List SiegeStats
    , categoriesCas : List SiegeStats
    , qpvs : List SiegeStats
    , qpvsEffectifs : List SiegeStats
    , qpvsCas : List SiegeStats
    , creations : List SiegeStats
    , fermetures : List SiegeStats
    , effectifs : List SiegeStats
    , cas : List SiegeStats
    , subventionsAnnees : List SiegeStats
    , communes : List SiegeStats
    , epcis : List SiegeStats
    , departements : List SiegeStats
    , regions : List SiegeStats
    }


emptyTDBStats : TDBStats
emptyTDBStats =
    { etablissements =
        { total = 0
        , entreprises = 0
        , sieges = 0
        , actifs = 0
        , actifsSieges = 0
        , avecCasSieges = 0
        , avecEffectifs = 0
        , enQpv = 0
        , enQpvSieges = 0
        , enQpvAvecEffectifs = 0
        , enQpvAvecCas = 0
        , avecSubvention = 0
        , avecSubventionSieges = 0
        , ess = 0
        , essSieges = 0
        , micro = 0
        , microSieges = 0
        , caTotalSieges = 0
        , caMedianSieges = 0
        , effectifs = 0
        , effectifsMedian = 0
        , subventions = 0
        }
    , nafs = []
    , nafsEffectifs = []
    , nafsCas = []
    , categories = []
    , categoriesEffectifs = []
    , categoriesCas = []
    , qpvs = []
    , qpvsEffectifs = []
    , qpvsCas = []
    , creations = []
    , fermetures = []
    , effectifs = []
    , cas = []
    , subventionsAnnees = []
    , communes = []
    , epcis = []
    , departements = []
    , regions = []
    }


type alias EtablissementsStats =
    { total : Int
    , entreprises : Int
    , sieges : Int
    , actifs : Int
    , actifsSieges : Int
    , avecCasSieges : Int
    , avecEffectifs : Int
    , enQpv : Int
    , enQpvSieges : Int
    , enQpvAvecEffectifs : Int
    , enQpvAvecCas : Int
    , avecSubvention : Int
    , avecSubventionSieges : Int
    , ess : Int
    , essSieges : Int
    , micro : Int
    , microSieges : Int
    , caTotalSieges : Int
    , caMedianSieges : Float
    , effectifs : Int
    , effectifsMedian : Float
    , subventions : Int
    }


type alias SiegeStats =
    { label : String
    , count : Int
    , value : Maybe Int
    , countSieges : Maybe Int
    , valueSieges : Maybe Int
    , cohorte : Int
    , id : String
    }


viewStats : Bool -> WebData TDBStats -> Html msg
viewStats full stats =
    div [ class "flex flex-col gap-4" ]
        [ div [ Grid.gridRow, class "p-2 sm:p-4" ]
            [ div [ Grid.col12 ]
                [ case stats of
                    RD.Success data ->
                        div []
                            [ div [ Grid.gridRowGutters, Grid.gridRow ]
                                [ Html.Lazy.lazy2 chiffresGlobaux full data.etablissements
                                ]
                            , hr [ class "fr-hr h-[1px] w-full !border-solid dark-blue-border !border-2 !p-0 !my-4" ] []
                            , div [ Grid.gridRowGutters, Grid.gridRow ]
                                [ Html.Lazy.lazy2 etablissementsParCodeNaf full data.nafs
                                , Html.Lazy.lazy3 effectifParCodeNaf full data.etablissements data.nafsEffectifs
                                , Html.Lazy.lazy3 caParCodeNaf full data.etablissements data.nafsCas
                                ]
                            , hr [ class "fr-hr h-[1px] w-full !border-solid dark-blue-border !border-2 !p-0 !my-4" ] []
                            , div [ Grid.gridRowGutters, Grid.gridRow ]
                                [ Html.Lazy.lazy3 etablissementsParTrancheDEffectif full data.etablissements data.effectifs
                                , Html.Lazy.lazy3 siegesParChiffreDAffaire full data.etablissements data.cas
                                , Html.Lazy.lazy3 subventionsParAnnees full data.etablissements data.subventionsAnnees
                                ]
                            , hr [ class "fr-hr h-[1px] w-full !border-solid dark-blue-border !border-2 !p-0 !my-4" ] []
                            , div [ Grid.gridRowGutters, Grid.gridRow ]
                                [ Html.Lazy.lazy3 comparaisonCreationsFermeturesParAnnee full data.creations data.fermetures
                                , Html.Lazy.lazy3 creationsNettesParAnnee full data.creations data.fermetures
                                ]
                            , hr [ class "fr-hr h-[1px] w-full !border-solid dark-blue-border !border-2 !p-0 !my-4" ] []
                            , div [ Grid.gridRowGutters, Grid.gridRow ]
                                [ Html.Lazy.lazy2 etablissementsParCategorieJuridique full data.categories
                                , Html.Lazy.lazy3 effectifParCategorieJuridique full data.etablissements data.categoriesEffectifs
                                , Html.Lazy.lazy3 caParCategorieJuridique full data.etablissements data.categoriesCas
                                ]
                            , hr [ class "fr-hr h-[1px] w-full !border-solid dark-blue-border !border-2 !p-0 !my-4" ] []
                            , div [ Grid.gridRowGutters, Grid.gridRow ]
                                [ Html.Lazy.lazy3 etablissementsParQuelqueChose full "Établissements par commune" data.communes
                                , Html.Lazy.lazy3 etablissementsParQuelqueChose full "Établissements par EPCI" data.epcis
                                , Html.Lazy.lazy3 etablissementsParQuelqueChose full "Établissements par département" data.departements
                                , Html.Lazy.lazy3 etablissementsParQuelqueChose full "Établissements par région" data.regions
                                ]
                            , hr [ class "fr-hr h-[1px] w-full !border-solid dark-blue-border !border-2 !p-0 !my-4" ] []
                            , div [ Grid.gridRowGutters, Grid.gridRow ]
                                [ Html.Lazy.lazy3 etablissementsParQPV full data.etablissements data.qpvs
                                , Html.Lazy.lazy3 effectifParQPV full data.etablissements data.qpvsEffectifs
                                , Html.Lazy.lazy3 caParQPV full data.etablissements data.qpvsCas
                                ]
                            ]

                    RD.NotAsked ->
                        div [ class "flex flex-col gap-4 p-4 fr-card--white" ]
                            [ text "Aucune recherche sélectionnée." ]

                    RD.Loading ->
                        div [ class "flex flex-col gap-4 p-4 fr-card--white" ]
                            [ text "Chargement en cours..." ]

                    RD.Failure _ ->
                        div [ class "flex flex-col gap-4 p-4 fr-card--white" ]
                            [ text "Une erreur s'est produite, veuillez réessayer."
                            ]
                ]
            ]
        ]


chiffresGlobaux : Bool -> EtablissementsStats -> Html msg
chiffresGlobaux full etablissementsStats =
    div [ Grid.col12 ] <|
        List.singleton <|
            div [ class "flex flex-col gap-4 p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text "Chiffres globaux" ]
                , div [ Grid.gridRow, Grid.gridRowGutters ] <|
                    [ div (class "flex flex-col gap-4" :: cols full)
                        [ voirStat "Établissements" .total etablissementsStats
                        , div []
                            [ text "dont\u{00A0}:"
                            , ul []
                                [ Html.li [] <| List.singleton <| voirStat ("siège" ++ (Lib.UI.plural <| .sieges <| etablissementsStats)) .sieges etablissementsStats
                                , Html.li [] <| List.singleton <| voirStatAvecSieges ("actif" ++ (Lib.UI.plural <| .actifs <| etablissementsStats)) .actifs .actifsSieges etablissementsStats
                                , Html.li [] <| List.singleton <| voirStat "avec CA disponible" .avecCasSieges etablissementsStats
                                , Html.li [] <| List.singleton <| voirStat "avec effectifs disponibles" .avecEffectifs etablissementsStats
                                , Html.li [] <| List.singleton <| voirStatAvecSieges "en QPV" .enQpv .enQpvSieges etablissementsStats
                                , Html.li [] <| List.singleton <| voirStatAvecSieges "dans l'ESS" .ess .essSieges etablissementsStats
                                , Html.li [] <| List.singleton <| voirStatAvecSieges "micro-entreprises" .micro .microSieges etablissementsStats
                                , Html.li [] <| List.singleton <| voirStat ("subventionné" ++ (Lib.UI.plural <| .avecSubvention <| etablissementsStats)) .avecSubvention etablissementsStats
                                ]
                            ]
                        , voirStat "Entreprises" .entreprises etablissementsStats
                        ]
                    , div (class "flex flex-col gap-4" :: cols full)
                        [ div []
                            [ text "Total des effectifs moyens annuels"
                            , text "\u{00A0}: "
                            , span [ Typo.textBold ] [ text <| formatIntWithThousandSpacing <| .effectifs <| etablissementsStats ]
                            ]
                        , div []
                            [ text "Médiane des effectifs moyens annuels"
                            , text "\u{00A0}: "
                            , span [ Typo.textBold ] [ text <| String.fromFloat <| .effectifsMedian <| etablissementsStats ]
                            ]
                        , div []
                            [ text "CA total des sièges"
                            , text "\u{00A0}: "
                            , span [ Typo.textBold ] [ text <| formatIntWithThousandSpacing <| .caTotalSieges <| etablissementsStats, text " €" ]
                            ]
                        , div []
                            [ text "CA médian des sièges"
                            , text "\u{00A0}: "
                            , span [ Typo.textBold ] [ text <| formatFloatWithThousandSpacing <| .caMedianSieges <| etablissementsStats, text " €" ]
                            ]
                        , div []
                            [ text "Subventions perçues"
                            , sup [ Attr.title "Total des subventions perçues, de 2020 à 2023" ]
                                [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ]
                                ]
                            , text "\u{00A0}: "
                            , span [ Typo.textBold ] [ text <| formatIntWithThousandSpacing <| .subventions <| etablissementsStats, text " €" ]
                            ]
                        ]
                    ]
                ]


voirStat : String -> (EtablissementsStats -> Int) -> EtablissementsStats -> Html msg
voirStat title accessor stats =
    div []
        [ text title
        , text "\u{00A0}: "
        , span [ Typo.textBold ]
            [ text <|
                formatIntWithThousandSpacing <|
                    accessor <|
                        stats
            ]
        ]


voirStatAvecSieges : String -> (EtablissementsStats -> Int) -> (EtablissementsStats -> Int) -> EtablissementsStats -> Html msg
voirStatAvecSieges title accessor getSieges stats =
    div []
        [ text title
        , text "\u{00A0}: "
        , span [ Typo.textBold ]
            [ text <|
                formatIntWithThousandSpacing <|
                    accessor <|
                        stats
            ]
        , text " (dont "
        , span [ Typo.textBold ]
            [ text <|
                formatIntWithThousandSpacing <|
                    getSieges <|
                        stats
            ]
        , text " siège"
        , text <|
            Lib.UI.plural <|
                getSieges <|
                    stats
        , text ")"
        ]


etablissementsParCodeNaf : Bool -> List SiegeStats -> Html msg
etablissementsParCodeNaf full nafs =
    let
        title =
            "Établissements par code NAF"

        subtitle =
            "limité aux 10 premiers codes NAF"
    in
    div (cols full) <|
        List.singleton <|
            div [ class "flex flex-col p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                , span [ class "!mb-0 italic" ] [ text subtitle ]
                , case nafs of
                    [] ->
                        aucunResultat

                    _ ->
                        Html.node "chart-bar-grouped"
                            [ Encode.object
                                [ ( "values"
                                  , nafs
                                        |> Encode.list
                                            (\{ label, count, countSieges } ->
                                                Encode.object
                                                    [ ( "label", Encode.string label )
                                                    , ( "count", Encode.int count )
                                                    , ( "subCount", Encode.int <| Maybe.withDefault 0 <| countSieges )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "title", Encode.string "Total" )
                                        , ( "titleSub", Encode.string "Sièges" )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Attr.attribute "data"
                            ]
                            []
                ]


etablissementsParCategorieJuridique : Bool -> List SiegeStats -> Html msg
etablissementsParCategorieJuridique full categories =
    let
        title =
            "Établissements par catégorie juridique"
    in
    div (cols full) <|
        List.singleton <|
            div [ class "flex flex-col p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                , case categories of
                    [] ->
                        aucunResultat

                    _ ->
                        Html.node "chart-bar-grouped"
                            [ Encode.object
                                [ ( "values"
                                  , categories
                                        |> Encode.list
                                            (\{ label, count, countSieges } ->
                                                Encode.object
                                                    [ ( "label", Encode.string label )
                                                    , ( "count", Encode.int count )
                                                    , ( "subCount", Encode.int <| Maybe.withDefault 0 <| countSieges )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "title", Encode.string "Total" )
                                        , ( "titleSub", Encode.string "Sièges" )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Attr.attribute "data"
                            ]
                            []
                ]


effectifParCategorieJuridique : Bool -> EtablissementsStats -> List SiegeStats -> Html msg
effectifParCategorieJuridique full etablissementsStats categoriesEffectifs =
    let
        title =
            "Effectifs par catégorie juridique"

        subtitle =
            "limité aux 10 premiers catégories juridiques"
    in
    div (cols full) <|
        List.singleton <|
            div [ class "flex flex-col p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                , span [ class "!mb-0 italic" ] [ text subtitle ]
                , span [ class "!mb-0 italic" ]
                    [ text ""
                    , text <|
                        formatIntWithThousandSpacing <|
                            .avecEffectifs <|
                                etablissementsStats
                    , text " établissement"
                    , text <|
                        Lib.UI.plural <|
                            .avecEffectifs <|
                                etablissementsStats
                    , text " avec effectifs sur "
                    , text <|
                        formatIntWithThousandSpacing <|
                            .total <|
                                etablissementsStats
                    , text " au total"
                    ]
                , case categoriesEffectifs of
                    [] ->
                        aucunResultat

                    _ ->
                        Html.node "chart-bar"
                            [ Encode.object
                                [ ( "values"
                                  , categoriesEffectifs
                                        |> Encode.list
                                            (\{ label, value, count } ->
                                                Encode.object
                                                    [ ( "label", Encode.string label )
                                                    , ( "count", Encode.int <| Maybe.withDefault 0 <| value )
                                                    , ( "population", Encode.int count )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "titleSub", Encode.string "Sièges" )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Attr.attribute "data"
                            ]
                            []
                ]


caParCategorieJuridique : Bool -> EtablissementsStats -> List SiegeStats -> Html msg
caParCategorieJuridique full etablissementsStats categoriesCas =
    let
        title =
            "CA des sièges par catégorie juridique"

        subtitle =
            "limité aux 10 premiers catégories juridiques"
    in
    div (cols full) <|
        List.singleton <|
            div [ class "flex flex-col p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                , span [ class "!mb-0 italic" ] [ text subtitle ]
                , span [ class "!mb-0 italic" ]
                    [ text ""
                    , text <|
                        formatIntWithThousandSpacing <|
                            .avecCasSieges <|
                                etablissementsStats
                    , text " siège"
                    , text <|
                        Lib.UI.plural <|
                            .avecCasSieges <|
                                etablissementsStats
                    , text " avec CA sur "
                    , text <|
                        formatIntWithThousandSpacing <|
                            .total <|
                                etablissementsStats
                    , text " au total"
                    ]
                , case categoriesCas of
                    [] ->
                        aucunResultat

                    _ ->
                        Html.node "chart-bar"
                            [ Encode.object
                                [ ( "values"
                                  , categoriesCas
                                        |> Encode.list
                                            (\{ label, value, count, id } ->
                                                Encode.object
                                                    [ ( "label", Encode.string label )
                                                    , ( "count", Encode.int <| Maybe.withDefault 0 <| value )
                                                    , ( "population", Encode.int count )
                                                    , ( "filterValue", Encode.string id )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "titleSub", Encode.string "Sièges" )
                                        , ( "filterKey", Encode.string <| queryKeys.categoriesJuridiques )
                                        , ( "defaultFilterKeys", Encode.list Encode.string [ "siege" ] )
                                        , ( "defaultFilterValues", Encode.list Encode.string [ "true" ] )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Attr.attribute "data"
                            ]
                            []
                ]


etablissementsParQPV : Bool -> EtablissementsStats -> List SiegeStats -> Html msg
etablissementsParQPV full etablissementsStats qpvs =
    let
        title =
            "Établissements par QPV"

        subtitle =
            "uniquement les établissements en QPV"
    in
    div (cols full) <|
        List.singleton <|
            div [ class "flex flex-col p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                , span [ class "!mb-0 italic" ] [ text subtitle ]
                , span [ class "!mb-0 italic" ]
                    [ text ""
                    , text <|
                        formatIntWithThousandSpacing <|
                            .enQpv <|
                                etablissementsStats
                    , text " établissement"
                    , text <|
                        Lib.UI.plural <|
                            .enQpv <|
                                etablissementsStats
                    , text " en QPV sur "
                    , text <|
                        formatIntWithThousandSpacing <|
                            .total <|
                                etablissementsStats
                    , text " au total"
                    ]
                , case qpvs of
                    [] ->
                        aucunResultat

                    _ ->
                        Html.node "chart-bar-grouped"
                            [ Encode.object
                                [ ( "values"
                                  , qpvs
                                        |> Encode.list
                                            (\{ label, count, countSieges } ->
                                                Encode.object
                                                    [ ( "label", Encode.string label )
                                                    , ( "count", Encode.int count )
                                                    , ( "subCount", Encode.int <| Maybe.withDefault 0 <| countSieges )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "title", Encode.string "Total" )
                                        , ( "titleSub", Encode.string "Sièges" )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Attr.attribute "data"
                            ]
                            []
                ]


effectifParQPV : Bool -> EtablissementsStats -> List SiegeStats -> Html msg
effectifParQPV full etablissementsStats qpvsEffectifs =
    let
        title =
            "Effectifs par QPV"

        subtitle =
            "limité aux 10 premiers QPV"
    in
    div (cols full) <|
        List.singleton <|
            div [ class "flex flex-col p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                , span [ class "!mb-0 italic" ] [ text subtitle ]
                , span [ class "!mb-0 italic" ]
                    [ text ""
                    , text <|
                        formatIntWithThousandSpacing <|
                            .enQpvAvecEffectifs <|
                                etablissementsStats
                    , text " établissement"
                    , text <|
                        Lib.UI.plural <|
                            .enQpvAvecEffectifs <|
                                etablissementsStats
                    , text " avec effectifs sur "
                    , text <|
                        formatIntWithThousandSpacing <|
                            .enQpv <|
                                etablissementsStats
                    , text " en QPV"
                    ]
                , case qpvsEffectifs of
                    [] ->
                        aucunResultat

                    _ ->
                        Html.node "chart-bar"
                            [ Encode.object
                                [ ( "values"
                                  , qpvsEffectifs
                                        |> Encode.list
                                            (\{ label, value, count } ->
                                                Encode.object
                                                    [ ( "label", Encode.string label )
                                                    , ( "count", Encode.int <| Maybe.withDefault 0 <| value )
                                                    , ( "population", Encode.int count )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "titleSub", Encode.string "Sièges" )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Attr.attribute "data"
                            ]
                            []
                ]


caParQPV : Bool -> EtablissementsStats -> List SiegeStats -> Html msg
caParQPV full etablissementsStats qpvsCas =
    let
        title =
            "CA des sièges par QPV"

        subtitle =
            "limité aux 10 premiers QPV"
    in
    div (cols full) <|
        List.singleton <|
            div [ class "flex flex-col p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                , span [ class "!mb-0 italic" ] [ text subtitle ]
                , span [ class "!mb-0 italic" ]
                    [ text ""
                    , text <|
                        formatIntWithThousandSpacing <|
                            .enQpvAvecCas <|
                                etablissementsStats
                    , text " établissement"
                    , text <|
                        Lib.UI.plural <|
                            .enQpvAvecCas <|
                                etablissementsStats
                    , text " avec CA sur "
                    , text <|
                        formatIntWithThousandSpacing <|
                            .enQpv <|
                                etablissementsStats
                    , text " en QPV"
                    ]
                , case qpvsCas of
                    [] ->
                        aucunResultat

                    _ ->
                        Html.node "chart-bar"
                            [ Encode.object
                                [ ( "values"
                                  , qpvsCas
                                        |> Encode.list
                                            (\{ label, value, count } ->
                                                Encode.object
                                                    [ ( "label", Encode.string label )
                                                    , ( "count", Encode.int <| Maybe.withDefault 0 <| value )
                                                    , ( "population", Encode.int count )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "titleSub", Encode.string "Sièges" )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Attr.attribute "data"
                            ]
                            []
                ]


subventionsParAnnees : Bool -> EtablissementsStats -> List SiegeStats -> Html msg
subventionsParAnnees full etablissementsStats subventionsAnnees =
    let
        title =
            "Subventions par années"

        subtitle =
            ""
    in
    div (cols full) <|
        List.singleton <|
            div [ class "flex flex-col p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                , span [ class "!mb-0 italic" ] [ text subtitle ]
                , span [ class "!mb-0 italic" ]
                    [ text ""
                    , text <|
                        formatIntWithThousandSpacing <|
                            .avecSubvention <|
                                etablissementsStats
                    , text " établissement"
                    , text <|
                        Lib.UI.plural <|
                            .avecSubvention <|
                                etablissementsStats
                    , text " subventionné"
                    , text <|
                        Lib.UI.plural <|
                            .avecSubvention <|
                                etablissementsStats
                    , text " sur "
                    , text <|
                        formatIntWithThousandSpacing <|
                            .total <|
                                etablissementsStats
                    , text " au total"
                    ]
                , case subventionsAnnees of
                    [] ->
                        aucunResultat

                    _ ->
                        Html.node "chart-column"
                            [ Encode.object
                                [ ( "values"
                                  , subventionsAnnees
                                        |> Encode.list
                                            (\{ label, value, count } ->
                                                Encode.object
                                                    [ ( "label", Encode.string label )
                                                    , ( "count", Encode.int <| Maybe.withDefault 0 <| value )
                                                    , ( "population", Encode.int count )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "titleSub", Encode.string "Sièges" )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Attr.attribute "data"
                            ]
                            []
                ]


etablissementsParTrancheDEffectif : Bool -> EtablissementsStats -> List SiegeStats -> Html msg
etablissementsParTrancheDEffectif full etablissementsStats effectifs =
    let
        title =
            "Établissements par tranche d'effectifs"

        subtitle =
            ""

        totalSieges =
            effectifs
                |> List.foldl (\{ count } total -> count + total) 0
    in
    div (cols full) <|
        List.singleton <|
            div [ class "flex flex-col p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                , span [ class "!mb-0 italic" ] [ text subtitle ]
                , span [ class "!mb-0 italic" ]
                    [ text ""
                    , text <|
                        formatIntWithThousandSpacing <|
                            .avecEffectifs <|
                                etablissementsStats
                    , text " établissement"
                    , text <|
                        Lib.UI.plural <|
                            .avecEffectifs <|
                                etablissementsStats
                    , text " avec effectifs sur "
                    , text <|
                        formatIntWithThousandSpacing <|
                            .total <|
                                etablissementsStats
                    , text " au total"
                    ]
                , case totalSieges of
                    0 ->
                        aucunResultat

                    _ ->
                        Html.node "chart-bar-grouped"
                            [ Encode.object
                                [ ( "values"
                                  , effectifs
                                        |> Encode.list
                                            (\{ label, count, countSieges } ->
                                                Encode.object
                                                    [ ( "label", Encode.string label )
                                                    , ( "count", Encode.int count )
                                                    , ( "subCount", Encode.int <| Maybe.withDefault 0 <| countSieges )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "title", Encode.string "Total" )
                                        , ( "titleSub", Encode.string "Sièges" )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Attr.attribute "data"
                            ]
                            []
                ]


siegesParChiffreDAffaire : Bool -> EtablissementsStats -> List SiegeStats -> Html msg
siegesParChiffreDAffaire full etablissementsStats cas =
    let
        title =
            "Sièges par tranche de CA"

        subtitle =
            ""

        totalSieges =
            cas
                |> List.foldl (\{ count } total -> count + total) 0
    in
    div (cols full) <|
        List.singleton <|
            div [ class "flex flex-col p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                , span [ class "!mb-0 italic" ] [ text subtitle ]
                , span [ class "!mb-0 italic" ]
                    [ text ""
                    , text <|
                        formatIntWithThousandSpacing <|
                            .avecCasSieges <|
                                etablissementsStats
                    , text " siège"
                    , text <|
                        Lib.UI.plural <|
                            .avecCasSieges <|
                                etablissementsStats
                    , text " avec CA sur "
                    , text <|
                        formatIntWithThousandSpacing <|
                            .total <|
                                etablissementsStats
                    , text " au total"
                    ]
                , span [ Typo.textSm, class "!mb-0 italic" ] [ text "(cliquer sur les barres pour lancer une recherche)" ]
                , case totalSieges of
                    0 ->
                        aucunResultat

                    _ ->
                        Html.node "chart-bar"
                            [ Encode.object
                                [ ( "values"
                                  , cas
                                        |> Encode.list
                                            (\{ label, count, id } ->
                                                Encode.object
                                                    [ ( "label", Encode.string label )
                                                    , ( "count", Encode.int count )
                                                    , ( "filterValue", Encode.string id )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "titleSub", Encode.string "Sièges" )
                                        , ( "filterKey", Encode.string <| queryKeys.exercices )
                                        , ( "defaultFilterKeys", Encode.list Encode.string [ "siege" ] )
                                        , ( "defaultFilterValues", Encode.list Encode.string [ "true" ] )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Attr.attribute "data"
                            ]
                            []
                ]


effectifParCodeNaf : Bool -> EtablissementsStats -> List SiegeStats -> Html msg
effectifParCodeNaf full etablissementsStats nafsEffectifs =
    let
        title =
            "Effectifs par code NAF"

        subtitle =
            "limité aux 10 premiers codes NAF"
    in
    div (cols full) <|
        List.singleton <|
            div [ class "flex flex-col p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                , span [ class "!mb-0 italic" ] [ text subtitle ]
                , span [ class "!mb-0 italic" ]
                    [ text ""
                    , text <|
                        formatIntWithThousandSpacing <|
                            .avecEffectifs <|
                                etablissementsStats
                    , text " établissement"
                    , text <|
                        Lib.UI.plural <|
                            .sieges <|
                                etablissementsStats
                    , text " avec effectifs sur "
                    , text <|
                        formatIntWithThousandSpacing <|
                            .total <|
                                etablissementsStats
                    , text " au total"
                    ]
                , case nafsEffectifs of
                    [] ->
                        aucunResultat

                    _ ->
                        Html.node "chart-bar-grouped"
                            [ Encode.object
                                [ ( "values"
                                  , nafsEffectifs
                                        |> Encode.list
                                            (\{ label, value, valueSieges } ->
                                                Encode.object
                                                    [ ( "label", Encode.string label )
                                                    , ( "count", Encode.int <| Maybe.withDefault 0 <| value )
                                                    , ( "subCount", Encode.int <| Maybe.withDefault 0 <| valueSieges )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "title", Encode.string "Total" )
                                        , ( "titleSub", Encode.string "Sièges" )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Attr.attribute "data"
                            ]
                            []
                ]


caParCodeNaf : Bool -> EtablissementsStats -> List SiegeStats -> Html msg
caParCodeNaf full etablissementsStats nafsCas =
    let
        title =
            "CA des sièges par code NAF"

        subtitle =
            "limité aux 10 premiers codes NAF"
    in
    div (cols full) <|
        List.singleton <|
            div [ class "flex flex-col p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                , span [ class "!mb-0 italic" ] [ text subtitle ]
                , span [ class "!mb-0 italic" ]
                    [ text ""
                    , text <|
                        formatIntWithThousandSpacing <|
                            .avecCasSieges <|
                                etablissementsStats
                    , text " siège"
                    , text <|
                        Lib.UI.plural <|
                            .avecCasSieges <|
                                etablissementsStats
                    , text " avec CA sur "
                    , text <|
                        formatIntWithThousandSpacing <|
                            .total <|
                                etablissementsStats
                    , text " au total"
                    ]
                , case nafsCas of
                    [] ->
                        aucunResultat

                    _ ->
                        Html.node "chart-bar"
                            [ Encode.object
                                [ ( "values"
                                  , nafsCas
                                        |> Encode.list
                                            (\{ label, count, value } ->
                                                Encode.object
                                                    [ ( "label", Encode.string label )
                                                    , ( "count"
                                                      , Encode.int <|
                                                            Maybe.withDefault 0 <|
                                                                value
                                                      )
                                                    , ( "population", Encode.int count )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "title", Encode.string "Total" )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Attr.attribute "data"
                            ]
                            []
                ]


comparaisonCreationsFermeturesParAnnee : Bool -> List SiegeStats -> List SiegeStats -> Html msg
comparaisonCreationsFermeturesParAnnee full creations fermetures =
    let
        title =
            "Créations et fermetures par année"

        subtitle =
            "sur les 10 dernières années"
    in
    div (cols full) <|
        List.singleton <|
            div [ class "flex flex-col p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                , span [ class "!mb-0 italic" ] [ text subtitle ]
                , case creations of
                    [] ->
                        aucunResultat

                    _ ->
                        Html.node "chart-column-comparison"
                            [ Encode.object
                                [ ( "values"
                                  , [ "2015"
                                    , "2016"
                                    , "2017"
                                    , "2018"
                                    , "2019"
                                    , "2020"
                                    , "2021"
                                    , "2022"
                                    , "2023"
                                    , "2024"
                                    ]
                                        |> List.map
                                            (\l ->
                                                let
                                                    creation =
                                                        creations
                                                            |> List.filter (\{ label } -> label == l)
                                                            |> List.head
                                                            |> Maybe.map .count
                                                            |> Maybe.withDefault 0

                                                    fermeture =
                                                        fermetures
                                                            |> List.filter (\{ label } -> label == l)
                                                            |> List.head
                                                            |> Maybe.map .count
                                                            |> Maybe.withDefault 0
                                                in
                                                { label = l, creation = creation, fermeture = fermeture }
                                            )
                                        |> Encode.list
                                            (\{ label, creation, fermeture } ->
                                                Encode.object
                                                    [ ( "label", Encode.string label )
                                                    , ( "count", Encode.int <| creation )
                                                    , ( "countSub", Encode.int <| fermeture )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "title", Encode.string "Créations" )
                                        , ( "titleSub", Encode.string "Fermetures" )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Attr.attribute "data"
                            ]
                            []
                ]


creationsNettesParAnnee : Bool -> List SiegeStats -> List SiegeStats -> Html msg
creationsNettesParAnnee full creations fermetures =
    let
        title =
            "Créations nettes par année"

        subtitle =
            "sur les 10 dernières années"
    in
    div (cols full) <|
        List.singleton <|
            div [ class "flex flex-col p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                , span [ class "!mb-0 italic" ] [ text subtitle ]
                , case fermetures of
                    [] ->
                        aucunResultat

                    _ ->
                        Html.node "chart-column-negative"
                            [ Encode.object
                                [ ( "values"
                                  , [ "2015"
                                    , "2016"
                                    , "2017"
                                    , "2018"
                                    , "2019"
                                    , "2020"
                                    , "2021"
                                    , "2022"
                                    , "2023"
                                    , "2024"
                                    ]
                                        |> List.map
                                            (\l ->
                                                let
                                                    creation =
                                                        creations
                                                            |> List.filter (\{ label } -> label == l)
                                                            |> List.head
                                                            |> Maybe.map .count
                                                            |> Maybe.withDefault 0

                                                    fermeture =
                                                        fermetures
                                                            |> List.filter (\{ label } -> label == l)
                                                            |> List.head
                                                            |> Maybe.map .count
                                                            |> Maybe.withDefault 0
                                                in
                                                { label = l, count = creation - fermeture }
                                            )
                                        |> Encode.list
                                            (\{ label, count } ->
                                                Encode.object
                                                    [ ( "label", Encode.string label )
                                                    , ( "count", Encode.int <| count )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "title", Encode.string "Créations nettes" )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Attr.attribute "data"
                            ]
                            []
                ]


etablissementsParQuelqueChose : Bool -> String -> List SiegeStats -> Html msg
etablissementsParQuelqueChose full title communes =
    div (cols full) <|
        List.singleton <|
            div [ class "flex flex-col p-4 fr-card--white" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                , case communes of
                    [] ->
                        aucunResultat

                    _ ->
                        Html.node "chart-bar-grouped"
                            [ Encode.object
                                [ ( "values"
                                  , communes
                                        |> Encode.list
                                            (\{ label, count, countSieges } ->
                                                Encode.object
                                                    [ ( "label", Encode.string label )
                                                    , ( "count", Encode.int count )
                                                    , ( "subCount", Encode.int <| Maybe.withDefault 0 <| countSieges )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "title", Encode.string "Total" )
                                        , ( "titleSub", Encode.string "Sièges" )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Attr.attribute "data"
                            ]
                            []
                ]


aucunResultat : Html msg
aucunResultat =
    div [ class "italic" ] [ text "Aucun résultat" ]


decodeTDBStats : Decoder TDBStats
decodeTDBStats =
    Decode.succeed TDBStats
        |> andMap (Decode.field "etablissements" <| decodeEtablissementsStats)
        |> andMap (Decode.field "nafs" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "nafsEffectifs" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "nafsCas" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "catjurs" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "catjursEffectifs" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "catjursCas" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "qpvs" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "qpvsEffectifs" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "qpvsCas" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "creations" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "fermetures" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "effectifs" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "cas" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "subventionsAnnees" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "communes" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "epcis" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "departements" <| Decode.list <| decodeSiegeStats)
        |> andMap (Decode.field "regions" <| Decode.list <| decodeSiegeStats)


decodeSiegeStats : Decoder SiegeStats
decodeSiegeStats =
    Decode.succeed SiegeStats
        |> andMap (Decode.field "label" Decode.string)
        |> andMap (Decode.field "count" Decode.int)
        |> andMap (Decode.field "value" <| Decode.nullable Decode.int)
        |> andMap (Decode.field "countSieges" <| Decode.nullable Decode.int)
        |> andMap (Decode.field "valueSieges" <| Decode.nullable Decode.int)
        |> andMap (Decode.map (Maybe.withDefault 0) <| optionalNullableField "cohorte" <| Decode.int)
        |> andMap (Decode.field "id" Decode.string)


decodeEtablissementsStats : Decoder EtablissementsStats
decodeEtablissementsStats =
    Decode.succeed EtablissementsStats
        |> andMap (Decode.field "total" Decode.int)
        |> andMap (Decode.field "entreprises" Decode.int)
        |> andMap (Decode.field "sieges" Decode.int)
        |> andMap (Decode.field "actifs" Decode.int)
        |> andMap (Decode.field "actifsSieges" Decode.int)
        |> andMap (Decode.field "avecCasSieges" Decode.int)
        |> andMap (Decode.field "avecEffectifs" Decode.int)
        |> andMap (Decode.field "enQpv" Decode.int)
        |> andMap (Decode.field "enQpvSieges" Decode.int)
        |> andMap (Decode.field "enQpvAvecEffectifs" Decode.int)
        |> andMap (Decode.field "enQpvAvecCas" Decode.int)
        |> andMap (Decode.field "avecSubvention" Decode.int)
        |> andMap (Decode.field "avecSubventionSieges" Decode.int)
        |> andMap (Decode.field "ess" Decode.int)
        |> andMap (Decode.field "essSieges" Decode.int)
        |> andMap (Decode.field "micro" Decode.int)
        |> andMap (Decode.field "microSieges" Decode.int)
        |> andMap (Decode.field "caTotalSieges" Decode.int)
        |> andMap (Decode.field "caMedianSieges" Decode.float)
        |> andMap (Decode.field "effectifs" Decode.int)
        |> andMap (Decode.field "effectifsMedian" Decode.float)
        |> andMap (Decode.field "subventions" Decode.int)


cols : Bool -> List (Html.Attribute msg)
cols full =
    if full then
        [ Grid.col12 ]

    else
        [ Grid.colXl6, Grid.col12 ]
