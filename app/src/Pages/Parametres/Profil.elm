module Pages.Parametres.Profil exposing (Model, Msg, page)

import Accessibility exposing (Html, div, formWithListeners, h1, h2, p, text)
import Api
import Api.Auth exposing (post)
import DSFR.Alert
import DSFR.Button
import DSFR.Grid
import DSFR.Input
import DSFR.Typography as Typo
import Data.Role
import Effect
import Html.Attributes exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Http
import Json.Encode as Encode
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import UI.Layout
import User
import View exposing (View)


type alias Model =
    { userInput : Maybe UserInput
    , saveProfileRequest : WebData User
    , passwordInput : Maybe PasswordInput
    , savePasswordRequest : WebData User
    , appUrl : String
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | ClickedEditUser
    | ClickedCancelUser
    | UpdatedUserInput UserField String
    | RequestedSaveUser
    | ReceivedSaveUser (WebData User)
    | ClickedEditPassword
    | ClickedCancelPassword
    | UpdatedPasswordInput PasswordField String
    | RequestedSavePassword
    | ReceivedSavePassword (WebData User)


type UserField
    = Nom
    | Prenom


type alias UserInput =
    { nom : String
    , prenom : String
    }


type PasswordField
    = Password
    | Repeat


type alias PasswordInput =
    { current : String
    , password : String
    , repeat : String
    }


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page { appUrl } user =
    Spa.Page.onNewFlags (\_ -> NoOp) <|
        Spa.Page.element
            { view = view user
            , init = init appUrl
            , update = update user
            , subscriptions = \_ -> Sub.none
            }


init : String -> () -> ( Model, Effect.Effect Shared.Msg Msg )
init appUrl () =
    ( { userInput = Nothing
      , saveProfileRequest = RD.NotAsked
      , passwordInput = Nothing
      , savePasswordRequest = RD.NotAsked
      , appUrl = appUrl
      }
    , Effect.none
    )
        |> Shared.pageChangeEffects


update : User -> Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update user msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ClickedEditUser ->
            { model
                | userInput = Just <| userToUserInput user
                , savePasswordRequest = RD.NotAsked
                , saveProfileRequest = RD.NotAsked
            }
                |> Effect.withNone

        ClickedCancelUser ->
            { model | userInput = Nothing, saveProfileRequest = RD.NotAsked }
                |> Effect.withNone

        UpdatedUserInput field value ->
            { model | userInput = model.userInput |> Maybe.map (updateUserInput field value) }
                |> Effect.withNone

        RequestedSaveUser ->
            case ( model.saveProfileRequest, model.userInput ) of
                ( RD.Loading, _ ) ->
                    model
                        |> Effect.withNone

                ( _, Just userInput ) ->
                    { model | saveProfileRequest = RD.Loading }
                        |> Effect.withCmd (saveProfile userInput)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedSaveUser response ->
            case model.saveProfileRequest of
                RD.Loading ->
                    case response of
                        RD.Success u ->
                            { model
                                | saveProfileRequest = response
                                , userInput = Nothing
                            }
                                |> Effect.withShared (Shared.LoggedIn u Nothing)

                        _ ->
                            { model | saveProfileRequest = response }
                                |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        ClickedEditPassword ->
            { model
                | passwordInput = Just { current = "", password = "", repeat = "" }
                , savePasswordRequest = RD.NotAsked
                , saveProfileRequest = RD.NotAsked
            }
                |> Effect.withNone

        ClickedCancelPassword ->
            { model | passwordInput = Nothing, saveProfileRequest = RD.NotAsked }
                |> Effect.withNone

        UpdatedPasswordInput field value ->
            { model | passwordInput = model.passwordInput |> Maybe.map (updatePasswordInput field value), savePasswordRequest = RD.NotAsked }
                |> Effect.withNone

        RequestedSavePassword ->
            case ( model.savePasswordRequest, model.passwordInput ) of
                ( RD.Loading, _ ) ->
                    model
                        |> Effect.withNone

                ( _, Just passwordInput ) ->
                    { model | savePasswordRequest = RD.Loading }
                        |> Effect.withCmd (savePassword passwordInput)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedSavePassword response ->
            case model.savePasswordRequest of
                RD.Loading ->
                    case response of
                        RD.Success u ->
                            { model
                                | savePasswordRequest = response
                                , passwordInput = Nothing
                            }
                                |> Effect.withShared (Shared.LoggedIn u Nothing)

                        _ ->
                            { model | savePasswordRequest = response }
                                |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone


view : User -> Model -> View Msg
view user model =
    { title = UI.Layout.pageTitle "Informations personnelles"
    , body = [ body user model ]
    , route = Route.ParamsProfil
    }


body : User -> Model -> Html Msg
body user model =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ] <|
        if Data.Role.isDeveco <| User.role user then
            [ div [ DSFR.Grid.col3 ] [ UI.Layout.parametresSideMenu Route.ParamsProfil ]
            , div [ DSFR.Grid.col9 ]
                [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ] <|
                    viewUserInfo user model
                ]
            ]

        else
            [ div [ DSFR.Grid.col12 ]
                [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ] <|
                    viewUserInfo user model
                ]
            ]


userToUserInput : User -> UserInput
userToUserInput user =
    { nom = User.nom user
    , prenom = User.prenom user
    }


updateUserInput : UserField -> String -> UserInput -> UserInput
updateUserInput field value userInput =
    case field of
        Nom ->
            { userInput | nom = value }

        Prenom ->
            { userInput | prenom = value }


updatePasswordInput : PasswordField -> String -> PasswordInput -> PasswordInput
updatePasswordInput field value passwordInput =
    case field of
        Password ->
            { passwordInput | password = value }

        Repeat ->
            { passwordInput | repeat = value }


viewUserInfo : User -> Model -> List (Html Msg)
viewUserInfo user { saveProfileRequest, userInput, savePasswordRequest, passwordInput } =
    [ case userInput of
        Nothing ->
            viewRecapUser saveProfileRequest user

        Just input ->
            viewFormUser saveProfileRequest user input
    , case passwordInput of
        Nothing ->
            viewRecapPassword user savePasswordRequest

        Just input ->
            viewFormPassword savePasswordRequest input
    ]


viewRecapUser : WebData User -> User -> Html Msg
viewRecapUser request user =
    div [ DSFR.Grid.col12, DSFR.Grid.colLg6, class "p-4 sm:p-8" ]
        [ div [ class "fr-card--white p-4" ]
            [ h1 [ Typo.fr_h2 ] [ text "Informations personnelles" ]
            , div [ class "flex flex-col" ]
                [ DSFR.Input.new { value = User.email user, label = text "Email", onInput = \_ -> NoOp, name = "email" }
                    |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                    |> DSFR.Input.textDisplay
                    |> DSFR.Input.view
                , div [ class "flex flex-row gap-4" ]
                    [ DSFR.Input.new { value = User.prenom user, label = text "Prénom", onInput = \_ -> NoOp, name = "prenom" }
                        |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                        |> DSFR.Input.textDisplay
                        |> DSFR.Input.view
                    , DSFR.Input.new { value = User.nom user, label = text "Nom", onInput = \_ -> NoOp, name = "nom" }
                        |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                        |> DSFR.Input.textDisplay
                        |> DSFR.Input.view
                    ]
                , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowRight ]
                    [ DSFR.Button.new { onClick = Just ClickedEditUser, label = "Modifier mes informations" }
                        |> DSFR.Button.withAttrs [ class "!mb-[1rem]" ]
                        |> DSFR.Button.submit
                        |> DSFR.Button.view
                    , case request of
                        RD.Success _ ->
                            DSFR.Alert.small { title = Nothing, description = text "Mise à jour effectuée avec succès\u{00A0}!" }
                                |> DSFR.Alert.alert Nothing DSFR.Alert.success
                                |> List.singleton
                                |> div [ class "!my-8" ]

                        _ ->
                            nothing
                    ]
                ]
            ]
        ]


viewFormUser : WebData User -> User -> UserInput -> Html Msg
viewFormUser request user input =
    div [ DSFR.Grid.col12, DSFR.Grid.colLg6, class "p-4 sm:p-8" ]
        [ div [ class "fr-card--white p-4" ]
            [ h2 [] [ text "Informations personnelles" ]
            , formWithListeners [ Events.onSubmit <| RequestedSaveUser, class "flex flex-col gap-4" ]
                [ div []
                    [ DSFR.Input.new { value = User.email user, label = text "Email", onInput = \_ -> NoOp, name = "email" }
                        |> DSFR.Input.withReadonly True
                        |> DSFR.Input.withDisabled True
                        |> DSFR.Input.view
                    ]
                , div [ class "flex flex-row gap-4" ]
                    [ DSFR.Input.new { value = input.prenom, label = text "Prénom", onInput = UpdatedUserInput Prenom, name = "prenom" }
                        |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                        |> DSFR.Input.view
                    , DSFR.Input.new { value = input.nom, label = text "Nom", onInput = UpdatedUserInput Nom, name = "nom" }
                        |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                        |> DSFR.Input.view
                    ]
                , footerUser request user input
                ]
            ]
        ]


footerUser : WebData User -> User -> UserInput -> Html Msg
footerUser request user userInput =
    let
        hasNotChanged =
            userInput == userToUserInput user

        ( isButtonDisabled, title ) =
            if request == RD.Loading || hasNotChanged then
                ( True, "Aucun changement effectué" )

            else
                ( False, "Enregistrer les modifications" )
    in
    div [ DSFR.Grid.gridRow, class "!mt-4" ]
        [ div [ DSFR.Grid.col12 ]
            [ case request of
                RD.Failure _ ->
                    DSFR.Alert.small { title = Just "Mise à jour échouée", description = text "Veuillez réessayer et nous contacter si le problème persiste." }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.error
                        |> List.singleton
                        |> div [ class "!my-8" ]

                _ ->
                    nothing
            , DSFR.Button.group
                [ DSFR.Button.new { onClick = Nothing, label = "Enregistrer" }
                    |> DSFR.Button.submit
                    |> DSFR.Button.withAttrs [ Html.Attributes.title title, Html.Attributes.name "submit-profile" ]
                    |> DSFR.Button.withDisabled isButtonDisabled
                , DSFR.Button.new { onClick = Just ClickedCancelUser, label = "Annuler" }
                    |> DSFR.Button.secondary
                ]
                |> DSFR.Button.inline
                |> DSFR.Button.alignedRightInverted
                |> DSFR.Button.viewGroup
            ]
        ]


viewRecapPassword : User -> WebData User -> Html Msg
viewRecapPassword user request =
    div [ DSFR.Grid.col12, DSFR.Grid.colLg6, class "p-4 sm:p-8" ]
        [ div [ class "fr-card--white p-4" ]
            [ h2 [] [ text "Gestion du mot de passe" ]
            , div [ class "flex flex-col gap-4" ]
                [ if User.aUnMotDePasse user then
                    div []
                        [ DSFR.Input.new { value = "********", label = text "Mot de passe", onInput = \_ -> NoOp, name = "password" }
                            |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                            |> DSFR.Input.password
                            |> DSFR.Input.textDisplay
                            |> DSFR.Input.view
                        ]

                  else
                    div [ class "h-[72px] italic" ] [ text "Vous n'avez pas encore créé votre mot de passe." ]
                , div [ class "h-[72px]" ] [ text "\u{00A0}" ]
                , div [ DSFR.Grid.gridRow, class "!mt-4" ]
                    [ div [ DSFR.Grid.col12 ]
                        [ [ DSFR.Button.new
                                { onClick = Just ClickedEditPassword
                                , label =
                                    if User.aUnMotDePasse user then
                                        "Modifier mon mot de passe"

                                    else
                                        "Créer mon mot de passe"
                                }
                                |> DSFR.Button.withAttrs [ class "!mb-[1rem]" ]
                                |> DSFR.Button.submit
                          ]
                            |> DSFR.Button.group
                            |> DSFR.Button.inline
                            |> DSFR.Button.alignedRightInverted
                            |> DSFR.Button.viewGroup
                        , case request of
                            RD.Success _ ->
                                DSFR.Alert.small { title = Nothing, description = text "Mise à jour effectuée avec succès\u{00A0}!" }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.success
                                    |> List.singleton
                                    |> div [ class "!my-8" ]

                            _ ->
                                nothing
                        ]
                    ]
                ]
            ]
        ]


viewFormPassword : WebData User -> PasswordInput -> Html Msg
viewFormPassword request input =
    div [ DSFR.Grid.col12, DSFR.Grid.colLg6, class "p-4 sm:p-8" ]
        [ div [ class "fr-card--white p-4" ]
            [ h2 [] [ text "Gestion du mot de passe" ]
            , formWithListeners [ class "flex flex-col gap-4", Events.onSubmit <| RequestedSavePassword ]
                [ p [] [ text "Le mot de passe doit faire au moins 10 caractères." ]
                , div []
                    [ DSFR.Input.new
                        { value = input.password
                        , label = text "Mot de passe"
                        , onInput = UpdatedPasswordInput Password
                        , name = "password"
                        }
                        |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                        |> DSFR.Input.newPassword
                        |> DSFR.Input.view
                    ]
                , div []
                    [ DSFR.Input.new
                        { value = input.repeat
                        , label = text "Confirmation"
                        , onInput = UpdatedPasswordInput Repeat
                        , name = "repeat"
                        }
                        |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                        |> DSFR.Input.confirmPassword
                        |> DSFR.Input.view
                    ]
                , footerPassword request input
                ]
            ]
        ]


footerPassword : WebData User -> PasswordInput -> Html Msg
footerPassword request { password, repeat } =
    let
        ( isButtonDisabled, title ) =
            if request == RD.Loading then
                ( True, "Une opération est déjà en cours" )

            else if password == "" then
                ( True, "Vous devez saisir un mot de passe" )

            else if String.length password < 10 then
                ( True, "Le mot de passe doit faire au moins 10 caractères" )

            else if password /= repeat then
                ( True, "Les mots de passe ne correspondent pas" )

            else
                ( False, "Enregistrer les modifications" )
    in
    div [ DSFR.Grid.gridRow, class "!mt-4" ]
        [ div [ DSFR.Grid.col12 ]
            [ case request of
                RD.Failure _ ->
                    DSFR.Alert.small { title = Just "Mise à jour échouée", description = text "Veuillez réessayer et nous contacter si le problème persiste." }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.error
                        |> List.singleton
                        |> div [ class "!my-8" ]

                _ ->
                    nothing
            , [ DSFR.Button.new { onClick = Nothing, label = "Enregistrer" }
                    |> DSFR.Button.submit
                    |> DSFR.Button.withAttrs [ Html.Attributes.title title, Html.Attributes.name "new-password" ]
                    |> DSFR.Button.withDisabled isButtonDisabled
              , DSFR.Button.new { onClick = Just ClickedCancelPassword, label = "Annuler" }
                    |> DSFR.Button.secondary
              ]
                |> DSFR.Button.group
                |> DSFR.Button.inline
                |> DSFR.Button.alignedRightInverted
                |> DSFR.Button.viewGroup
            ]
        ]


saveProfile : UserInput -> Cmd Msg
saveProfile { nom, prenom } =
    let
        jsonBody =
            [ ( "nom", Encode.string nom )
            , ( "prenom", Encode.string prenom )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    post
        { url = Api.updateAccount
        , body = jsonBody
        }
        { toShared = SharedMsg
        , logger = Nothing
        , handler = ReceivedSaveUser
        }
        User.decodeUser


savePassword : PasswordInput -> Cmd Msg
savePassword { password } =
    let
        jsonBody =
            [ ( "motDePasse", Encode.string password )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    post
        { url = Api.updatePassword
        , body = jsonBody
        }
        { toShared = SharedMsg
        , logger = Nothing
        , handler = ReceivedSavePassword
        }
        User.decodeUser
