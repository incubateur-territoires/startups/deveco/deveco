module Pages.Parametres.Notifications exposing (Model, Msg, page)

import Accessibility exposing (Html, div, text)
import Api
import Api.Auth exposing (get, post)
import DSFR.Card
import DSFR.Grid
import DSFR.Toggle
import Effect
import Html.Attributes exposing (class)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode exposing (Value)
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


type alias Model =
    { notifications : WebData Notifications
    , saveRequest : WebData ()
    }


type alias Notifications =
    { connexion : Bool
    , rappels : Bool
    , activite : Bool
    }


type NotificationType
    = Connexion
    | Rappels
    | Activite


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | ReceivedNotifications (WebData Notifications)
    | ToggleNotification NotificationType Bool
    | ReceivedSaveNotifications (WebData Notifications)


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.onNewFlags (\_ -> NoOp) <|
        Spa.Page.element
            { view = view
            , init = init
            , update = update
            , subscriptions = \_ -> Sub.none
            }


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init () =
    ( { notifications = RD.Loading
      , saveRequest = RD.NotAsked
      }
    , getNotifications
    )
        |> Shared.pageChangeEffects


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ReceivedNotifications notifications ->
            { model | notifications = notifications }
                |> Effect.withNone

        ToggleNotification notificationType on ->
            let
                notifications =
                    model.notifications
                        |> RD.map (updateNotifications notificationType on)

                effect =
                    notifications
                        |> RD.map saveNotifications
                        |> RD.withDefault Effect.none
            in
            { model | notifications = notifications }
                |> Effect.with effect

        ReceivedSaveNotifications (RD.Success notifications) ->
            { model | notifications = RD.Success notifications, saveRequest = RD.NotAsked }
                |> Effect.withNone

        ReceivedSaveNotifications response ->
            { model | saveRequest = response |> RD.map (\_ -> ()) }
                |> Effect.withNone


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle "SIG"
    , body = [ body model ]
    , route = Route.ParamsSIG
    }


body : Model -> Html Msg
body model =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col3 ] [ UI.Layout.parametresSideMenu Route.ParamsNotifications ]
        , div [ DSFR.Grid.col9 ]
            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ viewNotifications model.saveRequest model.notifications
                ]
            ]
        ]


viewNotifications : WebData () -> WebData Notifications -> Html Msg
viewNotifications request notifications =
    div [ DSFR.Grid.col12 ]
        [ DSFR.Card.card (text "Gestion des notifications") DSFR.Card.vertical
            |> DSFR.Card.withDescription
                (Just <|
                    case notifications of
                        RD.Loading ->
                            div []
                                [ text "Chargement en cours"
                                ]

                        RD.Success { connexion, rappels, activite } ->
                            div [ class "flex flex-col gap-4" ]
                                [ DSFR.Toggle.group
                                    { id = "notifications"
                                    , onChecked = Tuple.first >> ToggleNotification
                                    , values =
                                        [ ( Connexion, connexion )
                                        , ( Rappels, rappels )
                                        , ( Activite, activite )
                                        ]
                                    , valueAsString = Tuple.first >> notificationTypeToId
                                    , toChecked = Tuple.second
                                    , toId = Tuple.first >> notificationTypeToId
                                    , toLabel = Tuple.first >> notificationTypeToTitle >> text
                                    }
                                    |> DSFR.Toggle.groupWithToHint (Tuple.first >> notificationTypeToHint >> Just)
                                    |> DSFR.Toggle.groupWithNoState
                                    |> DSFR.Toggle.groupWithLabelLeft
                                    |> DSFR.Toggle.groupWithDisabled (request == RD.Loading)
                                    |> DSFR.Toggle.viewGroup
                                ]

                        _ ->
                            div []
                                [ text "Une erreur s'est produite, veuillez réessayer."
                                ]
                )
            |> DSFR.Card.view
        ]


notificationTypeToId : NotificationType -> String
notificationTypeToId notificationType =
    case notificationType of
        Connexion ->
            "connexion"

        Rappels ->
            "rappels"

        Activite ->
            "activite"


notificationTypeToTitle : NotificationType -> String
notificationTypeToTitle notificationType =
    case notificationType of
        Connexion ->
            "Notification de connexion à Deveco"

        Rappels ->
            "Notification de l'échéance d'un rappel"

        Activite ->
            "Notification de l'activité de l'équipe"


notificationTypeToHint : NotificationType -> String
notificationTypeToHint notificationType =
    case notificationType of
        Connexion ->
            "Recevoir un email à chaque connexion par mot de passe sur votre compte Deveco"

        Rappels ->
            "Recevoir un email deux jours avant l'échéance d'un rappel qui vous est assigné"

        Activite ->
            "Recevoir un email récapitulatif de l'activité de votre équipe toutes les deux semaines"


updateNotifications : NotificationType -> Bool -> Notifications -> Notifications
updateNotifications notificationType on notifications =
    case notificationType of
        Connexion ->
            { notifications | connexion = on }

        Rappels ->
            { notifications | rappels = on }

        Activite ->
            { notifications | activite = on }


getNotifications : Effect.Effect Shared.Msg Msg
getNotifications =
    Effect.fromCmd <|
        get
            { url = Api.getNotifications
            }
            { toShared = SharedMsg
            , logger = Nothing
            , handler = ReceivedNotifications
            }
            decodeNotifications


saveNotifications : Notifications -> Effect.Effect Shared.Msg Msg
saveNotifications notifications =
    Effect.fromCmd <|
        post
            { url = Api.getNotifications
            , body =
                [ ( "notifications"
                  , notifications
                        |> encodeNotifications
                  )
                ]
                    |> Encode.object
                    |> Http.jsonBody
            }
            { toShared = SharedMsg
            , logger = Nothing
            , handler = ReceivedSaveNotifications
            }
            decodeNotifications


decodeNotifications : Decoder Notifications
decodeNotifications =
    Decode.field "notifications" <|
        (Decode.succeed Notifications
            |> andMap (Decode.field "connexion" Decode.bool)
            |> andMap (Decode.field "rappels" Decode.bool)
            |> andMap (Decode.field "activite" Decode.bool)
        )


encodeNotifications : Notifications -> Value
encodeNotifications { connexion, rappels, activite } =
    [ ( "connexion", Encode.bool connexion )
    , ( "rappels", Encode.bool rappels )
    , ( "activite", Encode.bool activite )
    ]
        |> Encode.object
