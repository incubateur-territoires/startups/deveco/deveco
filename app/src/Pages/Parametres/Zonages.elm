module Pages.Parametres.Zonages exposing (Model, Msg, init, termine, update, view, zonageAjoute)

import Accessibility exposing (Html, div, label, p, span, text)
import Api
import Api.Auth exposing (post)
import Api.EntityId exposing (EntityId)
import DSFR.Button
import DSFR.Grid
import DSFR.Icons.System
import DSFR.Input
import DSFR.Modal
import DSFR.Typography as Typo
import Data.Zonage exposing (Zonage, ZonageId)
import Effect
import File
import File.Select
import Html.Attributes exposing (class, title)
import Html.Extra exposing (nothing, viewIf)
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Lib.Umap
import RemoteData as RD exposing (WebData)
import Shared
import Task


type alias Model =
    { zonageData : ZonageData
    , termine : Bool
    , zonageAjoute : Bool
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | CanceledAddZonage
    | UpdatedNomZonage String
    | UpdatedDescriptionZonage String
    | ClickedUploadFichierZonage
    | UploadedFichierZonage File.File
    | ReadFichierZonage String
    | ClickedRemovedZonageFile
    | ConfirmedAddZonage
    | ReceivedAddZonage (WebData ())


type alias ZonageData =
    { nom : String
    , description : String
    , zonageExistant : Maybe (EntityId ZonageId)
    , fichierNom : Maybe String
    , fichierContenu : Maybe String
    , request : WebData ()
    }


initZonage : Maybe Zonage -> ZonageData
initZonage maybeZonage =
    { nom = maybeZonage |> Maybe.map .nom |> Maybe.withDefault ""
    , description = maybeZonage |> Maybe.map .description |> Maybe.withDefault ""
    , zonageExistant = maybeZonage |> Maybe.map .id
    , fichierNom = Nothing
    , fichierContenu = Nothing
    , request = RD.NotAsked
    }


init : Maybe Zonage -> Model
init maybeZonage =
    { zonageData = initZonage maybeZonage
    , termine = False
    , zonageAjoute = False
    }


saveZonage : ZonageData -> Effect.Effect Shared.Msg Msg
saveZonage zonage =
    Effect.fromCmd <|
        post
            { url = Api.saveZonage <| zonage.zonageExistant
            , body =
                [ ( "nom", Encode.string zonage.nom )
                , ( "description", Encode.string zonage.description )
                , ( "geojsonString"
                  , Maybe.withDefault Encode.null <|
                        Maybe.map Encode.string <|
                            zonage.fichierContenu
                  )
                ]
                    |> Encode.object
                    |> Http.jsonBody
            }
            { toShared = SharedMsg
            , logger = Nothing
            , handler = ReceivedAddZonage
            }
            (Decode.succeed ())


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        UpdatedNomZonage nom ->
            { model
                | zonageData =
                    model.zonageData
                        |> (\z ->
                                { z
                                    | nom = nom
                                }
                           )
            }
                |> Effect.withNone

        UpdatedDescriptionZonage description ->
            { model
                | zonageData =
                    model.zonageData
                        |> (\z ->
                                { z
                                    | description = description
                                }
                           )
            }
                |> Effect.withNone

        ClickedUploadFichierZonage ->
            model
                |> Effect.withCmd (File.Select.file [] UploadedFichierZonage)

        UploadedFichierZonage fichier ->
            { model
                | zonageData =
                    model.zonageData
                        |> (\z ->
                                { z
                                    | fichierNom = Just <| File.name fichier
                                }
                           )
            }
                |> Effect.withCmd (Task.perform ReadFichierZonage <| File.toString fichier)

        ReadFichierZonage contenu ->
            { model
                | zonageData =
                    model.zonageData
                        |> (\z ->
                                { z
                                    | fichierContenu = Just contenu
                                }
                           )
            }
                |> Effect.withNone

        ClickedRemovedZonageFile ->
            { model
                | zonageData =
                    model.zonageData
                        |> (\z ->
                                { z
                                    | fichierNom = Nothing
                                    , fichierContenu = Nothing
                                }
                           )
            }
                |> Effect.withNone

        ConfirmedAddZonage ->
            ( { model
                | zonageData =
                    model.zonageData
                        |> (\z ->
                                { z
                                    | request = RD.Loading
                                }
                           )
              }
            , saveZonage model.zonageData
            )

        ReceivedAddZonage resp ->
            { model
                | zonageData =
                    model.zonageData
                        |> (\z ->
                                { z
                                    | request = resp
                                }
                           )
                , zonageAjoute = RD.isSuccess resp
            }
                |> Effect.withNone

        CanceledAddZonage ->
            { model | termine = True }
                |> Effect.withNone


view : List String -> Maybe Model -> Html Msg
view nomsLocalisations model =
    viewZonageModal nomsLocalisations <|
        Maybe.map .zonageData <|
            model


viewZonageModal : List String -> Maybe ZonageData -> Html Msg
viewZonageModal nomsLocalisations zonageData =
    let
        ( opened, content, title ) =
            case zonageData of
                Nothing ->
                    ( False, nothing, nothing )

                Just nz ->
                    ( True
                    , viewZonageDataBody nomsLocalisations nz
                    , text <|
                        if nz.zonageExistant == Nothing then
                            "Ajouter une étiquette avec zonage"

                        else
                            "Modifier le zonage de l'étiquette"
                    )
    in
    DSFR.Modal.view
        { id = "zonage"
        , label = "zonage"
        , openMsg = NoOp
        , closeMsg = Just CanceledAddZonage
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


viewZonageDataBody : List String -> ZonageData -> Html Msg
viewZonageDataBody nomsLocalisations zonageData =
    let
        alreadyUsed =
            nomsLocalisations
                |> List.map String.toLower
                |> List.member (zonageData |> .nom |> String.toLower)
    in
    div [ class "p-4" ]
        [ div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col12 ]
                [ case zonageData.request of
                    RD.Loading ->
                        div []
                            [ text <|
                                "Le zonage "
                                    ++ zonageData.nom
                                    ++ " est en cours de création."
                            ]

                    RD.Success _ ->
                        div []
                            [ p []
                                [ text <|
                                    "Le zonage "
                                        ++ zonageData.nom
                                        ++ " a été créé avec succès\u{00A0}!"
                                ]
                            ]

                    _ ->
                        let
                            nomZonageNonEditable =
                                zonageData.zonageExistant /= Nothing
                        in
                        div [ class "flex flex-col gap-8" ]
                            [ div [ class "flex flex-row gap-4" ]
                                [ div [ class "flex flex-row w-2/5" ]
                                    [ DSFR.Input.new
                                        { value = zonageData.nom
                                        , label = text "Nom de l'étiquette"
                                        , onInput = UpdatedNomZonage
                                        , name = "nom-zonage"
                                        }
                                        |> DSFR.Input.withRequired True
                                        |> DSFR.Input.withDisabled nomZonageNonEditable
                                        |> DSFR.Input.withError
                                            (if alreadyUsed && zonageData.zonageExistant == Nothing then
                                                Just [ text "Ce nom est déjà utilisé" ]

                                             else
                                                Nothing
                                            )
                                        |> (if nomZonageNonEditable then
                                                DSFR.Input.withExtraAttrs
                                                    [ class "w-full"
                                                    , title "Pour modifier le nom de l'étiquette, veuillez utiliser le bouton d'édition situé sur la droite"
                                                    ]

                                            else
                                                DSFR.Input.withExtraAttrs [ class "w-full" ]
                                           )
                                        |> DSFR.Input.view
                                    ]
                                , div [ class "flex w-3/5" ]
                                    [ DSFR.Input.new
                                        { value = zonageData.description
                                        , label = text "Description du zonage"
                                        , onInput = UpdatedDescriptionZonage
                                        , name = "description-zonage"
                                        }
                                        |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                                        |> DSFR.Input.textArea Nothing
                                        |> DSFR.Input.view
                                    ]
                                ]
                            , div [ class "flex flex-row gap-4" ]
                                [ div
                                    [ class "fr-upload-group w-2/5"
                                    ]
                                    [ label
                                        [ class "fr-label"
                                        , Html.Attributes.for "file-upload"
                                        ]
                                        [ text "Plan du zonage"
                                        , viewIf (zonageData.fichierNom == Nothing) <|
                                            span
                                                [ class "fr-hint-text"
                                                ]
                                                [ text "Fichier au format GeoJSON (données géographiques au format WGS84 - EPSG:4326)" ]
                                        , case zonageData.fichierNom of
                                            Just fichier ->
                                                div [ class "flex flex-row gap-2 items-center" ]
                                                    [ text fichier
                                                    , DSFR.Button.new { label = "", onClick = Just ClickedRemovedZonageFile }
                                                        |> DSFR.Button.onlyIcon DSFR.Icons.System.closeLine
                                                        |> DSFR.Button.tertiaryNoOutline
                                                        |> DSFR.Button.view
                                                    ]

                                            Nothing ->
                                                DSFR.Button.new { label = "Choisir...", onClick = Just ClickedUploadFichierZonage }
                                                    |> DSFR.Button.tertiaryNoOutline
                                                    |> DSFR.Button.view
                                        ]
                                    ]
                                , div [ class "w-3/5" ]
                                    [ p [ Typo.textSm ]
                                        [ text "Vous pouvez créer un fichier GeoJSON pour créer votre zonage sur une carte grâce à des services en ligne comme "
                                        , let
                                            lien =
                                                Lib.Umap.umapLink []
                                          in
                                          Typo.externalLink lien [] [ text lien ]
                                        , text "."
                                        ]
                                    , viewIf (RD.isFailure zonageData.request) (text "Une erreur s'est produite, veuillez vérifier que votre fichier de zonage est valide (format GeoJSON, Feature de type Polygon ou MultiPolygon, coordonnées au format WGS84 - EPSG:4326) puis réessayer. Contactez-nous si le problème persiste.")
                                    ]
                                ]
                            ]
                ]
            , div [ DSFR.Grid.col12 ]
                [ let
                    valid =
                        (zonageData.nom /= "")
                            && (zonageData.fichierContenu /= Nothing)
                            && not (alreadyUsed && zonageData.zonageExistant == Nothing)

                    ( isButtonDisabled, title_ ) =
                        case ( zonageData.request, valid ) of
                            ( RD.Loading, _ ) ->
                                ( True, "Enregistrer" )

                            ( _, False ) ->
                                ( True, "Enregistrer" )

                            _ ->
                                ( False, "Enregistrer" )

                    buttons =
                        case zonageData.request of
                            RD.Success _ ->
                                [ DSFR.Button.new { onClick = Just CanceledAddZonage, label = "OK" }
                                ]

                            _ ->
                                [ DSFR.Button.new { onClick = Just ConfirmedAddZonage, label = "Enregistrer" }
                                    |> DSFR.Button.withAttrs [ title title_, Html.Attributes.name "ajout-zonage" ]
                                    |> DSFR.Button.withDisabled isButtonDisabled
                                , DSFR.Button.new { onClick = Just CanceledAddZonage, label = "Annuler" }
                                    |> DSFR.Button.secondary
                                ]
                  in
                  DSFR.Button.group buttons
                    |> DSFR.Button.inline
                    |> DSFR.Button.alignedRightInverted
                    |> DSFR.Button.viewGroup
                ]
            ]
        ]


termine : Model -> Bool
termine model =
    model.termine


zonageAjoute : Model -> Bool
zonageAjoute model =
    model.zonageAjoute
