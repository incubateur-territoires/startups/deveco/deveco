module Pages.Parametres.Collegues exposing (Model, Msg, page)

import Accessibility exposing (Html, div, h3, hr, p, span, text)
import DSFR.Button
import DSFR.Card
import DSFR.Grid
import DSFR.Icons.System
import DSFR.Input
import DSFR.Modal
import DSFR.Typography as Typo
import Data.Collegues exposing (Collegue)
import Data.Equipe
import Data.Role
import Effect
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing, viewMaybe)
import Lib.Variables exposing (contactEmail)
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import UI.Clipboard
import UI.Collegue exposing (getCollegues)
import UI.Layout
import Url.Builder as Builder
import User
import View exposing (View)


type alias Model =
    { collegues : WebData (List Collegue)
    , inviterCollegue : Maybe ()
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | ReceivedCollègues (WebData (List Collegue))
    | ClickedInviterCollegue
    | CanceledInviterCollegue


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page _ user =
    Spa.Page.onNewFlags (\_ -> NoOp) <|
        Spa.Page.element
            { view = view user
            , init = init
            , update = update user
            , subscriptions = \_ -> Sub.none
            }


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init () =
    ( { collegues = RD.Loading
      , inviterCollegue = Nothing
      }
    , Effect.fromCmd (getCollegues SharedMsg Nothing ReceivedCollègues)
    )
        |> Shared.pageChangeEffects


update : User -> Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update user msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ReceivedCollègues response ->
            { model | collegues = response |> RD.map (List.filter (\collab -> collab.id /= User.id user)) }
                |> Effect.withNone

        ClickedInviterCollegue ->
            { model
                | inviterCollegue = Just ()
            }
                |> Effect.withNone

        CanceledInviterCollegue ->
            { model
                | inviterCollegue = Nothing
            }
                |> Effect.withNone


view : User.User -> Model -> View Msg
view user model =
    { title = UI.Layout.pageTitle "Gestion des collègues"
    , body = UI.Layout.lazyBody (body user) model
    , route = Route.ParamsCollegues
    }


body : User.User -> Model -> Html Msg
body user model =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ viewInviterCollegueModal model.inviterCollegue
        , div [ DSFR.Grid.col3 ] [ UI.Layout.parametresSideMenu Route.ParamsCollegues ]
        , div [ DSFR.Grid.col9 ]
            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ viewCollegues
                    (user
                        |> User.role
                        |> Data.Role.equipe
                        |> Maybe.map Data.Equipe.nom
                        |> Maybe.withDefault "?"
                    )
                    model.collegues
                ]
            ]
        ]


viewInviterCollegueModal : Maybe () -> Html Msg
viewInviterCollegueModal maybeInviterCollegue =
    DSFR.Modal.view
        { id = "inviter-collegue"
        , label = "inviter-collegue"
        , openMsg = NoOp
        , closeMsg = Just CanceledInviterCollegue
        , title = text "Inviter un collègue"
        , opened = maybeInviterCollegue /= Nothing
        , size = Just DSFR.Modal.md
        }
        (viewMaybe viewInviterCollègue maybeInviterCollegue)
        Nothing
        |> Tuple.first


viewInviterCollègue : () -> Html Msg
viewInviterCollègue () =
    let
        emails =
            []

        subject =
            Builder.string "subject" <|
                "Découvre Deveco"

        bod =
            Builder.string "body" <|
                "Bonjour,\nJ’utilise actuellement l'outil numérique Deveco (porté par l’Agence nationale de la Cohésion des Territoires) et je t’invite à me rejoindre sur l’outil.\nDeveco réunit dans une même solution\u{00A0}:\n- une base de données des entreprises et des locaux professionnels présents sur notre territoire\u{00A0};\n- un outil de gestion de la relation (CRM) que nous pouvons enrichir de nos contacts et échanges pour suivre les entreprises et les porteurs de projets que nous accompagnons\u{00A0};\n- une analyse de notre tissu économique et un reporting d’activité.\n\nPour obtenir un compte sur Deveco, inscris-toi à un des prochains webinaires de découverte (1h)\u{00A0}: "
                    ++ Lib.Variables.lienWebinaireDecouverte
                    ++ "\nBonne journée,\n"

        mailto =
            "mailto:"
                ++ String.join "," emails
                ++ Builder.toQuery [ subject, bod ]
    in
    div [ class "flex flex-col gap-4 pb-4" ]
        [ p [] [ text "Pour inviter un collègue sur Deveco, vous pouvez lui envoyer le lien d'inscription ou lui envoyer directement notre email d'invitation." ]
        , div [ class "flex flex-col gap-4" ]
            [ div [ class "flex flex-col" ]
                [ div [] [ text "Lien d'inscription au webinaire de découverte" ]
                , div [ class "flex flex-row gap-4 items-baseline" ]
                    [ DSFR.Input.new
                        { value = Lib.Variables.lienWebinaireDecouverte
                        , onInput = \_ -> NoOp
                        , label = nothing
                        , name = "lien-webinaire-decouverte"
                        }
                        |> DSFR.Input.withReadonly True
                        |> DSFR.Input.withExtraAttrs [ class "!w-3/5 !mb-0" ]
                        |> DSFR.Input.view
                    , UI.Clipboard.clipboard
                        { content = Lib.Variables.lienWebinaireDecouverte
                        , label = "Copier le lien"
                        , copiedLabel = "Copié\u{00A0}!"
                        , timeoutMilliseconds = 2000
                        }
                    ]
                ]
            , div [ class "flex flex-row gap-4 items-center w-full" ]
                [ hr [ class "w-full relative top-[0.7rem]" ] []
                , text "ou"
                , hr [ class "w-full relative top-[0.7rem]" ] []
                ]
            , div [ class "w-full" ]
                [ DSFR.Button.new
                    { label = "Envoyer l'invitation par email"
                    , onClick = Nothing
                    }
                    |> DSFR.Button.withAttrs [ class "!w-full justify-center" ]
                    |> DSFR.Button.linkButtonExternal mailto
                    |> DSFR.Button.view
                ]
            ]
        ]


viewCollegues : String -> WebData (List Collegue) -> Html Msg
viewCollegues equipe collegues =
    div [ DSFR.Grid.col12 ]
        [ DSFR.Card.card (text "Mes collègues") DSFR.Card.vertical
            |> DSFR.Card.withDescription
                (Just <|
                    div [] <|
                        [ p []
                            [ span [ Typo.textBold ] [ text "Seuls les collègues listés ci-dessous" ]
                            , text " peuvent accéder au contenu de l'outil Deveco de "
                            , text equipe
                            , text "."
                            ]
                        , case collegues of
                            RD.NotAsked ->
                                text "Une erreur s'est produite, veuillez recharger la page."

                            RD.Loading ->
                                text "Chargement en cours..."

                            RD.Failure _ ->
                                text "Une erreur s'est produite, veuillez recharger la page."

                            RD.Success collabs ->
                                let
                                    ( enabledCollabs, disabledCollegues ) =
                                        List.partition .actif <|
                                            collabs
                                in
                                div [ class "flex flex-row gap-4" ]
                                    [ div [ class "w-full flex flex-col gap-2" ]
                                        [ h3 [ Typo.fr_h6, class "!mb-0" ]
                                            [ text "Collègues actuels"
                                            ]
                                        , DSFR.Button.new
                                            { onClick = Just ClickedInviterCollegue
                                            , label = "Inviter un collègue"
                                            }
                                            |> DSFR.Button.small
                                            |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                                            |> DSFR.Button.tertiaryNoOutline
                                            |> DSFR.Button.view
                                        , if List.length enabledCollabs == 0 then
                                            text "Aucun collègue pour l'instant"

                                          else
                                            div [ class "flex flex-col gap-2" ] <|
                                                List.map viewCollegue <|
                                                    enabledCollabs
                                        ]
                                    , div [ class "w-full flex flex-col gap-2" ]
                                        [ h3 [ Typo.fr_h6, class "!mb-0" ]
                                            [ text "Anciens collègues" ]
                                        , let
                                            emails =
                                                [ contactEmail ]

                                            subject =
                                                Builder.string "subject" <|
                                                    "Demande de suppression d'un collègue"

                                            bod =
                                                Builder.string "body" <|
                                                    "Territoire\u{00A0}: "
                                                        ++ equipe
                                                        ++ "\nEmail du collègue\u{00A0}: \nNom du collègue\u{00A0}: \nPrénom du collègue\u{00A0}: \nFonction du collègue\u{00A0}: \n"

                                            mailto =
                                                "mailto:"
                                                    ++ String.join "," emails
                                                    ++ Builder.toQuery [ subject, bod ]
                                          in
                                          DSFR.Button.new
                                            { onClick = Nothing
                                            , label = "Demander la suppression d'un collègue"
                                            }
                                            |> DSFR.Button.small
                                            |> DSFR.Button.linkButtonExternal mailto
                                            |> DSFR.Button.leftIcon DSFR.Icons.System.subtractLine
                                            |> DSFR.Button.tertiaryNoOutline
                                            |> DSFR.Button.view
                                        , if List.length disabledCollegues == 0 then
                                            text "Aucun ancien collègue."

                                          else
                                            div [ class "flex flex-col gap-2" ] <|
                                                List.map viewCollegue <|
                                                    disabledCollegues
                                        ]
                                    ]
                        ]
                )
            |> DSFR.Card.view
        ]


viewCollegue : Collegue -> Html msg
viewCollegue collegue =
    div []
        [ div [] <|
            [ span [ Typo.textBold ] <| List.singleton <| text <| .email collegue
            ]
        , div [] <|
            List.singleton <|
                case ( .prenom collegue, .nom collegue ) of
                    ( "", "" ) ->
                        span [ class "italic" ]
                            [ text <|
                                "Pas de nom"
                            ]

                    ( "", nom ) ->
                        text <|
                            nom

                    ( prenom, "" ) ->
                        text <|
                            prenom

                    ( prenom, nom ) ->
                        text <|
                            prenom
                                ++ " "
                                ++ nom
        ]
