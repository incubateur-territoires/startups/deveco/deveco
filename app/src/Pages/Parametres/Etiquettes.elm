module Pages.Parametres.Etiquettes exposing (Model, Msg, page)

import Accessibility exposing (Html, br, div, h1, h2, hr, p, span, text)
import Api
import Api.Auth exposing (delete, get, post)
import Api.EntityId exposing (EntityId)
import Browser.Events
import DSFR.Accordion
import DSFR.Button
import DSFR.CallOut exposing (callout)
import DSFR.Footer exposing (lienTutoriel)
import DSFR.Grid
import DSFR.Icons.Design exposing (editFill)
import DSFR.Icons.System exposing (deleteFill, downloadLine, uploadLine)
import DSFR.Input
import DSFR.Modal
import DSFR.Typography as Typo
import Data.Equipe exposing (EquipeId)
import Data.Etiquette exposing (EtiquetteAvecUsages, EtiquettesAvecUsages, decodeEtiquettesAvecUsages)
import Data.Role
import Data.Zonage exposing (Zonage, decodeZonage)
import Effect
import Html
import Html.Attributes as Attrs
import Html.Attributes.Extra exposing (attributeIf)
import Html.Events
import Html.Extra exposing (nothing, viewIf)
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Lib.UI exposing (plural)
import Pages.Parametres.Zonages as Zonages
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import UI.Layout
import User
import View exposing (View)


type alias Model =
    { etiquettes : WebData EtiquettesAvecUsages
    , editRequest : Maybe EditRequest
    , deleteRequest : Maybe DeleteRequest
    , zonages : WebData (List Zonage)
    , zonagesModel : Maybe Zonages.Model
    , zonageDeleteRequest : Maybe ZonageDeleteRequest
    , nouvelleEtiquetteModal : Maybe NouvelleEtiquette
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | ZonagesMsg Zonages.Msg
    | ReceivedZonages (WebData (List Zonage))
    | ReceivedCurrentEtiquettes (WebData EtiquettesAvecUsages)
    | ClickedDelete (EntityId EquipeId) EtiquetteType EtiquetteAvecUsages
    | CanceledDelete
    | ConfirmedDelete
    | ReceivedDelete (WebData EtiquettesAvecUsages)
    | ClickedEdit (EntityId EquipeId) EtiquetteType EtiquetteAvecUsages
    | UpdateEditEtiquette String
    | CanceledEdit
    | ConfirmedEdit
    | ReceivedEdit (WebData EtiquettesAvecUsages)
    | ClickedAddZonage
    | ClickedEditZonage Zonage
    | ClickedNouvelleEtiquette
    | CanceledNouvelleEtiquette
    | UpdateNouvelleEtiquetteType EtiquetteType
    | UpdateNouvelleEtiquetteLabel String
    | NouvelleEtiquetteLabelFocused Bool
    | ConfirmedNouvelleEtiquette
    | ReceivedNouvelleEtiquette (WebData EtiquettesAvecUsages)


type alias EditRequest =
    { request : WebData EtiquettesAvecUsages
    , equipeId : EntityId EquipeId
    , etiquetteType : EtiquetteType
    , etiquette : EtiquetteAvecUsages
    , label : String
    }


type alias DeleteRequest =
    { request : WebData EtiquettesAvecUsages
    , equipeId : EntityId EquipeId
    , etiquetteType : EtiquetteType
    , etiquette : EtiquetteAvecUsages
    }


type alias ZonageDeleteRequest =
    { request : WebData (List Zonage)
    , zonage : Zonage
    }


type EtiquetteType
    = Activites
    | Zones
    | Mots


type alias NouvelleEtiquette =
    { type_ : EtiquetteType
    , label : String
    , request : WebData EtiquettesAvecUsages
    , selectFocused : Bool
    }


etiquetteTypeToString : EtiquetteType -> String
etiquetteTypeToString etiquetteType =
    case etiquetteType of
        Activites ->
            "activite"

        Zones ->
            "localisation"

        Mots ->
            "mot_cle"


etiquetteTypeToDisplay : EtiquetteType -> String
etiquetteTypeToDisplay etiquetteType =
    case etiquetteType of
        Activites ->
            "Activités réelles et filières"

        Zones ->
            "Zones géographiques"

        Mots ->
            "Mots-clés"


stringToEtiquetteType : String -> Maybe EtiquetteType
stringToEtiquetteType string =
    case string of
        "activite" ->
            Just Activites

        "localisation" ->
            Just Zones

        "mot_cle" ->
            Just Mots

        _ ->
            Nothing


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page _ user =
    Spa.Page.onNewFlags (\_ -> NoOp) <|
        Spa.Page.element
            { view = view user
            , init = init
            , update = update
            , subscriptions =
                \model ->
                    case model.nouvelleEtiquetteModal of
                        Nothing ->
                            Sub.none

                        Just { selectFocused } ->
                            if selectFocused then
                                Browser.Events.onMouseDown (clickedOutsideSelect nouvelleEtiquetteSelectId)

                            else
                                Sub.none
            }


view : User -> Model -> View Msg
view user model =
    { title = UI.Layout.pageTitle "Gestion des étiquettes"
    , body = [ body user model ]
    , route = Route.ParamsEtiquettes
    }


body : User -> Model -> Html Msg
body user model =
    let
        equipeId =
            user
                |> User.role
                |> Data.Role.equipe
                |> Maybe.map .id
                |> Maybe.withDefault Api.EntityId.newId

        zonages =
            model.zonages
                |> RD.withDefault []

        zonagesEnCours =
            zonages
                |> List.filter .enCours
    in
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ viewDeleteModal model.deleteRequest
        , viewEditModal model.etiquettes model.editRequest
        , viewEtiquetteModal model.etiquettes model.nouvelleEtiquetteModal
        , Html.map ZonagesMsg <|
            Zonages.view
                (model.etiquettes
                    |> RD.map (.localisations >> List.map .label)
                    |> RD.withDefault []
                )
                model.zonagesModel
        , div [ DSFR.Grid.col3 ] [ UI.Layout.parametresSideMenu Route.ParamsEtiquettes ]
        , div [ DSFR.Grid.col9 ]
            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col12, Attrs.class "p-4 sm:p-8" ]
                    [ div [ Attrs.class "flex flex-col gap-4 fr-card--white p-4" ]
                        [ div [ Attrs.class "flex flex-col lg:flex-row justify-between" ]
                            [ h1 [ Typo.fr_h2, Attrs.class "!mb-0" ] [ text "Gestion des étiquettes de qualification" ]
                            , DSFR.Button.new { label = "Ajouter une étiquette", onClick = Just ClickedNouvelleEtiquette }
                                |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                                |> DSFR.Button.view
                            ]
                        , callout Nothing Nothing <|
                            div []
                                [ p []
                                    [ text "Les modifications de qualification impactent l'ensemble des utilisateurs de votre équipe de référence. Nous recommandons de les évoquer en réunion d'équipe avant toute modification individuelle."
                                    ]
                                , br []
                                , p []
                                    [ text "Pourquoi utiliser des étiquettes de qualification dans Deveco\u{00A0}?"
                                    , text " "
                                    , text "Retrouvez plus d'explications sur le "
                                    , lienTutoriel False
                                    , text "."
                                    ]
                                ]
                        , let
                            nombreZonagesEnCours =
                                List.length zonagesEnCours
                          in
                          viewIf (nombreZonagesEnCours > 0) <|
                            callout Nothing Nothing <|
                                div []
                                    [ text "Le"
                                    , text <| plural nombreZonagesEnCours
                                    , text " zonage"
                                    , text <| plural nombreZonagesEnCours
                                    , text " suivant"
                                    , text <|
                                        if nombreZonagesEnCours > 1 then
                                            " sont"

                                        else
                                            " est"
                                    , text " en cours d'ajout, et les étiquettes ne sont pas toutes assignées\u{00A0}: "
                                    , text <|
                                        String.join ", " <|
                                            List.map .nom <|
                                                List.sortBy .nom <|
                                                    zonagesEnCours
                                    , text "."
                                    ]
                        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, Attrs.class "!mb-4" ] <|
                            case model.etiquettes of
                                RD.Success { activites, localisations, motsCles } ->
                                    [ div [ DSFR.Grid.col12 ] <|
                                        viewEtiquettes zonages equipeId Activites <|
                                            activites
                                    , div [ DSFR.Grid.col12 ] <|
                                        viewEtiquettes zonages equipeId Zones <|
                                            localisations
                                    , div [ DSFR.Grid.col12 ] <|
                                        viewEtiquettes zonages equipeId Mots <|
                                            motsCles
                                    ]

                                RD.Loading ->
                                    [ div [ DSFR.Grid.col12 ] [ text "Chargement en cours..." ] ]

                                RD.Failure _ ->
                                    [ div [ DSFR.Grid.col12 ] [ text "Erreur, veuillez contacter l'équipe de développement." ] ]

                                _ ->
                                    [ div [ DSFR.Grid.col12 ] [ text "Quelque chose aurait dû se passer..." ] ]
                        ]
                    ]
                ]
            ]
        ]


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init () =
    ( { etiquettes = RD.Loading
      , editRequest = Nothing
      , deleteRequest = Nothing
      , zonages = RD.Loading
      , zonagesModel = Nothing
      , zonageDeleteRequest = Nothing
      , nouvelleEtiquetteModal = Nothing
      }
    , Effect.batch
        [ getCurrentEtiquettes
        , getZonages
        ]
    )
        |> Shared.pageChangeEffects


getZonages : Effect.Effect Shared.Msg Msg
getZonages =
    Effect.fromCmd <|
        get
            { url = Api.getZonages
            }
            { toShared = SharedMsg
            , logger = Nothing
            , handler = ReceivedZonages
            }
            (Decode.list decodeZonage)


getCurrentEtiquettes : Effect.Effect sharedMsg Msg
getCurrentEtiquettes =
    Effect.fromCmd <|
        get
            { url = Api.getEquipeEtiquettes
            }
            { toShared = SharedMsg
            , logger = Nothing
            , handler = ReceivedCurrentEtiquettes
            }
            (Decode.field "etiquettes" decodeEtiquettesAvecUsages)


editEtiquette : EtiquetteAvecUsages -> String -> Effect.Effect sharedMsg Msg
editEtiquette { id } label =
    Effect.fromCmd <|
        post
            { url = Api.getEquipeEtiquette id
            , body =
                [ ( "nom", Encode.string label )
                ]
                    |> Encode.object
                    |> Http.jsonBody
            }
            { toShared = SharedMsg
            , logger = Nothing
            , handler = ReceivedEdit
            }
            (Decode.field "etiquettes" decodeEtiquettesAvecUsages)


deleteEtiquette : EtiquetteAvecUsages -> Effect.Effect sharedMsg Msg
deleteEtiquette etiquette =
    Effect.fromCmd <|
        delete
            { url = Api.getEquipeEtiquette etiquette.id
            , body = Http.emptyBody
            }
            { toShared = SharedMsg
            , logger = Nothing
            , handler = ReceivedDelete
            }
            (Decode.field "etiquettes" decodeEtiquettesAvecUsages)


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ZonagesMsg zonagesMsg ->
            case model.zonagesModel of
                Nothing ->
                    model
                        |> Effect.withNone

                Just zonagesModel ->
                    let
                        ( newZonagesModel, zonagesEffect ) =
                            Zonages.update zonagesMsg zonagesModel

                        termine =
                            Zonages.termine newZonagesModel

                        zonageAjoute =
                            Zonages.zonageAjoute newZonagesModel
                    in
                    ( { model
                        | zonagesModel =
                            if termine then
                                Nothing

                            else
                                Just newZonagesModel
                        , etiquettes =
                            if zonageAjoute then
                                RD.Loading

                            else
                                model.etiquettes
                        , zonages =
                            if zonageAjoute then
                                RD.Loading

                            else
                                model.zonages
                      }
                    , Effect.batch
                        [ Effect.map ZonagesMsg zonagesEffect
                        , if zonageAjoute then
                            Effect.batch
                                [ getZonages
                                , getCurrentEtiquettes
                                ]

                          else
                            Effect.none
                        ]
                    )

        ReceivedZonages zonages ->
            { model | zonages = zonages }
                |> Effect.withNone

        ReceivedCurrentEtiquettes etiquettes ->
            { model | etiquettes = etiquettes }
                |> Effect.withNone

        ClickedEdit equipeId etiquetteType etiquette ->
            ( { model
                | editRequest =
                    Just <|
                        EditRequest RD.NotAsked equipeId etiquetteType etiquette etiquette.label
              }
            , Effect.none
            )

        UpdateEditEtiquette label ->
            ( { model
                | editRequest =
                    model.editRequest
                        |> Maybe.map (\er -> { er | label = label })
              }
            , Effect.none
            )

        CanceledEdit ->
            ( { model | editRequest = Nothing }
            , Effect.none
            )

        ConfirmedEdit ->
            case model.editRequest of
                Nothing ->
                    model |> Effect.withNone

                Just er ->
                    ( { model | editRequest = Just <| { er | request = RD.Loading } }, editEtiquette er.etiquette er.label )

        ReceivedEdit response ->
            case response of
                RD.Success _ ->
                    { model
                        | editRequest = Nothing
                        , etiquettes = response
                        , zonages = RD.Loading
                    }
                        |> Effect.with getZonages

                _ ->
                    { model
                        | editRequest =
                            model.editRequest
                                |> Maybe.map (\er -> { er | request = response })
                    }
                        |> Effect.withNone

        ClickedDelete equipeId etiquetteType etiquette ->
            ( { model
                | deleteRequest =
                    Just <|
                        DeleteRequest RD.NotAsked equipeId etiquetteType etiquette
              }
            , Effect.none
            )

        CanceledDelete ->
            ( { model | deleteRequest = Nothing }
            , Effect.none
            )

        ConfirmedDelete ->
            case model.deleteRequest of
                Nothing ->
                    model |> Effect.withNone

                Just dr ->
                    ( { model | deleteRequest = Just <| { dr | request = RD.Loading } }, deleteEtiquette dr.etiquette )

        ReceivedDelete response ->
            case response of
                RD.Success _ ->
                    { model
                        | deleteRequest = Nothing
                        , etiquettes = response
                        , zonages = RD.Loading
                    }
                        |> Effect.with getZonages

                _ ->
                    { model
                        | deleteRequest =
                            model.deleteRequest
                                |> Maybe.map (\dr -> { dr | request = response })
                    }
                        |> Effect.withNone

        ClickedAddZonage ->
            { model | zonagesModel = Just <| Zonages.init Nothing }
                |> Effect.withNone

        ClickedEditZonage zonage ->
            { model | zonagesModel = Just <| Zonages.init (Just zonage) }
                |> Effect.withNone

        ClickedNouvelleEtiquette ->
            ( { model
                | nouvelleEtiquetteModal = Just <| NouvelleEtiquette Activites "" RD.NotAsked False
              }
            , Effect.none
            )

        CanceledNouvelleEtiquette ->
            ( { model
                | nouvelleEtiquetteModal = Nothing
              }
            , Effect.none
            )

        UpdateNouvelleEtiquetteType type_ ->
            ( { model
                | nouvelleEtiquetteModal =
                    model.nouvelleEtiquetteModal |> Maybe.map (\m -> { m | type_ = type_ })
              }
            , Effect.none
            )

        NouvelleEtiquetteLabelFocused focused ->
            ( { model
                | nouvelleEtiquetteModal =
                    model.nouvelleEtiquetteModal |> Maybe.map (\m -> { m | selectFocused = focused })
              }
            , Effect.none
            )

        UpdateNouvelleEtiquetteLabel label ->
            ( { model
                | nouvelleEtiquetteModal =
                    model.nouvelleEtiquetteModal |> Maybe.map (\m -> { m | label = label })
              }
            , Effect.none
            )

        ConfirmedNouvelleEtiquette ->
            ( { model
                | nouvelleEtiquetteModal =
                    model.nouvelleEtiquetteModal
                        |> Maybe.map (\m -> { m | request = RD.Loading })
              }
            , saveNouvelleEtiquette model.nouvelleEtiquetteModal
            )

        ReceivedNouvelleEtiquette response ->
            case response of
                RD.Success etiquettes ->
                    ( { model
                        | nouvelleEtiquetteModal = Nothing
                        , etiquettes = RD.Success etiquettes
                      }
                    , Effect.none
                    )

                _ ->
                    ( { model
                        | nouvelleEtiquetteModal =
                            model.nouvelleEtiquetteModal
                                |> Maybe.map
                                    (\m ->
                                        { m | request = response }
                                    )
                      }
                    , Effect.none
                    )


viewEtiquettes : List Zonage -> EntityId EquipeId -> EtiquetteType -> List EtiquetteAvecUsages -> List (Html Msg)
viewEtiquettes zonages territoryId etiquetteType etiquettes =
    List.singleton <|
        div [ Attrs.class "p-2" ] <|
            [ h2 [ Typo.fr_h4, Attrs.class "flex flex-row justify-between" ]
                [ text <| etiquetteTypeToDisplay etiquetteType
                , if etiquetteType == Zones then
                    DSFR.Button.new { label = "Ajouter un zonage", onClick = Just ClickedAddZonage }
                        |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                        |> DSFR.Button.view

                  else
                    nothing
                ]
            , case etiquettes of
                [] ->
                    span [ Attrs.class "italic" ] [ text "Aucune étiquette." ]

                _ ->
                    DSFR.Accordion.raw
                        { id = "accordion-" ++ etiquetteTypeToString etiquetteType
                        , title =
                            text "voir "
                                :: (if List.length etiquettes > 1 then
                                        [ text "les ", text <| String.fromInt <| List.length etiquettes, text " étiquettes" ]

                                    else
                                        [ text "l'étiquette"
                                        ]
                                   )
                        , content =
                            List.singleton <|
                                div [ Attrs.class "flex flex-col gap-2" ] <|
                                    List.map (viewEtiquette zonages territoryId etiquetteType) <|
                                        etiquettes
                        , borderless = False
                        }
            ]


viewEtiquette : List Zonage -> EntityId EquipeId -> EtiquetteType -> EtiquetteAvecUsages -> Html Msg
viewEtiquette zonages equipeId etiquetteType ({ label, usages } as etiquette) =
    let
        zonage =
            zonages
                |> List.filter (\z -> z.nom == etiquette.label)
                |> List.head

        zonageButtons =
            case zonage of
                Just z ->
                    [ DSFR.Button.new
                        { label = "Télécharger"
                        , onClick = Nothing
                        }
                        |> DSFR.Button.linkButtonExternal (Api.downloadZonage z.id)
                        |> DSFR.Button.leftIcon downloadLine
                        |> DSFR.Button.withAttrs [ Attrs.class "!mb-0" ]
                        |> DSFR.Button.secondary
                    , DSFR.Button.new
                        { label = "Mettre à jour"
                        , onClick = Just <| ClickedEditZonage z
                        }
                        |> DSFR.Button.leftIcon uploadLine
                        |> DSFR.Button.withAttrs [ Attrs.class "!mb-0" ]
                        |> DSFR.Button.primary
                    ]

                Nothing ->
                    []
    in
    div [ Attrs.class "flex flex-row gap-4 items-center justify-between" ]
        [ div [ Attrs.class "shrink-0 max-w-[35%]" ]
            [ span [ Typo.textBold ] [ text label ]
            , text " "
            , text "(utilisé "
            , text <| String.fromInt <| usages
            , text " fois"
            , text <|
                if zonage /= Nothing then
                    " manuellement "

                else
                    ""
            , text ")"
            ]
        , hr [ Attrs.class "fr-hr h-[1px] w-full !border-solid !border-1 !p-0 shrink-1" ] []
        , (zonageButtons
            ++ [ DSFR.Button.new
                    { label = "Modifier"
                    , onClick =
                        Just <|
                            ClickedEdit equipeId etiquetteType etiquette
                    }
                    |> DSFR.Button.withAttrs [ Attrs.class "!mb-0" ]
                    |> DSFR.Button.onlyIcon editFill
               , DSFR.Button.new
                    { label = "Supprimer"
                    , onClick =
                        Just <|
                            ClickedDelete equipeId etiquetteType etiquette
                    }
                    |> DSFR.Button.withAttrs [ Attrs.class "!mb-0" ]
                    |> DSFR.Button.onlyIcon deleteFill
                    |> DSFR.Button.secondary
               ]
          )
            |> DSFR.Button.group
            |> DSFR.Button.inline
            |> DSFR.Button.iconsLeft
            |> DSFR.Button.viewGroup
            |> List.singleton
            |> div [ Attrs.class "shrink-0 max-w-[60%]" ]
        ]


viewEditModal : WebData EtiquettesAvecUsages -> Maybe EditRequest -> Html Msg
viewEditModal etiquettes editRequest =
    let
        ( opened, content, title ) =
            case editRequest of
                Nothing ->
                    ( False, nothing, nothing )

                Just er ->
                    ( True
                    , let
                        labelDejaUtilise =
                            etiquettes
                                |> RD.map (getEtiquettesForType er.etiquetteType)
                                |> RD.map (List.map .label)
                                |> RD.map (List.filter ((/=) er.etiquette.label))
                                |> RD.map (List.map String.toLower >> List.member (String.toLower er.label))
                                |> RD.withDefault True
                      in
                      div [ Attrs.class "flex flex-col gap-4" ]
                        [ if er.etiquette.usages > 0 then
                            p []
                                [ text "Cette qualification est utilisée par "
                                , text <| String.fromInt <| er.etiquette.usages
                                , text " entreprise"
                                , text <| plural er.etiquette.usages
                                , text " ou créateur"
                                , text <| plural er.etiquette.usages
                                , text " d'entreprises."
                                ]

                          else
                            text "Cette qualification n'est pas utilisée."
                        , DSFR.Input.new
                            { label = text "Nom"
                            , value = er.label
                            , onInput = UpdateEditEtiquette
                            , name = "edit-etiquette-nom"
                            }
                            |> DSFR.Input.withExtraAttrs
                                [ attributeIf labelDejaUtilise <| Attrs.title "Ce nom est déjà pris"
                                , attributeIf labelDejaUtilise <| Attrs.class "fr-input-group--error"
                                ]
                            |> DSFR.Input.view
                        , viewIf labelDejaUtilise <|
                            div [ DSFR.Grid.col12 ]
                                [ p [ Attrs.class "fr-text-default--error" ]
                                    [ text "Il existe déjà une étiquette de type «\u{00A0}"
                                    , text <| etiquetteTypeToDisplay er.etiquetteType
                                    , text "\u{00A0}» avec ce nom."
                                    ]
                                ]
                        , DSFR.Button.group
                            [ DSFR.Button.new
                                { onClick = Just ConfirmedEdit
                                , label = "Confirmer la modification"
                                }
                                |> DSFR.Button.withDisabled (labelDejaUtilise || er.request == RD.Loading || er.etiquette.label == er.label)
                            , DSFR.Button.new { onClick = Just CanceledEdit, label = "Annuler" }
                                |> DSFR.Button.secondary
                            ]
                            |> DSFR.Button.inline
                            |> DSFR.Button.alignedRightInverted
                            |> DSFR.Button.viewGroup
                        ]
                    , div [] [ h2 [] [ text "Modification de «\u{00A0}", text er.etiquette.label, text "\u{00A0}»" ] ]
                    )
    in
    DSFR.Modal.view
        { id = "edit-etiquette-modal"
        , label = "edit-etiquette-modal"
        , openMsg = NoOp
        , closeMsg = Just CanceledEdit
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


viewDeleteModal : Maybe DeleteRequest -> Html Msg
viewDeleteModal dr =
    let
        ( opened, content, title ) =
            case dr of
                Nothing ->
                    ( False, nothing, nothing )

                Just { etiquette, request } ->
                    ( True
                    , div [ Attrs.class "flex flex-col gap-4" ]
                        [ if etiquette.usages > 0 then
                            div []
                                [ p []
                                    [ text "Cette qualification est utilisée par "
                                    , text <| String.fromInt <| etiquette.usages
                                    , text " entreprise"
                                    , text <| plural etiquette.usages
                                    , text " ou créateur"
                                    , text <| plural etiquette.usages
                                    , text " d'entreprises."
                                    ]
                                , p [] [ text "Confirmez-vous la suppression\u{00A0}?" ]
                                ]

                          else
                            text "Cette qualification n'est pas utilisée."
                        , DSFR.Button.group
                            [ DSFR.Button.new
                                { onClick = Just ConfirmedDelete
                                , label = "Confirmer la suppression"
                                }
                                |> DSFR.Button.withDisabled (request == RD.Loading)
                            , DSFR.Button.new { onClick = Just CanceledDelete, label = "Annuler" }
                                |> DSFR.Button.secondary
                            ]
                            |> DSFR.Button.inline
                            |> DSFR.Button.alignedRightInverted
                            |> DSFR.Button.viewGroup
                        ]
                    , div [] [ h2 [] [ text "Suppression de «\u{00A0}", text etiquette.label, text "\u{00A0}»" ] ]
                    )
    in
    DSFR.Modal.view
        { id = "delete-etiquette-modal"
        , label = "delete-etiquette-modal"
        , openMsg = NoOp
        , closeMsg = Just CanceledDelete
        , title = title
        , opened = opened
        , size = Nothing
        }
        content
        Nothing
        |> Tuple.first


nouvelleEtiquetteSelectId : String
nouvelleEtiquetteSelectId =
    "nouvelle-etiquette-modal-select"


getEtiquettesForType : EtiquetteType -> EtiquettesAvecUsages -> List EtiquetteAvecUsages
getEtiquettesForType type_ { activites, localisations, motsCles } =
    case type_ of
        Activites ->
            activites

        Zones ->
            localisations

        Mots ->
            motsCles


viewEtiquetteModal : WebData EtiquettesAvecUsages -> Maybe NouvelleEtiquette -> Html Msg
viewEtiquetteModal etiquettesExistantes nouvelleEtiquette =
    let
        ( opened, content, title ) =
            case nouvelleEtiquette of
                Nothing ->
                    ( False, nothing, nothing )

                Just { type_, label, request, selectFocused } ->
                    ( True
                    , let
                        labelVide =
                            label == ""

                        labelsExistants =
                            List.map .label <|
                                RD.withDefault [] <|
                                    RD.map (getEtiquettesForType type_) <|
                                        etiquettesExistantes

                        labelExistant =
                            List.member (String.toLower <| String.trim <| label) <|
                                List.map (String.toLower << String.trim) <|
                                    labelsExistants

                        labelsCorrespondants =
                            List.filter (\l -> String.contains (String.toLower <| String.trim <| label) (String.toLower <| String.trim <| l)) <|
                                List.sort <|
                                    labelsExistants
                      in
                      div [ Attrs.class "flex flex-col gap-4" ]
                        [ DSFR.Input.new
                            { value = etiquetteTypeToString type_
                            , onInput =
                                stringToEtiquetteType
                                    >> Maybe.withDefault Activites
                                    >> UpdateNouvelleEtiquetteType
                            , label = text "Type d'étiquette"
                            , name = "nouvelle-etiquette-modal-type"
                            }
                            |> DSFR.Input.select
                                { options =
                                    [ Activites, Zones, Mots ]
                                , toId = etiquetteTypeToString
                                , toLabel =
                                    etiquetteTypeToDisplay
                                        >> text
                                , toDisabled = Nothing
                                }
                            |> DSFR.Input.view
                        , Html.div
                            [ Attrs.id nouvelleEtiquetteSelectId
                            , Attrs.class "label-and-selector-container fr-input-group"
                            , Html.Events.onClick <| NouvelleEtiquetteLabelFocused True
                            ]
                            [ div []
                                [ DSFR.Input.new
                                    { value = label
                                    , onInput =
                                        UpdateNouvelleEtiquetteLabel
                                    , label = text "Nom de l'étiquette"
                                    , name = "nouvelle-etiquette-modal-nom"
                                    }
                                    |> DSFR.Input.view
                                ]
                            , if selectFocused then
                                div
                                    [ Attrs.class "w-full border-2 border-black relative p-2 top-[6px]"
                                    ]
                                    [ div [ Attrs.class "max-h-[60px] overflow-auto" ]
                                        (List.map
                                            (\option ->
                                                div
                                                    [ Attrs.class "p-2"
                                                    ]
                                                    [ div [] [ option ]
                                                    ]
                                            )
                                         <|
                                            List.filterMap identity <|
                                                (::)
                                                    (if label /= "" && not labelExistant then
                                                        Just <|
                                                            Html.span
                                                                [ Attrs.class "cursor-pointer"
                                                                , Typo.textBold
                                                                , Html.Events.stopPropagationOn "click" (Decode.succeed ( NouvelleEtiquetteLabelFocused False, True ))
                                                                ]
                                                                [ text "Créer l'étiquette"
                                                                , text " "
                                                                , text label
                                                                , text "\u{00A0}?"
                                                                ]

                                                     else if labelExistant then
                                                        Just <|
                                                            span [ Attrs.class "text-error", Typo.textBold ]
                                                                [ text "Cette étiquette existe déjà"
                                                                ]

                                                     else
                                                        Nothing
                                                    )
                                                <|
                                                    (::)
                                                        (if List.length labelsCorrespondants > 0 then
                                                            Just <|
                                                                span [ Attrs.class "italic" ]
                                                                    [ text "Étiquette"
                                                                    , text <| plural <| List.length labelsCorrespondants
                                                                    , text " existante"
                                                                    , text <| plural <| List.length labelsCorrespondants
                                                                    ]

                                                         else
                                                            Nothing
                                                        )
                                                    <|
                                                        List.map (Just << text) <|
                                                            labelsCorrespondants
                                        )
                                    ]

                              else
                                text ""
                            ]
                        , DSFR.Button.group
                            [ DSFR.Button.new
                                { onClick = Just ConfirmedNouvelleEtiquette
                                , label = "Créer l'étiquette"
                                }
                                |> DSFR.Button.withDisabled (request == RD.Loading || labelVide || labelExistant || selectFocused)
                            , DSFR.Button.new { onClick = Just CanceledNouvelleEtiquette, label = "Annuler" }
                                |> DSFR.Button.secondary
                            ]
                            |> DSFR.Button.inline
                            |> DSFR.Button.alignedRightInverted
                            |> DSFR.Button.viewGroup
                        ]
                    , div [] [ h2 [] [ text "Ajouter une étiquette" ] ]
                    )
    in
    DSFR.Modal.view
        { id = "nouvelle-etiquette-modal"
        , label = "nouvelle-etiquette-modal"
        , openMsg = NoOp
        , closeMsg = Just CanceledNouvelleEtiquette
        , title = title
        , opened = opened
        , size = Just DSFR.Modal.md
        }
        content
        Nothing
        |> Tuple.first


saveNouvelleEtiquette : Maybe NouvelleEtiquette -> Effect.Effect sharedMsg Msg
saveNouvelleEtiquette nouvelleEtiquette =
    Effect.fromCmd <|
        Maybe.withDefault Cmd.none <|
            Maybe.map
                (\{ type_, label } ->
                    post
                        { url = Api.getEquipeEtiquettes
                        , body =
                            [ ( "type", Encode.string <| etiquetteTypeToString type_ )
                            , ( "nom", Encode.string label )
                            ]
                                |> Encode.object
                                |> Http.jsonBody
                        }
                        { toShared = SharedMsg
                        , logger = Nothing
                        , handler = ReceivedNouvelleEtiquette
                        }
                        (Decode.field "etiquettes" decodeEtiquettesAvecUsages)
                )
            <|
                nouvelleEtiquette


clickedOutsideSelect : String -> Decode.Decoder Msg
clickedOutsideSelect componentId =
    Decode.field "target" (eventIsOutsideComponent componentId)
        |> Decode.andThen
            (\isOutside ->
                if isOutside then
                    Decode.succeed <| NouvelleEtiquetteLabelFocused False

                else
                    Decode.fail "inside component"
            )


eventIsOutsideComponent : String -> Decode.Decoder Bool
eventIsOutsideComponent componentId =
    Decode.oneOf
        [ Decode.field "id" Decode.string
            |> Decode.andThen
                (\id ->
                    if componentId == id then
                        -- found match by id
                        Decode.succeed False

                    else
                        -- try next decoder
                        Decode.fail "check parent node"
                )
        , Decode.lazy (\_ -> eventIsOutsideComponent componentId |> Decode.field "parentNode")

        -- fallback if all previous decoders failed
        , Decode.succeed True
        ]
