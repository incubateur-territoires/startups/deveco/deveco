module Pages.Parametres.Import exposing (Model, Msg, page)

import Accessibility exposing (Html, div, h1, li, ol, p, span, text, ul)
import DSFR.Button
import DSFR.Grid
import DSFR.Icons.System
import DSFR.Typography as Typo
import Effect
import Html.Attributes exposing (class, download)
import Lib.Variables exposing (contactEmail)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


type alias Model =
    { appUrl : String
    }


type Msg
    = NoOp


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page { appUrl } _ =
    Spa.Page.onNewFlags (\_ -> NoOp) <|
        Spa.Page.element
            { view = view
            , init = init appUrl
            , update = update
            , subscriptions = \_ -> Sub.none
            }


init : String -> () -> ( Model, Effect.Effect Shared.Msg Msg )
init appUrl () =
    ( { appUrl = appUrl
      }
    , Effect.none
    )
        |> Shared.pageChangeEffects


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone


view : Model -> View Msg
view _ =
    { title = UI.Layout.pageTitle "Importer mes données"
    , body = [ body ]
    , route = Route.ParamsImport
    }


body : Html Msg
body =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col3 ] [ UI.Layout.parametresSideMenu Route.ParamsImport ]
        , div [ DSFR.Grid.col9 ]
            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ] <|
                [ div [ DSFR.Grid.col12, class "p-4 sm:p-8" ]
                    [ div [ class "fr-card--white p-4" ]
                        [ h1 [ Typo.fr_h2 ] [ text "Importer mes données" ]
                        , div [ class "flex flex-col gap-4" ]
                            [ ol []
                                [ li []
                                    [ span [ Typo.textBold ] [ text "Lisez les bonnes pratiques." ]
                                    , text " C’est un pré-requis pour bien remplir le modèle et que celui-ci soit conforme."
                                    ]
                                , li [ Typo.textBold ] [ text "Téléchargez le modèle." ]
                                , li []
                                    [ span [ Typo.textBold ] [ text "Remplissez scrupuleusement le tableau" ]
                                    , text " en suivant le mode d’emploi et les formats indiqués\u{00A0}: 14 chiffres pour un SIRET, un «\u{00A0};\u{00A0}» entre deux étiquettes de qualification, etc."
                                    ]
                                , li []
                                    [ span [ Typo.textBold ] [ text "Envoyez le modèle rempli à " ]
                                    , text " à "
                                    , Typo.link ("mailto:" ++ contactEmail) [] [ text contactEmail ]
                                    , text "."
                                    ]
                                , ol []
                                    [ li []
                                        [ span [ Typo.textBold ] [ text "Si votre fichier est conforme," ]
                                        , text " il sera intégré dans votre outil Deveco et vous recevrez un email de confirmation."
                                        ]
                                    , li []
                                        [ span [ Typo.textBold ] [ text "Si votre fichier n’est pas conforme" ]
                                        , text " il vous sera retourné par email avec les points qui doivent être modifiés."
                                        ]
                                    ]
                                ]
                            , div [ class "fr-highlight" ]
                                [ p []
                                    [ text "Pour toute question concernant le remplissage du modèle d’import, n’hésitez pas à contacter l’équipe Deveco\u{00A0}:"
                                    ]
                                , ul []
                                    [ li []
                                        [ Typo.link ("mailto:" ++ contactEmail) [] [ text contactEmail ]
                                        , text "\u{00A0};"
                                        ]
                                    , li []
                                        [ text "sur le chat de l’outil Deveco."
                                        ]
                                    ]
                                ]
                            ]
                        , DSFR.Button.group
                            [ DSFR.Button.new
                                { onClick = Nothing
                                , label = "Modèle d'import"
                                }
                                |> DSFR.Button.withAttrs [ download "" ]
                                |> DSFR.Button.linkButton "/assets/modele-import-Deveco.xlsx"
                                |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                            , DSFR.Button.new
                                { onClick = Nothing
                                , label = "Bonnes pratiques"
                                }
                                |> DSFR.Button.withAttrs [ download "" ]
                                |> DSFR.Button.linkButton "/assets/bonnes_pratiques_import_deveco.pdf"
                                |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                            ]
                            |> DSFR.Button.inline
                            |> DSFR.Button.iconsLeft
                            |> DSFR.Button.alignedRightInverted
                            |> DSFR.Button.viewGroup
                        ]
                    ]
                ]
            ]
        ]
