module User exposing (User, UserId, aUnMotDePasse, cgu, decodeUser, email, id, montrerTutoriel, nom, prenom, role)

import Api.EntityId
import Data.Role exposing (Role)
import Json.Decode as Decode
import Json.Decode.Extra exposing (andMap, optionalNullableField)


type User
    = User UserData


type UserId
    = UserId


type alias UserData =
    { id : Api.EntityId.EntityId UserId
    , email : String
    , nom : String
    , prenom : String
    , aUnMotDePasse : Bool
    , montrerTutoriel : Bool
    , cgu : Bool
    , role : Role
    }


email : User -> String
email (User user) =
    user.email


nom : User -> String
nom (User user) =
    user.nom


prenom : User -> String
prenom (User user) =
    user.prenom


role : User -> Role
role (User user) =
    user.role


id : User -> Api.EntityId.EntityId UserId
id (User user) =
    user.id


aUnMotDePasse : User -> Bool
aUnMotDePasse (User user) =
    user.aUnMotDePasse


montrerTutoriel : User -> Bool
montrerTutoriel (User user) =
    user.montrerTutoriel


cgu : User -> Bool
cgu (User user) =
    user.cgu


decodeUser : Decode.Decoder User
decodeUser =
    Decode.succeed UserData
        |> andMap (Decode.field "compte" <| Decode.field "id" <| Api.EntityId.decodeEntityId)
        |> andMap (Decode.field "compte" <| Decode.field "email" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.field "compte" <| Decode.field "nom" <| Decode.maybe Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.field "compte" <| Decode.field "prenom" <| Decode.maybe Decode.string)
        |> andMap (Decode.field "aUnMotDePasse" Decode.bool)
        |> andMap (Decode.map (Maybe.withDefault False) <| optionalNullableField "montrerTutoriel" Decode.bool)
        |> andMap (Decode.map (Maybe.withDefault False) <| Decode.field "compte" <| optionalNullableField "cgu" Decode.bool)
        |> andMap Data.Role.decodeRole
        |> Decode.map User
