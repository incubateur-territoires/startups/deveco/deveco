module Api.EntityId exposing (EntityId, compare, decodeEntityId, encodeEntityId, entityIdParser, entityIdToInt, entityIdToString, newId, parseEntityId, unsafeConvert)

import Json.Decode as Decode
import Json.Encode as Encode exposing (Value)
import Url.Parser as Parser


type EntityId entity
    = EntityId Int


newId : EntityId entity
newId =
    EntityId -1


decodeEntityId : Decode.Decoder (EntityId entity)
decodeEntityId =
    Decode.map EntityId Decode.int


parseEntityId : String -> Maybe (EntityId entity)
parseEntityId =
    String.toInt >> Maybe.map EntityId


entityIdToInt : EntityId entity -> Int
entityIdToInt (EntityId id) =
    id


entityIdToString : EntityId entity -> String
entityIdToString (EntityId id) =
    String.fromInt id


encodeEntityId : EntityId entity -> Value
encodeEntityId =
    entityIdToString >> Encode.string


entityIdParser : Parser.Parser (EntityId entity -> b) b
entityIdParser =
    Parser.custom "ENTITY_ID" parseEntityId


compare : EntityId entity -> EntityId entity -> Order
compare (EntityId i1) (EntityId i2) =
    Basics.compare i1 i2


unsafeConvert : EntityId any -> EntityId entity
unsafeConvert (EntityId id) =
    EntityId id
