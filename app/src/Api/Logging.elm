port module Api.Logging exposing (log)

import Json.Encode exposing (Value)


port log : Value -> Cmd msg
