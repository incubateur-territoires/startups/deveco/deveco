module Api.Auth exposing
    ( checkAuth
    , delete
    , get
    , getWithError
    , post
    , request
    )

import Api
import Http
import Json.Decode as Decode exposing (Decoder)
import RemoteData as RD exposing (WebData)
import Shared exposing (LogErrorConfig, Msg, Redirect, TypeError(..))
import User


checkAuth : Bool -> Redirect -> Cmd Msg
checkAuth stayOnPage redir =
    let
        resultToMsg res =
            case res of
                Ok id ->
                    Shared.LoggedIn id redir

                Err _ ->
                    if stayOnPage then
                        Shared.NoOp

                    else
                        Shared.LoggedOut redir
    in
    Http.get
        { url = Api.authStatus
        , expect =
            Http.expectJson resultToMsg <|
                Decode.field "user" User.decodeUser
        }


errorHandling : LogErrorConfig data msg -> WebData data -> msg
errorHandling { toShared, logger, handler } response =
    case response of
        RD.Failure (Http.BadStatus 401) ->
            toShared <| Shared.LoggedOut Nothing

        RD.Failure (Http.BadStatus 403) ->
            toShared <| Shared.LoggedOut Nothing

        RD.Failure (Http.BadStatus statut) ->
            logger
                |> Maybe.map (\l -> l (handler response) { statut = statut, message = "BadStatus " ++ String.fromInt statut, type_ = Serveur })
                |> Maybe.withDefault (handler response)

        RD.Failure Http.Timeout ->
            logger
                |> Maybe.map (\l -> l (handler response) { statut = 0, message = "Timeout serveur", type_ = Serveur })
                |> Maybe.withDefault (handler response)

        RD.Failure (Http.BadUrl url) ->
            logger
                |> Maybe.map (\l -> l (handler response) { statut = 0, message = "Mauvaise URL : " ++ url, type_ = CodeFront })
                |> Maybe.withDefault (handler response)

        RD.Failure (Http.BadBody raison) ->
            logger
                |> Maybe.map (\l -> l (handler response) { statut = 0, message = raison, type_ = Decodeur })
                |> Maybe.withDefault (handler response)

        _ ->
            handler response


request :
    { method : String
    , headers : List Http.Header
    , url : String
    , body : Http.Body
    , timeout : Maybe Float
    , tracker : Maybe String
    }
    -> LogErrorConfig data msg
    -> Decoder data
    -> Cmd msg
request { method, headers, url, body, timeout, tracker } logger decodePayload =
    Http.request
        { method = method
        , headers = headers
        , url = url
        , body = body
        , expect = Http.expectJson (RD.fromResult >> errorHandling logger) decodePayload
        , timeout = timeout
        , tracker = tracker
        }


get :
    { url : String }
    -> LogErrorConfig data msg
    -> Decoder data
    -> Cmd msg
get { url } =
    request
        { method = "GET"
        , headers = []
        , url = url
        , body = Http.emptyBody
        , timeout = Nothing
        , tracker = Nothing
        }


post :
    { url : String, body : Http.Body }
    -> LogErrorConfig data msg
    -> Decoder data
    -> Cmd msg
post { url, body } =
    request
        { method = "POST"
        , headers = []
        , url = url
        , body = body
        , timeout = Nothing
        , tracker = Nothing
        }


delete :
    { url : String, body : Http.Body }
    -> LogErrorConfig data msg
    -> Decoder data
    -> Cmd msg
delete { url, body } =
    request
        { method = "DELETE"
        , headers = []
        , url = url
        , body = body
        , timeout = Nothing
        , tracker = Nothing
        }


getWithError :
    { url : String }
    -> LogErrorConfig (Result String data) msg
    -> Decoder (Result String data)
    -> Cmd msg
getWithError { url } =
    requestWithError
        { method = "GET"
        , headers = []
        , url = url
        , body = Http.emptyBody
        , timeout = Nothing
        , tracker = Nothing
        }


requestWithError :
    { method : String
    , headers : List Http.Header
    , url : String
    , body : Http.Body
    , timeout : Maybe Float
    , tracker : Maybe String
    }
    -> LogErrorConfig (Result String data) msg
    -> Decoder (Result String data)
    -> Cmd msg
requestWithError { method, headers, url, body, timeout, tracker } logger decodePayload =
    Http.request
        { method = method
        , headers = headers
        , url = url
        , body = body
        , expect = expectJsonOrError logger decodePayload
        , timeout = timeout
        , tracker = tracker
        }


expectJsonOrError : LogErrorConfig (Result String data) msg -> Decoder (Result String data) -> Http.Expect msg
expectJsonOrError logger decoder =
    Http.expectStringResponse (RD.fromResult >> errorHandling logger) <|
        \response ->
            case response of
                Http.BadUrl_ url ->
                    Err (Http.BadUrl url)

                Http.Timeout_ ->
                    Err Http.Timeout

                Http.NetworkError_ ->
                    Err Http.NetworkError

                Http.BadStatus_ metadata body ->
                    case Decode.decodeString decoder body of
                        Ok value ->
                            Ok <| value

                        Err _ ->
                            Err (Http.BadStatus metadata.statusCode)

                Http.GoodStatus_ _ body ->
                    case Decode.decodeString decoder body of
                        Ok value ->
                            Ok <| value

                        Err err ->
                            Err (Http.BadBody (Decode.errorToString err))
