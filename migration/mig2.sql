INSERT INTO categorie_juridique_type(id, nom, niveau, parent_categorie_juridique_type_id)
VALUES ('0001', 'indéfini', 3, '00');

-- entreprise_periode

-- personne morale / physique

-- 'F', 'M', 'N'
CREATE TABLE IF NOT EXISTS sexe_type (
    id varchar(1) NOT NULL,
    nom varchar(10) NOT NULL,

    PRIMARY KEY (id)
);
INSERT INTO sexe_type (id, nom)
VALUES
    ('F', 'feminin'),
    ('M', 'masculin'),
    ('N', 'N/D');

-- entreprise type : physique / morale (P, M)
CREATE TABLE IF NOT EXISTS entreprise_type (
    id varchar(1) NOT NULL,
    nom varchar(10) NOT NULL,

    PRIMARY KEY (id)
);
INSERT INTO entreprise_type (id, nom)
VALUES
    ('P', 'physique'),
    ('M', 'morale');

-- entreprise effectif : 'O'
CREATE TABLE IF NOT EXISTS effectif_type (
    id varchar(2) NOT NULL,
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);
INSERT INTO effectif_type (id, nom)
VALUES
  ('NN', 'Unité non employeuse (pas de salarié au cours de l’année de référence et pas d’effectif au 31/12)'),
  ('00', '0 salarié (n’ayant pas d effectif au 31/12 mais ayant employé des salariés au cours de l’année de référence)'),
  ('01', '1 ou 2 salariés'),
  ('02', '3 à 5 salariés'),
  ('03', '6 à 9 salariés'),
  ('11', '10 à 19 salariés'),
  ('12', '20 à 49 salariés'),
  ('21', '50 à 99 salariés'),
  ('22', '100 à 199 salariés'),
  ('31', '200 à 249 salariés'),
  ('32', '250 à 499 salariés'),
  ('41', '500 à 999 salariés'),
  ('42', '1 000 à 1 999 salariés'),
  ('51', '2 000 à 4 999 salariés'),
  ('52', '5 000 à 9 999 salariés'),
  ('53', '10 000 salariés et plus');

-- entreprise catégorie : PME, ETI, GE
CREATE TABLE IF NOT EXISTS categorie_entreprise_type (
    id varchar(3) NOT NULL,
    nom varchar NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO categorie_entreprise_type (id, nom)
VALUES
    ('PME', 'petite ou moyenne entreprise'),
    ('ETI', 'entreprise de taille intermédiaire'),
    ('GE', 'grande entreprise');
    -- ept

CREATE TABLE IF NOT EXISTS source_datagouv_commune_ept (
    insee_com varchar(5) NOT NULL,
    insee_reg varchar(2) NOT NULL,
    insee_dep varchar(2) NOT NULL,
    lib_com varchar NOT NULL,
    lib_ept varchar NOT NULL,
    insee_ept varchar(3) NOT NULL,
    siren varchar(9) NOT NULL,

    PRIMARY KEY (insee_com)
);

