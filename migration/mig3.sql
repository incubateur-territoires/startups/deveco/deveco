-- qpv

CREATE TABLE IF NOT EXISTS qpv (
    id varchar(8),
    nom varchar,
    geometrie geometry(MultiPolygon, 4326),

    PRIMARY KEY (id)
);

INSERT INTO qpv(id, nom, geometrie)
SELECT code_qp, nom_qp, geom
FROM source_datagouv_qpv;

-- SELECT AddGeometryColumn('','source_datagouv_qpv','geom','0','MULTIPOLYGON',2);

-- adresse (personne / etablissement / etablissement_creation)

CREATE TABLE IF NOT EXISTS adresse (
    id SERIAL NOT NULL,
    numero varchar,
    voie_nom varchar,
    commune_id varchar(5) NOT NULL,
    geolocalisation geometry(Point, 4326),
    -- champs supplémentaires provenant de la base SIRENE
    code_postal varchar(5),
    distribution_speciale varchar,
    cedex_code varchar(9),
    cedex_nom varchar,

    PRIMARY KEY (id)
);

-- ALTER TABLE adresse
-- ALTER COLUMN geolocalisation
-- TYPE geometry(Point) USING geolocalisation::geometry(Point);

-- entreprise

CREATE TABLE IF NOT EXISTS entreprise (
    id varchar(9) NOT NULL,

    creation_date date,

    effectif_annee int,
    effectif_type_id varchar(2),

    entreprise_type_id varchar(1) NOT NULL,
    diffusible boolean NOT NULL DEFAULT true,

    categorie_entreprise_type_id varchar(3),
    categorie_entreprise_annee int,

    unite_purgee boolean DEFAULT FALSE,

    nom varchar,

    PRIMARY KEY (id)
);

INSERT INTO entreprise (
    id,

    creation_date,

    entreprise_type_id,
    diffusible,

    effectif_annee,
    effectif_type_id,

    categorie_entreprise_type_id,
    categorie_entreprise_annee,

    unite_purgee,

    nom
)
SELECT
    siren,
    U.date_creation_unite_legale,

    CASE
        WHEN U.categorie_juridique_unite_legale = '1000'
        THEN 'P'
        ELSE 'M'
    END,
    CASE
        WHEN U.statut_diffusion_unite_legale = 'O'
        THEN true
        ELSE false
    END,

    U.annee_effectifs_unite_legale,
    U.tranche_effectifs_unite_legale,

    U.categorie_entreprise,
    U.annee_categorie_entreprise,

    U.unite_purgee_unite_legale,

    CONCAT_WS(
        ' ',
        U.pseudonyme_unite_legale,
        COALESCE(
            U.prenom_usuel_unite_legale,
            -- on ne prend que le premier prénom
            -- ils sont très peu remplis, et ce sont souvent les mêmes
            U.prenom1_unite_legale
        ),
        U.sigle_unite_legale,
        COALESCE(
            U.nom_usage_unite_legale,
            U.nom_unite_legale
        ),
        COALESCE(
            U.denomination_usuelle1_unite_legale,
            U.denomination_usuelle2_unite_legale,
            U.denomination_usuelle3_unite_legale,
            U.denomination_unite_legale
        )
    )
FROM source_api_sirene_unite_legale as U;

--

CREATE TABLE IF NOT EXISTS personne_physique (
    entreprise_id varchar(9) NOT NULL,
    sexe_type_id varchar(1),
    prenom_1 varchar,
    prenom_2 varchar,
    prenom_3 varchar,
    prenom_4 varchar,
    prenom_usuel varchar,
    pseudonyme varchar,

    PRIMARY KEY (entreprise_id)
);

INSERT INTO personne_physique (
    entreprise_id,
    sexe_type_id,
    prenom_1,
    prenom_2,
    prenom_3,
    prenom_4,
    prenom_usuel,
    pseudonyme
)
SELECT
    U.siren,
    CASE WHEN U.sexe_unite_legale = '[ND]' THEN NULL ELSE U.sexe_unite_legale END,
    prenom1_unite_legale,
    prenom2_unite_legale,
    prenom3_unite_legale,
    prenom4_unite_legale,
    prenom_usuel_unite_legale,
    pseudonyme_unite_legale
FROM source_api_sirene_unite_legale as U
WHERE U.categorie_juridique_unite_legale = '1000';

CREATE TABLE IF NOT EXISTS personne_morale (
    entreprise_id varchar(9) NOT NULL,
    sigle varchar,
    association_id varchar(10),

    PRIMARY KEY (entreprise_id)
);

INSERT INTO personne_morale (
    entreprise_id,
    sigle,
    association_id
)
SELECT
    U.siren,
    sigle_unite_legale,
    identifiant_association_unite_legale
FROM source_api_sirene_unite_legale as U
WHERE U.categorie_juridique_unite_legale <> '1000';



CREATE TABLE IF NOT EXISTS source_api_sirene_unite_legale_periode (
	  date_debut date,
    date_fin date,

	  siren varchar(9) NOT NULL,
    nic_siege_unite_legale varchar(5),

		nom_unite_legale varchar,
		nom_usage_unite_legale varchar,

    denomination_unite_legale varchar,
    denomination_usuelle_1_unite_legale varchar,
    denomination_usuelle_2_unite_legale varchar,
    denomination_usuelle_3_unite_legale varchar,

    nomenclature_activite_principale_unite_legale varchar(10),
    activite_principale_unite_legale varchar(6),

    categorie_juridique_Unite_legale varchar(4),

    etat_administratif_unite_legale source_sirene_unite_legale_etat_administratif_enum,
    caractere_employeur_unite_legale source_sirene_caractere_employeur_enum,
    economie_sociale_solidaire_unite_legale source_sirene_unite_legale_economie_sociale_solidaire_enum,
    societe_mission_unite_legale source_sirene_unite_legale_societe_mission_enum,

		changement_activite_principale_unite_legale boolean,
		changement_caractere_employeur_unite_legale boolean,
		changement_categorie_juridique_unite_legale boolean,
		changement_denomination_unite_legale boolean,
		changement_denomination_usuelle_unite_legale boolean,
		changement_economie_sociale_solidaire_unite_legale boolean,
		changement_societe_mission_unite_legale boolean,
		changement_etat_administratif_unite_legale boolean,
		changement_nic_siege_unite_legale boolean,
		changement_nom_unite_legale boolean,
		changement_nom_usage_unite_legale boolean,

		mise_a_jour_at timestamptz
);

ALTER TABLE source_api_sirene_unite_legale_periode ADD CONSTRAINT uk_source_api_sirene_unite_legale_periode UNIQUE (siren, date_debut);

INSERT INTO source_api_sirene_unite_legale_periode (
    date_debut,

    siren,
    nic_siege_unite_legale,

    nom_unite_legale,
    nom_usage_unite_legale,

    denomination_unite_legale,
    denomination_usuelle_1_unite_legale,
    denomination_usuelle_2_unite_legale,
    denomination_usuelle_3_unite_legale,

    nomenclature_activite_principale_unite_legale,
    activite_principale_unite_legale,

    categorie_juridique_Unite_legale,

    etat_administratif_unite_legale,
    caractere_employeur_unite_legale,
    economie_sociale_solidaire_unite_legale,
    societe_mission_unite_legale
)
SELECT
    U.date_debut,

    U.siren,
    U.nic_siege_unite_legale,

    nom_unite_legale,
    nom_usage_unite_legale,

    denomination_unite_legale,
    denomination_usuelle1_unite_legale,
    denomination_usuelle2_unite_legale,
    denomination_usuelle3_unite_legale,

    U.nomenclature_activite_principale_unite_legale,
    U.activite_principale_unite_legale,

    U.categorie_juridique_unite_legale,

    U.etat_administratif_unite_legale,
    U.caractere_employeur_unite_legale::source_sirene_caractere_employeur_enum,
    U.economie_sociale_solidaire_unite_legale,
    U.societe_mission_unite_legale
FROM source_api_sirene_unite_legale as U;


-- entreprise_periode

CREATE TABLE IF NOT EXISTS entreprise_periode (
    debut_date date,
    fin_date date,

    entreprise_id varchar(9) NOT NULL,
    siege_etablissement_id varchar(14) NOT NULL,

    nom varchar,
    nom_usage varchar,

    denomination varchar,
    denomination_usuelle_1 varchar,
    denomination_usuelle_2 varchar,
    denomination_usuelle_3 varchar,

    naf_revision_type_id varchar(10),
    naf_type_id varchar(6),

    categorie_juridique_type_id varchar(4) NOT NULL,

    actif boolean DEFAULT false NOT NULL,
    employeur boolean DEFAULT false NOT NULL,
    economie_sociale_solidaire boolean DEFAULT false NOT NULL,
    societe_mission boolean DEFAULT false,

    siege_etablissement_id_change boolean DEFAULT false NOT NULL,

    nom_change boolean DEFAULT false NOT NULL,
    nom_usage_change boolean DEFAULT false NOT NULL,

    denomination_change boolean DEFAULT false NOT NULL,
    denomination_usuelle_change boolean DEFAULT false NOT NULL,

    naf_type_change boolean DEFAULT false NOT NULL,

    categorie_juridique_change boolean DEFAULT false NOT NULL,

    actif_change boolean DEFAULT false NOT NULL,
    employeur_change boolean DEFAULT false NOT NULL,
    economie_sociale_solidaire_change boolean DEFAULT false NOT NULL,
    societe_mission_change boolean DEFAULT false NOT NULL
);

ALTER TABLE entreprise_periode ADD CONSTRAINT uk_entreprise_periode UNIQUE (entreprise_id, debut_date);

INSERT INTO entreprise_periode (
    debut_date,

    entreprise_id,
    siege_etablissement_id,

    nom,
    nom_usage,

    denomination,
    denomination_usuelle_1,
    denomination_usuelle_2,
    denomination_usuelle_3,

    naf_revision_type_id,
    naf_type_id,

    categorie_juridique_type_id,

    actif,
    employeur,
    economie_sociale_solidaire,
    societe_mission
)
SELECT
    U.date_debut,

    U.siren,
    U.siren || U.nic_siege_unite_legale,

    nom_unite_legale,
    nom_usage_unite_legale,

    denomination_unite_legale,
    denomination_usuelle1_unite_legale,
    denomination_usuelle2_unite_legale,
    denomination_usuelle3_unite_legale,

    U.nomenclature_activite_principale_unite_legale,
    U.activite_principale_unite_legale,

    U.categorie_juridique_unite_legale,

    CASE
        WHEN U.etat_administratif_unite_legale = 'A'
        THEN true
        ELSE false
    END,
    CASE
        WHEN U.caractere_employeur_unite_legale = 'O'
        THEN true
        ELSE false
    END,
    CASE
        WHEN U.economie_sociale_solidaire_unite_legale = 'O'
        THEN true
        ELSE false
    END,

    CASE
        WHEN U.societe_mission_unite_legale = 'O'
        THEN true
        ELSE false
    END
FROM source_api_sirene_unite_legale as U;

-- mandataires sociaux

CREATE TABLE IF NOT EXISTS source_api_entreprise_entreprise_mandataire_social (
    entreprise_id varchar(9) NOT NULL,
    payload jsonb NOT NULL,

    PRIMARY KEY (entreprise_id)
);

INSERT INTO source_api_entreprise_entreprise_mandataire_social (
    entreprise_id,
    payload
)
SELECT
    DISTINCT ON (SUBSTRING(siret, 1, 9))
    SUBSTRING(siret, 1, 9),
    mandataires_sociaux
FROM old.old_entreprise
WHERE
    mandataires_sociaux IS NOT NULL
    AND mandataires_sociaux <> '[]'
ORDER BY SUBSTRING(siret, 1, 9), last_api_update DESC;

CREATE TABLE IF NOT EXISTS mandataire_personne_physique (
    id SERIAL NOT NULL,

    entreprise_id varchar(9) NOT NULL,

    nom varchar NOT NULL,
    prenom varchar,
    fonction varchar,

    naissance_date date,
    naissance_lieu varchar,
    naissance_pays varchar,
    naissance_pays_code varchar,
    nationalite varchar,
    nationalite_code varchar,

    PRIMARY KEY (id)
);

INSERT INTO mandataire_personne_physique (
    entreprise_id,
    nom,
    prenom,
    fonction,
    naissance_date,
    naissance_lieu,
    naissance_pays,
    naissance_pays_code,
    nationalite,
    nationalite_code
)
SELECT
    entreprise_id,
    nom,
    prenom,
    fonction,
    CASE WHEN date_naissance = '' THEN NULL ELSE date_naissance::date END,
    lieu_naissance,
    pays_naissance,
    code_pays_naissance,
    nationalite,
    code_nationalite
FROM source_api_entreprise_entreprise_mandataire_social S,
    jsonb_to_recordset(S.payload) as mandataire(
        siren varchar,
        nom varchar,
        prenom varchar,
        type varchar,
        fonction varchar,
        date_naissance varchar,
        lieu_naissance varchar,
        pays_naissance varchar,
        code_pays_naissance varchar,
        nationalite varchar,
        code_nationalite varchar
    )
WHERE type IN ('PP', 'personne_physique');

CREATE TABLE IF NOT EXISTS mandataire_personne_morale (
    id SERIAL NOT NULL,

    entreprise_id varchar(9) NOT NULL,

    mandataire_entreprise_id varchar(9),

    fonction varchar NOT NULL,
    greffe_code varchar,
    greffe_libelle varchar,

    PRIMARY KEY (id)
);

INSERT INTO mandataire_personne_morale (
    entreprise_id,
    mandataire_entreprise_id,
    fonction,
    greffe_code,
    greffe_libelle
)
SELECT
    entreprise_id,
    NULLIF(numero_identification, '000000000'),
    fonction,
    code_greffe,
    libelle_greffe
FROM source_api_entreprise_entreprise_mandataire_social S,
    jsonb_to_recordset(S.payload) as mandataire(
        type varchar,
        numero_identification varchar,
        fonction varchar,
        code_greffe varchar,
        libelle_greffe varchar
    )
WHERE type IN ('PM', 'personne_morale');

-- etablissement

CREATE TABLE IF NOT EXISTS etablissement (
    id varchar(14),
    nic varchar(5),

    creation_date date,
    sirene_dernier_traitement_date date,

    effectif_type_id varchar(2),
    effectif_annee int,

    entreprise_id varchar(9) NOT NULL,

    commune_id varchar(5) NOT NULL,
    diffusible boolean NOT NULL DEFAULT true,

    siege boolean,
    nom varchar,

    PRIMARY KEY (id)
);

INSERT INTO etablissement (
    id,
    nic,

    creation_date,
    sirene_dernier_traitement_date,

    effectif_type_id,
    effectif_annee,

    entreprise_id,

    commune_id,
    diffusible,

    siege,

    nom
)
SELECT
    E.siret,
    E.nic,

    E.date_creation_etablissement,
    E.date_dernier_traitement_etablissement,

    E.tranche_effectifs_etablissement,
    E.annee_effectifs_etablissement::int,

    E.siren,

    E.code_commune_etablissement,
    CASE
        WHEN E.statut_diffusion_etablissement = 'O'
        THEN true
        ELSE false
    END,

    E.etablissement_siege,

    -- etablissement nom
    -- dans la plupart des cas la dénomination usuelle est identique à l'enseigne
    COALESCE(
        E.denomination_usuelle_etablissement,
        E.enseigne1_etablissement,
        E.enseigne2_etablissement,
        E.enseigne3_etablissement
    )
FROM source_api_sirene_etablissement E
JOIN commune as C ON C.id = e.code_commune_etablissement;

-- HACK (données de seed) insert les sieges d'établissement manquants
-- WITH etablissement_manquant AS (
--     SELECT entreprise_periode.siege_etablissement_id AS id
--     FROM entreprise_periode
--     LEFT JOIN etablissement ON entreprise_periode.siege_etablissement_id = etablissement.id
--     WHERE etablissement.id IS NULL
-- )
-- INSERT INTO etablissement (id, entreprise_id, commune_id)
-- SELECT id, SUBSTRING(id, 1, 9), '31555'
-- FROM etablissement_manquant;

-- Cette contrainte ne fonctionne pas car 321 000 sièges d'établissement ne sont pas dans notre table `etablissement`
-- ALTER TABLE entreprise_periode ADD CONSTRAINT fk_entreprise_periode__siege_etablissement FOREIGN KEY (siege_etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS etablissement_adresse_1 (
    etablissement_id varchar(14)
) INHERITS (adresse);

INSERT INTO etablissement_adresse_1 (
    etablissement_id,
    commune_id,
    numero,
    voie_nom,
    code_postal,
    distribution_speciale,
    cedex_code,
    cedex_nom,
    geolocalisation
)
SELECT
    E.siret,
    E.code_commune_etablissement,
    TRIM(
        CASE
            WHEN numero_voie_etablissement IS NOT NULL AND numero_voie_etablissement <> ''
            THEN numero_voie_etablissement || ' '
            ELSE ''
        END ||
        CASE
            WHEN indice_repetition_etablissement IS NOT NULL AND indice_repetition_etablissement <> ''
            THEN indice_repetition_etablissement || ' '
            ELSE ''
        END
    ),
    TRIM(
        CASE
            WHEN type_voie_etablissement IS NOT NULL AND type_voie_etablissement <> ''
            THEN type_voie_etablissement || ' '
            ELSE ''
        END ||
        CASE
            WHEN libelle_voie_etablissement IS NOT NULL AND libelle_voie_etablissement <> ''
            THEN libelle_voie_etablissement || ' '
            ELSE ''
        END ||
        CASE
            WHEN complement_adresse_etablissement IS NOT NULL AND complement_adresse_etablissement <> ''
            THEN complement_adresse_etablissement
            ELSE ''
        END
    ),
    E.code_postal_etablissement,
    E.distribution_speciale_etablissement,
    E.code_cedex_etablissement,
    E.libelle_cedex_etablissement,
    ST_SetSRID(ST_Point(E.longitude, E.latitude), 4326)
FROM source_api_sirene_etablissement E
-- évite les établissement dont l'adresse est à l'étranger ou dans les comer
JOIN commune as C ON C.id = E.code_commune_etablissement;

-- etablissement_periode

CREATE TABLE IF NOT EXISTS source_api_sirene_etablissement_periode (
    siret varchar(14) NOT NULL,
    activite_principale_etablissement varchar(6),
    caractere_employeur_etablissement source_sirene_caractere_employeur_enum,
    date_debut date,
    date_fin date,
    denomination_usuelle_etablissement varchar,

    enseigne1_etablissement varchar,
    enseigne2_etablissement varchar,
    enseigne3_etablissement varchar,

    etat_administratif_etablissement source_sirene_etablissement_etat_administratif_enum,
    nomenclature_activite_principale_etablissement varchar(8),
    changement_activite_principale_etablissement boolean,
    changement_caractere_employeur_etablissement boolean,
    changement_enseigne_etablissement boolean,
    changement_etat_administratif_etablissement boolean,
    mise_a_jour_at timestamptz
);

INSERT INTO source_api_sirene_etablissement_periode (
    siret,
    activite_principale_etablissement,
    caractere_employeur_etablissement,
    date_debut,
    date_fin,
    denomination_usuelle_etablissement,

    enseigne1_etablissement,
    enseigne2_etablissement,
    enseigne3_etablissement,

    etat_administratif_etablissement,
    nomenclature_activite_principale_etablissement
)
SELECT
    E.siret,
    E.activite_principale_etablissement,
    E.caractere_employeur_etablissement,
    E.date_debut,
    NULL,
    E.denomination_usuelle_etablissement,

    E.enseigne1_etablissement,
    E.enseigne2_etablissement,
    E.enseigne3_etablissement,

    E.etat_administratif_etablissement,
    E.nomenclature_activite_principale_etablissement
FROM source_api_sirene_etablissement E
-- uniquement pour les établissements sur les communes en france
JOIN commune as C ON C.id = E.code_commune_etablissement;

ALTER TABLE source_api_sirene_etablissement_periode ADD CONSTRAINT uk_source_api_sirene_etablissement_periode UNIQUE (siret, date_debut);

CREATE TABLE IF NOT EXISTS etablissement_periode (
    etablissement_id varchar(14),

    debut_date date,
    fin_date date,

    enseigne_1 varchar,
    enseigne_2 varchar,
    enseigne_3 varchar,

    naf_revision_type_id varchar(10),
    naf_type_id varchar(6),

    actif boolean,
    denomination_usuelle varchar,

    employeur boolean DEFAULT false NOT NULL,
    actif_change boolean DEFAULT false NOT NULL,
    enseigne_change boolean DEFAULT false NOT NULL,
    denomination_usuelle_change boolean DEFAULT false NOT NULL,
    naf_type_change boolean DEFAULT false NOT NULL,
    employeur_change boolean DEFAULT false NOT NULL

    -- why? revision inexistante?
    -- CONSTRAINT fk_etablissement_periode__naf_type_revision_type FOREIGN KEY (naf_type_id, naf_revision_type_id) REFERENCES naf_type(id, naf_revision_type_id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO etablissement_periode (
    etablissement_id,

    debut_date,
    fin_date,

    enseigne_1,
    enseigne_2,
    enseigne_3,

    naf_revision_type_id,
    naf_type_id,

    actif,
    denomination_usuelle,
    employeur
)
SELECT
    E.siret,

    E.date_debut,
    NULL,

    E.enseigne1_etablissement,
    E.enseigne2_etablissement,
    E.enseigne3_etablissement,

    E.nomenclature_activite_principale_etablissement,
    E.activite_principale_etablissement,

    CASE
        WHEN E.etat_administratif_etablissement = 'A'
        THEN true
        ELSE false
    END,
    E.denomination_usuelle_etablissement,
		CASE
        WHEN E.caractere_employeur_etablissement = 'O'
        THEN true
        ELSE false
    END
FROM source_api_sirene_etablissement E
-- uniquement pour les établissements sur les communes en france
JOIN commune as C ON C.id = E.code_commune_etablissement;

-- etablissement_exercice

CREATE TABLE IF NOT EXISTS source_api_entreprise_etablissement_exercice (
    siret varchar(14) NOT NULL,
    date_fin_exercice integer NOT NULL,
    chiffre_affaires varchar,

    PRIMARY KEY(siret, date_fin_exercice)
);

INSERT INTO source_api_entreprise_etablissement_exercice (
    siret,
    date_fin_exercice,
    chiffre_affaires
)
SELECT
    entreprise_id,
    extract( year FROM date_cloture )::int,
    ca
FROM old.old_exercice;

CREATE TABLE IF NOT EXISTS etablissement_exercice (
    etablissement_id varchar(14) NOT NULL,
    cloture_annee integer NOT NULL,
    chiffre_affaires varchar,

    PRIMARY KEY (etablissement_id, cloture_annee)
);

INSERT INTO etablissement_exercice (
    etablissement_id,
    cloture_annee,
    chiffre_affaires
)
SELECT
    siret,
    date_fin_exercice,
    chiffre_affaires
FROM source_api_entreprise_etablissement_exercice;

-- equipe

CREATE TABLE IF NOT EXISTS equipe (
    id SERIAL,

    nom varchar NOT NULL,
    groupement boolean DEFAULT false NOT NULL,

    territoire_type_id varchar NOT NULL,
    equipe_type_id varchar NOT NULL DEFAULT 'autre',

    PRIMARY KEY(id)
);

INSERT INTO equipe (
    id,
    nom,
    groupement,
    territoire_type_id,
    equipe_type_id
)
SELECT
    id,
    name,
    CASE
        WHEN departement_id = '6AE' THEN true
        WHEN perimetre_id IS NOT NULL THEN true
        ELSE false
    END,
    CASE
        WHEN territory_type IN ('COM', 'ARM') THEN 'commune'
        WHEN territory_type IN ('CA', 'CC', 'CU', 'EPT', 'ME') THEN 'epci'
        ELSE REPLACE(territory_type, 'groupement de ', '')
    END,
    CASE
        WHEN territory_type = 'COM' THEN 'commune'
        WHEN perimetre_id IS NOT NULL THEN 'deveco_locale'
        WHEN metropole_id IS NOT NULL THEN 'plm'
        ELSE territory_type
    END
FROM old.old_territory;

-- deveco

CREATE TABLE IF NOT EXISTS deveco (
    id SERIAL,

    compte_id integer NOT NULL,
    equipe_id integer NOT NULL,
    bienvenue_email boolean NOT NULL default false,

    created_at TIMESTAMP NOT NULL default now(),
    updated_at TIMESTAMP NOT NULL default now(),

    PRIMARY KEY(id)
);

INSERT INTO deveco (
    id,
    compte_id,
    equipe_id,
    bienvenue_email,
    created_at,
    updated_at
)
SELECT
    id,
    account_id,
    territory_id,
    welcome_email_sent,
    created_at,
    updated_at
FROM old.old_deveco;

-- favoris

CREATE TABLE IF NOT EXISTS etablissement_favori (
    deveco_id integer,
    etablissement_id varchar(14),

    PRIMARY KEY(deveco_id, etablissement_id)
);

INSERT INTO etablissement_favori(deveco_id, etablissement_id)
SELECT deveco_id, siret
FROM old.old_etablissement_favori
WHERE
    (siret <> '' OR siret IS NOT NULL)
    AND favori;

-- zonage

CREATE TABLE IF NOT EXISTS zonage (
    id SERIAL,

    nom varchar(254) NOT NULL,
    geometrie geometry(MultiPolygon, 4326),

    equipe_id integer NOT NULL,

    PRIMARY KEY(id)
);

INSERT INTO zonage (
    id,
    nom,
    geometrie,
    equipe_id
)
SELECT
    id,
    nom,
    geometry,
    territoire_id
FROM old.old_zonage;

-- territoire de l'équipe

-- commune

CREATE TABLE IF NOT EXISTS equipe__commune (
    equipe_id integer NOT NULL,
    commune_id varchar(5) NOT NULL,

    PRIMARY KEY(equipe_id, commune_id)
);

INSERT INTO equipe__commune (equipe_id, commune_id)
SELECT id, commune_id
FROM old.old_territory
WHERE commune_id IS NOT NULL;

INSERT INTO equipe__commune (equipe_id, commune_id)
SELECT T.id, P."communeInseeCom"
FROM old.old_territory T
JOIN old.old_perimetre_communes_commune P ON P."perimetreId" = T.perimetre_id;

-- epci

CREATE TABLE IF NOT EXISTS equipe__epci (
    equipe_id integer NOT NULL,
    epci_id varchar(9) NOT NULL,

    PRIMARY KEY(equipe_id, epci_id)
);

INSERT INTO equipe__epci (equipe_id, epci_id)
SELECT id, epci_id
FROM old.old_territory
WHERE epci_id IS NOT NULL;

INSERT INTO equipe__epci (equipe_id, epci_id)
SELECT T.id, P."epciInseeEpci"
FROM old.old_territory T
JOIN old.old_perimetre_epcis_epci P ON P."perimetreId" = T.perimetre_id;

-- metropole

CREATE TABLE IF NOT EXISTS equipe__metropole (
    equipe_id integer NOT NULL,
    metropole_id varchar(5) NOT NULL,

    PRIMARY KEY(equipe_id, metropole_id)
);

INSERT INTO equipe__metropole (equipe_id, metropole_id)
SELECT id, metropole_id
FROM old.old_territory
WHERE metropole_id IS NOT NULL;

-- petr

CREATE TABLE IF NOT EXISTS equipe__petr (
    equipe_id integer NOT NULL,
    petr_id varchar(5) NOT NULL,

    PRIMARY KEY(equipe_id, petr_id)
);

INSERT INTO equipe__petr (equipe_id, petr_id)
SELECT id, petr_id
FROM old.old_territory
WHERE petr_id IS NOT NULL;

-- departement

CREATE TABLE IF NOT EXISTS equipe__departement (
    equipe_id integer NOT NULL,
    departement_id varchar(3) NOT NULL,

    PRIMARY KEY(equipe_id, departement_id)
);

INSERT INTO equipe__departement (equipe_id, departement_id)
SELECT id, left(departement_id, -1)
FROM old.old_territory
WHERE departement_id IS NOT NULL AND departement_id <> '6AE';

-- Collectivité européenne d'Alsace => Bas-Rhin et Haut-Rhin
INSERT INTO equipe__departement (equipe_id, departement_id)
SELECT id, departement
FROM old.old_territory, (VALUES ('67'), ('68')) AS deps(departement)
WHERE departement_id = '6AE';

INSERT INTO equipe__departement (equipe_id, departement_id)
SELECT T.id, P."departementCtcd"
FROM old.old_territory T
JOIN old.old_perimetre_departements_departement P ON P."perimetreId" = T.perimetre_id;

-- region

CREATE TABLE IF NOT EXISTS equipe__region (
    equipe_id integer NOT NULL,
    region_id varchar(2) NOT NULL,

    PRIMARY KEY(equipe_id, region_id)
);

INSERT INTO equipe__region (equipe_id, region_id)
SELECT id, region_id
FROM old.old_territory
WHERE region_id IS NOT NULL;

-- etiquette
CREATE TABLE IF NOT EXISTS etiquette (
    id serial,
    nom varchar NOT NULL,

    equipe_id integer NOT NULL,
    etiquette_type_id varchar(16) NOT NULL,

    PRIMARY KEY(id)
);

INSERT INTO etiquette (
    nom,
    equipe_id,
    etiquette_type_id
)
SELECT
    mot_cle,
    territory_id,
    'mot_cle'
FROM old.old_mot_cle;

INSERT INTO etiquette (
    nom,
    equipe_id,
    etiquette_type_id
)
SELECT
    activite,
    territory_id,
    'activite'
FROM old.old_activite_entreprise;

INSERT INTO etiquette (
    nom,
    equipe_id,
    etiquette_type_id
)
SELECT
    localisation,
    territory_id,
    'localisation'
FROM old.old_localisation_entreprise;

-- echange

CREATE TABLE IF NOT EXISTS echange (
    id serial,

    nom varchar,
    description text,
    date timestamptz NOT NULL,

    echange_type_id varchar(10) NOT NULL,
    equipe_id integer NOT NULL,
    deveco_id integer NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO echange (
    id,
    nom,
    description,
    date,
    echange_type_id,
    equipe_id,
    deveco_id
)
SELECT
    E.id,
    E.titre,
    E.compte_rendu,
    date_echange,
    E.type_echange,
    D.territory_id,
    E.createur_id
FROM old.old_echange E
JOIN old.old_deveco D ON D.id = E.createur_id
WHERE E.type_echange <> 'transformation';

-- demande
CREATE TABLE IF NOT EXISTS demande (
    id SERIAL,

    date timestamptz NOT NULL,

    demande_type_id varchar(20) NOT NULL,
    equipe_id integer NOT NULL,

    cloture_motif varchar,

    PRIMARY KEY (id)
);

-- id fiche où il y a plusieurs demandes de même type :
    -- 9881
    -- 9616
    -- 15391
    -- 9881
    -- 9662

INSERT INTO demande (
    id,
    date,
    demande_type_id,
    equipe_id,
    cloture_motif
)
SELECT
    DISTINCT ON (D.id)
    D.id as demande_id,
    D.created_at,
    D.type_demande,
    F.territoire_id,
    D.motif
FROM old.old_demande D
INNER JOIN old.old_fiche F on D.fiche_id = F.id;

CREATE TABLE IF NOT EXISTS echange__demande (
    echange_id integer NOT NULL,
    demande_id integer NOT NULL,

    PRIMARY KEY (echange_id, demande_id)
);

INSERT INTO echange__demande (
    echange_id, demande_id
)
SELECT
    E.id, D.id
FROM old.old_echange E
JOIN old.old_demande D ON D.fiche_id = E.fiche_id
WHERE D.type_demande = ANY(string_to_array(E.themes, ','));

-- rappel

CREATE TABLE IF NOT EXISTS rappel (
    id SERIAL,

    nom varchar NOT NULL,
    date timestamptz NOT NULL,
    equipe_id integer NOT NULL,
    description text,

    PRIMARY KEY (id)
);

INSERT INTO rappel (
    id,
    nom,
    date,
    equipe_id,
    description
)
SELECT
    R.id,
    CASE WHEN LENGTH(R.titre) > 100
        THEN SUBSTRING(R.titre, 1, 97) || '...'
        ELSE R.titre
    END,
    R.date_rappel,
    D.territory_id,
    CASE WHEN LENGTH(R.titre) > 100
        THEN R.titre
        ELSE NULL
    END
FROM old.old_rappel R
JOIN old.old_deveco D ON D.id = R.createur_id;

-- personne

CREATE TABLE IF NOT EXISTS personne (
    id SERIAL,

    nom varchar NOT NULL,
    prenom varchar NOT NULL,
    email varchar,
    telephone varchar,
    naissance_date date,

    equipe_id integer NOT NULL,

    created_at timestamptz NOT NULL DEFAULT now(),

    PRIMARY KEY (id)
);

-- equipe etablissement

CREATE TABLE IF NOT EXISTS equipe__etablissement_exogene (
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    ajout_at timestamptz NOT NULL DEFAULT now(),

    PRIMARY KEY (equipe_id, etablissement_id)
    -- CONSTRAINT fk_equipe__etablissement__exogene__equipe__etablissement FOREIGN KEY (equipe_id, etablissement_id) REFERENCES equipe__etablissement(equipe_id, etablissement_id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO equipe__etablissement_exogene (
    equipe_id,
    etablissement_id,
    ajout_at
)
SELECT
    territoire_id,
    entreprise_id,
    created_at
FROM old.old_entite T
WHERE exogene;

-- description

CREATE TABLE IF NOT EXISTS equipe__etablissement_description (
    description text NOT NULL,

    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,

    PRIMARY KEY (equipe_id, etablissement_id)
);

INSERT INTO equipe__etablissement_description (
    equipe_id,
    etablissement_id,
    description
)
SELECT
    territoire_id,
    entreprise_id,
    activite_autre
FROM old.old_entite T
WHERE
    entreprise_id IS NOT NULL
    AND activite_autre <> '' AND activite_autre IS NOT NULL;

-- etiquette dans etablissement

CREATE TABLE IF NOT EXISTS equipe__etablissement__etiquette (
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    etiquette_id integer NOT NULL,

    PRIMARY KEY(equipe_id, etablissement_id, etiquette_id)
);

-- mots_cles

WITH etablissement_mot_cle AS (
    SELECT
      DISTINCT UNNEST(string_to_array(mots_cles, ',')) as mot_cle,
      T.territoire_id,
      E.siret as entreprise_id
    FROM old.old_entite T
    JOIN old.old_entreprise E ON E.siret = entreprise_id
)
INSERT INTO equipe__etablissement__etiquette (
    equipe_id,
    etablissement_id,
    etiquette_id
)
SELECT emc.territoire_id, emc.entreprise_id, EE.id
FROM etablissement_mot_cle as emc
JOIN etiquette EE
    ON EE.nom = emc.mot_cle
    AND EE.etiquette_type_id = 'mot_cle'
    AND EE.equipe_id = territoire_id;

-- localisations

WITH etablissement_entreprise_localisation AS (
    SELECT
      DISTINCT UNNEST(string_to_array(entreprise_localisations, ',')) as localisation,
      T.territoire_id,
      E.siret AS entreprise_id
    FROM old.old_entite T
    JOIN old.old_entreprise E ON E.siret = entreprise_id
)
INSERT INTO equipe__etablissement__etiquette (
    equipe_id,
    etablissement_id,
    etiquette_id
)
SELECT eml.territoire_id, eml.entreprise_id, EE.id
FROM etablissement_entreprise_localisation as eml
JOIN etiquette EE
    ON EE.nom = eml.localisation
    AND EE.etiquette_type_id = 'localisation'
    AND EE.equipe_id = territoire_id;

-- activites

WITH etablissement_activites_reelles AS (
    SELECT
      DISTINCT UNNEST(string_to_array(activites_reelles, ',')) as activite,
      T.territoire_id,
      E.siret as entreprise_id
    FROM old.old_entite T
    JOIN old.old_entreprise E ON E.siret = T.entreprise_id
)
INSERT INTO equipe__etablissement__etiquette (
    equipe_id,
    etablissement_id,
    etiquette_id
)
SELECT emr.territoire_id, emr.entreprise_id, EE.id
FROM etablissement_activites_reelles as emr
JOIN etiquette EE
    ON EE.nom = emr.activite
    AND EE.etiquette_type_id = 'activite'
    AND EE.equipe_id = territoire_id;

-- echange

CREATE TABLE IF NOT EXISTS equipe__etablissement__echange (
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    echange_id integer NOT NULL,

    PRIMARY KEY(equipe_id, etablissement_id, echange_id)
    -- CONSTRAINT fk_equipe__etablissement__echange__equipe__etablissement FOREIGN KEY (equipe_id, etablissement_id) REFERENCES equipe__etablissement(equipe_id, etablissement_id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO equipe__etablissement__echange (
    equipe_id,
    etablissement_id,
    echange_id
)
SELECT F.territoire_id, T.entreprise_id, E.id
FROM old.old_echange E
JOIN old.old_fiche F ON F.id = E.fiche_id
JOIN old.old_entite T ON T.id = F.entite_id
WHERE T.entreprise_id IS NOT NULL
AND E.type_echange <> 'transformation';

-- demande

CREATE TABLE IF NOT EXISTS equipe__etablissement__demande (
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    demande_id integer NOT NULL,

    PRIMARY KEY(equipe_id, etablissement_id, demande_id)
    -- CONSTRAINT fk_equipe__etablissement__demande__equipe__etablissement FOREIGN KEY (equipe_id, etablissement_id) REFERENCES equipe__etablissement(equipe_id, etablissement_id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO equipe__etablissement__demande (
    equipe_id,
    etablissement_id,
    demande_id
)
SELECT
    F.territoire_id,
    T.entreprise_id,
    D.id
FROM old.old_demande D
JOIN old.old_fiche F on D.fiche_id = F.id
JOIN old.old_entite T ON T.id = F.entite_id
WHERE T.entreprise_id IS NOT NULL;

-- rappel

CREATE TABLE IF NOT EXISTS equipe__etablissement__rappel (
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    rappel_id integer NOT NULL,

    PRIMARY KEY(equipe_id, etablissement_id, rappel_id)
    -- CONSTRAINT fk_equipe__etablissement__rappel__equipe__etablissement FOREIGN KEY (equipe_id, etablissement_id) REFERENCES equipe__etablissement(equipe_id, etablissement_id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO equipe__etablissement__rappel (
    equipe_id,
    etablissement_id,
    rappel_id
)
SELECT F.territoire_id, T.entreprise_id, R.id
FROM old.old_rappel R
JOIN old.old_fiche F ON F.id = R.fiche_id
JOIN old.old_entite T ON T.id = F.entite_id
WHERE T.entreprise_id IS NOT NULL;

-- contact

CREATE TABLE IF NOT EXISTS contact (
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    personne_id integer NOT NULL,

    fonction varchar,

    contact_source_type_id varchar(10) NOT NULL,

    PRIMARY KEY(equipe_id, etablissement_id, personne_id)
    -- CONSTRAINT fk_contact__equipe__etablissement FOREIGN KEY (equipe_id, etablissement_id) REFERENCES equipe__etablissement(equipe_id, etablissement_id) ON DELETE CASCADE ON UPDATE CASCADE
);

WITH personne as (
    INSERT INTO personne (
        id,
        nom,
        prenom,
        email,
        telephone,
        naissance_date,
        equipe_id,
        created_at
    )
    SELECT
        P.id,
        P.nom,
        P.prenom,
        P.email,
        P.telephone,
        P.date_de_naissance,
        E.territoire_id,
        C.created_at
    FROM old.old_particulier P
    JOIN old.old_contact C ON C.particulier_id = P.id
    JOIN old.old_entite E ON E.id = C.entite_id
    WHERE
        (C.source IS NULL OR C.source = 'import')
        AND E.entreprise_id IS NOT NULL
    ON CONFLICT DO NOTHING
    RETURNING id, equipe_id
)
INSERT INTO contact (
    equipe_id,
    etablissement_id,
    personne_id,
    fonction,
    contact_source_type_id
)
SELECT
    P.equipe_id,
    E.entreprise_id,
    P.id,
    C.fonction,
    CASE
        WHEN C.source IS NULL THEN 'deveco'
        ELSE C.source
    END
FROM old.old_contact C
JOIN personne P ON P.id = C.particulier_id
JOIN old.old_entite E ON E.id = C.entite_id
WHERE
    -- on n'insère pas les mendataires sociaux créés par l'API entreprise
    (C.source IS NULL OR C.source = 'import')
    AND E.entreprise_id IS NOT NULL;

-- equipe etablissement_creation

CREATE TABLE IF NOT EXISTS etablissement_creation (
    id SERIAL,
    enseigne varchar,
    description text,
    commentaire text,

    equipe_id integer NOT NULL,
    entreprise_id varchar(9),

    PRIMARY KEY(id)
);

INSERT INTO etablissement_creation (
    id,
    enseigne,
    description,
    commentaire,
    equipe_id
)
SELECT
    E.particulier_id,
    E.future_enseigne,
    PA.description,
    E.activite_autre,
    E.territoire_id
FROM old.old_entite E
LEFT JOIN old.old_proprietaire P ON P.entite_id = E.id
LEFT JOIN old.old_particulier PA ON PA.id = E.particulier_id
WHERE
    E.particulier_id IS NOT NULL
    AND P.entite_id IS NULL;

-- remise à niveau pour récupérer la description depuis particulier

UPDATE etablissement_creation
set
    description = PA.description,
    commentaire = E.activite_autre
FROM old.old_entite E
LEFT JOIN old.old_particulier PA ON E.id = E.particulier_id
WHERE
    E.particulier_id = etablissement_creation.id;

--

-- etiquette

CREATE TABLE IF NOT EXISTS etablissement_creation__etiquette (
    equipe_id integer NOT NULL,
    etablissement_creation_id integer NOT NULL,
    etiquette_id integer NOT NULL,

    PRIMARY KEY(etablissement_creation_id, etiquette_id)
);

-- mots_cles

WITH etablissement_creation_mots_cles AS (
    SELECT
      DISTINCT UNNEST(string_to_array(mots_cles, ',')) as mot_cle,
      T.territoire_id,
      particulier_id
    FROM old.old_entite T
    JOIN old.old_particulier op ON op.id = particulier_id
)
INSERT INTO etablissement_creation__etiquette (
    equipe_id,
    etablissement_creation_id,
    etiquette_id
)
SELECT territoire_id, particulier_id, EE.id
FROM etablissement_creation_mots_cles as ecmc
JOIN etiquette EE
    ON EE.nom = ecmc.mot_cle
    AND EE.etiquette_type_id = 'mot_cle'
    AND EE.equipe_id = territoire_id;

-- localisations

WITH entreprise_localisations AS (
    SELECT
      DISTINCT UNNEST(string_to_array(entreprise_localisations, ',')) as localisation,
      territoire_id,
      particulier_id
    FROM old.old_entite
    JOIN old.old_particulier AS op ON op.id = particulier_id
)
INSERT INTO etablissement_creation__etiquette (
    equipe_id,
    etablissement_creation_id,
    etiquette_id
)
SELECT territoire_id, particulier_id, EE.id
FROM entreprise_localisations as el
JOIN etiquette EE
    ON EE.nom = el.localisation
    AND EE.etiquette_type_id = 'localisation'
    AND EE.equipe_id = territoire_id;

-- activites

WITH activites_reelles AS (
    SELECT
        DISTINCT UNNEST(string_to_array(activites_reelles, ',')) as activite,
        territoire_id,
        particulier_id
    FROM old.old_entite
    JOIN old.old_particulier AS op ON op.id = particulier_id
)
INSERT INTO etablissement_creation__etiquette (
    equipe_id,
    etablissement_creation_id,
    etiquette_id
)
SELECT territoire_id, particulier_id, EE.id
FROM activites_reelles as ar
JOIN etiquette EE
    ON EE.nom = ar.activite
    AND EE.etiquette_type_id = 'activite'
    AND EE.equipe_id = territoire_id;

-- echange

CREATE TABLE IF NOT EXISTS etablissement_creation__echange (
    equipe_id integer NOT NULL,
    etablissement_creation_id integer NOT NULL,
    echange_id integer NOT NULL,

    PRIMARY KEY(etablissement_creation_id, echange_id)
);

INSERT INTO etablissement_creation__echange (
    equipe_id,
    etablissement_creation_id,
    echange_id
)
SELECT F.territoire_id, T.particulier_id, E.id
FROM old.old_echange E
JOIN old.old_fiche F ON F.id = E.fiche_id
JOIN old.old_entite T ON T.id = F.entite_id
WHERE T.particulier_id IS NOT NULL;

-- echange de type transformation

CREATE TABLE IF NOT EXISTS etablissement_creation_transformation (
    id SERIAL,
    date timestamptz NOT NULL,

    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    etablissement_creation_id integer NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO etablissement_creation_transformation (
    date,
    equipe_id,
    etablissement_creation_id,
    etablissement_id
)
SELECT
    date_echange,
    T.territoire_id,
    T.createur_id,
    T.entreprise_id
FROM old.old_echange E
JOIN old.old_fiche F ON F.id = E.fiche_id
JOIN old.old_entite T on T.id = F.entite_id
WHERE E.type_echange = 'transformation';

-- demande

CREATE TABLE IF NOT EXISTS etablissement_creation__demande (
    equipe_id integer NOT NULL,
    etablissement_creation_id integer NOT NULL,
    demande_id integer NOT NULL,

    PRIMARY KEY(etablissement_creation_id, demande_id)
);

INSERT INTO etablissement_creation__demande (
    equipe_id,
    etablissement_creation_id,
    demande_id
)
SELECT F.territoire_id, T.particulier_id, D.id
FROM old.old_demande D
JOIN old.old_fiche F ON F.id = D.fiche_id
JOIN old.old_entite T ON T.id = F.entite_id
WHERE T.particulier_id IS NOT NULL;

-- rappel

CREATE TABLE IF NOT EXISTS etablissement_creation__rappel (
    equipe_id integer NOT NULL,
    etablissement_creation_id integer NOT NULL,
    rappel_id integer NOT NULL,

    PRIMARY KEY(etablissement_creation_id, rappel_id)
);

INSERT INTO etablissement_creation__rappel (
    equipe_id,
    etablissement_creation_id,
    rappel_id
)
SELECT F.territoire_id, T.particulier_id, R.id
FROM old.old_rappel R
JOIN old.old_fiche F ON F.id = R.fiche_id
JOIN old.old_entite T ON T.id = F.entite_id
WHERE T.particulier_id IS NOT NULL;

-- portefeuille

CREATE TABLE IF NOT EXISTS equipe__etablissement_portefeuille AS
SELECT DISTINCT equipe_id, etablissement_id
FROM (
    SELECT equipe_id, etablissement_id, 'etiquette' as type
    FROM equipe__etablissement__etiquette
    GROUP BY equipe_id, etablissement_id
    UNION
    SELECT equipe_id, etablissement_id, 'description' as type
    FROM equipe__etablissement_description
    GROUP BY equipe_id, etablissement_id
    UNION
    SELECT equipe_id, etablissement_id, 'echange' as type
    FROM equipe__etablissement__echange
    GROUP BY equipe_id, etablissement_id
    UNION
    SELECT equipe_id, etablissement_id, 'demande' as type
    FROM equipe__etablissement__demande
    GROUP BY equipe_id, etablissement_id
    UNION
    SELECT equipe_id, etablissement_id, 'rappel' as type
    FROM equipe__etablissement__rappel
    GROUP BY equipe_id, etablissement_id
    UNION
    SELECT equipe_id, etablissement_id, 'contact' as type
    FROM contact
    GROUP BY equipe_id, etablissement_id
) t;

ALTER TABLE equipe__etablissement_portefeuille ADD CONSTRAINT PK_equipe__etablissement_portefeuille PRIMARY KEY (equipe_id, etablissement_id);

-- personne

INSERT INTO personne (
    id,
    nom,
    prenom,
    email,
    telephone,
    naissance_date,
    equipe_id,
    created_at
)
SELECT
    P.id,
    P.nom,
    P.prenom,
    P.email,
    P.telephone,
    P.date_de_naissance,
    E.territoire_id,
    P.created_at
FROM old.old_particulier P
JOIN old.old_entite E ON E.particulier_id = P.id
LEFT JOIN old.old_proprietaire R ON R.entite_id = E.id
WHERE
    E.particulier_id IS NOT NULL
    AND R.entite_id IS NULL
ON CONFLICT DO NOTHING;

CREATE TABLE IF NOT EXISTS personne_adresse (
    personne_id integer NOT NULL
) INHERITS (adresse);

WITH villes as (
    SELECT DISTINCT ON (L.nom, L.code_postal, L.code_insee)
    L.nom, L.code_postal, L.code_insee
    FROM source_laposte_code_insee_code_postal L
)
INSERT INTO personne_adresse (
    personne_id,
    numero,
    voie_nom,
    code_postal,
    geolocalisation,
    commune_id
)
SELECT
    P.id,
    CASE
        WHEN P.adresse ~ '^\d+(bis|ter|quater)?'
        THEN REGEXP_REPLACE(P.adresse, '^(\d+(bis|ter|quater)?)? ?.*', '\1')
        ELSE NULL
    END,
    REGEXP_REPLACE(REGEXP_REPLACE(P.adresse, '\d{5}.*$', ''), '^\d+(bis|ter|quater)? ', ''),
    P.code_postal,
    P.geolocation::point::geometry(Point, 4326),
    L.code_insee
FROM old.old_particulier P
JOIN old.old_entite E ON E.particulier_id = P.id
LEFT JOIN old.old_proprietaire R ON R.entite_id = E.id
LEFT JOIN villes L
    ON L.code_postal = P.code_postal
    AND LOWER(L.nom) LIKE
            REPLACE(
                REPLACE(
                    REPLACE(
                        LOWER(unaccent(P.ville)),
                        '-', ' '),
                    '''', ' '),
                'saint', 'st')
WHERE
    P.code_postal IS NOT NULL AND P.code_postal <> ''
    AND E.particulier_id IS NOT NULL
    AND R.entite_id IS NULL;

-- createur

CREATE TABLE IF NOT EXISTS createur (
    equipe_id integer NOT NULL,
    etablissement_creation_id integer NOT NULL,
    personne_id integer NOT NULL,
    fonction varchar(16),

    PRIMARY KEY(etablissement_creation_id, personne_id)
);

-- insertion des créateurs d'établissement dans la table personne

INSERT INTO personne (
    id,
    nom,
    prenom,
    email,
    telephone,
    naissance_date,
    equipe_id,
    created_at
)
SELECT
    P.id,
    P.nom,
    P.prenom,
    P.email,
    P.telephone,
    P.date_de_naissance,
    E.territoire_id,
    P.created_at
FROM old.old_particulier P
JOIN old.old_entite E ON E.particulier_id = P.id
LEFT JOIN old.old_proprietaire R ON R.entite_id = E.id
WHERE
    R.entite_id IS NULL
ON CONFLICT DO NOTHING;

INSERT INTO createur (
    equipe_id,
    etablissement_creation_id,
    personne_id,
    fonction
)
SELECT
    E.territoire_id,
    E.particulier_id,
    E.particulier_id,
    'Créateur'
FROM old.old_particulier P
JOIN old.old_entite E ON E.particulier_id = P.id
LEFT JOIN old.old_proprietaire R ON R.entite_id = E.id
JOIN personne PER ON PER.id = E.particulier_id
WHERE
    E.particulier_id IS NOT NULL
    AND R.entite_id IS NULL;

-- equipe local

CREATE TABLE IF NOT EXISTS local (
    id SERIAL NOT NULL,
    nom varchar,
    surface varchar,
    loyer varchar,
    description text,

    local_type_id varchar,
    local_statut_type_id varchar,
    equipe_id integer NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO local (
    id,
    nom,
    surface,
    loyer,
    description,
    local_statut_type_id,
    equipe_id
)
SELECT
    L.id,
    L.titre,
    L.surface,
    L.loyer,
    L.commentaire,
    L.local_statut,
    L.territoire_id
FROM old.old_local L;

CREATE TABLE IF NOT EXISTS local_adresse (
    local_id integer NOT NULL
) INHERITS (adresse);

WITH villes as (
    SELECT DISTINCT ON (L.nom, L.code_postal, L.code_insee)
    L.nom, L.code_postal, L.code_insee
    FROM source_laposte_code_insee_code_postal L
)
INSERT INTO local_adresse (
    local_id,
    numero,
    voie_nom,
    code_postal,
    geolocalisation,
    commune_id
)
SELECT
    O.id,
    CASE
        WHEN O.adresse ~ '^\d+(bis|ter|quater)?'
        THEN REGEXP_REPLACE(O.adresse, '^(\d+(bis|ter|quater)?)? ?.*', '\1')
        ELSE NULL
    END,
    REGEXP_REPLACE(REGEXP_REPLACE(O.adresse, '\d{5}.*$', ''), '^\d+(bis|ter|quater)? ', ''),
    O.code_postal,
    O.geolocation::point::geometry(Point, 4326),
    L.code_insee
FROM old.old_local O
LEFT JOIN villes L
    ON L.code_postal = O.code_postal
    AND LOWER(L.nom) LIKE
            REPLACE(
                REPLACE(
                    REPLACE(
                        LOWER(unaccent(O.ville)),
                        '-', ' '),
                    '''', ' '),
                'saint', 'st')
WHERE
    O.code_postal IS NOT NULL AND O.code_postal <> '';

-- types de locaux

CREATE TABLE IF NOT EXISTS local__local_type (
    local_id integer NOT NULL,
    local_type_id varchar(20) NOT NULL,

    PRIMARY KEY(local_id, local_type_id)
);

WITH local_local_types AS (
    SELECT id, UNNEST(string_to_array(local_types, ',')) as type
    FROM old.old_local
)
INSERT INTO local__local_type (
    local_id,
    local_type_id
)
SELECT
    id,
    type
FROM local_local_types;

-- etiquette dans local

CREATE TABLE IF NOT EXISTS local__etiquette (
    local_id integer NOT NULL,
    etiquette_id integer NOT NULL,

    PRIMARY KEY(local_id, etiquette_id)
);

WITH local_localisations AS (
    SELECT territoire_id, id as local_id, UNNEST(string_to_array(localisations, ',')) as localisation
    FROM old.old_local
)
INSERT INTO local__etiquette (
    local_id,
    etiquette_id
)
SELECT local_id, EE.id
FROM local_localisations
JOIN etiquette EE
    ON EE.nom = local_localisations.localisation
    AND EE.etiquette_type_id = 'localisation'
    AND EE.equipe_id = territoire_id;

-- proprietaire personne

CREATE TABLE IF NOT EXISTS proprietaire_personne (
    local_id integer NOT NULL,
    personne_id integer NOT NULL,

    PRIMARY KEY(local_id, personne_id)
);

WITH personne as (
    INSERT INTO personne (
        id,
        nom,
        prenom,
        email,
        telephone,
        naissance_date,
        equipe_id,
        created_at
    )
    SELECT
        P.id,
        P.nom,
        P.prenom,
        P.email,
        P.telephone,
        P.date_de_naissance,
        E.territoire_id,
        R.created_at
    FROM old.old_particulier P
    JOIN old.old_entite E ON E.particulier_id = P.id
    JOIN old.old_proprietaire R ON R.entite_id = E.id
    RETURNING id, equipe_id
)
INSERT INTO proprietaire_personne (
    local_id,
    personne_id
)
SELECT
    R.local_id,
    P.id
FROM old.old_proprietaire R
JOIN old.old_entite E ON E.id = R.entite_id
JOIN personne P ON P.id = E.particulier_id;

-- proprietaire etablissement

CREATE TABLE IF NOT EXISTS proprietaire_etablissement (
    local_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,

    PRIMARY KEY(local_id, etablissement_id)
);

INSERT INTO proprietaire_etablissement (
    local_id,
    etablissement_id
)
SELECT
    C.local_id,
    T.entreprise_id
FROM old.old_proprietaire C
JOIN old.old_entite T ON T.id = C.entite_id
WHERE T.entreprise_id IS NOT NULL;

-- evenement

CREATE TABLE IF NOT EXISTS evenement (
    id SERIAL NOT NULL,
    evenement_type_id varchar NOT NULL,
    date timestamptz NOT NULL DEFAULT now(),
    diff jsonb,
    deveco_id integer,
    compte_id integer,
    api_type_id varchar,

    PRIMARY KEY(id)
);

-- mise à jour de l'établissement par l'API Entreprise

CREATE TABLE IF NOT EXISTS evenement__etablissement (
    evenement_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,

    PRIMARY KEY(evenement_id, etablissement_id)
);

ALTER TABLE evenement ADD COLUMN tmp_id varchar;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    api_type_id
  )
  SELECT
    E.siret,
    'modification',
    E.last_api_update,
    'api_entreprise'
  FROM old.old_entreprise E
  WHERE
    E.last_api_update IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__etablissement (etablissement_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- mise à jour de l'entreprise par l'API Entreprise

CREATE TABLE IF NOT EXISTS evenement__entreprise (
    evenement_id integer NOT NULL,
    entreprise_id varchar(14) NOT NULL,

    PRIMARY KEY(evenement_id, entreprise_id)
);

ALTER TABLE evenement ADD COLUMN tmp_id varchar;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    api_type_id
  )
  SELECT
    SUBSTRING(E.siret, 1, 9),
    'modification',
    E.last_api_update,
    'api_entreprise'
  FROM old.old_entreprise E
  WHERE
    E.last_api_update IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__entreprise (entreprise_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- echange

CREATE TABLE IF NOT EXISTS evenement__echange (
    evenement_id integer NOT NULL,
    echange_id integer NOT NULL,

    PRIMARY KEY(evenement_id, echange_id)
);

-- echange creation

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    E.id,
    'creation',
    E.created_at,
    createur_id
  FROM old.old_echange E
  JOIN old.old_deveco D ON D.id = E.createur_id
  WHERE
    E.created_at IS NOT NULL
    AND type_echange <> 'transformation'
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__echange (echange_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- echange modification

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    E.id,
    'modification',
    E.updated_at,
    auteur_modification_id
  FROM old.old_echange E
  JOIN old.old_deveco D ON D.id = E.auteur_modification_id
  WHERE
    E.updated_at IS NOT NULL
    AND type_echange <> 'transformation'
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__echange (echange_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- demande

CREATE TABLE IF NOT EXISTS evenement__demande (
    evenement_id integer NOT NULL,
    demande_id integer NOT NULL,

    PRIMARY KEY(evenement_id, demande_id)
);

-- demande creation

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    DISTINCT ON (M.id)
    M.id,
    'creation',
    M.created_at,
    E.createur_id
  FROM old.old_demande M
  JOIN old.old_echange E on E.fiche_id = M.fiche_id
  JOIN old.old_deveco D ON D.id = E.createur_id
  WHERE M.created_at IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__demande (demande_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- demande modification

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    DISTINCT ON (M.id)
    M.id,
    'fermeture',
    M.date_cloture,
    E.createur_id
  FROM old.old_demande M
  JOIN old.old_echange E on E.fiche_id = M.fiche_id
  JOIN old.old_deveco D ON D.id = E.createur_id
  WHERE M.date_cloture IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__demande (demande_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- rappel

CREATE TABLE IF NOT EXISTS evenement__rappel (
    evenement_id integer NOT NULL,
    rappel_id integer NOT NULL,

    PRIMARY KEY(evenement_id, rappel_id)
);

-- rappel creation

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    E.id,
    'creation',
    E.created_at,
    createur_id
  FROM old.old_rappel E
  JOIN old.old_deveco D ON D.id = E.createur_id
  WHERE E.created_at IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__rappel (rappel_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- rappel fermeture

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    E.id,
    'fermeture',
    E.date_cloture,
    auteur_cloture_id
  FROM old.old_rappel E
  JOIN old.old_deveco D ON D.id = E.createur_id
  WHERE E.date_cloture IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__rappel (rappel_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- etiquette

--  aucun évènement à migrer sur les étiquettes

CREATE TABLE IF NOT EXISTS evenement__etiquette (
    evenement_id integer NOT NULL,
    etiquette_id integer NOT NULL,

    PRIMARY KEY(evenement_id, etiquette_id)
);

-- etiquette etablissement

CREATE TABLE IF NOT EXISTS evenement__etiquette__etablissement (
    evenement_id integer NOT NULL,
    etiquette_id integer NOT NULL,
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,

    PRIMARY KEY(evenement_id, etiquette_id, etablissement_id)
);

-- pas d'insertion dans evenement__etiquette__etablissement
-- car impossible de déterminer la date à laquelle une étiquette a été reliée à un établissement

CREATE TABLE IF NOT EXISTS evenement__etiquette__etablissement_creation (
    evenement_id integer NOT NULL,
    etiquette_id integer NOT NULL,
    etablissement_creation_id integer NOT NULL,

    PRIMARY KEY(evenement_id, etiquette_id, etablissement_creation_id)
);

-- pas d'insertion dans evenement__etiquette__etablissement_creation
-- car impossible de déterminer la date à laquelle une étiquette a été reliée à une création d'établissement

-- contact

CREATE TABLE IF NOT EXISTS evenement__contact (
    evenement_id integer NOT NULL,
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,
    personne_id integer NOT NULL,

    PRIMARY KEY(evenement_id, equipe_id, etablissement_id, personne_id)
);

-- contact creation

ALTER TABLE evenement ADD COLUMN tmp_equipe_id integer;
ALTER TABLE evenement ADD COLUMN tmp_etablissement_id varchar(14);
ALTER TABLE evenement ADD COLUMN tmp_personne_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_equipe_id,
    tmp_etablissement_id,
    tmp_personne_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    T.territoire_id,
    T.entreprise_id,
    C.particulier_id,
    'creation',
    C.created_at,
    T.deveco_id
  FROM old.old_contact C
  JOIN old.old_entite T ON T.id = C.entite_id
  JOIN old.old_deveco D ON D.id = T.deveco_id
  WHERE
    C.created_at IS NOT NULL
    AND T.entreprise_id IS NOT NULL
    AND (C.source IS NULL OR C.source = 'import')
  RETURNING id AS evenement_id, tmp_equipe_id, tmp_etablissement_id, tmp_personne_id
)
INSERT INTO evenement__contact (equipe_id, etablissement_id, personne_id, evenement_id)
SELECT tmp_equipe_id, tmp_etablissement_id, tmp_personne_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_equipe_id;
ALTER TABLE evenement DROP COLUMN tmp_etablissement_id;
ALTER TABLE evenement DROP COLUMN tmp_personne_id;

-- etablissement_creation

CREATE TABLE IF NOT EXISTS evenement__etablissement_creation (
    evenement_id integer NOT NULL,
    etablissement_creation_id integer NOT NULL,

    PRIMARY KEY(evenement_id, etablissement_creation_id)
);

-- etablissement_creation creation

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    E.particulier_id,
    'creation',
    E.created_at,
    E.deveco_id
  FROM old.old_entite E
  LEFT JOIN old.old_proprietaire P ON P.entite_id = E.id
  JOIN old.old_deveco D ON D.id = E.deveco_id
  WHERE E.created_at IS NOT NULL
    AND P.id is null
    AND E.particulier_id IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__etablissement_creation (etablissement_creation_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- etablissement_creation modification

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    E.particulier_id,
    'modification',
    E.updated_at,
    E.auteur_modification_id
  FROM old.old_entite E
  LEFT JOIN old.old_proprietaire P ON P.entite_id = E.id
  JOIN old.old_deveco D ON D.id = E.auteur_modification_id
  WHERE E.updated_at IS NOT NULL
    AND P.id is null
    AND E.particulier_id IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__etablissement_creation (etablissement_creation_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- etablissement_creation affichage

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    T.particulier_id,
    'affichage',
    E.date,
    E.deveco_id
  FROM old.old_event_log E
  JOIN old.old_entite T ON T.id = E.entity_id
  JOIN old.old_deveco D ON D.id = E.deveco_id
  WHERE
    entity_type = 'FICHE_PP'
    AND action = 'VIEW'
    AND T.particulier_id IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__etablissement_creation (etablissement_creation_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- etablissement_creation lien avec etablissement (transformation)

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    T.particulier_id,
    'transformation',
    E.date,
    E.deveco_id
  FROM old.old_event_log E
  JOIN old.old_entite T ON T.id = E.entity_id
  JOIN old.old_deveco D ON D.id = E.deveco_id
  WHERE
    entity_type = 'FICHE_PP'
    AND action = 'CREATE_ENTREPRISE'
    AND T.particulier_id IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__etablissement_creation (etablissement_creation_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- etablissement

CREATE TABLE IF NOT EXISTS evenement__equipe_etablissement (
    evenement_id integer NOT NULL,
    equipe_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,

    PRIMARY KEY(evenement_id, equipe_id, etablissement_id)
);

-- etablissement affichage

ALTER TABLE evenement ADD COLUMN tmp_equipe_id integer;
ALTER TABLE evenement ADD COLUMN tmp_etablissement_id varchar(14);

WITH E AS (
  INSERT INTO evenement (
    tmp_equipe_id,
    tmp_etablissement_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    T.territoire_id,
    T.entreprise_id,
    'affichage',
    E.date,
    E.deveco_id
  FROM old.old_event_log E
  JOIN old.old_entite T ON T.id = E.entity_id
  JOIN old.old_deveco D ON D.id = E.deveco_id
  WHERE
    entity_type = 'FICHE_PM'
    AND action = 'VIEW'
    AND T.entreprise_id IS NOT NULL
  RETURNING id AS evenement_id, tmp_equipe_id, tmp_etablissement_id
)
INSERT INTO evenement__equipe_etablissement (equipe_id, etablissement_id, evenement_id)
SELECT tmp_equipe_id, tmp_etablissement_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_equipe_id;
ALTER TABLE evenement DROP COLUMN tmp_etablissement_id;

-- local

CREATE TABLE IF NOT EXISTS evenement__local (
    evenement_id integer NOT NULL,
    local_id integer NOT NULL,

    PRIMARY KEY(evenement_id, local_id)
);

-- local creation

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    L.id,
    'creation',
    L.created_at,
    L.deveco_id
  FROM old.old_local L
  JOIN old.old_deveco D ON D.id = L.deveco_id
  WHERE L.created_at IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__local (local_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- local modification

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    L.id,
    'modification',
    L.updated_at,
    auteur_modification_id
  FROM old.old_local L
  JOIN old.old_deveco D ON D.id = L.auteur_modification_id
  WHERE L.updated_at IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__local (local_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- local affichage

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    E.entity_id,
    'affichage',
    E.date,
    E.deveco_id
  FROM old.old_event_log E
  JOIN old.old_deveco D ON D.id = E.deveco_id
  WHERE
    entity_type = 'LOCAL'
    AND action = 'VIEW'
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__local (local_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- local proprietaire personne

CREATE TABLE IF NOT EXISTS evenement__proprietaire_personne (
    evenement_id integer NOT NULL,
    local_id integer NOT NULL,
    personne_id integer NOT NULL,

    PRIMARY KEY(evenement_id, local_id, personne_id)
);

-- local proprietaire personne creation

ALTER TABLE evenement ADD COLUMN tmp_local_id integer;
ALTER TABLE evenement ADD COLUMN tmp_personne_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_local_id,
    tmp_personne_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    P.local_id,
    T.particulier_id,
    'creation',
    P.created_at,
    P.deveco_id
  FROM old.old_proprietaire P
  JOIN old.old_entite T ON T.id = P.entite_id
  WHERE
    P.created_at IS NOT NULL
    AND T.particulier_id IS NOT NULL
  RETURNING id AS evenement_id, tmp_local_id, tmp_personne_id
)
INSERT INTO evenement__proprietaire_personne (local_id, personne_id, evenement_id)
SELECT tmp_local_id, tmp_personne_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_local_id;
ALTER TABLE evenement DROP COLUMN tmp_personne_id;

-- local proprietaire etablissement

CREATE TABLE IF NOT EXISTS evenement__proprietaire_etablissement (
    evenement_id integer NOT NULL,
    local_id integer NOT NULL,
    etablissement_id varchar(14) NOT NULL,

    PRIMARY KEY(evenement_id, local_id, etablissement_id)
);

-- local proprietaire etablissement creation

ALTER TABLE evenement ADD COLUMN tmp_local_id integer;
ALTER TABLE evenement ADD COLUMN tmp_etablissement_id varchar(14);

WITH E AS (
  INSERT INTO evenement (
    tmp_local_id,
    tmp_etablissement_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    P.local_id,
    T.entreprise_id,
    'creation',
    P.created_at,
    P.deveco_id
  FROM old.old_proprietaire P
  JOIN old.old_entite T ON T.id = P.entite_id
  WHERE
    P.created_at IS NOT NULL
    AND T.entreprise_id IS NOT NULL
  RETURNING id AS evenement_id, tmp_local_id, tmp_etablissement_id
)
INSERT INTO evenement__proprietaire_etablissement (local_id, etablissement_id, evenement_id)
SELECT tmp_local_id, tmp_etablissement_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_local_id;
ALTER TABLE evenement DROP COLUMN tmp_etablissement_id;

-- personne

CREATE TABLE IF NOT EXISTS evenement__personne (
    evenement_id integer NOT NULL,
    personne_id integer NOT NULL,

    PRIMARY KEY(evenement_id, personne_id)
);

-- personne creation (contact, proprietaire, createur)

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    P.id,
    'creation',
    P.created_at,
    E.deveco_id
  FROM old.old_particulier P
  LEFT JOIN old.old_contact C ON C.particulier_id = P.id
  LEFT JOIN old.old_entite E ON E.particulier_id = P.id OR E.id = C.entite_id
  JOIN old.old_deveco D ON D.id = E.deveco_id
  WHERE
    E.created_at IS NOT NULL
    AND (C.source IS NULL OR C.source = 'import')
    AND (C.fonction IS NULL OR C.fonction <> 'Créateur')
  ON CONFLICT DO NOTHING
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__personne (personne_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- pourquoi en a-t-on le double ?

-- compte

CREATE TABLE IF NOT EXISTS evenement__compte (
    evenement_id integer NOT NULL,
    compte_id integer NOT NULL,

    PRIMARY KEY(evenement_id, compte_id)
);

-- compte creation

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    A.id,
    'creation',
    D.created_at,
    D.id
  FROM old.old_account A
  JOIN old.old_deveco D ON D.account_id = A.id
  WHERE D.created_at IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__compte (compte_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- compte modification

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    A.id,
    'modification',
    D.updated_at,
    D.id
  FROM old.old_account A
  JOIN old.old_deveco D ON D.account_id = A.id
  WHERE D.updated_at IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__compte (compte_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- compte connexion

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    A.id,
    'connexion',
    A.last_login,
    D.id
  FROM old.old_account A
  JOIN old.old_deveco D ON D.account_id = A.id
  WHERE A.last_login IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__compte (compte_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- compte authentification

ALTER TABLE evenement ADD COLUMN tmp_id integer;

WITH E AS (
  INSERT INTO evenement (
    tmp_id,
    evenement_type_id,
    date,
    deveco_id
  )
  SELECT
    A.id,
    'authentification',
    A.last_auth,
    D.id
  FROM old.old_account A
  JOIN old.old_deveco D ON D.account_id = A.id
  WHERE A.last_auth IS NOT NULL
  RETURNING id AS evenement_id, tmp_id
)
INSERT INTO evenement__compte (compte_id, evenement_id)
SELECT tmp_id, evenement_id
FROM E;

ALTER TABLE evenement DROP COLUMN tmp_id;

-- evenements, mise à jour de la colonne compte_id à partir de deveco_id

UPDATE evenement e
SET compte_id = d.compte_id
FROM deveco d
WHERE e.deveco_id = d.id;

ALTER TABLE evenement DROP COLUMN deveco_id;

--

CREATE TABLE IF NOT EXISTS stats (
    id SERIAL NOT NULL,
    commune_id character varying NOT NULL,
    date date NOT NULL,
    content jsonb NOT NULL,

    PRIMARY KEY (commune_id, date)
);

