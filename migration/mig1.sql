-- install extensions

CREATE EXTENSION IF NOT EXISTS pg_trgm;
CREATE EXTENSION IF NOT EXISTS unaccent;

CREATE SCHEMA old;

ALTER TABLE territory_etablissement_reference_zonages_zonage RENAME TO old_territory_etablissement_reference_zonages_zonage;
ALTER TABLE fiche_archive RENAME TO old_fiche_archive;
ALTER TABLE local_archive RENAME TO old_local_archive;
ALTER TABLE stats RENAME TO old_stats;
ALTER TABLE event_log RENAME TO old_event_log;
ALTER TABLE etablissement_favori RENAME TO old_etablissement_favori;
ALTER TABLE migration_table RENAME TO old_migration_table;
ALTER TABLE typeorm_metadata RENAME TO old_typeorm_metadata;
ALTER TABLE account RENAME TO old_account;
ALTER TABLE qp_metropoleoutremer_wgs84_epsg4326 RENAME TO old_qp_metropoleoutremer_wgs84_epsg4326;
ALTER TABLE naf RENAME TO old_naf;
ALTER TABLE region RENAME TO old_region;
ALTER TABLE departement RENAME TO old_departement;
ALTER TABLE epci_commune RENAME TO old_epci_commune;
ALTER TABLE petr RENAME TO old_petr;
ALTER TABLE metropole RENAME TO old_metropole;
ALTER TABLE epci RENAME TO old_epci;
ALTER TABLE commune RENAME TO old_commune;
ALTER TABLE entreprise RENAME TO old_entreprise;
ALTER TABLE etablissement RENAME TO old_etablissement;
ALTER TABLE territory RENAME TO old_territory;
ALTER TABLE deveco RENAME TO old_deveco;
ALTER SEQUENCE deveco_id_seq RENAME TO old_deveco_id_seq;
ALTER TABLE zonage RENAME TO old_zonage;
ALTER SEQUENCE zonage_id_seq RENAME TO old_zonage_id_seq;
ALTER TABLE perimetre RENAME TO old_perimetre;
ALTER TABLE perimetre_communes_commune RENAME TO old_perimetre_communes_commune;
ALTER TABLE perimetre_departements_departement RENAME TO old_perimetre_departements_departement;
ALTER TABLE perimetre_epcis_epci RENAME TO old_perimetre_epcis_epci;
ALTER TABLE mot_cle RENAME TO old_mot_cle;
ALTER TABLE localisation_entreprise RENAME TO old_localisation_entreprise;
ALTER TABLE activite_entreprise RENAME TO old_activite_entreprise;
ALTER TABLE fiche RENAME TO old_fiche;
ALTER TABLE echange RENAME TO old_echange;
ALTER SEQUENCE echange_id_seq RENAME TO old_echange_id_seq;
ALTER TABLE demande RENAME TO old_demande;
ALTER SEQUENCE demande_id_seq RENAME TO old_demande_id_seq;
ALTER TABLE rappel RENAME TO old_rappel;
ALTER SEQUENCE rappel_id_seq RENAME TO old_rappel_id_seq;
ALTER TABLE contact RENAME TO old_contact;
ALTER SEQUENCE contact_id_seq RENAME TO old_contact_id_seq;
ALTER TABLE particulier RENAME TO old_particulier;
ALTER TABLE exercice RENAME TO old_exercice;
ALTER TABLE proprietaire RENAME TO old_proprietaire;
ALTER TABLE territory_etablissement_reference RENAME TO old_territory_etablissement_reference;
ALTER TABLE entite RENAME TO old_entite;
ALTER TABLE local RENAME TO old_local;
ALTER SEQUENCE local_id_seq RENAME TO old_local_id_seq;
ALTER TABLE local_proprietaires_entite RENAME TO old_local_proprietaires_entite;
ALTER TABLE old_account SET SCHEMA old;
ALTER TABLE old_activite_entreprise SET SCHEMA old;
ALTER TABLE old_commune SET SCHEMA old;
ALTER TABLE old_contact SET SCHEMA old;
ALTER TABLE old_demande SET SCHEMA old;
ALTER TABLE old_departement SET SCHEMA old;
ALTER TABLE old_deveco SET SCHEMA old;
ALTER TABLE old_echange SET SCHEMA old;
ALTER TABLE old_entite SET SCHEMA old;
ALTER TABLE old_entreprise SET SCHEMA old;
ALTER TABLE old_epci SET SCHEMA old;
ALTER TABLE old_epci_commune SET SCHEMA old;
ALTER TABLE old_etablissement SET SCHEMA old;
ALTER TABLE old_etablissement_favori SET SCHEMA old;
ALTER TABLE old_event_log SET SCHEMA old;
ALTER TABLE old_exercice SET SCHEMA old;
ALTER TABLE old_fiche SET SCHEMA old;
ALTER TABLE old_fiche_archive SET SCHEMA old;
ALTER TABLE old_local SET SCHEMA old;
ALTER TABLE old_local_archive SET SCHEMA old;
ALTER TABLE old_local_proprietaires_entite SET SCHEMA old;
ALTER TABLE old_localisation_entreprise SET SCHEMA old;
ALTER TABLE old_metropole SET SCHEMA old;
ALTER TABLE old_migration_table SET SCHEMA old;
ALTER TABLE old_mot_cle SET SCHEMA old;
ALTER TABLE old_naf SET SCHEMA old;
ALTER TABLE old_particulier SET SCHEMA old;
ALTER TABLE old_perimetre SET SCHEMA old;
ALTER TABLE old_perimetre_communes_commune SET SCHEMA old;
ALTER TABLE old_perimetre_departements_departement SET SCHEMA old;
ALTER TABLE old_perimetre_epcis_epci SET SCHEMA old;
ALTER TABLE old_petr SET SCHEMA old;
ALTER TABLE old_proprietaire SET SCHEMA old;
ALTER TABLE old_qp_metropoleoutremer_wgs84_epsg4326 SET SCHEMA old;
ALTER TABLE old_rappel SET SCHEMA old;
ALTER TABLE old_region SET SCHEMA old;
ALTER TABLE old_stats SET SCHEMA old;
ALTER TABLE old_territory SET SCHEMA old;
ALTER TABLE old_territory_etablissement_reference SET SCHEMA old;
ALTER TABLE old_territory_etablissement_reference_zonages_zonage SET SCHEMA old;
ALTER TABLE old_typeorm_metadata SET SCHEMA old;
ALTER TABLE old_zonage SET SCHEMA old;

DROP TABLE login;
DROP TABLE consultation;

ALTER TABLE search_request RENAME TO recherche;
ALTER SEQUENCE search_request_id_seq RENAME TO recherche_id_seq;
ALTER TABLE recherche RENAME COLUMN type TO entite;
ALTER TABLE recherche RENAME COLUMN query TO requete;
ALTER TABLE recherche ADD COLUMN format varchar;
ALTER TABLE recherche DROP CONSTRAINT IF EXISTS "FK_1c22acefb8ac347d31d987e0539";
ALTER TABLE geo_token DROP CONSTRAINT IF EXISTS "FK_e686de08d0bc02c0f9f68997ca4";

-- territory_import_status TO equipe_import
ALTER TABLE territory_import_status RENAME TO equipe_import;
ALTER SEQUENCE territory_import_status_id_seq RENAME TO equipe_import_id_seq;
ALTER TABLE equipe_import RENAME COLUMN territoire_id TO equipe_id;
ALTER TABLE equipe_import DROP CONSTRAINT IF EXISTS uniq_territory;
ALTER TABLE equipe_import DROP CONSTRAINT IF EXISTS "FK_3660db63e1b58b8412a7071e0d0";
ALTER TABLE equipe_import DROP CONSTRAINT IF EXISTS "PK_0a5e37ff6b09e5a72ddf87efb26";
ALTER TABLE equipe_import ADD CONSTRAINT PK_equipe_import PRIMARY KEY (id);

-- sources données publiques

ALTER TABLE sirene_etablissement RENAME TO source_api_sirene_etablissement;
ALTER TABLE source_api_sirene_etablissement ADD COLUMN mise_a_jour_at timestamptz;
ALTER TABLE source_api_sirene_etablissement ALTER COLUMN etat_administratif_etablissement DROP NOT NULL;

ALTER TYPE sirene_etablissement_caractere_employeur_etablissement_enum RENAME TO source_sirene_caractere_employeur_enum;
ALTER TYPE sirene_etablissement_etat_administratif_etablissement_enum RENAME TO source_sirene_etablissement_etat_administratif_enum;
ALTER TYPE sirene_etablissement_statut_diffusion_etablissement_enum RENAME TO source_sirene_etablissement_statut_diffusion_enum;

ALTER TABLE sirene_unite_legale RENAME TO source_api_sirene_unite_legale;
ALTER TABLE source_api_sirene_unite_legale ADD COLUMN mise_a_jour_at timestamptz;
ALTER TABLE source_api_sirene_unite_legale RENAME COLUMN eco_soc_sol_unite_legale TO economie_sociale_solidaire_unite_legale;

ALTER TYPE sirene_unite_legale_categorie_entreprise_enum RENAME TO source_sirene_unite_legale_categorie_entreprise_enum;
ALTER TYPE sirene_unite_legale_eco_soc_sol_unite_legale_enum RENAME TO source_sirene_unite_legale_economie_sociale_solidaire_enum;
ALTER TYPE sirene_unite_legale_etat_administratif_unite_legale_enum RENAME TO source_sirene_unite_legale_etat_administratif_enum;
ALTER TYPE sirene_unite_legale_sexe_unite_legale_enum RENAME TO source_sirene_unite_legale_sexe_enum;
ALTER TYPE sirene_unite_legale_societe_mission_unite_legale_enum RENAME TO source_sirene_unite_legale_societe_mission_enum;
ALTER TYPE sirene_unite_legale_statut_diffusion_unite_legale_enum RENAME TO source_sirene_unite_legale_statut_diffusion_enum;

ALTER TABLE geoloc_entreprise RENAME TO source_geoloc_entreprise;

ALTER TABLE lien_succession RENAME TO source_sirene_lien_succession;
ALTER TABLE source_sirene_lien_succession ALTER COLUMN siret_predecesseur SET NOT NULL;
ALTER TABLE source_sirene_lien_succession ALTER COLUMN siret_successeur SET NOT NULL;

ALTER TABLE lien_succession_temp RENAME TO source_sirene_lien_succession_temp;
ALTER TABLE source_sirene_lien_succession_temp ALTER COLUMN siret_predecesseur SET NOT NULL;
ALTER TABLE source_sirene_lien_succession_temp ALTER COLUMN siret_successeur SET NOT NULL;

-- compte

CREATE TABLE IF NOT EXISTS compte (
    id SERIAL NOT NULL,
    nom varchar,
    prenom varchar,
    email varchar NOT NULL,
    compte_type_id varchar NOT NULL,
    cle varchar,
    actif boolean,

    PRIMARY KEY (id)
);

INSERT INTO compte (
    id,
    nom,
    prenom,
    email,
    compte_type_id,
    cle,
    actif
)
SELECT
    id,
    nom,
    prenom,
    email,
    account_type,
    access_key,
    enabled
FROM old.old_account;

ALTER TABLE password RENAME COLUMN account_id TO compte_id;
ALTER TABLE password RENAME COLUMN password TO hash;
ALTER TABLE password DROP CONSTRAINT IF EXISTS "FK_ad6708d47d7045166fab9c7ea34";
ALTER TABLE password DROP CONSTRAINT IF EXISTS "REL_ad6708d47d7045166fab9c7ea3";
ALTER TABLE password ADD CONSTRAINT fk_password__compte FOREIGN KEY (compte_id) REFERENCES compte(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE password ADD CONSTRAINT uk_password__compte UNIQUE (compte_id);
ALTER TABLE password RENAME TO mot_de_passe;
ALTER SEQUENCE password_id_seq RENAME TO mot_de_passe_id_seq;

--

CREATE TABLE IF NOT EXISTS tache (
    id SERIAL NOT NULL,
    nom VARCHAR NOT NULL,
    debut_at timestamptz DEFAULT NOW() NOT NULL,
    en_cours_at timestamptz,
    fin_at timestamptz,
    manuel boolean DEFAULT FALSE,
    rapport jsonb,

    PRIMARY KEY (id)
);

--

UPDATE recherche
SET
    entite = substring(entite, 8),
    format = 'xlsx'
WHERE entite ilike 'export-%';
DELETE FROM old.old_event_log
WHERE entity_type = 'LOCAL'
AND entity_id NOT IN (
    SELECT id
    FROM old.old_local
);

DROP TYPE etablissement_light_etat_administratif_etablissement_enum;

DROP TABLE IF EXISTS tmp_petr;

-- qpv

CREATE TABLE IF NOT EXISTS source_datagouv_qpv (
    gid serial,
    code_qp varchar(8),
    nom_qp varchar(254),
    commune_qp varchar(254),
    geom geometry(MultiPolygon,4326),

    PRIMARY KEY (gid)
);

INSERT INTO source_datagouv_qpv (
    gid,
    code_qp,
    nom_qp,
    commune_qp,
    geom
)
SELECT
    gid,
    code_qp,
    nom_qp,
    commune_qp,
    geom
FROM old.old_qp_metropoleoutremer_wgs84_epsg4326;

-- naf

CREATE TABLE IF NOT EXISTS naf_revision_type (
    id varchar(10) NOT NULL,
    nom varchar(30) NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO naf_revision_type (id, nom)
VALUES
    ('NAP', 'NAP 1973-1993'),
    ('NAF1993', 'NAF 1993'),
    ('NAFRev1', 'NAF Révision 1 (2003)'),
    ('NAFRev2', 'NAF Révision 2 (2008)');

CREATE TABLE IF NOT EXISTS naf_type (
    naf_revision_type_id varchar(10) NOT NULL,
    id varchar(10) NOT NULL,
    nom varchar NOT NULL,
    niveau int NOT NULL,
    parent_naf_type_id varchar(10),

    PRIMARY KEY (id, naf_revision_type_id)
);

-- etablissement / entreprise

CREATE TABLE IF NOT EXISTS categorie_juridique_type (
    id varchar(4) NOT NULL,
    nom varchar NOT NULL,
    parent_categorie_juridique_type_id varchar(4),
    niveau integer NOT NULL,

    PRIMARY KEY (id)
);
