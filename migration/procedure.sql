CREATE MATERIALIZED VIEW
  equipe__commune_vue AS
SELECT
  e.id AS equipe_id,
  c.id AS commune_id
FROM
  equipe e
  LEFT JOIN equipe__commune ec ON ec.equipe_id = e.id
  LEFT JOIN equipe__metropole em ON em.equipe_id = e.id
  LEFT JOIN equipe__epci ee ON ee.equipe_id = e.id
  LEFT JOIN equipe__petr ep ON ep.equipe_id = e.id
  LEFT JOIN equipe__departement ed ON ed.equipe_id = e.id
  LEFT JOIN equipe__region er ON er.equipe_id = e.id
  LEFT JOIN commune c ON c.id = ec.commune_id
  OR c.metropole_id = em.metropole_id
  OR c.epci_id = ee.epci_id
  OR c.petr_id = ep.petr_id
  OR c.departement_id = ed.departement_id
  OR c.region_id = er.region_id;

CREATE INDEX IDX_equipe__commune_vue__commune ON equipe__commune_vue (commune_id);

CREATE INDEX IDX_equipe__commune_vue__equipe ON equipe__commune_vue (equipe_id);

-- cluster
CREATE INDEX IDX_equipe__commune_vue__equipe__commune ON equipe__commune_vue (equipe_id, commune_id);

ALTER TABLE equipe__commune_vue
CLUSTER ON IDX_equipe__commune_vue__equipe;

CLUSTER equipe__commune_vue USING IDX_equipe__commune_vue__equipe;

-- Création d'une table partitionnée pour optimiser la recherche d'étblissement
CREATE TABLE IF NOT EXISTS
  equipe__etablissement (
    equipe_id integer,
    etablissement_id varchar(14),
    exogene boolean DEFAULT false NOT NULL,
    entreprise_id varchar(9),
    commune_id varchar(5),
    creation_date date,
    fermeture_date date,
    effectif_type_id varchar(2),
    categorie_juridique_3_type_id varchar(4) NOT NULL,
    categorie_juridique_2_type_id varchar(2) NOT NULL,
    entreprise_type_id varchar(1) NOT NULL,
    categorie_entreprise_type_id varchar(3),
    economie_sociale_solidaire boolean DEFAULT false NOT NULL,
    employeur boolean DEFAULT false NOT NULL,
    actif boolean,
    naf_type_id varchar(6),
    geolocalisation geometry (Point, 4326),
    code_postal varchar(5),
    numero varchar,
    voie_nom varchar,
    qpv_id varchar(8),
    zrr_type_id varchar(1) DEFAULT 'N'::varchar(1) NOT NULL,
    etablissement_noms varchar,
    contact_noms varchar,
    PRIMARY KEY (equipe_id, etablissement_id)
  )
PARTITION BY
  LIST (equipe_id);

-- Insertion du contenu d'une partition pour une équipe
CREATE
OR REPLACE PROCEDURE equipe__etablissement_create (eid integer) AS $$
BEGIN
    EXECUTE format(
        'CREATE TABLE IF NOT EXISTS %1$I PARTITION OF equipe__etablissement FOR VALUES IN (%2$s);',
        'equipe__etablissement_' || eid,
        eid
    );

    INSERT INTO equipe__etablissement (
        equipe_id,
        etablissement_id,
        exogene,
        entreprise_id,
        commune_id,
        creation_date,
        fermeture_date,
        effectif_type_id,
        categorie_juridique_3_type_id,
        categorie_juridique_2_type_id,
        entreprise_type_id,
        categorie_entreprise_type_id,
        economie_sociale_solidaire,
        employeur,
        actif,
        naf_type_id,
        geolocalisation,
        code_postal,
        numero,
        voie_nom,
        qpv_id,
        zrr_type_id,
        etablissement_noms,
        contact_noms
    )
    SELECT
        equipe__commune_vue.equipe_id,
        etablissement.id,
        FALSE,
        etablissement.entreprise_id,
        etablissement.commune_id,
        etablissement.creation_date,
        CASE
            WHEN etablissement_periode.actif IS NULL
            THEN etablissement_periode.fin_date
            ELSE NULL
        END,
        etablissement.effectif_type_id,
        entreprise_periode.categorie_juridique_type_id,
        SUBSTRING(
            entreprise_periode.categorie_juridique_type_id,
            1,
            2
        ),
        entreprise.entreprise_type_id,
        entreprise.categorie_entreprise_type_id,
        entreprise_periode.economie_sociale_solidaire,
        etablissement_periode.employeur,
        etablissement_periode.actif,
        etablissement_periode.naf_type_id,
        etablissement_adresse_1.geolocalisation,
        etablissement_adresse_1.code_postal,
        etablissement_adresse_1.numero,
        etablissement_adresse_1.voie_nom,
        qpv.id,
        commune.zrr_type_id,
        CONCAT_WS(
            ' ',
            LOWER(personne_physique.prenom_1),
            LOWER(personne_physique.prenom_2),
            LOWER(personne_physique.prenom_3),
            LOWER(personne_physique.prenom_4),
            LOWER(personne_physique.prenom_usuel),
            LOWER(personne_physique.pseudonyme),
            LOWER(personne_morale.sigle),
            LOWER(entreprise_periode.nom),
            LOWER(entreprise_periode.nom_usage),
            LOWER(entreprise_periode.denomination),
            LOWER(entreprise_periode.denomination_usuelle_1),
            LOWER(entreprise_periode.denomination_usuelle_2),
            LOWER(entreprise_periode.denomination_usuelle_3),
            LOWER(etablissement_periode.enseigne_1),
            LOWER(etablissement_periode.enseigne_2),
            LOWER(etablissement_periode.enseigne_3),
            LOWER(etablissement_periode.denomination_usuelle)
        ),
        LOWER(STRING_AGG(personne.prenom || ' ' || personne.nom, ' '))
    FROM equipe__commune_vue
    JOIN etablissement ON etablissement.commune_id = equipe__commune_vue.commune_id
    JOIN entreprise ON entreprise.id = etablissement.entreprise_id
    JOIN entreprise_periode ON entreprise_periode.entreprise_id = etablissement.entreprise_id
    JOIN etablissement_periode ON etablissement_periode.etablissement_id = etablissement.id
    JOIN etablissement_adresse_1 ON etablissement_adresse_1.etablissement_id = etablissement.id
    JOIN commune ON etablissement.commune_id = commune.id
    LEFT JOIN qpv ON etablissement_adresse_1.geolocalisation IS NOT NULL
        AND ST_Covers(qpv.geometrie, etablissement_adresse_1.geolocalisation)
    LEFT JOIN personne_physique ON personne_physique.entreprise_id = etablissement.entreprise_id
    LEFT JOIN personne_morale ON personne_morale.entreprise_id = etablissement.entreprise_id
    LEFT JOIN contact ON etablissement.id = contact.etablissement_id
    LEFT JOIN personne ON personne.id = contact.personne_id
    WHERE
        equipe__commune_vue.equipe_id = eid
    GROUP BY (
        etablissement.id,
        equipe__commune_vue.equipe_id,
        entreprise_periode.entreprise_id,
        entreprise_periode.debut_date,
				entreprise_periode.categorie_juridique_type_id,
				entreprise_periode.economie_sociale_solidaire,
				entreprise_periode.nom,
        entreprise_periode.nom_usage,
        entreprise_periode.denomination,
        entreprise_periode.denomination_usuelle_1,
        entreprise_periode.denomination_usuelle_2,
        entreprise_periode.denomination_usuelle_3,
        entreprise.id,
        etablissement_periode.etablissement_id,
        etablissement_periode.debut_date,
        etablissement_periode.fin_date,
        etablissement_periode.employeur,
        etablissement_periode.actif,
        etablissement_periode.naf_type_id,
        etablissement_periode.enseigne_1,
        etablissement_periode.enseigne_2,
        etablissement_periode.enseigne_3,
        etablissement_periode.denomination_usuelle,
        etablissement_adresse_1.geolocalisation,
        etablissement_adresse_1.code_postal,
        etablissement_adresse_1.numero,
				etablissement_adresse_1.voie_nom,
        commune.id,
        qpv.id,
        personne_physique.entreprise_id,
        personne_morale.entreprise_id
    )

    UNION

    SELECT
        equipe__etablissement_exogene.equipe_id,
        etablissement.id,
        TRUE,
        etablissement.entreprise_id,
        etablissement.commune_id,
        etablissement.creation_date,
        CASE
            WHEN etablissement_periode.actif IS NULL
            THEN etablissement_periode.fin_date
            ELSE NULL
        END,
        etablissement.effectif_type_id,
        entreprise_periode.categorie_juridique_type_id,
        SUBSTRING(entreprise_periode.categorie_juridique_type_id, 1, 2),
        entreprise.entreprise_type_id,
        entreprise.categorie_entreprise_type_id,
        entreprise_periode.economie_sociale_solidaire,
        etablissement_periode.employeur,
        etablissement_periode.actif,
        etablissement_periode.naf_type_id,
        etablissement_adresse_1.geolocalisation,
        etablissement_adresse_1.code_postal,
        etablissement_adresse_1.numero,
				etablissement_adresse_1.voie_nom,
        qpv.id,
        commune.zrr_type_id,
        CONCAT_WS(
            ' ',
            LOWER(personne_physique.prenom_1),
            LOWER(personne_physique.prenom_2),
            LOWER(personne_physique.prenom_3),
            LOWER(personne_physique.prenom_4),
            LOWER(personne_physique.prenom_usuel),
            LOWER(personne_physique.pseudonyme),
            LOWER(personne_morale.sigle),
            LOWER(entreprise_periode.nom),
            LOWER(entreprise_periode.nom_usage),
            LOWER(entreprise_periode.denomination),
            LOWER(entreprise_periode.denomination_usuelle_1),
            LOWER(entreprise_periode.denomination_usuelle_2),
            LOWER(entreprise_periode.denomination_usuelle_3),
            LOWER(etablissement_periode.enseigne_1),
            LOWER(etablissement_periode.enseigne_2),
            LOWER(etablissement_periode.enseigne_3),
            LOWER(etablissement_periode.denomination_usuelle)
        ),
        STRING_AGG(personne.prenom || ' ' || personne.nom, ' ')
    FROM equipe__etablissement_exogene
    JOIN etablissement ON equipe__etablissement_exogene.etablissement_id = etablissement.id
    JOIN entreprise ON entreprise.id = etablissement.entreprise_id
    JOIN entreprise_periode ON entreprise_periode.entreprise_id = etablissement.entreprise_id
    JOIN etablissement_periode ON etablissement_periode.etablissement_id = etablissement.id
    JOIN etablissement_adresse_1 ON etablissement_adresse_1.etablissement_id = etablissement.id
    JOIN commune on etablissement.commune_id = commune.id
    LEFT JOIN qpv ON etablissement_adresse_1.geolocalisation IS NOT NULL
        AND ST_Covers(qpv.geometrie, etablissement_adresse_1.geolocalisation)
    LEFT JOIN personne_physique ON personne_physique.entreprise_id = etablissement.entreprise_id
    LEFT JOIN personne_morale ON personne_morale.entreprise_id = etablissement.entreprise_id
    LEFT JOIN contact ON etablissement.id = contact.etablissement_id
    LEFT JOIN personne ON personne.id = contact.personne_id
    WHERE equipe__etablissement_exogene.equipe_id = eid
    GROUP BY (
        etablissement.id,
        equipe__etablissement_exogene.equipe_id,
        entreprise_periode.entreprise_id,
        entreprise_periode.debut_date,
				entreprise_periode.categorie_juridique_type_id,
				entreprise_periode.economie_sociale_solidaire,
				entreprise_periode.nom,
        entreprise_periode.nom_usage,
        entreprise_periode.denomination,
        entreprise_periode.denomination_usuelle_1,
        entreprise_periode.denomination_usuelle_2,
        entreprise_periode.denomination_usuelle_3,
        entreprise.id,
        etablissement_periode.etablissement_id,
        etablissement_periode.debut_date,
        etablissement_periode.fin_date,
        etablissement_periode.employeur,
        etablissement_periode.actif,
        etablissement_periode.naf_type_id,
        etablissement_periode.enseigne_1,
        etablissement_periode.enseigne_2,
        etablissement_periode.enseigne_3,
        etablissement_periode.denomination_usuelle,
        etablissement_periode.denomination_usuelle,
        etablissement_adresse_1.geolocalisation,
        etablissement_adresse_1.code_postal,
        etablissement_adresse_1.numero,
				etablissement_adresse_1.voie_nom,
        commune.id,
        qpv.id,
        personne_physique.entreprise_id,
        personne_morale.entreprise_id
    );
END
$$ LANGUAGE plpgsql;

-- insert into table for each team
DO $$
	DECLARE
		e RECORD;
	BEGIN
		FOR e IN
			SELECT id FROM equipe WHERE id >= 500 and id < 600
		LOOP
                    EXECUTE '
                        update equipe__etablissement_' || e.id || ' ee set economie_sociale_solidaire = exists (select ess.entreprise_id from source.source_ess_france ess where ess.entreprise_id = ee.entreprise_id);';
                        COMMIT;
		END LOOP;
	END;
$$ LANGUAGE plpgsql;

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement_equipe ON equipe__etablissement (equipe_id);

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement_etablissement ON equipe__etablissement (etablissement_id);

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement_entreprise ON equipe__etablissement (entreprise_id);

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement_etablissement_gin ON equipe__etablissement USING gin (etablissement_id gin_trgm_ops);

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__creation_date ON equipe__etablissement (creation_date);

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__fermeture_date ON equipe__etablissement (fermeture_date);

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__categorie_juridique_3 ON equipe__etablissement (categorie_juridique_3_type_id);

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__categorie_juridique_2 ON equipe__etablissement (categorie_juridique_2_type_id);

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__naf ON equipe__etablissement (naf_type_id);

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__commune ON equipe__etablissement (commune_id);

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__qpv ON equipe__etablissement (qpv_id);

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__etablissement_noms ON equipe__etablissement USING gin (etablissement_noms gin_trgm_ops);

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement__contact_noms ON equipe__etablissement USING gin (contact_noms gin_trgm_ops);

ALTER TABLE equipe__etablissement
ADD CONSTRAINT fk_equipe__etablissement__equipe FOREIGN KEY (equipe_id) REFERENCES equipe (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE equipe__etablissement
ADD CONSTRAINT fk_equipe__etablissement__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE equipe__etablissement
ADD CONSTRAINT fk_equipe__etablissement__commune FOREIGN KEY (commune_id) REFERENCES commune (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE equipe__etablissement
ADD CONSTRAINT fk_equipe__etablissement__effectif_type FOREIGN KEY (effectif_type_id) REFERENCES effectif_type (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE equipe__etablissement
ADD CONSTRAINT fk_equipe__etablissement__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE equipe__etablissement
ADD CONSTRAINT fk_equipe__etablissement__entreprise_type FOREIGN KEY (entreprise_type_id) REFERENCES entreprise_type (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE equipe__etablissement
ADD CONSTRAINT fk_equipe__etablissement__categorie_entreprise_type FOREIGN KEY (categorie_entreprise_type_id) REFERENCES categorie_entreprise_type (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE equipe__etablissement
ADD CONSTRAINT fk_equipe__etablissement__zrr_type FOREIGN KEY (zrr_type_id) REFERENCES zrr_type (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE equipe__etablissement
ADD CONSTRAINT fk_equipe__etablissement__qpv FOREIGN KEY (qpv_id) REFERENCES qpv (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- reset sequences
DO $$
DECLARE
    table_record record;
BEGIN
    FOR table_record IN (
        SELECT table_name
        FROM information_schema.columns
        WHERE table_schema = 'public'
        AND column_name = 'id'
        AND column_default LIKE 'nextval%'
				AND table_name NOT LIKE '%_adresse%'
    )
    LOOP
        EXECUTE '
            SELECT
                setval(
                    ''' || table_record.table_name || '_id_seq'',
                    (SELECT MAX(id) FROM ' || table_record.table_name || ')
                );';
    END LOOP;
END $$;

DO $$
        DECLARE
                e RECORD;
        BEGIN
                FOR e IN
                        select equipe_id from (select distinct on(deveco.equipe_id) deveco.equipe_id, evenement.date from deveco inner join evenement on evenement.compte_id = deveco.compte_id where evenement.evenement_type_id = 'authentification' order by deveco.equipe_id, evenement.date desc) as truc order by date desc
                LOOP
                    EXECUTE '
                        update equipe__etablissement_' || e.id || ' ee set economie_sociale_solidaire = exists (select ess.entreprise_id from source.source_ess_france ess where ess.entreprise_id = ee.entreprise_id);';
                        COMMIT;
                END LOOP;
        END;
$$ LANGUAGE plpgsql;
