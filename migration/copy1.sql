psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM 'api/_refactor/_sources/insee_nap_niveau_1.csv'
DELIMITER ','
CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM 'api/_refactor/_sources/insee_nap_niveau_2.csv'
DELIMITER ','
CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM 'api/_refactor/_sources/insee_nap_niveau_3.csv'
DELIMITER ','
CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id)
FROM 'api/_refactor/_sources/insee_nap_niveau_4.csv'
DELIMITER ','
CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id) FROM 'api/_refactor/_sources/insee_naf_1993_niveau_1.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id) FROM 'api/_refactor/_sources/insee_naf_1993_niveau_2.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id) FROM 'api/_refactor/_sources/insee_naf_1993_niveau_3.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id) FROM 'api/_refactor/_sources/insee_naf_1993_niveau_4.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id) FROM 'api/_refactor/_sources/insee_naf_1993_niveau_5.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id) FROM 'api/_refactor/_sources/insee_naf_rev1_niveau_1.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id) FROM 'api/_refactor/_sources/insee_naf_rev1_niveau_2.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id) FROM 'api/_refactor/_sources/insee_naf_rev1_niveau_3.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id) FROM 'api/_refactor/_sources/insee_naf_rev1_niveau_4.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id) FROM 'api/_refactor/_sources/insee_naf_rev1_niveau_5.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id) FROM 'api/_refactor/_sources/insee_naf_rev2_niveau_1.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id) FROM 'api/_refactor/_sources/insee_naf_rev2_niveau_2.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id) FROM 'api/_refactor/_sources/insee_naf_rev2_niveau_3.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id) FROM 'api/_refactor/_sources/insee_naf_rev2_niveau_4.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy naf_type(naf_revision_type_id, id, nom, niveau, parent_naf_type_id) FROM 'api/_refactor/_sources/insee_naf_rev2_niveau_5.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy categorie_juridique_type(id, nom, niveau) FROM 'api/_refactor/_sources/insee_categorie_juridique_1.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy categorie_juridique_type(id, nom, niveau, parent_categorie_juridique_type_id) FROM 'api/_refactor/_sources/insee_categorie_juridique_2.csv' DELIMITER ',' CSV HEADER;"

psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy categorie_juridique_type(id, nom, niveau, parent_categorie_juridique_type_id) FROM 'api/_refactor/_sources/insee_categorie_juridique_3.csv' DELIMITER ',' CSV HEADER;"

-- catégories qui existent dans la base SIRENE mais qui n'existent plus dans les sources
psql -U postgresql -d postgresql -h localhost -p 5436 -c "\copy categorie_juridique_type(id, nom, niveau, parent_categorie_juridique_type_id) FROM 'api/_refactor/_sources/insee_categorie_juridique_supplementaire.csv' DELIMITER ',' CSV HEADER;"
