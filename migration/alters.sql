-- --------------------------------------------------------------
--
-- Archivage des anciennes tables
--
-- --------------------------------------------------------------

CREATE SCHEMA source;

ALTER TABLE source_datagouv_commune_ept SET SCHEMA source;
ALTER TABLE source_datagouv_qpv SET SCHEMA source;
ALTER TABLE source_datagouv_zrr SET SCHEMA source;
ALTER TABLE source_geoloc_entreprise SET SCHEMA source;
ALTER TABLE source_insee_commune SET SCHEMA source;
ALTER TABLE source_insee_commune_historique SET SCHEMA source;
ALTER TABLE source_insee_commune_mouvement SET SCHEMA source;
ALTER TABLE source_insee_departement SET SCHEMA source;
ALTER TABLE source_insee_epci SET SCHEMA source;
ALTER TABLE source_insee_epci_commune SET SCHEMA source;
ALTER TABLE source_insee_region SET SCHEMA source;
ALTER TABLE source_laposte_code_insee_code_postal SET SCHEMA source;
ALTER TABLE source_odt_epci_petr SET SCHEMA source;
ALTER TABLE source_sirene_lien_succession SET SCHEMA source;
ALTER TABLE source_sirene_lien_succession_temp SET SCHEMA source;
ALTER TABLE source_api_entreprise_entreprise_mandataire_social SET SCHEMA source;
ALTER TABLE source_api_entreprise_etablissement_exercice SET SCHEMA source;
ALTER TABLE source_api_sirene_etablissement SET SCHEMA source;
ALTER TABLE source_api_sirene_unite_legale SET SCHEMA source;
ALTER TABLE source_api_sirene_etablissement_periode SET SCHEMA source;
ALTER TABLE source_api_sirene_unite_legale_periode SET SCHEMA source;


ALTER TYPE source_sirene_caractere_employeur_enum SET SCHEMA source;
ALTER TYPE source_sirene_etablissement_etat_administratif_enum SET SCHEMA source;
ALTER TYPE source_sirene_etablissement_statut_diffusion_enum SET SCHEMA source;
ALTER TYPE source_sirene_unite_legale_categorie_entreprise_enum SET SCHEMA source;
ALTER TYPE source_sirene_unite_legale_economie_sociale_solidaire_enum SET SCHEMA source;
ALTER TYPE source_sirene_unite_legale_etat_administratif_enum SET SCHEMA source;
ALTER TYPE source_sirene_unite_legale_sexe_enum SET SCHEMA source;
ALTER TYPE source_sirene_unite_legale_societe_mission_enum SET SCHEMA source;
ALTER TYPE source_sirene_unite_legale_statut_diffusion_enum SET SCHEMA source;

-- --------------------------------------------------------------
--
-- Optimisation
--
-- --------------------------------------------------------------

--  Indexes

CREATE INDEX IF NOT EXISTS IDX_contact_equipe ON contact (equipe_id);
CREATE INDEX IF NOT EXISTS IDX_contact_etablissement ON contact (etablissement_id);
CREATE INDEX IF NOT EXISTS IDX_contact_personne ON contact (personne_id);

CREATE INDEX IF NOT EXISTS IDX_etablissement__commune ON etablissement (commune_id);
CREATE INDEX IF NOT EXISTS IDX_etablissement__etablissement_commune ON etablissement (id, commune_id);
CREATE INDEX IF NOT EXISTS IDX_etablissement__creation_date ON etablissement (creation_date);
CREATE INDEX IF NOT EXISTS IDX_etablissement__entreprise ON etablissement (entreprise_id);

CREATE INDEX IF NOT EXISTS IDX_etablissement_adresse_1__etablissement ON etablissement_adresse_1 (etablissement_id);

--

CREATE INDEX IF NOT EXISTS IDX_etablissement_periode__etablissement ON etablissement_periode (etablissement_id);
CREATE INDEX IF NOT EXISTS IDX_etablissement_periode__fin_date ON etablissement_periode (fin_date);
CREATE INDEX IF NOT EXISTS IDX_etablissement_periode__courante_fin_date ON etablissement_periode ((fin_date IS NULL));
CREATE INDEX IF NOT EXISTS IDX_etablissement_periode__actif ON etablissement_periode (actif);
CREATE INDEX IF NOT EXISTS IDX_etablissement_periode__naf_type ON etablissement_periode (naf_type_id);

--

CREATE INDEX IF NOT EXISTS IDX_entreprise_periode__entreprise ON entreprise_periode (entreprise_id);
CREATE INDEX IF NOT EXISTS IDX_entreprise_periode__courante_fin_date ON entreprise_periode ((fin_date IS NULL));
CREATE INDEX IF NOT EXISTS IDX_entreprise_periode__categorie_juridique ON entreprise_periode (categorie_juridique_type_id);
CREATE INDEX IF NOT EXISTS IDX_entreprise_periode__fin_date_partial ON entreprise_periode (entreprise_id) where fin_date IS NULL;

--

CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement_exogene__etablissement ON equipe__etablissement_exogene(etablissement_id);
CREATE INDEX IF NOT EXISTS IDX_equipe__etablissement_exogene__equipe ON equipe__etablissement_exogene(equipe_id);

--

CREATE INDEX IF NOT EXISTS IDX_adresse__geolocalisation ON adresse USING GiST (geolocalisation);
CREATE INDEX IF NOT EXISTS IDX_zonage__geometrie ON zonage USING GiST (geometrie);
CREATE INDEX IF NOT EXISTS IDX_qpv__geometrie ON qpv USING GiST (geometrie);
CREATE INDEX IF NOT EXISTS IDX_personne__nom ON personne USING gin (nom gin_trgm_ops);

--

CREATE INDEX IF NOT EXISTS IDX_source_api_sirene_unite_legale ON source.source_api_sirene_unite_legale (mise_a_jour_at);
CREATE INDEX IF NOT EXISTS IDX_source_api_sirene_etablissement ON source.source_api_sirene_etablissement (mise_a_jour_at);
CREATE INDEX IF NOT EXISTS IDX_source_api_sirene_etablissement_periode ON source.source_api_sirene_etablissement_periode (mise_a_jour_at);

-- CREATE INDEX IDX_personne__nom ON personne USING gin ((ts_vector('french', nom)));
-- Création d'une vue pour faire le lien entre les équipes et les communes

-- --------------------------------------------------------------
--
-- Ajout des contraintes
--
-----------------------------------------------------------------

ALTER TABLE tache ADD CONSTRAINT uk_tache UNIQUE (nom, debut_at);
ALTER TABLE naf_type ADD CONSTRAINT fk_naf_type__naf_revision_type FOREIGN KEY (naf_revision_type_id) REFERENCES naf_revision_type(id);
ALTER TABLE naf_type ADD CONSTRAINT fk_naf_type__parent_naf_type FOREIGN KEY (parent_naf_type_id, naf_revision_type_id) REFERENCES naf_type(id, naf_revision_type_id);
ALTER TABLE categorie_juridique_type ADD CONSTRAINT fk_categorie_juridique_type__parent_categorie_juridique_type FOREIGN KEY (parent_categorie_juridique_type_id) REFERENCES categorie_juridique_type(id);
ALTER TABLE departement ADD CONSTRAINT fk_departement__region FOREIGN KEY (region_id) REFERENCES region(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE epci ADD CONSTRAINT fk_epci__epci_type FOREIGN KEY (epci_type_id) REFERENCES epci_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE epci ADD CONSTRAINT fk_epci__petr FOREIGN KEY (petr_id) REFERENCES petr(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE commune ADD CONSTRAINT fk_commune__commune_type FOREIGN KEY (commune_type_id) REFERENCES commune_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE commune ADD CONSTRAINT fk_commune__metropole FOREIGN KEY (metropole_id) REFERENCES metropole(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE commune ADD CONSTRAINT fk_commune__epci FOREIGN KEY (epci_id) REFERENCES epci(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE commune ADD CONSTRAINT fk_commune__petr FOREIGN KEY (petr_id) REFERENCES petr(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE commune ADD CONSTRAINT fk_commune__departement FOREIGN KEY (departement_id) REFERENCES departement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE commune ADD CONSTRAINT fk_commune__region FOREIGN KEY (region_id) REFERENCES region(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE commune ADD CONSTRAINT fk_commune__zrr_type FOREIGN KEY (zrr_type_id) REFERENCES zrr_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE adresse ADD CONSTRAINT fk_adresse__commune FOREIGN KEY (commune_id) REFERENCES commune(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE entreprise ADD CONSTRAINT fk_entreprise__entreprise_type FOREIGN KEY (entreprise_type_id) REFERENCES entreprise_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE entreprise ADD CONSTRAINT fk_entreprise__categorie_entreprise_type FOREIGN KEY (categorie_entreprise_type_id) REFERENCES categorie_entreprise_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE personne_physique ADD CONSTRAINT fk_personne_physique__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE personne_physique ADD CONSTRAINT fk_personne_physique__sexe_type FOREIGN KEY (sexe_type_id) REFERENCES sexe_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE personne_morale ADD CONSTRAINT fk_personne_morale__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE entreprise_periode ADD CONSTRAINT fk_entreprise_periode__categorie_juridique_type FOREIGN KEY (categorie_juridique_type_id) REFERENCES categorie_juridique_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE entreprise_periode ADD CONSTRAINT fk_entreprise_periode__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE mandataire_personne_physique ADD CONSTRAINT fk_mandataire_personne_physique__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise(id);
ALTER TABLE mandataire_personne_morale ADD CONSTRAINT fk_mandataire_personne_morale__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise(id);
ALTER TABLE mandataire_personne_morale ADD CONSTRAINT fk_mandataire_personne_morale__mandataire_entreprise FOREIGN KEY (mandataire_entreprise_id) REFERENCES entreprise(id);
ALTER TABLE etablissement_adresse_1 ADD CONSTRAINT fk_etablissement_adresse_1__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_periode ADD CONSTRAINT uk_etablissement_periode UNIQUE (etablissement_id, debut_date);
ALTER TABLE etablissement_periode ADD CONSTRAINT fk_etablissement_periode__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_exercice ADD CONSTRAINT uk_etablissement_exercice UNIQUE (etablissement_id, cloture_date);
ALTER TABLE etablissement_exercice ADD CONSTRAINT fk_etablissement_exercice__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe ADD CONSTRAINT fk_equipe__territoire_type FOREIGN KEY (territoire_type_id) REFERENCES territoire_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe ADD CONSTRAINT fk_equipe__equipe_type FOREIGN KEY (equipe_type_id) REFERENCES equipe_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE deveco ADD CONSTRAINT fk_deveco__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE deveco ADD CONSTRAINT fk_deveco__compte FOREIGN KEY (compte_id) REFERENCES compte(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_favori ADD CONSTRAINT fk_etablissement_favori__deveco FOREIGN KEY (deveco_id) REFERENCES deveco(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_favori ADD CONSTRAINT fk_etablissement_favori__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE zonage ADD CONSTRAINT fk_zonage__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__commune ADD CONSTRAINT fk_equipe__commune__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__commune ADD CONSTRAINT fk_equipe__commune__commune FOREIGN KEY (commune_id) REFERENCES commune(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__epci ADD CONSTRAINT fk_equipe__epci__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__epci ADD CONSTRAINT fk_equipe__epci__epci FOREIGN KEY (epci_id) REFERENCES epci(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__metropole ADD CONSTRAINT fk_equipe__metropole__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__metropole ADD CONSTRAINT fk_equipe__metropole__metropole FOREIGN KEY (metropole_id) REFERENCES metropole(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__petr ADD CONSTRAINT fk_equipe__petr__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__petr ADD CONSTRAINT fk_equipe__petr__petr FOREIGN KEY (petr_id) REFERENCES petr(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__departement ADD CONSTRAINT fk_equipe__departement__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__departement ADD CONSTRAINT fk_equipe__departement__departement FOREIGN KEY (departement_id) REFERENCES departement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__region ADD CONSTRAINT fk_equipe__region__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__region ADD CONSTRAINT fk_equipe__region__region FOREIGN KEY (region_id) REFERENCES region(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe_import ADD CONSTRAINT uk_equipe_import_equipe UNIQUE (equipe_id);
ALTER TABLE equipe_import ADD CONSTRAINT fk_equipe_import__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etiquette ADD CONSTRAINT fk_etiquette__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etiquette ADD CONSTRAINT fk_etiquette__etiquette_type FOREIGN KEY (etiquette_type_id) REFERENCES etiquette_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etiquette ADD CONSTRAINT uk_etiquette UNIQUE (etiquette_type_id, nom, equipe_id);
ALTER TABLE echange ADD CONSTRAINT fk_echange__echange_type FOREIGN KEY (echange_type_id) REFERENCES echange_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE echange ADD CONSTRAINT fk_echange__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE echange ADD CONSTRAINT fk_echange__deveco FOREIGN KEY (deveco_id) REFERENCES deveco(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE demande ADD CONSTRAINT fk_demande__demande_type FOREIGN KEY (demande_type_id) REFERENCES demande_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE demande ADD CONSTRAINT fk_demande__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE echange__demande ADD CONSTRAINT fk_echange__demande__echange FOREIGN KEY (echange_id) REFERENCES echange(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE echange__demande ADD CONSTRAINT fk_echange__demande__demande FOREIGN KEY (demande_id) REFERENCES demande(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE rappel ADD CONSTRAINT fk_rappel__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE personne ADD CONSTRAINT fk_personne__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__etiquette ADD CONSTRAINT fk_equipe__etablissement__etiquette__etiquette FOREIGN KEY (etiquette_id) REFERENCES etiquette(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__etiquette ADD CONSTRAINT fk_equipe__etablissement__etiquette__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__etiquette ADD CONSTRAINT fk_equipe__etablissement__etiquette__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__echange ADD CONSTRAINT fk_equipe__etablissement__echange__echange FOREIGN KEY (echange_id) REFERENCES echange(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__echange ADD CONSTRAINT fk_equipe__etablissement__echange__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__echange ADD CONSTRAINT fk_equipe__etablissement__echange__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__demande ADD CONSTRAINT fk_equipe__etablissement__demande__demande FOREIGN KEY (demande_id) REFERENCES demande(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__demande ADD CONSTRAINT fk_equipe__etablissement__demande__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__demande ADD CONSTRAINT fk_equipe__etablissement__demande__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__rappel ADD CONSTRAINT fk_equipe__etablissement__rappel__rappel FOREIGN KEY (rappel_id) REFERENCES rappel(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__rappel ADD CONSTRAINT fk_equipe__etablissement__rappel__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE equipe__etablissement__rappel ADD CONSTRAINT fk_equipe__etablissement__rappel__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE contact ADD CONSTRAINT fk_contact__personne FOREIGN KEY (personne_id) REFERENCES personne(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE contact ADD CONSTRAINT fk_contact__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE contact ADD CONSTRAINT fk_contact__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation ADD CONSTRAINT fk_etablissement_creation__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation ADD CONSTRAINT fk_etablissement_creation__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__etiquette ADD CONSTRAINT fk_etablissement_creation__etiquette__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__etiquette ADD CONSTRAINT fk_etablissement_creation__etiquette__etiquette FOREIGN KEY (etiquette_id) REFERENCES etiquette(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__echange ADD CONSTRAINT fk_etablissement_creation__echange__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__echange ADD CONSTRAINT fk_etablissement_creation__echange__echange FOREIGN KEY (echange_id) REFERENCES echange(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation_transformation ADD CONSTRAINT fk_etablissement_creation_transfo__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation_transformation ADD CONSTRAINT fk_etablissement_creation_transfo__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation_transformation ADD CONSTRAINT fk_etablissement_creation_transfo__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__demande ADD CONSTRAINT fk_etablissement_creation__demande__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__demande ADD CONSTRAINT fk_etablissement_creation__demande__demande FOREIGN KEY (demande_id) REFERENCES demande(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE createur ADD CONSTRAINT fk_createur__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE createur ADD CONSTRAINT fk_createur__personne FOREIGN KEY (personne_id) REFERENCES personne(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__rappel ADD CONSTRAINT fk_etablissement_creation__rappel__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_creation__rappel ADD CONSTRAINT fk_etablissement_creation__rappel__rappel FOREIGN KEY (rappel_id) REFERENCES rappel(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE personne_adresse ADD CONSTRAINT fk_personne_adresse__personne FOREIGN KEY (personne_id) REFERENCES personne(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE local ADD CONSTRAINT fk_local__local_statut_type FOREIGN KEY (local_statut_type_id) REFERENCES local_statut_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE local ADD CONSTRAINT fk_local__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE local__local_type ADD CONSTRAINT fk_local__local_type__local FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE local__local_type ADD CONSTRAINT fk_local__local_type__local_type FOREIGN KEY (local_type_id) REFERENCES local_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE local__etiquette ADD CONSTRAINT fk_local__etiquette__local FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE local__etiquette ADD CONSTRAINT fk_local__etiquette__etiquette FOREIGN KEY (etiquette_id) REFERENCES etiquette(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE local_adresse ADD CONSTRAINT fk_local_adresse__local  FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE proprietaire_personne ADD CONSTRAINT fk_proprietaire_personne__local FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE proprietaire_personne ADD CONSTRAINT fk_proprietaire_personne__personne FOREIGN KEY (personne_id) REFERENCES personne(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE proprietaire_etablissement ADD CONSTRAINT fk_proprietaire_etablissement__local FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE proprietaire_etablissement ADD CONSTRAINT fk_proprietaire_etablissement__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement ADD CONSTRAINT fk_evenement__evenement_type FOREIGN KEY (evenement_type_id) REFERENCES evenement_type(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement ADD CONSTRAINT fk_evenement__compte FOREIGN KEY (compte_id) REFERENCES compte(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__local ADD CONSTRAINT fk_evenement__local__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__local ADD CONSTRAINT fk_evenement__local__local FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__personne ADD CONSTRAINT fk_evenement__personne__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__personne ADD CONSTRAINT fk_evenement__personne__personne FOREIGN KEY (personne_id) REFERENCES personne(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etablissement ADD CONSTRAINT fk_evenement__etablissement__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etablissement ADD CONSTRAINT fk_evenement__etablissement__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__entreprise ADD CONSTRAINT fk_evenement__entreprise__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__entreprise ADD CONSTRAINT fk_evenement__entreprise__entreprise FOREIGN KEY (entreprise_id) REFERENCES entreprise(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__echange ADD CONSTRAINT fk_evenement__echange__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__echange ADD CONSTRAINT fk_evenement__echange__echange FOREIGN KEY (echange_id) REFERENCES echange(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__demande ADD CONSTRAINT fk_evenement__demande__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__demande ADD CONSTRAINT fk_evenement__demande__demande FOREIGN KEY (demande_id) REFERENCES demande(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__rappel ADD CONSTRAINT fk_evenement__rappel__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__rappel ADD CONSTRAINT fk_evenement__rappel__rappel FOREIGN KEY (rappel_id) REFERENCES rappel(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette ADD CONSTRAINT fk_evenement__etiquette__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette ADD CONSTRAINT fk_evenement__etiquette__etiquette FOREIGN KEY (etiquette_id) REFERENCES etiquette(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette__etablissement ADD CONSTRAINT fk_evenement__etiquette__etablissement__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette__etablissement ADD CONSTRAINT fk_evenement__etiquette__etablissement__etiquette FOREIGN KEY (etiquette_id) REFERENCES etiquette(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette__etablissement ADD CONSTRAINT fk_evenement__etiquette__etablissement__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette__etablissement ADD CONSTRAINT fk_evenement__etiquette__etablissement__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette__etablissement_creation ADD CONSTRAINT fk_evenement__etiquette__etablissement_creation__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette__etablissement_creation ADD CONSTRAINT fk_evenement__etiquette__etablissement_creation__etiquette FOREIGN KEY (etiquette_id) REFERENCES etiquette(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etiquette__etablissement_creation ADD CONSTRAINT fk_evenement__etiquette__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__contact ADD CONSTRAINT fk_evenement__contact__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__contact ADD CONSTRAINT fk_evenement__contact__contact FOREIGN KEY (equipe_id, etablissement_id, personne_id) REFERENCES contact(equipe_id, etablissement_id, personne_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etablissement_creation ADD CONSTRAINT fk_evenement__etablissement_creation__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__etablissement_creation ADD CONSTRAINT fk_evenement__etablissement_creation__etablissement_creation FOREIGN KEY (etablissement_creation_id) REFERENCES etablissement_creation(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__equipe_etablissement ADD CONSTRAINT fk_evenement__equipe_etablissement__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__equipe_etablissement ADD CONSTRAINT fk_evenement__equipe_etablissement__equipe FOREIGN KEY (equipe_id) REFERENCES equipe(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__equipe_etablissement ADD CONSTRAINT fk_evenement__equipe_etablissement__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__proprietaire_personne ADD CONSTRAINT fk_evenement__proprietaire_personne__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__proprietaire_personne ADD CONSTRAINT fk_evenement__proprietaire_personne__local FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__proprietaire_personne ADD CONSTRAINT fk_evenement__proprietaire_personne__personne FOREIGN KEY (personne_id) REFERENCES personne(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__proprietaire_etablissement ADD CONSTRAINT fk_evenement__proprietaire_etablissement__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__proprietaire_etablissement ADD CONSTRAINT fk_evenement__proprietaire_etablissement__local FOREIGN KEY (local_id) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__proprietaire_etablissement ADD CONSTRAINT fk_evenement__proprietaire_etablissement__etablissement FOREIGN KEY (etablissement_id) REFERENCES etablissement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__compte ADD CONSTRAINT fk_evenement__compte__evenement FOREIGN KEY (evenement_id) REFERENCES evenement(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE evenement__compte ADD CONSTRAINT fk_evenement__compte__compte FOREIGN KEY (compte_id) REFERENCES compte(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE stats ADD CONSTRAINT uk_stats__commune_date UNIQUE (commune_id, date);
ALTER TABLE recherche ADD CONSTRAINT fk_recherche__deveco FOREIGN KEY (deveco_id) REFERENCES deveco(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE geo_token ADD CONSTRAINT fk_geo_token__deveco FOREIGN KEY (deveco_id) REFERENCES deveco(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE etablissement_adresse_1 ADD CONSTRAINT uk_etablissement_adresse_1__etablissement_id UNIQUE (etablissement_id);
ALTER TABLE local_adresse ADD CONSTRAINT uk_local_adresse__etablissement_id UNIQUE (local_id);
ALTER TABLE personne_adresse ADD CONSTRAINT uk_personne_adresse__personne_id UNIQUE (personne_id);

