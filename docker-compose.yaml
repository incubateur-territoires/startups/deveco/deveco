services:
  e2e:
    build:
      context: ./e2e/
      dockerfile: Dockerfile_test
    container_name: deveco_e2e_runner
    command: ["yarn", "test"]
    networks:
      - deveco-e2e-network
    depends_on:
      front:
        condition: service_started
      back:
        condition: service_started
    env_file:
      - ./.env
    volumes:
      - ./e2e/output/:/e2e/output/
  migrate:
    build:
      context: ./api/
      dockerfile: Dockerfile_migrate
      args:
        SOURCES_PATH: "_full"
    container_name: deveco_e2e_migrate
    command: ["./migrate.sh"]
    networks:
      - deveco-e2e-network
    env_file:
      - ./.env
    environment:
      TARGET_DB: ""
    depends_on:
      db:
        condition: service_healthy
  migrate_fixtures:
    build:
      context: ./api/
      dockerfile: Dockerfile_migrate
      args:
        SOURCES_PATH: "_fixtures"
    container_name: deveco_e2e_migrate_fixtures
    command: ["./migrate-and-dump.sh"]
    networks:
      - deveco-e2e-network
    env_file:
      - ./.env
    environment:
      TARGET_DB: "_SEED"
    volumes:
      - ./api/data:/data
    depends_on:
      db:
        condition: service_healthy
  front:
    build:
      context: ./app/
      dockerfile: Dockerfile_test
    container_name: deveco_e2e_front
    env_file:
      - ./.env
    environment:
      NGINX_ENVSUBST_TEMPLATE_SUFFIX: ".template.conf"
    ports:
      - "${APP_PORT}:${APP_PORT}"
    networks:
      - deveco-e2e-network
    volumes:
      - ./app/:/etc/nginx/templates
    command: ["nginx", "-g", "daemon off;"]
    depends_on:
      back:
        condition: service_started
  back:
    build:
      context: ./api/
      dockerfile: Dockerfile_test
    container_name: deveco_e2e_back
    command: ["dist/index.js"]
    depends_on:
      elasticsearch:
        condition: service_started
      db:
        condition: service_healthy
    env_file:
      - ./.env
    environment:
      - NODE_ENV=test
      - NODE_TLS_REJECT_UNAUTHORIZED=0 # pour le certificat autosigné côté ElasticSearch
    networks:
      - deveco-e2e-network
    ports:
      - "${API_PORT}:4000"
      - "${MONITORING_PORT}:${MONITORING_PORT}"
    mem_limit: 4g
    healthcheck:
      test:
        [
          "CMD",
          "wget",
          "-o",
          "test",
          "http://${API_HOST}:${MONITORING_PORT}/metrics",
        ]
      interval: 5s
      timeout: 5s
      retries: 10
      start_period: 5s
  db:
    image: postgis/postgis:15-3.5-alpine
    container_name: deveco_e2e_db
    env_file:
      - ./.env
    ports:
      - "${EXTERNAL_POSTGRES_PORT}:5432"
    volumes:
      - ./api/data/seed/pre-init.sql:/docker-entrypoint-initdb.d/11-pre-init.sql
      - ./api/data/seed/init.sql:/docker-entrypoint-initdb.d/12-init.sql
      - ./api/data/seed/init-seed.sh:/docker-entrypoint-initdb.d/20-init-seed.sh
      - deveco-e2e-pgdata:/var/lib/postgresql/data
    networks:
      - deveco-e2e-network
    restart: always
    shm_size: 1g
    healthcheck:
      test:
        [
          "CMD",
          "psql",
          "-U",
          "$POSTGRES_USER",
          "-h",
          "localhost",
          "-d",
          "$POSTGRES_DB",
          "-p",
          "5432",
          "-c",
          "SELECT 1;",
        ]
      interval: 10s
      timeout: 10s
      retries: 10
      start_period: 20s
  seed:
    build:
      context: ./api/
      dockerfile: Dockerfile_seeder
    command: ["./seed.sh"]
    container_name: deveco_e2e_seed
    networks:
      - deveco-e2e-network
    depends_on:
      db:
        condition: service_healthy
    env_file:
      - ./.env
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:7.10.2
    container_name: deveco_e2e_elasticsearch
    env_file:
      - ./.env
    environment:
      - discovery.type=single-node # Start a cluster of one node only
      - bootstrap.memory_lock=true # Disable JVM heap memory swapping
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m" # Set min and max JVM heap sizes to at least 50% of system RAM
    ulimits:
      memlock:
        soft: -1 # Set memlock to unlimited (no soft or hard limit)
        hard: -1
      nofile:
        soft: 65536 # Maximum number of open files for the elasticsearch user - set to at least 65536
        hard: 65536
    ports:
      - "${EXTERNAL_ELASTICSEARCH_PORT}:9200"
      - "${ELASTICSEARCH_NODES_PORT}:9300"
    volumes:
      - deveco-e2e-esdata:/usr/share/elasticsearch/data
    networks:
      - deveco-e2e-network
    restart: always
    shm_size: 1g

volumes:
  deveco-e2e-pgdata:
    name: deveco-e2e-pgdata
  deveco-e2e-esdata:
    name: deveco-e2e-esdata

networks:
  deveco-e2e-network:
    external: false
    name: deveco-e2e-network
